#!/usr/bin/python3
import base64;
import os;

files = os.listdir("webpki-certs");
files = list(filter(lambda x: len(x) == 64, files));
files = sorted(files);

certdata = b"";
for fname in files:
	data = open("webpki-certs/"+fname,"rb");
	cert = data.read();
	certdata += cert;
fp=open("webpki-certs.mder","wb");
fp.write(certdata);
