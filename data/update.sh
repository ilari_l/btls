#!/bin/bash
cd libs/publiccalist
./update.py
cd ../..
cd libs/certificate-transparency/builtin
./update.py
cd ../../..
cd utils/httpdemux2/hsts
./update-hsts.py
cd ../../..
cd data
./update-mder.py
cd ..
