#!/bin/sh
cd data &&
wget --output-document=roots.csv         https://ccadb-public.secure.force.com/mozilla/IncludedCACertificateReportPEMCSV &&
wget --output-document=intermediates.csv https://ccadb-public.secure.force.com/mozilla/PublicAllIntermediateCertsWithPEMCSV &&
wget --output-document=google-logs2.json https://www.gstatic.com/ct/log_list/v2/log_list.json &&
wget --output-document=google-logs3.json https://www.gstatic.com/ct/log_list/v3/log_list.json &&
wget --output-document=apple-logs.json   https://valid.apple.com/ct/log_list/current_log_list.json --no-check-certificate &&
wget --output-document=hsts.inc          https://hg.mozilla.org/mozilla-central/raw-file/tip/security/manager/ssl/nsSTSPreloadList.inc &&
wget --output-document=revoke.dat        https://firefox.settings.services.mozilla.com/v1/buckets/blocklists/collections/certificates/records &&
date +%s >timestamp.txt
touch extra.pem
python3 update-certs.py
