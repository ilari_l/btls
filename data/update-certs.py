#!/usr/bin/python3
import base64;
import hashlib;

data = open("roots.csv","rb");
fp1 = data.read().decode("utf-8");
data = open("intermediates.csv","rb");
fp2 = data.read().decode("utf-8");
data = open("extra.pem","rb");
fp3 = data.read().decode("utf-8");
pemcsv = fp1+fp2+fp3;
in_cert = False;
for line in pemcsv.splitlines():
	if in_cert:
		if line.startswith("-----END CERTIFICATE-----"):
			cert_data = base64.b64decode(pems);
			name = hashlib.sha256(cert_data).hexdigest();
			fp=open("webpki-certs/"+name,"wb");
			fp.write(cert_data);
			in_cert = False;
		else:
			pems += line+"\n";
	elif line.endswith("-----BEGIN CERTIFICATE-----"):
		pems = "";
		in_cert = True;

