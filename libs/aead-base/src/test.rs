#![allow(deprecated)]
use super::InputOutputBuffer;

#[test]
fn buffers_single()
{
	let mut x = [0;256];
	let xptrm = x.as_mut_ptr();
	let xptrc = x.as_ptr();
	let mut buf = InputOutputBuffer::Single(&mut x[..], 128);
	let rawout = buf.raw_output(256).unwrap();
	let rawin = buf.raw_input().unwrap();
	assert_eq!(rawout.0, xptrm);
	assert_eq!(rawout.1, 256);
	assert_eq!(rawin.0, xptrc);
	assert_eq!(rawin.1, 128);
}

#[test]
fn buffers_single2()
{
	let mut x = [0;256];
	let xptrm = x.as_mut_ptr();
	let xptrc = x.as_ptr();
	let mut buf = InputOutputBuffer::Single(&mut x[..], 256);
	let rawout = buf.raw_output(256).unwrap();
	let rawin = buf.raw_input().unwrap();
	assert_eq!(rawout.0, xptrm);
	assert_eq!(rawout.1, 256);
	assert_eq!(rawin.0, xptrc);
	assert_eq!(rawin.1, 256);
}

#[test]
fn buffers_single_invalid()
{
	let mut x = [0;256];
	let xptrm = x.as_mut_ptr();
	let mut buf = InputOutputBuffer::Single(&mut x[..], 257);
	let rawout = buf.raw_output(256).unwrap();
	buf.raw_input().unwrap_err();
	assert_eq!(rawout.0, xptrm);
	assert_eq!(rawout.1, 256);
}

#[test]
fn buffers_double()
{
	let mut x = [0;256];
	let y = [0;257];
	let xptr = x.as_mut_ptr();
	let yptr = y.as_ptr();
	let mut buf = InputOutputBuffer::Double(&mut x[..], &y[..]);
	let rawout = buf.raw_output(256).unwrap();
	let rawin = buf.raw_input().unwrap();
	assert_eq!(rawout.0, xptr);
	assert_eq!(rawout.1, 256);
	assert_eq!(rawin.0, yptr);
	assert_eq!(rawin.1, 257);
}
