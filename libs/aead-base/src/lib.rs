//!AEAD encryption/decryption, base definitions
//!
//!This crate contains the base definitions for  encrypting and decrypting data using various AEAD (Authenticated
//!Encryption with Associated Data) encryption methods.
//!
//!It is intended that only [`btls-aux-aead`] crate, and the crates implementing low-level encryption backends
//!depend on this crate. This is a seperate crate due to the latter otherwise being circular dependency (as
//!low-level encryption backends are dependicies of the AEAD crate), which Rust can not deal with.
//!
//![`btls-aux-aead`]: ../btls_aux_aead/index.html
#![deny(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]

use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_memory::AliasReadSlice;
use btls_aux_memory::AliasReadWriteSlice;
use btls_aux_memory::AliasWriteSlice;
use btls_aux_memory::bound_buffer_size;
use btls_aux_memory::NonNull;
use btls_aux_memory::NonNullMut;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::mem::size_of;

///Error from encryption or decryption operation.
///
///This structure implements `Display` for printing human-readable error message using the `{}` format placeholder.
#[derive(Clone, Debug, PartialEq, Eq)]
#[non_exhaustive]
pub enum AeadError
{
	///The buffer passed to operation claims to be larger than it can be.
	///
	///More specifically, the buffer passed is [`InputOutputBuffer::Single`], and the length field contains value
	///greater than the length of the slice.
	///
	/// # Arguments:
	///
	/// 1. `claimed_size`: The claimed size of the buffer.
	/// 1. `available_size`: The available size of the buffer.
	///
	/// # Notes:
	///
	/// * Most commonly, this error condition is checked by [`InputOutputBuffer::buffers()`] or
	///[`InputOutputBuffer::buffers2()`] methods.
	///
	///[`InputOutputBuffer::Single`]: enum.InputOutputBuffer.html
	///[`InputOutputBuffer::buffers()`]: enum.InputOutputBuffer.html#method.buffers
	///[`InputOutputBuffer::buffers2()`]: enum.InputOutputBuffer.html#method.buffers2
	BadBuffers(usize, usize),
	///The key passed to operation is of wrong length.
	///
	/// # Arguments:
	///
	/// 1. `algorithm`: Name of the algorithm used.
	/// 1. `key_size`: The bad key size used.
	///
	/// # Notes:
	///
	/// * Most commonly, this error condition is checked by [`assert_key()`] method.
	///
	///[`assert_key()`]:#method.assert_key
	BadKeyLength(&'static str, usize),
	///The nonce passed to operation is of wrong length.
	///
	/// # Arguments:
	///
	/// 1. `algorithm`: Name of the algorithm used.
	/// 1. `nonce_size`: The bad nonce size used.
	///
	/// # Notes:
	///
	/// * Most commonly, this error condition is checked by [`assert_nonce()`] method.
	///
	///[`assert_nonce()`]:#method.assert_nonce
	BadNonceLength(&'static str, usize),
	///Associated data passed to operation is too big.
	///
	/// # Arguments:
	///
	/// 1. `algorithm`: Name of the algorithm used.
	/// 1. `ad_size`: The too big associated data size used.
	///
	/// # Notes:
	///
	/// * On 32-bit platforms, the assoicated data size is capped at `2GB`, regardless of the algorithm
	///(unless the algorithm is not even capable of `2GB`).
	/// * Most commonly, this error condition is checked by [`assert_ad()`] method.
	///
	///[`assert_ad()`]:#method.assert_ad
	AdTooBig(&'static str, usize),
	///Plaintext passed to operation is too big.
	///
	/// # Arguments:
	///
	/// 1. `algorithm`: Name of the algorithm used.
	/// 1. `pt_size`: The too big plaintext size used.
	///
	/// # Notes:
	///
	/// * On 32-bit platforms, the plaintext size is capped at `2GB`, regardless of the algorithm
	///(unless the algorithm is not even capable of `2GB`).
	/// * Most commonly, this error condition is checked by [`assert_plaintext()`] method.
	///
	///[`assert_plaintext()`]:#method.assert_plaintext
	PlaintextTooBig(&'static str, usize),
	///The passed output buffer has size insufficient space to hold the algorithm output.
	///
	/// # Arguments:
	///
	/// 1. `required`: The output size needed.
	/// 1. `available`: The available output size.
	OutputTooBig(usize, usize),
	///Ciphertext passed to operation is too big.
	///
	/// # Arguments:
	///
	/// 1. `algorithm`: Name of the algorithm used.
	/// 1. `ct_size`: The too big ciphertext size used.
	///
	/// # Notes:
	///
	/// * On 32-bit platforms, the ciphertext size is capped at `2GB`, regardless of the algorithm
	///(unless the algorithm is not even capable of `2GB`).
	/// * Most commonly, this error condition is checked by [`assert_ciphertext()`] method.
	///
	///[`assert_ciphertext()`]:#method.assert_ciphertext
	CiphertextTooBig(&'static str, usize),
	///MAC check failed.
	///
	///The usual causes of this error are:
	///
	/// * The message wrong size to be valid.
	/// * Trying to decrypt with wrong key.
	/// * Trying to decrypt with wrong nonce.
	/// * Associated data or ciphertext have been tampered with.
	///
	/// # Notes:
	///
	/// * Most commonly, this error condition is checked by [`assert_tag_ok()`] method.
	/// * For bad ciphertext size, it is most commonly checked by [`assert_ciphertext()`] method.
	///
	///[`assert_tag_ok()`]:#method.assert_tag_ok
	///[`assert_ciphertext()`]:#method.assert_ciphertext
	MacCheckFailed,
	///Partially overlapping buffers.
	///
	///The operation does not allow partially overlapping buffers.
	PartialBufferOverlap,
	///Internal implementation fault. This is a bug.
	///
	/// # Arguments:
	///
	/// 1. `algorithm`: Name of the algorithm used.
	InternalFault(&'static str),
}

impl AeadError
{
	///Assert that length of key is acceptable.
	///
	/// # Parameters:
	///
	/// * `aname`: Name of the algorithm.
	/// * `key`: The key used.
	/// * `keylen_pred`: Predicate function for key length, returning true if the passed in key length is
	///acceptable, false otherwise.
	///
	/// # Error conditions:
	///
	/// * The predicate function returns false for `key.len()`.
	pub fn assert_key<P>(aname: &'static str, key: &[u8], keylen_pred: P) -> Result<(), AeadError>
		where P: FnOnce(usize) -> bool
	{
		fail_if!(!keylen_pred(key.len()), AeadError::BadKeyLength(aname, key.len()));
		Ok(())
	}
	///Assert that length of nonce is acceptable.
	///
	/// # Parameters:
	///
	/// * `aname`: Name of the algorithm.
	/// * `nonce`: The nonce used.
	/// * `noncelen_pred`: Predicate function for nonce length, returning true if the passed in nonce length is
	///acceptable, false otherwise.
	///
	/// # Error conditions:
	///
	/// * The predicate function returns false for `nonce.len()`.
	pub fn assert_nonce<P>(aname: &'static str, nonce: &[u8], noncelen_pred: P) -> Result<(), AeadError>
		where P: FnOnce(usize) -> bool
	{
		fail_if!(!noncelen_pred(nonce.len()), AeadError::BadNonceLength(aname, nonce.len()));
		Ok(())
	}
	///Assert that length of associated data is acceptable.
	///
	/// # Parameters:
	///
	/// * `aname`: Name of the algorithm.
	/// * `ad`: The associated data used.
	/// * `maxad`: The maximum supported associated data size.
	///
	/// # Error conditions:
	///
	/// * The length of associated data exceeds `maxad`.
	/// * The length of associated data exceeds 2GB on 32-bit systems.
	pub fn assert_ad(aname: &'static str, ad: &[u8], maxad: u64) -> Result<(), AeadError>
	{
		fail_if!(ad.len() > bound_buffer_size(maxad), AeadError::AdTooBig(aname, ad.len()));
		Ok(())
	}
	///Assert that length of plaintext is acceptable.
	///
	/// # Parameters:
	///
	/// * `aname`: Name of the algorithm.
	/// * `ptsize`: The size of plaintext in bytes.
	/// * `expansion`: The expansion of ciphertext in bytes.
	/// * `maxpt`: The maximum supported plaintext size.
	///
	/// # Error conditions:
	///
	/// * The length of plaintext exceeds `maxpt`.
	/// * The length of plaintext exceeds 2GB on 32-bit systems, or 8EB on 64-bit systems.
	/// * The length of ciphertext exceeds 2GB on 32-bit systems, or 8EB on 64-bit systems.
	///
	/// # Return value:
	///
	///Size of the ciphertext in bytes.
	pub fn assert_plaintext(aname: &'static str, ptsize: usize, expansion: usize, maxpt: u64) ->
		Result<usize, AeadError>
	{
		//If this overflows, it is guaranteed that the size check will fail.
		let ctsize = ptsize.saturating_add(expansion);
		//If the resulting ciphertext is too big, then the plaintext is too big. Note that the isize
		//check is for ctsize, not ptsize. This is as the ciphertext must fit into memory.
		fail_if!(ctsize > isize::MAX as usize, AeadError::PlaintextTooBig(aname, ptsize));
		fail_if!(ptsize > bound_buffer_size(maxpt), AeadError::PlaintextTooBig(aname, ptsize));
		Ok(ctsize)
	}
	///Assert that length of ciphertext is acceptable.
	///
	/// # Parameters:
	///
	/// * `aname`: Name of the algorithm.
	/// * `ctsize`: The size of ciphertext in bytes.
	/// * `maxct`: The maximum ciphertext size in bytes.
	/// * `ptsize`: Compute maximum plaintext size for given ciphertext size.
	///
	/// # Error conditions:
	///
	/// * The ciphertext size exceeds `maxct`.
	/// * `ptsize` for given ciphertext size returns `None`.
	///
	/// # Return value:
	///
	///The return value of `ptsize(ctsize)`.
	pub fn assert_ciphertext<P>(aname: &'static str, ctsize: usize, maxct: u64, ptsize: P) ->
		Result<usize, AeadError> where P:Fn(usize) -> Option<usize>
	{
		fail_if!(ctsize > bound_buffer_size(maxct), AeadError::CiphertextTooBig(aname, ctsize));
		let ptsize = ptsize(ctsize).ok_or(AeadError::MacCheckFailed)?;
		Ok(ptsize)
	}
	///Assert MAC check, and zeroize given buffer on failure.
	///
	/// # Parameters:
	///
	/// * `tag_ok`: Is the tag OK?
	/// * `obuf`: The output buffer.
	///
	/// # Error conditions:
	///
	/// * `tag_ok` is false.
	pub fn assert_tag_ok(tag_ok: bool, obuf: &mut [u8]) -> Result<(), AeadError>
	{
		if !tag_ok {
			//This is not "secure" wipe. Secure wipe is not needed because this buffer is presumably
			//used later, and if not, it is okay to leave it as-is, as material left around is no
			//concern.
			for b in obuf.iter_mut() { *b = 0; }
			fail!(AeadError::MacCheckFailed);
		}
		Ok(())
	}
	///Assert buffers do not overlap partially.
	///
	/// # Parameters:
	///
	/// * `output`: The output buffer.
	/// * `input`: The input buffer.
	///
	/// # Error conditions:
	///
	/// * The buffers partially overlap.
	pub fn assert_same_or_disjoint<T:Sized>(output: &AliasWriteSlice<T>, input: &AliasReadSlice<T>) ->
		Result<(), AeadError>
	{
		let instart = input.as_ptr().addr();
		let inend = instart + size_of::<T>() * input.len();
		let outstart = output.as_ptr().addr();
		let outend = outstart + size_of::<T>() * output.len();
		//The OK special cases.
		if instart == outstart || instart == inend || outstart == outend { return Ok(()); }
		//The bad cases.
		fail_if!(inend > outstart && outstart > instart, AeadError::PartialBufferOverlap);
		fail_if!(outend > instart && instart > outstart, AeadError::PartialBufferOverlap);
		//Okay.
		Ok(())
	}
	#[allow(missing_docs)]
	#[deprecated(since="3.2.0", note="Use .assert_ciphertext() instead")]
	pub fn assert_ciphertext_size(ctlen: usize, expansion: usize) -> Result<usize, AeadError>
	{
		ctlen.checked_sub(expansion).ok_or(AeadError::MacCheckFailed)
	}
	///Construct internal error.
	///
	///
	/// # Parameters:
	///
	/// * `aname`: Name of the algorithm.
	///
	/// # Return value:
	///
	///Error object for internal error.
	pub fn internal_fault(aname: &'static str) -> AeadError { AeadError::InternalFault(aname) }
}

impl Display for AeadError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::AeadError::*;
		match self {
			&BadBuffers(claim, avail) => write!(f, "Invalid buffer ({claim} > {avail})"),
			&BadKeyLength(cipher, size) => write!(f, "Bad key length ({size} bytes to {cipher})"),
			&BadNonceLength(cipher, size) => write!(f, "Bad nonce length ({size} bytes to {cipher})"),
			&AdTooBig(cipher, size) => write!(f, "Associated data too big ({size} bytes to {cipher})"),
			&PlaintextTooBig(cipher, size) => write!(f, "Plaintext too big ({size} bytes to {cipher})"),
			&OutputTooBig(limit, size) => write!(f, "Output too big ({size} > {limit})"),
			&CiphertextTooBig(cipher, size) => write!(f, "Ciphertext too big ({size} bytes to {cipher})"),
			&MacCheckFailed => write!(f, "Ciphertext fails MAC check"),
			&PartialBufferOverlap => write!(f, "Partially overlapping buffers not allowed"),
			&InternalFault(cipher) => write!(f, "Internal implementation fault in {cipher}"),
		}
	}
}


///A single input/output buffer, or distinct input and output buffers.
///
///The input and output buffers can be distinct or a single buffer. This is useful, because the rust borrowing
///model does not allow one to have two borrows into the same data if one of the borrows is mutable.
pub enum InputOutputBuffer<'a,'b>
{
	///A single input and output buffer.
	///
	/// # Arguments:
	///
	/// 1. `buffer`: The buffer itself.
	/// 1. `insize`: The length of input buffer. The input buffer is assumed to be the starting part of the
	///buffer. This must be at most the size of the buffer.
	Single(&'a mut [u8], usize),
	///An input and output buffer pair.
	///
	/// # Arguments:
	///
	/// 1. `output`: The output buffer.
	/// 2. `input`: The input buffer.
	Double(&'a mut [u8], &'b [u8]),
}

impl<'a,'b> InputOutputBuffer<'a,'b>
{
	///Get output/input buffer base/size.
	///
	/// # Error conditions:
	///
	/// * The variant is `Single`, and `insize` is greater than `buffer.len()`.
	///
	/// # Return value:
	///
	///`((optr, olen), (iptr, ilen))`, where:
	///
	/// * `optr` is base address of the output buffer. It is guaranteed to point to `olen` writable and
	///initialized bytes.
	/// * `olen` is size of the output buffer in bytes.
	/// * `iptr` is base address of the input buffer. It is guaranteed to point to `ilen` readable and
	///initialized bytes.
	/// * `ilen` is size of the input buffer in bytes.
	/// * `optr` and `iptr` are either the same or do not overlap at all.
	///
	/// # Notes:
	///
	/// * The last condition makes it illegal to cast input to constant slice and output to mutable slice,
	///as the slices would overlap, which is not allowed.
	pub fn buffers(&mut self) -> Result<((*mut u8, usize), (*const u8, usize)), AeadError>
	{
		let ((optr, olen), (iptr, ilen)) = self.buffers2()?;
		Ok(((optr.to_raw(), olen), (iptr.to_raw(), ilen)))
	}
	///Get output/input buffer base/size.
	///
	///The same as [`buffers()`], but with `NonNull` pointers instead of raw pointers.
	///
	///[`buffers()`]:#method.buffers
	pub fn buffers2(&mut self) -> Result<((NonNullMut<u8>, usize), (NonNull<u8>, usize)), AeadError>
	{
		let (optr, iptr, olen, ilen) = match self {
			&mut InputOutputBuffer::Single(ref mut x, y) => {
				let olen = x.len();
				let ilen = y;
				fail_if!(ilen > olen, AeadError::BadBuffers(ilen, olen));
				let optr = NonNullMut::new_slice_base(x);
				(optr, optr.to_const(), olen, ilen)
			},
			&mut InputOutputBuffer::Double(ref mut x, ref y) => {
				let olen = x.len();
				let ilen = y.len();
				(NonNullMut::new_slice_base(x), NonNull::new_slice_base(y), olen, ilen)
			},
		};
		Ok(((optr, olen), (iptr, ilen)))
	}
	///Get output/input buffer base/size.
	///
	///The same as [`buffers2()`], but with `AliasReadSlice` and `AliasWriteSlice` instead of raw pointers.
	///
	///[`buffers2()`]:#method.buffers2
	pub fn buffers3<'c>(&'c mut self) -> Result<(AliasWriteSlice<'c, u8>, AliasReadSlice<'c, u8>), AeadError>
	{
		let (output, input) = match self {
			&mut InputOutputBuffer::Single(ref mut x, y) => {
				let tlen = x.len();
				let xput = AliasReadWriteSlice::new(x);
				let (mut x, y2) = xput.fork();
				x.truncate(y).map_err(|_|{
					AeadError::BadBuffers(y, tlen)
				})?;
				(y2, x)
			},
			&mut InputOutputBuffer::Double(ref mut x, ref y) => {
				let x = AliasWriteSlice::new(x);
				let y = AliasReadSlice::new(y);
				(x, y)
			},
		};
		Ok((output, input))
	}
	#[allow(missing_docs)]
	#[deprecated(since="3.2.0", note="Use .buffers() instead")]
	pub fn raw_output(&mut self, needed: usize) -> Result<(*mut u8, usize), AeadError>
	{
		let (ptr, len) = match self {
			&mut InputOutputBuffer::Single(ref mut x, _) => (x.as_mut_ptr(), x.len()),
			&mut InputOutputBuffer::Double(ref mut x, _) => (x.as_mut_ptr(), x.len()),
		};
		let len = bound_buffer_size(len);
		fail_if!(needed > len, AeadError::OutputTooBig(needed, len));
		Ok((ptr, len))
	}
	#[allow(missing_docs)]
	#[deprecated(since="3.2.0", note="Use .buffers() instead")]
	pub fn raw_input_pt(&mut self, aname: &'static str, trailerlen: usize, max_pt: u64) ->
		Result<(*const u8, usize), AeadError>
	{
		let (ptr, len) = self.raw_input()?;
		//The code below is guaranteed to fail for maximum usize.
		let rlen = len.saturating_add(trailerlen);
		fail_if!(rlen > bound_buffer_size(max_pt), AeadError::PlaintextTooBig(aname, len));
		Ok((ptr, len))
	}
	#[allow(missing_docs)]
	#[deprecated(since="3.2.0", note="Use .buffers() instead")]
	pub fn raw_input_ct(&mut self, aname: &'static str, max_ct: u64) -> Result<(*const u8, usize), AeadError>
	{
		let (ptr, len) = self.raw_input()?;
		fail_if!(len > bound_buffer_size(max_ct), AeadError::CiphertextTooBig(aname, len));
		Ok((ptr, len))
	}
	fn raw_input(&mut self) -> Result<(*const u8, usize), AeadError>
	{
		Ok(match self {
			&mut InputOutputBuffer::Single(ref mut x, y) => {
				fail_if!(y > x.len(), AeadError::BadBuffers(y, x.len()));
				(x.as_mut_ptr(), y)
			},
			&mut InputOutputBuffer::Double(_, x) => (x.as_ptr(), x.len()),
		})
	}
}

#[allow(missing_docs)]
#[deprecated(since="3.2.0", note="Use AeadError assert_plaintext()/assert_ciphertext() instead")]
pub fn size_at_most(x: usize, y: u64) -> bool
{
	x <= bound_buffer_size(y)
}

///A low-level AEAD encryptor/decryptor.
///
///The types implementing this trait hold encryption and decryption keys (a context) for the AEAD
///(Authenticated Encryption with Associated Data) algorithm it is for.
///
///Also note that unlike the high-level contexts in [`btls-aux-aead`](../btls_aux_aead/index.html) crate, the
///low-level contexts do have their own associated constructor and are unified for encryption and decryption.
pub trait LowLevelAeadContext: Sized+Send
{
	///Create a low-level encryption/decryption context for this type, using specified key.
	///
	/// # Parameters:
	///
	/// * `key`: The key to use.
	///
	/// # Error conditions:
	///
	/// * The key is of wrong length.
	/// * The algorithm fails its POST (Power-On Self-Test).
	///
	/// # Return value:
	///
	///The newly created context.
	fn new_context(key: &[u8]) -> Result<Self, AeadError>;
	///Encrypt data using AEAD.
	///
	///The trailer section acts as a second section of plaintext, logically concatenated with the first part in
	///the plaintext buffer. The trailer section is located in the ciphertext buffer, at offset equal to number
	///of bytes in the plaintext buffer. This is useful for handling built-in padding, e.g., in `TLS 1.3`.
	///
	/// # Parameters:
	///
	/// * `nonce`: The nonce to use.
	/// * `ad`: The associated data to use.
	/// * `output`: The ciphertext output buffer.
	/// * `input`: The plaintext input buffer.
	/// * `inputlength`: The length of input in bytes.
	/// * `trailerlen`: The length of trailer section.
	///
	/// # Error conditions:
	///
	/// * The nonce is not of acceptable length.
	/// * The associated data is too big.
	/// * The input (plaintext) is too big.
	///
	/// # Return value:
	///
	///Number of bytes of ciphertext produced.
	///
	/// # Safety:
	///
	/// * `output` must point to sufficient writable bytes. Use [`encrypted_size()`] function to calculate
	///the amount of space needed.
	/// * `input` must point to `inputlength` readable and initialized bytes.
	/// * `input` and `output` must either be the same or not overlap.
	///
	///[`encrypted_size()`]: #tymethod.encrypted_size
	unsafe fn encrypt3(&self, nonce: &[u8], ad: &[u8], output: *mut u8, input: *const u8, inputlength: usize,
		trailerlen: usize) -> Result<usize, AeadError>;
	///Decrypt data using AEAD.
	///
	/// # Parameters:
	///
	/// * `nonce`: The nonce to use.
	/// * `ad`: The associated data to use.
	/// * `output`: The plaintext output buffer.
	/// * `input`: The ciphertext input buffer.
	/// * `inputlength`: The length of input in bytes.
	///
	/// # Error conditions:
	///
	/// * The nonce is not of acceptable length.
	/// * The associated data is too big.
	/// * The input (ciphertext) is too big.
	/// * The decryption fails.
	///
	/// # Return value:
	///
	///Number of bytes of plaintext produced.
	///
	/// # Safety:
	///
	/// * `output` must point to sufficient writable bytes. Use [`max_plaintext_size()`] function to calculate
	///the amount of space needed.
	/// * `input` must point to `inputlength` readable and initialized bytes.
	/// * `input` and `output` must either be the same or not overlap.
	///
	///[`max_plaintext_size()`]: #tymethod.max_plaintext_size
	unsafe fn decrypt3(&self, nonce: &[u8], ad: &[u8], output: *mut u8, input: *const u8, inputlength: usize) ->
		Result<usize, AeadError>;
	///Get size of encryption.
	///
	/// # Parameters:
	///
	/// * `size`: Size of the plaintext.
	///
	/// # Error conditions:
	///
	/// * The plaintext size is too big.
	///
	/// # Return value:
	///
	///Size of ciphertext in bytes.
	fn encrypted_size(&self, size: usize) -> Option<usize>;
	///Get maximum size of plaintext that fits in specified ciphertext size.
	///
	/// # Parameters:
	///
	/// * `size`: Size of the ciphertext.
	///
	/// # Error conditions:
	///
	/// * The ciphertext size is not possible for the algorithm.
	///
	/// # Return value:
	///
	///The maximum possible size of plaintext in bytes.
	///
	/// # Notes:
	///
	/// * If the encryption algorithm is blocked, the plaintext size for given ciphertext size might vary. In
	///that case, this gives the largest possible value.
	/// * The largest possible plaintext size can be assumed to be at most the ciphertext size.
	fn max_plaintext_size(&self, size: usize) -> Option<usize>;
}

#[cfg(test)]
mod test;
