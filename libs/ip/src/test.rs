use crate::decode_ipv4;
use crate::decode_ipv6;
use crate::IpAddress;
use crate::IpAddressBlock;
use std::str::FromStr;
use std::string::ToString;


#[cfg(feature="stdipaddr")]
mod stdipaddr
{
	use crate::IpAddress;
	use crate::IpAddressBlock;
	use std::net::IpAddr;
	use std::net::Ipv4Addr;
	use std::net::Ipv6Addr;
	use std::str::FromStr;
	use std::string::ToString;
	#[test]
	fn test_stdip_ipaddrblock()
	{
		assert_eq!(&IpAddressBlock::from(IpAddr::from_str("1:2:3:4:5:6:7:8").unwrap()).to_string(),
			"1:2:3:4:5:6:7:8");
		assert_eq!(&IpAddressBlock::from(Ipv6Addr::from_str("1:2:3:4:5:6:7:8").unwrap()).to_string(),
			"1:2:3:4:5:6:7:8");
		assert_eq!(&IpAddressBlock::from(Ipv4Addr::from_str("1.2.3.4").unwrap()).to_string(),
			"1.2.3.4");
	}

	#[test]
	fn test_stdip_ipaddr()
	{
		let x: IpAddr = IpAddress::from(IpAddr::from_str("1:2:3:4:5:6:7:8").unwrap()).into();
		assert_eq!(&IpAddress::from(x).to_string(), "1:2:3:4:5:6:7:8");
		let x: IpAddr = IpAddress::from(Ipv6Addr::from_str("1:2:3:4:5:6:7:8").unwrap()).into();
		assert_eq!(&IpAddress::from(x).to_string(), "1:2:3:4:5:6:7:8");
		let x: IpAddr = IpAddress::from(Ipv4Addr::from_str("1.2.3.4").unwrap()).into();
		assert_eq!(&IpAddress::from(x).to_string(), "1.2.3.4");
	}
}

#[test]
fn test_ipaddrblock_from_ipaddr()
{
	assert_eq!(&IpAddressBlock::from(IpAddress::from_str("1:2:3:4:5:6:7:8").unwrap()).to_string(),
		"1:2:3:4:5:6:7:8");
	assert_eq!(&IpAddressBlock::from(IpAddress::from_str("1.2.3.4").unwrap()).to_string(),
		"1.2.3.4");
}

#[test]
fn test_ipaddrblock_from_ip()
{
	assert_eq!(&IpAddressBlock::from_ipv6([1,2,3,4,5,6,7,8]).to_string(), "1:2:3:4:5:6:7:8");
	assert_eq!(&IpAddressBlock::from_ipv4([1,2,3,4]).to_string(), "1.2.3.4");
}

#[test]
fn test_ipaddrblock_from_ip_mask_and_masklen()
{
	assert_eq!(&IpAddressBlock::from_ipv6_mask([1,2,3,4,5,6,7,8], [0xfff0,0,0,0,0,0,0,1]).to_string(),
		"1:2:3:4:5:6:7:8/fff0::1");
	assert_eq!(&IpAddressBlock::from_ipv4_mask([1,2,3,4], [255,255,0,1]).to_string(), "1.2.3.4/255.255.0.1");
	assert_eq!(&IpAddressBlock::from_ipv6_mask([1,2,3,4,5,6,7,8], [0xfff0,0,0,0,0,0,0,0]).to_string(),
		"1:2:3:4:5:6:7:8/12");
	assert_eq!(&IpAddressBlock::from_ipv4_mask([1,2,3,4], [255,255,0,0]).to_string(), "1.2.3.4/16");
	assert_eq!(&IpAddressBlock::from_ipv6_masklen([1,2,3,4,5,6,7,8], 12).to_string(),
		"1:2:3:4:5:6:7:8/12");
	assert_eq!(&IpAddressBlock::from_ipv4_masklen([1,2,3,4],16).to_string(), "1.2.3.4/16");
	for i in 0..128 {
		assert_eq!(&IpAddressBlock::from_ipv6_masklen([1,2,3,4,5,6,7,8], i).to_string(),
			&format!("1:2:3:4:5:6:7:8/{i}"));
	}
	for i in 0..32 {
		assert_eq!(&IpAddressBlock::from_ipv4_masklen([1,2,3,4], i).to_string(),
			&format!("1.2.3.4/{i}"));
	}
	assert_eq!(&IpAddressBlock::from_ipv4_masklen([1,2,3,4], 32).to_string(), "1.2.3.4");
	assert_eq!(&IpAddressBlock::from_ipv6_masklen([1,2,3,4,5,6,7,8], 128).to_string(),
		"1:2:3:4:5:6:7:8");
}

#[test]
fn test_ipaddrblock_contains()
{
	let v6_2400_12 = IpAddressBlock::from_ipv6_masklen([0x2400,0,0,0,0,0,0,0], 12);
	let v4_10_8 = IpAddressBlock::from_ipv4_masklen([10,0,0,0], 8);

	assert!(!v6_2400_12.matches_ip(IpAddress::from_str("23ff:ffff:ffff:ffff:ffff:ffff:ffff:ffff").unwrap()));
	assert!(v6_2400_12.matches_ip(IpAddress::from_str("2400::").unwrap()));
	assert!(v6_2400_12.matches_ip(IpAddress::from_str("240f:ffff:ffff:ffff:ffff:ffff:ffff:ffff").unwrap()));
	assert!(!v6_2400_12.matches_ip(IpAddress::from_str("2410::").unwrap()));
	assert!(!v6_2400_12.matches_ip(IpAddress::from_str("36.0.0.0").unwrap()));

	assert!(!v6_2400_12.matches_ipv6(&[0x23FF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF]));
	assert!(v6_2400_12.matches_ipv6(&[0x2400,0,0,0,0,0,0,0]));
	assert!(v6_2400_12.matches_ipv6(&[0x240F,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF]));
	assert!(!v6_2400_12.matches_ipv6(&[0x2410,0,0,0,0,0,0,0]));
	assert!(!v6_2400_12.matches_ipv4(&[36,0,0,0]));

	assert!(!v4_10_8.matches_ip(IpAddress::from_str("9.255.255.255").unwrap()));
	assert!(v4_10_8.matches_ip(IpAddress::from_str("10.0.0.0").unwrap()));
	assert!(v4_10_8.matches_ip(IpAddress::from_str("10.255.255.255").unwrap()));
	assert!(!v4_10_8.matches_ip(IpAddress::from_str("11.0.0.0").unwrap()));
	assert!(!v4_10_8.matches_ip(IpAddress::from_str("1000::").unwrap()));

	assert!(!v4_10_8.matches_ipv4(&[9,255,255,255]));
	assert!(v4_10_8.matches_ipv4(&[10,0,0,0]));
	assert!(v4_10_8.matches_ipv4(&[10,255,255,255]));
	assert!(!v4_10_8.matches_ipv4(&[11,0,0,0]));
	assert!(!v4_10_8.matches_ipv6(&[0x1000,0,0,0,0,0,0,0]));
}

#[test]
fn test_is_subset_of()
{
	let v6_2400_12 = IpAddressBlock::from_ipv6_masklen([0x2400,0,0,0,0,0,0,0], 12);
	let v4_10_8 = IpAddressBlock::from_ipv4_masklen([10,0,0,0], 8);

	assert!(!IpAddressBlock::from_ipv6_masklen([0x23FF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF], 16).
		is_subset_of(&v6_2400_12));
	assert!(IpAddressBlock::from_ipv6_masklen([0x2400,0,0,0,0,0,0,0], 16).
		is_subset_of(&v6_2400_12));
	assert!(IpAddressBlock::from_ipv6_masklen([0x240F,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF], 16).
		is_subset_of(&v6_2400_12));
	assert!(!IpAddressBlock::from_ipv6_masklen([0x2410,0,0,0,0,0,0,0], 16).
		is_subset_of(&v6_2400_12));
	assert!(!IpAddressBlock::from_ipv4_masklen([36,0,0,0], 16).
		is_subset_of(&v6_2400_12));

	assert!(!IpAddressBlock::from_ipv4_masklen([9,255,255,255], 16).is_subset_of(&v4_10_8));
	assert!(IpAddressBlock::from_ipv4_masklen([10,0,0,0], 16).is_subset_of(&v4_10_8));
	assert!(IpAddressBlock::from_ipv4_masklen([10,255,255,255], 16).is_subset_of(&v4_10_8));
	assert!(!IpAddressBlock::from_ipv4_masklen([11,0,0,0], 16).is_subset_of(&v4_10_8));
	assert!(!IpAddressBlock::from_ipv6_masklen([0x1000,0,0,0,0,0,0,0], 16).is_subset_of(&v4_10_8));
}

#[test]
fn test_ipaddrblock_raw()
{
	let v6_2400_12 = IpAddressBlock::from_ipv6_masklen([0x2400,0,0,0,0,0,0,0], 12);
	let v4_10_8 = IpAddressBlock::from_ipv4_masklen([10,0,0,0], 8);

	assert_eq!(v6_2400_12.as_ipv4(), None);
	assert_eq!(v6_2400_12.as_ipv6(), Some(([0x2400,0,0,0,0,0,0,0], [0xFFF0,0,0,0,0,0,0,0])));
	assert_eq!(v4_10_8.as_ipv6(), None);
	assert_eq!(v4_10_8.as_ipv4(), Some(([10,0,0,0], [255,0,0,0])));
}

#[test]
fn test_ipv4()
{
	assert_eq!(decode_ipv4("0"), Ok([0,0,0,0]));
	assert_eq!(decode_ipv4("1"), Ok([0,0,0,1]));
	assert_eq!(decode_ipv4("16909060"), Ok([1,2,3,4]));
	assert_eq!(decode_ipv4("4294967295"), Ok([255,255,255,255]));
	assert_eq!(decode_ipv4("0.0"), Ok([0,0,0,0]));
	assert_eq!(decode_ipv4("1.0"), Ok([1,0,0,0]));
	assert_eq!(decode_ipv4("1.1"), Ok([1,0,0,1]));
	assert_eq!(decode_ipv4("1.131844"), Ok([1,2,3,4]));
	assert_eq!(decode_ipv4("255.16777215"), Ok([255,255,255,255]));
	assert_eq!(decode_ipv4("0.0.0"), Ok([0,0,0,0]));
	assert_eq!(decode_ipv4("1.2.0"), Ok([1,2,0,0]));
	assert_eq!(decode_ipv4("1.2.1"), Ok([1,2,0,1]));
	assert_eq!(decode_ipv4("1.2.772"), Ok([1,2,3,4]));
	assert_eq!(decode_ipv4("255.255.65535"), Ok([255,255,255,255]));
	assert_eq!(decode_ipv4("0.0.0.0"), Ok([0,0,0,0]));
	assert_eq!(decode_ipv4("1.2.3.0"), Ok([1,2,3,0]));
	assert_eq!(decode_ipv4("1.2.3.1"), Ok([1,2,3,1]));
	assert_eq!(decode_ipv4("1.2.3.4"), Ok([1,2,3,4]));
	assert_eq!(decode_ipv4("255.255.255.255"), Ok([255,255,255,255]));
	assert!(decode_ipv4("4294967296").is_err());
	assert!(decode_ipv4("1.16777216").is_err());
	assert!(decode_ipv4("1.1.65536").is_err());
	assert!(decode_ipv4("1.1.1.256").is_err());
	assert!(decode_ipv4("256.0").is_err());
	assert!(decode_ipv4("256.0.0").is_err());
	assert!(decode_ipv4("0.256.0").is_err());
	assert!(decode_ipv4("256.0.0.0").is_err());
	assert!(decode_ipv4("0.256.0.0").is_err());
	assert!(decode_ipv4("0.0.256.0").is_err());
}


#[test]
fn test_ipv6()
{
	assert_eq!(decode_ipv6("::"), Ok([0,0,0,0,0,0,0,0]));
	assert_eq!(decode_ipv6("::1"), Ok([0,0,0,0,0,0,0,1]));
	assert_eq!(decode_ipv6("2000::"), Ok([0x2000,0,0,0,0,0,0,0]));
	assert_eq!(decode_ipv6("2000::1"), Ok([0x2000,0,0,0,0,0,0,1]));
	assert_eq!(decode_ipv6("2000::1.2.3.4"), Ok([0x2000,0,0,0,0,0,0x0102,0x0304]));
	assert_eq!(decode_ipv6("2000::ffff:1.2.3.4"), Ok([0x2000,0,0,0,0,0xffff,0x0102,0x0304]));
	assert_eq!(decode_ipv6("2000::FFFF:1.2.3.4"), Ok([0x2000,0,0,0,0,0xffff,0x0102,0x0304]));
	assert_eq!(decode_ipv6("::1.2.3.4"), Ok([0,0,0,0,0,0,0x0102,0x0304]));
	assert_eq!(decode_ipv6("1:2:3:4:5:6:7:8"), Ok([1,2,3,4,5,6,7,8]));
	assert_eq!(decode_ipv6("1:2:3:4:5:6:7::"), Ok([1,2,3,4,5,6,7,0]));
	assert_eq!(decode_ipv6("1:2:3:4:5:6::"), Ok([1,2,3,4,5,6,0,0]));
	assert_eq!(decode_ipv6("1:2:3:4:5::"), Ok([1,2,3,4,5,0,0,0]));
	assert_eq!(decode_ipv6("1:2:3:4::"), Ok([1,2,3,4,0,0,0,0]));
	assert_eq!(decode_ipv6("1:2:3::"), Ok([1,2,3,0,0,0,0,0]));
	assert_eq!(decode_ipv6("1:2::"), Ok([1,2,0,0,0,0,0,0]));
	assert_eq!(decode_ipv6("1::"), Ok([1,0,0,0,0,0,0,0]));
	assert_eq!(decode_ipv6("1::3:4:5:6:7:8"), Ok([1,0,3,4,5,6,7,8]));
	assert_eq!(decode_ipv6("1::4:5:6:7:8"), Ok([1,0,0,4,5,6,7,8]));
	assert_eq!(decode_ipv6("1::5:6:7:8"), Ok([1,0,0,0,5,6,7,8]));
	assert_eq!(decode_ipv6("1::6:7:8"), Ok([1,0,0,0,0,6,7,8]));
	assert_eq!(decode_ipv6("1::7:8"), Ok([1,0,0,0,0,0,7,8]));
	assert_eq!(decode_ipv6("1::8"), Ok([1,0,0,0,0,0,0,8]));
	assert_eq!(decode_ipv6("1:2::8"), Ok([1,2,0,0,0,0,0,8]));
	assert!(decode_ipv6("2000:::1").is_err());
	assert!(decode_ipv6("2000:::").is_err());
	assert!(decode_ipv6("2000::1.2.3.4.5").is_err());
	assert!(decode_ipv6("2000::1.2.3").is_err());
	assert!(decode_ipv6("2000::1.2").is_err());
	assert!(decode_ipv6("2000::g").is_err());
}

#[test]
fn test_continuous_ipv4()
{
	for i in 0..33 {
		let mut m = [0;4];
		for j in 0..i { m[j/8] |= 1 << 7-j%8; }
		assert!(IpAddressBlock::from_ipv4_mask([0;4],m).is_continuous());
	}
	for i in 0..32 {
		for k in i+1..32 {
			let mut m = [0;4];
			for j in 0..i { m[j/8] |= 1 << 7-j%8; }
			m[k/8] |= 1 << 7-k%8;
			assert!(!IpAddressBlock::from_ipv4_mask([0;4],m).is_continuous());
		}
	}
}

#[test]
fn test_continuous_ipv6()
{
	for i in 0..129 {
		let mut m = [0;8];
		for j in 0..i { m[j/16] |= 1 << 15-j%16; }
		assert!(IpAddressBlock::from_ipv6_mask([0;8],m).is_continuous());
	}
	for i in 0..127 {
		for k in i+1..128 {
			let mut m = [0;8];
			for j in 0..i { m[j/16] |= 1 << 15-j%16; }
			m[k/16] |= 1 << 15-k%16;
			assert!(!IpAddressBlock::from_ipv6_mask([0;8],m).is_continuous());
		}
	}
}

fn roundtrip_address(addr: IpAddress)
{
	let addr2 = addr.to_rdns().to_string();
	let addr3 = addr2.to_ascii_uppercase();
	println!("addr2={addr2}");
	let addr2b = IpAddress::from_rdns(addr2.as_bytes()).expect("A1").expect("A2");
	let addr3b = IpAddress::from_rdns(addr3.as_bytes()).expect("B1").expect("B2");
	println!("addr={addr}, addr2={addr2b}");
	assert!(addr == addr2b);
	assert!(addr == addr3b);
}

#[test]
fn ipv4_basic_roundtrip()
{
	roundtrip_address(IpAddress::from_str("65.212.194.127").unwrap());
}

#[test]
fn ipv4_bad_rdns()
{
	assert!(IpAddress::from_rdns(b"1.2.3./.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.:.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.01.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.09.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.1/.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.1:.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.9/.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.9:.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.:0.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.:9.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.011.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.099.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.10/.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.10:.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.19/.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.19:.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.20/.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.20:.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.24/.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.24:.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.25/.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.256.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.260.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.4.5.in-addr.arpa").is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.4.jn-addr.arpa").expect("C").is_none());
}

#[test]
fn ipv4_edge_roundtrip()
{
	roundtrip_address(IpAddress::from_str("1.2.3.0").unwrap());
	roundtrip_address(IpAddress::from_str("1.2.3.9").unwrap());
	roundtrip_address(IpAddress::from_str("1.2.3.10").unwrap());
	roundtrip_address(IpAddress::from_str("1.2.3.99").unwrap());
	roundtrip_address(IpAddress::from_str("1.2.3.100").unwrap());
	roundtrip_address(IpAddress::from_str("1.2.3.199").unwrap());
	roundtrip_address(IpAddress::from_str("1.2.3.200").unwrap());
	roundtrip_address(IpAddress::from_str("1.2.3.249").unwrap());
	roundtrip_address(IpAddress::from_str("1.2.3.250").unwrap());
	roundtrip_address(IpAddress::from_str("1.2.3.255").unwrap());
}

#[test]
fn ipv6_basic_roundtrip()
{
	roundtrip_address(IpAddress::from_str("7bf2:b4db:66d7:5f80:18e5:12d5:fec9:f4f7").unwrap());
}

#[test]
fn ipv6_bad_rdns()
{
	assert!(IpAddress::from_rdns(b"1.2.3.4.5.6.7.8.9.a.b.c.d.e.f.0.1.2.3.4.5.6.7.8.9.a.b.c.d.e.f./.ip6.arpa").
		is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.4.5.6.7.8.9.a.b.c.d.e.f.0.1.2.3.4.5.6.7.8.9.a.b.c.d.e.f.:.ip6.arpa").
		is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.4.5.6.7.8.9.a.b.c.d.e.f.0.1.2.3.4.5.6.7.8.9.a.b.c.d.e.f.@.ip6.arpa").
		is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.4.5.6.7.8.9.a.b.c.d.e.f.0.1.2.3.4.5.6.7.8.9.a.b.c.d.e.f.G.ip6.arpa").
		is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.4.5.6.7.8.9.a.b.c.d.e.f.0.1.2.3.4.5.6.7.8.9.a.b.c.d.e.f.`.ip6.arpa").
		is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.4.5.6.7.8.9.a.b.c.d.e.f.0.1.2.3.4.5.6.7.8.9.a.b.c.d.e.f.g.ip6.arpa").
		is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.4.5.6.7.8.9.a.b.c.d.e.f.0.1.2.3.4.5.6.7.8.9.a.b.c.d.e.f.0.1.ip6.arpa").
		is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.4.5.6.7.8.9.a.b.c.d.e.f.0.1.2.3.4.5.6.7.8.9.a.b.c.d.e.f.ip6.arpa").
		is_err());
	assert!(IpAddress::from_rdns(b"1.2.3.4.5.6.7.8.9.a.b.c.d.e.f.0.1.2.3.4.5.6.7.8.9.a.b.c.d.e.f.0.jp6.arpa").
		expect("D").is_none());
}

#[test]
fn ipv6_edge_roundtrip()
{
	roundtrip_address(IpAddress::from_str("2001:db8::0").unwrap());
	roundtrip_address(IpAddress::from_str("2001:db8::9").unwrap());
	roundtrip_address(IpAddress::from_str("2001:db8::a").unwrap());
	roundtrip_address(IpAddress::from_str("2001:db8::f").unwrap());
}
