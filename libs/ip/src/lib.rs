//!Routines related to IP address handling.
//!
//!This crate contains implementations of various routines related to handling of IP addresses.
//!
//!The most important items are:
//!
//! * A block of IP addresses: [`IpAddressBlock`](struct.IpAddressBlock.html)
//! * An IP address: [`IpAddress`](enum.IpAddress.html)
#![no_std]
#![forbid(unsafe_code)]
#![forbid(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]

#[cfg(any(test, feature="stdipaddr"))] #[macro_use] extern crate std;
use btls_aux_fail::dtry;
use btls_aux_fail::f_return;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::fail_if_none;
use btls_aux_memory::rsplit_at;
use core::cmp::min;
use core::convert::From;
use core::fmt::Display;
use core::fmt::Formatter;
use core::fmt::Error as FmtError;
use core::str::FromStr;

#[cfg(feature="stdipaddr")]
mod stdipaddr
{
	use super::IpAddressBlock;
	use super::_IpAddressBlock;
	use super::IpAddress;
	use std::net::IpAddr;
	use std::net::Ipv4Addr;
	use std::net::Ipv6Addr;
	use core::convert::Into;

	fn _dummy()
	{
		format!("");	//Squash warning.
	}

	impl From<IpAddr> for IpAddressBlock
	{
		fn from(address: IpAddr) -> IpAddressBlock
		{
			match address {
				IpAddr::V4(addr) => IpAddressBlock(_IpAddressBlock::Ipv4(addr.octets(), [0xFF;4])),
				IpAddr::V6(addr) =>
					IpAddressBlock(_IpAddressBlock::Ipv6(addr.segments(), [0xFFFF;8])),
			}
		}
	}

	impl From<Ipv4Addr> for IpAddressBlock
	{
		fn from(address: Ipv4Addr) -> IpAddressBlock
		{
			IpAddressBlock(_IpAddressBlock::Ipv4(address.octets(), [0xFF;4]))
		}
	}

	impl From<Ipv6Addr> for IpAddressBlock
	{
		fn from(address: Ipv6Addr) -> IpAddressBlock
		{
			IpAddressBlock(_IpAddressBlock::Ipv6(address.segments(), [0xFFFF;8]))
		}
	}

	impl From<IpAddr> for IpAddress
	{
		fn from(address: IpAddr) -> IpAddress
		{
			match address {
				IpAddr::V4(x) => IpAddress::Ipv4(x.octets()),
				IpAddr::V6(x) => IpAddress::Ipv6(x.segments()),
			}
		}
	}

	impl From<Ipv4Addr> for IpAddress
	{
		fn from(address: Ipv4Addr) -> IpAddress { IpAddress::Ipv4(address.octets()) }
	}

	impl From<Ipv6Addr> for IpAddress
	{
		fn from(address: Ipv6Addr) -> IpAddress { IpAddress::Ipv6(address.segments()) }
	}

	impl Into<IpAddr> for IpAddress
	{
		fn into(self) -> IpAddr
		{
			match self {
				IpAddress::Ipv4(x) => IpAddr::V4(Ipv4Addr::new(x[0], x[1], x[2], x[3])),
				IpAddress::Ipv6(x) => IpAddr::V6(Ipv6Addr::new(x[0], x[1], x[2], x[3],
					x[4], x[5], x[6], x[7])),
			}
		}
	}
}

impl From<IpAddress> for IpAddressBlock
{
	fn from(address: IpAddress) -> IpAddressBlock
	{
		match address {
			IpAddress::Ipv4(addr) => IpAddressBlock(_IpAddressBlock::Ipv4(addr, [0xFF;4])),
			IpAddress::Ipv6(addr) => IpAddressBlock(_IpAddressBlock::Ipv6(addr, [0xFFFF;8])),
		}
	}
}

///Raw IPv4 address: Four octets.
pub type RawIpv4Addr = [u8;4];

///Raw IPv6 address: Eight sextets.
pub type RawIpv6Addr = [u16;8];

#[derive(Copy,Clone,Debug,PartialEq,Eq)]
enum _IpAddressBlock
{
	Ipv4(RawIpv4Addr, RawIpv4Addr),
	Ipv6(RawIpv6Addr, RawIpv6Addr),
}

///IP address block.
///
///For single addresses, besides constructors [`from_ipv4()`](#method.from_ipv4) and
///[`from_ipv6()`](#method.from_ipv6), this structure also can undergo `From` trait conversions from
///[`IpAddress`](enum.IpAddress.html) and if feature `stdipaddr` is enabled, also from `std::net::IpAddr`,
///`std::net::Ipv4Addr` and `std::net::Ipv6Addr`.
///
///For multi-address blocks, this structure can be constructed using [`from_ipv4_mask()`](#method.from_ipv4_mask),
///[`from_ipv6_mask()`](#method.from_ipv6_mask), [`from_ipv4_masklen()`](#method.from_ipv4_masklen),
///[`from_ipv6_masklen()`](#method.from_ipv6_masklen).
///
///For casting back to raw representations, there are methods [`as_ipv4()`](#method.as_ipv4) and
///[`as_ipv6()`](#method.as_ipv6).
///
///The address block can be pretty-printed using the `{}` operator (trait `Display`).
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub struct IpAddressBlock(_IpAddressBlock);

impl IpAddressBlock
{
	///Create from IPv4 address.
	///
	///The created block will be a /32.
	pub fn from_ipv4(address: RawIpv4Addr) -> IpAddressBlock
	{
		IpAddressBlock(_IpAddressBlock::Ipv4(address, [0xFF;4]))
	}
	///Create from IPv6 address.
	///
	///The created block will be a /128.
	pub fn from_ipv6(address: RawIpv6Addr) -> IpAddressBlock
	{
		IpAddressBlock(_IpAddressBlock::Ipv6(address, [0xFFFF;8]))
	}
	///Create block of addresses from IPv4 address `address` with mask `mask`.
	pub fn from_ipv4_mask(address: RawIpv4Addr, mask: RawIpv4Addr) -> IpAddressBlock
	{
		IpAddressBlock(_IpAddressBlock::Ipv4(address, mask))
	}
	///Create block of addresses from IPv6 address `address` with mask `mask`.
	pub fn from_ipv6_mask(address: RawIpv6Addr, mask: RawIpv6Addr) -> IpAddressBlock
	{
		IpAddressBlock(_IpAddressBlock::Ipv6(address, mask))
	}
	///Create block of addresses from IPv4 address `address` with number of fixed most significant bits
	///`masklen`.
	pub fn from_ipv4_masklen(address: RawIpv4Addr, masklen: u8) -> IpAddressBlock
	{
		IpAddressBlock(_IpAddressBlock::Ipv4(address, ipv4_netmask_masklen(masklen)))
	}
	///Create block of addresses from IPv6 address `address` with number of fixed most significant bits
	///`masklen`.
	pub fn from_ipv6_masklen(address: RawIpv6Addr, masklen: u8) -> IpAddressBlock
	{
		IpAddressBlock(_IpAddressBlock::Ipv6(address, ipv6_netmask_masklen(masklen)))
	}
	///Given IP address `address` is inside given block?
	pub fn matches_ip(&self, address: IpAddress) -> bool
	{
		if let (&_IpAddressBlock::Ipv4(ref a, ref m), IpAddress::Ipv4(addr)) = (&self.0, address) {
			let mut syndrome = 0u8;
			for i in 0..4 { syndrome |= (addr[i] & m[i]) ^ (a[i] & m[i]); }
			return syndrome == 0;
		} else if let (&_IpAddressBlock::Ipv6(ref a, ref m), IpAddress::Ipv6(addr)) = (&self.0, address) {
			let mut syndrome = 0u16;
			for i in 0..8 { syndrome |= (addr[i] & m[i]) ^ (a[i] & m[i]); }
			return syndrome == 0;
		} else {
			false
		}
	}
	///Given IPv4 address `addr` is inside block?
	pub fn matches_ipv4(&self, address: &RawIpv4Addr) -> bool { self.matches_ip(IpAddress::Ipv4(*address)) }
	///Given IPv6 address `address` is inside block?
	pub fn matches_ipv6(&self, address: &RawIpv6Addr) -> bool { self.matches_ip(IpAddress::Ipv6(*address)) }
	///Is subset of another address block `another`?
	pub fn is_subset_of(&self, another: &IpAddressBlock) -> bool
	{
		//The address is subset of another if M & M' = M' and  A & M' = A' & M'.
		match (&self.0, &another.0) {
			(&_IpAddressBlock::Ipv4(ref a1, ref m1), &_IpAddressBlock::Ipv4(ref a2, ref m2)) => {
				let mut syndrome = 0u8;
				for i in 0..4 {
					syndrome |= (a1[i] & m2[i]) ^ (a2[i] & m2[i]);
					syndrome |= (m1[i] & m2[i]) ^ m2[i];
				}
				return syndrome == 0;
			},
			(&_IpAddressBlock::Ipv6(ref a1, ref m1), &_IpAddressBlock::Ipv6(ref a2, ref m2)) => {
				let mut syndrome = 0u16;
				for i in 0..8 {
					syndrome |= (a1[i] & m2[i]) ^ (a2[i] & m2[i]);
					syndrome |= (m1[i] & m2[i]) ^ m2[i];
				}
				return syndrome == 0;
			},
			//Cross-address space never matches.
			_ => false,
		}
	}
	///Unwrap raw IPv6 address.
	///
	///If the address is valid IPv6 address, returns `Some((addr, mask))`, where `addr` is the base address
	///and `mask` is the mask. If it is not an IPv6 address, returns `None`.
	pub fn as_ipv6(&self) -> Option<(RawIpv6Addr,RawIpv6Addr)>
	{
		if let _IpAddressBlock::Ipv6(ref a, ref b) = self.0 { Some((*a,*b)) } else { None }
	}
	///Unwrap raw IPv4 address.
	///
	///If the address is valid IPv4 address, returns `Some((addr, mask))`, where `addr` is the base address
	///and `mask` is the mask. If it is not an IPv4 address, returns `None`.
	pub fn as_ipv4(&self) -> Option<(RawIpv4Addr,RawIpv4Addr)>
	{
		if let _IpAddressBlock::Ipv4(ref a, ref b) = self.0 { Some((*a,*b)) } else { None }
	}
	///Unwrap as address if single.
	pub fn as_single(&self) -> Option<IpAddress>
	{
		match &self.0 {
			&_IpAddressBlock::Ipv6(ref a, ref m) if m == &[!0u16;8] => Some(IpAddress::Ipv6(*a)),
			&_IpAddressBlock::Ipv4(ref a, ref m) if m == &[!0u8;4] => Some(IpAddress::Ipv4(*a)),
			_ => None,
		}
	}
	///Check range is continuous.
	///
	///If the range is in one part, returns true, otherwise returns false.
	pub fn is_continuous(&self) -> bool
	{
		match self.0 {
			_IpAddressBlock::Ipv4(_, ref m) => {
				let mut m = &m[..];
				while m.len() > 0 && m[0] == 0xFF { m = &m[1..]; }
				while m.len() > 0 && m[m.len()-1] == 0 { m = &m[..m.len()-1]; }
				m.len() <= 1 && m.get(0).map(|&v|is_continuous8(v)).unwrap_or(true)
			},
			_IpAddressBlock::Ipv6(_, ref m) => {
				let mut m = &m[..];
				while m.len() > 0 && m[0] == 0xFFFF { m = &m[1..]; }
				while m.len() > 0 && m[m.len()-1] == 0 { m = &m[..m.len()-1]; }
				m.len() <= 1 && m.get(0).map(|&v|is_continuous16(v)).unwrap_or(true)
			},
		}
	}
}

fn is_continuous8(x: u8) -> bool { ((x << 1) | 1 << x.trailing_zeros()) == x }
fn is_continuous16(x: u16) -> bool { ((x << 1) | 1 << x.trailing_zeros()) == x }

fn ipv4_netmask_masklen(masklen: u8) -> [u8;4]
{
	let mut x = [0;4];
	for i in 0..4 {
		let sbits = min((masklen as u32).saturating_sub(8 * i as u32), 8);
		//Sbits=0 is a special case.
		x[i] = if sbits > 0 { !(1u8 << 8 - sbits).wrapping_sub(1) } else { 0 };
	}
	x
}

fn ipv6_netmask_masklen(masklen: u8) -> [u16;8]
{
	let mut x = [0;8];
	for i in 0..8 {
		let sbits = min((masklen as u32).saturating_sub(16 * i as u32), 16);
		//Sbits=0 is a special case.
		x[i] = if sbits > 0 { !(1u16 << 16 - sbits).wrapping_sub(1) } else { 0 };
	}
	x
}

fn netbits_ipv6(data: &RawIpv6Addr) -> Option<u8>
{
	let idx = {
		let mut f = 0;
		while f < 128 {
			//f < 128, so f/16 < 8.
			if (data[f/16] >> 15-(f%16)) & 1 == 0 { break; }
			f += 1;
		}
		f
	};
	//All remaining bits must be 0.
	for j in idx..128 { fail_if_none!((data[j/16] >> 15-(j%16)) & 1 != 0); }
	//Ok.
	Some(idx as u8)
}

fn netbits_ipv4(data: &RawIpv4Addr) -> Option<u8>
{
	let idx = {
		let mut f = 0;
		while f < 32 {
			//f < 32, so f/8 < 4.
			if (data[f/8] >> 7-(f%8)) & 1 == 0 { break; }
			f += 1;
		}
		f
	};
	//All remaining bits must be 0.
	for j in idx..32 { fail_if_none!((data[j/8] >> 7-(j%8)) & 1 != 0); }
	//Ok.
	Some(idx as u8)
}

fn maxrun(mask: u8) -> (usize, usize)
{
	for i in 0..8 {
		let j = 8 - i;
		let m = ((1u16 << j) - 1) as u8;
		for k in 0..(i+1) { if (mask >> k) & m == m { return (k, k + j); } }
	}
	(0, 0)
}

fn print_ipv4(fmt: &mut Formatter, addr: &RawIpv4Addr) -> Result<(), FmtError>
{
	let [a,b,c,d] = *addr;
	write!(fmt, "{a}.{b}.{c}.{d}")
}

fn print_ipv6(fmt: &mut Formatter, addr: &RawIpv6Addr) -> Result<(), FmtError>
{
	let mut m = 0;
	for i in 0..8 { if addr[i] == 0 { m |= 1 << i; } }
	let (a, b) = maxrun(m);
	for i in 0..8 {
		if i < a || i >= b {
			write!(fmt, "{hextet:x}", hextet=addr[i])?;
			if i < 7 && i + 1 != a { fmt.write_str(":")?; }
		}
		if i == a && b > a { fmt.write_str("::")?; }
	}
	Ok(())
}

impl Display for IpAddressBlock
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		match &self.0 {
			&_IpAddressBlock::Ipv4(ref addr, ref mask) => {
				let bits = netbits_ipv4(mask);
				print_ipv4(fmt, addr)?;
				match bits {
					Some(32) => (),
					Some(masklen) => write!(fmt, "/{masklen}")?,
					None => {
						fmt.write_str("/")?;
						print_ipv4(fmt, mask)?;
					}
				}
				Ok(())
			},
			&_IpAddressBlock::Ipv6(ref addr, ref mask) => {
				let bits = netbits_ipv6(mask);
				print_ipv6(fmt, addr)?;
				match bits {
					Some(128) => (),
					Some(masklen) => write!(fmt, "/{masklen}")?,
					None => {
						fmt.write_str("/")?;
						print_ipv6(fmt, mask)?;
					}
				}
				Ok(())
			},
		}
	}
}

struct PrintReverseDns(IpAddress);

impl Display for PrintReverseDns
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		match &self.0 {
			&IpAddress::Ipv4(addr) => {
				let [d,c,b,a] = addr;
				write!(fmt, "{a}.{b}.{c}.{d}.in-addr.arpa")
			},
			&IpAddress::Ipv6(addr) => {
				for i in 0..32 {
					let h = addr[7-i/4]>>i%4*4&15;
					write!(fmt, "{h:x}.")?;
				}
				//The above loop writes 32nd dot.
				write!(fmt, "ip6.arpa")
			},
		}
	}
}

fn parse_octet(x: &[u8]) -> Option<u8>
{
	match x {
		//0-9
		&[b]           if                   b>=0x30&&b<=0x39 => Some(                 b-0x30 ),
		//10-99
		&[a,b]         if a>=0x31&&a<=0x39&&b>=0x30&&b<=0x39 => Some(    (a-0x30)*10+(b-0x30)),
		//100-199
		&[0x31,a,b]    if a>=0x30&&a<=0x39&&b>=0x30&&b<=0x39 => Some(100+(a-0x30)*10+(b-0x30)),
		//200-249
		&[0x32,a,b]    if a>=0x30&&a<=0x34&&b>=0x30&&b<=0x39 => Some(200+(a-0x30)*10+(b-0x30)),
		//250-255
		&[0x32,0x35,b] if                   b>=0x30&&b<=0x35 => Some(250+            (b-0x30)),
		_ => None
	}
}

fn parse_hex(x: u8) -> Option<u16>
{
	let x = x as u16;
	match x {
		0x30..=0x39 => Some(x - 0x30),
		0x41..=0x46 => Some(x - 0x37),
		0x61..=0x66 => Some(x - 0x57),
		_ => None,
	}
}

macro_rules! grab_ip4_octet
{
	($i:expr) => {
		match $i.next() {
			Some(i) => match parse_octet(i) {
				Some(i) => i,
				None => fail!(())
			},
			None => fail!(())
		}
	};
}

fn strip_suffix_lc<'a>(addr: &'a [u8], suffix: &[u8]) -> Option<&'a [u8]>
{
	let (head, tail) = rsplit_at(addr, suffix.len()).ok()?;
	if tail.eq_ignore_ascii_case(suffix) { Some(head) } else { None }
}

fn from_rdns_ipv4(addr: &[u8]) -> Result<Option<[u8;4]>, ()>
{
	//Check that the suffix is correct. If this fails, return Ok(None), as suffix is not valid.
	let addr = f_return!(strip_suffix_lc(addr, b".in-addr.arpa"), Ok(None));
	//Ok, the suffix is correct, parse addr. The address consists of 4 octets, separated by dots. The parts
	//are in revese order.
	let mut i = addr.split(|&c|c==b'.');
	let d = grab_ip4_octet!(i);
	let c = grab_ip4_octet!(i);
	let b = grab_ip4_octet!(i);
	let a = grab_ip4_octet!(i);
	fail_if!(i.next().is_some(), ());	//There is supposed to be exactly 4 parts!
	Ok(Some([a,b,c,d]))
}

fn from_rdns_ipv6(addr: &[u8]) -> Result<Option<[u16;8]>, ()>
{
	//Check that the suffix is correct. If this fails, return Ok(None), as suffix is not valid.
	let addr = f_return!(strip_suffix_lc(addr, b".ip6.arpa"), Ok(None));
	//Ok, the suffix is correct, parse addr. The address always has 32 single-hex components, separated by
	//31 dots.
	fail_if!(addr.len() != 63, ());
	for i in 0..31 { fail_if!(addr[2*i+1] != b'.', ()); }
	let mut out = [0;8];
	for i in 0..32 {
		let h = dtry!(parse_hex(addr[2*i]));
		out[7-i/4] |= h<<i%4*4;
	}
	Ok(Some(out))
}

///An IP address.
///
///For single addresses, besides manual construction, if feature `stdipaddr` is enabled, also from
///`std::net::IpAddr`, `std::net::Ipv4Addr` and `std::net::Ipv6Addr`. It can also undergo `std::str::FromStr`
///conversion from `&str`, altough this conversion is fallible.
///
///The address block can be pretty-printed using the `{}` operator (trait `Display`).
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum IpAddress
{
	///IPv4 address, specified as 4 octets.
	Ipv4(RawIpv4Addr),
	///IPv6 address, specified as 8 hextets.
	Ipv6(RawIpv6Addr),
}

fn decode_ipv4(s: &str) -> Result<RawIpv4Addr, ()>
{
	let mut parts = s.split('.');
	let pcount = parts.clone().count();
	let mut value = 0u32;
	//Do not chek last component here.
	for _ in 1..pcount {
		let t = dtry!(u32::from_str(dtry!(parts.next())));
		fail_if!(t > 255, ());		//Out of range.
		value = (value << 8) | t;
	}
	let (delta,max,shift) = match pcount {
		1 => (dtry!(u32::from_str(dtry!(parts.next()))), 0xFFFFFFFF,0),
		2 => (dtry!(u32::from_str(dtry!(parts.next()))), 0xFFFFFF,24),
		3 => (dtry!(u32::from_str(dtry!(parts.next()))), 0xFFFF,16),
		4 => (dtry!(u32::from_str(dtry!(parts.next()))), 0xFF,8),
		_ => fail!(())			//Bad.
	};
	fail_if!(delta > max, ());	//Last component out of range.
	value = (value << shift) | delta;
	let mut buf = [0;4];
	for i in 0..4 { buf[i] = (value >> 24 - 8 * i) as u8; }
	Ok(buf)
}

#[derive(Copy,Clone,Debug)]
enum Ipv6Part
{
	B,
	E,
	S(u16),
	D(u16, u16),
}

impl Ipv6Part
{
	fn from_str(part: &str) -> Option<Ipv6Part>
	{
		let r = if part.len() == 0 {
			Ipv6Part::E
		} else if let Ok(val) = u16::from_str_radix(part, 16) {
			Ipv6Part::S(val)
		} else {
			let mut parts2 = part.split('.').map(|part2|u8::from_str(part2).ok());
			//There must be exactly 4 valid parts.
			let a = parts2.next()?? as u16;
			let b = parts2.next()?? as u16;
			let c = parts2.next()?? as u16;
			let d = parts2.next()?? as u16;
			fail_if_none!(parts2.next().is_some());
			Ipv6Part::D(a*256+b, c*256+d)
		};
		Some(r)
	}
}


fn decode_ipv6(s: &str) -> Result<RawIpv6Addr, ()>
{
	//IPv6 addresses can have up to 9 parts.
	let mut p = [Ipv6Part::B; 9];
	for (pcount, part) in s.split(':').enumerate() {
		let t = dtry!(Ipv6Part::from_str(part));
		fail_if!(pcount >= p.len(), ());
		//This is in-range by above check.
		p[pcount] = t;
	}
	//Find pair D-B at end. If found, replace by S-S.
	for i in 0..p.len()-1 {
		if let (D(x, y), B) = (p[i+0], p[i+1]) {
			p[i+0] = Ipv6Part::S(x);
			p[i+1] = Ipv6Part::S(y);
		}
	}
	use self::Ipv6Part::*;
	let r = match (p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], p[8]) {
		(S(a), S(b), S(c), S(d), S(e), S(f), S(g), S(h), B   ) => [a, b, c, d, e, f, g, h],
		(E,    E,    S(b), S(c), S(d), S(e), S(f), S(g), S(h)) => [0, b, c, d, e, f, g, h],
		(E,    E,    S(c), S(d), S(e), S(f), S(g), S(h), B   ) => [0, 0, c, d, e, f, g, h],
		(E,    E,    S(d), S(e), S(f), S(g), S(h), B   , B   ) => [0, 0, 0, d, e, f, g, h],
		(E,    E,    S(e), S(f), S(g), S(h), B   , B   , B   ) => [0, 0, 0, 0, e, f, g, h],
		(E,    E,    S(f), S(g), S(h), B   , B   , B   , B   ) => [0, 0, 0, 0, 0, f, g, h],
		(E,    E,    S(g), S(h), B   , B   , B   , B   , B   ) => [0, 0, 0, 0, 0, 0, g, h],
		(E,    E,    S(h), B   , B   , B   , B   , B   , B   ) => [0, 0, 0, 0, 0, 0, 0, h],
		(E,    E,    E,    B   , B   , B   , B   , B   , B   ) => [0, 0, 0, 0, 0, 0, 0, 0],
		(S(a), E,    S(c), S(d), S(e), S(f), S(g), S(h), B   ) => [a, 0, c, d, e, f, g, h],
		(S(a), E,    S(d), S(e), S(f), S(g), S(h), B   , B   ) => [a, 0, 0, d, e, f, g, h],
		(S(a), E,    S(e), S(f), S(g), S(h), B   , B   , B   ) => [a, 0, 0, 0, e, f, g, h],
		(S(a), E,    S(f), S(g), S(h), B   , B   , B   , B   ) => [a, 0, 0, 0, 0, f, g, h],
		(S(a), E,    S(g), S(h), B   , B   , B   , B   , B   ) => [a, 0, 0, 0, 0, 0, g, h],
		(S(a), E,    S(h), B   , B   , B   , B   , B   , B   ) => [a, 0, 0, 0, 0, 0, 0, h],
		(S(a), E,    E,    B   , B   , B   , B   , B   , B   ) => [a, 0, 0, 0, 0, 0, 0, 0],
		(S(a), S(b), E,    S(d), S(e), S(f), S(g), S(h), B   ) => [a, b, 0, d, e, f, g, h],
		(S(a), S(b), E,    S(e), S(f), S(g), S(h), B   , B   ) => [a, b, 0, 0, e, f, g, h],
		(S(a), S(b), E,    S(f), S(g), S(h), B   , B   , B   ) => [a, b, 0, 0, 0, f, g, h],
		(S(a), S(b), E,    S(g), S(h), B   , B   , B   , B   ) => [a, b, 0, 0, 0, 0, g, h],
		(S(a), S(b), E,    S(h), B   , B   , B   , B   , B   ) => [a, b, 0, 0, 0, 0, 0, h],
		(S(a), S(b), E,    E,    B   , B   , B   , B   , B   ) => [a, b, 0, 0, 0, 0, 0, 0],
		(S(a), S(b), S(c), E,    S(e), S(f), S(g), S(h), B   ) => [a, b, c, 0, e, f, g, h],
		(S(a), S(b), S(c), E,    S(f), S(g), S(h), B   , B   ) => [a, b, c, 0, 0, f, g, h],
		(S(a), S(b), S(c), E,    S(g), S(h), B   , B   , B   ) => [a, b, c, 0, 0, 0, g, h],
		(S(a), S(b), S(c), E,    S(h), B   , B   , B   , B   ) => [a, b, c, 0, 0, 0, 0, h],
		(S(a), S(b), S(c), E,    E,    B   , B   , B   , B   ) => [a, b, c, 0, 0, 0, 0, 0],
		(S(a), S(b), S(c), S(d), E,    S(f), S(g), S(h), B   ) => [a, b, c, d, 0, f, g, h],
		(S(a), S(b), S(c), S(d), E,    S(g), S(h), B   , B   ) => [a, b, c, d, 0, 0, g, h],
		(S(a), S(b), S(c), S(d), E,    S(h), B   , B   , B   ) => [a, b, c, d, 0, 0, 0, h],
		(S(a), S(b), S(c), S(d), E,    E,    B   , B   , B   ) => [a, b, c, d, 0, 0, 0, 0],
		(S(a), S(b), S(c), S(d), S(e), E,    S(g), S(h), B   ) => [a, b, c, d, e, 0, g, h],
		(S(a), S(b), S(c), S(d), S(e), E,    S(h), B   , B   ) => [a, b, c, d, e, 0, 0, h],
		(S(a), S(b), S(c), S(d), S(e), E,    E,    B   , B   ) => [a, b, c, d, e, 0, 0, 0],
		(S(a), S(b), S(c), S(d), S(e), S(f), E,    S(h), B   ) => [a, b, c, d, e, f, 0, h],
		(S(a), S(b), S(c), S(d), S(e), S(f), E,    E,    B   ) => [a, b, c, d, e, f, 0, 0],
		(S(a), S(b), S(c), S(d), S(e), S(f), S(g), E,    E   ) => [a, b, c, d, e, f, g, 0],
		_ => fail!(())
	};
	Ok(r)
}

impl FromStr for IpAddress
{
	type Err = ();
	fn from_str(s: &str) -> Result<IpAddress, ()>
	{
		if let Ok(addr) = decode_ipv6(s) {
			Ok(IpAddress::Ipv6(addr))
		} else if let Ok(addr) = decode_ipv4(s) {
			Ok(IpAddress::Ipv4(addr))
		} else {
			Err(())
		}
	}
}


impl Display for IpAddress
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&IpAddress::Ipv4(ref addr) => print_ipv4(fmt, addr),
			&IpAddress::Ipv6(ref addr) => print_ipv6(fmt, addr),
		}
	}
}

impl IpAddress
{
	///Parse Reverse-DNS address as IP address.
	///
	/// # Parameters:
	///
	/// * `addr`: The address to parse.
	///
	/// # Error conditions:
	///
	/// * The address is not a valid Reverse-DNS address.
	///
	/// # Return value:
	///
	///The parsed address.
	///
	/// # Notes:
	///
	/// * In case the name does not have valid suffix, returns `Ok(None)`.
	/// * In case name has valid suffix, but is not valid, returns `Err(())`.
	pub fn from_rdns(addr: &[u8]) -> Result<Option<IpAddress>, ()>
	{
		if let Some(addr) = from_rdns_ipv4(addr)? {
			Ok(Some(IpAddress::Ipv4(addr)))
		} else if let Some(addr) = from_rdns_ipv6(addr)? {
			Ok(Some(IpAddress::Ipv6(addr)))
		} else {
			Ok(None)
		}
	}
	///Return object that displays the address as Reverse-DNS address.
	///
	/// # Return value:
	///
	///Object implementing `Display`.
	pub fn to_rdns(&self) -> impl Display+'static { PrintReverseDns(*self) }
}

#[cfg(test)]
mod test;
