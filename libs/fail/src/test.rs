use super::ResultExt;

fn fail1() -> Result<(), u32>
{
	fail!(42u32);
}

fn fail2() -> Result<(), u32>
{
	fail_if!(true, 43u32);
	Ok(())
}

fn fail3() -> Result<(), u32>
{
	fail_if!(false, 42u32);
	Ok(())
}

#[test]
fn test_fail()
{
	assert_eq!(fail1().unwrap_err(), 42);
	assert_eq!(fail2().unwrap_err(), 43);
	assert_eq!(fail3().unwrap(), ());
}

fn test_try2_1() -> i32
{
	assert_eq!(f_return!(Ok(42), F[|x:i32|x+1]), 42);
	f_return!(Err(42), F[|x:i32|x+1]);
	unreachable!();
}

fn test_try2_2() -> i32
{
	let v: Result<i32, i32> = Ok(42);
	assert_eq!(f_return!(v, 43), 42);
	f_return!(Err(42), 43);
	unreachable!();
}

fn test_try2_3()
{
	let v: Result<i32, i32> = Ok(42);
	assert_eq!(f_return!(v), 42);
	f_return!(Err(42));
	unreachable!();
}

fn test_try2_4() -> i32
{
	assert_eq!(f_return!(Some(42), 43), 42);
	f_return!(None, 43);
	unreachable!();
}

fn test_try2_5()
{
	assert_eq!(f_return!(Some(42)), 42);
	f_return!(None);
	unreachable!();
}

fn test_try2_b1() -> i32
{
	loop {
		assert_eq!(f_break!(Ok(42), F[|x:i32|x+1]), 42);
		f_break!(Err(42), F[|x:i32|x+1]);
		unreachable!();
	}
}

fn test_try2_b2() -> i32
{
	loop {
		let v: Result<i32, i32> = Ok(42);
		assert_eq!(f_break!(v, 43), 42);
		f_break!(Err(42), 43);
		unreachable!();
	}
}

fn test_try2_b3()
{
	loop {
		let v: Result<i32, i32> = Ok(42);
		assert_eq!(f_break!(v), 42);
		f_break!(Err(42));
		unreachable!();
	}
}

fn test_try2_b4() -> i32
{
	loop {
		assert_eq!(f_break!(Some(42), 43), 42);
		f_break!(None, 43);
		unreachable!();
	}
}

fn test_try2_b5()
{
	loop {
		assert_eq!(f_break!(Some(42)), 42);
		f_break!(None);
		unreachable!();
	}
}

fn test_try2_bl1() -> i32
{
	'outer: loop { loop {
		assert_eq!(f_break!(Ok(42), 'outer F[|x:i32|x+1]), 42);
		f_break!(Err(42), 'outer F[|x:i32|x+1]);
		unreachable!();
	}}
}
fn test_try2_bl2() -> i32
{
	'outer: loop { loop {
		let v: Result<i32, i32> = Ok(42);
		assert_eq!(f_break!(v, 'outer 43), 42);
		f_break!(Err(42), 'outer 43);
		unreachable!();
	}}
}

fn test_try2_bl3()
{
	'outer: loop { loop {
		let v: Result<i32, i32> = Ok(42);
		assert_eq!(f_break!(v, 'outer), 42);
		f_break!(Err(42), 'outer);
		unreachable!();
	}}
}

fn test_try2_bl4() -> i32
{
	'outer: loop { loop {
		assert_eq!(f_break!(Some(42), 'outer 43), 42);
		f_break!(None, 'outer 43);
		unreachable!();
	}}
}

fn test_try2_bl5()
{
	'outer: loop { loop {
		assert_eq!(f_break!(Some(42), 'outer), 42);
		f_break!(None, 'outer);
		unreachable!();
	}}
}

fn test_try2_c1()
{
	for _ in 0..1 {
		let v: Result<i32, i32> = Ok(42);
		f_continue!(v);
	}
	for _ in 0..1 {
		f_continue!(Err(42));
		unreachable!();
	}
	'outer: for _ in 0..1 {
		for _ in 0..1 {
			let v: Result<i32, i32> = Ok(42);
			f_continue!(v, 'outer);
		}
	}
	'outer2: for _ in 0..1 {
		for _ in 0..1 {
			f_continue!(Err(42), 'outer2);
			unreachable!();
		}
		unreachable!();
	}
}


fn test_try2_c2()
{
	for _ in 0..1 {
		f_continue!(Some(42));
	}
	for _ in 0..1 {
		f_continue!(None);
		unreachable!();
	}
	'outer: for _ in 0..1 {
		for _ in 0..1 {
			f_continue!(Some(42), 'outer);
		}
	}
	'outer2: for _ in 0..1 {
		for _ in 0..1 {
			f_continue!(None, 'outer2);
			unreachable!();
		}
		unreachable!();
	}
}

#[test]
fn test_set_ok()
{
	let v: Result<i32, i16> = Ok(42);
	let v: Result<u32, i16> = v.set_ok(666u32);
	assert!(matches!(v, Ok(666)));
	let v: Result<i32, i16> = Err(42);
	let v: Result<u32, i16> = v.set_ok(666u32);
	assert!(matches!(v, Err(42)));
}

#[test]
fn test_set_err()
{
	let v: Result<i32, i16> = Ok(42);
	let v: Result<i32, u32> = v.set_err(666u32);
	assert!(matches!(v, Ok(42)));
	let v: Result<i32, i16> = Err(42);
	let v: Result<i32, u32> = v.set_err(666u32);
	assert!(matches!(v, Err(666)));
}

fn test_dtry_inner1() -> Result<i32, ()>
{
	let v: Result<i32, i8> = Ok(1);
	dtry!(v);
	Ok(0)
}

fn test_dtry_inner2() -> Result<i32, ()>
{
	dtry!(Err(0));
	Ok(1)
}

fn test_dtry_inner3() -> Result<i32, ()>
{
	dtry!(Some(1));
	Ok(0)
}

fn test_dtry_inner4() -> Result<i32, ()>
{
	dtry!(None);
	Ok(1)
}

#[test]
fn test_dtry()
{
	matches!(test_dtry_inner1(), Ok(0));
	matches!(test_dtry_inner2(), Err(()));
	matches!(test_dtry_inner3(), Ok(0));
	matches!(test_dtry_inner4(), Err(()));
}


#[test]
fn test_if_pattern()
{
	let v: Result<i32, i32> = Ok(42);
	assert!(!if_match!(v, Err(_)));
	let v: Result<i32, i32> = Err(42);
	assert!(if_match!(v, Err(_)));
}

#[test]
fn test_try2()
{
	assert_eq!(test_try2_1(), 43);
	assert_eq!(test_try2_2(), 43);
	test_try2_3();
	assert_eq!(test_try2_4(), 43);
	test_try2_5();
	assert_eq!(test_try2_b1(), 43);
	assert_eq!(test_try2_b2(), 43);
	test_try2_b3();
	assert_eq!(test_try2_b4(), 43);
	test_try2_b5();
	assert_eq!(test_try2_bl1(), 43);
	assert_eq!(test_try2_bl2(), 43);
	test_try2_bl3();
	assert_eq!(test_try2_bl4(), 43);
	test_try2_bl5();
	test_try2_c1();
	test_try2_c2();
}
