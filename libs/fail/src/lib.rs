//!Some error handling macros.
//!
//!This crate provodes error handling macros `fail!` and `fail_if!`.
#![forbid(unsafe_code)]
//#![deny(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]

#[doc(hidden)]
pub mod __internal
{
	pub use core::convert::From;

	pub trait DecayResult<T:Sized>
	{
		fn decay(self) -> Option<T>;
	}

	impl<T:Sized,E:Sized> DecayResult<T> for Result<T,E>
	{
		fn decay(self) -> Option<T> { self.ok() }
	}

	impl<T:Sized> DecayResult<T> for Option<T>
	{
		fn decay(self) -> Option<T> { self }
	}
}

///Fail the current function with error
///
/// # Parameters:
///
/// * `$f`: A value to exit with.
///
/// # Early returns:
///
///Always returns `Err($f)`.
#[macro_export]
macro_rules! fail
{
	($f:expr) => { return Err($crate::__internal::From::from($f)) };
}

///Fail the current function with an error if give condition is true.
///
/// # Parameters:
///
/// * `$c`: A condition.
/// * `$f`: A value to exit with.
///
/// # Early returns:
///
///If `$c` is true, exits current function with `Err($f)`.
#[macro_export]
macro_rules! fail_if
{
	($c:expr, $f:expr) => { if $c { return Err($crate::__internal::From::from($f)); } };
}

///Fail the current function with `None` if given condition is true.
///
/// # Parameters:
///
/// * `$c`: A condition.
///
/// # Early returns:
///
///If `$c` is true, exits current function with `None`.
#[macro_export]
macro_rules! fail_if_none
{
	($c:expr) => { if $c { return None; } };
}

///Return if expression is `Err(_)` or `None`.
///
///# Parameters:
///
/// * `$e`: The expression to check.
/// * `F[$e2]`: Closure that is called with unwrapped value of the `Err` variant in order to yield the actual value
///the function returns. If this is specified, `$e` must be `Result`.
/// * `$e2`: The value to return. If not given, defaults to `()`.
///
/// # Early returns:
///
///If `$e` is `Err` or `None`.
///
/// # Return value:
///
///If `$e` is `Ok` or `Some`, the value is unwrapped.
#[macro_export]
macro_rules! f_return
{
	($e:expr, F[$e2:expr]) => { match $e { Ok(x) => x, Err(e) => return $e2(e) } };
	($e:expr, $e2:expr) => {
		match $crate::__internal::DecayResult::decay($e) { Some(x) => x, None => return $e2 }
	};
	($e:expr) => { match $crate::__internal::DecayResult::decay($e) { Some(x) => x, None => return } };
}

///Break if expression is `Err(_)` or `None`.
///
///# Parameters:
///
/// * `$e`: The expression to check.
/// * `$lt`: Label of loop to break out of. If not given, the innermost loop is broken out of.
/// * `F[$e2]`: Closure that is called with unwrapped value of the `Err` variant in order to yield the actual value
///the loop is broken with. If this is specified, `$e` must be `Result`.
/// * `$e2`: The value to break loop with. If not given, defaults to `()`.
///
/// # Early returns
///
///If `$e` is `Err` or `None`, breaks the indicated loop.
///
/// # Return value:
///
///If `$e` is `Ok` or `Some`, the value is unwrapped.
#[macro_export]
macro_rules! f_break
{
	($e:expr, $lt:lifetime F[$e2:expr]) => { match $e { Ok(x) => x, Err(e) => break $lt $e2(e)} };
	($e:expr, $lt:lifetime $e2:expr) => {
		match $crate::__internal::DecayResult::decay($e) { Some(x) => x, None => break $lt $e2}
	};
	($e:expr, $lt:lifetime) => {
		match $crate::__internal::DecayResult::decay($e) { Some(x) => x, None => break $lt }
	};
	($e:expr, F[$e2:expr]) => { match $e { Ok(x) => x, Err(e) => break $e2(e) } };
	($e:expr, $e2:expr) => {
		match $crate::__internal::DecayResult::decay($e) { Some(x) => x, None => break $e2 }
	};
	($e:expr) => {
		match $crate::__internal::DecayResult::decay($e) { Some(x) => x, None => break }
	};
}

///Continue if expression is `Err` or `None`.
///
///# Parameters:
///
/// * `$e`: The expression to check.
/// * `$lt`: Label of loop to continued. If not given, the innermost loop is continued.
///
/// # Early returns
///
///If `$e` is `Err` or `None`, continues the indicated loop.
///
/// # Return value:
///
///If `$e` is `Ok` or `Some`, the value is unwrapped.
#[macro_export]
macro_rules! f_continue
{
	($e:expr, $lt:lifetime) => {
		match $crate::__internal::DecayResult::decay($e) { Some(x) => x, None => continue $lt }
	};
	($e:expr) => {
		match $crate::__internal::DecayResult::decay($e) { Some(x) => x, None => continue }
	};
}

///If expression is `Err()` or `None`, discard expression and return `Err(())`.
///
/// # Parameters:
///
/// * `$e`: The expression.
///
/// # Early returns
///
///If `$e` is `Err()` or `None`, returns `Err(())`.
///
/// # Return value:
///
///The unwrapped `$e`.
#[macro_export]
macro_rules! dtry
{
	($e:expr) => {
		match $crate::__internal::DecayResult::decay($e) { Some(x) => x, None => return Err(()) }
	};
	(NORET $e:expr) => {
		match $crate::__internal::DecayResult::decay($e) { Some(x) => Ok(x), None => Err(()) }
	};
}

///Transform error like the try operator.
///
/// # Parameters:
///
/// * `$e`: The expression.
///
/// # Return value:
///
/// * If `$e` is `Ok`, `$e` unmodified.
/// * If `$e` is `Err`, `From::from($e)`.
#[macro_export]
macro_rules! ttry {
	($e:expr) => {
		match $e { Ok(x) => Ok(x), Err(y) => Err(From::from(y)) }
	}
}

#[allow(missing_docs)]
#[deprecated(since="1.2.1", note="Use matches!() instead")]
#[macro_export]
macro_rules! if_match
{
	($e:expr, $($p:tt)+) => { match $e { $($p)* => true, _ => false } }
}

///Extension methods on `Result` type.
pub trait ResultExt<T:Sized,E:Sized>
{
	///Set the `Ok()` value if `Result` is `Ok`.
	///
	/// # Parameters:
	///
	/// * `val`: The value to set to.
	///
	/// # Returns value:
	///
	///The resulting result.
	fn set_ok<U:Sized>(self, val: U) -> Result<U,E>;
	///Set the `Err()` value if `Result` is `Err`.
	///
	/// # Parameters:
	///
	/// * `val`: The value to set to.
	///
	/// # Returns value:
	///
	///The resulting result.
	fn set_err<U:Sized>(self, val: U) -> Result<T,U>;
}

impl<T:Sized,E:Sized> ResultExt<T,E> for Result<T,E>
{
	fn set_ok<U:Sized>(self, val: U) -> Result<U,E>
	{
		match self { Ok(_) => Ok(val), Err(e) => Err(e) }
	}
	fn set_err<U:Sized>(self, val: U) -> Result<T,U>
	{
		match self { Ok(v) => Ok(v), Err(_) => Err(val) }
	}
}

#[cfg(test)]
mod test;
