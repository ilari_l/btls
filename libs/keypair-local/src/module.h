#ifndef _btls_module_h_included_
#define _btls_module_h_included_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>

#define BTLS_ALG_RSA_PKCS1_SHA256 0x0401
#define BTLS_ALG_RSA_PKCS1_SHA384 0x0501
#define BTLS_ALG_RSA_PKCS1_SHA512 0x0601
#define BTLS_ALG_RSA_PSS_SHA256 0x0804
#define BTLS_ALG_RSA_PSS_SHA384 0x0805
#define BTLS_ALG_RSA_PSS_SHA512 0x0806
#define BTLS_ALG_ECDSA_P256_SHA256 0x0403
#define BTLS_ALG_ECDSA_P384_SHA384 0x0503
#define BTLS_ALG_ED25519 0x0807
#define BTLS_ALG_ED448 0x0808

struct btls_signature_reply
{
	//The context. Pass this to all functions in this structure.
	void* context;
	//Finish signing and return reply.
	//
	//The structure MUST NOT be used in any way after this call.
	//
	//Set the parameters as follows:
	//
	//context => The context field of the parent structure.
	//signature => Pointer to signature, or NULL if signing failed. The signature needs to be stable during the
	//             duration of this call.
	//signaturelen => The length of signature (only meaningful if signature is not NULL).
	void (*finish)(void* context, const uint8_t* signature, size_t signaturelen);
	//Log a message.
	//
	//Set the context to the context field of the parent structure. The message is in UTF-8!
	void (*log)(void* context, const char* message);
};

struct btls_pubkey_reply
{
	//The context. Pass this to all functions in this structure.
	void* context;
	//Return the public key and available algorithms.
	//
	//Set the parameters as follows:
	//
	//context => The context field of the parent structure.
	//pubkey => The public key. The public key needs to be stable during duration of this call.
	//pubkeylen => The length of public key in bytes.
	//schemes => Available TLS SignatureSchemes. See the BTLS_ALG_* constants. This needs to be stable
	///          during the duration of this call.
	//schemescount => Number of schemes available.
	void (*finish)(void* context, const uint8_t* pubkey, size_t pubkeylen, const uint16_t* schemes,
		size_t schemescount);
};

//Get handle to key.
//
//Parameters:
//
// * name: The name of the key to get handle for.
// * errcode: If error is encountered, write some code here to help debugging.
//
//Returns:
//
// * Handle for the key. If name is invalid, return an unspecified invalid handle.
//
//Notes:
//
// * The module needs to implement either this function or btls_mod_get_key_handle2, or both.
uint64_t btls_mod_get_key_handle(const char* name, uint32_t* errcode);

//Get handle to inline key.
//
//Parameters:
//
// * data: The the key to get handle for. Only held stable during the call.
// * datalen: The length of the key.
// * errcode: If error is encountered, write some code here to help debugging.
//
//Returns:
//
// * Handle for the key. If name is invalid, return an unspecified invalid handle.
//
//Notes:
//
// * The module needs to implement either this function or btls_mod_get_key_handle, or both.
// * The key data needs to be copied, as the buffer is not valid after call.
uint64_t btls_mod_get_key_handle2(const uint8_t* data, size_t datalen, uint32_t* errcode);

//Request signature
//
//Note that after signature generation either succeeds or fails, the reply.finish(reply.context, ...)
//MUST be called. It is safe to transfer reply structure to another thread and return from this function
//before calling finish. In fact, this is recommended if signing operation is not very fast. However, if you
//do this, take note that data is only stable during this call, so it needs to be copied before transfer to
//another thread.
//
//Parameters:
//
// * handle: The handle of the key.
// * reply: The signature reply structure.
// * algorithm: The algorithm to sign with, one of BTLS_ALG_*.
// * data: The data to sign. Note: This is only stable during duration of this call.
// * datalen: The length of data to sign in bytes.
//
//Notes:
//
// * The module needs to implement this function.
void btls_mod_request_sign(uint64_t handle, struct btls_signature_reply reply, uint16_t algorithm,
	const uint8_t* data, size_t datalen);

//Request public key and available schemes.
//
//Note that this function needs to call reply.finish(reply.context, ...) WITHIN ITS EXECUTION, or it is deemed
//to have failed. It is NOT safe to transfer the handle to another thread.
//
//This is called at key load time, so that this executes quickly isn't so critical.
//
//Notes:
//
// * The module needs to implement this function.
void btls_mod_request_pubkey(uint64_t handle, struct btls_pubkey_reply reply);

#ifdef __cplusplus
}
#endif

#endif
