//!Keypairs implemented in-process.
//!
//!This crate provodes an in-process implementation of the `KeyPair` trait from `btls-aux-keypair` crate.
//!
//!The most important items are:
//!
//! * A local key pair: [`LocalKeyPair`](struct.LocalKeyPair.html)
#![deny(unsafe_code)]
#![forbid(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]

extern crate std;
pub use self::module::LogSink;
pub use self::module::ModuleError;
pub use self::module::ModuleKey;
use btls_aux_collections::Arc;
use btls_aux_collections::Box;
use btls_aux_collections::Cow;
use btls_aux_collections::String;
use btls_aux_collections::ToOwned;
use btls_aux_collections::ToString;
use btls_aux_collections::Vec;
use btls_aux_fail::dtry;
use btls_aux_fail::fail;
use btls_aux_fail::ttry;
use btls_aux_futures::FutureMapperFn;
use btls_aux_futures::FutureReceiver;
use btls_aux_signatures::convert_key_from_cwk;
use btls_aux_signatures::convert_key_from_jwk;
use btls_aux_signatures::convert_key_from_sexpr;
use btls_aux_signatures::EcdsaCurve;
use btls_aux_signatures::EcdsaKeyLoadingError;
use btls_aux_signatures::EcdsaKeyPair;
use btls_aux_signatures::Ed25519KeyLoadingError;
use btls_aux_signatures::Ed25519KeyPair;
use btls_aux_signatures::Ed448KeyLoadingError;
use btls_aux_signatures::Ed448KeyPair;
use btls_aux_signatures::Hasher;
use btls_aux_signatures::KcDecodingError;
use btls_aux_signatures::KcKeyFormat;
#[allow(deprecated)] use btls_aux_signatures::KeyPair;
use btls_aux_signatures::KeyPair2;
use btls_aux_signatures::SignatureAlgorithm2;
use btls_aux_signatures::SignatureFormat as SFmt;
use btls_aux_signatures::SignatureKeyPair;
use btls_aux_signatures::SignatureKeyPair2;
use btls_aux_signatures::SignatureKeyPair3;
use btls_aux_signatures::SubjectPublicKeyInfo;
use btls_aux_signatures::SupportedSchemes;
use btls_aux_signatures::TemporaryRandomStream;
use btls_aux_tls_iana::SignatureScheme;
#[cfg(unix)] use btls_aux_unix::Path as UnixPath;
use core::convert::AsRef;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::ops::Deref;
#[cfg(feature="std")] use std::io::Read as IoRead;
#[cfg(feature="std")] use std::path::Path;


mod module;


//Error for loading a keypair. Hide this because it isn't reachable from anything documented.
#[doc(hidden)]
#[non_exhaustive]
#[derive(Clone,Debug,PartialEq,Eq)]
pub enum KeyLoadingError
{
	#[doc(hidden)]
	Ecdsa(EcdsaKeyLoadingError),
	#[doc(hidden)]
	Ed25519(Ed25519KeyLoadingError),
	#[doc(hidden)]
	Ed448(Ed448KeyLoadingError),
	#[doc(hidden)]
	KeyDecodeError(KcDecodingError),
	#[doc(hidden)]
	UnrecognizedPrivateKeyFormat,
	#[doc(hidden)]
	UnrecognizedKeyVersion,
	#[doc(hidden)]
	UnknownPrivateKeyType,
	#[doc(hidden)]
	Module(ModuleError),
	#[doc(hidden)]
	NotSynchronouslyLoadable,
}


///Error reading key pair
///
///This error type is internally constructed by many key reading functions.
///
///This can be turned into human-readable error message using the `{}` format operator (trait `Display`).
#[derive(Debug)]
#[non_exhaustive]
pub enum KeyReadError
{
	#[doc(hidden)]
	ParseError(String, KeyLoadingError),
	#[doc(hidden)]
	OpenError(String, String),
	#[doc(hidden)]
	ReadError(String, String),
	#[doc(hidden)]
	KeyGenerationFailed,
}

impl Display for KeyLoadingError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::KeyLoadingError::*;
		match self {
			&Ecdsa(ref err) => write!(fmt, "ECDSA loading: {err}"),
			&Ed25519(ref err) => write!(fmt, "Ed25519 loading: {err}"),
			&Ed448(ref err) => write!(fmt, "Ed448 loading: {err}"),
			&KeyDecodeError(err) => write!(fmt, "Error decoding key: {err}"),
			&UnrecognizedPrivateKeyFormat => fmt.write_str("Unrecognized private key format"),
			&UnrecognizedKeyVersion => fmt.write_str("Unrecognized key format version"),
			&UnknownPrivateKeyType => fmt.write_str("Unknown private key type"),
			&Module(ref err) => write!(fmt, "Can't load module key: {err}"),
			&NotSynchronouslyLoadable => fmt.write_str("Synchronous load not supported for key type"),
		}
	}
}

impl Display for KeyReadError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::KeyReadError::*;
		match self {
			&ParseError(ref fname, ref err) => write!(fmt, "Can't parse keypair '{fname}': {err}"),
			&OpenError(ref fname, ref err) => write!(fmt, "Can't open keypair '{fname}': {err}"),
			&ReadError(ref fname, ref err) => write!(fmt, "Can't read keypair '{fname}': {err}"),
			&KeyGenerationFailed => fmt.write_str("Can't generate new key"),
		}
	}
}

//These are unwind safe.
impl std::panic::RefUnwindSafe for KeyReadError {}
impl std::panic::UnwindSafe for KeyReadError {}

//Don't put anything that isn't Send and Sync here!
enum LocalKeyPairStorage
{
	Ed25519(Ed25519KeyPair, Vec<u8>),
	Ed448(Ed448KeyPair, Vec<u8>),
	Ecdsa(EcdsaCurve, EcdsaKeyPair, Vec<u8>),
	Module(ModuleKey, Vec<u8>, Vec<u16>),
}


fn convert_key<'a>(key: &'a [u8]) -> Result<(KcKeyFormat, Cow<'a, [u8]>), KeyLoadingError>
{
	use self::KeyLoadingError::*;
	if key.get(0).map(|x|*x) == Some(40) {		//40 => ( from S-Expr.
		return ttry!(convert_key_from_sexpr(key).map_err(|x|KeyDecodeError(x)).map(|x|(x.0,x.1)));
	} else if looks_like_json(key) {		//123 => { from JSON.
		return ttry!(convert_key_from_jwk(key).map_err(|x|KeyDecodeError(x)).map(|x|(x.0,x.1)));
	} else if key.get(0).map(|x|*x&0xE0) == Some(160) {	//160-191 (CBOR major 5)
		return ttry!(convert_key_from_cwk(key).map_err(|x|KeyDecodeError(x)).map(|x|(x.0,x.1)));
	}
	fail!(UnrecognizedPrivateKeyFormat);
}

fn aspace(b: u8) -> bool
{
	b == 9 || b == 10 || b == 13 || b == 32
}

fn looks_like_json(key: &[u8]) -> bool
{
	//If the string starts with BOM, skip it.
	let key = key.strip_prefix(b"\xEF\xBB\xBF").unwrap_or(key);
	for &c in key.iter() {
		if aspace(c) { continue; }	//Ignore space.
		return c == 123;	//The first non-space must be '{'.
	}
	//If we reached here, the string was pure whitespace.
	false
}

fn is_module(data: &[u8]) -> bool
{
	data.starts_with(b"/") || data.starts_with(b"$builtin/")
}

enum ImmedateOrDelayed
{
	Immediate(Result<usize, ()>),
	Delayed(FutureReceiver<Result<Vec<u8>, ()>>),
}

impl LocalKeyPairStorage
{
	fn get_key_type(&self) -> &'static str
	{
		match self {
			&LocalKeyPairStorage::Ed25519(_, _) => "ed25519",
			&LocalKeyPairStorage::Ed448(_, _) => "ed448",
			&LocalKeyPairStorage::Ecdsa(crv, _, _) => crv.get_ecdsa_name(),
			&LocalKeyPairStorage::Module(ref x, _, _) => x.get_key_type(),
		}
	}
	fn new_synchronous(data: &[u8]) -> Result<LocalKeyPairStorage, KeyLoadingError>
	{
		use self::KeyLoadingError::*;
		//47 => / from module.
		if is_module(data) {
			let mkey = ModuleKey::new(data, true).map_err(|x|Module(x))?;
			let pubkey = mkey.get_public_key();
			let schemes = mkey.get_schemes();
			Ok(LocalKeyPairStorage::Module(mkey, pubkey, schemes))
		} else {
			fail!(NotSynchronouslyLoadable);
		}
	}
	fn new(data: &[u8]) -> Result<LocalKeyPairStorage, KeyLoadingError>
	{
		use self::KeyLoadingError::*;
		//47 => / from module.
		if is_module(data) {
			let mkey = ModuleKey::new(data, false).map_err(|x|Module(x))?;
			let pubkey = mkey.get_public_key();
			let schemes = mkey.get_schemes();
			return Ok(LocalKeyPairStorage::Module(mkey, pubkey, schemes));
		}
		let (kind, data) = convert_key(data)?;
		match kind
		{
			KcKeyFormat::Ed25519 => {
				let (_, privkey, pubkey) = Ed25519KeyPair::keypair_load(data.deref()).
					map_err(|x|Ed25519(x))?;
				Ok(LocalKeyPairStorage::Ed25519(privkey, pubkey))
			},
			KcKeyFormat::Ed448 => {
				let (_, privkey, pubkey) = Ed448KeyPair::keypair_load(data.deref()).
					map_err(|x|Ed448(x))?;
				Ok(LocalKeyPairStorage::Ed448(privkey, pubkey))
			},
			KcKeyFormat::Ecdsa => {
				let (variant, privkey, pubkey) = EcdsaKeyPair::keypair_load(data.deref()).
					map_err(|x|Ecdsa(x))?;
				Ok(LocalKeyPairStorage::Ecdsa(variant, privkey, pubkey))
			},
			_ => fail!(UnknownPrivateKeyType)
		}
	}
	fn get_schemes(&self) -> Cow<'static, [u16]>
	{
		match self {
			&LocalKeyPairStorage::Ed25519(ref key, _) => key.supported_tls_schemes(),
			&LocalKeyPairStorage::Ed448(ref key, _) => key.supported_tls_schemes(),
			&LocalKeyPairStorage::Ecdsa(_, ref key, _) => key.supported_tls_schemes(),
			&LocalKeyPairStorage::Module(_, _, ref sch) => Cow::Owned(sch.clone()),
		}
	}
	fn get_pubkey(&self) -> Vec<u8>
	{
		match self {
			&LocalKeyPairStorage::Ed25519(_, ref pkey) => pkey.clone(),
			&LocalKeyPairStorage::Ed448(_, ref pkey) => pkey.clone(),
			&LocalKeyPairStorage::Ecdsa(_, _, ref pkey) => pkey.clone(),
			&LocalKeyPairStorage::Module(_, ref pkey, _) => pkey.clone(),
		}
	}
	fn get_signature_format(&self) -> SFmt
	{
		match self {
			&LocalKeyPairStorage::Ed25519(_, _) => SFmt::Raw,
			&LocalKeyPairStorage::Ed448(_, _) => SFmt::Raw,
			&LocalKeyPairStorage::Ecdsa(_, ref key, _) => key.get_signature_format(),
			&LocalKeyPairStorage::Module(ref key, _, _) => key.get_signature_format(),
		}
	}
	fn get_signature_algorithms<'a>(&'a self) -> &'a [SignatureAlgorithm2]
	{
		match self {
			&LocalKeyPairStorage::Ed25519(ref key, _) => key.get_signature_algorithms(),
			&LocalKeyPairStorage::Ed448(ref key, _) => key.get_signature_algorithms(),
			&LocalKeyPairStorage::Ecdsa(_, ref key, _) => key.get_signature_algorithms(),
			&LocalKeyPairStorage::Module(ref key, _, _) => key.get_algorithms(),
		}
	}
	fn with_sign(f: impl FnOnce(&mut [u8]) -> ImmedateOrDelayed) -> FutureReceiver<Result<Vec<u8>, ()>>
	{
		use self::ImmedateOrDelayed::*;
		let mut output = [0;256];	//Should be big enough for ECDSA.
		FutureReceiver::from(match f(&mut output) {
			Immediate(Ok(siglen)) => dtry!(NORET output.get(..siglen).map(|x|x.to_owned())),
			Immediate(Err(_)) => Err(()),
			Delayed(x) => return x
		})
	}
	fn with_sign_fmt(fmt: SFmt, f: impl FnOnce(&mut [u8]) -> ImmedateOrDelayed) ->
		FutureReceiver<Result<(SFmt, Vec<u8>), ()>>
	{
		use self::ImmedateOrDelayed::*;
		let mut output = [0;256];	//Should be big enough for ECDSA.
		FutureReceiver::from(match f(&mut output) {
			Immediate(Ok(siglen)) => dtry!(NORET output.get(..siglen).map(|x|(fmt, x.to_owned()))),
			Immediate(Err(_)) => Err(()),
			Delayed(x) => return x.map(FutureMapperFn::new(move |sig: Result<Vec<u8>, ()>|{
				sig.map(|sig|(fmt, sig))
			}))
		})
	}
	fn sign(&self, data: &[u8], algorithm: SignatureScheme, rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<Vec<u8>, ()>>
	{
		use self::ImmedateOrDelayed::*;
		let algorithm = algorithm.get();
		Self::with_sign(|output|match self {
			&LocalKeyPairStorage::Ed25519(ref key, _) =>
				Immediate(key.keypair_sign(data, algorithm, output, rng)),
			&LocalKeyPairStorage::Ed448(ref key, _) =>
				Immediate(key.keypair_sign(data, algorithm, output, rng)),
			&LocalKeyPairStorage::Ecdsa(_, ref key, _) =>
				Immediate(key.keypair_sign(data, algorithm, output, rng)),
			&LocalKeyPairStorage::Module(ref key, _, _) =>
				Delayed(key.sign(data, algorithm))
		})
	}
	fn sign_data(&self, data: &[u8], algorithm: SignatureAlgorithm2, rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<(SFmt, Vec<u8>), ()>>
	{
		use self::ImmedateOrDelayed::*;
		let fmt = self.get_signature_format();
		Self::with_sign_fmt(fmt, |output|match self {
			&LocalKeyPairStorage::Ed25519(ref key, _) =>
				Immediate(key.sign_data(data, algorithm, output, rng)),
			&LocalKeyPairStorage::Ed448(ref key, _) =>
				Immediate(key.sign_data(data, algorithm, output, rng)),
			&LocalKeyPairStorage::Ecdsa(_, ref key, _) =>
				Immediate(key.sign_data(data, algorithm, output, rng)),
			&LocalKeyPairStorage::Module(ref key, _, _) =>
				Delayed(key.sign_data(data, algorithm))
		})
	}
	fn sign_callback(&self, algorithm: SignatureAlgorithm2, rng: &mut TemporaryRandomStream,
		msg: &mut dyn FnMut(&mut Hasher)) -> FutureReceiver<Result<(SFmt, Vec<u8>), ()>>
	{
		use self::ImmedateOrDelayed::*;
		let fmt = self.get_signature_format();
		Self::with_sign_fmt(fmt, |output|match self {
			&LocalKeyPairStorage::Ed25519(ref key, _) =>
				Immediate(key.sign_callback(output, algorithm, rng, msg)),
			&LocalKeyPairStorage::Ed448(ref key, _) =>
				Immediate(key.sign_callback(output, algorithm, rng, msg)),
			&LocalKeyPairStorage::Ecdsa(_, ref key, _) =>
				Immediate(key.sign_callback(output, algorithm, rng, msg)),
			&LocalKeyPairStorage::Module(ref key, _, _) => {
				//For now, signing callbacks is not supported, so flatten the data.
				let msg = Hasher::collect(msg);
				Delayed(key.sign_data(&msg, algorithm))
			}
		})
	}
}

struct _LocalKeyPair
{
	privkey: LocalKeyPairStorage,
	pubkey: SubjectPublicKeyInfo,
	schemes: SupportedSchemes,
	keytype: &'static str,
}

impl _LocalKeyPair
{
	fn new(data: &[u8], synchronous: bool) -> Result<_LocalKeyPair, KeyLoadingError>
	{
		let privkey = if synchronous {
			LocalKeyPairStorage::new_synchronous(data)?
		} else {
			LocalKeyPairStorage::new(data)?
		};
		let pubkey = privkey.get_pubkey();
		let schemes = privkey.get_schemes();
		let keytype = privkey.get_key_type();
		Ok(_LocalKeyPair{
			privkey:privkey,
			pubkey:SubjectPublicKeyInfo(Arc::new(pubkey)),
			schemes:SupportedSchemes(Arc::new(schemes)),
			keytype: keytype
		})
	}
	fn get_signature_format(&self) -> SFmt { self.privkey.get_signature_format() }
}

///Key pair that is implemented by signing using a local private key.
///
///This implements the [`KeyPair`](trait.KeyPair.html) trait. It is created by one of:
///
/// * Load a keypair from stream: [`new()`](#method.new)
/// * Load a keypair from file: [`new_file()`](#method.new_file)
/// * Generate a transient P-256 key: [`new_transient_ecdsa_p256()`](#method.new_transient_ecdsa_p256)
///
///# DANGER:
///
///The private key is contained in the memory space of the process and thus can be leaked by memory disclosure
///vulernabilities. Such vulernabilities do not have to reside in the TLS implementation, as memory disclosure in
///the application is just as effective for attacks.
///
///The keys that are in Internal Moudle Format can be shells for remote keys. Such keys can not be dumped using
///memory disclosure. In case of remote code execution, the difficulty of dumping the key is dictated by a variety
///of factors, ranging from easy to very hard.
#[derive(Clone)]
pub struct LocalKeyPair(Arc<_LocalKeyPair>);

impl LocalKeyPair
{
	///Create a new key from byte slice.
	pub fn new_bytes(privkey: &[u8], name: &str, synchronous: bool) -> Result<LocalKeyPair, KeyReadError>
	{
		let low = _LocalKeyPair::new(privkey, synchronous).
			map_err(|x|KeyReadError::ParseError(name.to_owned(), x))?;
		Ok(LocalKeyPair(Arc::new(low)))
	}
	#[cfg(unix)]
	fn _load_unix_file(file: &UnixPath) -> Result<(Vec<u8>, String), KeyReadError>
	{
		use btls_aux_filename::Filename;
		//use btls_aux_unix::FileDescriptor;
		use btls_aux_unix::OpenFlags;
		use btls_aux_unix::OsError;
		let filename = Filename::from_bytes(file.to_inner()).to_string();
		let fd = match file.open(OpenFlags::O_RDONLY|OpenFlags::O_CLOEXEC) {
			Ok(x) => x,
			Err(err) => fail!(KeyReadError::OpenError(filename, err.to_string()))
		};
		let mut contents = match fd.stat() {
			Ok(ref st) if st.size() > 262144 =>
				fail!(KeyReadError::ReadError(filename, "Key too big".to_string())),
			Ok(ref st) => {
				let mut contents = Vec::new();
				contents.resize(st.size() as usize, 0);
				contents
			},
			Err(err) => fail!(KeyReadError::ReadError(filename, err.to_string()))
		};
		let mut itr = 0;
		while itr < contents.len() { match fd.read(&mut contents[itr..]) {
			Ok(0) => {
				contents.truncate(itr);
				break;
			},
			Ok(amt) => itr += amt,
			Err(err) if err == OsError::EINTR => (),
			Err(err) => fail!(KeyReadError::ReadError(filename, err.to_string()))
		}}
		Ok((contents, filename))
	}
	///Load keypair out of unix file.
	#[cfg(unix)]
	pub fn new_unix_file(file: &UnixPath, synchronous: bool) -> Result<LocalKeyPair, KeyReadError>
	{
		let (contents, filename) = Self::_load_unix_file(file)?;
		Self::new_bytes(&contents, &filename, synchronous)
	}
	#[cfg(feature="std")]
	fn _new(privkey: &mut impl IoRead, name: &str, synchronous: bool) -> Result<LocalKeyPair, KeyReadError>
	{
		let mut _privkey = Vec::new();
		privkey.read_to_end(&mut _privkey).
			map_err(|x|KeyReadError::ReadError(name.to_owned(), x.to_string()))?;
		Self::new_bytes(&_privkey, name, synchronous)
	}
	#[cfg(feature="std")]
	fn _new_file(privkey: impl AsRef<Path>, synchronous: bool) -> Result<LocalKeyPair, KeyReadError>
	{
		use btls_aux_filename::Filename;
		let privkey = privkey.as_ref();
		let call_as = Filename::from_path(privkey).to_string();
		let mut _privkey = std::fs::File::open(privkey).map_err(|x|{
			KeyReadError::OpenError(call_as.clone(), x.to_string())
		})?;
		Self::_new(&mut _privkey, &call_as, synchronous)
	}
	///Create a keypair out of stream `privkey` implementing `Read`.
	///
	///The key is called `name` in various log and error messages.
	///
	///On success, returns `Ok(keypair)` where `keypair` is the loaded keypair. Otherwise returns `Err(error)`
	///where `error` describes the error that occured.
	///
	///The following key formats are currently supported.
	///
	/// * COSE key format.
	/// * JWK format.
	/// * S-Expression format.
	/// * Internal module format.
	///
	///This method can be used to load keys from buffers since `&[u8]: Read` (but note the extra `&mut`
	///qualifier, so the parameter needs to be `&mut &[u8]`).
	#[cfg(feature="std")]
	pub fn new<R1:IoRead>(privkey: &mut R1, name: &str) -> Result<LocalKeyPair, KeyReadError>
	{
		Self::_new(privkey, name, false)
	}
	///Create a keypair out of a file `privkey`.
	///
	///On success, returns `Ok(keypair)` where `keypair` is the loaded keypair. Otherwise returns `Err(error)`
	///where `error` describes the error that occured.
	///
	///See [`new()`](#method.new) for list of supported key formats.
	#[cfg(feature="std")]
	pub fn new_file<P1: AsRef<Path>>(privkey: P1) -> Result<LocalKeyPair, KeyReadError>
	{
		Self::_new_file(privkey, false)
	}
	///Like `new()`, but synchronous.
	#[cfg(feature="std")]
	pub fn new_synchronous<R1:IoRead>(privkey: &mut R1, name: &str) -> Result<LocalKeyPair, KeyReadError>
	{
		Self::_new(privkey, name, true)
	}
	///Like `new_file()`, but synchronous.
	#[cfg(feature="std")]
	pub fn new_file_synchronous<P1: AsRef<Path>>(privkey: P1) -> Result<LocalKeyPair, KeyReadError>
	{
		Self::_new_file(privkey, true)
	}
	///Create a fresh temporary ECDSA P-256 keypair.
	///
	///On success, returns `Ok(keypair)` where `keypair` is the loaded keypair. Otherwise returns `Err(error)`
	///where `error` describes the error that occured.
	///
	///Note that this is only suitable for transient keys: There is no way to save the keypair anywhere. This
	///is mainly meant to generate keys for ACME validation requests.
	pub fn new_transient_ecdsa_p256(rng: &mut TemporaryRandomStream) -> Result<LocalKeyPair, KeyReadError>
	{
		use self::KeyReadError::*;
		let (kp, pubkey) = match EcdsaKeyPair::new_transient_ecdsa_p256(rng) {
			Ok((x, y)) => (x, y),
			Err(_) => fail!(KeyGenerationFailed)
		};
		let privkey = LocalKeyPairStorage::Ecdsa(EcdsaCurve::NsaP256, kp, pubkey.clone());
		let schemes = privkey.get_schemes();
		let keytype = "ecdsa-p256";
		Ok(LocalKeyPair(Arc::new(_LocalKeyPair{privkey:privkey, pubkey:SubjectPublicKeyInfo(
			Arc::new(pubkey)), schemes:SupportedSchemes(Arc::new(schemes)),
			keytype:keytype})))
	}
	///Load a module `name`.
	pub fn load_module_trusted(name: &str) -> Result<(), String>
	{
		ModuleKey::load_module_trusted(name).map_err(|e|e.to_string())
	}
	///Set no modules flag.
	pub fn disable_module_loading() { ModuleKey::disable_module_loading() }
	///Set module log sink.
	pub fn set_module_log_sink(sink: Box<dyn LogSink+Send+Sync>) { ModuleKey::set_module_log_sink(sink) }
	///Get signature format for public key.
	pub fn get_signature_format(&self) -> SFmt { self.0.get_signature_format() }
	///Get signing algorithms supported.
	pub fn get_signature_algorithms<'a>(&'a self) -> &'a [SignatureAlgorithm2]
	{
		self.0.deref().privkey.get_signature_algorithms()
	}
	///Sign with callback.
	pub fn sign_callback(&self, algorithm: SignatureAlgorithm2, rng: &mut TemporaryRandomStream,
		msg: &mut dyn FnMut(&mut Hasher)) -> FutureReceiver<Result<(SFmt, Vec<u8>), ()>>
	{
		self.0.deref().privkey.sign_callback(algorithm, rng, msg)
	}
}

#[allow(deprecated)]
impl KeyPair for LocalKeyPair
{
	fn sign(&self, data: &[u8], algorithm: u16, rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<Vec<u8>, ()>>
	{
		self.0.deref().privkey.sign(data, SignatureScheme::new(algorithm), rng)
	}
	fn get_schemes(&self) -> SupportedSchemes { self.0.deref().schemes.clone() }
	fn get_public_key(&self) -> SubjectPublicKeyInfo { self.0.deref().pubkey.clone() }
	fn get_key_type(&self) -> &'static str { self.0.deref().keytype }
}

impl KeyPair2 for LocalKeyPair
{
	fn get_signature_algorithms<'a>(&'a self) -> &'a [SignatureAlgorithm2]
	{
		self.0.deref().privkey.get_signature_algorithms()
	}
	fn sign_data(&self, data: &[u8], algorithm: SignatureAlgorithm2, rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<(SFmt, Vec<u8>), ()>>
	{
		self.0.deref().privkey.sign_data(data, algorithm, rng)
	}
	fn sign_callback(&self, algorithm: SignatureAlgorithm2, rng: &mut TemporaryRandomStream,
		msg: &mut dyn FnMut(&mut Hasher)) -> FutureReceiver<Result<(SFmt, Vec<u8>), ()>>
	{
		self.0.deref().privkey.sign_callback(algorithm, rng, msg)
	}
	fn get_public_key2<'a>(&'a self) -> &'a [u8] { self.0.deref().pubkey.0.deref().deref() }
	fn get_key_type2<'a>(&'a self) -> &'a str { self.0.deref().keytype }
}


#[cfg(test)]
mod test;
