use crate::_LocalKeyPair;
use crate::LocalKeyPair;
use btls_aux_collections::Arc;
use btls_aux_collections::String;
use btls_aux_collections::Vec;
use btls_aux_filename::Filename;
use btls_aux_random::TemporaryRandomLifetimeTag;
use btls_aux_random::TemporaryRandomStream;
use btls_aux_signatures::KeyPair;
use btls_aux_signatures::SignatureAlgorithmEnabled2;
use btls_aux_signatures::SignatureAlgorithmTls2;
use btls_aux_signatures::SignatureFlags2;
use btls_aux_signatures::SignatureTls2;
use std::format;
use std::fs::File;
use std::io::Read;
use std::path::Path;


fn kp_new_file(privkey: &str) -> Result<LocalKeyPair, String>
{
	let privkey = Path::new(privkey);
	let kdisp = Filename::from_path(privkey);
	let mut _privkey = File::open(privkey).map_err(|err|{
		format!("Can't open keypair '{kdisp}': {err}")
	})?;
	let mut __privkey = Vec::new();
	_privkey.read_to_end(&mut __privkey).map_err(|err|{
		format!("Can't read keypair '{kdisp}': {err}")
	})?;
	let k = _LocalKeyPair::new(&__privkey, false).map_err(|err|{
		format!("Can't parse keypair '{kdisp}': {err}")
	})?;
	Ok(LocalKeyPair(Arc::new(k)))
}

#[test]
fn test_eddsa()
{
	let scheme = 0x807;
	let algo = SignatureAlgorithmTls2::by_tls_id(scheme).unwrap();
	let any_policy = SignatureAlgorithmEnabled2::unsafe_any_policy();
	let keypair = kp_new_file("src/testkey.raw").unwrap();
	let message = b"Hello, World!";
	assert_eq!(&(keypair.get_schemes().iter().cloned().collect::<Vec<_>>()), &[scheme]);
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let s = keypair.sign(message, scheme, &mut rng).read();
	let s = s.ok().unwrap();
	let sig = s.unwrap();
	assert_eq!(sig.len(), 64);
	let key = keypair.get_public_key();
	let signature = SignatureTls2::new(algo, &sig[..]);
	signature.verify(&key.0, message, any_policy, SignatureFlags2::CERTIFICATE).unwrap();
	signature.verify(&key.0, message, any_policy, SignatureFlags2::NO_RSA).unwrap();
}

#[test]
fn test_eddsa_unknown()
{
	let scheme = 0x403;
	let keypair = kp_new_file("src/testkey.raw").unwrap();
	let message = b"Hello, World!";
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let s = keypair.sign(message, scheme, &mut rng).read();
	let s = s.ok().unwrap();	//Future settled.
	s.unwrap_err();			//Signing failed.
}

#[test]
fn test_eddsa2()
{
	let scheme = 0x808;
	let algo = SignatureAlgorithmTls2::by_tls_id(scheme).unwrap();
	let any_policy = SignatureAlgorithmEnabled2::unsafe_any_policy();
	let refsig = include_bytes!("hellow.sig");
	let keypair = kp_new_file("src/testkey2.raw").unwrap();
	let message = b"Hello, World!";
	assert_eq!(&(keypair.get_schemes().iter().cloned().collect::<Vec<_>>()), &[scheme]);
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let s = keypair.sign(message, scheme, &mut rng).read();
	let s = s.ok().unwrap();
	let sig = s.unwrap();
	assert_eq!(sig.len(), 114);
	assert_eq!(&sig[..], &refsig[..]);
	let key = keypair.get_public_key();
	let signature = SignatureTls2::new(algo, &sig[..]);
	signature.verify(&key.0, message, any_policy, SignatureFlags2::CERTIFICATE).unwrap();
	signature.verify(&key.0, message, any_policy, SignatureFlags2::NO_RSA).unwrap();
}

#[test]
fn test_eddsa2_unknown()
{
	let scheme = 0x807;
	let keypair = kp_new_file("src/testkey2.raw").unwrap();
	let message = b"Hello, World!";
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let s = keypair.sign(message, scheme, &mut rng).read();
	let s = s.ok().unwrap();	//Future settled.
	s.unwrap_err();			//Signing failed.
}

#[test]
fn test_jose_boms()
{
	LocalKeyPair::new_file("src/test-boms.jose").unwrap();
}
