//Module keys
//
//This module provodes an implementation of module keys (keys where operations are delegated to an module, in
//this case a in-process shared object).
//
//This can be used e.g. for implementing out-of-process (implying not vulernable to e.g. Heartbleed) keys. It Can
//also be used for having the keys reside on a HSM.

#![allow(unsafe_code)]
use btls_aux_collections::Arc;
use btls_aux_collections::Box;
use btls_aux_collections::BTreeMap;
use btls_aux_collections::GlobalMutex;
use btls_aux_collections::GlobalRwLock;
use btls_aux_collections::String;
use btls_aux_collections::ToOwned;
use btls_aux_collections::ToString;
use btls_aux_collections::Vec;
use btls_aux_dlfcn::LibraryHandle as DlfcnHandle;
use btls_aux_fail::dtry;
use btls_aux_fail::f_return;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::ResultExt;
use btls_aux_filename::Filename;
use btls_aux_futures::create_future;
use btls_aux_futures::FutureReceiver;
use btls_aux_futures::FutureSender;
use btls_aux_memory::EscapeByteString;
use btls_aux_memory::ObjectExt;
use btls_aux_memory::NonNull;
use btls_aux_memory::NonNullMut;
use btls_aux_memory::split_at;
use btls_aux_signatures::SignatureAlgorithm2;
use btls_aux_signatures::SignatureAlgorithmTls2;
use btls_aux_signatures::SignatureFormat;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::str::from_utf8;
use core::sync::atomic::AtomicBool;
use core::sync::atomic::Ordering;

static DISABLE_MODULE_LOADING: AtomicBool = AtomicBool::new(false);

type GetKeyHandleT = extern "C" fn(*const u8, *mut u32) -> u64;
type GetKeyHandle2T = extern "C" fn(*const u8, usize, *mut u32) -> u64;
type RequestSignT = extern "C" fn(u64, SignatureRequest, u16, *const u8, usize);
type RequestPubkeyT = extern "C" fn(u64, PubkeyRequest);


struct RawLibrarySymbols
{
	get_key_handle: Option<GetKeyHandleT>,
	get_key_handle2: Option<GetKeyHandle2T>,
	request_sign: Option<RequestSignT>,
	request_pubkey: Option<RequestPubkeyT>,
}

impl RawLibrarySymbols
{
	fn new(lib: &DlfcnHandle) -> RawLibrarySymbols
	{
		static GET_KEY_HANDLE_SYM: &'static [u8] = b"btls_mod_get_key_handle";
		static GET_KEY_HANDLE2_SYM: &'static [u8] = b"btls_mod_get_key_handle2";
		static REQUEST_SIGN_SYM: &'static [u8] = b"btls_mod_request_sign";
		static REQUEST_PUBKEY_SYM: &'static [u8] = b"btls_mod_request_pubkey";
		RawLibrarySymbols {
			get_key_handle: unsafe{lib.load_typed(GET_KEY_HANDLE_SYM).ok()},
			get_key_handle2: unsafe{lib.load_typed(GET_KEY_HANDLE2_SYM).ok()},
			request_sign: unsafe{lib.load_typed(REQUEST_SIGN_SYM).ok()},
			request_pubkey: unsafe{lib.load_typed(REQUEST_PUBKEY_SYM).ok()},
		}
	}
}

///Log sink.
pub trait LogSink
{
	///Message printed.
	fn print(&mut self, priority: u8, msg: &str);
}

struct LogSinkS
{
	sink: Option<Box<dyn LogSink+Send+Sync>>,
}

impl Default for LogSinkS
{
	fn default() -> LogSinkS
	{
		LogSinkS {
			sink: None,
		}
	}
}

impl LogSinkS
{
	fn print(&mut self, msg: &str)
	{
		if let Some(sink) = self.sink.as_mut() {
			let (head, tail) = split_at(msg, 3).unwrap_or(("", msg));
			let (prio, msg) = match head.as_bytes() {
				&[b'<',p,b'>'] if p & 248 == 48 => (p, tail),
				_ => (b'5', msg),		//5 => Notice.
			};
			sink.print(prio, msg);
		} else {
			__default_log_sink(msg);
		}
	}
}

#[cfg(feature="std")]
fn __default_log_sink(msg: &str)
{
	use std::io::stderr;
	use std::io::Write as IoWrite;
	//Ignore errors.
	let _ = writeln!(stderr(), "{msg}");
}

//TODO: Version that uses Unix.
#[cfg(not(feature="std"))]
fn __default_log_sink(_: &str)
{
	//Just discard the message.
}

static LOG_SINK: GlobalMutex<LogSinkS> = GlobalMutex::new();

//Has to be public due to private-in-public rules.
pub struct _SignatureRequest
{
	answer: FutureSender<Result<Vec<u8>, ()>>,
}

//Has to be public due to private-in-public rules.
#[doc(hidden)]
#[repr(C)]
pub struct SignatureRequest
{
	context: *mut _SignatureRequest,
	finish: unsafe extern "C" fn(NonNullMut<_SignatureRequest>, Option<NonNull<u8>>, usize),
	//This is supposed to be c_char, but signedness does not matter, as all bytes are effectively
	//typecasted if the signedness does not match.
	log: unsafe extern "C" fn(NonNullMut<_SignatureRequest>, NonNull<u8>)
}

impl SignatureRequest
{
	fn new() -> (SignatureRequest, FutureReceiver<Result<Vec<u8>, ()>>)
	{
		let (sender, receiver) = create_future();
		let req = Box::new(_SignatureRequest{answer:sender});
		let req = SignatureRequest{
			context: Box::into_raw(req),
			finish: __signature_request_finish,
			log: __signature_request_log
		};
		(req, receiver)
	}
}

//Has to be public due to private-in-public rules.
pub struct _PubkeyRequest
{
	called: bool,
	pubkey: Vec<u8>,
	schemes: Vec<u16>,
}

//Has to be public due to private-in-public rules.
#[doc(hidden)]
#[repr(C)]
pub struct PubkeyRequest
{
	context: *mut _PubkeyRequest,
	finish: unsafe extern "C" fn(NonNullMut<_PubkeyRequest>, NonNull<u8>, usize, NonNull<u16>, usize),
}

impl PubkeyRequest
{
	fn new(func: RequestPubkeyT, handle: u64) -> Option<(Vec<u8>, Vec<u16>)>
	{
		let mut lreq = _PubkeyRequest{called: false, pubkey:Vec::new(), schemes:Vec::new()};
		let req = PubkeyRequest{
			context: lreq.pointer_to_mut(),
			finish: __pubkey_request_finish
		};
		(func)(handle, req);
		if lreq.called { Some((lreq.pubkey, lreq.schemes)) } else { None }
	}
}

unsafe extern "C" fn __signature_request_finish(mut sig: NonNullMut<_SignatureRequest>, data: Option<NonNull<u8>>,
	datalen: usize)
{
	//The sig can be turned back into a box, and dropped at the end.
	let sig: Box<_SignatureRequest> = unsafe{Box::from_raw(sig.as_mut())};
	let data = match data {
		//The data is of length datalen by definition.
		Some(data) => Ok(unsafe{data.make_slice(datalen).to_vec()}),
		None => Err(())
	};
	sig.answer.settle(data);
}

//This is supposed to be c_char, but signedness does not matter, as all bytes are effectively
//typecasted if the signedness does not match.
unsafe extern "C" fn __signature_request_log(_sig: NonNullMut<_SignatureRequest>, msg: NonNull<u8>)
{
	let msg = EscapeByteString(unsafe{msg.cstr_as_slice()}).to_string();
	LOG_SINK.with(|log|log.print(&msg));
}

unsafe extern "C" fn __pubkey_request_finish(mut pubk: NonNullMut<_PubkeyRequest>, pubkey: NonNull<u8>,
	pubkeylen: usize, schemes: NonNull<u16>, schemescount: usize)
{
	let pubk =  unsafe{pubk.as_mut()};
	pubk.called = true;
	//The public key is pubkeylen by definition, and schemes is schemescount by definition.
	pubk.pubkey = unsafe{pubkey.make_slice(pubkeylen).to_vec()};
	pubk.schemes = unsafe{schemes.make_slice(schemescount).to_vec()};
}

fn __security_check(cname: &Filename) -> Result<(), ModuleError>
{
	//Always pass builtins, as those are already loaded.
	if cname.starts_with("$builtin/") { return Ok(()); }
	//Never pass if module loading has been disabled.
	fail_if!(DISABLE_MODULE_LOADING.load(Ordering::Relaxed),
		ModuleError::CantLoadLibrary(cname.to_string(), "Module loading disabled".to_owned()));
	//Unix-specific owner check.
	#[cfg(unix)] {
		use core::fmt::Write;
		use btls_aux_unix::Gid;
		use btls_aux_unix::Path;
		use btls_aux_unix::Uid;
		let r_uid = Uid::get_real();
		let privuid = if r_uid != Uid::get_effective() ||
			Gid::get_real() != Gid::get_effective() { Uid::root() } else { r_uid };
		let failed = |msg:String|ModuleError::CantLoadLibrary(cname.to_string(), msg);
		let cname2: &Path = cname.into_unix_path().ok_or_else(||failed("Invalid filename".to_string()))?;
		let st = cname2.stat().map_err(|err|{
			let mut msg = String::new();
			write!(msg, "Can't stat: {err}").ok();	//Can't fail.
			failed(msg)
		})?;
		fail_if!(st.uid() != privuid && st.uid() != Uid::root(),
			failed("Permission denied (bad owner)".to_string()));
		fail_if!(st.permissions() & 64 != 64, failed("Permission denied (no +x)".to_string()));
	}
	//Ok.
	Ok(())
}

struct ModuleLibraryHandle
{
	//This purely exists to hold handle to library to prevent unloading.
	_library: DlfcnHandle,
	get_key_handle: Option<GetKeyHandleT>,
	get_key_handle2: Option<GetKeyHandle2T>,
	request_sign: RequestSignT,
	request_pubkey: RequestPubkeyT,
}

//This is thread-safe.
unsafe impl Send for ModuleLibraryHandle {}
unsafe impl Sync for ModuleLibraryHandle {}

impl ModuleLibraryHandle
{
	fn new(name: &Filename, override_security: bool) -> Result<ModuleLibraryHandle, ModuleError>
	{
		use self::ModuleError::NotValidModule;
		if !override_security { __security_check(name)?; }
		let lib = DlfcnHandle::new2(name).
			map_err(|err|ModuleError::CantLoadLibrary(name.to_string(), err.to_string()))?;
		let syms = RawLibrarySymbols::new(&lib);
		//We need btls_mod_get_key_handle or btls_mod_get_key_handle2 or both.
		fail_if!(syms.get_key_handle.is_none() && syms.get_key_handle2.is_none(), NotValidModule);
		Ok(ModuleLibraryHandle{
			_library: lib,
			get_key_handle: syms.get_key_handle,
			get_key_handle2: syms.get_key_handle2,
			request_sign: syms.request_sign.ok_or(NotValidModule)?,
			request_pubkey: syms.request_pubkey.ok_or(NotValidModule)?,
		})
	}
	fn has_get_key_handle(&self) -> bool { self.get_key_handle.is_some() }
	fn has_get_key_handle2(&self) -> bool { self.get_key_handle2.is_some() }
	fn get_key_handle(&self, name: &str, errcode: &mut u32) -> Result<KeyHandle, ()>
	{
		//NUL-terminate the name.
		let mut n = name.as_bytes().to_owned();
		n.push(0);
		let func = dtry!(self.get_key_handle);
		let h = func(n.as_ptr(), errcode.pointer_to_mut());
		Ok(KeyHandle(h))
	}
	fn get_key_handle2(&self, data: &[u8], errcode: &mut u32) -> Result<KeyHandle, ()>
	{
		let func = dtry!(self.get_key_handle2);
		let h = func(data.as_ptr(), data.len(), errcode.pointer_to_mut());
		Ok(KeyHandle(h))
	}
	fn request_sign(&self, handle: KeyHandle, alg: u16, data: &[u8]) -> FutureReceiver<Result<Vec<u8>, ()>>
	{
		let _data = data.as_ptr();
		let _datalen = data.len();
		let (ctx, receiver) = SignatureRequest::new();
		(self.request_sign)(handle.0, ctx, alg, _data, _datalen);
		receiver
	}
	fn request_pubkey(&self, handle: KeyHandle) -> Result<(Vec<u8>, Vec<u16>), ()>
	{
		dtry!(NORET PubkeyRequest::new(self.request_pubkey, handle.0))
	}
}

#[derive(Clone)]
struct ModuleLibraryHandleA(Arc<ModuleLibraryHandle>);

impl ModuleLibraryHandleA
{
	fn new(name: &Filename, override_security: bool) -> Result<ModuleLibraryHandleA, ModuleError>
	{
		let low = ModuleLibraryHandle::new(name, override_security)?;
		Ok(ModuleLibraryHandleA(Arc::new(low)))
	}
	fn has_get_key_handle(&self) -> bool { self.0.has_get_key_handle() }
	fn has_get_key_handle2(&self) -> bool { self.0.has_get_key_handle2() }
	fn get_key_handle(&self, name: &str, errcode: &mut u32) -> Result<KeyHandle, ()>
	{
		self.0.get_key_handle(name, errcode)
	}
	fn get_key_handle2(&self, data: &[u8], errcode: &mut u32) -> Result<KeyHandle, ()>
	{
		self.0.get_key_handle2(data, errcode)
	}
	fn request_sign(&self, handle: KeyHandle, alg: u16, data: &[u8]) -> FutureReceiver<Result<Vec<u8>, ()>>
	{
		self.0.request_sign(handle, alg, data)
	}
	fn request_pubkey(&self, handle: KeyHandle) -> Result<(Vec<u8>, Vec<u16>), ()>
	{
		self.0.request_pubkey(handle)
	}
}

static MODULES: GlobalRwLock<BTreeMap<String, ModuleLibraryHandleA>> = GlobalRwLock::new();

//Synchronous operation, does not load modules.
fn lookup_module_synchronous(name: &str) -> Option<ModuleLibraryHandleA>
{
	MODULES.with_read(|x|x.get(name).map(Clone::clone))
}

//Loads the module if it doesn't exist yet.
fn lookup_module(name: &str, override_security: bool) -> Result<ModuleLibraryHandleA, ModuleError>
{
	//If module is already loaded, return it.
	if let Some(x) = MODULES.with_read(|x|x.get(name).map(Clone::clone)) { return Ok(x); }
	//Not in map, need to load it.
	MODULES.with_write(|x|{
		//Check again, race might have added it.
		if let Some(x) = x.get(name) { return Ok(x.clone()); }
		//Nope, really need to load it.
		let name2 = Filename::from_str(name);
		let h = ModuleLibraryHandleA::new(name2, override_security)?;
		x.insert(name.to_owned(), h.clone());
		Ok(h)
	})
}

//Error from module. Hide this because it isn't reachable from anything documented.
#[doc(hidden)]
#[non_exhaustive]
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum ModuleError
{
	#[doc(hidden)]
	CantLoadLibrary(String, String),
	#[doc(hidden)]
	NotValidModule,
	#[doc(hidden)]
	CantLockModules,
	#[doc(hidden)]
	NoSuchKey(u32),
	#[doc(hidden)]
	MalformedReference,
	#[doc(hidden)]
	OutOfLineRefsNotSupported,
	#[doc(hidden)]
	InLineRefsNotSupported,
	#[doc(hidden)]
	SynchronousNotInline,
	#[doc(hidden)]
	SynchronousNotLoaded,
	#[doc(hidden)]
	UnknownAlgorithm,
}

impl Display for ModuleError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::ModuleError::*;
		match self {
			&CantLoadLibrary(ref lib, ref err) =>
				write!(fmt, "Can't load referenced library '{lib}': {err}"),
			&NotValidModule => fmt.write_str("Not a valid module"),
			&CantLockModules => fmt.write_str("Can't lock module storage"),
			&NoSuchKey(errcode) => write!(fmt, "No such key found (code {errcode})"),
			&MalformedReference => fmt.write_str("Malformed module reference file"),
			&OutOfLineRefsNotSupported =>
				fmt.write_str("Out-of-line references not supported by module"),
			&InLineRefsNotSupported => fmt.write_str("In-line references not supported by module"),
			&SynchronousNotInline => fmt.write_str("Synchronous loads must be inline keys"),
			&SynchronousNotLoaded => fmt.write_str("Synchronous loads from not loaded module"),
			&UnknownAlgorithm => fmt.write_str("No known algorithms supported by key"),
		}
	}
}

#[derive(Copy,Clone)]
struct KeyHandle(u64);




fn guess_keytype(schemes: &[u16]) -> &'static str
{
	for i in schemes.iter() {
		match *i {
			0x807 => return "ed25519",
			0x808 => return "ed448",
			0x403 => return "ecdsa-p256",
			0x503 => return "ecdsa-p384",
			0x603 => return "ecdsa-p521",
			0x401 => return "rsa",
			0x501 => return "rsa",
			0x601 => return "rsa",
			0x804 => return "rsa",
			0x805 => return "rsa",
			0x806 => return "rsa",
			_ => (),
		};
	}
	"unknown"
}

//Module keys. Hide this because it is for some reason exported, even if it should not be.
#[doc(hidden)]
pub struct ModuleKey(ModuleLibraryHandleA, KeyHandle, Vec<u8>, Vec<u16>, &'static str,
	Vec<SignatureAlgorithm2>, SignatureFormat);

impl ModuleKey
{
	fn new_inline(data: &[u8], synchronous: bool) -> Result<ModuleKey, ModuleError>
	{
		//Find the first unescaped \r or \n.
		let mut mendpos = data.len();
		let mut escape = false;
		for i in data.iter().enumerate() {
			if escape {
				escape = false;
			} else {
				escape = *i.1 == 92;
				if *i.1 == 13 || *i.1 == 10 { mendpos = i.0; break; }
			}
		}
		//mendpos is tied to length and thus can't be near overflow.
		let lfa = data.get(mendpos).map(|x|*x);
		let lfb = data.get(mendpos+1).map(|x|*x);
		let dpos = if lfa == Some(10) {
			mendpos + 1
		} else if lfa == Some(13) && lfb != Some(10) {
			mendpos + 1
		} else if lfa == Some(13) && lfb == Some(10) {
			mendpos + 2
		} else {
			fail!(ModuleError::MalformedReference);
		};
		fail_if!(dpos > data.len(), ModuleError::MalformedReference);
		let keydata = &data[dpos..];
		let lname = &data[..mendpos];	//mendpos is smaller than dpos.
		let lname = from_utf8(lname).set_err(ModuleError::MalformedReference)?;
		let mut libname = String::new();
		escape = false;
		for i in lname.chars() {
			if escape {
				libname.push(i);
				escape = false;
			} else if i == '\\' {
				escape = true;
			} else {
				libname.push(i);
			}
		}
		let modulelib = if synchronous {
			//Due to constraints, the key must always be inline (like in get_key_handle2).
			lookup_module_synchronous(&libname).ok_or(ModuleError::SynchronousNotLoaded)?
		} else {
			let modulelib = lookup_module(&libname, false)?;
			modulelib
		};
		//Inline keys require get_key_handle2().
		fail_if!(!modulelib.has_get_key_handle2(), ModuleError::InLineRefsNotSupported);
		let mut errcode = 0u32;
		let handle = modulelib.get_key_handle2(keydata, &mut errcode);
		Self::new_tail(modulelib, handle, errcode)
	}
	fn new_out_of_line(data: &[u8]) -> Result<ModuleKey, ModuleError>
	{
		let mut libname = String::new();
		let mut keyname = String::new();
		let mut escape = false;
		let mut component = false;
		let mut end = false;
		let data = from_utf8(data).set_err(ModuleError::MalformedReference)?;
		for i in data.chars() {
			fail_if!(end && i != '\r' && i != '\n', ModuleError::MalformedReference);
			let c = if component { &mut keyname } else { &mut libname };
			if escape {
				c.push(i);
				escape = false;
			} else {
				if i == '\r' {
					end = true;
				} else if i == '\n' {
					end = true;
				} else if i == '\\' {
					escape = true;
				} else if i == ' ' {
					//Expect 2 components.
					fail_if!(component, ModuleError::MalformedReference);
					component = true;
				} else {
					c.push(i);
				}
			}
		}
		fail_if!(keyname == "", ModuleError::MalformedReference);
		let modulelib = lookup_module(&libname, false)?;
		let mut errcode = 0u32;
		fail_if!(!modulelib.has_get_key_handle(), ModuleError::OutOfLineRefsNotSupported);
		let handle = modulelib.get_key_handle(&keyname, &mut errcode);
		Self::new_tail(modulelib, handle, errcode)
	}
	fn new_tail(modulelib: ModuleLibraryHandleA, handle: Result<KeyHandle, ()>, errcode: u32) ->
		Result<ModuleKey, ModuleError>
	{
		let handle = handle.set_err(ModuleError::NoSuchKey(0xFFFFFFFF))?;
		let (pubkey, schemes) = modulelib.request_pubkey(handle).
			set_err(ModuleError::NoSuchKey(errcode))?;
		let keytype = guess_keytype(&schemes);
		let format = SignatureFormat::for_public_key(&pubkey);
		let palg: Vec<SignatureAlgorithm2> = schemes.iter().filter_map(|&tlsid|{
			SignatureAlgorithmTls2::by_tls_id(tlsid).map(|x|x.to_generic())
		}).collect();
		fail_if!(palg.len() == 0, ModuleError::UnknownAlgorithm);
		Ok(ModuleKey(modulelib, handle, pubkey, schemes, keytype, palg, format))
	}
	pub(crate) fn set_module_log_sink(sink: Box<dyn LogSink+Send+Sync>)
	{
		LOG_SINK.with(|x|x.sink = Some(sink));
	}
	///Load a module.
	pub fn load_module_trusted(name: &str) -> Result<(), ModuleError>
	{
		lookup_module(name, true).map(|_|())
	}
	///Set no modules flag.
	pub fn disable_module_loading() { DISABLE_MODULE_LOADING.store(true, Ordering::Relaxed); }
	///Create a new module key.
	pub fn new(data: &[u8], synchronous: bool) -> Result<ModuleKey, ModuleError>
	{
		//Check if the reference is inline or out-of-line. If first unescaped space is \r or \n, it is
		//in-line, If it is ' ' it is out-of-line.
		let mut is_inline = None;
		let mut escape = false;
		for i in data.iter() {
			if escape {
				escape = false;
			} else {
				escape = *i == 92;
				if *i == 13 || *i == 10 { is_inline = Some(true); break; }
				if *i == 32 { is_inline = Some(false); break; }
			}
		}
		//Synchonous key load is only supported with inline keys.
		match (is_inline, synchronous) {
			(Some(false), false) => Self::new_out_of_line(data),
			(Some(false), _) => fail!(ModuleError::SynchronousNotInline),
			(Some(true), _) => Self::new_inline(data, synchronous),
			(None, _) => fail!(ModuleError::MalformedReference)
		}
	}
	///Sign with key.
	pub fn sign(&self, data: &[u8], algorithm: u16) -> FutureReceiver<Result<Vec<u8>, ()>>
	{
		self.0.request_sign(self.1, algorithm, data)
	}
	///Sign with key.
	pub fn sign_data(&self, msg: &[u8], algorithm: SignatureAlgorithm2) ->
		FutureReceiver<Result<Vec<u8>, ()>>
	{
		//Can not sign with algorithm with no TLS ID.
		let algorithm = f_return!(algorithm.downcast_tls(), FutureReceiver::from(Err(())));
		self.0.request_sign(self.1, algorithm.tls_id(), msg)
	}
	///Get list of schemes suppored.
	pub fn get_schemes(&self) -> Vec<u16> { self.3.clone() }
	///Get the public key as X.509 SPKI structure.
	pub fn get_public_key(&self) -> Vec<u8> { self.2.clone() }
	///Get the key type as string.
	pub fn get_key_type(&self) -> &'static str { self.4 }
	///Get the signature format.
	pub fn get_signature_format(&self) -> SignatureFormat { self.6 }
	///Get the supported signature algorithms.
	///
	///These are the algorithms `sign_data()` and `sign_callback()` use.
	pub fn get_algorithms(&self) -> &[SignatureAlgorithm2] { &self.5 }
}
