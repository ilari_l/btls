use super::AssertFailed;
use std::ops::Deref;

#[test]
fn construct_assert_failed_lit()
{
	let foo = assert_failure!("foo");
	assert_eq!(foo.0.deref(), "foo");
}

#[test]
fn construct_assert_failed_fmt()
{
	let foo = assert_failure!("foo {}", 6);
	assert_eq!(foo.0.deref(), "foo 6");
}

#[test]
fn construct_assert_failed_force_fmt()
{
	let foo = assert_failure!(F "foo {{}}");
	assert_eq!(foo.0.deref(), "foo {}");
}

fn no_throw() -> Result<(), AssertFailed>
{
	sanity_check!(true, "foo {}", 7);
	Ok(())
}

fn do_throw() -> Result<(), AssertFailed>
{
	sanity_check!(false, "foo {}", 8);
	Ok(())
}

fn do_throw5() -> Result<(), AssertFailed>
{
	sanity_check!(false, F "foo {{10}}");
	Ok(())
}

fn do_throw2() -> Result<(), AssertFailed>
{
	sanity_check!(false, "foo 9");
	Ok(())
}

#[test]
fn sanity_check_test()
{
	no_throw().unwrap();
	assert_eq!(do_throw().unwrap_err().0.deref(), "foo 8");
	assert_eq!(do_throw2().unwrap_err().0.deref(), "foo 9");
	assert_eq!(do_throw5().unwrap_err().0.deref(), "foo {10}");
}

fn do_throw3() -> Result<(), AssertFailed>
{
	sanity_failed!("foo {}", 15);
}

fn do_throw6() -> Result<(), AssertFailed>
{
	sanity_failed!(F "foo {{19}}");
}

fn do_throw4() -> Result<(), AssertFailed>
{
	sanity_failed!("foo 17");
}


#[test]
fn sanity_failed_test()
{
	assert_eq!(do_throw3().unwrap_err().0.deref(), "foo 15");
	assert_eq!(do_throw4().unwrap_err().0.deref(), "foo 17");
	assert_eq!(do_throw6().unwrap_err().0.deref(), "foo {19}");
}
