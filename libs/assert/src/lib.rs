//!Assertion errors.
//!
//!This crate contains types for handling assertion errors as failures. That is, without triggering unwinding, which
//!may not actually be supported by the underlying runtime.
#![deny(unsafe_code)]
#![forbid(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]

#[cfg(feature="std")] extern crate std;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
#[cfg(feature="std")] use std::error::Error;

use btls_aux_collections::String;

#[doc(hidden)]
pub mod __internal
{
	use crate::AssertFailed;
	use btls_aux_collections::String;
	pub use btls_aux_collections::ToOwned;
	pub use btls_aux_collections::ToString;
	pub use core::convert::From;
	pub use core::fmt::Arguments;
	pub use core::fmt::Write;
	pub fn empty_string() -> String { String::new() }
	pub fn format_af(args: Arguments) -> AssertFailed { AssertFailed(args.to_string()) }

	pub trait DecayResult<T:Sized>
	{
		fn decay(self) -> Option<T>;
	}

	impl<T:Sized,E:Sized> DecayResult<T> for Result<T,E>
	{
		fn decay(self) -> Option<T> { self.ok() }
	}

	impl<T:Sized> DecayResult<T> for Option<T>
	{
		fn decay(self) -> Option<T> { self }
	}
}


///Assertion failed error.
///
///The contained field is the assertion failure error message.
///
///This structure is intended to be created by the [`assert_failure!`](macro.assert_failure.html),
///[`sanity_failed!`](macro.sanity_failed.html), and [`sanity_check!`](macro.sanity_check.html) macros.
#[derive(Clone,Debug,PartialEq,Eq)]
pub struct AssertFailed(pub String);

impl AssertFailed
{
	///Prepend a prefix to assertion failed message.
	///
	/// # Parameters:
	///
	/// * `pfx`: The prefix to prepend.
	///
	/// # Return value.
	///
	///The message with given prefix prepended.
	///
	/// # Example:
	///
	/// * Prefixing string `"foo: "` into object containing `"bar`" gives an object containing
	///`"foo: bar"`.
	pub fn prefix_assert(&self, pfx: &str) -> AssertFailed
	{
		let mut newerr = crate::__internal::empty_string();
		newerr.push_str(pfx);
		newerr.push_str(&self.0);
		AssertFailed(newerr)
	}
}

//AssertFailed does not have interior mutability that can alter its length, and it can be interpretted as a byte
//string -> TrustedInvariantLength.
#[allow(unsafe_code)]
unsafe impl btls_aux_memory::TrustedInvariantLength for AssertFailed
{
	fn as_byte_slice(&self) -> &[u8] { self.0.as_byte_slice() }
}

impl Display for AssertFailed
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError> { fmt.write_str(&self.0) }
}

#[cfg(feature="std")]
impl Error for AssertFailed
{
	fn description(&self) -> &str { &self.0 }
}

///Create an unwrapped assertion failure object with the given message.
///
/// # Parameters:
///
/// * `$msg`: The raw message to use. The type `T` must statisfy `<T as ToOwned>::Owned == String`. E.g.,
///`&str`, `String` or `&String`.
/// * `$fmt`: Format string to use in order to interpolate the message.
/// * `$args`: Arguments for the format string.
///
/// # Return value:
///
///The [`AssertFailed`](struct.AssertFailed.html) object.
///
/// # Notes:
///
/// * The `$fmt` and `$args` are formatted like `format_args!($fmt,$($args)*)` (in fact, it is done exactly that
///way internally).
///
/// # Examples:
///
///This is intended to be used in `.map_err()` methods, like:
///
///```no_run
///do_something().map_err(|(x,y)|assert_failure!("Foo is too big: {} is over limit {}", x, y))?;
///```
///
///or:
///
///```no_run
///do_bar().map_err(|_|assert_failure!("Bar failed"))?;
///```
#[macro_export]
macro_rules! assert_failure
{
	($msg:expr) => {{
		use $crate::__internal::ToOwned;
		use $crate::AssertFailed;
		AssertFailed($msg.to_owned())
	}};
	(F $fmt:expr) => { $crate::__internal::format_af(format_args!($fmt)) };
	($fmt:expr,$($args:tt)*) => { $crate::__internal::format_af(format_args!($fmt, $($args)*)) };
}

///Assert that condition is `true`, and fail the function otherwise.
/// # Parameters:
///
/// * `$cond`: The expression to evaluate, of type `bool`.
/// * `$x`: Format string and its arguments for the error message.
///
/// # Early returns:
///
/// * if `$cond` is `false`.
///
/// # Notes:
///
/// * The early return uses `From` conversions from [`AssertFailed`](struct.AssertFailed.html).
/// * The `$x` is formatted like `format_args!($($x)*)`.
///
///# Examples:
///
///This is intended to be used in checks that are expected to be true, howerver might not be:
///
///```no_run
///sanity_check!(source.len() < target.len(), "Target is too small ({}, need {})", target.len(), source.len());
///(&mut target[..source.len()]).copy_from_slice(source);
///```
///
///```no_run
///sanity_check!(foo, "foo is set");
///```
#[macro_export(local_inner_macros)]
macro_rules! sanity_check
{
	($cond:expr,$($x:tt)*) => {
		if !$cond {
			let t = assert_failure!($($x)*);
			let t = $crate::__internal::From::from(t);
			return Err(t);
		}
	};
}

///Unconditionally fail the function.
///
/// # Parameters:
///
/// * `$x`: Format string and its arguments for the error message.
///
/// # Early returns:
///
/// * Always
///
/// # Notes:
///
/// * The early return uses `From` conversions from [`AssertFailed`](struct.AssertFailed.html).
/// * The `$x` is formatted like `format_args!($($x)*)`.
///
/// # Examples:
///
///This is intended to be used in match branches, like:
///
///```no_run
///match something {
///	...
///	Err(x) => sanity_failed!("something failed: {}", x)
///}
///```
///
///or:
///
///```no_run
///match something {
///	...
///	7 => sanity_failed!("something is 7???")
///	...
///}
///```
///
///[`assert_failure!`]: macro.assert_failure.html
#[macro_export(local_inner_macros)]
macro_rules! sanity_failed
{
	($($x:tt)*) => {{
		let t = assert_failure!($($x)*);
		let t = $crate::__internal::From::from(t);
		return Err(t);
	}};
}

///Assert that the value is `Some`, and fail the function otherwise.
///
/// # Parameters:
///
/// * `$cond`: The expression to evaluate, of type `Option<T>`.
/// * `$x`: Format string and its arguments for the error message.
///
/// # Early returns:
///
/// * if `$cond` is `None`.
///
/// # Return value:
///
///`$cond` with `Some()` unwrapped.
///
/// # Notes:
///
/// * The early return uses `From` conversions from [`AssertFailed`](struct.AssertFailed.html).
/// * The `$x` is formatted like `format_args!($($x)*)`.
#[macro_export(local_inner_macros)]
macro_rules! assert_some
{
	($cond:expr,$($x:tt)*) => {
		match $crate::__internal::DecayResult::decay($cond) { Some(x) => x, None => sanity_failed!($($x)*) }
	};
}

///Assert that the value is `Ok`, and fail the function otherwise.
///
/// # Parameters:
///
/// * `$cond`: The expression to evaluate, of type `Result<T,E>`.
/// * `$x`: Format string and its arguments for the error message. Named argument `err` is additionally bound to
///contents of `$cond` with `Err()` unwrapped.
///
/// # Early returns:
///
/// * if `$cond` is `Err()`.
///
/// # Return value:
///
///`$cond` with `Ok()` unwrapped.
///
/// # Notes:
///
/// * The early return uses `From` conversions from [`AssertFailed`](struct.AssertFailed.html).
/// * The `$x` is formatted like `format_args!($($x)*, err=err)`, where `err` is the value inside `Err`.
#[macro_export(local_inner_macros)]
macro_rules! assert_ok
{
	($cond:expr,$($x:tt)*) => {
		match $cond { Ok(x) => x, Err(err) => sanity_failed!($($x)*, err=err) }
	};
}

#[cfg(test)]
mod test;
