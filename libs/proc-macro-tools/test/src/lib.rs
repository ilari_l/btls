use proc_macro::TokenStream;
use btls_aux_proc_macro_tools::RootElementList;
use btls_aux_proc_macro_tools::ElementStream;
use btls_aux_proc_macro_tools::Element;

fn print_interesting_punct(x: &str)
{
	match x {
		"&="|"|="|"^="|"+="|"-="|"*="|"/="|"%="|"<<="|">>="|"<="|">="|"!="|"=="|"..=" => (),
		"&"|"|"|"^"|"+"|"-"|"*"|"/"|"%"|"<<"|">>"|"<"|">"|"!"|".."|"&&"|"||" => (),
		"=>"|"::"|":"|";"|"="|","|"?"|"@" => (),
		err => println!("punct: '{err}'")
	}
}

fn walk(mut strm: ElementStream)
{
	loop {
		match strm.next() {
			Element::Brace(g) => walk(g.to_stream()),
			Element::Bracket(g) => walk(g.to_stream()),
			Element::Paren(g) => walk(g.to_stream()),
			Element::GroupNone(g) => walk(g.to_stream()),
			Element::Punct(p) => print_interesting_punct(p.get_punct()),
			Element::Ident(_) => (),
			Element::Lifetime(_) => (),
			Element::Literal(_) => (),
			Element::End(_) => break,
		}
	}
}

#[proc_macro]
pub fn test_punct(s: TokenStream) -> TokenStream
{
	let ast = RootElementList::new(s.clone());
	walk(ast.to_stream());
	s
}

#[proc_macro]
pub fn is_single(s: TokenStream) -> TokenStream
{
	let ast = RootElementList::new(s.clone());
	let mut ast = ast.to_stream();
	ast.next();
	assert!(!ast.more());
	TokenStream::new()
}
