#[derive(Clone,Debug)]
pub struct Span(());

impl Span
{
	pub(crate) fn dummy() -> Span { Span(()) }
	pub(crate) fn whole() -> Span { Span(()) }
	pub(crate) fn end() -> Span { Span(()) }
	pub(crate) fn union(&self, _: &Span) -> Span { Span(()) }
	pub(crate) fn group_close(_: &proc_macro::Group) -> Span { Span(()) }
	pub(crate) fn group(_: &proc_macro::Group) -> Span { Span(()) }
	pub(crate) fn ident(_: &proc_macro::Ident) -> Span { Span(()) }
	pub(crate) fn literal(_: &proc_macro::Literal) -> Span { Span(()) }
	pub(crate) fn punct(_: &proc_macro::Punct) -> Span { Span(()) }
}
