use super::ElementPunct;
use crate::span::Span;
use std::collections::BTreeSet;

fn assert_single_operator(op: &str)
{
	let mut p = ElementPunct::blank();
	let mut ns = Vec::new();
	for c in op.chars() {
		assert!(p.may_combine(c));
		p.push(c, Span::dummy(), &mut ns);
	}
	assert!(ns.len() == 1);
}

fn assert_run_on_operator(op1: &str, op2: &str)
{
	println!("{op1}{op2}");
	let mut p = ElementPunct::blank();
	let mut ns = Vec::new();
	for c in op1.chars() {
		assert!(p.may_combine(c));
		p.push(c, Span::dummy(), &mut ns);
	}
	let mut first = true;
	for c in op2.chars() {
		if first {
			assert!(!p.may_combine(c));
			p = ElementPunct::blank();
		} else {
			assert!(p.may_combine(c));
		}
		p.push(c, Span::dummy(), &mut ns);
		first = false;
	}
	assert!(ns.len() == 2);
}

#[test]
fn single_operators()
{
	static OPS: &'static [&'static str] = &[
		//What is this <- operator?
		"&=", "|=", "^=", "+=", "-=", "/=", "%=", "<<=", ">>=", "<<", ">>", "<=", "==", "!=", ">=", "=>",
		"..", "..=", "&&", "||", "::", "<-",
	];
	for &op in OPS.iter() { assert_single_operator(op); }
}

#[test]
fn one_char_operators()
{
	static OPS: &'static [&'static str] = &[
		"!", "%", "&", "*", "+", ",", "-", "/", ":", ";", "<", "=", ">", "?", "@", "^", "|",
	];
	for &op in OPS.iter() { assert_single_operator(op); }
}

#[test]
fn run_on_operators()
{
	static OPS: &'static [(&'static str, &'static str)] = &[
		//Binary-prefix, suffix-binary.
		//The exceptional cases are && (single operator), /* (comment start) and <- (???).
		("!=", "!"),("!=", "&"),("!=", "*"),("!=", "-"),("?","!=" ),
		("%=", "!"),("%=", "&"),("%=", "*"),("%=", "-"),("?","%=" ),
		("&=", "!"),("&=", "&"),("&=", "*"),("&=", "-"),("?","&=" ),
		("*=", "!"),("*=", "&"),("*=", "*"),("*=", "-"),("?","*=" ),
		("+=", "!"),("+=", "&"),("+=", "*"),("+=", "-"),("?","+=" ),
		("-=", "!"),("-=", "&"),("-=", "*"),("-=", "-"),("?","-=" ),
		("/=", "!"),("/=", "&"),("/=", "*"),("/=", "-"),("?","/=" ),
		("<=", "!"),("<=", "&"),("<=", "*"),("<=", "-"),("?","<=" ),
		("==", "!"),("==", "&"),("==", "*"),("==", "-"),("?","==" ),
		(">=", "!"),(">=", "&"),(">=", "*"),(">=", "-"),("?",">=" ),
		("^=", "!"),("^=", "&"),("^=", "*"),("^=", "-"),("?","^=" ),
		("|=", "!"),("|=", "&"),("|=", "*"),("|=", "-"),("?","|=" ),
		("<<=","!"),("<<=","&"),("<<=","*"),("<<=","-"),("?","<<="),
		(">>=","!"),(">>=","&"),(">>=","*"),(">>=","-"),("?",">>="),
		("%",  "!"),("%",  "&"),("%",  "*"),("%",  "-"),("?","%"  ),
		("&",  "!"),            ("&",  "*"),("&",  "-"),("?","&"  ),
		("*",  "!"),("*"  ,"&"),("*",  "*"),("*",  "-"),("?","*"  ),
		("+",  "!"),("+",  "&"),("+",  "*"),("+",  "-"),("?","+"  ),
		("-",  "!"),("-",  "&"),("-",  "*"),("-",  "-"),("?","-"  ),
		("/",  "!"),("/",  "&"),            ("/",  "-"),("?","/"  ),
		("<",  "!"),("<",  "&"),("<",  "*"),            ("?","<"  ),
		(">",  "!"),(">",  "&"),(">",  "*"),(">",  "-"),("?",">"  ),
		("^",  "!"),("^",  "&"),("^",  "*"),("^",  "-"),("?","^"  ),
		("|",  "!"),("|",  "&"),("|",  "*"),("|",  "-"),("?","|"  ),
		("<<", "!"),("<<", "&"),("<<", "*"),("<<", "-"),("?","<<" ),
		(">>", "!"),(">>", "&"),(">>", "*"),(">>", "-"),("?",">>" ),
		//Prefix-prefix, assign-prefix.
		//The exceptional cases are && (single operator).
		("!","!"),("!","&"),("!","*"),("!","-"),
		("&","!"),          ("&","*"),("&","-"),
		("*","!"),("*","&"),("*","*"),("*","-"),
		("-","!"),("-","&"),("-","*"),("-","-"),
		("=","!"),("=","&"),("=","*"),("=","-"),
		//Postfix-postfix, postfix-assign, postfix-separator.
		("?","?"),("?","="),("?",","),("?",";"),
		//Separator-separator,separator-prefix.
		//TODO: ;, ,; and ,, have not been observed yet.
		(";",";"),          (";","!"),(";","&"),(";","*"),(";","-"),
		                    (",","!"),(",","&"),(",","*"),(",","-"),
		//=>, .., ..= and prefixes.
		("=>", "!"),("=>", "&"),("=>", "*"),("=>", "-"),
		("..", "!"),("..", "&"),("..", "*"),("..", "-"),
		("..=","!"),("..=","&"),("..=","*"),("..=","-"),
		//Other.
		("::","<"),(">","::"),	//Turbofish.
		("<","::"),		//extern prelude path in generic.
		("@","&"),		//Can appear in patterns.
		("=",".."),		//Assign range.
		("..",";"),("..",","),	//RangeFrom/RangeFull.
	];
	let mut counter: BTreeSet<(&'static str, &'static str)> = BTreeSet::new();
	for &(op1, op2) in OPS.iter() { counter.insert((op1, op2)); }
	println!("{clen} run-on operators to consider", clen=counter.len());
	for &(op1, op2) in OPS.iter() { assert_run_on_operator(op1, op2); }
}

fn __fn1(_: i32, _: i32) {}
fn __fn2(_: std::ops::RangeFull, _: i32) {}
fn __fn3(_: i32, _: &i32) {}

fn __every_run_on() -> Result<(),()>
{
	let mut a: i32 = 0;
	let b: i32 = 1;
	let c = &b;
	let d: Result<i32,()> = Ok(2);
	let _ = a!=!b; let _ = &a!=&b; let _ = a!=*c; let _ = a!=-b; let _ = d?!=b;
	let _ = a==!b; let _ = &a==&b; let _ = a==*c; let _ = a==-b; let _ = d?==b;
	let _ = a<=!b; let _ = &a<=&b; let _ = a<=*c; let _ = a<=-b; let _ = d?<=b;
	let _ = a>=!b; let _ = &a>=&b; let _ = a>=*c; let _ = a>=-b; let _ = d?>=b;
	let _ = a<!b; let _ = &a<&b; let _ = a<*c;               let _ = d?<b;
	let _ = a>!b; let _ = &a>&b; let _ = a>*c; let _ = a>-b; let _ = d?>b;
	a%=!b; a%=&b; a%=*c; a%=-b; let e = Ok(&mut a); *e?%=b;
	a&=!b; a&=&b; a&=*c; a&=-b; let e = Ok(&mut a); *e?&=b;
	a*=!b; a*=&b; a*=*c; a*=-b; let e = Ok(&mut a); *e?*=b;
	a+=!b; a+=&b; a+=*c; a+=-b; let e = Ok(&mut a); *e?+=b;
	a-=!b; a-=&b; a-=*c; a-=-b; let e = Ok(&mut a); *e?-=b;
	a/=!b; a/=&b; a/=*c; a/=-b; let e = Ok(&mut a); *e?/=b;
	a|=!b; a|=&b; a|=*c; a|=-b; let e = Ok(&mut a); *e?|=b;
	a^=!b; a^=&b; a^=*c; a^=-b; let e = Ok(&mut a); *e?^=b;
	a<<=!b; a<<=&b; a<<=*c; a<<=-b; let e = Ok(&mut a); *e?<<=b;
	a>>=!b; a>>=&b; a>>=*c; a>>=-b; let e = Ok(&mut a); *e?>>=b;
	let _ = a%!b; let _ = &a%&b; let _ = a%*c; let _ = a%-b; let _ = d?%b;
	let _ = a&!b;                let _ = a&*c; let _ = a&-b; let _ = d?&b;
	let _ = a*!b; let _ = &a*&b; let _ = a**c; let _ = a*-b; let _ = d?*b;
	let _ = a+!b; let _ = &a+&b; let _ = a+*c; let _ = a+-b; let _ = d?+b;
	let _ = a-!b; let _ = &a-&b; let _ = a-*c; let _ = a--b; let _ = d?-b;
	let _ = a/!b; let _ = &a/&b;               let _ = a/-b; let _ = d?/b;
	let _ = a^!b; let _ = &a^&b; let _ = a^*c; let _ = a^-b; let _ = d?^b;
	let _ = a|!b; let _ = &a|&b; let _ = a|*c; let _ = a|-b; let _ = d?|b;
	let _ = a<<!b; let _ = &a<<&b; let _ = a<<*c; let _ = a<<-b; let _ = d?<<b;
	let _ = a>>!b; let _ = &a>>&b; let _ = a>>*c; let _ = a>>-b; let _ = d?>>b;
	let _ = !!a; let _ = !&a; let _ = !*c; let _ = !-a;
	let _ = &!a; let _ = &*c; let _ = &-a;
	let _ = -!a; let _ = -&a; let _ = -*c; let _ = --a;
	let _ =!a; let _ =&a; let _ =*c; let _ =-a;
	let _ = *&a; let _ = ** & &a; let _ = *&a;
	let _ = Ok(Ok(a))??;
	let e = Ok(&mut a); *e?=b;
	__fn1(Ok(a)?,b);
	let _ = Ok(a)?;
	let _ = ..!a; let _ = ..&a; let _ = ..*c; let _ = ..-a; let _ = ..;
	let _ = ..=!a; let _ = ..=&a; let _ = ..=*c; let _ = ..=-a; let _ =..;
	let _ = match a { x =>!x };
	let _ = match a { x =>&x };
	let _ = match c { x =>*x };
	let _ = match a { x =>-x };
	let _ = match c { x@&_ => x };
	let _ = ..;
	__fn2(..,a);
	let _ = std::cell::RefCell::<::std::ops::RangeFull>::new(..);
	let e = &mut a ;*e=b;
	__fn1(42,*c); __fn3(42,&a); __fn1(42,!a); __fn1(42,-a);
	Ok(())
/*
		//TODO: ;, ,; and ,, have not been observed yet.
*/
}

#[allow(unused_must_use)]
fn __every_run_on_dirty1()
{
	let a = 42i32;
	let _ = a;-a;!a;&a;
}

#[allow(redundant_semicolons)]
fn __every_run_on_dirty2()
{
	let _ = 0i32;;
}
