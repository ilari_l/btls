//!Tools for parsing and serializing code in procedural macros.
extern crate proc_macro;
use crate::span::Span;
use proc_macro::Delimiter as DE;
use proc_macro::Group as GRP;
use proc_macro::Ident as ID;
use proc_macro::Literal as LI;
use proc_macro::Punct as PU;
use proc_macro::Spacing as SP;
use proc_macro::Span as HY;
use proc_macro::TokenTree as TT;
use proc_macro::TokenStream as TS;
use std::cell::RefCell;
use std::fmt::Debug;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::iter::once;
use std::mem::replace;
use std::ops::Deref;
use std::rc::Rc;
use std::str::FromStr;


#[path="dummy-span.rs"]
mod span;

type TreeT = Rc<RefCell<Vec<(usize, Element)>>>;


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Traits
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///Trait: Something with sequence number.
pub trait HasSequenceNumber
{
	fn sequence_number(&self) -> usize;
}

///Trait for elements that can be used with the `output!()` macro.
pub trait TranslateElement
{
	///Push the token trees into vector of `OwnedOutput`.
	fn epush(&self, to: &mut Vec<OwnedOutput>);
}

trait MaybeFallible: Sized
{
	fn to_residual(self) -> Option<Self>;
	fn succesful() -> Self;
}

///Trait for types that can be used with the [`OutputSuffixed`](struct.OutputSuffixed.html) structure.
///
///This is implemented for the numeric types.
pub trait NumericType: Copy
{
	///Parse the number from string.
	fn parse(x: &str) -> Option<Self>;
	///Translate the element into `proc_macro::Literal`. 
	fn translate(self) -> LI;
}

macro_rules! def_te_ref
{
	($base:ident) => {
		impl<'a> TranslateElement for &'a $base
		{
			fn epush(&self, to: &mut Vec<OwnedOutput>) { (*self).epush(to) }
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Trait definitions on builtin types
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
impl MaybeFallible for ()
{
	fn to_residual(self) -> Option<Self> { None }	//Never fails.
	fn succesful() -> Self { () }
}

impl<E> MaybeFallible for Result<(), E>
{
	fn to_residual(self) -> Option<Self>
	{
		match self { x@Err(_) => Some(x), Ok(_) => None }
	}
	fn succesful() -> Self { Ok(()) }
}

macro_rules! def_te_num
{
	($x:ident $y:ident $z:ident) => {
		impl TranslateElement for $x
		{
			fn epush(&self, to: &mut Vec<OwnedOutput>)
			{
				to.push(OwnedOutput::from_li(LI::$y(*self)))
			}
		}
		impl NumericType for $x
		{
			fn parse(x: &str) -> Option<Self> { Self::from_str(x).ok() }
			fn translate(self) -> LI { LI::$z(self) }
		}
	}
}

def_te_num!(i8 i8_unsuffixed i8_suffixed);
def_te_num!(u8 u8_unsuffixed u8_suffixed);
def_te_num!(i16 i16_unsuffixed i16_suffixed);
def_te_num!(u16 u16_unsuffixed u16_suffixed);
def_te_num!(i32 i32_unsuffixed i32_suffixed);
def_te_num!(u32 u32_unsuffixed u32_suffixed);
def_te_num!(i64 i64_unsuffixed i64_suffixed);
def_te_num!(u64 u64_unsuffixed u64_suffixed);
def_te_num!(i128 i128_unsuffixed i128_suffixed);
def_te_num!(u128 u128_unsuffixed u128_suffixed);
def_te_num!(isize isize_unsuffixed isize_suffixed);
def_te_num!(usize usize_unsuffixed usize_suffixed);

impl TranslateElement for bool
{
	fn epush(&self, to: &mut Vec<OwnedOutput>)
	{
		let k = if *self { "true" } else { "false" };
		to.push(OwnedOutput::from_id(ID::new(k, HY::call_site())));
	}
}

impl<'a> TranslateElement for &'a str
{
	fn epush(&self, to: &mut Vec<OwnedOutput>)
	{
		let code = TS::from_str(self).unwrap_or_else(|err|{
			panic!("Failed to lex '{self}': {err}")
		});
		for i in code { to.push(tt_to_owned_output(i)); }
		//to.push(OwnedOutput::Ident(ID::new_raw(self, HY::call_site())))
	}
}

impl TranslateElement for String
{
	fn epush(&self, to: &mut Vec<OwnedOutput>) { self.deref().epush(to) }
}

impl TranslateElement for char
{
	fn epush(&self, to: &mut Vec<OwnedOutput>)
	{
		to.push(OwnedOutput::from_li(LI::character(*self)));
	}
}

impl TranslateElement for PU
{
	fn epush(&self, to: &mut Vec<OwnedOutput>) { to.push(OwnedOutput::from_pu(self.clone())) }
}

impl TranslateElement for LI
{
	fn epush(&self, to: &mut Vec<OwnedOutput>) { to.push(OwnedOutput::from_li(self.clone())) }
}

impl TranslateElement for ID
{
	fn epush(&self, to: &mut Vec<OwnedOutput>) { to.push(OwnedOutput::from_id(self.clone())) }
}


def_te_ref!(String);
def_te_ref!(PU);
def_te_ref!(LI);
def_te_ref!(ID);


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Misc type definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#[derive(Copy,Clone,PartialEq,Eq,Debug)]
enum TokenClass
{
	Group,
	Punct,
	Word,
}

type TokenVec = Vec<(TokenClass,String)>;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Main element types
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///Element: Ident.
///
///Idents include identifiers, keywords and primitive values `true` and `false` (which are actually keywords).
///
///This implements the [`TranslateElement`](trait.TranslateElement.html) trait, so this can be directly used as an
///argument to the [`output!`](macro.output.html) macro. However, there is no way to construct keywords. For
///outputting keywords, use [`OutputKeyword`](enum.OutputKeyword.html).
#[derive(Clone)]
pub struct ElementIdent
{
	master: ID,
	identifier: Option<String>,
	span: Span,
	seqno: usize,
}

impl Debug for ElementIdent
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "{master}", master=self.master.to_string())
	}
}

impl ElementIdent
{
	fn from_id(master: ID, nseqno: &mut TokenVec) -> ElementIdent
	{
		let identifier = id_to_identifier(&master);
		let span = Span::ident(&master);
		let seqno = assign_seqno(nseqno, TokenClass::Word, master.to_string());
		ElementIdent { master, identifier, span, seqno }
	}
	///Get string representation of the identifier.
	///
	///Note that identifiers that are not keywords may or may not have the `r#` prefix. However, identifiers
	///that are keywords always have `r#` prefix.
	pub fn to_string(&self) -> String { self.master.to_string() }
	///If element is an identifier, return it, otherwise return `None'.
	///
	///The identifier never has the `r#` prefix. Note this may return identifiers that are keywords, e.g.,
	///`class`.
	pub fn as_identifier(&self) -> Option<&str> { self.identifier.as_deref() }
	///Is this ident specified keyword?
	///
	///The parameter `x` is the keyword to match against.
	///
	///Note that this will not match identifiers, even if name matches. So `class` matches keyword `class`, but
	///not identifier `class`.
	pub fn is_keyword(&self, x: &str) -> bool
	{
		let y = self.to_string();
		//raw identifiers do not match.
		if y.starts_with("r#") { return false; }
		//Keywords do match.
		if let Some(y) = y.strip_prefix("k#") { return y == x; }
		//Other kinds of special idents do not match.
		if y.contains("#") { return false; }
		//Otherwise compare the raw form.
		&y[..] == x
	}
	///Is this ident specified identifier?
	///
	///The parameter `x` is the identifier to match against.
	///
	///Note that this will not match keywords, even if name matches. So `true` matches identifier `true`, but
	///not keyword `true`.
	pub fn is_identifier(&self, x: &str) -> bool
	{
		let y = self.to_string();
		//raw identifier.
		if let Some(y) = y.strip_prefix("r#") { return y == x; }
		//Other kinds of special idents do not match.
		if y.contains("#") { return false; }
		//Otherwise compare the raw form.
		&y[..] == x
	}
	///Get span.
	pub fn span(&self) -> Span { self.span.clone() }
	///Get sequence number.
	///
	///See [`Element::seqno()`](enum.Element.html#method.seqno) for more information about sequence numbers.
	pub fn seqno(&self) -> usize { self.seqno }
}

impl HasSequenceNumber for ElementIdent
{
	fn sequence_number(&self) -> usize { self.seqno }
}

impl TranslateElement for ElementIdent
{
	fn epush(&self, to: &mut Vec<OwnedOutput>) { to.push(OwnedOutput::from_id(self.master.clone())) }
}

def_te_ref!(ElementIdent);

///Element: Lifetime
///
///These elements are lifetimes.
///
///This implements the [`TranslateElement`](trait.TranslateElement.html) trait, so this can be directly used as an
///argument to the [`output!`](macro.output.html) macro.
#[derive(Clone)]
pub struct ElementLifetime
{
	master: ID,
	span: Span,
	seqno: usize,
}

impl Debug for ElementLifetime
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "'{master}", master=self.master.to_string())
	}
}

impl ElementLifetime
{
	fn from_id(master: ID, pspan: Span, nseqno: &mut TokenVec) -> ElementLifetime
	{
		let t = master.to_string();
		let span =  pspan.union(&Span::ident(&master));
		let seqno = assign_seqno(nseqno, TokenClass::Word, format!("'{t}"));
		if t == "static" && t != "k#static" && id_to_identifier(&master).is_none() {
			panic!("Lifetime argument {master:?} is not an identifier!")
		}
		ElementLifetime { master, span, seqno }
	}
	///Get string representation of the lifetime symbol.
	pub fn to_string(&self) -> String { self.master.to_string() }
	///Get span.
	///
	///The span covers both the initial single quote and the lifetime identifer.
	pub fn span(&self) -> Span { self.span.clone() }
	///Get sequence number.
	///
	///See [`Element::seqno()`](enum.Element.html#method.seqno) for more information about sequence numbers.
	pub fn seqno(&self) -> usize { self.seqno }
}

impl HasSequenceNumber for ElementLifetime
{
	fn sequence_number(&self) -> usize { self.seqno }
}

impl TranslateElement for ElementLifetime
{
	fn epush(&self, to: &mut Vec<OwnedOutput>)
	{
		to.push(OwnedOutput::from_pu(PU::new('\'', SP::Joint)));
		to.push(OwnedOutput::from_id(self.master.clone()));
	}
}

def_te_ref!(ElementLifetime);


///Element: Literal
///
///These elements are the primitive values, except for unit value `()` (which is a group) and boolean values `true`
///and `false` (which are keywords and thus idents).
///
///This implements the [`TranslateElement`](trait.TranslateElement.html) trait, so this can be directly used as an
///argument to the [`output!`](macro.output.html) macro. Note that there is no way to construct integer literal.
///For outputting those, either directly use integers, or use the [`OutputSuffixed`](struct.OutputSuffixed.html)
///type.
#[derive(Clone)]
pub struct ElementLiteral
{
	master: LI,
	span: Span,
	seqno: usize
}

impl Debug for ElementLiteral
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		f.write_str(&self.master.to_string())
	}
}

impl ElementLiteral
{
	fn from_li(master: LI, nseqno: &mut TokenVec) -> ElementLiteral
	{
		let span = Span::literal(&master);
		let seqno = assign_seqno(nseqno, TokenClass::Word, master.to_string());
		ElementLiteral { master, span, seqno }
	}
	///Get string representation of the literal.
	pub fn to_string(&self) -> String { self.master.to_string() }
	///Get span.
	pub fn span(&self) -> Span { self.span.clone() }
	///Get sequence number.
	///
	///See [`Element::seqno`](enum.Element.html#method.seqno) for more information about sequence numbers.
	pub fn seqno(&self) -> usize { self.seqno }
}

impl HasSequenceNumber for ElementLiteral
{
	fn sequence_number(&self) -> usize { self.seqno }
}

impl TranslateElement for ElementLiteral
{
	fn epush(&self, to: &mut Vec<OwnedOutput>) { to.push(OwnedOutput::from_li(self.master.clone())) }
}

def_te_ref!(ElementLiteral);

///Element: Punct.
///
///These are various sequences of one or more special characters.
///
///The characters are mostly combined according to the `Joint/Alone` properties from the Rust compiler. However,
///there are exceptions: `>,` and `?,` never combine.
///
///This type does not implement `TranslateElement`. To output operators, instead use the
///[`OutputPunct`](enum.OutputPunct.html) type.
#[derive(Clone)]
pub struct ElementPunct
{
	punct: String,
	span: Span,
	seqno: usize
}

impl Debug for ElementPunct
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		f.write_str(&self.punct)
	}
}

impl ElementPunct
{
	fn blank() -> ElementPunct
	{
		ElementPunct {
			punct: String::new(),
			span: Span::dummy(),
			seqno: !0,
		}
	}
	fn nontrivial(&self) -> bool { self.punct.len() > 0 }
	fn may_combine(&self, c: char) -> bool
	{
		//Special case: '-' may be combined to '<', as that forms '<-', which is not a run-on. However,
		//'-' can not be combined to '<<' to form '<<-', as that is run-on.
		if self.punct == "<" && c == '-' { return true; }
		!refuse_to_combine(string_last(&self.punct), c)
	}
	fn push(&mut self, pu_ch: char, pu_span: Span, nseqno: &mut TokenVec)
	{
		//If there is no assigned sequence number, assign it.
		if self.seqno == !0 {
			self.seqno = assign_seqno(nseqno, TokenClass::Punct, String::new());
		}
		//If adding subsequent symbol, combine spans. If adding the first symbol, override the span,
		//as call_site() is used as a dummy span.
		if self.punct.len() > 0 {
			self.span = self.span.union(&pu_span);
		} else {
			self.span = pu_span;
		}
		self.punct.push(pu_ch);
		//Update the string assigned to sequence number, as the punctuation changed.
		nseqno.get_mut(self.seqno).map(|t|t.1=self.punct.clone());
	}
	///Is this ident specified punctuation?
	///
	///The parameter `x` is the punctuation to match against.
	pub fn is_punct(&self, x: &str) -> bool
	{
		x == &self.punct
	}
	//Does this ident start with specified character?
	///
	///The parameter `c` gives the character to match against.
	pub fn starts_with(&self, c: char) -> bool { self.punct.starts_with(c) }
	///Maintain angle depth.
	///
	///The parameter `depth` gives reference to the depth.
	///
	///This counts the number of `<` and `>` characters in the punct. For each, `<`, the depth is incremented by
	///1, and for each `>`, the depth is decremented by 1.
	pub fn angle_depth(&self, depth: &mut usize)
	{
		for c in self.punct.chars() { match c {
			'<' => *depth += 1,
			'>' => *depth -= 1,
			_ => ()
		}}
	}
	///Get the punctuation.
	pub fn get_punct(&self) -> &str { &self.punct }
	///Get span.
	///
	///Note that the span covers all the characters in the punctuation.
	pub fn span(&self) -> Span { self.span.clone() }
	///Get sequence number.
	///
	///See [`Element::seqno`](enum.Element.html#method.seqno) for more information about sequence numbers.
	pub fn seqno(&self) -> usize { self.seqno }
}

impl HasSequenceNumber for ElementPunct
{
	fn sequence_number(&self) -> usize { self.seqno }
}

impl TranslateElement for ElementPunct
{
	fn epush(&self, to: &mut Vec<OwnedOutput>) { handle_punct(&self.punct, to) }
}

def_te_ref!(ElementPunct);

///Element: End.
///
///This is the end-of-stream element. It is there in order to assign it a span (which is the closing brace) and a
///sequence number.
#[derive(Clone)]
pub struct ElementEnd
{
	span: Span,
	seqno: usize,
}

impl Debug for ElementEnd
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		f.write_str("(end)")
	}
}

impl ElementEnd
{
	///Get span.
	///
	///This span points to the end-of-group brace/bracket/parenthesis.
	pub fn span(&self) -> Span { self.span.clone() }
	///Get sequence number.
	///
	///End-of-stream elements have their own sequence number, which resolves to the end-of-group
	///brace/bracket/parenthesis. In case of top-level, the sequence number resolves to a special end-of-macro
	///element.
	///
	///End-of-stream sequence numbers are fused: Trying to advance the iterator more will still return the same
	///sequence number.
	///
	///See [`Element::seqno()`](enum.Element.html#method.seqno) for more information about sequence numbers.
	pub fn seqno(&self) -> usize { self.seqno }
}

impl HasSequenceNumber for ElementEnd
{
	fn sequence_number(&self) -> usize { self.seqno }
}

///Code element.
///
///This roughly corresponds to the `TokenTree` type from `proc_macro`, except punctuation is combined.
///
///As special case, the code refuses to combine `>` and `,`, even if compier says they are combined.
///
///This implements the [`TranslateElement`](trait.TranslateElement.html) trait, so this can be directly used as an
///argument to the [`output!`](macro.output.html) macro.
#[derive(Clone)]
pub enum Element
{
	///Code block enclosed in `{...}`.
	Brace(ElementList),
	///Code block enclosed in `[...]`.
	Bracket(ElementList),
	///Code block enclosed in `(...)`.
	Paren(ElementList),
	///Code block enclosed in `Ø...Ø` (the implicit delimiters).
	GroupNone(ElementList),
	///Identifier (keyword or variable).
	Ident(ElementIdent),
	///Lifetime name.
	Lifetime(ElementLifetime),
	///Literal (primitive value constant).
	Literal(ElementLiteral),
	///One or more punctuation characters.
	Punct(ElementPunct),
	///End of input.
	End(ElementEnd),
}

impl Element
{
	///See [`ElementIdent::as_identifier()`](struct.ElementIdent.html#method.as_identifier).
	pub fn as_identifier(&self) -> Option<&str> 
	{
		if let &Element::Ident(ref y) = self { y.as_identifier() } else { None }
	}
	///If element is an ident, return it, otherwise return `None`.
	pub fn as_raw_ident(&self) -> Option<ElementIdent>
	{
		if let &Element::Ident(ref y) = self { Some(y.clone()) } else { None }
	}
	///See [`ElementPunct::is_punct()`](struct.ElementPunct.html#method.is_punct).
	pub fn is_punct(&self, x: &str) -> bool
	{
		if let &Element::Punct(ref y) = self { y.is_punct(x) } else { false }
	}
	///See [`ElementIdent::is_keyword()`](struct.ElementIdent.html#method.is_keyword).
	pub fn is_keyword(&self, x: &str) -> bool
	{
		if let &Element::Ident(ref y) = self { y.is_keyword(x) } else { false }
	}
	///See [`ElementIdent::is_identifier()`](struct.ElementIdent.html#method.is_identifier).
	pub fn is_identifier(&self, x: &str) -> bool
	{
		if let &Element::Ident(ref y) = self { y.is_identifier(x) } else { false }
	}
	///Is the element end of stream element?
	pub fn is_end(&self) -> bool
	{
		matches!(self, &Element::End(_))
	}
	///Get span.
	///
	///Note that if the element is brace/bracket/parenthesis, the result is not the same as calling span on the
	///inner element: The returned span includes the brace/bracket/parenthesis, while the inner element span does
	///not.
	pub fn span(&self) -> Span
	{
		match self {
			&Element::Brace(ref x) => x.span_outer(),
			&Element::Bracket(ref x) => x.span_outer(),
			&Element::Paren(ref x) => x.span_outer(),
			&Element::GroupNone(ref x) => x.span_outer(),
			&Element::Ident(ref x) => x.span(),
			&Element::Lifetime(ref x) => x.span(),
			&Element::Literal(ref x) => x.span(),
			&Element::Punct(ref x) => x.span(),
			&Element::End(ref x) => x.span(),
		}
	}
	///Get sequence number.
	///
	///Each token of the input will get a consequtive sequence number, starting from 0. This includes the
	///opening and closing characters of groups. Then there is special sequence number for end of the entiere
	///macro input. However, the beginning of the input has no sequence number: Sequence number 0 is the first
	///token.
	///
	///Synthethized tokens will not have valid sequence numbers. The returned sequence number in those cases is
	///always `!0`.
	///
	///The sequence numbers can be used to dump few last tokens using the
	///[`RootElementList::get_last_symbols()`](struct.RootElementList.html#method.get_last_symbols) method.
	///This is useful for showing macro parse errors.
	pub fn seqno(&self) -> usize
	{
		match self {
			&Element::Brace(ref x) => x.seqno(),
			&Element::Bracket(ref x) => x.seqno(),
			&Element::Paren(ref x) => x.seqno(),
			&Element::GroupNone(ref x) => x.seqno(),
			&Element::Ident(ref x) => x.seqno(),
			&Element::Lifetime(ref x) => x.seqno(),
			&Element::Literal(ref x) => x.seqno(),
			&Element::Punct(ref x) => x.seqno(),
			&Element::End(ref x) => x.seqno(),
		}
	}
}

impl HasSequenceNumber for Element
{
	fn sequence_number(&self) -> usize { self.seqno() }
}

impl<'a> TranslateElement for Element
{
	fn epush(&self, to: &mut Vec<OwnedOutput>) { push_element_to_output(self, to); }
}

def_te_ref!(Element);

//Be careful when printing this, as it can give loads of garbage.
impl Debug for Element
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::Element::*;
		match self {
			&Brace(_) => f.write_str("{...}"),
			&Bracket(_) => f.write_str("[...]"),
			&Paren(_) => f.write_str("(...)"),
			&GroupNone(_) => f.write_str("\u{2205}...\u{2205}"),
			&Ident(ref t) => write!(f, "ident:{t}", t=t.to_string()),
			&Lifetime(ref t) => write!(f, "'{t}", t=t.to_string()),
			&Literal(ref t) => write!(f, "literal:{t}", t=t.to_string()),
			&Punct(ref t) => write!(f, "punct:{t}", t=&t.punct),
			&End(_) => f.write_str("<END>")
		}
	}
}

///A list of elements.
///
///This is similar to the `TokenStream` type from the `proc_macro` crate.
///
///This implements the [`TranslateElement`](trait.TranslateElement.html) trait, so this can be directly used as an
///argument to the [`output!`](macro.output.html) macro.
#[derive(Clone,Debug)]
pub struct ElementList
{
	//The usize is index of next sibling.
	tree: TreeT,
	//First index in the list.
	first: usize,
	span: Span,
	whole_span: Span,
	start_seqno: usize,
}

impl ElementList
{
	fn new_inner(from: TS, tree: TreeT, espan: Span, bspan: Span, nseqno: &mut TokenVec, d: Option<DE>) ->
		ElementList
	{
		//let mut list = Vec::new();
		let mut first = !0;
		let mut prev = !0;
		let mut punct = ElementPunct::blank();
		let mut lt_pspan: Option<Span> = None;
		let mut span: Option<Span> = None;
		//The initial element list has no starting sequence number.
		use TokenClass as TC;
		let start_seqno = match d {
			//d=None is only possible for the root. The sequence number does not matter as the seqno
			//of root can never be accessed.
			None => 0,
			Some(DE::Brace) => assign_seqno(nseqno, TC::Group, '{'.to_string()),
			Some(DE::Bracket) => assign_seqno(nseqno, TC::Group, '['.to_string()),
			Some(DE::Parenthesis) => assign_seqno(nseqno, TC::Group, '('.to_string()),
			Some(DE::None) => assign_seqno(nseqno, TC::Group, '«'.to_string()),
		};
		for elem in from { match elem {
			TT::Group(grp) => {
				join_to_span(&mut span, Span::group(&grp));
				let espan = Span::group_close(&grp);
				let bspan = Span::group(&grp);
				if punct.nontrivial() {
					panic!("Lexing error: Unexpected group after punct ({punct:?})");
				}
				let d = grp.delimiter();
				let ctor = match d {
					DE::Brace => Element::Brace,
					DE::Bracket => Element::Bracket,
					DE::Parenthesis => Element::Paren,
					DE::None => Element::GroupNone,
				};
				//The init flag in recursive calls is always false.
				let t = ElementList::new_inner(grp.stream(), tree.clone(), espan, bspan, nseqno,
					Some(d));
				let t = ctor(t);
				add_to_list(&tree, &mut prev, &mut first, t);
			},
			TT::Ident(id) => {
				join_to_span(&mut span, Span::ident(&id));
				if let Some(pspan) = lt_pspan.take() {
					//This is special: Lifetime. Unassign sequence number, so the lifetime gets
					//the sequence number of the punct that starts it. This way there is no
					//gaps in the sequence.
					nseqno.pop();
					punct = ElementPunct::blank();
					let t = Element::Lifetime(ElementLifetime::from_id(id, pspan, nseqno));
					add_to_list(&tree, &mut prev, &mut first, t);
					continue;
				}
				if punct.nontrivial() {
					panic!("Lexing error: Unexpected ident after punct ({punct:?})");
				}
				let t = Element::Ident(ElementIdent::from_id(id, nseqno));
				add_to_list(&tree, &mut prev, &mut first, t);
			},
			TT::Literal(li) => {
				join_to_span(&mut span, Span::literal(&li));
				if punct.nontrivial() {
					panic!("Lexing error: Unexpected literal after punct ({punct:?})");
				}
				let t = Element::Literal(ElementLiteral::from_li(li, nseqno));
				add_to_list(&tree, &mut prev, &mut first, t);
			},
			TT::Punct(pu) => {
				join_to_span(&mut span, Span::punct(&pu));
				//Set span for lifetime ' prefix. This is needed so that span can be saved.
				if pu.as_char() == '\'' && pu.spacing() == SP::Joint {
					lt_pspan = Some(Span::punct(&pu));
				} else {
					lt_pspan = None;
				}
				//Some special combinations that refuse to combine.
				if !punct.may_combine(pu.as_char()) {
					let t = Element::Punct(replace(&mut punct, ElementPunct::blank()));
					add_to_list(&tree, &mut prev, &mut first, t);
				}
				punct.push(pu.as_char(), Span::punct(&pu), nseqno);
				if pu.spacing() == SP::Alone {
					//Punct sequence is terminated by Alone.
					let t = Element::Punct(replace(&mut punct, ElementPunct::blank()));
					add_to_list(&tree, &mut prev, &mut first, t);
				}
			},
		}}
		//Use call_site() as dummy span.
		let span = span.unwrap_or(bspan.clone());
		let end_seqno = match d {
			None => assign_seqno(nseqno, TC::Group, '␄'.to_string()),
			Some(DE::Brace) => assign_seqno(nseqno, TC::Group, '}'.to_string()),
			Some(DE::Bracket) => assign_seqno(nseqno, TC::Group, ']'.to_string()),
			Some(DE::Parenthesis) => assign_seqno(nseqno, TC::Group, ')'.to_string()),
			Some(DE::None) => assign_seqno(nseqno, TC::Group, '»'.to_string()),
		};
		//Add end-of-list element. This has special looping index. The addition of this entry also
		//guarantees first is valid index.
		let t = Element::End(ElementEnd {
			seqno: end_seqno,
			span: espan.clone(),
		});
		add_to_list(&tree, &mut prev, &mut first, t);
		tree.borrow_mut()[prev].0 = prev;

		ElementList {
			tree: tree,
			first: first,
			span: span,
			whole_span: bspan,
			start_seqno: start_seqno,
		}
	}
	///Turn the list of elements into stream of elements.
	pub fn to_stream(&self) -> ElementStream
	{
		ElementStream {
			tree: self.tree.clone(),
			index: self.first
		}
	}
	///Get span.
	///
	///Note that the span does not include the braces/brackets/parenthesis around the group.
	pub fn span(&self) -> Span { self.span.clone() }
	fn span_outer(&self) -> Span { self.whole_span.clone() }
	///Get sequence number.
	///
	///The sequence number will refer to the opening brace/bracket/parenthesis.
	///
	///See [`Element::seqno`](enum.Element.html#method.seqno) for more information about sequence numbers.
	pub fn seqno(&self) -> usize { self.start_seqno }
}

impl HasSequenceNumber for ElementList
{
	fn sequence_number(&self) -> usize { self.start_seqno }
}

impl TranslateElement for ElementList
{
	fn epush(&self, to: &mut Vec<OwnedOutput>)
	{
		//Only borrow the tree for read, because there will may be recursive borrows.
		for_each_node_in_chain(&self.tree.borrow(), self.first, false, |elem|{
			push_element_to_output(elem, to);
		})
	}
}

def_te_ref!(ElementList);

impl IntoIterator for ElementList
{
	type Item = Element;
	type IntoIter = ElementListIterator;
	fn into_iter(self) -> ElementListIterator { ElementListIterator(self.to_stream()) }
}

///A root list of elements.
///
///This is similar to [`ElementList`](struct.ElementList.html), but is used for the root element list, which is
///special in some ways. For instance, this list has sequence number only for ending element, not for beginning
///element.
#[derive(Clone,Debug)]
pub struct RootElementList
{
	//The usize is index of next sibling.
	tree: TreeT,
	//It is not guaranteed that the first index in the root list is zero!
	first: usize,
	symbol_sequence: TokenVec, 
}

impl RootElementList
{
	///Create a new element list by parsing `proc_macro::TokenStream`.
	///
	///The parameter `from` gives the token stream to parse.
	pub fn new(from: TS) -> RootElementList
	{
		let mut symseq: TokenVec = Vec::new();
		let tree: TreeT = Rc::new(RefCell::new(Vec::new()));
		let espan = Span::end();
		let bspan = Span::whole();
		let t = ElementList::new_inner(from, tree, espan, bspan, &mut symseq, None);
		verify_tree(t.tree.borrow().deref(), t.first);

		RootElementList {
			tree: t.tree,
			first: t.first,
			symbol_sequence: symseq,
		}
	}
	///Turn the list of elements into stream of elements.
	pub fn to_stream(&self) -> ElementStream
	{
		ElementStream {
			tree: self.tree.clone(),
			index: self.first,
		}
	}
	///Get last symbols up to given sequence number.
	///
	///The parameter `seqno` gives the sequence number of the last element to include in the output.
	///
	///This is mainly useful for displaying error messages when something goes wrong with parsing the macro,
	///as there is no way to give errors associated with spans (the methods are unstable).
	pub fn get_last_symbols(&self, seqno: usize) -> String
	{
		const LOOKBACK: usize = 10;
		let mut t = String::new();
		let mut i = seqno.saturating_sub(LOOKBACK);
		let mut prev: Option<&(TokenClass, String)> = None;
		while i <= seqno && i < self.symbol_sequence.len() {
			let this = &self.symbol_sequence[i];
			if let Some(prev) = prev {
				if !tokens_may_join(prev, this) { t.push(' '); }
			}
			t.push_str(&this.1);
			i += 1;
			prev = Some(this);
		}
		t
	}
	pub fn panic(&self, seqno: &impl HasSequenceNumber, msg: impl Display) -> !
	{
		let seqno = seqno.sequence_number();
		panic!("{strm} --> {msg}", strm=self.get_last_symbols(seqno));
	}
}

impl HasSequenceNumber for RootElementList
{
	//This is degenerate: The first sequence number in root list is always 0.
	fn sequence_number(&self) -> usize { 0 }
}

impl IntoIterator for RootElementList
{
	type Item = Element;
	type IntoIter = ElementListIterator;
	fn into_iter(self) -> ElementListIterator { ElementListIterator(self.to_stream()) }
}

///Iterator over elements in element list.
///
///Note that this does not yield the end-of-stream elements: The iterator ends after the last real element.
#[derive(Clone,Debug)]
pub struct ElementListIterator(ElementStream);

impl Iterator for ElementListIterator
{
	type Item = Element;
	fn next(&mut self) -> Option<Element> { self.0.next_iterate() }
}

///Stream of elements.
///
///This is not quite an iterator: Instead of returning `None` on end-of-stream, it returns special end-of-stream
///element.
#[derive(Clone)]
pub struct ElementStream
{
	//The usize is index of next sibling.
	tree: TreeT,
	//Next index in the list.
	index: usize,
}

impl ElementStream
{
	///Expect next element to be paren-expr, and extract it.
	pub fn expect_paren_expr(&mut self, root: &RootElementList) -> ElementStream
	{
		match self.next() {
			Element::Paren(x) => x.to_stream(),
			ref y => root.panic(y, "Expected '('")
		}
	}
	///Expect next element to be brace-expr, and extract it.
	pub fn expect_brace_expr(&mut self, root: &RootElementList) -> ElementStream
	{
		match self.next() {
			Element::Brace(x) => x.to_stream(),
			ref y => root.panic(y, "Expected '{'")
		}
	}
	///Expect next element to be bracket-expr, and extract it.
	pub fn expect_bracket_expr(&mut self, root: &RootElementList) -> ElementStream
	{
		match self.next() {
			Element::Bracket(x) => x.to_stream(),
			ref y => root.panic(y, "Expected '['")
		}
	}
	///Expect next element to be ident, and extract it.
	pub fn expect_ident(&mut self, root: &RootElementList) -> ElementIdent
	{
		match self.next() {
			Element::Ident(x) => x,
			ref y => root.panic(y, "Expected ident")
		}
	}
	///Expect next element to be literal, and extract it.
	pub fn expect_literal(&mut self, root: &RootElementList) -> ElementLiteral
	{
		match self.next() {
			Element::Literal(x) => x,
			ref y => root.panic(y, "Expected literal")
		}
	}
	///Expect next element to be integer, and extract it.
	pub fn expect_integer<Num:NumericType>(&mut self, root: &RootElementList) -> Num
	{
		let t = self.expect_literal(root);
		match Num::parse(&t.to_string()) {
			Some(x) => x,
			None => root.panic(&t, "Expected integer")
		}
	}
	///Expect next element to be lifetime, and extract it.
	pub fn expect_lifetime(&mut self, root: &RootElementList) -> ElementLifetime
	{
		match self.next() {
			Element::Lifetime(x) => x,
			ref y => root.panic(y, "Expected lifetime")
		}
	}
	///Expect next element to be identifier, and extract it.
	pub fn expect_identifier(&mut self, root: &RootElementList) -> String
	{
		match self.next() {
			Element::Ident(ref x) => match x.as_identifier() {
				Some(y) => y.to_string(),
				None => root.panic(x, "Expected identifier")
			},
			ref y => root.panic(y, "Expected identifier")
		}
	}
	///Expect next element to be given punct.
	pub fn expect_punct(&mut self, root: &RootElementList, punct: &str)
	{
		match self.next() {
			Element::Punct(x) if x.is_punct(punct) => (),
			ref y => root.panic(y, format_args!("Expected '{punct}'"))
		}
	}
	///Expect list comma.
	pub fn expect_list_comma(&mut self, root: &RootElementList)
	{
		match self.next() {
			Element::End(_) => (),
			Element::Punct(x) if x.is_punct(",") => (),
			ref y => root.panic(y, format_args!("Expected ',' or end"))
		}
	}
	///Expect next element to be end of stream.
	pub fn expect_end(mut self, root: &RootElementList)
	{
		match self.next() {
			Element::End(_) => (),
			ref y => root.panic(y, "Expected end")
		}
	}
	///Is there more?
	pub fn more(&self) -> bool
	{
		//All execpt end nodes point forwards in chain.
		self.tree.borrow()[self.index].0 > self.index
	}
	///Peek the next element, without removing it.
	///
	///The end of stream is fused: This function will return `Element::End` forever.
	pub fn peek(&self) -> Element
	{
		let tree = self.tree.borrow();
		tree[self.index].1.clone()
	}
	///Discard an element, without returning it.
	pub fn discard(&mut self)
	{
		let tree = self.tree.borrow();
		//End nodes have looping index.
		let nindex = tree[self.index].0;
		self.index = nindex;
	}
	///Return the next element and discard it from stream.
	///
	///The end of stream is fused: This function will return `Element::End` forever.
	pub fn next(&mut self) -> Element
	{
		let t = self.peek();
		self.discard();
		t
	}
	///Like `next()`, but instead returns `None` of `Element::End`.
	///
	///This is useful with `while let Some(x) = list { ... }` loops.
	pub fn next_iterate(&mut self) -> Option<Element>
	{
		let t = self.next();
		if t.is_end() { None } else { Some(t) }
	}
	///If (inner) attribute is next in the stream, read it.
	///
	///If there is no attribute at this position, returns `None`. Otherwise returns the attribute.
	pub fn next_attribute(&mut self, root: &RootElementList) -> Option<ElementAttribute>
	{
		let tmp = self.peek();
		let seqno = tmp.seqno();
		if tmp.is_punct("#") {
			self.discard();
			match self.next() {
				Element::Bracket(x) => Some(ElementAttribute(seqno, false, x)),
				ref t => root.panic(t, "Expected '['")
			}
		} else if tmp.is_punct("#!") {
			self.discard();
			match self.next() {
				Element::Bracket(x) => Some(ElementAttribute(seqno, true, x)),
				ref t => root.panic(t, "Expected '['")
			}
		} else {
			None
		}
	}
	///If item visibility is next in the stream, read it.
	///
	///If there is no item visibility at this point, returns `None`. If there is global public visibility
	///(`pub`), returns `Some(None)`. If there is restricted visibility, returns `Some(Some(vis))`, where
	///`vis` is the contents of parenthesis expression in visibility.
	pub fn next_visibility(&mut self) -> Option<Option<ElementList>>
	{
		let tmp = self.peek();
		if tmp.is_keyword("pub") {
			self.discard();
			Some(match self.peek() {
				Element::Paren(x) => {
					self.discard();
					Some(x)
				},
				_ => None
			})
		} else {
			 None
		}
	}
	///Read next elements in stream as path. Note that a singular identifier is a special case of path.
	///
	/// # panics
	///
	///This function panics if what comes next in stream is not a path.
	pub fn next_path(&mut self) -> ElementList
	{
		let tree: TreeT = Rc::new(RefCell::new(Vec::new()));
		let mut first = !0;
		let mut prev = !0;
		let mut end_seqno = 0;
		let mut last_dc = false;
		let espan;
		let mut safety_countdown = self.tree.borrow().len();
		loop {
			match self.peek() {
				Element::Punct(ref x) if x.is_punct("::") => {
					last_dc = true;
					end_seqno =  x.seqno() + 1;
					let t = Element::Punct(x.clone());
					add_to_list(&tree, &mut prev, &mut first, t);
				},
				Element::Ident(x) => {
					last_dc = false;
					end_seqno =  x.seqno() + 1;
					let t = Element::Ident(x);
					add_to_list(&tree, &mut prev, &mut first, t);
				}
				t => {
					espan = t.span();
					break;
				}
			}
			self.discard();
			safety_countdown = safety_countdown.checked_sub(1).
				expect("Lockup at ElementStream::next_path()!!!");
		}
		if first == !0 { panic!("Empty path not allowed"); }
		if last_dc { panic!("Path can not end with :"); }
		//Add end-of-path node, in similar manner as in ElementList::new_inner().
		let t = Element::End(ElementEnd {
			seqno: end_seqno,
			span: espan.clone(),
		});
		add_to_list(&tree, &mut prev, &mut first, t);
		tree.borrow_mut()[prev].0 = prev;
		verify_tree(tree.borrow().deref(), first);
		//Go through entries to compute the span.
		let (mut span, start_seqno, mut index) = {
			let tree_ent = &tree.borrow()[first];
			(tree_ent.1.span(), tree_ent.1.seqno(), tree_ent.0)
		};
		let mut safety_countdown = tree.borrow().len();
		loop {
			//Do not go to end-of-list node.
			if tree.borrow()[index].0 == index { break; }
			let tree_ent = &tree.borrow()[index];
			span = span.union(&tree_ent.1.span());
			index = tree_ent.0;
			safety_countdown = safety_countdown.checked_sub(1).
				expect("Lockup at ElementStream::next_path()!!!");
		}

		ElementList {
			tree: tree,
			first: first,
			span: span,
			whole_span: Span::dummy(),	//Can not be queried.
			start_seqno: start_seqno,
		}
	}
}

impl HasSequenceNumber for ElementStream
{
	//Give next sequence number.
	fn sequence_number(&self) -> usize { self.peek().seqno() }
}

impl Debug for ElementStream
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		f.write_str("ElementStream(")?;
		let mut first = true;
		for_each_node_in_chain(&self.tree.borrow(), self.index, true, |entry|{
			if !replace(&mut first, false) { f.write_str(", ")?; }
			write!(f, "{entry:?}")
		})?;
		f.write_str(")")
	}
}

///Attribute.
#[derive(Clone,Debug)]
pub struct ElementAttribute(usize, bool, ElementList);

impl ElementAttribute
{
	///Is inner attribute?
	pub fn is_inner(&self) -> bool { self.1 }
	///Get element stream.
	pub fn to_stream(&self) -> ElementStream { self.2.to_stream() }
	///Expect outer attribute.
	pub fn expect_outer(&self, root: &RootElementList)
	{
		if self.1 { root.panic(self, "Inner attributes not allowed here"); }
	}
}

impl HasSequenceNumber for ElementAttribute
{
	fn sequence_number(&self) -> usize { self.0 }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
fn classify_punct(c: char) -> u8
{
	match c {
		'!' => 0,
		'#' => 18,
		//'$' =>
		'%' => 1,
		'&' => 2,
		//'\'' =>
		'*' => 3,
		'+' => 4,
		',' => 5,
		'-' => 6,
		'.' => 17,
		'/' => 7,
		':' => 8,
		';' => 9,
		'<' => 10,
		'=' => 11,
		'>' => 12,
		'?' => 13,
		'@' => 16,
		//'\\' =>
		'^' => 14,
		//'`' =>
		'|' => 15,
		//'~' =>
		_ => 31
	}
}

fn refuse_to_combine(x: char, y: char) -> bool
{
	//80 pairs so far.
	static MAGIC: [u32;32] = [
		0x2FEFF,		// !:  . |^?>=<; /-,+*&%!
		0x02000,		// %:      ?
		0x3FEFB,		// &:  .@|^?>=<; /-,+* %!
		0x2FF7F,		// *:  . |^?>=<;: -,+*&%!		/* is comment start.
		0x02000,		// +:      ?
		0x23008,		// ,:  .   ?>        *
		0x2FEFF,		// -:  . |^?>=<; /-,+*&%!		What is <- ???
		0x02000,		// /:      ?
		0x01400,		// ::       > <
		0x22200,		// ;:  .   ?   ;
		0x02100,		// <:      ?    :
		0x02000,		// =:      ?
		0x02000,		// >:      ?
		0x02000,		// ?:      ?
		0x02000,		// ^:      ?
		0x02000,		// |:      ?
		0x00000,		// @:
		0x00800,		// .:        =
		0x20F25,		// #:  .     =<;:  ,  & !         
		0,0,0,0,0,0,0,0,0,0,0,0,0,		//Not in use.
	];
	let x = classify_punct(x);
	let y = classify_punct(y);
	MAGIC[y as usize & 31] >> (x&31) & 1 != 0
}

fn string_first(x: &str) -> char
{
	//Common-case optimization.
	match x.as_bytes().first() {
		Some(&x) if x < 128 => return x as char,
		Some(_) => (),
		None => return '\u{fffd}'
	}
	x.chars().next().unwrap_or('\u{fffd}')
}

fn string_last(x: &str) -> char
{
	//Common-case optimization.
	match x.as_bytes().last() {
		Some(&x) if x < 128 => return x as char,
		Some(_) => (),
		None => return '\u{fffd}'
	}
	let xl = x.len();
	for i in 1..5 {
		if xl >= i && x.is_char_boundary(xl-i) {
			return x[xl-i..].chars().next().unwrap_or('\u{fffd}')
		}
	}
	'\u{fffd}'
}


fn assign_seqno(nseqno: &mut TokenVec, tclass: TokenClass, val: String) -> usize
{
	let t = nseqno.len();
	nseqno.push((tclass, val));
	t
}

fn id_to_identifier(y: &ID) -> Option<String>
{
	let y = y.to_string();
	//Raw identifier.
	if let Some(y) = y.strip_prefix("r#") { return Some(y.to_string()); }
	//Other kinds of special idents do not match.
	if y.contains("#") { return None; }
	//Otherwise return the raw form.
	Some(y)
}

fn join_to_span(span: &mut Option<Span>, new: Span)
{
	if let Some(span) = span.as_mut() {
		*span = span.union(&new);
	} else {
		*span = Some(new);
	}
}

fn tokens_may_join(prev: &(TokenClass, String), this: &(TokenClass, String)) -> bool
{
	//Things of different class can always join.
	if prev.0 != this.0 { return true; }
	//Groups can join with anything.
	if prev.0 == TokenClass::Group { return true; }
	if this.0 == TokenClass::Group { return true; }
	//Puncts join under some conditions.
	if prev.0 == TokenClass::Punct {
		let prev = &prev.1;
		let this = &this.1;
		//Special edge case: "<" and "-" can not join, as that would form "<-", which is apparently not
		//a run-on operator. However, "<<" and "-" may join, as that is a run-on operator. There is
		//currently no known operator that starts with - that might be candidate for run-on with <. E.g.,
		//-> or -=.
		if prev == "<" && this == "-" { return true; }
		if refuse_to_combine(string_last(prev), string_first(this)) { return true; }
	}
	false
}

fn add_to_list(tree: &TreeT, prev: &mut usize, first: &mut usize, elem: Element)
{
	let assigned = tree.borrow().len();
	tree.borrow_mut().push((!0, elem));
	// !0 is invalid index used for first added element.
	if *prev != !0 {
		tree.borrow_mut()[*prev].0 = assigned;
	} else {
		*first = assigned;
	}
	*prev = assigned;
}

fn __mark_reachable(reachable: &mut [bool], index: usize)
{
	if reachable[index] { panic!("Element {index} in tree reached twice!"); }
	reachable[index] = true;
}

fn __verify_tree(tree: &[(usize,Element)], first: usize, reachable: &mut [bool])
{
	let mut index = first;
	loop {
		__mark_reachable(reachable, index);
		let nindex = tree[index].0;
		//Walk the subtree.
		match &tree[index].1 {
			&Element::Brace(ref sub) => __verify_tree(tree, sub.first, reachable),
			&Element::Bracket(ref sub) => __verify_tree(tree, sub.first, reachable),
			&Element::Paren(ref sub) => __verify_tree(tree, sub.first, reachable),
			&Element::GroupNone(ref sub) => __verify_tree(tree, sub.first, reachable),
			//The remaining do not have sub-elements.
			_ => ()
		}
		if index == nindex { break; }	//Reached end of list.
		if nindex < index { panic!("Invalid tree sibling link {index} -> {nindex}"); }
		index = nindex;
	}
}

fn verify_tree(tree: &[(usize,Element)], first: usize)
{
	//println!("Completed parse, {tlen} elements, starting from {first}", tlen=tree.len());
	let mut reachable = vec![false;tree.len()];
	__verify_tree(tree, first, &mut reachable);
	for i in 0..reachable.len() {
		if !reachable[i] { panic!("Element {i} in tree is unreachable!"); }
	}
	//println!("Tree verify complete");
}

macro_rules! forward_maybe_fallible
{
	($val:expr) => {
		if let Some(e) = MaybeFallible::to_residual($val) { return e; }
	}
}


fn for_each_node_in_chain<R:MaybeFallible>(tree: &[(usize,Element)], first: usize, include_end: bool,
	mut f: impl FnMut(&Element) -> R) -> R
{
	let mut index = first;
	let mut safety = tree.len();
	loop {
		let nindex = tree[index].0;
		if nindex == index {
			//If end node was asked, include it too.
			if include_end { forward_maybe_fallible!(f(&tree[index].1)); }
			break;
		}
		forward_maybe_fallible!(f(&tree[index].1));
		index = nindex;
		//Just in case.
		safety = safety.checked_sub(1).expect("Looping node chain");
	}
	R::succesful()
}



///Raw identifier.
pub struct OutputIdentifier<'a>(pub &'a str);

impl<'a> TranslateElement for OutputIdentifier<'a>
{
	fn epush(&self, to: &mut Vec<OwnedOutput>)
	{
		to.push(OwnedOutput::from_id(ID::new_raw(self.0, HY::call_site())))
	}
}

///Output numeric type with type suffix. 
///
///This implements the [`TranslateElement`](trait.TranslateElement.html) trait, so this can be directly used as an
///argument to the [`output!`](macro.output.html) macro. 
pub struct OutputSuffixed<T:NumericType>(pub T);

impl<T:NumericType> TranslateElement for OutputSuffixed<T>
{
	fn epush(&self, to: &mut Vec<OwnedOutput>)
	{
		to.push(OwnedOutput::from_li(NumericType::translate(self.0)))
	}
}

impl<'a> TranslateElement for &'a [OwnedOutput]
{
	fn epush(&self, to: &mut Vec<OwnedOutput>)
	{
		for elem in self.iter() { elem.epush(to); }
	}
}

impl TranslateElement for Vec<OwnedOutput>
{
	fn epush(&self, to: &mut Vec<OwnedOutput>) { self.deref().epush(to); }
}

impl TranslateElement for OwnedOutput
{
	fn epush(&self, to: &mut Vec<OwnedOutput>)
	{
		match &self.0 {
			&_OO::Recursive(ref t) => for elem in t.iter() { elem.epush(to); },
			other => to.push(OwnedOutput(other.clone()))
		}
	}
}

impl<'a> TranslateElement for &'a OwnedOutput
{
	fn epush(&self, to: &mut Vec<OwnedOutput>) { (*self).epush(to) }
}

fn handle_element_group(delimiter: DE, elems: &ElementList) -> OwnedOutput
{
	let mut out: Vec<OwnedOutput> = Vec::new();
	elems.epush(&mut out);
	OwnedOutput::from_grp(delimiter, out)
}

fn handle_punct(kw: &str, output: &mut Vec<OwnedOutput>)
{
	for (i,c) in kw.char_indices() {
		let is_last = i + c.len_utf8() >= kw.len();
		let sp = if is_last { SP::Alone } else { SP::Joint };
		output.push(OwnedOutput::from_pu(PU::new(c, sp)));
	}
}

fn push_element_to_output(elem: &Element, output: &mut Vec<OwnedOutput>)
{
	match elem {
		&Element::Brace(ref list) => output.push(handle_element_group(DE::Brace, list)),
		&Element::Bracket(ref list) => output.push(handle_element_group(DE::Bracket, list)),
		&Element::Paren(ref list) => output.push(handle_element_group(DE::Parenthesis, list)),
		&Element::GroupNone(ref list) => output.push(handle_element_group(DE::None, list)),
		&Element::Ident(ref t) => t.epush(output),
		&Element::Lifetime(ref t) => t.epush(output),
		&Element::Literal(ref t) => t.epush(output),
		&Element::Punct(ref p) => handle_punct(&p.punct, output),
		&Element::End(_) => (),
	}
}

#[doc(hidden)]
pub fn __push_element(x: &impl TranslateElement, to: &mut Vec<OwnedOutput>)
{
	x.epush(to);
}

#[doc(hidden)]
pub fn __to_group(out: OwnedOutput, delim: DE) -> OwnedOutput
{
	let t = match out.0 {
		_OO::Recursive(t) => _OO::Group(delim, t),
		t => _OO::Group(delim, vec![OwnedOutput(t)]),
	};
	OwnedOutput(t)
}

///Construct output stream (`OwnedOutput`) from list of elements.
///
///The elements can be of any type implementing the `TranslateElement` trait.
///
/// * If the first token is `()`, the output stream is enclosed in `(...)`, and returned as `OwnedOutput`.
/// * If the first token is `[]`, the output stream is enclosed in `[...]`, and returned as `OwnedOutput`.
/// * If the first token is `{}`, the output stream is enclosed in `{...}`, and returned as `OwnedOutput`.
/// * If the first token is `TO[foo]`, the output stream is appended to vector `foo`. There is no return value.
/// * Otherwise a fresh 'OwnedOutput` is returend with the elements in it.
///
///Note that all the elements are automatically borrowed, and no ownership is taken.
#[macro_export]
macro_rules! output
{
	([] $($elems:expr),*) => { $crate::__to_group(output!($($elems),*), proc_macro::Delimiter::Bracket) };
	(() $($elems:expr),*) => { $crate::__to_group(output!($($elems),*), proc_macro::Delimiter::Parenthesis) };
	({} $($elems:expr),*) => { $crate::__to_group(output!($($elems),*), proc_macro::Delimiter::Brace) };
	(TO[$to:ident]) => { () };
	(TO[$to:ident] $elem:expr) => {
		$crate::__push_element(&$elem, &mut $to)
	};
	(TO[$to:ident] $head:expr, $($tail:expr),*) => {{
		$crate::__push_element(&$head, &mut $to);
		output!(TO[$to] $($tail),*)
	}};
	($($elems:expr),*) => {{
		let mut out = Vec::new();
		output!(TO[out] $($elems),*);
		OwnedOutput::from_rec(out)
	}};
}

include!(concat!(env!("OUT_DIR"), "/autogenerated-keyword.rs"));


impl<'a> TranslateElement for OutputKeyword<'a>
{
	fn epush(&self, to: &mut Vec<OwnedOutput>)
	{
		let kw = self.as_str();
		//Special snowflake: 'static.
		if let Some(kw) = kw.strip_prefix("'") {
			to.push(OwnedOutput(_OO::Punct(PU::new('\'', SP::Joint))));
			to.push(OwnedOutput(_OO::Ident(ID::new(kw, HY::call_site()))));
			return;
		}
		to.push(OwnedOutput(_OO::Ident(ID::new(kw, HY::call_site()))))
	}
}

include!(concat!(env!("OUT_DIR"), "/autogenerated-punct.rs"));

impl<'a> TranslateElement for OutputPunct<'a>
{
	fn epush(&self, to: &mut Vec<OwnedOutput>)
	{
		let kw = self.as_str();
		for (i,c) in kw.char_indices() {
			let is_last = i + c.len_utf8() >= kw.len();
			let sp = if is_last { SP::Alone } else { SP::Joint };
			to.push(OwnedOutput(_OO::Punct(PU::new(c, sp))));
		}
	}
}

include!(concat!(env!("OUT_DIR"), "/autogenerated-pseudoword.rs"));

impl TranslateElement for OutputPseudoword
{
	fn epush(&self, to: &mut Vec<OwnedOutput>)
	{
		to.push(self.as_token());
	}
}

fn deraw(x: String) -> String
{
	if let Some(x) = x.strip_prefix("r#") {
		if OutputKeyword::from_str(x).is_none() { return x.to_string(); }
	}
	x
}

fn tt_to_owned_output(code: TT) -> OwnedOutput
{
	let t = match code {
		TT::Group(g) => {
			let mut out: Vec<OwnedOutput> = Vec::new();
			for i in g.stream() { out.push(tt_to_owned_output(i)); }
			_OO::Group(g.delimiter(), out)
		}
		TT::Ident(i) => _OO::Ident(i),
		TT::Literal(l) => _OO::Literal(l),
		TT::Punct(p) => _OO::Punct(p),
	};
	OwnedOutput(t)
}

///Parse a block of rust code as `OwnedOutput`.
pub fn rust(code: &str) -> OwnedOutput
{
	let code = TS::from_str(code).unwrap_or_else(|err|{
		panic!("Failed to lex '{code}': {err}");
	});
	let mut out: Vec<OwnedOutput> = Vec::new();
	for i in code { out.push(tt_to_owned_output(i)); }
	OwnedOutput(_OO::Recursive(out))
}

///Proc-macro output.
///
///This is constructed by the [`output!`](macro.output.html) macro. It is intended to construct the output of the
///procedural macro.
#[derive(Clone)]
pub struct OwnedOutput(_OO);
#[derive(Clone)]
enum _OO
{
	Punct(PU),
	Literal(LI),
	Ident(ID),
	Group(DE, Vec<OwnedOutput>),
	Recursive(Vec<OwnedOutput>),
}

impl Debug for OwnedOutput
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		let mut v = Vec::new();
		let mut fl = false;
		self.to_stringvec(&mut v, &mut fl);
		let mut prev: Option<&(TokenClass, String)> = None;
		for this in v.iter() {
			if let Some(prev) = prev {
				if !tokens_may_join(prev, this) {
					f.write_str(" ")?;
				}
			}
			f.write_str(&this.1)?;
			prev = Some(this);
		}
		Ok(())
	}
}

impl OwnedOutput
{
	fn from_pu(x: PU) -> OwnedOutput { OwnedOutput(_OO::Punct(x)) }
	fn from_li(x: LI) -> OwnedOutput { OwnedOutput(_OO::Literal(x)) }
	fn from_id(x: ID) -> OwnedOutput { OwnedOutput(_OO::Ident(x)) }
	fn from_grp(x: DE, y: Vec<OwnedOutput>) -> OwnedOutput { OwnedOutput(_OO::Group(x, y)) }
	#[doc(hidden)]	//This is used by a macro.
	pub fn from_rec(x: Vec<OwnedOutput>) -> OwnedOutput { OwnedOutput(_OO::Recursive(x)) }
	fn to_stringvec(&self, out: &mut TokenVec, last_joint: &mut bool)
	{
		match &self.0 {
			&_OO::Literal(ref x) => out.push((TokenClass::Word, x.to_string())),
			&_OO::Ident(ref x) => out.push((TokenClass::Word, deraw(x.to_string()))),
			&_OO::Recursive(ref x) => Self::to_stringvec_many(x, out, last_joint),
			&_OO::Group(g, ref x) => {
				let (go, gc) = match g {
					DE::Brace => ('{','}'),
					DE::Bracket => ('[',']'),
					DE::Parenthesis => ('(',')'),
					DE::None => ('«','»'),
				};
				out.push((TokenClass::Group, go.to_string()));
				Self::to_stringvec_many(x, out, last_joint);
				out.push((TokenClass::Group, gc.to_string()));
			},
			&_OO::Punct(ref x) => {
				//Joining has multiple requirements:
				//1) The topmost entry must be punct.
				//2) last_joint is set.
				//3) Not trying to join ?, nor >,
				if let Some((TokenClass::Punct, ref mut text)) = out.last_mut() {
					let mut disallow_join = false;
					disallow_join |= refuse_to_combine(string_last(text), x.as_char());
					if *last_joint && !disallow_join {
						*last_joint = x.spacing() == SP::Joint;
						text.push(x.as_char());
						return;
					}
				}
				//If join is not possible, start a new punct. Note that the previous punct that
				//could not be joined to is left on the list.
				*last_joint = x.spacing() == SP::Joint;
				out.push((TokenClass::Punct, x.as_char().to_string()));
			},
		}
	}
	fn to_stringvec_many(this: &[OwnedOutput], out: &mut TokenVec, last_joint: &mut bool)
	{
		for elem in this.iter() { elem.to_stringvec(out, last_joint); }
	}
	fn __extend_ts(&self, out: &mut TS)
	{
		match &self.0 {
			&_OO::Punct(ref t) => out.extend(once(TT::Punct(t.clone()))),
			&_OO::Literal(ref t) => out.extend(once(TT::Literal(t.clone()))),
			&_OO::Ident(ref t) => out.extend(once(TT::Ident(t.clone()))),
			&_OO::Group(d, ref list) => {
				let mut out2 = TS::new();
				for elem in list.iter() { elem.__extend_ts(&mut out2); }
				out.extend(once(TT::Group(GRP::new(d, out2))));
			},
			&_OO::Recursive(ref list) => {
				for elem in list.iter() { elem.__extend_ts(out); }
			},
		}
	}
	///Turn the output into `proc_macro::TokenStream`.
	pub fn to_token_stream(&self) -> TS
	{
		let mut out = TS::new();
		self.__extend_ts(&mut out);
		out
	}
}

#[cfg(test)]
mod test;
