use proc_macro_show_punct::is_single;
use proc_macro_show_punct::test_punct;

fn foobar<T>() -> T { panic!("foo") }
fn foobar1(_: i32, _:i32) {}
fn foobar2(_: i32, _:&i32) {}

#[allow(unused_must_use)]	//Cut down on crap needed.
#[allow(redundant_semicolons)]
fn xmain() -> Result<(), ()>
{
	let mut foo = 5;
	let bar = 6;
	let qux = Ok(7);
	let quux = Ok(Ok(8));
	let _xref = 9;
	let xref = &_xref;
	let mut _yref = 9;

	//Binary-prefix
	test_punct!(foo&=!bar;);  test_punct!(foo&=-bar;);  test_punct!(foo&=*xref;);
	test_punct!(foo|=!bar;);  test_punct!(foo|=-bar;);  test_punct!(foo|=*xref;);
	test_punct!(foo^=!bar;);  test_punct!(foo^=-bar;);  test_punct!(foo^=*xref;);
	test_punct!(foo+=!bar;);  test_punct!(foo+=-bar;);  test_punct!(foo+=*xref;);
	test_punct!(foo-=!bar;);  test_punct!(foo-=-bar;);  test_punct!(foo-=*xref;);
	test_punct!(foo*=!bar;);  test_punct!(foo*=-bar;);  test_punct!(foo*=*xref;);
	test_punct!(foo/=!bar;);  test_punct!(foo/=-bar;);  test_punct!(foo/=*xref;);
	test_punct!(foo%=!bar;);  test_punct!(foo%=-bar;);  test_punct!(foo%=*xref;);
	test_punct!(foo<<=!bar;); test_punct!(foo<<=-bar;); test_punct!(foo<<=*xref;);
	test_punct!(foo>>=!bar;); test_punct!(foo>>=-bar;); test_punct!(foo>>=*xref;);
	test_punct!(foo&!bar;);   test_punct!(foo&-bar;);   test_punct!(foo&*xref;);
	test_punct!(foo|!bar;);   test_punct!(foo|-bar;);   test_punct!(foo|*xref;);   test_punct!(&foo|&bar);
	test_punct!(foo^!bar;);   test_punct!(foo^-bar;);   test_punct!(foo^*xref;);   test_punct!(&foo^&bar);
	test_punct!(foo+!bar;);   test_punct!(foo+-bar;);   test_punct!(foo+*xref;);   test_punct!(&foo+&bar);
	test_punct!(foo-!bar;);   test_punct!(foo--bar;);   test_punct!(foo-*xref;);   test_punct!(&foo-&bar);
	test_punct!(foo*!bar;);   test_punct!(foo*-bar;);   test_punct!(foo**xref;);   test_punct!(&foo*&bar);
	test_punct!(foo/!bar;);   test_punct!(foo/-bar;);                              test_punct!(&foo/&bar);
	test_punct!(foo%!bar;);   test_punct!(foo%-bar;);   test_punct!(foo%*xref;);   test_punct!(&foo%&bar);
	test_punct!(foo<<!bar;);  test_punct!(foo<<-bar;);  test_punct!(foo<<*xref;);  test_punct!(&foo<<&bar);
	test_punct!(foo>>!bar;);  test_punct!(foo>>-bar;);  test_punct!(foo>>*xref;);  test_punct!(&foo>>&bar);
	test_punct!(foo<!bar;);                             test_punct!(foo<*xref;);   test_punct!(&foo<&bar);
	test_punct!(foo<=!bar;);  test_punct!(foo<=-bar;);  test_punct!(foo<=*xref;);  test_punct!(&foo<=&bar);
	test_punct!(foo==!bar;);  test_punct!(foo==-bar;);  test_punct!(foo==*xref;);  test_punct!(&foo==&bar);
	test_punct!(foo!=!bar;);  test_punct!(foo!=-bar;);  test_punct!(foo!=*xref;);  test_punct!(&foo!=&bar);
	test_punct!(foo>=!bar;);  test_punct!(foo>=-bar;);  test_punct!(foo>=*xref;);  test_punct!(&foo>=&bar);
	test_punct!(foo>!bar;);   test_punct!(foo>-bar;);   test_punct!(foo>*xref;);   test_punct!(&foo>&bar);
	//Postfix-Binary.
	let yref = Ok(&mut _yref); test_punct!(*yref?&=bar;);
	let yref = Ok(&mut _yref); test_punct!(*yref?|=bar;);
	let yref = Ok(&mut _yref); test_punct!(*yref?^=bar;);
	let yref = Ok(&mut _yref); test_punct!(*yref?+=bar;);
	let yref = Ok(&mut _yref); test_punct!(*yref?-=bar;);
	let yref = Ok(&mut _yref); test_punct!(*yref?*=bar;);
	let yref = Ok(&mut _yref); test_punct!(*yref?/=bar;);
	let yref = Ok(&mut _yref); test_punct!(*yref?%=bar;);
	let yref = Ok(&mut _yref); test_punct!(*yref?<<=bar;);
	let yref = Ok(&mut _yref); test_punct!(*yref?>>=bar;);
	test_punct!(qux?&bar;);
	test_punct!(qux?|bar;);
	test_punct!(qux?^bar;);
	test_punct!(qux?+bar;);
	test_punct!(qux?-bar;);
	test_punct!(qux?*bar;);
	test_punct!(qux?/bar;);
	test_punct!(qux?%bar;);
	test_punct!(qux?<<bar;);
	test_punct!(qux?>>bar;);
	test_punct!(qux?<bar;);
	test_punct!(qux?<=bar;);
	test_punct!(qux?==bar;);
	test_punct!(qux?!=bar;);
	test_punct!(qux?>=bar;);
	test_punct!(qux?>bar;);
	//Prefix-prefix
	test_punct!(--bar);
	test_punct!(-!bar);
	test_punct!(!-bar);
	test_punct!(!!bar);
	test_punct!(-*xref);
	test_punct!(!*xref);
	test_punct!(&*xref);
	test_punct!(*&foo);
	test_punct!(!&foo);
	test_punct!(foobar2(0,&!foo));
	test_punct!(foobar2(0,&-foo));
	//Assign-prefix.
	test_punct!(let _ =-bar;);
	test_punct!(let _ =!bar;);
	test_punct!(let _ =*xref;);
	test_punct!(let _ =&bar;);
	//Assign-range.
	test_punct!(let _ =.. bar;);
	//Range-prefix.
	test_punct!(let _ = ..-bar;);
	test_punct!(let _ = ..!bar;);
	test_punct!(let _ = ..*xref;);
	test_punct!(let _ = ..&bar;);
	test_punct!(let _ = ..=-bar;);
	test_punct!(let _ = ..=!bar;);
	test_punct!(let _ = ..=*xref;);
	test_punct!(let _ = ..=&bar;);
	//postfix-Assign
	let yref = Ok(&mut _yref); test_punct!(*yref?=42;);
	//Postfix-postfix.
	test_punct!(quux??;);
	//Postfix-separator.
	test_punct!(qux?;foo+1);
	test_punct!(foobar1(qux?,foo));
	//Separator-separator.
	test_punct!(bar+1;;foo+1);
	//Separator-prefix.
	test_punct!(bar+1;-bar);
	test_punct!(bar+1;!bar);
	test_punct!(bar+1;*xref);
	test_punct!(bar+1;&foo);
	test_punct!(foobar1(foo,-foo));
	test_punct!(foobar1(foo,!foo));
	test_punct!(foobar1(foo,*xref));
	test_punct!(foobar2(foo,&foo));
	//Other.
	test_punct!(foobar::<i32>(););
	test_punct!(foobar::<::core::ascii::EscapeDefault>(););
	test_punct!(let _ = std::cell::Cell::<i32>::new;);
	
	//What is this <- operator?
	

	test_punct!(let _ = foo..=bar;);
	test_punct!(let _ = foo..bar;);
	test_punct!(let _ = true||false;);
	test_punct!(let _ = true&&false;);
	test_punct!(let _ = true&&!false;);

	test_punct!(match foo {
		1 =>!bar,
		2 =>-bar,
		3 =>*xref,
		_ => 0
	};);
	test_punct!(match xref {
		_y@&_ => 42
	};);
	test_punct!(let _ = foobar::<i32>(););
	test_punct!(let _ = foobar::<::core::ascii::EscapeDefault>(););
	test_punct!(let _ = std::cell::Cell::<i32>::new;);

	test_punct!(let _ = {
		struct _Foo<'a,'b>(&'a &'b [u8]);
		5
	};);

	is_single!(&=);
	is_single!(|=);
	is_single!(^=);
	is_single!(+=);
	is_single!(-=);
	is_single!(*=);
	is_single!(/=);
	is_single!(%=);
	is_single!(<<=);
	is_single!(>>=);
	is_single!(<<);
	is_single!(>>);
	is_single!(<=);
	is_single!(==);
	is_single!(!=);
	is_single!(>=);
	is_single!(=>);
	is_single!(..);
	is_single!(..=);
	is_single!(&&);
	is_single!(||);
	is_single!(::);
	//is_single!(<-);			//What is this <- operator?
	Ok(())
}

fn main()
{
	xmain().ok();
}
