use core::mem::transmute_copy;


///Source type for raw pointer transmute.
///
///The type SHALL be layout-compatible with a raw sized pointer.
pub unsafe trait RawPointerFrom: Sized
{
	///Cast to another raw pointer.
	///
	///Note that this function is safe: It always returns a valid pointer. However, using the pointer may not
	///be sound.
	fn cast2<To:RawPointerTo>(self) -> To { transmute_ptr(self) }
}

///Target type for raw pointer transmute.
///
///The type SHALL be layout-compatible with a raw sized pointer and any initialized value SHALL be valid.
///
///Note that the latter requirement prohibits `NonNull<T>` from implementing this: `Null` is not a valid value for
///`NonNull<T>`. The same requirement also prohibits references from implementing this type.
pub unsafe trait RawPointerTo: Sized
{
}

fn transmute_ptr<From:RawPointerFrom,To:RawPointerTo>(ptr: From) -> To
{
	//Just transmute_copy the thing. Plaint transmute can not handle generics.
	unsafe{transmute_copy::<From,To>(&ptr)}
}

unsafe impl<T> RawPointerFrom for *const T
{
}

unsafe impl<T> RawPointerFrom for *mut T
{
}

unsafe impl<T> RawPointerTo for *const T
{
}

unsafe impl<T> RawPointerTo for *mut T
{
}
