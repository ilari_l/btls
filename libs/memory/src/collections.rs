use crate::__do_binary_escape;
use crate::__do_string_escape;
use crate::ByteEscape;
use crate::Escape;
use crate::EscapeByteString;
use crate::EscapeString;
use crate::SafeShow;
use crate::SafeShowByteString;
use crate::SafeShowString;
use crate::TrustedInvariantLength;
use btls_aux_collections::Cow;
use btls_aux_collections::ToString;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::ops::Deref;
use core::str::from_utf8;


unsafe impl<T:TrustedInvariantLength> TrustedInvariantLength for btls_aux_collections::Arc<T>
{
	fn as_byte_slice(&self) -> &[u8] { self.deref().as_byte_slice() }
}

unsafe impl<T:TrustedInvariantLength> TrustedInvariantLength for btls_aux_collections::Rc<T>
{
	fn as_byte_slice(&self) -> &[u8] { self.deref().as_byte_slice() }
}

unsafe impl TrustedInvariantLength for btls_aux_collections::Vec<u8>
{
	fn as_byte_slice(&self) -> &[u8] { self }
}

unsafe impl<'a> TrustedInvariantLength for &'a btls_aux_collections::Vec<u8>
{
	fn as_byte_slice(&self) -> &[u8] { self }
}

unsafe impl TrustedInvariantLength for btls_aux_collections::String
{
	fn as_byte_slice(&self) -> &[u8] { self.as_bytes() }
}

unsafe impl<'a> TrustedInvariantLength for &'a btls_aux_collections::String
{
	fn as_byte_slice(&self) -> &[u8] { self.as_bytes() }
}

impl<'a> EscapeByteString<'a>
{
	pub fn cow(frag: &'a [u8]) -> Cow<'a, str>
	{
		__do_escape_binary_to_cow::<Escape>(frag)
	}
}

impl<'a> SafeShowByteString<'a>
{
	pub fn cow(frag: &'a [u8]) -> Cow<'a, str>
	{
		__do_escape_binary_to_cow::<SafeShow>(frag)
	}
}

impl<'a> EscapeString<'a>
{
	pub fn cow(frag: &'a str) -> Cow<'a, str>
	{
		__do_escape_string_to_cow::<Escape>(frag)
	}
}

impl<'a> SafeShowString<'a>
{
	pub fn cow(frag: &'a str) -> Cow<'a, str>
	{
		__do_escape_string_to_cow::<SafeShow>(frag)
	}
}

fn __do_escape_binary_to_cow<'a,E:ByteEscape>(frag: &'a [u8]) -> Cow<'a, str>
{
	use core::marker::PhantomData;
	struct ByteStringHelper<'b,F:ByteEscape>(&'b [u8], PhantomData<F>);
	impl<'b,F:ByteEscape> Display for ByteStringHelper<'b,F>
	{
		fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> { __do_binary_escape::<F>(self.0, f) }
	}
	//Try to cast to UTF-8, because if there is nothing to escape, the original memory should be reused.
	if let Ok(frag) = from_utf8(frag) {
		__do_escape_string_to_cow::<E>(frag)
	} else {
		Cow::Owned(ByteStringHelper::<E>(frag, PhantomData).to_string())
	}
}

fn __do_escape_string_to_cow<'a,E:ByteEscape>(frag: &'a str) -> Cow<'a, str>
{
	use core::marker::PhantomData;
	struct StringHelper<'b,F:ByteEscape>(&'b str, PhantomData<F>);
	impl<'b,F:ByteEscape> Display for StringHelper<'b,F>
	{
		fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> { __do_string_escape::<F>(self.0, f) }
	}
	//If next_jump is to the end, there is no need to escape anything. Otherwise it needs escaping.
	if E::next_jump(frag) >= frag.len() {
		Cow::Borrowed(frag)
	} else {
		Cow::Owned(StringHelper::<E>(frag, PhantomData).to_string())
	}
}
