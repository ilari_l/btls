#[cfg(feature="use-memchr")] use core::mem::size_of;
use core::convert::TryInto;
use core::hint::unreachable_unchecked;
use core::ops::Range;
use core::slice::from_raw_parts;
use core::slice::from_raw_parts_mut;
use core::str::from_utf8_unchecked;

#[cfg(feature="use-memchr")]
mod libc
{
	extern {
		pub(super) fn memchr(s: *const u8, c: i32, n: usize) -> *const u8;
		pub(super) fn memrchr(s: *const u8, c: i32, n: usize) -> *const u8;
	}
}

#[cfg(feature="use-memchr")]
macro_rules! do_memchr
{
	($func:ident, $haystack:ident, $needle:ident) => {
		if <T as PartialEq>::eq as usize == <u8 as PartialEq>::eq as usize && size_of::<T>() == 1 {
			fn ref2ptr<U>(x: &U) -> *const U { x as *const U }
			let pb: *const u8 = $haystack.as_ptr().cast();
			let pl = $haystack.len();
			let needle: *const u8 = ref2ptr($needle).cast();
			let p = unsafe{libc::$func(pb, *needle as i32, pl)};
			if p.is_null() { return None; }
			return Some(p as usize - pb as usize);
		}
	}
}

#[cfg(not(feature="use-memchr"))]
macro_rules! do_memchr
{
	($func:ident, $haystack:ident, $needle:ident) => {
		//Do nothing.
	}
}

unsafe fn from_raw_parts_str<'a>(data: *const u8, len: usize) -> &'a str
{
	unsafe{from_utf8_unchecked(from_raw_parts(data, len))}
}

fn slice_find<T:Eq>(haystack: &[T], needle: &[T]) -> Option<usize>
{
	//Special-case needle being only one element for speed.
	if needle.len() == 1 {
		let needle: &T = &needle[0];
		//Extra-special case for single char.
		do_memchr!(memchr, haystack, needle);
		for (index, value) in haystack.iter().enumerate() {
			if value == needle { return Some(index); }
		}
		return None;
	}
	//TODO: Better algorithm.
	let mut i = 0;
	while i + needle.len() <= haystack.len() {
		let x = unsafe{haystack.get_unchecked(i..i+needle.len())};
		if x == needle { return Some(i); }
		i += 1;
	}
	None
}

fn slice_rfind<T:Eq>(haystack: &[T], needle: &[T]) -> Option<usize>
{
	//Special-case needle being only one element for speed.
	if needle.len() == 1 {
		let needle: &T = &needle[0];
		//Extra-special case for single char.
		do_memchr!(memrchr, haystack, needle);
		for (index, value) in haystack.iter().enumerate().rev() {
			if value == needle { return Some(index); }
		}
		return None;
	}
	//TODO: Better algorithm.
	let mut i = haystack.len().checked_sub(needle.len())?;
	loop {
		let x = unsafe{haystack.get_unchecked(i..i+needle.len())};
		if x == needle { return Some(i); }
		if i == 0 { return None; }
		i -= 1;
	}
}

fn is_prefix<T:Eq>(string: &[T], prefix: &[T]) -> Option<usize>
{
	let n = prefix.len();
	let tmp = string.get(..n)?;
	if tmp == prefix { Some(n) } else { None }
}

fn is_suffix<T:Eq>(string: &[T], prefix: &[T]) -> Option<usize>
{
	let n = string.len().checked_sub(prefix.len())?;
	let tmp = string.get(n..)?;
	if tmp == prefix { Some(n) } else { None }
}

///Pattern attachment
pub enum Attachment
{
	///Left: Attach to left part.
	Left,
	///Center: Attach to neither part.
	Center,
	///Right: Attach to right part.
	Right,
}

macro_rules! split_attach_core2
{
	($pos:expr, $haystack:ident, $attach:ident, $nlen:expr) => {
		match $pos {
			Some(p) => {
				let (l,r) = match $attach {
					Attachment::Left => (p+$nlen,p+$nlen),
					Attachment::Center => (p,p+$nlen),
					Attachment::Right => (p,p),
				};
				unsafe{Ok($haystack.split(l, r))}
			},
			None => Err($haystack)
		}
	}
}

///Needle type.
///
/// # Safety:
///
/// * `len` MUST return valid delta for position to split on.
pub unsafe trait NeedleT
{
	fn length(&self) -> usize;
}

unsafe impl<T:Sized> NeedleT for [T]
{
	fn length(&self) -> usize { self.len() }
}

unsafe impl NeedleT for str
{
	fn length(&self) -> usize { self.len() }
}

///String type.
pub unsafe trait Splittable: Sized
{
	fn can_split(&self, pos: usize) -> bool;
	fn length(&self) -> usize;
	unsafe fn split(self, l: usize, r: usize) -> (Self, Self);
}

///Splittable into head and tail.
pub unsafe trait HtSplittable: Sized
{
	type Element: Sized;
	fn split_head_tail(self) -> Option<(<Self as HtSplittable>::Element, Self)>;
}

///Haystack type.
///
/// # Safety:
///
/// * `find_first` MUST return valid position to split on.
/// * `find_last` MUST return valid position to split on.
/// * `can_split` MUST be correct.
pub unsafe trait Haystack: Splittable
{
	type Needle: NeedleT+?Sized;
	fn find_first(&self, needle: &<Self as Haystack>::Needle) -> Option<usize>;
	fn find_last(&self, needle: &<Self as Haystack>::Needle) -> Option<usize>;
	fn do_strip_prefix(self, prefix: &<Self as Haystack>::Needle) -> Option<Self>;
	fn do_strip_suffix(self, suffix: &<Self as Haystack>::Needle) -> Option<Self>;
}

unsafe impl<'a,T:Sized> Splittable for &'a [T]
{
	fn can_split(&self, pos: usize) -> bool { pos <= self.len() }
	fn length(&self) -> usize { self.len() }
	unsafe fn split(self, l: usize, r: usize) -> (Self, Self)
	{
		let len = self.len();
		let ptr = self.as_ptr();
		unsafe{(from_raw_parts(ptr, l), from_raw_parts(ptr.add(r), len - r))}
	}
}

unsafe impl<'a,T:Copy+Sized> HtSplittable for &'a [T]
{
	type Element = T;
	fn split_head_tail(self) -> Option<(T, &'a [T])>
	{
		//head splitting guarantees tail is valid.
		let head = *self.get(0)?;
		let tail = unsafe{self.get_unchecked(1..)};
		Some((head, tail))
	}
}

unsafe impl<'a,T:Sized+Eq> Haystack for &'a [T]
{
	type Needle = [T];
	fn find_first(&self, needle: &[T]) -> Option<usize> { slice_find(self, needle) }
	fn find_last(&self, needle: &[T]) -> Option<usize> { slice_rfind(self, needle) }
	fn do_strip_prefix(self, prefix: &[T]) -> Option<Self>
	{
		let n = is_prefix(self, prefix)?;
		Some(unsafe{self.get_unchecked(n..)})
	}
	fn do_strip_suffix(self, suffix: &[T]) -> Option<Self>
	{
		let n = is_suffix(self, suffix)?;
		Some(unsafe{self.get_unchecked(..n)})
	}
}

unsafe impl<'a,T:Sized> Splittable for &'a mut [T]
{
	fn can_split(&self, pos: usize) -> bool { pos <= self.len() }
	fn length(&self) -> usize { self.len() }
	unsafe fn split(self, l: usize, r: usize) -> (Self, Self)
	{
		let len = self.len();
		let ptr = self.as_mut_ptr();
		unsafe{(from_raw_parts_mut(ptr, l), from_raw_parts_mut(ptr.add(r), len - r))}
	}
}

unsafe impl<'a,T:Copy+Sized> HtSplittable for &'a mut [T]
{
	type Element = T;
	fn split_head_tail(self) -> Option<(T, &'a mut [T])>
	{
		//head splitting guarantees tail is valid.
		let head = *self.get(0)?;
		let tail = unsafe{self.get_unchecked_mut(1..)};
		Some((head, tail))
	}
}

unsafe impl<'a,T:Sized+Eq> Haystack for &'a mut [T]
{
	type Needle = [T];
	fn find_first(&self, needle: &[T]) -> Option<usize> { slice_find(self, needle) }
	fn find_last(&self, needle: &[T]) -> Option<usize> { slice_rfind(self, needle) }
	fn do_strip_prefix(self, prefix: &[T]) -> Option<Self>
	{
		let n = is_prefix(self, prefix)?;
		Some(unsafe{self.get_unchecked_mut(n..)})
	}
	fn do_strip_suffix(self, suffix: &[T]) -> Option<Self>
	{
		let n = is_suffix(self, suffix)?;
		Some(unsafe{self.get_unchecked_mut(..n)})
	}
}

unsafe impl<'a> Splittable for &'a str
{
	fn can_split(&self, pos: usize) -> bool { self.is_char_boundary(pos) }
	fn length(&self) -> usize { self.len() }
	unsafe fn split(self, l: usize, r: usize) -> (Self, Self)
	{
		let len = self.len();
		let ptr = self.as_ptr();
		unsafe{(from_raw_parts_str(ptr, l), from_raw_parts_str(ptr.add(r), len - r))}
	}
}

//s MUST be non-empty string slice cast as &[u8]. On return, first element is a valid character code, and the second
//element is valid element to split on.
#[allow(unsafe_op_in_unsafe_fn)]
unsafe fn string_fcode(s: &[u8]) -> (u32, usize)
{
	let b1 = *s.get_unchecked(0) as u32;
	match b1 {
		0x00..=0x7F => {
			(b1, 1)
		},
		0xC0..=0xDF => {
			let b2 = *s.get_unchecked(1) as u32;
			((b1 << 6 ^ b2) & 0x7FF ^ 0x80, 2)
		},
		0xE0..=0xEF => {
			let b2 = *s.get_unchecked(1) as u32;
			let b3 = *s.get_unchecked(2) as u32;
			((b1 << 12 ^ b2 << 6 ^ b3) & 0xFFFF ^ 0x2080, 3)
		},
		0xF0..=0xF7 => {
			let b2 = *s.get_unchecked(1) as u32;
			let b3 = *s.get_unchecked(2) as u32;
			let b4 = *s.get_unchecked(3) as u32;
			((b1 << 18 ^ b2 << 12 ^ b3 << 6 ^ b4) & 0x1FFFFF ^ 0x82080, 4)
		},
		//The first byte of UTF-8 can not be anything else.
		_ => unreachable_unchecked()
	}
}

unsafe impl<'a> HtSplittable for &'a str
{
	type Element = char;
	fn split_head_tail(self) -> Option<(char, &'a str)>
	{
		if self.len() == 0 { return None; }
		let (head, headlen) = unsafe{string_fcode(self.as_bytes())};
		//This is always valid to split on, and head is always valid character code.
		let head = unsafe{char::from_u32_unchecked(head)};
		let tail = unsafe{self.get_unchecked(headlen..)};
		Some((head, tail))
	}
}

unsafe impl<'a> Haystack for &'a str
{
	type Needle = str;
	fn find_first(&self, needle: &str) -> Option<usize> { self.find(needle) }
	fn find_last(&self, needle: &str) -> Option<usize> { self.rfind(needle) }
	fn do_strip_prefix(self, prefix: &str) -> Option<Self> { self.strip_prefix(prefix) }
	fn do_strip_suffix(self, suffix: &str) -> Option<Self> { self.strip_suffix(suffix) }
}

///Strip prefix.
///
/// # Parameters:
///
/// * `string`: The string to strip. This may be:
///   * `&'a [T]`, for any `Sized` and `Eq` type `T`.
///   * `&'a mut [T]`, for any `Sized` and `Eq` type `T`.
///   * `&'a str`
/// * `prefix`: The slice/string to search. The type depends on type of `string`:
///   * `&[T]` if `string` is `&'a [T]` or `&'a mut [T]`.
///   * `&str` if `string` is `&'a str`.
///
/// # Error conditions:
///
/// * The `string` does not start with `prefix`.
///
/// # Return value:
///
///The slice/string, minus the initial prefix.
pub fn strip_prefix<H:Haystack>(string: H, prefix: &<H as Haystack>::Needle) -> Option<H>
{
	string.do_strip_prefix(prefix)
}

///Strip suffix.
///
/// # Parameters:
///
/// * `string`: The string to strip. This may be:
///   * `&'a [T]`, for any `Sized` and `Eq` type `T`.
///   * `&'a mut [T]`, for any `Sized` and `Eq` type `T`.
///   * `&'a str`
/// * `suffix`: The slice/string to search. The type depends on type of `string`:
///   * `&[T]` if `string` is `&'a [T]` or `&'a mut [T]`.
///   * `&str` if `string` is `&'a str`.
///
/// # Error conditions:
///
/// * The `string` does not end with `suffix`.
///
/// # Return value:
///
///The slice/string, minus the final suffix.
pub fn strip_suffix<H:Haystack>(string: H, suffix: &<H as Haystack>::Needle) -> Option<H>
{
	string.do_strip_suffix(suffix)
}

///Split slice/string at first match with given attachment.
///
/// # Parameters:
///
/// * `haystack`: The slice/string to search from. This may be:
///   * `&'a [T]`, for any `Sized` and `Eq` type `T`.
///   * `&'a mut [T]`, for any `Sized` and `Eq` type `T`.
///   * `&'a str`
/// * `needle`: The slice/string to search. The type depends on type of `haystack`:
///   * `&[T]` if `haystack` is `&'a [T]` or `&'a mut [T]`.
///   * `&str` if `haystack` is `&'a str`.
/// * `attachment`: Attachment for the searched slice.
///
/// # Error conditions:
///
/// * Specified slice/string is not found.
///
/// # Return value:
///
///Tuple (both returned parts have the same type (including lifetimes) as the argument type):
///
/// 1. The left part.
/// 1. The right part.
pub fn split_attach_first<H:Haystack>(haystack: H, needle: &<H as Haystack>::Needle, attachment: Attachment) ->
	Result<(H, H), H>
{
	let pos = haystack.find_first(needle);
	split_attach_core2!(pos, haystack, attachment, needle.length())
}

///Split slice/string at last match with given attachment.
///
/// # Parameters:
///
/// * `haystack`: The slice/string to search from. This may be:
///   * `&'a [T]`, for any `Sized` and `Eq` type `T`.
///   * `&'a mut [T]`, for any `Sized` and `Eq` type `T`.
///   * `&'a str`
/// * `needle`: The slice/string to search. The type depends on type of `haystack`:
///   * `&[T]` if `haystack` is `&'a [T]` or `&'a mut [T]`.
///   * `&str` if `haystack` is `&'a str`.
/// * `attachment`: Attachment for the searched slice.
///
/// # Error conditions:
///
/// * Specified slice/string is not found.
///
/// # Return value:
///
///Tuple (both returned parts have the same type (including lifetimes) as the argument type):
///
/// 1. The left part.
/// 1. The right part.
pub fn split_attach_last<H:Haystack>(haystack: H, needle: &<H as Haystack>::Needle, attachment: Attachment) ->
	Result<(H, H), H>
{
	let pos = haystack.find_last(needle);
	split_attach_core2!(pos, haystack, attachment, needle.length())
}

///Split slice/string at index.
///
/// # Parameters:
///
/// * `a`: The slice/string to split. This may be:
///   * `&'a [T]`, for any `Sized` type `T`.
///   * `&'a mut [T]`, for any `Sized` type `T`.
///   * `&'a str`
/// * `n`: Number of elments to place to first part.
///
/// # Error conditions:
///
/// * Length of `a` is less than `n`.
/// * `n` is not a valid codepoint boundary (when splitting `&'a str`).
///
/// # Return value:
///
///A tuple (both returned parts have the same type (including lifetimes) as the argument type):
///
/// 1. `first`: First part with `n` elements.
/// 1. `second`: The remaining elements.
pub fn split_at<S:Splittable>(a: S, n: usize) -> Result<(S, S), S>
{
	if a.can_split(n) { Ok(unsafe{a.split(n, n)}) } else { Err(a) }
}

///Split slice/string at reverse index.
///
/// # Parameters:
///
/// * `a`: The slice/string to split. This may be:
///   * `&'a [T]`, for any `Sized` type `T`.
///   * `&'a mut [T]`, for any `Sized` type `T`.
///   * `&'a str`
/// * `n`: Number of elments to place to second part.
///
/// # Error conditions:
///
/// * Length of `a` is less than `n`.
/// * `len-n` is not a valid codepoint boundary (when splitting `&'a str`).
///
/// # Return value:
///
///A tuple (both returned parts have the same type (including lifetimes) as the argument type):
///
/// 1. `first`: The remaining elements.
/// 1. `second`: Second part with `n` elements.
pub fn rsplit_at<S:Splittable>(a: S, n: usize) -> Result<(S, S), S>
{
	let n = a.length().wrapping_sub(n);	//This will be OOR if n is bad.
	split_at(a, n)
}

///Split slice/string into head element and tail.
///
/// # Parameters:
///
/// * `a`: The slice/string to split. This may be:
///   * `&'a [T]`, for any `Copy+Sized` type `T`.
///   * `&'a mut [T]`, for any `Copy+Sized` type `T`.
///   * `&'a str`
///
/// # Error conditions:
///
/// * `a` is empty.
///
/// # Return value:
///
///A tuple:
///
/// 1. `head`: The first element.
/// 1. `tail`: The remaining elements. This has the same type (including lifetime) as the argument type.
pub fn split_head_tail<S:HtSplittable>(a: S) -> Option<(<S as HtSplittable>::Element, S)>
{
	a.split_head_tail()
}

///Split first array reference.
///
/// # Parameters:
///
/// * `a`: Slice of any `Sized` type.
///
/// # Error conditions:
///
/// * `a` is not at least `N` elements
///
/// # Return value:
///
///A tuple:
///
/// 1. `head`: Reference to array of first `N` elements.
/// 1. `tail`: The remaining elements.
pub fn split_array_head<'a,T:Sized+'a,const N: usize>(a: &'a [T]) -> Option<(&'a [T;N], &'a [T])>
{
	let (head, tail) = split_at(a, N).ok()?;
	//This should never fail.
	let head: &[T;N] = head.try_into().ok()?;
	Some((head, tail))
}

///Split first array mutable reference.
///
/// # Parameters:
///
/// * `a`: Mutable slice of any `Sized` type.
///
/// # Error conditions:
///
/// * `a` is not at least `N` elements
///
/// # Return value:
///
///A tuple:
///
/// 1. `head`: Mutable reference to array of first `N` elements.
/// 1. `tail`: The remaining elements.
pub fn split_array_head_mut<'a,T:Sized+'a,const N: usize>(a: &'a mut [T]) -> Option<(&'a mut [T;N], &'a mut [T])>
{
	let (head, tail) = split_at(a, N).ok()?;
	//This should never fail.
	let head: &mut [T;N] = head.try_into().ok()?;
	Some((head, tail))
}

///Split second array reference.
///
/// # Parameters:
///
/// * `a`: Slice of any `Sized` type.
///
/// # Error conditions:
///
/// * `a` is not at least `N` elements
///
/// # Return value:
///
///A tuple:
///
/// 1. `head`: The remaining elements.
/// 1. `tail`: reference to array of last `N` elements.
pub fn split_array_tail<'a,T:Sized+'a,const N: usize>(a: &'a [T]) -> Option<(&'a [T], &'a [T;N])>
{
	let (head, tail) = rsplit_at(a, N).ok()?;
	//This should never fail.
	let tail: &[T;N] = tail.try_into().ok()?;
	Some((head, tail))
}

///Split second array mutable reference.
///
/// # Parameters:
///
/// * `a`: Mutable slice of any `Sized` type.
///
/// # Error conditions:
///
/// * `a` is not at least `N` elements
///
/// # Return value:
///
///A tuple:
///
/// 1. `head`: The remaining elements.
/// 1. `tail`: Mutable reference to array of last `N` elements.
pub fn split_array_tail_mut<'a,T:Sized+'a,const N: usize>(a: &'a mut [T]) -> Option<(&'a mut [T], &'a mut [T;N])>
{
	let (head, tail) = rsplit_at(a, N).ok()?;
	//This should never fail.
	let tail: &mut [T;N] = tail.try_into().ok()?;
	Some((head, tail))
}

#[deprecated(since="1.1.0",note="use split_at() instead")]
pub fn slice_split_at<'a,T:Sized+'a>(a: &'a [T], n: usize) -> Option<(&'a [T], &'a [T])>
{
	split_at(a, n).ok()
}

#[deprecated(since="1.1.0",note="use split_at() instead")]
pub fn slice_split_at_mut<'a,T:Sized+'a>(a: &'a mut [T], n: usize) -> Option<(&'a mut [T], &'a mut [T])>
{
	split_at(a, n).ok()
}

#[deprecated(since="1.1.0",note="use rsplit_at() instead")]
pub fn slice_rsplit_at<'a,T:Sized+'a>(a: &'a [T], n: usize) -> Option<(&'a [T], &'a [T])>
{
	rsplit_at(a, n).ok()
}

#[deprecated(since="1.1.0",note="use rsplit_at() instead")]
pub fn slice_rsplit_at_mut<'a,T:Sized+'a>(a: &'a mut [T], n: usize) -> Option<(&'a mut [T], &'a mut [T])>
{
	rsplit_at(a, n).ok()
}

fn __is_ascii_whitespace(x: u8) -> bool { matches!(x, 9|10|13|32) }

fn __trim_ascii(x: &[u8]) -> Range<usize>
{
	let mut s = 0;
	let mut e = x.len();
	while s < x.len() && __is_ascii_whitespace(x[s]) { s += 1; }
	while e > s && __is_ascii_whitespace(x[e-1]) { e -= 1; }
	s..e
}

///Some kind of string-like thing one can strip ASCII whitespace from. 
pub unsafe trait AsciiTrimmable: Sized
{
	fn to_slice(&self) -> &[u8];
	unsafe fn slice_unchecked(self, r: Range<usize>) -> Self;
}

unsafe impl<'a> AsciiTrimmable for &'a [u8]
{
	fn to_slice(&self) -> &[u8] { self }
	unsafe fn slice_unchecked(self, r: Range<usize>) -> Self { unsafe{self.get_unchecked(r)} }
}

unsafe impl<'a> AsciiTrimmable for &'a mut [u8]
{
	fn to_slice(&self) -> &[u8] { self }
	unsafe fn slice_unchecked(self, r: Range<usize>) -> Self { unsafe{self.get_unchecked_mut(r)} }
}

unsafe impl<'a> AsciiTrimmable for &'a str
{
	fn to_slice(&self) -> &[u8] { self.as_bytes() }
	unsafe fn slice_unchecked(self, r: Range<usize>) -> Self { unsafe{self.get_unchecked(r)} }
}


///Trim ASCII whitespace from start/end of slice/string.
///
/// # Parameters:
///
/// * `parent`: The parent slice/string. This may be:
///   * `&'a [u8]`.
///   * `&'a mut [u8]`.
///   * `&'a str`
///
/// # Returns:
///
///Slice with whitespace (codes 9, 10, 13 and 32) stripped from start and end.
///
///The return type (including lifetimes) is the same as argument type.
pub fn trim_ascii<S:AsciiTrimmable>(parent: S) -> S
{
	let r = __trim_ascii(parent.to_slice());
	unsafe{parent.slice_unchecked(r)}
}

#[test]
fn test_slice_split_attach_first()
{
	const A: &'static [u8] = b"foobarbazbarzot";
	const B: &'static [u8] = b"bar";
	const C: &'static [u8] = b"qux";
	assert!(split_attach_first(A, B, Attachment::Left) == Ok((b"foobar", b"bazbarzot")));
	assert!(split_attach_first(A, B, Attachment::Center) == Ok((b"foo", b"bazbarzot")));
	assert!(split_attach_first(A, B, Attachment::Right) == Ok((b"foo", b"barbazbarzot")));
	assert!(split_attach_first(A, C, Attachment::Left) == Err(A));
	assert!(split_attach_first(A, C, Attachment::Center) == Err(A));
	assert!(split_attach_first(A, C, Attachment::Right) == Err(A));
}

#[test]
fn test_slice_split_attach_last()
{
	const A: &'static [u8] = b"foobarbazbarzot";
	const B: &'static [u8] = b"bar";
	const C: &'static [u8] = b"qux";
	assert!(split_attach_last(A, B, Attachment::Left) == Ok((b"foobarbazbar", b"zot")));
	assert!(split_attach_last(A, B, Attachment::Center) == Ok((b"foobarbaz", b"zot")));
	assert!(split_attach_last(A, B, Attachment::Right) == Ok((b"foobarbaz", b"barzot")));
	assert!(split_attach_last(A, C, Attachment::Left) == Err(A));
	assert!(split_attach_last(A, C, Attachment::Center) == Err(A));
	assert!(split_attach_last(A, C, Attachment::Right) == Err(A));
}

#[test]
fn test_str_split_attach_first()
{
	const A: &'static str = "foobarbazbarzot";
	const B: &'static str = "bar";
	const C: &'static str = "qux";
	assert!(split_attach_first(A, B, Attachment::Left) == Ok(("foobar", "bazbarzot")));
	assert!(split_attach_first(A, B, Attachment::Center) == Ok(("foo", "bazbarzot")));
	assert!(split_attach_first(A, B, Attachment::Right) == Ok(("foo", "barbazbarzot")));
	assert!(split_attach_first(A, C, Attachment::Left) == Err(A));
	assert!(split_attach_first(A, C, Attachment::Center) == Err(A));
	assert!(split_attach_first(A, C, Attachment::Right) == Err(A));
}

#[test]
fn test_str_split_attach_last()
{
	const A: &'static str = "foobarbazbarzot";
	const B: &'static str = "bar";
	const C: &'static str = "qux";
	assert!(split_attach_last(A, B, Attachment::Left) == Ok(("foobarbazbar", "zot")));
	assert!(split_attach_last(A, B, Attachment::Center) == Ok(("foobarbaz", "zot")));
	assert!(split_attach_last(A, B, Attachment::Right) == Ok(("foobarbaz", "barzot")));
	assert!(split_attach_last(A, C, Attachment::Left) == Err(A));
	assert!(split_attach_last(A, C, Attachment::Center) == Err(A));
	assert!(split_attach_last(A, C, Attachment::Right) == Err(A));
}

#[test]
fn test_slice_split_attach_one_first()
{
	const A: &'static [u8] = b"foo/baz/zot";
	const B: &'static [u8] = b"/";
	const C: &'static [u8] = b"qux";
	assert!(split_attach_first(A, B, Attachment::Left) == Ok((b"foo/", b"baz/zot")));
	assert!(split_attach_first(A, B, Attachment::Center) == Ok((b"foo", b"baz/zot")));
	assert!(split_attach_first(A, B, Attachment::Right) == Ok((b"foo", b"/baz/zot")));
	assert!(split_attach_first(A, C, Attachment::Left) == Err(A));
	assert!(split_attach_first(A, C, Attachment::Center) == Err(A));
	assert!(split_attach_first(A, C, Attachment::Right) == Err(A));
}

#[test]
fn test_slice_split_attach_one_last()
{
	const A: &'static [u8] = b"foo/baz/zot";
	const B: &'static [u8] = b"/";
	const C: &'static [u8] = b"qux";
	assert!(split_attach_last(A, B, Attachment::Left) == Ok((b"foo/baz/", b"zot")));
	assert!(split_attach_last(A, B, Attachment::Center) == Ok((b"foo/baz", b"zot")));
	assert!(split_attach_last(A, B, Attachment::Right) == Ok((b"foo/baz", b"/zot")));
	assert!(split_attach_last(A, C, Attachment::Left) == Err(A));
	assert!(split_attach_last(A, C, Attachment::Center) == Err(A));
	assert!(split_attach_last(A, C, Attachment::Right) == Err(A));
}

#[test]
fn test_str_split_attach_one()
{
	const A: &'static str = "foo/baz/zot";
	const B: &'static str = "/";
	assert!(split_attach_first(A, B, Attachment::Left) == Ok(("foo/", "baz/zot")));
	assert!(split_attach_first(A, B, Attachment::Center) == Ok(("foo", "baz/zot")));
	assert!(split_attach_first(A, B, Attachment::Right) == Ok(("foo", "/baz/zot")));
	assert!(split_attach_last(A, B, Attachment::Left) == Ok(("foo/baz/", "zot")));
	assert!(split_attach_last(A, B, Attachment::Center) == Ok(("foo/baz", "zot")));
	assert!(split_attach_last(A, B, Attachment::Right) == Ok(("foo/baz", "/zot")));
}

#[test]
fn test_string_fcode()
{
	for i in 0..0x110000 {
		let k = match char::from_u32(i) { Some(t) => t, None => continue };
		let mut j = [0;4];
		let j = k.encode_utf8(&mut j);
		let l = k.len_utf8();
		assert_eq!(unsafe{string_fcode(j.as_bytes())}, (i, l));
	}
}

#[test]
fn test_split_array()
{
	let mut orig = [0u8;5];
	orig.copy_from_slice(b"bazot");
	let orig = &mut orig;

	let (head, tail): (&[u8;3],&[u8]) = split_array_head(orig).unwrap();
	assert!(head == b"baz" && tail == b"ot");
	let (head, tail): (&[u8],&[u8;3]) = split_array_tail(orig).unwrap();
	assert!(head == b"ba" && tail == b"zot");
	let (head, tail): (&mut [u8;3],&mut [u8]) = split_array_head_mut(orig).unwrap();
	assert!(head == b"baz" && tail == b"ot");
	let (head, tail): (&mut [u8],&mut [u8;3]) = split_array_tail_mut(orig).unwrap();
	assert!(head == b"ba" && tail == b"zot");

	let (head, tail): (&[u8;5],&[u8]) = split_array_head(orig).unwrap();
	assert!(head == b"bazot" && tail == b"");
	let (head, tail): (&[u8],&[u8;5]) = split_array_tail(orig).unwrap();
	assert!(head == b"" && tail == b"bazot");
	let (head, tail): (&mut [u8;5],&mut [u8]) = split_array_head_mut(orig).unwrap();
	assert!(head == b"bazot" && tail == b"");
	let (head, tail): (&mut [u8],&mut [u8;5]) = split_array_tail_mut(orig).unwrap();
	assert!(head == b"" && tail == b"bazot");

	assert!(split_array_head::<u8,6>(orig).is_none());
	assert!(split_array_head_mut::<u8,6>(orig).is_none());
	assert!(split_array_tail::<u8,6>(orig).is_none());
	assert!(split_array_tail_mut::<u8,6>(orig).is_none());
}
