#[cfg(test)] use super::ByteStringLines;
use btls_aux_fail::f_break;
use btls_aux_fail::fail_if_none;
use core::cmp::min;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::mem::transmute;
use core::slice::from_raw_parts;
use core::str::from_utf8;
use core::str::from_utf8_unchecked;

///Byte or fragment of string.
pub enum MUtf8StringFragment<'a>
{
	String(&'a str),
	Byte(u8),
}

///Iterator over parts of mostly-UTF8 string.
pub struct MUtf8StringIterator<'a>
{
	remaining: &'a [u8]
}

impl<'a> Iterator for MUtf8StringIterator<'a>
{
	type Item = MUtf8StringFragment<'a>;
	fn next(&mut self) -> Option<MUtf8StringFragment<'a>>
	{
		//End of string?
		fail_if_none!(self.remaining.len() == 0);
		let r = match from_utf8(self.remaining) {
			Ok(s) => {
				//This was all.
				self.remaining = &[];
				MUtf8StringFragment::String(s)
			},
			Err(e) => {
				let len = e.valid_up_to();
				if len > 0 {
					//This is some string fragment before invalid byte.
					//len < self.remaining.len() by definition.
					//By definition, this is valid Utf-8
					let head = unsafe{from_utf8_unchecked(self.remaining.get_unchecked(..len))};
					self.remaining = unsafe{self.remaining.get_unchecked(len..)};
					MUtf8StringFragment::String(head)
				} else {
					//This is singular invalid byte. self.remaining.len() > 0.
					let b = *unsafe{self.remaining.get_unchecked(0)};
					self.remaining = unsafe{self.remaining.get_unchecked(1..)};
					MUtf8StringFragment::Byte(b)
				}
			}
		};
		Some(r)
	}
}

///Iterate fragments of mostly-UTF8 string.
pub fn iterate_mostly_utf8_fragments<'a>(x: &'a [u8]) -> MUtf8StringIterator<'a>
{
	MUtf8StringIterator { remaining: x }
}

pub(crate) trait ByteEscape
{
	fn next_jump(input: &str) -> usize;
	fn escape(ch: char, f: &mut Formatter) -> FmtRet;
	//input.len() > 0. Return number of bytes processed.
	unsafe fn binary(input: &[u8], f: &mut Formatter) -> Result<usize, FmtError>;
}

type FmtRet = Result<(), FmtError>;

///Escape a byte string.
///
///The argument is the slice to dump.
///
///All non-UTF-8 and control characters are escaped.
///
///This structure implements `Display` for printing human-readable error message using the `{}` format placeholder.
pub struct EscapeByteString<'a>(pub &'a [u8]);

///Escape a byte string.
///
///The argument is the slice to dump.
///
///All non-UTF-8 and control characters, except `HT`, `CR` and `LF` are mangled.
///
///This structure implements `Display` for printing human-readable error message using the `{}` format placeholder.
pub struct SafeShowByteString<'a>(pub &'a [u8]);
///Escape a string.
///
///The argument is the string to dump.
///
///All control characters are escaped.
///
///This structure implements `Display` for printing human-readable error message using the `{}` format placeholder.
pub struct EscapeString<'a>(pub &'a str);

///Escape a string.
///
///The argument is the string to dump.
///
///All control characters, except `HT`, `CR` and `LF` are mangled.
///
///This structure implements `Display` for printing human-readable error message using the `{}` format placeholder.
pub struct SafeShowString<'a>(pub &'a str);

pub(crate) fn __do_binary_escape<E:ByteEscape>(input: &[u8], f: &mut Formatter) -> FmtRet
{
	let mut i = 0;
	while i < input.len() {
		let remainder = &input[i..];
		let delta = match from_utf8(remainder) {
			Ok(s) => {
				__do_string_escape::<E>(s, f)?;
				s.len()
			}
			Err(e) => {
				let len = e.valid_up_to();
				if len > 0 {
					//len < remainder.len(). And this is by definition valid UTF-8.
					let s = unsafe{remainder.get_unchecked(..len)};
					let s = unsafe{from_utf8_unchecked(s)};
					__do_string_escape::<E>(s, f)?;
					len
				} else {
					unsafe{E::binary(remainder, f)}?
				}
			}
		};
		i += delta;
	}
	Ok(())
}

pub(crate) fn __do_string_escape<E:ByteEscape>(frag: &str, f: &mut Formatter) -> FmtRet
{
	let mut i = 0;
	while i < frag.len() {
		//Assume i is at codepoint boundary.
		let frag = unsafe{frag.get_unchecked(i..)};
		let len = E::next_jump(frag);
		//Assume E::next_jump() gives codepoint boundary.
		if len > 0 {
			let frag = unsafe{frag.get_unchecked(..len)};
			f.write_str(frag)?;
			i += frag.len();
		}
		let frag = unsafe{frag.get_unchecked(len..)};
		//Extract first character, which is presumably special. If there is nothing, we are done.
		let ch = f_break!(frag.chars().next());
		//A few codepoints use special escapes.
		E::escape(ch, f)?;
		i += ch.len_utf8();
	}
	Ok(())
}

fn __is_bad_control(b: u8) -> bool
{
	//Controls, except HT, LF and CR.
	matches!(b, 0..=8|11|12|14..=31)
}

//frag shall be valid UTF-8, i shall be < frag.len().
unsafe fn __is_extended_bad_char(frag: &[u8], b: u8, i: usize) -> bool
{
	if b == 0x7F { return true; }
	if b == 0xC2 {
		//Because frag is valid UTF-8, there is always a next byte. And it is always at
		//least 128.
		let b2 = *unsafe{frag.get_unchecked(i+1)};
		if b2 < 160 { return true; }
	}
	if b == 0xE2 {
		//Because frag is valid UTF-8, there is always two next bytes. And it is always at
		//least 128.
		let b2 = *unsafe{frag.get_unchecked(i+1)};
		let b3 = *unsafe{frag.get_unchecked(i+2)};
		if b2 == 0x80 && b3 >= 0xAA && b3 <= 0xAE { return true; }
		if b2 == 0x81 && b3 >= 0xA6 && b3 <= 0xA9 { return true; }
	}
	if b == 0xEF {
		//Because frag is valid UTF-8, there is always two next bytes. And it is always at
		//least 128.
		let b2 = *unsafe{frag.get_unchecked(i+1)};
		let b3 = *unsafe{frag.get_unchecked(i+2)};
		if b2 == 0xBF && b3 >= 0xBE { return true; }
	}
	false
}

impl<'a> Display for EscapeByteString<'a>
{
	fn fmt(&self, f: &mut Formatter) -> FmtRet
	{
		__do_binary_escape::<Escape>(self.0, f)
	}
}

impl<'a> Display for SafeShowByteString<'a>
{
	fn fmt(&self, f: &mut Formatter) -> FmtRet
	{
		__do_binary_escape::<SafeShow>(self.0, f)
	}
}

impl<'a> Display for EscapeString<'a>
{
	fn fmt(&self, f: &mut Formatter) -> FmtRet
	{
		__do_string_escape::<Escape>(self.0, f)
	}
}

impl<'a> Display for SafeShowString<'a>
{
	fn fmt(&self, f: &mut Formatter) -> FmtRet
	{
		__do_string_escape::<SafeShow>(self.0, f)
	}
}

pub(crate) struct Escape;
impl ByteEscape for Escape
{
	fn next_jump(frag: &str) -> usize
	{
		let frag = frag.as_bytes();
		let mut i = 0;
		while i < frag.len() {
			let b = frag[i];
			//Bytes 0-1F, 22, 27, 5c, and extended bad.
			if b < 32 || b == 34 || b == 39 || b == 92 { break; }
			if unsafe{__is_extended_bad_char(frag, b, i)} { break; }
			i += 1;
		}
		//There are no limits in frag, process to end.
		i
	}
	fn escape(ch: char, f: &mut Formatter) -> FmtRet
	{
		//A few codepoints use special escapes.
		match ch as u32 {
			7 => f.write_str("\\a"),
			8 => f.write_str("\\b"),
			9 => f.write_str("\\t"),
			10 => f.write_str("\\n"),
			11 => f.write_str("\\v"),
			12 => f.write_str("\\f"),
			13 => f.write_str("\\r"),
			34 => f.write_str("\\\""),
			39 => f.write_str("\\\'"),
			92 => f.write_str("\\\\"),
			ch => write!(f, "\\u{{{ch:x}}}"),
		}
	}
	//Safety: frag.len() > 0.
	unsafe fn binary(frag: &[u8], f: &mut Formatter) -> Result<usize, FmtError>
	{
		//Surrogate specials.
		if frag.len() >= 3 && frag[0] == 0xED && frag[1] & 0xE0 == 0xA0 && frag[2] & 0xC0 == 0x80 {
			let v = (frag[1] as u16 & 0x1F) << 6 | frag[2] as u16 & 0x3F | 0x800;
			write!(f, "\\sd{v:03x}")?;
			return Ok(3);
		}
		//This is singular invalid byte. frag.len() > 0.
		let b = unsafe{frag.get_unchecked(0)};
		write!(f, "\\x{b:02x}")?;
		Ok(1)
	}
}

pub(crate) struct SafeShow;
impl ByteEscape for SafeShow
{
	fn next_jump(frag: &str) -> usize
	{
		let frag = frag.as_bytes();
		let mut i = 0;
		while i < frag.len() {
			let b = frag[i];
			//Controls (except 9, 10 and 13), and extended bad.
			if __is_bad_control(b) { break; }
			if unsafe{__is_extended_bad_char(frag, b, i)} { break; }
			i += 1;
		}
		//There are no limits in frag, process to end.
		i
	}
	fn escape(ch: char, f: &mut Formatter) -> FmtRet
	{
		write!(f, "ᐸu{ch:x}ᐳ", ch=ch as u32)
	}
	//Safety: frag.len() > 0.
	unsafe fn binary(frag: &[u8], f: &mut Formatter) -> Result<usize, FmtError>
	{
		let b = unsafe{frag.get_unchecked(0)};
		write!(f, "ᐸx{b:02x}ᐳ")?;
		Ok(1)
	}
}

const BLOCK_SIZE: usize = 128;
const BLOCK_HEXES: usize = 2 * BLOCK_SIZE;

//The output is guaranteed to be valid UTF-8.
fn hexdump_block(output: &mut [u8], input: &[u8])
{
	let max = min(output.len() / 2, input.len());
	for i in 0..max {
		let b = unsafe{input.get_unchecked(i)};
		let b1 = b / 16;
		let b2 = b % 16;
		unsafe{
			*output.get_unchecked_mut(2*i+0) = 48 + b1 + (b1 + 6) / 16 * 39;
			*output.get_unchecked_mut(2*i+1) = 48 + b2 + (b2 + 6) / 16 * 39;
		}
	}
}

///Print a hexdump of byte slice.
///
///The argument is the slice to dump.
///
///This structure implements `Display` for printing human-readable error message using the `{}` format placeholder.
pub struct Hexdump<'a>(pub &'a [u8]);

impl<'a> Display for Hexdump<'a>
{
	fn fmt(&self, f: &mut Formatter) -> FmtRet
	{
		let mut mem = [0u8;BLOCK_HEXES];
		let mut itr = self.0;
		while itr.len() >= BLOCK_SIZE {
			let (head, tail) = itr.split_at(BLOCK_SIZE);
			itr = tail;
			let head: &[u8;BLOCK_SIZE] = unsafe{transmute(head.as_ptr())};
			hexdump_block(&mut mem, head);
			f.write_str(unsafe{from_utf8_unchecked(&mem)})?;
		}
		let rlen = 2 * itr.len();
		hexdump_block(&mut mem, itr);
		f.write_str(unsafe{from_utf8_unchecked(mem.get_unchecked(..rlen))})?;
		Ok(())
	}
}

///Truncate specified slice on first `#` byte, or take the entiere thing if there is none.
///
/// # Parameters:
///
/// * `x`: The slice to truncate.
///
/// # Return value:
///
///The truncated line.
///
/// # Notes:
///
/// * The return value never contains '#' byte.
pub fn truncate_line_hash<'a>(x: &'a [u8]) -> &'a [u8]
{
	let mut i = 0;
	while i < x.len() {
		if x[i] == b'#' { return &x[..i]; }
		i += 1;
	}
	x
}

///Interpret given slice as UTF-8 string.
///
/// # Parameters:
///
/// * `x`: The slice to re-interpret.
///
/// # Error conditions:
///
/// * The slice is not valid UTF-8.
///
/// # Return value:
///
///The UTF-8 string.
pub fn filter_map_utf8<'a>(x: &'a [u8]) -> Option<&'a str>
{
	from_utf8(x).ok()
}

///Merge two options.
///
/// # Parameters:
///
/// * `a`: The first option to merge.
/// * `b`: The second option to merge.
/// * `f`: Merge function to run if both options are `Some`.
///
/// # Return value:
///
/// * If both `a` and `b` are `None`, `None`.
/// * If `a` xor `b` is `Some`, that value.
/// * If both `a` and `b` are `Some`, `Some(f(a.unwrap(), b.unwrap()))`.
pub fn option_merge<T,F:FnOnce(T,T)->T>(a: Option<T>, b: Option<T>, f: F) -> Option<T>
{
	match (a, b) {
		(Some(a), Some(b)) => Some(f(a, b)),
		(Some(a), None) => Some(a),
		(None, Some(b)) => Some(b),
		(None, None) => None,
	}
}

///Trait: C-style string.
pub trait CStyleString: Copy
{
	fn canonicalize(self) -> *const u8;
}

impl CStyleString for *const u8
{
	fn canonicalize(self) -> *const u8 { self }
}

impl CStyleString for *mut u8
{
	fn canonicalize(self) -> *const u8 { self }
}

impl CStyleString for *const i8
{
	fn canonicalize(self) -> *const u8 { self.cast() }
}

impl CStyleString for *mut i8
{
	fn canonicalize(self) -> *const u8 { self.cast() }
}

impl CStyleString for core::ptr::NonNull<u8>
{
	fn canonicalize(self) -> *const u8 { self.as_ptr() }
}

impl CStyleString for core::ptr::NonNull<i8>
{
	fn canonicalize(self) -> *const u8 { self.as_ptr().cast() }
}

impl CStyleString for crate::NonNull<u8>
{
	fn canonicalize(self) -> *const u8 { self.to_raw() }
}

impl CStyleString for crate::NonNull<i8>
{
	fn canonicalize(self) -> *const u8 { self.to_raw().cast() }
}

impl CStyleString for crate::NonNullMut<u8>
{
	fn canonicalize(self) -> *const u8 { self.to_raw() }
}

impl CStyleString for crate::NonNullMut<i8>
{
	fn canonicalize(self) -> *const u8 { self.to_raw().cast() }
}


///Trait: Can be referenced into `&[u8]'.
pub trait AsRefU8Slice
{
	fn as_ref_u8_slice(&self) -> &[u8];
}

impl AsRefU8Slice for [u8]
{
	fn as_ref_u8_slice(&self) -> &[u8] { self }
}

impl AsRefU8Slice for [i8]
{
	fn as_ref_u8_slice(&self) -> &[u8] { unsafe{transmute(self)} }
}

impl<const N:usize> AsRefU8Slice for [u8;N]
{
	fn as_ref_u8_slice(&self) -> &[u8] { self }
}

impl<const N:usize> AsRefU8Slice for [i8;N]
{
	fn as_ref_u8_slice(&self) -> &[u8] { unsafe{transmute(&self[..])} }
}

///Read slice as NUL-terminated ASCII.
///
/// # Parameters:
///
/// * `input`: The buffer to read the string from.
///
/// # Return value:
///
///The read bstr.
pub fn asciiz_slice(input: &(impl AsRefU8Slice+?Sized)) -> &[u8]
{
	__asciiz_slice(input.as_ref_u8_slice())
}

fn __asciiz_slice(input: &[u8]) -> &[u8]
{
	let mut len = 0;
	while len < input.len() && input[len] != 0 { len += 1; }
	unsafe{input.get_unchecked(..len)}
}

///Get length of NUL-terminated ASCII string.
///
/// # Safety:
///
/// The `input` must point to memory block containing a `NUL`.
///
/// # Parameters:
///
/// * `input`: The pointer to start reading the string from.
///
/// # Return value:
///
///The length of string in bytes.
pub unsafe fn asciiz_strlen(input: impl CStyleString) -> usize
{
	unsafe{__asciiz_strlen(input.canonicalize())}
}

///Read pointer as NUL-terminated ASCII.
///
/// # Safety:
///
/// The `input` must point to memory block containing a `NUL`.
///
/// # Parameters:
///
/// * `input`: The pointer to start reading the string from.
///
/// # Return value:
///
///The read bstr.
pub unsafe fn asciiz_pointer<'a>(input: impl CStyleString) -> &'a [u8]
{
	unsafe{__asciiz_pointer(input.canonicalize())}
}

unsafe fn __asciiz_strlen<'a>(input: *const u8) -> usize
{
	let mut len = 0;
	while unsafe{input.add(len).read()} != 0 { len += 1; }
	len
}

unsafe fn __asciiz_pointer<'a>(input: *const u8) -> &'a [u8]
{
	unsafe{from_raw_parts(input, __asciiz_strlen(input))}
}

#[cfg(test)]
fn do_binarylines_test(input: &[u8], output: &[&[u8]])
{
	let mut r = ByteStringLines::new(input);
	for part in output.iter().copied() {
		
		assert_eq!(r.next(), Some(part));
	}
	assert!(r.next().is_none());
}

#[cfg(test)]
fn id(x: &[u8]) -> &[u8] { x }

#[test]
fn binarylines_basic()
{
	let input = b"foo\nbar\nbaz";
	let output = [id(b"foo"), id(b"bar"), id(b"baz")];
	do_binarylines_test(input, &output);
}

#[test]
fn binarylines_trailing_lf()
{
	let input = b"foo\nbar\nbaz\n";
	let output = [id(b"foo"), id(b"bar"), id(b"baz")];
	do_binarylines_test(input, &output);
}

#[test]
fn binarylines_cr()
{
	let input = b"foo\rbar\rbaz";
	let output = [id(b"foo"), id(b"bar"), id(b"baz")];
	do_binarylines_test(input, &output);
}

#[test]
fn binarylines_crlf()
{
	let input = b"foo\r\nbar\r\nbaz";
	let output = [id(b"foo"), id(b"bar"), id(b"baz")];
	do_binarylines_test(input, &output);
}

#[test]
fn truncate_line_hash_yes()
{
	assert!(truncate_line_hash(b"foo#bar") == b"foo");
}

#[test]
fn truncate_line_hash_no()
{
	assert!(truncate_line_hash(b"foo") == b"foo");
}

#[test]
fn utf8_pipeline()
{
	let i = b"foo#\xffx\nbar\xff#x\nbaz#x\nzot\n";
	let mut i = ByteStringLines::new(i).map(truncate_line_hash).filter_map(filter_map_utf8);
	assert!(i.next() == Some("foo"));
	assert!(i.next() == Some("baz"));
	assert!(i.next() == Some("zot"));
	assert!(i.next() == None);
}
