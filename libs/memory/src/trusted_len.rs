
///Trait that marks that the trait logically encapsulates byte array of constant length.
///
/// # Safety:
///
///The implementations of this trait must ensure that every call onto `as_byte_slice()` method returns slice of the
///same length. However, the contents of the slice, and even its memory address may vary.
pub unsafe trait TrustedInvariantLength
{
	///Get slice.
	fn as_byte_slice(&self) -> &[u8];
}


unsafe impl TrustedInvariantLength for [u8]
{
	fn as_byte_slice(&self) -> &[u8] { self }
}

unsafe impl<'a> TrustedInvariantLength for &'a [u8]
{
	fn as_byte_slice(&self) -> &[u8] { self }
}

unsafe impl<const N: usize> TrustedInvariantLength for [u8;N]
{
	fn as_byte_slice(&self) -> &[u8] { self.as_ref() }
}

unsafe impl<'a,const N: usize> TrustedInvariantLength for &'a [u8;N]
{
	fn as_byte_slice(&self) -> &[u8] { self.as_ref() }
}

unsafe impl TrustedInvariantLength for str
{
	fn as_byte_slice(&self) -> &[u8] { self.as_bytes() }
}

unsafe impl<'a> TrustedInvariantLength for &'a str
{
	fn as_byte_slice(&self) -> &[u8] { self.as_bytes() }
}

//This is used for some testing.
unsafe impl TrustedInvariantLength for (u16, &'static str)
{
	fn as_byte_slice(&self) -> &[u8] { self.1.as_bytes() }
}
