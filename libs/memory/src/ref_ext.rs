use crate::nonnull_ext::NonNull;
use crate::nonnull_ext::NonNullMut;


///Extension to owned objects.
///
///This adds a few additional methods to owned objects.
pub unsafe trait ObjectExt<T>
{
	///Get pointer to object.
	fn pointer_to(&self) -> *const T where T: Sized;
	///Get mutable pointer to object.
	fn pointer_to_mut(&mut self) -> *mut T where T: Sized;
	///Get pointer to object.
	fn nonnull_to(&self) -> NonNull<T> where T: Sized;
	///Get mutable pointer to object.
	fn nonnull_to_mut(&mut self) -> NonNullMut<T> where T: Sized;
}

unsafe impl<T> ObjectExt<T> for T
{
	fn pointer_to(&self) -> *const T where T: Sized { self as *const T }
	fn pointer_to_mut(&mut self) -> *mut T where T: Sized { self as *mut T }
	fn nonnull_to(&self) -> NonNull<T> where T: Sized
	{
		unsafe{NonNull::new_unchecked(self.pointer_to())}
	}
	fn nonnull_to_mut(&mut self) -> NonNullMut<T> where T: Sized
	{
		unsafe{NonNullMut::new_unchecked(self.pointer_to_mut())}
	}
}
