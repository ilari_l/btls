use crate::NonNull;
use crate::NonNullMut;

///Trait on slices and arrays.
pub trait SliceArrayExt<T>
{
	///Get base address as non-NULL ptr.
	fn as_nonnull_ptr(&mut self) -> NonNullMut<T>;
	///Get base address as non-NULL ptr (read-only)
	fn as_nonnull_ptr_c(&self) -> NonNull<T>;
}

impl<T> SliceArrayExt<T> for [T]
{
	fn as_nonnull_ptr(&mut self) -> NonNullMut<T> { unsafe{NonNullMut::new_unchecked(self.as_mut_ptr())} }
	fn as_nonnull_ptr_c(&self) -> NonNull<T> { unsafe{NonNull::new_unchecked(self.as_ptr())} }
}

impl<T,const N: usize> SliceArrayExt<T> for [T;N]
{
	fn as_nonnull_ptr(&mut self) -> NonNullMut<T> { unsafe{NonNullMut::new_unchecked(self.as_mut_ptr())} }
	fn as_nonnull_ptr_c(&self) -> NonNull<T> { unsafe{NonNull::new_unchecked(self.as_ptr())} }
}
