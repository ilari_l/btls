use crate::Compact;
use crate::NonNull;
use crate::NonNullMut;
use crate::ObjectExt;
use btls_aux_fail::fail_if_none;
use core::cmp::min;
use core::mem::MaybeUninit;
use core::mem::size_of;
use core::ptr::swap as ptr_swap;


///Small vector stored on stack.
pub struct SmallVec<T:Copy+Sized,const N: usize>
{
	base: [MaybeUninit<T>;N],
	len: usize
}

impl<T:Copy+Sized,const N: usize> Copy for SmallVec<T,N> {}

impl<T:Copy+Sized,const N: usize> Clone for SmallVec<T,N>
{
	fn clone(&self) -> Self
	{ unsafe {
		let mut ret = SmallVec::<T,N>::new();
		let src = NonNull::new_slice_base_uninit(&self.base);
		let dst = NonNullMut::new_slice_base_uninit(&mut ret.base);
		dst.copy_nonoverlapping(src, self.len);		//Copy the initialized elements.
		ret.len = self.len;				//Copy has same number of elements.
		ret
	}}
}

impl<T:Copy+Sized,const N: usize> SmallVec<T,N>
{
	///Create new vector.
	pub fn new() -> SmallVec<T,N>
	{ unsafe {
		//MaybeUninit<T> can not be copied, so do this bit roundabout way.
		let mut ret: MaybeUninit<SmallVec<T,N>> = MaybeUninit::uninit();
		//Write length.
		(*ret.as_mut_ptr()).len = 0;
		//The first field may be left uninitialized, as it is MaybeUninit.
		ret.assume_init()
	}}
	///Get length of vector.
	pub fn len(&self) -> usize { self.len }
	///Push element.
	///
	///The parameter `elem` gives the element to insert.
	///
	///The number of initialized elements increases by 1, and amount of uninitialized elements decreases by 1.
	///
	///If the element does not fit, returns `None`. Otherwise returns number of elements inserted (always 1).
	pub fn push(&mut self, elem: T) -> Option<usize>
	{
		fail_if_none!(self.len >= N);
		let base = NonNullMut::new_slice_base_uninit(&mut self.base);
		//By above check, self.len < N, and by definition self.base has N writable elements. Therefore the
		//write is in bounds. Furthermore, the elements are Copy, so never need dropping.
		unsafe{base.add(self.len).write(elem)};
		self.len += 1;
		Some(1)
	}
	///Push slice of elements.
	///
	///The parameter `elems` gives the elements to insert.
	///
	///The number of initialized elements increases by number of elements given, and amount of uninitialized
	///elements decreases by amount of elements given.
	///
	///If the elements do not fit, returns `None`. Otherwise returns number of elements inserted .
	pub fn extend(&mut self, elems: &[T]) -> Option<usize>
	{
		let elems_len = elems.len();
		let newlen = self.len.checked_add(elems_len)?;
		fail_if_none!(newlen > N);
		let elems_ptr = NonNull::new_slice_base(elems);
		let tgtarray = NonNullMut::new_slice_base_uninit(&mut self.base);
		//By definition, elems_ptr has elems_len readable and initialized elements. Therefore read is in
		//bounds.
		//self.len + elems_len <= N by above check. Therefore write is in bounds.
		//The buffers can not overlap, since one is &[T].
		unsafe{tgtarray.add(self.len).copy_nonoverlapping(elems_ptr, elems_len)};
		self.len = newlen;
		Some(elems.len())
	}
	///Push as many copyable elements as possible from slice.
	///
	///Like [`extend()`], but does not fail if the elements to insert do not fit. Instead, as many (first) elements
	///as fits are inserted, and rest are discarded.
	///
	///Returns number of elements inserted.
	///
	///[`extend()`]:#method.extend
	pub fn extend_fitting(&mut self, elems: &[T]) -> usize
	{
		let amt = min(N - self.len, elems.len());
		let elems_ptr = NonNull::new_slice_base(elems);
		let tgtarray = NonNullMut::new_slice_base_uninit(&mut self.base);
		//amt <= elems.len(), so source is in range.
		//self.len + amt <= self.len + (N - self.len) = N, so target is in range.
		//The elements can not overlap because elems is &[T].
		unsafe{tgtarray.add(self.len).copy_nonoverlapping(elems_ptr, amt)};
		self.len += amt;
		amt
	}
	///Read individual element.
	///
	///The parameter `idx` gives the index of element to read.
	pub fn get_value(&self, idx: usize) -> Option<T>
	{
		let base = NonNull::new_slice_base_uninit(&self.base);
		if idx < self.len {
			//The index is in-bounds.
			Some(unsafe{base.add(idx).read()})
		} else {
			//Not in bounds.
			None
		}
	}
	///Swap individual element.
	///
	///The parameter `idx` gives the index of element to read/write.
	///
	///The parameter `val` gives the element to write.
	///
	///Depending on the index, there are four cases:
	///
	/// 1. The index points to initialized part: The value given is written to given index, and the old value
	///that was in the slot is returned.
	/// 1. The index points one past initialized part: The value given is written to given index, the size of
	///initialized part is increased by 1, and `None` is returned.
	/// 1. The index points to uninitialized part, but not the first uninitialized slot: The value is written
	///into the given index.
	/// 1. The index does not point into the vector at all: The value given is returned with no further actions.
	pub fn swap_element(&mut self, idx: usize, mut val: T) -> Option<T>
	{
		let tgtarray = NonNullMut::new_slice_base_uninit(&mut self.base);
		if idx < self.len {
			//The write is to initialized part. Swap the old value and val, and then return val.
			//Both arguments are valid for read/write, and aligned.
			unsafe {
				ptr_swap(tgtarray.add(idx).to_raw(), val.pointer_to_mut());
			}
			return Some(val);
		} else if idx < N {
			//The write is not to initialized part, but still fits.
			//The index is in bounds (<N).
			unsafe{tgtarray.add(idx).write(val)};
			//If index is on boundary, extend boundary.
			if idx == self.len { self.len += 1; }
			return None;
		} else {
			//The write does not fit at all.
			Some(val)
		}
	}
	///Get the initialized part as slice.
	pub fn as_inner(&self) -> &[T]
	{
		//The initialize part is self.len elements, starting at self.base
		unsafe{NonNull::new_slice_base_uninit(&self.base).make_slice(self.len)}
	}
	///Return capacity of the vector (that is, sum of sizes of initialized and uninitialized parts).
	pub fn capacity(&self) -> usize { N }
	///Return the remaining capacity of the vector (that is, size of uninitialized part).
	pub fn remaining_capacity(&self) -> usize { N - self.len }
	///Get pointer.
	pub fn as_ptr(&self) -> *const T { NonNull::new_slice_base_uninit(&self.base).to_raw() }
	///Get pointer to unfilled part.
	///
	///There are [`remaining_capacity()`] wriable, but not necressarily initialized elements in the returned
	///buffer.
	///
	///[`remaining_capacity()`]:#method.remaining_capacity
	pub fn unfilled_pointer(&mut self) -> *mut T { self.unfilled_nonnull().to_raw() }
	///Get pointer to unfilled part.
	///
	///Like [`unfilled_pointer()`], but the pointer is `NonNullMut<T>` instead of `*mut T`.
	///
	///[`unfilled_pointer()`]:#method.unfilled_pointer
	pub fn unfilled_nonnull(&mut self) -> NonNullMut<T>
	{
		unsafe{NonNullMut::new_slice_base_uninit(&mut self.base).add(self.len)}
	}
	///Get slice to unfilled part.
	///
	///This is like [`unfilled_pointer()`], but it returns a slice instead of pointer.
	///
	///[`unfilled_pointer()`]:#method.unfilled_pointer
	pub fn unfilled_slice(&mut self) -> &mut [MaybeUninit<T>]
	{
		let len = self.remaining_capacity();
		unsafe{self.unfilled_nonnull().cast::<MaybeUninit<T>>().make_slice_mut(len)}
	}
	///Assume elements initialized.
	///
	///The parameter `n` gives number of elements to assume initialized.
	///
	///The size of initialized part grows by specified amount, and size of uninitialized part shrinks by the
	///specified amount.
	///
	///# Safety:
	///
	/// * The elements must have been initialized.
	/// * The size of uninitialized part must not go negative.
	pub unsafe fn assume_init(&mut self, n: usize)
	{
		let new_len = self.len + n;
		debug_assert!(new_len <= N);
		self.len = new_len;
	}
	///Clear all data.
	///
	///This logically makes all initialized elements be uninitialized.
	pub fn clear(&mut self) { self.len = 0; }
	///Is full?
	///
	///The buffer is deemed full if there are no uninitialized elements.
	pub fn is_full(&self) -> bool { self.len >= N }
	///Unwrap and return the raw inner array.
	pub fn into_inner(self) -> [MaybeUninit<T>;N] { self.base }
}

impl<const N: usize> SmallVec<u8,N>
{
	///Push something compact.
	///
	///The parameter `c` gives the compact object to push. If this does not fit, returns `None`.
	///
	///If the data fits, returns the number of bytes pushed.
	pub fn write_compact<C:Compact>(&mut self, c: C) -> Option<usize>
	{
		let n = size_of::<C>();
		let newlen = self.len.checked_add(n)?;
		fail_if_none!(newlen > N);
		//self.len + size_of::<C>() <= N.
		unsafe{self.unfilled_nonnull().cast::<C>().write_unaligned(c)};
		self.len = newlen;
		Some(n)
	}
}

/*
	///Read individual element.
	///
	///The parameter `idx` gives the index to read. If the index points outside initialized part, returns
	///`None`.
	pub fn get(&self, idx: usize) -> Option<&T>
	{
		fail_if_none!(idx >= self.len);
		unsafe{transmute(self.base.add(idx))}
	}
	///Read individual element.
	///
	///Like [`get()`] but clones the returned element instead of returning a reference.
	///
	///[`get()`]:#method.get
	pub fn get_value(&self, idx: usize) -> Option<T> where T: Clone { self.get(idx).map(Clone::clone) }
	///Push slice of elements.
	///
	///This is like [`extend()`], but with different return value.
	///
	///[`extend()`]:#method.extend
	pub fn write_slice(&mut self, elems: &[T]) -> Option<()> where T: Copy
	{
		self.extend(elems).map(|_|())
	}
}

impl<'a> BackedVector<'a,u8>
{
	///Write zero bytes.
	///
	///The parameter `n` gives number of zeroes to write.
	///
	///If the data fits, returns `Some(())`, otherwise returns `None`.
	pub fn write_zeros(&mut self, n: usize) -> Option<()>
	{
		let newlen = self.len.checked_add(n)?;
		fail_if_none!(newlen > self.capacity);
		//self.len + n <= self.capacity.
		unsafe{self.base.add(self.len).write_bytes(0, n)};
		self.len = newlen;
		Some(())
	}
}

*/
