use crate::misc::asciiz_pointer;
use crate::misc::asciiz_strlen;
use crate::transmute_ptr::RawPointerFrom;
use crate::transmute_ptr::RawPointerTo;
use core::marker::PhantomData;
use core::mem::MaybeUninit;
use core::ptr::NonNull as _NonNull;
use core::ptr::copy_nonoverlapping;
use core::slice::from_raw_parts;
use core::slice::from_raw_parts_mut;
use core::str::from_utf8_unchecked;

macro_rules! common_methods
{
	($clazz:ident) => {
		//Manually impl these to get rid of Copy and Clone bounds.
		impl<T> Copy for $clazz<T> {}
		impl<T> Clone for $clazz<T> { fn clone(&self) -> Self { *self }}
		impl<T> $clazz<T>
		{
			///Get segment offset.
			///
			///The returned address is not necressarily full memory address, it is offset in segment,
			///which may be 0 even for non-NULL pointer. No object may cross segment boundary.
			///
			///If there is no segmentation, offset is always nonzero and is the complete memory address.
			///It is not possible to turn the address back into pointer.
			pub fn segment_offset(self) -> usize { self.0.as_ptr() as usize }
			///Get address.
			///
			///It is not possible to turn the address back into pointer.
			///
			/// # Return value:
			///
			///The address.
			pub fn addr(self) -> usize { self.0.as_ptr() as usize }
			///Dereference pointer.
			///
			///The value is copied, so if the value has `Drop` impl or glue, either the read value
			///must be forgotten, or the pointed-to value must be afterwards treated as uninitialized.
			///
			///# Safety:
			///
			/// * The pointer must point to a `T`.
			/// * The pointer must be aligned.
			/// * The pointed `T` must be valid.
			/// * This must not be used to cause double-drops.
			pub unsafe fn read(self) -> T { unsafe{self.0.as_ptr().read()} }
			///Dereference potentially unaligned pointer.
			///
			///Like [`read()`], but the pointer does not have to be aligned.
			///
			///[`read()`]:#method.read
			pub unsafe fn read_unaligned(self) -> T { unsafe{self.0.as_ptr().read_unaligned()} }
			///Dereference volatile pointer.
			///
			///Like [`read()`], but the pointer access is volatile.
			///
			///[`read()`]:#method.read
			pub unsafe fn read_volatile(self) -> T { unsafe{self.0.as_ptr().read_volatile()} }
			///Increment pointer.
			///
			///The parameter `amt` is the number of elements to increment the pointer.
			///
			///# Safety:
			///
			///The pointer must point to within object, or one past end of object after the move.
			pub unsafe fn add(self, amt: usize) -> Self
			{	
				let r = unsafe{_NonNull::new_unchecked(self.0.as_ptr().add(amt))};
				$clazz(r, PhantomData)
			}
			///Decrement pointer.
			///
			///The parameter `amt` is the number of elements to decrement the pointer.
			///
			///# Safety:
			///
			///The pointer must point to within object, or one past end of object after the move.
			pub unsafe fn sub(self, amt: usize) -> Self
			{
				let r = unsafe{_NonNull::new_unchecked(self.0.as_ptr().sub(amt))};
				$clazz(r, PhantomData)
			}
			///Adjust pointer.
			///
			///The parameter `amt` is the number of elements to adjust the pointer.
			///
			///# Safety:
			///
			///The pointer must point to within object, or one past end of object after the move.
			pub unsafe fn offset(self, amt: isize) -> Self
			{
				let r = unsafe{_NonNull::new_unchecked(self.0.as_ptr().offset(amt))};
				$clazz(r, PhantomData)
			}
			///Increment pointer.
			///
			///Like [`add()`], but instead stores the result back to `self`.
			///
			///[`add()`]:#method.add
			pub unsafe fn add_assign(&mut self, amt: usize) { *self = unsafe{(*self).add(amt)}; }
			///Decrement pointer.
			///
			///Like [`sub()`], but instead stores the result back to `self`.
			///
			///[`sub()`]:#method.sub
			pub unsafe fn sub_assign(&mut self, amt: usize) { *self = unsafe{(*self).sub(amt)}; }
			///Adjust pointer.
			///
			///Like [`offset()`], but instead stores the result back to `self`.
			///
			///[`offset()`]:#method.offset
			pub unsafe fn offset_assign(&mut self, amt: isize) { *self = unsafe{(*self).offset(amt)}; }
			///Make slice with given pointer as base.
			///
			///The parameter `amt` is the number of elements to place in the slice.
			///
			///# Safety:
			///
			/// * The pointer must be aligned and point to indicated number of valid `T`s.
			/// * The memory must not be accessed after being freed.
			pub unsafe fn make_slice<'a>(self, amt: usize) -> &'a [T]
			{
				unsafe{from_raw_parts(self.0.as_ptr(), amt)}
			}
			///Cast to pointer to `U`.
			pub fn cast<U>(self) -> $clazz<U>
			{
				let r = unsafe{_NonNull::new_unchecked(self.0.as_ptr().cast::<U>())};
				$clazz(r, PhantomData)
			}
			///Return a reference.
			///
			///# Safety:
			///
			/// * The pointer must point to a valid element of `T`.
			/// * The pointer must be aligned.
			/// * The pointer must not be used after object is freed.
			pub unsafe fn as_ref<'a>(&'a self) -> &'a T { unsafe{self.0.as_ref()} }
		}
	}
}

///Constant non-NULL pointer.
///
///It is guaranteed that `NonNull<T>` and `Option<NonNull<T>>` have the same layout as `*const T`, and furthermore
///that `NULL` represents `None`.
///
///This is covariant in `T`, just like `*const T`.
#[repr(transparent)]
#[derive(PartialEq)]
pub struct NonNull<T>(_NonNull<T>, PhantomData<*const T>);


common_methods!(NonNull);

impl<T> NonNull<T>
{
	///Create from raw pointer.
	///
	///The parameter `p` gives the original raw pointer to convert.
	///
	///Returns pointer to the same location as given pointer. If the pointer given is `NULL`, returns `None`.
	pub fn new(p: *const T) -> Option<NonNull<T>>
	{
		if !p.is_null() { Some(unsafe{Self::new_unchecked(p)}) } else { None }
	}
	///Create from raw pointer.
	///
	///The parameter `p` gives the original raw pointer to convert. It MUST be non-NULL.
	///
	///Returns pointer to the same location as given pointer. 
	///
	///# Safety:
	///
	///The given pointer SHALL be non-NULL.
	pub unsafe fn new_unchecked(p: *const T) -> NonNull<T>
	{
		NonNull(unsafe{_NonNull::new_unchecked(p as *mut T)}, PhantomData)
	}
	///Create from slice base.
	///
	///The parameter `s` gives the slice to create the raw pointer to.
	pub fn new_slice_base(p: &[T]) -> NonNull<T>
	{
		unsafe{Self::new_unchecked(p.as_ptr())}
	}
	///Create from uninitialized slice base.
	///
	///The parameter `s` gives the slice to create the raw pointer to.
	pub fn new_slice_base_uninit(p: &[MaybeUninit<T>]) -> NonNull<T>
	{
		unsafe{Self::new_unchecked(p.as_ptr().cast::<T>())}
	}
	///Get as raw pointer.
	pub fn to_raw(self) -> *const T { self.0.as_ptr() as *const T }
}

macro_rules! nonnull_bytes_specials
{
	($x:ident) => {
		impl NonNull<$x>
		{
			///Get length of NUL-terminated string.
			pub unsafe fn strlen(self) -> usize { unsafe{asciiz_strlen(self)} }
			///Get NUL-terminated string as slice.
			pub unsafe fn cstr_as_slice<'a>(self) -> &'a [u8] { unsafe{asciiz_pointer(self)} }
			///Get NUL-terminated string as string.
			pub unsafe fn cstr_as_str<'a>(self) -> &'a str
			{
				unsafe{from_utf8_unchecked(self.cstr_as_slice())}
			}
		}
	}
}

nonnull_bytes_specials!(i8);
nonnull_bytes_specials!(u8);


unsafe fn __strlen(x: NonNull<u8>) -> usize
{
	let mut i = 0;
	while unsafe{x.add(i).read()} != 0 { i += 1; }
	i
}

///Mutable non-NULL pointer.
///
///It is guaranteed that `NonNullMut<T>` and `Option<NonNullMut<T>>` have the same layout as `*mut T`, and
///furthermore that `NULL` represents `None`.
///
///This is invariant in `T`, just like `*mut T`.
#[repr(transparent)]
#[derive(PartialEq)]
pub struct NonNullMut<T>(_NonNull<T>, PhantomData<*mut T>);

common_methods!(NonNullMut);

impl<T> NonNullMut<T>
{
	///Create a pointer pointing to some invalid place.
	///
	///The pointer does not point to anything accessable. Nevertheless, it is aligned.
	pub fn dangling() -> NonNullMut<T>
	{
		NonNullMut(_NonNull::dangling(), PhantomData)
	}
	///Create from raw pointer.
	///
	///The parameter `p` gives the original raw pointer to convert.
	///
	///Returns pointer to the same location as given pointer. If the pointer given is `NULL`, returns `None`.
	pub fn new(p: *mut T) -> Option<NonNullMut<T>>
	{
		if !p.is_null() { Some(unsafe{Self::new_unchecked(p)}) } else { None }
	}
	///Create from raw pointer.
	///
	///The parameter `p` gives the original raw pointer to convert. It MUST be non-NULL.
	///
	///Returns pointer to the same location as given pointer. 
	///
	///# Safety:
	///
	///The given pointer SHALL be non-NULL.
	pub unsafe fn new_unchecked(p: *mut T) -> NonNullMut<T>
	{
		NonNullMut(unsafe{_NonNull::new_unchecked(p as *mut T)}, PhantomData)
	}
	///Create from slice base.
	///
	///The parameter `s` gives the slice to create the raw pointer to.
	pub fn new_slice_base(p: &mut [T]) -> NonNullMut<T>
	{
		unsafe{Self::new_unchecked(p.as_mut_ptr())}
	}
	///Create from uninitialized slice base.
	///
	///The parameter `s` gives the slice to create the raw pointer to.
	pub fn new_slice_base_uninit(p: &mut [MaybeUninit<T>]) -> NonNullMut<T>
	{
		unsafe{Self::new_unchecked(p.as_mut_ptr().cast::<T>())}
	}
	///Write to pointed to location.
	///
	///Prior to overwriting the value, the old value at pointed-to location is forgotten.
	///
	///The parameter `val` gives the value to write. This value is not dropped.
	///
	///# Safety:
	///
	/// * The pointer must point to writable memory.
	/// * The pointer must be aligned.
	pub unsafe fn write(self, val: T) { unsafe{self.0.as_ptr().write(val)} }
	///Write to unaligned pointed to location.
	///
	///Like [`write()`], but the pointer does not have to be aligned.
	///
	///[`write()`]:#method.write
	pub unsafe fn write_unaligned(self, val: T) { unsafe{self.0.as_ptr().write_unaligned(val)} }
	///Write to volatile pointed to location.
	///
	///Like [`write()`], but the pointer access is volatile.
	///
	///[`write()`]:#method.write
	pub unsafe fn write_volatile(self, val: T) { unsafe{self.0.as_ptr().write_volatile(val)} }
	///Write contents of slice to pointed to memory.
	///
	///The old values are forgotten before overwriting.
	///
	///The parameter `arr` gives the source array to take the values from.
	///
	///# Safety:
	///
	/// * The pointer must have enough writable elements to write the contents of the slice to.
	/// * The pointer must be aligned.
	pub unsafe fn write_slice(self, arr: &[T]) where T: Copy
	{
		unsafe {
			let clen = arr.len();
			let sptr: *const T = arr.as_ptr();
			let dptr: *mut T = self.0.as_ptr();
			copy_nonoverlapping(sptr, dptr, clen);
		}
	}
	///Write contents of array to pointed to memory.
	///
	///Like [`write_slice()`], but with array instead of slice.
	///
	///[`write_slice()`]:#method.write_slice
	pub unsafe fn write_array<const N: usize>(self, arr: &[T;N]) where T: Copy
	{
		unsafe {
			let sptr: *const T = arr.as_ptr();
			let dptr: *mut T = self.0.as_ptr();
			copy_nonoverlapping(sptr, dptr, N);
		}
	}
	///Fill pointed-to location with given byte.
	///
	///The parameter `val` gives the byte to fill with.
	///
	///The parameter `count` gives the number of elements to fill.
	///
	///Any old overwritten elements are forgotten.
	///
	///# Safety:
	///
	/// * There must be enough writable space at pointer to write the elements to.
	/// * The pointer must be aligned.
	pub unsafe fn write_bytes(self, val: u8, count: usize)
	{
		unsafe{self.0.as_ptr().write_bytes(val, count)}
	}
	///Write to pointed location and advance
	///
	///Like [`write()`], but the pointer is advanced after write.
	///
	///[`write()`]:#method.write
	pub unsafe fn write_adv(&mut self, val: T)
	{
		unsafe {
			self.write(val);
			self.add_assign(1);
		}
	}
	///Write to unaligned pointed location and advance
	///
	///Like [`write_unaligned()`], but the pointer is advanced after write.
	///
	///[`write_unaligned()`]:#method.write_unaligned
	pub unsafe fn write_unaligned_adv(&mut self, val: T)
	{
		unsafe {
			self.write_unaligned(val);
			self.add_assign(1);
		}
	}
	///Write contents of slice to pointed location and advance
	///
	///Like [`write_slice()`], but the pointer is advanced after write.
	///
	///[`write_slice()`]:#method.write_slice
	pub unsafe fn write_slice_adv(&mut self, arr: &[T]) where T: Copy
	{
		unsafe {
			self.write_slice(arr);
			self.add_assign(arr.len());
		}
	}
	///Write contents of array to pointed location and advance
	///
	///Like [`write_array()`], but the pointer is advanced after write.
	///
	///[`write_array()`]:#method.write_array
	pub unsafe fn write_array_adv<const N: usize>(&mut self, arr: &[T;N]) where T: Copy
	{
		unsafe {
			self.write_array(arr);
			self.add_assign(N);
		}
	}
	///Fill pointed location with given byte and advance
	///
	///Like [`write_bytes()`], but the pointer is advanced after write.
	///
	///[`write_bytes()`]:#method.write_bytes
	pub unsafe fn write_bytes_adv(&mut self, val: u8, count: usize)
	{
		unsafe {
			self.write_bytes(val, count);
			self.add_assign(count);
		}
	}
	///Make mutable slice with given pointer as base.
	///
	///Like [`make_slice()`], but with mutable slice.
	///
	///[`make_slice()`]:#method.make_slice
	pub unsafe fn make_slice_mut<'a>(self, amt: usize) -> &'a mut [T]
	{
		unsafe{from_raw_parts_mut(self.0.as_ptr(), amt)}
	}
	///Copy non-overlapping memory to this pointer.
	///
	///The parameter `src` gives the source buffer to copy.
	///
	///The parameter `count` gives number of elements to copy.
	///
	///Any old overwritten elements are forgotten.
	///
	///# Safety:
	///
	/// * The source must be aligned and have enough readable elements.
	/// * The destination must be aligned and have enough writable elements.
	/// * The source and destination must not overlap.
	pub unsafe fn copy_nonoverlapping(self, src: NonNull<T>, count: usize)
	{
		unsafe {
			let sptr = src.to_raw();
			let dptr = self.0.as_ptr();
			copy_nonoverlapping(sptr, dptr, count);
		}
	}
	///Cast to const pointer.
	pub fn to_const(self) -> NonNull<T> { NonNull(self.0, PhantomData) }
	///Get as raw pointer.
	pub fn to_raw(self) -> *mut T { self.0.as_ptr() }
	///Return a mutable reference.
	///
	///# Safety:
	///
	/// * The pointer must point to a valid element of `T`.
	/// * The pointer must be aligned.
	/// * The pointer must not be used after object is freed.
	/// * There can be only one outstanding mutable reference at a time.
	pub unsafe fn as_mut<'a>(&'a mut self) -> &'a mut T { unsafe{self.0.as_mut()} }
}


unsafe impl<T> RawPointerFrom for NonNull<T>
{
}

unsafe impl<T> RawPointerFrom for NonNullMut<T>
{
}

unsafe impl<T> RawPointerFrom for Option<NonNull<T>>
{
}

unsafe impl<T> RawPointerFrom for Option<NonNullMut<T>>
{
}

//Due to the NULL hole, NonNull<T> and NonNullMut<T> can not be RawPointerTo.
unsafe impl<T> RawPointerTo for Option<NonNull<T>>
{
}

unsafe impl<T> RawPointerTo for Option<NonNullMut<T>>
{
}

