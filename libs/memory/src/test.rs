use super::*;
use btls_aux_fail::f_continue;
use std::char::from_u32;
use std::string::String;
use std::string::ToString;
use std::vec::Vec;


#[test]
fn copy_maximum_lt()
{
	let mut a = [0;3];
	let b = [1,6,16,36,42];
	assert!(copy_maximum(&mut a, &b) == 3);
	assert_eq!(a, [1,6,16]);
}

#[test]
fn copy_maximum_eq()
{
	let mut a = [0;5];
	let b = [1,6,16,36,42];
	assert!(copy_maximum(&mut a, &b) == 5);
	assert_eq!(a, [1,6,16,36,42]);
}

#[test]
fn copy_maximum_gt()
{
	let mut a = [0;5];
	let b = [1,6,16];
	assert!(copy_maximum(&mut a, &b) == 3);
	assert_eq!(a, [1,6,16,0,0]);
}

#[test]
fn utf8_prefix_empty()
{
	let b = b"\xffabc";
	assert_eq!(from_utf8_prefix(b), "");
}

#[test]
fn utf8_prefix_nontrivial()
{
	let b = b"\xc2\x80\xffabc";
	assert_eq!(from_utf8_prefix(b), "\u{80}");
}

#[test]
fn utf8_prefix_nontrivial2()
{
	let b = b"\xc2\x80x\xffabc";
	assert_eq!(from_utf8_prefix(b), "\u{80}x");
}

#[test]
fn utf8_prefix_all()
{
	let b = b"\xc2\x80xyzzy";
	assert_eq!(from_utf8_prefix(b), "\u{80}xyzzy");
}

#[test]
fn x_nth_bit()
{
	let b = [0x18,0xfd,0x92,0x52];
	let c = u32::from_le_bytes(b) as u64;
	let mut d = 0;
	for i in 0..64 { d |= (nth_bit(&b, i) as u64) << i; }
	assert_eq!(c, d);
}

#[test]
fn slice_strip_many()
{
	let b = [1,3,6,36,42];
	assert_eq!(slice_strip_first(&b), &[3,6,36,42]);
}

#[test]
fn slice_strip_one()
{
	let b = [42];
	assert_eq!(slice_strip_first(&b), &[]);
}

#[test]
fn slice_strip_none()
{
	let b: [u32;0] = [];
	assert_eq!(slice_strip_first(&b), &[]);
}

#[test]
fn map_subarray_offset()
{
	let b = [0,0,0x18,0xfd,0x92,0x52,0,0];
	let c = map_subarray(&b, 2, u32::from_be_bytes).expect("Should be valid");
	assert_eq!(c, 0x18fd9252);
}

#[test]
fn map_subarray_oor()
{
	let b = [0,0,0x18,0xfd,0x92,0x52,0,0];
	let c = map_subarray(&b, 5, u32::from_be_bytes);
	assert!(c.is_none());
}


#[test]
fn bound_buffer()
{
	assert!(bound_buffer_size(32767u16) == 32767);
	assert!(bound_buffer_size(usize::MAX) == isize::MAX as usize);
}

#[test]
fn test_str_first_tail()
{
	assert_eq!(str_first_tail(""), None);
	assert_eq!(str_first_tail("\u{0}foo"), Some(('\u{0}', "foo")));
	assert_eq!(str_first_tail("\u{7f}foo"), Some(('\u{7f}', "foo")));
	assert_eq!(str_first_tail("\u{80}foo"), Some(('\u{80}', "foo")));
	assert_eq!(str_first_tail("\u{7ff}foo"), Some(('\u{7ff}', "foo")));
	assert_eq!(str_first_tail("\u{800}foo"), Some(('\u{800}', "foo")));
	assert_eq!(str_first_tail("\u{d7ff}foo"), Some(('\u{d7ff}', "foo")));
	assert_eq!(str_first_tail("\u{e000}foo"), Some(('\u{e000}', "foo")));
	assert_eq!(str_first_tail("\u{ffff}foo"), Some(('\u{ffff}', "foo")));
	assert_eq!(str_first_tail("\u{10000}foo"), Some(('\u{10000}', "foo")));
	assert_eq!(str_first_tail("\u{10ffff}foo"), Some(('\u{10ffff}', "foo")));
}

#[test]
fn escape_string_scan()
{
	for i in 0..0x11000u32 {
		let c = f_continue!(from_u32(i));
		let src = c.to_string();
		let dst = EscapeString(&src).to_string();
		let escape = match i {
			7 => "\\a".to_string(),
			8 => "\\b".to_string(),
			9 => "\\t".to_string(),
			10 => "\\n".to_string(),
			11 => "\\v".to_string(),
			12 => "\\f".to_string(),
			13 => "\\r".to_string(),
			34 => "\\\"".to_string(),
			39 => "\\\'".to_string(),
			92 => "\\\\".to_string(),
			0..=31|127..=159|0x202A..=0x202E|0x2066..=0x2069|0xFFFE|0xFFFF => format!("\\u{{{i:x}}}"),
			_ => c.to_string(),
		};
		assert_eq!(dst, escape);
	}
}

#[test]
fn test_escape_bytes()
{
	let input = b"xxx\x80yyy\xe2\x81\xa6zzz\x7f\x0awww";
	let output = "xxx\\x80yyy\\u{2066}zzz\\u{7f}\\nwww";
	let escaped = EscapeByteString(input).to_string();
	assert_eq!(escaped, output);
}

#[test]
fn test_escape_surrogates()
{
	let input = b"x\xed\xa0\x80y\xed\xbf\xbfz";
	let output = "x\\sd800y\\sdfffz";
	let escaped = EscapeByteString(input).to_string();
	assert_eq!(escaped, output);
}

#[test]
fn test_escape_surrogates2()
{
	let input = b"x\xed\xa0\x81y\xed\xbf\xbez";
	let output = "x\\sd801y\\sdffez";
	let escaped = EscapeByteString(input).to_string();
	assert_eq!(escaped, output);
}

#[test]
fn test_escape_backslash()
{
	let input = b"\\";
	let output = "\\\\";
	let escaped = EscapeByteString(input).to_string();
	assert_eq!(escaped, output);
}

#[test]
fn test_escape_unicode()
{
	let input = "\u{0}\u{1}\u{2}\u{3}\u{4}\u{5}\u{6}\u{e}\u{f}\u{10}\u{11}\u{12}\u{13}\u{14}\
		\u{15}\u{16}\u{17}\u{18}\u{19}\u{1a}\u{1b}\u{1c}\u{1d}\u{1e}\u{1f}\u{7f}\
		\u{80}\u{81}\u{82}\u{83}\u{84}\u{85}\u{86}\u{87}\
		\u{88}\u{89}\u{8a}\u{8b}\u{8c}\u{8d}\u{8e}\u{8f}\
		\u{90}\u{91}\u{92}\u{93}\u{94}\u{95}\u{96}\u{97}\
		\u{98}\u{99}\u{9a}\u{9b}\u{9c}\u{9d}\u{9e}\u{9f}\
		\u{202a}\u{202b}\u{202c}\u{202d}\u{202e}\u{2066}\u{2067}\u{2068}\u{2069}\
		\u{fffe}\u{ffff}";
	let output = "\\u{0}\\u{1}\\u{2}\\u{3}\\u{4}\\u{5}\\u{6}\\u{e}\\u{f}\\u{10}\\u{11}\\u{12}\\u{13}\\u{14}\
		\\u{15}\\u{16}\\u{17}\\u{18}\\u{19}\\u{1a}\\u{1b}\\u{1c}\\u{1d}\\u{1e}\\u{1f}\\u{7f}\
		\\u{80}\\u{81}\\u{82}\\u{83}\\u{84}\\u{85}\\u{86}\\u{87}\
		\\u{88}\\u{89}\\u{8a}\\u{8b}\\u{8c}\\u{8d}\\u{8e}\\u{8f}\
		\\u{90}\\u{91}\\u{92}\\u{93}\\u{94}\\u{95}\\u{96}\\u{97}\
		\\u{98}\\u{99}\\u{9a}\\u{9b}\\u{9c}\\u{9d}\\u{9e}\\u{9f}\
		\\u{202a}\\u{202b}\\u{202c}\\u{202d}\\u{202e}\\u{2066}\\u{2067}\\u{2068}\\u{2069}\
		\\u{fffe}\\u{ffff}";
	let escaped = EscapeString(input).to_string();
	assert_eq!(escaped, output);
}

#[test]
fn test_escape_special()
{
	let input = "\u{7}\u{8}\u{9}\u{a}\u{b}\u{c}\u{d}\u{22}\u{27}\u{5c}";
	let output = "\\a\\b\\t\\n\\v\\f\\r\\\"\\\'\\\\";
	let escaped = EscapeString(input).to_string();
	assert_eq!(escaped, output);
}

#[test]
fn safe_show_string_scan()
{
	for i in 0..0x11000u32 {
		let c = f_continue!(from_u32(i));
		let src = c.to_string();
		let dst = SafeShowString(&src).to_string();
		let escape = match i {
			9 => "\t".to_string(),
			10 => "\n".to_string(),
			13 => "\r".to_string(),
			0..=31|127..=159|0x202A..=0x202E|0x2066..=0x2069|0xFFFE|0xFFFF => format!("ᐸu{i:x}ᐳ"),
			_ => c.to_string(),
		};
		assert_eq!(dst, escape);
	}
}

#[test]
fn test_safe_show_bytes()
{
	let input = b"xxx\x80yyy\xe2\x81\xa6zzz\x7f\x0awww";
	let output = "xxxᐸx80ᐳyyyᐸu2066ᐳzzzᐸu7fᐳ\nwww";
	let escaped = SafeShowByteString(input).to_string();
	assert_eq!(escaped, output);
}

#[test]
fn hexdump_all_bytes()
{
	let mut input = Vec::new();
	let mut output = String::new();
	for i in 0..256u16 {
		input.push(i as u8);
		write!(output, "{i:02x}", i=i as u8).ok();
	}
	let output2 = Hexdump(&input).to_string();
	assert!(output == output2);
}

#[test]
fn hexdump_short()
{
	let input = b"Hello, World!\n";
	let output = "48656c6c6f2c20576f726c64210a";
	let output2 = Hexdump(input).to_string();
	assert!(output == output2);
}
