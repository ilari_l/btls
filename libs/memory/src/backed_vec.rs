use crate::AliasWriteSlice;
use crate::NonNull;
use crate::NonNullMut;
use crate::WriteOnlyArray;
use crate::ref_ext::ObjectExt;
use btls_aux_fail::fail_if_none;
use core::cmp::min;
use core::marker::PhantomData;
use core::mem::MaybeUninit;
use core::mem::size_of;
use core::mem::transmute;
use core::ptr::swap as ptr_swap;


macro_rules! compact_for_int
{
	//The type $x is assumed to be Copy and have no padding.
	($x:ident) => {
		//$x can implement Compact by assemption.
		unsafe impl Compact for $x {}
	}
}

//All Arrays over compact elements are themselves compact.
unsafe impl<T:Compact, const N:usize> Compact for [T;N] {}


///Copy types with no padding.
pub unsafe trait Compact: Copy {}
compact_for_int!(u8);
compact_for_int!(u16);
compact_for_int!(u32);
compact_for_int!(u64);
compact_for_int!(u128);
compact_for_int!(usize);
compact_for_int!(i8);
compact_for_int!(i16);
compact_for_int!(i32);
compact_for_int!(i64);
compact_for_int!(i128);
compact_for_int!(isize);

///Backed vector.
///
///The vector is backed by fixed-size stack buffer.
pub struct BackedVector<'a,T:Sized+'a>
{
	base: NonNullMut<T>,
	len: usize,
	capacity: usize,
	//This is logically a mutable slice of T's. This is needed to make 'a invariant, which it needs to be
	//for soundness.
	phantom: PhantomData<&'a mut T>,
}

impl<'a,T:Sized+'a> BackedVector<'a,T>
{
        ///Create a new vector.
        ///
	/// # Parameters:
	///
	/// * `backing`: The backing storage.
	///
	/// # Return value:
	///
	///The new vector.
	pub fn new_woa(backing: &'a mut (impl WriteOnlyArray<T>+?Sized)) -> BackedVector<'a,T>
	{
		let (base, len) = backing.as_raw_region();
		//By assumption, base has len RW elements, and len >= 0.
		BackedVector { base: base, len: 0, capacity: len, phantom: PhantomData }
	}
	///Create a new vector.
	///
	///The parameter `backing` is the backing memory to use.
	pub fn new(backing: &'a mut [MaybeUninit<T>]) -> BackedVector<'a,T>
	{
		let len = backing.len();
		//as_mut_ptr() always returns non-NULL pointer.
		let base = unsafe{NonNullMut::<T>::new_unchecked(backing.as_mut_ptr().cast())};
		BackedVector {
			base: base,
			len: 0,
			capacity: len,
			phantom: PhantomData,
		}
	}
	///Get the initialized part as slice.
	pub fn into_inner(self) -> &'a [T]
	{
		//The initialize part is self.len elements, starting at self.base
		unsafe{self.base.make_slice(self.len)}
	}
	///Get the initialized part as slice.
	///
	///Unlike [`into_inner()`], this does not consume the vector.
	///
	///[`into_inner()`]:#method.into_inner
	pub fn as_inner(&self) -> &[T]
	{
		//The initialize part is self.len elements, starting at self.base
		unsafe{self.base.make_slice(self.len)}
	}
	///Read individual element.
	///
	///The parameter `idx` gives the index to read. If the index points outside initialized part, returns
	///`None`.
	pub fn get(&self, idx: usize) -> Option<&T>
	{
		fail_if_none!(idx >= self.len);
		unsafe{transmute(self.base.add(idx))}
	}
	///Read individual element.
	///
	///Like [`get()`] but clones the returned element instead of returning a reference.
	///
	///[`get()`]:#method.get
	pub fn get_value(&self, idx: usize) -> Option<T> where T: Clone { self.get(idx).map(Clone::clone) }
	///Swap individual element.
	///
	///The parameter `idx` gives the index of element to read/write.
	///
	///The parameter `val` gives the element to write.
	///
	///Depending on the index, there are four cases:
	///
	/// 1. The index points to initialized part: The value given is written to given index, and the old value
	///that was in the slot is returned.
	/// 1. The index points one past initialized part: The value given is written to given index, the size of
	///initialized part is increased by 1, and `None` is returned.
	/// 1. The index points to uninitialized part, but not the first uninitialized slot: The value is written
	///into the given index.
	/// 1. The index does not point into the vector at all: The value given is returned with no further actions.
	pub fn swap_element(&mut self, idx: usize, mut val: T) -> Option<T>
	{
		if idx < self.len {
			//The write is to initialized part. Swap the old value and val, and then return val.
			unsafe {
				//Both arguments are valid for read/write, and aligned.
				ptr_swap(self.base.add(idx).to_raw(), val.pointer_to_mut());
			}
			return Some(val);
		} else if idx < self.capacity {
			//The write is not to initialized part, but still fits.
			unsafe {
				//The index is in bounds (self.capacity).
				self.base.add(idx).write(val);
			}
			//If index is on boundary, extend boundary.
			if idx == self.len { self.len += 1; }
			return None;
		} else {
			//The write does not fit at all.
			Some(val)
		}
	}
	///Return length of the vector (that is, number of elements in initialized part).
	pub fn len(&self) -> usize { self.len }
	///Return capacity of the vector (that is, sum of sizes of initialized and uninitialized parts).
	pub fn capacity(&self) -> usize { self.capacity }
	///Return the remaining capacity of the vector (that is, size of uninitialized part).
	pub fn remaining_capacity(&self) -> usize { self.capacity - self.len }
	///Get pointer to unfilled part.
	///
	///There are [`remaining_capacity()`] wriable, but not necressarily initialized elements in the returned
	///buffer.
	///
	///[`remaining_capacity()`]:#method.remaining_capacity
	pub fn unfilled_pointer(&mut self) -> *mut T { self.unfilled_nonnull().to_raw() }
	///Get pointer to unfilled part.
	///
	///Like [`unfilled_pointer()`], but the pointer is `NonNullMut<T>` instead of `*mut T`.
	///
	///[`unfilled_pointer()`]:#method.unfilled_pointer
	pub fn unfilled_nonnull(&mut self) -> NonNullMut<T> { unsafe{self.base.add(self.len)} }
	///Get slice to unfilled part.
	///
	///This is like [`unfilled_pointer()`], but it returns a slice instead of pointer.
	///
	///[`unfilled_pointer()`]:#method.unfilled_pointer
	pub fn unfilled_slice(&mut self) -> &mut [MaybeUninit<T>]
	{
		let len = self.remaining_capacity();
		unsafe{self.unfilled_nonnull().cast::<MaybeUninit<T>>().make_slice_mut(len)}
	}
	///Get aliasing write slice to unfilled part.
	///
	/// # Return value:
	///
	///Aliasing write slice.
	pub fn unfilled_aliasing_write<'x>(&'x mut self) -> AliasWriteSlice<'x,T>
	{
		AliasWriteSlice::new_uninit(self.unfilled_slice())
	}
	///Assume elements initialized.
	///
	///The parameter `n` gives number of elements to assume initialized.
	///
	///The size of initialized part grows by specified amount, and size of uninitialized part shrinks by the
	///specified amount.
	///
	///# Safety:
	///
	/// * The elements must have been initialized.
	/// * The size of uninitialized part must not go negative.
	pub unsafe fn assume_init(&mut self, n: usize)
	{
		let new_len = self.len + n;
		debug_assert!(new_len <= self.capacity);
		self.len = new_len;
	}
	///Push element.
	///
	///The parameter `elem` gives the element to insert.
	///
	///The number of initialized elements increases by 1, and amount of uninitialized elements decreases by 1.
	///
	///If the element does not fit, returns `None`. Otherwise returns number of elements inserted (always 1).
	pub fn push(&mut self, elem: T) -> Option<usize>
	{
		fail_if_none!(self.len >= self.capacity);
		unsafe{
			//By above check, self.len < self.capacity, and by definition self.base has self.capacity
			//writable elements. Therefore the write is in bounds. Furthermore, the old element is not
			//initialized, so it must not be dropped.
			self.base.add(self.len).write(elem);
		}
		self.len += 1;
		Some(1)
	}
	///Push slice of elements.
	///
	///The parameter `elems` gives the elements to insert.
	///
	///The number of initialized elements increases by number of elements given, and amount of uninitialized
	///elements decreases by amount of elements given.
	///
	///If the elements do not fit, returns `None`. Otherwise returns number of elements inserted .
	pub fn extend(&mut self, elems: &[T]) -> Option<usize> where T:Copy
	{
		let elems_len = elems.len();
		let newlen = self.len.checked_add(elems_len)?;
		fail_if_none!(newlen > self.capacity);
		unsafe {
			//as_ptr() always gives non-NULL pointer.
			let elems_ptr = NonNull::new_unchecked(elems.as_ptr());
			//By definition, elems.as_ptr() has elems.len() readable and initialized elements.
			//Therefore read is in bounds.
			//self.base has self.capacity writable elements. Therefore self.base.add(self.len) has
			//self.capactity-self.len writable elements. Therefore it must be that
			//elems.len()<=self.capactity-self.len. But this is equivalent to
			//self.len+elems.len()<=self.capacity, which in turn is equivalent to newlen<=self.capacity,
			//which is checked above. Therefore write is in bounds.
			//The buffers can not overlap, since one is &[T].
			self.base.add(self.len).copy_nonoverlapping(elems_ptr, elems_len);
		}
		self.len = newlen;
		Some(elems.len())
	}
	///Push slice of elements.
	///
	///This is like [`extend()`], but with different return value.
	///
	///[`extend()`]:#method.extend
	pub fn write_slice(&mut self, elems: &[T]) -> Option<()> where T: Copy
	{
		self.extend(elems).map(|_|())
	}
	///Push as maly copyable elements as possible from slice.
	///
	///Like [`extend()`], but does not fail if the elements to insert do not fit. Instead, as many (first) elements
	///as fits are inserted, and rest are discarded.
	///
	///Returns number of elements inserted.
	///
	///[`extend()`]:#method.extend
	pub fn extend_fitting(&mut self, elems: &[T]) -> usize where T:Copy
	{
		let amt = min(self.capacity - self.len, elems.len());
		unsafe {
			//as_ptr() always gives non-NULL pointer.
			let elems_ptr = NonNull::new_unchecked(elems.as_ptr());
			//amt <= elems.len(), and elems.as_ptr() has elems.len() readable elements. Therefore the read
			//is in range.
			//self.base+self.len+amt<=self.base+self.len+(self.capacity-self.len)=self.base+self.capacity,
			//and therefore write is in bounds.
			//The buffers can not overlap, since one is &[T].
			self.base.add(self.len).copy_nonoverlapping(elems_ptr, amt);
		}
		self.len += amt;
		amt
	}
	///Clear all data.
	///
	///This logically makes all initialized elements be uninitialized.
	pub fn clear(&mut self) { self.len = 0; }
	///Is full?
	///
	///The buffer is deemed full if there are no uninitialized elements.
	pub fn is_full(&self) -> bool { self.len >= self.capacity }
}

impl<'a> BackedVector<'a,u8>
{
	///Push something compact.
	///
	///The parameter `c` gives the compact object to push. If this does not fit, returns `None`.
	///
	///If the data fits, returns the number of bytes pushed.
	pub fn write_compact<C:Compact>(&mut self, c: C) -> Option<usize>
	{
		let n = size_of::<C>();
		let newlen = self.len.checked_add(n)?;
		fail_if_none!(newlen > self.capacity);
		//self.len + size_of::<C>() <= self.capacity.
		unsafe{self.base.add(self.len).cast::<C>().write_unaligned(c)};
		self.len = newlen;
		Some(n)
	}
	///Write zero bytes.
	///
	///The parameter `n` gives number of zeroes to write.
	///
	///If the data fits, returns `Some(())`, otherwise returns `None`.
	pub fn write_zeros(&mut self, n: usize) -> Option<()>
	{
		let newlen = self.len.checked_add(n)?;
		fail_if_none!(newlen > self.capacity);
		//self.len + n <= self.capacity.
		unsafe{self.base.add(self.len).write_bytes(0, n)};
		self.len = newlen;
		Some(())
	}
}
