//!Various memory-related routines.
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]

#[cfg(test)] #[macro_use] extern crate std;
pub use crate::alias_slice::*;
pub use crate::backed_vec::*;
pub use crate::misc::*;
pub use crate::nonnull_ext::*;
pub use crate::ref_ext::*;
pub use crate::slice_array_ext::*;
pub use crate::small_vec::*;
pub use crate::split_mut::*;
#[allow(deprecated)]
pub use crate::ssplit::{slice_split_at,slice_rsplit_at,slice_split_at_mut,slice_rsplit_at_mut};
pub use crate::ssplit::{split_at,rsplit_at,split_head_tail};
pub use crate::ssplit::{split_array_head,split_array_head_mut,split_array_tail,split_array_tail_mut};
pub use crate::ssplit::{split_attach_first,split_attach_last,Attachment};
pub use crate::ssplit::{strip_prefix,strip_suffix};
pub use crate::ssplit::trim_ascii;
pub use crate::transmute_ptr::*;
pub use crate::trusted_len::TrustedInvariantLength;
use btls_aux_fail::dtry;
use btls_aux_fail::fail_if;
use btls_aux_fail::fail_if_none;
use core::cmp::min;
use core::convert::TryInto;
use core::fmt::Error as FmtError;
use core::fmt::Write as FmtWrite;
use core::hint::unreachable_unchecked;
use core::mem::MaybeUninit;
#[doc(hidden)]		//Only used by one macro.
pub use core::mem::MaybeUninit as __CoreMaybeUninit;
use core::mem::size_of;
use core::mem::transmute;
use core::mem::transmute_copy;
use core::ops::Range;
use core::ptr::copy_nonoverlapping;
use core::slice::from_raw_parts;
use core::str::from_utf8;
use core::str::from_utf8_unchecked;


mod alias_slice;
mod backed_vec;
#[cfg(feature="collections")] mod collections;
mod misc;
mod nonnull_ext;
mod ref_ext;
mod slice_array_ext;
mod small_vec;
mod split_mut;
mod ssplit;
#[cfg(feature="std")] mod stdlib;
mod transmute_ptr;
mod trusted_len;

///Bound unsigned integer to range of `isize`.
///
///The parameter `x` gives the integer to cast.
///
///This casts the specified unsigned integer into `usize`. However, if the numeric value is greater than maximumm
///`isize`, the integer is capped to maximum `isize`.
///
///The results are undefined if the specified value is negative.
pub fn bound_buffer_size<T>(x: T) -> usize where isize: core::convert::TryFrom<T>
{
	use core::convert::TryFrom;
	isize::try_from(x).unwrap_or(isize::MAX) as usize
}

///Copy slice to another if it fits.
///
///The parameter `dst` gives the destination slice to copy to.
///
///The parameter `src` gives the source slice to copy from.
///
///Returns number of elements copied, or `None` if data will not fit.
pub fn copy_if_fits<T:Copy>(dst: &mut [T], src: &[T]) -> Option<usize>
{
	if dst.len() >= src.len() { unsafe {
		let clen = src.len();
		let sptr = src.as_ptr();
		let dptr = dst.as_mut_ptr();
		copy_nonoverlapping::<T>(sptr, dptr, clen);
		Some(src.len())
	}} else {
		None
	}
}

///Copy as much of prefix of one slice to another as fits.
///
///The parameter `dst` gives the destination slice to copy to.
///
///The parameter `src` gives the source slice to copy from.
///
///Returns the number of elements copied, that is, the size of the smaller of the two slices.
pub fn copy_maximum<T:Copy>(dst: &mut [T], src: &[T]) -> usize
{
	unsafe {
		let clen = min(dst.len(), src.len());
		let sptr = src.as_ptr();
		let dptr = dst.as_mut_ptr();
		copy_nonoverlapping::<T>(sptr, dptr, clen);
		clen
	}
}

///Cast UTF-8 prefix of byte array to string.
///
///The parameter `x` gives the byte string to operate on.
///
///Returns as string slice the longest prefix of `x` that is valid UTF-8. This may be empty if the byte array is empty,
///or if the byte array does not start with a valid UTF-8 codepoint.
pub fn from_utf8_prefix<'a>(x: &'a [u8]) -> &'a str
{
	match from_utf8(x) {
		Ok(y) => y,
		//By definition of valid_up_to(), this is valid UTF-8 string.
		Err(y) => unsafe{from_utf8_unchecked(x.get_unchecked(..y.valid_up_to()))},
	}
}

///Extract nth bit of slice.
///
///The parameter `slice` gives the byte array to extract bit from.
///
///The parameter `n` gives 0-based index of bit to extract.
///
///Returns the extracted bit, or `false` if index is out of range.
pub fn nth_bit(slice: &[u8], n: usize) -> bool
{
	let w = n / 8;
	if w < slice.len() { slice[w] >> n % 8 & 1 != 0 } else { false }
}

///Strip first element from slice.
///
///The parameter `slice` gives the slice to operate on.
///
///If the slice is empty, returns empty slice. Otherwise returns the slice with the first element removed.
pub fn slice_strip_first<T:Sized>(slice: &[T]) -> &[T]
{
	slice.get(1..).unwrap_or(slice)
}

///Split string to first character and the rest.
///
/// # Parameters:
///
/// * `s`: The string to split.
///
/// # Error conditions:
///
/// * `s` is empty.
///
/// # Return value:
///
///Tuple:
///
/// 1. First character
/// 1. The rest
pub fn str_first_tail<'a>(s: &'a str) -> Option<(char, &'a str)>
{
	let sb = s.as_bytes();
	let f = *sb.get(0)?;
	let (c, hlen) = match f as u32 {
		f@0x00..=0x7F => (f, 1),
		f@0xC0..=0xDF => unsafe {
			let f2 = *sb.get_unchecked(1) as u32 & 0x3F;
			((f << 6 | f2) & 0x7FF, 2)
		},
		f@0xE0..=0xEF => unsafe {
			let f2 = *sb.get_unchecked(1) as u32 & 0x3F;
			let f3 = *sb.get_unchecked(2) as u32 & 0x3F;
			((f << 12 | f2 << 6 | f3) & 0xFFFF, 3)
		},
		f@0xF0..=0xF4 => unsafe {
			let f2 = *sb.get_unchecked(1) as u32 & 0x3F;
			let f3 = *sb.get_unchecked(2) as u32 & 0x3F;
			let f4 = *sb.get_unchecked(3) as u32 & 0x3F;
			((f << 18 | f2 << 12 | f3 << 6 | f4) & 0x1FFFFF, 4)
		},
		//The remaining cases just can not happen.
		_ => unsafe{unreachable_unchecked()}
	};
	Some(unsafe{(char::from_u32_unchecked(c), s.get_unchecked(hlen..))})
}

///Read array from slice and map it.
///
///The parameter `slice` gives the slice to operate on.
///
///The parameter `offset` is index in slice to start reading from.
///
///The parameter `map` gives function pointer to map the array with.
///
///Returns the return value of specified map invoked on an array that consists of values of indices
///`offset` (inclusive) to `offset+N` (exclusive) of the specifed slice. If the range goes out of range, returns
///`None`.
///
///Note: Various integer `u?::from_?e_bytes` functions are especially good candidates for map function.
pub fn map_subarray<T:Copy+Sized, R:Sized, const N: usize>(slice: &[T], offset: usize, map: fn([T;N]) -> R) ->
	Option<R>
{
	let eoffset = offset + N;
	let array: [T;N] = slice.get(offset..eoffset)?.try_into().ok()?;
	Some(map(array))
}

///Strip first elements from mutable slice.
///
///The parameter `s` gives the input slice.
///
///The parameter `n` gives the number of first elements to strip.
///
///Returns the slice with indicated number of stripped elements. If the number is greater than number of elements
///available, returns an empty slice.
pub fn slice_tail_mut<'a,T:Sized+'a>(s: &'a mut [T], n: usize) -> &'a mut [T] { s.get_mut(n..).unwrap_or(&mut []) }

///Truncate slice and assume slice has been initialized.
///
///The parameter `s` gives the input slice.
///
///The parameter `n` gives number of elements in the slice.
///
/// # Safety:
///
///`n` First elements of the slice must be initialized.
pub unsafe fn slice_assume_init<T>(s: &[MaybeUninit<T>], n: usize) -> &[T]
{
	unsafe{transmute_copy::<&[MaybeUninit<T>], &[T]>(&s.get_unchecked(..n))}
}

///Make wrapping array.
///
///The parameter `a` gives the original array.
pub fn make_array_wrapping<T,const N:usize>(a: [T;N]) -> [core::num::Wrapping<T>;N]
{
	unsafe{transmute_copy(&a)}
}

///Fill slice `a` with value `v`.
pub fn slice_fill<T:Copy>(a: &mut [T], v: T)
{
	for e in a.iter_mut() { *e = v; }
}

///Write-only array.
pub unsafe trait WriteOnlyArray<T:Sized>
{
	///Get base and size of the region.
	///
	/// # Return value:
	///
	///A tuple:
	///
	/// 1. Base address of the region.
	/// 1. Size of the region in elements.
	fn as_raw_region(&mut self) -> (NonNullMut<T>, usize);
}

unsafe impl<T:Sized> WriteOnlyArray<T> for [T]
{
	fn as_raw_region(&mut self) -> (NonNullMut<T>, usize)
	{
		let y = self.len();
		//The return value of as_mut_ptr() is always non-NULL.
		let x = unsafe{NonNullMut::new_unchecked(self.as_mut_ptr())};
		(x, y)
	}
}

unsafe impl<T:Sized, const N: usize> WriteOnlyArray<T> for [T;N]
{
	fn as_raw_region(&mut self) -> (NonNullMut<T>, usize)
	{
		let y = self.len();
		//The return value of as_mut_ptr() is always non-NULL.
		let x = unsafe{NonNullMut::new_unchecked(self.as_mut_ptr())};
		(x, y)
	}
}

unsafe impl<T:Sized> WriteOnlyArray<T> for [MaybeUninit<T>]
{
	fn as_raw_region(&mut self) -> (NonNullMut<T>, usize)
	{
		let y = self.len();
		//The return value of as_mut_ptr() is always non-NULL.
		let x = unsafe{NonNullMut::new_unchecked(self.as_mut_ptr())};
		//MaybeUninit<T> and T are compatible.
		(x.cast(), y)
	}
}

unsafe impl<T:Sized, const N: usize> WriteOnlyArray<T> for [MaybeUninit<T>;N]
{
	fn as_raw_region(&mut self) -> (NonNullMut<T>, usize)
	{
		let y = self.len();
		//The return value of as_mut_ptr() is always non-NULL.
		let x = unsafe{NonNullMut::new_unchecked(self.as_mut_ptr())};
		//MaybeUninit<T> and T are compatible.
		(x.cast(), y)
	}
}


///Slice range.
///
///The following are valid `SliceRange`:
///
/// 1. `RangeFull`: `..`.
/// 1. `RangeFrom`: `a..`.
/// 1. `RangeTo`: `..b`.
/// 1. `RangeToInclusive`: `..=b`.
/// 1. `Range`: `a..b`.
/// 1. `RangeInclusive`: `a..=b`.
pub trait SliceRange
{
	///...
	unsafe fn get_unchecked<'a,T>(&self, parent: &'a [T]) -> &'a [T];
	///...
	unsafe fn get_unchecked_mut<'a,T>(&self, parent: &'a mut [T]) -> &'a mut [T];
	///...
	fn get<'a,T>(&self, parent: &'a [T]) -> Option<&'a [T]>;
	///...
	fn get_mut<'a,T>(&self, parent: &'a mut [T]) -> Option<&'a mut [T]>;
}

macro_rules! def_slice_range
{
	([$($x:tt)*]) => {
		impl SliceRange for core::ops:: $($x)*
		{
			unsafe fn get_unchecked<'a,T>(&self, parent: &'a [T]) -> &'a [T]
			{
				unsafe{parent.get_unchecked(self.clone())}
			}
			unsafe fn get_unchecked_mut<'a,T>(&self, parent: &'a mut [T]) -> &'a mut [T]
			{
				unsafe{parent.get_unchecked_mut(self.clone())}
			}
			fn get<'a,T>(&self, parent: &'a [T]) -> Option<&'a [T]>
			{
				parent.get(self.clone())
			}
			fn get_mut<'a,T>(&self, parent: &'a mut [T]) -> Option<&'a mut [T]>
			{
				parent.get_mut(self.clone())
			}
		}
	}
}

def_slice_range!([Range<usize>]);
def_slice_range!([RangeFrom<usize>]);
def_slice_range!([RangeFull]);
def_slice_range!([RangeInclusive<usize>]);
def_slice_range!([RangeTo<usize>]);
def_slice_range!([RangeToInclusive<usize>]);


///Slice with maybe uninitialized elements.
///
///This type essentially wraps `[MaybeUninit<T>]`, offering an alternative API more focused on slice of uninitialized
///elements.
#[repr(transparent)]
pub struct MaybeUninitSlice<T:Sized>([MaybeUninit<T>]);

impl<T:Sized> MaybeUninitSlice<T>
{
	///Create from immutable reference.
	///
	///The parameter `x` gives the slice to wrap.
	///
	///All the elements of resulting slice are actually initialized.
	pub fn new_ref(x: &[T]) -> &MaybeUninitSlice<T> { unsafe{transmute(x)} }
	///Create from mutable reference.
	///
	///The parameter `x` gives the slice to wrap.
	///
	///All the elements of resulting slice are actually initialized.
	///
	///# Safety:
	///
	///This function is unsafe, because it can be used to uninitialize elements in slice, which is unsound thing
	///to do.
	pub unsafe fn new_mut(x: &mut [T]) -> &mut MaybeUninitSlice<T> { unsafe{transmute(x)} }
	///Create from immutable reference.
	///
	///The parameter `x` gives the slice to wrap.
	pub fn wrap_ref(x: &[MaybeUninit<T>]) -> &MaybeUninitSlice<T> { unsafe{transmute(x)} }
	///Create from mutable reference.
	///
	///The parameter `x` gives the slice to wrap.
	pub fn wrap_mut(x: &mut [MaybeUninit<T>]) -> &mut MaybeUninitSlice<T> { unsafe{transmute(x)} }
	///Logically apply `Self::new_ref(<...>).into_ref()` to each element of slice.
	pub fn new_dref<'a,'b>(x: &'a [&'b [T]]) -> &'a [&'b [MaybeUninit<T>]] { unsafe{transmute(x)} }
	///Extract the inner slice (immutable).
	pub fn into_ref(&self) -> &[MaybeUninit<T>] { &self.0 }
	///Extract the inner slice (mutable).
	pub fn into_mut(&mut self) -> &mut [MaybeUninit<T>] { &mut self.0 }
	///Assume the slice has been initialized (immutable).
	///
	///# Safety:
	///
	///The slice must be completely initialized.
	pub unsafe fn assume_init_ref(&self) -> &[T] { unsafe{transmute(self)} }
	///Assume the slice has been initialized.
	///
	///# Safety:
	///
	///The slice must be completely initialized.
	pub unsafe fn assume_init_mut(&mut self) -> &mut [T] { unsafe{transmute(self)} }
	///Get length.
	pub fn len(&self) -> usize { self.0.len() }
	///Get pointer.
	///
	///The returned pointer is always non-NULL.
	pub fn as_mut_ptr(&mut self) -> *mut T { self.0.as_mut_ptr().cast() }
	///Get pointer.
	///
	///Like [`as_mut_ptr()`], but the pointer is `NonNullMut<T>` instead of `*mut T`.
	///
	///[`as_mut_ptr()`]:#method.as_mut_ptr
	pub fn as_nonnull_ptr(&mut self) -> NonNullMut<T> { unsafe{NonNullMut::new_unchecked(self.as_mut_ptr())} }
	///Extract subslice without bounds checking.
	///
	///The parameter `index` gives the slice range to extract. See [`SliceRange`] for what it can stand for.
	///
	///# Safety:
	///
	///The given range must be valid range of indexes in the slice.
	///
	///[`get()`]:#method.get
	///[`SliceRange`]:trait.SliceRange.html
	pub unsafe fn get_unchecked(&self, index: impl SliceRange) -> &MaybeUninitSlice<T>
	{
		let tmp: &[MaybeUninit<T>] = unsafe{index.get_unchecked(&self.0)};
		unsafe{transmute(tmp)}
	}
	///Extract subslice without bounds checking.
	///
	///Like [`get_unchecked()`], but returns mutable slice instead.
	///
	///[`get_unchecked()`]:#method.get_unchecked
	pub unsafe fn get_unchecked_mut(&mut self, index: impl SliceRange) -> &mut MaybeUninitSlice<T>
	{
		let tmp: &mut [MaybeUninit<T>] = unsafe{index.get_unchecked_mut(&mut self.0)};
		unsafe{transmute(tmp)}
	}
	///Extract subslice with bounds checking.
	///
	///The parameter `index` gives the slice range to extract. See [`SliceRange`] for what it can stand for.
	///
	///If the slice range is not valid index range, returns `None`. Otherwise returns the subslice with the
	///given index range.
	///
	///[`SliceRange`]:trait.SliceRange.html
	pub fn get(&self, index: impl SliceRange) -> Option<&MaybeUninitSlice<T>>
	{
		let tmp: &[MaybeUninit<T>] = index.get(&self.0)?;
		Some(unsafe{transmute(tmp)})
	}
	///Extract subslice with bounds checking.
	///
	///Like [`get()`], but returns mutable slice instead.
	///
	///[`get()`]:#method.get
	pub fn get_mut(&mut self, index: impl SliceRange) -> Option<&mut MaybeUninitSlice<T>>
	{
		let tmp: &mut [MaybeUninit<T>] = index.get_mut(&mut self.0)?;
		Some(unsafe{transmute(tmp)})
	}
}

///Buffer to print a message to.
///
///If message is too long, it will be truncated on the last UTF-8 boundary that fits.
///
///This implements `core::fmt::Write`, so it can be written to using the `write!()` and `writeln!()` macros.
pub struct PrintBuffer<'a>(&'a mut [MaybeUninit<u8>], usize);

impl<'a> PrintBuffer<'a>
{
	///Create a new print buffer.
	///
	///The parameter `x` gives the backing buffer.
	pub fn new(x: &'a mut [MaybeUninit<u8>]) -> PrintBuffer<'a> { PrintBuffer(x, 0) }
	///Consume print buffer and yield a byte slice.
	pub fn into_inner_bytes(self) -> &'a [u8] { self.into_inner().as_bytes() }
	///Consume print buffer and yield a string slice.
	///
	///Like [`into_inner_bytes()`], but returns a string instead.
	///
	///[`into_inner_bytes()`]:#method.into_inner_bytes
	pub fn into_inner(self) -> &'a str
	{
		//It is guaranteed that self.1 always points at codepoint boundary and self.0 is valid UTF-8,
		//thus this is always safe.
		unsafe{transmute(&self.0[..min(self.1, self.0.len())])}
	}
	///If the message does not already end in linefeed, add a linefeed.
	///
	///If the buffer is full, but not empty, the last character is ovewritten in order to make room for the LF.
	pub fn force_last_linefeed(&mut self)
	{
		self.__force_last_byte(10);
	}
	///If the message does not already end in NUL, add a NUL.
	///
	///If the buffer is full, but not empty, the last character is ovewritten in order to make room for the NUL.
	///
	///After this method finishes, if the backing buffer is not zero-size, the buffer is guaranteed to end in
	///NUL byte. This is useful for C interop.
	pub fn force_last_nul(&mut self)
	{
		self.__force_last_byte(0);
	}
	fn __force_last_byte(&mut self, b: u8)
	{
		//If the message already ends in linefeed, do nothing. Reading of <self.1 is safe, so self.1-1 is
		//safe.
		if unsafe { self.1 > 0 && self.0[self.1-1].assume_init() == b } { return; }
		//If there is space, just add the linefeed.
		if self.1 < self.0.len() {
			self.0[self.1] = MaybeUninit::new(b);
			self.1 += 1;
		}
		//No space! We have to overwrite the last charater.
		//If last byte is continuation, remove it. Then overwrite the last character. The result will be
		//valid UTF-8.
		while unsafe { self.1 > 0 && self.0[self.1-1].assume_init() & 0xc0 == 0x80 } { self.1 -= 1; }
		if self.1 > 0 {
			self.0[self.1-1] = MaybeUninit::new(b);
		} else if self.0.len() > 0 {
			//This happens if one tries to force a linefeed on empty string.
			self.0[0] = MaybeUninit::new(b);
			self.1 = 1;
		}
		//If self.0.len() == 0, we can not do anything.
	}
}

impl<'a> FmtWrite for PrintBuffer<'a>
{
	fn write_str(&mut self, s: &str) -> Result<(), FmtError>
	{
		let left = match self.0.len().checked_sub(self.1) { Some(x) => x, None => return Ok(()) };
		let mut left = min(left, s.len());
		//Truncate to whole codepoint.
		while left > 0 && !s.is_char_boundary(left) { left -= 1; }
		//This caps at left <= s.len().
		let s = &s[..left];
		//This caps at self.1+left <= self.1+self.0.len()-self.1=self.0.len(). So bounds are ok.
		let tgt = &mut self.0[self.1..][..left];
		for (t, s) in tgt.iter_mut().zip(s.as_bytes().iter()) { *t = MaybeUninit::new(*s); }
		//Left points at codepoint boundary, so this leaves self.1 at codepoint boundary.
		self.1 += left;
		Ok(())
	}
}

///Uninitialized best-effort buffer.
///
///Best-effort here means that any overflowing data is silently discarded.
pub struct BEWriteBuffer<'a>(&'a mut [MaybeUninit<u8>], usize);

impl<'a> BEWriteBuffer<'a>
{
	///Create a new buffer.
	///
	///The parameter `buf` gives the backing buffer to use.
	pub fn new(buf: &'a mut [MaybeUninit<u8>]) -> BEWriteBuffer<'a> { BEWriteBuffer(buf, 0) }
	///Append data to be buffer.
	///
	///The parameter `data` gives the data to append. Any overflowing data is silently discarded.
	pub fn append(&mut self, data: &[u8])
	{
		if self.1 >= self.0.len() { return; }
		let r = min(self.0.len() - self.1, data.len());
		unsafe {
			//data.as_ptr()+r<=data.as_ptr()+data.len(). Therefore read is in range.
			//self.0.as_mut_ptr()+self.1+r<=self.0.as_mut_ptr()+self.1+(self.0.len()-self.1)=
			//self.0.as_mut_ptr()+self.0.len(). Therefore write is in range.
			//Since data is &[u8], the buffers can not overlap.
			let sptr: *const u8 = data.as_ptr();
			let dptr: *mut u8 = self.0.as_mut_ptr().add(self.1).cast();
			copy_nonoverlapping(sptr, dptr, r);
		}
		self.1 += r;
	}
	///Append byte to the buffer. 
	///
	///The parameter `data` gives the byte to append. Any overflowing data is silently discarded.
	pub fn append_byte(&mut self, data: u8)
	{
		self.0.get_mut(self.1).map(|t|*t = MaybeUninit::new(data));
		self.1 = self.1.saturating_add(1);
	}
	///Get the buffer contents.
	///
	///This returns any written data (except for the data that has been discarded as overflowing).
	pub fn into_inner(self) -> &'a [u8]
	{
		let r = min(self.0.len(), self.1);
		//By assumption, initialized part is to r.
		unsafe{from_raw_parts(self.0.as_ptr().cast(), r)}
	}
}

///Iterator over lines on byte string.
pub struct ByteStringLines<'a>(&'a [u8]);

impl<'a> ByteStringLines<'a>
{
	///Create new iterator.
	///
	/// # Parameters:
	///
	/// * `s`: The string to iterate over.
	///
	/// # Return value:
	///
	///The iterator.
	pub fn new(s: &'a [u8]) -> ByteStringLines<'a> { ByteStringLines(s) }
}

impl<'a> Iterator for ByteStringLines<'a>
{
	type Item = &'a [u8];
	fn next(&mut self) -> Option<&'a [u8]>
	{
		//If string is empty, emit nothing.
		fail_if_none!(self.0.len() == 0);
		//Find end of line.
		let mut i = 0;
		while i < self.0.len() && self.0[i] != 13 && self.0[i] != 10 { i += 1; }
		//Split the lines. Now tail can be empty, start with CR, LF or CRLF. Remove those.
		let (head, tail) = self.0.split_at(i);
		if tail.len() >= 2 && &tail[..2] == &[13,10] {
			self.0 = &tail[2..];
		} else if tail.len() >= 1 {
			//This has to be CR or LF.
			self.0 = &tail[1..];
		} else {
			//The tail is empty. This is the last value returned.
			self.0 = tail;
		}
		Some(head)
	}
}

///Calculate syndrome of two byte arrays (OR of XORs).
///
/// # Parameters:
///
/// * `a`: The first array.
/// * `b`: The second array.
///
/// # Return value:
///
///The syndrome.
///
/// # Notes:
///
/// * If arrays are not of the same length, immediately returns 0xFF.
pub fn calculate_syndrome(a: &[u8], b: &[u8]) -> u8
{
	if a.len() != b.len() { return 0xFF; }
	a.iter().zip(b.iter()).fold(0,|x,(y,z)|x|(y^z))
}

///Compare two byte strings for equality case-insensitive.
///
///This always uses ASCII locale.
///
/// # Parameters:
///
/// * `a`: The first string.
/// * `b`: The second string.
///
/// # Return value:
///
///`true` if strings are equal, `false` if not.
pub fn equals_case_insensitive(a: &[u8], b: &[u8]) -> bool
{
	if a.len() != b.len() { return false; }
	//TODO: Use SIMD.
	for (c, d) in a.iter().cloned().zip(b.iter().cloned()) {
		let c = c.to_ascii_lowercase();
		let d = d.to_ascii_lowercase();
		if c != d { return false; }
	}
	true
}

///Return slice range of child relative to parent.
///
/// # Parameters:
///
/// * `parent`: The parent slice.
/// * `child`: The child slice.
///
/// # Error conditions:
///
/// * The child is not sublice of the parent.
/// * The array element type is ZST.
///
/// # Returns:
///
/// * Range in `parent` that corresponds to `child`.
pub fn subslice_to_range<T:Sized>(parent: &[T], child: &[T]) -> Result<Range<usize>, ()>
{
	let elemsize = size_of::<T>();
	//The addresses of parent and child.
	let parent_addr = parent.as_ptr() as usize;
	let child_addr = child.as_ptr() as usize;
	//Compute offset of child in bytes.
	let child_offset = dtry!(child_addr.checked_sub(parent_addr));
	//Check that the child address is aligned w.r.t. parent.
	fail_if!(child_offset.checked_rem(elemsize) != Some(0), ());
	//Compute starting element of child.
	let child_start = child_offset / elemsize;
	//Compute ending element of child.
	let child_end = dtry!(child_start.checked_add(child.len()));
	//Check that the child is in range.
	fail_if!(child_end > parent.len(), ());
	//Ok.
	Ok(child_start..child_end)
}


#[cfg(test)] mod test;
