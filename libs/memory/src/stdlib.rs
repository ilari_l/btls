extern crate std;
use super::TrustedInvariantLength;

unsafe impl TrustedInvariantLength for std::ffi::OsString
{
	fn as_byte_slice(&self) -> &[u8] { self.as_os_str().as_byte_slice() }
}

unsafe impl<'a> TrustedInvariantLength for &'a std::ffi::OsString
{
	fn as_byte_slice(&self) -> &[u8] { self.as_os_str().as_byte_slice() }
}

unsafe impl TrustedInvariantLength for std::path::PathBuf
{
	fn as_byte_slice(&self) -> &[u8] { self.as_os_str().as_byte_slice() }
}

unsafe impl<'a> TrustedInvariantLength for &'a std::path::PathBuf
{
	fn as_byte_slice(&self) -> &[u8] { self.as_os_str().as_byte_slice() }
}

unsafe impl TrustedInvariantLength for std::path::Path
{
	fn as_byte_slice(&self) -> &[u8] { self.as_os_str().as_byte_slice() }
}

unsafe impl<'a> TrustedInvariantLength for &'a std::path::Path
{
	fn as_byte_slice(&self) -> &[u8] { self.as_os_str().as_byte_slice() }
}

unsafe impl TrustedInvariantLength for std::ffi::OsStr
{
	fn as_byte_slice(&self) -> &[u8] { unsafe { core::mem::transmute(self) } }
}

unsafe impl<'a> TrustedInvariantLength for &'a std::ffi::OsStr
{
	fn as_byte_slice(&self) -> &[u8] { (*self).as_byte_slice() }
}
