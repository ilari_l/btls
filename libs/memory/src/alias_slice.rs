use super::NonNull;
use super::NonNullMut;
use btls_aux_fail::fail_if;
use btls_aux_fail::fail_if_none;
use core::marker::PhantomData;
use core::mem::MaybeUninit;
use core::mem::transmute;
use core::ops::Bound;
use core::ops::Range;
use core::ops::RangeBounds;
use core::ptr::drop_in_place;
use core::ptr::swap as ptr_swap;


pub(crate) fn normalize(x: impl RangeBounds<usize>, y: usize) -> Option<Range<usize>>
{
	let end = match x.end_bound() {
		Bound::Included(x) if *x < y => x + 1,
		Bound::Excluded(x) if *x <= y => *x,
		Bound::Unbounded => y,
		_ => return None
	};
	let start = match x.start_bound() {
		Bound::Included(x) if *x <= end => *x,
		Bound::Excluded(x) if *x < end => x + 1,
		Bound::Unbounded => 0,
		_ => return None
	};
	Some(start..end)
}

pub(crate) fn normalize_unchecked(x: impl RangeBounds<usize>, y: usize) -> Range<usize>
{
	let end = match x.end_bound() {
		Bound::Included(x) => x.saturating_add(1),
		Bound::Excluded(x) => *x,
		Bound::Unbounded => y,
	};
	let start = match x.start_bound() {
		Bound::Included(x) => *x,
		Bound::Excluded(x) => x.saturating_add(1),
		Bound::Unbounded => 0,
	};
	start..end
}

macro_rules! impl_slice_common
{
	($clazz:ident, $ptr:ident) => {
		impl<'a,T:Sized+'a> $clazz<'a,T>
		{
			fn __raw_ctor(base: $ptr<T>, len: usize) -> $clazz<'a,T>
			{
				$clazz { base, len, phantom: PhantomData }
			}
			///Get base of slice.
			///
			/// # Return value
			///
			///The base.
			pub fn as_ptr(&self) -> $ptr<T> { self.base }
			///Get length.
			///
			/// # Return value
			///
			///The length of slice in elements.
			pub fn len(&self) -> usize { self.len }
			///Get base and length.
			///
			/// # Return value:
			///
			///A tuple:
			///
			/// 1. The base of slice.
			/// 1. The length of slice in elemnts.
			pub fn base_and_len(&self) -> ($ptr<T>, usize) { (self.base, self.len) }
			///Get specified slice.
			///
			/// # Parameter:
			///
			/// * `range`: The range to get.
			///
			/// # Error conditions:
			///
			/// * The range is invalid.
			/// * The range is out of range.
			///
			/// # Return value:
			///
			///The new aliasing slice.
			pub fn subslice(&self, range: impl RangeBounds<usize>) -> Option<$clazz<'a,T>>
			{
				let range = normalize(range, self.len)?;
				Some($clazz::__raw_ctor(unsafe{self.base.add(range.start)}, range.end - range.start))
			}
			pub unsafe fn subslice_unchecked(&self, range: impl RangeBounds<usize>) -> $clazz<'a,T>
			{
				let range = normalize_unchecked(range, self.len);
				debug_assert!(range.end >= range.start && range.end <= self.len);
				$clazz::__raw_ctor(unsafe{self.base.add(range.start)}, range.end - range.start)
			}
			///Truncate to specified length.
			///
			/// # Parameters:
			///
			/// * `nlen': The new length in elements.
			///
			/// # Error conditions:
			///
			/// * 'nlen` is bigger than the current length.
			pub fn truncate(&mut self, nlen: usize) -> Result<(), ()>
			{
				fail_if!(nlen > self.len, ());
				self.len = nlen;
				Ok(())
			}
		}
	}
}

macro_rules! impl_read
{
	($clazz:ident) => {
		impl<'a,T:Sized+'a> $clazz<'a,T>
		{
			///Read element if in range.
			///
			/// # Parameters:
			///
			/// * `index`: The index of element.
			///
			/// # Error conditions:
			///
			/// * Index is out of range.
			///
			/// # Return value:
			///
			///The read element.
			pub fn read(&self, index: usize) -> Option<&T>
			{
				fail_if_none!(index >= self.len);
				//Safety: In-range by check above.
				let elem = unsafe{self.base.add(index)};
				//Transmute gets around borrow checker.
				unsafe{transmute(elem)}
			}
		}
	}
}

///Possibly-aliasing read slice.
///
///This differs from `&[T]` in that it may alias writeable memory ranges.
pub struct AliasReadSlice<'a,T:Sized+'a>
{
	base: NonNull<T>,
	len: usize,
	phantom: PhantomData<&'a T>,
}

unsafe impl<'a,T:Sized+Send+'a> Send for AliasReadSlice<'a,T> {}
unsafe impl<'a,T:Sized+Sync+'a> Sync for AliasReadSlice<'a,T> {}

impl_slice_common!(AliasReadSlice, NonNull);
impl_read!(AliasReadSlice);

impl<'a,T:Sized+'a> AliasReadSlice<'a,T>
{
	///Create new slice.
	///
	/// # Parameters:
	///
	/// * `x`: The slice.
	///
	/// # Return value:
	///
	///The newly created aliasing read slice.
	pub fn new(x: &'a [T]) -> AliasReadSlice<'a,T>
	{
		Self::__raw_ctor(NonNull::new_slice_base(x), x.len())
	}
}

///Possibly-aliasing write slice.
pub struct AliasWriteSlice<'a,T:Sized+'a>
{
	base: NonNullMut<T>,
	len: usize,
	phantom: PhantomData<&'a mut T>,
}

unsafe impl<'a,T:Sized+Send+'a> Send for AliasWriteSlice<'a,T> {}
unsafe impl<'a,T:Sized+Sync+'a> Sync for AliasWriteSlice<'a,T> {}

impl_slice_common!(AliasWriteSlice, NonNullMut);

impl<'a,T:Sized+'a> AliasWriteSlice<'a,T>
{
	///Create new.
	///
	/// # Parameters:
	///
	/// * `x`: The slice.
	///
	/// # Return value:
	///
	///The newly created aliasing write slice.
	pub fn new(x: &'a mut [T]) -> AliasWriteSlice<'a,T>
	{
		Self::__raw_ctor(NonNullMut::new_slice_base(x), x.len())
	}
	///Create new.
	///
	///This differs from [`AliasWriteSlice::new()`] in that the passed in slice is slice of `MaybeUninit<T>`
	///instead of `T`.
	///
	/// # Parameters:
	///
	/// * `x`: The slice.
	///
	/// # Return value:
	///
	///The newly created aliasing write slice.
	///
	///[`AliasWriteSlice::new()`]: #method.new
	pub fn new_uninit(x: &'a mut [MaybeUninit<T>]) -> AliasWriteSlice<'a,T>
	{
		Self::__raw_ctor(NonNullMut::new_slice_base(x).cast(), x.len())
	}
	///Write element if in range.
	///
	/// # Parameters:
	///
	/// * `index`: The index to write.
	/// * `value`: The value to write.
	///
	/// # Error conditions:
	///
	/// * Index is out of range.
	///
	/// # Notes:
	///
	/// * The old element is not dropped, it is leaked! This is because it is not guaranteed that the element
	///is initialized.
	pub fn write(&self, index: usize, value: T) -> Option<()>
	{
		fail_if_none!(index >= self.len);
		unsafe {
			//Safety: In-range by check above. And the resulting value is always aligned.
			let elem = self.base.add(index);
			elem.write(value);
		}
		Some(())
	}
}

///Possibly-aliasing read/write slice.
pub struct AliasReadWriteSlice<'a,T:Sized+'a>
{
	base: NonNullMut<T>,
	len: usize,
	phantom: PhantomData<&'a mut T>,
}

unsafe impl<'a,T:Sized+Send+'a> Send for AliasReadWriteSlice<'a,T> {}
unsafe impl<'a,T:Sized+Sync+'a> Sync for AliasReadWriteSlice<'a,T> {}

impl_slice_common!(AliasReadWriteSlice, NonNullMut);
impl_read!(AliasReadWriteSlice);

impl<'a,T:Sized+'a> AliasReadWriteSlice<'a,T>
{
	///Create new.
	pub fn new(x: &'a mut [T]) -> AliasReadWriteSlice<'a,T>
	{
		Self::__raw_ctor(NonNullMut::new_slice_base(x), x.len())
	}
	///Fork to read and write parts.
	pub fn fork(self) -> (AliasReadSlice<'a,T>, AliasWriteSlice<'a,T>)
	{
		let r = AliasReadSlice::__raw_ctor(self.base.to_const(), self.len);
		let w = AliasWriteSlice::__raw_ctor(self.base, self.len);
		(r, w)
	}
	///Write element if in range.
	///
	/// # Parameters:
	///
	/// * `index`: The index to write.
	/// * `value`: The value to write.
	///
	/// # Error conditions:
	///
	/// * Index is out of range.
	pub fn write(&self, index: usize, mut value: T) -> Option<()>
	{
		fail_if_none!(index >= self.len);
		unsafe {
			//Safety: In-range by check above.
			let elem = self.base.add(index);
			let value = &mut value as *mut T;
			ptr_swap(elem.to_raw(), value);
			drop_in_place(value);
		}
		Some(())
	}
}
