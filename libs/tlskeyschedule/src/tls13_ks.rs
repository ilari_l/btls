use super::__call_debug;
use super::__call_event;
use super::_FinishedValue;
use super::_RecordDeprotector;
use super::_RecordProtector;
use super::_TlsExtractor;
use super::BoxedDeprotector;
use super::BoxedExtractor;
use super::BoxedProtector;
use super::DiffieHellmanSharedSecret;
use super::Exportable;
use super::FinishedValue;
use super::GenerateFinished;
use super::HasAd;
use super::IsServer;
use super::MAX_KEY_CHUNK;
use super::Tls13RecordMode;
use super::TlsDebug;
use btls_aux_aead::ProtectorType;
use btls_aux_assert::assert_failure;
use btls_aux_assert::AssertFailed;
use btls_aux_assert::sanity_check;
use btls_aux_assert::sanity_failed;
use btls_aux_collections::Box;
use btls_aux_fail::fail_if;
use btls_aux_hash::HashFunction2;
use btls_aux_hash::HashOutput2;
use btls_aux_hash::slice_eq_ct;
use btls_aux_memory::slice_splitn_at_mut;
use btls_aux_signatures::Hasher;
use btls_aux_signatures::SignatureAlgorithmEnabled2;
use btls_aux_signatures::SignatureAlgorithmTls2;
use btls_aux_signatures::SignatureError2;
use btls_aux_signatures::SignatureFlags2;
use btls_aux_signatures::SignatureTls2;
use btls_aux_tls_iana::Alert;
use core::ops::Deref;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;


///Error in TLS 1.3 key schedule.
#[derive(Clone,Debug)]
#[non_exhaustive]
pub enum Tls13KeyScheduleError
{
	///Assertion failed.
	InternalError(AssertFailed),
	///Signature does not verify.
	SignatureFailed(SignatureError2),
	///Bad finished MAC.
	BadFinishedMac,
	///Bad binder.
	BadBinder,
	///Unexpected algorithm.
	UnexpectedAlgorithm(SignatureAlgorithmTls2, SignatureAlgorithmTls2),
}

impl Tls13KeyScheduleError
{
	///Get TLS alert for error.
	pub fn alert(&self) -> Alert
	{
		match self {
			&Tls13KeyScheduleError::InternalError(_) => Alert::INTERNAL_ERROR,
			&Tls13KeyScheduleError::SignatureFailed(_) => Alert::DECRYPT_ERROR,
			&Tls13KeyScheduleError::BadFinishedMac => Alert::DECRYPT_ERROR,
			&Tls13KeyScheduleError::BadBinder => Alert::DECRYPT_ERROR,
			&Tls13KeyScheduleError::UnexpectedAlgorithm(_,_) => Alert::DECRYPT_ERROR,
		}
	}
}

impl Display for Tls13KeyScheduleError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&Tls13KeyScheduleError::InternalError(ref err) => write!(f, "Internal Error: {err}"),
			&Tls13KeyScheduleError::SignatureFailed(ref err) =>
				write!(f, "Verifying signature failed: {err}"),
			&Tls13KeyScheduleError::BadFinishedMac => f.write_str("Bad Finished MAC"),
			&Tls13KeyScheduleError::BadBinder => f.write_str("Bad Binder MAC"),
			&Tls13KeyScheduleError::UnexpectedAlgorithm(ref got, ref expected) =>
				write!(f, "Unexpected algorithm {got:?}, expected {expected:?}"),
		}
	}
}

impl From<AssertFailed> for Tls13KeyScheduleError
{
	fn from(e: AssertFailed) -> Tls13KeyScheduleError { Tls13KeyScheduleError::InternalError(e) }
}


pub(crate) fn tls13_l<'a>(x: &'a str) -> [&'a str;2] { ["tls13 ", x] }

#[derive(Copy,Clone)]
pub(crate) enum HkdfLabel<'a>
{
	EarlyTraffic,
	Traffic(bool, bool),
	ExporterD,
	Finished,
	DerivedSecret,
	Key,
	Iv,
	ExporterE,
	ExporterM,
	KeyUpdate,
	ResumptionMaster,
	Resumption,
	Binder(bool),
	Custom(&'a str),
}

impl<'a> HkdfLabel<'a>
{
	//Get the label.
	pub(crate) fn get(&'a self) -> &'a str
	{
		match *self {
			HkdfLabel::EarlyTraffic          => "c e traffic",
			HkdfLabel::Traffic(false, false) => "c hs traffic",
			HkdfLabel::Traffic(false, true ) => "c ap traffic",
			HkdfLabel::Traffic(true,  false) => "s hs traffic",
			HkdfLabel::Traffic(true,  true ) => "s ap traffic",
			HkdfLabel::DerivedSecret         => "derived",
			HkdfLabel::ExporterE             => "e exp master",
			HkdfLabel::ExporterM             => "exp master",
			HkdfLabel::KeyUpdate             => "traffic upd",
			HkdfLabel::ExporterD             => "exporter",
			HkdfLabel::Finished              => "finished",
			HkdfLabel::Key                   => "key",
			HkdfLabel::Iv                    => "iv",
			HkdfLabel::ResumptionMaster      => "res master",
			HkdfLabel::Resumption            => "resumption",
			HkdfLabel::Binder(true)          => "ext binder",
			HkdfLabel::Binder(false)         => "res binder",
			HkdfLabel::Custom(x)             => x,
		}
	}
}

fn __compute_tls13_finished(basekey: &HashOutput2, hs_hash: &HashOutput2, prf: HashFunction2,
	debug: &mut Option<&mut dyn FnMut(TlsDebug)>, received: Option<&[u8]>) -> FinishedValue
{
	let finished_key = prf.tls13_derive_secret(&basekey, &tls13_l(HkdfLabel::Finished.get()), &[]);
	let ret = prf.tls13_hmac(&finished_key, &hs_hash);
	__call_debug(debug, TlsDebug::Tls13Finished {
		base_key: basekey,
		finished_key: &finished_key,
		handshake_hash: hs_hash,
		calculated_mac: &ret,
		received_mac: received,
	});
	FinishedValue(_FinishedValue::Tls13(ret))
}

fn rachet_common(key: &mut HashOutput2, func: HashFunction2, mut debug: Option<&mut dyn FnMut(TlsDebug)>,
	is_tx: bool)
{
	//Yes, it apparently is "application traffic secret", for both directions (ladders are disjoint,
	//so this is no crypto problem). Also, this one uses hdkf-expand-label with empty context in the
	//spec.
	let tmpkey = func.tls13_derive_secret(&key, &tls13_l(HkdfLabel::KeyUpdate.get()), &[]);
	__call_debug(&mut debug, TlsDebug::Tls13BumpTrafficSecret {
		is_tx: is_tx,
		old: &key,
		new: &tmpkey,
	});
	//Update the key, as tls13_hkdf_expand_label can't directly return a hash.
	*key = tmpkey;
}

fn compute_enc_keys(keyblock: &mut [u8], from_secret: &HashOutput2, protection: ProtectorType,
	func: HashFunction2) -> Result<usize, Tls13KeyScheduleError>
{
	let keysize = protection.get_key_length();
	let ivsize = protection.get_nonce_length();

	let blocklen = keysize + ivsize;	//Should be reasonable, and at least keysize.
	let keyblock_len = keyblock.len();
	sanity_check!(keysize <= blocklen, F "Keysize {keysize} >= blocklen {blocklen}???");
	let keyblock = keyblock.get_mut(..blocklen).ok_or_else(||{
		assert_failure!(F "Keyblock too small ({keyblock_len}, need {blocklen})")
	})?;
	let (store_key, store_iv) = keyblock.split_at_mut(keysize);
	func.tls13_hkdf_expand_label(&from_secret, &tls13_l(HkdfLabel::Key.get()), &[], store_key);
	func.tls13_hkdf_expand_label(&from_secret, &tls13_l(HkdfLabel::Iv.get()), &[], store_iv);
	Ok(blocklen)
}

fn __compute_tls13_traffic_secret(master_key: &HashOutput2, hs_hash: &HashOutput2, prf: HashFunction2,
	label: HkdfLabel, debug: &mut Option<&mut dyn FnMut(TlsDebug)>) -> HashOutput2
{
	let label = label.get();
	let masterkey = prf.tls13_derive_secret(&master_key, &tls13_l(label), hs_hash);
	__call_debug(debug, TlsDebug::Tls13TrafficSecret {
		label: label,
		master_secret: &master_key,
		handshake_hash: &hs_hash,
		traffic_secret: &masterkey,
	});
	masterkey
}

//The server_write=TRUE means server->client direction keys.
fn compute_tls13_traffic_secret(master_key: &HashOutput2, hs_hash: &HashOutput2, prf: HashFunction2, hfinal: bool,
	server_write: bool, debug: &mut Option<&mut dyn FnMut(TlsDebug)>) -> HashOutput2
{
	let label = HkdfLabel::Traffic(server_write, hfinal);
	__compute_tls13_traffic_secret(master_key, hs_hash, prf, label, debug)
}

fn compute_tls13_early_traffic_secret(master_key: &HashOutput2, hs_hash: &HashOutput2, prf: HashFunction2,
	debug: &mut Option<&mut dyn FnMut(TlsDebug)>) -> HashOutput2
{
	__compute_tls13_traffic_secret(master_key, hs_hash, prf, HkdfLabel::EarlyTraffic, debug)
}

fn mix_tls13_key(existing_key: &HashOutput2, incoming_key: &[u8], prf: HashFunction2, kname: &str, is_dhe: bool,
	debug: &mut Option<&mut dyn FnMut(TlsDebug)>) -> HashOutput2
{
	//Only one of the two set_pms calls is affected by this.
	let chain_key: HashOutput2 = if is_dhe {
		//Derive-secret step was added before zero block.
		let empty_hash = prf.hash(b"");
		let hash = prf.tls13_derive_secret(&existing_key, &tls13_l(HkdfLabel::DerivedSecret.get()),
			&empty_hash);
		__call_debug(debug, TlsDebug::Tls13PreMixExpansion {
			old: &existing_key,
			new: &hash
		});
		hash
	} else {
		//Copy the key, because we can't borrow premaster_secret properly.
		existing_key.clone()
	};
	let newres = prf.tls13_hkdf_extract(incoming_key, &chain_key);
	__call_debug(debug, TlsDebug::Tls13KeyMix {
		key_name: kname,
		old: &chain_key,
		input: incoming_key,
		new: &newres,
	});
	newres
}

//Format the server signature TBS for TLS 1.3.
//The maximum possible output length is 98 + MAX_HASH_OUTPUT
pub fn format_tbs_tls13<'b>(hash: HashOutput2, server: bool, output: &'b mut [u8]) -> Result<&'b [u8], AssertFailed>
{
	let label = if server { "TLS 1.3, server CertificateVerify" } else { "TLS 1.3, client CertificateVerify" };
	let padding = [32;32];
	let hash = hash.deref();
	let hlen = hash.len();
	if hlen > 255 { sanity_failed!("Hash output too big ({hlen} > 255)"); }
	let output_len = output.len();
	let lengths = (padding.len(), padding.len(), label.len(), 1usize, hash.len());
	let ((w_pad1, w_pad2, w_label, w_zero, w_hash), olen) = slice_splitn_at_mut(output, lengths).ok_or_else(||{
		let need = lengths.0+lengths.1+lengths.2+lengths.3+lengths.4;
		assert_failure!(F "TBS too big ({need} > {output_len})")
	})?;
	w_pad1.copy_from_slice(&padding);
	w_pad2.copy_from_slice(&padding);
	w_label.copy_from_slice(label.as_bytes());
	w_zero[0] = 0;
	w_hash.copy_from_slice(hash);
	//slice_splitn_at_mut() suceeding guarantees this is valid.
	Ok(unsafe{output.get_unchecked_mut(..olen)})
}

///The TLS CertificateVerify message
pub struct TlsCertificateVerify<'a>
{
	///Signature algorithm.
	pub algorithm: SignatureAlgorithmTls2,
	///The signature value.
	pub signature: &'a [u8],
}

///Unsigned TLS 1.3 handshake secret.
pub struct UnsignedTls13HandshakeSecret(Tls13HandshakeSecret);

impl UnsignedTls13HandshakeSecret
{
	///Open the unsigned secret.
	pub fn to_handshake_secret(self, server_spki: &[u8], hash_ch_to_ccert: HashOutput2,
		pmsg: &TlsCertificateVerify, flags: SignatureFlags2, expected_alg: CheckSignatureType,
		enabled_sig: SignatureAlgorithmEnabled2, mut debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<Tls13HandshakeSecret, Tls13KeyScheduleError>
	{
		expected_alg.check(pmsg.algorithm)?;
		//Format TBS and verify signature.
		let mut tbs = |h: &mut Hasher|{
			h.append(&[32;64]);	//Padding for Random.
			h.append(b"TLS 1.3, server CertificateVerify\0");
			h.append(&hash_ch_to_ccert);
		};
		key_debug!(&mut debug, TlsDebug::TlsServerSignature {
			public_key: server_spki,
			server_tbs: &Hasher::collect(&mut tbs),
			algorithm: pmsg.algorithm,
			signature: pmsg.signature,
		});
		//Cap at ALLOW_RSA_PSS, since this is TLS 1.3 on client side.
		SignatureTls2::new(pmsg.algorithm, pmsg.signature).verify_callback(server_spki, enabled_sig,
			flags & SignatureFlags2::ALLOW_RSA_PSS, &mut tbs).
			map_err(|x|Tls13KeyScheduleError::SignatureFailed(x))?;
		Ok(self.0)
	}
	//The hs_hash is after ServerHello.
	pub fn to_traffic_secrets(&self, hash_ch_to_sh: &HashOutput2, debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<(Tls13TrafficSecretIn, Tls13TrafficSecretOut), Tls13KeyScheduleError>
	{
		self.0.to_traffic_secrets(hash_ch_to_sh, debug)
	}
}

pub struct CheckSignatureType(Option<SignatureAlgorithmTls2>);

impl CheckSignatureType
{
	pub fn no_requirement() -> CheckSignatureType { CheckSignatureType(None) }
	pub fn require(a: SignatureAlgorithmTls2) -> CheckSignatureType { CheckSignatureType(Some(a)) }
	pub fn check(&self, alg:SignatureAlgorithmTls2) -> Result<(),Tls13KeyScheduleError>
	{
		if let Some(chk) = self.0 {
			fail_if!(chk != alg, Tls13KeyScheduleError::UnexpectedAlgorithm(alg, chk));
		}
		Ok(())
	}
}

///TLS 1.3 Early secret
struct Tls13EarlySecret
{
	secret: HashOutput2,
	protector: ProtectorType,
	prf: HashFunction2,
	is_server: IsServer,
	exportable: Exportable,
	ad: HasAd,
	external: bool,
}

impl Tls13EarlySecret
{
	fn new(psk: &[u8], protector: ProtectorType, prf: HashFunction2, is_server: IsServer,
		exportable: Exportable, ad: HasAd, external: bool, mut debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<Tls13EarlySecret, Tls13KeyScheduleError>
	{
		let secret = prf.zeroes_tls13_handshake();
		let secret = mix_tls13_key(&secret, &psk, prf, "PSK", false, &mut debug);
		Ok(Tls13EarlySecret {secret, protector, prf, is_server, exportable, ad, external })
	}
	fn with_secret(&self, secret: HashOutput2) -> Result<Tls13HandshakeSecret, Tls13KeyScheduleError>
	{
		Ok(Tls13HandshakeSecret {
			secret: secret,
			protector: self.protector,
			prf: self.prf,
			is_server: self.is_server,
			exportable: self.exportable,
			ad: self.ad,
		})
	}
	fn to_handshake_secret_pure_psk(&self, mut debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<Tls13HandshakeSecret, Tls13KeyScheduleError>
	{
		let zeros = self.prf.zeroes_tls13_handshake();
		let secret = mix_tls13_key(&self.secret, &zeros, self.prf, "dummy DH", true, &mut debug);
		self.with_secret(secret)
	}
	fn to_handshake_secret_dhe_psk(&self, dhe_secret: impl DiffieHellmanSharedSecret,
		mut debug: Option<&mut dyn FnMut(TlsDebug)>) -> Result<Tls13HandshakeSecret, Tls13KeyScheduleError>
	{
		let secret = do_dhe(&self.secret, dhe_secret, self.prf, &mut debug)?;
		self.with_secret(secret)
	}
	fn get_binder(&self, partial_ch_hash: &HashOutput2, debug: &mut Option<&mut dyn FnMut(TlsDebug)>,
		received: Option<&[u8]>) -> Result<HashOutput2, Tls13KeyScheduleError>
	{
		//Binders are computed like Finished, but with binder key as base key.
		let basekey = self.prf.tls13_derive_secret(&self.secret,
			&tls13_l(HkdfLabel::Binder(self.external).get()), &[]);
		let finished_key = self.prf.tls13_derive_secret(&basekey, &tls13_l(HkdfLabel::Finished.get()), &[]);
		let ret = self.prf.tls13_hmac(&finished_key, partial_ch_hash);
		__call_debug(debug, TlsDebug::Tls13Binder {
			psk: self.secret.deref(),
			binder_type: if self.external { "external" } else { "resumption" },
			base_key: &basekey,
			finished_key: &finished_key,
			handshake_hash: &partial_ch_hash,
			calculated_mac: &ret,
			received_mac: received,
		});
		Ok(ret)
	}
	pub fn to_exporter(&self, ch_hash: &HashOutput2, mut debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<BoxedExtractor, Tls13KeyScheduleError>
	{
		make_tls13_early_extractor(&self.secret, ch_hash, self.prf, &mut debug).map(|x|{
			BoxedExtractor(_TlsExtractor::Tls13(x))
		})
	}
}

///TLS 1.3 Early secret (client)
pub struct Tls13EarlySecretClient(Tls13EarlySecret);

impl Tls13EarlySecretClient
{
	pub fn new(psk: &[u8], external: bool, protector: ProtectorType, prf: HashFunction2,
		exportable: Exportable, ad: HasAd, debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<Tls13EarlySecretClient, Tls13KeyScheduleError>
	{
		Ok(Tls13EarlySecretClient(Tls13EarlySecret::new(psk, protector, prf, IsServer(false), exportable,
			ad, external, debug)?))
	}
	///Generate binder.
	pub fn get_binder(&self, partial_ch_hash: &HashOutput2, mut debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<HashOutput2, Tls13KeyScheduleError>
	{
		self.0.get_binder(partial_ch_hash, &mut debug, None)
	}
	///Transform to handshake secret, pure-psk case.
	pub fn to_handshake_secret_pure_psk(&self, debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<Tls13HandshakeSecret, Tls13KeyScheduleError>
	{
		self.0.to_handshake_secret_pure_psk(debug)
	}
	///Transform to handshake secret, with DH mixed.
	pub fn to_handshake_secret_dhe_psk(&self, dhe_secret: impl DiffieHellmanSharedSecret,
		debug: Option<&mut dyn FnMut(TlsDebug)>) -> Result<Tls13HandshakeSecret, Tls13KeyScheduleError>
	{
		self.0.to_handshake_secret_dhe_psk(dhe_secret, debug)
	}
	///Transform to handshake secret, with DH mixed, tls13-cert-with-extern-psk
	pub fn to_handshake_secret_dhe_psk_cert(&self, dhe_secret: impl DiffieHellmanSharedSecret,
		debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<UnsignedTls13HandshakeSecret, Tls13KeyScheduleError>
	{
		Ok(UnsignedTls13HandshakeSecret(self.0.to_handshake_secret_dhe_psk(dhe_secret, debug)?))
	}
	///Get early exporter.
	pub fn get_early_exporter(&self, ch_hash: &HashOutput2, debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<BoxedExtractor, Tls13KeyScheduleError>
	{
		self.0.to_exporter(ch_hash, debug)
	}
	///Get early data protector.
	pub fn get_early_protector(&self, ch_hash: &HashOutput2, mut debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<Tls13TrafficSecretOut, Tls13KeyScheduleError>
	{
		Tls13TrafficSecretOut::new_early(&self.0.secret, ch_hash, self.0.protector, self.0.prf,
			self.0.exportable, self.0.ad, &mut debug)
	}
}

///TLS 1.3 Early secret (server)
pub struct Tls13EarlySecretServer(Tls13EarlySecret);

impl Tls13EarlySecretServer
{
	pub fn new(psk: &[u8], external: bool, protector: ProtectorType, prf: HashFunction2,
		exportable: Exportable, ad: HasAd, debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<Tls13EarlySecretServer, Tls13KeyScheduleError>
	{
		Ok(Tls13EarlySecretServer(Tls13EarlySecret::new(psk, protector, prf, IsServer(true), exportable,
			ad, external, debug)?))
	}
	fn check_binder(&self, partial_ch_hash: HashOutput2, binder: &[u8],
		debug: &mut Option<&mut dyn FnMut(TlsDebug)>) -> Result<(), Tls13KeyScheduleError>
	{
		let reference = self.0.get_binder(&partial_ch_hash, debug, Some(binder))?;
		fail_if!(!slice_eq_ct(reference.deref(), binder), Tls13KeyScheduleError::BadBinder);
		Ok(())
	}
	///Transform to handshake secret, pure-psk case.
	pub fn to_handshake_secret_pure_psk(&self, partial_ch_hash: HashOutput2, binder: &[u8],
		mut debug: Option<&mut dyn FnMut(TlsDebug)>) -> Result<Tls13HandshakeSecret, Tls13KeyScheduleError>
	{
		self.check_binder(partial_ch_hash, binder, &mut debug)?;
		self.0.to_handshake_secret_pure_psk(debug)
	}
	///Transform to handshake secret, with DH mixed.
	pub fn to_handshake_secret_dhe_psk(&self, dhe_secret: impl DiffieHellmanSharedSecret,
		partial_ch_hash: HashOutput2, binder: &[u8], mut debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<Tls13HandshakeSecret, Tls13KeyScheduleError>
	{
		self.check_binder(partial_ch_hash, binder, &mut debug)?;
		self.0.to_handshake_secret_dhe_psk(dhe_secret, debug)
	}
	///Get early exporter.
	pub fn get_early_exporter(&self, ch_hash: &HashOutput2, debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<BoxedExtractor, Tls13KeyScheduleError>
	{
		self.0.to_exporter(ch_hash, debug)
	}
	///Get early data deprotector.
	pub fn get_early_deprotector(&self, ch_hash: &HashOutput2, mut debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<Tls13TrafficSecretIn, Tls13KeyScheduleError>
	{
		Tls13TrafficSecretIn::new_early(&self.0.secret, ch_hash, self.0.protector, self.0.prf,
			self.0.exportable, self.0.ad, &mut debug)
	}
}

fn do_dhe(secret: &HashOutput2, dhe_secret: impl DiffieHellmanSharedSecret, prf: HashFunction2,
	debug: &mut Option<&mut dyn FnMut(TlsDebug)>) -> Result<HashOutput2, Tls13KeyScheduleError>
{
	let dhe_secret = dhe_secret.get_secret(crate::private::PrivateMarker);
	let dhe_secret = dhe_secret.as_raw();
	let dhe_secret_len = dhe_secret.len();
	sanity_check!(dhe_secret_len >= 32, "Diffie-Hellman output too short ({dhe_secret_len}, minimum 32)");
	let secret = mix_tls13_key(secret, dhe_secret, prf, "DH result", true, debug);
	Ok(secret)
}

///TLS 1.3 handshake secret.
pub struct Tls13HandshakeSecret
{
	secret: HashOutput2,
	protector: ProtectorType,
	prf: HashFunction2,
	is_server: IsServer,
	exportable: Exportable,
	ad: HasAd,
}

impl Tls13HandshakeSecret
{
	fn new_dhe(dhe_secret: impl DiffieHellmanSharedSecret, protector: ProtectorType, prf: HashFunction2,
		is_server: IsServer, exportable: Exportable, ad: HasAd,
		mut debug: Option<&mut dyn FnMut(TlsDebug)>) -> Result<Tls13HandshakeSecret, Tls13KeyScheduleError>
	{
		let secret = prf.zeroes_tls13_handshake();
		let secret = mix_tls13_key(&secret, &secret, prf, "dummy PSK", false, &mut debug);
		let secret = do_dhe(&secret, dhe_secret, prf, &mut debug)?;
		Ok(Tls13HandshakeSecret {
			secret: secret,
			protector: protector,
			prf: prf,
			is_server: is_server,
			exportable: exportable,
			ad: ad,
		})
	}
	pub fn new_client(dhe_secret: impl DiffieHellmanSharedSecret, protector: ProtectorType,
		prf: HashFunction2, exportable: Exportable, ad: HasAd, debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<UnsignedTls13HandshakeSecret, Tls13KeyScheduleError>
	{
		Ok(UnsignedTls13HandshakeSecret(Tls13HandshakeSecret::new_dhe(dhe_secret, protector, prf,
			IsServer(false), exportable, ad, debug)?))
	}
	pub fn new_server(dhe_secret: impl DiffieHellmanSharedSecret, protector: ProtectorType,
		prf: HashFunction2, exportable: Exportable, ad: HasAd, debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<Tls13HandshakeSecret, Tls13KeyScheduleError>
	{
		Tls13HandshakeSecret::new_dhe(dhe_secret, protector, prf, IsServer(true), exportable, ad, debug)
	}
	//The hs_hash is after ServerHello.
	pub fn to_traffic_secrets(&self, hash_ch_to_sh: &HashOutput2,
		mut debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<(Tls13TrafficSecretIn, Tls13TrafficSecretOut), Tls13KeyScheduleError>
	{
		let ts_in = Tls13TrafficSecretIn::new(&self.secret, hash_ch_to_sh, self.protector, self.prf,
			self.is_server, false, self.exportable, self.ad, &mut debug)?;
		let ts_out = Tls13TrafficSecretOut::new(&self.secret, hash_ch_to_sh, self.protector, self.prf,
			self.is_server, false, self.exportable, self.ad, &mut debug)?;
		Ok((ts_in, ts_out))
	}
	pub fn to_master_secret(&self, mut debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<Tls13MasterSecret, Tls13KeyScheduleError>
	{
		let zeroes = self.prf.zeroes_tls13_handshake();
		let master_secret = mix_tls13_key(&self.secret, &zeroes, self.prf, "mixes done", true, &mut debug);
		Ok(Tls13MasterSecret{
			secret: master_secret,
			protector: self.protector,
			prf: self.prf,
			is_server: self.is_server,
			exportable: self.exportable,
			ad: self.ad,
		})
	}
}

///TLS 1.3 master secret.
pub struct Tls13MasterSecret
{
	secret: HashOutput2,
	protector: ProtectorType,
	prf: HashFunction2,
	is_server: IsServer,
	exportable: Exportable,
	ad: HasAd,
}

impl Tls13MasterSecret
{
	///Construct application traffic secrets and exporter.
	///
	///The hash is from client hello to server finished.
	pub fn to_traffic_secrets(&self, hash_ch_to_sfin: &HashOutput2,
		mut debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<(Tls13TrafficSecretIn, Tls13TrafficSecretOut, BoxedExtractor), Tls13KeyScheduleError>
	{
		let ts_in = Tls13TrafficSecretIn::new(&self.secret, hash_ch_to_sfin, self.protector, self.prf,
			self.is_server, true, self.exportable, self.ad, &mut debug)?;
		let ts_out = Tls13TrafficSecretOut::new(&self.secret, hash_ch_to_sfin, self.protector, self.prf,
			self.is_server, true, self.exportable, self.ad, &mut debug)?;
		let exporter = make_tls13_extractor(&self.secret, hash_ch_to_sfin, self.prf, &mut debug).map(|x|{
			BoxedExtractor(_TlsExtractor::Tls13(x))
		})?;
		Ok((ts_in, ts_out, exporter))
	}
	///Construct resumption master secret.
	///
	///The hash is from client hello to client finished.
	pub fn to_resumption_master_secret(&mut self, hash_ch_to_cfin: &HashOutput2,
		mut debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<Tls13ResumptionMasterSecret, Tls13KeyScheduleError>
	{
		Tls13ResumptionMasterSecret::new(&self.secret, hash_ch_to_cfin, self.prf, &mut debug)
	}
}

fn ts_sanity_checks(master_key: &HashOutput2, hs_hash: &HashOutput2, prf: HashFunction2) ->
	Result<(), Tls13KeyScheduleError>
{
	let mkey_len = master_key.len();
	let hshash_len = hs_hash.len();
	let prf_olen = prf.output_length();
	let prf_slen = prf.tls13_secret_length();
	sanity_check!(mkey_len == prf_slen, F "Wrong master key length ({mkey_len}, expected {prf_slen})");
	sanity_check!(hshash_len == prf_olen, F "Wrong handshake hash length ({hshash_len}, expected {prf_olen})");
	Ok(())
}

///TLS 1.3 traffic secret (input)
pub struct Tls13TrafficSecretIn
{
	secret: HashOutput2,
	protector: ProtectorType,
	prf: HashFunction2,
	is_server: IsServer,
	application: bool,
	exportable: Exportable,
	ad: HasAd,
}

impl Tls13TrafficSecretIn
{
	fn new(master_key: &HashOutput2, hs_hash: &HashOutput2, protector: ProtectorType, prf: HashFunction2,
		is_server: IsServer, application: bool, exportable: Exportable, ad: HasAd,
		debug: &mut Option<&mut dyn FnMut(TlsDebug)>) -> Result<Tls13TrafficSecretIn, Tls13KeyScheduleError>
	{
		ts_sanity_checks(master_key, hs_hash, prf)?;
		//This is read side, so server_write should be asserted on client.
		let secret = compute_tls13_traffic_secret(master_key, hs_hash, prf, application, !is_server.0, debug);
		Ok(Tls13TrafficSecretIn{secret, protector, prf, is_server, application, exportable, ad})
	}
	fn new_early(master_key: &HashOutput2, hs_hash: &HashOutput2, protector: ProtectorType, prf: HashFunction2,
		exportable: Exportable, ad: HasAd, debug: &mut Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<Tls13TrafficSecretIn, Tls13KeyScheduleError>
	{
		ts_sanity_checks(master_key, hs_hash, prf)?;
		//This is always server side.
		let secret = compute_tls13_early_traffic_secret(master_key, hs_hash, prf, debug);
		Ok(Tls13TrafficSecretIn {
			secret, protector, prf, exportable, ad,
			is_server: IsServer(true),
			application: false,
		})
	}
	pub fn reveal<F>(&self, cb: F) -> Result<(), Tls13KeyScheduleError>
		where F: FnOnce(HashFunction2, ProtectorType, &[u8])
	{
		fail_if!(!self.exportable.0, assert_failure!("Can not reveal() non-exportable keys"));
		cb(self.prf, self.protector, &self.secret);
		Ok(())
	}
	pub fn to_deprotector(&self, mut debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<BoxedDeprotector, Tls13KeyScheduleError>
	{
		fail_if!(self.exportable.0, assert_failure!("Exportable keys can not be turned to deprotector"));
		use super::tls13::Deprotector;
		let mut keyblock = [0; MAX_KEY_CHUNK];
		let tlen = compute_enc_keys(&mut keyblock, &self.secret, self.protector, self.prf)?;
		let d = create_deprotector!(&mut debug, self.protector, keyblock, tlen, 12,
			Tls13RecordMode::from_version(!self.application, self.ad));
		Ok(BoxedDeprotector(_RecordDeprotector::Tls13(d)))
	}
	pub fn check_finished(&self, hs_hash: &HashOutput2, finished: &[u8],
		mut debug: Option<&mut dyn FnMut(TlsDebug)>) -> Result<(), Tls13KeyScheduleError>
	{
		let mac = __compute_tls13_finished(&self.secret, hs_hash, self.prf, &mut debug, Some(finished));
		fail_if!(!slice_eq_ct(mac.deref(), finished), Tls13KeyScheduleError::BadFinishedMac);
		__call_event(&mut debug, TlsDebug::FinishedOk{
			server: !self.is_server.0
		});
		Ok(())
	}
	pub fn rachet(&mut self, debug: Option<&mut dyn FnMut(TlsDebug)>) -> Result<(), Tls13KeyScheduleError>
	{
		sanity_check!(self.application, "Only application data keys can be stepped forward");
		Ok(rachet_common(&mut self.secret, self.prf, debug, false))
	}
	pub fn debug_export(&self) -> &[u8] { &self.secret.deref() }
}

///TLS 1.3 traffic secret (output)
pub struct Tls13TrafficSecretOut
{
	secret: HashOutput2,
	protector: ProtectorType,
	prf: HashFunction2,
	application: bool,
	exportable: Exportable,
	ad: HasAd,
}

impl Tls13TrafficSecretOut
{
	fn new(master_key: &HashOutput2, hs_hash: &HashOutput2, protector: ProtectorType, prf: HashFunction2,
		is_server: IsServer, application: bool, exportable: Exportable, ad: HasAd,
		debug: &mut Option<&mut dyn FnMut(TlsDebug)>) -> Result<Tls13TrafficSecretOut, Tls13KeyScheduleError>
	{
		ts_sanity_checks(master_key, hs_hash, prf)?;
		//This is write side, so server_write should be asserted on server.
		let secret = compute_tls13_traffic_secret(master_key, hs_hash, prf, application, is_server.0, debug);
		Ok(Tls13TrafficSecretOut{secret, protector, prf, application, exportable, ad})
	}
	fn new_early(master_key: &HashOutput2, hs_hash: &HashOutput2, protector: ProtectorType, prf: HashFunction2,
		exportable: Exportable, ad: HasAd, debug: &mut Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<Tls13TrafficSecretOut, Tls13KeyScheduleError>
	{
		ts_sanity_checks(master_key, hs_hash, prf)?;
		//This is always client side.
		let secret = compute_tls13_early_traffic_secret(master_key, hs_hash, prf, debug);
		Ok(Tls13TrafficSecretOut {
			secret, protector, prf, exportable, ad,
			application: false,
		})
	}
	pub fn reveal<F>(&self, cb: F) -> Result<(), Tls13KeyScheduleError>
		where F: FnOnce(HashFunction2, ProtectorType, &[u8])
	{
		fail_if!(!self.exportable.0, assert_failure!("Can not reveal() non-exportable keys"));
		cb(self.prf, self.protector, &self.secret);
		Ok(())
	}
	pub fn to_protector(&self, mut debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<BoxedProtector, Tls13KeyScheduleError>
	{
		fail_if!(self.exportable.0, assert_failure!("Exportable keys can not be turned to protector"));
		use super::tls13::Protector;
		let mut keyblock = [0; MAX_KEY_CHUNK];
		let tlen = compute_enc_keys(&mut keyblock, &self.secret, self.protector, self.prf)?;
		//Nominally allow fake CCS for headers. Actually this does not matter for protection, since
		//procetion never checks for fake CCS.
		let p = create_protector!(&mut debug, self.protector, keyblock, tlen, 12,
			Tls13RecordMode::from_version(!self.application, self.ad));
		Ok(BoxedProtector(_RecordProtector::Tls13(p)))
	}
	pub fn rachet(&mut self, debug: Option<&mut dyn FnMut(TlsDebug)>) -> Result<(), Tls13KeyScheduleError>
	{
		sanity_check!(self.application, "Only application data keys can be stepped forward");
		Ok(rachet_common(&mut self.secret, self.prf, debug, true))
	}
	pub fn debug_export(&self) -> &[u8] { &self.secret.deref() }
}

impl GenerateFinished for Tls13TrafficSecretOut
{
	fn generate_finished(&self, hs_hash: &HashOutput2, mut debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<FinishedValue, AssertFailed>
	{
		Ok(__compute_tls13_finished(&self.secret, hs_hash, self.prf, &mut debug, None))
	}
}

///TLS 1.3 resumption master secret
pub struct Tls13ResumptionMasterSecret
{
	secret: HashOutput2,
	prf: HashFunction2,
}

impl Tls13ResumptionMasterSecret
{
	fn new(master_key: &HashOutput2, hs_hash: &HashOutput2, prf: HashFunction2,
		debug: &mut Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<Tls13ResumptionMasterSecret, Tls13KeyScheduleError>
	{
		ts_sanity_checks(master_key, hs_hash, prf)?;
		let label = HkdfLabel::ResumptionMaster;
		let label = label.get();
		let masterkey = prf.tls13_derive_secret(&master_key, &tls13_l(label), hs_hash);
		__call_debug(debug, TlsDebug::Tls13ResumptionMasterSecret {
			master_secret: &master_key,
			handshake_hash: &hs_hash,
			resumption_master_secret: &masterkey,
		});
		Ok(Tls13ResumptionMasterSecret{
			//This is read side, so server_write should be asserted on client.
			secret: masterkey,
			prf: prf,
		})
	}
	pub fn to_psk(&self, nonce: &[u8], mut debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<HashOutput2, Tls13KeyScheduleError>
	{
		let noncelen = nonce.len();
		sanity_check!(noncelen <= 255, "Ticket nonce too long ({noncelen} > 255)");
		//Abuse tls13_derive_secret() here. This works because tls13_derive_secret() does not actually
		//have the Transcript-Hash operation from Derive-Secret, but assumes that already has been done.
		//so the passed context info ends up raw into tls13_hkdf_expand_label().
		let psk = self.prf.tls13_derive_secret(&self.secret, &tls13_l(HkdfLabel::Resumption.get()), nonce);
		__call_debug(&mut debug, TlsDebug::Tls13ResumptionPsk {
			resumption_master_secret: &self.secret,
			ticket_nonce: nonce,
			resumption_psk: &psk,
		});
		Ok(psk)
	}
}


//This uses the post-server-finished hash.
fn make_tls13_extractor(master_key: &HashOutput2, ems_hash: &HashOutput2, prf: HashFunction2,
	debug: &mut Option<&mut dyn FnMut(TlsDebug)>) ->
	Result<Box<crate::tls13::Extractor>, Tls13KeyScheduleError>
{
	ts_sanity_checks(master_key, ems_hash, prf)?;
	let masterkey = prf.tls13_derive_secret(&master_key, &tls13_l(HkdfLabel::ExporterM.get()), ems_hash);
	__call_debug(debug, TlsDebug::Tls13MasterExporter {
		master_secret: &master_key,
		handshake_hash: &ems_hash,
		exporter_master_secret: &masterkey,
	});
	Ok(Box::new(super::tls13::Extractor::new(master_key, prf)?))
}

//This uses the post-client-hello hash.
fn make_tls13_early_extractor(master_key: &HashOutput2, ch_hash: &HashOutput2, prf: HashFunction2,
	debug: &mut Option<&mut dyn FnMut(TlsDebug)>) ->
	Result<Box<crate::tls13::Extractor>, Tls13KeyScheduleError>
{
	ts_sanity_checks(master_key, ch_hash, prf)?;
	let masterkey = prf.tls13_derive_secret(&master_key, &tls13_l(HkdfLabel::ExporterE.get()), ch_hash);
	__call_debug(debug, TlsDebug::Tls13EarlyExporter {
		master_secret: &master_key,
		handshake_hash: &ch_hash,
		exporter_master_secret: &masterkey,
	});
	Ok(Box::new(super::tls13::Extractor::new(master_key, prf)?))
}
