//!TLS 1.2/1.3 key schedule
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]
pub use self::tls12_ks::*;
pub use self::tls13_ks::*;
use btls_aux_aead::MAX_KEY;
use btls_aux_aead::MAX_NONCE;
use btls_aux_aead::ProtectorType;
use btls_aux_assert::AssertFailed;
use btls_aux_collections::Box;
use btls_aux_dhf::KEM;
use btls_aux_dhf::KemCiphertext;
use btls_aux_dhf::KemInitiator;
use btls_aux_dhf::KemPlaintext;
use btls_aux_dhf::KemResponder;
use btls_aux_fail::fail_if;
use btls_aux_hash::HashOutput2;
use btls_aux_random::TemporaryRandomLifetimeTag;
use btls_aux_random::TemporaryRandomStream;
use btls_aux_signatures::SignatureAlgorithmTls2;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::ops::Deref;
use core::ops::Range;

macro_rules! key_debug
{
	($sink:expr, $value:expr) => {{
		let _sink: &mut Option<&mut dyn FnMut(TlsDebug)> = $sink;
		//This is just to type-check the argument. DCE will just kill the code evaluating the
		//argument.
		#[cfg(not(feature = "enable-key-debug"))]
		{ if false { let _: TlsDebug = $value; }}
		#[cfg(feature = "enable-key-debug")]
		{ if let Some(_sink) = _sink.as_mut() {
			let _value: TlsDebug = $value;
			_sink(_value)
		}}
	}}
}

fn key_from_block<'a>(keyblock: &'a [u8], keylen: usize) -> Result<&'a [u8], AssertFailed>
{
	use btls_aux_assert::assert_failure;
	let kblocklen = keyblock.len();
	keyblock.get(..keylen).ok_or_else(||{
		assert_failure!(F "Key block too small ({kblocklen}, need {keylen})")
	})
}

macro_rules! create_protector {
	($debug:expr, $prot:expr, $keyblock:expr, $keylen:expr, $aes_iv:expr, $special:expr) =>  {{
		let key = crate::key_from_block(&$keyblock, $keylen)?;
		__call_debug($debug, TlsDebug::KeyingEncryption{
			protector: $prot,
			key: key,
		});
		let error = assert_failure!("Can't create record protector");
		Box::new(Protector::new_x(key, $prot, $special).map_err(|_|error.clone())?)
	}}
}

macro_rules! create_deprotector {
	($debug:expr, $prot:expr, $keyblock:expr, $keylen:expr, $aes_iv:expr, $special:expr) =>  {{
		let key = crate::key_from_block(&$keyblock, $keylen)?;
		__call_debug($debug, TlsDebug::KeyingDecryption{
			protector: $prot,
			key: key,
		});
		let error = assert_failure!("Can't create record deprotector");
		Box::new(Deprotector::new_x(key, $prot, $special).map_err(|_|error.clone())?)
	}}
}

mod null;
mod tls12;
mod tls12_ks;
mod tls13;
mod tls13_ks;


#[derive(Copy,Clone,Debug)] pub struct Exportable(pub bool);
#[derive(Copy,Clone,Debug)] pub struct HasAd(pub bool);
#[derive(Copy,Clone,Debug)] struct IsServer(bool);

const TLS12_FINISHED_LEN: usize = 12;
const MAX_KEY_CHUNK: usize = MAX_KEY + MAX_NONCE;	//32 bytes key material + 12 bytes IV.
const MAX_TLS_RECORD_PLAIN: usize = 16384;
const PROTO_CCS: u8 = 20;
const PROTO_APPDATA: u8 = 23;

//The record header for TLS 1.3.
enum Tls13RecordHeader
{
	//TLS 1.2 records, no AD.
	Tls12,
	//TLS 1.2 records, with AD.
	Tls12Ad
}

impl Tls13RecordHeader
{
	//Get minor version.
	fn get_minor(&self) -> u8
	{
		match self {
			&Tls13RecordHeader::Tls12 => 3,
			&Tls13RecordHeader::Tls12Ad => 3,
		}
	}
	//Transform record header for AD.
	fn transform_ad<'a>(&self, hdr: &'a [u8]) -> &'a [u8]
	{
		//In Tls12Ad, this is identity, otherwise it is empty slice.
		match self {
			&Tls13RecordHeader::Tls12Ad => hdr,
			_ => &hdr[0..0],
		}
	}
}

//The record mode for TLS 1.3.
pub struct Tls13RecordMode
{
	header: Tls13RecordHeader,
	fake_ccs: bool,
}

impl Tls13RecordMode
{
	fn tls12() -> Tls13RecordMode
	{
		Tls13RecordMode{
			header: Tls13RecordHeader::Tls12,
			fake_ccs: false,
		}
	}
	fn from_version(ccs: bool, ad: HasAd) -> Tls13RecordMode
	{
		Tls13RecordMode {
			header: if ad.0 { Tls13RecordHeader::Tls12Ad } else { Tls13RecordHeader::Tls12 },
			fake_ccs: ccs
		}
	}
	fn get_minor(&self) -> u8 { self.header.get_minor() }
	fn transform_ad<'a>(&self, hdr: &'a [u8]) -> &'a [u8] { self.header.transform_ad(hdr) }
	fn is_fake_ccs(&self, rec: &[u8]) -> bool { self.fake_ccs && rec == &[PROTO_CCS, 3, 3, 0, 1, 1] }
}

//Internal stuff used by TLS 1.2 and TLS 1.3 code.
struct AlgorithmInfo
{
	keylen: usize,		//Key length, maximum 128.
	noncelen: usize,	//Nonce length, minimum 8, maximum 128 or MAX_NONCE, whichever is less.
	tls12_inoncelen: usize,	//TLS 1.2 Implicit nonce length. If !gcmstyle, always noncelen. Else noncelen-8.
	taglen: usize,		//Tag length, maximum 255
	gcmstyle: bool,		//Is GCM style?
}

impl AlgorithmInfo
{
	fn new(algorithm: ProtectorType) -> Result<AlgorithmInfo, ()>
	{
		let keylen = algorithm.get_key_length();
		let noncelen = algorithm.get_nonce_length();
		let taglen = algorithm.get_tag_length();
		let gcmstyle = algorithm.is_gcm_style();
		fail_if!(keylen > 128, ());		//Sanity check.
		fail_if!(noncelen < 8, ());		//TLS 1.2 and 1.3 needs at least 8 byte tag.
		fail_if!(noncelen > MAX_NONCE, ());
		fail_if!(noncelen > 128, ());		//Sanity check.
		fail_if!(taglen > 255, ());		//TLS 1.3 has limit of 255 byte tag.
		Ok(AlgorithmInfo{
			keylen: keylen,
			noncelen: noncelen,
			tls12_inoncelen: noncelen - gcmstyle as usize * 8,
			taglen: taglen,
			gcmstyle: gcmstyle,
		})
	}
}

fn check_range(buffer: &[u8], range: &Range<usize>) -> Result<(), ()>
{
	fail_if!(range.end < range.start, ());
	fail_if!(buffer.len() < range.end, ());
	Ok(())
}

enum _RecordProtector
{
	Null(bool),
	//Box the actual protectors, so they do not get moved around.
	Tls12(Box<crate::tls12::Protector>),
	Tls13(Box<crate::tls13::Protector>),
}

enum _RecordDeprotector
{
	Null,
	//Box the actual deprotectors, so they do not get moved around.
	Tls12(Box<crate::tls12::Deprotector>),
	Tls13(Box<crate::tls13::Deprotector>),
}

enum _TlsExtractor
{
	Null,
	Banned,
	//Box the actual extractors, so they do not get moved around.
	Tls12(Box<crate::tls12::Extractor>),
	Tls13(Box<crate::tls13::Extractor>),
}


///Error from extractor.
#[derive(Clone,Debug)]
#[non_exhaustive]
pub enum ExtractorError
{
	///Assertion failed.
	InternalError(AssertFailed),
	///Extraction not allowed.
	ExtractorNotAllowed,
	///Extraction not ready.
	NotReady,
}

impl Display for ExtractorError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&ExtractorError::InternalError(ref err) => write!(f, "Internal Error: {err}"),
			&ExtractorError::ExtractorNotAllowed =>
				f.write_str("TLS Extractor not allowed without EMS or TLS 1.3"),
			&ExtractorError::NotReady => f.write_str("TLS Extractor not yet ready"),
		}
	}
}

impl From<AssertFailed> for ExtractorError
{
	fn from(e: AssertFailed) -> ExtractorError { ExtractorError::InternalError(e) }
}

//These traits are not exported outside record module, and the set protector thingies take these.
pub struct BoxedProtector(_RecordProtector);
pub struct BoxedDeprotector(_RecordDeprotector);
pub struct BoxedExtractor(_TlsExtractor);

impl BoxedProtector
{
	pub fn new_null() -> BoxedProtector
	{
		BoxedProtector(_RecordProtector::Null(true))
	}
	pub fn set_modern_version(&mut self)
	{
		//Only NULL has the flag.
		if let &mut _RecordProtector::Null(ref mut flag) = &mut self.0 { *flag = false; }
	}
	///Protect a record inplace.
	///
	///Parameters:
	///
	/// * buf: The input/output buffer.
	/// * rtype: The record type to protect.
	/// * range: Range into buffer that forms the input.
	/// * pad: Number of bytes of padding to request.
	///
	///Note that the input buffer is formatted as follows: pregap | data | postgap. The pregap needs to be
	///at least `get_min_pregap()`, and the postgap needs to be at least `get_min_postgap()`. The pregap
	///is used to write the record headers, and postgap is used for record MAC.
	///
	///Return value:
	///
	/// * On success: Range in buffer that is the protected record.
	/// * On failure: Unit.
	pub fn protect(&mut self, buf: &mut [u8], rtype: u8, range: Range<usize>, pad: usize) ->
		Result<Range<usize>, ()>
	{
		match &mut self.0 {
			&mut _RecordProtector::Null(flag) => crate::null::null_protect(flag, buf, rtype, range),
			&mut _RecordProtector::Tls12(ref mut x) => x.protect(buf, rtype, range, pad),
			&mut _RecordProtector::Tls13(ref mut x) => x.protect(buf, rtype, range, pad),
		}
	}
	///Protect record buffer to buffer.
	///
	///Parameters:
	///
	/// * out: The output buffer.
	/// * inp: The input buffer.
	/// * rtype: The record type to protect.
	/// * max_write_size: The maximum amount of data to protect per record.
	/// * maxsize: If true, always pad records to maximum size.
	///
	///Assumes output buffer is large enough for at least one byte of input (that is, it is strictly larger
	///than minimum pregap plus minimum postgap).
	///
	///Return value:
	///
	/// * On success: Number of bytes written into output buffer, and number of bytes consumed from input
	///buffer (respectively).
	/// * On failure: Unit.
	pub fn protect_coutput(&mut self, out: &mut [u8], inp: &[u8], rtype: u8, max_write_size: usize,
		maxsize: bool) -> Result<(usize, usize), ()>
	{
		match &mut self.0 {
			&mut _RecordProtector::Null(flag) =>
				crate::null::null_protect_coutput(flag, out, inp, rtype, max_write_size),
			&mut _RecordProtector::Tls12(ref mut x) =>
				x.protect_coutput(out, inp, rtype, max_write_size),
			&mut _RecordProtector::Tls13(ref mut x) =>
				x.protect_coutput(out, inp, rtype, max_write_size, maxsize),
		}
	}
	///Like `protect_coutput()`, but takes maybe uninitialized buffer.
	pub fn protect_btb2<'a>(&mut self, rtype: btls_aux_tls_iana::ContentType, src: &[u8],
		dst: &'a mut [core::mem::MaybeUninit<u8>], maxsize: bool) -> Result<&'a [u8], ()>
	{
		match &mut self.0 {
			&mut _RecordProtector::Null(flag) => crate::null::null_protect_btb2(flag, dst, src, rtype),
			&mut _RecordProtector::Tls12(ref mut x) => x.protect_btb2(dst, src, rtype),
			&mut _RecordProtector::Tls13(ref mut x) => x.protect_btb2(dst, src, rtype, maxsize),
		}
	}
	///Get smallest allowed pregap.
	pub fn get_min_pregap(&self) -> usize
	{
		match &self.0 {
			&_RecordProtector::Null(_) => 5,
			&_RecordProtector::Tls12(ref x) => x.get_min_pregap(),
			&_RecordProtector::Tls13(_) => 5,
		}
	}
	///Get smallest allowed postgap.
	pub fn get_min_postgap(&self) -> usize
	{
		match &self.0 {
			&_RecordProtector::Null(_) => 0,
			&_RecordProtector::Tls12(ref x) => x.get_min_postgap(),
			&_RecordProtector::Tls13(ref x) => x.get_min_postgap(),
		}
	}
	///Supports padding?
	pub fn supports_padding(&self) -> bool
	{
		matches!(&self.0, &_RecordProtector::Tls13(_))
	}
	///Get the ciphertext size for given plaintext size.
	pub fn get_ciphertext_size(&self, ptsize: usize) -> usize
	{
		match &self.0 {
			&_RecordProtector::Null(_) => ptsize.saturating_add(5),
			&_RecordProtector::Tls12(ref x) => x.get_ciphertext_size(ptsize),
			&_RecordProtector::Tls13(ref x) => x.get_ciphertext_size(ptsize),
		}
	}
	///Get the largest possible plaintext size for given ciphertext size.
	pub fn get_max_plaintext_size(&self, ctsize: usize) -> usize
	{
		match &self.0 {
			&_RecordProtector::Null(_) => ctsize.saturating_sub(5),
			&_RecordProtector::Tls12(ref x) => x.get_max_plaintext_size(ctsize),
			&_RecordProtector::Tls13(ref x) => x.get_max_plaintext_size(ctsize),
		}
	}
}

impl BoxedDeprotector
{
	pub fn new_null() -> BoxedDeprotector
	{
		BoxedDeprotector(_RecordDeprotector::Null)
	}
	///Deprotect a record inplace.
	///
	///Parameters:
	///
	/// * buf: The input/output buffer.
	///
	///Return value:
	///
	/// * On success: Record type, and range into output buffer (respectively).
	/// * On failure: Unit.
	pub fn deprotect(&mut self, buf: &mut [u8]) -> Result<(u8, Range<usize>), ()>
	{
		match &mut self.0 {
			&mut _RecordDeprotector::Null => crate::null::null_deprotect(buf),
			&mut _RecordDeprotector::Tls12(ref mut x) => x.deprotect(buf),
			&mut _RecordDeprotector::Tls13(ref mut x) => x.deprotect(buf),
		}
	}
	///Deprotect a record buffer-to-buffer.
	///
	///Parameters:
	///
	/// * src: The input buffer.
	/// * dst: The output buffer.
	///
	///Assumes output buffer is at least the size of input buffer.
	///
	///Return value:
	///
	/// * On success: Record type, and size of output (respectively).
	/// * On failure: Unit.
	pub fn deprotect_btb(&mut self, src: &[u8], dst: &mut [u8]) -> Result<(u8, usize), ()>
	{
		match &mut self.0 {
			&mut _RecordDeprotector::Null => crate::null::null_deprotect_btb(src, dst),
			&mut _RecordDeprotector::Tls12(ref mut x) => x.deprotect_btb(src, dst),
			&mut _RecordDeprotector::Tls13(ref mut x) => x.deprotect_btb(src, dst),
		}
	}
	///Like `deprotect_btb()`, but takes uninitialized target buffer and spits out the plaintext instead of
	///its length.
	pub fn deprotect_btb2<'a>(&mut self, src: &[u8], dst: &'a mut [core::mem::MaybeUninit<u8>]) ->
		Result<(btls_aux_tls_iana::ContentType, &'a [u8]), ()>
	{
		match &mut self.0 {
			&mut _RecordDeprotector::Null => crate::null::null_deprotect_btb2(src, dst),
			&mut _RecordDeprotector::Tls12(ref mut x) => x.deprotect_btb2(src, dst),
			&mut _RecordDeprotector::Tls13(ref mut x) => x.deprotect_btb2(src, dst),
		}
	}
}

impl BoxedExtractor
{
	pub fn new_null() -> BoxedExtractor
	{
		BoxedExtractor(_TlsExtractor::Null)
	}
	///Get output from TLS extractor.
	///
	///Parameters:
	///
	/// * label: The label to extract.
	/// * context: Optional context.
	/// * buffer: The output buffer (filled).
	/// * debug: Debugger context.
	pub fn extract(&self, label: &str, context: Option<&[u8]>, buffer: &mut [u8],
		debug: &mut Option<&mut dyn FnMut(TlsDebug)>) -> Result<(), ExtractorError>
	{
		match &self.0 {
			&_TlsExtractor::Null => Err(ExtractorError::NotReady),
			&_TlsExtractor::Banned => Err(ExtractorError::ExtractorNotAllowed),
			&_TlsExtractor::Tls12(ref x) => x.extract(label, context, buffer, debug),
			&_TlsExtractor::Tls13(ref x) => x.extract(label, context, buffer, debug),
		}
	}
	pub fn debug_export(&self) -> &[u8]
	{
		//This is nontrivial only for TLS 1.3.
		match &self.0 {
			&_TlsExtractor::Tls13(ref x) => x.debug_export(),
			_ => &[]
		}
	}
}

///TLS finished value.
///
///For getting the raw octet string value, use the `Deref` conversion to `&[u8]`.
pub struct FinishedValue(_FinishedValue);
pub enum _FinishedValue
{
	Tls12([u8;TLS12_FINISHED_LEN]),
	Tls13(HashOutput2)
}

impl Deref for FinishedValue
{
	type Target = [u8];
	fn deref(&self) -> &[u8]
	{
		match &self.0 {
			&_FinishedValue::Tls12(ref h) => h,
			&_FinishedValue::Tls13(ref h) => h,
		}
	}
}

///Private call marker.
mod private
{
	pub struct PrivateMarker;
}

pub trait DiffieHellmanSharedSecret
{
	fn get_secret(self, _: self::private::PrivateMarker) -> KemPlaintext;
}

///DH shared result.
pub struct DiffieHellmanSharedSecretInitiator
{
	secret: KemPlaintext,
	kem: KEM,
}

impl DiffieHellmanSharedSecret for DiffieHellmanSharedSecretInitiator
{
	fn get_secret(self, _: self::private::PrivateMarker) -> KemPlaintext { self.secret }
}

///Error from DH agreement.
#[non_exhaustive]
pub enum DiffieHellmanError
{
	AgreeError(u16),
}

impl DiffieHellmanSharedSecretInitiator
{
	pub fn new(private_key: KemInitiator, peer_public: &[u8]) ->
		Result<DiffieHellmanSharedSecretInitiator, DiffieHellmanError>
	{
		let kem = private_key.kem();
		let secret = private_key.decapsulate(peer_public).
			map_err(|_|DiffieHellmanError::AgreeError(kem.tls_id()))?;
		Ok(DiffieHellmanSharedSecretInitiator {
			secret: secret,
			kem: kem
		})
	}
	pub fn get_kem(&self) -> KEM { self.kem }
}

impl DiffieHellmanSharedSecret for DiffieHellmanSharedSecretResponder
{
	fn get_secret(self, _: self::private::PrivateMarker) -> KemPlaintext { self.secret }
}

///DH shared result.
pub struct DiffieHellmanSharedSecretResponder
{
	secret: KemPlaintext,
	public_key: KemCiphertext,
	kem: KEM,
}

impl DiffieHellmanSharedSecretResponder
{
	pub fn new(private_key: KemResponder, peer_public: &[u8]) ->
		Result<DiffieHellmanSharedSecretResponder, DiffieHellmanError>
	{
		let kem = private_key.kem();
		let lifetime = TemporaryRandomLifetimeTag;
		let (secret, public_key) = private_key.encapsulate(peer_public,
			&mut TemporaryRandomStream::new_system(&lifetime)).
			map_err(|_|DiffieHellmanError::AgreeError(kem.tls_id()))?;
		Ok(DiffieHellmanSharedSecretResponder {
			secret: secret,
			public_key: public_key,
			kem: kem
		})
	}
	pub fn get_kem(&self) -> KEM { self.kem }
	pub fn get_public_key(&self) -> KemCiphertext { self.public_key.clone() }
}

#[allow(missing_docs)]
///Debug values for TLS.
#[non_exhaustive]
pub enum TlsDebug<'a>
{
	Tls13Binder{
		psk: &'a [u8],
		binder_type: &'a str,
		base_key: &'a [u8],
		finished_key: &'a [u8],
		handshake_hash: &'a [u8],
		calculated_mac: &'a [u8],
		received_mac: Option<&'a [u8]>,
	},
	Tls13Finished{
		base_key: &'a [u8],
		finished_key: &'a [u8],
		handshake_hash: &'a [u8],
		calculated_mac: &'a [u8],
		received_mac: Option<&'a [u8]>,
	},
	Tls13Exporter{
		exporter_master_secret: &'a [u8],
		label: &'a str,
		per_label_key: &'a [u8],
		context: &'a [u8],
		exported_value: &'a [u8],
	},
	Tls13EarlyExporter{
		master_secret: &'a [u8],
		handshake_hash: &'a [u8],
		exporter_master_secret: &'a [u8],
	},
	Tls13MasterExporter{
		master_secret: &'a [u8],
		handshake_hash: &'a [u8],
		exporter_master_secret: &'a [u8],
	},
	Tls13BumpTrafficSecret{
		is_tx: bool,
		old: &'a [u8],
		new: &'a [u8],
	},
	Tls13TrafficSecret{
		label: &'a str,
		master_secret: &'a [u8],
		handshake_hash: &'a [u8],
		traffic_secret: &'a [u8],
	},
	Tls13ResumptionPsk {
		resumption_master_secret: &'a [u8],
		ticket_nonce: &'a [u8],
		resumption_psk: &'a [u8],
	},
	Tls13PreMixExpansion {
		old: &'a [u8],
		new: &'a [u8],
	},
	Tls13KeyMix{
		key_name: &'a str,
		old: &'a [u8],
		input: &'a [u8],
		new: &'a [u8],
	},
	Tls13ResumptionMasterSecret{
		master_secret: &'a [u8],
		handshake_hash: &'a [u8],
		resumption_master_secret: &'a [u8],
	},
	Tls12Finished{
		master_secret: &'a [u8],
		handshake_hash: &'a [u8],
		calculated_mac: &'a [u8],
		received_mac: Option<&'a [u8]>,
	},
	Tls12PremasterSecret{
		premaster_secret: &'a [u8],
		compressed_premaster_secret: &'a [u8],
	},
	Tls12MasterSecret{
		extended: bool,
		premaster_secret: &'a [u8],
		handshake_hash: &'a [u8],
		client_random: &'a [u8],
		server_random: &'a [u8],
		master_secret: &'a [u8],
	},
	Tls12Exporter{
		master_secret: &'a [u8],
		label: &'a str,
		context: Option<&'a [u8]>,
		client_random: &'a [u8],
		server_random: &'a [u8],
		exported_value: &'a [u8],
	},
	KeyingEncryption{
		protector: ProtectorType,
		key: &'a [u8],
	},
	KeyingDecryption{
		protector: ProtectorType,
		key: &'a [u8],
	},
	TlsServerSignature{
		public_key: &'a [u8],
		server_tbs: &'a [u8],
		algorithm: SignatureAlgorithmTls2,
		signature: &'a [u8],
	},
	FinishedOk {
		server: bool,
	},
}


#[cfg(feature = "enable-key-debug")]
fn __call_debug(debug: &mut Option<&mut dyn FnMut(TlsDebug)>, val: TlsDebug)
{
	if let Some(debug) = debug.as_mut() { (debug)(val) }
}

#[cfg(not(feature = "enable-key-debug"))]
fn __call_debug(_: &mut Option<&mut dyn FnMut(TlsDebug)>, _: TlsDebug) {}

fn __call_event(debug: &mut Option<&mut dyn FnMut(TlsDebug)>, val: TlsDebug)
{
	if let Some(debug) = debug.as_mut() { (debug)(val) }
}


///generates finished message.
pub trait GenerateFinished
{
	///Generate a finished message.
	fn generate_finished(&self, hs_hash: &HashOutput2, debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<FinishedValue, AssertFailed>;
}


