use super::__call_debug;
use super::AlgorithmInfo;
use super::check_range;
use super::ExtractorError;
use super::Tls12KeyScheduleError;
use super::Tls13RecordMode;
use super::TlsDebug;
use btls_aux_aead::DecryptionKey;
use btls_aux_aead::EncryptionKey;
use btls_aux_aead::MAX_NONCE;
use btls_aux_aead::ProtectorType;
use btls_aux_assert::sanity_check;
use btls_aux_assert::sanity_failed;
use btls_aux_fail::dtry;
use btls_aux_fail::fail_if;
use btls_aux_hash::HashFunction2;
use btls_aux_memory::copy_maximum;
use btls_aux_memory::slice_splitn_at_exact;
use btls_aux_tls_iana::ContentType;
use core::cmp::min;
use core::mem::MaybeUninit;
use core::ops::Range;
use core::ptr::copy_nonoverlapping;
use core::ptr::read_unaligned;
use core::ptr::write_unaligned;


const MS_LEN: usize = 48;

fn increment_rsn(rsn: &mut u64, destroyed: &mut bool)
{
	//Increment the RSN, checking for overflows.
	*rsn = match rsn.checked_add(1) {
		Some(x) => x,
		None => {*destroyed = true; *rsn }
	};
}

//Assumes nonce <= MAX_NONCE-8.
fn make_iv_and_ad_encrypt(iv: &[u8; MAX_NONCE], rtype: u8, nonce: usize, rsn: u64, psize: usize) ->
	Result<([u8; MAX_NONCE], [u8; 13]), ()>
{
	let mut _iv: [u8; MAX_NONCE] = *iv;
	let mut _ad: [u8; 13] = [0, 0, 0, 0, 0, 0, 0, 0, rtype, 3, 3, 0, 0];
	//Write the packet plaintext size to the AD.
	(&mut _ad[11..13]).copy_from_slice(&(psize as u16).to_be_bytes());
	//Write the RSN to AD and IV. The IV one is XORed for Chacha20 mode. In GCM mode, we assume the
	//octets are zeroes, so XOR is the same as store.
	let rsn = rsn.to_be_bytes();
	(&mut _ad[0..8]).copy_from_slice(&rsn);
	//This should not happen!
	let nend = nonce + 8;
	fail_if!(!(nend >= nonce && nend <= MAX_NONCE), ());
	let ivnonce = &mut _iv[nonce..nend];
	for i in 0..8usize { ivnonce[i] ^= rsn[i]; }
	Ok((_iv, _ad))
}

//key, IV, RSN, vulernable to NDA, destroyed, tagsize, noncesize
pub struct Protector
{
	key: EncryptionKey,
	iv: [u8;MAX_NONCE],
	rsn: u64,
	nda: bool,
	destroyed: bool,
	tag: usize,
	nonce: usize,
}

pub struct Deprotector
{
	key: DecryptionKey,
	iv: [u8;MAX_NONCE],
	rsn: u64,
	nda: bool,
	destroyed: bool,
	tag: usize,
	nonce: usize
}

pub struct Extractor
{
	secret: [u8; MS_LEN],		//TLS 1.2 MasterSecret is always 48 bytes.
	randoms: [u8; 64],
	function: HashFunction2,
}

const BASE_HEADER_LENGTH: usize = 5;
const CHACHA_HEADER_LENGTH: usize = BASE_HEADER_LENGTH;
const GCM_HEADER_LENGTH: usize = BASE_HEADER_LENGTH + 8;
const MAX_RECORD_PTXT: usize = 16 * 1024;


impl Protector
{
	pub fn new_x(key: &[u8], algorithm: ProtectorType, _dummy: Tls13RecordMode) -> Result<Self, ()>
	{
		let alginfo = AlgorithmInfo::new(algorithm)?;
		//Split the key. key_iv.len() will be at most alginfo.noncelen, which is at most MAX_NONCE.
		//If gcmstyle is set, subtract 8 from nonce length, as those are explicit.
		let (key_k, key_iv) = dtry!(slice_splitn_at_exact(key, (alginfo.keylen, alginfo.tls12_inoncelen)));
		let mut iv = [0; MAX_NONCE];
		copy_maximum(&mut iv, key_iv);
		Ok(Protector{
			key: dtry!(algorithm.new_encryptor(key_k)),
			iv: iv,
			rsn: 0,
			nda: alginfo.gcmstyle,
			destroyed: false,
			tag: alginfo.taglen,
			//The nonce is always offset by 8 bytes, because that is where the nonce gets written to,
			//regardless of if gcmstyle is set or not.
			nonce: alginfo.noncelen - 8,	//Always offset by 8 bytes.
		})
	}
}

impl Protector
{
	fn fixup_header(&self, buf: &mut [u8], _iv: &[u8], _ad: &[u8; 13], hdroff: usize,
		outlen: usize) -> Result<(), ()>
	{
		if self.nda {
			//This should not happen.
			let hend = hdroff + 13;
			let nend = self.nonce + 8;
			let buf = dtry!(buf.get_mut(hdroff..hend));
			let ivnonce = dtry!(_iv.get(self.nonce..nend));
			//The length in header is not the same, so correct it.
			let reallen = outlen as u16 + 8;	//Count the 8 bytes the size.
			(&mut buf[0..3]).copy_from_slice(&_ad[8..11]);
			(&mut buf[3..5]).copy_from_slice(&reallen.to_be_bytes());
			//Copy the used IV for AES-GCM.
			(&mut buf[5..13]).copy_from_slice(ivnonce);
		} else {
			//This should not happen.
			let hend = hdroff + 5;
			let buf = dtry!(buf.get_mut(hdroff..hend));
			let reallen = outlen as u16;
			(&mut buf[0..3]).copy_from_slice(&_ad[8..11]);
			(&mut buf[3..5]).copy_from_slice(&reallen.to_be_bytes());
		}
		Ok(())
	}
	fn check_flag_get_header(&self) -> Result<usize, ()>
	{
		//If destroyed flag is set, fail.
		fail_if!(self.destroyed, ());

		//The size of header is 13 bytes (8+5) in GCM mode, and 5 bytes in Chacha mode.
		let hdrsize = if self.nda { GCM_HEADER_LENGTH } else { CHACHA_HEADER_LENGTH };
		Ok(hdrsize)
	}
	pub(crate) fn protect(&mut self, buf: &mut [u8], rtype: u8, range: Range<usize>, _pad: usize) ->
		Result<Range<usize>, ()>
	{
		let hdrsize = self.check_flag_get_header()?;
		//Check that given range is valid. The specified range has to be in-buffer and range.start has to
		//be at least hdrsize.
		check_range(buf, &range)?;
		fail_if!(range.start < hdrsize, ());
		//Check that message is 16384 bytes maximum. range.end >= range.start by range validity.
		let plaintext_size = range.end - range.start;
		fail_if!(plaintext_size > MAX_RECORD_PTXT, ());
		//Check that there's enough space for tag. buf.len() >= range.end by range validity.
		fail_if!(buf.len() - range.end < self.tag, ());

		//Encrypt the record. We know that:
		// - self.nonce + 8 <= MAX_NONCE = _iv.len().
		// - range.start + plaintext_size = range.end <= buf.len().
		// - range.start + plaintext_size + self.tag = range.end + self.tag <= buf.len().
		let (_iv, _ad) = make_iv_and_ad_encrypt(&self.iv, rtype, self.nonce, self.rsn, plaintext_size)?;
		let _iv = &_iv[..(self.nonce+8)];		//Strip extra part away.
		let outlen = dtry!(self.key.encrypt_inplace(&_iv, &_ad, &mut buf[range.start..], plaintext_size));

		//Compute the range of target fragment. We know that range.start >= hdrsize, and outlen better
		//be plaintext_size + self.tag, which is at most buf.len() - range.start.
		let hdroff = range.start - hdrsize;
		let dend = range.start + outlen;

		self.fixup_header(buf, _iv, &_ad, hdroff, outlen)?;
		increment_rsn(&mut self.rsn, &mut self.destroyed);
		//The range of data after protection.
		Ok(hdroff..dend)
	}
	pub(crate) fn protect_coutput(&mut self, out: &mut [u8], inp: &[u8], rtype: u8, max_write_size: usize) ->
		Result<(usize, usize), ()>
	{
		let hdrsize = self.check_flag_get_header()?;
		let overhead = hdrsize + self.tag;
		//If overhead does not fit into output, no data is taken.
		if out.len() < overhead { return Ok((0, 0)); }
		//Compute the maximum amount of plaintext that fits (capping at MAX_RECORD_PTXT).
		//We know that out.len() >= overhead.
		let len = min(min(MAX_RECORD_PTXT, max_write_size), min(out.len() - overhead, inp.len()));

		//Do buffer-to-buffer encryption. We know that:
		// - len < inp.len()
		// - hdrsize + len + self.tag = ovehead + len <= out.len().
		//-  self.noce + 8 <= MAX_NONCE = _iv.len().
		let (_iv, _ad) = make_iv_and_ad_encrypt(&self.iv, rtype, self.nonce, self.rsn, len)?;
		let _iv = &_iv[..(self.nonce+8)];		//Strip extra part away.
		let outlen = dtry!(self.key.encrypt_buffer_to_buffer(&_iv, &_ad, &mut out[hdrsize..], &inp[..len],
			None)).len();

		self.fixup_header(out, _iv, &_ad, 0, outlen)?;
		increment_rsn(&mut self.rsn, &mut self.destroyed);
		//outlen is at most MAX_RECORD_PTXT+24, so +13 fits.
		Ok((outlen+hdrsize, len))
	}
	pub(crate) fn protect_btb2<'a>(&mut self, dst: &'a mut [MaybeUninit<u8>], src: &[u8],
		rtype: ContentType) -> Result<&'a [u8], ()>
	{
		let hdrsize = self.check_flag_get_header()?;
		let len = src.len();
		fail_if!(dst.len() < hdrsize || len > MAX_RECORD_PTXT, ());
		let (header, payload) = dst.split_at_mut(hdrsize);
		let (_iv, _ad) = make_iv_and_ad_encrypt(&self.iv, rtype.get(), self.nonce, self.rsn, len)?;
		let _iv = &_iv[..(self.nonce+8)];		//Strip extra part away.

		let plen = dtry!(self.key.encrypt_buffer_to_buffer(&_iv, &_ad, payload, src, None)).len();
		let dst: &'a [u8] = unsafe{
			//_ad is 13 bytes, and header is at least 5, so this is sound.
			let plen2 = plen as u16 + self.nda as u16 * 8;
			copy_nonoverlapping(_ad.as_ptr().add(8), header.as_mut_ptr().cast(), 3);
			write_unaligned(header.as_mut_ptr().add(3).cast(), plen2.to_be());
			if self.nda {
				//_iv is self.nonce+8 bytes, and header is 13 bytes, so this is sound.
				let nonce = read_unaligned(_iv.as_ptr().add(self.nonce).cast::<u64>());
				write_unaligned(header.as_mut_ptr().add(5).cast(), nonce);
			}
			//This all has been initialized, so this is sound. The size can not be exceeded, as otherwise
			//encrypt_buffer_to_buffer() would have failed.
			btls_aux_memory::slice_assume_init(dst, hdrsize + plen)
		};
		increment_rsn(&mut self.rsn, &mut self.destroyed);
		Ok(dst)
	}
	pub(crate) fn get_min_pregap(&self) -> usize
	{
		//5 or 13.
		if self.nda { GCM_HEADER_LENGTH } else { CHACHA_HEADER_LENGTH }
	}
	pub(crate) fn get_min_postgap(&self) -> usize
	{
		self.tag	//Just the tag.
	}
	pub(crate) fn get_ciphertext_size(&self, ptsize: usize) -> usize
	{
		let pregap = self.get_min_pregap();
		if let Some(ctsize) = self.key.encrypted_size(ptsize) {
			ctsize.saturating_add(pregap)
		} else {
			//??? That should have worked.
			ptsize.saturating_add(self.tag + pregap)
		}
	}
	pub(crate) fn get_max_plaintext_size(&self, ctsize: usize) -> usize
	{
		let pregap = self.get_min_pregap();
		if let Some(ptsize) = self.key.max_plaintext_size(ctsize.saturating_sub(pregap)) {
			ptsize
		} else {
			//??? That should have worked.
			ctsize.saturating_sub(self.tag + pregap)
		}
	}
}

impl Deprotector
{
	pub fn new_x(key: &[u8], algorithm: ProtectorType, _dummy: Tls13RecordMode) -> Result<Self, ()>
	{
		let alginfo = AlgorithmInfo::new(algorithm)?;
		//Split the key. key_iv.len() will be at most alginfo.noncelen, which is at most MAX_NONCE.
		//If gcmstyle is set, subtract 8 from nonce length, as those are explicit.
		let (key_k, key_iv) = dtry!(slice_splitn_at_exact(key, (alginfo.keylen, alginfo.tls12_inoncelen)));
		let mut iv = [0; MAX_NONCE];
		copy_maximum(&mut iv, key_iv);
		Ok(Deprotector{
			key: dtry!(algorithm.new_decryptor(key_k)),
			iv: iv,
			rsn: 0,
			nda: alginfo.gcmstyle,
			destroyed: false,
			tag: alginfo.taglen,
			//The nonce is always offset by 8 bytes, because that is where the nonce gets written to,
			//regardless of if gcmstyle is set or not.
			nonce: alginfo.noncelen - 8,
		})
	}
	fn compose_iv_ad(&self, _iv: &mut [u8; MAX_NONCE], _ad: &mut [u8; 13], buf: &[u8], hdrsize: usize)
		-> Result<(), ()>
	{
		//If destroyed flag is set, fail.
		fail_if!(self.destroyed, ());

		//The record must at minimum contain a header and a tag (16 bytes). The addition result is under
		//300.
		fail_if!(buf.len() < hdrsize + self.tag, ());

		//The size of the ciphertext and plaintext. We know that buf.len() is at lest hdrsize + self.tag(),
		//and hdrsize.
		let ciphertext_size = buf.len() - hdrsize;
		let plaintext_size = ciphertext_size - self.tag;
		fail_if!(plaintext_size > MAX_RECORD_PTXT, ());

		//Check the length in header. buf.len() is guaranteed to be at least 5(=BASE_HEADER_LENGTH) by the
		//check above. And MAX_RECORD_CTXT - MAX_RECORD_PTXT = 2048, which is bigger than hdrsize + self.tag
		//< 300. So MAX_RECORD_CTXT can not be reached.
		let recsize_header = (buf[3] as usize) * 256 + (buf[4] as usize);
		let recsize_check = buf.len() - BASE_HEADER_LENGTH;
		fail_if!(recsize_header != recsize_check, ());

		//Sanity check.
		fail_if!(buf.len() < if self.nda { GCM_HEADER_LENGTH } else { BASE_HEADER_LENGTH }, ());
		//Assemble the AD. The first eight bytes are the RSN (we don't copy it here), then comes the header,
		//except with the length being amount of plaintext. buf.len() is guaranteed to be at least hdrsize+
		//TAG_LENGTH by check above.
		_ad[8] = buf[0];
		_ad[9] = buf[1];
		_ad[10] = buf[2];
		_ad[11] = (plaintext_size >> 8) as u8;
		_ad[12] = plaintext_size as u8;
		//Assemble the IV assuming we are Chacha mode. If this isn't true, then the wrong data will be
		//overwritten later. Also, fill the RSN filed in AD.
		for i in 0..8usize {
			let byteshift = (!i as u32) & 7;
			let x = (self.rsn >> 8 * byteshift) as u8;	//Byteshift is at most 7.
			_iv[i + self.nonce] ^= x;	//In range by check above.
			_ad[i] = x;
		}
		//If in GCM mode, overwrite the explicit IV part.
		if self.nda {
			(&mut _iv[self.nonce..][..8]).copy_from_slice(&buf[BASE_HEADER_LENGTH..GCM_HEADER_LENGTH]);
		}
		Ok(())
	}
	pub(crate) fn deprotect(&mut self, buf: &mut [u8]) -> Result<(u8, Range<usize>), ()>
	{
		let mut _iv: [u8; MAX_NONCE] = self.iv;
		let mut _ad: [u8; 13] = [0; 13];

		//The size of header is 13 bytes (8+5) in GCM mode, and 5 bytes in Chacha mode.
		let hdrsize = if self.nda { GCM_HEADER_LENGTH } else { CHACHA_HEADER_LENGTH };
		self.compose_iv_ad(&mut _iv, &mut _ad, buf, hdrsize)?;
		//Finally try to decrypt the packet. We know that self.nonce+8<=MAX_NONCE=_iv.len().
		let outlen = {
			let buffer = &mut buf[hdrsize..];
			let _iv = &_iv[..(self.nonce+8)];		//Strip extra part away.
			dtry!(self.key.decrypt_inplace(&_iv, &_ad, buffer))
		};
		//The plaintext data starts at hdrsize and runs for outlen bytes. Now, outlen is at most buf.len()-
		//hdrsize, so this can't overflow usize. Buf is at least 5 bytes.
		let pktend = outlen + hdrsize;
		let rtype = buf[0];
		//Increment the RSN, checking for overflow.
		increment_rsn(&mut self.rsn, &mut self.destroyed);
		Ok((rtype, hdrsize..pktend))
	}
	fn __deprotect_btb<R>(&mut self, src: &[u8],
		f: impl FnOnce(&mut Self, &[u8], &[u8], &[u8]) -> Result<R, ()>) -> Result<(u8, R), ()>
	{
		let mut _iv: [u8; MAX_NONCE] = self.iv;
		let mut _ad: [u8; 13] = [0; 13];

		//The size of header is 13 bytes (8+5) in GCM mode, and 5 bytes in Chacha mode.
		let hdrsize = if self.nda { GCM_HEADER_LENGTH } else { CHACHA_HEADER_LENGTH };
		self.compose_iv_ad(&mut _iv, &mut _ad, src, hdrsize)?;
		//Finally try to decrypt the packet. We know that self.nonce+8<=MAX_NONCE=_iv.len().
		let ret = {
			let ibuffer = &src[hdrsize..];
			let _iv = &_iv[..(self.nonce+8)];		//Strip extra part away.
			f(self, &_iv, &_ad, ibuffer)?
		};
		//The output.
		let rtype = src[0];
		//Increment the RSN, checking for overflow.
		increment_rsn(&mut self.rsn, &mut self.destroyed);
		Ok((rtype, ret))
	}
	pub(crate) fn deprotect_btb(&mut self, src: &[u8], dst: &mut [u8]) -> Result<(u8, usize), ()>
	{
		self.__deprotect_btb(src, move |selfx, iv, ad, input|{
			Ok(dtry!(selfx.key.decrypt_buffer_to_buffer(iv, ad, dst, input)).len())
		})
	}
	pub(crate) fn deprotect_btb2<'a>(&mut self, src: &[u8], dst: &'a mut [core::mem::MaybeUninit<u8>]) ->
		Result<(ContentType, &'a [u8]), ()>
	{
		self.__deprotect_btb(src, move |selfx, iv, ad, input|{
			dtry!(NORET selfx.key.decrypt_buffer_to_buffer(iv, ad, dst, input))
		}).map(|(t,p)|(ContentType::new(t),p))
	}
}

impl Extractor
{
	pub fn new(master_key: &[u8], client_random: &[u8; 32], server_random: &[u8; 32], prf: HashFunction2) ->
		Result<Self, Tls12KeyScheduleError>
	{
		let master_keylen = master_key.len();
		sanity_check!(master_keylen == MS_LEN, F
			"Wrong key length for TLS 1.2 extractor ({master_keylen}, expected {MS_LEN})");
		let mut tmp = Ok(Extractor{secret:[0;MS_LEN], randoms:[0;64], function:prf});
		if let &mut Ok(ref mut inner) = &mut tmp {
			inner.secret.copy_from_slice(master_key);
			(&mut inner.randoms[0..32]).copy_from_slice(client_random);
			(&mut inner.randoms[32..64]).copy_from_slice(server_random);
		} else {
			sanity_failed!("tls12::Extractor::new(): This should be unreachable!");
		}
		tmp
	}
	pub(crate) fn extract(&self, label: &str, context: Option<&[u8]>, buffer: &mut [u8],
		debug: &mut Option<&mut dyn FnMut(TlsDebug)>) -> Result<(), ExtractorError>
	{
		let func = self.function;
		//Check that nobody tries to use reserved for TLS 1.2 labels.
		sanity_check!(label != "client finished" && label != "server finished" &&
			label != "master secret" && label != "key expansion", "Label is reserved for TLS 1.2");
		if let Some(ctx) = context {
			let ctxlen = [(ctx.len() >> 8) as u8, ctx.len() as u8];
			func.tls_prf(&self.secret, label, |x| {
				x.append(&self.randoms);
				x.append(&ctxlen);
				x.append(ctx);
			}, buffer);
		} else {
			func.tls_prf(&self.secret, label, |x| {
				x.append(&self.randoms);
			}, buffer);
		}
		__call_debug(debug, TlsDebug::Tls12Exporter {
			master_secret: &self.secret,
			label: label,
			context: context,
			client_random: &self.randoms[..32],
			server_random: &self.randoms[32..],
			exported_value: &buffer,
		});
		Ok(())
	}
}

/*
#[test]
fn basic_protectors()
{
	let key = [1;44];
	let mut msg = [0;128];
	let msg2 = [0;42-5];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..58);
	assert_eq!(msg[0], 27);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 3);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 53);
	assert_eq!(d.deprotect(&mut msg[..58]).unwrap(), (27, 5..42));
	assert_eq!(&msg[5..42], &msg2[..]);
}

#[test]
fn basic_protectors2()
{
	let key = [1;20];
	let mut msg = [0;128];
	let msg2 = [0;42-13];
	let mut e = Protector::new_aes128(&key).unwrap();
	let mut d = Deprotector::new_aes128(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 13..42, 0).unwrap(), 0..58);
	assert_eq!(msg[0], 27);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 3);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 53);
	assert_eq!(&msg[5..13], &msg2[..8]);
	assert_eq!(d.deprotect(&mut msg[..58]).unwrap(), (27, 13..42));
	assert_eq!(&msg[13..42], &msg2[..]);
}

#[test]
fn basic_protectors3()
{
	let key = [1;36];
	let mut msg = [0;128];
	let msg2 = [0;42-13];
	let mut e = Protector::new_aes256(&key).unwrap();
	let mut d = Deprotector::new_aes256(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 13..42, 0).unwrap(), 0..58);
	assert_eq!(msg[0], 27);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 3);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 53);
	assert_eq!(&msg[5..13], &msg2[..8]);
	assert_eq!(d.deprotect(&mut msg[..58]).unwrap(), (27, 13..42));
	assert_eq!(&msg[13..42], &msg2[..]);
}

#[test]
fn gap_too_small()
{
	let key = [1;20];
	let mut msg = [0;128];
	let mut e = Protector::new_aes128(&key).unwrap();
	e.protect(&mut msg, 27, 12..42, 0).unwrap_err();
	e.protect(&mut msg, 27, 14..42, 0).unwrap();
	let key = [1;44];
	let mut e = Protector::new_chacha20(&key).unwrap();
	e.protect(&mut msg, 27, 4..42, 0).unwrap_err();
	e.protect(&mut msg, 27, 6..42, 0).unwrap();
}

#[test]
fn basic_protectors_desync()
{
	let key = [1;44];
	let mut msg = [0;128];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..58);
	for i in msg.iter_mut() { *i = 0; }
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..58);
	assert_eq!(msg[0], 27);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 3);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 53);
	d.deprotect(&mut msg[..58]).unwrap_err();
}

#[test]
fn basic_protectors_wrap_e()
{
	let key = [1;44];
	let mut msg = [0;128];
	let mut e = Protector::new_chacha20(&key).unwrap();
	e.2 = !0u64;
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..58);
	for i in msg.iter_mut() { *i = 0; }
	e.protect(&mut msg, 27, 5..42, 0).unwrap_err();
}


#[test]
fn basic_protectors_wrap_d()
{
	let key = [1;44];
	let mut msg = [0;128];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut e2 = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	e2.2 = !0u64;
	d.2 = !0u64;
	assert_eq!(e2.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..58);
	assert_eq!(d.deprotect(&mut msg[..58]).unwrap(), (27, 5..42));
	for i in msg.iter_mut() { *i = 0; }
	assert_eq!(e2.2, !0u64);
	assert_eq!(d.2, !0u64);
	e.2 = d.2;
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..58);
	d.deprotect(&mut msg[..58]).unwrap_err();
}

#[test]
fn test_aes_gcm_nonces()
{
	let key = [1;20];
	let mut msg = [0;128];
	let mut e = Protector::new_aes128(&key).unwrap();
	let mut d = Deprotector::new_aes128(&key).unwrap();
	e.2 = 0x01030507090B0D0F;
	d.2 = 0x01030507090B0D0F;
	for i in 4..12 { e.1[i] = 0xFF; }
	assert_eq!(e.protect(&mut msg, 27, 13..42, 0).unwrap(), 0..58);
	assert_eq!(&msg[5..13], &[0xFE, 0xFC, 0xFA, 0xF8, 0xF6, 0xF4, 0xF2, 0xF0][..]);
	assert_eq!(d.deprotect(&mut msg[..58]).unwrap(), (27, 13..42));
}

#[test]
fn test_aes_gcm_nonces2()
{
	let key = [1;20];
	let mut msg = [0;128];
	let mut e = Protector::new_aes128(&key).unwrap();
	let mut d = Deprotector::new_aes128(&key).unwrap();
	e.2 = 0x01030507090B0D0F;
	d.2 = 0x01030507090B0D0E;
	for i in 4..12 { e.1[i] = 0xFF; }
	assert_eq!(e.protect(&mut msg, 27, 13..42, 0).unwrap(), 0..58);
	d.deprotect(&mut msg[..58]).unwrap_err();
}

#[test]
fn test_aes_gcm_nonces3()
{
	let key = [1;20];
	let mut msg = [0;128];
	let mut e = Protector::new_aes128(&key).unwrap();
	let mut d = Deprotector::new_aes128(&key).unwrap();
	e.2 = 0x01030507090B0D0F;
	d.2 = 0x01030507090B0D0F;
	for i in 4..12 { e.1[i] = 0xFF; }
	assert_eq!(e.protect(&mut msg, 27, 13..42, 0).unwrap(), 0..58);
	msg[5] += 1;
	d.deprotect(&mut msg[..58]).unwrap_err();
}

#[test]
fn basic_protectors_multi()
{
	let key = [1;44];
	let mut msg = [0;128];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..58);
	assert_eq!(d.deprotect(&mut msg[..58]).unwrap(), (27, 5..42));
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..58);
	assert_eq!(d.deprotect(&mut msg[..58]).unwrap(), (27, 5..42));
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..58);
	assert_eq!(d.deprotect(&mut msg[..58]).unwrap(), (27, 5..42));
	assert_eq!(e.2, 3);
	assert_eq!(d.2, 3);
}

#[test]
fn basic_protectors_off()
{
	let key = [1;44];
	let mut msg = [0;128];
	let msg2 = [0;42-7];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 7..42, 0).unwrap(), 2..58);
	assert_eq!(msg[2], 27);
	assert_eq!(msg[3], 3);
	assert_eq!(msg[4], 3);
	assert_eq!(msg[5], 0);
	assert_eq!(msg[6], 51);
	assert_eq!(d.deprotect(&mut msg[2..58]).unwrap(), (27, 5..40));
	assert_eq!(&msg[7..42], &msg2[..]);
}

#[test]
fn basic_protectors_exact()
{
	let key = [1;44];
	let mut msg = [0;58];
	let msg2 = [0;42-5];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..58);
	assert_eq!(msg[0], 27);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 3);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 53);
	assert_eq!(d.deprotect(&mut msg[..58]).unwrap(), (27, 5..42));
	assert_eq!(&msg[5..42], &msg2[..]);
}

#[test]
fn basic_protectors_pad()
{
	let key = [1;44];
	let mut msg = [0;58];
	let msg2 = [0;42-5];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 10000).unwrap(), 0..58);
	assert_eq!(msg[0], 27);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 3);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 53);
	assert_eq!(d.deprotect(&mut msg[..58]).unwrap(), (27, 5..42));
	assert_eq!(&msg[5..42], &msg2[..]);
}
*/
