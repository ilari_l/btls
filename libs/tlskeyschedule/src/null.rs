use super::MAX_TLS_RECORD_PLAIN;
use super::PROTO_APPDATA;
use btls_aux_fail::dtry;
use btls_aux_fail::fail_if;
use btls_aux_memory::BackedVector;
use btls_aux_memory::split_array_head_mut;
use core::cmp::min;
use core::mem::MaybeUninit;
use core::ops::Range;
use core::ptr::copy_nonoverlapping;


pub(crate) fn null_protect(flag: bool, buf: &mut [u8], rtype: u8, range: Range<usize>) -> Result<Range<usize>, ()>
{
	//We need:
	//- At least 5 bytes of space before range start.
	//- Range must be valid (end >= start).
	//- Range must be within buffer.
	//- Range must be at most 16384 bytes in size.
	let hstart = dtry!(range.start.checked_sub(5));
	let rngrec = hstart..range.end;
	let record = dtry!(buf.get_mut(rngrec.clone()));
	let (header, payload): (&mut [u8;5], &mut [u8]) = dtry!(split_array_head_mut(record));
	let len = payload.len();
	fail_if!(len > 16384, ());
	//By the checks above, this can not overflow, and at least 5 bytes from this can be accessed.
	header[0] = rtype;
	//Hack, if TLS 1.2, use TLS 1.2 record protocol.
	header[1] = 3;
	header[2] = if flag { 1 } else { 3 };
	header[3] = (len >> 8) as u8;
	header[4] = len as u8;
	Ok(rngrec)
}

pub(crate) fn null_protect_coutput(flag: bool, out: &mut [u8], inp: &[u8], rtype: u8, max_write_size: usize) ->
	Result<(usize, usize), ()>
{
	//We need at least 5 bytes of output space, otherwise we can't flush anything.
	if out.len() < 5 { return Ok((0, 0)); }
	//The maximum input length is minimum of output length minus five and the maximum packet size.
	//Also, by checks above, at least 5 bytes from out are writeable.
	let len = min(min(MAX_TLS_RECORD_PLAIN, max_write_size), min(out.len() - 5, inp.len()));
	out[0] = rtype;
	//Hack, if TLS 1.2, use TLS 1.2 record protocol.
	out[1] = 3;
	out[2] = if flag { 1 } else { 3 };
	out[3] = (len >> 8) as u8;		//Length.
	out[4] = len as u8;
	//Proof of safety:
	//1) By definition of inp.as_ptr() and inp.len(), inp.as_ptr() points to inp.len() readable elements.
	//Furthermore, len <= inp.len(), because inp.len() is a minimum term in len. Therefore inp.as_ptr()
	//points to len readable elements.
	//2) By definition of out.as_mut_ptr() and out.len(), out.as_mut_ptr() points to out.len() writable
	//elements. By check above, out.len() >= 5, therefore out.as_mut_ptr().add(5) points to out.len()-5
	//writable elements. Furthermore, len <= out.len() - 5, because out.len() - 5 is a minimum term in
	//len. Therefore out.as_mut_ptr().add(5) points to len writable elements.
	//3) Because out is &mut [u8], it can not overlap anything. Therefore inp.as_ptr() and
	//out.as_mut_ptr().add(5) can not overlap.
	//Q.E.D.
	fail_if!(out.len() < len + 5, ());	//Len can be at most 65535, not even close to overflow.
	fail_if!(inp.len() < len, ());
	unsafe{copy_nonoverlapping(inp.as_ptr(), out.as_mut_ptr().add(5), len);}
	Ok((len + 5, len))			//5 bytes of headers.
}

pub(crate) fn null_protect_btb2<'a>(flag: bool, dst: &'a mut [MaybeUninit<u8>], src: &[u8],
	rtype: btls_aux_tls_iana::ContentType) -> Result<&'a [u8], ()>
{
	let len = src.len();
	fail_if!(len > MAX_TLS_RECORD_PLAIN, ());
	let mut dst = BackedVector::new(dst);
	let header = [rtype.get(), 3, if flag { 1 } else { 3 }, (len >> 8) as u8, len as u8];
	dtry!(dst.extend(&header));
	dtry!(dst.extend(src));
	Ok(dst.into_inner())
}

fn __deprotect_pre_common(src: &[u8]) -> Result<(u8, usize), ()>
{
	fail_if!(src.len() < 5, ());
	let len = src[3] as usize * 256 + src[4] as usize;
	let rtype = src[0];
	fail_if!(rtype == PROTO_APPDATA, ());	//This is not supposed to happen.
	Ok((rtype, len))
}

pub(crate) fn null_deprotect(buf: &mut [u8]) -> Result<(u8, Range<usize>), ()>
{
	let (rtype, len) = __deprotect_pre_common(buf)?;
	let rsize = len + 5;			//Len can be at most 65535, not even close to overflow.
	fail_if!(buf.len() != rsize, ());	//Check that the size is correct.
	Ok((rtype, 5..rsize))
}

pub(crate) fn null_deprotect_btb(src: &[u8], dst: &mut [u8]) -> Result<(u8, usize), ()>
{
	let (rtype, len) = __deprotect_pre_common(src)?;
	let rsize = len + 5;			//Len can be at most 65535, not even close to overflow.
	//Proof of safety:
	//1) By definition of src.as_ptr() and src.len(), src.as_ptr() points to src.len() readable elements.
	//By the condition, src.len() >= rsize. rsize is len.wrapping_add(5). len can be at most 65535 by definition
	//of __deprotect_pre_common(), and thus since usize can store at least up to 2^32-1, len+5 can not overflow.
	//Therefore src.as_ptr().add(5) points to len readaable elements.
	//2) By definition of dst.as_mut_ptr() and dst.len(), dst.as_mut_ptr() point to dst.len() writable
	//elements. By the condition, dst.len() >= len. Therefore dst.as_mut_ptr() points to at least len
	//writable elements.
	//3) Because dst is &mut [u8], it can not overlap anything. Therefore src and dst do not overlap.
	//Q.E.D.
	fail_if!(src.len() < rsize, ());
	fail_if!(dst.len() < len, ());
	unsafe{copy_nonoverlapping(src.as_ptr().add(5), dst.as_mut_ptr(), len)};
	Ok((rtype, len))
}

pub(crate) fn null_deprotect_btb2<'a>(src: &[u8], dst: &'a mut [MaybeUninit<u8>]) ->
	Result<(btls_aux_tls_iana::ContentType, &'a [u8]), ()>
{
	let (rtype, len) = __deprotect_pre_common(src)?;
	let rsize = len + 5;			//Len can be at most 65535, not even close to overflow.
	fail_if!(src.len() < rsize, ());
	let mut dst = BackedVector::new(dst);
	dtry!(dst.extend(&src[5..rsize]));
	Ok((btls_aux_tls_iana::ContentType::new(rtype), dst.into_inner()))
}
