use super::__call_debug;
use super::__call_event;
use super::_FinishedValue;
use super::_RecordDeprotector;
use super::_RecordProtector;
use super::_TlsExtractor;
use super::BoxedDeprotector;
use super::BoxedExtractor;
use super::BoxedProtector;
use super::DiffieHellmanSharedSecret;
use super::FinishedValue;
use super::IsServer;
use super::MAX_KEY_CHUNK;
use super::TLS12_FINISHED_LEN;
use super::Tls13RecordMode;
use super::TlsDebug;
use super::GenerateFinished;
use btls_aux_aead::ProtectorType;
use btls_aux_assert::assert_failure;
use btls_aux_assert::AssertFailed;
use btls_aux_assert::sanity_check;
use btls_aux_collections::Box;
use btls_aux_dhf::KEM;
use btls_aux_fail::fail_if;
use btls_aux_hash::HashBlock2;
use btls_aux_hash::HashFunction2;
use btls_aux_hash::HashOutput2;
use btls_aux_hash::slice_eq_ct;
use btls_aux_signatures::Hasher;
use btls_aux_signatures::SignatureAlgorithmEnabled2;
use btls_aux_signatures::SignatureAlgorithmTls2;
use btls_aux_signatures::SignatureError2;
use btls_aux_signatures::SignatureFlags2;
use btls_aux_signatures::SignatureTls2;
use btls_aux_tls_iana::Alert;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;


const TLS12_MS_LEN: usize = 48;
const TLS_RANDOM_LEN: usize = 32;
const CLIENT_FINISHED_LABEL: &'static str = "client finished";
const SERVER_FINISHED_LABEL: &'static str = "server finished";

#[derive(Copy,Clone,Debug)] pub struct IsEms(pub bool);

///Error in TLS 1.2 key schedule.
#[derive(Clone,Debug)]
#[non_exhaustive]
pub enum Tls12KeyScheduleError
{
	///Assertion failed.
	InternalError(AssertFailed),
	///Signature does not verify.
	SignatureFailed(SignatureError2),
	///Bad finished MAC.
	BadFinishedMac,
}

impl Tls12KeyScheduleError
{
	///Get TLS alert for error.
	pub fn alert(&self) -> Alert
	{
		match self {
			&Tls12KeyScheduleError::InternalError(_) => Alert::INTERNAL_ERROR,
			&Tls12KeyScheduleError::SignatureFailed(_) => Alert::DECRYPT_ERROR,
			&Tls12KeyScheduleError::BadFinishedMac => Alert::DECRYPT_ERROR,
		}
	}
}

impl Display for Tls12KeyScheduleError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&Tls12KeyScheduleError::InternalError(ref err) => write!(f, "Internal Error: {err}"),
			&Tls12KeyScheduleError::SignatureFailed(ref err) =>
				write!(f, "Verifying signature failed: {err}"),
			&Tls12KeyScheduleError::BadFinishedMac => f.write_str("Bad Finished MAC"),
		}
	}
}

impl From<AssertFailed> for Tls12KeyScheduleError
{
	fn from(e: AssertFailed) -> Tls12KeyScheduleError { Tls12KeyScheduleError::InternalError(e) }
}

fn __compute_tls12_finished(prf: HashFunction2, master_secret: &[u8;TLS12_MS_LEN], label: &str,
	hash_sofar: &HashOutput2, debug: &mut Option<&mut dyn FnMut(TlsDebug)>, received: Option<&[u8]>) ->
	Result<FinishedValue, AssertFailed>
{
	let hlen = hash_sofar.len();
	let prflen = prf.output_length();
	sanity_check!(hlen == prflen, F "Unexpected finished hash length ({hlen} != {prflen})");
	let mut out = [0; TLS12_FINISHED_LEN];
	prf.tls_prf(master_secret, label, |x|{x.append(&hash_sofar);}, &mut out);
	__call_debug(debug, TlsDebug::Tls12Finished {
		master_secret: master_secret,
		handshake_hash: &hash_sofar,
		calculated_mac: &out,
		received_mac: received,
	});
	Ok(FinishedValue(_FinishedValue::Tls12(out)))
}

///The TLS ServerKeyExchange message
pub struct TlsServerKeyExchange<'a>
{
	///The KEM.
	pub kem: KEM,
	///The key share.
	pub key_share: &'a [u8],
	///Signature algorithm.
	pub algorithm: SignatureAlgorithmTls2,
	///The signature value.
	pub signature: &'a [u8],
}

///TLS 1.2 premaster secret.
pub struct Tls12PremasterSecret
{
	//The premaster secret can be compressed to one hash input block.
	secret: HashBlock2,
	protector: ProtectorType,
	prf: HashFunction2,
	is_server: IsServer,
	is_ems: IsEms,			//Needed by exporters.
}

impl Tls12PremasterSecret
{
	fn new(premaster_secret: impl DiffieHellmanSharedSecret, protector: ProtectorType, prf: HashFunction2,
		is_server: IsServer, is_ems: IsEms, mut debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Tls12PremasterSecret
	{
		let premaster_secret = premaster_secret.get_secret(crate::private::PrivateMarker);
		let premaster_secret = premaster_secret.as_raw();
		//If the PMS is longer than hash, apply a trick to compress it. This works because PMS only
		//appears as HMAC key, which is prehashed if too long.
		let secret = prf.hmac_precompress_key(premaster_secret);
		__call_debug(&mut debug, TlsDebug::Tls12PremasterSecret {
			premaster_secret: premaster_secret,
			compressed_premaster_secret: &secret
		});
		Tls12PremasterSecret{
			secret: secret,
			protector: protector,
			prf: prf,
			is_server: is_server,
			is_ems: is_ems
		}
	}
	pub fn new_server<Dhe:DiffieHellmanSharedSecret>(premaster_secret: Dhe, protector: ProtectorType,
		prf: HashFunction2, is_ems: IsEms, debug: Option<&mut dyn FnMut(TlsDebug)>) -> Tls12PremasterSecret
	{
		Tls12PremasterSecret::new(premaster_secret, protector, prf, IsServer(true), is_ems, debug)
	}
	pub fn new_client<Dhe:DiffieHellmanSharedSecret>(premaster_secret: Dhe, protector: ProtectorType,
		prf: HashFunction2, is_ems: IsEms, server_spki: &[u8], client_random: &[u8;32],
		server_random: &[u8;32], pmsg: &TlsServerKeyExchange, flags: SignatureFlags2,
		enabled_sig: SignatureAlgorithmEnabled2, mut debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<Tls12PremasterSecret, Tls12KeyScheduleError>
	{
		//Construct TBS and validate the server signature.
		let share = pmsg.key_share;
		let sharelen = share.len();
		sanity_check!(sharelen <= 255, "Key share too big ({sharelen} > 255)");
		let mut tbs = |h: &mut Hasher|{
			let t = 0x03000000u32 + pmsg.kem.tls_id() as u32 * 256 + share.len() as u32;
			h.append(client_random);
			h.append(server_random);
			h.append(&t.to_be_bytes());
			h.append(share);
		};
		key_debug!(&mut debug, TlsDebug::TlsServerSignature {
			public_key: server_spki,
			server_tbs: &Hasher::collect(&mut tbs),
			algorithm: pmsg.algorithm,
			signature: pmsg.signature,
		});
		//This is TLS 1.2, so cap at RSA-PKCS#1 allowed.
		SignatureTls2::new(pmsg.algorithm, pmsg.signature).verify_callback(server_spki, enabled_sig,
			flags & SignatureFlags2::ALLOW_RSA_PKCS1, &mut tbs).
			map_err(|e|Tls12KeyScheduleError::SignatureFailed(e))?;
		Ok(Tls12PremasterSecret::new(premaster_secret, protector, prf, IsServer(false), is_ems, debug))
	}
}

///TLS 1.2 master secret.
pub struct Tls12MasterSecret
{
	secret: [u8; TLS12_MS_LEN],
	client_random: [u8; TLS_RANDOM_LEN],
	server_random: [u8; TLS_RANDOM_LEN],
	protector: ProtectorType,
	prf: HashFunction2,
	is_server: IsServer,
	is_ems: IsEms,			//Needed by exporters.
}

impl Tls12MasterSecret
{
	pub fn new(premaster_secret: Tls12PremasterSecret, client_random: &[u8; 32], server_random: &[u8; 32],
		post_cke_hash: HashOutput2, debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<Tls12MasterSecret, Tls12KeyScheduleError>
	{
		let pckelen = post_cke_hash.len();
		let prflen = premaster_secret.prf.output_length();
		sanity_check!(pckelen == prflen, F
			"Handshake hash length is not expected ({pckelen}, expected {prflen})");
		let mut ret = Tls12MasterSecret{
			secret: [0;TLS12_MS_LEN],
			client_random: *client_random,
			server_random: *server_random,
			protector: premaster_secret.protector,
			prf: premaster_secret.prf,
			is_server: premaster_secret.is_server,
			is_ems: premaster_secret.is_ems
		};
		Self::derive_ms(debug, &mut ret.secret, &premaster_secret.secret, client_random,
			server_random, premaster_secret.prf, post_cke_hash, premaster_secret.is_ems);
		Ok(ret)
	}
	pub fn get_deprotector(&self, mut debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<BoxedDeprotector, Tls12KeyScheduleError>
	{
		use crate::tls12::Deprotector;
		let mut keyblock = [0; MAX_KEY_CHUNK];
		let tlen = self.compute_key_chunk(&mut keyblock, !self.is_server.0)?;
		let d = create_deprotector!(&mut debug, self.protector, keyblock, tlen, 4, Tls13RecordMode::tls12());
		Ok(BoxedDeprotector(_RecordDeprotector::Tls12(d)))
	}
	pub fn get_protector(&self, mut debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<BoxedProtector, Tls12KeyScheduleError>
	{
		use crate::tls12::Protector;
		let mut keyblock = [0; MAX_KEY_CHUNK];
		let tlen = self.compute_key_chunk(&mut keyblock, self.is_server.0)?;
		let p = create_protector!(&mut debug, self.protector, keyblock, tlen, 4, Tls13RecordMode::tls12());
		Ok(BoxedProtector(_RecordProtector::Tls12(p)))
	}
	pub fn get_exporter(&self, _debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<BoxedExtractor, Tls12KeyScheduleError>
	{
		//Extractors are not allowed in TLS 1.2 non-EMS.
		if !self.is_ems.0 { return Ok(BoxedExtractor(_TlsExtractor::Banned));  }
		let e = Box::new(crate::tls12::Extractor::new(&self.secret, &self.client_random,
			&self.server_random, self.prf)?);
		Ok(BoxedExtractor(_TlsExtractor::Tls12(e)))
	}
	pub fn check_finished(&self, hs_hash: &HashOutput2, mut debug: Option<&mut dyn FnMut(TlsDebug)>,
		finished: &[u8]) -> Result<(), Tls12KeyScheduleError>
	{
		//These are swapped, since it is checking peer's result.
		let label = if self.is_server.0 { CLIENT_FINISHED_LABEL } else { SERVER_FINISHED_LABEL };
		let mac = __compute_tls12_finished(self.prf, &self.secret, label, hs_hash, &mut debug,
			Some(finished))?;
		fail_if!(!slice_eq_ct(&mac, finished), Tls12KeyScheduleError::BadFinishedMac);
		//Notify of OK finished.
		__call_event(&mut debug, TlsDebug::FinishedOk {
			server: !self.is_server.0,
		});
		Ok(())
	}
	//Derive MS from PMS.
	fn derive_ms(mut debug: Option<&mut dyn FnMut(TlsDebug)>, ms_out: &mut [u8; TLS12_MS_LEN], pms: &[u8],
		client_random: &[u8; 32], server_random: &[u8; 32], prf: HashFunction2, hs_hash: HashOutput2,
		ems_enabled: IsEms)
	{
		//sanity_check!(self.nontrivial_kex, "Interlock no nontrivial key exchange");
		let label = if ems_enabled.0 {"extended master secret"} else {"master secret"};
		prf.tls_prf(pms, label,  |x| {
			if ems_enabled.0 {
				x.append(&hs_hash);
			} else {
				x.append(client_random);
				x.append(server_random);
			}
		}, ms_out);
		__call_debug(&mut debug, TlsDebug::Tls12MasterSecret {
			extended: ems_enabled.0,
			premaster_secret: pms,
			handshake_hash: &hs_hash,
			client_random: client_random,
			server_random: server_random,
			master_secret: ms_out,
		});
	}
	fn compute_key_chunk(&self, keyblock: &mut [u8], server_write: bool) -> Result<usize, Tls12KeyScheduleError>
	{
		//sanity_check!(self.nontrivial_kex, "Interlock no nontrivial key exchange");
		let mut masterblock = [0; 2*MAX_KEY_CHUNK];	//Currently largest possible is 88 bytes (2*32+2*12)
		let keysize = self.protector.get_key_length();
		let ivsize = self.protector.get_nonce_length().saturating_sub(if self.protector.is_gcm_style() {
			8 } else { 0 });
		let blocklen = keysize + ivsize;	//Should be reasonable.
		let masterlen = blocklen * 2;		//Should be reasonable.
		let func = self.prf;
		//We use the length extension of the TLS PRF.
		func.tls_prf(&self.secret, "key expansion", |x| {
			x.append(&self.server_random);
			x.append(&self.client_random);
		}, &mut masterblock[..]);
		let mblock_len = masterblock.len();
		let kblock_len = keyblock.len();
		sanity_check!(keysize <= blocklen, F "Keysize {keysize} >= blocklen {blocklen}???");
		let t = masterblock.get(..masterlen).ok_or_else(||{
			assert_failure!(F "Masterblock too small ({mblock_len}, need {masterlen})")
		})?;
		let keyblock = keyblock.get_mut(..blocklen).ok_or_else(||{
			assert_failure!(F "Keyblock too small ({kblock_len}, need {blocklen})")
		})?;
		let (store_key, store_iv) = keyblock.split_at_mut(keysize);
		let (client_write_key, t) = t.split_at(keysize);
		let (server_write_key, t) = t.split_at(keysize);
		let (client_write_iv, server_write_iv) = t.split_at(ivsize);
		let write_key = if server_write { server_write_key } else { client_write_key };
		let write_iv = if server_write { server_write_iv } else { client_write_iv };
		store_key.copy_from_slice(write_key);
		store_iv.copy_from_slice(write_iv);
		Ok(blocklen)
	}
	pub fn debug_export(&self) -> &[u8] { &self.secret }
}

impl GenerateFinished for Tls12MasterSecret
{
	fn generate_finished(&self, hs_hash: &HashOutput2, mut debug: Option<&mut dyn FnMut(TlsDebug)>) ->
		Result<FinishedValue, AssertFailed>
	{
		let label = if self.is_server.0 { SERVER_FINISHED_LABEL } else { CLIENT_FINISHED_LABEL };
		__compute_tls12_finished(self.prf, &self.secret, label, hs_hash, &mut debug, None)
	}
}
