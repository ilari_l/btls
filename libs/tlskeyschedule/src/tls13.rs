use super::__call_debug;
use super::AlgorithmInfo;
use super::check_range;
use super::ExtractorError;
use super::PROTO_CCS;
use super::Tls13RecordMode;
use super::TlsDebug;
use super::tls13_ks::HkdfLabel;
use super::tls13_ks::tls13_l;
use super::tls13_ks::Tls13KeyScheduleError;
use btls_aux_aead::DecryptionKey;
use btls_aux_aead::EncryptionKey;
use btls_aux_aead::MAX_NONCE;
use btls_aux_assert::sanity_check;
use btls_aux_aead::ProtectorType;
use btls_aux_fail::dtry;
use btls_aux_fail::fail_if;
use btls_aux_hash::HashFunction2;
use btls_aux_hash::HashOutput2;
use btls_aux_memory::copy_maximum;
use btls_aux_memory::slice_assume_init;
use btls_aux_memory::slice_splitn_at_exact;
use btls_aux_tls_iana::ContentType;
use core::cmp::min;
use core::mem::MaybeUninit;
use core::ops::Deref;
use core::ops::Range;
use core::ptr::write_unaligned;


fn increment_rsn(rsn: &mut u64, destroyed: &mut bool)
{
	//Increment the RSN, checking for overflows.
	*rsn = match rsn.checked_add(1) {
		Some(x) => x,
		None => {*destroyed = true; *rsn }
	};
}

//Assumes nonce <= MAX_NONCE-8.
fn make_iv_encrypt(iv: &[u8; MAX_NONCE], rsn: u64, nonce: usize) -> [u8; MAX_NONCE]
{
	let mut _iv: [u8; MAX_NONCE] = *iv;
	if let Some(ivspace) = _iv.get_mut(nonce..nonce.wrapping_add(8)) {
		//The above succeeding guarantees ivspace is 8 bytes. However, can not use references because
		//alignmnent is not guaranteed.
		unsafe {
			let mut t = ivspace.as_mut_ptr().cast::<u64>().read_unaligned();
			t ^= rsn.to_be();
			ivspace.as_mut_ptr().cast::<u64>().write_unaligned(t);
		}
	}
	_iv
}

#[test]
fn test_make_iv_encrypt()
{
	let mut iv = [0;MAX_NONCE];
	//Assume MAX_NONCE is at least 12.
	(&mut iv[..12]).copy_from_slice(&[0x20,0x21,0x22,0x23,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10]);
	let iv2 = make_iv_encrypt(&iv, 0x0102030405060708, 4);
	assert_eq!(iv2, [0x20u8,0x21,0x22,0x23,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18]);
}

//key, IV, RSN, vulernable to NDA, destroyed, tagsize, noncesize
pub struct Protector
{
	key: EncryptionKey,
	iv: [u8;MAX_NONCE],
	rsn: u64,
	destroyed: bool,
	tag: usize,
	nonce: usize,
	mode: Tls13RecordMode,
}

pub struct Deprotector
{
	key: DecryptionKey,
	iv: [u8;MAX_NONCE],
	rsn: u64,
	destroyed: bool,
	nonce: usize,
	mode: Tls13RecordMode,
}

pub struct Extractor
{
	secret: HashOutput2,
	function: HashFunction2,
}

impl Protector
{
	pub fn new_x(key: &[u8], algorithm: ProtectorType, mode: Tls13RecordMode) -> Result<Self, ()>
	{
		let alginfo = AlgorithmInfo::new(algorithm)?;
		//Split the key. key_iv.len() will be at most alginfo.noncelen, which is at most MAX_NONCE.
		let (key_k, key_iv) = dtry!(slice_splitn_at_exact(key, (alginfo.keylen, alginfo.noncelen)));
		let mut iv = [0; MAX_NONCE];
		copy_maximum(&mut iv, key_iv);
		Ok(Protector{
			key: dtry!(algorithm.new_encryptor(key_k)),
			iv: iv,
			rsn: 0,
			destroyed: false,
			tag: alginfo.taglen,
			nonce: alginfo.noncelen - 8,	//Always offset by 8 bytes.
			mode: mode,
		})
	}
	pub(crate) fn protect(&mut self, buf: &mut [u8], rtype: u8, range: Range<usize>, pad: usize) ->
		Result<Range<usize>, ()>
	{
		//If context has been destroyed, fail.
		fail_if!(self.destroyed, ());

		//The range must be valid. There must be at least 5 bytes of space before start for header.
		check_range(buf, &range)?;
		fail_if!(range.start < 5, ());

		//Size of packet data. We have checked that range.end >= range.start. This can be at most 16384.
		//This has to hold even after padding.
		let pktsize = range.end - range.start;
		fail_if!(pktsize > MAX_RECORD_PTXT, ());
		let opktsize = min(MAX_RECORD_PTXT, pktsize.saturating_add(pad));

		//Check that the packet fits. opktsize can be at most 16kB and self.tag is at most 128.
		fail_if!(buf.len() - range.start < opktsize + 1 + self.tag, ());

		//Compute range. dend <= buf.len() by check above.
		let dstart = range.start - 5;
		let dend = range.start + opktsize + 1 + self.tag;

		//dend >= range.start + opktsize + 1 >= range.start + pktsize + 1.
		buf[range.start + pktsize] = rtype;
		for i in pktsize..opktsize { buf[range.start + i + 1] = 0; }

		//This is in-range, since range.start + ciphertext_size = dend, which is known to be valid bound,
		//and ciphertext_size >= plaintext_size. plaintext_size = opktsize + 1.
		fail_if!(self.nonce.saturating_add(8) > MAX_NONCE, ());
		let _iv = make_iv_encrypt(&self.iv, self.rsn, self.nonce);
		let _iv = &_iv[..(self.nonce+8)];		//Strip extra part away.
		let ciphertext_size = opktsize+1+self.tag;	//Will not exceeed 20000.
		//Split buffers because rust aliasing rules. Also, first dstart bytes have to be discarded.
		let hdrlen = range.start - dstart;
		let buf2 = &mut buf[dstart..];
		let (header, buffer) = buf2.split_at_mut(hdrlen);
		write_header(header, 0, ciphertext_size, &self.mode);
		dtry!(self.key.encrypt_inplace(&_iv, self.mode.transform_ad(header), buffer, opktsize + 1));
		increment_rsn(&mut self.rsn, &mut self.destroyed);

		//Return the range.
		Ok(dstart..dend)
	}
	pub(crate) fn protect_coutput(&mut self, out: &mut [u8], inp: &[u8], rtype: u8, max_write_size: usize,
		maxsize: bool) -> Result<(usize, usize), ()>
	{
		//If context has been destroyed, fail.
		fail_if!(self.destroyed, ());

		let hdrsize = 5usize;
		let overhead = hdrsize + 1 + self.tag;			//< 300.
		//Padded size is at most MAX_RECORD_PTXT or max_write_size plus overhead or output buffer size.
		let paddedsize = min(min(MAX_RECORD_PTXT, max_write_size) + overhead, out.len());
		if out.len() < overhead { return Ok((0, 0)); }		//Too little space?
		//out.len() >= overhead by check above, so difference is positive.
		let len = min(min(MAX_RECORD_PTXT, max_write_size), min(out.len() - overhead, inp.len()));
		//Real rtype. pktsize <= opktsize < ciphertext_size and index is thus < dend, which is in bounds.
		out[5 + len] = rtype;
		//The padding and its size. By above, paddedsize >= len+overhead.
		let epadding = Some((rtype, if maxsize { paddedsize-len-overhead } else { 0 }));
		//There is no padding at the moment. Call buffer-to-buffer encryption. We know that
		//self.nonce + 8 <= MAX_NONCE = _iv.len()
		let _iv = make_iv_encrypt(&self.iv, self.rsn, self.nonce);
		let _iv = &_iv[..(self.nonce+8)];		//Strip extra part away.
		//Always under 20000. Subtract 5 from padded size, as that includes header and outlen does not.
		let outlen = if maxsize { paddedsize.saturating_sub(5) } else { len + 1 + self.tag };
		//Split the output buffer because of rust aliasing rules.
		let (header, output) = out.split_at_mut(5);
		write_header(header, 0, outlen, &self.mode);
		let _ad = self.mode.transform_ad(header);
		dtry!(self.key.encrypt_buffer_to_buffer(_iv, _ad, output, &inp[..len], epadding));
		increment_rsn(&mut self.rsn, &mut self.destroyed);
		//outlen is at most MAX_RECORD_PTXT+17, so +5 fits.
		Ok((outlen+5, len))
	}
	pub(crate) fn protect_btb2<'a>(&mut self, dst: &'a mut [MaybeUninit<u8>], src: &[u8],
		rtype: ContentType, maxsize: bool) -> Result<&'a [u8], ()>
	{
		//If context has been destroyed, fail.
		fail_if!(self.destroyed, ());
		let len = src.len();
		fail_if!(dst.len() < 5 || len > MAX_RECORD_PTXT, ());
		//Padding amount.
		let pad_amount = if maxsize {
			//Cap write size to maximum ciphertext size allowed by TLS 1.3. This prevents spurious
			//errors with completely whacky output sizes.
			let dstlen = min(dst.len(), MAX_RECORD_CTXT);
			let ctlen = dtry!(dstlen.checked_sub(5));	//Header.
			let mptlen = min(dtry!(self.key.max_plaintext_size(ctlen)), MAX_RECORD_PTXT+1);
			let mptlen = dtry!(mptlen.checked_sub(1));	//Content type.
			//Never pad more than to 16384. Otherwise pad to capacity.
			dtry!(mptlen.checked_sub(len))
		} else {
			0	//No padding.
		};
		let need = dtry!(self.key.encrypted_size(len + pad_amount + 1));
		let (header, output) = dst.split_at_mut(5);
		fail_if!(output.len() < need, ());
		let _iv = make_iv_encrypt(&self.iv, self.rsn, self.nonce);
		let _iv = &_iv[..(self.nonce+8)];		//Strip extra part away.
		let out: &'a [u8] = unsafe {
			//These are sound because header is always 5 bytes.
			write_unaligned(header.as_mut_ptr().cast(), 23u8);	//Always application data.
			write_unaligned(header.as_mut_ptr().add(1).cast(), 0x0303u16.to_be());
			write_unaligned(header.as_mut_ptr().add(3).cast(), (need as u16).to_be());
			let _ad = self.mode.transform_ad(slice_assume_init(header, 5));
			let pad = Some((rtype.get(),pad_amount));
			let plen = dtry!(self.key.encrypt_buffer_to_buffer(&_iv, _ad, output, src, pad)).len();
			//Length of dst is 5 + length of output, and plen is at most length of output, threfore
			//this is in-bounds. Futhermore, this all has been initialized.
			slice_assume_init(dst, 5+plen)
		};
		fail_if!(out.len() != 5 + need, ());	//These should be equal.
		increment_rsn(&mut self.rsn, &mut self.destroyed);
		Ok(out)
	}
	pub(crate) fn get_min_postgap(&self) -> usize
	{
		1 + self.tag	//Extra 1 byte for length.
	}
	pub(crate) fn get_ciphertext_size(&self, ptsize: usize) -> usize
	{
		if let Some(ctsize) = self.key.encrypted_size(ptsize.saturating_add(1)) {
			ctsize.saturating_add(5)
		} else {
			//??? That should have worked.
			ptsize.saturating_add(6+self.tag)
		}
	}
	pub(crate) fn get_max_plaintext_size(&self, ctsize: usize) -> usize
	{
		if let Some(ptsize) = self.key.max_plaintext_size(ctsize.saturating_sub(5)) {
			ptsize.saturating_sub(1)
		} else {
			//??? That should have worked.
			ctsize.saturating_sub(6+self.tag)
		}
	}
}

const MAX_RECORD_PTXT: usize = 16384;
const MAX_RECORD_CTXT: usize = 16645;

fn write_header(buf: &mut [u8], dstart: usize, ciphertext_size: usize, mode: &Tls13RecordMode)
{
	buf[dstart+0] = 23;	//Outer type is always 23.
	buf[dstart+1] = 3;				//"TLS 1.0" or "TLS 1.2".
	buf[dstart+2] = mode.get_minor();
	buf[dstart+3] = (ciphertext_size >> 8) as u8;
	buf[dstart+4] = ciphertext_size as u8;
}

impl Deprotector
{
	pub fn new_x(key: &[u8], algorithm: ProtectorType, mode: Tls13RecordMode) -> Result<Self, ()>
	{
		let alginfo = AlgorithmInfo::new(algorithm)?;
		//Split the key. key_iv.len() will be at most alginfo.noncelen, which is at most MAX_NONCE.
		let (key_k, key_iv) = dtry!(slice_splitn_at_exact(key, (alginfo.keylen, alginfo.noncelen)));
		let mut iv = [0; MAX_NONCE];
		copy_maximum(&mut iv, key_iv);
		Ok(Deprotector{
			key: dtry!(algorithm.new_decryptor(key_k)),
			iv: iv,
			rsn: 0,
			destroyed: false,
			nonce: alginfo.noncelen - 8,	//Always offset by 8 bytes.
			mode: mode,
		})
	}
	fn decrypt_common(&self, buf: &[u8], _iv: &mut [u8; MAX_NONCE]) -> Result<bool, ()>
	{
		//Fake CCS handling.
		if self.mode.is_fake_ccs(buf) { return Ok(true); }
		//If context has been destroyed, fail.
		fail_if!(self.destroyed, ());
		//Iv. We know self.nonce + 8 <= MAX_NONCE = _iv.len().
		for i in 0..8usize {
			let byteshift = (!i as u32) & 7;
			let x = (self.rsn >> 8 * byteshift) as u8;
			_iv[i+self.nonce] ^= x;
		}
		fail_if!(buf.len() < 5, ());
		fail_if!(buf[0] != 23, ());		//Bad outer type.
		//By check above, buf.len() >= 5.
		let recsize_check = buf.len() - 5;
		let recsize_header = (buf[3] as usize) * 256 + (buf[4] as usize);
		fail_if!(recsize_header != recsize_check, ());
		Ok(false)
	}
	fn depad(&self, buf: &[u8]) -> Result<(u8, usize), ()>
	{
		let mut pktend = dtry!(buf.len().checked_sub(1));
		while pktend > 0 && buf[pktend] == 0 { pktend -= 1; }
		let rtype = buf[pktend];
		//ChangeCipherSpec can not be protected in TLS 1.3.
		fail_if!(rtype == PROTO_CCS || rtype == 0, ());
		Ok((rtype, pktend))
	}
	pub(crate) fn deprotect(&mut self, buf: &mut [u8]) -> Result<(u8, Range<usize>), ()>
	{
		let mut _iv: [u8; MAX_NONCE] = self.iv;
		//This returns true on fake CCS.
		if self.decrypt_common(buf, &mut _iv)?  { return Ok((0, 0..0)); }
		//Buf is at least 5 bytes, so slice is in-range.
		let outlen = {
			let (header, buffer) = buf.split_at_mut(5);
			let _iv = &_iv[..(self.nonce+8)];		//Strip extra part away.
			dtry!(self.key.decrypt_inplace(&_iv, self.mode.transform_ad(header), buffer))
		};
		//outlen <= buf.len() - 5, so outlen + 4 is a valid index and also fits into usize (since outlen
		//can be at most 65535). If padding is valid, pktend stays out of header. It might decrement all
		//the way to 5 for empty record.
		let (rtype, pktend) = self.depad(&buf[..(outlen+5)])?;
		fail_if!(pktend < 5, ());
		//Increment the RSN and return range.
		increment_rsn(&mut self.rsn, &mut self.destroyed);
		Ok((rtype, 5..pktend))
	}
	fn __pre_deprotect_btb<'a,'b>(&mut self, src: &'a [u8], _iv: &'b mut [u8;MAX_NONCE]) ->
		Result<Option<(&'a [u8], &'a [u8], &'b [u8])>,()>
	{
		//This returns true on fake CCS.
		if self.decrypt_common(src, _iv)?  { return Ok(None); }

		//Buf is at least 5 bytes, so slice is in-range.
		let (header, buffer) = src.split_at(5);
		let _iv = &_iv[..(self.nonce+8)];		//Strip extra part away.
		Ok(Some((self.mode.transform_ad(header), buffer, _iv)))
	}
	pub(crate) fn deprotect_btb(&mut self, src: &[u8], dst: &mut [u8]) -> Result<(u8, usize), ()>
	{
		//This returns true on fake CCS.
		let mut _iv: [u8; MAX_NONCE] = self.iv;
		let outlen = match self.__pre_deprotect_btb(src, &mut _iv)? {
			Some((ad, buffer, iv)) => {
				dtry!(self.key.decrypt_buffer_to_buffer(iv, ad, dst, buffer)).len()
			},
			None => return Ok((0, 0)),
		};
		//Depad. This checks for overrun internally.
		let (rtype, pktend) = self.depad(&dst[..outlen])?;
		//Increment the RSN and return range.
		increment_rsn(&mut self.rsn, &mut self.destroyed);
		Ok((rtype, pktend))
	}
	pub(crate) fn deprotect_btb2<'a>(&mut self, src: &[u8], dst: &'a mut [core::mem::MaybeUninit<u8>]) ->
		Result<(ContentType, &'a [u8]), ()>
	{
		//This returns true on fake CCS.
		let mut _iv: [u8; MAX_NONCE] = self.iv;
		let outbuf = match self.__pre_deprotect_btb(src, &mut _iv)? {
			Some((ad, buffer, iv)) => dtry!(self.key.decrypt_buffer_to_buffer(iv, ad, dst, buffer)),
			None => return Ok((ContentType::new(0), &[])),
		};
		//Depad. This checks for overrun internally.
		let (rtype, pktend) = self.depad(outbuf)?;
		let outbuf = dtry!(outbuf.get(..pktend));
		//Increment the RSN and return range.
		increment_rsn(&mut self.rsn, &mut self.destroyed);
		Ok((ContentType::new(rtype), outbuf))
	}
}

impl Extractor
{
	pub fn new(master_key: &HashOutput2, prf: HashFunction2) -> Result<Self, Tls13KeyScheduleError>
	{
		Ok(Extractor{secret:master_key.clone(), function:prf})
	}
	pub(crate) fn extract(&self, label: &str, context: Option<&[u8]>, buffer: &mut [u8],
		debug: &mut Option<&mut dyn FnMut(TlsDebug)>) -> Result<(), ExtractorError>
	{
		let context = context.unwrap_or(&[]);	//Defaults to empty.
		//The maximum output is 255 output blocks, or 65535 bytes, whatever is less.
		let max_hkdf = min(self.function.tls13_hkdf_expand_label_max(), 65535);
		let bufferlen = buffer.len();
		sanity_check!(bufferlen <= max_hkdf, F
			"TLS 1.3 exporter output too long ({bufferlen}, max {max_hkdf} bytes)");
		//TLS 1.3 hashes the context, rederives key on label (supposedly for forward secrecy, but
		//we can't use it) and doesn't have length limit.
		let empty_hash = self.function.hash(b"");
		let tmpkey = self.function.tls13_derive_secret(&self.secret,
			&tls13_l(HkdfLabel::Custom(label).get()), &empty_hash);
		let hctx = self.function.hash(context);
		self.function.tls13_hkdf_expand_label(&tmpkey, &tls13_l(HkdfLabel::ExporterD.get()), &hctx, buffer);
		__call_debug(debug, TlsDebug::Tls13Exporter {
			exporter_master_secret: &self.secret,
			label: label,
			per_label_key: &tmpkey,
			context: context,
			exported_value: &buffer,
		});
		Ok(())
	}
	pub(crate) fn debug_export(&self) -> &[u8] { self.secret.deref() }
}

/*
#[test]
fn basic_protectors()
{
	let key = [1;44];
	let mut msg = [0;128];
	let msg2 = [0;42-5];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	assert_eq!(msg[0], 23);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 1);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 54);
	assert_eq!(d.deprotect(&mut msg[..59]).unwrap(), (27, 5..42));
	assert_eq!(&msg[5..42], &msg2[..]);
}

#[test]
fn basic_protectors_off()
{
	let key = [1;44];
	let mut msg = [0;128];
	let msg2 = [0;42-7];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 7..42, 0).unwrap(), 2..59);
	assert_eq!(msg[2], 23);
	assert_eq!(msg[3], 3);
	assert_eq!(msg[4], 1);
	assert_eq!(msg[5], 0);
	assert_eq!(msg[6], 52);
	assert_eq!(d.deprotect(&mut msg[2..59]).unwrap(), (27, 5..40));
	assert_eq!(&msg[7..42], &msg2[..]);
}

#[test]
fn basic_protectors_pad()
{
	let key = [1;44];
	let mut msg = [0;128];
	let msg2 = [0;42-5];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	msg[50] = 255;
	assert_eq!(e.protect(&mut msg, 27, 5..42, 11).unwrap(), 0..70);
	assert_eq!(msg[0], 23);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 1);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 65);
	assert_eq!(d.deprotect(&mut msg[..70]).unwrap(), (27, 5..42));
	assert_eq!(&msg[5..42], &msg2[..]);
}

#[test]
fn basic_protectors_exact()
{
	let key = [1;44];
	let mut msg = [0;59];
	let msg2 = [0;42-5];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	assert_eq!(msg[0], 23);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 1);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 54);
	assert_eq!(d.deprotect(&mut msg[..59]).unwrap(), (27, 5..42));
	assert_eq!(&msg[5..42], &msg2[..]);
}

#[test]
fn basic_protectors_pad_big()
{
	let key = [1;44];
	let mut msg = [0;20*1024];
	let msg2 = [0;42-5];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	msg[50] = 255;
	assert_eq!(e.protect(&mut msg, 27, 5..42, 32768).unwrap(), 0..16406);
	assert_eq!(msg[0], 23);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 1);
	assert_eq!(msg[3], 0x40);
	assert_eq!(msg[4], 0x11);
	assert_eq!(d.deprotect(&mut msg[..16406]).unwrap(), (27, 5..42));
	assert_eq!(&msg[5..42], &msg2[..]);
}

#[test]
fn basic_protectors_multi()
{
	let key = [1;44];
	let mut msg = [0;128];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	assert_eq!(d.deprotect(&mut msg[..59]).unwrap(), (27, 5..42));
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	assert_eq!(d.deprotect(&mut msg[..59]).unwrap(), (27, 5..42));
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	assert_eq!(d.deprotect(&mut msg[..59]).unwrap(), (27, 5..42));
	assert_eq!(e.2, 3);
	assert_eq!(d.2, 3);
}

#[test]
fn gap_too_small()
{
	let key = [1;28];
	let mut msg = [0;128];
	let mut e = Protector::new_aes128(&key).unwrap();
	e.protect(&mut msg, 27, 4..42, 0).unwrap_err();
	e.protect(&mut msg, 27, 6..42, 0).unwrap();
	let key = [1;44];
	let mut e = Protector::new_chacha20(&key).unwrap();
	e.protect(&mut msg, 27, 4..42, 0).unwrap_err();
	e.protect(&mut msg, 27, 6..42, 0).unwrap();
}

#[test]
fn basic_protectors_desync()
{
	let key = [1;44];
	let mut msg = [0;128];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	for i in msg.iter_mut() { *i = 0; }
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	assert_eq!(msg[0], 23);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 1);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 54);
	d.deprotect(&mut msg[..59]).unwrap_err();
}

#[test]
fn basic_protectors_wrap_e()
{
	let key = [1;44];
	let mut msg = [0;128];
	let mut e = Protector::new_chacha20(&key).unwrap();
	e.2 = !0u64;
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	for i in msg.iter_mut() { *i = 0; }
	e.protect(&mut msg, 27, 5..42, 0).unwrap_err();
}


#[test]
fn basic_protectors_wrap_d()
{
	let key = [1;44];
	let mut msg = [0;128];
	let mut e = Protector::new_chacha20(&key).unwrap();
	let mut e2 = Protector::new_chacha20(&key).unwrap();
	let mut d = Deprotector::new_chacha20(&key).unwrap();
	e2.2 = !0u64;
	d.2 = !0u64;
	assert_eq!(e2.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	assert_eq!(d.deprotect(&mut msg[..59]).unwrap(), (27, 5..42));
	for i in msg.iter_mut() { *i = 0; }
	assert_eq!(e2.2, !0u64);
	assert_eq!(d.2, !0u64);
	e.2 = d.2;
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	d.deprotect(&mut msg[..59]).unwrap_err();
}

#[test]
fn basic_protectors2()
{
	let key = [1;28];
	let mut msg = [0;128];
	let msg2 = [0;42-5];
	let mut e = Protector::new_aes128(&key).unwrap();
	let mut d = Deprotector::new_aes128(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	assert_eq!(msg[0], 23);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 1);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 54);
	assert_eq!(d.deprotect(&mut msg[..59]).unwrap(), (27, 5..42));
	assert_eq!(&msg[5..42], &msg2[..]);
}

#[test]
fn basic_protectors3()
{
	let key = [1;44];
	let mut msg = [0;128];
	let msg2 = [0;42-5];
	let mut e = Protector::new_aes256(&key).unwrap();
	let mut d = Deprotector::new_aes256(&key).unwrap();
	assert_eq!(e.protect(&mut msg, 27, 5..42, 0).unwrap(), 0..59);
	assert_eq!(msg[0], 23);
	assert_eq!(msg[1], 3);
	assert_eq!(msg[2], 1);
	assert_eq!(msg[3], 0);
	assert_eq!(msg[4], 54);
	assert_eq!(d.deprotect(&mut msg[..59]).unwrap(), (27, 5..42));
	assert_eq!(&msg[5..42], &msg2[..]);
}
*/
