use super::Asn1Header;
use super::Certificate;
use core::ops::Deref;

#[test]
fn idp_aalto_fi_precert_reconstruct()
{
	let cert = Certificate(include_bytes!("idp.aalto.fi.der"));
	let precert = include_bytes!("idp.aalto.fi.der.precert");
	let tbs_cert = cert.extract_tbs().unwrap();
	let pieces = tbs_cert.reconstruct_v1_precert().unwrap();
	let reconstructed = pieces.to_vector();
	assert_eq!(&reconstructed, &&precert[..]);
}

#[test]
fn write_sequence_header()
{
	assert!(Asn1Header::blank().deref() == &[]);
	assert!(Asn1Header::new(0x30, 0).unwrap().deref() == &[0x30, 0]);
	assert!(Asn1Header::new(0x30, 1).unwrap().deref() == &[0x30, 1]);
	assert!(Asn1Header::new(0x30, 127).unwrap().deref() == &[0x30, 127]);
	assert!(Asn1Header::new(0x30, 128).unwrap().deref() == &[0x30, 129, 128]);
	assert!(Asn1Header::new(0x30, 255).unwrap().deref() == &[0x30, 129, 255]);
	assert!(Asn1Header::new(0x30, 256).unwrap().deref() == &[0x30, 130, 1, 0]);
	assert!(Asn1Header::new(0x30, 258).unwrap().deref() == &[0x30, 130, 1, 2]);
	assert!(Asn1Header::new(0x30, 65535).unwrap().deref() == &[0x30, 130, 255, 255]);
	assert!(Asn1Header::new(0x30, 65536).unwrap().deref() == &[0x30, 131, 1, 0, 0]);
	assert!(Asn1Header::new(0x30, 66051).unwrap().deref() == &[0x30, 131, 1, 2, 3]);
	assert!(Asn1Header::new(0x30, 16777215).unwrap().deref() == &[0x30, 131, 255, 255, 255]);
}
