#!/usr/bin/python3
from datetime import datetime, timezone;
import base64;
import json;
import time;
import sys;
import os;
from hashlib import sha256;

def write_file(filename, contents):
	m = sha256();
	m.update(contents);
	newhash = m.digest();
	try:
		fp=open(filename,"rb");
		oldcontent = fp.read();
		m = sha256();
		m.update(oldcontent);
		oldhash = m.digest();
		#If hashes match, no update.
		if oldhash == newhash: return False
	except IOError:
		#Failed to compute hash. Assume update.
		pass;
	print("Updating "+filename);
	fp=open(filename,"wb");
	fp.write(contents);
	return True;

def lname_to_symbol(lname):
	xname = "";
	for c in lname:
		if ord(c) >= 97 and ord(c) <= 122:
			xname += chr(ord(c)-32);
		elif c == '-':
			xname += '_';
		else:
			xname += c;
	return xname;

data = open("../../../data/timestamp.txt","rb");
timestamp = int(data.read().decode("utf-8"));
special_cutoff = int(timestamp);	#Use current time.

LOG_LINGER=1187;
#Certificate validty change.
if special_cutoff > 1591142400: LOG_LINGER=825;
if special_cutoff > 1633305600: LOG_LINGER=398;

desc_remap={
	"DigiCert 'Nessie 2018' log": "DigiCert Nessie2018 Log",
	"DigiCert 'Nessie 2019' log": "DigiCert Nessie2019 Log",
	"DigiCert 'Nessie 2020' log": "DigiCert Nessie2020 Log",
	"DigiCert 'Nessie 2021' log": "DigiCert Nessie2021 Log",
	"DigiCert 'Nessie 2022' log": "DigiCert Nessie2022 Log",
	"DigiCert 'Nessie 2023' log": "DigiCert Nessie2023 Log",
	"DigiCert 'Nessie 2024' log": "DigiCert Nessie2024 Log",
	"DigiCert 'Nessie 2025' log": "DigiCert Nessie2025 Log",
	"DigiCert 'Yeti 2018' log": "DigiCert Yeti2018 Log",
	"DigiCert 'Yeti 2019' log": "DigiCert Yeti2019 Log",
	"DigiCert 'Yeti 2020' log": "DigiCert Yeti2020 Log",
	"DigiCert 'Yeti 2021' log": "DigiCert Yeti2021 Log",
	"DigiCert 'Yeti 2022' log": "DigiCert Yeti2022 Log",
	"DigiCert 'Yeti 2022' log #2": "DigiCert Yeti2022 Log 2",
	"DigiCert Yeti2022-2 Log": "DigiCert Yeti2022 Log 2",
	"DigiCert 'Yeti 2023' log": "DigiCert Yeti2023 Log",
	"DigiCert 'Yeti 2024' log": "DigiCert Yeti2024 Log",
	"DigiCert 'Yeti 2025' log": "DigiCert Yeti2025 Log",
	"Cloudflare 'Nimbus 2017' log": "Cloudflare 'Nimbus2017' Log",
	"Cloudflare 'Nimbus 2018' log": "Cloudflare 'Nimbus2018' Log",
	"Cloudflare 'Nimbus 2019' log": "Cloudflare 'Nimbus2019' Log",
	"Cloudflare 'Nimbus 2020' log": "Cloudflare 'Nimbus2020' Log",
	"Cloudflare 'Nimbus 2021' log": "Cloudflare 'Nimbus2021' Log",
	"Cloudflare 'Nimbus 2022' log": "Cloudflare 'Nimbus2022' Log",
	"Cloudflare 'Nimbus 2023' log": "Cloudflare 'Nimbus2023' Log",
	"Cloudflare 'Nimbus 2024' log": "Cloudflare 'Nimbus2024' Log",
	"Cloudflare 'Nimbus 2025' log": "Cloudflare 'Nimbus2025' Log",
	"Google 'Argon 2018' log": "Google 'Argon2018' log",
	"Google 'Argon 2019' log": "Google 'Argon2019' log",
	"Google 'Argon 2020' log": "Google 'Argon2020' log",
	"Google 'Argon 2021' log": "Google 'Argon2021' log",
	"Google 'Argon 2022' log": "Google 'Argon2022' log",
	"Google 'Argon 2023' log": "Google 'Argon2023' log",
	"Google 'Argon 2024' log": "Google 'Argon2024' log",
	"Google 'Argon 2025' log": "Google 'Argon2025' log",
	"Google 'Argon 2017' log": "Google 'Argon2017' log",
	"Google 'Xenon 2018' log": "Google 'Xenon2018' log",
	"Google 'Xenon 2019' log": "Google 'Xenon2019' log",
	"Google 'Xenon 2020' log": "Google 'Xenon2020' log",
	"Google 'Xenon 2021' log": "Google 'Xenon2021' log",
	"Google 'Xenon 2022' log": "Google 'Xenon2022' log",
	"Google 'Xenon 2023' log": "Google 'Xenon2023' log",
	"Google 'Xenon 2024' log": "Google 'Xenon2024' log",
	"Google 'Xenon 2025' log": "Google 'Xenon2025' log",
	"Google 'Xenon 2023 log": "Google 'Xenon2023' log",
	"Google 'Xenon 2024 log": "Google 'Xenon2024' log",
	"Google 'Xenon 2025 log": "Google 'Xenon2025' log",
	"TrustAsia Log 2020": "Trust Asia Log2020",
	"TrustAsia Log 2021": "Trust Asia Log2021",
	"TrustAsia Log 2022": "Trust Asia Log2022",
	"TrustAsia Log 2023": "Trust Asia Log2023",
	"TrustAsia Log 2024": "Trust Asia Log2024",
	"TrustAsia Log 2025": "Trust Asia Log2025",
};

compromised={
	"DigiCert Log Server 2": True,
};

lnames={
	#Cloudflare
	"Cloudflare 'Nimbus2017' Log": "cloudflare-nimbus2017",
	"Cloudflare 'Nimbus2018' Log": "cloudflare-nimbus2018",
	"Cloudflare 'Nimbus2019' Log": "cloudflare-nimbus2019",
	"Cloudflare 'Nimbus2020' Log": "cloudflare-nimbus2020",
	"Cloudflare 'Nimbus2021' Log": "cloudflare-nimbus2021",
	"Cloudflare 'Nimbus2022' Log": "cloudflare-nimbus2022",
	"Cloudflare 'Nimbus2023' Log": "cloudflare-nimbus2023",
	"Cloudflare 'Nimbus2024' Log": "cloudflare-nimbus2024",
	"Cloudflare 'Nimbus2025' Log": "cloudflare-nimbus2025",
	#Sectigo (previously Comodo). Use the old "comodo" name for compatiblity.
	"Sectigo 'Sabre' CT log": "comodo-sabre",
	"Sectigo 'Mammoth' CT log": "comodo-mammoth",
	#Digicert
	"DigiCert Log Server": "digicert",
	"DigiCert Log Server 2": "digicert2",
	"DigiCert Yeti2018 Log": "digicert-yeti2018",
	"DigiCert Yeti2019 Log": "digicert-yeti2019",
	"DigiCert Yeti2020 Log": "digicert-yeti2020",
	"DigiCert Yeti2021 Log": "digicert-yeti2021",
	"DigiCert Yeti2022 Log": "digicert-yeti2022",		#Fucked over by data corruption.
	"DigiCert Yeti2022 Log 2": "digicert-yeti2022-2",
	"DigiCert Yeti2023 Log": "digicert-yeti2023",
	"DigiCert Yeti2024 Log": "digicert-yeti2024",
	"DigiCert Yeti2025 Log": "digicert-yeti2025",
	"DigiCert Nessie2018 Log": "digicert-nessie2018",
	"DigiCert Nessie2019 Log": "digicert-nessie2019",
	"DigiCert Nessie2020 Log": "digicert-nessie2020",
	"DigiCert Nessie2021 Log": "digicert-nessie2021",
	"DigiCert Nessie2022 Log": "digicert-nessie2022",
	"DigiCert Nessie2023 Log": "digicert-nessie2023",
	"DigiCert Nessie2024 Log": "digicert-nessie2024",
	"DigiCert Nessie2025 Log": "digicert-nessie2025",
	#Google
	"Google 'Argon2017' log": "google-argon2017",
	"Google 'Argon2018' log": "google-argon2018",
	"Google 'Argon2019' log": "google-argon2019",
	"Google 'Argon2020' log": "google-argon2020",
	"Google 'Argon2021' log": "google-argon2021",
	"Google 'Argon2022' log": "google-argon2022",
	"Google 'Argon2023' log": "google-argon2023",
	"Google 'Argon2024' log": "google-argon2024",
	"Google 'Argon2025' log": "google-argon2025",
	"Google 'Xenon2018' log": "google-xenon2018",
	"Google 'Xenon2019' log": "google-xenon2019",
	"Google 'Xenon2020' log": "google-xenon2020",
	"Google 'Xenon2021' log": "google-xenon2021",
	"Google 'Xenon2022' log": "google-xenon2022",
	"Google 'Xenon2023' log": "google-xenon2023",
	"Google 'Xenon2024' log": "google-xenon2024",
	"Google 'Xenon2025' log": "google-xenon2025",
	"Google 'Aviator' log": "google-aviator",
	"Google 'Icarus' log": "google-icarus",
	"Google 'Pilot' log": "google-pilot",
	"Google 'Rocketeer' log": "google-rocketeer",
	"Google 'Skydiver' log": "google-skydiver",
	#Symantec (now Digicert)
	"Symantec log": "symantec",
	"Symantec 'Vega' log": "symantec-vega",
	"Symantec 'Sirius' log": "symantec-sirus",
	#Venafi
	"Venafi Gen2 CT log": "venafi-gen2",
	#Let's encrypt.
	"Let's Encrypt 'Oak2019' log": "letsencrypt-oak2019",
	"Let's Encrypt 'Oak2020' log": "letsencrypt-oak2020",
	"Let's Encrypt 'Oak2021' log": "letsencrypt-oak2021",
	"Let's Encrypt 'Oak2022' log": "letsencrypt-oak2022",
	"Let's Encrypt 'Oak2023' log": "letsencrypt-oak2023",
	"Let's Encrypt 'Oak2024' log": "letsencrypt-oak2024",
	"Let's Encrypt 'Oak2024H1' log": "letsencrypt-oak2024h1",
	"Let's Encrypt 'Oak2024H2' log": "letsencrypt-oak2024h2",
	"Let's Encrypt 'Oak2025' log": "letsencrypt-oak2025",
	#Trust asia.
	"Trust Asia Log2020": "trustasia-2020",
	"Trust Asia Log2021": "trustasia-2021",
	"Trust Asia Log2022": "trustasia-2022",
	"Trust Asia Log2023": "trustasia-2023",
	"Trust Asia Log2024": "trustasia-2024",
	"Trust Asia Log2024-2": "trustasia-2024-2",
	"Trust Asia Log2025": "trustasia-2025",
	"Trust Asia CT2021": "trustasia-ct-2021",
	"Trust Asia CT2022": "trustasia-ct-2022",
	"Trust Asia CT2023": "trustasia-ct-2023",
	"Trust Asia CT2024": "trustasia-ct-2024",
	"Trust Asia CT2025": "trustasia-ct-2025",
	#Various disqualified logs.
	"Certly.IO log": "certlyio",
	"Izenpe log": "izenpe",
	"WoSign log": "wosign",
	"Venafi log": "venafi",
	"CNNIC CT log": "cnnic-ct",
	"StartCom log": "startcom",
	"GDCA 'CT Log 1' log": "gdca-log1",
	"GDCA 'CT Log 2' log": "gdca-log2",
	"PuChuangSiDa CT log": "puchuangsida",
};

def decode_iso(ts):
	year = int(ts[0:4]);
	month = int(ts[5:7]);
	day = int(ts[8:10]);
	hour = int(ts[11:13]);
	minute = int(ts[14:16]);
	second = int(ts[17:19]);
	dt = datetime(year, month, day, hour=hour, minute=minute, second=second, tzinfo=timezone.utc);
	dt = (dt - datetime(1970,1,1,tzinfo=timezone.utc)).total_seconds();
	return int(dt);

def do_schema_v2_log(logdata, j):
	desc = j["description"];
	if desc in desc_remap: desc = desc_remap[desc];
	if desc in compromised: return;		#Compromised!!!
	key = j["key"];
	state = j["state"];
	if (not "retired" in state) and (not "usable" in state) and (not "qualified" in state) and (not "rejected" in state) and (not "readonly" in state) and (not "pending" in state):
		print("Warning: Unknown log '"+desc+"' state", file=sys.stderr);
		return;
	if "rejected" in state:
		return;		#Ignore log.
	special = None;
	disc = None;
	if "rejected" in state:
		return;		#Ignore log.
	special = None;
	disc = None;
	if "retired" in state:
		disc = decode_iso(state["retired"]["timestamp"]);
	if "temporal_interval" in j:
		special = decode_iso(j["temporal_interval"]["end_exclusive"]);
	if desc not in lnames:
		print("Warning: Unknown log '"+desc+"'", file=sys.stderr);
		return;
	lname = lnames[desc];
	if disc is not None and desc in logdata and (logdata[desc]["disc"] is None or logdata[desc]["disc"] > disc):
		logdata[desc]["disc"] = disc;
	if special is not None and desc in logdata and logdata[desc]["special"] is None:
		logdata[desc]["special"] = special;
	if not desc in logdata:
		logdata[desc] = {
			'desc': desc,
			'key': key,
			'disc': disc,
			'lname': lname,
			'special': special,
		};

logdata = {};
data = open("../../../data/google-logs3.json","rb");
content = data.read().decode("utf-8");
data=json.loads(content);
data=data["operators"];
for i in data:
	logs = i["logs"];
	for j in logs: do_schema_v2_log(logdata, j);

data = open("../../../data/apple-logs.json","rb");
content = data.read().decode("utf-8");
data=json.loads(content);
data=data["operators"];
for i in data:
	logs = i["logs"];
	for j in logs: do_schema_v2_log(logdata, j);

to_delete = [];
for desc in logdata:
	disc = logdata[desc]['disc'];
	special = logdata[desc]['special'];
	#If preassigned date range ran out, expire the log immediately, as all certificates in it should be
	#expired by now.
	if special is not None and special < special_cutoff: to_delete.append(desc);
	#If disqualification was more than certificate max validity ago, expire the log as it should not
	#have any valid certificates anymore.
	if disc is not None and disc < special_cutoff - LOG_LINGER * 86400: to_delete.append(desc);

lname_to_delete=[];
for delent in to_delete:
	if delent in logdata:
		del logdata[delent];

fp = open("src/builtin-logs.inc.tmp", "w");
seqno=0;
lookup={};
for desc in logdata:
	key = logdata[desc]['key'];
	desc = logdata[desc]['desc'];
	disc = logdata[desc]['disc'];
	lname = logdata[desc]['lname'];
	special = logdata[desc]['special'];
	lookup[lname] = lname_to_symbol(lname);
	print("static LOG_{:s}: BuiltinLogData = BuiltinLogData {{".format(lookup[lname]), file=fp);
	seqno += 1;
	print("\tname: \"{:s}\",".format(desc), file=fp);
	if disc != None: print("\texpiry: Some({:d}),".format(disc), file=fp);
	elif special != None: print("\texpiry: Some({:d}),".format(special), file=fp);
	else: print("\texpiry: None,", file=fp);
	keyd = base64.b64decode(key);
	key2 = "";
	for b in keyd: key2 += "{:d},".format(b);
	print("\tkey: &[{:s}],".format(key2), file=fp);
	print("};", file=fp);
print("static _ALL_LOGS: [&'static BuiltinLogData; {:d}] = [".format(seqno), file=fp);
for k in lookup: print("\t&LOG_{:s},".format(lookup[k]), file=fp);
print("];", file=fp);
print("fn _for_log(name: &str) -> Option<&'static BuiltinLogData> { match name {", file=fp);
for k in lookup: print("\t\"{:s}\" => Some(&LOG_{:s}),".format(k, lookup[k]), file=fp);
print("_ => None}}", file=fp);
fp.close();

data = open("../../../data/timestamp.txt","rb");
timestamp = int(data.read().decode("utf-8"));
ts = time.gmtime(timestamp);
vcode = "{:04}{:02}{:02}".format(ts.tm_year, ts.tm_mon, ts.tm_mday);

fp=open("src/builtin-logs.inc.tmp","rb");
newcontent = fp.read();
if write_file("src/builtin-logs.inc", newcontent):
	print("Certificate transparency DB updated");
	fp=open("Cargo.toml.in","r");
	fpw=open("Cargo.toml","w");
	lines = [line.rstrip('\n') for line in fp];
	for line in lines:
		p = line.find("@minor_versioncode@");
		if p < 0:
			#No mods, print as is.
			print(line, file=fpw);
		else:
			print(line[:p]+vcode+line[p+19:], file=fpw);

os.remove("src/builtin-logs.inc.tmp");
