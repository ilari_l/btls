//!Built-in certificate transparency log data
#![forbid(unsafe_code)]
#![forbid(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]

use btls_aux_certificate_transparency::TrustedLogInfo2;

///A log data
pub struct BuiltinLogData
{
	name: &'static str,
	expiry: Option<i64>,
	key: &'static [u8],
}

impl TrustedLogInfo2 for BuiltinLogData
{
	fn get_name<'a>(&'a self) -> &'a str { self.name }
	fn get_expiry(&self) -> Option<i64> { self.expiry }
	fn get_key<'a>(&'a self) -> &'a [u8] { self.key }
}

impl BuiltinLogData
{
	///Get by log.
	///
	/// # Parameters:
	///
	/// * `name`: Name of the log.
	///
	/// # Error conditions:
	///
	/// * Log name is unknown.
	///
	/// # Return value:
	///
	///The data for the log.
	pub fn for_log(name: &str) -> Option<&'static BuiltinLogData> { _for_log(name) }
	///Get list of logs.
	///
	/// # Return value:
	///
	///Slice of all available builin logs.
	pub fn get_all_logs() -> &'static [&'static BuiltinLogData] { &_ALL_LOGS[..] }
}

include!("builtin-logs.inc");
