//!Chacha20-Poly1305-AEAD
//!
//!This crate contains a low-level implementation of the Chacha20-Poly1305-AEAD encryption method, and a fast
//!backtracking-resistant DRBG.
//!
//!The most important items are:
//!
//! * Structure [`Chacha20Poly1305`](struct.Chacha20Poly1305.html), which is low-level encryption/decryption context
//!for the Chacha20-Poly1305-AEAD algorithm.
//! * Structure [`FastKeDrbg`](struct.FastKeDrbg.html), which is an implementation of fast-key-erasure DRBG, which
//!is an efficient backtracking-resistant expansion of short random seed into stream of random bytes of almost
//!arbitrary length.
#![forbid(missing_docs)]
#![allow(unsafe_code)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]

pub use btls_aux_aead_base::AeadError;
pub use btls_aux_aead_base::LowLevelAeadContext;
use btls_aux_fail::fail_if;
use btls_aux_sha3::Shake256384;
use btls_aux_chacha20poly1305aead_cimpl::chacha_final;
use btls_aux_chacha20poly1305aead_cimpl::chacha_update;
use btls_aux_chacha20poly1305aead_cimpl::chacha_initialize_state;
use btls_aux_chacha20poly1305aead_cimpl::poly1305_finish;
use btls_aux_chacha20poly1305aead_cimpl::poly1305_init_ext;
use btls_aux_chacha20poly1305aead_cimpl::poly1305_update;
use btls_aux_chacha20poly1305aead_cimpl::do_post as do_poweron;
use btls_aux_chacha20poly1305aead_cimpl::CHACHA_BUFFER_SIZE;
use btls_aux_chacha20poly1305aead_cimpl::POLY1305_BUFFER_SIZE;
use btls_aux_memory::calculate_syndrome;
use btls_aux_memory::copy_maximum;
use btls_aux_memory::ObjectExt;
use core::cmp::min;
use core::mem::size_of;
use core::mem::size_of_val;
use core::slice::from_raw_parts;
use core::slice::from_raw_parts_mut;

fn internal_error() -> AeadError { AeadError::internal_fault(CHACHA20POLY1305_NAME) }

fn fast_ke_drbg_expand(data: &mut [u8], used: &mut usize)
{
	static ZEROES: [u8;4096] = [0;4096];		//Assumed to be multiple of 64.
	let mut scratch = ChachaState([0;CHACHA_BUFFER_SIZE]);
	let scratch = &mut scratch.0;
	//Write state. scratch is 256 bytes anwyay, so these indices <56 are definitely in bounds.
	copy_maximum(&mut scratch[..48], data);		//Will return 48.
	if u32::from_le(0x12345678) == 0x12345678 {
		scratch[48] = 20;		//Little-endian.
	} else {
		scratch[47+size_of::<usize>()] = 20;	//Big-endian.
	}
	//Handle output in chunks of 4096 bytes.
	let mut dptr = 0;
	while dptr < data.len() {
		let remaining = min(data.len() - dptr, ZEROES.len());
		unsafe {
			//1) Scratch is 256 bytes and aligned to 64 bytes.
			//2) ZEROES is at least remaining.
			//3) dptr < data.len(), so add is in bounds.
			//4) remaining + dptr <= data.len().
			//5) remaining is multiple of 64.
			let dst = data.as_mut_ptr().add(dptr);
			chacha_update(scratch.as_mut_ptr(), ZEROES.as_ptr(), dst, remaining);
		}
		dptr += remaining;
	}
	*used = 48;
}

fn __extract(self0: &mut usize, output: &mut [u8], state: &mut [u8])
{
	//The point to start wiping.
	let mut old_used = *self0;
	let mut dptr = 0;
	while dptr < output.len()
	{
		//The invariants that hold at this point are dptr < output.len(), state.len() > 48 and
		//self.0 <= state.len(). The first holds by the fact that the loop termination condition
		//does not hold. The second holds because state.len() = self.1, which needs to be multiple
		//of 64 at least 48 (so at least 64). The third holds by condition on loop end, or the
		//check that self.0 is not larger than state.len() above.
		if *self0 >= state.len() {
			//Rotate buffer, everything has been used. self.0 is reset to 48. The point to start
			//wiping is 48, as the last pert has already been wiped. Part 0...47 is never wiped.
			fast_ke_drbg_expand(state, self0);
			old_used = 48;	//Reset, so wipe starts from start of buffer.
		}
		let src = state.get(*self0..).unwrap_or_default();
		let dst = output.get_mut(dptr..).unwrap_or_default();
		let amt = copy_maximum(dst, src);
		//These can grow self.0 to at most state.len(), so loop invariant is satisfied. And
		//dptr can grow to at most output.len(), which triggers exit from loop if equal.
		*self0 += amt;
		dptr += amt;
	}
	//Erase past contents to provode byte-granularity backtracing resistance. By invariants, old_used
	//<= self.0 <= state.len(). The part after does not get erased because it is future input to use.
	//The buffer is presumably used afterwards, so not security erase.
	if let Some(part) = state.get_mut(old_used..*self0) {
		for b in part.iter_mut() { *b = 0; }
	}
}

//The state MUST be at least 48 bytes.
unsafe fn __reseed(self0: &mut usize, state: &mut [u8], entropy: &[u8])
{
	//The state must be at least 48 bytes.
	let nstate = unsafe{state.get_unchecked_mut(..48)};
	//25 element array for Keccak. Perform Shake256(state|i|entropy, 384)
	let mut xstate = Shake256384::new();
	xstate.input(nstate);
	xstate.input(&(*self0 as u64).to_le_bytes());
	xstate.input(entropy);
	nstate.copy_from_slice(&xstate.output());
	fast_ke_drbg_expand(state, self0);
}


///Part of state for fast-key-erasure DRBG.
///
///This DRBG (Deterministic Random Bit Generator) expands a short random seed into random-looking byte string of
///almost arbitrary length. Additionally, this generator has the properties that the byte sequence extracted is
///independent of sizes of the requests, and that it is is efficient down to very small requests. The DRBG is
///fast-key-erasure DRBG, as described by [`Daniel J. Bernstein`]. The underlying cipher is Chacha20, and mixing
///new seeds is performed using `Shake256`. DRBGs are also called CSPRNGs (Cryptographically Secure Pseudo Random
///Number Generators).
///
///The (possibly large) byte buffer is not stored inside this structure: It must be stored externally.
///
///[`Daniel J. Bernstein`]: https://blog.cr.yp.to/20170723-random.html
pub struct FastKeDrbg(usize, usize);

impl FastKeDrbg
{
	///Initialize a fast-key-erasure DRBG with given state buffer and seed.
	///
	///The seed array  should have full 384 bits of entropy: If it has less than 256 bits of entropy, the
	///output can start to become predictable. Seeds of arbitrary length can be used by passing all zeroes
	///seed and then using the [`reseed()`](#method.reseed) method. However, this will not give the same
	///results for 48-byte seeds as calling this function directly with the seed.
	///
	///The stream generated is function of the seed. Thus, passing the same seed always gives the same output.
	///However, two different seed, even differing by one bit, should give totally different and computationally
	///independent outputs.
	///
	///The state buffer must be non-zero multiple of 64 bytes (e.g. 192 bytes), or the function fails. Also,
	///POST failing causes this function to fail.
	pub fn new(state: &mut [u8], seed: &[u8;48]) -> Result<FastKeDrbg, ()>
	{
		do_poweron()?;
		fail_if!(state.len() <= 48, ());
		fail_if!(state.len() % 64 != 0, ());
		let mut used = FastKeDrbg(0, state.len());
		copy_maximum(state, seed);
		fast_ke_drbg_expand(state, &mut used.0);
		//Ok.
		Ok(used)
	}
	///Extracts data from the fast-key-erasure DRBG into output buffer.
	///
	///The output buffer is filled with pseudorandom bytes and the state buffer is updated in a way
	///that the extracted random numbers can not be determined from the new state.
	///
	///This operation does not deplete "entropy" in the state. If the state has high entropy, recovering the
	///state from the generator output remains hard, no matter how long stream is extracted (within practical
	///limits). This holds even against attacker with access to a large quantum computer.
	///
	///The state buffer must be of the same size as the one passed to the [`FastKeDrbg::new()`](#method.new)
	///call, or this method will fail.
	pub fn extract(&mut self, output: &mut [u8], state: &mut [u8]) -> Result<(), ()>
	{
		//The constructor is not supposed to accept anything that is not nonzero multiple of 64.
		fail_if!(self.0 < 48 || self.0 > state.len(), ());
		fail_if!(self.1 != state.len(), ());
		__extract(&mut self.0, output, state);
		Ok(())
	}
	///Reseed the fast-key-erasure DRBG using given entropy and state buffer.
	///
	///The passed entropy should contain at least 128 bits of entropy. Otherwise an attacker can break the
	///DRBG by brute-force searching the seed.
	///
	///However, if the state is being forked, one may pass a short identifier for the fork (e.g., its process ID)
	///as seed (or even `[0]` on one side and `[1]` on another). This does not provode any security if the parent
	///state has been compromised, but does prevent the child states from producing the same output. Child states
	///producing the same output likely leads to catastrophic system failure.
	///
	///The state buffer must be of the same size as the one passed to the [`FastKeDrbg::new()`](#method.new)
	///call, or this method will fail.
	pub fn reseed(&mut self, state: &mut [u8], entropy: &[u8]) -> Result<(), ()>
	{
		fail_if!(self.1 != state.len() || state.len() < 48, ());
		//state is at least 48 bytes.
		unsafe{__reseed(&mut self.0, state, entropy)};
		Ok(())
	}
}

#[repr(C)]	//Guarantee the parts are consequtive.
struct SB<const N:usize>([u8;64], [[u8;64];N]);

impl<const N:usize> SB<N>
{
	fn new(seed: &[u8;48]) -> SB<N>
	{
		let mut t: SB<N> = unsafe{core::mem::zeroed()};	//Can be zero-initialized.
		copy_maximum(&mut t.0, seed);	//Will return 48.
		t
	}
	#[inline(always)]
	fn as_inner(&mut self) -> &mut [u8]
	{
		let s = size_of_val(self);
		unsafe{from_raw_parts_mut(self.pointer_to_mut().cast(), s)}
	}
}

///State for fast-key-erasure DRBG.
///
///This DRBG (Deterministic Random Bit Generator) expands a short random seed into random-looking byte string of
///almost arbitrary length. Additionally, this generator has the properties that the byte sequence extracted is
///independent of sizes of the requests, and that it is is efficient down to very small requests. The DRBG is
///fast-key-erasure DRBG, as described by [`Daniel J. Bernstein`]. The underlying cipher is Chacha20, and mixing
///new seeds is performed using `Shake256`. DRBGs are also called CSPRNGs (Cryptographically Secure Pseudo Random
///Number Generators).
///
///The parameter `N` controls the size of the buffer. The visible buffer size is `64*N+16` bytes. Note that
///changing the buffer size changes the stream generated.
///
///[`Daniel J. Bernstein`]: https://blog.cr.yp.to/20170723-random.html
pub struct FastKeDrbg2<const N:usize>(usize, SB<N>);

impl<const N:usize> FastKeDrbg2<N>
{
	///Initialize a fast-key-erasure DRBG with given state buffer and seed.
	pub fn new(seed: &[u8;48]) -> Result<FastKeDrbg2<N>, ()>
	{
		do_poweron()?;
		let mut used = FastKeDrbg2(0, SB::new(seed));
		fast_ke_drbg_expand(used.1.as_inner(), &mut used.0);
		//Ok.
		Ok(used)
	}
	///Extracts data from the fast-key-erasure DRBG into output buffer.
	pub fn extract(&mut self, output: &mut [u8])
	{
		let state = self.1.as_inner();
		__extract(&mut self.0, output, state)
	}
	///Reseed the fast-key-erasure DRBG using given entropy and state buffer.
	pub fn reseed(&mut self, entropy: &[u8])
	{
		//self.1.as_inner() returns at least 64 bytes.
		unsafe{__reseed(&mut self.0, self.1.as_inner(), entropy)};
	}
}


//The first is key, the second is standby buffer. We need 64 bytes for padding, 256 for Chacha20 and 320 for
//poly1305.
///Key for Chacha20-Poly1305-AEAD encryption/decryption.
///
///The key used for Chacha20-Poly1305-AEAD encryption and decryption. It also forms a low-level context for
///Chacha20-Poly1305-AEAD, and thus implements [`LowLevelAeadContext`].
///
///This structure is created by the [`LowLevelAeadContext::new_context()`] function.
///
///On release, the key material is wiped from memory.
///
///[`LowLevelAeadContext`]: trait.LowLevelAeadContext.html
///[`LowLevelAeadContext::new_context()`]: trait.LowLevelAeadContext.html#tymethod.new_context
pub struct Chacha20Poly1305([u8;32]);

fn taillen16(x: usize) -> usize
{
	//The subtraction can never overflow because % 16 produces 0 to 15.
	(16 - x % 16) % 16
}

#[repr(align(64))] struct ChachaState([u8;CHACHA_BUFFER_SIZE]);
#[repr(align(64))] struct Poly1305State([u8;POLY1305_BUFFER_SIZE]);

impl ChachaState
{
	fn new(key: &[u8], nonce: &[u8]) -> ChachaState
	{
		let mut state = ChachaState([0;CHACHA_BUFFER_SIZE]);
		chacha_initialize_state(&mut state.0, key, nonce);
		state
	}
	fn ptr(&mut self) -> *mut u8 { self.0.as_mut_ptr() }
}

impl Poly1305State
{
	fn new() -> Poly1305State { Poly1305State([0;POLY1305_BUFFER_SIZE]) }
	fn ptr(&mut self) -> *mut u8 { self.0.as_mut_ptr() }
}

fn write_terminal_mac_block(block: &mut [u8;16], adlen: usize, ptlen: usize)
{
	(&mut block[0..8]).copy_from_slice(&(adlen as u64).to_le_bytes());
	(&mut block[8..16]).copy_from_slice(&(ptlen as u64).to_le_bytes());
}

///Human-readable name for Chacha20-Poly1305.
pub static CHACHA20POLY1305_NAME: &'static str = "Chacha20-Poly1305";
const P_MAX: u64 = 274877906880u64;
const C_MAX: u64 = 274877906896u64;

impl LowLevelAeadContext for Chacha20Poly1305
{
	fn new_context(key: &[u8]) -> Result<Chacha20Poly1305, AeadError>
	{
		AeadError::assert_key(CHACHA20POLY1305_NAME, key, |n|n==32)?;
		do_poweron().map_err(|_|{
			internal_error()
		})?;
		let mut ctx = Chacha20Poly1305([0;32]);
		copy_maximum(&mut ctx.0, key);
		Ok(ctx)
	}
	fn encrypted_size(&self, size: usize) -> Option<usize>
	{
		AeadError::assert_plaintext("", size, 16, P_MAX).ok()
	}
	fn max_plaintext_size(&self, size: usize) -> Option<usize>
	{
		AeadError::assert_ciphertext("", size, C_MAX, |s|s.checked_sub(16)).ok()
	}
	unsafe fn encrypt3(&self, nonce: &[u8], ad: &[u8], optr: *mut u8, iptr: *const u8, ilen: usize,
		trailerlen: usize) -> Result<usize, AeadError>
	{
		AeadError::assert_nonce(CHACHA20POLY1305_NAME, nonce, |n|n==12)?;
		AeadError::assert_ad(CHACHA20POLY1305_NAME, ad, 0xFFFFFFFFFFFFFFFFu64)?;
		let reallen = ilen.saturating_add(trailerlen);
		let tagend = AeadError::assert_plaintext(CHACHA20POLY1305_NAME, reallen, 16, P_MAX)?;

		let mut chacha_state = ChachaState::new(&self.0, nonce);
		let mut poly_state = Poly1305State::new();
		//ad.len() nor reallen can be very near overflow. And this is hint anyway, no real damage if it
		//overflows.
		let hint = (ad.len().saturating_add(15) / 16 + reallen.saturating_add(15) / 16 + 1).
			saturating_mul(16);
		let mut polykey = [0u8; 64];
		let mut tmp = [0u8; 16];

		Self::compute_poly_key(&mut chacha_state, &mut polykey);
		//Safety: From here on out, chacha_state points to valid Chacha state.
		let chacha_state = chacha_state.ptr();
		//Safety: From here on out, poly_state points to valid Poly1305 state.
		let poly_state = poly_state.ptr();
		//Safety: polykey.as_ptr() points to at least 32 readable and initialized bytes.
		unsafe{poly1305_init_ext(poly_state, polykey.as_ptr(), hint);}
		//Safety: ad.as_ptr() points to ad.len() readable and initialized bytes.
		//tmp points to 16 readable and initialized bytes, and return value of taillen16() is
		//always <16.
		unsafe{poly1305_update(poly_state, ad.as_ptr(), ad.len());}
		unsafe{poly1305_update(poly_state, tmp.as_ptr(), taillen16(ad.len()));}

		//Safety: iptr points ilen readable and initialized bytes, and optr points to
		//encrypted_size(ilen+trailerlen) = ilen + trailerlen + 16 > ilen writable bytes.
		let r = unsafe{chacha_update(chacha_state, iptr, optr, ilen)};
		let r2 = if trailerlen > 0 {
			//Handle the trailer. We need to start reading from the start of trailer, but we
			//need to write to whereever the output of previous Chacha run ended.
			//Safety: optr.add(ilen) is valid for trailerlen readable and initialized bytes, r is ilen
			//rounded down, and there is ilen + trailerlen writable bytes at optr.
			unsafe{chacha_update(chacha_state, optr.add(ilen), optr.add(r), trailerlen)}
		} else { 0 };
		//Safety: r + r2 is ilen + trailerlen rounded down, so optr points to at least
		//ilen + trailerlen writable bytes.
		unsafe{chacha_final(chacha_state, optr.add(r + r2));}

		//The trailer is encrypted directly after the first part, and we MAC ciphertext, so this gets
		//the both parts.
		//Safety: optr points to at least reallen = ilen + trailerlen readable and initialized bytes.
		//tmp.as_ptr() points to at least 16 readable and initialized bytes, and taillen16() is
		//always <16.
		unsafe{poly1305_update(poly_state, optr, reallen);}
		unsafe{poly1305_update(poly_state, tmp.as_ptr(), taillen16(reallen));}
		//Safety: tmp.as_ptr() points to at least 16 readable and initialized bytes.
		write_terminal_mac_block(&mut tmp, ad.len(), reallen);
		unsafe{poly1305_update(poly_state, tmp.as_ptr(), 16);}
		//Safety: tmp.as_ptr() points to 16 writable bytes.
		unsafe{poly1305_finish(poly_state, tmp.as_mut_ptr());}
		//Safety: optr points to ilen + trailerlen + 16 = reallen + 16 writable bytes, so optr.add(reallen)
		//points to 16 writable bytes.
		unsafe{from_raw_parts_mut(optr.add(reallen), 16).copy_from_slice(&tmp);}
		Ok(tagend)
	}
	unsafe fn decrypt3(&self, nonce: &[u8], ad: &[u8], optr: *mut u8, iptr: *const u8, ilen: usize) ->
		Result<usize, AeadError>
	{
		AeadError::assert_nonce(CHACHA20POLY1305_NAME, nonce, |n|n==12)?;
		AeadError::assert_ad(CHACHA20POLY1305_NAME, ad, 0xFFFFFFFFFFFFFFFFu64)?;
		//Tag is 16 bytes.
		let rawlen = AeadError::assert_ciphertext(CHACHA20POLY1305_NAME, ilen, C_MAX, |s|s.checked_sub(16))?;

		//Sanity checks.
		let mut chacha_state = ChachaState::new(&self.0, nonce);
		let mut poly_state = Poly1305State::new();
		//ad.len() nor rawlen can be very near overflow. And this is hint anyway, no real damage if it
		//overflows.
		let hint = (ad.len().saturating_add(15) / 16 + rawlen.saturating_add(15) / 16 + 1).
			saturating_mul(16);
		let mut polykey = [0u8; 64];
		let mut tmp = [0u8; 16];

		Self::compute_poly_key(&mut chacha_state, &mut polykey);
		//Safety: From here on out, chacha_state points to valid Chacha state.
		let chacha_state = chacha_state.ptr();
		//Safety: From here on out, poly_state points to valid Poly1305 state.
		let poly_state = poly_state.ptr();
		//Safety: polykey.as_ptr() points to at least 32 readable and initialized bytes.
		unsafe{poly1305_init_ext(poly_state, polykey.as_ptr(), hint);}
		//Safety: ad.as_ptr() points to ad.len() readable and initialized bytes.
		//tmp points to 16 readable and initialized bytes, and return value of taillen16() is
		//always <16.
		unsafe{poly1305_update(poly_state, ad.as_ptr(), ad.len());}
		unsafe{poly1305_update(poly_state, tmp.as_ptr(), taillen16(ad.len()));}
		//Safety: iptr points to at least ilen = rawlen + 16 > rawlen readable and initialized bytes.
		//tmp points to 16 readable and initialized bytes, and return value of taillen16() is
		//always <16.
		unsafe{poly1305_update(poly_state, iptr, rawlen);}
		unsafe{poly1305_update(poly_state, tmp.as_ptr(), taillen16(rawlen));}
		//Safety: tmp.as_ptr() points to at least 16 readable and initialized bytes.
		write_terminal_mac_block(&mut tmp, ad.len(), rawlen);
		unsafe{poly1305_update(poly_state, tmp.as_ptr(), 16);}
		//Safety: tmp.as_ptr() points to 16 writable bytes.
		unsafe{poly1305_finish(poly_state, tmp.as_mut_ptr());}
		let syndrome = calculate_syndrome(&tmp, unsafe{from_raw_parts(iptr.add(rawlen), 16)});
		//Safety: optr points to max_plaintext_size(ilen) = ilen - 16 = rawlen = rawlen writable bytes.
		AeadError::assert_tag_ok(syndrome == 0, unsafe{from_raw_parts_mut(optr, rawlen)})?;
		//Safety: iptr points to ilen = rawlen + 16 > rawlen readable and initialized bytes, and optr
		//points to max_plaintext_size(ilen) = ilen - 16 = rawlen writable bytes.
		let r = unsafe{chacha_update(chacha_state, iptr, optr, rawlen)};
		//Safety: r is round-down of rawlen, and optr has at least rawlen writable bytes.
		unsafe{chacha_final(chacha_state, optr.add(r));}
		Ok(rawlen)
	}
}

impl Chacha20Poly1305
{
	//ASSUMES: key is all zeroes at call!
	fn compute_poly_key(chacha_state: &mut ChachaState, key: &mut [u8;64])
	{
		//Outputs multiple of 64 bytes are always synchronously emitted. Additionally, we can't have any
		//pending input.
		//Safety: chacha_state.ptr() is valid chacha state, key.as_ptr/as_mut_ptr() has 64 readable,
		//writable and initialized bytes.
		let key = key.as_mut_ptr();
		unsafe{chacha_update(chacha_state.ptr(), key, key, 64);}
	}
}


#[cfg(test)]
mod test;
