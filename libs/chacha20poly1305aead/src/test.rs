use super::Chacha20Poly1305;
use super::LowLevelAeadContext;

#[test]
fn simple_encrypt_decrypt()
{
	let ad = "foobar";
	let msg = "Hello, World!";
	let iv = "0123456789AB";
	let key = "ABCDEFGHIJKLMNOPQRSTUVWXYZ012345";
	let mut buf = [0;29];
	(&mut buf[..13]).copy_from_slice(msg.as_bytes());
	let ctx = Chacha20Poly1305::new_context(key.as_bytes()).unwrap();
	unsafe {
		assert_eq!(ctx.encrypt3(iv.as_bytes(), ad.as_bytes(), buf.as_mut_ptr(), buf.as_ptr(), 13, 0), Ok(29));
		assert_eq!(ctx.decrypt3(iv.as_bytes(), ad.as_bytes(), buf.as_mut_ptr(), buf.as_ptr(), 29), Ok(13));
	}
	assert_eq!(&buf[..13], msg.as_bytes());
}

#[test]
fn simple_encrypt_decrypt_trailer()
{
	let ad = "foobar";
	let msg = "Hello, World!";
	let iv = "0123456789AB";
	let key = "ABCDEFGHIJKLMNOPQRSTUVWXYZ012345";
	let mut buf = [0;29];
	(&mut buf[..13]).copy_from_slice(msg.as_bytes());
	let ctx = Chacha20Poly1305::new_context(key.as_bytes()).unwrap();
	unsafe {
		assert_eq!(ctx.encrypt3(iv.as_bytes(), ad.as_bytes(), buf.as_mut_ptr(), buf.as_ptr(), 6, 7), Ok(29));
		assert_eq!(ctx.decrypt3(iv.as_bytes(), ad.as_bytes(), buf.as_mut_ptr(), buf.as_ptr(), 29), Ok(13));
	}
	assert_eq!(&buf[..13], msg.as_bytes());
}

#[test]
fn simple_encrypt_decrypt_trailer2()
{
	let ad = "foobar";
	let msg = "Hello, World!";
	let iv = "0123456789AB";
	let key = "ABCDEFGHIJKLMNOPQRSTUVWXYZ012345";
	let mut buf = [0;29];
	let mut buf2 = [0;6];
	(&mut buf[..13]).copy_from_slice(msg.as_bytes());
	(&mut buf2[..6]).copy_from_slice(&buf[..6]);
	for i in 0..6 { buf[i] = b'*'; }
	let ctx = Chacha20Poly1305::new_context(key.as_bytes()).unwrap();
	unsafe {
		assert_eq!(ctx.encrypt3(iv.as_bytes(), ad.as_bytes(), buf.as_mut_ptr(), buf2.as_ptr(), 6, 7), Ok(29));
		assert_eq!(ctx.decrypt3(iv.as_bytes(), ad.as_bytes(), buf.as_mut_ptr(), buf.as_ptr(), 29), Ok(13));
	}
	assert_eq!(&buf[..13], msg.as_bytes());
}
