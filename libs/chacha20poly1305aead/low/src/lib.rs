//!Low-level bindings to Chacha-opt and Poly1305-opt libraries, offering high performance implementation of
//!Chacha20 and Poly1305 cryptographic primitives.
//!
//!Note: These primitives are not composed into anything usable, like the IRTF Chacha20-Poly1305-AEAD. These are
//!just the raw primitives. This crate is only meant to be used by the `blts-aux-chacha20poly1305aead` crate.
//!
//!This is its own crate because there can be only one crate in entiere program like that. Thus it must remain
//!semver-compatible, which is much easier if the crate does as little as possible.
#![allow(unsafe_code)]
#![no_std]

#[allow(deprecated)] const ATOMIC_BOOL_INIT: AtomicBool = ::core::sync::atomic::ATOMIC_BOOL_INIT;
use core::sync::atomic::AtomicBool;
use core::sync::atomic::Ordering;
use core::mem::size_of;
use x::Once;
use x::ONCE_INIT;


#[cfg(feature="use_collections")]
mod x {
	extern crate btls_aux_collections; 
	pub use self::btls_aux_collections::Once;
	pub const ONCE_INIT: Once = Once::new();
}

#[cfg(all(feature="threadsafe", not(feature="use_collections")))]
mod x {
	extern crate std;
	#[allow(deprecated)] pub const ONCE_INIT: Once = std::sync::ONCE_INIT;
	pub use self::std::sync::Once;
}
#[cfg(all(not(feature="threadsafe"), not(feature="use_collections")))]
mod x {
	#[allow(deprecated)] const ATOMIC_USIZE_INIT: AtomicUsize = ::core::sync::atomic::ATOMIC_USIZE_INIT;
	use core::sync::atomic::AtomicUsize;
	use core::sync::atomic::Ordering;

	pub struct Once(AtomicUsize);
	pub const ONCE_INIT: Once = Once(ATOMIC_USIZE_INIT);
	impl Once
	{
		pub fn call_once<F>(&'static self, cb: F) where F: FnOnce()
		{
			//Completed?
			if self.0.load(Ordering::Acquire) > 1 { return; }
			//Should we run the callback?
			if self.0.compare_exchange(0, 1, Ordering::AcqRel, Ordering::Acquire) == Ok(0) {
				cb();
				self.0.store(2, Ordering::Release);
				return;
			}
			//Wait for call to finish. The value can not be 0, because the compare_and_swap()
			//above would have branched. And if value is >1, it means we raced with complete
			//initialization, so we can complete immediately.
			while self.0.load(Ordering::Acquire) == 1 { }
		}
	}
}

///Perform Power-On Self Test of the library.
pub fn do_post() -> Result<(), ()>
{
	static START: Once = ONCE_INIT;
	static POST_OK: AtomicBool = ATOMIC_BOOL_INIT;
	START.call_once(|| {
		if unsafe{chacha_startup()} != 0 { return; }
		if unsafe{poly1305_startup()} != 0 { return; }
		POST_OK.store(true, Ordering::Relaxed);
	});
	if !POST_OK.load(Ordering::Relaxed) { return Err(()); }
	Ok(())
}

///The amount of state space Chacha20 needs in bytes.
///
///Can be assumed to be multiple of 64 bytes, and the buffer MUST be aligned to 64 bytes.
pub const CHACHA_BUFFER_SIZE: usize = 256;
///The amount of state space Poly1305 needs in bytes.
///
///This buffer space MUST be aligned to 64 bytes.
pub const POLY1305_BUFFER_SIZE: usize = 320;

///Initialize Chacha20 state using key and nonce.
///
///The state MUST be at least CHACHA_BUFFER_SIZE bytes and MUST be aligned to 64 bytes. The key MUST be 32 octets.
///The nonce MUST be 12 octets.
///
///Altough this function has alignment restrictions on state buffer, it is not unsafe because it does not directly
///lead to Undefined Behavior if state buffer is not correctly aligned.
pub fn chacha_initialize_state(state: &mut [u8], key: &[u8], nonce: &[u8])
{
	for i in 48..256 { state[i] = 0; }		//State is 256 bytes.
	(&mut state[..32]).copy_from_slice(key);	//Key is 32 bytes.
	for i in 32..36 { state[i] = 0; }		//Zero counter.
	(&mut state[36..48]).copy_from_slice(nonce);	//Nonce is 12 bytes.
	if u32::from_le(0x12345678) == 0x12345678 {
		state[48] = 20;		//Little-endian.
	} else {
		state[47+size_of::<usize>()] = 20;	//Big-endian.
	}
}

extern
{
	///chacha_startup() from Chacha-opt.
	pub fn chacha_startup() -> i32;
	///chacha_update() from Chacha-opt.
	pub fn chacha_update(S: *mut u8, input: *const u8, output: *mut u8, inlen: usize) -> usize;
	///chacha_final() from Chacha-opt.
	pub fn chacha_final(S: *mut u8, out: *mut u8) -> usize;
	///poly1305_startup() from Poly1305-opt.
	pub fn poly1305_startup() -> i32;
	///poly1305_init_ext() from Poly1305-opt.
	pub fn poly1305_init_ext(S: *mut u8, key: *const u8, bytes_hint: usize);
	///poly1305_update() from Poly1305-opt.
	pub fn poly1305_update(S: *mut u8, input: *const u8, inlen: usize);
	///poly1305_finish() from Poly1305-opt.
	pub fn poly1305_finish(S: *mut u8, mac: *mut u8);
}
