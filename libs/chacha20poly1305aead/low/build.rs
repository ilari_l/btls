extern crate cc;
use cc::Build;
use std::env::var;
use std::process::Command;
use std::path::Path;
use std::path::PathBuf;
use std::fmt::Write as FmtWrite;
use std::io::Write as IoWrite;
use std::fs::create_dir_all;
use std::fs::File;
use std::io::stderr;

fn get_c_compiler() -> PathBuf
{
	let mut b = Build::new();
	b.file("src/poly1305-opt/app/extensions/poly1305/impl.c");
	let c = b.get_compiler();
	if !c.is_like_gnu() && !c.is_like_clang() { panic!("C compiler found is not GCC nor CLANG"); }
	c.path().to_path_buf()
}

struct PlatformInfo
{
	bits64: bool,
	mflag: i8,
	ptype: &'static str
}

const I386: PlatformInfo = PlatformInfo{bits64: false, mflag: -1, ptype:"x86"};
const AMD64: PlatformInfo = PlatformInfo{bits64: true, mflag: 1, ptype:"x86"};
//ARM does not support -m32/-m64.
const ARM32: PlatformInfo = PlatformInfo{bits64: false, mflag: 0, ptype:"arm"};
const ARM64: PlatformInfo = PlatformInfo{bits64: true, mflag: 0, ptype:"arm"};
const GENERIC32: PlatformInfo = PlatformInfo{bits64: false, mflag: -1, ptype:"generic"};
const GENERIC64: PlatformInfo = PlatformInfo{bits64: true, mflag: 1, ptype:"generic"};
const GENERIC32NF: PlatformInfo = PlatformInfo{bits64: false, mflag: 0, ptype:"generic"};
const GENERIC64NF: PlatformInfo = PlatformInfo{bits64: true, mflag: 0, ptype:"generic"};

fn get_targetinfo() -> PlatformInfo
{
	let target = var("TARGET").unwrap();
	//i386/AMD64 series.
	if target.starts_with("x86_64") { return AMD64; }
	if target.starts_with("i386") { return I386; }
	if target.starts_with("i486") { return I386; }
	if target.starts_with("i586") { return I386; }
	if target.starts_with("i686") { return I386; }
	//ARM.
	if target.starts_with("aarch64") { return ARM64; }
	if target.starts_with("arm") { return ARM32; }
	//MIPS. These do not support -m32/-m64.
	if target.starts_with("mips64") { return GENERIC64NF; }
	if target.starts_with("mips") { return GENERIC32NF; }
	//PowerPC. Supports -m32/-m64
	if target.starts_with("powerpc64") { return GENERIC64; }
	if target.starts_with("powerpc") { return GENERIC32; }
	//Alpha. Always 32-bit.
	if target.starts_with("alpha") { return GENERIC32NF; }
	//M68k. Always 32-bit.
	if target.starts_with("m68k") { return GENERIC32NF; }
	//S390x. Always 32-bit?
	if target.starts_with("s390x") { return GENERIC32NF; }
	//SH4. Always 32-bit.
	if target.starts_with("sh4") { return GENERIC32NF; }
	//HP-PA. These do not support -m32/-m64
	if target.starts_with("hppa64") { return GENERIC64NF; }
	if target.starts_with("hppa") { return GENERIC32NF; }
	//Sparc. Supports -m32/-m64
	if target.starts_with("sparc64") { return GENERIC64; }
	if target.starts_with("sparc") { return GENERIC32; }
	//Unkown. Assume 32-bit with no -m32.
	GENERIC32NF
}


fn run_common(mut c: Command, clazz: &str)
{
	let fcmd = format!("{:?}", c);
	match c.output() {
		Ok(x) => if !x.status.success() {
			println!("Errors from {}:", fcmd);
			if x.stdout.len() > 0 {
				println!("Stdout:");
				println!("-------");
				println!("{}", String::from_utf8_lossy(&x.stdout));
			}
			if x.stderr.len() > 0 {
				println!("Stderr:");
				println!("-------");
				println!("{}", String::from_utf8_lossy(&x.stderr));
			}
			panic!("{} failed", clazz);
		},
		Err(x) => {
			println!("Failed to execute {}: {}", fcmd, x);
			panic!("{} failed", clazz);
		}
	}
}

fn include_paths(pfx: &str, platname: &str, outdir: &str) -> Vec<String>
{
	vec![
		format!("{}/app/extensions", pfx),
		format!("{}/app/include", pfx),
		format!("{}/framework/include", pfx),
		format!("{}/framework/driver", pfx),
		format!("{}/framework/driver/{}", pfx, platname),
		format!("{}/include/{}", outdir, pfx),
	]
}

fn invoke_assembler(outputs: &mut Vec<String>, output: &str, input: &str, pfx: &str)
{
	let compiler = get_c_compiler();
	let PlatformInfo{mflag:bitness, ptype:platname, ..} = get_targetinfo();
	let outdir = var("OUT_DIR").unwrap();
	if platname == "generic" { return; }	//No assembly on generic.
	let mut c = Command::new(compiler);
	if bitness < 0 { c.arg("-m32"); }
	if bitness > 0 { c.arg("-m64"); }
	for i in include_paths(pfx, platname, &outdir).iter() { c.arg(&format!("-I{}", i)); }
	for i in include_paths(pfx, platname, &outdir).iter() { c.arg(&format!("-Wa,-I{}", i)); }
	c.arg("-DBUILDING_ASM");
	c.arg("-o");
	let output = Path::new(&outdir).join(output).display().to_string();
	c.arg(&output);
	c.arg("-c");
	c.arg(&format!("{}/{}", pfx, input));
	run_common(c, "Assembler");
	outputs.push(output);
}

fn invoke_c_compiler(outputs: &mut Vec<String>, output: &str, input: &str, pfx: &str)
{
	let compiler = get_c_compiler();
	let PlatformInfo{mflag:bitness, ptype:platname, ..} = get_targetinfo();
	let outdir = var("OUT_DIR").unwrap();
	let mut c = Command::new(compiler);
	if bitness < 0 { c.arg("-m32"); }
	if bitness > 0 { c.arg("-m64"); }
	c.arg("-fvisibility=hidden");
	c.arg("-DLIB_PUBLIC=__attribute__ ((visibility (\"default\")))");
	c.arg("-fPIC");
	c.arg("-O2");
	if platname != "generic" { c.arg("-DBUILDING_ASMOPT_POLY1305"); }
	for i in include_paths(pfx, platname, &outdir).iter() { c.arg(&format!("-I{}", i)); }
	c.arg("-o");
	let output = Path::new(&outdir).join(output).display().to_string();
	c.arg(&output);
	c.arg("-c");
	c.arg(&format!("{}/{}", pfx, input));
	run_common(c, "C compiler");
	outputs.push(output);
}

fn test_c_compiler(name: &str, clazz: &str, prog: &str) -> bool
{
	let compiler = get_c_compiler();
	let PlatformInfo{mflag:bitness, ..} = get_targetinfo();
	let outdir = var("OUT_DIR").unwrap();
	let mut c = Command::new(compiler);
	if bitness < 0 { c.arg("-m32"); }
	if bitness > 0 { c.arg("-m64"); }
	c.arg("-o");
	c.arg(&format!("{}/test.tmp.o", outdir));
	{
		let mut file = File::create(&format!("{}/test.tmp.{}", outdir, clazz)).unwrap();
		file.write_all(prog.as_bytes()).unwrap();
	}
	c.arg("-c");
	c.arg(&format!("{}/test.tmp.{}", outdir, clazz));
	let fcmd = format!("{:?}", c);
	let s = match c.output() {
		Ok(x) => x.status.success(),
		Err(x) => {
			println!("Failed to execute {}: {}", fcmd, x);
			panic!("C compiler failed");
		}
	};
	writeln!(stderr(), "Testing {}...{}", name, if s { "Yes" } else { "No" }).unwrap();
	s
}

fn compile_chacha_lib(assembler_works: bool)
{
	let mut outputs = Vec::new();
	let PlatformInfo{ptype:platname, ..} = get_targetinfo();
	let pfx = "src/chacha-opt";

	//Compile the chacha implementation.
	invoke_c_compiler(&mut outputs, "chacha-cpucycles.o", "framework/driver/cpucycles.c", pfx);
	invoke_c_compiler(&mut outputs, "chacha-cpuid.o", "framework/driver/cpuid.c", pfx);
	invoke_c_compiler(&mut outputs, "chacha-impl.o", "app/extensions/chacha/impl.c", pfx);
	if assembler_works {
		invoke_assembler(&mut outputs, "chacha-chacha.o", "app/extensions/chacha/chacha.S", pfx);
	}
	if assembler_works && platname == "x86" {
		invoke_assembler(&mut outputs, "chacha-driver.o", "framework/driver/x86/driver.S", pfx);
	}
	let mut c = Build::new();
	for i in outputs.iter() { c.object(i); }
	c.compile("chacha");
}

fn compile_poly1305_lib(assembler_works: bool)
{
	let mut outputs = Vec::new();
	let PlatformInfo{ptype:platname, ..} = get_targetinfo();
	let pfx = "src/poly1305-opt";

	//Compile the poly1305 implementation.
	invoke_c_compiler(&mut outputs, "poly1305-cpucycles.o", "framework/driver/cpucycles.c", pfx);
	invoke_c_compiler(&mut outputs, "poly1305-cpuid.o", "framework/driver/cpuid.c", pfx);
	invoke_c_compiler(&mut outputs, "poly1305-impl.o", "app/extensions/poly1305/impl.c", pfx);
	if assembler_works {
		invoke_assembler(&mut outputs, "poly1305-poly1305.o", "app/extensions/poly1305/poly1305.S", pfx);
	}
	if assembler_works && platname == "x86" {
		invoke_assembler(&mut outputs, "poly1305-driver.o", "framework/driver/x86/driver.S", pfx);
	}
	let mut c = Build::new();
	for i in outputs.iter() { c.object(i); }
	c.compile("poly1305");
}

fn have_uint128_t_variant1() -> bool
{
test_c_compiler("128-bit type via unsigned __int128", "c", r#"
unsigned __int128 foo() { return 0; }
"#)
}

fn have_uint128_t_variant2() -> bool
{
test_c_compiler("128-bit type via uint128_t in <stdint.h>", "c", r#"
#include <stdint.h>
uint128_t foo() { return 0; }
"#)
}

fn have_uint128_t() -> bool
{
	have_uint128_t_variant1() || have_uint128_t_variant2()
}

fn need_define_uint128_t() -> bool
{
	have_uint128_t() && !test_c_compiler("if <stdint.h> defines uint128_t", "c", r#"
#include <stdint.h>
uint128_t foo() { return 0; }
"#)
}

fn have_as_hidden() -> bool
{
	test_c_compiler("Assembler .hidden directive", "s", r#"
.text
.globl myfn
.hidden myfn
myfn:
"#)
}

fn have_as_private_extern() -> bool
{
	test_c_compiler("Assembler .private_extern directive", "s", r#"
.text
.globl myfn
.private_extern myfn
myfn:
"#)
}

fn have_gettimeofday() -> bool
{
	test_c_compiler("gettimeofday", "c", r#"
#include <sys/time.h>
int foo()
{
	struct timeval t;
	gettimeofday(&t, 0);
	return t.tv_sec;
}
"#)
}

fn have_slashmacro(ins: &str, reg: &str) -> bool
{
	test_c_compiler("Assembler macro style (slashmacro)", "s", &format!(r#"
.text
.macro TEST1 op
\op {reg}, {reg}
.endm
TEST1 {ins}
"#, ins=ins, reg=reg))
}

fn have_dollarmacro(ins: &str, reg: &str) -> bool
{
	test_c_compiler("Assembler macro style (dollarmacro)", "s", &format!(r#"
.text
.macro TEST1 op
\$0 {reg}, {reg}
.endm
TEST1 {ins}
"#, ins=ins, reg=reg))
}

fn c_compiler_is_sane() -> bool
{
	test_c_compiler("Basic C compiler sanity", "c", r#"
int foo() { return 0; }
"#)
}

fn is_x86() -> bool
{
	test_c_compiler("Assembler targets i386/amd64", "s", r#"
.text
xorb %al, %al
"#)
}

fn is_arm() -> bool
{
	test_c_compiler("Assembler targets ARM/AARCH64", "s", r#"
.text
and r0, r0, r0
"#)
}

const ARM6: &'static str = "armv6";
const ARM7A: &'static str = "armv7-a";
const ARM8A: &'static str = "armv8-a";

trait InstructionVariant
{
	fn output(&self, out: &mut String);
	fn output2(&self, _out: &mut String) {}
}

struct BasicIsa;

impl InstructionVariant for BasicIsa
{
	fn output(&self, _out: &mut String) {}
}

struct ArmExt<V:InstructionVariant>(&'static str, V);

impl<V:InstructionVariant> InstructionVariant for ArmExt<V>
{
	fn output(&self, out: &mut String)
	{
		self.1.output(out);
		out.write_fmt(format_args!(".arch_extension {}\n", self.0)).unwrap();
	}
}


struct ArmFpu<V:InstructionVariant>(&'static str, V);

impl<V:InstructionVariant> InstructionVariant for ArmFpu<V>
{
	fn output(&self, out: &mut String)
	{
		self.1.output(out);
		out.write_fmt(format_args!(".fpu {}\n", self.0)).unwrap();
	}
}


struct ArmV8B;

impl InstructionVariant for ArmV8B
{
	fn output(&self, out: &mut String) { out.write_fmt(format_args!(".arch {}\n", ARM8A)).unwrap(); }
}

struct ArmThumb<V:InstructionVariant>(V);

impl<V:InstructionVariant> InstructionVariant for ArmThumb<V>
{
	fn output(&self, out: &mut String) { self.0.output(out); }
	fn output2(&self, out: &mut String)
	{
		out.write_str(".thumb\n").unwrap();
		out.write_str(".thumb_func\n").unwrap();
	}
}


struct ArmV7B;

impl InstructionVariant for ArmV7B
{
	fn output(&self, out: &mut String) { out.write_fmt(format_args!(".arch {}\n", ARM7A)).unwrap(); }
}

struct ArmV6B;

impl InstructionVariant for ArmV6B
{
	fn output(&self, out: &mut String) { out.write_fmt(format_args!(".arch {}\n", ARM6)).unwrap(); }
}


fn test_instruction<V:InstructionVariant>(out: &mut String, ins: &str, symbol: &str, v: V)
{
	let mut prog = String::new();
	v.output(&mut prog);
	prog.write_str(".text\n").unwrap();
	v.output2(&mut prog);
	prog.write_fmt(format_args!("{}\n", ins)).unwrap();
	let s = test_c_compiler(&format!("Presence of {}({})", symbol, ins), "s", &prog);
	if s { out.write_fmt(format_args!("#define HAVE_{}\n", symbol)).unwrap(); }
}

fn generate_extra_includes(path: &str, projectname: &str, assembler_works: &mut bool)
{
	if !c_compiler_is_sane() { panic!("Dude, your C compiler is busted"); }

	let mut out = String::new();
	out.write_str("#ifndef ASMOPT_INTERNAL_H\n").unwrap();
	out.write_str("#define ASMOPT_INTERNAL_H\n").unwrap();
	out.write_str("\n").unwrap();
	//CPU bitness.
	if get_targetinfo().bits64 {
		out.write_str("#define CPU_64BITS\n").unwrap();
	} else {
		out.write_str("#define CPU_32BITS\n").unwrap();
	}
	//Assembly.
	let (mins, mreg) = if is_x86() {
		out.write_str("#define ARCH_X86\n").unwrap();
		*assembler_works = true;
		test_instruction(&mut out, "vpaddq %zmm0, %zmm0, %zmm0", "AVX512", BasicIsa);
		test_instruction(&mut out, "vpaddq %ymm0, %ymm0, %ymm0", "AVX2", BasicIsa);
		test_instruction(&mut out, "vpaddq %xmm0, %xmm0, %xmm0", "AVX", BasicIsa);
		test_instruction(&mut out, "vprotd $15, %xmm0, %xmm0", "XOP", BasicIsa);
		test_instruction(&mut out, "crc32b %cl, %ebx", "SSE4_2", BasicIsa);
		test_instruction(&mut out, "pblendw $0, %xmm0, %xmm0", "SSE4_1", BasicIsa);
		test_instruction(&mut out, "pshufb %xmm0, %xmm0", "SSSE3", BasicIsa);
		test_instruction(&mut out, "lddqu 0(%esi), %xmm0", "SSE3", BasicIsa);
		test_instruction(&mut out, "pmuludq %xmm0, %xmm0", "SSE2", BasicIsa);
		test_instruction(&mut out, "movaps 0(%esi), %xmm0", "SSE", BasicIsa);
		test_instruction(&mut out, "punpckhdq %mm0, %mm0", "MMX", BasicIsa);
		("xorl", "%eax")
	} else if is_arm() {
		out.write_str("#define ARCH_ARM\n").unwrap();
		*assembler_works = true;
		if get_targetinfo().bits64 {
			test_instruction(&mut out, "hlt", "ARMv8", ArmV8B);
			test_instruction(&mut out, "vvselge.f32 s4, s1, s23", "VFP4", ArmFpu("neon-fp-armv8",
				ArmV8B));
			test_instruction(&mut out, "vcvta.u32.f32 s2, s3", "ASIMD", ArmFpu("neon-fp-armv8", ArmV8B));
			test_instruction(&mut out, "aese.8 q0, q0", "AES", ArmExt("crypto", ArmV8B));
			test_instruction(&mut out, "sha1p.32 q0,q0,q0", "SHA1", ArmExt("crypto", ArmV8B));
			test_instruction(&mut out, "sha256h.32 q0, q0, q0", "SHA256", ArmExt("crypto", ArmV8B));
			test_instruction(&mut out, "vmull.p64 q0, d0, d0", "PMULL", ArmExt("crypto", ArmV8B));
			test_instruction(&mut out, "crc32b r0, r1, r2", "CRC32", ArmExt("crc", ArmV8B));
		} else {
			test_instruction(&mut out, "uqadd8 r3,r3,r3", "ARMv6", ArmV6B);
			test_instruction(&mut out, "pli [r0]", "ARMv7", ArmV7B);
			test_instruction(&mut out, "veor d0,d0,d0", "NEON", ArmFpu("neon", ArmV7B));
			test_instruction(&mut out, "fcpyd d3,d22", "VFP3", ArmFpu("vfpv3", ArmV7B));
			test_instruction(&mut out, "fcpyd d3,d4", "VFP3D16", ArmFpu("vfpv3-d16", ArmV7B));
			test_instruction(&mut out, "vfma.f32 q1, q2, q3", "VFP4", ArmFpu("neon-vfpv4", ArmV7B));
			test_instruction(&mut out, "wzeroge wr7", "IWMMXT", ArmExt("iwmmxt", ArmV7B));
			test_instruction(&mut out, "udiv r0, r1, r2", "IDIVA", ArmExt("idiv", ArmV7B));
			test_instruction(&mut out, "udiv r0, r1, r2", "IDIVT", ArmThumb(ArmExt("idiv", ArmV7B)));
		}
		("tst", "r0")
	} else {
		("invalid", "invalid")
	};
	if have_slashmacro(mins, mreg)  { out.write_str("#define HAVE_SLASHMACRO\n").unwrap(); }
	if have_dollarmacro(mins, mreg)  { out.write_str("#define HAVE_DOLLARMACRO\n").unwrap(); }
	if have_as_hidden() { out.write_str("#define HAVE_AS_HIDDEN\n").unwrap(); }
	else if have_as_private_extern() { out.write_str("#define HAVE_AS_PRIVATE_EXTERN\n").unwrap(); }
	if have_gettimeofday() { out.write_str("#define HAVE_GETTIMEOFDAY\n").unwrap(); }
	//We always assume we have stdint, which contains 8, 16, 32 and 64 bit types.
	out.write_str("#define HAVE_INT8\n").unwrap();
	out.write_str("#define HAVE_INT16\n").unwrap();
	out.write_str("#define HAVE_INT32\n").unwrap();
	out.write_str("#define HAVE_INT64\n").unwrap();
	if have_uint128_t() { out.write_str("#define HAVE_INT128\n").unwrap(); }
	out.write_str("#define HAVE_STDINT\n").unwrap();
	out.write_str("#if !defined(BUILDING_ASM)\n").unwrap();
	out.write_str("#include <stddef.h>\n").unwrap();
	out.write_str("#include <stdint.h>\n").unwrap();
	if need_define_uint128_t() {
		out.write_str("typedef signed __int128 int128_t;\n").unwrap();
		out.write_str("typedef unsigned __int128 uint128_t;\n").unwrap();
	}
	out.write_str("#endif\n").unwrap();

	out.write_str("\n").unwrap();
	out.write_str("#define LOCAL_PREFIX3(a,b) a##_##b\n").unwrap();
	out.write_str("#define LOCAL_PREFIX2(a,b) LOCAL_PREFIX3(a,b)\n").unwrap();
	out.write_str("#define LOCAL_PREFIX(n) LOCAL_PREFIX2(PROJECT_NAME,n)\n").unwrap();
	//That's right: There are no quotes around the project name.
	out.write_fmt(format_args!("#define PROJECT_NAME {}\n", projectname)).unwrap();
	out.write_str("\n").unwrap();
	out.write_str("#endif /* ASMOPT_INTERNAL_H */\n").unwrap();
	let mut file = File::create(&format!("{}/asmopt_internal.h", path)).unwrap();
	file.write_all(out.as_bytes()).unwrap();
}

fn main()
{
	let outdir = var("OUT_DIR").unwrap();
	let genincdirc = format!("{}/include/src/chacha-opt", outdir);
	let genincdirp = format!("{}/include/src/poly1305-opt", outdir);
	create_dir_all(&genincdirc).unwrap();
	create_dir_all(&genincdirp).unwrap();
	let mut assembler_works = false;
	generate_extra_includes(&genincdirc, "chacha", &mut assembler_works);
	generate_extra_includes(&genincdirp, "poly1305", &mut assembler_works);

	compile_chacha_lib(assembler_works);
	compile_poly1305_lib(assembler_works);

	println!("cargo:rerun-if-changed=build.rs");	//This has no external deps to rebuild for.
}
