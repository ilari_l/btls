//!Simple implementation of futures.
//!
//!This crate provodes a simple implementation of futures. Futures represent result of computation that might not
//!have been finished yet. This is handy for representing asynchronous processes.
//!
//!The most important items are:
//!
//! * [`FutureReceiver`]: Object that acts as the result of computation that has possibly not finished yet.
//! * [`FutureSender`]: Object that acts as capability to resolve the value of `FutureReceiver`.
//! * [`create_future`]: Create a linked [`FutureSender`] and [`FutureReceiver`] pair.
//!
//![`FutureReceiver`]: struct.FutureReceiver.html
//![`FutureSender`]: struct.FutureSender.html
//![`create_future`]: fn.create_future.html
#![deny(unsafe_code)]
#![forbid(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]

use btls_aux_collections::Arc;
use btls_aux_collections::Box;
use btls_aux_collections::Mutex;
use core::mem::replace;
use core::marker::PhantomData;
use core::sync::atomic::AtomicBool;
use core::sync::atomic::Ordering as MemOrdering;


///A function object mapping the resolved value of future.
///
///Objects of this type are used by the [`FutureReceiver::map()`] method in order to map the return value of the
///future into another value that will be returned. This trait is object-safe, so it can be boxed as trait for
///dynamic dispatch.
///
///For constructing this object out of closure, see [`FutureMapperFn`](struct.FutureMapperFn.html).
///
///As note, this mechanism has the following limitations:
///
/// * The transform used must be quickly computable, it can not use futures itself.
/// * The transform can only depend on a single future as input.
///
///[`FutureReceiver::map()`]: struct.FutureReceiver.html#method.map
pub trait FutureMapper<T: Sized+Send, U: Sized+Send>: Sized+Send
{
	///Return whatever value the value `val` maps into.
	fn map(&mut self, val: T) -> U;
}

///A wrapper for implementing [`FutureMapper`](trait.FutureMapper.html) for a closure.
///
///# Example:
///
///```no_run
///let dest = source.map(FutureMapperFn::new(|val|{
///	//Some code to map val and return the result.
///}));
///```
pub struct FutureMapperFn<T: Sized+Send, U: Sized+Send, X:FnMut(T) -> U + Send>(X, PhantomData<T>, PhantomData<U>);

impl<T: Sized+Send, U: Sized+Send, X:FnMut(T) -> U + Send> FutureMapperFn<T, U, X>
{
	///Create a new future mapper object from closure `xfn`.
	pub fn new(xfn: X) -> FutureMapperFn<T,U,X> { FutureMapperFn(xfn, PhantomData, PhantomData) }
}

impl<T: Sized+Send, U: Sized+Send, X:FnMut(T) -> U + Send> FutureMapper<T,U> for FutureMapperFn<T, U, X>
{
	fn map(&mut self, val: T) -> U { (self.0)(val) }
}

//A FutureMapper wrapped as FutureSink.
struct FutureMapperSink<T:Sized+Send, U:Sized+Send, X:FutureMapper<T,U>>
{
	mapper: X,
	nexthop: Option<FutureSender<U>>,
	phantom: PhantomData<T>
}

impl<T:Sized+Send, U:Sized+Send, X:FutureMapper<T,U>> FutureSink<T> for FutureMapperSink<T, U, X>
{
	fn sink(&mut self, value: T)
	{
		let new_val = self.mapper.map(value);
		//This can only be called once.
		if let Some(nexthop) = self.nexthop.take() { nexthop.settle(new_val); }
	}
}

///A function object to call when the future resolves.
///
///Objects of this type are used by the [`FutureReceiver::sink_to()`] method in order to have a function consume the
///return value of future instead of returning it through a [`FutureReceiver`](struct.FutureReceiver.html),
///This trait is object-safe, so it can be boxed as trait for dynamic dispatch.
///
///[`FutureReceiver::sink_to()`]: struct.FutureReceiver.html#method.sink_to
pub trait FutureSink<T: Sized+Send>
{
	///Process the final resolved value `value`.
	///
	///There is no return value, as the original receiver is gone due to [`FutureReceiver::sink_to()`] having
	///consumed it. This routine should quickly process the value and then resolve the value of some other
	///future.
	///
	///[`FutureReceiver::sink_to()`]: struct.FutureReceiver.html#method.sink_to
	fn sink(&mut self, value: T);
}

///A future sender, that eventually resolves the value of the future.
///
///The value is returned by using the [`settle()`](#method.settle) method.
pub struct FutureSender<T: Sized+Send>
{
	value: Arc<(Mutex<FutureReceiverInner<T>>, AtomicBool)>
}

impl<T:Sized+Send> FutureSender<T>
{
	///Resolve the future to `val`. The original sender is consumed in this operation. Any callbacks
	///registered on the future will run.
	///
	///This method is essentially the futures equivalent to the normal `return` statement.
	pub fn settle(self, val: T)
	{
		let mut x = self.value.0.lock();
		//This should never be Value(...).
		if let FutureReceiverInner::Sink(mut sink) = x.take() {
			sink.sink(val);
			//No need to set the flag, because there is no receiver for this.
		} else {
			*x = FutureReceiverInner::Value(val);
			self.value.1.store(true, MemOrdering::Release);
		};
	}
}

enum FutureReceiverInner<T: Sized+Send>
{
	Sink(Box<dyn FutureSink<T>+Send>),
	Value(T),
	None
}

impl<T: Sized+Send> FutureReceiverInner<T>
{
	fn take(&mut self) -> Self
	{
		replace(self, FutureReceiverInner::None)
	}
}

enum _FutureReceiver<T: Sized+Send>
{
	//Have separate flag to avoid locking unsettled futures.
	Delayed(Arc<(Mutex<FutureReceiverInner<T>>, AtomicBool)>),
	Immediate(T),
}

///A future value receiver.
///
///The future value might either be unresolved (not known yet), or resolved (the final value is available). It is
///used to represent computations that have possibly not finished yet due to their long duration.
///
///If created via the `From` trait, the return value of the future is the value converted, available immediately.
///In this case, the sender capability for this future will never exist.
///
///For more information about futures, see [`create_future`].
///
///One can check if the future has resolved yet by using the [`settled()`](#method.settled) method and if it is,
///read the value using the [`read()`](#method.read) method.
///
///The values can be mapped by using the [`map()`](#method.map) and [`sink_to()`](#method.sink_to) methods.
///
///[`create_future`]: fn.create_future.html
pub struct FutureReceiver<T: Sized+Send>(_FutureReceiver<T>);

impl<T:Sized+Send> From<T> for FutureReceiver<T>
{
	fn from(val: T) -> FutureReceiver<T> { FutureReceiver(_FutureReceiver::Immediate(val)) }
}

/*
impl<T:Sized+Send,E:Sized+Send> FutureReceiver<Result<T,E>>
{
	///Create a value from error.
	pub fn from_error(val: E) -> FutureReceiver<Result<T,E>>
	{
		FutureReceiver::from(Err(val))
	}
}
*/
impl<T:Sized+Send> FutureReceiver<T>
{
	///Set function `sink` to consume the return value of the future. If the future has already resolved, the
	///sepecified function object is synchronously called with the return value as argument.
	pub fn sink_to(self, mut sink: Box<dyn FutureSink<T>+Send>)
	{
		match self.0 {
			_FutureReceiver::Delayed(x) => {
				let mut y = x.0.lock();
				if let FutureReceiverInner::Value(val) = y.take() {
					sink.sink(val);
				} else {
					*y = FutureReceiverInner::Sink(sink);
				}
			},
			_FutureReceiver::Immediate(x) => sink.sink(x),
		};
	}
	///Is the final value known yet?
	pub fn settled(&self) -> bool
	{
		match &self.0 {
			&_FutureReceiver::Delayed(ref x) => x.1.load(MemOrdering::Acquire),
			&_FutureReceiver::Immediate(_) => true,
		}
	}
	///Read the final value of the future.
	///
	///If the future has resolved, returns `Ok(val)`, where `val` is the final value. Otherwise returns
	///`Err(self)`, where `self` is the receiver of the method (i.e., the object is returned back to the
	///caller).
	///
	///Usually one wants to first check using [`settled()`](#method.settled) if the future has resolved yet.
	///If [`settled()`](#method.settled) returns `true`, then this method always returns `Ok(...)`. The
	///converse is not true due to possibility of race conditions.
	pub fn read(self) -> Result<T, FutureReceiver<T>>
	{
		match self.0 {
			_FutureReceiver::Immediate(x) => return Ok(x),
			_FutureReceiver::Delayed(y) => {
				//First check the flag before trying to lock it.
				if y.1.load(MemOrdering::Acquire) {
					let mut z = y.0.lock();
					//The inner value can not be Sink here, as the only way to set Sink consumes
					//the receiver.
					if let FutureReceiverInner::Value(w) = z.take() { return Ok(w); }
				}
				return Err(FutureReceiver(_FutureReceiver::Delayed(y)));
			}
		}
	}
}

impl<T:Sized+Send+'static> FutureReceiver<T>
{
	///Map the value of future when resolved using function `mapper`. If the value has already resolved,
	///synchronously calls the given function object. The original value is consumed in the process.
	///
	///Returns future that eventually resolves to the result of applying `mapper` to the final value of the
	///original future. Where eventually either means synchronously on call if the original has resolved,
	///or immediately the original future resolves otherwise.
	pub fn map<U:Sized+Send+'static,X:FutureMapper<T,U>+'static>(self, mut mapper: X) -> FutureReceiver<U>
	{
		match self.0 {
			_FutureReceiver::Delayed(x) => {
				let mut y = x.0.lock();
				//The object can not be Sink here, because only things that set Sink consume the
				//receiver.
				if let FutureReceiverInner::Value(yval) = y.take() {
					//No need to drop the lock: The function can not access the locked
					//future anyway.
					FutureReceiver::from(mapper.map(yval))
				} else {
					let (sender, receiver) = create_future::<U>();
					let map_sink = FutureMapperSink{
						mapper: mapper,
						nexthop:Some(sender),
						phantom:PhantomData
					};
					*y = FutureReceiverInner::Sink(Box::new(map_sink));
					receiver
				}
			},
			_FutureReceiver::Immediate(x) => FutureReceiver::from(mapper.map(x)),
		}
	}
}

///Create a `(sender, receiver)` pair for future value of type `T`.
///
///The return value is  `(sender, receiver)`, where `sender` is the object that can resolve the value, and
///`receiver` is for reading the value of computation, initially not known. The type of the value is generic type
///parameter `T`, which must both have size known at compile time (not a Dynamically Sized Type)  and be safe to
///send across threads (trait `Send`). Additionally the [`FutureReceiver::map()`] method requires the T to not have
///any non-static lifetimes (so the values of `T` can be valid for an unbounded duration).
///
///# Futures:
///
///Futures model computation that happens asynchronously, and whose final value may not be known yet.
///When the value is finally resolved at end of computation, the return value of computation can then be extracted
///from the future. The computation invoked can also be synchronous: In that case, the value is resolved during
///invocation of the computation and is immediately available afterwards.
///
///Because the values are stored, they need to be `Sized` and because the values can cross threads,
///the values need to be `Send`. Because no synchronous mutation is possible, `Sync` is not needed.
///
///In this implementation of futures, one can:
///
/// * Create an future representing unfinished computation, and object that can resolve the value of the computation
///(using the [`FutureSender::settle()`] method, even across threads), by using the `create_future()` function.
/// * Create an future with immediately resolved value by using the [`FutureReceiver::from`] method (from `From`
///trait).
/// * Check if value has been resolved by using the [`FutureReceiver::settled()`] method on the receiver.
/// * When the value has been resolved, read the value using [`FutureReceiver::read()`] value on receiver, consuming
///the receiver in process.
/// * Map or sink return values using function objects: [`FutureReceiver::map()`] and [`FutureReceiver::sink_to()`].
///
///[`FutureReceiver::from`]: struct.FutureReceiver.html
///[`FutureReceiver::settled()`]: struct.FutureReceiver.html#method.settled
///[`FutureSender::settle()`]: struct.FutureSender.html#method.settle
///[`FutureReceiver::read()`]: struct.FutureReceiver.html#method.read
///[`FutureReceiver::map()`]: struct.FutureReceiver.html#method.map
///[`FutureReceiver::sink_to()`]: struct.FutureReceiver.html#method.sink_to
pub fn create_future<T:Sized+Send>() -> (FutureSender<T>, FutureReceiver<T>)
{
	let inner = Arc::new((Mutex::new(FutureReceiverInner::None), AtomicBool::new(false)));
	let sender = FutureSender{value:inner.clone()};
	let receiver = FutureReceiver(_FutureReceiver::Delayed(inner));
	(sender, receiver)
}

#[cfg(test)]
mod test;
