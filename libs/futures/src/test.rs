use super::*;
use btls_aux_collections::Arc;
use core::sync::atomic::AtomicUsize;
use core::sync::atomic::Ordering;

struct Sink(Arc<AtomicUsize>);

impl FutureSink<usize> for Sink
{
	fn sink(&mut self, val: usize)
	{
		self.0.store(val, Ordering::Relaxed);
	}
}

#[test]
fn immediate()
{
	let f: FutureReceiver<u32> = FutureReceiver::from(42);
	assert!(f.settled());
	match f.read() {
		Ok(42) => (),
		_ => unreachable!()
	}
}

#[test]
fn delayed()
{
	let (s, f) = create_future::<u32>();
	assert!(!f.settled());
	let f = match f.read() {
		Ok(_) => unreachable!(),
		Err(x) => x
	};
	assert!(!f.settled());
	s.settle(42);
	assert!(f.settled());
	match f.read() {
		Ok(42) => (),
		_ => unreachable!()
	}
}

#[test]
fn map_immediate()
{
	let f: FutureReceiver<u32> = FutureReceiver::from(42);
	let map = FutureMapperFn::new(|input:u32| -> i32 { -(input as i32) });
	let f = f.map(map);
	assert!(f.settled());
	match f.read() {
		Ok(-42) => (),
		_ => unreachable!()
	}
}

#[test]
fn map_ready()
{
	let (s, f) = create_future::<u32>();
	let map = FutureMapperFn::new(|input:u32| -> i32 { -(input as i32) });
	s.settle(42);
	let f = f.map(map);
	assert!(f.settled());
	match f.read() {
		Ok(-42) => (),
		_ => unreachable!()
	}
}

#[test]
fn map_delayed()
{
	let (s, f) = create_future::<u32>();
	let map = FutureMapperFn::new(|input:u32| -> i32 { -(input as i32) });
	let f = f.map(map);
	assert!(!f.settled());
	let f = match f.read() {
		Ok(_) => unreachable!(),
		Err(x) => x
	};
	s.settle(42);
	assert!(f.settled());
	match f.read() {
		Ok(-42) => (),
		_ => unreachable!()
	}
}

#[test]
fn sink_immediate()
{
	let f: FutureReceiver<usize> = FutureReceiver::from(42);
	let sink_t = Arc::new(AtomicUsize::new(0));
	f.sink_to(Box::new(Sink(sink_t.clone())));
	assert!(sink_t.load(Ordering::Relaxed) == 42);
}

#[test]
fn sink_ready()
{
	let (s, f) = create_future::<usize>();
	s.settle(42);
	let sink_t = Arc::new(AtomicUsize::new(0));
	f.sink_to(Box::new(Sink(sink_t.clone())));
	assert!(sink_t.load(Ordering::Relaxed) == 42);
}

#[test]
fn sink_delayed()
{
	let (s, f) = create_future::<usize>();
	let sink_t = Arc::new(AtomicUsize::new(0));
	f.sink_to(Box::new(Sink(sink_t.clone())));
	assert!(sink_t.load(Ordering::Relaxed) == 0);
	s.settle(42);
	assert!(sink_t.load(Ordering::Relaxed) == 42);
}
