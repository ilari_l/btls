#ifndef PQCLEAN_KYBER512_CLEAN_SYMMETRIC_H
#define PQCLEAN_KYBER512_CLEAN_SYMMETRIC_H
/*
Modified 30.4.2023 by Ilari Liusvaara:

- Refector some common stuff to common header.
- Namespace xof_state.
*/
#include "fips202.h"
#include "params.h"
#include "symmetric-common.h"
#include <stddef.h>
#include <stdint.h>

void PQCLEAN_KYBER512_CLEAN_kyber_shake128_absorb(PQCLEAN_xof_state *s,
        const uint8_t seed[KYBER_SYMBYTES],
        uint8_t x,
        uint8_t y);

void PQCLEAN_KYBER512_CLEAN_kyber_shake256_prf(uint8_t *out, size_t outlen, const uint8_t key[KYBER_SYMBYTES], uint8_t nonce);

#define XOF_BLOCKBYTES SHAKE128_RATE

#define xof_absorb(STATE, SEED, X, Y) PQCLEAN_KYBER512_CLEAN_kyber_shake128_absorb(STATE, SEED, X, Y)
#define prf(OUT, OUTBYTES, KEY, NONCE) PQCLEAN_KYBER512_CLEAN_kyber_shake256_prf(OUT, OUTBYTES, KEY, NONCE)


#endif /* SYMMETRIC_H */
