#ifndef PQCLEAN_RANDOMBYTES_H
#define PQCLEAN_RANDOMBYTES_H
/*
Modified 30.4.2023 by Ilari Liusvaara:

- Add context to randombytes.
- Stddef/stdint is also enough on Win32.
*/

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>

/*
 * Write `n` bytes of high quality random bytes to `buf`
 */
#define randombytes2     PQCLEAN_randombytes
int randombytes2(void* context, uint8_t *output, size_t n);

#ifdef __cplusplus
}
#endif

#endif /* PQCLEAN_RANDOMBYTES_H */
