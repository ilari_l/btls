//Common stuff.
#include <string.h>
#define hash_h(OUT, IN, INBYTES) PQCLEAN_sha3_256(OUT, IN, INBYTES)
#define hash_g(OUT, IN, INBYTES) PQCLEAN_sha3_512(OUT, IN, INBYTES)
#define kdf(OUT, IN, INBYTES) PQCLEAN_shake256(OUT, KYBER_SSBYTES, IN, INBYTES)
#define xof_squeezeblocks(OUT, OUTBLOCKS, STATE) PQCLEAN_shake128_squeezeblocks(OUT, OUTBLOCKS, STATE)
#define shake128_squeezeblocks(OUT, OUTBLOCKS, STATE) PQCLEAN_shake128_squeezeblocks(OUT, OUTBLOCKS, STATE)
#define shake256 PQCLEAN_shake256
#define shake128_absorb PQCLEAN_shake128_absorb

//No-op, since state is allocated on stack.
#define shake128_ctx_release(CTX) do {} while(0)
#define xof_ctx_release(CTX) do {} while(0)

//The state needs to be zeroed.
#define DEFINE_XOF_STATE(VAR) \
	PQCLEAN_xof_state VAR ; \
	memset(& VAR ,0, 208);
