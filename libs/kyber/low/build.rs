extern crate cc;
use cc::Build;
use std::env::var;
use std::env::var_os;
use std::fs::File;
use std::io::Write;
use std::path::Path;
use std::thread::JoinHandle;
use std::thread::spawn;
// <<< std:: This is build script >>>

macro_rules! w
{
	($fp:ident S $arg:expr) => { w!($fp "{t}", t=$arg) };
	($fp:ident $($args:tt)*) => { writeln!($fp, $($args)*).expect("Write failed") };
}

fn keypair_call(pfd: &str) -> String
{
	format!("{pfd}_keypair(rng_ctx, pk, sk)")
}

fn keypair_proto(pfd: &str) -> String
{
	format!("fn {pfd}_keypair(rng_ctx: *mut (), pk: *mut u8, sk: *mut u8) -> i32")
}

fn enc_call(pfd: &str) -> String
{
	format!("{pfd}_enc(rng_ctx, ct, ss, pk)")
}

fn enc_proto(pfd: &str) -> String
{
	format!("fn {pfd}_enc(rng_ctx: *mut (), ct: *mut u8, ss: *mut u8, pk: *const u8) -> i32")
}

fn dec_call(pfd: &str) -> String
{
	format!("{pfd}_dec(ss, ct, sk, check)")
}

fn dec_proto(pfd: &str) -> String
{
	format!("fn {pfd}_dec(ss: *mut u8, ct: *const u8, sk: *const u8, check: i32) -> i32")
}

static VARIANTS: &'static [usize] = &[512, 768, 1024];
static FUNCS: &'static [(fn(&str) -> String, fn(&str) -> String)] = &[
	(keypair_proto, keypair_call),
	(enc_proto, enc_call),
	(dec_proto, dec_call),
];

fn autogen_amd64(mut fp: File)
{
	w!(fp S "extern");
	w!(fp S "{");
	for variant in VARIANTS {
		let avx2 = format!("PQCLEAN_KYBER{variant}_AVX2_crypto_kem");
		let base = format!("PQCLEAN_KYBER{variant}_CLEAN_crypto_kem");
		for (func, _) in FUNCS {
			w!(fp "\t{proto};", proto=func(&avx2));
			w!(fp "\t{proto};", proto=func(&base));
		}
	}
	w!(fp S "}");
	for variant in VARIANTS {
		let avx2 = format!("PQCLEAN_KYBER{variant}_AVX2_crypto_kem");
		let base = format!("PQCLEAN_KYBER{variant}_CLEAN_crypto_kem");
		let exp = format!("kyber{variant}");
		for (func, call) in FUNCS {
			w!(fp "pub unsafe {proto}", proto=func(&exp));
			w!(fp S "{");
			w!(fp S "\tif avx2_available() {");
			w!(fp "\t\t{t}", t=call(&avx2));
			w!(fp S "\t} else {");
			w!(fp "\t\t{t}", t=call(&base));
			w!(fp S "\t}");
			w!(fp S "}");
		}
	}
}

fn autogen_single(mut fp: File, var: &str)
{
	w!(fp S "extern");
	w!(fp S "{");
	for variant in VARIANTS {
		let base = format!("PQCLEAN_KYBER{variant}_{var}_crypto_kem");
		for (func, _) in FUNCS {
			w!(fp "\t{proto};", proto=func(&base));
		}
	}
	w!(fp S "}");
	for variant in VARIANTS {
		let base = format!("PQCLEAN_KYBER{variant}_{var}_crypto_kem");
		let exp = format!("kyber{variant}");
		for (func, call) in FUNCS {
			w!(fp "pub unsafe {proto}", proto=func(&exp));
			w!(fp S "{");
			w!(fp "\t{t}", t=call(&base));
			w!(fp S "}");
		}
	}
}

fn add_file(config: &mut Build, file: impl AsRef<str>)
{
	let file = file.as_ref();
	println!("cargo:rerun-if-changed={file}");
	config.file(file);
}

fn compile_kyber_clean_base()
{
	//This does not compile anything.
	let common_files = ["fips202.h", "fq.inc", "randombytes.h", "shuffle.inc", "stack.inc", "symmetric-common.h"];
	for file in common_files { println!("cargo:rerun-if-changed=src/kyber-common/{file}"); }
}

fn compile_kyber_clean(variant: usize)
{
	let base_files = ["cbd", "indcpa", "kem", "ntt", "poly", "polyvec", "reduce", "symmetric-shake", "verify"];
	let mut config = Build::new();
	config.include("src/kyber-common");
	for file in base_files.iter() {
		add_file(&mut config, format!("src/kyber-{variant}/clean/{file}.c"));
	}
	config.compile(&format!("kyber-clean-{variant}"));
}

fn compile_kyber_avx2_base()
{
	let mut config = Build::new();
	config.include("src/kyber-common");
	config.flag("-mavx2");
	config.flag("-mbmi2");
	config.flag("-mpopcnt");
	add_file(&mut config, "src/kyber-common/keccak4x/KeccakP-1600-times4-SIMD256.c");
	config.compile(&format!("kyber-avx2"));
}

fn compile_kyber_avx2(variant: usize)
{
	let avx2_files = ["cbd", "consts", "fips202x4", "indcpa", "kem", "poly", "polyvec", "rejsample",
		"symmetric-shake", "verify"];
	let avx2_files_s = ["basemul", "fq", "invntt", "ntt", "shuffle"];
	let mut config = Build::new();
	config.include("src/kyber-common");
	config.flag("-mavx2");
	config.flag("-mbmi2");
	config.flag("-mpopcnt");
	for variant in VARIANTS {
		for file in avx2_files.iter() {
			add_file(&mut config, format!("src/kyber-{variant}/avx2/{file}.c"));
		}
		for file in avx2_files_s.iter() {
			add_file(&mut config, format!("src/kyber-{variant}/avx2/{file}.S"));
		}
	}
	config.compile(&format!("kyber-avx2-{variant}"));
}

fn compile_kyber_aarch64_base()
{
	//Nothing to do with this.
}

fn compile_kyber_aarch64(variant: usize)
{
	let aarch64_files = ["cbd", "fips202x2", "indcpa", "kem", "neon_poly", "neon_polyvec",
		"neon_symmetric-shake", "ntt", "poly", "polyvec", "reduce", "rejsample", "symmetric-shake", "verify"];
	let aarch64_files_s = ["__asm_base_mul", "__asm_NTT", "__asm_iNTT", "__asm_poly"];
	let mut config = Build::new();
	config.include("src/kyber-common");
	for file in aarch64_files.iter() {
		add_file(&mut config, format!("src/kyber-{variant}/aarch64/{file}.c"));
	}
	for file in aarch64_files_s.iter() {
		add_file(&mut config, format!("src/kyber-{variant}/aarch64/{file}.S"));
	}
	config.compile(&format!("kyber-aarch64-{variant}"));
}

fn main()
{
	let out_dir = var_os("OUT_DIR").expect("No OUT_DIR set");
	let dest_path = Path::new(&out_dir).join("autogenerated.inc.rs");
	let fp = File::create(dest_path).expect("Failed to open output file");
	let target = var("CARGO_CFG_TARGET_ARCH").expect("No CARGO_CFG_TARGET_ARCH");
	let mut threads: Vec<JoinHandle<()>> = Vec::new();

	threads.push(spawn(||compile_kyber_clean_base()));
	for variant in VARIANTS { threads.push(spawn(||compile_kyber_clean(*variant))); }

	if target == "x86_64" {
		threads.push(spawn(||compile_kyber_avx2_base()));
		for variant in VARIANTS { threads.push(spawn(||compile_kyber_avx2(*variant))); }
		autogen_amd64(fp);
	} else if target == "aarch64" {
		threads.push(spawn(||compile_kyber_aarch64_base()));
		for variant in VARIANTS { threads.push(spawn(||compile_kyber_aarch64(*variant))); }
		autogen_single(fp, "AARCH64");
	} else {
		autogen_single(fp, "CLEAN");
	}
	for thread in threads.drain(..) {
		if thread.join().is_err() { panic!("Compiler failed"); }
	}
	println!("cargo:rerun-if-changed=build.rs");
}
