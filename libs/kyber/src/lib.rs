#![no_std]
use btls_aux_kyber_cimpl as C;
use btls_aux_random::TemporaryRandomStream;
use core::convert::AsRef;
use core::mem::MaybeUninit;
use core::mem::transmute;

#[no_mangle]
pub static KYBER_IMPL_KECCAK_PERMUTE: fn(&mut [u64;25]) = btls_aux_sha3::raw_keccak_permutation;
#[no_mangle]
pub static KYBER_IMPL_GETRANDOM: fn(*mut (), buf: &mut [u8]) = __getrandom;

fn __getrandom(ctx: *mut (), mem: &mut [u8])
{
	let ctx: &mut TemporaryRandomStream = unsafe{transmute(ctx)};
	ctx.fill_bytes(mem);
}

///Cast from byte slice.
pub trait FromByteSlice
{
	///Cast a slice.
	fn from_slice<'a>(slice: &'a [u8]) -> Option<&'a Self>;
	///Get length.
	fn length() -> usize;
}

impl<const N: usize> FromByteSlice for [u8;N]
{
	fn from_slice<'a>(slice: &'a [u8]) -> Option<&'a Self> { slice.try_into().ok() }
	fn length() -> usize { N }
}

///A Key Encapsulation Method.
pub trait KEM
{
	///Secret key.
	type SK: AsRef<[u8]>+Copy;
	///Public key.
	type PK: AsRef<[u8]>+FromByteSlice+Copy;
	///Ciphertext.
	type CT: AsRef<[u8]>+FromByteSlice+Copy;
	///Shared secret.
	type SS: AsRef<[u8]>+Copy;
	///Generate keypair.
	///
	///sk MUST be SKLEN, pk must be PKLEN.
	fn keypair<'skl,'pkl>(rng: &mut TemporaryRandomStream, sk: &'skl mut MaybeUninit<<Self as KEM>::SK>,
		pk: &'pkl mut MaybeUninit<<Self as KEM>::PK>) -> (&'skl <Self as KEM>::SK, &'pkl <Self as KEM>::PK);
	///Encapsulate.
	///
	///ct MUST be CTLEN, ss MUST be SSLEN.
	fn encapsulate<'ctl,'ssl>(rng: &mut TemporaryRandomStream, ct: &'ctl mut MaybeUninit<<Self as KEM>::CT>,
		ss: &'ssl mut MaybeUninit<<Self as KEM>::SS>, pk: &<Self as KEM>::PK) ->
		(&'ctl <Self as KEM>::CT, &'ssl <Self as KEM>::SS);
	//Decapsulate.
	fn decapsulate<'ssl>(ss: &'ssl mut MaybeUninit<<Self as KEM>::SS>, ct: &<Self as KEM>::CT,
		sk: &<Self as KEM>::SK) -> Result<&'ssl <Self as KEM>::SS, ()>;
	//Decapsulate without checking.
	fn decapsulate_unchecked<'ssl>(ss: &'ssl mut MaybeUninit<<Self as KEM>::SS>, ct: &<Self as KEM>::CT,
		sk: &<Self as KEM>::SK) -> &'ssl <Self as KEM>::SS;
}

fn ref2ptr_mut<T>(x: &mut T) -> *mut T { x as *mut T }

fn pass_ctx(rng: &mut TemporaryRandomStream) -> *mut ()
{
	ref2ptr_mut(rng).cast::<()>()
}

fn pass_ref<const N: usize>(slice: &[u8;N]) -> *const u8
{
	slice.as_ptr().cast::<u8>()
}

fn pass_mut<const N: usize>(slice: &mut MaybeUninit<[u8;N]>) -> *mut u8
{
	slice.as_mut_ptr().cast::<u8>()
}

macro_rules! impl_kyber
{
	($clazz:ident SK[$sklen:ident] PK[$pklen:ident] CT[$ctlen:ident] SS[$sslen:ident] KEYPAIR[$keypair:ident]
		ENC[$enc:ident] DEC[$dec:ident]) => {
		impl KEM for $clazz
		{
			type SK = [u8;C::$sklen];
			type PK = [u8;C::$pklen];
			type CT = [u8;C::$ctlen];
			type SS = [u8;C::$sslen];
			fn keypair<'skl,'pkl>(rng: &mut TemporaryRandomStream,
				sk: &'skl mut MaybeUninit<[u8;C::$sklen]>,
				pk: &'pkl mut MaybeUninit<[u8;C::$pklen]>) ->
				(&'skl [u8;C::$sklen], &'pkl [u8;C::$pklen])
			{
				unsafe{C::$keypair(pass_ctx(rng), pass_mut(pk), pass_mut(sk));}
				unsafe{(sk.assume_init_ref(), pk.assume_init_ref())}
			}
			fn encapsulate<'ctl,'ssl>(rng: &mut TemporaryRandomStream,
				ct: &'ctl mut MaybeUninit<[u8;C::$ctlen]>,
				ss: &'ssl mut MaybeUninit<[u8;C::$sslen]>, pk: &[u8;C::$pklen]) ->
				(&'ctl [u8;C::$ctlen], &'ssl [u8;C::$sslen])
			{
				unsafe{C::$enc(pass_ctx(rng), pass_mut(ct), pass_mut(ss), pass_ref(pk));}
				unsafe{(ct.assume_init_ref(), ss.assume_init_ref())}
			}
			fn decapsulate<'ssl>(ss: &'ssl mut MaybeUninit<[u8;C::$sslen]>, ct: &[u8;C::$ctlen],
				sk: &[u8;C::$sklen]) -> Result<&'ssl [u8;C::$sslen], ()>
			{
				let r = unsafe{C::$dec(pass_mut(ss), pass_ref(ct), pass_ref(sk), 1)};
				if r == 0 { Ok(unsafe{ss.assume_init_ref()}) } else { Err(()) }
			}
			fn decapsulate_unchecked<'ssl>(ss: &'ssl mut MaybeUninit<[u8;C::$sslen]>, ct: &[u8;C::$ctlen],
				sk: &[u8;C::$sklen]) -> &'ssl [u8;C::$sslen]
			{
				unsafe{C::$dec(pass_mut(ss), pass_ref(ct), pass_ref(sk), 0);}
				unsafe{ss.assume_init_ref()}
			}
		}
	}
}


///Kyber-512
pub struct Kyber512;
impl_kyber!(Kyber512
	SK[KYBER512_SKLEN] PK[KYBER512_PKLEN] CT[KYBER512_CTLEN] SS[KYBER512_SSLEN]
	KEYPAIR[kyber512_keypair] ENC[kyber512_enc] DEC[kyber512_dec]
);
///Kyber-768
pub struct Kyber768;
impl_kyber!(Kyber768
	SK[KYBER768_SKLEN] PK[KYBER768_PKLEN] CT[KYBER768_CTLEN] SS[KYBER768_SSLEN]
	KEYPAIR[kyber768_keypair] ENC[kyber768_enc] DEC[kyber768_dec]
);
///Kyber-1024
pub struct Kyber1024;
impl_kyber!(Kyber1024
	SK[KYBER1024_SKLEN] PK[KYBER1024_PKLEN] CT[KYBER1024_CTLEN] SS[KYBER1024_SSLEN]
	KEYPAIR[kyber1024_keypair] ENC[kyber1024_enc] DEC[kyber1024_dec]
);

#[cfg(test)]
macro_rules! test_kem
{
	($K:ident) => {{
		let mut pk = MaybeUninit::<<$K as KEM>::PK>::uninit();
		let mut sk = MaybeUninit::<<$K as KEM>::SK>::uninit();
		let mut ct = MaybeUninit::<<$K as KEM>::CT>::uninit();
		let mut ss1 = MaybeUninit::<<$K as KEM>::SS>::uninit();
		let mut ss2 = MaybeUninit::<<$K as KEM>::SS>::uninit();
		let mut ss3 = MaybeUninit::<<$K as KEM>::SS>::uninit();
		let rng = btls_aux_random::TemporaryRandomLifetimeTag;
		let mut rng = TemporaryRandomStream::new_system(&rng);
		let (sk, pk) = <$K as KEM>::keypair(&mut rng, &mut sk, &mut pk).expect("Keypair failed");
		let (ct, ss1) = <$K as KEM>::encapsulate(&mut rng, &mut ct, &mut ss1, pk).expect("Encapsulate failed");
		let ss2 = <$K as KEM>::decapsulate(&mut ss2, ct, sk).expect("Decapsulate failed");
		let ss3 = <$K as KEM>::decapsulate_unchecked(&mut ss3, ct, sk);
		assert!(ss1 == ss2);
		assert!(ss1 == ss3);
	}}
}

#[test] fn kyber_512() { test_kem!(Kyber512); }
#[test] fn kyber_768() { test_kem!(Kyber768); }
#[test] fn kyber_1024() { test_kem!(Kyber1024); }

#[test] fn sha3_256()
{
	fn check(data: &[u8])
	{
		let mut buf1 = [0;32];
		unsafe{C::PQCLEAN_sha3_256(buf1.as_mut_ptr(), data.as_ptr(), data.len());}
		let mut h = btls_aux_sha3::Sha3256::new();
		h.input(data);
		let buf2 = h.output();
		assert!(buf1 == buf2);
	}
	check(b"abc");
	check(b"abcdefgh");
	check(b"abcdefghijk");
	check(&[42;153]);
}

#[test] fn sha3_512()
{
	fn check(data: &[u8])
	{
		let mut buf1 = [0;64];
		unsafe{C::PQCLEAN_sha3_512(buf1.as_mut_ptr(), data.as_ptr(), data.len());}
		let mut h = btls_aux_sha3::Sha3512::new();
		h.input(data);
		let buf2 = h.output();
		assert!(buf1 == buf2);
	}
	check(b"abc");
	check(b"abcdefgh");
	check(b"abcdefghijk");
	check(&[42;153]);
}

#[test] fn shake256()
{
	fn check(data: &[u8])
	{
		let mut buf1 = [0;128];
		let mut buf2 = [0;128];
		unsafe{C::PQCLEAN_shake256(buf1.as_mut_ptr(), 128, data.as_ptr(), data.len());}
		let mut h = btls_aux_sha3::Shake256::new();
		h.input(data);
		h.output(&mut buf2);
		assert!(buf1 == buf2);
	}
	check(b"abc");
	check(b"abcdefgh");
	check(b"abcdefghijk");
	check(&[42;153]);
}

#[test] fn shake128()
{
	use core::mem::zeroed;
	let buf: [u8;34] = [
		1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34
	];
	let mut state: btls_aux_kyber_cimpl::Context = unsafe{zeroed()};
	let mut rstate = btls_aux_sha3::Shake128::new();
	unsafe{btls_aux_kyber_cimpl::PQCLEAN_shake128_absorb(&mut state, buf.as_ptr(), buf.len());}
	rstate.input(&buf);
	let mut rout = [0;168*32];
	rstate.output(&mut rout);
	for i in 0..16 {
		let mut out = [0;168*2];
		unsafe{btls_aux_kyber_cimpl::PQCLEAN_shake128_squeezeblocks(out.as_mut_ptr(), 2, &mut state);}
		assert!(&out[..] == &rout[168*2*i..][..168*2]);
	}
}
