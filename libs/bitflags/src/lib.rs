use btls_aux_proc_macro_tools::Element;
use btls_aux_proc_macro_tools::ElementList;
use btls_aux_proc_macro_tools::output;
use btls_aux_proc_macro_tools::OutputIdentifier as Id;
use btls_aux_proc_macro_tools::OwnedOutput;
use btls_aux_proc_macro_tools::RootElementList;
use proc_macro::TokenStream;
use std::cmp::min;
use std::ops::Deref;


#[derive(Clone,Debug)]
struct Parsed3
{
	name: ElementList,
	publicity: Option<Option<ElementList>>,
	flags: Vec<String>,
	fields: Vec<usize>,
}

fn stream_to_tokens2(s: TokenStream) -> Parsed3
{
	let rast = RootElementList::new(s);
	let mut ast = rast.to_stream();
	let mut flag_names: Vec<String> = Vec::new();
	let mut fields: Vec<usize> = Vec::new();
	//Attributes.
	while let Some(attribute) = ast.next_attribute(&rast) {
		attribute.expect_outer(&rast);
		let mut attribute = attribute.to_stream();
		let attrname = attribute.expect_ident(&rast);
		if attrname.is_identifier("bitflag") {
			let mut attribute2 = attribute.expect_paren_expr(&rast);
			attribute.expect_end(&rast);
			flag_names.push(attribute2.expect_identifier(&rast));
			attribute2.expect_end(&rast);
		}
	}
	//Visibility.
	let publicity = ast.next_visibility();
	//The actual structure. Only tuple structs with no generics are supported.
	let tmp = ast.next();
	if !tmp.is_keyword("struct") { rast.panic(&tmp, "Expected struct"); }
	let struct_name = ast.next_path();
	let mut ast2 = match ast.next() {
		Element::Paren(grp) => grp.to_stream(),
		//Give a bit nicer error messages if someone tries to use generics or named fields, neither of which
		//are supported.
		Element::Punct(ref x) if x.starts_with('<') => rast.panic(x, "Generics not supported"),
		Element::Brace(grp) => rast.panic(&grp, "Named fields not supported"),
		ref t => rast.panic(t, "Expected '('"),
	};
	ast.expect_punct(&rast, ";");
	ast.expect_end(&rast);
	
	while ast2.more() {
		//Ignore attributes and visibility, if any.
		while let Some(attr) = ast2.next_attribute(&rast) { attr.expect_outer(&rast); }
		ast2.next_visibility();
		//Get the type of field. Only u8/u16/u32/u64/u128 are supported.
		let tname = ast2.expect_ident(&rast);
		match tname.to_string().deref() {
			"u8" => fields.push(8),
			"u16" => fields.push(16),
			"u32" => fields.push(32),
			"u64" => fields.push(64),
			"u128" => fields.push(128),
			_ => rast.panic(&tname, "Unsupported type")
		}
		//Eat the list comma. This allows omitting the comma at last element.
		ast2.expect_list_comma(&rast);
	}
	Parsed3 {
		name: struct_name,
		publicity: publicity,
		flags: flag_names,
		fields: fields,
	}
}

fn emit_not2(name: &ElementList) -> OwnedOutput
{
	output!("impl ::core::ops::Not for", name, output!({}
		"type Output = ", name, ";",
		"fn not(self) -> Self", output!({} "self ^ Self::all")
	))
}

fn emit_imm_op3(name: &ElementList, tr: &str, meth: &str, ch: &str, fields: usize) -> OwnedOutput
{
	use btls_aux_proc_macro_tools::OutputPunct::*;
	let mut op_body = Vec::new();
	for i in 0..fields {
		output!(TO[op_body] "self.", i, Punct(ch), "othr.", i, ",");
	}
	output!("impl ::core::ops::", Id(tr), "for", name, output!({}
		"type Output = ", name, ";",
		"fn", Id(meth), "(self, othr: Self) -> Self",
		output!({} name, output!(() op_body))
	))
}

fn emit_ass_op3(name: &ElementList, tr: &str, meth: &str, ch: &str, fields: usize) -> OwnedOutput
{
	use btls_aux_proc_macro_tools::OutputPunct::*;
	let mut op_body = Vec::new();
	for i in 0..fields {
		output!(TO[op_body] "self.", i, Punct(ch), "othr.", i, ";");
	}
	output!("impl ::core::ops::", Id(tr), "for", name, output!({}
		"fn", Id(meth), "(&mut self, othr: Self)", output!({} op_body)
	))
}

fn emit_visibility2(visibility: &Option<Option<ElementList>>) -> OwnedOutput
{
	match visibility {
		&Some(Some(ref v)) => output!("pub", output!(() v)),
		&Some(None) => output!("pub"),
		&None => output!(),
	}
}

fn emit_single_bit_ops2(visibility: &Option<Option<ElementList>>, flag: &str, w: usize, b: usize) -> OwnedOutput
{
	let inline_always = output!("#[inline(always)]");
	let visibility = emit_visibility2(visibility);
	let pmask = output!("1 << ", b);
	let nmask = output!("!", output!(() pmask));
	let set_bit = output!("self.", w, "|=", pmask, ";");
	let clear_bit = output!("self.", w, "&=", nmask, ";");
	let force_bit = output!("if flag", output!({} set_bit), "else", output!({} clear_bit));
	output!(
		inline_always, visibility, "fn", format!("r#is_{flag}"), "(self) -> bool",
			output!({} "self.", w, ">>", b, "& 1 != 0"),
		inline_always, visibility, "fn", format!("r#set_{flag}"), "(&mut self)",
			output!({} set_bit),
		inline_always, visibility, "fn", format!("r#clear_{flag}"), "(&mut self)",
			output!({} clear_bit),
		inline_always, visibility, "fn", format!("r#force_{flag}"), "(&mut self, flag: bool)",
			output!({} force_bit),
		inline_always, visibility, "fn", format!("r#fset_{flag}"), "(mut self) -> Self",
			output!({} set_bit, "self"),
		inline_always, visibility, "fn", format!("r#fclear_{flag}"), "(mut self) -> Self",
			output!({} clear_bit, "self"),
		inline_always, visibility, "fn", format!("r#fforce_{flag}"), "(mut self, flag: bool) -> Self",
			output!({} force_bit, "self")
	)
}


fn define_constant3(visibility: &Option<Option<ElementList>>, name: &str, dtype: &ElementList, values: &[u128]) ->
	OwnedOutput
{
	let visibility = emit_visibility2(visibility);
	let mut body = Vec::new();
	for &value in values.iter() { body.push(output!(value, ",")); }
	output!(visibility, "const", Id(name), ":", dtype, "=", dtype, output!(() body), ";")
}

fn flag_slots(data: &[usize], mut position: usize) -> Vec<u128>
{
	let mut slots = Vec::new();
	for &slotsize in data.iter() {
		let val = if position < slotsize { 1u128 << position } else { 0 };
		slots.push(val);
		position = position.wrapping_sub(slotsize);
	}
	slots
}

fn none_slots(data: &[usize]) -> Vec<u128>
{
	vec![0u128;data.len()]
}

fn all_slots(data: &[usize], mut count: usize) -> Vec<u128>
{
	let mut slots = Vec::new();
	for &slotsize in data.iter() {
		let left = min(count, slotsize);
		let val = if left < 128 { (1u128 << left) - 1 } else { !0 };
		slots.push(val);
		count = count.saturating_sub(slotsize);
	}
	slots
}

fn bit_position(data: &[usize], mut index: usize) -> (usize, usize)
{
	for (w, &slotsize) in data.iter().enumerate() {
		if index < slotsize { return (w, index); }
		index -= slotsize;
	}
	panic!("BUG: Bit position out of range");
}

fn emit_global_methods2(visibility: &Option<Option<ElementList>>, fieldcount: usize) -> OwnedOutput
{
	let visibility = emit_visibility2(visibility);
	let rfalse = output!("{ return false; }");
	let rtrue = output!("{ return true; }");
	let mut test_any_body = Vec::new();
	let mut test_all_body = Vec::new();
	let mut test_ex_body = Vec::new();
	for i in 0..fieldcount {
		output!(TO[test_any_body] "if self.", i, "& mask.", i, "!= 0", rtrue);
		output!(TO[test_all_body] "if self.", i, "& mask.", i, "!= mask.", i, rfalse);
		output!(TO[test_ex_body] "if self.", i, "& mask.", i, "!= result.", i, rfalse);
	}
	output!(
		visibility, "fn test_any(self, mask: Self) -> bool",
		output!({} test_any_body, "false"),
		visibility, "fn test_all(self, mask: Self) -> bool",
		output!({} test_all_body, "true"),
		visibility, "fn test_exact(self, mask: Self, result: Self) -> bool",
		output!({} test_ex_body, "true"),
		visibility, "fn and(self, flag: bool) -> Self
		{
			if flag { self } else { Self::none }
		}",
		visibility, "fn force(self, mask: Self, flag: bool) -> Self
		{
			if flag { self | mask } else { self & !mask }
		}",
		visibility, "fn force_imp(&mut self, mask: Self, flag: bool)
		{
			if flag { *self |= mask; } else { *self &= !mask; }
		}"
	)
}

///Generate a bitset
#[proc_macro_derive(Bitflags, attributes(bitflag))]
pub fn bitset2(s: TokenStream) -> TokenStream
{
	let tokens2 = stream_to_tokens2(s.clone());
	let maxflags = tokens2.fields.iter().fold(0usize,|x,y|x+*y);
	let fcount = tokens2.flags.len();
	if fcount > maxflags { panic!("Too many flags: {fcount} > {maxflags}"); }
	let mut bitops = Vec::new();
	for (idx,flag) in tokens2.flags.iter().enumerate() {
		let slots = flag_slots(&tokens2.fields, idx);
		bitops.push(define_constant3(&tokens2.publicity, flag, &tokens2.name, &slots));
		let (w,b) = bit_position(&tokens2.fields, idx);
		bitops.push(emit_single_bit_ops2(&tokens2.publicity, flag, w, b));
	}
	let nslots = none_slots(&tokens2.fields);
	let aslots = all_slots(&tokens2.fields, tokens2.flags.len());
	output!(
		"impl", &tokens2.name, output!({}
			//Per-bit operations.
			bitops,
			//None and all.
			define_constant3(&tokens2.publicity, "none", &tokens2.name, &nslots),
			define_constant3(&tokens2.publicity, "all", &tokens2.name, &aslots),
			emit_global_methods2(&tokens2.publicity, tokens2.fields.len())
		),
		emit_not2(&tokens2.name),
		emit_imm_op3(&tokens2.name, "BitOr", "bitor", "|", tokens2.fields.len()),
		emit_imm_op3(&tokens2.name, "BitXor", "bitxor", "^", tokens2.fields.len()),
		emit_imm_op3(&tokens2.name, "BitAnd", "bitand", "&", tokens2.fields.len()),
		emit_ass_op3(&tokens2.name, "BitOrAssign", "bitor_assign", "|=", tokens2.fields.len()),
		emit_ass_op3(&tokens2.name, "BitXorAssign", "bitxor_assign", "^=", tokens2.fields.len()),
		emit_ass_op3(&tokens2.name, "BitAndAssign", "bitand_assign", "&=", tokens2.fields.len())
	).to_token_stream()
}
