pub use btls_aux_codegen_derive::quote;
use std::borrow::Cow;
use std::cmp::max;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::io::Write;


#[doc(hidden)]
pub use std::borrow::Cow as __StdCow;

#[derive(Copy,Clone,PartialEq,Eq,Debug)]
pub enum TerminalKind
{
	Group,
	Punct,
	Ident,
}

#[derive(Copy,Clone,PartialEq,Eq,Debug)]
pub enum GroupKind
{
	Brace,
	Bracket,
	Parenthesis,
	None,
}

#[derive(Clone,PartialEq,Eq,Debug)]
pub enum Elements
{
	Group(GroupKind, bool),		//opening flag.
	Ident(Cow<'static, str>),
	Lifetime(Cow<'static, str>),
	Literal(Cow<'static, str>),
	Punct(Cow<'static, str>),
}

impl Elements
{
	pub fn find_matching_close(array: &[Elements], mut index: usize) -> usize
	{
		let mut stack: Vec<GroupKind> = Vec::new();
		let this = array.get(index).expect("find_matching_close: Bad start index");
		if let &Elements::Group(kind, true) = this {
			stack.push(kind);
		} else {
			panic!("find_matching_close: Start element is not open group");
		}
		//Advance past the starting open.
		index += 1;
		while stack.len() > 0 {
			let this = array.get(index).expect("find_matching_close: Unexpected end");
			match this {
				&Elements::Group(kind, true) => stack.push(kind),
				&Elements::Group(kind, false) => {
					let kind2 = stack.pop().expect("find_matching_close: Empty stack???");
					if kind != kind2 { panic!("find_matching_close: Mismatched groups"); }
				},
				_ => ()		//Ignore non-group.
			}
			//Next token.
			index += 1;
		}
		//Return one past end index.
		index
	}
}

fn terminals_incompatible(a: TerminalKind, b: TerminalKind) -> bool
{
	match (a, b) {
		(TerminalKind::Punct, TerminalKind::Punct) => true,
		(TerminalKind::Ident, TerminalKind::Ident) => true,
		_ => false
	}
}

fn emit_array<B:Substitution>(a: &[B]) -> String
{
	let mut o = String::new();
	o.push('[');
	for i in 0..a.len() {
		if i > 0 { o.push(','); }
		let e = &a[i];
		let (x, _, _) = e.to_code();
		o.push_str(&x);
	}
	o.push(']');
	o
}

#[derive(Clone,Debug)]
pub struct Symbol(String);

pub fn mkid(x: &str) -> Symbol { Symbol(x.to_string()) }

#[macro_export]
macro_rules! mkidf
{
	($($args:tt)*) => { $crate::mkid(&format!($($args)*)) }
}

pub trait Substitution
{
	fn to_code(&self) -> (String, TerminalKind, TerminalKind);
}

impl<'a,S:Substitution> Substitution for &'a S
{
	fn to_code(&self) -> (String, TerminalKind, TerminalKind) { (*self).to_code() }
}

impl<B:BitmaskBase> Substitution for BitmaskSub<B>
{
	fn to_code(&self) -> (String, TerminalKind, TerminalKind)
	{
		(emit_array(&self.set), TerminalKind::Group, TerminalKind::Group)
	}
}

impl<B:BitmaskBase> Substitution for Bitmask<B>
{
	fn to_code(&self) -> (String, TerminalKind, TerminalKind)
	{
		(emit_array(&self.universe), TerminalKind::Group, TerminalKind::Group)
	}
}

impl Substitution for CodeBlock
{
	fn to_code(&self) -> (String, TerminalKind, TerminalKind)
	{
		(self.code.clone(), self.start_terminal, self.end_terminal)
	}
}

impl Substitution for Vec<CodeBlock>
{
	fn to_code(&self) -> (String, TerminalKind, TerminalKind)
	{
		//Transform each element independently.
		let code: Vec<(String, TerminalKind, TerminalKind)> = self.iter().map(|x|{
			x.to_code()
		}).collect();
		//Fall back to Group, as it can combine with anything.
		let left = code.first().map_or(TerminalKind::Group, |&(_,k,_)|k);
		//Start from group, since it combines with anything.
		let mut kind = TerminalKind::Group;
		let mut out = String::new();
		for &(ref frag, start, end) in code.iter() {
			//Seprator if not combining.
			if terminals_incompatible(kind, start) { out.push(' '); }
			out.push_str(frag);
			kind = end;
		}
		(out, left, kind)
	}
}

impl Substitution for Symbol
{
	fn to_code(&self) -> (String, TerminalKind, TerminalKind)
	{
		let t = TerminalKind::Ident;
		(format!("r#{sym}", sym=&self.0), t, t)
	}
}

macro_rules! def_int_substitution
{
	($x:ident) => {
		impl Substitution for $x
		{
			fn to_code(&self) -> (String, TerminalKind, TerminalKind)
			{
				let t = TerminalKind::Ident;
				(format!("{self}{suffix}", suffix=stringify!($x)), t, t)
			}
		}
	}
}

def_int_substitution!(i8);
def_int_substitution!(i16);
def_int_substitution!(i32);
def_int_substitution!(i64);
def_int_substitution!(i128);
def_int_substitution!(isize);
def_int_substitution!(u8);
def_int_substitution!(u16);
def_int_substitution!(u32);
def_int_substitution!(u64);
def_int_substitution!(u128);
def_int_substitution!(usize);

impl Substitution for bool
{
	fn to_code(&self) -> (String, TerminalKind, TerminalKind)
	{
		let t = TerminalKind::Ident;
		let x = if *self { "true" } else { "false" };
		(x.to_string(), t, t)
	}
}

fn do_string_substitution(x: &str) -> (String, TerminalKind, TerminalKind)
{
	let t = TerminalKind::Ident;
	let mut o = String::new();
	o.push('\"');
	for c in x.chars() { match c {
		'\\' => o.push_str("\\\\"),
		'\"' => o.push_str("\\\""),
		c => o.push(c),
	}}
	o.push('\"');
	(o, t, t)
}

fn do_bstring_substitution(x: &[u8]) -> (String, TerminalKind, TerminalKind)
{
	use std::fmt::Write;
	let t = TerminalKind::Ident;
	let mut o = String::new();
	o.push('b');
	o.push('\"');
	for &c in x.iter() { match c {
		9 => o.push_str("\\t"),
		10 => o.push_str("\\n"),
		13 => o.push_str("\\r"),
		b'\\' => o.push_str("\\\\"),
		b'\"' => o.push_str("\\\""),
		32..=126 => o.push(c as char),
		c => write!(o, "\\x{c:02x}").unwrap()
	}}
	o.push('\"');
	(o, t, t)
}

impl Substitution for String
{
	fn to_code(&self) -> (String, TerminalKind, TerminalKind)
	{
		do_string_substitution(self)
	}
}

impl<'a> Substitution for &'a str
{
	fn to_code(&self) -> (String, TerminalKind, TerminalKind)
	{
		do_string_substitution(self)
	}
}

#[derive(Clone)]
pub struct BString(pub Vec<u8>);

impl Substitution for BString
{
	fn to_code(&self) -> (String, TerminalKind, TerminalKind)
	{
		do_bstring_substitution(&self.0)
	}
}

pub struct BareLiteral<T:Display>(pub T);

impl<T:Display> Substitution for BareLiteral<T>
{
	fn to_code(&self) -> (String, TerminalKind, TerminalKind)
	{
		(self.0.to_string(), TerminalKind::Ident, TerminalKind::Ident)
	}
}

pub struct CommaList<T:Substitution>(pub Vec<T>);

impl<T:Substitution> Substitution for CommaList<T>
{
	fn to_code(&self) -> (String, TerminalKind, TerminalKind)
	{
		//Transform each element independently.
		let code: Vec<(String, TerminalKind, TerminalKind)> = self.0.iter().map(|x|{
			x.to_code()
		}).collect();
		//Fall back to Group, as it can combine with anything.
		let left = code.first().map_or(TerminalKind::Group, |&(_,k,_)|k);
		let right = code.last().map_or(TerminalKind::Group, |&(_,_,k)|k);
		let mut out = String::new();
		for &(ref frag, _, _) in code.iter() {
			//Nothing ever combines with comma, so no need to care about interrior kinds.
			if out.len() > 0 { out.push(','); }
			out.push_str(frag);
		}
		(out, left, right)
	}
}


pub struct WIPCodeBlock
{
	start_terminal: TerminalKind,
	end_terminal: TerminalKind,
	any_token: bool,
	code: String,
}

impl WIPCodeBlock
{
	pub fn new() -> WIPCodeBlock
	{
		WIPCodeBlock {
			start_terminal: TerminalKind::Group,
			end_terminal: TerminalKind::Group,
			any_token: false,
			code: String::new(),
		}
	}
	pub fn push_masktype<B:BitmaskBase>(&mut self, bs: &Bitmask<B>)
	{
		let code = format!("[{elt_ty};{sz}]", elt_ty=B::elt_type(), sz=bs.universe.len());
		self.push_raw(&code, TerminalKind::Group, TerminalKind::Group);
	}
	pub fn push_comma(&mut self, x: &str)
	{
		self.push_raw(x, TerminalKind::Punct, TerminalKind::Punct);
	}
	pub fn push_raw(&mut self, x: &str, st: TerminalKind, et: TerminalKind) 
	{
		//If incompatible terminals, add space.
		if self.any_token && terminals_incompatible(self.end_terminal, st) {
			self.code.push(' ');
		}
		self.code.push_str(x);
		//If nothing yet, make the start terminal of raw start terminal of the code.
		if !self.any_token {
			self.start_terminal = st;
			self.any_token = true;
		}
		//Update end terminal of the raw to be end terminal of the code.
		self.end_terminal = et;
	}
	pub fn push_l0(&mut self, x: &impl Substitution)
	{
		let (x, st, et) = x.to_code();
		self.push_raw(&x, st, et);
	}
	pub fn finalize(self) -> CodeBlock
	{
		CodeBlock {
			start_terminal: self.start_terminal,
			end_terminal: self.end_terminal,
			code: self.code,
		}
	}
}

#[derive(Clone)]
pub struct CodeBlock
{	
	start_terminal: TerminalKind,
	end_terminal: TerminalKind,
	code: String,
}

impl AsRef<str> for CodeBlock
{
	fn as_ref(&self) -> &str { &self.code }
}

//So .to_string() exists.
impl Display for CodeBlock
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		Display::fmt(&self.code, f)
	}
}

pub fn qwrite<W:Write,S:AsRef<str>>(out: &mut W, s: S)
{
	out.write_all(format_code(s.as_ref()).as_bytes()).expect("Writing code failed");
}

pub trait BitmaskBase: Substitution+Copy
{
	fn bits() -> usize;
	fn zero() -> Self;
	fn elt_type() -> &'static str;
	fn v_setbit(x: &mut Self, p: usize);
}

macro_rules! impl_bitmask_base
{
	($xtype:ident,$bits:tt) => {
		impl BitmaskBase for $xtype
		{
			fn bits() -> usize { $bits }
			fn zero() -> Self { 0 }
			fn elt_type() -> &'static str { stringify!($xtype) }
			fn v_setbit(x: &mut Self, p: usize) { *x |= 1 << p; }
		}
	}
}

impl_bitmask_base!(u8, 8);
impl_bitmask_base!(u16, 16);
impl_bitmask_base!(u32, 32);
impl_bitmask_base!(u64, 64);
impl_bitmask_base!(u128, 128);

pub struct BitmaskSub<B:BitmaskBase>
{
	bound: usize,
	set: Vec<B>,
}

impl<B:BitmaskBase> BitmaskSub<B>
{
	pub fn add(&mut self, b: usize)
	{
		let s = B::bits();
		assert!(b < self.bound, "Bit index out of range");
		B::v_setbit(&mut self.set[b/s], b%s);
	}
}

pub struct Bitmask<B:BitmaskBase>
{
	bound: usize,
	universe: Vec<B>,
}

impl<B:BitmaskBase> Bitmask<B>
{
	pub fn new<I:Iterator<Item=usize>>(mut iter: I) -> Bitmask<B>
	{
		let s = B::bits();
		let mut bound = 0;
		let mut universe = Vec::<B>::new();
		while let Some(b) = iter.next() {
			bound = max(bound, b + 1);
			while universe.len() <= b/s { universe.push(B::zero()); }
			B::v_setbit(&mut universe[b/s], b%s);
		}
		Bitmask{bound, universe}
	}
	pub fn len(&self) -> usize { self.universe.len() }
	pub fn make<I:Iterator<Item=usize>>(&self, mut iter: I) -> Vec<B>
	{
		let s = B::bits();
		let mut x = vec![B::zero(); (self.bound + s - 1) / s];
		while let Some(b) = iter.next() {
			assert!(b < self.bound, "Bit index out of range");
			B::v_setbit(&mut x[b/s], b%s);
		}
		x
	}
	pub fn make64<I:Iterator<Item=usize>>(&self, mut iter: I) -> u64
	{
		let mut x = 0u64;
		while let Some(b) = iter.next() {
			if b < 64 { x |= 1 << b; }
		}
		x
	}
	pub fn sub(&self) -> BitmaskSub<B>
	{
		BitmaskSub {
			bound: self.bound,
			set: vec![B::zero();self.universe.len()],
		}
	}
}

pub fn encode_bstr(x: &[u8]) -> String
{
	use std::fmt::Write;
	let mut out = String::new();
	out.push_str(r#"b""#);
	for &b in x.iter() {
		match b {
			9 => out.push_str("\\t"),
			10 => out.push_str("\\n"),
			13 => out.push_str("\\r"),
			b'\"' => out.push_str("\\\""),
			b'\\' => out.push_str("\\\\"),
			32..=126 => out.push(b as char),
			b => write!(out, "\\x{b:02x}").unwrap()
		}
	}
	out.push_str(r#"""#);
	out
}

/*********************************************************************************************************************/
fn classify_punct(c: char) -> u8
{
	match c {
		'!' => 0,
		'#' => 18,
		//'$' =>
		'%' => 1,
		'&' => 2,
		//'\'' =>
		'*' => 3,
		'+' => 4,
		',' => 5,
		'-' => 6,
		'.' => 17,
		'/' => 7,
		':' => 8,
		';' => 9,
		'<' => 10,
		'=' => 11,
		'>' => 12,
		'?' => 13,
		'@' => 16,
		//'\\' =>
		'^' => 14,
		//'`' =>
		'|' => 15,
		//'~' =>
		_ => 31
	}
}

fn refuse_to_combine(x: char, y: char) -> bool
{
	//80 pairs so far.
	static MAGIC: [u32;32] = [
		0x2FEFF,		// !:  . |^?>=<; /-,+*&%!
		0x02000,		// %:      ?
		0x3FEFB,		// &:  .@|^?>=<; /-,+* %!
		0x2FF7F,		// *:  . |^?>=<;: -,+*&%!		/* is comment start.
		0x02000,		// +:      ?
		0x23008,		// ,:  .   ?>        *
		0x2FEFF,		// -:  . |^?>=<; /-,+*&%!		What is <- ???
		0x02000,		// /:      ?
		0x01400,		// ::       > <
		0x22200,		// ;:  .   ?   ;
		0x02100,		// <:      ?    :
		0x02000,		// =:      ?
		0x02000,		// >:      ?
		0x02000,		// ?:      ?
		0x02000,		// ^:      ?
		0x02000,		// |:      ?
		0x00000,		// @:
		0x00800,		// .:        =
		0x20F25,		// #:  .     =<;:  ,  & !         
		0,0,0,0,0,0,0,0,0,0,0,0,0,		//Not in use.
	];
	let x = classify_punct(x);
	let y = classify_punct(y);
	MAGIC[y as usize & 31] >> (x&31) & 1 != 0
}

/*********************************************************************************************************************/
fn format_code(code: &str) -> String
{
	let mut code2 = tokenize_code(code);
	cleanup_tonization(&mut code2);
	emit_code(&code2)
}

fn emit_code(code: &[String]) -> String
{
	let mut linelen = 0;
	let mut brace_depth = 0usize;
	let mut paren_depth = 0i32;
	let mut last_space = false;
	let mut force_linebreak = false;
	let mut output = String::new();
	for token in code.iter() {
		if linelen > get_max_line_len(brace_depth) || force_linebreak {
			output.push_str("\n");
			for _ in 0..brace_depth { output.push_str("\t"); }
			linelen = 0;
			force_linebreak = false;
			last_space = true;
		}
		if token == "" {
			if !last_space {
				output.push_str(" ");
				linelen += 1;
			}
			last_space = true;
		} else {
			if token == "}" {
				brace_depth -= 1;
				output.push_str("\n");
				for _ in 0..brace_depth { output.push_str("\t"); }
				force_linebreak = true;
			}
			output.push_str(token);
			linelen += token.len();
			last_space = false;
			if token == "{" {
				brace_depth += 1;
				force_linebreak = true;
			} else if token.ends_with(";") && paren_depth == 0 {
				force_linebreak = true;
			} else if token == "(" || token =="[" {
				paren_depth += 1;
			} else if token == "]" || token ==")" {
				paren_depth -= 1;
			}
		}
	}
	output.push_str("\n");
	output
}

fn get_max_line_len(depth: usize) -> usize
{
	if depth <= 5 { 90 - 8 * depth } else { 50 }
}

fn cleanup_tonization(x: &mut Vec<String>)
{
	let mut blacklist: Vec<usize> = Vec::new();
	for i in 0..x.len() {
		if x[i] == "" {
			if i == 0 || i == x.len() - 1 {
				blacklist.push(i);
				continue;
			}
			let prev = x[i-1].clone();
			let next = x[i+1].clone();
			if prev.ends_with("?") && next.starts_with(";") {
				blacklist.push(i);
				continue;
			}
		}
	}
	let mut i = 0;
	let mut r = 0;
	let mut w = 0;
	while r < x.len() {
		if i < blacklist.len() && r == blacklist[i] {
			i += 1;
		} else {
			x[w] = x[r].clone();
			w += 1;
		}
		r += 1;
	}
	x.truncate(w);
}

fn is_ident(c: char) -> bool
{
	//Assume non-ASCII is all idents, even if that is not actually the case.
	if c as u32 >= 128 { return true; }
	//For ASCII, assume idents are alphanumeric and '_'.
	c.is_ascii_alphanumeric() || c == '_'
}

fn is_punct(c: char) -> bool
{
	if classify_punct(c) < 31 { return true; }
	false
}

fn trunc_string(word: &str) -> &str
{
	let mut olen = None;
	for (i,_) in word.char_indices().take(11) { olen = Some(i); }
	let olen = olen.unwrap_or(word.len());
	&word[..olen]
}

fn take_while_predicate<'a>(x: &'a str, predicate: impl Fn(char) -> bool) -> &'a str
{
	for (i,c) in x.char_indices() {
		if !predicate(c) { return &x[..i]; }
	}
	x
}

fn take_string_normal<'a>(word: &'a str, delta: usize) -> Option<usize>
{
	//Normal string.
	let mut itr = None;
	let mut escape = false;
	for (i,c) in word[delta..].char_indices() {
		if c == '\"' && !escape { itr = Some(i); break; }
		// Backslash escapes the next character.
		escape = !escape && c == '\\';
	}
	let itr = itr?;
	Some(itr+delta+1)
}

fn take_string_raw<'a>(word: &'a str, delta: usize) -> Option<usize>
{
	let rest = &word[delta..];
	let hashes = take_while_predicate(rest, |c|c=='#');
	let rest = &rest[hashes.len()..];
	if !rest.starts_with("\"") { return None; }
	let rest = &rest[1..];
	for (i,c) in rest.char_indices() {
		if c == '\"' && rest[i+1..].starts_with(hashes) {
			return Some(i+2*hashes.len()+delta+2);
		}
	}
	None
}

fn take_char<'a>(word: &'a str, delta: usize) -> Option<usize>
{
	let rest = &word[delta..];
	let fchar = rest.chars().next()?;
	if is_ident(fchar) {
		//Check the character after this is '.
		let rest = &rest[fchar.len_utf8()..];
		if !rest.starts_with("\'") { return None; }
		return Some(fchar.len_utf8()+delta+1);
	}
	//Otherwise take to '.
	for (i,c) in rest.char_indices() {
		if c == '\'' { return Some(i+delta+1); }
	}
	None
}

fn take_string<'a>(word: &'a str) -> Option<usize>
{
	if word.starts_with("\"") { return take_string_normal(word, 1); }
	if word.starts_with("b\"") { return take_string_normal(word, 2); }
	if word.starts_with("r") { return take_string_raw(word, 1); }
	if word.starts_with("br") { return take_string_raw(word, 2); }
	if word.starts_with("\'") { return take_char(word, 1); }
	if word.starts_with("b\'") { return take_char(word, 2); }
	None
}

fn consume_next_token2<'a>(code: &'a str) -> usize
{
	//We do not handle comments. Assume doc-comments have been transformed to the #[doc="..."] form.
	if code.starts_with("//") || code.starts_with("/*") {
		panic!("Comments not supported: {code}", code=trunc_string(code));
	}
	//Whitespace.
	if code.starts_with(" ") || code.starts_with("\t") || code.starts_with("\r") || code.starts_with("\n") {
		return 0;
	}
	//Groups.
	if code.starts_with("(") || code.starts_with(")") || code.starts_with("[") || code.starts_with("]") ||
		code.starts_with("{") || code.starts_with("}") {
		return 1;
	}
	if let Some(string) = take_string(code) { return string; }
	//Raw ident.
	if code.starts_with("r#") {
		let ident = take_while_predicate(&code[2..], is_ident);
		if ident != "" { return ident.len() + 2; }
	}
	//Ident.
	let ident = take_while_predicate(code, is_ident);
	if ident != "" { return ident.len(); }
	//Lifetimes.
	if code.starts_with("\'") {
		let name = take_while_predicate(&code[1..], is_ident);
		if name != "" { return name.len() + 1; }
	}
	//Punct.
	let punct = take_while_predicate(code, is_punct);
	if punct != "" {
		//The entiere sequence may not be returned if there is some incompatble sequence.
		let mut prev = None;
		for (i,c) in punct.char_indices() {
			if let Some(prev) = prev {
				if refuse_to_combine(prev, c) { return i; }
			}
			prev = Some(c);
		}
		return punct.len();
	}
	panic!("Unsupported: {code}", code=trunc_string(code));
}

fn tokenize_code(code: &str) -> Vec<String>
{
	let mut output = Vec::new();
	let mut itr = 0;
	while itr < code.len() {
		let tokenl = consume_next_token2(&code[itr..]);
		if tokenl > 0 {
			output.push(code[itr..itr+tokenl].to_string());
			itr += tokenl;
		} else {
			//Assume this is whitespace.
			output.push(String::new());
			itr += 1;
		}
	}
	output
}

#[test]
fn token_consume()
{
	assert!(consume_next_token2(r#####""foo"X"#####) == 5);
	assert!(consume_next_token2(r#####""foo\"bar"X"#####) == 10);
	assert!(consume_next_token2(r#####""foobar\\"X"#####) == 10);
	assert!(consume_next_token2(r#####""foobar\u{1234}"X"#####) == 16);
	assert!(consume_next_token2(r#####"b"foo"X"#####) == 6);
	assert!(consume_next_token2(r#####"b"foo\"bar"X"#####) == 11);
	assert!(consume_next_token2(r#####"b"foobar\\"X"#####) == 11);
	assert!(consume_next_token2(r#####"b"foobar\u{1234}"X"#####) == 17);
	assert!(consume_next_token2(r#####"r##""#"##X"#####) == 9);
	assert!(consume_next_token2(r#####"br##""#"##X"#####) == 10);
	assert!(consume_next_token2(r#####"'x'X"#####) == 3);
	assert!(consume_next_token2(r#####"'\r'X"#####) == 4);
	assert!(consume_next_token2(r#####"b'x'X"#####) == 4);
	assert!(consume_next_token2(r#####"b'\r'X"#####) == 5);
	assert!(consume_next_token2(r#####"'foo#"#####) == 4);
	assert!(consume_next_token2(r#####"&'foo#"#####) == 1);
	assert!(consume_next_token2(r#####"Option<((&'a mut[T],&'a mut[T]),usize)>"#####) == 6);
	assert!(consume_next_token2(r#####"<((&'a mut[T],&'a mut[T]),usize)>"#####) == 1);
	assert!(consume_next_token2(r#####"((&'a mut[T],&'a mut[T]),usize)>"#####) == 1);
	assert!(consume_next_token2(r#####"(&'a mut[T],&'a mut[T]),usize)>"#####) == 1);
	assert!(consume_next_token2(r#####"&'a mut[T],&'a mut[T]),usize)>"#####) == 1);
	assert!(consume_next_token2(r#####"'a mut[T],&'a mut[T]),usize)>"#####) == 2);
	assert!(consume_next_token2(r#####" mut[T],&'a mut[T]),usize)>"#####) == 0);
	assert!(consume_next_token2(r#####"mut[T],&'a mut[T]),usize)>"#####) == 3);
	assert!(consume_next_token2(r#####"[T],&'a mut[T]),usize)>"#####) == 1);
	assert!(consume_next_token2(r#####"T],&'a mut[T]),usize)>"#####) == 1);
	assert!(consume_next_token2(r#####"],&'a mut[T]),usize)>"#####) == 1);
	assert!(consume_next_token2(r#####",&'a mut[T]),usize)>"#####) == 1);
	assert!(consume_next_token2(r#####"&'a mut[T]),usize)>"#####) == 1);
	assert!(consume_next_token2(r#####"'a mut[T]),usize)>"#####) == 2);
	assert!(consume_next_token2(r#####" mut[T]),usize)>"#####) == 0);
	assert!(consume_next_token2(r#####"mut[T]),usize)>"#####) == 3);
	assert!(consume_next_token2(r#####"[T]),usize)>"#####) == 1);
	assert!(consume_next_token2(r#####"T]),usize)>"#####) == 1);
	assert!(consume_next_token2(r#####"]),usize)>"#####) == 1);
	assert!(consume_next_token2(r#####"),usize)>"#####) == 1);
	assert!(consume_next_token2(r#####",usize)>"#####) == 1);
	assert!(consume_next_token2(r#####"usize)>"#####) == 5);
	assert!(consume_next_token2(r#####")>"#####) == 1);
	assert!(consume_next_token2(r#####">"#####) == 1);
	assert!(consume_next_token2(r#####"Some((r#x0,r#x1,r#x2,r#x3))"#####) == 4);
	assert!(consume_next_token2(r#####"((r#x0,r#x1,r#x2,r#x3))"#####) == 1);
	assert!(consume_next_token2(r#####"(r#x0,r#x1,r#x2,r#x3))"#####) == 1);
	assert!(consume_next_token2(r#####"r#x0,r#x1,r#x2,r#x3))"#####) == 4);
	assert!(consume_next_token2(r#####",r#x1,r#x2,r#x3))"#####) == 1);
	assert!(consume_next_token2(r#####"r#x1,r#x2,r#x3))"#####) == 4);
	assert!(consume_next_token2(r#####",r#x2,r#x3))"#####) == 1);
	assert!(consume_next_token2(r#####"r#x2,r#x3))"#####) == 4);
	assert!(consume_next_token2(r#####",r#x3))"#####) == 1);
	assert!(consume_next_token2(r#####"r#x3))"#####) == 4);
	assert!(consume_next_token2(r#####"))"#####) == 1);
	assert!(consume_next_token2(r#####")"#####) == 1);
}
