use btls_aux_proc_macro_tools::Element;
use btls_aux_proc_macro_tools::ElementStream;
use btls_aux_proc_macro_tools::RootElementList;
use btls_aux_proc_macro_tools::output;
use btls_aux_proc_macro_tools::OwnedOutput;
use proc_macro::Ident;
use proc_macro::Literal;
use proc_macro::Span;
use proc_macro::TokenStream;
use std::mem::replace;
use std::collections::BTreeSet;


#[derive(Copy,Clone,PartialEq,Eq,Debug)]
enum TerminalKind
{
	NotSet,
	Group,
	Punct,
	Ident,
}

#[derive(Clone,Debug)]
enum L0Kind
{
	Raw,
	Noarray,
	Mapped(String),
}

#[derive(Clone,Debug)]
enum LinearCodeR
{
	Raw(String, TerminalKind, TerminalKind),
	//noarr flag, mapper.
	L0(String, L0Kind),
	L1(Vec<LinearCodeR>, String),
	Masktype(String),
}

fn set_tkind(s: &mut TerminalKind, e: &mut TerminalKind, k: TerminalKind)
{
	//Only set first kind if unset.
	if *s == TerminalKind::NotSet { *s = k; }
	*e = k;
}

fn flush_code(out: &mut Vec<LinearCodeR>, last: &mut String, ltkind: &mut TerminalKind, tkind: TerminalKind)
{
	if last.len() > 0 {
		let t = replace(last, String::new());
		let t2 = replace(ltkind, TerminalKind::NotSet);
		out.push(LinearCodeR::Raw(t, t2, tkind));
	}
}

fn lower_linear_code(c: &[LinearCode]) -> Vec<LinearCodeR>
{
	use TerminalKind as TK;
	let mut last = String::new();
	let mut ltkind = TerminalKind::NotSet;
	let mut out = Vec::new();
	//The first symbol always combines, so use tkind that combines with everything after it.
	let mut tkind = TerminalKind::Group;
	for e in c.iter() { match e {
		&LinearCode::LeftBrace => { last.push('{'); set_tkind(&mut ltkind, &mut tkind, TK::Group); },
		&LinearCode::RightBrace => { last.push('}'); set_tkind(&mut ltkind, &mut tkind, TK::Group); },
		&LinearCode::LeftBracket => { last.push('['); set_tkind(&mut ltkind, &mut tkind, TK::Group); },
		&LinearCode::RightBracket => { last.push(']'); set_tkind(&mut ltkind, &mut tkind, TK::Group); },
		&LinearCode::LeftParen => { last.push('('); set_tkind(&mut ltkind, &mut tkind, TK::Group); },
		&LinearCode::RightParen => { last.push(')'); set_tkind(&mut ltkind, &mut tkind, TK::Group); },
		&LinearCode::Punct(ref p) => {
			//punct combines with everything before except another punct.
			if tkind == TerminalKind::Punct { last.push(' '); }
			last.push_str(p);
			set_tkind(&mut ltkind, &mut tkind, TK::Punct);
		},
		&LinearCode::Ident(ref p) => {
			//ident combines with everything before except another ident.
			if tkind == TerminalKind::Ident { last.push(' '); }
			last.push_str(p);
			set_tkind(&mut ltkind, &mut tkind, TK::Ident);
		},
		&LinearCode::Lifetime(ref p) => {
			//Lifetimes work like idents.
			if tkind == TerminalKind::Ident { last.push(' '); }
			last.push('\'');
			last.push_str(p);
			set_tkind(&mut ltkind, &mut tkind, TK::Ident);
		},
		&LinearCode::Literal(ref p) => {
			//TODO: Is it correct to treat Literals as having terminal kind ident?
			if tkind == TerminalKind::Ident { last.push(' '); }
			last.push_str(p);
			set_tkind(&mut ltkind, &mut tkind, TK::Ident);
		},
		&LinearCode::L0(ref p, ref kind) => {
			//Need to flush before L0 because stuff gets pasted.
			flush_code(&mut out, &mut last, &mut ltkind, tkind);
			out.push(LinearCodeR::L0(p.to_string(), kind.clone()));
			tkind = TerminalKind::Group;	//Can combine everything afterwards.
		},
		&LinearCode::L1(ref p, ref flag) => {
			//Need to flush before L1 because stuff gets pasted.
			flush_code(&mut out, &mut last, &mut ltkind, tkind);
			let out2 = lower_linear_code(p);
			out.push(LinearCodeR::L1(out2, flag.to_string()));
			tkind = TerminalKind::Group;	//Can combine everything afterwards.
		},
		&LinearCode::Masktype(ref p) => {
			//Need to flush before L0 because stuff gets pasted.
			flush_code(&mut out, &mut last, &mut ltkind, tkind);
			out.push(LinearCodeR::Masktype(p.to_string()));
			tkind = TerminalKind::Group;	//Can combine everything afterwards.
		},
	}}
	//If there is remainder, push it as raw. As normally only L0/L1 triggers a flush.
	flush_code(&mut out, &mut last, &mut ltkind, tkind);
	out
}


#[derive(Clone,Debug)]
enum LinearCode
{
	LeftBrace,
	RightBrace,
	LeftBracket,
	RightBracket,
	LeftParen,
	RightParen,
	Punct(String),
	Ident(String),
	Lifetime(String),
	Literal(String),
	L0(String, L0Kind),
	L1(Vec<LinearCode>, String),
	Masktype(String),
}

fn push_linear_elements(mut s: ElementStream, root: &RootElementList, l: &mut Vec<LinearCode>,
	inh_base: Option<String>, is_l1: bool)
{
	let mut was_hash = false;
	let mut was_hashbang = false;
	let mut base: Option<String> = inh_base;
	let mut first = true;
	loop {
		let hash = replace(&mut was_hash, false);
		let hashbang = replace(&mut was_hashbang, false);
		match s.next() {
			Element::Bracket(ref g) if first && is_l1 => {
				//First token on L1 being bracket is used to set the base.
				let mut sub = g.to_stream();
				match sub.next() {
					Element::Ident(ref g) => {
						if let Some(id) = g.as_identifier() {
							base = Some(id.to_string());
						} else {
							root.panic(g, "Unexpected keyword")
						}
					},
					ref g => root.panic(g, "Expected ident")
				};
				//sub better end here.
				let end = sub.next();
				if !end.is_end() { root.panic(&end, "Expected end of group") }
			},
			Element::Bracket(ref g) => {
				//If this appears in hash, the previous hash must not be squashed.
				if hash { l.push(LinearCode::Punct("#".to_string())); }
				if hashbang { l.push(LinearCode::Punct("#!".to_string())); }
				l.push(LinearCode::LeftBracket);
				push_linear_elements(g.to_stream(), root, l, base.clone(), false);
				l.push(LinearCode::RightBracket);
			},
			Element::Paren(ref g) if hash => {
				let mut l1 = Vec::new();
				//Do not inherit the base, since this starts new L1 block.
				push_linear_elements(g.to_stream(), root, &mut l1, None, true);
				let mut comma = String::new();
				// * to end the L1.
				match s.next() {
					Element::Punct(ref h) if h.is_punct("*") => (),
					Element::Punct(ref h) => {
						comma = h.get_punct().to_string();
						match s.next() {
							Element::Punct(ref h) if h.is_punct("*") => (),
							ref h => root.panic(h, "Not allowed after L1 paren")
						}
					},
					ref h => root.panic(h, "Not allowed after L1 paren")
				}
				l.push(LinearCode::L1(l1, comma));
			}
			//Only identifiers are allowed for idents, not keywords.
			Element::Ident(ref g) if hash => if let Some(id) = g.as_identifier() {
				//If base is set, this becomes a map.
				if let Some(base) = base.as_ref() {
					l.push(LinearCode::L0(base.clone(), L0Kind::Mapped(id.to_string())));
				} else {
					l.push(LinearCode::L0(id.to_string(), L0Kind::Raw));
				};
			} else {
				root.panic(g, "Not allowed after #");
			}
			Element::Ident(ref g) if hashbang && g.is_identifier("MASKTYPE") => {
				let ident = s.expect_identifier(root);
				l.push(LinearCode::Masktype(ident));
			},
			Element::Ident(ref g) if hashbang && g.is_identifier("REPEAT") => {
				let ident = s.expect_identifier(root);
				l.push(LinearCode::L0(ident, L0Kind::Noarray));
			},
			ref g if hash => root.panic(g, "Not allowed after #"),
			ref g if hashbang => root.panic(g, "Not allowed after #!"),
			Element::Brace(ref g) => {
				l.push(LinearCode::LeftBrace);
				push_linear_elements(g.to_stream(), root, l, base.clone(), false);
				l.push(LinearCode::RightBrace);
			},
			Element::Paren(ref g) => {
				l.push(LinearCode::LeftParen);
				push_linear_elements(g.to_stream(), root, l, base.clone(), false);
				l.push(LinearCode::RightParen);
			},
			//Can not deal with this!
			Element::GroupNone(ref g) => root.panic(g, "GroupNone not supported"),
			Element::Ident(ref g) => l.push(LinearCode::Ident(g.to_string())),
			Element::Lifetime(ref g) => l.push(LinearCode::Lifetime(g.to_string())),
			Element::Literal(ref g) => l.push(LinearCode::Literal(g.to_string())),
			Element::Punct(ref g) if g.is_punct("#") => was_hash = true,
			Element::Punct(ref g) if g.is_punct("#!") => was_hashbang = true,
			Element::Punct(ref g) => {
				let h = g.get_punct();
				// # is not supposed to combine with anything before.
				if h.contains("#") {
					println!("BAD PUNCT: {h}");
				}
				assert!(!h.contains("#"));
				l.push(LinearCode::Punct(h.to_string()));
			},
			Element::End(_) => break	//No more with this.
		}
		first = false;
	}
}

fn element_list_to_linear_code(s: &RootElementList) -> Vec<LinearCode>
{
	let mut l = Vec::new();
	//There is no base to be inherited.
	push_linear_elements(s.to_stream(), s, &mut l, None, false);
	l
}

fn lower_terminal_kind(t: TerminalKind) -> OwnedOutput
{
	match t {
		TerminalKind::NotSet => panic!("TerminalKind::NotSet"),
		TerminalKind::Group => output!("::btls_aux_codegen::TerminalKind::Group"),
		TerminalKind::Punct => output!("::btls_aux_codegen::TerminalKind::Punct"),
		TerminalKind::Ident => output!("::btls_aux_codegen::TerminalKind::Ident"),
	}
}

fn do_l1(instructions2: &mut Vec<OwnedOutput>, code: &[LinearCodeR], comma: &str)
{
	let mut instructions: Vec<OwnedOutput> = Vec::new();
	let mut l0set = BTreeSet::new();
	for e in code.iter() { match e {
		//Ignore raw and Noarray L0, as those do not induce an array but stay constant.
		&LinearCodeR::Raw(_,_,_) => (),			//Ignore raw.
		&LinearCodeR::L0(_, L0Kind::Noarray) => (),	//Ignore noarr L0.
		&LinearCodeR::L0(ref x, _) => {l0set.insert(x.to_string());},
		&LinearCodeR::L1(_,_) => panic!("Nested L1 not allowed"),
		&LinearCodeR::Masktype(_) => panic!("Masktype not allowed in L1"),
	}}
	let first_l0 = l0set.iter().next().expect("L1 with no L0 inside");
	output!(TO[instructions] "let __wip_code_count: usize = ", Ident::new_raw(first_l0, Span::call_site()),
		".len();");
	for l0 in l0set.iter() {
		output!(TO[instructions] "let __wip_code_xcount: usize = ", Ident::new_raw(l0, Span::call_site()),
			".len();");
		output!(TO[instructions] "if __wip_code_count != __wip_code_xcount",
			output!({} "panic!", output!(()
				Literal::string("Mismatch lengths of {}({}), {}({})"),",",
				Literal::string(first_l0),",__wip_code_count,",
				Literal::string(l0),",__wip_code_xcount"
			))
		);
	}
	let mut ins_body: Vec<OwnedOutput> = Vec::new();
	if comma.len() > 0 {
		output!(TO[ins_body] "if __wip_code_itr > 0", output!({}
			"__wip_code.push_comma",output!(() Literal::string(comma)),";"
		));
	}
	let mkid = |sym|Ident::new_raw(sym, Span::call_site());
	for e in code.iter() { match e {
		&LinearCodeR::Raw(ref frag, st, et) => {
			let st = lower_terminal_kind(st);
			let et = lower_terminal_kind(et);
			output!(TO[ins_body] "__wip_code.push_raw", output!(()
				Literal::string(frag), ",", st, ",", et
			),";");
		},
		&LinearCodeR::L0(ref sym, L0Kind::Noarray) => {
			output!(TO[ins_body] "__wip_code.push_l0", output!(()
				"&", mkid(sym)
			),";");
		},
		&LinearCodeR::L0(ref sym, L0Kind::Raw) => {
			output!(TO[ins_body] "__wip_code.push_l0", output!(()
				"&", mkid(sym), output!([] "__wip_code_itr")
			),";");
		},
		&LinearCodeR::L0(ref sym, L0Kind::Mapped(ref map)) => {
			output!(TO[ins_body] "__wip_code.push_l0", output!(()
				"&", mkid(sym), output!([] "__wip_code_itr"), ".", mkid(map), "()"
			),";");
		},
		//Only raw or L0 is possible here, the check for that is at top of this function.
		_ => unreachable!(),
	}}
	output!(TO[instructions] "for __wip_code_itr in 0..__wip_code_count", output!({} ins_body));
	instructions2.extend_from_slice(&instructions);
}

#[proc_macro]
pub fn quote(s: TokenStream) -> TokenStream
{
	let s = RootElementList::new(s);
	let l = element_list_to_linear_code(&s);
	let l = lower_linear_code(&l);
	//println!("linear code R: {l:?}");
	let mut instructions: Vec<OwnedOutput> = Vec::new();
	output!(TO[instructions] "let mut __wip_code = btls_aux_codegen::WIPCodeBlock::new();");
	for op in l.iter() { match op {
		&LinearCodeR::Raw(ref frag, st, et) => {
			let st = lower_terminal_kind(st);
			let et = lower_terminal_kind(et);
			output!(TO[instructions] "__wip_code.push_raw", output!(()
				Literal::string(frag), ",", st, ",", et
			),";");
		},
		&LinearCodeR::L0(ref sym,_) => {
			output!(TO[instructions] "__wip_code.push_l0", output!(()
				"&", Ident::new_raw(sym, Span::call_site())
			),";");
		},
		&LinearCodeR::L1(ref code, ref comma) => do_l1(&mut instructions, code, comma),
		&LinearCodeR::Masktype(ref sym) => {
			output!(TO[instructions] "__wip_code.push_masktype", output!(()
				"&", Ident::new_raw(sym, Span::call_site())
			),";");
		},
	}}
	output!(TO[instructions] "__wip_code.finalize()");
	//println!("{instructions:?}");
	output!({} instructions).to_token_stream()
}
