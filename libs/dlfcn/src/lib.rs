//!This crate contains routines to load dynamic libraries.
//Can't deny missing_docs due to rustc bug where this lint can not be overridden for macros.
//#![deny(missing_docs)]
#![allow(unsafe_code)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]
#[cfg(any(feature="std",test))] #[macro_use] extern crate std;
use btls_aux_collections::Arc;
use btls_aux_collections::BTreeMap;
use btls_aux_collections::GlobalMutex;
use btls_aux_collections::String;
use btls_aux_collections::ToOwned;
use btls_aux_collections::ToString;
use btls_aux_collections::Vec;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_filename::Filename;
use btls_aux_filename::FilenameBuf;
use btls_aux_memory::EscapeByteString;
use core::any::type_name;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::mem::size_of;
use core::mem::transmute_copy;
#[cfg(feature="std")] pub use std::io::Error as StdIoError;
#[cfg(feature="std")] pub use std::io::ErrorKind as StdIoErrorKind;
#[cfg(feature="std")] use std::path::Path;

///Type of error.
#[non_exhaustive]
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum ErrorKind
{
	///Invalid input.
	InvalidInput,
	///File not found.
	NotFound,
	///Unsupported operation.
	Unsupported,
	///Out of memory.
	OutOfMemory,
	///Invalid filename.
	InvalidFilename,
	///Invalid data.
	InvalidData,
	///Other error.
	Other,
}

#[derive(Clone,Debug)]
enum ErrorMessage
{
	OutOfMemory,
	NullSymbol,
	Unreachable,
	InvalidPath,
	InvalidSymbol,
	NoDlfcn,
	IllegalType(&'static str),
	BuiltinNotFound(String),
	SymbolNotFound(String),
	Custom(String),
}

impl Display for ErrorMessage
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&ErrorMessage::OutOfMemory => f.write_str("Out of Memory"),
			&ErrorMessage::NullSymbol => f.write_str("Symbol is NULL"),
			&ErrorMessage::Unreachable => f.write_str("Internal error: Unreachable code"),
			&ErrorMessage::InvalidPath => f.write_str("Invalid path"),
			&ErrorMessage::InvalidSymbol => f.write_str("Invalid symbol"),
			&ErrorMessage::NoDlfcn => f.write_str("Dynamic libraries not supported"),
			&ErrorMessage::IllegalType(s) => write!(f, "Illegal symbol type {s}"),
			&ErrorMessage::BuiltinNotFound(ref s) => write!(f, "No builtin library {s}"),
			&ErrorMessage::SymbolNotFound(ref s) => write!(f, "No such symbol {s}"),
			&ErrorMessage::Custom(ref s) => f.write_str(s),
		}

	}
}

///Error.
#[derive(Clone,Debug)]
pub struct Error(ErrorKind, ErrorMessage);

impl Error
{
	///Get kind of error.
	pub fn kind(&self) -> ErrorKind { self.0 }
}

impl Display for Error
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> { Display::fmt(&self.1, f) }
}

pub use inner::__dummy_error_symbols;

#[cfg(not(unix))]
mod inner
{
	#[doc(hidden)]
	pub fn __dummy_error_symbols() -> Option<Error>
	{
		if false { return Some(Error(ErrorKind::Other, ErrorMessage::InvalidPath)); }
		if false { return Some(Error(ErrorKind::Other, ErrorMessage::InvalidSymbol)); }
		if false { return Some(Error(ErrorKind::Other, ErrorMessage::Custom(String::new()))); }
		None
	}

	use btls_aux_collections::String;
	use btls_aux_filename::Filename;
	use super::Error;
	use super::ErrorKind;
	use super::ErrorMessage;
	use super::Symbol;
	//This type can never be created.
	#[derive(Debug)]
	pub enum LibraryHandle {}
	impl LibraryHandle
	{
		pub fn new(_: &Filename) -> Result<LibraryHandle, Error>
		{
			Err(Error(ErrorKind::Unsupported, ErrorMessage::NoDlfcn))
		}
		#[allow(dead_code)]	//Here to make code compile, can never be called.
		pub fn load(&self, _: &[u8]) -> Result<Symbol, Error>
		{
			//This function is uncallable.
			unreachable!();
		}
	}
}

#[cfg(unix)]
mod inner
{
	#[doc(hidden)]
	pub fn __dummy_error_symbols() -> Option<Error>
	{
		if false { return Some(Error(ErrorKind::Other, ErrorMessage::Unreachable)); }
		if false { return Some(Error(ErrorKind::Other, ErrorMessage::NoDlfcn)); }
		None
	}

	use btls_aux_collections::ToString;
	use btls_aux_filename::Filename;
	use btls_aux_unix::Path as UnixPath;
	use btls_aux_unix::DlHandle;
	use btls_aux_unix::DlopenFlags;
	use super::Error;
	use super::ErrorKind;
	use super::ErrorMessage;
	use super::Symbol;

	#[derive(Debug)]
	pub struct LibraryHandle(DlHandle);
	//Oh, LibraryHandle is thread-safe.
	unsafe impl Send for LibraryHandle {}
	unsafe impl Sync for LibraryHandle {}
	
	impl LibraryHandle
	{
		pub fn new(path: &Filename) -> Result<LibraryHandle, Error>
		{
			let path = path.into_unix_path().
				ok_or(Error(ErrorKind::InvalidFilename, ErrorMessage::InvalidPath))?;
			unsafe{
				path.dlopen(DlopenFlags::NOW).map(LibraryHandle).map_err(|e|{
					Error(ErrorKind::Other, ErrorMessage::Custom(e.to_string()))
				})
			}
		}
		pub fn load(&self, symbol: &[u8]) -> Result<Symbol, Error>
		{
			let path = UnixPath::new(symbol).
				ok_or(Error(ErrorKind::InvalidFilename, ErrorMessage::InvalidSymbol))?;
			unsafe {
				path.dlsym(&self.0).map(|s|Symbol(s.cast())).map_err(|e|{
					Error(ErrorKind::NotFound, ErrorMessage::Custom(e.to_string()))
				})
			}
		}
	}
}

///Symbol.
#[derive(Copy,Clone,Debug)]
pub struct Symbol(*mut u8);
//Oh, Symbol is thread-safe.
unsafe impl Send for Symbol {}
unsafe impl Sync for Symbol {}

impl Symbol
{
	///Construct a symbol from pointer-like type `T`.
	///
	///Pointer-like types include all data and funcion pointer types (as well as `Option<function-pointer>`).
	///
	///This is Undefined Behavior if `T` does not have the same size as a pointer.
	pub unsafe fn new<T>(x: T) -> Symbol
	{
		let a = size_of::<T>();
		let b = size_of::<*mut u8>();
		if a != b { panic!("Symbol::new(): Bad symbol size {a}, expected {b}"); }
		//Use transmute_copy instead of transmute, because transmute can not handle dependently sized
		//types. Use explicit types in order to assert this function is doing the right transmute.
		Symbol(unsafe{transmute_copy::<T, *mut u8>(&x)})
	}
	///Cast a symbol to pointer-like type `T`.
	///
	///Pointer-like types include all data and funcion pointer types (as well as `Option<function-pointer>`).
	///
	///This is Undefined Behavior if `T` does not have the same size as a pointer. It is also Undefined
	///Behavor to use symbol of wrong type.
	pub unsafe fn to<T>(&self) -> T
	{
		//Use transmute_copy instead of transmute, because transmute can not handle dependently sized
		//types. Use explicit types in order to assert this function is doing the right transmute.
		unsafe{transmute_copy::<*mut u8, T>(&self.0)}
	}
	///Is NULL symbol?
	pub fn is_null(&self) -> bool { self.0.is_null() }
}

#[derive(Debug)]
enum _LibraryHandle2
{
	Dynamic(inner::LibraryHandle),
	Static(BTreeMap<Vec<u8>, Symbol>),
}

impl _LibraryHandle2
{
	fn load(&self, symbol: &[u8]) -> Result<Symbol, Error>
	{
		match self {
			#[cfg(unix)]
			&_LibraryHandle2::Dynamic(ref lib) => lib.load(symbol),
			#[cfg(not(unix))] 
			&_LibraryHandle2::Dynamic(_) => Err(Error(ErrorKind::Other, ErrorMessage::Unreachable)),
			&_LibraryHandle2::Static(ref lib) => match lib.get(symbol) {
				Some(x) => Ok(*x),
				None => {
					let y = EscapeByteString(symbol).to_string();
					Err(Error(ErrorKind::NotFound, ErrorMessage::SymbolNotFound(y)))
				}
			}
		}
	}
}

static LOADED_PATHS: GlobalMutex<BTreeMap<FilenameBuf, Arc<_LibraryHandle2>>> = GlobalMutex::new();


#[cfg(feature="std")]
fn convert_kind(x: ErrorKind) -> StdIoErrorKind
{
	match x {
		ErrorKind::InvalidInput => StdIoErrorKind::InvalidInput,
		ErrorKind::NotFound => StdIoErrorKind::NotFound,
		ErrorKind::InvalidData => StdIoErrorKind::InvalidData,
		ErrorKind::Other => StdIoErrorKind::Other,
		//These three are not in Rust 1.51.
		ErrorKind::InvalidFilename => StdIoErrorKind::Other,
		ErrorKind::Unsupported => StdIoErrorKind::Other,
		ErrorKind::OutOfMemory => StdIoErrorKind::Other,
	}
}

#[cfg(feature="std")]
fn convert_error<T:Sized>(x: Result<T,Error>) -> Result<T,StdIoError>
{
	x.map_err(|e|StdIoError::new(convert_kind(e.kind()), e.1.to_string()))
}

fn is_builtin_lib(path: Option<&str>) -> bool
{
	match path {
		Some(path) => path.starts_with("$builtin/"),
		None => false		//Non-UTF-8 paths are never builtin.
	}
}

///A handle to loaded library.
#[derive(Debug)]
pub struct LibraryHandle(Arc<_LibraryHandle2>);

impl LibraryHandle
{
	///Load a library.
	#[cfg(feature="std")]
	pub fn new(path: &Path) -> Result<LibraryHandle, StdIoError>
	{
		//Don't canonicalize builtin paths.
		let is_builtin = is_builtin_lib(path.to_str());
		let path = if is_builtin { path.to_path_buf() } else { path.canonicalize()? };
		convert_error(Self::new2(Filename::from_path(&path)))
	}
	///Load a library.
	#[allow(unused_mut)]	//Happens on non-Unix.
	pub fn new2(path: &Filename) -> Result<LibraryHandle, Error>
	{
		let is_builtin = is_builtin_lib(path.into_str());
		let mut path2: Option<FilenameBuf> = None;
		//If calling new(), the canonicalize should have placed / here. If path is relative and not
		//builtin, prepend cwd on Unix.
		#[cfg(unix)] { if !is_builtin && !path.starts_with("/") {
			let cwd = btls_aux_unix::getcwd().
				ok_or(Error(ErrorKind::OutOfMemory, ErrorMessage::OutOfMemory))?;
			let mut path3 = Filename::from_bytes(&cwd).to_owned();
			path3.push("/");
			path3.push_fname(path);
			path2 = Some(path3);
		}}
		let path2 = path2.unwrap_or_else(||path.to_owned());
		LOADED_PATHS.with(|library_lock|{
			if let Some(lib) = library_lock.get(&path2) { return Ok(LibraryHandle(lib.clone())); }
			//Not found, need to load the library. Except don't load builtin libraries.
			if is_builtin {
				let y = path.to_string();
				fail!(Error(ErrorKind::NotFound, ErrorMessage::BuiltinNotFound(y)));
			}
			let lib = Arc::new(_LibraryHandle2::Dynamic(inner::LibraryHandle::new(&path)?));
			//Insert the loaded library to map and return handle.
			library_lock.insert(path2, lib.clone());
			return Ok(LibraryHandle(lib));
		})
	}
	///Create a builtin library.
	pub fn new_builtin2(name: &str, symbols: &[(&str, Symbol)])
	{
		let mut symbols2 = BTreeMap::new();
		for &(name, value) in symbols.iter() { symbols2.insert(name.as_bytes().to_owned(), value); }
		Self::__new_builtin_common(name, symbols2);
	}
	fn __new_builtin_common(name: &str, symbols2: BTreeMap<Vec<u8>, Symbol>)
	{
		let mut path = "$builtin/".to_owned();
		path.push_str(name);
		let path = FilenameBuf::from_string(path);
		LOADED_PATHS.with(|library_lock|{
			//Construct new handle and insert to map.
			let lib = Arc::new(_LibraryHandle2::Static(symbols2));
			library_lock.insert(path, lib.clone());
		})
	}
	///Load a symbol from library.
	#[cfg(feature="std")]
	pub fn load2(&self, symbol: &[u8]) -> Result<Symbol, StdIoError>
	{
		convert_error(self.load3(symbol))
	}
	///Load a symbol from library.
	pub fn load3(&self, symbol: &[u8]) -> Result<Symbol, Error> { self.0.load(symbol) }
	///Load typed symbol from library.
	///
	///Note that this is Undefined Behavior unless T has the same size as a pointer (e.g., is a data pointer or
	///function pointer).
	pub unsafe fn load_typed<T>(&self, symbol: &[u8]) -> Result<T, Error>
	{
		//Check T is legal.
		let a = size_of::<T>();
		let b = size_of::<*mut u8>();
		fail_if!(a != b, Error(ErrorKind::InvalidInput, ErrorMessage::IllegalType(type_name::<T>())));

		let sym = self.load3(symbol)?;
		//Don't UB if symbol is NULL.
		fail_if!(sym.is_null(), Error(ErrorKind::InvalidData, ErrorMessage::NullSymbol));
		//The symbol is assumed to be of type T.
		Ok(unsafe{sym.to::<T>()})
	}
}

#[cfg(feature="std")]
impl LibraryHandle
{
	#[doc(hidden)]
	pub unsafe fn __load_typed<T>(&self, symbol: &[u8]) -> Result<T, StdIoError>
	{
		let sym = self.load2(symbol)?;
		//Don't UB if symbol is NULL.
		fail_if!(sym.is_null(), StdIoError::new(StdIoErrorKind::Other, "Symbol is NULL"));
		//The symbol is assumed to be of type T.
		Ok(unsafe{sym.to::<T>()})
	}
}

#[cfg(not(feature="std"))]
impl LibraryHandle
{
	#[doc(hidden)]
	pub unsafe fn __load_typed<T>(&self, symbol: &[u8]) -> Result<T, Error>
	{
		let sym = self.load3(symbol)?;
		//Don't UB if symbol is NULL.
		fail_if!(sym.is_null(), Error(ErrorKind::InvalidData, ErrorMessage::NullSymbol));
		//The symbol is assumed to be of type T.
		Ok(unsafe{sym.to::<T>()})
	}
}

#[deprecated(since="1.3.0", note="Use Library::load_typed::<T> instead")]
#[macro_export]
macro_rules! typed_load_symbol
{
	([$xtype:ty] $lib:ident $name:expr) => { $lib.__load_typed::<$xtype>($name) }
}
