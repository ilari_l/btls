import sys;
import copy;

class explicit_register:
	def __init__(self, name):
		self.name = name;
	def deref(self, *args):
		return memory_reference.create(self, *args);
	def as_string(self):
		return "%%" + self.name;
class implicit_register:
	def __init__(self, name, *args):
		if len(args) >= 1:
			self.cast = args[0];
		else:
			self.cast = "";
		if type(name) is int:
			self.numeric = True;
			self.number = name;
		elif type(name) is str:
			self.numeric = False;
			self.name = name;
		else:
			raise ValueError("Implicit register must be numeric or string");
	def deref(self, *args):
		return memory_reference.create(self, *args);
	def as_string(self):
		if self.numeric:
			return "%"+self.cast+str(self.number);
		else:
			return "%"+self.cast+"["+str(self.name)+"]";

RCX = explicit_register("rcx");
RBP = explicit_register("rbp");
RSP = explicit_register("rsp");
RSI = explicit_register("rsi");
RDI = explicit_register("rdi");
XMM0 = explicit_register("xmm0");
XMM1 = explicit_register("xmm1");
XMM2 = explicit_register("xmm2");
XMM3 = explicit_register("xmm3");
XMM4 = explicit_register("xmm4");
XMM5 = explicit_register("xmm5");
XMM6 = explicit_register("xmm6");
XMM7 = explicit_register("xmm7");
XMM8 = explicit_register("xmm8");
XMM9 = explicit_register("xmm9");
XMM10 = explicit_register("xmm10");
XMM11 = explicit_register("xmm11");
XMM12 = explicit_register("xmm12");
XMM13 = explicit_register("xmm13");
XMM14 = explicit_register("xmm14");
XMM15 = explicit_register("xmm15");
XMM = [XMM0, XMM1, XMM2, XMM3, XMM4, XMM5, XMM6, XMM7, XMM8, XMM9, XMM10, XMM11, XMM12, XMM13, XMM14, XMM15]

def is_register(obj):
	return (type(obj) is explicit_register) or (type(obj) is implicit_register);

class memory_reference:
	def __init__(self):
		pass
	@classmethod
	def create(*args):
		ref = memory_reference();
		ref.multiply = 1;
		if len(args) == 2:
			ref.count = 1;
			ref.a = args[1];
			ref.offset = 0;
		elif len(args) == 3 and is_register(args[2]):
			ref.count = 2;
			ref.a = args[1];
			ref.b = args[2];
			ref.offset = 0;
		elif len(args) == 3:
			ref.count = 1;
			ref.a = args[1];
			ref.offset = args[2];
		elif len(args) == 4:
			ref.count = 2;
			ref.a = args[1];
			ref.b = args[2];
			ref.offset = args[3];
		else:
			raise ValueError("Bad argument count");
		if not is_register(ref.a): raise ValueError("Bad register as first argument");
		if ref.count > 1 and  not is_register(ref.b): raise ValueError("Bad register as second argument");
		if type(ref.offset) is not int: raise ValueError("Bad offset");
		return ref;
	def adjust(self, amount):
		scopy = copy.deepcopy(self);
		scopy.offset += amount;
		return scopy;
	def as_string(self):
		if self.count == 1:
			return str(self.offset) + "(" + self.a.as_string() + ")";
		elif self.count == 2:
			return str(self.offset) + "(" + self.a.as_string() + "," + self.b.as_string() + ", " + str(self.multiply) + ")";
		else:
			raise ValueError("Bad operand");

def argument_as_string(obj):
	if type(obj) is int:
		return "$"+str(obj);
	elif (type(obj) is explicit_register) or (type(obj) is implicit_register) or (type(obj) is memory_reference):
		return obj.as_string();
	else:
		raise ValueError("Bad operand");

class assembler:
	def __init__(self, stream):
		self.stream = stream;
	def instruction(self, name, *args):
		s = "\t\t\"\\t"+name;
		for i in range(0, len(args)):
			if i == 0:
				s += " ";
			else:
				s += ", ";
			s += argument_as_string(args[i]);
		s += "\\n\"";
		print(s, file=self.stream);
	def jump(self, op, name):
		print("\t\t\"\\t"+op+" "+name+"\\n\"", file=self.stream);
	def label(self, name):
		print("\""+name+":\"", file=self.stream);
	def add(self, a, b): self.instruction("add", a, b);
	def aesenc(self, a, b): self.instruction("aesenc", a, b);
	def aesenclast(self, a, b): self.instruction("aesenclast", a, b);
	def aeskeygenassist(self, a, b, c): self.instruction("aeskeygenassist", a, b, c);
	def bswap(self, a): self.instruction("bswap", a);
	def _cmp(self, a, b): self.instruction("cmp", a, b);
	def dec(self, a): self.instruction("dec", a);
	def inc(self, a): self.instruction("inc", a);
	def jb(self, t): self.jump("jb", t);
	def jmp(self, t): self.jump("jmp", t);
	def jz(self, t): self.jump("jz", t);
	def lea(self, a, b): self.instruction("lea", a, b);
	def mov(self, a, b): self.instruction("mov", a, b);
	def movd(self, a, b): self.instruction("movd", a, b);
	def movdqa(self, a, b): self.instruction("movdqa", a, b);
	def movdqu(self, a, b): self.instruction("movdqu", a, b);
	def _or(self, a, b): self.instruction("or", a, b);
	def pand(self, a, b): self.instruction("pand", a, b);
	def pclmulqdq(self, a, b, c): self.instruction("pclmulqdq", a, b, c);
	def pop(self, a): self.instruction("pop", a);
	def por(self, a, b): self.instruction("por", a, b);
	def pshufd(self, a, b, c): self.instruction("pshufd", a, b, c);
	def pslldq(self, a, b): self.instruction("pslldq", a, b);
	def psrldq(self, a, b): self.instruction("psrldq", a, b);
	def psllq(self, a, b): self.instruction("psllq", a, b);
	def psrlq(self, a, b): self.instruction("psrlq", a, b);
	def push(self, a): self.instruction("push", a);
	def pxor(self, a, b): self.instruction("pxor", a, b);
	def shl(self, a, b): self.instruction("shl", a, b);
	def shr(self, a, b): self.instruction("shr", a, b);
	def sub(self, a, b): self.instruction("sub", a, b);
	def test(self, a, b): self.instruction("test", a, b);
	def xor(self, a, b): self.instruction("xor", a, b);

def allocate_stack_space(asm, size):
	asm.sub(512, RSP);			#Skip past any redzone.
	asm.push(RBP);				#Allocate aligned scratch space on heap.
	asm.sub(size, RSP);
	asm.mov(RSP, RBP);
	asm._or(15, RBP);
	asm.inc(RBP);

def deallocate_stack_space(asm, size):
	asm.add(size, RSP);
	asm.pop(RBP);
	asm.add(512, RSP);

#Perform a round of swapbytes operation. The source/destination is srctgt, and x1-x3 are used as scratch registers.
#s1/s2 are scalar 32-bit scratch registers. c1 and c2 are the dword mask constants, and a is amount to shift.
def swapbytes_round(asm, srctgt, x1, x2, x3, s1, s2, c1, c2, a):
	#Load masks into xmm1 and xmm3
	asm.mov(c1, s1)
	asm.mov(c2, s2)
	asm.movd(s1, x1)
	asm.movd(s2, x3)
	asm.pshufd(0, x1, x1)
	asm.pshufd(0, x3, x3)
	#xmm0 <- xmm0 << a, xmm2 <- xmm0 >> a
	asm.movdqa(srctgt, x2)
	asm.psllq(a, srctgt)
	asm.psrlq(a, x2)
	#Mask the bits that should not be there.
	asm.pand(x1, srctgt)
	asm.pand(x3, x2)
	#Can combine by OR, because masks are disjoint.
	asm.por(x2, srctgt)

#Bit reflect bits in each byte. The source/destination is srctgt, and x1-x3 are used as scratch registers.
#s1/s2 are scalar 32-bit scratch registers.
def swapbytes(asm, srctgt, x1, x2, x3, s1, s2):
	swapbytes_round(asm, srctgt, x1, x2, x3, s1, s2, 0xF0F0F0F0, 0x0F0F0F0F, 4);
	swapbytes_round(asm, srctgt, x1, x2, x3, s1, s2, 0xCCCCCCCC, 0x33333333, 2);
	swapbytes_round(asm, srctgt, x1, x2, x3, s1, s2, 0xAAAAAAAA, 0x55555555, 1);

#Generate carryless multiply. inout is input/output, in_b is input, x1-x3 are scratch, s1/s2 are 32-bit scratch.
def clmul(asm, inout, in_b, x1, x2, x3, s1, s2):
	asm.movdqa(inout, x1);
	asm.movdqa(inout, x2);
	asm.movdqa(inout, x3);
	asm.pclmulqdq(0, in_b, inout);
	asm.pclmulqdq(1, in_b, x1);
	asm.pclmulqdq(16, in_b, x2);
	asm.pclmulqdq(17, in_b, x3);
	asm.pxor(x2, x1);
	asm.movdqa(x1, x2);
	asm.pslldq(8, x1);
	asm.psrldq(8, x2);
	asm.pxor(x1, inout);
	asm.pxor(x2, x3);
	asm.pxor(x3, inout);
	asm.movdqa(x3, x2)
	asm.psllq(1, x2)
	asm.pxor(x2, inout)
	asm.psllq(1, x2)
	asm.pxor(x2, inout)
	asm.psllq(5, x2)
	asm.pxor(x2, inout)
	asm.movdqa(x3, x2)
	asm.pslldq(8, x2)
	asm.psrlq(57, x2)
	asm.pxor(x2, inout)
	asm.psrlq(5, x2)
	asm.pxor(x2, inout)
	asm.psrlq(1, x2)
	asm.pxor(x2, inout)
	asm.psrldq(12, x3)
	asm.movd(x3, s1)
	asm.shr(25, s1)
	asm.mov(s1, s2)
	asm.shr(5, s1)
	asm.xor(s1, s2)
	asm.shr(1, s1)
	asm.xor(s1, s2)
	asm.mov(s2, s1)
	asm.shl(1, s2)
	asm.xor(s2, s1)
	asm.shl(1, s2)
	asm.xor(s2, s1)
	asm.shl(5, s2)
	asm.xor(s2, s1)
	asm.movd(s1, x1)
	asm.pxor(x1, inout)

#Complute state = (state + refl(incoming)) * multiplier. multiplier is not modified. incoming and x0-x2 are scratch
#registers. s1/s2 are 32-bit scratch registers.
def clmul_reflxor(asm, state, multiplier, incoming, x0, x1, x2, s1, s2):
	#Swap the incoming bytes and XOR into state.
	swapbytes(asm, incoming, x0, x1, x2, s1, s2);
	asm.pxor(incoming, state);
	clmul(asm, state, multiplier, x0, x1, x2, s1, s2);

def gcm_accumulate_blocks2(asm, state, multiplier, ptr, blocks, x0, x1, x2, x3, s1, s2):
	asm.label("L_%=_1");
	asm._cmp(1, blocks);
	asm.jb("L_%=_2");
	#Load word.
	asm.movdqu(ptr.deref(), x3);
	clmul_reflxor(asm, state, multiplier, x3, x0, x1, x2, s1, s2);
	#Loop.
	asm.dec(blocks);
	asm.add(16, ptr);
	asm.jmp("L_%=_1");
	asm.label("L_%=_2");

def _aes_encrypt_pass_1(asm, out, bcount, nonce, blocks, lanes):
	#Load nonces into XMM0...XMM7
	for i in range(0,lanes):
		asm.bswap(bcount);
		asm.mov(bcount, nonce.deref(12));
		asm.bswap(bcount);
		asm.movdqa(nonce.deref(), XMM[i]);
		asm.inc(bcount);
	#Half-process 8 blocks.
	for i in range(0,lanes): asm.pxor(XMM[8], XMM[i]);
	for j in range(1,8):
		for i in range(0,lanes): asm.aesenc(XMM[j+8], XMM[i]);
	for i in range(0, lanes): asm.movdqu(XMM[i], out.deref(16 * i));
	asm.add(16 * lanes, out);
	asm.sub(lanes, blocks);

def _aes_encrypt_pass_2(asm, out, ct, pt, blocks, lanes, r):
	#Half-process 8 blocks.
	for i in range(0, lanes): asm.movdqu(ct.deref(16 * i), XMM[i]);
	for j in range(0,r-1):
		for i in range(0,lanes): asm.aesenc(XMM[j+8], XMM[i]);
	for i in range(0,lanes): asm.aesenclast(XMM[r+7], XMM[i]);
	for i in range(0, lanes):
		asm.movdqu(pt.deref(16 * i), XMM[15]);		#XMM15 is free.
		asm.pxor(XMM[15], XMM[i]);			#XOR with plaintext.
		asm.movdqu(XMM[i], out.deref(16 * i));
	asm.add(16 * lanes, out);
	asm.add(16 * lanes, ct);
	asm.add(16 * lanes, pt);
	asm.sub(lanes, blocks);
	

def aes_encrypt_pass_1(asm, buf, blocks, iv, icount):
	SUPERBLOCK1_LOOP = "L_%=_superblock1_loop";
	SUPERBLOCK1_END = "L_%=_superblock1_end";
	PARTIALS = ["L_%=_superblock1_partial"+str(i) for i in range(0,8)];
	#Process in 128 byte superblocks.
	asm.label(SUPERBLOCK1_LOOP);
	asm._cmp(8, blocks);		#While at least 8 blocks...
	asm.jb(SUPERBLOCK1_END);
	_aes_encrypt_pass_1(asm, buf, icount, iv, blocks, 8)
	asm.jmp(SUPERBLOCK1_LOOP);
	asm.label(SUPERBLOCK1_END);
	#Process the remaining 1-7 blocks (or 0).
	for i in range(1, 8):
		asm._cmp(i, blocks);
		asm.jz(PARTIALS[i]);
	asm.jmp(PARTIALS[0]);
	for i in range(1, 8):
		asm.label(PARTIALS[i]);
		_aes_encrypt_pass_1(asm, buf, icount, iv, blocks, i)
		asm.jmp(PARTIALS[0]);
	asm.label(PARTIALS[0]);


def aes_encrypt_pass_2(asm, tgt, buf, src, blocks, r):
	SUPERBLOCK2_LOOP = "L_%=_superblock2_loop";
	SUPERBLOCK2_END = "L_%=_superblock2_end";
	PARTIALS = ["L_%=_superblock2_partial"+str(i) for i in range(0,8)];
	#Process in 128 byte superblocks.
	asm.label(SUPERBLOCK2_LOOP);
	asm._cmp(8, blocks);		#While at least 8 blocks...
	asm.jb(SUPERBLOCK2_END);
	_aes_encrypt_pass_2(asm, tgt, buf, src,blocks, 8, r)
	asm.jmp(SUPERBLOCK2_LOOP);
	asm.label(SUPERBLOCK2_END);
	#Process the remaining 1-7 blocks (or 0).
	for i in range(1, 8):
		asm._cmp(i, blocks);
		asm.jz(PARTIALS[i]);
	asm.jmp(PARTIALS[0]);
	for i in range(1, 8):
		asm.label(PARTIALS[i]);
		_aes_encrypt_pass_2(asm, tgt, buf, src,blocks, i, r)
		asm.jmp(PARTIALS[0]);
	asm.label(PARTIALS[0]);


def aes128_expround(asm, outaddr, xmm0, xmm1, xmm2, xmm3, r):
	RC = [1,2,4,8,16,32,64,128,27,54]
	#Do encryption round. This does not happen on the first round, since data=0, so the initial block equals
	#the key.
	if r > 0: asm.aesenc(xmm1, xmm0)
	#Compute aeskeygenassist.
	asm.aeskeygenassist(RC[r], xmm1, xmm2)
	#Distribute all high words and XOR into key.
	asm.pshufd(255, xmm2, xmm2)
	asm.movdqa(xmm1, xmm3)
	asm.pslldq(4, xmm3)
	asm.pxor(xmm3, xmm1)
	asm.pslldq(4, xmm3)
	asm.pxor(xmm3, xmm1)
	asm.pslldq(4, xmm3)
	asm.pxor(xmm3, xmm1)
	asm.pslldq(4, xmm3)
	asm.pxor(xmm3, xmm1)
	asm.pxor(xmm2, xmm1)
	#Store computed key block.
	asm.movdqu(xmm1, outaddr.deref(16*r+16))
	

def aes128keygen(asm, outaddr, inaddr, xmm0, xmm1, xmm2, xmm3, s1, s2):
	asm.movdqu(memory_reference.create(inaddr), xmm1)
	asm.movdqu(xmm1, memory_reference.create(outaddr))
	asm.movdqa(xmm1, xmm0)
	for i in range(10): aes128_expround(asm, outaddr, xmm0, xmm1, xmm2, xmm3, i);
	asm.aesenclast(xmm1, xmm0)
	#Reflect the block, so it is correct for multiplication.
	swapbytes(asm, xmm0, xmm1, xmm2, xmm3, s1, s2);
	asm.movdqu(xmm0, outaddr.deref(176))

def aes192keygen_sub(asm, outaddr, inaddr, xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, s1, s2, r):
	c = 4**r;
	o = 48*r+16;
	asm.aeskeygenassist(c, xmm2, xmm4);
	asm.pshufd(85, xmm4, xmm4);
	asm.pslldq(8, xmm2);
	asm.psrldq(8, xmm2);
	asm.movdqa(xmm1, xmm3);
	asm.pslldq(8, xmm3);
	asm.movdqa(xmm3, xmm0);
	asm.pslldq(4, xmm0);
	asm.pxor(xmm0, xmm3);
	asm.pxor(xmm3, xmm2);
	asm.movdqa(xmm1, xmm3);
	asm.psrldq(8, xmm3);
	asm.movdqa(xmm2, xmm0);
	asm.pslldq(8, xmm0);
	asm.pxor(xmm0, xmm3);
	asm.movdqa(xmm2, xmm0);
	asm.psrldq(12, xmm0);
	asm.pxor(xmm0, xmm3);
	asm.movdqa(xmm3, xmm0);
	asm.pslldq(4, xmm0);
	asm.pxor(xmm0, xmm3);
	asm.pslldq(4, xmm0);
	asm.pxor(xmm0, xmm3);
	asm.pslldq(4, xmm0);
	asm.pxor(xmm0, xmm3);
	asm.pslldq(4, xmm0);
	asm.pxor(xmm0, xmm3);
	asm.pxor(xmm4, xmm3);
	asm.pslldq(8, xmm4);
	asm.pxor(xmm4, xmm2);
	asm.movdqa(xmm2, outaddr.deref(o+0));
	asm.movdqa(xmm3, outaddr.deref(o+16));
	asm.aesenc(xmm2, xmm5);
	asm.aesenc(xmm3, xmm5);
	asm.aeskeygenassist(2*c, xmm3, xmm4);
	asm.pshufd(255, xmm4, xmm4);
	asm.movdqa(xmm2, xmm1);
	asm.psrldq(8, xmm1);
	asm.movdqa(xmm3, xmm2);
	asm.psrldq(8, xmm2);
	asm.pslldq(8, xmm3);
	asm.pxor(xmm3, xmm1);
	asm.movdqa(xmm1, xmm0);
	asm.pslldq(4, xmm0);
	asm.pxor(xmm0, xmm1);
	asm.pslldq(4, xmm0);
	asm.pxor(xmm0, xmm1);
	asm.pslldq(4, xmm0);
	asm.pxor(xmm0, xmm1);
	asm.pslldq(4, xmm0);
	asm.pxor(xmm0, xmm1);
	asm.movdqa(xmm1, xmm0);
	asm.psrldq(12, xmm0);
	asm.pxor(xmm0, xmm2);
	asm.movdqa(xmm2, xmm0);
	asm.pslldq(4, xmm0);
	asm.pxor(xmm0, xmm2);
	asm.pxor(xmm4, xmm1);
	asm.pxor(xmm4, xmm2);
	asm.movdqa(xmm1, outaddr.deref(o+32));
	if r == 3:
		asm.aesenclast(xmm1, xmm5);
	else:
		asm.aesenc(xmm1, xmm5);


def aes192keygen(asm, outaddr, inaddr, xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, s1, s2):
	asm.movdqu(inaddr.deref(0), xmm1);
	asm.movdqa(xmm1, outaddr.deref(0));
	asm.movdqa(xmm1, xmm5);
	asm.movdqu(inaddr.deref(8), xmm2);
	asm.psrldq(8, xmm2);
	for r in range(4): aes192keygen_sub(asm, outaddr, inaddr, xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, s1, s2, r);
	swapbytes(asm, xmm5, xmm1, xmm2, xmm3, s1, s2);
	asm.movdqa(xmm5, outaddr.deref(208));

def aes256keygen_sub(asm, outaddr, inaddr, xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, s1, s2, r):
	c = 2**r;
	o = 32*r+32;
	asm.aeskeygenassist(c, xmm2, xmm4);
	asm.pshufd(255, xmm4, xmm4);
	asm.movdqa(xmm1, xmm0);
	asm.pslldq(4, xmm0);
	asm.pxor(xmm0, xmm1);
	asm.pslldq(4, xmm0);
	asm.pxor(xmm0, xmm1);
	asm.pslldq(4, xmm0);
	asm.pxor(xmm0, xmm1);
	asm.pslldq(4, xmm0);
	asm.pxor(xmm0, xmm1);
	asm.pxor(xmm4, xmm1);
	asm.aeskeygenassist(0, xmm1, xmm4);
	asm.pshufd(170, xmm4, xmm4);
	asm.movdqa(xmm2, xmm0);
	asm.pslldq(4, xmm0);
	asm.pxor(xmm0, xmm2);
	asm.pslldq(4, xmm0);
	asm.pxor(xmm0, xmm2);
	asm.pslldq(4, xmm0);
	asm.pxor(xmm0, xmm2);
	asm.pslldq(4, xmm0);
	asm.pxor(xmm0, xmm2);
	asm.pxor(xmm4, xmm2);
	asm.movdqa(xmm1, outaddr.deref(o));
	if r == 6:
		asm.aesenclast(xmm1, xmm5);
	else:
		asm.movdqa(xmm2, outaddr.deref(o+16));
		asm.aesenc(xmm1, xmm5);
		asm.aesenc(xmm2, xmm5);

def aes256keygen(asm, outaddr, inaddr, xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, s1, s2):
	asm.movdqu(inaddr.deref(0), xmm1);
	asm.movdqa(xmm1, outaddr.deref(0));
	asm.movdqu(inaddr.deref(16), xmm2);
	asm.movdqa(xmm2, outaddr.deref(16));
	asm.movdqa(xmm1, xmm5);
	asm.aesenc(xmm2, xmm5);
	for r in range(7): aes256keygen_sub(asm, outaddr, inaddr, xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, s1, s2, r);
	swapbytes(asm, xmm5, xmm1, xmm2, xmm3, s1, s2);
	asm.movdqa(xmm5, outaddr.deref(240));

directory = "libs/hwaesgcm/low/src/";

fp = open(directory+"gcm_accumulate.asm","w");
gcm_accumulate_blocks2(assembler(fp), XMM0, XMM1, implicit_register("ptr"), implicit_register("blocks"), XMM2, XMM3, XMM4, XMM5, implicit_register("scratch1"), implicit_register("scratch2"));

fp = open(directory+"swapbytes_amd64.asm","w");
swapbytes(assembler(fp), XMM0, XMM1, XMM2, XMM3, implicit_register(1), implicit_register(2));

fp = open(directory+"multiply_field_reflxor_amd64.asm","w");
clmul_reflxor(assembler(fp), XMM0, XMM1, XMM2, XMM3, XMM4, XMM5, implicit_register("scratch1"), implicit_register("scratch2"))

fp = open(directory+"aes_encrypt_pass_1.asm","w");
aes_encrypt_pass_1(assembler(fp), implicit_register("buf"), implicit_register("blocks"), implicit_register("iv"), implicit_register("icount"));

fp = open(directory+"aes128_encrypt_pass_2.asm","w");
aes_encrypt_pass_2(assembler(fp), implicit_register("tgt"), implicit_register("buf"), implicit_register("src"), implicit_register("blocks"), 3);

fp = open(directory+"aes192_encrypt_pass_2.asm","w");
aes_encrypt_pass_2(assembler(fp), implicit_register("tgt"), implicit_register("buf"), implicit_register("src"), implicit_register("blocks"), 5);

fp = open(directory+"aes256_encrypt_pass_2.asm","w");
aes_encrypt_pass_2(assembler(fp), implicit_register("tgt"), implicit_register("buf"), implicit_register("src"), implicit_register("blocks"), 7);

fp = open(directory+"aes_128_keygen_amd64.asm","w");
aes128keygen(assembler(fp), implicit_register(0), implicit_register(1), XMM0, XMM1, XMM2, XMM3, explicit_register("r8d"), explicit_register("r9d"));

fp = open(directory+"aes_192_keygen_amd64.asm","w");
aes192keygen(assembler(fp), implicit_register(0), implicit_register(1), XMM0, XMM1, XMM2, XMM3, XMM4, XMM5, explicit_register("r8d"), explicit_register("r9d"));

fp = open(directory+"aes_256_keygen_amd64.asm","w");
aes256keygen(assembler(fp), implicit_register(0), implicit_register(1), XMM0, XMM1, XMM2, XMM3, XMM4, XMM5, explicit_register("r8d"), explicit_register("r9d"));
