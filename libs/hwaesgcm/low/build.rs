use cc::Build;

fn main()
{
	//Try AMD64 first.
	let mut config = Build::new();
	config.file("src/amd64.c");
	let failed = config.try_compile("hwaes.a").is_err();

	if failed {
		//Fall back to dummy.
		let mut config = Build::new();
		config.file("src/dummy.c");
		config.compile("hwaes.a");
	}

	println!("cargo:rerun-if-changed=build.rs");
	println!("cargo:rerun-if-changed=gen.py");
	println!("cargo:rerun-if-changed=src/amd64.c");
}
//...
