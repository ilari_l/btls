#ifndef __amd64__
#error "Only supported on AMD64 architecture"
#endif
#include <stdlib.h>
#include <string.h>
#include <stdalign.h>

int _aes_gcm_supported()
{
	unsigned leaf = 1;
	unsigned flags = 0;
	//Need to check both AES-NI and PCLMULQDQ.
	const unsigned check = (1 << 25) | (1 << 1);
	asm volatile("cpuid": "=c"(flags), "=a"(leaf): "a"(leaf): "ebx", "edx");
	return ((flags & check) == check) ? 0 : -1;
}

void _expand_key_aes128(unsigned char* out, const unsigned char* in)
{
	memset(out, 0, 192);
	asm volatile(
#include "aes_128_keygen_amd64.asm"
		: : "r"(out), "r"(in) : "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "r8", "r9");
}

void _expand_key_aes192(unsigned char* out, const unsigned char* in)
{
	memset(out, 0, 224);
	asm volatile(
#include "aes_192_keygen_amd64.asm"
		: : "r"(out), "r"(in) : "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "r8", "r9");
}

void _expand_key_aes256(unsigned char* out, const unsigned char* in)
{
	memset(out, 0, 240);
	asm volatile(
#include "aes_256_keygen_amd64.asm"
		: : "r"(out), "r"(in) : "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "r8", "r9");
}

void _aes128_encrypt_block(unsigned char* out, const unsigned char* in, const unsigned char* ctx)
{
	asm volatile(
		"\tmovdqu (%1), %%xmm0\n"
		"\tmovdqu 0(%2), %%xmm1\n"
		"\tmovdqu 16(%2), %%xmm2\n"
		"\tmovdqu 32(%2), %%xmm3\n"
		"\tmovdqu 48(%2), %%xmm4\n"
		"\tmovdqu 64(%2), %%xmm5\n"
		"\tmovdqu 80(%2), %%xmm6\n"
		"\tmovdqu 96(%2), %%xmm7\n"
		"\tmovdqu 112(%2), %%xmm8\n"
		"\tmovdqu 128(%2), %%xmm9\n"
		"\tmovdqu 144(%2), %%xmm10\n"
		"\tmovdqu 160(%2), %%xmm11\n"
		"\tpxor %%xmm1, %%xmm0\n"
		"\taesenc %%xmm2, %%xmm0\n"
		"\taesenc %%xmm3, %%xmm0\n"
		"\taesenc %%xmm4, %%xmm0\n"
		"\taesenc %%xmm5, %%xmm0\n"
		"\taesenc %%xmm6, %%xmm0\n"
		"\taesenc %%xmm7, %%xmm0\n"
		"\taesenc %%xmm8, %%xmm0\n"
		"\taesenc %%xmm9, %%xmm0\n"
		"\taesenc %%xmm10, %%xmm0\n"
		"\taesenclast %%xmm11, %%xmm0\n"
		"\tmovdqu %%xmm0, (%0)\n"
		: : "r"(out), "r"(in), "r"(ctx) : "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
		"xmm8", "xmm9", "xmm10", "xmm11");
}

void _aes192_encrypt_block(unsigned char* out, const unsigned char* in, const unsigned char* ctx)
{
	asm volatile(
		"\tmovdqu (%1), %%xmm0\n"
		"\tmovdqu 0(%2), %%xmm1\n"
		"\tmovdqu 16(%2), %%xmm2\n"
		"\tmovdqu 32(%2), %%xmm3\n"
		"\tmovdqu 48(%2), %%xmm4\n"
		"\tmovdqu 64(%2), %%xmm5\n"
		"\tmovdqu 80(%2), %%xmm6\n"
		"\tmovdqu 96(%2), %%xmm7\n"
		"\tmovdqu 112(%2), %%xmm8\n"
		"\tmovdqu 128(%2), %%xmm9\n"
		"\tmovdqu 144(%2), %%xmm10\n"
		"\tmovdqu 160(%2), %%xmm11\n"
		"\tmovdqu 176(%2), %%xmm12\n"
		"\tmovdqu 192(%2), %%xmm13\n"
		"\tpxor %%xmm1, %%xmm0\n"
		"\taesenc %%xmm2, %%xmm0\n"
		"\taesenc %%xmm3, %%xmm0\n"
		"\taesenc %%xmm4, %%xmm0\n"
		"\taesenc %%xmm5, %%xmm0\n"
		"\taesenc %%xmm6, %%xmm0\n"
		"\taesenc %%xmm7, %%xmm0\n"
		"\taesenc %%xmm8, %%xmm0\n"
		"\taesenc %%xmm9, %%xmm0\n"
		"\taesenc %%xmm10, %%xmm0\n"
		"\taesenc %%xmm11, %%xmm0\n"
		"\taesenc %%xmm12, %%xmm0\n"
		"\taesenclast %%xmm13, %%xmm0\n"
		"\tmovdqu %%xmm0, (%0)\n"
		: : "r"(out), "r"(in), "r"(ctx) : "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
		"xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13");
}

void _aes256_encrypt_block(unsigned char* out, const unsigned char* in, const unsigned char* ctx)
{
	asm volatile(
		"\tmovdqu (%1), %%xmm0\n"
		"\tmovdqu 0(%2), %%xmm1\n"
		"\tmovdqu 16(%2), %%xmm2\n"
		"\tmovdqu 32(%2), %%xmm3\n"
		"\tmovdqu 48(%2), %%xmm4\n"
		"\tmovdqu 64(%2), %%xmm5\n"
		"\tmovdqu 80(%2), %%xmm6\n"
		"\tmovdqu 96(%2), %%xmm7\n"
		"\tmovdqu 112(%2), %%xmm8\n"
		"\tmovdqu 128(%2), %%xmm9\n"
		"\tmovdqu 144(%2), %%xmm10\n"
		"\tmovdqu 160(%2), %%xmm11\n"
		"\tmovdqu 176(%2), %%xmm12\n"
		"\tmovdqu 192(%2), %%xmm13\n"
		"\tmovdqu 208(%2), %%xmm14\n"
		"\tmovdqu 224(%2), %%xmm15\n"
		"\tpxor %%xmm1, %%xmm0\n"
		"\taesenc %%xmm2, %%xmm0\n"
		"\taesenc %%xmm3, %%xmm0\n"
		"\taesenc %%xmm4, %%xmm0\n"
		"\taesenc %%xmm5, %%xmm0\n"
		"\taesenc %%xmm6, %%xmm0\n"
		"\taesenc %%xmm7, %%xmm0\n"
		"\taesenc %%xmm8, %%xmm0\n"
		"\taesenc %%xmm9, %%xmm0\n"
		"\taesenc %%xmm10, %%xmm0\n"
		"\taesenc %%xmm11, %%xmm0\n"
		"\taesenc %%xmm12, %%xmm0\n"
		"\taesenc %%xmm13, %%xmm0\n"
		"\taesenc %%xmm14, %%xmm0\n"
		"\taesenclast %%xmm15, %%xmm0\n"
		"\tmovdqu %%xmm0, (%0)\n"
		: : "r"(out), "r"(in), "r"(ctx) : "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7",
		"xmm8", "xmm9", "xmm10", "xmm11", "xmm12", "xmm13", "xmm14", "xmm15");
}


void _gcm_accumulate(const unsigned char* ctx, unsigned char* accumulator, const unsigned char* data, size_t blocks)
{
	unsigned scratch1 = 0, scratch2 = 0;
	asm volatile(
		"\tmovdqa (%[accumulator]), %%xmm0\n"
		"\tmovdqa (%[context]), %%xmm1\n"
#include "gcm_accumulate.asm"
		"\tmovdqa %%xmm0, (%[accumulator])\n"
		: [scratch1]"=&r"(scratch1), [scratch2]"=&r"(scratch2), [ptr]"=&r"(data), [blocks]"=&r"(blocks) :
		[context]"r"(ctx), [accumulator]"r"(accumulator), "[ptr]"(data), "[blocks]"(blocks) :
		"xmm0", "xmm1", "xmm2", "xmm3", "memory");
}

void _gcm_finish(const unsigned char* ctx, unsigned char* tag, const unsigned char* accumulator, size_t alen,
	size_t clen, const unsigned char* mask)
{
	unsigned char partial[16];
	for(unsigned i = 0; i < 8; i++) {
		partial[i+0] = 8 * (unsigned long long)alen >> (56 - 8 * i);
		partial[i+8] = 8 * (unsigned long long)clen >> (56 - 8 * i);
	}
	unsigned scratch1 = 0, scratch2 = 0;
	asm volatile(
		"\tmovdqu (%[acc]), %%xmm0\n"
		"\tmovdqu (%[mul]), %%xmm1\n"
		"\tmovdqu (%[buf]), %%xmm2\n"
#include "multiply_field_reflxor_amd64.asm"
		//The accumulator is reflected, so unreflect it before masking.
#include "swapbytes_amd64.asm"
		"\tmovdqu (%[mask]), %%xmm1\n"
		"\tpxor %%xmm1, %%xmm0\n"
		"\tmovdqu %%xmm0, (%[tag])\n"
		: [scratch1]"=&r"(scratch1), [scratch2]"=&r"(scratch2) : [acc]"r"(accumulator), [mul]"r"(ctx),
			[buf]"r"(partial), [mask]"r"(mask), [tag]"r"(tag) :
			"xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "memory");
}

//Fills buf with IV and counter, and partially encrypts buffer.
static void _aes_encrypt_pass1(const unsigned char* ctx, unsigned char* buf, size_t blocks, unsigned char* iv, 
	unsigned icount)
{
	asm volatile(
		"\tmovdqa 0(%[context]), %%xmm8\n"
		"\tmovdqa 16(%[context]), %%xmm9\n"
		"\tmovdqa 32(%[context]), %%xmm10\n"
		"\tmovdqa 48(%[context]), %%xmm11\n"
		"\tmovdqa 64(%[context]), %%xmm12\n"
		"\tmovdqa 80(%[context]), %%xmm13\n"
		"\tmovdqa 96(%[context]), %%xmm14\n"
		"\tmovdqa 112(%[context]), %%xmm15\n"
#include "aes_encrypt_pass_1.asm"
		: [buf]"=&r"(buf), [blocks]"=&r"(blocks), [icount]"=&r"(icount)
		: [context]"r"(ctx), "[buf]"(buf), "[blocks]"(blocks), "[icount]"(icount), [iv]"r"(iv)
		: "xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7", "xmm8", "xmm9", "xmm10",
		"xmm11", "xmm12", "xmm13", "xmm14", "xmm15", "memory");
}

//These all are aliases.
void _aes128_encrypt_pass1(const unsigned char* ctx, unsigned char* buf, size_t blocks, unsigned char* iv, 
	unsigned icount)
{
	_aes_encrypt_pass1(ctx, buf, blocks, iv, icount);
}
void _aes192_encrypt_pass1(const unsigned char* ctx, unsigned char* buf, size_t blocks, unsigned char* iv, 
	unsigned icount)
{
	_aes_encrypt_pass1(ctx, buf, blocks, iv, icount);
}
void _aes256_encrypt_pass1(const unsigned char* ctx, unsigned char* buf, size_t blocks, unsigned char* iv, 
	unsigned icount)
{
	_aes_encrypt_pass1(ctx, buf, blocks, iv, icount);
}

//Partially encrypts buf. After encryption, computes *tgt = *buf ^ *src.
void _aes128_encrypt_pass2(const unsigned char* ctx, unsigned char* tgt, unsigned char* buf,
	const unsigned char* src, size_t blocks)
{
	asm volatile(
		"\tmovdqa 128(%[context]), %%xmm8\n"
		"\tmovdqa 144(%[context]), %%xmm9\n"
		"\tmovdqa 160(%[context]), %%xmm10\n"
#include "aes128_encrypt_pass_2.asm"
		: [tgt]"=&r"(tgt), [buf]"=&r"(buf), [src]"=&r"(src), [blocks]"=&r"(blocks) : [context]"r"(ctx),
			"[tgt]"(tgt), "[buf]"(buf), "[src]"(src), "[blocks]"(blocks) :
			"xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7", "xmm8", "xmm9", "xmm10",
			"xmm11", "xmm12", "xmm13", "xmm14", "xmm15", "memory");
}

void _aes192_encrypt_pass2(const unsigned char* ctx, unsigned char* tgt, unsigned char* buf,
	const unsigned char* src, size_t blocks)
{
	asm volatile(
		"\tmovdqa 128(%[context]), %%xmm8\n"
		"\tmovdqa 144(%[context]), %%xmm9\n"
		"\tmovdqa 160(%[context]), %%xmm10\n"
		"\tmovdqa 176(%[context]), %%xmm11\n"
		"\tmovdqa 192(%[context]), %%xmm12\n"
#include "aes192_encrypt_pass_2.asm"
		: [tgt]"=&r"(tgt), [buf]"=&r"(buf), [src]"=&r"(src), [blocks]"=&r"(blocks) : [context]"r"(ctx),
			"[tgt]"(tgt), "[buf]"(buf), "[src]"(src), "[blocks]"(blocks) :
			"xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7", "xmm8", "xmm9", "xmm10",
			"xmm11", "xmm12", "xmm13", "xmm14", "xmm15", "memory");
}

void _aes256_encrypt_pass2(const unsigned char* ctx, unsigned char* tgt, unsigned char* buf,
	const unsigned char* src, size_t blocks)
{
	asm volatile(
		"\tmovdqa 128(%[context]), %%xmm8\n"
		"\tmovdqa 144(%[context]), %%xmm9\n"
		"\tmovdqa 160(%[context]), %%xmm10\n"
		"\tmovdqa 176(%[context]), %%xmm11\n"
		"\tmovdqa 192(%[context]), %%xmm12\n"
		"\tmovdqa 208(%[context]), %%xmm13\n"
		"\tmovdqa 224(%[context]), %%xmm14\n"
#include "aes256_encrypt_pass_2.asm"
		: [tgt]"=&r"(tgt), [buf]"=&r"(buf), [src]"=&r"(src), [blocks]"=&r"(blocks) : [context]"r"(ctx),
			"[tgt]"(tgt), "[buf]"(buf), "[src]"(src), "[blocks]"(blocks) :
			"xmm0", "xmm1", "xmm2", "xmm3", "xmm4", "xmm5", "xmm6", "xmm7", "xmm8", "xmm9", "xmm10",
			"xmm11", "xmm12", "xmm13", "xmm14", "xmm15", "memory");
}
