use btls_aux_hwaesgcm_cimpl::*;
use std::time::Instant;

fn bench(iamt: usize, tries: usize, msg: &str, mut f: impl FnMut())
{
	let t1 = Instant::now();
	for _ in 0..tries { f(); }
	let t2 = Instant::now();
	let dt = t2.duration_since(t1);
	let cpb = 3.6 * dt.as_nanos() as f64 / (iamt as f64 * tries as f64);
	let speed = 1000.0 * iamt as f64 * tries as f64 / (dt.as_nanos() as f64);
	println!("{}: {} MB/s ({} c/b)", msg, speed, cpb);
}

fn main()
{
	let keydata = [0;32];
	let nonce = [0;12];
	let buf1 = [0;16384];
	let mut buf2 = [0;16384+16];
	let key1 = unsafe{AesGcmKey::<Aes128Key>::keygen(keydata.as_ptr())};
	let key2 = unsafe{AesGcmKey::<Aes192Key>::keygen(keydata.as_ptr())};
	let key3 = unsafe{AesGcmKey::<Aes256Key>::keygen(keydata.as_ptr())};
	bench(16384, 65536, "AES-128-GCM MAC-only", ||unsafe{
		key1.encrypt(nonce.as_ptr(), buf1.as_ptr(), buf1.len(), buf1.as_ptr(), buf2.as_mut_ptr(), 0, 0);
	});
	bench(16384, 65536, "AES-192-GCM MAC-only", ||unsafe{
		key2.encrypt(nonce.as_ptr(), buf1.as_ptr(), buf1.len(), buf1.as_ptr(), buf2.as_mut_ptr(), 0, 0);
	});
	bench(16384, 65536, "AES-256-GCM MAC-only", ||unsafe{
		key3.encrypt(nonce.as_ptr(), buf1.as_ptr(), buf1.len(), buf1.as_ptr(), buf2.as_mut_ptr(), 0, 0);
	});
	bench(16384, 65536, "AES-128-GCM Buffer-to-Buffer", ||unsafe{
		key1.encrypt(nonce.as_ptr(), buf1.as_ptr(), 0, buf1.as_ptr(), buf2.as_mut_ptr(), buf1.len(), 0);
	});
	bench(16384, 65536, "AES-192-GCM Buffer-to-Buffer", ||unsafe{
		key2.encrypt(nonce.as_ptr(), buf1.as_ptr(), 0, buf1.as_ptr(), buf2.as_mut_ptr(), buf1.len(), 0);
	});
	bench(16384, 65536, "AES-256-GCM Buffer-to-Buffer", ||unsafe{
		key3.encrypt(nonce.as_ptr(), buf1.as_ptr(), 0, buf1.as_ptr(), buf2.as_mut_ptr(), buf1.len(), 0);
	});
	bench(16384, 65536, "AES-128-GCM In-Place", ||unsafe{
		key1.encrypt(nonce.as_ptr(), buf1.as_ptr(), 0, buf2.as_ptr(), buf2.as_mut_ptr(), buf1.len(), 0);
	});
	bench(16384, 65536, "AES-192-GCM In-Place", ||unsafe{
		key2.encrypt(nonce.as_ptr(), buf1.as_ptr(), 0, buf2.as_ptr(), buf2.as_mut_ptr(), buf1.len(), 0);
	});
	bench(16384, 65536, "AES-256-GCM In-Place", ||unsafe{
		key3.encrypt(nonce.as_ptr(), buf1.as_ptr(), 0, buf2.as_ptr(), buf2.as_mut_ptr(), buf1.len(), 0);
	});
}
