"L_%=_1:"
		"\tcmp $1, %[blocks]\n"
		"\tjb L_%=_2\n"
		"\tmovdqu 0(%[ptr]), %%xmm5\n"
		"\tmov $4042322160, %[scratch1]\n"
		"\tmov $252645135, %[scratch2]\n"
		"\tmovd %[scratch1], %%xmm2\n"
		"\tmovd %[scratch2], %%xmm4\n"
		"\tpshufd $0, %%xmm2, %%xmm2\n"
		"\tpshufd $0, %%xmm4, %%xmm4\n"
		"\tmovdqa %%xmm5, %%xmm3\n"
		"\tpsllq $4, %%xmm5\n"
		"\tpsrlq $4, %%xmm3\n"
		"\tpand %%xmm2, %%xmm5\n"
		"\tpand %%xmm4, %%xmm3\n"
		"\tpor %%xmm3, %%xmm5\n"
		"\tmov $3435973836, %[scratch1]\n"
		"\tmov $858993459, %[scratch2]\n"
		"\tmovd %[scratch1], %%xmm2\n"
		"\tmovd %[scratch2], %%xmm4\n"
		"\tpshufd $0, %%xmm2, %%xmm2\n"
		"\tpshufd $0, %%xmm4, %%xmm4\n"
		"\tmovdqa %%xmm5, %%xmm3\n"
		"\tpsllq $2, %%xmm5\n"
		"\tpsrlq $2, %%xmm3\n"
		"\tpand %%xmm2, %%xmm5\n"
		"\tpand %%xmm4, %%xmm3\n"
		"\tpor %%xmm3, %%xmm5\n"
		"\tmov $2863311530, %[scratch1]\n"
		"\tmov $1431655765, %[scratch2]\n"
		"\tmovd %[scratch1], %%xmm2\n"
		"\tmovd %[scratch2], %%xmm4\n"
		"\tpshufd $0, %%xmm2, %%xmm2\n"
		"\tpshufd $0, %%xmm4, %%xmm4\n"
		"\tmovdqa %%xmm5, %%xmm3\n"
		"\tpsllq $1, %%xmm5\n"
		"\tpsrlq $1, %%xmm3\n"
		"\tpand %%xmm2, %%xmm5\n"
		"\tpand %%xmm4, %%xmm3\n"
		"\tpor %%xmm3, %%xmm5\n"
		"\tpxor %%xmm5, %%xmm0\n"
		"\tmovdqa %%xmm0, %%xmm2\n"
		"\tmovdqa %%xmm0, %%xmm3\n"
		"\tmovdqa %%xmm0, %%xmm4\n"
		"\tpclmulqdq $0, %%xmm1, %%xmm0\n"
		"\tpclmulqdq $1, %%xmm1, %%xmm2\n"
		"\tpclmulqdq $16, %%xmm1, %%xmm3\n"
		"\tpclmulqdq $17, %%xmm1, %%xmm4\n"
		"\tpxor %%xmm3, %%xmm2\n"
		"\tmovdqa %%xmm2, %%xmm3\n"
		"\tpslldq $8, %%xmm2\n"
		"\tpsrldq $8, %%xmm3\n"
		"\tpxor %%xmm2, %%xmm0\n"
		"\tpxor %%xmm3, %%xmm4\n"
		"\tpxor %%xmm4, %%xmm0\n"
		"\tmovdqa %%xmm4, %%xmm3\n"
		"\tpsllq $1, %%xmm3\n"
		"\tpxor %%xmm3, %%xmm0\n"
		"\tpsllq $1, %%xmm3\n"
		"\tpxor %%xmm3, %%xmm0\n"
		"\tpsllq $5, %%xmm3\n"
		"\tpxor %%xmm3, %%xmm0\n"
		"\tmovdqa %%xmm4, %%xmm3\n"
		"\tpslldq $8, %%xmm3\n"
		"\tpsrlq $57, %%xmm3\n"
		"\tpxor %%xmm3, %%xmm0\n"
		"\tpsrlq $5, %%xmm3\n"
		"\tpxor %%xmm3, %%xmm0\n"
		"\tpsrlq $1, %%xmm3\n"
		"\tpxor %%xmm3, %%xmm0\n"
		"\tpsrldq $12, %%xmm4\n"
		"\tmovd %%xmm4, %[scratch1]\n"
		"\tshr $25, %[scratch1]\n"
		"\tmov %[scratch1], %[scratch2]\n"
		"\tshr $5, %[scratch1]\n"
		"\txor %[scratch1], %[scratch2]\n"
		"\tshr $1, %[scratch1]\n"
		"\txor %[scratch1], %[scratch2]\n"
		"\tmov %[scratch2], %[scratch1]\n"
		"\tshl $1, %[scratch2]\n"
		"\txor %[scratch2], %[scratch1]\n"
		"\tshl $1, %[scratch2]\n"
		"\txor %[scratch2], %[scratch1]\n"
		"\tshl $5, %[scratch2]\n"
		"\txor %[scratch2], %[scratch1]\n"
		"\tmovd %[scratch1], %%xmm2\n"
		"\tpxor %%xmm2, %%xmm0\n"
		"\tdec %[blocks]\n"
		"\tadd $16, %[ptr]\n"
		"\tjmp L_%=_1\n"
"L_%=_2:"
