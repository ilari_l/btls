#include <stdlib.h>

int _aes_gcm_supported() { return -1; }		//No.

void _expand_key_aes128(unsigned char* out, const unsigned char* in)
{
	(void)out; (void)in;
}

void _expand_key_aes192(unsigned char* out, const unsigned char* in)
{
	(void)out; (void)in;
}

void _expand_key_aes256(unsigned char* out, const unsigned char* in)
{
	(void)out; (void)in;
}

void _aes128_encrypt_block(unsigned char* out, const unsigned char* in, const unsigned char* ctx)
{
	(void)out; (void)in; (void)ctx;
}

void _aes192_encrypt_block(unsigned char* out, const unsigned char* in, const unsigned char* ctx)
{
	(void)out; (void)in; (void)ctx;
}

void _aes256_encrypt_block(unsigned char* out, const unsigned char* in, const unsigned char* ctx)
{
	(void)out; (void)in; (void)ctx;
}

void _gcm_accumulate(const unsigned char* ctx, unsigned char* accumulator, const unsigned char* data, size_t blocks)
{
	(void)ctx; (void)accumulator; (void)data; (void)blocks;
}

void _gcm_finish(const unsigned char* ctx, unsigned char* tag, const unsigned char* accumulator, size_t alen,
	size_t clen, const unsigned char* mask)
{
	(void)ctx; (void)tag; (void)accumulator; (void)alen; (void)clen; (void)mask;
}

void _aes128_encrypt_pass1(const unsigned char* ctx, unsigned char* buf, size_t blocks, unsigned char* iv, 
	unsigned icount)
{
	(void)ctx; (void)buf; (void)blocks; (void)iv; (void)icount;
}
void _aes192_encrypt_pass1(const unsigned char* ctx, unsigned char* buf, size_t blocks, unsigned char* iv, 
	unsigned icount)
{
	(void)ctx; (void)buf; (void)blocks; (void)iv; (void)icount;
}

void _aes256_encrypt_pass1(const unsigned char* ctx, unsigned char* buf, size_t blocks, unsigned char* iv, 
	unsigned icount)
{
	(void)ctx; (void)buf; (void)blocks; (void)iv; (void)icount;
}

//Partially encrypts buf. After encryption, computes *tgt = *buf ^ *src.
void _aes128_encrypt_pass2(const unsigned char* ctx, unsigned char* tgt, unsigned char* buf,
	const unsigned char* src, size_t blocks)
{
	(void)ctx; (void)tgt; (void)buf; (void)src; (void)blocks;
}

void _aes192_encrypt_pass2(const unsigned char* ctx, unsigned char* tgt, unsigned char* buf,
	const unsigned char* src, size_t blocks)
{
	(void)ctx; (void)tgt; (void)buf; (void)src; (void)blocks;
}

void _aes256_encrypt_pass2(const unsigned char* ctx, unsigned char* tgt, unsigned char* buf,
	const unsigned char* src, size_t blocks)
{
	(void)ctx; (void)tgt; (void)buf; (void)src; (void)blocks;
}
