//!Hardware AES-GCM support low level implementations.
//!
//!This crate contains low-level hardware-accelerated implementations of `AES-GCM`
//!
//!This is only meant to be used by the [`btls-aux-hwaesgcm`](../btls_aux_hwaesgcm/index.html) crate.
#![allow(improper_ctypes)]
#![no_std]
#[cfg(test)] #[macro_use] extern crate std;

///Check if AES-GCM is supported. Returns 0 if yes, -1 if no. Calling other routines if this returns no
///is Undefined Behavior.
pub unsafe extern fn aes_gcm_supported() -> i32 { _aes_gcm_supported() }

	
	
use core::cmp::min;
use core::mem::transmute;
use core::mem::zeroed;
use core::ptr::copy_nonoverlapping;
use core::slice::from_raw_parts;

#[derive(Copy,Clone)] #[repr(C,align(16))] pub struct Aes128Key([u8;176]);
#[derive(Copy,Clone)] #[repr(C,align(16))] pub struct Aes192Key([u8;208]);
#[derive(Copy,Clone)] #[repr(C,align(16))] pub struct Aes256Key([u8;240]);
#[doc(hidden)] #[derive(Copy,Clone)] #[repr(C,align(16))] pub struct GcmKey([u8;16]);
#[doc(hidden)] #[repr(C,align(16))] pub struct Aligned16([u8;16]);
#[repr(C,align(16))] struct GcmAccumulator([u8;16]);

impl GcmAccumulator
{
	unsafe fn accumulate(&mut self, key: &GcmKey, data: *const u8, size: usize)
	{
		let blocks = size / 16;
		let leftover = size - 16 * blocks;
		let shift = 16 * blocks;
		_gcm_accumulate(key as *const _, self as *mut _, data, blocks);
		if leftover > 0 {
			let mut partial = [0;16];
			copy_nonoverlapping(data.add(shift), partial.as_mut_ptr(), leftover);
			_gcm_accumulate(key as *const _, self as *mut _, partial.as_ptr(), 1);
		}
	}
	unsafe fn finish(self, key: &GcmKey, tag: *mut u8, alen: usize, clen: usize, mask: &[u8;16])
	{
		_gcm_finish(key as *const _, tag, &self as *const _, alen, clen, mask.as_ptr());
	}
}

pub trait AesKey: Sized
{
	unsafe fn keygen(key: *const u8) -> AesGcmKey<Self>;
	unsafe fn encrypt_block(&self, out: &mut [u8;16], inp: &[u8;16]);
	unsafe fn ctr_blocks_pass1(&self, buf: *mut u8, blocks: usize, iv: *mut Aligned16, icount: u32);
	unsafe fn ctr_blocks_pass2(&self, tgt: *mut u8, ct: *mut u8, pt: *const u8, blocks: usize);
	fn keylength() -> usize;	//Only for testing.
}

impl AesKey for Aes128Key
{
	unsafe fn keygen(key: *const u8) -> AesGcmKey<Self>
	{
		let mut o: AesGcmKey<Self> = zeroed();
		_expand_key_aes128(&mut o as *mut _, key);
		o
	}
	unsafe fn encrypt_block(&self, out: &mut [u8;16], inp: &[u8;16])
	{
		_aes128_encrypt_block(out.as_mut_ptr(), inp.as_ptr(), self as *const _);
	}
	unsafe fn ctr_blocks_pass1(&self, buf: *mut u8, blocks: usize, iv: *mut Aligned16, icount: u32)
	{
		_aes128_encrypt_pass1(self as *const _, buf, blocks, iv, icount);
	}
	unsafe fn ctr_blocks_pass2(&self, tgt: *mut u8, ct: *mut u8, pt: *const u8, blocks: usize)
	{
		_aes128_encrypt_pass2(self as *const _, tgt, ct, pt, blocks);
	}
	fn keylength() -> usize { 16 }
}

impl AesKey for Aes192Key
{
	unsafe fn keygen(key: *const u8) -> AesGcmKey<Self>
	{
		let mut o: AesGcmKey<Self> = zeroed();
		_expand_key_aes192(&mut o as *mut _, key);
		o
	}
	unsafe fn encrypt_block(&self, out: &mut [u8;16], inp: &[u8;16])
	{
		_aes192_encrypt_block(out.as_mut_ptr(), inp.as_ptr(), self as *const _);
	}
	unsafe fn ctr_blocks_pass1(&self, buf: *mut u8, blocks: usize, iv: *mut Aligned16, icount: u32)
	{
		_aes192_encrypt_pass1(self as *const _, buf, blocks, iv, icount);
	}
	unsafe fn ctr_blocks_pass2(&self, tgt: *mut u8, ct: *mut u8, pt: *const u8, blocks: usize)
	{
		_aes192_encrypt_pass2(self as *const _, tgt, ct, pt, blocks);
	}
	fn keylength() -> usize { 24 }
}

impl AesKey for Aes256Key
{
	unsafe fn keygen(key: *const u8) -> AesGcmKey<Self>
	{
		let mut o: AesGcmKey<Self> = zeroed();
		_expand_key_aes256(&mut o as *mut _, key);
		o
	}
	unsafe fn encrypt_block(&self, out: &mut [u8;16], inp: &[u8;16])
	{
		_aes256_encrypt_block(out.as_mut_ptr(), inp.as_ptr(), self as *const _);
	}
	unsafe fn ctr_blocks_pass1(&self, buf: *mut u8, blocks: usize, iv: *mut Aligned16, icount: u32)
	{
		_aes256_encrypt_pass1(self as *const _, buf, blocks, iv, icount);
	}
	unsafe fn ctr_blocks_pass2(&self, tgt: *mut u8, ct: *mut u8, pt: *const u8, blocks: usize)
	{
		_aes256_encrypt_pass2(self as *const _, tgt, ct, pt, blocks);
	}
	fn keylength() -> usize { 32 }
}

#[derive(Copy,Clone)]
union Byte { bad: (), _good: u8 }

#[derive(Copy,Clone)]
#[repr(C)]
pub struct AesGcmKey<AES:AesKey> {
	aes: AES,
	gcm: GcmKey,
}

impl<AES:AesKey> AesGcmKey<AES>
{
	//Adjust buffers so that ptlen1 is multiple of blocksize, and pt/ct do not alias for first part.
	unsafe fn adjust_buffers(pt: *const u8, ct: *mut u8, ptlen1: &mut usize, ptlen2: &mut usize)
	{
		if pt == ct {
			*ptlen2 += *ptlen1;
			*ptlen1 = 0;
		} else {
			let newptlen1 = *ptlen1 & !15;
			let overlap = *ptlen1 & 15;
			if overlap > 0 {
				copy_nonoverlapping(pt.add(newptlen1), ct.add(newptlen1), overlap);
			}
			*ptlen2 += overlap;
			*ptlen1 = newptlen1;
		}
	}
	//Derive mask.
	unsafe fn derive_mask(&self, nonce: &[u8;12], iv: &mut Aligned16, mask: &mut [u8;16])
	{
		(&mut iv.0[..12]).copy_from_slice(nonce);
		(&mut iv.0[12..]).copy_from_slice(&1u32.to_be_bytes());	//The mask value.
		self.aes.encrypt_block(mask, &iv.0);
	}
	//CTR encrypt part 2.
	//Generates enough cipherstream for bytes bytes, with iv(counter), where counter starts as icount. The
	//stream is XORed with ct.
	unsafe fn ctr_part_2(&self, mut ct: *mut u8, mut bytes: usize, iv: &mut Aligned16, mut icount: u32)
	{
		let mut tmp = [Byte{bad:()};2048];
		let tmpp = tmp.as_mut_ptr() as *mut u8;
		while bytes > 16 {
			let maxblocks = min(bytes, tmp.len()) / 16;
			self.aes.ctr_blocks_pass1(tmpp, maxblocks, iv as *mut _, icount);
			self.aes.ctr_blocks_pass2(ct, tmpp, ct, maxblocks);
			icount += maxblocks as u32; 
			bytes -= 16 * maxblocks;
			ct = ct.add(16 * maxblocks);
		}
		if bytes > 0 {
			//Fastpath for single last block.
			let mut stream = [0;16];
			(&mut iv.0[12..]).copy_from_slice(&icount.to_be_bytes());
			self.aes.encrypt_block(&mut stream, &iv.0);
			for i in 0..bytes {
				*ct.add(i) ^= *stream.as_ptr().add(i);
			}
		}
	}
	pub unsafe fn keygen(key: *const u8) -> Self { AES::keygen(key) }
	pub unsafe fn decrypt(&self, nonce: *const u8, ad: *const u8, adlen: usize, pt: *mut u8, ct: *const u8,
		ctlen: usize) -> i32
	{
		let mut iv: Aligned16 = zeroed();
		let mut mask = [0;16];
		let ptlen = ctlen.saturating_sub(16);

		//Accumulate AD and ciphertext and check the tag.
		let mut tag = [0;16];
		let mut accum: GcmAccumulator = zeroed();
		self.derive_mask(transmute(nonce), &mut iv, &mut mask);
		accum.accumulate(&self.gcm, ad, adlen);
		accum.accumulate(&self.gcm, ct, ptlen);		//ptlen, as tag is not accumulated.
		accum.finish(&self.gcm, tag.as_mut_ptr(), adlen, ptlen, &mask);
		let syndrome = {
			let mut syndrome = 0;
			let mtag = from_raw_parts(ct.add(ptlen), 16);
			for i in 0..16 { syndrome |= mtag[i] ^ tag[i]; }
			syndrome
		};
		if syndrome != 0 { return -1; }
		//Tag is OK, decrypt.
		if ct == pt {
			//Inplace, use only ctr_part_2, and start nonces at 2.
			self.ctr_part_2(pt, ptlen, &mut iv, 2);
		} else {
			//Buffer-to-buffer. Do the first part using ctr_part_1 and the incomplete block using
			//ctr_part_2.
			let ptblocks = ptlen / 16;
			let ptp2shift = ptblocks * 16;
			let remaining = ptlen % 16;
			self.aes.ctr_blocks_pass1(pt, ptblocks, &mut iv as *mut _, 2);
			self.aes.ctr_blocks_pass2(pt, pt, ct, ptblocks);
			if remaining > 0 {
				copy_nonoverlapping(ct.add(ptp2shift), pt.add(ptp2shift), remaining);
				self.ctr_part_2(pt.add(ptp2shift), remaining, &mut iv, ptblocks as u32 + 2);
			}
		}
		0
	}
	pub unsafe fn encrypt(&self, nonce: *const u8, ad: *const u8, adlen: usize, pt: *const u8, ct: *mut u8,
		mut ptlen1: usize, mut ptlen2: usize)
	{
		let mut iv: Aligned16 = zeroed();
		let mut mask = [0;16];

		//Derive mask.
		Self::adjust_buffers(pt, ct, &mut ptlen1, &mut ptlen2);
		self.derive_mask(transmute(nonce), &mut iv, &mut mask);
		//Accumulate AD.
		let mut accum: GcmAccumulator = zeroed();
		accum.accumulate(&self.gcm, ad, adlen);
		//Encrypt.
		self.aes.ctr_blocks_pass1(ct, ptlen1 / 16, &mut iv as *mut _, 2);
		self.aes.ctr_blocks_pass2(ct, ct, pt, ptlen1 / 16);
		self.ctr_part_2(ct.add(ptlen1), ptlen2, &mut iv, ptlen1 as u32 / 16 + 2);
		//Acccumulate and finish ciphertext.
		let ctlen = ptlen1 + ptlen2;
		accum.accumulate(&self.gcm, ct, ctlen);
		accum.finish(&self.gcm, ct.add(ctlen), adlen, ctlen, &mask);
	}
}

extern
{
	fn _aes_gcm_supported() -> i32;

	fn _gcm_accumulate(ctx: *const GcmKey, accumulator: *mut GcmAccumulator, data: *const u8, blocks: usize);
	fn _gcm_finish(ctx: *const GcmKey, tag: *mut u8, accumulator: *const GcmAccumulator, alen: usize,
		clen: usize, mask: *const u8);

	fn _expand_key_aes128(output: *mut AesGcmKey<Aes128Key>, input: *const u8);
	fn _aes128_encrypt_block(output: *mut u8, input: *const u8, ctx: *const Aes128Key);
	fn _aes128_encrypt_pass1(key: *const Aes128Key, buf: *mut u8, blocks: usize, iv: *mut Aligned16,
		icount: u32);
	fn _aes128_encrypt_pass2(key: *const Aes128Key, tgt: *mut u8, ct: *mut u8, pt: *const u8, blocks: usize);

	fn _expand_key_aes192(output: *mut AesGcmKey<Aes192Key>, input: *const u8);
	fn _aes192_encrypt_block(output: *mut u8, input: *const u8, ctx: *const Aes192Key);
	fn _aes192_encrypt_pass1(key: *const Aes192Key, buf: *mut u8, blocks: usize, iv: *mut Aligned16,
		icount: u32);
	fn _aes192_encrypt_pass2(key: *const Aes192Key, tgt: *mut u8, ct: *mut u8, pt: *const u8, blocks: usize);

	fn _expand_key_aes256(output: *mut AesGcmKey<Aes256Key>, input: *const u8);
	fn _aes256_encrypt_block(output: *mut u8, input: *const u8, ctx: *const Aes256Key);
	fn _aes256_encrypt_pass1(key: *const Aes256Key, buf: *mut u8, blocks: usize, iv: *mut Aligned16,
		icount: u32);
	fn _aes256_encrypt_pass2(key: *const Aes256Key, tgt: *mut u8, ct: *mut u8, pt: *const u8, blocks: usize);
}

#[cfg(test)]
mod test;
