use super::*;
use btls_aux_aead_base::LowLevelAeadContext;
use btls_aux_nettle::Aes128GcmContext;
use btls_aux_nettle::Aes192GcmContext;
use btls_aux_nettle::Aes256GcmContext;
use btls_aux_random::secure_random;
use std::vec::Vec;
use std::slice::from_raw_parts;

fn encrypt_aes_gcm_ref(key: &[u8], nonce: &[u8;12], ad: &[u8], msg: &[u8]) -> Vec<u8>
{
	let mut out = vec![0;msg.len()+16];
	let len = unsafe { match key.len() {
		16 => Aes128GcmContext::new_context(key).unwrap().
			encrypt3(nonce, ad, out.as_mut_ptr(), msg.as_ptr(), msg.len(), 0).unwrap(),
		24 => Aes192GcmContext::new_context(key).unwrap().
			encrypt3(nonce, ad, out.as_mut_ptr(), msg.as_ptr(), msg.len(), 0).unwrap(),
		32 => Aes256GcmContext::new_context(key).unwrap().
			encrypt3(nonce, ad, out.as_mut_ptr(), msg.as_ptr(), msg.len(), 0).unwrap(),
		_ => unreachable!()
	}};
	assert_eq!(len, out.len());
	out
}

unsafe fn roundtrip_test<AES:AesKey>(key: AesGcmKey<AES>, nonce: &[u8], ad: &[u8], msg: &[u8])
{
	//Do test out-of-place. Because in-place uses scratch buffer.
	let mut out = vec![0;msg.len()+16];
	let mut out2 = vec![0;msg.len()];
	key.encrypt(nonce.as_ptr(), ad.as_ptr(), ad.len(), msg.as_ptr(), out.as_mut_ptr(), msg.len(), 0);
	assert!(key.decrypt(nonce.as_ptr(), ad.as_ptr(), ad.len(), out2.as_mut_ptr(), out.as_ptr(),
		out.len()) == 0);
	assert_eq!(&out2[..], &msg[..]);
}

unsafe fn test_split_data<AES:AesKey>(key: AesGcmKey<AES>, nonce: &[u8;12], ad: &[u8], msg: &[u8], fast: bool)
{
	let lowkey = from_raw_parts(&key as *const _ as *const u8, AES::keylength());
	let refcipher = encrypt_aes_gcm_ref(lowkey, nonce, ad, msg);
	let mut out = vec![0;msg.len()+16];
	let mut out2 = vec![0;msg.len()];
	let mut input = vec![0;msg.len()];
	//Test inplace.
	(&mut out[..msg.len()]).copy_from_slice(msg);
	key.encrypt(nonce.as_ptr(), ad.as_ptr(), ad.len(), out.as_ptr(), out.as_mut_ptr(), msg.len(), 0);
	assert_eq!(&out[..], &refcipher[..]);
	//Test all possible splits.
	for split in 0..=msg.len() {
		//Only try all or none in fast mode.
		if fast && split != 0 && split != msg.len() { continue; }
		for i in 0..split { input[i] = msg[i]; out[i] = 0; }
		for i in split..msg.len() { out[i] = msg[i]; input[i] = 0; }
		for i in msg.len()..msg.len()+16 { out[i] = 0; }
		key.encrypt(nonce.as_ptr(), ad.as_ptr(), ad.len(), input.as_ptr(), out.as_mut_ptr(), split,
			msg.len() - split);
		assert_eq!(&out[..], &refcipher[..]);
	}
	//Test forgeries.
	let mut forged = refcipher.clone();
	for i in 0..8*refcipher.len() {
		//Not in fast mode.
		if fast { break; }
		forged[i/8] ^= 1 << i % 8;
		assert!(key.decrypt(nonce.as_ptr(), ad.as_ptr(), ad.len(), out2.as_mut_ptr(), forged.as_ptr(),
			refcipher.len()) == -1);
		forged[i/8] ^= 1 << i % 8;
	}
	//Test decryption, inplace and out-of-place.
	assert!(key.decrypt(nonce.as_ptr(), ad.as_ptr(), ad.len(), out2.as_mut_ptr(), refcipher.as_ptr(),
		refcipher.len()) == 0);
	assert_eq!(&out2[..], &msg[..]);
	assert!(key.decrypt(nonce.as_ptr(), ad.as_ptr(), ad.len(), out.as_mut_ptr(), out.as_ptr(),
		refcipher.len()) == 0);
	assert_eq!(&out[..msg.len()], &msg[..]);
}

fn test_split_data_random_keys(adlen: usize, msglen: usize, fast: bool)
{
	unsafe {
		let mut key1 = [0;16];
		let mut key2 = [0;24];
		let mut key3 = [0;32];
		let mut nonce = [0;12];
		let mut ad = vec![0;adlen];
		let mut msg = vec![0;msglen];
		secure_random(&mut key1);
		secure_random(&mut key2);
		secure_random(&mut key3);
		secure_random(&mut nonce);
		secure_random(&mut ad);
		secure_random(&mut msg);
		let key1 = AesGcmKey::<Aes128Key>::keygen(key1.as_ptr());
		let key2 = AesGcmKey::<Aes192Key>::keygen(key2.as_ptr());
		let key3 = AesGcmKey::<Aes256Key>::keygen(key3.as_ptr());
		test_split_data(key1, &nonce, &ad, &msg, fast);
		test_split_data(key2, &nonce, &ad, &msg, fast);
		test_split_data(key3, &nonce, &ad, &msg, fast);
	}
}

fn bad_alignment<'a>(buf: &'a mut [u8]) -> &'a mut [u8]
{
	let xlen = buf.len() - 16;
	let xalign = (17 - (buf.as_ptr() as usize) % 16) % 16;
	&mut buf[xalign..][..xlen]
}

//Align things as badly as possible.
fn test_align_worst(adlen: usize, msglen: usize)
{
	unsafe {
		let mut key1 = [0;32]; let key1 = bad_alignment(&mut key1);
		let mut key2 = [0;40]; let key2 = bad_alignment(&mut key2);
		let mut key3 = [0;48]; let key3 = bad_alignment(&mut key3);
		let mut nonce = [0;28]; let nonce = bad_alignment(&mut nonce);
		let mut ad = vec![0;adlen+16]; let ad = bad_alignment(&mut ad);
		let mut msg = vec![0;msglen+16]; let msg = bad_alignment(&mut msg);
		secure_random(key1);
		secure_random(key2);
		secure_random(key3);
		secure_random(nonce);
		secure_random(ad);
		secure_random(msg);
		let key1 = AesGcmKey::<Aes128Key>::keygen(key1.as_ptr());
		let key2 = AesGcmKey::<Aes192Key>::keygen(key2.as_ptr());
		let key3 = AesGcmKey::<Aes256Key>::keygen(key3.as_ptr());
		roundtrip_test(key1, nonce, ad, msg);
		roundtrip_test(key2, nonce, ad, msg);
		roundtrip_test(key3, nonce, ad, msg);
	}
}

#[test]
fn test_split_data_0_256()
{
	for size in 0..=256 {
		test_split_data_random_keys(0, size, false);
	}
}

#[test]
fn test_split_data_pot()
{
	for size1 in 8..=14 {
		for size2 in 0..=32 {
			test_split_data_random_keys(0, (1 << size1) + size2, true);
		}
	}
}

#[test]
fn test_ad_0_256()
{
	for size in 0..=256 {
		test_split_data_random_keys(size, 0, false);
	}
}

#[test]
fn test_ad_pot()
{
	for size1 in 8..=14 {
		for size2 in 0..=32 {
			test_split_data_random_keys((1 << size1) + size2, 0, false);
		}
	}
}

#[test]
fn test_bad_align()
{
	for size in 0..=4096 { test_align_worst(128, size); }
}

#[test]
fn test_rand_size()
{
	test_split_data_random_keys(137, 274, false);
}
