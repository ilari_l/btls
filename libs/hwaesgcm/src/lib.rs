//!Hardware AES-GCM support
//!
//!This crate contains hardware-accelerated implementations of AES-GCM
//!
//!The most important items are:
//!
//! * Context for AES-128-GCM: [`Aes128GcmContext`](struct.Aes128GcmContext.html)
//! * Context for AES-192-GCM: [`Aes192GcmContext`](struct.Aes192GcmContext.html)
//! * Context for AES-256-GCM: [`Aes256GcmContext`](struct.Aes256GcmContext.html)
#![allow(unsafe_code)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]

pub use btls_aux_aead_base::AeadError;
pub use btls_aux_aead_base::LowLevelAeadContext;
use btls_aux_fail::fail_if;
use btls_aux_hwaesgcm_cimpl::*;
use core::slice::from_raw_parts_mut;

///Human-readable name of AES-128-GCM.
pub static AES128GCM_NAME: &'static str = "AES-128-GCM";
///Human-readable name of AES-192-GCM.
pub static AES192GCM_NAME: &'static str = "AES-192-GCM";
///Human-readable name of AES-256-GCM.
pub static AES256GCM_NAME: &'static str = "AES-256-GCM";

///Does the system support hardware AES?
///
///Note that all other methods in this crate fail with an internal error if hardware AES is not supported.
pub fn hw_aes_supported() -> bool
{
	unsafe{aes_gcm_supported() >= 0}
}

///Key for AES-128-GCM encryption/decryption.
///
///This structure contains the key used for AES-128-GCM encryption and decryption. It also forms a
///low-level context for AES-128-GCM, and thus implements [`LowLevelAeadContext`].
///
///This structure is created by the [`LowLevelAeadContext::new_context()`] function.
///
///On release, the key material is wiped from memory.
///
///The supported methods are:
///
/// * Traits:
///   * `Drop`
///   * [`LowLevelAeadContext`]
///
///[`LowLevelAeadContext`]: trait.LowLevelAeadContext.html
///[`LowLevelAeadContext::new_context()`]: trait.LowLevelAeadContext.html#tymethod.new_context
pub struct Aes128GcmContext(AesGcmKey<Aes128Key>);

///Key for AES-192-GCM encryption/decryption.
///
///This structure contains the key used for AES-192-GCM encryption and decryption. It also forms a
///low-level context for AES-192-GCM, and thus implements [`LowLevelAeadContext`].
///
///This structure is created by the [`LowLevelAeadContext::new_context()`] function.
///
///On release, the key material is wiped from memory.
///
///The supported methods are:
///
/// * Traits:
///   * `Drop`
///   * [`LowLevelAeadContext`]
///
///[`LowLevelAeadContext`]: trait.LowLevelAeadContext.html
///[`LowLevelAeadContext::new_context()`]: trait.LowLevelAeadContext.html#tymethod.new_context
pub struct Aes192GcmContext(AesGcmKey<Aes192Key>);

///Key for AES-256-GCM encryption/decryption.
///
///This structure contains the key used for AES-256-GCM encryption and decryption. It also forms a
///low-level context for AES-256-GCM, and thus implements [`LowLevelAeadContext`].
///
///This structure is created by the [`LowLevelAeadContext::new_context()`] function.
///
///On release, the key material is wiped from memory.
///
///The supported methods are:
///
/// * Traits:
///   * `Drop`
///   * [`LowLevelAeadContext`]
///
///[`LowLevelAeadContext`]: trait.LowLevelAeadContext.html
///[`LowLevelAeadContext::new_context()`]: trait.LowLevelAeadContext.html#tymethod.new_context
pub struct Aes256GcmContext(AesGcmKey<Aes256Key>);

const P_MAX: u64 = 68719476704;
const C_MAX: u64 = 68719476720;

macro_rules! aesgcm_impl
{
	($clazz:ident, $name:expr, $keysize:expr) => {
		impl LowLevelAeadContext for $clazz
		{
			fn new_context(key: &[u8]) -> Result<$clazz, AeadError>
			{
				AeadError::assert_key($name, key, |n|n==$keysize)?;

				fail_if!(unsafe{aes_gcm_supported()} < 0, AeadError::internal_fault($name));
				let inner = unsafe{AesKey::keygen(key.as_ptr())};
				Ok($clazz(inner))
			}
			fn encrypted_size(&self, size: usize) -> Option<usize>
			{
				AeadError::assert_plaintext("", size, 16, P_MAX).ok()
			}
			fn max_plaintext_size(&self, size: usize) -> Option<usize>
			{
				AeadError::assert_ciphertext("", size, C_MAX, |s|s.checked_sub(16)).ok()
			}
			unsafe fn encrypt3(&self, nonce: &[u8], ad: &[u8], optr: *mut u8, iptr: *const u8,
				ilen: usize, trailerlen: usize) -> Result<usize, AeadError>
			{
				//No need to check if AES-GCM is supported, because it is impossible to reach here
				//if no, because the receiver is unconstructable in that case.
				AeadError::assert_nonce($name, nonce, |n|n==12)?;
				AeadError::assert_ad($name, ad, 0x1FFFFFFFFFFFFFFFu64)?;
				let reallen = ilen.saturating_add(trailerlen);
				let tagend = AeadError::assert_plaintext($name, reallen, 16, P_MAX)?;
				unsafe {
					let nptr = nonce.as_ptr();
					let adlen = ad.len();
					let adptr = ad.as_ptr();
					self.0.encrypt(nptr, adptr, adlen, iptr, optr, ilen, trailerlen);
				}
				Ok(tagend)
			}
			unsafe fn decrypt3(&self, nonce: &[u8], ad: &[u8], optr: *mut u8, iptr: *const u8,
				ilen: usize) -> Result<usize, AeadError>
			{
				//No need to check if AES-GCM is supported, because it is impossible to reach here
				//if no, because the receiver is unconstructable in that case.
				AeadError::assert_nonce($name, nonce, |n|n==12)?;
				AeadError::assert_ad($name, ad, 0x1FFFFFFFFFFFFFFFu64)?;
				//Tag is 16 bytes.
				let rawlen = AeadError::assert_ciphertext($name, ilen, C_MAX, |s|s.checked_sub(16))?;
				let code = unsafe{
					let nptr = nonce.as_ptr();
					let adlen = ad.len();
					let adptr = ad.as_ptr();
					self.0.decrypt(nptr, adptr, adlen, optr, iptr, ilen)
				};
				AeadError::assert_tag_ok(code >= 0, unsafe{from_raw_parts_mut(optr, rawlen)})?;
				Ok(rawlen)
			}
		}
	}
}

aesgcm_impl!(Aes128GcmContext, AES128GCM_NAME, 16);
aesgcm_impl!(Aes192GcmContext, AES192GCM_NAME, 24);
aesgcm_impl!(Aes256GcmContext, AES256GCM_NAME, 32);

#[cfg(test)]
mod test;
