//!Bindings to btls modules
//!
//!This crate provodes items for implementing `btls` key modules in Rust.
//!
//!
//!The most important items are:
//!
//! * Key Handle trait: [`HandleType`](trait.HandleType.html)
//! * Helper macro for defining the external functions: [`btls_module`](macro.btls_module.html)
//!
//!Use like follows:
//!
//!```no_run
//!#[macro_use]
//!extern crate btls_aux_keypair_local_module_definitions;
//!use btls_aux_keypair_local_module_definitions::{HandleType, PubkeyReply, SignatureReply};
//!
//!#[derive(Clone)]
//!struct FooKeyHandle
//!{
//!	//...
//!}
//!
//!impl HandleType for FooKeyHandle
//!{
//!	//Implement get_key_handle, request_sign and request_pubkey.
//!}
//!
//!btls_module!(FooKeyHandle);
//!```

#![warn(unsafe_op_in_unsafe_fn)]
use btls_aux_fail::f_return;
use btls_aux_collections::GlobalMutex;
use btls_aux_collections::GlobalRwLock;
use std::any::TypeId;
use std::collections::BTreeMap;
use std::marker::PhantomData;
use std::mem::drop;
use std::mem::transmute;
use std::ops::Deref;
use std::ptr::null;
use std::ptr::null_mut;
use std::panic::catch_unwind;
use std::panic::AssertUnwindSafe;
use std::ptr::read as ptr_read;
use std::slice::from_raw_parts;
use std::str::from_utf8;


///RSA PKCS#1 v1.5 signature with SHA-256.
pub const BTLS_ALG_RSA_PKCS1_SHA256: u16 = 0x0401;
///RSA PKCS#1 v1.5 signature with SHA-384.
pub const BTLS_ALG_RSA_PKCS1_SHA384: u16 = 0x0501;
///RSA PKCS#1 v1.5 signature with SHA-512.
pub const BTLS_ALG_RSA_PKCS1_SHA512: u16 = 0x0601;
///RSA PSS signature with SHA-256 hash and MGF1, 32 byte salt.
pub const BTLS_ALG_RSA_PSS_SHA256: u16 = 0x0804;
///RSA PSS signature with SHA-384 hash and MGF1, 48 byte salt.
pub const BTLS_ALG_RSA_PSS_SHA384: u16 = 0x0805;
///RSA PSS signature with SHA-512 hash and MGF1, 64 byte salt.
pub const BTLS_ALG_RSA_PSS_SHA512: u16 = 0x0806;
///ECDSA with P-256 curve and SHA-256 hash.
pub const BTLS_ALG_ECDSA_P256_SHA256: u16 = 0x0403;
///ECDSA with P-384 curve and SHA-384 hash.
pub const BTLS_ALG_ECDSA_P384_SHA384: u16 = 0x0503;
///ECDSA with P-521 curve and SHA-512 hash.
pub const BTLS_ALG_ECDSA_P521_SHA512: u16 = 0x0603;
///Ed25519
pub const BTLS_ALG_ED25519: u16 = 0x0807;
///Ed448
pub const BTLS_ALG_ED448: u16 = 0x0808;

struct CStr<'a>(&'a [u8]);

impl<'a> CStr<'a>
{
	fn from_ptr(x: *const u8) -> CStr<'a>
	{
		let mut len = 0usize;
		while unsafe{ptr_read(x.add(len)) != 0 } { len += 1; }
		CStr(unsafe{from_raw_parts(x, len)})
	}
	fn to_bytes(&self) -> &'a [u8] { self.0 }
	fn to_string(&self) -> Option<&'a str> { from_utf8(self.0).ok() }
}

struct CString(Vec<u8>);

impl CString
{
	fn new(x: &str) -> Result<CString, ()>
	{
		if x.chars().any(|x|x=='\0') { return Err(()); }
		let mut y = x.as_bytes().to_owned();
		y.push(0);
		Ok(CString(y))
	}
	fn as_ptr(&self) -> *const u8 { self.0.as_ptr() }
}

///Trait for reflecting signature request back to Rust on calling side.
pub trait SignatureReplyFuncs: Sized+Send
{
	///Finish a request.
	fn finish(&mut self, signature: Option<&[u8]>);
	///Log a warning/error.
	fn log(&mut self, msg: &[u8]);
}

///Trait for reflecting pubkey request back to Rust on calling side.
pub trait PubkeyReplyFuncs: Sized
{
	///Finish a request.
	fn finish(&mut self, pubkey: &[u8], schemes: &[u16]);
}

extern "C" fn srf_finish_trampoline<T:SignatureReplyFuncs>(ctx: *mut SignatureReplyInner, data: *const u8,
	datalen: usize)
{
	unsafe {
		let ctx = transmute::<_,*mut T>(ctx);	//The ctx is really of type T, not SignatureReplyInner.
		if !data.is_null() {
			let data = from_raw_parts(data, datalen);
			(*ctx).finish(Some(data));
		} else {
			(*ctx).finish(None);
		}
		//Actually ctx is a box. Drop it on the floor.
		drop(Box::from_raw(ctx));
	}
}

extern "C" fn srf_log_trampoline<T:SignatureReplyFuncs>(ctx: *mut SignatureReplyInner, message: *const u8)
{
	unsafe {
		let ctx = transmute::<_,*mut T>(ctx);	//The ctx is really of type T, not SignatureReplyInner.
		let message = CStr::from_ptr(message);
		(*ctx).log(message.to_bytes());
	}
}

extern "C" fn prf_finish_trampoline<T:PubkeyReplyFuncs>(ctx: *mut PubkeyReplyInner, pubkey: *const u8,
	pubkeylen: usize, schemes: *const u16, schemescount: usize)
{
	unsafe {
		let ctx = transmute::<_,*mut T>(ctx);	//The ctx is really of type T, not PubkeyReplyInner.
		let pubkey = from_raw_parts(pubkey, pubkeylen);
		let schemes = from_raw_parts(schemes, schemescount);
		(*ctx).finish(pubkey, schemes);
	}
}

#[repr(C)]
struct SignatureReplyInner;
#[repr(C)]
struct PubkeyReplyInner;

///The signature reply
///
///If this structure is dropped without answering the signature using the [`finish()`](#method.finish), the
///signature is deemed failed.
///
///It is safe to transfer this object between threads and delay calling [`finish()`](#method.finish) until after
///the original signature request function has returned to its caller.
#[repr(C)]
pub struct SignatureReply
{
	ctx: *mut SignatureReplyInner,
	_finish: extern "C" fn(*mut SignatureReplyInner, *const u8, usize),
	_log: extern "C" fn(*mut SignatureReplyInner, *const u8)
}

unsafe impl Send for SignatureReply {}
unsafe impl Sync for SignatureReply {}

impl Drop for SignatureReply
{
	fn drop(&mut self)
	{
		//If not answered yet, answer with error.
		if !self.ctx.is_null() { (self._finish)(self.ctx, null(), 0) }
	}
}

impl SignatureReply
{
	///Create a new signature reply context.
	///
	///This is only meant for directly using modules written in Rust from Rust code.
	pub fn create_from<T:SignatureReplyFuncs>(ctx: Box<T>) -> SignatureReply
	{
		SignatureReply {
			ctx: unsafe{transmute(Box::into_raw(ctx))},
			_finish: srf_finish_trampoline::<T>,
			_log: srf_log_trampoline::<T>,
		}
	}
	///Finish signature computation and return signature `signature`.
	///
	///If the signature is `Some(x)`, then the signature is successful and returns `x`. If the signature is
	///`None`, then the signature failed. Notice that this method consumes its receiver, and thus can be called
	///only once.
	pub fn finish(mut self, signature: Option<&[u8]>)
	{
		match signature {
			Some(x) => (self._finish)(self.ctx, x.as_ptr(), x.len()),
			None => (self._finish)(self.ctx, null(), 0),
		};
		//Nasty hack: Set context to NULL, so we know on dropping SignatureReply that it has already been
		//answered.
		self.ctx = null_mut();
	}
	///Log a message `message`.
	///
	///Messages are in general meant for error or warning conditions, not for normal operation.
	pub fn log(&self, message: &str)
	{
		let tmp = match CString::new(message) { Ok(x) => x, Err(_) => return };
		(self._log)(self.ctx, tmp.as_ptr());
	}
}

///The public key reply
///
///Note that these requests **must** be answered (using [`finish()`](#method.finish) before the method this handle
///is passed to returns. Failure to do so causes the request to be deemed failed.
#[repr(C)]
pub struct PubkeyReply<'a>
{
	ctx: *mut PubkeyReplyInner,
	_finish: extern "C" fn(*mut PubkeyReplyInner, pubkey: *const u8, pubkeylen: usize, schemes: *const u16,
		schemescount: usize),
	_phantom: PhantomData<&'a u8>	//So we can have lifetime argument.
}

impl<'a> PubkeyReply<'a>
{
	///Create a new pubkey reply context.
	///
	///This is only meant for directly using modules written in Rust from Rust code.
	pub fn create_from<T:PubkeyReplyFuncs>(ctx: &'a mut T) -> PubkeyReply<'a>
	{
		PubkeyReply {
			ctx: unsafe{transmute(ctx)},
			_finish: prf_finish_trampoline::<T>,
			_phantom: PhantomData,
		}
	}
	///Return a public key `pubkey` supporting schemes `schmes`.
	///
	///The public key is in X.509 SubjectPublicKeyInfo format, including the leading SEQUENCE header. The
	///schemes are list of TLS SignatureSchme values.
	pub fn finish(self, pubkey: &[u8], schemes: &[u16])
	{
		(self._finish)(self.ctx, pubkey.as_ptr(), pubkey.len(), schemes.as_ptr(), schemes.len());
	}
}

//The hidden type MUST be Send, Sized, Clone and 'static.
struct TypeErased
{
	object: *mut u8,
	of: TypeId,
}

impl TypeErased
{
	fn new<T:Send+Sized+Clone+'static>(obj: &T) -> TypeErased
	{
		let of = TypeId::of::<T>();
		let object = Box::new(obj.clone());
		let object = Box::into_raw(object) as *mut u8;
		TypeErased { object, of }
	}
	fn get<T:Send+Sized+Clone+'static>(&self) -> Option<T>
	{
		let selftype = TypeId::of::<T>();
		if self.of != selftype { return None; }	//Wrong type!
		let obj: &T = unsafe{transmute::<_,&T>(self.object)};
		Some(obj.clone())
	}
}

//This is send/sync with all handle types.
unsafe impl Send for TypeErased {}
unsafe impl Sync for TypeErased {}

fn with_trap<T:Sized, F: FnOnce() -> T>(cb: F, on_ex: T) -> T
{
	match catch_unwind(AssertUnwindSafe(||cb())) {
		Ok(x) => x,
		Err(_) => on_ex
	}
}

static NAME_MAP: GlobalRwLock<BTreeMap<String, u64>> = GlobalRwLock::new();
static ITEM_MAP: GlobalRwLock<BTreeMap<u64, TypeErased>> = GlobalRwLock::new();
static NEXT_INDEX: GlobalMutex<u64> = GlobalMutex::new();
const INVALID_HANDLE: u64 = 0xFFFFFFFFFFFFFFFFu64;

///Trait that key handle types implement.
///
///Note that all of these methods can be called from arbitrary thread.
pub trait HandleType: Send+Clone+Sized+'static
{
	///Get the key handle for key named `_name`.
	///
	///One can return a separate structure for each lookup: This method should not be called more than once
	///for each key. In case of failure, it is useful to assign `_errcode` to some sort of internal error code
	///in order to help debugging.
	///
	///By default this method fails (returns `None`). This is useful as this method is alternate with
	///[`get_key_handle2()`](#method.get_key_handle2)
	///
	///On success returns `Some(x)`, where `x` is the key handle. Otherwise returns `None`.
	fn get_key_handle(_name: &str, _errcode: &mut u32) -> Option<Self> { None }
	///Get the key handle to inline key with contents `_data`.
	///
	///Note that unlike get_key_handle, the returned objects are not cached. Nevertheless, this should not be
	///called repeatedly with the same key data. In case of failure, it is useful to assign `_errcode` to some
	///sort of internal error code in order to help debugging.
	///
	///By default this method fails (returns `None`). This is useful as this method is alternate with
	///[`get_key_handle()`](#method.get_key_handle)
	///
	///On success, returns `Some(x)`, where `x` is the key handle. Otherwise returns `None`.
	fn get_key_handle2(_data: &[u8], _errcode: &mut u32) -> Option<Self> { None }
	///Request a signature for data `data` using algorithm `algorithm`. Context structure `reply` is provoded
	///for returning the signature.
	///
	///The call to [`reply.finish()`](struct.SignatureReply.html#method.finish) MAY be delayed and occur in
	///another thread after this call has returned. If the reply context is dropped without calling
	///[ `reply.finish()`](struct.SignatureReply.html#method.finish), the signature operation is assumed to
	///have failed.
	fn request_sign(&self, reply: SignatureReply, algorithm: u16, data: &[u8]);
	///Request a public key corresponding to this handle. Context structure `reply` is provoded
	///for returning the public key.
	///
	///If [`reply.finish()`](struct.PubkeyReply.html#method.finish) is not called during execution of this
	///routine, the request is deemed failed. Because of this, transferring the handle to another structure is
	///not useful.
	fn request_pubkey<'a>(&self, reply: PubkeyReply<'a>);
	#[doc(hidden)]
	fn name_to_handle(name: &str) -> Option<u64>
	{
		NAME_MAP.with_read(|x|x.get(name).map(|x|*x))
	}
	#[doc(hidden)]
	fn handle_to_raw(&self, name: Option<&str>) -> u64
	{
		NAME_MAP.with_write(|names|ITEM_MAP.with_write(|items|NEXT_INDEX.with(|next|{
			let newidx = *next;
			//If this would overflow, that means that handles have been exhausted.
			*next = f_return!(next.checked_add(1), INVALID_HANDLE);
			name.map(|_name|names.insert(_name.to_owned(), newidx));
			items.insert(newidx, TypeErased::new(self));
			newidx
		})))
	}
	#[doc(hidden)]
	fn handle_from_raw(h: u64) -> Option<Self>
	{
		ITEM_MAP.with_read(|x|x.get(&h).and_then(|y|y.get::<Self>()))
	}
	#[doc(hidden)]
	unsafe fn with_get_key_handle(name: *const u8, errcode: *mut u32) -> u64
	{
		//Treat handles that are not valid UTF-8 as invalid.
		let name = match CStr::from_ptr(name).to_string() {
			Some(name) => name,
			None => {
				unsafe{errcode.write(0x75746638);}
				return INVALID_HANDLE;
			},
		};
		//Check name cache.
		if let Some(x) = Self::name_to_handle(name.deref()) { return x; }
		//Named.
		let errcode = unsafe{&mut *errcode};
		f_return!(with_trap(||Self::get_key_handle(name.deref(), errcode), None), INVALID_HANDLE).
			handle_to_raw(Some(name.deref()))
	}
	#[doc(hidden)]
	unsafe fn with_get_key_handle2(data: *const u8, datalen: usize, errcode: *mut u32) -> u64
	{
		let data = unsafe{from_raw_parts(data, datalen)};
		//No name.
		let errcode = unsafe{&mut *errcode};
		f_return!(with_trap(||Self::get_key_handle2(data, errcode), None), INVALID_HANDLE).
			handle_to_raw(None)
	}
	#[doc(hidden)]
	unsafe fn with_request_sign(handle: u64, reply: SignatureReply, algorithm: u16, data: *const u8,
		datalen: usize)
	{
		let data = unsafe{from_raw_parts(data, datalen)};
		match Self::handle_from_raw(handle) {
			Some(x) => with_trap(||{x.request_sign(reply, algorithm, data)}, ()),
			None => reply.finish(None)	//NAK the signature.
		}
	}
	#[doc(hidden)]
	unsafe fn with_request_pubkey<'a>(handle: u64, reply: PubkeyReply<'a>)
	{
		if let Some(x) = Self::handle_from_raw(handle) {
			with_trap(||{x.request_pubkey(reply)}, ())
		}
	}
}

///Helper macro for defining the required external functions for a module with handle type `$name`.
///
///See the example at top of the [crate](index.html) for example code.
#[macro_export]
macro_rules! btls_module {
	($name:ident) => {
#[no_mangle]
pub unsafe extern "C" fn btls_mod_get_key_handle(a: *const u8, b: *mut u32) -> u64
{
	$name::with_get_key_handle(a, b)
}

#[no_mangle]
pub unsafe extern "C" fn btls_mod_get_key_handle2(a: *const u8, b: usize, c: *mut u32) -> u64
{
	$name::with_get_key_handle2(a, b, c)
}

#[no_mangle]
pub unsafe extern "C" fn btls_mod_request_sign(a: u64, b: SignatureReply, c: u16, d: *const u8, e: usize)
{
	$name::with_request_sign(a, b, c, d, e)
}

#[no_mangle]
pub unsafe extern "C" fn btls_mod_request_pubkey(a: u64, b: PubkeyReply)
{
	$name::with_request_pubkey(a, b)
}
	}
}

#[cfg(test)]
mod test;
