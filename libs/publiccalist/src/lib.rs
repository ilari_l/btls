//!This crate provodes a list of public CAs.
//!
//!The important items include:
//!
//! * Check if given name refers to public CA [`is_public_ca()`](fn.is_public_ca.html)
//! * Check if given CA certificate is known to be issued by a CA [`known_signature()`](fn.known_signature.html)
//! * Check if given CA certificate is revoked [`is_revoked()`](fn.is_revoked.html)
//! * Check if given CA certificate is known [`is_public_intermediate()`](fn.is_public_intermediate.html)
#![no_std]
#![forbid(missing_docs)]
#![forbid(unsafe_code)]
#![warn(unsafe_op_in_unsafe_fn)]
#[cfg(test)] #[macro_use] extern crate std;

use btls_aux_hash::Hash;
use btls_aux_hash::Sha256;
use core::cmp::Ordering;

static CA_EVER_SEEN: &'static [u8] = include_bytes!("public-certificates-seen.dat");
static PUBLIC_CERTIFICATES_EXP: &'static [u8] = include_bytes!("public-certificates-expiry.dat");
static PUBLIC_CERTIFICATES: &'static [u8] = include_bytes!("public-certificates.dat");
static SIGNATURE_HASHES: &'static [u8] = include_bytes!("signature-hashes.dat");
static CRL_ISSUER_SERIAL: &'static [u8] = include_bytes!("crl-issuer-serial.dat");
static CRL_PUBKEY: &'static [u8] = include_bytes!("crl-pubkey.dat");
static PUBLIC_INTERMEDIATES: &'static [u8] = include_bytes!("public-intermediates.dat");
static TA_BLACKLIST: &'static [u8] = include_bytes!("ta-blacklist.dat");
static BUILD_DATE: &[u8;8] = include_bytes!("build-date.dat");

fn search_list_index(l: &[u8], x: &[u8]) -> Option<usize>
{
	if x.len() != 32 { return None; }	//Entries are always 32 octets.
	let mut lbound = 0;		//Inclusive lower bound.
	let mut ubound = l.len();	//Exclusive upper bound.
	while lbound < ubound {
		let xtry = (lbound + ubound) >> 6 << 5;	//Always multiple of 32.
		if xtry > l.len() || l.len() - xtry < 32 { return None; }		//Can't happen!
		//Above guarantees these are in-range.
		let candidate = &l[xtry..][..32];
		match candidate.cmp(x) {
			//candidate < target. Search list after. Add +32 to not research this one.
			Ordering::Less => lbound = xtry + 32,
			//candidate > target. Search list before. No +32 because exclusive bound.
			Ordering::Greater => ubound = xtry,
			//candidate = target. Found it.
			Ordering::Equal => return Some(xtry >> 5)
		}
	}
	//If the above loop did not find match, it does not exist.
	None
}


fn search_list(l: &[u8], x: &[u8]) -> bool
{
	search_list_index(l,x).is_some()
}

///Determine if `spki` is on trust anchor blacklist.
///
///The spki includes the leading SEQUENCE header. Thus it always has `0x30` as the first byte.
pub fn is_ta_blacklisted(spki: &[u8]) -> bool
{
	search_list(&TA_BLACKLIST[..], &Sha256::hash(spki))
}

///Determine if `issuer` refers to a known public CA?
///
///The name includes the leading SEQUENCE header. Thus it always has `0x30` as the first byte.
pub fn is_public_ca(issuer: &[u8]) -> bool
{
	issuer.len() > 0 && search_list(&PUBLIC_CERTIFICATES[..], &Sha256::hash(issuer))
}

///Determine if `issuer` refers to expired CA.
pub fn is_expired_ca(issuer: &[u8], ts: i64) -> bool
{
	let h = Sha256::hash(issuer);
	//assume CAs never heard about are not expired.
	if !search_list(CA_EVER_SEEN, &h) { return false; }
	//If not found from PUBLIC_CERTIFICATES, assume expired.
	match search_list_index(PUBLIC_CERTIFICATES, &h).and_then(|idx|PUBLIC_CERTIFICATES_EXP.get(8*idx..8*idx+8)) {
		Some(e) if e.len() == 8 => {
			let mut t = [0;8];
			t.copy_from_slice(e);
			ts > i64::from_le_bytes(t)
		},
		_ => true	//Not found, expired.
	}
}

///Determine if `(issuer, issuerkey, certificate)` tuple is on list of known public CA certificates.
///
///This list encompasses all the known unconstrained TLS root and intermediate CAs issued by any public CAs. It
///does not encompass constrained sub-CAs, nor end-entity certificates.
pub fn known_signature(issuer: &[u8], issuerkey: &[u8], certificate: &[u8]) -> bool
{
	search_list(&SIGNATURE_HASHES[..], &Sha256::hash_v(&[issuer, issuerkey, certificate]))
}

///Determine if `(issuer, serial, pubkey)` tuple is on list of revoked certificates.
///
///The public key `pubkey` is expected to be the public key of the certificate to check, *not* the public key of
///the issuer.
///
///This list is a revocation list of public CA certificates.
pub fn is_revoked(issuer: &[u8], serial: &[u8], pubkey: &[u8]) -> bool
{
	search_list(&CRL_ISSUER_SERIAL[..], &Sha256::hash_v(&[issuer, serial])) ||
		search_list(&CRL_PUBKEY[..], &Sha256::hash(pubkey))
}

///Determine if `certificate` is on list of known public CA certificates.
///
///This list encompasses all known TLS root and intermediate CAs issued by any public CAs. It
///does not encompass constrained sub-CAs, nor end-entity certificates. This differs from
///[`known_signature()`](fn.known_signature.html) function by not considering the issuer of the certificate.
pub fn is_public_intermediate(certificate: &[u8]) -> bool
{
	search_list(&PUBLIC_INTERMEDIATES[..], &Sha256::hash(certificate))
}

///Get the build date as unix day number (unix_ts / 86400)
pub fn get_build_date() -> i64 { i64::from_le_bytes(*BUILD_DATE) }


#[test]
fn lets_encrypt()
{
	assert!(is_public_ca(&[48, 79, 49, 11, 48, 9, 6, 3, 85, 4, 6, 19, 2, 85, 83, 49, 41, 48, 39, 6, 3, 85, 4,
		10, 19, 32, 73, 110, 116, 101, 114, 110, 101, 116, 32, 83, 101, 99, 117, 114, 105, 116, 121, 32, 82,
		101, 115, 101, 97, 114, 99, 104, 32, 71, 114, 111, 117, 112, 49, 21, 48, 19, 6, 3, 85, 4, 3, 19, 12,
		73, 83, 82, 71, 32, 82, 111, 111, 116, 32, 88, 49][..]));	//X1
	assert!(is_public_ca(&[48, 79, 49, 11, 48, 9, 6, 3, 85, 4, 6, 19, 2, 85, 83, 49, 41, 48, 39, 6, 3, 85, 4,
		10, 19, 32, 73, 110, 116, 101, 114, 110, 101, 116, 32, 83, 101, 99, 117, 114, 105, 116, 121, 32, 82,
		101, 115, 101, 97, 114, 99, 104, 32, 71, 114, 111, 117, 112, 49, 21, 48, 19, 6, 3, 85, 4, 3, 19, 12,
		73, 83, 82, 71, 32, 82, 111, 111, 116, 32, 88, 50][..]));	//X2
	assert!(is_public_ca(&[48, 50, 49, 11, 48, 9, 6, 3, 85, 4, 6, 19, 2, 85, 83, 49, 22, 48, 20, 6, 3, 85, 4, 10,
		19, 13, 76, 101, 116, 39, 115, 32, 69, 110, 99, 114, 121, 112, 116, 49, 11, 48, 9, 6, 3, 85, 4, 3,
		19, 2, 69, 49][..]));	//E1
	assert!(is_public_ca(&[48, 50, 49, 11, 48, 9, 6, 3, 85, 4, 6, 19, 2, 85, 83, 49, 22, 48, 20, 6, 3, 85, 4, 10,
		19, 13, 76, 101, 116, 39, 115, 32, 69, 110, 99, 114, 121, 112, 116, 49, 11, 48, 9, 6, 3, 85, 4, 3,
		19, 2, 69, 50][..]));	//E2
	assert!(is_public_ca(&[48, 50, 49, 11, 48, 9, 6, 3, 85, 4, 6, 19, 2, 85, 83, 49, 22, 48, 20, 6, 3, 85, 4, 10,
		19, 13, 76, 101, 116, 39, 115, 32, 69, 110, 99, 114, 121, 112, 116, 49, 11, 48, 9, 6, 3, 85, 4, 3,
		19, 2, 82, 51][..]));	//R3
	assert!(is_public_ca(&[48, 50, 49, 11, 48, 9, 6, 3, 85, 4, 6, 19, 2, 85, 83, 49, 22, 48, 20, 6, 3, 85, 4, 10,
		19, 13, 76, 101, 116, 39, 115, 32, 69, 110, 99, 114, 121, 112, 116, 49, 11, 48, 9, 6, 3, 85, 4, 3,
		19, 2, 82, 52][..]));	//R4
	assert!(!is_public_ca(&[48, 79, 49, 11, 48, 9, 6, 3, 85, 4, 6, 19, 2, 85, 83, 49, 41, 48, 39, 6, 3, 85, 4,
		10, 19, 32, 73, 110, 116, 101, 114, 110, 101, 116, 32, 83, 101, 99, 117, 114, 105, 116, 121, 32, 82,
		101, 115, 101, 97, 114, 99, 104, 32, 71, 114, 111, 117, 112, 49, 21, 48, 19, 6, 3, 85, 4, 3, 19, 12,
		73, 83, 82, 71, 32, 82, 111, 111, 116, 32, 88, 47][..]));	//X?
	assert!(!is_public_ca(&[48, 50, 49, 11, 48, 9, 6, 3, 85, 4, 6, 19, 2, 85, 83, 49, 22, 48, 20, 6, 3, 85, 4,
		10, 19, 13, 76, 101, 116, 39, 115, 32, 69, 110, 99, 114, 121, 112, 116, 49, 11, 48, 9, 6, 3, 85, 4,
		3, 19, 2, 69, 47][..]));	//E?
	assert!(!is_public_ca(&[48, 50, 49, 11, 48, 9, 6, 3, 85, 4, 6, 19, 2, 85, 83, 49, 22, 48, 20, 6, 3, 85, 4,
		10, 19, 13, 76, 101, 116, 39, 115, 32, 69, 110, 99, 114, 121, 112, 116, 49, 11, 48, 9, 6, 3, 85, 4,
		3, 19, 2, 82, 47][..]));	//R?
}

#[test]
fn isrg_x1_expiry()
{
	static SUBJECT: &'static [u8] = include_bytes!("isrg-root-x1.subj");
	let h = Sha256::hash(SUBJECT);
	println!("hash={h:?}, index={idx:?}", idx=search_list_index(PUBLIC_CERTIFICATES, &h));
	let e = &PUBLIC_CERTIFICATES_EXP[8*1870..8*1870+8];
	let mut t = [0;8];
	t.copy_from_slice(e);
	println!("expiry={expiry}", expiry=i64::from_le_bytes(t));
	assert!(!is_expired_ca(SUBJECT, 2_064_567_878));
	assert!( is_expired_ca(SUBJECT, 2_064_567_879));
}
