#!/usr/bin/python3
import urllib.request;
import base64;
import json;
import hashlib;
import time;
import os;
from datetime import datetime

def sha256(x):
	m = hashlib.sha256();
	for y in x: m.update(y);
	return m.digest();

def write_file(filename, contents):
	newhash = sha256([contents]);
	try:
		fp=open(filename,"rb");
		oldcontent = fp.read();
		oldhash = sha256([oldcontent]);
		#If hashes match, no update.
		if oldhash == newhash: return False
	except IOError:
		#Failed to compute hash. Assume update.
		pass;
	print("Updating "+filename);
	fp=open(filename,"wb");
	fp.write(contents);
	return True;


def sort_and_delete_dupes(x):
	y = {};
	x.sort();
	i = 0;
	while i < len(x):
		if x[i] in y:
			del x[i];
		else:
			y[x[i]] = True;
			i += 1;

def error2(x):
	print("Bad certificate: "+x);
	raise ValueError("Unable to process certificate")

def read_asn_tag(data):
	if data[0] & 0x1F == 0x1F: raise ValueError("Extended tags not supported");
	if data[1] == 0x80 or data[1] > 0x83: raise ValueError("Bad data length");
	xlen = data[1];
	offset = 2;
	if xlen == 129:
		xlen = data[2];
		offset = 3;
		if xlen < 128: raise ValueError("Bad data length");
	elif xlen == 130:
		xlen = 256 * data[2] + data[3];
		offset = 4;
		if xlen < 256: raise ValueError("Bad data length");
	elif xlen == 131:
		xlen = 65536 * data[2] + 256 * data[3] + data[4];
		offset = 5;
		if xlen < 65536: raise ValueError("Bad data length");
	end = offset + xlen;
	return data[0], data[offset:end], data[end:], data[:end];

def is_time(x): return x == 23 or x == 24
def is_sequence(x): return x == 48
def is_integer(x): return x == 2
def is_octets(x): return x == 4
def is_oid(x): return x == 6

def read_asn_inner(data, check, fail):
	tag, inner, data, _ = read_asn_tag(data);
	if not check(tag): return error2(fail);
	return data, inner;

def read_asn_outer(data, check, fail):
	tag, _, data, outer = read_asn_tag(data);
	if not check(tag): return error2(fail);
	return data, outer;

def read_asn_ignore(data, check, fail):
	tag, _, data, _ = read_asn_tag(data);
	if not check(tag): return error2(fail);
	return data;

def to_timestamp(e):
	year = int(e[0:4]);
	month = int(e[4:6]);
	day = int(e[6:8]);
	hour = int(e[8:10]);
	minute = int(e[10:12]);
	second = int(e[12:14]);
	#One supercycle is 12,622,780,800 seconds. supercycle 5 startpoint is 946,684,800
	ts = year // 400 * 12622780800 - 62167219200;
	year %= 400;
	#Compensate for missing leap years. Then each cycle is 126,230,400 seconds.
	if (year == 100 and month > 2) or year > 100: ts -= 86400;
	if (year == 200 and month > 2) or year > 200: ts -= 86400;
	if (year == 300 and month > 2) or year > 300: ts -= 86400;
	ts += year // 4 * 126230400;
	year %= 4;
	if year > 0: ts += 366*86400;
	if year > 1: ts += 365*86400;
	if year > 2: ts += 365*86400;
	if month > 1: ts += 31*86400;			#January
	if month > 2: ts += 28*86400;			#February.
	if month > 2 and year == 0: ts += 86400;	#Leap day.
	if month > 3: ts += 31*86400;			#March
	if month > 4: ts += 30*86400;			#April
	if month > 5: ts += 31*86400;			#May
	if month > 6: ts += 30*86400;			#June
	if month > 7: ts += 31*86400;			#July
	if month > 8: ts += 31*86400;			#August
	if month > 9: ts += 30*86400;			#September
	if month > 10: ts += 31*86400;			#October
	if month > 11: ts += 30*86400;			#November
	ts += (day - 1)*86400;				#Day in moth.
	return ts + hour * 3600 + minute * 60 + second;

assert(to_timestamp("19991231235959Z") == 946684799);
assert(to_timestamp("20211007083806Z") == 1633595886);

def process_certificate(cert):
	#Certificate = SEQUENCE { TBS, signaturealgorithm, SignatureValue }.
	_, tbs = read_asn_inner(cert, is_sequence, "Toplevel not SEQUENCE");
	# TBS = SQUENCE { item1, item2, ... }. We need both inner and outer.
	_, tbs = read_asn_inner(tbs, is_sequence, "TBS not SEQUENCE")
	# The first item has to be v3 marker (we do not support v1).
	if tbs[:5] != b"\xa0\x03\x02\x01\x02": return error2("Not V3 certificate");
	tbs = tbs[5:];
	# The items are serial, algorithm, issuer, validity, subject and spki.
	tbs = read_asn_ignore(tbs, is_integer, "serial not INTEGER");
	tbs = read_asn_ignore(tbs, is_sequence, "signatureAlgorithm not SEQUENCE");
	tbs, issuer = read_asn_outer(tbs, is_sequence, "issuer not SEQUENCE");
	tbs, validity = read_asn_inner(tbs, is_sequence, "validity not SEQUENCE");
	validity = read_asn_ignore(validity, is_time, "notBefore not UTCTIME/GENERALTIME");
	_, expiry = read_asn_inner(validity, is_time, "notAfter not UTCTIME/GENERALTIME");
	tbs, subject = read_asn_outer(tbs, is_sequence, "subject not SEQUENCE");
	tbs, spki = read_asn_outer(tbs, is_sequence, "spki not SEQUENCE");
	# Find the extensions.
	tag = 0;
	while tag != 163: tag, extensions, tbs, _ = read_asn_tag(tbs);
	_, extensions = read_asn_inner(extensions, is_sequence, "extensions not SEQUENCE");
	#Scan the extensions for EKU.
	has_tls_eku = True;
	while len(extensions) > 0:
		extensions, extension = read_asn_inner(extensions, is_sequence, "extension not SEQUECE");
		if extension[:5] == b"\x06\x03U\x1d%":	#EKU.
			has_tls_eku = False;
			offset = 5;
			#Ignore BOOLEAN.
			if extension[5:8] == b"\x01\x01\xff": offset = 8;
			if extension[5:8] == b"\x01\x01\x00": offset = 8;
			#Read the OCTET STRING value, and SEQUENCE inside.
			_, value = read_asn_inner(extension[offset:], is_octets, "extension value not OCTET STRING");
			_, value = read_asn_inner(value, is_sequence, "EKU not SEQUENCE");
			while len(value) > 0:
				value, eku = read_asn_inner(value, is_oid, "EKU item not OBJECT IDENTIFIER");
				has_tls_eku |= eku == b"U\x1d%\x00";	#AnyExtendedKeyUsage.
				has_tls_eku |= eku == b'+\x06\x01\x05\x05\x07\x03\x01'; #TlsServer
				has_tls_eku |= eku == b'+\x06\x01\x05\x05\x07\x03\x02'; #TlsClient
	if len(expiry) == 13:
		if expiry[0] < 53: expiry = b"20" + expiry;
		if expiry[0] > 53: expiry = b"19" + expiry;
	#Check that certificate is not expired
	now = datetime.utcnow();
	now = "{:04d}{:02d}{:02d}{:02d}{:02d}{:02d}Z".format(now.year, now.month, now.day, now.hour, now.minute, now.second);
	if expiry < now.encode(): raise ValueError("Certificate Expired")
	if not has_tls_eku: raise ValueError("Certificate not for TLS")
	return subject, issuer, spki, to_timestamp(expiry);

#List of public names ever seen.
names_ever_seen = [];
data = open("src/public-certificates-seen.dat","rb");
data = data.read();
for idx in range(0,len(data),32):
	names_ever_seen.append(data[idx:idx+32]);

files = os.listdir("../../data/webpki-certs");
files = list(filter(lambda x: len(x) == 64, files));
files = sorted(files);

in_cert = False;
certs = [];
for fname in files:
        data = open("../../data/webpki-certs/"+fname,"rb");
        certs.append(data.read());

keys = {};
certset = [];
issuers = [];
subjects = [];
idmap = {};
seq = 0;
output = [];
ta_blacklist = {};
ta_whitelist = {};
expiry_timestamp = {};
expiry_timestamp2 = {};
for cert in certs:
	subject = None;
	issuer = None;
	spki = None;
	try:
		subject, issuer, spki, expiry = process_certificate(cert);
	except ValueError: continue;

	#Add ta blacklist/whitelist entries. Any self-signed entry makes to whitelist. Can add everything to
	#blacklist, as whitelist removes the entries.
	spki_hash = sha256([spki])
	ta_blacklist[spki_hash] = True;
	if subject == issuer: ta_whitelist[spki_hash] = True;
	#Add association subject -> key.
	if not subject in keys: keys[subject] = {};
	keys[subject][spki] = True;
	#Add cert, issuer and subject to arrays.
	certset.append(cert);
	issuers.append(issuer);
	subjects.append(subject);
	#Maximum expiry is taken even if not new.
	subjhash = sha256([subject]);
	if subjhash not in expiry_timestamp2 or expiry_timestamp2[subjhash] < expiry:
		expiry_timestamp[subjhash] = expiry.to_bytes(8, byteorder="little");
		expiry_timestamp2[subjhash] = expiry;
	#Both subject and issuer are considered seen.
	names_ever_seen.append(subjhash);
	names_ever_seen.append(sha256([issuer]));
	#Is new issuer?
	if subject in idmap:
		continue;	#Already seen.
	idmap[subject] = seq;
	output.append(subjhash);
	seq += 1;
	#print("SERIAL="+str(serial)+" ISSUER="+str(issuer)+" EXPIRY="+str(expiry)+" SUBJECT="+str(subject)+" SPKI="+str(spki));

#Remove all whitelist entries from blacklist.
for hash in ta_whitelist: del ta_blacklist[hash];

output2 = [];
output3 = [];
output4 = [];
total_cert_size = 0;
for idx in range(0, len(issuers)):
	issuer = issuers[idx];
	cert = certset[idx];
	total_cert_size += len(cert);
	#Keys is actually map from subject to set of keys, and all should be added.
	if issuer in keys:
		for isspub in keys[issuer]:
			h = sha256([issuer, isspub, cert]);
			output2.append(h);
		output3.append(sha256([cert]));
for hash in ta_blacklist: output4.append(hash);

sort_and_delete_dupes(output);
sort_and_delete_dupes(output2);
sort_and_delete_dupes(output3);
sort_and_delete_dupes(output4);
sort_and_delete_dupes(names_ever_seen);

print("Total certificate size:"+str(total_cert_size));

public_certificates_expiry_dat = b"";
public_certificates_dat = b"";
for ent in output:
	public_certificates_dat += ent;
	public_certificates_expiry_dat += expiry_timestamp[ent];
signature_hashes_dat = b"";
for ent in output2: signature_hashes_dat += ent;
public_intermediates_dat = b"";
for ent in output3: public_intermediates_dat += ent;
ta_blacklist_dat = b"";
for ent in output4: ta_blacklist_dat += ent;
public_certificates_seen_dat = b"";
for ent in names_ever_seen: public_certificates_seen_dat += ent;


updated = False;
updated |= write_file("src/public-certificates-seen.dat", public_certificates_seen_dat);
updated |= write_file("src/public-certificates-expiry.dat", public_certificates_expiry_dat);
updated |= write_file("src/public-certificates.dat", public_certificates_dat);
updated |= write_file("src/signature-hashes.dat", signature_hashes_dat);
updated |= write_file("src/public-intermediates.dat", public_intermediates_dat);
updated |= write_file("src/ta-blacklist.dat", ta_blacklist_dat);

data = open("../../data/revoke.dat","rb");
content = data.read().decode("utf-8");
data=json.loads(content);
data=data["data"];
output=[];
output2=[];
dedup={};
for entry in data:
	if not entry["enabled"]: continue;
	if "issuerName" in entry and "serialNumber" in entry:
		m = hashlib.sha256();
		m.update(base64.b64decode(entry["issuerName"]));
		m.update(base64.b64decode(entry["serialNumber"]));
		v = m.digest();
		if not v in dedup:
			output.append(v);
			dedup[v] = True;
	elif "pubKeyHash" in entry:
		output2.append(base64.b64decode(entry["pubKeyHash"]));
	else:
		print(str(entry));
		raise ValueError("Unrecognized entry type (no issuerName/serialNumber)");
output.sort();
output2.sort();


crl_issuer_serial_dat = b"";
for ent in output: crl_issuer_serial_dat += ent;
crl_pubkey_dat = b"";
for ent in output2: crl_pubkey_dat += ent;

updated |= write_file("src/crl-issuer-serial.dat", crl_issuer_serial_dat);
updated |= write_file("src/crl-pubkey.dat",crl_pubkey_dat);

data = open("../../data/timestamp.txt","rb");
timestamp = int(data.read().decode("utf-8"));
daynum = timestamp//86400;
ts = time.gmtime(timestamp);
vcode = "{:02}{:02}{:02}".format(ts.tm_year-2000, ts.tm_mon, ts.tm_mday);

if updated:
	daynum = daynum.to_bytes(8, byteorder='little');
	write_file("src/build-date.dat",daynum);
	print("Updated public CA list");
	fp=open("Cargo.toml.in","r");
	fpw=open("Cargo.toml","w");
	lines = [line.rstrip('\n') for line in fp];
	for line in lines:
		p = line.find("@minor_versioncode@");
		if p < 0:
			#No mods, print as is.
			print(line, file=fpw);
		else:
			print(line[:p]+vcode+line[p+19:], file=fpw);
