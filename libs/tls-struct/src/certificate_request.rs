use crate::DbgT;
use crate::ExtensionError as ExtErr;
use crate::LEN_CERTIFICATE_AUTHORITIES;
use crate::LEN_CERTIFICATE_REQUEST_CONTEXT;
use crate::LEN_CR_CERTIFICATE_TYPE_LIST;
use crate::LEN_CR_EXTENSION_LIST;
use crate::LEN_OID_FILTER_LIST;
use crate::LEN_SIGNATURE_ALGORITHMS;
use crate::errors::Struct as EStruct;
use crate::extensions as exts;
use crate::iters2::CertificateAuthority;
use crate::iters2::Extension;
use crate::iters2::IntoGenericIterator as ListOf;
use crate::iters2::Message;
use crate::iters2::OidFilter;
use crate::iters2::ServerName;
use btls_aux_fail::fail_if;
use btls_aux_tls_iana::ClientCertificateType;
use btls_aux_tls_iana::CertificateCompressionAlgorithmId;
use btls_aux_tls_iana::ExtensionType as IanaExtension;
use btls_aux_tls_iana::SignatureScheme;
use core::marker::PhantomData;

macro_rules! field
{
	(E $location:ident $x:expr) => {
		$x.map_err(EStruct::$location).map_err(ExtErr::Structured)?
	};
	(S $location:ident $x:expr) => {
		$x.map_err(EStruct::$location).map_err(_CertificateRequestParseError::Structured)?
	};
}

macro_rules! extension
{
	($exts2:ident $dbg:ident $name:ident $handler:expr) => {
		$exts2.with_stdext(IanaExtension::$name, _CertificateRequestParseError::JunkAfterExtension,
			_CertificateRequestParseError::Extension, $dbg, $handler)?
	}
}

///Error from parsing Certificate Request.
///
///The most notable things to query errors are the `alert()` method and the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct CertificateRequestParseError(_CertificateRequestParseError);

#[derive(Copy,Clone,Debug)]
enum _CertificateRequestParseError
{
	JunkAfterEnd,
	NoSignatureAlgorithms,
	Extension(IanaExtension, ExtErr),
	Structured(crate::errors::Struct),
	JunkAfterExtension(IanaExtension),
}

impl CertificateRequestParseError
{
	///Get the alert code one should use for this parse error.
	pub fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::_CertificateRequestParseError::*;
		use btls_aux_tls_iana::Alert as A;
		match &self.0 {
			&JunkAfterEnd => A::DECODE_ERROR,
			&NoSignatureAlgorithms => A::MISSING_EXTENSION,
			&Extension(_, ref err) => err.alert(),
			&Structured(ref x) => crate::iters2::AlertForError::alert(x),
			&JunkAfterExtension(_) => A::DECODE_ERROR,
		}
	}
}

impl core::fmt::Display for CertificateRequestParseError
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		use self::_CertificateRequestParseError::*;
		match &self.0 {
			&JunkAfterEnd => write!(f, "Junk after end of message"),
			&NoSignatureAlgorithms => write!(f, "No suported signature algorithms"),
			&Extension(ext, ref err) => write!(f, "extension {ext}: {err}"),
			&Structured(ref e) => core::fmt::Display::fmt(e, f),
			&JunkAfterExtension(ext) => write!(f, "Junk after end of extension {ext}"),
		}
	}
}

///Parsed TLS 1.3 Certificate Request.
///
///This object corresponds to the `CertificateRequest` TLS structure.
#[derive(Clone)]
pub struct CertificateRequestTls13<'a>
{
	extension_mask: [u32;exts::EXTENSION_WORDS],
	context: &'a [u8],
	extensions: ListOf<'a, Extension<'a>>,
	supports_signed_certificate_timestamp: bool,
	supports_transparency_info: bool,
	supports_ocsp_stapling: bool,
	supported_certificate_authorities: ListOf<'a,CertificateAuthority<'a>>,
	supported_signature_algorithms: ListOf<'a,SignatureScheme>,
	supported_signature_algorithms_cert: ListOf<'a,SignatureScheme>,
	supported_signature_algorithms_dc: ListOf<'a,SignatureScheme>,
	supported_certificate_compression: ListOf<'a,CertificateCompressionAlgorithmId>,
	oid_filters: ListOf<'a,OidFilter<'a>>,
	server_host_name: Option<&'a str>,
	server_name: ListOf<'a, ServerName<'a>>,
	x: PhantomData<&'a u8>
}

impl<'a> CertificateRequestTls13<'a>
{
	///Get the certificate request context.
	///
	///This context is replayed back in Certificate message. It MUST be empty during initial handshake.
	///
	///This corresponds to the `certificate_request_context` field of the TLS `CertificateRequest` structure.
	pub fn get_request_context(&self) -> &'a [u8] { self.context }
	///Get the Certificate request extensions.
	///
	///This corresponds to the `extensions` field of the TLS `CertificateRequest` strucure.
	pub fn get_extensions(&self) -> ListOf<'a, Extension<'a>> { self.extensions }
	///Does the server support Certificate Transparency v1 Signed Certificate Timestamp stapling?
	///
	///This corresponds to the `signed_certificate_timestamp` TLS extension.
	pub fn may_staple_sct(&self) -> bool { self.supports_signed_certificate_timestamp }
	///Does the server support Certificate Transparency v2 Transparency Item stapling?
	///
	///This corresponds to the `transparency_info` TLS extension.
	pub fn may_staple_trans_item(&self) -> bool { self.supports_transparency_info }
	///Does the server support OCSP stapling?
	///
	///This corresponds to the `status_request` TLS extension.
	pub fn may_staple_ocsp(&self) -> bool { self.supports_ocsp_stapling }
	///Get list of criteria for certficiates the server accepts.
	///
	///If the list is empty, the constraints are unspecified. Unknown constraints MUST be ignored.
	///
	///This corresponds to the `oid_filters` TLS extension.
	pub fn get_oid_filters(&self) -> ListOf<'a,OidFilter<'a>> { self.oid_filters }
	///Get list of X.509 names of the certificate authorities server accepts.
	///
	///If the list is empty, the actual list of certificate authorities is assumed to be pre-agreed.
	///
	///This corresponds to the `certificate_authorities` TLS extension.
	pub fn get_trused_certficate_authorities(&self) -> ListOf<'a,CertificateAuthority<'a>>
	{
		self.supported_certificate_authorities
	}
	///Get list of supported signature algorithms for private key proof-of-possession.
	///
	///The selected signature algorithm (the `algorithm` field in CertificateVerify message) MUST be one of
	///thsese. This list can not be empty.
	///
	///If delegated credentials are used, the `algorithm` field of the `DelegatedCredential` structure MUST
	///be one of these.
	///
	///This corresponds to the `signature_algorithms` TLS extension.
	pub fn get_signature_algorithms(&self) -> ListOf<'a,SignatureScheme>
	{
		self.supported_signature_algorithms
	}
	///Get list of supported certificate signature algorithms.
	///
	///This list is advisory. The server might not actually need to verify all signatures (in practicular,
	///self-signatures should always be ignored), and might support algorithms that do not have TLS signature
	///scheme identifiers.
	///
	///This corresponds to the `signature_algorithms_cert` TLS extension. In case that extension is missing,
	///it falls back to `signature_algorithms` extension.
	pub fn get_signature_algorithms_certificate(&self) -> ListOf<'a,SignatureScheme>
	{
		self.supported_signature_algorithms_cert
	}
	///Get list of supported delegated credentials signature algorithms.
	///
	///The selected delegated credential signature algorithm (the `dc_cert_verify_algorithm` field in
	///`Credential` TLS structure MUST be one of thsese. If the list is empty, the server does not support
	///delegated credentials.
	///
	///This corresponds to the `delegated_credentials` TLS extension.
	pub fn get_signature_algorithms_delegated_credentials(&self) -> ListOf<'a,SignatureScheme>
	{
		self.supported_signature_algorithms_dc
	}
	///Get list of supported certificate compression algorithms.
	///
	///The selected certificate compression algorithm MUST be one of these. If the list is empty, the server
	///does not support copressed certificates.
	///
	///This corresponds to the `compress_certificate` TLS extension.
	pub fn get_certificate_compression_algorithms(&self) -> ListOf<'a,CertificateCompressionAlgorithmId>
	{
		self.supported_certificate_compression
	}
	///Get server host name the client is trying to access, if any.
	///
	///If the client did not send a host name, this is `None`. This is always `None` for certificate requests
	///sent by the server.
	pub fn get_server_host_name(&self) -> Option<&'a str> { self.server_host_name }
	///Get other names of the server the client is trying to access.
	///
	///As of currently, this should never contain anything (unless one does `iter_all()`, then one gets
	///raw entry for host name).
	pub fn get_server_other_names(&self) -> ListOf<'a, ServerName<'a>> { self.server_name }
	///Was specified extension sent?
	pub fn has_extension(&self, ext: IanaExtension) -> bool
	{
		match exts::extension_bit(ext) {
			Some(bit) => {
				if let Some(rw) = self.extension_mask.get(bit / 32) {
					*rw & 1u32 << bit % 32 != 0
				} else {
					false
				}
			},
			None => self.extensions.iter_all().any(|e|{
				e.extension_type == ext
			}),
		}
	}
	///Parse certificate request to its component parts.
	pub fn parse(msg: &'a [u8], mut dbg: DbgT) ->
		Result<CertificateRequestTls13<'a>, CertificateRequestParseError>
	{
		parse_cr(Message::new(msg), &mut dbg, false).map_err(CertificateRequestParseError)
	}
	///Parse client certificate request to its component parts.
	pub fn parse_client(msg: &'a [u8], mut dbg: DbgT) ->
		Result<CertificateRequestTls13<'a>, CertificateRequestParseError>
	{
		parse_cr(Message::new(msg), &mut dbg, true).map_err(CertificateRequestParseError)
	}
}

fn parse_cr<'a>(mut msg: Message<'a>, dbg: &mut DbgT, is_client: bool) ->
	Result<CertificateRequestTls13<'a>, _CertificateRequestParseError>
{
	// struct {
	//	 opaque certificate_request_context<0..2^8-1>;
	//	 Extension extensions<2..2^16-1>;
	// } CertificateRequest;
	//
	// - RFC8446 section 4.3.2.
	let context = field!(S CertificateRequestContext msg.slice(LEN_CERTIFICATE_REQUEST_CONTEXT));
	let extensions = field!(S Extensions msg.list_of::<Extension<'a>>(LEN_CR_EXTENSION_LIST));
	msg.assert_end(_CertificateRequestParseError::JunkAfterEnd)?;

	//Client certificate requests have different set of valid extensions.
	let ext_is_ok = if is_client {
		IanaExtension::allowed_in_client_certificate_request
	} else {
		IanaExtension::allowed_in_certificate_request
	};
	let exts2 = field!(S Extensions2 exts::check_extensions_block(extensions, ext_is_ok));

	let supports_ocsp_stapling = extension!(exts2 dbg STATUS_REQUEST exts::ext_supported).is_some();

	let supported_signature_algorithms =
		extension!(exts2 dbg SIGNATURE_ALGORITHMS_CERT exts::clientext_signature_algorithms).
		unwrap_or_default();

	let supports_signed_certificate_timestamp =
		extension!(exts2 dbg SIGNED_CERTIFICATE_TIMESTAMP exts::ext_supported).is_some();

	let supported_certificate_compression =
		extension!(exts2 dbg COMPRESS_CERTIFICATE exts::clientext_compress_certificate).unwrap_or_default();

	let supported_signature_algorithms_dc =
		extension!(exts2 dbg DELEGATED_CREDENTIALS exts::clientext_signature_algorithms).unwrap_or_default();

	let supported_certificate_authorities =
		extension!(exts2 dbg CERTIFICATE_AUTHORITIES exts::clientext_certificate_authorities).
		unwrap_or_default();

	let oid_filters = extension!(exts2 dbg OID_FILTERS |extn, extv, dbg|{
		// struct {
		//	 OIDFilter filters<0..2^16-1>;
		// } OIDFilterExtension;
		//
		// - RFC8446 section 4.2.5.
		let filters = field!(E Filters extv.list_of::<OidFilter<'a>>(LEN_OID_FILTER_LIST));
		debug!(E dbg extn "{fcnt} OID filters", fcnt=filters.count_all(true));
		Ok(filters)
	}).unwrap_or_default();

	let supported_signature_algorithms_cert =
		extension!(exts2 dbg SIGNATURE_ALGORITHMS_CERT exts::clientext_signature_algorithms);

	let supports_transparency_info = extension!(exts2 dbg TRANSPARENCY_INFO exts::ext_supported).is_some();

	crate::extensions::report_unknown_extensions(extensions, dbg);

	fail_if!(supported_signature_algorithms.count_all(false) == 0,
		_CertificateRequestParseError::NoSignatureAlgorithms);
	//Signature Algorithms Cert defaults to normal one.
	let supported_signature_algorithms_cert = supported_signature_algorithms_cert.
		unwrap_or(supported_signature_algorithms);

	//This is client_certificate_request only, the check is done above in the ext_is_ok() passed to
	//extension block parsing.
	let (server_name, server_host_name) = extension!(exts2 dbg SERVER_NAME |extn, extv, dbg|{
		let (snl, host_name) = field!(E ServerNameList extv.server_name_list());
		if let Some(host_name) = host_name { debug!(E dbg extn "Host name is {}", host_name); }
		if snl.iter().count() > 0 { debug!(E dbg extn "Other names are {:?}", snl.iter()); }
		Ok((snl,host_name))
	}).unwrap_or_default();

	Ok(CertificateRequestTls13{
		extension_mask: exts2.get_mask(),
		context,
		extensions,
		supports_signed_certificate_timestamp,
		supports_transparency_info,
		supports_ocsp_stapling,
		supported_certificate_authorities,
		supported_signature_algorithms,
		supported_signature_algorithms_cert,
		supported_signature_algorithms_dc,
		supported_certificate_compression,
		oid_filters,
		server_host_name,
		server_name,
		x: PhantomData
	})
}

///Parsed TLS 1.2 Certificate Request.
///
///This object corresponds to the `CertificateRequest` TLS structure.
#[derive(Clone)]
pub struct CertificateRequestTls12<'a>
{
	certificate_authorities: ListOf<'a,CertificateAuthority<'a>>,
	supported_signature_algorithms: ListOf<'a,SignatureScheme>,
	supported_certificate_types: ListOf<'a,ClientCertificateType>,
	x: PhantomData<&'a u8>
}

impl<'a> CertificateRequestTls12<'a>
{
	///Get list of X.509 names of the certificate authorities server accepts.
	///
	///If the list is empty, the actual list of certificate authorities is assumed to be pre-agreed.
	pub fn get_trused_certficate_authorities(&self) -> ListOf<'a,CertificateAuthority<'a>>
	{
		self.certificate_authorities
	}
	///Get list of supported signature algorithms for private key proof-of-possession.
	///
	///The selected signature algorithm (the `algorithm` field in CertificateVerify message) MUST be one of
	///thsese. If this list is empty, signature authentication is not supported.
	///
	///The signature algorithms are given as TLS 1.3 signature schemes, even in TLS 1.2. Most signature
	///schemes have the same meaning in TLS 1.3 and TLS 1.2. However, one set of exceptions are `0x0403`,
	///`0x0503` and `0x0603`. In TLS 1.3 these imply curve, in TLS 1.2 they do not, they are just `ECDSA`
	///with given hash function (`SHA-256`, `SHA-384` and `SHA-512`, respectively).
	pub fn get_signature_algorithms(&self) -> ListOf<'a,SignatureScheme>
	{
		self.supported_signature_algorithms
	}
	///Get list of supported client certificate types.
	///
	///This is used for things like (EC)DH certificates. Note that anything but signature certificates are
	///deprecated.
	pub fn get_client_certificate_types(&self) -> ListOf<'a,ClientCertificateType>
	{
		self.supported_certificate_types
	}
	///Parse certificate request to its component parts.
	pub fn parse(msg: &'a [u8], mut dbg: DbgT) -> Result<CertificateRequestTls12<'a>, CertificateRequestParseError>
	{
		parse_cr2(Message::new(msg), &mut dbg).map_err(CertificateRequestParseError)
	}
}

fn parse_cr2<'a>(mut msg: Message<'a>, dbg: &mut DbgT) ->
	Result<CertificateRequestTls12<'a>, _CertificateRequestParseError>
{
	// struct {
	//	 ClientCertificateType certificate_types<1..2^8-1>;
	//	 SignatureAndHashAlgorithm supported_signature_algorithms<2^16-1>;
	//	 DistinguishedName certificate_authorities<0..2^16-1>;
	// } CertificateRequest;
	//
	// - RFC5246 section 7.4.4.
	let supported_certificate_types = field!(S CertificateTypes2
		msg.list_of::<ClientCertificateType>(LEN_CR_CERTIFICATE_TYPE_LIST)
	);
	let supported_signature_algorithms = field!(S SupportedSignatureAlgorithms
		msg.list_of::<SignatureScheme>(LEN_SIGNATURE_ALGORITHMS)
	);
	let certificate_authorities = field!(S CertificateAuthorities
		msg.list_of::<CertificateAuthority<'a>>(LEN_CERTIFICATE_AUTHORITIES)
	);
	msg.assert_end(_CertificateRequestParseError::JunkAfterEnd)?;
	debug!(dbg "Signature algorithms: {supported_signature_algorithms:?}", );
	debug!(dbg "Certificate types: {supported_certificate_types:?}");
	let cacnt = certificate_authorities.count_all(true);
	if cacnt > 0 { debug!(dbg "{cacnt} supported CAs"); }
	Ok(CertificateRequestTls12 {
		certificate_authorities,
		supported_certificate_types,
		supported_signature_algorithms,
		x: PhantomData
	})
}
