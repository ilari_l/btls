use crate::DbgT;
use crate::KeyCheck;
use crate::LEN_DH_P;
use crate::LEN_ECBIN_K;
use crate::LEN_ECBIN_P;
use crate::LEN_ECDH_Y;
use crate::LEN_ECPRIME_P;
use crate::LEN_PSK_IDENTITY;
use crate::LEN_PSK_IDENTITY_HINT;
use crate::LEN_RSA_PMS;
use crate::LEN_SIGNATURE;
use crate::LEN_SRP_N;
use crate::LEN_SRP_S;
use crate::RemoteFeatureSet;
use crate::certificate::Signature;
use crate::iters2::AlertForError;
use crate::iters2::CertificateStatus;
use crate::iters2::Message;
use crate::iters2::PrimitiveReadError;
use crate::iters2::SliceReadError;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_memory::Hexdump;
use btls_aux_tls_iana::CipherSuite;
use btls_aux_tls_iana::NamedGroup;
use btls_aux_tls_iana::SignatureScheme;
use core::marker::PhantomData;

///Error from parsing Server Key Exchange.
///
///The most notable things to query errors are the `alert()` method and the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct ServerKeyExchangeParseError(_ServerKeyExchangeParseError);

#[derive(Copy,Clone,Debug)]
enum _ServerKeyExchangeParseError
{
	CurveType(PrimitiveReadError),
	PrimeP(SliceReadError),
	PrimeA(SliceReadError),
	PrimeB(SliceReadError),
	PrimeBase(SliceReadError),
	PrimeOrder(SliceReadError),
	PrimeCofactor(SliceReadError),
	BinaryM(PrimitiveReadError),
	BinaryBasis(PrimitiveReadError),
	BinaryK(SliceReadError),
	BinaryK1(SliceReadError),
	BinaryK2(SliceReadError),
	BinaryK3(SliceReadError),
	BinaryA(SliceReadError),
	BinaryB(SliceReadError),
	BinaryBase(SliceReadError),
	BinaryOrder(SliceReadError),
	BinaryCofactor(SliceReadError),
	Named(PrimitiveReadError),
	Public(SliceReadError),
	Algorithm(PrimitiveReadError),
	Signature(SliceReadError),
	DhP(SliceReadError),
	DhG(SliceReadError),
	Public3(SliceReadError),
	SrpB(SliceReadError),
	SrpG(SliceReadError),
	SrpN(SliceReadError),
	SrpS(SliceReadError),
	PskIdentityHint(SliceReadError),
	BadSignature(SignatureScheme),
	BadCurveType(u8),
	BadBasis(u8),
	PrimeATooLarge,
	PrimeBTooLarge,
	BinaryMOutOfRange(u16),
	BinaryKOutOfRange,
	BinaryK1OutOfRange,
	BinaryK2OutOfRange,
	BinaryK3OutOfRange,
	BadNamed(NamedGroup),
	BadCurve(NamedGroup),
	BadPoint(NamedGroup),
	//TODO: Error about bad point encoding.
	//TODO: Error about bad base encoding.
	JunkAfterEnd,
	UnsupportedCipher(CipherSuite),
	MessageAbsent,
}

impl ServerKeyExchangeParseError
{
	///Get the alert code one should use for this parse error.
	pub fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::_ServerKeyExchangeParseError::*;
		use btls_aux_tls_iana::Alert as A;
		match &self.0 {
			&CurveType(ref err) => err.alert(),
			&PrimeP(ref err) => err.alert(),
			&PrimeA(ref err) => err.alert(),
			&PrimeB(ref err) => err.alert(),
			&PrimeBase(ref err) => err.alert(),
			&PrimeOrder(ref err) => err.alert(),
			&PrimeCofactor(ref err) => err.alert(),
			&BinaryM(ref err) => err.alert(),
			&BinaryBasis(ref err) => err.alert(),
			&BinaryK(ref err) => err.alert(),
			&BinaryK1(ref err) => err.alert(),
			&BinaryK2(ref err) => err.alert(),
			&BinaryK3(ref err) => err.alert(),
			&BinaryA(ref err) => err.alert(),
			&BinaryB(ref err) => err.alert(),
			&BinaryBase(ref err) => err.alert(),
			&BinaryOrder(ref err) => err.alert(),
			&BinaryCofactor(ref err) => err.alert(),
			&Named(ref err) => err.alert(),
			&Public(ref err) => err.alert(),
			&Algorithm(ref err) => err.alert(),
			&Signature(ref err) => err.alert(),
			&DhP(ref err) => err.alert(),
			&DhG(ref err) => err.alert(),
			&Public3(ref err) => err.alert(),
			&SrpB(ref err) => err.alert(),
			&SrpG(ref err) => err.alert(),
			&SrpN(ref err) => err.alert(),
			&SrpS(ref err) => err.alert(),
			&PskIdentityHint(ref err) => err.alert(),
			&BadSignature(_) => A::ILLEGAL_PARAMETER,
			&BadCurveType(_) => A::ILLEGAL_PARAMETER,
			&BadBasis(_) => A::ILLEGAL_PARAMETER,
			&PrimeATooLarge => A::ILLEGAL_PARAMETER,
			&PrimeBTooLarge => A::ILLEGAL_PARAMETER,
			&BinaryMOutOfRange(_) => A::ILLEGAL_PARAMETER,
			&BinaryKOutOfRange => A::ILLEGAL_PARAMETER,
			&BinaryK1OutOfRange => A::ILLEGAL_PARAMETER,
			&BinaryK2OutOfRange => A::ILLEGAL_PARAMETER,
			&BinaryK3OutOfRange => A::ILLEGAL_PARAMETER,
			&BadNamed(_) => A::ILLEGAL_PARAMETER,
			&BadCurve(_) => A::ILLEGAL_PARAMETER,
			&BadPoint(_) => A::ILLEGAL_PARAMETER,
			&JunkAfterEnd => A::DECODE_ERROR,
			&UnsupportedCipher(_) => A::ILLEGAL_PARAMETER,
			&MessageAbsent => A::UNEXPECTED_MESSAGE,
		}
	}
}

impl core::fmt::Display for ServerKeyExchangeParseError
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		let exp_p = "curve_params::explicit_prime";
		let exp_2 = "curve_params::explicit_char2";
		let trinom = "basis::ec_trinomial";
		let pentanom = "basis::ec_pentanomial";
		use self::_ServerKeyExchangeParseError::*;
		match &self.0 {
			&CurveType(ref err) => write!(f, "curve_type: {err}"),
			&PrimeP(ref err) => write!(f, "{exp_p}.prime_p: {err}"),
			&PrimeA(ref err) => write!(f, "{exp_p}.curve.a: {err}"),
			&PrimeB(ref err) => write!(f, "{exp_p}.curve.b: {err}"),
			&PrimeBase(ref err) => write!(f, "{exp_p}.base: {err}"),
			&PrimeOrder(ref err) => write!(f, "{exp_p}.order: {err}"),
			&PrimeCofactor(ref err) => write!(f, "{exp_p}.cofactor: {err}"),
			&BinaryM(ref err) => write!(f, "{exp_2}.m: {err}"),
			&BinaryBasis(ref err) => write!(f, "{exp_2}.basis: {err}"),
			&BinaryCofactor(ref err) => write!(f, "{exp_2}.cofactor: {err}"),
			&BinaryK(ref err) => write!(f, "{exp_2}.{trinom}.k: {err}"),
			&BinaryK1(ref err) => write!(f, "{exp_2}.{pentanom}.k1: {err}"),
			&BinaryK2(ref err) => write!(f, "{exp_2}.{pentanom}.k2: {err}"),
			&BinaryK3(ref err) => write!(f, "{exp_2}.{pentanom}.k3: {err}"),
			&BinaryA(ref err) => write!(f, "{exp_2}.basis::curve.a: {err}"),
			&BinaryB(ref err) => write!(f, "{exp_2}.basis::curve.b: {err}"),
			&BinaryBase(ref err) => write!(f, "{exp_2}.base: {err}"),
			&BinaryOrder(ref err) => write!(f, "{exp_2}.basis::order: {err}"),
			&Named(ref err) => write!(f, "curve_params::named_curve.namecurve: {err}"),
			&Public(ref err) => write!(f, "public: {err}"),
			&Algorithm(ref err) => write!(f, "signed_params.algorithm: {err}"),
			&Signature(ref err) => write!(f, "signed_params.signature: {err}"),
			&DhP(ref err) => write!(f, "params.dh_p: {err}"),
			&DhG(ref err) => write!(f, "params.dh_g: {err}"),
			&Public3(ref err) => write!(f, "params.dh_ys: {err}"),
			&SrpB(ref err) => write!(f, "srp_B: {err}"),
			&SrpG(ref err) => write!(f, "srp_g: {err}"),
			&SrpN(ref err) => write!(f, "srp_N: {err}"),
			&SrpS(ref err) => write!(f, "srp_s: {err}"),
			&PskIdentityHint(ref err) => write!(f, "psk_identity_hint: {err}"),
			&BadSignature(ref alg) => write!(f, "signed_params.algorithm: Illegal algorithm {alg}"),
			&BadCurveType(ref val) => write!(f, "curve_type: Illegal value {val}"),
			&BadBasis(ref val) => write!(f, "{exp_2}.basis: Illegal value {val}"),
			&PrimeATooLarge => write!(f, "{exp_p}.curve.a: Bad value"),
			&PrimeBTooLarge => write!(f, "{exp_p}.curve.b: Bad value"),
			&BinaryMOutOfRange(ref val) => write!(f, "{exp_2}.m: Value {val} out of range"),
			&BinaryKOutOfRange => write!(f, "{exp_2}.{trinom}: Value out of range"),
			&BinaryK1OutOfRange => write!(f, "{exp_2}.{pentanom}.k1: Value out of range"),
			&BinaryK2OutOfRange => write!(f, "{exp_2}.{pentanom}.k2: Value out of range"),
			&BinaryK3OutOfRange => write!(f, "{exp_2}.{pentanom}.k3: Value out of range"),
			&BadNamed(ref name) => write!(f, "curve_params::named_curve.namecurve: Bad curve {name}"),
			&BadCurve(ref curve) => write!(f, "curve_type: Bad curve {curve}"),
			&BadPoint(ref group) => write!(f, "public: Bad point for group {group}"),
			&JunkAfterEnd => write!(f, "Junk after end of message"),
			&UnsupportedCipher(ref cipher) => write!(f, "Unsupported cipher {cipher}"),
			&MessageAbsent => write!(f, "ServerKeyExchange should have been absent"),
		}
	}
}

///Error from parsing Client Key Exchange.
///
///The most notable things to query errors are the `alert()` method and the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct ClientKeyExchangeParseError(_ClientKeyExchangeParseError);

#[derive(Copy,Clone,Debug)]
enum _ClientKeyExchangeParseError
{
	Public(SliceReadError),
	Public2(SliceReadError),
	EncryptedPremasterSecret(SliceReadError),
	SrpA(SliceReadError),
	PskIdentity(SliceReadError),
	BadPoint(NamedGroup),
	JunkAfterEnd,
	UnsupportedCipher(CipherSuite),
}

impl ClientKeyExchangeParseError
{
	///Get the alert code one should use for this parse error.
	pub fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::_ClientKeyExchangeParseError::*;
		use btls_aux_tls_iana::Alert as A;
		match &self.0 {
			&Public(ref err) => err.alert(),
			&Public2(ref err) => err.alert(),
			&EncryptedPremasterSecret(ref err) => err.alert(),
			&SrpA(ref err) => err.alert(),
			&PskIdentity(ref err) => err.alert(),
			&BadPoint(_) => A::ILLEGAL_PARAMETER,
			&JunkAfterEnd => A::DECODE_ERROR,
			&UnsupportedCipher(_) => A::ILLEGAL_PARAMETER,
		}
	}
}

impl core::fmt::Display for ClientKeyExchangeParseError
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		use self::_ClientKeyExchangeParseError::*;
		match &self.0 {
			&Public(ref err) => write!(f, "ecdh_public.ecdh_yc: {err}"),
			&Public2(ref err) => write!(f, "dh_public.dh_yc: {err}"),
			&EncryptedPremasterSecret(ref err) => write!(f, "encrypted_premaster_secret: {err}"),
			&SrpA(ref err) => write!(f, "srp_A: {err}"),
			&PskIdentity(ref err) => write!(f, "psk_identity: {err}"),
			&BadPoint(ref err) => write!(f, "ecdh_public.ecdh_yc: Bad point for group {err}"),
			&JunkAfterEnd => write!(f, "Junk after end of message"),
			&UnsupportedCipher(ref cipher) => write!(f, "Unsupported cipher {cipher}"),
		}
	}
}

///Error from parsing Certificate Status.
///
///The most notable things to query errors are the `alert()` method and the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct CertificateStatusParseError(_CertificateStatusParseError);

#[derive(Copy,Clone,Debug)]
enum _CertificateStatusParseError
{
	CertificateStatus(crate::iters2::CertificateStatusError),
	JunkAfterEnd,
}

impl CertificateStatusParseError
{
	///Get the alert code one should use for this parse error.
	pub fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::_CertificateStatusParseError::*;
		use btls_aux_tls_iana::Alert as A;
		match self.0 {
			CertificateStatus(ref err) => err.alert(),
			JunkAfterEnd => A::DECODE_ERROR,
		}
	}
}

impl core::fmt::Display for CertificateStatusParseError
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		use self::_CertificateStatusParseError::*;
		match &self.0 {
			&CertificateStatus(ref g) => core::fmt::Display::fmt(g, f),
			&JunkAfterEnd => write!(f, "Junk after end of message"),
		}
	}
}


fn __read_k(data: &mut Message, m: u16, read: fn(SliceReadError) -> _ServerKeyExchangeParseError,
	range: _ServerKeyExchangeParseError) -> Result<u16, _ServerKeyExchangeParseError>
{
	let v = match __remove_zeros(data.slice(LEN_ECBIN_K).map_err(read)?) {
		&[a] => a as u16,
		&[a,b] => a as u16 * 256 + b as u16,
		_ => fail!(range)
	};
	fail_if!(v == 0 || v >= m, range);
	Ok(v)
}

fn __read_basis(data: &mut Message) -> Result<(u16, u16, Option<(u16, u16)>), _ServerKeyExchangeParseError>
{
	use _ServerKeyExchangeParseError as SKEPE;
	let m: u16 = data.cp().map_err(SKEPE::BinaryM)?;
	fail_if!(m < 2 || m > 2040, SKEPE::BinaryMOutOfRange(m));
	let d: u8 = data.cp().map_err(SKEPE::BinaryBasis)?;
	let r = match d {
		1 => {
			let k = __read_k(data, m, SKEPE::BinaryK, SKEPE::BinaryKOutOfRange)?;
			(m, k, None)
		},
		2  => {
			let k1 = __read_k(data, m, SKEPE::BinaryK1, SKEPE::BinaryK1OutOfRange)?;
			let k2 = __read_k(data, k1, SKEPE::BinaryK2, SKEPE::BinaryK2OutOfRange)?;
			let k3 = __read_k(data, k2, SKEPE::BinaryK3, SKEPE::BinaryK3OutOfRange)?;
			(m, k1, Some((k2, k3)))
		},
		v => fail!(SKEPE::BadBasis(v))
	};
	Ok(r)
}

fn __remove_zeros<'a>(x: &'a [u8]) -> &'a [u8]
{
	static ZERO: &'static [u8] = &[0];
	match x.iter().cloned().position(|b|b>0) {
		Some(leading_zeroes) => x.get(leading_zeroes..).unwrap_or(ZERO),
		None => ZERO
	}
}

fn __number_ge_z(a: &[u8], b: &[u8]) -> bool
{
	if a.len() > b.len() { return false; }
	if a.len() < b.len() { return true; }
	for (a,b) in a.iter().zip(b.iter()) {
		if a > b { return true; }
		if a < b { return false; }
	}
	true
}

fn __is_odd(x: &[u8]) -> bool
{
	x.last().map(|y|y%2!=0).unwrap_or(false)
}

///Elliptic Curve over prime field.
#[derive(Copy,Clone)]
pub struct PrimeCurve<'a>
{
	///The base field modulus.
	pub p: &'a [u8],
	///Curve parmeter a.
	///
	///This is coefficient of the term linear in x in the curve equation.
	pub a: &'a [u8],
	///Curve parmeter b.
	///
	///This is coefficient of the term constant in x in the curve equation.
	pub b: &'a [u8],
	///Curve base point.
	pub base: &'a [u8],
	///Curve order.
	pub order: &'a [u8],
	///Curve cofactor.
	///
	///Order times cofactor approximately equals `p`.
	pub cofactor: &'a [u8],
	x: PhantomData<&'a u8>
}

impl<'a> PrimeCurve<'a>
{
	fn __read(data: &mut Message<'a>) -> Result<PrimeCurve<'a>, _ServerKeyExchangeParseError>
	{
		use _ServerKeyExchangeParseError as SKEPE;
		let p = __remove_zeros(data.slice(LEN_ECPRIME_P).map_err(SKEPE::PrimeP)?);
		let a = __remove_zeros(data.slice(LEN_ECPRIME_P).map_err(SKEPE::PrimeA)?);
		let b = __remove_zeros(data.slice(LEN_ECPRIME_P).map_err(SKEPE::PrimeB)?);
		let base = data.slice(LEN_ECPRIME_P).map_err(SKEPE::PrimeBase)?;
		let order = __remove_zeros(data.slice(LEN_ECPRIME_P).map_err(SKEPE::PrimeOrder)?);
		let cofactor = __remove_zeros(data.slice(LEN_ECPRIME_P).map_err(SKEPE::PrimeCofactor)?);
		fail_if!(__number_ge_z(a, p) || !__is_odd(a), SKEPE::PrimeATooLarge);
		fail_if!(__number_ge_z(b, p) || !__is_odd(b), SKEPE::PrimeBTooLarge);
		//TODO: Check base encoding.
		//TODO: Check order.
		//TODO: Check cofactor.
		Ok(PrimeCurve {
			p,a,b,base,order,cofactor,
			x: PhantomData
		})
	}
}

///An Elliptic Curve over binary field.
#[derive(Copy,Clone)]
pub struct BinaryCurve<'a>
{
	///The highest power in field element basis.
	///
	///The basis can be either `x^m+x^k+1=0` or `x^m+x^k1+x^k2+x^k3+1=0`.
	///
	///This is equivalently:
	///
	/// * Base-2 logarithm of field size.
	/// * Number of bits in elements.
	pub m: u16,
	//These are vectors in TLS, but as they are <m, and m is 16-bit...
	///The middle power(s) in field element basis.
	///
	///This can either be `(k,None)` or `(k1,Some((k2,k3)))`, depending on if the basis equation has three
	///(trinomial) or five (pentanomial) terms. `m>k1>k2>k3>0`, and `m>k>0`.
	pub basis: (u16, Option<(u16, u16)>),
	///The binary representation of coefficient a of the curve.
	pub a: &'a [u8],
	///The binary representation of coefficient b of the curve.
	pub b: &'a [u8],
	///Base point on the curve.
	pub base: &'a [u8],
	///Order of the base point.
	pub order: &'a [u8],
	///Cofactor of the order of the curve.
	///
	///Order times cofactor approximately equals `2^m`.
	pub cofactor: &'a [u8],
	x: PhantomData<&'a u8>
}

impl<'a> BinaryCurve<'a>
{
	fn __read(data: &mut Message<'a>) -> Result<BinaryCurve<'a>, _ServerKeyExchangeParseError>
	{
		use _ServerKeyExchangeParseError as SKEPE;
		let (m, k, kh) = __read_basis(data)?;
		let a = data.slice(LEN_ECBIN_P).map_err(SKEPE::BinaryA)?;
		let b = data.slice(LEN_ECBIN_P).map_err(SKEPE::BinaryB)?;
		let base = data.slice(LEN_ECBIN_P).map_err(SKEPE::BinaryBase)?;
		let order = data.slice(LEN_ECBIN_P).map_err(SKEPE::BinaryOrder)?;
		let cofactor = data.slice(LEN_ECBIN_P).map_err(SKEPE::BinaryCofactor)?;
		//TODO: Check a encoding.
		//TODO: Check b encoding.
		//TODO: Check order.
		//TODO: Check cofactor.
		Ok(BinaryCurve {
			m,a,b,base,order,cofactor,
			basis: (k, kh),
			x: PhantomData
		})
	}
}

///An Elliptic Curve.
///
///The curve may be:
///
/// * Over binary field.
/// * Over prime field.
/// * Special named elliptic curve (the most common case).
#[derive(Copy,Clone)]
#[non_exhaustive]
pub enum EllipticCurve<'a>
{
	///Arbitrary curve over prime field.
	ExplicitPrime(PrimeCurve<'a>),
	///Arbitrary curve over binary field.
	ExplicitBinary(BinaryCurve<'a>),
	///Special named elliptic curve (this is the most common).
	Named(NamedGroup),
}

impl<'a> EllipticCurve<'a>
{
	fn __read(data: &mut Message<'a>) -> Result<EllipticCurve<'a>, _ServerKeyExchangeParseError>
	{
		use _ServerKeyExchangeParseError as SKEPE;
		let d: u8 = data.cp().map_err(SKEPE::CurveType)?;
		let r = match d {
			1 => PrimeCurve::__read(data).map(EllipticCurve::ExplicitPrime)?,
			2 => BinaryCurve::__read(data).map(EllipticCurve::ExplicitBinary)?,
			3 => {
				let g: NamedGroup = data.cp().map_err(SKEPE::Named)?;
				let badn = SKEPE::BadNamed(g);
				fail_if!(g.is_grease(), badn);
				fail_if!(g == NamedGroup::ARBITRARY_EXPLICIT_PRIME_CURVES, badn);
				fail_if!(g == NamedGroup::ARBITRARY_EXPLICIT_CHAR2_CURVES, badn);
				EllipticCurve::Named(g)
			},
			v => fail!(SKEPE::BadCurveType(v))
		};
		Ok(r)
	}
}

///Parse Certificate Status message.
///
///The parameter `enabled` controls what TLS features are enabled. This is used to enforce the TLS
///restriction that answers can not contain algorithms or extensions not present in offers. For server
///certificate, the corresponding offer is Client Hello, and for client certificate, the corresponding
///offer is Certificate Request. The following features may be requested:
///
/// * `Extension(STATUS_REQUEST)` or `Extension(STATUS_REQUEST_V2)`, together with:
///   * `StatusRequestType(type)`, where `type` is type of the status (usually `OCSP`).
pub fn parse_certificate_status<'a>(data: &'a [u8], enable: RemoteFeatureSet, dbg: &mut DbgT) ->
	Result<CertificateStatus<'a>, CertificateStatusParseError>
{
	let mut data = Message::new(data);
	let status = CertificateStatus::read_item(&mut data, enable, false).map_err(|e|{
		CertificateStatusParseError(_CertificateStatusParseError::CertificateStatus(e))
	})?;
	data.assert_end(CertificateStatusParseError(_CertificateStatusParseError::JunkAfterEnd))?;
	if let Some(x) = status.as_ocsp() {
		debug!(dbg "Received OCSP staple ({xlen} bytes)", xlen=x.get_response().len());
	} else if let Some(x) = status.as_ocsp_multi() {
		debug!(dbg "Received OCSP multistaple for {xcnt} certs", xcnt=x.count_all(true));
	} else {
		debug!(dbg "Received unknown status type {st} ({sl} bytes)",
			st=status.get_status_type(), sl=status.get_raw_response().len());
	}
	Ok(status)
}

///Form of Server Key Exchange.
#[derive(Copy,Clone)]
#[non_exhaustive]
pub enum ServerPublicKey<'a>
{
	///Elliptic curve public key.
	Ec{curve: EllipticCurve<'a>, point: &'a [u8]},
	///Diffie-Hellman public key (p,g,y_s).
	Dh{modulus: &'a [u8], generator: &'a [u8], public: &'a [u8]},
	///Secure Remote Password (N,g,s,B).
	Srp{modulus: &'a [u8], generator: &'a [u8], salt: &'a [u8], public: &'a [u8]},
}

///Server Key Exchange message.
///
/// # Supported message subtypes:
///
/// * `{DH,DHE}-{RSA,DSS}`
/// * `{ECDH,ECDHE}-{RSA,ECDSA}`
/// * `{DHE,ECDHE}-{Anon,PSK}`
/// * `SRP-SHA{,-RSA,-DSS}`
/// * `PSK`
/// * `RSA-PSK`
///
/// # Unsupported message subtypes.
///
/// * `EXPORT` anything: Those are not even valid in TLS.
/// * `ECCPWD` (a.k.a. Dragonfly). Too screwed up spec.
/// * `KRB5`: Nobody cares about it (no AES for instance).
/// * `TLS13`: TLS 1.3 uses totally different message.
#[derive(Clone)]
pub struct ServerKeyExchange<'a>
{
	public: Option<ServerPublicKey<'a>>,
	psk_identity_hint: Option<&'a [u8]>,
	signature: Option<Signature<'a>>,
}

impl<'a> ServerKeyExchange<'a>
{
	///Get the PSK identity hint, if any.
	///
	///Only `*_PSK` types have identity hints.
	pub fn get_psk_identity_hint(&self) -> Option<&'a [u8]> { self.psk_identity_hint }
	///Get the public key if any.
	///
	///Only types `DHE_*`, `ECDHE_*` and `SRP_*` have public keys.
	pub fn get_public_key(&self) -> Option<ServerPublicKey<'a>> { self.public }
	///Get the signature, if any.
	///
	///Only types `DHE_RSA`, `DHE_DSS`, `ECDHE_RSA`, `ECDHE_ECDSA`, `SRP_RSA` and `SRP_DSS` have signatures.
	///
	///The partial TBS is the raw parameters structure.
	pub fn get_signature(&self) -> Option<Signature<'a>> { self.signature }
	///Parse Server Key Exchange message.
	///
	///The cipher suite is used to decide the format of the SKE.  The key_ok callback is only used on
	///named-group ECDHE keys. It is assumed that enable allows the ciphersuite.
	pub fn parse(data: &'a [u8], cipher_suite: CipherSuite, enable: RemoteFeatureSet, key_ok: KeyCheck,
		dbg: &mut DbgT) -> Result<ServerKeyExchange<'a>, ServerKeyExchangeParseError>
	{
		Self::_parse(Message::new(data), cipher_suite, enable, key_ok, dbg).
			map_err(ServerKeyExchangeParseError)
	}
	fn _parse(mut data: Message<'a>, cipher_suite: CipherSuite, enable: RemoteFeatureSet, key_ok: KeyCheck,
		dbg: &mut DbgT) -> Result<ServerKeyExchange<'a>, _ServerKeyExchangeParseError>
	{
		use self::_ServerKeyExchangeParseError as SKEPE;
		let omsg: &'a [u8] = data.as_inner();
		use btls_aux_tls_iana::CipherSuiteKex as KEX;
		let psk_identity_hint = match cipher_suite.kex() {
			KEX::DhePsk|KEX::Psk|KEX::RsaPsk|KEX::EcdhePsk => {
				let id = data.slice(LEN_PSK_IDENTITY_HINT).map_err(SKEPE::PskIdentityHint)?;
				debug!(dbg "PSK identity hint <{id}>", id=Hexdump(id));
				Some(id)
			},
			_ => None
		};
		let public = match cipher_suite.kex() {
			KEX::DhDss|KEX::DhRsa|KEX::EcdhEcdsa|KEX::EcdhRsa|KEX::Rsa => fail!(SKEPE::MessageAbsent),
			KEX::Psk|KEX::RsaPsk => None,
			KEX::DheAnon|KEX::DheDss|KEX::DhePsk|KEX::DheRsa => {
				let dh_p = data.slice(LEN_DH_P).map_err(SKEPE::DhP)?;
				let dh_g = data.slice(LEN_DH_P).map_err(SKEPE::DhG)?;
				let dh_ys = data.slice(LEN_DH_P).map_err(SKEPE::Public3)?;
				debug!(dbg "Diffie-Hellman key for key exchange: {dhlen} bytes", dhlen=dh_p.len());
				Some(ServerPublicKey::Dh{modulus:dh_p, generator:dh_g, public:dh_ys})
			},
			KEX::EcdheAnon|KEX::EcdheEcdsa|KEX::EcdhePsk|KEX::EcdheRsa => {
				let curve = EllipticCurve::__read(&mut data)?;
				let gcheck = match &curve {
					&EllipticCurve::ExplicitBinary(_) =>
						NamedGroup::ARBITRARY_EXPLICIT_CHAR2_CURVES,
					&EllipticCurve::ExplicitPrime(_) =>
						NamedGroup::ARBITRARY_EXPLICIT_PRIME_CURVES,
					&EllipticCurve::Named(g) => g,
				};
				fail_if!(!crate::ask_group(enable, gcheck), SKEPE::BadCurve(gcheck));
				let point = data.slice(LEN_ECDH_Y).map_err(SKEPE::Public)?;
				match &curve {
					&EllipticCurve::Named(g) => fail_if!(!key_ok(g, point), SKEPE::BadPoint(g)),
					_ => ()		//TODO: Check these.
				}

				debug!(dbg "Elliptic curve for key exchange: {gcheck} ({plen} bytes)",
					plen=point.len());
				Some(ServerPublicKey::Ec{curve, point})
			},
			KEX::SrpSha|KEX::SrpShaDss|KEX::SrpShaRsa => {
				let srp_n = data.slice(LEN_SRP_N).map_err(SKEPE::SrpN)?;
				let srp_g = data.slice(LEN_SRP_N).map_err(SKEPE::SrpG)?;
				let srp_s = data.slice(LEN_SRP_S).map_err(SKEPE::SrpS)?;
				let srp_b = data.slice(LEN_SRP_N).map_err(SKEPE::SrpB)?;
				Some(ServerPublicKey::Srp{modulus:srp_n, generator:srp_g, salt:srp_s, public:srp_b})
			},
			_ => fail!(SKEPE::UnsupportedCipher(cipher_suite))
		};
		let params_len = omsg.len() - data.length();
		let params = omsg.get(..params_len).unwrap_or(b"");
		let has_signature = match cipher_suite.kex() {
			KEX::DhDss|KEX::DheAnon|KEX::DhePsk|KEX::DhRsa|KEX::EcdheAnon|KEX::EcdhEcdsa|KEX::EcdhePsk|
				KEX::EcdhRsa|KEX::Psk|KEX::Rsa|KEX::RsaPsk|KEX::SrpSha => false,
			KEX::DheDss|KEX::DheRsa|KEX::EcdheEcdsa|KEX::EcdheRsa|KEX::SrpShaDss|KEX::SrpShaRsa =>
				true,
			_ => fail!(SKEPE::UnsupportedCipher(cipher_suite))
		};
		let signature = if has_signature {
			let algorithm: SignatureScheme = data.cp().map_err(SKEPE::Algorithm)?;
			fail_if!(!crate::ask_signature_algorithm(enable, algorithm), SKEPE::BadSignature(algorithm));
			let signature = data.slice(LEN_SIGNATURE).map_err(SKEPE::Signature)?;
			debug!(dbg "Signature algorithm {algorithm} ({slen} bytes)", slen=signature.len());
			Some(Signature {
				partial_tbs: params,
				algorithm,
				signature,
			})
		} else {
			debug!(dbg "No server signature");
			None
		};
		data.assert_end(_ServerKeyExchangeParseError::JunkAfterEnd)?;
		Ok(ServerKeyExchange{public, psk_identity_hint, signature})
	}
}

///Form of Client Key Exchange.
#[derive(Copy,Clone)]
#[non_exhaustive]
pub enum ClientPublicKey<'a>
{
	///Elliptic curve public key.
	Ec{point: &'a [u8]},
	///Diffie-Hellman public key.
	Dh{public: &'a [u8]},
	///RSA Encrypted Pre-master secret.
	Rsa{encrypted_pms: &'a [u8]},
	///SRP A value.
	Srp{public: &'a [u8]},
}

///Client Key Exchange message.
#[derive(Clone)]
pub struct ClientKeyExchange<'a>
{
	psk_identity: Option<&'a [u8]>,
	public: Option<ClientPublicKey<'a>>,
	x: PhantomData<&'a u8>
}


impl<'a> ClientKeyExchange<'a>
{
	///Get the PSK identity used, if any.
	pub fn get_psk_identity(&self) -> Option<&'a [u8]> { self.psk_identity }
	///Get the client public key, if any.
	pub fn get_public_key(&self) -> Option<ClientPublicKey<'a>> { self.public }
	///Parse Client Key Exchange message.
	///
	///The `cipher_suite` argument is used to determine the subformat of the Client Key Exchange message.
	///The same key exchange types are supported as in `ServerKeyExchange`. If the subformat is
	///`ECDH_*` or `ECDHE_*`, the `group` paramter is passed to the `key_ok` callback used to validate the
	///public key.
	pub fn parse(data: &'a [u8], cipher_suite: CipherSuite, group: NamedGroup, implicit_key: Option<&'a [u8]>,
		key_ok: KeyCheck, dbg: &mut DbgT) -> Result<ClientKeyExchange<'a>, ClientKeyExchangeParseError>
	{
		Self::_parse(Message::new(data), cipher_suite, group, implicit_key, key_ok, dbg).
			map_err(ClientKeyExchangeParseError)
	}
	fn _parse(mut data: Message<'a>, cipher_suite: CipherSuite, group: NamedGroup, implicit_key: Option<&'a [u8]>,
		key_ok: KeyCheck, dbg: &mut DbgT) -> Result<ClientKeyExchange<'a>, _ClientKeyExchangeParseError>
	{
		use btls_aux_tls_iana::CipherSuiteKex as KEX;
		use _ClientKeyExchangeParseError as CKEPE;
		let psk_identity = match cipher_suite.kex() {
			KEX::DhePsk|KEX::Psk|KEX::RsaPsk|KEX::EcdhePsk => {
				let id = data.slice(LEN_PSK_IDENTITY).map_err(CKEPE::PskIdentity)?;
				debug!(dbg "PSK identity <{id}>", id=Hexdump(id));
				Some(id)
			},
			_ => None
		};
		let public = match cipher_suite.kex() {
			KEX::DhDss|KEX::DheAnon|KEX::DheDss|KEX::DheRsa|KEX::DhRsa|KEX::DhePsk => {
				let point = if let Some(ik) = implicit_key {
					ik
				} else {
					data.slice(LEN_DH_P).map_err(CKEPE::Public2)?
				};
				debug!(dbg "Client diffie-hellman key: {plen} bytes", plen=point.len());
				Some(ClientPublicKey::Dh{public:point})
			},
			KEX::EcdhEcdsa|KEX::EcdheAnon|KEX::EcdheEcdsa|KEX::EcdheRsa|KEX::EcdhRsa|KEX::EcdhePsk => {
				let point = if let Some(ik) = implicit_key {
					ik
				} else {
					data.slice(LEN_ECDH_Y).map_err(CKEPE::Public)?
				};
				fail_if!(!key_ok(group, point), CKEPE::BadPoint(group));
				debug!(dbg "Client elliptic curve key: {plen} bytes", plen=point.len());
				Some(ClientPublicKey::Ec{point})
			},
			KEX::Rsa|KEX::RsaPsk => {
				let point = data.slice(LEN_RSA_PMS).map_err(CKEPE::EncryptedPremasterSecret)?;
				debug!(dbg "Client RSA encrypted premaster secret: {plen} bytes", plen=point.len());
				Some(ClientPublicKey::Rsa{encrypted_pms:point})
			},
			KEX::SrpSha|KEX::SrpShaDss|KEX::SrpShaRsa => {
				let point = data.slice(LEN_SRP_N).map_err(CKEPE::SrpA)?;
				debug!(dbg "Secure Remote Password A: {plen} bytes", plen=point.len());
				Some(ClientPublicKey::Srp{public:point})
			}
			KEX::Psk => None,
			_ => fail!(CKEPE::UnsupportedCipher(cipher_suite))
		};
		data.assert_end(CKEPE::JunkAfterEnd)?;
		Ok(ClientKeyExchange {
			public,
			psk_identity,
			x: PhantomData
		})
	}
}
