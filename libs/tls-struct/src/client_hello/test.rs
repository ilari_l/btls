use super::ClientHello;
use std::vec::Vec;

fn vec_from_hex(x: &str) -> Vec<u8>
{
	let mut y = Vec::new();
	let mut h: u8 = 255;
	for c in x.chars() {
		let j = match c as u32 {
			z@48..=57 => z as u8 - 48,
			z@65..=70 => z as u8 - 55,
			z@97..=102 => z as u8 - 87,
			_ => continue
		};
		if h > 15 {
			h = j;
		} else {
			y.push(h * 16 + j);
			h = 255;
		}
	}
	y
}

#[test]
fn valid_client_hello()
{
	let msg = vec_from_hex("
0303 20F0 1D55 D96D 46AE A60C 5A4F 2591
B166 1294 363A BC8B 3761 2FB1 4DA8 6383
C5C2 20CF 5FB4 63B9 BD58 7174 9DE2 DB71
F67B 11BB 532A 756A 64DB AEFE 3AD9 800B
5FF0 CB00 20FA FA13 0113 0213 03C0 2BC0
2FC0 2CC0 30CC A9CC A8C0 13C0 1400 9C00
9D00 2F00 3501 0001 931A 1A00 0000 0000
1400 1200 000F 6C63 6A70 7269 6E74 696E
672E 636F 6D00 1700 00FF 0100 0100 000A
000A 0008 9A9A 001D 0017 0018 000B 0002
0100 0023 0000 0010 000E 000C 0268 3208
6874 7470 2F31 2E31 0005 0005 0100 0000
0000 0D00 1200 1004 0308 0404 0105 0308
0505 0108 0606 0100 1200 0000 3300 2B00
299A 9A00 0100 001D 0020 D80C 4132 0673
B987 FAE5 0934 3348 D5B3 01B8 C725 FF35
3678 55D1 4D46 37A9 F42F 002D 0002 0101
002B 000B 0A7A 7A03 0403 0303 0203 0100
1B00 0302 0002 3A3A 0001 0000 1500 CD00
0000 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000          
	");
	let ch = ClientHello::parse(&msg, &|_,_|true, Some(&mut |a|{
		println!("{a}");
	})).expect("Client hello should be valid");
	assert_eq!(ch.client_version, btls_aux_tls_iana::TlsVersion::TLS_1_2);
	assert_eq!(&ch.random, b"\x20\xF0\x1D\x55\xD9\x6D\x46\xAE\xA6\x0C\x5A\x4F\x25\x91\xB1\x66\
		\x12\x94\x36\x3A\xBC\x8B\x37\x61\x2F\xB1\x4D\xA8\x63\x83\xC5\xC2");
	assert_eq!(&ch.session_id, b"\xCF\x5F\xB4\x63\xB9\xBD\x58\x71\x74\x9D\xE2\xDB\x71\xF6\x7B\x11\
		\xBB\x53\x2A\x75\x6A\x64\xDB\xAE\xFE\x3A\xD9\x80\x0B\x5F\xF0\xCB");
	assert!(!ch.supports_client_certificate_url);
	assert!(!ch.supports_truncated_hmac);
	assert!(ch.supports_signed_certificate_timestamp);
	assert!(!ch.supports_encrypt_then_mac);
	assert!(ch.supports_extended_master_secret);
	assert!(!ch.supports_tls_lts);
	assert!(!ch.supports_tls_cert_with_extern_psk);
	assert!(!ch.supports_early_data);
	assert!(!ch.supports_post_handshake_auth);
	assert!(!ch.supports_transparency_info);
	assert!(ch.supported_user_mapping_types.iter().count() == 0);
	assert!(ch.supported_client_authz_formats.iter().count() == 0);
	assert!(ch.supported_server_authz_formats.iter().count() == 0);
	assert!(ch.supported_certificate_types.iter().count() == 1);
	assert!(ch.supported_client_certificate_types.iter().count() == 1);
	assert!(ch.supported_server_certificate_types.iter().count() == 1);
	assert!(ch.supported_signature_algorithms_dcred.iter().count() == 0);
	assert!(ch.supported_ekt_ciphers.iter().count() == 0);
	assert!(ch.heartbeat == None);
	//FIXME: Make the test actually check all fields.
	println!("{ch:?}");
/*
	let get_cipher_suites = ch.get_cipher_suites()
	let get_compression_methods = ch.get_compression_methods()
	println!("Cipher suites: {get_cipher_suites:?}");
	println!("Compression methods: {get_compression_methods:?}");
	for (extn, extv) in ch.get_extensions() {
		let extvl = extv.len();
		println!("Extension {extn} ({extvl} bytes)");
		assert!(ch.has_extension(extn));
	}
	let supported_groups = ch.supported_groups()
	let supported_ec_point_formats = ch.supported_ec_point_formats()
	let supported_psk_key_exchange_modes = ch.supported_psk_key_exchange_modes()
	let supported_signature_algorithms = ch.supported_signature_algorithms()
	let supported_signature_algorithms_cert = ch.supported_signature_algorithms_cert()
	let supported_certificate_compressions = ch.supported_certificate_compressions()
	let get_application_layer_protocols = ch.get_application_layer_protocols()
	println!("Supported groups: {supported_groups:?}");
	println!("Supported EC point formats: {supported_ec_point_formats:?}");
	println!("Supported PSK key exchange modes: {supported_psk_key_exchange_modes:?}");
	println!("Supported signature algorithms: {supported_signature_algorithms:?}");
	println!("Supported signature algorithms (cert): {supported_signature_algorithms_cert:?}");
	println!("Supported certificate compressions: {supported_certificate_compressions:?}");
	println!("Supported application protocols: {get_application_layer_protocols:?}");
*/
	/*
impl<'a> ClientHello<'a>
{
	pub fn get_key_shares(&self) -> KeySharesIter<'a> { self.key_shares.clone() }
	pub fn get_cached_info(&self) -> CachedInformationIter<'a> { self.ext_cached_info.clone() }
	pub fn get_application_layer_protocols(&self) -> ApplicationLayerProtocolIter<'a>
	pub fn get_srp_username(&self) -> &'a [u8] { self.ext_srp }
	pub fn get_padding_amount(&self) -> usize { self.ext_padding }
	pub fn get_host_name(&self) -> &'a [u8] { self.ext_server_name_host_name }
	pub fn get_server_name(&self) -> ServerNameIter<'a> { self.ext_server_name.clone() }
}

impl<'a> ClientHello<'a>
{
	pub fn suupported_trusted_ca_keys(&self) -> TrustedCaKeyIter<'a> { self.ext_trusted_ca_keys.clone() }
	pub fn supported_status_request_v2(&self) -> StatusRequestV2Iter<'a> { self.ext_status_request_v2.clone() }
	pub fn supported_versions(&self) -> TlsVersionList<'a> { self.ext_supported_versions }
	pub fn supported_ertificate_authorities(&self) -> CertificateAuthoritiesIter<'a>
}

impl<'a> ClientHello<'a>
{
	pub fn get_max_fragment_length(&self) -> Option<MaximumFragmentLength> { self.ext_max_fragment_length }
	pub fn get_status_request(&self) -> StatusRequest<'a> { self.ext_status_request }
	pub fn get_use_srtp(&self) -> Option<UseSrtpRequest<'a>> { self.ext_use_srtp }
	pub fn get_token_binding(&self) -> Option<TokenBindingRequest<'a>> { self.ext_token_binding }
	pub fn get_record_size_limit(&self) -> Option<u16> { self.ext_record_size_limit }
	pub fn get_pwd_protect(&self) -> Option<&'a [u8]> { self.ext_pwd_protect }
	pub fn get_pwd_clear(&self) -> Option<&'a [u8]> { self.ext_pwd_clear }
	pub fn get_password_salt(&self) -> Option<&'a [u8]> { self.ext_password_salt }
	pub fn get_ticket_pinning(&self) -> Option<TicketPinningRequest<'a>> { self.ext_ticket_pinning }
	pub fn get_session_ticket(&self) -> Option<&'a [u8]> { self.ext_session_ticket }
	pub fn get_tlmsp(&self) -> Option<&'a [u8]> { self.ext_tlmsp }
	pub fn get_tlmsp_proxying(&self) -> Option<&'a [u8]> { self.ext_tlmsp_proxying }
	pub fn get_tlmsp_delegate(&self) -> Option<&'a [u8]> { self.ext_tlmsp_delegate }
	pub fn get_pre_shared_key(&self) -> PreSharedKeyIter<'a> { self.ext_pre_shared_key.clone() }
	pub fn get_cookie(&self) -> Option<&'a [u8]> { self.ext_cookie }
	pub fn get_connection_id_old(&self) -> Option<&'a [u8]> { self.ext_connection_id_old }
	pub fn get_connection_id(&self) -> Option<&'a [u8]> { self.ext_connection_id }
	pub fn get_external_id_hash(&self) -> Option<&'a [u8]> { self.ext_external_id_hash }
	pub fn get_external_session_id(&self) -> Option<&'a [u8]> { self.ext_external_session_id }
	pub fn get_quic_transport_parameters(&self) -> Option<&'a [u8]> { self.ext_quic_transport_parameters }
	pub fn get_ticket_request(&self) -> Option<ClientTicketRequest> { self.ext_ticket_request }
	pub fn get_dnssec_chain(&self) -> Option<u16> { self.ext_dnssec_chain }
	pub fn get_renegotiation_info(&self) -> Option<&'a [u8]> { self.ext_renegotiation_info }
}

*/
	//assert!(false);
}

/*
*/
