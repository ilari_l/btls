pub(crate) struct PrintAscii<'a>(pub(crate) &'a [u8]);

impl<'a> core::fmt::Display for PrintAscii<'a>
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		write!(f, "<")?;
		for &b in self.0.iter() {
			if b >= 33 && b <= 126 && b != b'\\' {
				write!(f, "{c}", c=b as char)?;
			} else {
				write!(f, "\\{b:02x}")?;
			}
		}
		write!(f, ">")?;
		Ok(())
	}
}

