#![allow(unsafe_code)]
#![deny(unused_must_use)]

mod certificate;
mod cipher_suite;
//mod delegated_credential;
mod key_share_entry;
mod ocsp;
mod psk;
mod server_name;
mod simple;
mod types;
pub use self::certificate::*;
//pub use self::delegated_credential::*;
pub use self::key_share_entry::*;
pub use self::ocsp::*;
pub use self::psk::*;
pub use self::server_name::*;
pub use self::types::*;
#[doc(hidden)] pub mod sort_test;

use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_serialization::VectorLength;
use btls_aux_tls_iana::Alert;
use btls_aux_tls_iana::helper::MakeCodepoint;
use btls_aux_tls_iana::helper::XRead;
use core::cmp::Ordering;
use core::marker::PhantomData;
use core::fmt::Debug;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter as Formatter;
use core::mem::replace;

struct PrintableItemWrapper<'a,P:PrintableItem>(&'a P);

impl<'a,P:PrintableItem> Display for PrintableItemWrapper<'a,P>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		PrintableItem::display_item(self.0, f)
	}
}
	
///Get alert.
pub(crate) trait AlertForError
{
	///Alert for error.
	fn alert(&self) -> Alert;
}

///Printable type.
pub(crate) trait PrintableItem
{
	///Display the item.
	fn display_item(&self, f: &mut Formatter) -> Result<(), FmtError>;
}

impl PrintableItem for crate::Void
{
	//Can not be called.
	fn display_item(&self, _: &mut Formatter) -> Result<(), FmtError> { Ok(()) }
}

#[derive(Copy,Clone,Debug)]
pub(crate) struct Trivial;

impl AlertForError for Trivial
{
	fn alert(&self) -> Alert { Alert::INTERNAL_ERROR }
}

impl Display for Trivial
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		f.write_str("Internal error: infallible operation failed")
	}
}

///Error reading primimitive value.
#[derive(Copy,Clone,Debug)]
pub(crate) enum PrimitiveReadError
{
	///Value truncated.
	Truncated(usize, usize),
}

impl AlertForError for PrimitiveReadError
{
	fn alert(&self) -> Alert { Alert::DECODE_ERROR }
}

impl Display for PrimitiveReadError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::PrimitiveReadError::*;
		match self {
			Truncated(a,b) => write!(f, "Value truncated ({a} < {b})"),
		}
	}
}

///Error reading a slice.
#[derive(Copy,Clone,Debug)]
pub(crate) enum SliceReadError
{
	///Length truncated.
	LengthTruncated(usize, usize),
	///Value truncated.
	ValueTruncated(usize, usize),
	///Bad value length.
	BadLength(usize),
	///Invalid size upper bound.
	InvalidSizeUpperBound(usize),
}

impl AlertForError for SliceReadError
{
	fn alert(&self) -> Alert
	{
		//InvalidSizeUpperBound is INTERNAL_ERROR, as it is code bug. The rest are all DECODE_ERROR.
		match self {
			SliceReadError::InvalidSizeUpperBound(_) => Alert::INTERNAL_ERROR,
			_ => Alert::DECODE_ERROR
		}
		
	}
}

impl Display for SliceReadError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::SliceReadError::*;
		match self {
			LengthTruncated(a,b) => write!(f, "Vector length truncated ({a} < {b})"),
			ValueTruncated(a,b) => write!(f, "Vector body truncated ({a} < {b})"),
			BadLength(a) => write!(f, "Bad vector body length {a}"),
			InvalidSizeUpperBound(a) => write!(f, "Internal Error: Invalid size upper bound {a}"),
		}
	}
}

///Error reading single item.
#[derive(Copy,Clone)]
pub(crate) enum ItemError<I:IterableItemE+'static>
{
	Single(PrimitiveReadError),
	Single2(<I as IterableItemE>::ReadError),
	BogusElement(<I as IterableItemE>::LaunderedItem),
	Containing(SliceReadError),
	MoreThanOneItem,
}

impl<I:IterableItemE+'static> Debug for ItemError<I>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&ItemError::Single(ref e) => write!(f, "ItemError::Single({e:?})"),
			&ItemError::Single2(ref e) => write!(f, "ItemError::Single2({e:?})"),
			&ItemError::BogusElement(ref e) => write!(f, "ItemError::BogusElement({e:?})"),
			&ItemError::Containing(ref e) => write!(f, "ItemError::Containing({e:?})"),
			&ItemError::MoreThanOneItem => write!(f, "ItemError::MoreThanOneItem"),
		}
	}
}

impl<I:IterableItemE+'static> AlertForError for ItemError<I>
{
	fn alert(&self) -> Alert
	{
		match self {
			&ItemError::Single(ref e) => e.alert(),
			&ItemError::Single2(ref e) => e.alert(),
			&ItemError::BogusElement(_) => Alert::ILLEGAL_PARAMETER,
			&ItemError::Containing(e) => e.alert(),
			&ItemError::MoreThanOneItem => Alert::DECODE_ERROR,
		}
	}
}

impl<I:IterableItemE+'static> Display for ItemError<I>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&ItemError::Single(ref e) => Display::fmt(e, f),
			&ItemError::Single2(ref e) => Display::fmt(e, f),
			&ItemError::BogusElement(ref elem) =>
				write!(f, "Illegal {elem}", elem=PrintableItemWrapper(elem)),
			&ItemError::Containing(ref err) => write!(f, "Containing list: {err}"),
			&ItemError::MoreThanOneItem => write!(f, "Junk after end of item"),
		}
	}
}


///Error reading vector.
#[derive(Copy,Clone)]
pub(crate) enum VectorError<I:IterableItemE+'static>
{
	///Failed to read the top list.
	TopList(SliceReadError),
	///Failed to read element.
	Element(usize, <I as IterableItemE>::ReadError),
	///Filtered list is empty and it can not be.
	FilteredEmpty,
	///Bogus element.
	BogusElement(<I as IterableItemE>::LaunderedItem),
}

impl<I:IterableItemE+'static> Debug for VectorError<I>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&VectorError::TopList(ref err) => write!(f, "VectorError::TopList({err:?})"),
			&VectorError::Element(idx, ref err) => write!(f, "VectorError::Element({idx}, {err:?})"),
			&VectorError::FilteredEmpty => write!(f, "VectorError::FilteredEmpty"),
			&VectorError::BogusElement(ref err) => write!(f, "VectorError::BogusElement({err:?})"),
		}
	}
}

impl<I:IterableItemE+'static> AlertForError for VectorError<I>
{
	fn alert(&self) -> Alert
	{
		match self {
			&VectorError::TopList(ref err) => err.alert(),
			&VectorError::Element(_, ref err) => err.alert(),
			&VectorError::FilteredEmpty => Alert::ILLEGAL_PARAMETER,
			&VectorError::BogusElement(_) => Alert::ILLEGAL_PARAMETER,
		}
	}
}

impl<I:IterableItemE+'static> Display for VectorError<I>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&VectorError::TopList(ref err) => Display::fmt(err, f),
			&VectorError::Element(idx, ref err) => write!(f, "Item#{idx}: {err}"),
			&VectorError::FilteredEmpty => write!(f, "No item in list was valid"),
			&VectorError::BogusElement(ref elem) =>
				write!(f, "Illegal {elem}", elem=PrintableItemWrapper(elem)),
		}
	}
}


///Generic item to iterate over.
///
/// # Unsafety:
///
///The only invariant the implementation must uphold is that `special_default()` returns a valid datastream, with
///valid item count, and none of these items are filtered.
///
///Note that empty datastream with count of 0 is always valid. Therefore unless `special_default()` is overridden,
///this trait is actually safe to implement.
pub unsafe trait IterableItem<'a>: Copy+Sized
{
	///Class of item.
	fn listclass() -> &'static str;
	///Read raw item.
	///
	///Unsafety: It is **UNDEFINED BEHAVIOR** to invoke this on datastream that does not have a valid next item.
	///Doing so probably leads into **ILLEGAL OPERATION**, something that **MUST NEVER HAPPEN**.
	unsafe fn read_item_unchecked(data: &mut Message<'a>) -> Self;
	///Should the item be filtered?
	fn is_filtered(&self) -> bool { false }
	///Print item name.
	fn print(&self, f: &mut Formatter) -> Result<(), FmtError>;
	///Special default. The first part is the datastram, the second is number of items in the datastream.
	///
	///The default implementation returns empty slice with 0 items (this is always safe).
	///
	///Unsafety: The data stream must be valid, and count must be correct. In addition, none of the items may
	///be filtered.
	fn special_default() -> (&'static [u8], usize) { (b"", 0) }
}

///Lifetime-invariant attributes of generic item.
pub(crate) trait IterableItemE
{
	///Constant size of item. Set to zero if item size is variable.
	///
	///If nonzero, there are additional invariants that must be upheld:
	///
	/// * `read_item_unchecked()` MUST consume this amount of bytes each time it is called.
	/// * `read_item_unchecked()` MUST NOT execute Illegal Operations if called on at least that amount of data.
	/// * If NONTRIVIAL_CHECK is false, `read_item_unchecked()` MUST return Safe values.
	/// * If NONTRIVIAL_CHECK is true, `check_item()` MUST error on any non-Safe values, and MUST NOT execute
	///Illegal Operations on them.
	const ITEM_SIZE: usize = 0;
	///If true, is_filtered is nontrivial. Note that is_filtered may still be called even if this is is
	///false.
	///
	///Specifically is_filtered is called if:
	/// * ITEM_SIZE is 0.
	/// * IS_FILTERED is true.
	/// * NONTRIVIAL_CHECK is true, and doing checked read.
	const IS_FILTERED: bool = false;
	///If true, check_item is nontrivial. Note that check_item may still be called even if this is false.
	///
	///Specifically check_item is called during checked read if:
	/// * ITEM_SIZE is 0.
	/// * IS_FILTERED or NONTRIVIAL_CHECK is true.
	const NONTRIVIAL_CHECK: bool = false;
	///Self with static lifetime.
	type WithStaticLifetime: IterableItemE+'static;
	///Lifetime-laundered version of item.
	type LaunderedItem: PrintableItem+Debug+Copy+'static;
	///Read error type.
	type ReadError: AlertForError+Copy+Display+Debug+'static;
	///Type kind.
	type Kind: Copy+Sized;
	///Check item is good.
	///
	///If overridden, set NONTRIVIAL_CHECK = true.
	fn check_item(&self) ->
		Result<(), <<Self as IterableItemE>::WithStaticLifetime as IterableItemE>::ReadError>
	{
		Ok(())		//Only some types have nontrivial checks.
	}
	///Check item is enabled. On error, returns the item kind.
	fn check_enabled<F:Fn(<Self as IterableItemE>::Kind) -> bool>(&self, _enabled: F) ->
		Result<(), <<Self as IterableItemE>::WithStaticLifetime as IterableItemE>::LaunderedItem>
	{
		Ok(())		//Only some types have nontrivial checks.
	}
}

///Read an item.
pub(crate) unsafe trait IterableItem2<'a>: IterableItem<'a>+IterableItemE
{
	///Read an item.
	fn read_item(data: &mut Message<'a>) ->
		Result<Self, <<Self as IterableItemE>::WithStaticLifetime as IterableItemE>::ReadError>;
}

///Generic iterator.
///
///This type is notable for implementing the `Iterator<Item=I>` trait, where `I` can be any type implementing
///`IterableItem`.
///
///The `size_hint()` is always both accurate and precise. `Using size_hint().0` is preferred over `clone().count()`,
///because the first is faster due to not having to copy the structure.
///
///Note there are some list types that have items that do not implement `IterableItem` (probably because the
///items involve crossreferencing two lists in TLS wire format), like `PreSharedKeyIter` (item `PreSharedKey`
///and `KeyShareIter` (item `KeyShare`).
#[derive(Clone)]
pub struct GenericIterator<'a,I:IterableItem<'a>>
{
	data: Message<'a>,
	left: usize,
	filtering: bool,
	phantom: PhantomData<I>
}

impl<'a,I:IterableItem<'a>> Iterator for GenericIterator<'a,I>
{
	type Item = I;
	fn next(&mut self) -> Option<I>
	{
		loop{
			//At end of iteration?
			if self.data.is_empty() { return None; }
			let item = unsafe{I::read_item_unchecked(&mut self.data)};
			if self.filtering && I::is_filtered(&item) { continue; }
			self.left -= 1;
			break Some(item)
		}
	}
	fn size_hint(&self) -> (usize, Option<usize>) { (self.left, Some(self.left)) }
	fn count(self) -> usize { self.left }
}

impl<'a,I:IterableItem<'a>> Debug for GenericIterator<'a,I>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		unsafe {
			write!(f, "IteratorOf<{lclass}>{{", lclass=I::listclass())?;
			let mut cont = false;
			let mut data = self.data.clone();
			while data.more() {
				let item = I::read_item_unchecked(&mut data);
				if self.filtering && I::is_filtered(&item) { continue; }
				if replace(&mut cont, true) { f.write_str(", ")?; }
				I::print(&item, f)?;
			}
			f.write_str("}")?;
			Ok(())
		}
	}
}

///Kind of quantifier.
#[derive(Copy,Clone)]
pub enum Quantifier
{
	///Any (Exists)
	Any,
	///All (Forall)
	All,
	///None (!Exists)
	None,
	///NotAll (!ForAll)
	NotAll,
}

impl Quantifier
{
	fn __flags(self) -> (bool, bool)
	{
		match self {
			Quantifier::Any => (true, true),
			Quantifier::All => (false, false),
			Quantifier::None => (true, false),
			Quantifier::NotAll => (false, true),
		}
	}
}

///Condition Polarity.
#[derive(Copy,Clone)]
pub enum Polarity
{
	///Satisfying.
	Satisfying,
	///Failing.
	Failing,
}

impl Polarity
{
	fn __flags(self) -> bool
	{
		match self {
			Polarity::Satisfying => true,
			Polarity::Failing => false,
		}
	}
}

///Optimization goal.
#[derive(Copy,Clone)]
pub enum Optimization
{
	///Minimize cost.
	Minimize,
	///Maximize profit.
	Maximize,
}

impl Optimization
{
	fn __flags(self) -> Ordering
	{
		match self {
			Optimization::Minimize => Ordering::Less,
			Optimization::Maximize => Ordering::Greater,
		}
	}
}

///Tiebreak
#[derive(Copy,Clone)]
pub enum Tiebreak
{
	///Tiebreak to first.
	First,
	///Tiebreak to last.
	Last,
}

impl Tiebreak
{
	fn __flags(self) -> bool
	{
		match self {
			Tiebreak::First => false,
			Tiebreak::Last => true,
		}
	}
}

///Into Generic iterator.
///
///This can be used for list of any type implementing `IterableItem`. It is not an iterator itself, but can be
///turned into one, including via automated conversions using `IntoIterator` (which corresponds to the `.iter()`
///method.
///
///Unlike the `GenericIterator` (as iterators should not be Copy), this type is Copy.
///
///Note there are some list types that have items that do not implement `IterableItem` (probably because the
///items involve crossreferencing two lists in TLS wire format), like `IntoPreSharedKeyIter` (item `PreSharedKey`
///and `IntoKeyShareIter` (item `KeyShare`).
#[derive(Copy,Clone)]
pub struct IntoGenericIterator<'a,I:IterableItem<'a>>
{
	data: IntoMessage<'a>,
	left_all: usize,
	left_filter: usize,
	phantom: PhantomData<I>
}

impl<'a,I:IterableItem<'a>> IntoGenericIterator<'a,I>
{
	///Iterate over values, filtering things like GREASE and SCSV.
	///
	///The return type implements `Iterator<Type=I>`.
	pub fn iter(&self) -> GenericIterator<'a,I>
	{
		GenericIterator {
			data: self.data.into_message(),
			filtering: true,
			left: self.left_filter,
			phantom: self.phantom,
		}
	}
	///Iterate over all values, with no filtering.
	///
	///The return type implements `Iterator<Type=I>`.
	pub fn iter_all(&self) -> GenericIterator<'a,I>
	{
		GenericIterator {
			data: self.data.into_message(),
			filtering: false,
			left: self.left_all,
			phantom: self.phantom,
		}
	}
	///Apply quantifier.
	///
	///This returns value of quantifier for list. If the parameter `all` is true, no filtering is performed
	///on the items passed to the predicate `f`. Otherwise things like GREASE and SCSV values are filtered out.
	///Predicate evaluation is short-circuited: As soon as return value is determined, predicate evaluation
	///stops (e.g., any satisfied predicate for `Quantifier::None`).
	///
	///The possible values of `q` are:
	///
	/// * `Quantifier::None`: Return true iff all of the elements fail the predicate (none satisfies it).
	///Always true on empty list.
	/// * `Quantifier::Any`: Return true iff any of the elements satisfies the predicate.
	///Always false on empty list.
	/// * `Quantifier::All`: Return true iff all of the elements satisfy the predicate.
	///Always true on empty list.
	/// * `Quantifier::NotAll`: Return true iff any of the elements fails the predicate (not all satisfy it).
	///Always false on empty list.
	pub fn quantifier(&self, all: bool, q: Quantifier, f: impl Fn(I) -> bool) -> bool
	{
		let (p, r) = q.__flags();
		self.__for_each(all, !r, |item|if f(item) == p { Some(r) } else { None })
	}
	///Find first element satisfying/failing predicate.
	///
	///If `all` is true, no filterering is performed on the list. Otherwise things like GREASE and SCSV
	///values are filtered out before predicates are evaluated.
	///
	///If `p` is `Polarity::Satisfying`, then the first element that satisfies the predicate `f` is returned.
	///If none satsify it, returns `None`. If `p` is `Polarity::Failing`, then the first element that fails
	///the predicate is retunred. If all satisfy it, returns `None`.
	///
	///The evaluation is short-circuited. As soon as the desired element has been found, predicate evaluation
	///stops.
	pub fn find(&self, all: bool, p: Polarity, f: impl Fn(I) -> bool) -> Option<I>
	{
		let p = p.__flags();
		self.__for_each(all, None, |item|if f(item) == p { Some(Some(item)) } else { None })
	}
	///Count all elements.
	///
	///If `all` is true, returns total number of elements in list. Otherwise returns number of elements in the
	///list that are not things like GREASE or SCSV values.
	pub fn count_all(&self, all: bool) -> usize
	{
		if all { self.left_all } else { self.left_filter }
	}
	///Count elements satisfying/failing predicate.
	///
	///If `all` is true, no filterering is performed on the list. Otherwise things like GREASE and SCSV
	///values are filtered out before predicates are evaluated.
	///
	///If `p` is `Polarity::Satisfying`, then the number of elements that satisfy the predicate `f` is
	///returned. If `p` is `Polarity::Failing`, then the number of elements that fail the predicate is
	///returned.
	///
	///Unlike most methods of this type, this is *NOT* short-circuiting: It always evaluates all predicates
	///that have not been filtered out.
	pub fn count(&self, all: bool, p: Polarity, f: impl Fn(I) -> bool) -> usize
	{
		let p = p.__flags();
		let mut ret = 0usize;
		self.__for_each(all, (), |item|{if f(item) == p { ret += 1; } None});
		ret
	}
	///Optimize given criteria.
	///
	///If `all` is true, no filterering is performed on the list. Otherwise things like GREASE and SCSV
	///values are filtered out before criteria are evaluated.
	///
	///The criteria `f` returns either the cost of choice (if `optimization` is `Optimization::Minimize`) or
	///profit of choice (if `optimization` is `Optimization::Maximize`). If criteria returns `None`, the
	///item is assumed to be not acceptable. The cost/profit may be any type that implements `Ord`.
	///
	///The parameter `tie` controls how to consider ties. If `tie` is `Tiebreak::First`, the first element
	///in the list is taken in case of tie. If `tie` is `Tiebreak::Last`, then the last item is taken in
	///case of a tie.
	///
	///Returns the optimal element, or `None` if there are no acceptable choices.
	///
	///Unlike most methods of this type, this is *NOT* short-circuiting: It always evaluates all criteria
	///that have not been filtered out.
	pub fn optimize<B:Ord>(&self, all: bool, optimization: Optimization, tie: Tiebreak,
		f: impl Fn(I) -> Option<B>) -> Option<I>
	{
		self.__optimize_map(all, optimization.__flags(), tie.__flags(), |item|f(item).map(|s|(s,item)))
	}
	///Optimize given predicate and map.
	///
	///This is like `optimize()`, but instead of just returning cost/profit, the criteria also returns a mapped
	///element. And instead of returning the original item, this returns the mapped element corresponding to
	///the optimal choice.
	pub fn optimize_map<B:Ord,J>(&self, all: bool, optimization: Optimization, tie: Tiebreak,
		f: impl Fn(I) -> Option<(B,J)>) -> Option<J>
	{
		self.__optimize_map(all, optimization.__flags(), tie.__flags(), f)
	}
}

impl<'a,I:IterableItem<'a>> IntoGenericIterator<'a,I>
{
	fn __for_each<J>(&self, all: bool, dflt: J, mut f: impl FnMut(I) -> Option<J>) -> J
	{
		let mut data = self.data.into_message();
		while data.more() {
			let item = unsafe{I::read_item_unchecked(&mut data)};
			if !all && I::is_filtered(&item) { continue; }
			if let Some(j) = f(item) { return j; }
		}
		dflt
	}
	fn __optimize_map<B:Ord,J>(&self, all: bool, p: Ordering, e: bool, f: impl Fn(I) -> Option<(B,J)>) ->
		Option<J>
	{
		let mut current: Option<(B,J)> = None;
		self.__for_each(all, (), |item|{
			if let Some((new_metric, new_item)) = f(item) {
				//Ok, new item is valid.
				let better = if let Some((old_metric,_)) = current.as_ref() {
					match new_metric.cmp(old_metric) {
						Ordering::Equal => e,
						x => x == p
					}
				} else {
					true	//Old does not even exist.
				};
				if better { current = Some((new_metric, new_item)); }
			}
			None
		});
		current.map(|(_,i)|i)
	}
	pub(crate) fn nontrivial(&self) -> bool { self.left_all > 0 }
	///Return the raw TLS data backing the list.
	pub fn as_raw_slice(&self) -> &'a [u8] { self.data.as_inner() }
	pub(crate) fn split_first(&mut self) -> Option<I>
	{
		//This is special, as it needs to edit IntoMessage, which is normally immutable.
		let mut message = self.data.into_message();
		if message.is_empty() { return None; }
		//This is known to be valid, since this is IntoIterator.
		let item = unsafe{I::read_item_unchecked(&mut message)};
		//Store back the pre-iterator and Sutract one item.
		self.data = IntoMessage::from_message(message);
		self.left_all -= 1;
		if !I::is_filtered(&item) { self.left_filter -= 1; }
		Some(item)
	}
/*
	pub(crate) unsafe fn from_inner_unchecked<'a>(mut data: &'a [u8]) ->
		IntoGenericIterator<'a,I:IterableItem<'a>>
	{
	}
*/
}

impl<'a,I:IterableItem<'a>> IntoIterator for IntoGenericIterator<'a,I>
{
	type Item = I;
	type IntoIter = GenericIterator<'a,I>;
	fn into_iter(self) -> GenericIterator<'a,I> { self.iter() }
}

impl<'a,I:IterableItem<'a>> Default for IntoGenericIterator<'a,I>
{
	fn default() -> Self
	{
		let (data, items) = I::special_default();
		IntoGenericIterator {
			data: IntoMessage(data),
			left_all: items,
			left_filter: items,
			phantom: PhantomData,
		}
	}
}

impl<'a,I:IterableItem<'a>> Debug for IntoGenericIterator<'a,I>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		unsafe {
			write!(f, "ListOf<{lclass}>{{", lclass=I::listclass())?;
			let mut cont = false;
			let mut data = self.data.into_message();
			while data.more() {
				let item = I::read_item_unchecked(&mut data);
				if replace(&mut cont, true) { f.write_str(", ")?; }
				I::print(&item, f)?;
			}
			f.write_str("}")?;
			Ok(())
		}
	}
}

//data.len() MUST NOT be smaller than item_size.
unsafe fn __read_one_item<'a,I:IterableItem2<'a>+'a>(data: &mut Message<'a>, item_size: usize) ->
	Result<I, ItemError<<I as IterableItemE>::WithStaticLifetime>>
{
	let ret = if item_size == 0 {
		I::read_item(data).map_err(ItemError::Single2)?
	} else {
		let r = unsafe{I::read_item_unchecked(data)};
		//This should optimize if trivial, so do not check NONTRIVIAL_CHECK.
		IterableItemE::check_item(&r).map_err(ItemError::Single2)?;
		r
	};
	Ok(ret)
}

fn __read_list<'a,I:IterableItem2<'a>+'a>(data: &'a [u8]) ->
	Result<IntoGenericIterator<'a,I>, VectorError<<I as IterableItemE>::WithStaticLifetime>>
{
	let mut total = 0;
	let mut filtered = 0;
	let item_size = <I as IterableItemE>::ITEM_SIZE;
	let need_loop = <I as IterableItemE>::IS_FILTERED || <I as IterableItemE>::NONTRIVIAL_CHECK;
	let data = IntoMessage(data);
	let mut data2 = data.into_message();
	if item_size > 0 {
		//Constant-length fast case.
		//If size_hint does not divide data.len(), data length is invalid.
		fail_if!(data2.length() % item_size != 0,
			VectorError::TopList(SliceReadError::BadLength(data2.length())));
		//Total number of items is length of data divided by size of elemnent.
		total = data2.length() / item_size;
		//Do not care about precise values of IS_FILTERED and NONTRIVIAL_CHECK inside this loop.
		//This is because optimizer should optimize the dummy calls to nothing.
		if need_loop {
			//Count number of non-filtered elements.
			for i in 0..total {
				//There is at least one element.
				let item = unsafe{I::read_item_unchecked(&mut data2)};
				IterableItemE::check_item(&item).map_err(|e|{
					VectorError::Element(i, e)
				})?;
				if !I::is_filtered(&item) { filtered += 1; }
			}
		} else {
			//If no filtering, filtered item count is total item count.
			filtered = total;
		}
	} else {
		//Variable-length case.
		//Do not bother about IS_FILTERED and NONTRIVIAL_CHECK here, since loop is needed anyway,
		//and dummy calls should optimize.
		while data2.more() {
			let item = I::read_item(&mut data2).map_err(|e|{
				VectorError::Element(total, e)
			})?;
			total += 1;
			if I::is_filtered(&item) { continue; }
			filtered += 1;
		}
	}
	Ok(IntoGenericIterator {
		data: data,
		left_all: total,
		left_filter: filtered,
		phantom: PhantomData,
	})
}

unsafe fn __read_list_unchecked<'a,I:IterableItem2<'a>+'a>(data: &'a [u8]) -> IntoGenericIterator<'a,I>
{
	let mut total = 0;
	let mut filtered = 0;
	let item_size = <I as IterableItemE>::ITEM_SIZE;
	let is_filtered = <I as IterableItemE>::IS_FILTERED;
	let data = IntoMessage(data);
	let mut data2 = data.into_message();
	if item_size > 0 {
		//Constant-length fast case.
		//Total number of items is length of data divided by size of elemnent. This division
		//is assumed to be even.
		total = data2.length() / item_size;
		if is_filtered {
			//Count number of non-filtered elements.
			for _ in 0..total {
				//There is at least one element.
				let item = unsafe{I::read_item_unchecked(&mut data2)};
				if !I::is_filtered(&item) { filtered += 1; }
			}
		} else {
			//If no filtering, filtered item count is total item count.
			filtered = total;
		}
	} else {
		//Variable-length case. Do not bother with is_filtered, because dummy calls should be
		//optimzied out.
		while data2.more() {
			let item = unsafe{I::read_item_unchecked(&mut data2)};
			total += 1;
			if I::is_filtered(&item) { continue; }
			filtered += 1;
		}
	}
	IntoGenericIterator {
		data: data,
		left_all: total,
		left_filter: filtered,
		phantom: PhantomData,
	}
}

fn __read_xread<C:XRead>(src: &mut &[u8]) -> Result<C, PrimitiveReadError>
{
	<C as XRead>::read(src).ok_or_else(||{
		let len = <C as XRead>::size();
		PrimitiveReadError::Truncated(src.len(), len)
	})
}

///Into message.
#[derive(Copy,Clone)]
struct IntoMessage<'a>(&'a [u8]);

impl<'a> IntoMessage<'a>
{
	fn into_message(self) -> Message<'a> { Message(self.0) }
	fn from_message(msg: Message<'a>) -> IntoMessage<'a> { IntoMessage(msg.0) }
	fn as_inner(self) -> &'a [u8] { self.0 }
}

///Message.
#[derive(Clone)]
pub struct Message<'a>(&'a [u8]);

impl<'a> Message<'a>
{
	pub(crate) fn new(data: &'a [u8]) -> Message<'a> { Message(data) }
	pub(crate) fn list_of<I:IterableItem2<'a>+'a>(&mut self, length: VectorLength) ->
		Result<IntoGenericIterator<'a,I>, VectorError<<I as IterableItemE>::WithStaticLifetime>>
	{
		let non_empty = !length.acceptable_length(0);
		let data = self.slice(length).map_err(VectorError::TopList)?;
		let ret = __read_list(data)?;
		//This will blow up for ServerName, but that one is read via read_server_name_list(), not
		//this function.
		fail_if!(non_empty && ret.left_filter == 0, VectorError::FilteredEmpty);
		Ok(ret)
	}
	unsafe fn list_of_unchecked<I:IterableItem2<'a>+'a>(&mut self, length: VectorLength) ->
		IntoGenericIterator<'a,I>
	{
		let data = unsafe{self.slice_unchecked(length)};
		unsafe{__read_list_unchecked(data)}
	}
	pub(crate) fn list_one<I:IterableItem2<'a>+'a,F>(&mut self, length: VectorLength, enable: F) ->
		Result<I, ItemError<<I as IterableItemE>::WithStaticLifetime>> where
		F: Fn(<I as IterableItemE>::Kind) -> bool
	{
		//Assume item is not filtered.
		let item_size = <I as IterableItemE>::ITEM_SIZE;
		let mut data = self.message(length).map_err(ItemError::Containing)?;
		fail_if!(data.length() == 0 || (item_size != 0 && data.length() != item_size),
			ItemError::Containing(SliceReadError::BadLength(data.length())));
		let item = unsafe{__read_one_item(&mut data, item_size)?};
		data.assert_end(ItemError::MoreThanOneItem)?;
		I::check_enabled(&item, enable).map_err(ItemError::BogusElement)?;
		Ok(item)
	}
	pub(crate) fn list_at_most_one<I:IterableItem2<'a>+'a>(&mut self, length: VectorLength) ->
		Result<Option<I>, ItemError<<I as IterableItemE>::WithStaticLifetime>>
	{
		//Assume item is not filtered.
		let mut data = self.message(length).map_err(ItemError::Containing)?;
		if data.more() {
			let item_size = <I as IterableItemE>::ITEM_SIZE;
			fail_if!(data.length() < item_size,
				ItemError::Single(PrimitiveReadError::Truncated(data.length(), item_size)));
			let item = unsafe{__read_one_item(&mut data, item_size)?};
			data.assert_end(ItemError::MoreThanOneItem)?;
			Ok(Some(item))
		} else {
			Ok(None)
		}
	}
	pub(crate) fn server_name_list(&mut self) ->
		Result<(IntoGenericIterator<'a,ServerName<'a>>, Option<&'a str>), VectorError<ServerName<'static>>>
	{
		server_name::read_server_name_list(self)
	}
	pub(crate) fn item<I:IterableItem2<'a>+'a, F>(&mut self, enable: F) ->
		Result<I, ItemError<<I as IterableItemE>::WithStaticLifetime>> where
		F:Fn(<I as IterableItemE>::Kind) -> bool
	{
		let item_size = <I as IterableItemE>::ITEM_SIZE;
		fail_if!(self.length() < item_size,
			ItemError::Single(PrimitiveReadError::Truncated(self.length(), item_size)));
		let item = unsafe{__read_one_item(self, item_size)?};
		I::check_enabled(&item, enable).map_err(ItemError::BogusElement)?;
		Ok(item)
	}
	pub(crate) fn slice(&mut self, length: VectorLength) -> Result<&'a [u8], SliceReadError>
	{
		use btls_aux_tls_iana::helper::U24;
		let mut src2 = self.0.clone();
		let len = match length.length_bytes() {
			0 => Ok(length.constant_length()),
			1 => __read_xread::<u8>(&mut src2).map(XRead::__cast_usize),
			2 => __read_xread::<u16>(&mut src2).map(XRead::__cast_usize),
			3 => __read_xread::<U24>(&mut src2).map(XRead::__cast_usize),
			4 => __read_xread::<u32>(&mut src2).map(XRead::__cast_usize),
			//This case can not actually happen.
			x => fail!(SliceReadError::InvalidSizeUpperBound(x))
		};
		let len = len.map_err(|e|match e {
			PrimitiveReadError::Truncated(a, b) => SliceReadError::LengthTruncated(a, b),
		})?;
		fail_if!(!length.acceptable_length(len), SliceReadError::BadLength(len));
		fail_if!(src2.len() < len, SliceReadError::ValueTruncated(src2.len(), len));
		let (head, tail) = src2.split_at(len);
		self.0 = tail;
		Ok(head)
	}
	unsafe fn slice_unchecked(&mut self, length: VectorLength) -> &'a [u8]
	{
		use btls_aux_tls_iana::helper::U24;
		let len = match length.length_bytes() {
			0 => length.constant_length(),
			1 => unsafe{<u8 as XRead>::read_unchecked(&mut self.0)}.__cast_usize(),
			2 => unsafe{<u16 as XRead>::read_unchecked(&mut self.0)}.__cast_usize(),
			3 => unsafe{<U24 as XRead>::read_unchecked(&mut self.0)}.__cast_usize(),
			4 => unsafe{<u32 as XRead>::read_unchecked(&mut self.0)}.__cast_usize(),
			_ => unsafe{core::hint::unreachable_unchecked()}
		};
		let x = unsafe{self.0.get_unchecked(..len)};
		self.0 = unsafe{self.0.get_unchecked(len..)};
		x
	}
	pub(crate) fn message(&mut self, length: VectorLength) -> Result<Message<'a>, SliceReadError>
	{
		self.slice(length).map(Message)
	}
	unsafe fn message_unchecked(&mut self, length: VectorLength) -> Message<'a>
	{
		Message(unsafe{self.slice_unchecked(length)})
	}
	fn message_into(&mut self, length: VectorLength) -> Result<IntoMessage<'a>, SliceReadError>
	{
		self.slice(length).map(IntoMessage)
	}
	pub(crate) fn array<const N: usize>(&mut self) -> Result<[u8;N], PrimitiveReadError>
	{
		fail_if!(self.0.len() < N, PrimitiveReadError::Truncated(self.0.len(), N));
		let (head, tail) = self.0.split_at(N);
		let head = unsafe{head.as_ptr().cast::<[u8;N]>().read()};
		self.0 = tail;
		Ok(head)
	}
	unsafe fn array_unchecked<const N: usize>(&mut self) -> [u8;N]
	{
		let head = unsafe{self.0.as_ptr().cast::<[u8;N]>().read()};
		self.0 = unsafe{self.0.get_unchecked(N..)};
		head
	}
	pub(crate) fn cp<C:MakeCodepoint>(&mut self) -> Result<C, PrimitiveReadError>
	{
		__read_xread::<<C as MakeCodepoint>::Inner2>(&mut self.0).map(<C as MakeCodepoint>::__new)
	}
	unsafe fn cp_unchecked<C:MakeCodepoint>(&mut self) -> C
	{
		let inner = unsafe{<<C as MakeCodepoint>::Inner2 as XRead>::read_unchecked(&mut self.0)};
		<C as MakeCodepoint>::__new(inner)
	}
	pub(crate) fn all(&mut self) -> &'a [u8]
	{
		replace(&mut self.0, b"")
	}
	pub(crate) fn assert_end<E>(&self, err: E) -> Result<(), E>
	{
		if self.0.len() == 0 { Ok(()) } else { Err(err) }
	}
	pub(crate) fn more(&self) -> bool { self.0.len() > 0 }
	pub(crate) fn is_empty(&self) -> bool { self.0.len() == 0 }
	pub(crate) fn length(&self) -> usize { self.0.len() }
	pub(crate) fn as_inner(&self) -> &'a [u8] { self.0 }
}

/*******************************************************************************************************************/
/*******************************************************************************************************************/

