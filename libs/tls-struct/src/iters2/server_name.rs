use super::IntoGenericIterator;
use super::IterableItem;
use super::IterableItem2;
use super::IterableItemE;
use super::Message;
use super::PrimitiveReadError;
use super::VectorError;
use crate::LEN_SERVER_NAME_LIST;
use crate::LEN_SERVER_NAME_NAME;
use crate::SliceReadError;
use btls_aux_fail::fail_if;
use btls_aux_memory::Hexdump;
use btls_aux_tls_iana::ServerNameType;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter as Formatter;
use core::marker::PhantomData;


#[derive(Copy,Clone,Debug)]
pub(crate) enum ServerNameError
{
	NameType(PrimitiveReadError),
	Name(SliceReadError),
	EmptyHostName,
	InvalidHostName,
}

impl super::AlertForError for ServerNameError
{
	fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::ServerNameError::*;
		match self {
			&NameType(ref e) => e.alert(),
			&Name(ref e) => e.alert(),
			&EmptyHostName => btls_aux_tls_iana::Alert::ILLEGAL_PARAMETER,
			&InvalidHostName => btls_aux_tls_iana::Alert::ILLEGAL_PARAMETER,
		}
	}
}

impl Display for ServerNameError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::ServerNameError::*;
		match self {
			&NameType(ref err) => write!(f, "name_type: {err}"),
			&Name(ref err) => write!(f, "name: {err}"),
			&EmptyHostName => write!(f, "Empty hostname"),
			&InvalidHostName => write!(f, "Illegal hostname"),
		}
	}
}


pub(crate) fn read_server_name_list<'a>(data: &mut Message<'a>) ->
	Result<(IntoGenericIterator<'a,ServerName<'a>>, Option<&'a str>), VectorError<ServerName<'static>>>
{
	//Optimize with knowledge that fastread_props() is (0, true) and check_item is trivial.
	let data = data.message_into(LEN_SERVER_NAME_LIST).map_err(VectorError::TopList)?;
	let mut total = 0;
	let mut filtered = 0;
	let mut host_name = None;
	let mut data2 = data.into_message();
	while data2.more() {
		let item = ServerName::read_item(&mut data2).map_err(|e|{
			VectorError::Element(total, e)
		})?;
		total += 1;
		if item.name_type == ServerNameType::HOST_NAME {
			//Checked already this is valid UTF-8.
			host_name = Some(unsafe{core::str::from_utf8_unchecked(item.name)});
			continue;
		}
		filtered += 1;
	}
	Ok((IntoGenericIterator {
		data: data,
		left_all: total,
		left_filter: filtered,
		phantom: PhantomData
	}, host_name))
}


///Server Name.
///
///This corresponds to `ServerName` TLS structure.
///
///Note that one probably does not want to use this: If one wants the server host name, that is reported
///separatedly. See `ClientHello`.
///
///This type implements `IterableItem`, so it can be used with `GenericIterator`.
#[derive(Copy,Clone)]
pub struct ServerName<'a>
{
	name_type: ServerNameType,
	name: &'a [u8],
}

impl<'a> ServerName<'a>
{
	///Get the type of the server name.
	///
	///This corresponds to the `name_type` field of the `ServerName` TLS structure.
	pub fn get_name_type(&self) -> ServerNameType { self.name_type }
	///Get the raw name.
	///
	///However, the name is not entierely raw: Two octet length field has been stripped from the name. This
	///is to make `server_name` extension parseable in presence of unknown name types.
	///
	///This corresponds to the `name` field of the `ServerName` TLS structure.
	pub fn get_raw_name(&self) -> &'a [u8] { self.name }
}

unsafe impl<'a> IterableItem<'a> for ServerName<'a>
{
	fn listclass() -> &'static str { "server_name" }
	unsafe fn read_item_unchecked(data: &mut Message<'a>) -> Self
	{
		// opaque HostName<1..2^16-1>;
		//
		// struct {
		//	 NameType name_type;
		//		 select (name_type) {
		//			 case host_name: HostName;
		// 	} name;
		// } ServerName;
		//
		// - RFC6066 Section 3
		let name_type: ServerNameType = unsafe{data.cp_unchecked()};
		let name = unsafe{data.slice_unchecked(LEN_SERVER_NAME_NAME)};
		ServerName {
			name_type,
			name,
		}
	}
	fn is_filtered(&self) -> bool { self.name_type == ServerNameType::HOST_NAME }
	fn print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		//TODO: Better printing.
		write!(f, "{ntype}:<{name}>", ntype=self.name_type, name=Hexdump(self.name))
	}
}

impl<'a> IterableItemE for ServerName<'a>
{
	const IS_FILTERED: bool = true;
	type WithStaticLifetime = ServerName<'static>;
	type ReadError = ServerNameError;
	//Nothing uses these two.
	type Kind = crate::Void;
	type LaunderedItem = crate::Void;
}

unsafe impl<'a> IterableItem2<'a> for ServerName<'a>
{
	fn read_item(data: &mut Message<'a>) -> Result<Self, ServerNameError>
	{
		use self::ServerNameError as SE;
		// opaque HostName<1..2^16-1>;
		//
		// struct {
		//	 NameType name_type;
		//		 select (name_type) {
		//			 case host_name: HostName;
		// 	} name;
		// } ServerName;
		//
		// - RFC6066 Section 3
		let name_type: ServerNameType = data.cp().map_err(SE::NameType)?;
		let name = data.slice(LEN_SERVER_NAME_NAME).map_err(SE::Name)?;
		if name_type == ServerNameType::HOST_NAME {
			let mut name2 = name;
			//If name ends with '.', strip it.
			let lp = name2.len().wrapping_sub(1);
			if lp < name2.len() && name2[lp] == b'.' { name2 = &name2[..lp]; }
			//Check for name validity.
			fail_if!(name2.len() == 0, SE::EmptyHostName);
			let mut since_last_dot = 0;
			for &b in name2.iter() { match b {
				48..=57|65..=90|97..=122|b'-'|b'_' => since_last_dot += 1,
				b'.' if since_last_dot > 0 => since_last_dot &= !63,
				_ => since_last_dot = 64
			}}
			if since_last_dot == 0 { since_last_dot = 64; }
			fail_if!(since_last_dot >> 6 != 0, self::ServerNameError::InvalidHostName);
		}
		Ok(ServerName {
			name_type,
			name,
		})
	}
}
