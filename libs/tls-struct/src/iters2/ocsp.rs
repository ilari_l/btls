use super::IntoGenericIterator;
use super::IterableItem;
use super::Message;
use super::OcspResponder;
use super::OcspResponse;
use crate::LEN_OCSP_EXTENSIONS;
use crate::LEN_OCSP_RESPONDER_ID_LIST;
use crate::LEN_OCSP_RESPONSE_LIST;
use crate::RemoteFeatureSet;
use crate::SliceReadError;
use btls_aux_fail::fail_if;
use btls_aux_tls_iana::CertificateStatusType;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;


/*******************************************************************************************************************/
#[derive(Copy,Clone,Debug)]
pub(crate) enum OcspStatusRequestError
{
	ResponderIdList(super::VectorError<crate::iters2::OcspResponder<'static>>),
	RequestExtensions(SliceReadError),
}

impl super::AlertForError for OcspStatusRequestError
{
	fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::OcspStatusRequestError::*;
		match self {
			&ResponderIdList(ref e) => e.alert(),
			&RequestExtensions(ref e) => e.alert(),
		}
	}
}

impl Display for OcspStatusRequestError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::OcspStatusRequestError::*;
		match self {
			&ResponderIdList(ref err) => write!(f, "responder_id_list: {err}"),
			&RequestExtensions(ref err) => write!(f, "request_extensions: {err}"),
		}
	}
}


///OCSP status request.
///
///This object represents OCSP status request. It contains list of acceptable responder IDs and possible request
///extensions. The corresponding TLS structure is `OCSPStatusRequest`.
///
///This type implements `IterableItem`, so it can be used with `GenericIterator`.
#[derive(Copy,Clone)]
pub struct OcspStatusRequest<'a>
{
	pub(super) responders: IntoGenericIterator<'a, OcspResponder<'a>>,
	pub(super) extensions: &'a [u8],
}

impl<'a> OcspStatusRequest<'a>
{
	///Is any responder acceptable?
	///
	///The field `responder_id_list` being empty signals that any responder for certificate issuer is
	///acceptable. This tests for that condition.
	pub fn accepts_any_responder(&self) -> bool { self.responders.count_all(false) == 0 }
	///Get list of X.509 names of acceptable responders.
	///
	///This corresponds to the `responder_id_list` field of the `OCSPStatusRequest` TLS structure.
	///If the list is empty, any responder for the certificate issuer is acceptable.
	pub fn get_responders(&self) -> IntoGenericIterator<'a, OcspResponder<'a>> { self.responders }
	///Get the extensions in OCSP status request, e.g. nonce.
	///
	///This corresponds to the `request_extensions` field of the `OCSPStatusRequest` TLS structure.
	pub fn get_extensions(&self) -> &'a [u8] { self.extensions }
	pub(crate) unsafe fn read_unchecked(data: &mut Message<'a>) -> OcspStatusRequest<'a>
	{
		// struct {
		//	 ResponderID responder_id_list<0..2^16-1>;
		//	 Extensions request_extensions;
		// } OCSPStatusRequest;
		//
		// opaque ResponderID<1..2^16-1>;
		// opaque Extensions<0..2^16-1>;
		//
		// - RFC6961 section 2.2
		let ril = unsafe{data.list_of_unchecked(LEN_OCSP_RESPONDER_ID_LIST)};
		let ext = unsafe{data.slice_unchecked(LEN_OCSP_EXTENSIONS)};
		OcspStatusRequest {
			responders: ril,
			extensions: ext,
		}
	}
	pub(crate) fn read(data: &mut Message<'a>) -> Result<OcspStatusRequest<'a>, OcspStatusRequestError>
	{
		use self::OcspStatusRequestError::*;
		// struct {
		//	 ResponderID responder_id_list<0..2^16-1>;
		//	 Extensions request_extensions;
		// } OCSPStatusRequest;
		//
		// opaque ResponderID<1..2^16-1>;
		// opaque Extensions<0..2^16-1>;
		//
		// - RFC6961 section 2.2
		let ril = data.list_of(LEN_OCSP_RESPONDER_ID_LIST).map_err(ResponderIdList)?;
		let ext = data.slice(LEN_OCSP_EXTENSIONS).map_err(RequestExtensions)?;
		Ok(OcspStatusRequest{
			responders: ril,
			extensions: ext,
		})
	}
}
/*******************************************************************************************************************/
#[derive(Copy,Clone,Debug)]
pub(crate) enum CertificateStatusError
{
	StatusType(super::ItemError<CertificateStatusType>),
	ResponseOcsp(super::ItemError<OcspResponse<'static>>),
	ResponseOcspMulti(super::VectorError<OcspResponse<'static>>),
	JunkAfterResponse,
	OcspMultiNotInTls13,
}

impl super::AlertForError for CertificateStatusError
{
	fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::CertificateStatusError::*;
		match self {
			&StatusType(ref e) => e.alert(),
			&ResponseOcsp(ref e) => e.alert(),
			&ResponseOcspMulti(ref e) => e.alert(),
			&JunkAfterResponse => btls_aux_tls_iana::Alert::DECODE_ERROR,
			&OcspMultiNotInTls13 => btls_aux_tls_iana::Alert::ILLEGAL_PARAMETER,
		}
	}
}

impl Display for CertificateStatusError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::CertificateStatusError::*;
		match self {
			&StatusType(ref err) => write!(f, "status_type: {err}"),
			&ResponseOcsp(ref err) => write!(f, "OCSP: {err}"),
			&ResponseOcspMulti(ref err) => write!(f, "OCSP-multi: {err}"),
			&JunkAfterResponse => write!(f, "Junk after response"),
			&OcspMultiNotInTls13 => write!(f, "Illegal OCSP-multi in TLS 1.3"),
		}
	}
}

///Certificate status response.
///
///This corresponds to `CertificateStatus` TLS structure.
#[derive(Copy,Clone,Debug)]
pub struct CertificateStatus<'a>
{
	status_type: CertificateStatusType,
	response: &'a [u8],
	special: SpecialCertificateStatus<'a>,
}

#[derive(Copy,Clone,Debug)]
enum SpecialCertificateStatus<'a>
{
	None,
	Ocsp(OcspResponse<'a>),
	OcspMulti(IntoGenericIterator<'a,OcspResponse<'a>>),
}


impl<'a> CertificateStatus<'a>
{
	///Get the type of certificate status.
	///
	///This corresponds to the `status_type` field in the `CertificateStatus` TLS structure.
	pub fn get_status_type(&self) -> CertificateStatusType { self.status_type }
	///Get the raw response payload.
	///
	///Note that this is not a valid OCSP response if `get_status_type()` gives `CertificateStatusType::OCSP`,
	///due to presence of headers. Use `as_ocsp()` to extract valid OCSP response from these objects.
	///
	///This corresponds to the `response` field in the `CertificateStatus` TLS structure.
	pub fn get_raw_response(&self) -> &'a [u8] { self.response }
	///Obtain the certificate status response as OCSP response if possible.
	///
	///This succeeds if and only if `status_type == CertificateStatusType::OCSP`.
	pub fn as_ocsp(&self) -> Option<OcspResponse<'a>>
	{
		if let SpecialCertificateStatus::Ocsp(r) = self.special { Some(r) } else { None }
	}
	///Obtain the certificate status response as OCSP multi response if possible.
	///
	///Each element in the returned list corresponds to OCSP response for a single certificate in chain, in
	///order certificates are in TLS `Certificate` message.
	///
	///This succeeds if and only if `status_type == CertificateStatusType::OCSP_MULTI`.
	pub fn as_ocsp_multi(&self) -> Option<IntoGenericIterator<'a, OcspResponse<'a>>>
	{
		if let SpecialCertificateStatus::OcspMulti(r) = self.special { Some(r) } else { None }
	}
	pub(crate) unsafe fn read_unchecked(data: &mut Message<'a>) -> CertificateStatus<'a>
	{
		// struct {
		//	 OCSPResponse ocsp_response_list<1..2^24-1>;
		// } OCSPResponseList;
		//
		// struct {
		//	 CertificateStatusType status_type;
		//	 select (status_type) {
		//	 	case ocsp: OCSPResponse;
		//	 	case ocsp_multi: OCSPResponseList;
		//	 } response;
		// } CertificateStatus;
		//
		// - RFC6961 section 2.2
		let status_type: CertificateStatusType = unsafe{data.cp_unchecked()};
		let response = data.as_inner();
		let special = match status_type {
			CertificateStatusType::OCSP =>
				SpecialCertificateStatus::Ocsp(unsafe{OcspResponse::read_item_unchecked(data)}),
			CertificateStatusType::OCSP_MULTI => {
				let t = unsafe{data.list_of_unchecked(LEN_OCSP_RESPONSE_LIST)};
				SpecialCertificateStatus::OcspMulti(t)
			},
			_ => {
				data.all();		//Consume the rest.
				SpecialCertificateStatus::None
			}
		};
		CertificateStatus{status_type, response, special}
	}
	pub(crate) fn read_item(data: &mut Message<'a>, enable: RemoteFeatureSet, tls13: bool) ->
		Result<CertificateStatus<'a>, CertificateStatusError>
	{
		use self::CertificateStatusError::*;
		// struct {
		//	 OCSPResponse ocsp_response_list<1..2^24-1>;
		// } OCSPResponseList;
		//
		// struct {
		//	 CertificateStatusType status_type;
		//	 select (status_type) {
		//	 	case ocsp: OCSPResponse;
		//	 	case ocsp_multi: OCSPResponseList;
		//	 } response;
		// } CertificateStatus;
		//
		// - RFC6961 section 2.2
		let status_type = data.item(&|t|{
			crate::ask_certificate_status_type(enable, t)
		}).map_err(StatusType)?;
		let response = data.as_inner();
		let special = match status_type {
			CertificateStatusType::OCSP => {
				let x = data.item(&|_|true).map_err(ResponseOcsp)?;
				data.assert_end(JunkAfterResponse)?;
				SpecialCertificateStatus::Ocsp(x)
			},
			CertificateStatusType::OCSP_MULTI => {
				fail_if!(tls13, OcspMultiNotInTls13);
				let x = data.list_of(LEN_OCSP_RESPONSE_LIST).map_err(ResponseOcspMulti)?;
				data.assert_end(JunkAfterResponse)?;
				SpecialCertificateStatus::OcspMulti(x)
			},
			_ => {
				data.all();		//Consume the rest.
				SpecialCertificateStatus::None
			}
		};
		Ok(CertificateStatus{status_type, response, special})
	}
}
