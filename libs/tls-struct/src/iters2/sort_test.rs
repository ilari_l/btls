#[cfg(target_arch="x86_64")]
mod implementation
{
	//This produces crazy amount of these warnings.
	#![allow(unsafe_op_in_unsafe_fn)]
	use core::arch::x86_64::__m128i as M128;
	use core::arch::x86_64 as intr;

/*******************************************************************************************************************/
	pub fn sort64(x: &mut [u16;64], n: usize)
	{ unsafe {
		match n {
			0..=1 => (),
			//__sort_1 does badly with two elements.
			2 => {
				let t = ((x[1] as u32).wrapping_sub(x[0] as u32) >> 16) as u16;
				let t = (x[0] ^ x[1]) & t;
				x[0] ^= t;
				x[1] ^= t;
			},
			3..=8 => __sort_1(x.as_mut_ptr() as *mut M128, n % 8),
			9..=16 => __sort_2(x.as_mut_ptr() as *mut M128, n % 8),
			17..=24 => __sort_3(x.as_mut_ptr() as *mut M128, n % 8),
			25..=32 => __sort_4(x.as_mut_ptr() as *mut M128, n % 8),
			33..=40 => __sort_5(x.as_mut_ptr() as *mut M128, n % 8),
			41..=48 => __sort_6(x.as_mut_ptr() as *mut M128, n % 8),
			49..=56 => __sort_7(x.as_mut_ptr() as *mut M128, n % 8),
			_ => __sort_8(x.as_mut_ptr() as *mut M128, n % 8),
		}
	}}

	pub fn sort8(x: &mut [u16;8], n: usize)
	{ unsafe {
		match n {
			0..=1 => (),
			//__sort_1 does badly with two elements.
			2 => {
				let t = ((x[1] as u32).wrapping_sub(x[0] as u32) >> 16) as u16;
				let t = (x[0] ^ x[1]) & t;
				x[0] ^= t;
				x[1] ^= t;
			},
			_ => __sort_1(x.as_mut_ptr() as *mut M128, n % 8),
		}
	}}

	macro_rules! __load_biased
	{
		($x:ident, $offset:expr, $($args:expr),*; $mask:expr) => {
			(__load_biased($x.add($offset), 0), __load_biased!($x, $($args),*; $mask))
		};
		($x:ident, $offset:expr; $mask:expr) => {
			__load_biased($x.add($offset), $mask)
		};
	}
	macro_rules! __store_unbiased
	{
		($x:ident, $offset:expr, $value:expr, $($args:expr),*) => {{
			__store_unbiased($x.add($offset), $value);
			__store_unbiased!($x, $($args),*)
		}};
		($x:ident, $offset:expr, $value:expr) => {
			__store_unbiased($x.add($offset), $value)
		};
	}
	macro_rules! __sort_block
	{
		($x:ident, $($args:ident),*) => {
			(__sort_block($x), __sort_block!($($args),*))
		};
		($x:ident) => {
			__sort_block($x)
		};
	}
	macro_rules! __partial_sort_block
	{
		($x:ident, $($args:ident),*) => {
			(__partial_sort_block($x), __partial_sort_block!($($args),*))
		};
		($x:ident) => {
			__partial_sort_block($x)
		};
	}
	macro_rules! __butterfly
	{
		($x:ident, $y:ident, $($args:ident),*) => {
			(__butterfly($x, $y), __butterfly!($($args),*))
		};
		($x:ident, $y:ident) => {
			__butterfly($x, $y)
		};
	}
	macro_rules! __linear
	{
		($x:ident, $y:ident, $($args:ident),*) => {
			(__linear($x, $y), __linear!($($args),*))
		};
		($x:ident, $y:ident) => {
			__linear($x, $y)
		};
	}

	unsafe fn __sort_1(x: *mut M128, mask: usize)
	{
		//1x8 element bitonic sorter.
		let a = __load_biased!(x, 0; mask);
		let a = __sort_block!(a);
		__store_unbiased!(x, 0, a);
	}

	unsafe fn __sort_2(x: *mut M128, mask: usize)
	{
		//2x8 element bitonic sorter.
		let (a, b) = __load_biased!(x, 0, 1; mask);
		let (a, b) = __sort_block!(a, b);
		let (a, b) = __butterfly!(a, b);
		let (a, b) = __partial_sort_block!(a, b);
		__store_unbiased!(x, 0, a, 1, b);
	}

	unsafe fn __sort_3(x: *mut M128, mask: usize)
	{
		//Derived from __sort_4 by deleting all mentions of d.
		let (a, (b, c)) = __load_biased!(x, 0, 1, 2; mask);
		let (a, (b, c)) = __sort_block!(a, b, c);
		let (a, b) = __butterfly!(a, b);
		let (a, (b, c)) = __partial_sort_block!(a, b, c);
		let (b, c) = __butterfly!(b, c);
		let (a, b) = __linear!(a, b);
		let (a, (b, c)) = __partial_sort_block!(a, b, c);
		__store_unbiased!(x, 0, a, 1, b, 2, c);
	}

	unsafe fn __sort_4(x: *mut M128, mask: usize)
	{
		//4x8 element bitonic sorter.
		let (a, (b, (c, d))) = __load_biased!(x, 0, 1, 2, 3; mask);
		let (a, (b, (c, d))) = __sort_block!(a, b, c, d);
		let ((a, b), (c, d)) = __butterfly!(a, b, c, d);
		let (a, (b, (c, d))) = __partial_sort_block!(a, b, c, d);
		let ((a, d), (b, c)) = __butterfly!(a, d, b, c);
		let ((a, b), (c, d)) = __linear!(a, b, c, d);
		let (a, (b, (c, d))) = __partial_sort_block!(a, b, c, d);
		__store_unbiased!(x, 0, a, 1, b, 2, c, 3, d);
	}

	unsafe fn __sort_5(x: *mut M128, mask: usize)
	{
		//Derived from __sort_6 by deleting all mentions of f.
		let (a, (b, (c, (d, e)))) = __load_biased!(x, 0, 1, 2, 3, 4; mask);
		let (a, (b, (c, (d, e)))) = __sort_block!(a, b, c, d, e);
		let ((a, b), (c, d)) = __butterfly!(a, b, c, d);
		let (a, (b, (c, (d, e)))) = __partial_sort_block!(a, b, c, d, e);
		let ((a, d), (b, c)) = __butterfly!(a, d, b, c);
		let ((a, b), (c, d)) = __linear!(a, b, c, d);
		let (a, (b, (c, (d, e)))) = __partial_sort_block!(a, b, c, d, e);
		let (d, e) = __butterfly!(d, e);
		let ((a, c), (b, d)) = __linear!(a, c, b, d);
		let ((a, b), (c, d)) = __linear!(a, b, c, d);
		let (a, (b, (c, (d, e)))) = __partial_sort_block!(a, b, c, d, e);
		__store_unbiased!(x, 0, a, 1, b, 2, c, 3, d, 4, e);
	}

	unsafe fn __sort_6(x: *mut M128, mask: usize)
	{
		//Derived from __sort_7 by deleting all mentions of g.
		let (a, (b, (c, (d, (e, f))))) = __load_biased!(x, 0, 1, 2, 3, 4, 5; mask);
		let (a, (b, (c, (d, (e, f))))) = __sort_block!(a, b, c, d, e, f);
		let ((a, b), ((c, d), (e, f))) = __butterfly!(a, b, c, d, e, f);
		let (a, (b, (c, (d, (e, f))))) = __partial_sort_block!(a, b, c, d, e, f);
		let ((a, d), (b, c)) = __butterfly!(a, d, b, c);
		let ((a, b), ((c, d), (e, f))) = __linear!(a, b, c, d, e, f);
		let (a, (b, (c, (d, (e, f))))) = __partial_sort_block!(a, b, c, d, e, f);
		let ((c, f), (d, e)) = __butterfly!(c, f, d, e);
		let ((a, c), (b, d)) = __linear!(a, c, b, d);
		let ((a, b), ((c, d), (e, f))) = __linear!(a, b, c, d, e, f);
		let (a, (b, (c, (d, (e, f))))) = __partial_sort_block!(a, b, c, d, e, f);
		__store_unbiased!(x, 0, a, 1, b, 2, c, 3, d, 4, e, 5, f);
	}

	unsafe fn __sort_7(x: *mut M128, mask: usize)
	{
		//Derived from __sort_8 by deleting all mentions of h.
		let (a, (b, (c, (d, (e, (f, g)))))) = __load_biased!(x, 0, 1, 2, 3, 4, 5, 6; mask);
		let (a, (b, (c, (d, (e, (f, g)))))) = __sort_block!(a, b, c, d, e, f, g);
		let ((a, b), ((c, d), (e, f))) = __butterfly!(a, b, c, d, e, f);
		let (a, (b, (c, (d, (e, (f, g)))))) = __partial_sort_block!(a, b, c, d, e, f, g);
		let ((a, d), ((b, c), (f, g))) = __butterfly!(a, d, b, c, f, g);
		let ((a, b), ((c, d), (e, f))) = __linear!(a, b, c, d, e, f);
		let (a, (b, (c, (d, (e, (f, g)))))) = __partial_sort_block!(a, b, c, d, e, f, g);
		let ((b, g), ((c, f), (d, e))) = __butterfly!(b, g, c, f, d, e);
		let ((a, c), ((b, d), (e, g))) = __linear!(a, c, b, d, e, g);
		let ((a, b), ((c, d), (e, f))) = __linear!(a, b, c, d, e, f);
		let (a, (b, (c, (d, (e, (f, g)))))) = __partial_sort_block!(a, b, c, d, e, f, g);
		__store_unbiased!(x, 0, a, 1, b, 2, c, 3, d, 4, e, 5, f, 6, g);
	}

	unsafe fn __sort_8(x: *mut M128, mask: usize)
	{
		//8x8 element bitonic sorter.
		let (a, (b, (c, (d, (e, (f, (g, h))))))) = __load_biased!(x, 0, 1, 2, 3, 4, 5, 6, 7; mask);
		let (a, (b, (c, (d, (e, (f, (g, h))))))) = __sort_block!(a, b, c, d, e, f, g, h);
		let ((a, b), ((c, d), ((e, f), (g, h)))) = __butterfly!(a, b, c, d, e, f, g, h);
		let (a, (b, (c, (d, (e, (f, (g, h))))))) = __partial_sort_block!(a, b, c, d, e, f, g, h);
		let ((a, d), ((b, c), ((e, h), (f, g)))) = __butterfly!(a, d, b, c, e, h, f, g);
		let ((a, b), ((c, d), ((e, f), (g, h)))) = __linear!(a, b, c, d, e, f, g, h);
		let (a, (b, (c, (d, (e, (f, (g, h))))))) = __partial_sort_block!(a, b, c, d, e, f, g, h);
		let ((a, h), ((b, g), ((c, f), (d, e)))) = __butterfly!(a, h, b, g, c, f, d, e);
		let ((a, c), ((b, d), ((e, g), (f, h)))) = __linear!(a, c, b, d, e, g, f, h);
		let ((a, b), ((c, d), ((e, f), (g, h)))) = __linear!(a, b, c, d, e, f, g, h);
		let (a, (b, (c, (d, (e, (f, (g, h))))))) = __partial_sort_block!(a, b, c, d, e, f, g, h);
		__store_unbiased!(x, 0, a, 1, b, 2, c, 3, d, 4, e, 5, f, 6, g, 7, h);
	}

	macro_rules! sort_round
	{
		($shuffle:ident, $striped:ident, $a:ident) => {{
			let b = $shuffle($a);
			let t = intr::_mm_xor_si128(intr::_mm_cmpgt_epi16($a, b), $striped());
			intr::_mm_xor_si128($a, intr::_mm_and_si128(intr::_mm_xor_si128($a, b), t))
		}}
	}

	unsafe fn __butterfly(a: M128, b: M128) -> (M128, M128)
	{
		let b = shuffle_76543210(b);
		let t = intr::_mm_and_si128(intr::_mm_xor_si128(a, b), intr::_mm_cmpgt_epi16(a, b));
		let a = intr::_mm_xor_si128(a, t);
		let b = intr::_mm_xor_si128(b, t);
		let b = shuffle_76543210(b);
		(a,b)
	}

	unsafe fn __linear(a: M128, b: M128) -> (M128, M128)
	{
		let t = intr::_mm_and_si128(intr::_mm_xor_si128(a, b), intr::_mm_cmpgt_epi16(a, b));
		let a = intr::_mm_xor_si128(a, t);
		let b = intr::_mm_xor_si128(b, t);
		(a,b)
	}

	unsafe fn __sort_block(a: M128) -> M128
	{
		let a = sort_round!(shuffle_10325476, striped16, a);
		let a = sort_round!(shuffle_32107654, striped32, a);
		let a = sort_round!(shuffle_10325476, striped16, a);
		let a = sort_round!(shuffle_76543210, striped64, a);
		let a = sort_round!(shuffle_23016745, striped32, a);
		let a = sort_round!(shuffle_10325476, striped16, a);
		a
	}

	unsafe fn __partial_sort_block(a: M128) -> M128
	{
		let a = sort_round!(shuffle_45670123, striped64, a);
		let a = sort_round!(shuffle_23016745, striped32, a);
		let a = sort_round!(shuffle_10325476, striped16, a);
		a
	}

	unsafe fn __load_biased(ptr: *mut M128, mask: usize) -> M128
	{
		let x = intr::_mm_loadu_si128(ptr);
		let y = match mask {
			0 => intr::_mm_set_epi16( 0, 0, 0, 0, 0, 0, 0, 0),
			7 => intr::_mm_set_epi16(-1, 0, 0, 0, 0, 0, 0, 0),
			6 => intr::_mm_set_epi16(-1,-1, 0, 0, 0, 0, 0, 0),
			5 => intr::_mm_set_epi16(-1,-1,-1, 0, 0, 0, 0, 0),
			4 => intr::_mm_set_epi16(-1,-1,-1,-1, 0, 0, 0, 0),
			3 => intr::_mm_set_epi16(-1,-1,-1,-1,-1, 0, 0, 0),
			2 => intr::_mm_set_epi16(-1,-1,-1,-1,-1,-1, 0, 0),
			1 => intr::_mm_set_epi16(-1,-1,-1,-1,-1,-1,-1, 0),
			_ => intr::_mm_set_epi16(-1,-1,-1,-1,-1,-1,-1,-1),
		};
		let x = intr::_mm_or_si128(x, y);
		intr::_mm_add_epi16(x, intr::_mm_set1_epi16(-32768))
	}

	unsafe fn __store_unbiased(ptr: *mut M128, v: M128)
	{
		intr::_mm_storeu_si128(ptr, intr::_mm_add_epi16(v, intr::_mm_set1_epi16(-32768)))
	}

	#[inline(always)]
	fn shuffle_10325476(x: M128) -> M128
	{ unsafe {
		intr::_mm_shufflehi_epi16(intr::_mm_shufflelo_epi16(x, 0xB1), 0xB1)
	}}
	#[inline(always)]
	fn shuffle_23016745(x: M128) -> M128 { unsafe { intr::_mm_shuffle_epi32(x, 0xB1) } }
	#[inline(always)]
	fn shuffle_45670123(x: M128) -> M128 { unsafe { intr::_mm_shuffle_epi32(x, 0x4E) } }
	#[inline(always)]	//Reverse halves.
	fn shuffle_32107654(x: M128) -> M128
	{ unsafe {
		intr::_mm_shufflehi_epi16(intr::_mm_shufflelo_epi16(x, 0x1B), 0x1B)
	} }
	#[inline(always)]	//Reverse groups of two.
	fn shuffle_67452301(x: M128) -> M128 { unsafe { intr::_mm_shuffle_epi32(x, 0x1B) } }
	#[inline(always)]	//Reverse vector.
	fn shuffle_76543210(x: M128) -> M128 { shuffle_10325476(shuffle_67452301(x)) }
	#[inline(always)]
	fn striped16() -> M128 { unsafe { intr::_mm_set_epi16(-1,0,-1,0,-1,0,-1,0) }}
	#[inline(always)]
	fn striped32() -> M128 { unsafe { intr::_mm_set_epi32(-1,0,-1,0) }}
	#[inline(always)]
	fn striped64() -> M128 { unsafe { intr::_mm_set_epi64x(-1,0) }}

/*******************************************************************************************************************/
	#[cfg(test)]
	fn mu(x: M128) -> u128 { unsafe{core::mem::transmute(x)} }

	#[test]
	fn shuffle_test_1()
	{
		let a = unsafe{intr::_mm_set_epi16(1,2,3,4,5,6,7,8)};
		let b = unsafe{intr::_mm_set_epi16(2,1,4,3,6,5,8,7)};
		assert_eq!(mu(shuffle_10325476(a)), mu(b));
	}
	#[test]
	fn shuffle_test_2()
	{
		let a = unsafe{intr::_mm_set_epi16(1,2,3,4,5,6,7,8)};
		let b = unsafe{intr::_mm_set_epi16(3,4,1,2,7,8,5,6)};
		assert_eq!(mu(shuffle_23016745(a)), mu(b));
	}
	#[test]
	fn shuffle_test_3()
	{
		let a = unsafe{intr::_mm_set_epi16(1,2,3,4,5,6,7,8)};
		let b = unsafe{intr::_mm_set_epi16(5,6,7,8,1,2,3,4)};
		assert_eq!(mu(shuffle_45670123(a)), mu(b));
	}
	#[test]
	fn shuffle_test_4()
	{
		let a = unsafe{intr::_mm_set_epi16(1,2,3,4,5,6,7,8)};
		let b = unsafe{intr::_mm_set_epi16(4,3,2,1,8,7,6,5)};
		assert_eq!(mu(shuffle_32107654(a)), mu(b));
	}
	#[test]
	fn shuffle_test_5()
	{
		let a = unsafe{intr::_mm_set_epi16(1,2,3,4,5,6,7,8)};
		let b = unsafe{intr::_mm_set_epi16(7,8,5,6,3,4,1,2)};
		assert_eq!(mu(shuffle_67452301(a)), mu(b));
	}
	#[test]
	fn shuffle_test_6()
	{
		let a = unsafe{intr::_mm_set_epi16(1,2,3,4,5,6,7,8)};
		let b = unsafe{intr::_mm_set_epi16(8,7,6,5,4,3,2,1)};
		assert_eq!(mu(shuffle_76543210(a)), mu(b));
	}

	#[test]
	fn test_blocksort()
	{ unsafe {
		//Use zero-one-principle to exhaustively test the sort.
		for i in 0..256u32 {
			let a = {
				intr::_mm_set_epi16(
					(i >> 7) as i16 & 1, (i >> 6) as i16 & 1,
					(i >> 5) as i16 & 1, (i >> 4) as i16 & 1,
					(i >> 3) as i16 & 1, (i >> 2) as i16 & 1,
					(i >> 1) as i16 & 1, (i >> 0) as i16 & 1,
				)
			};
			let w = i.count_ones();
			let b = {
				intr::_mm_set_epi16(
					(w > 0) as i16, (w > 1) as i16, (w > 2) as i16, (w > 3) as i16,
					(w > 4) as i16, (w > 5) as i16, (w > 6) as i16, (w > 7) as i16,
				)
			};
			let a = __sort_block(a);
			assert_eq!(mu(a), mu(b));
		}
	}}

	#[test]
	fn random_sorting()
	{ unsafe {
		use std::io::Read;
		let mut fp = std::fs::File::open("/dev/urandom").expect("Failed to open urandom");
		let mut buf = [0u16;64];
		for l in 0..65 { for _ in 0..100 {
			let bbuf = std::slice::from_raw_parts_mut(buf.as_mut_ptr() as *mut u8, 128);
			fp.read_exact(bbuf).expect("Reading urandom failed");
			let mut buf2: [u16;64] = buf;
			sort64(&mut buf, l);
			(&mut buf2[..l]).sort();
			assert_eq!(&buf[..l], &buf2[..l]);
		}}
	}}
/*******************************************************************************************************************/
}


#[cfg(not(target_arch="x86_64"))]
mod implementation
{
	pub fn sort64(x: &mut [u16;64], n: usize)
	{ unsafe {
		if n < 64 {
			(&mut x[..n]).sort_unstable();
		} else {
			x.sort_unstable();
		}
	}}

	pub fn sort64(x: &mut [u16;8], n: usize)
	{ unsafe {
		if n < 8 {
			(&mut x[..n]).sort_unstable();
		} else {
			x.sort_unstable();
		}
	}}
}

pub use self::implementation::*;
