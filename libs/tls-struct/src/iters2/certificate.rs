use super::Message;
use crate::LEN_CERTIFICATE_ENTRY;
use crate::LEN_EXTENSION_DATA;
use crate::LEN_SERIALIZED_SCT;
use crate::LEN_SERIALIZED_TRANS_ITEM;
use crate::iters2::CertificateStatus;
use crate::iters2::Extension;
use crate::iters2::IntoGenericIterator as ListOf;
use crate::iters2::IterableItem;
use crate::iters2::IterableItem2;
use crate::iters2::IterableItemE;
use crate::iters2::SerializedSCT;
use crate::iters2::SerializedTransItem;
use crate::iters2::SliceReadError;
use btls_aux_memory::Hexdump;
use btls_aux_tls_iana::ExtensionType;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;


///TLS 1.2 certificate.
///
///This object is an certificate in TLS 1.2 (TLS 1.3 uses `CertificateEntry2`). The corresponding TLS structure
///is `ASN.1Cert`.
///
///This type implements `IterableItem`, so it can be used with `GenericIterator`.
#[derive(Copy,Clone)]
pub struct CertificateEntry<'a>(&'a [u8]);

impl<'a> CertificateEntry<'a>
{
	///Get the actual certificate.
	///
	///This corresponds to the `ASN.1Cert` TLS strucure.
	pub fn get_certificate(&self) -> &'a [u8] { self.0 }
}

unsafe impl<'a> IterableItem<'a> for CertificateEntry<'a>
{
	fn listclass() -> &'static str { "certificate" }
	unsafe fn read_item_unchecked(data: &mut Message<'a>) -> CertificateEntry<'a>
	{
		// opaque ASN.1Cert<1..2^24-1>;
		//
		// - RFC5246 section 7.4.2.
		CertificateEntry(unsafe{data.slice_unchecked(LEN_CERTIFICATE_ENTRY)})
	}
	fn print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		//TODO: Better printing.
		write!(f, "<{entry}>", entry=Hexdump(self.0))
	}
}

impl<'a> IterableItemE for CertificateEntry<'a>
{
	type WithStaticLifetime = CertificateEntry<'static>;
	type ReadError = SliceReadError;
	//Nothing uses thse.
	type LaunderedItem = crate::Void;
	type Kind = crate::Void;
}

unsafe impl<'a> IterableItem2<'a> for CertificateEntry<'a>
{
	fn read_item(data: &mut Message<'a>) -> Result<CertificateEntry<'a>, SliceReadError>
	{
		// opaque ASN.1Cert<1..2^24-1>;
		//
		// - RFC5246 section 7.4.2.
		data.slice(LEN_CERTIFICATE_ENTRY).map(CertificateEntry)
	}
}

/*******************************************************************************************************************/
#[derive(Copy,Clone,Debug)]
pub(crate) enum CertificateEntry2Error
{
	CertData(SliceReadError),
	Extensions(super::VectorError<Extension<'static>>),
	SignedCertificateTimestamp(super::VectorError<SerializedSCT<'static>>),
	TransparencyInfo(super::VectorError<SerializedTransItem<'static>>),
	CertificateStatus2(super::CertificateStatusError),
	Extension2(crate::extensions::ExtensionError2),
	JunkAfterExtension(ExtensionType),
}

impl super::AlertForError for CertificateEntry2Error
{
	fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::CertificateEntry2Error::*;
		match self {
			&CertData(ref e) => e.alert(),
			&Extensions(ref e) => e.alert(),
			&SignedCertificateTimestamp(ref e) => e.alert(),
			&TransparencyInfo(ref e) => e.alert(),
			&CertificateStatus2(ref e) => e.alert(),
			&Extension2(ref e) => e.alert(),
			&JunkAfterExtension(_) => btls_aux_tls_iana::Alert::DECODE_ERROR,
		}
	}
}

impl Display for CertificateEntry2Error
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		const SCT_EXT: ExtensionType = ExtensionType::SIGNED_CERTIFICATE_TIMESTAMP;
		const TINFO_EXT: ExtensionType = ExtensionType::TRANSPARENCY_INFO;
		const OCSP_EXT: ExtensionType = ExtensionType::STATUS_REQUEST;
		use self::CertificateEntry2Error::*;
		match self {
			&CertData(ref err) => write!(f, "cert_data: {err}"),
			&Extensions(ref err) => write!(f, "extensions: {err}"),
			&SignedCertificateTimestamp(ref err) => write!(f, "extension {SCT_EXT}: {err}"),
			&TransparencyInfo(ref err) => write!(f, "extension {TINFO_EXT}: {err}"),
			&CertificateStatus2(ref err) => write!(f, "extension {OCSP_EXT}: {err}"),
			&Extension2(ref err) => core::fmt::Display::fmt(err, f),
			&JunkAfterExtension(ext) => write!(f, "Junk after extension {ext}"),
		}
	}
}

///TLS 1.3 certificate.
///
///This object is an certificate in TLS 1.3 (TLS 1.2 uses `CertificateEntry`). The corresponding TLS strucure is
///`CertificateEntry`.
///
///This type implements `IterableItem`, so it can be used with `GenericIterator`.
#[derive(Copy,Clone)]
pub struct CertificateEntry2<'a>
{
	certificate: &'a [u8],
	extensions: ListOf<'a,Extension<'a>>,
	status: Option<CertificateStatus<'a>>,
	certificate_transparency1: ListOf<'a,SerializedSCT<'a>>,
	certificate_transparency2: ListOf<'a,SerializedTransItem<'a>>,
}

impl<'a> CertificateEntry2<'a>
{
	///Get the actual certificate.
	///
	///This corresponds to the `cert_data` (and corresponding field for other certificate types) field of the
	///TLS `CertificateEntry` strucure.
	pub fn get_certificate(&self) -> &'a [u8] { self.certificate }
	///Get the Certificate extensions.
	///
	///This corresponds to the `extensions` field of the TLS `CertificateEntry` structure.
	pub fn get_extensions(&self) -> ListOf<'a,Extension<'a>> { self.extensions }
	///Get the stapled certificate status for the certificate, if any.
	///
	///This corresponds to the `status_request` extension in TLS `CertificateEntry` structure extensions block.
	pub fn get_status(&self) -> Option<CertificateStatus<'a>> { self.status }
	///Get stapled Certificate Transparency v1 Signed Certificate Timestamps.
	///
	///This corresponds to the `signed_certificate_timestamp` extension in TLS `CertificateEntry`
	///structure extensions block.
	pub fn get_signed_certificate_timestamps(&self) -> ListOf<'a,SerializedSCT<'a>>
	{
		self.certificate_transparency1
	}
	///Get stapled Certificate Transparency v2 Transparency Items.
	///
	///This corresponds to the `transparency_info` extension in TLS `CertificateEntry` structure extensions
	///block.
	pub fn get_transparency_items(&self) -> ListOf<'a,SerializedTransItem<'a>> { self.certificate_transparency2 }
	
}

unsafe impl<'a> IterableItem<'a> for CertificateEntry2<'a>
{
	fn listclass() -> &'static str { "certificate" }
	unsafe fn read_item_unchecked(data: &mut Message<'a>) -> CertificateEntry2<'a>
	{
		let mut status = None;
		let mut ct_v1: ListOf<'a,SerializedSCT<'a>> = Default::default();
		let mut ct_v2: ListOf<'a,SerializedTransItem<'a>> = Default::default();
		// struct {
		//	 opaque cert_data<1..2^24-1>;
		//	 Extension extensions<0..2^16-1>;
		// } CertificateEntry;
		//
		// - RFC8446 section 4.4.2.
		let cert_data = unsafe{data.slice_unchecked(LEN_CERTIFICATE_ENTRY)};
		let extensions = unsafe{data.list_of_unchecked::<Extension<'a>>(LEN_EXTENSION_DATA)};
		for Extension{extension_type, extension_data, ..} in extensions {
			let mut extension_data = Message(extension_data);
			match extension_type {
				ExtensionType::STATUS_REQUEST => status = unsafe {
					Some(CertificateStatus::read_unchecked(&mut extension_data))
				},
				// struct {
				//	 SerializedSCT sct_list <1..2^16-1>;
				// } SignedCertificateTimestampList;
				//
				// - RFC6962 section 3.3.
				ExtensionType::SIGNED_CERTIFICATE_TIMESTAMP => ct_v1 = unsafe {
					extension_data.list_of_unchecked(LEN_SERIALIZED_SCT)
				},
				// struct {
				//	 SerializedTransItem trans_item_list<1..2^16-1>;
				// } TransItemList;
				//
				// - RFC9162 section 6.3
				ExtensionType::TRANSPARENCY_INFO => ct_v2 = unsafe {
					extension_data.list_of_unchecked(LEN_SERIALIZED_TRANS_ITEM)
				},
				_ => ()		//Do nothing with these.
			}
		}
		CertificateEntry2 {
			certificate: cert_data,
			extensions: extensions,
			status: status,
			certificate_transparency1: ct_v1,
			certificate_transparency2: ct_v2,
		}
	}
	fn print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		//TODO: Better printing.
		write!(f, "<{cert}>[extensions={extensions:?}]",
			cert=Hexdump(self.certificate), extensions=self.extensions)
	}
}

impl<'a> IterableItemE for CertificateEntry2<'a>
{
	type WithStaticLifetime = CertificateEntry2<'static>;
	type ReadError = CertificateEntry2Error;
	//Nothing uses thse.
	type LaunderedItem = crate::Void;
	type Kind = crate::Void;
}

unsafe impl<'a> IterableItem2<'a> for CertificateEntry2<'a>
{
	fn read_item(data: &mut Message<'a>) -> Result<CertificateEntry2<'a>, CertificateEntry2Error>
	{
		use self::CertificateEntry2Error::*;
		let mut status = None;
		let mut ct_v1: ListOf<'a,SerializedSCT<'a>> = Default::default();
		let mut ct_v2: ListOf<'a,SerializedTransItem<'a>> = Default::default();
		// struct {
		//	 opaque cert_data<1..2^24-1>;
		//	 Extension extensions<0..2^16-1>;
		// } CertificateEntry;
		//
		// - RFC8446 section 4.4.2.
		let cert_data = data.slice(LEN_CERTIFICATE_ENTRY).map_err(CertData)?;
		let extensions = data.list_of::<Extension<'a>>(LEN_EXTENSION_DATA).map_err(Extensions)?;
		//This does duplicate extension checking.
		crate::extensions::check_extensions_block_cert(extensions).map_err(Extension2)?;
		for Extension{extension_type, extension_data, ..} in extensions {
			let mut extension_data = Message(extension_data);
			match extension_type {
				//Check status type is valid later.
				ExtensionType::STATUS_REQUEST => status =
					Some(CertificateStatus::read_item(&mut extension_data, &|_|true, true).
					map_err(CertificateStatus2)?),
				// struct {
				//	 SerializedSCT sct_list <1..2^16-1>;
				// } SignedCertificateTimestampList;
				//
				// - RFC6962 section 3.3.
				ExtensionType::SIGNED_CERTIFICATE_TIMESTAMP => ct_v1 =
					extension_data.list_of(LEN_SERIALIZED_SCT).
					map_err(SignedCertificateTimestamp)?,
				// struct {
				//	 SerializedTransItem trans_item_list<1..2^16-1>;
				// } TransItemList;
				//
				// - RFC9162 section 6.3
				ExtensionType::TRANSPARENCY_INFO => ct_v2 =
					extension_data.list_of(LEN_SERIALIZED_TRANS_ITEM).
					map_err(TransparencyInfo)?,
				_ => drop(extension_data.all())		//No junk check.
			}
			extension_data.assert_end(JunkAfterExtension(extension_type))?;
		}
		Ok(CertificateEntry2 {
			certificate: cert_data,
			extensions: extensions,
			status: status,
			certificate_transparency1: ct_v1,
			certificate_transparency2: ct_v2,
		})
	}
}
