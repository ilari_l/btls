use super::GenericIterator;
use super::KeyShareEntry;
use super::IntoGenericIterator;
use btls_aux_fail::fail_if;
use btls_aux_tls_iana::NamedGroup;

/*******************************************************************************************************************/
///KEM entry with maybe public key.
///
///This object represents a KEM function, possibly together with a public key for it. In terms of `TLS 1.3`, it
///represents knowledge about a single KEM from `Client Hello`: If it is supported and if it is, if there is a
///public key for it. This structure does not directly correspond to any single TLS structure.
///
///This does not implement `IterableItem`, use `KeyShareIter` for iterator over these items.
#[derive(Copy,Clone)]
pub struct KeyShare<'a>
{
	kem: NamedGroup,
	public_key: Option<&'a [u8]>,
}

impl<'a> KeyShare<'a>
{
	///Get the KEM this share is for.
	pub fn get_kem(&self) -> NamedGroup { self.kem }
	///Get the public key for KEM, if available.
	pub fn get_public_key(&self) -> Option<&'a [u8]> { self.public_key }
}

///Into `KeyShareIter`.
///
///This type exists mainly to be turned into the corresponding iterator `KeyShareIter<'a>` using the
///`iter()` method or the `IntoIterator` trait.
///
///Unlike the `KeyShareIter` (as iterators should not be `Copy`), this type is `Copy`.
///
///This type is distinct from `IntoGenericIterator`, because the iterator type acts on two sources at the same time,
///which is something `GenericIterator` can not do. Also, the items are always filtered: there is no `iter_all()`
///method, and only `count()` is always filtered. This is because `GREASE` key exchange elements are never useful
///(`IntoIterator` can consider those, as it operates over some types where sometimes one wants `GREASE` elements
///too, e.g., extensions).
#[derive(Copy,Clone)]
pub struct IntoKeyShareIter<'a>
{
	groups: IntoGenericIterator<'a,NamedGroup>,
	shares: IntoGenericIterator<'a, KeyShareEntry<'a>>,
}

impl<'a> IntoKeyShareIter<'a>
{
	///Count number of entries.
	///
	///This counts all entries that `iter()` returns: Entries that are not GREASE entries. These entries may
	///or may not have a public key.
	///
	///This operation is `O(1)` time.
	pub fn count(&self) -> usize { self.groups.left_filter }
	///Get iterator over `KeyShare` entries.
	///
	///This iterator skips all GREASE entries, but includes non-GREASE entries that lack public keys.
	pub fn iter(&self) -> KeyShareIter<'a>
	{
		KeyShareIter {
			groups: self.groups.iter(),
			shares: self.shares.iter(),
			remain: self.groups.left_filter,
		}
	}
	pub(crate) fn blank() -> IntoKeyShareIter<'a>
	{
		IntoKeyShareIter{
			groups: Default::default(),
			shares: Default::default(),
		}
	}
	//The only possible error is groups and key shares not matching up.
	pub(crate) fn new(groups: IntoGenericIterator<'a,NamedGroup>,
		shares: IntoGenericIterator<'a, KeyShareEntry<'a>>) -> Result<IntoKeyShareIter<'a>, ()>
	{
		let mut g = groups.iter_all();
		let mut remain = 0usize;
		for KeyShareEntry{group: sgrp, ..} in shares.iter_all() {
			//Scan g forward to find this group. if s is subset of g, this will be found.
			let mut found = false;
			while let Some(ggrp) = g.next() {
				if !ggrp.is_grease() { remain = remain.wrapping_add(1); }
				if sgrp == ggrp { found = true; break; }
			}
			fail_if!(!found, ());
		}
		//Go through remaining groups.
		for ggrp in g {
			if !ggrp.is_grease() { remain = remain.wrapping_add(1); }
		}
		Ok(IntoKeyShareIter{groups, shares})
	}
}

impl<'a> IntoIterator for IntoKeyShareIter<'a>
{
	type Item = KeyShare<'a>;
	type IntoIter = KeyShareIter<'a>;
	fn into_iter(self) -> KeyShareIter<'a> { self.iter() }
}

///Iterator over `KeyShare` objects.
///
///This type is notable for implementing the `Iterator<Item=KeyShare<`a>>` trait.
///
///The `size_hint()` is always both accurate and precise. `Using size_hint().0` is preferred over `clone().count()`,
///because the first is faster due to not having to copy the structure.
///
///This is not `GenericIterator`, since the action of this iterator involves joint iteration of two data sources,
///which is something `GenericIterator` can not do.
#[derive(Clone)]
pub struct KeyShareIter<'a>
{
	groups: GenericIterator<'a,NamedGroup>,
	shares: GenericIterator<'a, KeyShareEntry<'a>>,
	remain: usize,
}

impl<'a> Iterator for KeyShareIter<'a>
{
	type Item = KeyShare<'a>;
	fn next(&mut self) -> Option<KeyShare<'a>>
	{
		//Note that this relies on both groups and key shares performing GREASE filtering.
		//If this does not give a group, quit iterating.
		let group = self.groups.next()?;
		//Peek the iterator, because the group might not be correct.
		let backup_shares = self.shares.clone();
		if let Some(share) = self.shares.next() { if group == share.group {
			//Groups match, return this.
			self.remain -= 1;
			return Some(KeyShare {
				kem: group,
				public_key: Some(share.key_exchange),
			});
		}}
		//The groups did not match up. Restore the backup and yield.
		self.shares = backup_shares;
		self.remain -= 1;
		Some(KeyShare {
			kem: group,
			public_key: None,
		})
	}
	fn size_hint(&self) -> (usize, Option<usize>) { (self.remain, Some(self.remain)) }
	fn count(self) -> usize { self.remain }
}
