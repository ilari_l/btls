use super::IterableItem;
use super::IterableItem2;
use super::IterableItemE;
use super::Message;
use super::PrintableItem;
use super::Trivial;
use btls_aux_fail::fail_if;
use btls_aux_fail::ResultExt;
use core::fmt::Error as FmtError;
use core::fmt::Formatter as Formatter;

macro_rules! defaulted
{
	(empty) => {};
	(single_zero) => { fn special_default() -> (&'static [u8], usize) { (&[0], 1) } };
	(tls12) => { fn special_default() -> (&'static [u8], usize) { (&[3,3], 1) } };
}

//TODO: Set 16-bit is-filtereds to false where the items do not have GREASE values.
macro_rules! is_filtered
{
	(ne8) => { false };
	(ne16s) => { true };
	(ne16l) => { true };
}

macro_rules! item_size
{
	(ne8) => { 1 };
	(ne16s) => { 2 };
	(ne16l) => { 2 };
}


macro_rules! impl_for_type
{
	($clazz:ident $kind:ident $default:ident) => {
		impl PrintableItem for btls_aux_tls_iana::$clazz
		{
			fn display_item(&self, f: &mut Formatter) -> Result<(), FmtError>
			{
				write!(f, "{kind} {self}", kind=Self::kind())
			}
		}
		unsafe impl<'a> IterableItem<'a> for btls_aux_tls_iana::$clazz
		{
			fn listclass() -> &'static str { Self::kind() }
			unsafe fn read_item_unchecked(data: &mut Message<'a>) -> Self
			{
				unsafe{data.cp_unchecked()}
			}
			fn is_filtered(&self) -> bool { self.is_grease() }
			fn print(&self, f: &mut Formatter) -> Result<(), FmtError>
			{
				write!(f, "{self}")
			}
			defaulted!($default);
		}
		impl IterableItemE for btls_aux_tls_iana::$clazz
		{
			const ITEM_SIZE: usize = item_size!($kind);
			const IS_FILTERED: bool = is_filtered!($kind);
			type WithStaticLifetime = btls_aux_tls_iana::$clazz;	//No lifetimes.
			type LaunderedItem = btls_aux_tls_iana::$clazz;
			type ReadError = Trivial;	//read2() should never be called!
			type Kind = btls_aux_tls_iana::$clazz;
			fn check_enabled<F:Fn(Self) -> bool>(&self, enabled: F) -> Result<(), Self>
			{
				//GREASE/SCSV codepoints are never valid alone.
				fail_if!(self.is_grease() || !enabled(*self), *self);
				Ok(())
			}
		}
		unsafe impl<'a> IterableItem2<'a> for btls_aux_tls_iana::$clazz
		{
			fn read_item(data: &mut Message<'a>) -> Result<Self, Trivial>
			{
				//Should never call this!
				data.cp().set_err(Trivial)
			}
		}
	}
}

// ClientCertificateType certificate_types<1..2^8-1>;
//
// - RFC5246 setion 7.4.4
impl_for_type!(ClientCertificateType ne8 empty);
// NamedGroup named_group_list<2..2^16-1>;
//
// - RFC8446 section 4.2.7
impl_for_type!(NamedGroup ne16l empty);
// ECPointFormat ec_point_format_list<1..2^8-1>
//
// - RFC4492 section 5.1.2
impl_for_type!(EcPointFormat ne8 single_zero);
// UserMappingType user_mapping_types<1..2^8-1>;
//
// - RFC4681 section 2
impl_for_type!(UserMappingType ne8 empty);
// AuthzDataFormats authz_format_list<1..2^8-1>;
//
// - RFC5878 section 2.3
impl_for_type!(AuthorizationDataFormat ne8 empty);
// SignatureScheme supported_signature_algorithms<2..2^16-2>;
//
// - RFC8446 section 4.2.3
impl_for_type!(SignatureScheme ne16l empty);
// PskKeyExchangeMode ke_modes<1..255>;
//
// - RFC8446 section 4.2.9
impl_for_type!(PskKeyExchangeMode ne8 empty);
// CertificateType certificate_types<1..2^8-1>;
//
// - RFC6091 section 3.1
impl_for_type!(CertificateType ne8 single_zero);
// CertificateCompressionAlgorithm algorithms<2..2^8-2>;
//
// - RFC8879 section 3
impl_for_type!(CertificateCompressionAlgorithmId ne16s empty);
// CompressionMethod compression_methods<1..2^8-1>;
//
// - RFC5246 section 7.4.1.2
impl_for_type!(CompressionMethod ne8 single_zero);
// EKTCipherType supported_ciphers<1..255>;
//
// - RFC8870 section 5.2.1
impl_for_type!(EktCipher ne16l empty);
// SRTPProtectionProfile SRTPProtectionProfiles<2..2^16-1>;
//
// - RFC5764 section 4.1.1
impl_for_type!(DtlsSrtpProtectionProfile ne16l empty);
// TokenBindingKeyParameters key_parameters_list<1..2^8-1>
//
// - RFC8472 section 2.
impl_for_type!(TokenBindingKeyParameter ne8 empty);
// CachedObject cached_info<1..2^16-1>;
//
// - RFC7924 section 3.
impl_for_type!(CachedInformationType ne16l empty);
// ProtocolVersion versions<2..254>;
//
// - RFC8446 section 4.2.1
impl_for_type!(TlsVersion ne16s tls12);
// SeqNumEncAlgs supported_algs<1..255>;
//
// - draft-pismenny-tls-dtls-plaintext-sequence-number-01 section 3.
impl_for_type!(SequenceNumberEncryptionAlgorithm ne16s empty);

//Does TLS even use this anywhere???
impl_for_type!(SupplementalDataFormat ne16l empty);

//The following make no sense as lists, but are listed for completeness.
impl_for_type!(ContentType ne8 empty);
impl_for_type!(HandshakeType ne8 empty);
impl_for_type!(Alert ne8 empty);
impl_for_type!(EcCurveType ne8 empty);
impl_for_type!(HeartbeatMessageType ne8 empty);
impl_for_type!(HeartbeatMode ne8 empty);
impl_for_type!(SignatureAlgorithm ne8 empty);
impl_for_type!(HashAlgorithm ne8 empty);
impl_for_type!(ExtensionType ne16l empty);
impl_for_type!(CertificateStatusType ne8 empty);
impl_for_type!(ServerNameType ne8 empty);
