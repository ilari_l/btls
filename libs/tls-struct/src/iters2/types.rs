use super::AlertForError;
use super::IterableItem;
use super::IterableItemE;
use super::IterableItem2;
use super::Message;
use super::OcspStatusRequest;
use super::OcspStatusRequestError;
use super::PrimitiveReadError;
use super::PrintableItem;
use super::SliceReadError;
use crate::LEN_CACHED_OBJECT_HASH;
use crate::LEN_CONNECTION_ID;
use crate::LEN_CSRI2_REQUEST;
use crate::LEN_DISTINGUISHED_NAME;
use crate::LEN_EXTENSION_DATA;
use crate::LEN_KEY_SHARE_ENTRY_KEX;
use crate::LEN_OCSP_RESPONSE;
use crate::LEN_OID_FILTER_OID;
use crate::LEN_OID_FILTER_VALUES;
use crate::LEN_PINNING_PROOF;
use crate::LEN_PINNING_TICKET;
use crate::LEN_PROTOCOL_NAME;
use crate::LEN_PSK_BINDER_ENTRY;
use crate::LEN_PSK_IDENTITY_IDENTITY;
use crate::LEN_OCSP_RESPONDER_ID;
use crate::LEN_SCT_V1_EXTENSIONS;
use crate::LEN_SCT_V1_SIGNATURE;
use crate::LEN_SERIALIZED_SCT;
use crate::LEN_SERIALIZED_TRANS_ITEM;
use crate::LEN_SUPPLEMENTAL_DATA_ENTRY_DATA;
use crate::LEN_URL_AND_HASH_URL;
use crate::types::PrintAscii;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_memory::Hexdump;
use btls_aux_tls_iana::AlpnProtocolId;
use btls_aux_tls_iana::CachedInformationType;
use btls_aux_tls_iana::CertificateStatusType;
use btls_aux_tls_iana::ExtensionType;
use btls_aux_tls_iana::NamedGroup;
use btls_aux_tls_iana::SignatureScheme;
use btls_aux_tls_iana::SupplementalDataFormat;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;


macro_rules! __error_type
{
	(SliceReadError $x:ident) => { SliceReadError };
	(eponymous $x:ident) => { crate::errors::$x };
	($y:ident $x:ident) => { $y };
}

macro_rules! wrap_iterator
{
	($clazz:ident $error:ident) => {
		unsafe impl<'a> IterableItem<'a> for $clazz<'a>
		{
			fn listclass() -> &'static str { stringify!($clazz) }
			unsafe fn read_item_unchecked(data: &mut Message<'a>) -> $clazz<'a>
			{
				unsafe{Self::__read_unchecked(data)}
			}
			fn print(&self, f: &mut Formatter) -> Result<(), FmtError> { self.__print(f) }
		}
		unsafe impl<'a> IterableItem2<'a> for $clazz<'a>
		{
			fn read_item(data: &mut Message<'a>) -> Result<$clazz<'a>, __error_type!($error $clazz)>
			{
				Self::__read(data)
			}
		}
	}
}

/*******************************************************************************************************************/
#[derive(Copy,Clone,Debug)]
pub(crate) enum UrlAndHashError
{
	Url(SliceReadError),
	Padding(PrimitiveReadError),
	BadPadding(u8),
	Sha1Hash(PrimitiveReadError),
}

impl AlertForError for UrlAndHashError
{
	fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::UrlAndHashError::*;
		match self {
			&Url(ref e) => e.alert(),
			&Padding(ref e) => e.alert(),
			&BadPadding(_) => btls_aux_tls_iana::Alert::ILLEGAL_PARAMETER,
			&Sha1Hash(ref e) => e.alert(),
		}
	}
}

impl Display for UrlAndHashError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::UrlAndHashError::*;
		match self {
			&Url(ref err) => write!(f, "url: {err}"),
			&Padding(ref err) => write!(f, "padding: {err}"),
			&BadPadding(val) => write!(f, "padding: Bad value {val}"),
			&Sha1Hash(ref err) => write!(f, "sha1_hash: {err}"),
		}
	}
}

///Certificate URL URL and Hash.
///
///This corresponds to the `UrlAndHash` TLS structure.
///
///This type implements `IterableItem`, so it can be used with `GenericIterator`.
#[derive(Copy,Clone)]
pub struct UrlAndHash<'a>
{
	url: &'a [u8],
	sha1_hash: [u8;20],
}

impl<'a> UrlAndHash<'a>
{
	///Get the URL of the object.
	///
	///This corresponds to the `url` field of the `UrlAndHash` TLS structure.
	pub fn get_url(&self) -> &'a [u8] { self.url }
	///Get the SHA-1 hash of the object.
	///
	///This corresponds to the `sha1_hash` field of the `UrlAndHash` TLS structure.
	pub fn get_hash_sha1(&self) -> [u8;20] { self.sha1_hash }
	
	unsafe fn __read_unchecked(data: &mut Message<'a>) -> UrlAndHash<'a>
	{
		// struct {
		//	 opaque url<1..2^16-1>;
		//	 unint8 padding;
		//	 opaque SHA1Hash[20];
		// } UrlAndHash;
		//
		// - RFC6066 section 5.
		let url = unsafe{data.slice_unchecked(LEN_URL_AND_HASH_URL)};
		let _padding: u8 = unsafe{data.cp_unchecked()};
		let sha1_hash = unsafe{data.array_unchecked()};
		UrlAndHash{url, sha1_hash}
	}
	fn __read(data: &mut Message<'a>) -> Result<UrlAndHash<'a>, UrlAndHashError>
	{
		use self::UrlAndHashError::*;
		// struct {
		//	 opaque url<1..2^16-1>;
		//	 unint8 padding;
		//	 opaque SHA1Hash[20];
		// } UrlAndHash;
		//
		// - RFC6066 section 5.
		let url = data.slice(LEN_URL_AND_HASH_URL).map_err(Url)?;
		let padding: u8 = data.cp().map_err(Padding)?;
		fail_if!(padding != 1, BadPadding(padding));
		let sha1_hash = data.array().map_err(Sha1Hash)?;
		Ok(UrlAndHash{url, sha1_hash})
	}
	fn __print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "{url}[sha1:<{hash}>]", url=PrintAscii(self.url), hash=Hexdump(&self.sha1_hash))
	}
}

impl<'a> IterableItemE for UrlAndHash<'a>
{
	const IS_FILTERED: bool = false;
	type WithStaticLifetime = UrlAndHash<'static>;
	type ReadError = UrlAndHashError;
	//Nothing uses thse.
	type LaunderedItem = crate::Void;
	type Kind = crate::Void;
}

wrap_iterator!(UrlAndHash UrlAndHashError);
/*******************************************************************************************************************/
#[derive(Copy,Clone,Debug)]
pub(crate) enum TrustedAuthorityError
{
	IdentifierType(PrimitiveReadError),
	IdentifierK(PrimitiveReadError),
	IdentifierX(SliceReadError),
	IdentifierC(PrimitiveReadError),
	BadIdentifierType(u8),
}

impl AlertForError for TrustedAuthorityError
{
	fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::TrustedAuthorityError::*;
		match self {
			&IdentifierType(ref e) => e.alert(),
			&IdentifierK(ref e) => e.alert(),
			&IdentifierX(ref e) => e.alert(),
			&IdentifierC(ref e) => e.alert(),
			&BadIdentifierType(_) => btls_aux_tls_iana::Alert::ILLEGAL_PARAMETER
		}
	}
}

impl Display for TrustedAuthorityError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::TrustedAuthorityError::*;
		match self {
			&IdentifierType(ref err) => write!(f, "identifier_type: {err}"),
			&IdentifierK(ref err) => write!(f, "key_sha1_hash: {err}"),
			&IdentifierX(ref err) => write!(f, "x509_name: {err}"),
			&IdentifierC(ref err) => write!(f, "cert_sha1_hash: {err}"),
			&BadIdentifierType(itype) => write!(f, "Illegal identifier type {itype}"),
		}
	}
}

//This is ok being an enumeration because it is not technically feasible to add anything to it, and it is also not
//feasible to break it down further.
//
///Trusted Authority.
///
///This object specifies a Certificate Authority using one of the following methods:
///
/// * Pre-agreed.
/// * Key SHA-1 hash.
/// * X.509 name.
/// * Certificate SHA-1 hash.
///
///It corresponds to the `TrustedAuthority` TLS structure.
///
///This type implements `IterableItem`, so it can be used with `GenericIterator`.
#[derive(Copy,Clone)]
pub enum TrustedAuthority<'a>
{
	///Pre-agreed.
	PreAgreed,
	///SHA-1 hash of key.
	KeySha1Hash([u8;20]),
	///X.509 name.
	X509Name(&'a [u8]),
	///SHA-1 hash of certificate.
	CertSha1Hash([u8;20]),
}

const TCA_PRE_AGREED: u8 = 0;
const TCA_KEY_SHA1: u8 = 1;
const TCA_X509_NAME: u8 = 2;
const TCA_CERT_SHA1: u8 = 3;

impl<'a> TrustedAuthority<'a>
{
	unsafe fn __read_unchecked(data: &mut Message<'a>) -> TrustedAuthority<'a>
	{
		// struct {
		//	 IdentifierType identifier_type;
		//	 select (identifier_type) {
		//		 case pre_agreed: struct {};
		//		 case key_sha1_hash: SHA1Hash;
		//		 case x509_name: DistinguishedName;
		//		 case cert_sha1_hash: SHA1Hash;
		//	 } identifier;
		// } TrustedAuthority;
		//
		// opaque DistinguishedName<1..2^16-1>;
		// opaque SHA1Hash[20];
		//
		// - RFC6066 section 6
		let d: u8 = unsafe{data.cp_unchecked()};
		match d {
			TCA_PRE_AGREED => TrustedAuthority::PreAgreed,
			TCA_KEY_SHA1 => TrustedAuthority::KeySha1Hash(unsafe{data.array_unchecked()}),
			TCA_X509_NAME => {
				let dn = unsafe{data.slice_unchecked(LEN_DISTINGUISHED_NAME)};
				TrustedAuthority::X509Name(dn)
			},
			TCA_CERT_SHA1 => TrustedAuthority::CertSha1Hash(unsafe{data.array_unchecked()}),
			_ => unsafe{core::hint::unreachable_unchecked()}
		}
	}
	fn __read(data: &mut Message<'a>) -> Result<TrustedAuthority<'a>, TrustedAuthorityError>
	{
		// struct {
		//	 IdentifierType identifier_type;
		//	 select (identifier_type) {
		//		 case pre_agreed: struct {};
		//		 case key_sha1_hash: SHA1Hash;
		//		 case x509_name: DistinguishedName;
		//		 case cert_sha1_hash: SHA1Hash;
		//	 } identifier;
		// } TrustedAuthority;
		//
		// opaque DistinguishedName<1..2^16-1>;
		// opaque SHA1Hash[20];
		//
		// - RFC6066 section 6
		use self::TrustedAuthorityError::*;
		let d: u8 = data.cp().map_err(IdentifierType)?;
		let r = match d {
			TCA_PRE_AGREED => TrustedAuthority::PreAgreed,
			TCA_KEY_SHA1 => TrustedAuthority::KeySha1Hash(data.array().map_err(IdentifierK)?),
			TCA_X509_NAME => {
				let x = data.slice(LEN_DISTINGUISHED_NAME).map_err(IdentifierX)?;
				TrustedAuthority::X509Name(x)
			},
			TCA_CERT_SHA1 => TrustedAuthority::CertSha1Hash(data.array().map_err(IdentifierC)?),
			x => fail!(BadIdentifierType(x))
		};
		Ok(r)
	}
	fn __print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&TrustedAuthority::PreAgreed => write!(f, "pre_agreed"),
			&TrustedAuthority::KeySha1Hash(ref h) => write!(f, "key(<{h}>)", h=Hexdump(h)),
			//TDOO: Better printing.
			&TrustedAuthority::X509Name(h) => write!(f, "x509(<{h}>)", h=Hexdump(h)),
			&TrustedAuthority::CertSha1Hash(ref h) => write!(f, "cert(<{h}>)", h=Hexdump(h)),
		}
	}
}

impl<'a> IterableItemE for TrustedAuthority<'a>
{
	type WithStaticLifetime = TrustedAuthority<'static>;
	type ReadError = TrustedAuthorityError;
	//Nothing uses thse.
	type LaunderedItem = crate::Void;
	type Kind = crate::Void;
}

wrap_iterator!(TrustedAuthority TrustedAuthorityError);
/*******************************************************************************************************************/
#[derive(Copy,Clone,Debug)]
pub(crate) enum SupplementalDataEntryError
{
	SuppDataType(PrimitiveReadError),
	SuppData(SliceReadError),
}

impl AlertForError for SupplementalDataEntryError
{
	fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::SupplementalDataEntryError::*;
		match self {
			&SuppDataType(ref e) => e.alert(),
			&SuppData(ref e) => e.alert(),
		}
	}
}

impl Display for SupplementalDataEntryError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::SupplementalDataEntryError::*;
		match self {
			&SuppDataType(ref err) => write!(f, "supp_data_type: {err}"),
			&SuppData(ref err) => write!(f, "supp_data: {err}"),
		}
	}
}


///TLS Supplemental Data Entry.
///
///This corresponds to the `SupplementalDataEntry` TLS structure.
///
///This type implements `IterableItem`, so it can be used with `GenericIterator`.
#[derive(Copy,Clone)]
pub struct SupplementalDataEntry<'a>
{
	supp_data_type: SupplementalDataFormat,
	supp_data: &'a [u8],
}

impl<'a> SupplementalDataEntry<'a>
{
	///Get the type of supplemental data.
	///
	///This corresponds to the `supp_data_type` field of the `SupplementalDataEntry` TLS structure.
	pub fn get_supplemental_data_type(&self) -> SupplementalDataFormat { self.supp_data_type }
	///Get the raw supplemental data.
	///
	///This corresponds to the anonymous field following `supp_data_length` field of the
	///`SupplementalDataEntry` TLS structure.
	pub fn get_raw_supplemental_data(&self) -> &'a [u8] { self.supp_data }

	unsafe fn __read_unchecked(data: &mut Message<'a>) -> SupplementalDataEntry<'a>
	{
		// struct {
		//	 SupplementalDataType supp_data_type;
		//	 uint16 supp_data_length;
		//	 select(SupplementalDataType) { }
		// } SupplementalDataEntry;
		//
		// - RFC4680 section 2.
		let supp_data_type = unsafe{data.cp_unchecked()};
		let supp_data = unsafe{data.slice_unchecked(LEN_SUPPLEMENTAL_DATA_ENTRY_DATA)};
		SupplementalDataEntry{supp_data_type, supp_data}
	}
	fn __read(data: &mut Message<'a>) -> Result<SupplementalDataEntry<'a>, SupplementalDataEntryError>
	{
		use self::SupplementalDataEntryError::*;
		// struct {
		//	 SupplementalDataType supp_data_type;
		//	 uint16 supp_data_length;
		//	 select(SupplementalDataType) { }
		// } SupplementalDataEntry;
		//
		// - RFC4680 section 2.
		let supp_data_type = data.cp().map_err(SuppDataType)?;
		let supp_data = data.slice(LEN_SUPPLEMENTAL_DATA_ENTRY_DATA).map_err(SuppData)?;
		Ok(SupplementalDataEntry{supp_data_type, supp_data})
	}
	fn __print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "{sdt}:<{supp_data}>", sdt=self.supp_data_type, supp_data=Hexdump(self.supp_data))
	}
}

impl<'a> IterableItemE for SupplementalDataEntry<'a>
{
	const IS_FILTERED: bool = false;
	type WithStaticLifetime = SupplementalDataEntry<'static>;
	type ReadError = SupplementalDataEntryError;
	//Nothing uses thse.
	type LaunderedItem = crate::Void;
	type Kind = crate::Void;
}

wrap_iterator!(SupplementalDataEntry SupplementalDataEntryError);
/*******************************************************************************************************************/
#[derive(Copy,Clone,Debug)]
pub(crate) enum PskIdentityError
{
	Identity(SliceReadError),
	ObfuscatedTicketAge(PrimitiveReadError),
}

impl AlertForError for PskIdentityError
{
	fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::PskIdentityError::*;
		match self {
			&Identity(ref e) => e.alert(),
			&ObfuscatedTicketAge(ref e) => e.alert(),
		}
	}
}

impl Display for PskIdentityError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::PskIdentityError::*;
		match self {
			&Identity(ref err) => write!(f, "identity: {err}"),
			&ObfuscatedTicketAge(ref err) => write!(f, "obfuscated_ticket_age: {err}"),
		}
	}
}


#[derive(Copy,Clone)]
pub(crate) struct PskBinderEntry<'a>(pub(crate) &'a [u8]);

#[derive(Copy,Clone)]
pub(crate) struct PskIdentity<'a>
{
	pub(crate) identity: &'a [u8],
	pub(crate) obfuscated_ticket_age: u32,
}

impl<'a> PskBinderEntry<'a>
{
	unsafe fn __read_unchecked(data: &mut Message<'a>) -> PskBinderEntry<'a>
	{
		// opaque PskBinderEntry<32..255>;
		//
		// - RFC8446 section 4.2.11.
		PskBinderEntry(unsafe{data.slice_unchecked(LEN_PSK_BINDER_ENTRY)})
	}
	fn __read(data: &mut Message<'a>) -> Result<PskBinderEntry<'a>, SliceReadError>
	{
		// opaque PskBinderEntry<32..255>;
		//
		// - RFC8446 section 4.2.11.
		data.slice(LEN_PSK_BINDER_ENTRY).map(PskBinderEntry)
	}
	fn __print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "<{h}>", h=Hexdump(self.0))
	}
}

impl<'a> PskIdentity<'a>
{
	unsafe fn __read_unchecked(data: &mut Message<'a>) -> PskIdentity<'a>
	{
		// struct {
		//	 opaque identity<1..2^16-1>;
		//	 uint32 obfuscated_ticket_age;
		// } PskIdentity;
		//
		// - RFC8446 section 4.2.11.
		let identity = unsafe{data.slice_unchecked(LEN_PSK_IDENTITY_IDENTITY)};
		let obfuscated_ticket_age = unsafe{data.cp_unchecked()};
		PskIdentity{identity, obfuscated_ticket_age}
	}
	fn __read(data: &mut Message<'a>) -> Result<PskIdentity<'a>, PskIdentityError>
	{
		use self::PskIdentityError::*;
		// struct {
		//	 opaque identity<1..2^16-1>;
		//	 uint32 obfuscated_ticket_age;
		// } PskIdentity;
		//
		// - RFC8446 section 4.2.11.
		let identity = data.slice(LEN_PSK_IDENTITY_IDENTITY).map_err(Identity)?;
		let obfuscated_ticket_age = data.cp().map_err(ObfuscatedTicketAge)?;
		Ok(PskIdentity{identity, obfuscated_ticket_age})
	}
	fn __print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		let obs_age = self.obfuscated_ticket_age;
		write!(f, "[id=<{identity}>,o_age={obs_age:08x}]", identity=Hexdump(self.identity))
	}
}

impl<'a> IterableItemE for PskBinderEntry<'a>
{
	type WithStaticLifetime = PskBinderEntry<'static>;
	type ReadError = SliceReadError;
	//Nothing uses thse.
	type LaunderedItem = crate::Void;
	type Kind = crate::Void;
}

impl<'a> IterableItemE for PskIdentity<'a>
{
	type WithStaticLifetime = PskIdentity<'static>;
	type ReadError = PskIdentityError;
	//Nothing uses thse.
	type LaunderedItem = crate::Void;
	type Kind = crate::Void;
}

wrap_iterator!(PskBinderEntry SliceReadError);
wrap_iterator!(PskIdentity PskIdentityError);
/*******************************************************************************************************************/
#[derive(Copy,Clone)]
pub(crate) struct PinningTicket<'a>(pub(crate) &'a [u8]);

impl<'a> PinningTicket<'a>
{
	unsafe fn __read_unchecked(data: &mut Message<'a>) -> PinningTicket<'a>
	{
		// opaque pinning_ticket<0..2^16-1>;
		//
		// - RFC8672 section 3
		PinningTicket(unsafe{data.slice_unchecked(LEN_PINNING_TICKET)})
	}
	fn __read(data: &mut Message<'a>) -> Result<PinningTicket<'a>, SliceReadError>
	{
		// opaque pinning_ticket<0..2^16-1>;
		//
		// - RFC8672 section 3
		data.slice(LEN_PINNING_TICKET).map(PinningTicket)
	}
	fn __print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "<{h}>", h=Hexdump(self.0))
	}
}

impl<'a> IterableItemE for PinningTicket<'a>
{
	type WithStaticLifetime = PinningTicket<'static>;
	type ReadError = SliceReadError;
	//Nothing uses thse.
	type LaunderedItem = crate::Void;
	type Kind = crate::Void;
}

wrap_iterator!(PinningTicket SliceReadError);
/*******************************************************************************************************************/
#[derive(Copy,Clone)]
pub(crate) struct PinningProof<'a>(pub(crate) &'a [u8]);

impl<'a> PinningProof<'a>
{
	unsafe fn __read_unchecked(data: &mut Message<'a>) -> PinningProof<'a>
	{
		// opaque pinning_proof<0..2^8-1>;
		//
		// - RFC8672 section 3
		PinningProof(unsafe{data.slice_unchecked(LEN_PINNING_PROOF)})
	}
	fn __read(data: &mut Message<'a>) -> Result<PinningProof<'a>, SliceReadError>
	{
		// opaque pinning_proof<0..2^8-1>;
		//
		// - RFC8672 section 3
		data.slice(LEN_PINNING_PROOF).map(PinningProof)
	}
	fn __print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "<{h}>", h=Hexdump(self.0))
	}
}

impl<'a> IterableItemE for PinningProof<'a>
{
	type WithStaticLifetime = PinningProof<'static>;
	type ReadError = SliceReadError;
	//Nothing uses thse.
	type LaunderedItem = crate::Void;
	type Kind = crate::Void;
}

wrap_iterator!(PinningProof SliceReadError);
/*******************************************************************************************************************/
#[derive(Copy,Clone,Debug)]
pub(crate) enum OidFilterError
{
	CertificateExtensionOid(SliceReadError),
	CertificateExtensionValues(SliceReadError),
}

impl AlertForError for OidFilterError
{
	fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::OidFilterError::*;
		match self {
			&CertificateExtensionOid(ref e) => e.alert(),
			&CertificateExtensionValues(ref e) => e.alert(),
		}
	}
}

impl Display for OidFilterError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::OidFilterError::*;
		match self {
			&CertificateExtensionOid(ref err) => write!(f, "certificate_extension_oid: {err}"),
			&CertificateExtensionValues(ref err) => write!(f, "certificate_extension_values: {err}"),
		}
	}
}

///TLS OID Filter
///
///TLS OID Filters are used to filter client certificates that may be used. These correspond to the
///`OIDFilter` TLS structure.
///
///This type implements `IterableItem`, so it can be used with `GenericIterator`.
#[derive(Copy,Clone)]
pub struct OidFilter<'a>
{
	certificate_extension_oid: &'a [u8],
	certificate_extension_values: &'a [u8],
}

impl<'a> OidFilter<'a>
{
	///Get the OID to filter on.
	///
	///The OID is represented using "DER-encoded format". This corresponds to th `certificate_extension_oid`
	///field of the `OIDFilter` TLS structure.
	pub fn get_oid(&self) -> &'a [u8] { self.certificate_extension_oid }
	///Get the values to filter on.
	///
	///All these values MUST appear in the certificate. This corresponds to th `certificate_extension_values`
	///field of the `OIDFilter` TLS structure.
	pub fn get_values(&self) -> &'a [u8] { self.certificate_extension_values }
	
	unsafe fn __read_unchecked(data: &mut Message<'a>) -> OidFilter<'a>
	{
		// struct {
		//	 opaque certificate_extension_oid<1..2^8-1>;
		//	 opaque certificate_extension_values<0..2^16-1>;
		// } OIDFilter;
		//
		// - RFC7924 Section 3.
		let certificate_extension_oid = unsafe{data.slice_unchecked(LEN_OID_FILTER_OID)};
		let certificate_extension_values = unsafe{data.slice_unchecked(LEN_OID_FILTER_VALUES)};
		OidFilter{certificate_extension_oid, certificate_extension_values}
	}
	fn __read(data: &mut Message<'a>) -> Result<OidFilter<'a>, OidFilterError>
	{
		use self::OidFilterError::*;
		// struct {
		//	 opaque certificate_extension_oid<1..2^8-1>;
		//	 opaque certificate_extension_values<0..2^16-1>;
		// } OIDFilter;
		//
		// - RFC7924 Section 3.
		let certificate_extension_oid = data.slice(LEN_OID_FILTER_OID).
			map_err(CertificateExtensionOid)?;
		let certificate_extension_values = data.slice(LEN_OID_FILTER_VALUES).
			map_err(CertificateExtensionValues)?;
		Ok(OidFilter{certificate_extension_oid, certificate_extension_values})
	}
	fn __print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		//FIXME: Print OID as textual.
		write!(f, "<{ext_oid}>:<{ext_val}>",
			ext_oid=Hexdump(self.certificate_extension_oid),
			ext_val=Hexdump(self.certificate_extension_values))
	}
}

impl<'a> IterableItemE for OidFilter<'a>
{
	const IS_FILTERED: bool = false;
	type WithStaticLifetime = OidFilter<'static>;
	type ReadError = OidFilterError;
	//Nothing uses thse.
	type LaunderedItem = crate::Void;
	type Kind = crate::Void;
}

wrap_iterator!(OidFilter OidFilterError);
/*******************************************************************************************************************/
///OCSP response
///
///This object represents OCSP response in is raw PKIX ASN.1 DER form.
///
///This type implements `IterableItem`, so it can be used with `GenericIterator`.
#[derive(Copy,Clone,Debug)]
pub struct OcspResponse<'a>(&'a [u8]);

impl<'a> OcspResponse<'a>
{
	///Get the response.
	pub fn get_response(&self) -> &'a [u8] { self.0 }

	unsafe fn __read_unchecked(data: &mut Message<'a>) -> OcspResponse<'a>
	{
		// opaque OCSPResponse<0..2^24-1>;
		//
		// - RFC6961 section 2.2
		OcspResponse(unsafe{data.slice_unchecked(LEN_OCSP_RESPONSE)})
	}
	fn __read(data: &mut Message<'a>) -> Result<OcspResponse<'a>, SliceReadError>
	{
		// opaque OCSPResponse<0..2^24-1>;
		//
		// - RFC6961 section 2.2
		data.slice(LEN_OCSP_RESPONSE).map(OcspResponse)
	}
	fn __print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "<{h}>", h=Hexdump(self.0))
	}
}

impl<'a> IterableItemE for OcspResponse<'a>
{
	type WithStaticLifetime = OcspResponse<'static>;
	type ReadError = SliceReadError;
	//Nothing uses thse.
	type LaunderedItem = crate::Void;
	type Kind = crate::Void;
}

wrap_iterator!(OcspResponse SliceReadError);
/*******************************************************************************************************************/
///OCSP responder.
///
///This object represents OCSP Responder ID. A Responder ID can either be X.509 name or SHA-1 hash of responder's
///public key. It is encoded as ASN.1 DER encoding of ResponderID from RFC2560. It corresponds to the
///`ResponderID` TLS structure.
///
///This type implements `IterableItem`, so it can be used with `GenericIterator`.
#[derive(Copy,Clone)]
pub struct OcspResponder<'a>(&'a [u8]);

impl<'a> OcspResponder<'a>
{
	///Get the responder.
	pub fn get_responder(&self) -> &'a [u8] { self.0 }

	unsafe fn __read_unchecked(data: &mut Message<'a>) -> OcspResponder<'a>
	{
		// opaque ResponderID<1..2^16-1>;
		//
		// - RFC6066 section 8
		OcspResponder(unsafe{data.slice_unchecked(LEN_OCSP_RESPONDER_ID)})
	}
	fn __read(data: &mut Message<'a>) -> Result<OcspResponder<'a>, SliceReadError>
	{
		// opaque ResponderID<1..2^16-1>;
		//
		// - RFC6066 section 8
		data.slice(LEN_OCSP_RESPONDER_ID).map(OcspResponder)
	}
	fn __print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		//TODO: Better printing.
		write!(f, "<{h}>", h=Hexdump(self.0))
	}
}

impl<'a> IterableItemE for OcspResponder<'a>
{
	type WithStaticLifetime = OcspResponder<'static>;
	type ReadError = SliceReadError;
	//Nothing uses thse.
	type LaunderedItem = crate::Void;
	type Kind = crate::Void;
}

wrap_iterator!(OcspResponder SliceReadError);
/*******************************************************************************************************************/
///Certificate Authority.
///
///This object represents Certificate Authority by its X.509 name. It corresponds to the `DistinguishedName`
///TLS structure.
///
///This type implements `IterableItem`, so it can be used with `GenericIterator`.
#[derive(Copy,Clone)]
pub struct CertificateAuthority<'a>(&'a [u8]);

impl<'a> CertificateAuthority<'a>
{
	///Get the name of the certificate authority.
	pub fn get_name(&self) -> &'a [u8] { self.0 }

	unsafe fn __read_unchecked(data: &mut Message<'a>) -> CertificateAuthority<'a>
	{
		// opaque DistinguishedName<1..2^16-1>;
		//
		// - RFC8446 section 4.2.4.
		CertificateAuthority(unsafe{data.slice_unchecked(LEN_DISTINGUISHED_NAME)})
	}
	fn __read(data: &mut Message<'a>) -> Result<CertificateAuthority<'a>, SliceReadError>
	{
		// opaque DistinguishedName<1..2^16-1>;
		//
		// - RFC8446 section 4.2.4.
		data.slice(LEN_DISTINGUISHED_NAME).map(CertificateAuthority)
	}
	fn __print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		//TODO: Better printing.
		write!(f, "<{h}>", h=Hexdump(self.0))
	}
}

impl<'a> IterableItemE for CertificateAuthority<'a>
{
	type WithStaticLifetime = CertificateAuthority<'static>;
	type ReadError = SliceReadError;
	//Nothing uses thse.
	type LaunderedItem = crate::Void;
	type Kind = crate::Void;
}

wrap_iterator!(CertificateAuthority SliceReadError);
/*******************************************************************************************************************/
#[derive(Copy,Clone,Debug)]
pub(crate) enum KeyShareEntryError
{
	Group(PrimitiveReadError),
	KeyExchange(SliceReadError),
}

impl AlertForError for KeyShareEntryError
{
	fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::KeyShareEntryError::*;
		match self {
			&Group(ref e) => e.alert(),
			&KeyExchange(ref e) => e.alert(),
		}
	}
}

impl Display for KeyShareEntryError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::KeyShareEntryError::*;
		match self {
			&Group(ref err) => write!(f, "group: {err}"),
			&KeyExchange(ref err) => write!(f, "key_exchange: {err}"),
		}
	}
}


#[derive(Copy,Clone)]
pub(crate) struct KeyShareEntry<'a>
{
	pub(crate) group: NamedGroup,
	pub(crate) key_exchange: &'a [u8],
}

impl<'a> KeyShareEntry<'a>
{
	unsafe fn __read_unchecked(data: &mut Message<'a>) -> KeyShareEntry<'a>
	{
		// struct {
		//	 NamedGroup group;
		//	 opaque key_exchange<1..2^16-1>;
		// } KeyShareEntry;
		//
		// - RFC8446 section 4.2.8.
		let group = unsafe{data.cp_unchecked()};
		let key_exchange = unsafe{data.slice_unchecked(LEN_KEY_SHARE_ENTRY_KEX)};
		KeyShareEntry{group, key_exchange}
	}
	fn __read(data: &mut Message<'a>) -> Result<KeyShareEntry<'a>, KeyShareEntryError>
	{
		use self::KeyShareEntryError::*;
		// struct {
		//	 NamedGroup group;
		//	 opaque key_exchange<1..2^16-1>;
		// } KeyShareEntry;
		//
		// - RFC8446 section 4.2.8.
		let group = data.cp().map_err(Group)?;
		let key_exchange = data.slice(LEN_KEY_SHARE_ENTRY_KEX).map_err(KeyExchange)?;
		Ok(KeyShareEntry{group, key_exchange})
	}
	fn __print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "{grp}:<{kex}>", grp=self.group, kex=Hexdump(self.key_exchange))
	}
}

unsafe impl<'a> IterableItem<'a> for KeyShareEntry<'a>
{
	fn listclass() -> &'static str { "KeyShareEntry" }
	unsafe fn read_item_unchecked(data: &mut Message<'a>) -> KeyShareEntry<'a>
	{
		unsafe{Self::__read_unchecked(data)}
	}
	fn is_filtered(&self) -> bool { self.group.is_grease() }
	fn print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		self.__print(f)
	}
}

impl<'a> IterableItemE for KeyShareEntry<'a>
{
	const IS_FILTERED: bool = true;
	type WithStaticLifetime = KeyShareEntry<'static>;
	type ReadError = KeyShareEntryError;
	type LaunderedItem = NamedGroup;
	type Kind = NamedGroup;
	fn check_enabled<F:Fn(NamedGroup) -> bool>(&self, enabled: F) -> Result<(), NamedGroup>
	{
		let item = self.group;
		//GREASE/SCSV codepoints are never valid alone.
		fail_if!(item.is_grease() || !enabled(item), item);
		Ok(())
	}
}

unsafe impl<'a> IterableItem2<'a> for KeyShareEntry<'a>
{
	fn read_item(data: &mut Message<'a>) -> Result<KeyShareEntry<'a>, KeyShareEntryError>
	{
		Self::__read(data)
	}
}
/*******************************************************************************************************************/
#[derive(Copy,Clone,Debug)]
pub(crate) enum ExtensionError
{
	ExtensionType(PrimitiveReadError),
	ExtensionData(SliceReadError),
}

impl AlertForError for ExtensionError
{
	fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::ExtensionError::*;
		match self {
			&ExtensionType(ref e) => e.alert(),
			&ExtensionData(ref e) => e.alert(),
		}
	}
}

impl Display for ExtensionError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::ExtensionError::*;
		match self {
			&ExtensionType(ref err) => write!(f, "extension_type: {err}"),
			&ExtensionData(ref err) => write!(f, "extension_data: {err}"),
		}
	}
}


///TLS Extension.
///
///This object is TLS extension. Many TLS messages have extension blocks possibly containing extensions. This
///corresponds to the `Extension` TLS structure.
///
///This type implements `IterableItem`, so it can be used with `GenericIterator`.
#[derive(Copy,Clone)]
pub struct Extension<'a>
{
	pub(crate) extension_type: ExtensionType,
	pub(crate) extension_data: &'a [u8],
}

impl<'a> Extension<'a>
{
	///Get the type of extension.
	///
	///This corresponds to the `extension_type` field of the `Extension` TLS structure.
	pub fn get_type(&self) -> ExtensionType { self.extension_type }
	///Get the raw data of extension.
	///
	///This corresponds to the `extension_data` field of the `Extension` TLS structure.
	pub fn get_raw_data(&self) -> &'a [u8] { self.extension_data }

	unsafe fn __read_unchecked(data: &mut Message<'a>) -> Extension<'a>
	{
		// struct {
		//	 ExtensionType extension_type;
		//	 opaque extension_data<0..2^16-1>;
		// } Extension;
		//
		// - RFC5246 Section 7.4.1.4.
		let extension_type = unsafe{data.cp_unchecked()};
		let extension_data = unsafe{data.slice_unchecked(LEN_EXTENSION_DATA)};
		Extension{extension_type, extension_data}
	}
	fn __read(data: &mut Message<'a>) -> Result<Extension<'a>, ExtensionError>
	{
		use self::ExtensionError::ExtensionType as ExtensionTypeF;
		use self::ExtensionError::ExtensionData;
		// struct {
		//	 ExtensionType extension_type;
		//	 opaque extension_data<0..2^16-1>;
		// } Extension;
		//
		// - RFC5246 Section 7.4.1.4.
		let extension_type = data.cp().map_err(ExtensionTypeF)?;
		let extension_data = data.slice(LEN_EXTENSION_DATA).map_err(ExtensionData)?;
		Ok(Extension{extension_type, extension_data})
	}
	fn __print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "{ext_ty}:<{ext_d}>", ext_ty=self.extension_type, ext_d=Hexdump(self.extension_data))
	}
}

unsafe impl<'a> IterableItem<'a> for Extension<'a>
{
	fn listclass() -> &'static str { "Extension" }
	unsafe fn read_item_unchecked(data: &mut Message<'a>) -> Extension<'a>
	{
		unsafe{Self::__read_unchecked(data)}
	}
	fn is_filtered(&self) -> bool { self.extension_type.is_grease() }
	fn print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		self.__print(f)
	}
}

impl<'a> IterableItemE for Extension<'a>
{
	const IS_FILTERED: bool = true;
	type WithStaticLifetime = Extension<'static>;
	type ReadError = ExtensionError;
	//Nothing uses thse.
	type LaunderedItem = crate::Void;
	type Kind = crate::Void;
}

unsafe impl<'a> IterableItem2<'a> for Extension<'a>
{
	fn read_item(data: &mut Message<'a>) -> Result<Extension<'a>, ExtensionError>
	{
		Self::__read(data)
	}
}
/*******************************************************************************************************************/
#[derive(Copy,Clone,Debug)]
pub(crate) enum CachedObjectError
{
	Type(PrimitiveReadError),
	HashValue(SliceReadError),
}

impl AlertForError for CachedObjectError
{
	fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::CachedObjectError::*;
		match self {
			&Type(ref e) => e.alert(),
			&HashValue(ref e) => e.alert(),
		}
	}
}

impl Display for CachedObjectError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::CachedObjectError::*;
		match self {
			&Type(ref err) => write!(f, "type: {err}"),
			&HashValue(ref err) => write!(f, "hash_value: {err}"),
		}
	}
}

///TLS Cached Object.
///
///TLS Cached Objects may be certificates or certificate requests. These can be used to save bandwidth by
///transmitting only hash of object that the peer already posesses. It corresponds to the `CachedObject` TLS
///structure.
///
///This type implements `IterableItem`, so it can be used with `GenericIterator`.
#[derive(Copy,Clone)]
pub struct CachedObject<'a>
{
	kind: CachedInformationType,
	hash: &'a [u8],
}

impl<'a> CachedObject<'a>
{
	///Get the type of cached information object.
	///
	///This corresponds to the `type` field of the `CachedObject` TLS structure.
	pub fn get_type(&self) -> CachedInformationType { self.kind }
	///Get the hash value of the cached information object.
	///
	/// * For cached information type `CachedInformationType::CERT`, this is `SHA-256` hash of the ceritifcate
	///message.
	/// * For cached information type `CachedInformationType::CERT_REQ`, this is `SHA-256` hash of the
	///certificate request message.
	///
	///This corresponds to the `hash_value` field of the `CachedObject` TLS structure.
	pub fn get_hash_value(&self) -> &'a [u8] { self.hash }

	unsafe fn __read_unchecked(data: &mut Message<'a>) -> CachedObject<'a>
	{
		// struct {
		//	 select (type) {
		//		 case client:
		//			 CachedInformationType type;
		//			 opaque hash_value<1..255>;
		//<...>
		//	 } body;
		// } CachedObject;
		//
		// - RFC7924 Section 3.
		let kind = unsafe{data.cp_unchecked()};
		let hash = unsafe{data.slice_unchecked(LEN_CACHED_OBJECT_HASH)};
		CachedObject{kind, hash}
	}
	fn __read(data: &mut Message<'a>) -> Result<CachedObject<'a>, CachedObjectError>
	{
		use self::CachedObjectError::*;
		// struct {
		//	 select (type) {
		//		 case client:
		//			 CachedInformationType type;
		//			 opaque hash_value<1..255>;
		//<...>
		//	 } body;
		// } CachedObject;
		//
		// - RFC7924 Section 3.
		let kind = data.cp().map_err(Type)?;
		let hash = data.slice(LEN_CACHED_OBJECT_HASH).map_err(HashValue)?;
		Ok(CachedObject{kind, hash})
	}
	fn __print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "{kind}:<{hash}>", kind=self.kind, hash=Hexdump(self.hash))
	}
}

unsafe impl<'a> IterableItem<'a> for CachedObject<'a>
{
	fn listclass() -> &'static str { "CachedObject" }
	unsafe fn read_item_unchecked(data: &mut Message<'a>) -> CachedObject<'a>
	{
		unsafe{Self::__read_unchecked(data)}
	}
	fn is_filtered(&self) -> bool { self.kind.is_grease() }
	fn print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		self.__print(f)
	}
}

impl<'a> IterableItemE for CachedObject<'a>
{
	const IS_FILTERED: bool = true;
	type WithStaticLifetime = CachedObject<'static>;
	type ReadError = CachedObjectError;
	//Nothing uses thse.
	type LaunderedItem = crate::Void;
	type Kind = crate::Void;
}

unsafe impl<'a> IterableItem2<'a> for CachedObject<'a>
{
	fn read_item(data: &mut Message<'a>) -> Result<CachedObject<'a>, CachedObjectError>
	{
		Self::__read(data)
	}
}
/*******************************************************************************************************************/
///Certificate Transparency v1 staple entry.
///
///This object is one entry for Certificate Transparency v1 (RFC6962). The objects are always Signed Certificate
///Timestamps. It corresponds to the `SerializedSCT` TLS structure.
///
///This type implements `IterableItem`, so it can be used with `GenericIterator`.
#[derive(Copy,Clone)]
pub struct SerializedSCT<'a>(&'a [u8]);

impl<'a> SerializedSCT<'a>
{
	///Get the serialized Signed Certificate Timestamp.
	pub fn get_serialized_sct(&self) -> &'a [u8] { self.0 }

	unsafe fn __read_unchecked(data: &mut Message<'a>) -> SerializedSCT<'a>
	{
		// opaque SerializedSCT<1..2^16-1>;
		//
		// - RFC6962 section 3.3
		SerializedSCT(unsafe{data.slice_unchecked(LEN_SERIALIZED_SCT)})
	}
	fn __read(data: &mut Message<'a>) -> Result<SerializedSCT<'a>, SliceReadError>
	{
		// opaque SerializedSCT<1..2^16-1>;
		//
		// - RFC6962 section 3.3
		data.slice(LEN_SERIALIZED_SCT).map(SerializedSCT)
	}
	fn __print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		if !print_ct_v1_item(self.0, f)? {
			write!(f, "<{h}>", h=Hexdump(self.0))?;
		}
		Ok(())
	}
}

impl<'a> IterableItemE for SerializedSCT<'a>
{
	type WithStaticLifetime = SerializedSCT<'static>;
	type ReadError = SliceReadError;
	//Nothing uses thse.
	type LaunderedItem = crate::Void;
	type Kind = crate::Void;
}

macro_rules! reject_err
{
	($x:expr) => { match $x { Ok(x) => x, Err(_) => return Ok(false) } }
}

fn print_ct_v1_item(item: &[u8], f: &mut core::fmt::Formatter) -> Result<bool, core::fmt::Error>
{
	let mut item = Message(item);
	let version: u8 = reject_err!(item.cp());
	if version != 0 { return Ok(false); }	//Bad version.
	let logid: [u8;32] = reject_err!(item.array());
	let timestamp: u64 = reject_err!(item.cp());
	let extensions = reject_err!(item.slice(LEN_SCT_V1_EXTENSIONS));
	let sigalgo: SignatureScheme = reject_err!(item.cp());
	let sigdata = reject_err!(item.slice(LEN_SCT_V1_SIGNATURE));
	if item.more() { return Ok(false); }
	write!(f, "[log=<{logid}>,timestamp={timestamp},extensions=<{extensions}>,sig={sigalgo}:<{sigdata}>]",
		logid=Hexdump(&logid), extensions=Hexdump(extensions), sigdata=Hexdump(sigdata))?;
	Ok(true)
}

wrap_iterator!(SerializedSCT SliceReadError);
/*******************************************************************************************************************/
///Certificate Transparency v2 staple entry.
///
///This object is one entry for Certificate Transparency v2 (RFC6962bis). The objects are Transparency Items.
///It corresponds to the `SerializedTransItem` TLS structure.
///
///This type implements `IterableItem`, so it can be used with `GenericIterator`.
#[derive(Copy,Clone)]
pub struct SerializedTransItem<'a>(&'a [u8]);

impl<'a> SerializedTransItem<'a>
{
	///Get the serialized Transparency Item.
	pub fn get_serialized_trans_item(&self) -> &'a [u8] { self.0 }

	unsafe fn __read_unchecked(data: &mut Message<'a>) -> SerializedTransItem<'a>
	{
		// opaque SerializedTransItem<1..2^16-1>;
		//
		// - RFC9162 section 6.3
		SerializedTransItem(unsafe{data.slice_unchecked(LEN_SERIALIZED_TRANS_ITEM)})
	}
	fn __read(data: &mut Message<'a>) -> Result<SerializedTransItem<'a>, SliceReadError>
	{
		// opaque SerializedTransItem<1..2^16-1>;
		//
		// - RFC9162 section 6.3
		data.slice(LEN_SERIALIZED_TRANS_ITEM).map(SerializedTransItem)
	}
	fn __print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		//FIXME: Prettier.
		write!(f, "<{h}>", h=Hexdump(self.0))
	}
}

impl<'a> IterableItemE for SerializedTransItem<'a>
{
	type WithStaticLifetime = SerializedTransItem<'static>;
	type ReadError = SliceReadError;
	//Nothing uses thse.
	type LaunderedItem = crate::Void;
	type Kind = crate::Void;
}

wrap_iterator!(SerializedTransItem SliceReadError);
/*******************************************************************************************************************/
///Connection ID
///
///This object represents TLS Connection ID. It corresponds to the `ConnectionId` TLS structure.
///
///This type implements `IterableItem`, so it can be used with `GenericIterator`.
#[derive(Copy,Clone)]
pub struct ConnectionId<'a>(&'a [u8]);

impl<'a> ConnectionId<'a>
{
	///Get the connection ID.
	pub fn get_cid(&self) -> &'a [u8] { self.0 }

	unsafe fn __read_unchecked(data: &mut Message<'a>) -> ConnectionId<'a>
	{
		// opaque ConnectionId<0..2^8-1>;
		//
		// - draft-ietf-tls-dtls13-43 section 9.
		ConnectionId(unsafe{data.slice_unchecked(LEN_CONNECTION_ID)})
	}
	fn __read(data: &mut Message<'a>) -> Result<ConnectionId<'a>, SliceReadError>
	{
		// opaque ConnectionId<0..2^8-1>;
		//
		// - draft-ietf-tls-dtls13-43 section 9.
		data.slice(LEN_CONNECTION_ID).map(ConnectionId)
	}
	fn __print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "<{h}>", h=Hexdump(self.0))
	}
}

impl<'a> IterableItemE for ConnectionId<'a>
{
	type WithStaticLifetime = ConnectionId<'static>;
	type ReadError = SliceReadError;
	//Nothing uses thse.
	type LaunderedItem = crate::Void;
	type Kind = crate::Void;
}

wrap_iterator!(ConnectionId SliceReadError);
/*******************************************************************************************************************/
#[derive(Copy,Clone,Debug)]
pub(crate) struct LaunderedAlpnProtocolId(u8,[u8;23]);

impl LaunderedAlpnProtocolId
{
	fn launder<'a>(x: AlpnProtocolId<'a>) -> LaunderedAlpnProtocolId
	{
		let mut y = [0;23];
		let x = x.get();
		if let Some((_, mut id)) = AlpnProtocolId::get_standard(x) {
			//Standard id. Encode specially.
			y[0] = 1;
			let mut i = 1;
			while i < y.len() && id > 0 {
				y[i] = id as u8;
				i += 1;
				id >>= 8;
			}
			LaunderedAlpnProtocolId(0, y)
		} else {
			if x.len() <= 23 {
				(&mut y[..x.len()]).copy_from_slice(x);
			} else if x.len() > 12 {	//Actually at least 24.
				(&mut y[..12]).copy_from_slice(&x[..12]);
				(&mut y[12..]).copy_from_slice(&x[x.len()-11..]);
			}
			LaunderedAlpnProtocolId(x.len() as u8, y)
		}
	}
}

fn print_binary_to_stream(y: &[u8],f: &mut Formatter) -> Result<(), FmtError>
{
	for &b in y.iter() {
		if b == b'\\' {
			write!(f, "\\\\")?;
		} if b >= 33 && b <= 126 {
			write!(f, "{c}", c=b as char)?;
		} else {
			write!(f, "\\x{b:02x}")?;
		}
	}
	Ok(())
}

impl Display for LaunderedAlpnProtocolId
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		let l = self.0 as usize;
		if l == 0 && self.1[0] == 1 {
			//One of those whacky standard ones.
			let mut id = 0usize;
			let mut idmul = 1usize;
			for &v in self.1.iter().skip(1) {
				id |= idmul * v as usize;
				idmul <<= 8;
			}
			//Assume this does not contain anything whacky.
			if let Some(entry) = AlpnProtocolId::standard_by_index(id) {
				write!(f, "{entry}")?;
			} else {
				write!(f, "<Bad standard #{id}>")?;
			}
		} else if l <= 23 {
			let y = &self.1[..l];
			if y.len() == 2 && y[0] == y[1] && y[0] % 16 == 10 {
				//This is GREASE.
				write!(f, "<GREASE_{x:x}>", x=y[0]/16)?;
				return Ok(());
			}
			print_binary_to_stream(y, f)?;
		} else {
			let (front, back) = self.1.split_at(12);
			let missing = l - 23;
			print_binary_to_stream(front, f)?;
			write!(f, "<...{missing}...>")?;
			print_binary_to_stream(back, f)?;
		}
		Ok(())
	}
}

impl PrintableItem for LaunderedAlpnProtocolId
{
	fn display_item(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "alpn_protocol_id {self}")
	}
}

unsafe impl<'a> IterableItem<'a> for AlpnProtocolId<'a>
{
	fn listclass() -> &'static str { "AlpnProtocolId" }
	unsafe fn read_item_unchecked(data: &mut Message<'a>) -> Self
	{
		//opaque ProtocolName<1..2^8-1>;
		//
		// - RFC7301 section 3.1.
		unsafe{AlpnProtocolId::new(data.slice_unchecked(LEN_PROTOCOL_NAME))}
	}
	fn is_filtered(&self) -> bool { self.is_grease() }
	fn print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "{self}")
	}
}

impl<'a> IterableItemE for AlpnProtocolId<'a>
{
	const IS_FILTERED: bool = true;
	type WithStaticLifetime = AlpnProtocolId<'static>;
	type LaunderedItem = LaunderedAlpnProtocolId;
	type ReadError = SliceReadError;
	type Kind = AlpnProtocolId<'a>;
	fn check_enabled<F:Fn(Self) -> bool>(&self, enabled: F) -> Result<(), LaunderedAlpnProtocolId>
	{
		fail_if!(self.is_grease() || !enabled(*self), LaunderedAlpnProtocolId::launder(*self));
		Ok(())
	}
}

unsafe impl<'a> IterableItem2<'a> for AlpnProtocolId<'a>
{
	fn read_item(data: &mut Message<'a>) -> Result<Self, SliceReadError>
	{
		//opaque ProtocolName<1..2^8-1>;
		//
		// - RFC7301 section 3.1.
		data.slice(LEN_PROTOCOL_NAME).map(AlpnProtocolId::new)
	}
}
/*******************************************************************************************************************/
#[derive(Copy,Clone,Debug)]
pub(crate) enum CertificateStatusRequestItemV2Error
{
	StatusType(PrimitiveReadError),
	Request(SliceReadError),
	RequestOcsp(OcspStatusRequestError),
	RequestOcspMulti(OcspStatusRequestError),
	JunkInRequest,
}

impl super::AlertForError for CertificateStatusRequestItemV2Error
{
	fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::CertificateStatusRequestItemV2Error::*;
		match self {
			&StatusType(ref e) => e.alert(),
			&Request(ref e) => e.alert(),
			&RequestOcsp(ref e) => e.alert(),
			&RequestOcspMulti(ref e) => e.alert(),
			&JunkInRequest => btls_aux_tls_iana::Alert::DECODE_ERROR,
		}
	}
}

impl Display for CertificateStatusRequestItemV2Error
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::CertificateStatusRequestItemV2Error::*;
		match self {
			&StatusType(ref err) => write!(f, "status_type: {err}"),
			&Request(ref err) => write!(f, "request: {err}"),
			&RequestOcsp(ref err) => write!(f, "OCSP request: {err}"),
			&RequestOcspMulti(ref err) => write!(f, "OCSP-multi request: {err}"),
			&JunkInRequest => write!(f, "Junk after end of request"),
		}
	}
}

///Version 2 Certificate Status Request item.
///
///This object is one certificate status request for V2 request. It corresponds to the
///`CertificateStatusRequestItemV2` TLS structure.
///
///This type implements `IterableItem`, so it can be used with `GenericIterator`.
#[derive(Copy,Clone)]
pub struct CertificateStatusRequestItemV2<'a>
{
	status_type: CertificateStatusType,
	request: &'a [u8],
	special: CSRv2Special<'a>,
}

#[derive(Copy,Clone)]
enum CSRv2Special<'a>
{
	Ocsp(OcspStatusRequest<'a>),
	OcspMulti(OcspStatusRequest<'a>),
	None
}

impl<'a> CertificateStatusRequestItemV2<'a>
{
	///Get the type of the certificate status being requested.
	///
	///This corresponds to the `status_type` field of the `CertificateStatusRequestItemV2` TLS structure.
	pub fn get_type(&self) -> CertificateStatusType { self.status_type }
	///Get the raw request of certificate status.
	///
	///This corresponds to the `request` field of the `CertificateStatusRequestItemV2` TLS structure.
	pub fn get_raw_request(&self) -> &'a [u8] { self.request }
	///Get the OCSP certificate status request.
	///
	///This succeeds if and only if `status_type == CertificateStatusType::OCSP`.
	pub fn get_ocsp_request(&self) -> Option<OcspStatusRequest<'a>>
	{
		if let CSRv2Special::Ocsp(x) = self.special { Some(x) } else { None }
	}
	///Get the OCSP multi certificate status request.
	///
	///This succeeds if and only if `status_type == CertificateStatusType::OCSP_MULTI`.
	pub fn get_ocsp_multi_request(&self) -> Option<OcspStatusRequest<'a>>
	{
		if let CSRv2Special::OcspMulti(x) = self.special { Some(x) } else { None }
	}
}

unsafe impl<'a> IterableItem<'a> for CertificateStatusRequestItemV2<'a>
{
	fn listclass() -> &'static str { "certificate_status_request_item_v2" }
	unsafe fn read_item_unchecked(data: &mut Message<'a>) -> CertificateStatusRequestItemV2<'a>
	{
		// struct {
		//	 CertificateStatusType status_type;
		//	 uint16 request_length; /* Length of request field in bytes */
		//	 select (status_type) {
		//		 case ocsp: OCSPStatusRequest;
		//		 case ocsp_multi: OCSPStatusRequest;
		//	 } request;
		// } CertificateStatusRequestItemV2;
		//
		// - RFC6961 section 2.2
		let status_type: CertificateStatusType = unsafe{data.cp_unchecked()};
		let mut req = unsafe{data.message_unchecked(LEN_CSRI2_REQUEST)};
		let request = req.as_inner();
		let special = match status_type {
			CertificateStatusType::OCSP => {
				let x = unsafe{OcspStatusRequest::read_unchecked(&mut req)};
				CSRv2Special::Ocsp(x)
			},
			CertificateStatusType::OCSP_MULTI=> {
				let x = unsafe{OcspStatusRequest::read_unchecked(&mut req)};
				CSRv2Special::OcspMulti(x)
			},
			_ => CSRv2Special::None
		};
		CertificateStatusRequestItemV2{status_type, request, special}
	}
	fn print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self.special {
			CSRv2Special::Ocsp(OcspStatusRequest{responders, extensions, ..}) =>
				write!(f, "ocsp{{respoders:{responders:?}, extensions:<{extensions}>}}",
					extensions=Hexdump(extensions)),
			CSRv2Special::OcspMulti(OcspStatusRequest{responders, extensions, ..}) =>
				write!(f,"ocsp_multi{{respoders:{responders:?}, extensions:<{extensions}>}}",
					extensions=Hexdump(extensions)),
			CSRv2Special::None => write!(f, "{stype}:{req}",
				stype=self.status_type, req=Hexdump(self.request)),
		}
	}
}

impl<'a> IterableItemE for CertificateStatusRequestItemV2<'a>
{
	type WithStaticLifetime = CertificateStatusRequestItemV2<'static>;
	type ReadError = CertificateStatusRequestItemV2Error;
	//Nothing uses thse.
	type LaunderedItem = crate::Void;
	type Kind = crate::Void;
}

unsafe impl<'a> IterableItem2<'a> for CertificateStatusRequestItemV2<'a>
{
	fn read_item(data: &mut Message<'a>) ->
		Result<CertificateStatusRequestItemV2<'a>, CertificateStatusRequestItemV2Error>
	{
		use self::CertificateStatusRequestItemV2Error::*;
		// struct {
		//	 CertificateStatusType status_type;
		//	 uint16 request_length; /* Length of request field in bytes */
		//	 select (status_type) {
		//		 case ocsp: OCSPStatusRequest;
		//		 case ocsp_multi: OCSPStatusRequest;
		//	 } request;
		// } CertificateStatusRequestItemV2;
		//
		// - RFC6961 section 2.2
		let status_type: CertificateStatusType = data.cp().map_err(StatusType)?;
		let mut req = data.message(LEN_CSRI2_REQUEST).map_err(Request)?;
		let request = req.as_inner();
		let special = match status_type {
			CertificateStatusType::OCSP => {
				let x = OcspStatusRequest::read(&mut req).map_err(RequestOcsp)?;
				req.assert_end(JunkInRequest)?;
				CSRv2Special::Ocsp(x)
			},
			CertificateStatusType::OCSP_MULTI=> {
				let x = OcspStatusRequest::read(&mut req).map_err(RequestOcspMulti)?;
				req.assert_end(JunkInRequest)?;
				CSRv2Special::OcspMulti(x)
			},
			_ => CSRv2Special::None
		};
		Ok(CertificateStatusRequestItemV2{status_type, request, special})
	}
}

#[test]
fn launder_standard_alpns()
{
	use crate::std::string::ToString;
	let mut idx = 0;
	while let Some(standard) = AlpnProtocolId::standard_by_index(idx) {
		idx += 1;
		let laundered = LaunderedAlpnProtocolId::launder(standard);
		let back = laundered.to_string();
		assert!(back.as_bytes() == standard.get());
	}
}

#[test]
fn launder_1()
{
	use crate::std::string::ToString;
	const INPUT: &[u8] = b"0";
	let laundered = LaunderedAlpnProtocolId::launder(AlpnProtocolId::new(INPUT));
	let back = laundered.to_string();
	assert!(back.as_bytes() == INPUT);
}

#[test]
fn launder_23()
{
	use crate::std::string::ToString;
	const INPUT: &[u8] = b"0123456789ABCDEFGHIJKLM";
	let laundered = LaunderedAlpnProtocolId::launder(AlpnProtocolId::new(INPUT));
	let back = laundered.to_string();
	assert!(back.as_bytes() == INPUT);
}

#[test]
fn launder_24()
{
	use crate::std::string::ToString;
	const INPUT: &[u8] = b"0123456789ABCDEFGHIJKLMN";
	const OUTPUT: &[u8] = b"0123456789AB<...1...>DEFGHIJKLMN";
	let laundered = LaunderedAlpnProtocolId::launder(AlpnProtocolId::new(INPUT));
	let back = laundered.to_string();
	assert_eq!(back.as_bytes(), OUTPUT);
}

#[test]
fn launder_36()
{
	use crate::std::string::ToString;
	const INPUT: &[u8] = b"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	const OUTPUT: &[u8] = b"0123456789AB<...13...>PQRSTUVWXYZ";
	let laundered = LaunderedAlpnProtocolId::launder(AlpnProtocolId::new(INPUT));
	let back = laundered.to_string();
	assert_eq!(back.as_bytes(), OUTPUT);
}
