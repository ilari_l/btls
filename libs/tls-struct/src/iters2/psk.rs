use super::GenericIterator;
use super::IntoGenericIterator;
use super::PskBinderEntry;
use super::PskIdentity;
use btls_aux_fail::fail_if;
use btls_aux_memory::Hexdump;
use btls_aux_memory::calculate_syndrome;

///Into `PreSharedKeyIter`.
///
///This type exists mainly to be turned into the corresponding iterator `PreSharedKeyIter<'a>` using the
///`iter()` method or the `IntoIterator` trait.
///
///Unlike the `PreSharedKeyIter` (as iterators should not be `Copy`), this type is `Copy`.
///
///This type is distinct from `IntoGenericIterator`, because the iterator type acts on two sources at the same time,
///which is something `GenericIterator` can not do. Also, since there is no filtering of `PreSharedKey` items,
///there is no `iter_all()` method, and only non-filtering `count()`.
#[derive(Copy,Clone)]
pub struct IntoPreSharedKeyIter<'a>
{
	identities: IntoGenericIterator<'a,PskIdentity<'a>>,
	binders: IntoGenericIterator<'a,PskBinderEntry<'a>>,
}

impl<'a> Default for IntoPreSharedKeyIter<'a>
{
	fn default() -> IntoPreSharedKeyIter<'a>
	{
		IntoPreSharedKeyIter {
			identities: IntoGenericIterator::default(),
			binders: IntoGenericIterator::default(),
		}
	}
}

impl<'a> IntoPreSharedKeyIter<'a>
{
	//The only possible error is PSK count mismatch.
	pub(crate) fn new(identities: IntoGenericIterator<'a,PskIdentity<'a>>,
		binders: IntoGenericIterator<'a,PskBinderEntry<'a>>) ->
		Result<IntoPreSharedKeyIter<'a>, ()>
	{
		fail_if!(identities.left_all != binders.left_all, ());
		Ok(IntoPreSharedKeyIter{
			identities: identities,
			binders: binders,
		})
	}
	///Count number of entries.
	///
	///This counts all entries that `iter()` returns.
	///
	///This operation is `O(1)` time.
	pub fn count(&self) -> usize { self.identities.left_filter }
	///Get iterator over `PreShareKey` entries.
	///
	///This iterator returns an entry for each offered PSK identity.
	pub fn iter(&self) -> PreSharedKeyIter<'a>
	{
		PreSharedKeyIter {
			identities: self.identities.iter(),
			binders: self.binders.iter(),
		}
	}
}

impl<'a> IntoIterator for IntoPreSharedKeyIter<'a>
{
	type Item = PreShareKey<'a>;
	type IntoIter = PreSharedKeyIter<'a>;
	fn into_iter(self) -> PreSharedKeyIter<'a> { self.iter() }
}


///Iterator over `PreShareKey` objects.
///
///This type is notable for implementing the `Iterator<Item=PreSharedKey<`a>>` trait.
///
///The `size_hint()` is always both accurate and precise. `Using size_hint().0` is preferred over `clone().count()`,
///because the first is faster due to not having to copy the structure.
///
///This is not `GenericIterator`, since the action of this iterator involves joint iteration of two data sources,
///which is something `GenericIterator` can not do.
#[derive(Clone)]
pub struct PreSharedKeyIter<'a>
{
	identities: GenericIterator<'a,PskIdentity<'a>>,
	binders: GenericIterator<'a,PskBinderEntry<'a>>,
}

impl<'a> core::fmt::Debug for PreSharedKeyIter<'a>
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		let mut cont = true;
		write!(f, "[")?;
		for PreShareKey{identity, obfuscated_age, binder} in self.clone() {
			if core::mem::replace(&mut cont, true) { write!(f, ", ")?; }
			write!(f, "[psk=<{identity}>,o_age={obfuscated_age:08x},binder=<{binder}>]",
				identity=Hexdump(identity), binder=Hexdump(binder))?;
		}
		write!(f, "]")?;
		Ok(())
	}
}

///Pre-Shared Key Entry.
///
///This structure contains identity, age information and binder for a pre-shared key. It corresponds to
///joint contents of `PskIdentity` and `PskBinderEntry` TLS structures.
///
///This does not implement `IterableItem`, use `PreSharedKeyIter` for iterator over these items.
#[derive(Copy,Clone)]
pub struct PreShareKey<'a>
{
	identity: &'a [u8],
	obfuscated_age: u32,
	binder: &'a [u8],
}

impl<'a> PreShareKey<'a>
{
	///Get the identity of Pre-Shared Key.
	pub fn get_identity(&self) -> &'a [u8] { self.identity }
	///Given the key protecting obfuscated_ticket_age, decrypt the age and return the result.
	pub fn get_age(&self, key: u32) -> u32 { self.obfuscated_age.wrapping_sub(key) }
	///Given binder for the Pre-Shared Key, check if the binder is correct.
	///
	///Returns `Ok(())` if the calclated binder matches, else returns `Err(())`.
	pub fn check_binder(&self, calculated: &[u8]) -> Result<(), ()>
	{
		let syndrome = calculate_syndrome(self.binder, calculated);
		if syndrome == 0 { Ok(()) } else { Err(()) }
	}
}

impl<'a> Iterator for PreSharedKeyIter<'a>
{
	type Item = PreShareKey<'a>;
	fn next(&mut self) -> Option<PreShareKey<'a>>
	{
		let identity = self.identities.next()?;
		let binder = self.binders.next()?;
		Some(PreShareKey {
			identity: identity.identity,
			obfuscated_age: identity.obfuscated_ticket_age,
			binder: binder.0,
		})
	}
	fn count(self) -> usize { self.identities.count() }
	fn size_hint(&self) -> (usize, Option<usize>) { self.identities.size_hint() }
}
