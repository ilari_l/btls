use super::IterableItem;
use super::IterableItem2;
use super::IterableItemE;
use super::Message;
use super::PrintableItem;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::ResultExt;
use btls_aux_tls_iana::Alert;
use btls_aux_tls_iana::CipherSuite;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;

#[derive(Copy,Clone,Debug)]
pub(crate) struct CipherSuiteError(Option<CipherSuite>);

impl Display for CipherSuiteError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match &self.0 {
			&Some(ref err) => write!(f, "Illegal {kind} {err}", kind=CipherSuite::kind()),
			&None => Display::fmt(&super::Trivial, f)
		}
	}
}

impl super::AlertForError for CipherSuiteError
{
	fn alert(&self) -> Alert
	{
		match &self.0 {
			&Some(_) => Alert::ILLEGAL_PARAMETER,
			//This should never ever happen.
			&None => Alert::INTERNAL_ERROR,
		}
	}
}

impl PrintableItem for CipherSuite
{
	fn display_item(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "{kind} {self}", kind=Self::kind())
	}
}

unsafe impl<'a> IterableItem<'a> for CipherSuite
{
	fn listclass() -> &'static str { Self::kind() }
	unsafe fn read_item_unchecked(data: &mut Message<'a>) -> Self
	{
		unsafe{data.cp_unchecked()}
	}
	fn is_filtered(&self) -> bool
	{
		match *self {
			CipherSuite::EMPTY_RENEGOTIATION_INFO_SCSV => true,
			CipherSuite::FALLBACK_SCSV => true,
			i => i.is_grease()
		}
	}
	fn print(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "{self}")
	}
}

impl IterableItemE for CipherSuite
{
	const ITEM_SIZE: usize = 2;
	const IS_FILTERED: bool = true;
	const NONTRIVIAL_CHECK: bool = true;
	type WithStaticLifetime = CipherSuite;		//No lifetimes involved.
	type LaunderedItem = CipherSuite;
	type ReadError = CipherSuiteError;
	type Kind = CipherSuite;
	fn check_item(&self) -> Result<(), CipherSuiteError>
	{
		match *self {
			item@CipherSuite::NULL_WITH_NULL_NULL => fail!(CipherSuiteError(Some(item))),
			_ => Ok(())
		}
	}
	fn check_enabled<F:Fn(Self) -> bool>(&self, enabled: F) -> Result<(), Self>
	{
		//GREASE/SCSV codepoints are never valid alone.
		fail_if!(Self::is_filtered(self) || !enabled(*self), *self);
		Ok(())
	}
}

unsafe impl<'a> IterableItem2<'a> for CipherSuite
{
	fn read_item(data: &mut Message<'a>) -> Result<Self, CipherSuiteError>
	{
		//Should never call this!
		let x: CipherSuite = data.cp().set_err(CipherSuiteError(None))?;
		x.check_item()?;
		Ok(x)
	}
}

