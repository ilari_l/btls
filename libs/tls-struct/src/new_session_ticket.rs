use crate::DbgT;
use crate::ExtensionError;
use crate::LEN_EXTENSION_LIST;
use crate::LEN_TICKET;
use crate::LEN_TICKET_NONCE;
use crate::extensions as exts;
use crate::iters2::AlertForError;
use crate::iters2::Extension;
use crate::iters2::IntoGenericIterator as ListOf;
use crate::iters2::Message;
use btls_aux_tls_iana::ExtensionType as IanaExtension;

macro_rules! field
{
	(E $location:ident $x:expr) => {
		$x.map_err(crate::errors::Struct::$location).map_err(ExtensionError::Structured)?
	};
	(S $location:ident $x:expr) => {
		$x.map_err(crate::errors::Struct::$location).map_err(_NewSessionTicketParseError::Structured)?
	};
}

macro_rules! extension
{
	($exts2:ident $dbg:ident $name:ident $handler:expr) => {
		$exts2.with_stdext(IanaExtension::$name, _NewSessionTicketParseError::JunkAfterExtension,
			_NewSessionTicketParseError::Extension, $dbg, $handler)?
	}
}

///Error from parsing New Session Ticket.
///
///The most notable things to query errors are the `alert()` method and the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct NewSessionTicketParseError(_NewSessionTicketParseError);

#[derive(Copy,Clone,Debug)]
enum _NewSessionTicketParseError
{
	Structured(crate::errors::Struct),
	JunkAfterExtensions,
	Extension(IanaExtension, ExtensionError),
	JunkAfterExtension(IanaExtension),
}

impl NewSessionTicketParseError
{
	///Get the alert code one should use for this parse error.
	pub fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::_NewSessionTicketParseError::*;
		use btls_aux_tls_iana::Alert as A;
		match self.0 {
			Structured(ref g) => g.alert(),
			JunkAfterExtensions => A::DECODE_ERROR,
			Extension(_,g) => g.alert(),
			JunkAfterExtension(_) => A::DECODE_ERROR,
		}
	}
}

impl core::fmt::Display for NewSessionTicketParseError
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		use self::_NewSessionTicketParseError::*;
		match &self.0 {
			&Structured(ref err) => core::fmt::Display::fmt(err, f),
			&JunkAfterExtensions => write!(f, "Junk after end of extensions"),
			&Extension(ext, ref err) => write!(f, "extension {ext}: {err}"),
			JunkAfterExtension(ext) => write!(f, "Junk after end of extension {ext}"),
		}
	}
}

///Parsed TLS 1.3 New Session Ticket message.
#[derive(Clone)]
pub struct NewSessionTicket<'a>
{
	extension_mask: [u32;exts::EXTENSION_WORDS],
	ticket_lifetime: u32,
	ticket_age_add: u32,
	ticket_nonce: &'a [u8],
	ticket: &'a [u8],
	extensions: ListOf<'a, Extension<'a>>,
	max_early_data_size: u32,
}

impl<'a> NewSessionTicket<'a>
{
	///Get the ticket lifetime in seconds.
	///
	///The lifetime MUST NOT exceed 604800 without special profile.
	pub fn get_lifetime(&self) -> u32 { self.ticket_lifetime }
	///Get ticket age encryption key.
	pub fn get_age_key(&self) -> u32 { self.ticket_age_add }
	///Get ticket nonce.
	pub fn get_nonce(&self) -> &'a [u8] { self.ticket_nonce }
	///Get the ticket.
	pub fn get_ticket(&self) -> &'a [u8] { self.ticket }
	///Get the maximum amount of early data allowed.
	///
	///0 means early data is not allowed at all.
	pub fn get_max_early_data(&self) -> u32 { self.max_early_data_size }
	///Get extensions.
	///
	///This can be used to read extensions other than the supported ones.
	pub fn get_extensions(&self) -> ListOf<'a, Extension<'a>> { self.extensions }
	///Was specified extension sent?
	pub fn has_extension(&self, ext: IanaExtension) -> bool
	{
		match exts::extension_bit(ext) {
			Some(bit) => {
				if let Some(rw) = self.extension_mask.get(bit / 32) {
					*rw & 1u32 << bit % 32 != 0
				} else {
					false
				}
			},
			None => self.extensions.iter_all().any(|e|{
				e.extension_type == ext
			})
		}
	}
	///Parse New Session Tticket to its component parts.
	pub fn parse(msg: &'a [u8], mut dbg: DbgT) -> Result<NewSessionTicket<'a>, NewSessionTicketParseError>
	{
		parse_nst(Message::new(msg), &mut dbg).map_err(NewSessionTicketParseError)
	}
}

fn parse_nst<'a>(mut msg: Message<'a>, dbg: &mut DbgT) -> Result<NewSessionTicket<'a>, _NewSessionTicketParseError>
{
	// struct {
	//	 uint32 ticket_lifetime;
	//	 uint32 ticket_age_add;
	//	 opaque ticket_nonce<0..255>;
	//	 opaque ticket<1..2^16-1>;
	//	 Extension extensions<0..2^16-2>;
	// } NewSessionTicket;
	//
	// - RFC8446 section 4.6.1.
	use self::_NewSessionTicketParseError::*;
	let ticket_lifetime: u32 = field!(S TicketLifetime msg.cp());
	let ticket_age_add: u32 = field!(S TicketAgeAdd msg.cp());
	let ticket_nonce = field!(S TicketNonce msg.slice(LEN_TICKET_NONCE));
	let ticket = field!(S Ticket2 msg.slice(LEN_TICKET));
	let extensions = field!(S Extensions msg.list_of::<crate::iters2::Extension<'a>>(LEN_EXTENSION_LIST));
	msg.assert_end(JunkAfterExtensions)?;

	let exts2 = field!(S Extensions2
		exts::check_extensions_block(extensions, IanaExtension::allowed_in_new_session_ticket)
	);

	let max_early_data_size = extension!(exts2 dbg EARLY_DATA |extn, extv, dbg|{
		let max_size: u32 = field!(E MaxEarlyDataSize extv.cp());
		debug!(E dbg extn "Maximum early data size {max_size}");
		Ok(max_size)
	}).unwrap_or(0);

	exts::report_unknown_extensions(extensions, dbg);

	Ok(NewSessionTicket{
		extension_mask: exts2.get_mask(),
		ticket_lifetime,
		ticket_age_add,
		ticket_nonce,
		ticket,
		extensions,
		max_early_data_size,
	})
}
