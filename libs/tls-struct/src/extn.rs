use crate::DbgT;
use crate::ExtensionError;
use crate::iters2::GenericIteratorFactory;
use crate::iters2::IntoGenericIterator;
use crate::iters2::ServerName;
use btls_aux_tls_iana::ExtensionType;

macro_rules! field
{
	($name:ident $e:expr) => {
		$e.map_err(crate::errors::Struct::$name).map_err(ExtensionError::Structured)?
	}
}

pub(crate) fn clientext_server_name<'a>(extv: &mut &'a [u8], extn: ExtensionType, dbg: &mut DbgT) ->
	Result<(Option<&'a str>, IntoGenericIterator<'a, ServerName<'a>>), ExtensionError>
{
	let (snl, host_name) = field!(ServerNameList GenericIteratorFactory::read_server_name_list(extv));
	if let Some(host_name) = host_name { debug!(E dbg extn "Host name is {host_name}"); }
	if snl.iter().count() > 0 { debug!(E dbg extn "Other names are {snli:?}", snli=snl.iter()); }
	Ok((host_name, snl))
}
