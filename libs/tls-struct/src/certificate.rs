use crate::DbgT;
use crate::ExtensionError;
use crate::LEN_CERTIFICATE_LIST;
use crate::LEN_CERTIFICATE_REQUEST_CONTEXT;
use crate::LEN_DC_SPKI;
use crate::LEN_OPENPGP_CERTIFICATE;
use crate::LEN_OPENPGP_FINGERPRINT;
use crate::LEN_OPENPGP_SUBKEY_ID;
use crate::LEN_SIGNATURE;
use crate::RemoteFeatureSet;
//use crate::ExtensionError;
//use btls_aux_tls_iana::ExtensionType as IanaExtension;
use crate::extensions::DnssecChain;
use crate::iters2::AlertForError;
use crate::iters2::CertificateEntry;
use crate::iters2::CertificateEntry2;
use crate::iters2::CertificateStatus;
use crate::errors::OpenPgpDescriptor;
use crate::errors::OpenPgpEmptyCert;
use crate::errors::Struct as EStruct;
use crate::iters2::Extension;
use crate::iters2::IntoGenericIterator as ListOf;
use crate::iters2::ItemError;
use crate::iters2::Message;
use crate::iters2::PrimitiveReadError;
use crate::iters2::SerializedSCT;
use crate::iters2::SerializedTransItem;
use crate::iters2::SliceReadError;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::fail_if_none;
use btls_aux_tls_iana::CertificateStatusType;
use btls_aux_tls_iana::ExtensionType;
use btls_aux_tls_iana::SignatureScheme;
use btls_aux_tls_iana::helper::U24;
use btls_aux_tls_iana::helper::XRead;
use core::marker::PhantomData;
use core::ops::Range;



macro_rules! field
{
	(S $location:ident $x:expr) => {
		$x.map_err(EStruct::$location)?
	};
	(E $location:ident $x:expr) => {
		$x.map_err(EStruct::$location).map_err(ExtensionError::Structured)?
	};
	($clazz:ident :: $location:ident $x:expr) => {
		$x.map_err(EStruct::$location).map_err($clazz::Structured)?
	};
}

///Error from parsing Certificate.
///
///The most notable things to query errors are the `alert()` method and the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct CertificateParseError(_CertificateParseError);

#[derive(Copy,Clone,Debug)]
enum _CertificateParseError
{
	Structured(crate::errors::Struct),
	Extension(usize, ExtensionType, ExtensionError),
	IllegalChainExtension(usize,ExtensionType),
	UnsupportedStatus(usize,CertificateStatusType),
	RpkChainNotEmpty,
	JunkAfterEnd,
}

impl CertificateParseError
{
	///Get the alert code one should use for this parse error.
	pub fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::_CertificateParseError::*;
		use btls_aux_tls_iana::Alert as A;
		match self.0 {
			Structured(ref g) => g.alert(),
			Extension(_,_,ref g) => g.alert(),
			IllegalChainExtension(_,_) => A::UNSUPPORTED_EXTENSION,
			UnsupportedStatus(_,_) => A::ILLEGAL_PARAMETER,
			RpkChainNotEmpty => A::DECODE_ERROR,
			JunkAfterEnd => A::DECODE_ERROR,
		}
	}
}

impl core::fmt::Display for CertificateParseError
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		use self::_CertificateParseError::*;
		match &self.0 {
			&Structured(ref e) => core::fmt::Display::fmt(e, f),
			&Extension(idx, ext, ref err) =>
				write!(f, "certificate_list[{idx}].extensions: Extension {ext}: {err}"),
			&IllegalChainExtension(idx, ext) =>
				write!(f, "certificate_list[{idx}].extensions: Illegal extension {ext}"),
			&UnsupportedStatus(idx, stype) =>
				write!(f, "certificate_list[{idx}].extensions: Unsupported status type {stype}"),
			&RpkChainNotEmpty => write!(f, "Only one entry allowed for raw public key"),
			&JunkAfterEnd => write!(f, "Junk after end of certificate message"),
		}
	}
}

///TLS 1.2 X.509 Certificate.
///
///This corresponds to the `Certificate` TLS structure.
#[derive(Clone)]
pub struct CertificateTls12X509<'a>
{
	entity: &'a [u8],
	issuers: ListOf<'a,CertificateEntry<'a>>,
	x: PhantomData<&'a u8>
}

impl<'a> CertificateTls12X509<'a>
{
	///Get the end-entity certificate.
	///
	///This gives the first certificate in the certificate list.
	pub fn get_end_entity(&self) -> &'a [u8] { self.entity }
	///Get the issuer chain.
	///
	///This gives the remaining certificates in the certificate chain, each being signed by the next. This
	///list may be empty.
	pub fn get_issuer_chain(&self) -> ListOf<'a,CertificateEntry<'a>> { self.issuers }
	///Parse TLS 1.2 X.509 Certificate message.
	///
	///If the parsed certificate chain is empty (refusal to authenticate), returns `Ok(None)`. This happens
	///if server sends `CertificateRequest`, but the client has no certificate to send. In this case, the
	///handshake state machine jumps straight to client Finished, instead of awaiting client CertificateVerify
	///next.
	pub fn parse(msg: &'a [u8], dbg: &mut DbgT) ->
		Result<Option<CertificateTls12X509<'a>>, CertificateParseError>
	{
		Self::_parse(Message::new(msg), dbg).map_err(CertificateParseError)
	}
	fn _parse(mut msg: Message<'a>, dbg: &mut DbgT) ->
		Result<Option<CertificateTls12X509<'a>>, _CertificateParseError>
	{
		use self::_CertificateParseError as CTPE;
		// struct {
		//	 ASN.1Cert certificate_list<0..2^24-1>;
		// } Certificate;
		//
		// - RFC5246 section 7.4.2.
		let mut list = field!(CTPE::CertificateList msg.list_of::<CertificateEntry<'a>>(LEN_CERTIFICATE_LIST));
		msg.assert_end(CTPE::JunkAfterEnd)?;
		let ee_cert = list.split_first();
		Ok(if let Some(ee_cert) = ee_cert {
			debug!(dbg "Received X.509 certificate with {icnt} issuers", icnt=list.count_all(true));
			//Nontrivial chain. Extract the first element of the chain.
			Some(CertificateTls12X509{
				entity: ee_cert.get_certificate(),
				issuers: list,
				x: PhantomData
			})
		} else {
			//Rejected authentication.
			debug!(dbg "Authentication rejected");
			None
		})
	}
}

//This can be enumeration because it is technically infeasible to extend this nor drill in to the fields more.
///TLS 1.2 OpenPGP Certificate.
///
///This corresponds to the `Certificate` TLS structure.
#[derive(Clone)]
pub enum CertificateTls12OpenPgp<'a>
{
	///The peer refused authentication. This is only possible for the client, it is illegal for the server
	///to send this.
	Empty,
	///Certificate.
	///
	///The certificate is in binary OpenPGP format.
	Certificate{subkey_id: &'a [u8], certificate: &'a [u8]},
	///Certificate fingerprint.
	///
	///Instead of certificate, a fingerprint is sent, leaving it peer's problem to find the certificate
	///(or fail if it can not).
	Fingerprint{subkey_id: &'a [u8], fingerprint: &'a [u8]},
}

const OPENPGP_EMPTY: u8 = 1;
const OPENPGP_CERTIFICATE: u8 = 2;
const OPENPGP_FINGERPRINT: u8 = 3;

impl<'a> CertificateTls12OpenPgp<'a>
{
	///Parse TLS 1.2 OpenPGP Certificate message.
	///
	///If the parsed certificate chain is empty (refusal to authenticate), returns
	///`Ok(CertificateTls12OpenPgp::Empty)`. This happens if server sends `CertificateRequest`, but the client
	///has no certificate to send. In this case, the handshake state machine jumps straight to client Finished,
	///instead of awaiting client CertificateVerify next.
	pub fn parse(msg: &'a [u8], dbg: &mut DbgT) -> Result<CertificateTls12OpenPgp<'a>, CertificateParseError>
	{
		Self::_parse(Message::new(msg), dbg).map_err(CertificateParseError)
	}
	fn _parse(mut msg: Message<'a>, dbg: &mut DbgT) -> Result<CertificateTls12OpenPgp<'a>, _CertificateParseError>
	{
		use self::_CertificateParseError as CTPE;
		let descriptor: u8 = field!(CTPE::DescriptorType msg.cp().map_err(OpenPgpDescriptor::Read));
		let r = match descriptor {
			OPENPGP_EMPTY => {
				let v: U24 = field!(CTPE::EmptyCert msg.cp().map_err(OpenPgpEmptyCert::Read));
				let v = v.__cast_usize() as u32;	//Hack to get value.
				fail_if!(v != 0, CTPE::Structured(EStruct::EmptyCert(
					OpenPgpEmptyCert::BadValue(v))));
				debug!(dbg "Authentication rejected");
				CertificateTls12OpenPgp::Empty
			},
			OPENPGP_CERTIFICATE => {
				let subkey_id = field!(CTPE::OpenPgpKeyId msg.slice(LEN_OPENPGP_SUBKEY_ID));
				let certificate = field!(CTPE::OpenPgpCert msg.slice(LEN_OPENPGP_CERTIFICATE));
				debug!(dbg "Received OpenPGP certificate");
				CertificateTls12OpenPgp::Certificate{subkey_id, certificate}
			},
			OPENPGP_FINGERPRINT => {
				let subkey_id = field!(CTPE::OpenPgpFpKeyId msg.slice(LEN_OPENPGP_SUBKEY_ID));
				let fingerprint = field!(CTPE::OpenPgpFpCert msg.slice(LEN_OPENPGP_FINGERPRINT));
				debug!(dbg "Received OpenPGP certificate fingerprint");
				CertificateTls12OpenPgp::Fingerprint{subkey_id, fingerprint}
			},
			c => fail!(CTPE::Structured(EStruct::DescriptorType(OpenPgpDescriptor::BadValue(c)))),
		};
		msg.assert_end(_CertificateParseError::JunkAfterEnd)?;
		Ok(r)
	}
}

///TLS 1.2 Raw Public Key.
///
///This corresponds to the `Certificate` TLS structure.
#[derive(Clone)]
pub struct CertificateTls12Rpk<'a>
{
	key: &'a [u8],
	x: PhantomData<&'a u8>
}

impl<'a> CertificateTls12Rpk<'a>
{
	///Get the public key in X.509 SubjectPublicKeyInfo format with DER encoding.
	pub fn get_public_key(&self) -> &'a [u8] { self.key }
	///Parse TLS 1.2 Raw Public Key Certificate message.
	///
	///If the parsed certificate chain is empty (refusal to authenticate), returns `Ok(None)`. This happens
	///if server sends `CertificateRequest`, but the client has no public key to send. In this case, the
	///handshake state machine jumps straight to client Finished, instead of awaiting client CertificateVerify
	///next.
	pub fn parse(msg: &'a [u8], dbg: &mut DbgT) -> Result<Option<CertificateTls12Rpk<'a>>, CertificateParseError>
	{
		Self::_parse(Message::new(msg), dbg).map_err(CertificateParseError)
	}
	fn _parse(mut msg: Message<'a>, dbg: &mut DbgT) ->
		Result<Option<CertificateTls12Rpk<'a>>, _CertificateParseError>
	{
		use self::_CertificateParseError as CTPE;
		//HACK: Use LEN_CERTIFICATE_LIST instead of LEN_CERTIFICATE_ENTRY, as that also allows 0-length
		//entry, which presumably signals refusal to authenticate.
		let cert = field!(CTPE::SubjectPublicKeyInfo msg.slice(LEN_CERTIFICATE_LIST));
		msg.assert_end(_CertificateParseError::JunkAfterEnd)?;
		//This is not quite up to spec, but interpret 0-length key as refusal.
		Ok(if cert.len() > 0 {
			debug!(dbg "Received Raw public key");
			Some(CertificateTls12Rpk {
				key: cert,
				x: PhantomData
			})
		} else {
			debug!(dbg "Authentication rejected");
			None
		})
	}
}

fn generic_parse_tls13_cert<'a>(mut msg: Message<'a>, enable: RemoteFeatureSet) ->
	Result<(&'a [u8], Option<(CertificateEntry2<'a>, ListOf<'a,CertificateEntry2<'a>>)>),
	_CertificateParseError>
{
	use self::_CertificateParseError as CTPE;
	// struct {
	//	 opaque certificate_request_context<0..2^8-1>;
	//	 CertificateEntry certificate_list<0..2^24-1>;
	// } Certificate;
	//
	// -RFC8446 section 4.4.2.
	let reqctx = field!(CTPE::CertificateRequestContext msg.slice(LEN_CERTIFICATE_REQUEST_CONTEXT));
	let mut certlist = field!(CTPE::CertificateList2 msg.list_of::<CertificateEntry2<'a>>(LEN_CERTIFICATE_LIST));
	msg.assert_end(CTPE::JunkAfterEnd)?;
	//ListOf<CertificateEntry2> does not check if the unknown extensions are valid. Do that here. It does
	//duplicate extension check tho.
	for (index, chain_entry) in certlist.iter().enumerate() {
		for Extension{extension_type, ..} in chain_entry.get_extensions() {
			fail_if!(!extension_type.allowed_in_certificate() ||
				!crate::ask_extension(enable, extension_type),
				CTPE::IllegalChainExtension(index, extension_type));
		}
	}
	//Split out the entity certificate.
	let ee_cert = certlist.split_first();
	//The handling of extensions dnssec_chain and delegated_credentials is special due to those extensions
	//only being allowed in the end entity certificate. Check those extensions are not present in chain
	//certificates. If ee_cert = None, the list is empty, so this does nothing.
	for (index, chain_entry) in certlist.iter().enumerate() {
		for Extension{extension_type, ..} in chain_entry.get_extensions() { match extension_type {
			ExtensionType::DELEGATED_CREDENTIALS|ExtensionType::DNSSEC_CHAIN => {
				//The indices are off by one, since ee cert has been removed.
				fail!(CTPE::IllegalChainExtension(index + 1, extension_type));
			},
			_ => ()
		}}
	}
	Ok((reqctx, ee_cert.map(|e|(e, certlist))))
}

fn print_entity_info(ent: &CertificateEntry2, index: usize, print_as: core::fmt::Arguments,
	enable: RemoteFeatureSet, dbg: &mut DbgT) -> Result<(), _CertificateParseError>
{
	use self::_CertificateParseError as CTPE;
	let ct1cnt = ent.get_signed_certificate_timestamps().count_all(true);
	let ct2cnt = ent.get_transparency_items().count_all(true);
	if let Some(s) = ent.get_status() {
		//ocsp_multi is not allowed in TLS 1.3.
		fail_if!(!crate::ask_certificate_status_type(enable, s.get_status_type()) ||
			s.get_status_type() == CertificateStatusType::OCSP_MULTI,
			CTPE::UnsupportedStatus(index, s.get_status_type()));
		//No need to query as_ocsp_multi(), as above check banned ocsp_multi responses.
		if let Some(s) = s.as_ocsp() {
			debug!(dbg "{print_as}: Received OCSP staple ({slen} bytes)",
				slen=s.get_response().len());
		} else {
			debug!(dbg "{print_as}: Received {stype} staple ({slen} bytes)",
				stype=s.get_status_type(), slen=s.get_raw_response().len());
		}
	}
	if ct1cnt > 0 {
		debug!(dbg "{print_as}: Received {ct1cnt} certificate transparency v1 staples");
	}
	if ct2cnt > 0 {
		debug!(dbg "{print_as}: Received {ct2cnt} certificate transparency v2 staples");
	}
	Ok(())
}

fn print_chain_info<'a>(entity: &CertificateEntry2<'a>, chain: ListOf<'a,CertificateEntry2<'a>>,
	enable: RemoteFeatureSet, dbg: &mut DbgT) -> Result<(), _CertificateParseError>
{
	print_entity_info(&entity, 0, format_args!("Entity certificate"), enable, dbg)?;
	for (i, entity2) in chain.iter().enumerate() {
		let idx = i + 1;
		print_entity_info(&entity2, idx, format_args!("Issuer #{idx}"), enable, dbg)?;
	}
	Ok(())
}

fn do_extensions_block<'a>(entity: &CertificateEntry2<'a>, index: usize,
	mut f: impl FnMut(ExtensionType, &mut Message<'a>) -> Result<(), ExtensionError>) ->
	Result<(), _CertificateParseError>
{
	use self::_CertificateParseError as CTPE;
	for Extension{extension_type, extension_data, ..} in entity.get_extensions() {
		let mut extension_data = Message::new(extension_data);
		f(extension_type, &mut extension_data).map_err(|e|{
			CTPE::Extension(index, extension_type, e)
		})?;
		extension_data.assert_end(CTPE::Extension(index, extension_type, ExtensionError::JunkAfterExtension))?;
	}
	Ok(())
}

///Maybe TLS 1.3 certificate.
///
///This structure contains certificate request context and possibly a certificate of given subtype.
///
///This corresponds to the `Certificate` TLS structure.
#[derive(Clone)]
pub struct MaybeTls13Certificate<'a,T:Clone+'a>
{
	context: &'a [u8],
	certificate: Option<T>,
}

impl<'a,T:Clone+'a> MaybeTls13Certificate<'a,T>
{
	///Get the request context.
	///
	///The context is always empty when certificate message is sent in handshake.
	pub fn get_request_context(&self) -> &'a [u8] { self.context }
	///Get the certificate chain.
	///
	///If peer refused to authenticate, returns `None`. This can happen if the server sends `CertificateRequest`,
	///but the client has no certificate to send. In this case, the handshake state machine jumps to client
	///Finished, instead of advancing to client CertificateVerify.
	pub fn get_certificate_chain(&self) -> Option<T> { self.certificate.clone() }
}

///TLS 1.3 X.509 certificate.
///
///This corresponds to the `certificate_list` field in the `Certificate` TLS structure.
#[derive(Clone)]
pub struct CertificateTls13X509<'a>
{
	entity: &'a [u8],
	entity_extensions: ListOf<'a,Extension<'a>>,
	certificate_status: Option<CertificateStatus<'a>>,
	dnssec_chain: Option<DnssecChain<'a>>,
	delegated_credentials: Option<DelegatedCredentials<'a>>,
	scts: ListOf<'a,SerializedSCT<'a>>,
	trans_items: ListOf<'a,SerializedTransItem<'a>>,
	issuers: ListOf<'a,CertificateEntry2<'a>>,
	x: PhantomData<&'a u8>
}

impl<'a> CertificateTls13X509<'a>
{
	///Get the end-entity Certificate.
	pub fn get_end_entity(&self) -> &'a [u8] { self.entity }
	///Get the issuer chain.
	///
	///This is all certificates in the chain except the first.
	pub fn get_issuer_chain(&self) -> ListOf<'a,CertificateEntry2<'a>> { self.issuers }
	///Get the end-entity Certificate extensions.
	///
	///This corresponds to the `extensions` field of the TLS `CertificateEntry` structure in the end-entity
	///certificate.
	///
	///The extensions of other certificates in chain can be accessed via the certificate entries.
	pub fn get_extensions(&self) -> ListOf<'a,Extension<'a>> { self.entity_extensions }
	///Get stapled certificate status response, if any.
	pub fn get_status(&self) -> Option<CertificateStatus<'a>> { self.certificate_status }
	///Get the DNSSec chain certifying the certificate, if any.
	pub fn get_dnssec_chain(&self) -> Option<DnssecChain<'a>> { self.dnssec_chain }
	///Get list of stapled Certificate transparency v1 Signed Certificate Timestamps for the end-entity
	///certificate.
	///
	///This corresponds to `signed_certificate_timestamp` extension on the end-endity certificate.
	///
	///The corresponding Signed Certificate Timetamps of other certificates can be accessed via certificate
	///entries for those certificates.
	pub fn get_signed_certificate_timestamps(&self) -> ListOf<'a,SerializedSCT<'a>> { self.scts }
	///Get list of stapled Certificate transparency v2 Transparency Items for the end-entity certificate.
	///
	///This corresponds to `transparency_info` extension on the end-endity certificate.
	///
	///The corresponding Transparency Items of other certificates can be accessed via certificate entries
	///for those certificates.
	pub fn get_transparency_items(&self) -> ListOf<'a,SerializedTransItem<'a>> { self.trans_items }
	///Get the Delegated Credential, if any.
	pub fn get_delegated_credential(&self) -> Option<DelegatedCredentials<'a>>
	{
		self.delegated_credentials.clone()
	}
	///Parse TLS 1.3 X.509 Certificate message.
	///
	///The parameter `enable` controls what TLS features are enabled. This is used to enforce the TLS
	///restriction that answers can not contain algorithms or extensions not present in offers. For server
	///certificate, the corresponding offer is Client Hello, and for client certificate, the corresponding
	///offer is Certificate Request. The following features may be requested:
	///
	/// * `Extension(STATUS_REQUEST)`
	///   * Together with `CertificateStatusType(type)`, where `type` is the type of response.
	/// * `Extension(SIGNED_CERTIFICATE_TIMESTAMP`
	/// * `Extension(DELEGATED_CREDENTIALS)`
	///   * Together with `DelegatedCredential(algo)`, where `algo` is the expected certificate verify
	///algorithm.
	/// * `Extension(TRANSPARENCY_INFO)`
	/// * `Extension(DNSSEC_CHAIN)`
	/// * `Extension(foo)` for some other not currently defined extension `foo`.
	pub fn parse(msg: &'a [u8], enable: RemoteFeatureSet, dbg: &mut DbgT) ->
		Result<MaybeTls13Certificate<'a,CertificateTls13X509<'a>>, CertificateParseError>
	{
		Self::_parse(Message::new(msg), enable, dbg).map_err(CertificateParseError)
	}
	fn _parse(msg: Message<'a>, enable: RemoteFeatureSet, dbg: &mut DbgT) ->
		Result<MaybeTls13Certificate<'a,CertificateTls13X509<'a>>, _CertificateParseError>
	{
		let (context, entity, chain) = match generic_parse_tls13_cert(msg, enable)? {
			(ctx, Some((entity, chain))) => {
				print_chain_info(&entity, chain, enable, dbg)?;
				(ctx, entity, chain)
			},
			(context, None) => {
				debug!(dbg "Authentication rejected");
				return Ok(MaybeTls13Certificate{context, certificate:None});
			}
		};
		use crate::extensions::serverext_dnssec_chain;
		//Another part of dnssec_chain/delegated_credentials being special: Parse those out of ee
		//certificate, since CertificateEntry2 does not do that.
		let mut dnssec_chain = None;
		let mut delegated_credentials = None;
		do_extensions_block(&entity, 0, |etype, edata|{
			match etype {
				ExtensionType::DNSSEC_CHAIN => dnssec_chain = 
					Some(serverext_dnssec_chain(etype, edata, dbg)?),
				ExtensionType::DELEGATED_CREDENTIALS => delegated_credentials =
					Some(DelegatedCredentials::parse(edata, enable, dbg)?),
				_ => drop(edata.all())		//Neutralize junk check.
			}
			Ok(())
		})?;
		debug!(dbg "Received X.509 certificate with {icnt} issuers", icnt=chain.count_all(true));
		Ok(MaybeTls13Certificate{context, certificate:Some(CertificateTls13X509 {
			entity: entity.get_certificate(),
			entity_extensions: entity.get_extensions(),
			certificate_status: entity.get_status(),
			delegated_credentials: delegated_credentials,
			scts: entity.get_signed_certificate_timestamps(),
			trans_items: entity.get_transparency_items(),
			dnssec_chain: dnssec_chain,
			issuers: chain,
			x: PhantomData
		})})
	}
}

///TLS 1.3 Raw Public Key.
///
///This corresponds to the `certificate_list` field in the `Certificate` TLS structure.
#[derive(Clone)]
pub struct CertificateTls13Rpk<'a>
{
	key: &'a [u8],
	extensions: ListOf<'a,Extension<'a>>,
	dnssec_chain: Option<DnssecChain<'a>>,
	x: PhantomData<&'a u8>
}

impl<'a> CertificateTls13Rpk<'a>
{
	///Get the public key in X.509 SubjectPublicKeyInfo format with DER encoding.
	pub fn get_public_key(&self) -> &'a [u8] { self.key }
	///Get the Certificate extensions.
	///
	///This corresponds to the `extensions` field of the TLS `CertificateEntry` structure. In case of Raw
	///Public Key, there can be only one of these.
	pub fn get_extensions(&self) -> ListOf<'a,Extension<'a>> { self.extensions }
	///Get the DNSSec chain certifying the public key, if any.
	pub fn get_dnssec_chain(&self) -> Option<DnssecChain<'a>> { self.dnssec_chain }
	///Parse TLS 1.3 Raw Public Key Certificate message.
	///
	///The parameter `enabled` controls what TLS features are enabled. This is used to enforce the TLS
	///restriction that answers can not contain algorithms or extensions not present in offers. For server
	///certificate, the corresponding offer is Client Hello, and for client certificate, the corresponding
	///offer is Certificate Request. The following features may be requested:
	///
	/// * `Extension(DNSSEC_CHAIN)`
	/// * `Extension(foo)` for some other not currently defined extension `foo`.
	pub fn parse(msg: &'a [u8], enable: RemoteFeatureSet, dbg: &mut DbgT) ->
		Result<MaybeTls13Certificate<'a,CertificateTls13Rpk<'a>>, CertificateParseError>
	{
		Self::_parse(Message::new(msg), enable, dbg).map_err(CertificateParseError)
	}
	fn _parse(msg: Message<'a>, enable: RemoteFeatureSet, dbg: &mut DbgT) ->
		Result<MaybeTls13Certificate<'a,CertificateTls13Rpk<'a>>, _CertificateParseError>
	{
		let (context, entity) = match generic_parse_tls13_cert(msg, enable)? {
			(ctx, Some((entity, chain))) => {
				fail_if!(chain.nontrivial(), _CertificateParseError::RpkChainNotEmpty);
				(ctx, entity)
			},
			(context, None) => {
				debug!(dbg "Authentication rejected");
				return Ok(MaybeTls13Certificate{context, certificate:None});
			}
		};
		//Parse extensions.
		let mut dnssec_chain = None;
		use crate::extensions::serverext_dnssec_chain;
		do_extensions_block(&entity, 0, |etype, edata|{
			match etype {
				ExtensionType::SIGNED_CERTIFICATE_TIMESTAMP|ExtensionType::TRANSPARENCY_INFO|
					ExtensionType::STATUS_REQUEST => fail!(ExtensionError::IncompatibleRpk),
				ExtensionType::DNSSEC_CHAIN => dnssec_chain = 
					Some(serverext_dnssec_chain(etype, edata, dbg)?),
				ExtensionType::DELEGATED_CREDENTIALS =>
					fail!(ExtensionError::IncompatibleNonX509),
				_ => drop(edata.all())		//Neutralize junk check.
			}
			Ok(())
		})?;
		debug!(dbg "Received Raw public key");
		Ok(MaybeTls13Certificate{context, certificate:Some(CertificateTls13Rpk {
			key: entity.get_certificate(),
			extensions: entity.get_extensions(),
			dnssec_chain: dnssec_chain,
			x: PhantomData
		})})
	}
}

///TLS 1.3 IEEE 1609.2 certificate.
///
///This corresponds to the `certificate_list` field in the `Certificate` TLS structure.
#[derive(Clone)]
pub struct CertificateTls13Ieee1609Dot2<'a>
{
	entity: &'a [u8],
	entity_extensions: ListOf<'a,Extension<'a>>,
	certificate_status: Option<CertificateStatus<'a>>,
	dnssec_chain: Option<DnssecChain<'a>>,
	scts: ListOf<'a,SerializedSCT<'a>>,
	trans_items: ListOf<'a,SerializedTransItem<'a>>,
	issuers: ListOf<'a,CertificateEntry2<'a>>,
	x: PhantomData<&'a u8>
}

impl<'a> CertificateTls13Ieee1609Dot2<'a>
{
	///Get the end-entity Certificate.
	pub fn get_end_entity(&self) -> &'a [u8] { self.entity }
	///Get the issuer chain.
	///
	///This is all certificates in the chain except the first.
	pub fn get_issuer_chain(&self) -> ListOf<'a,CertificateEntry2<'a>> { self.issuers }
	///Get the end-entity Certificate extensions.
	///
	///This corresponds to the `extensions` field of the TLS `CertificateEntry` structure in the end-entity
	///certificate.
	///
	///The extensions of other certificates in chain can be accessed via the certificate entries.
	pub fn get_extensions(&self) -> ListOf<'a,Extension<'a>> { self.entity_extensions }
	///Get stapled certificate status response, if any.
	pub fn get_status(&self) -> Option<CertificateStatus<'a>> { self.certificate_status }
	///Get the DNSSec chain certifying the certificate, if any.
	///
	///Note that currently there is no way for TLSA record to verify IEEE 1609.2 certificate.
	pub fn get_dnssec_chain(&self) -> Option<DnssecChain<'a>> { self.dnssec_chain }
	///Get list of stapled Certificate transparency v1 Signed Certificate Timestamps for the end-entity
	///certificate.
	///
	///This corresponds to `signed_certificate_timestamp` extension on the end-endity certificate.
	///
	///The corresponding Signed Certificate Timetamps of other certificates can be accessed via certificate
	///entries for those certificates.
	pub fn get_signed_certificate_timestamps(&self) -> ListOf<'a,SerializedSCT<'a>> { self.scts }
	///Get list of stapled Certificate transparency v2 Transparency Items for the end-entity certificate.
	///
	///This corresponds to `transparency_info` extension on the end-endity certificate.
	///
	///The corresponding Transparency Items of other certificates can be accessed via certificate entries
	///for those certificates.
	pub fn get_transparency_items(&self) -> ListOf<'a,SerializedTransItem<'a>> { self.trans_items }
	///Parse TLS 1.3 IEEE 1609.2 Certificate message.
	///
	///The parameter `enabled` controls what TLS features are enabled. This is used to enforce the TLS
	///restriction that answers can not contain algorithms or extensions not present in offers. For server
	///certificate, the corresponding offer is Client Hello, and for client certificate, the corresponding
	///offer is Certificate Request. The following features may be requested:
	///
	/// * `Extension(STATUS_REQUEST)`
	///   * Together with `CertificateStatusType(type)`, where `type` is the type of response.
	/// * `Extension(SIGNED_CERTIFICATE_TIMESTAMP`
	/// * `Extension(TRANSPARENCY_INFO)`
	/// * `Extension(DNSSEC_CHAIN)`
	/// * `Extension(foo)` for some other not currently defined extension `foo`.
	pub fn parse(msg: &'a [u8], enable: RemoteFeatureSet, dbg: &mut DbgT) ->
		Result<MaybeTls13Certificate<'a,CertificateTls13Ieee1609Dot2<'a>>, CertificateParseError>
	{
		Self::_parse(Message::new(msg), enable, dbg).map_err(CertificateParseError)
	}
	fn _parse(msg: Message<'a>, enable: RemoteFeatureSet, dbg: &mut DbgT) ->
		Result<MaybeTls13Certificate<'a,CertificateTls13Ieee1609Dot2<'a>>, _CertificateParseError>
	{
		let (context, entity, chain) = match generic_parse_tls13_cert(msg, enable)? {
			(ctx, Some((entity, chain))) => {
				print_chain_info(&entity, chain, enable, dbg)?;
				(ctx, entity, chain)
			},
			(context, None) => {
				debug!(dbg "Authentication rejected");
				return Ok(MaybeTls13Certificate{context, certificate:None});
			}
		};
		//Parse extensions.
		use crate::extensions::serverext_dnssec_chain;
		let mut dnssec_chain = None;
		do_extensions_block(&entity, 0, |etype, edata|{
			match etype {
				ExtensionType::DNSSEC_CHAIN => dnssec_chain = 
					Some(serverext_dnssec_chain(etype, edata, dbg)?),
				ExtensionType::DELEGATED_CREDENTIALS =>
					fail!(ExtensionError::IncompatibleNonX509),
				_ => drop(edata.all())	//Neutralize junk check.
			}
			Ok(())
		})?;
		debug!(dbg "Received IEEE 1609.2 certificate with {icnt} issuers", icnt=chain.count_all(true));
		Ok(MaybeTls13Certificate{context, certificate:Some(CertificateTls13Ieee1609Dot2 {
			entity: entity.get_certificate(),
			entity_extensions: entity.get_extensions(),
			certificate_status: entity.get_status(),
			dnssec_chain: dnssec_chain,
			scts: entity.get_signed_certificate_timestamps(),
			trans_items: entity.get_transparency_items(),
			issuers: chain,
			x: PhantomData
		})})
	}
}

///Signature.
#[derive(Copy,Clone)]
pub struct Signature<'a>
{
	pub(crate) partial_tbs: &'a [u8],
	pub(crate) algorithm: btls_aux_tls_iana::SignatureScheme,
	pub(crate) signature: &'a [u8],
}

impl<'a> Signature<'a>
{
	///Get the partial data to be signed.
	///
	///This data may be partial or even missing. See description of various functions and methods that produce
	///this structure for more information as to what this actually is.
	pub fn get_partial_tbs(&self) -> &'a [u8] { self.partial_tbs }
	///Get the signature algorithm.
	pub fn get_algorithm(&self) -> btls_aux_tls_iana::SignatureScheme { self.algorithm }
	///Get the signature.
	pub fn get_signature(&self) -> &'a [u8] { self.signature }
}

///Delegated Credential.
#[derive(Clone)]
pub struct DelegatedCredentials<'a>
{
	valid_time: u32,
	dc_cert_verify_algorithm: btls_aux_tls_iana::SignatureScheme,
	subject_public_key_info: &'a [u8],
	raw_cred: &'a[u8],
	algorithm: btls_aux_tls_iana::SignatureScheme,
	signature: &'a [u8],
	x: PhantomData<&'a u8>
}

impl<'a> DelegatedCredentials<'a>
{
	///Get the signature on delegated credential.
	///
	///The `get_partial_tbs()` method does return the raw Credential structure.
	pub fn get_signature(&self) -> Signature<'a>
	{
		Signature {
			partial_tbs: self.raw_cred,
			algorithm: self.algorithm,
			signature: self.signature
		}
	}
	///Get the delegated public key.
	///
	///The public key is in X.509 SubjectPublicKeyInfo format with DER encoding.
	pub fn get_delegated_public_key(&self) -> &'a [u8] { self.subject_public_key_info }
	///Get the expected verify algorithm.
	///
	///This should be passed to `parse_certificate_verify()`. It has already been checked against
	///enable of type `DelegatedCredential`.
	pub fn get_algorithm(&self) -> SignatureScheme { self.dc_cert_verify_algorithm }
	///Given the valid range of parent certificate, compute the valid range of the delegated credential.
	///
	///The unit of time is the second. The validity of delegated credential is relative, so epoch can be
	///anything.
	pub fn get_validity(&self, parent_cert_range: Range<i64>) -> Range<i64>
	{
		const DURATION: u32 = 7*24*60*60;
		//Start time. Do not go before certificate start time.
		let a = parent_cert_range.start.saturating_add(self.valid_time.saturating_sub(DURATION) as i64);
		let b = parent_cert_range.start.saturating_add(self.valid_time as i64);
		//End time. Do not go after certificate expiry time.
		let b = core::cmp::min(b, parent_cert_range.end);
		a..b
	}
	///Parse stand-alone delegated credential
	pub fn parse_standalone(msg: &'a [u8]) -> Option<DelegatedCredentials<'a>>
	{
		//Enable everything, we want to parse.
		let mut msg = Message::new(msg);
		let c = Self::parse(&mut msg, &mut |_|true, &mut None).ok()?;
		fail_if_none!(msg.more());	//Trailing junk.
		Some(c)
	}

	fn parse(msg: &mut Message<'a>, enable: RemoteFeatureSet, dbg: &mut DbgT) ->
		Result<DelegatedCredentials<'a>, ExtensionError>
	{
		let omsg = msg.as_inner();
		let valid_time: u32 = field!(E ValidTime msg.cp());
		let delegated_algo: SignatureScheme = field!(E DcCertVerifyAlgorithm msg.cp().
			map_err(ItemError::Single));
		let subject_public_key_info = field!(E SubjectPublicKeyInfo2 msg.slice(LEN_DC_SPKI));
		let cred_len = omsg.len().saturating_sub(msg.length());
		let raw_cred = omsg.get(..cred_len).unwrap_or(b"");	//Everything so far.
		let algorithm: SignatureScheme = field!(E Algorithm msg.cp().map_err(ItemError::Single));
		let signature = field!(E Signature msg.slice(LEN_SIGNATURE));
		//Check algorithms.
		use ExtensionError::Structured as ES;
		use ItemError::BogusElement as BE;
		fail_if!(!crate::ask_delegated_credential(enable, delegated_algo),
			ES(EStruct::DcCertVerifyAlgorithm(BE(delegated_algo))));
		fail_if!(!crate::ask_signature_algorithm(enable, algorithm),
			ES(EStruct::Algorithm(BE(algorithm))));
		debug!(dbg "Received delegated credentials ({})", delegated_algo.display());
		Ok(DelegatedCredentials {
			valid_time,
			dc_cert_verify_algorithm: delegated_algo,
			subject_public_key_info,
			raw_cred,
			algorithm,
			signature,
			x: PhantomData
		})
	}
}

///Error from parsing Certificate Verify.
///
///The most notable things to query errors are the `alert()` method and the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct CertificateVerifyParseError(_CertificateVerifyParseError);

#[derive(Copy,Clone,Debug)]
enum _CertificateVerifyParseError
{
	Algorithm(PrimitiveReadError),
	Signature(SliceReadError),
	BadAlgorithm(SignatureScheme),
	JunkAfterEnd,
}

impl CertificateVerifyParseError
{
	///Get the alert code one should use for this parse error.
	pub fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::_CertificateVerifyParseError::*;
		use btls_aux_tls_iana::Alert as A;
		match &self.0 {
			&Algorithm(ref err) => err.alert(),
			&Signature(ref err) => err.alert(),
			&BadAlgorithm(_) => A::ILLEGAL_PARAMETER,
			&JunkAfterEnd => A::DECODE_ERROR,
		}
	}
}

impl core::fmt::Display for CertificateVerifyParseError
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		use self::_CertificateVerifyParseError::*;
		match self.0 {
			Algorithm(err) => write!(f, "algorithm: {err}"),
			Signature(err) => write!(f, "signature: {err}"),
			BadAlgorithm(alg) => write!(f, "algorithm: Bogus value {alg}"),
			JunkAfterEnd => f.write_str("Junk after end of message"),
		}
	}
}

///Parse the Certificate Verify message.
///
///If `req_algo` is `None`, the signature algorithm is parsed from the message. The following features are then
///required to be enabled via the `enable` parameter:
///
/// * `Extension(SUPPORTED_SIGNATURE_ALGORITHMS)`
/// * `SignatureAlgorithm(algo)`, where `algo` is the used algorithm.
///
///If `req_algo` is `Some(algo)`, then the message is required to have the same signature algorithm. No enable
///check is done, as it is assumed such check has been done already. This is useful if validating delegated
///credentials.
///
///The partial TBS is not useful (it is set to empty string).
pub fn parse_certificate_verify<'a>(msg: &'a [u8], enable: RemoteFeatureSet, req_algo: Option<SignatureScheme>,
	dbg: &mut DbgT) -> Result<Signature<'a>, CertificateVerifyParseError>
{
	_parse_cv(Message::new(msg), enable, req_algo, dbg).map_err(CertificateVerifyParseError)
}

fn _parse_cv<'a>(mut msg: Message<'a>, enable: RemoteFeatureSet, req_algo: Option<SignatureScheme>, dbg: &mut DbgT) ->
	Result<Signature<'a>, _CertificateVerifyParseError>
{
	use _CertificateVerifyParseError as CVPE;
	// struct {
	//	 SignatureAndHashAlgorithm algorithm;
	//	 opaque signature<0..2^16-1>;
	// } DigitallySigned;
	//
	// -RFC5246, section 4.7.
	let algorithm: SignatureScheme = msg.cp().map_err(CVPE::Algorithm)?;
	let signature = msg.slice(LEN_SIGNATURE).map_err(CVPE::Signature)?;
	msg.assert_end(CVPE::JunkAfterEnd)?;
	//If req_algo is set, require that algorithm and force it enabled. Otherwise do normal enabled
	//check.
	if let Some(req_algo) = req_algo {
		fail_if!(algorithm != req_algo, CVPE::BadAlgorithm(algorithm));
	} else {
		fail_if!(!crate::ask_signature_algorithm(enable, algorithm),
			CVPE::BadAlgorithm(algorithm));
	}
	debug!(dbg "Signature algorithm {algorithm} ({slen} bytes)", slen=signature.len());
	Ok(Signature {
		partial_tbs: b"",
		algorithm: algorithm,
		signature: signature,
	})
}

