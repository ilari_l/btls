#![allow(unsafe_code)]


pub(crate) trait Print: core::fmt::Debug
{
	fn format(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>;
}
