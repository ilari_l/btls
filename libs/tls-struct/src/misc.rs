use crate::DbgT;
use crate::LEN_COMPRESSED_CERT_MSG;
use crate::LEN_CONNECTION_ID_LIST;
use crate::LEN_COOKIE_TLS12;
use crate::LEN_EKT_KEYVALUE;
use crate::LEN_EKT_MASTERSALT;
use crate::LEN_SUPPLEMENTAL_DATA_ENTRY_LIST;
use crate::LEN_URL_AND_HASH_LIST;
use crate::RemoteFeatureSet;
use crate::iters2::AlertForError;
use crate::errors::Struct as EStruct;
use crate::iters2::ConnectionId;
use crate::iters2::IntoGenericIterator as ListOf;
use crate::iters2::Message;
use crate::iters2::SupplementalDataEntry;
use crate::iters2::UrlAndHash;
use btls_aux_tls_iana::CertChainType;
use btls_aux_tls_iana::CertificateCompressionAlgorithmId;
use btls_aux_tls_iana::ConnectionIdUsage;
use btls_aux_tls_iana::TlsVersion;
use btls_aux_tls_iana::helper::U24;
use btls_aux_tls_iana::helper::XRead;
use core::marker::PhantomData;

macro_rules! field
{
	($clazz:ident :: $location:ident $x:expr) => {
		$x.map_err(EStruct::$location).map_err($clazz::Structured)?
	};
}

#[derive(Copy,Clone,Debug)]
enum _ParseError
{
	Structured(crate::errors::Struct),
	JunkAfterEnd(&'static str),
}

impl _ParseError
{
	///Get the alert code one should use for this parse error.
	pub fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::_ParseError::*;
		use btls_aux_tls_iana::Alert as A;
		match self {
			Structured(ref g) => g.alert(),
			JunkAfterEnd(_) => A::DECODE_ERROR,
		}
	}
}

impl core::fmt::Display for _ParseError
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		use self::_ParseError::*;
		match self {
			Structured(e) => core::fmt::Display::fmt(e, f),
			JunkAfterEnd(t) => write!(f, "Junk after end of {t} message"),
		}
	}
}

macro_rules! wrap_generic_error
{
	($clazz:ident) => {
		impl $clazz
		{
			///Get the alert code one should use for this parse error.
			pub fn alert(&self) -> btls_aux_tls_iana::Alert
			{
				self.0.alert()
			}
		}
		impl core::fmt::Display for $clazz
		{
			fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
			{
				core::fmt::Display::fmt(&self.0, f)
			}
		}
	}
}

///Error from parsing Compressed Certificate.
///
///The most notable things to query errors are the `alert()` method and the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct CompressedCertificateParseError(_ParseError);
wrap_generic_error!(CompressedCertificateParseError);

///Compressed Certificate message.
#[derive(Clone)]
pub struct CompressedCertificate<'a>
{
	algorithm: CertificateCompressionAlgorithmId,
	uncompressed_length: usize,
	compressed_certificate_message: &'a [u8],
	x: PhantomData<&'a u8>
}

impl<'a> CompressedCertificate<'a>
{
	///Get the compression algorithm.
	pub fn get_algorithm(&self) -> CertificateCompressionAlgorithmId { self.algorithm }
	///Get the uncompressed length of the message.
	///
	///This allows preallocating buffer for the certificate message, as well as detecting beforehand if the
	///decompressed size is too big.
	pub fn get_uncompressed_length(&self) -> usize { self.uncompressed_length }
	///Get the raw compressed certificate message data.
	pub fn get_compressed_message(&self) -> &'a [u8] { self.compressed_certificate_message }
	///Parse Compressed Certificate message.
	///
	///The parameter `enabled` controls what TLS features are enabled. This is used to enforce the TLS
	///restriction that answers can not contain algorithms or extensions not present in offers. For server
	///certificate, the corresponding offer is Client Hello, and for client certificate, the corresponding
	///offer is Certificate Request. The following features may be requested:
	///
	/// * `Extension(CompressCertificate)`, together with:
	///    * `CertificateCompression(alg)`: Use certificate compression `alg`.
	pub fn parse(msg: &'a [u8], enable: RemoteFeatureSet, dbg: &mut DbgT) ->
		Result<CompressedCertificate<'a>, CompressedCertificateParseError>
	{
		Self::_parse(Message::new(msg), enable, dbg).map_err(CompressedCertificateParseError)
	}
	fn _parse(mut msg: Message<'a>, enable: RemoteFeatureSet, dbg: &mut DbgT) ->
		Result<CompressedCertificate<'a>, _ParseError>
	{
		use _ParseError as PE;
		// struct {
		//	 CertificateCompressionAlgorithm algorithm;
		//	 uint24 uncompressed_length;
		//	 opaque compressed_certificate_message<1..2^24-1>;
		// } CompressedCertificate;
		//
		// - RFC8879 section 4.
		let algorithm: CertificateCompressionAlgorithmId = field!(PE::Algorithm2 msg.item(|v|{
			crate::ask_certificate_compression(enable, v)
		}));
		let uncompressed_length: U24 = field!(PE::UncompressedLength msg.cp());
		let uncompressed_length = uncompressed_length.__cast_usize();
		let compressed_certificate_message = field!(PE::CompressedCertificateMessage
			msg.slice(LEN_COMPRESSED_CERT_MSG)
		);
		msg.assert_end(PE::JunkAfterEnd("compressed certificate"))?;
		debug!(dbg "Algorithm {algorithm}, uncompressed size {uncompressed_length} bytes");
		Ok(CompressedCertificate {
			algorithm,
			uncompressed_length,
			compressed_certificate_message,
			x: PhantomData
		})
	}
}

///Error from parsing Hello Verify Request.
///
///The most notable things to query errors are the `alert()` method and the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct HelloVerifyRequestParseError(_ParseError);
wrap_generic_error!(HelloVerifyRequestParseError);

///Hello Verify Request message.
///
///Note that this is only used in DTLS 1.2, not TLS 1.2.
#[derive(Clone)]
pub struct HelloVerifyRequest<'a>
{
	server_version: TlsVersion,
	cookie: &'a [u8],
}

impl<'a> HelloVerifyRequest<'a>
{
	///Get the server version in use.
	pub fn get_server_version(&self) -> TlsVersion { self.server_version }
	///Get the cookie server sent.
	pub fn get_cookie(&self) -> &'a [u8] { self.cookie }
	///Parse Hello Verify Request message.
	pub fn parse(msg: &'a [u8], dbg: &mut DbgT) ->
		Result<HelloVerifyRequest<'a>, HelloVerifyRequestParseError>
	{
		Self::_parse(Message::new(msg), dbg).map_err(HelloVerifyRequestParseError)
	}
	fn _parse(mut msg: Message<'a>, dbg: &mut DbgT) -> Result<HelloVerifyRequest<'a>, _ParseError>
	{
		use _ParseError as PE;
		// struct {
		//	 ProtocolVersion server_version;
		//	 opaque cookie<0..2^8-1>;
		// } HelloVerifyRequest;
		//
		// - RFC6347 section 4.2.1.
		let server_version: TlsVersion = field!(PE::ServerVersion2 msg.cp());
		let cookie = field!(PE::Cookie msg.slice(LEN_COOKIE_TLS12));
		msg.assert_end(PE::JunkAfterEnd("hello verify request"))?;
		debug!(dbg "Server version {server_version} cookie {clen} bytes", clen=cookie.len());
		Ok(HelloVerifyRequest {
			server_version,
			cookie,
		})
	}
}

///Error from parsing Request Connection ID.
///
///The most notable things to query errors are the `alert()` method and the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct RequestConnectionIdParseError(_ParseError);
wrap_generic_error!(RequestConnectionIdParseError);

///Request connection IDs message.
#[derive(Clone)]
pub struct RequestConnectionId<'a>
{
	count: u8,
	x: PhantomData<&'a u8>
}

impl<'a> RequestConnectionId<'a>
{
	///Get number of CIDs requested.
	pub fn get_requested_count(&self) -> u8 { self.count }
	///Parse Request Connection IDs message.
	pub fn parse(msg: &'a [u8], dbg: &mut DbgT) ->
		Result<RequestConnectionId<'a>, RequestConnectionIdParseError>
	{
		Self::_parse(Message::new(msg), dbg).map_err(RequestConnectionIdParseError)
	}
	fn _parse(mut msg: Message<'a>, dbg: &mut DbgT) -> Result<RequestConnectionId<'a>, _ParseError>
	{
		use _ParseError as PE;
		// struct {
		//	 uint8 num_cids;
		// } RequestConnectionId;
		//
		// - draft-ietf-tls-dtls13-43 section 9.
		let count: u8 = field!(PE::CidCount msg.cp());
		msg.assert_end(PE::JunkAfterEnd("request connection id"))?;
		debug!(dbg "{count} connection IDs requested");
		Ok(RequestConnectionId {
			count,
			x: PhantomData
		})
	}
}

///Error from parsing New Connection ID.
///
///The most notable things to query errors are the `alert()` method and the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct NewConnectionIdParseError(_ParseError);
wrap_generic_error!(NewConnectionIdParseError);

///New Connection ID message.
#[derive(Clone)]
pub struct NewConnectionId<'a>
{
	usage: ConnectionIdUsage,
	cids: ListOf<'a,ConnectionId<'a>>,
}

impl<'a> NewConnectionId<'a>
{
	///Get the Connection ID usage.
	pub fn get_usage(&self) -> ConnectionIdUsage { self.usage }
	///Get the list of new CIDs.
	pub fn get_cid_list(&self) -> ListOf<'a,ConnectionId<'a>> { self.cids }
	///Parse New Connection ID mesage.
	pub fn parse(msg: &'a [u8], dbg: &mut DbgT) -> Result<NewConnectionId<'a>, NewConnectionIdParseError>
	{
		Self::_parse(Message::new(msg), dbg).map_err(NewConnectionIdParseError)
	}
	fn _parse(mut msg: Message<'a>, dbg: &mut DbgT) -> Result<NewConnectionId<'a>, _ParseError>
	{
		use _ParseError as PE;
		// opaque ConnectionId<0..2^8-1>;
		//
		// struct {
		//	 ConnectionIds cids<0..2^16-1>;
		//	 ConnectionIdUsage usage;
		// } NewConnectionId;
		//
		// - draft-ietf-tls-dtls13-43 section 9.
		let cids = field!(PE::Cids msg.list_of::<ConnectionId<'a>>(LEN_CONNECTION_ID_LIST));
		let usage: ConnectionIdUsage = field!(PE::Usage msg.cp());
		msg.assert_end(PE::JunkAfterEnd("certificate URL"))?;
		debug!(dbg "Type {usage}, {ccnt} CIDs", ccnt=cids.count_all(true));
		Ok(NewConnectionId {
			cids,
			usage,
		})
	}
}

///Error from parsing Certificate URL.
///
///The most notable things to query errors are the `alert()` method and the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct CertificateUrlParseError(_ParseError);
wrap_generic_error!(CertificateUrlParseError);


///Client Certificate URL message.
#[derive(Clone)]
pub struct CertificateUrl<'a>
{
	chain_type: CertChainType,
	url_and_hash_list: ListOf<'a,UrlAndHash<'a>>,
	x: PhantomData<&'a u8>
}

impl<'a> CertificateUrl<'a>
{
	///Get the chain type.
	pub fn get_chain_type(&self) -> CertChainType { self.chain_type }
	///Get the list of URLs and corresponding hashes.
	pub fn get_urls(&self) -> ListOf<'a,UrlAndHash<'a>> { self.url_and_hash_list }
	///Parse Client Certificate URL message.
	pub fn parse(msg: &'a [u8], dbg: &mut DbgT) ->
		Result<CertificateUrl<'a>, CertificateUrlParseError>
	{
		Self::_parse(Message::new(msg), dbg).map_err(CertificateUrlParseError)
	}
	fn _parse(mut msg: Message<'a>, dbg: &mut DbgT) -> Result<CertificateUrl<'a>, _ParseError>
	{
		use _ParseError as PE;
		// struct {
		//	 CertChainType type;
		//	 URLAndHash url_and_hash_list<1..2^16-1>;
		// } CertificateURL;
		//
		// - RFC6066 section 5.
		let chain_type: CertChainType = field!(PE::ChainType msg.cp());
		let url_and_hash_list = field!(PE::UrlAndHashList
			msg.list_of::<UrlAndHash<'a>>(LEN_URL_AND_HASH_LIST)
		);
		msg.assert_end(PE::JunkAfterEnd("certificate URL"))?;
		debug!(dbg "type {chain_type}, {ucnt} URLs", ucnt=url_and_hash_list.count_all(true));
		Ok(CertificateUrl {
			chain_type,
			url_and_hash_list,
			x: PhantomData,
		})
	}
}

///Error from parsing Supplemental Data.
///
///The most notable things to query errors are the `alert()` method and the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct SupplementalDataParseError(_ParseError);
wrap_generic_error!(SupplementalDataParseError);

///Supplemental Data
#[derive(Clone)]
pub struct SupplementalData<'a>
{
	supp_data: ListOf<'a,SupplementalDataEntry<'a>>,
}

impl<'a> SupplementalData<'a>
{
	///Get list of Supplemental Data items.
	pub fn get_supplemental_data(&self) -> ListOf<'a,SupplementalDataEntry<'a>> { self.supp_data }
	///Parse Supplemental Data message.
	pub fn parse(msg: &'a [u8], dbg: &mut DbgT) ->
		Result<SupplementalData<'a>, SupplementalDataParseError>
	{
		Self::_parse(Message::new(msg), dbg).map_err(SupplementalDataParseError)
	}
	fn _parse(mut msg: Message<'a>, dbg: &mut DbgT) -> Result<SupplementalData<'a>, _ParseError>
	{
		use _ParseError as PE;
		// struct {
		//	 SupplementalDataEntry supp_data<1..2^24-1>;
		// } SupplementalData;
		//
		// struct {
		//	 SupplementalDataType supp_data_type;
		//	 uint16 supp_data_length;
		//	 select(SupplementalDataType) { }
		// } SupplementalDataEntry;
		//
		// - RFC4680 section 2.
		let supp_data = field!(PE::SuppData
			msg.list_of::<SupplementalDataEntry<'a>>(LEN_SUPPLEMENTAL_DATA_ENTRY_LIST)
		);
		msg.assert_end(PE::JunkAfterEnd("supplemental data"))?;
		debug!(dbg "{scnt} supplemental data entries", scnt=supp_data.count_all(true));
		Ok(SupplementalData {
			supp_data: supp_data,
		})
	}
}

///Error from parsing EKT Key.
///
///The most notable things to query errors are the `alert()` method and the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct EktKeyParseError(_ParseError);
wrap_generic_error!(EktKeyParseError);


///EKT Key message.
#[derive(Clone)]
pub struct EktKey<'a>
{
	ekt_key_value: &'a [u8],
	srtp_master_salt: &'a [u8],
	ekt_spi: u16,
	ekt_ttl: u32,
}

impl<'a> EktKey<'a>
{
	///Get the secret key.
	pub fn get_secret_key(&self) -> &'a [u8] { self.ekt_key_value }
	///Get the SRTP master salt.
	pub fn get_srtp_master_salt(&self) -> &'a [u8] { self.srtp_master_salt }
	///Get the EKT Security Parameter Index value.
	pub fn get_spi(&self) -> u16 { self.ekt_spi }
	///Get the EKT lifetime in seconds.
	pub fn get_ttl(&self) -> u32 { self.ekt_ttl }
	///Parse EKT Key message.
	pub fn parse(msg: &'a [u8], dbg: &mut DbgT) -> Result<EktKey<'a>, EktKeyParseError>
	{
		Self::_parse(Message::new(msg), dbg).map_err(EktKeyParseError)
	}
	fn _parse(mut msg: Message<'a>, dbg: &mut DbgT) -> Result<EktKey<'a>, _ParseError>
	{
		use _ParseError as PE;
		// struct {
		//	 opaque ekt_key_value<1..256>;
		//	 opaque srtp_master_salt<1..256>;
		//	 uint16 ekt_spi;
		//	 uint24 ekt_ttl;
		// } EKTKey;
		//
		// - RFC8870 section 5.2.2.

		//I assume those 256s should be 255s.
		let ekt_key_value = field!(PE::EktKeyValue msg.slice(LEN_EKT_KEYVALUE));
		let srtp_master_salt = field!(PE::SrtpMasterSalt msg.slice(LEN_EKT_MASTERSALT));
		let ekt_spi: u16 = field!(PE::EktSpi msg.cp());
		let ekt_ttl: U24 = field!(PE::EktTtl msg.cp());
		let ekt_ttl = ekt_ttl.__cast_usize() as u32;
		msg.assert_end(PE::JunkAfterEnd("EKT key"))?;
		debug!(dbg "SPI {ekt_spi}, TTL {ekt_ttl}");
		Ok(EktKey {
			ekt_key_value,
			srtp_master_salt,
			ekt_spi,
			ekt_ttl,
		})
	}
}
