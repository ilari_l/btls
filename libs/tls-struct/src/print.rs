macro_rules! define_print
{
	($clazz:ident) => {
		impl crate::iters::Print for btls_aux_tls_iana::$clazz
		{
			fn format(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
			{
				write!(f, "{kind} {self}", kind=Self::kind())
			}
		}
	};
}

define_print!(AuthorizationDataFormat);
define_print!(CertificateCompressionAlgorithmId);
define_print!(CertificateType);
define_print!(CipherSuite);
define_print!(ClientCertificateType);
define_print!(CompressionMethod);
define_print!(DtlsSrtpProtectionProfile);
define_print!(EcPointFormat);
define_print!(EktCipher);
define_print!(ExtensionType);
define_print!(PskKeyExchangeMode);
define_print!(SignatureScheme);
define_print!(NamedGroup);
define_print!(TokenBindingKeyParameter);
define_print!(TlsVersion);
define_print!(UserMappingType);
