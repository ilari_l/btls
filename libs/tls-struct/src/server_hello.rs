use crate::DbgT;
use crate::ExtensionError;
use crate::KeyCheck;
use crate::LEN_COOKIE;
use crate::LEN_EC_POINT_FORMATS;
use crate::LEN_EXTENSION_LIST;
use crate::LEN_KEY_SHARE_ENTRY_KEX;
use crate::LEN_RENEGOTIATED_CONNECTION;
use crate::LEN_SERIALIZED_SCT_LIST;
use crate::LEN_SERIALIZED_TRANS_ITEM_LIST;
use crate::LEN_SESSION_ID;
use crate::LEN_TB_KEY_PARAMETERS_LIST;
use crate::LEN_USER_MAPPING_TYPE_LIST;
use crate::RemoteFeatureSet;
use crate::extensions as exts;
use crate::iters2::Extension;
use crate::iters2::IntoGenericIterator as ListOf;
use crate::iters2::Message;
use crate::iters2::VectorError;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_memory::Hexdump;
use btls_aux_tls_iana::CertificateType;
use btls_aux_tls_iana::ExtensionType;
use btls_aux_tls_iana::TlsVersion;
use btls_aux_tls_iana::CipherSuite;
use btls_aux_tls_iana::CompressionMethod;
use btls_aux_tls_iana::PskKeyExchangeMode;
use btls_aux_tls_iana::SequenceNumberEncryptionAlgorithm;
use btls_aux_tls_iana::NamedGroup;


macro_rules! field
{
	(E $location:ident $x:expr) => {
		$x.map_err(crate::errors::Struct::$location).map_err(ExtensionError::Structured)?
	};
	(S $location:ident $x:expr) => {
		$x.map_err(crate::errors::Struct::$location).map_err(_ServerHelloParseError::Structured)?
	};
}

macro_rules! extension
{
	($exts2:ident $dbg:ident $name:ident $handler:expr) => {
		$exts2.with_stdext(ExtensionType::$name, _ServerHelloParseError::JunkAfterExtension,
			_ServerHelloParseError::Extension, $dbg, $handler)?
	}
}

///Error from parsing Server Hello.
///
///The most notable things to query errors are the `alert()` method and the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct ServerHelloParseError(_ServerHelloParseError);

#[derive(Copy,Clone,Debug)]
enum _ServerHelloParseError
{
	Extension(ExtensionType, ExtensionError),
	CertTypeConflict,
	CidConflict,
	NoKeyExchange,
	BogusKeyExchangeMode(PskKeyExchangeMode),
	CertExternPskReq,
	TokenBindingRequiresEms,
	DowngradeAttack,
	JunkAfterExtensions,
	CompressionMethodTls13(btls_aux_tls_iana::CompressionMethod),
	Structured(crate::errors::Struct),
	JunkAfterExtension(ExtensionType),
}

impl ServerHelloParseError
{
	///Get the alert code one should use for this parse error.
	pub fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::_ServerHelloParseError::*;
		use btls_aux_tls_iana::Alert as A;
		match self.0 {
			Extension(_,g) => g.alert(),
			CertTypeConflict => A::UNSUPPORTED_EXTENSION,
			CidConflict => A::UNSUPPORTED_EXTENSION,
			NoKeyExchange => A::MISSING_EXTENSION,
			BogusKeyExchangeMode(_) => A::ILLEGAL_PARAMETER,
			CertExternPskReq => A::MISSING_EXTENSION,
			TokenBindingRequiresEms => A::MISSING_EXTENSION,
			DowngradeAttack => A::PROTOCOL_VERSION,
			JunkAfterExtensions => A::DECODE_ERROR,
			CompressionMethodTls13(_) => A::ILLEGAL_PARAMETER,
			Structured(ref x) => crate::iters2::AlertForError::alert(x),
			JunkAfterExtension(_) => A::DECODE_ERROR,
		}
	}
}

impl core::fmt::Display for ServerHelloParseError
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		use self::_ServerHelloParseError::*;
		match &self.0 {
			&Extension(ext, ref err) => write!(f, "extension {ext}: {err}"),
			&CertTypeConflict =>
				write!(f, "cert_type and {{client,server}}_certificate_type are mutually exclusive"),
			&CidConflict => write!(f, "connection_id extensions are mutually exclusive"),
			&NoKeyExchange => write!(f, "No key exchange available"),
			&BogusKeyExchangeMode(ref pmode) => write!(f, "Illegal PSK mode {pmode}"),
			&CertExternPskReq =>
				write!(f, "tls_cert_with_extern_psk requires both key_share and pre_shared_key"),
			&TokenBindingRequiresEms => write!(f, "token_binding requires extended_master_secret"),
			&DowngradeAttack => write!(f, "Downgrade attack detected"),
			&JunkAfterExtensions => write!(f, "Junk after end of extensions"),
			&CompressionMethodTls13(ref meth) => write!(f, "Illegal compression method {meth} in TLS 1.3"),
			&Structured(ref e) => core::fmt::Display::fmt(e, f),
			&JunkAfterExtension(ext) => write!(f, "Junk after end of extension {ext}"),
		}
	}
}

static HRR_MAGIC: [u8; 32] = [
        0xCF, 0x21, 0xAD, 0x74, 0xE5, 0x9A, 0x61, 0x11, 0xBE, 0x1D, 0x8C, 0x02, 0x1E, 0x65, 0xB8, 0x91,
        0xC2, 0xA2, 0x11, 0x16, 0x7A, 0xBB, 0x8C, 0x5E, 0x07, 0x9E, 0x09, 0xE2, 0xC8, 0xA8, 0x33, 0x9C,
];

///Parsed TLS Server Hello.
///
///This is not just Server Hello, this is all messages that are able to stand in as Server Hello. This includes:
///
/// * TLS 1.2 Server Hello.
/// * TLS 1.3 Server Hello.
/// * TLS 1.3 Hello Retry Request.
#[derive(Clone)]
#[non_exhaustive]
pub enum ServerHello<'a>
{
	HelloRetryRequest(HelloRetryRequest<'a>),
	ServerHelloTls12(ServerHelloTls12<'a>),
	ServerHelloTls13(ServerHelloTls13<'a>),
}

impl<'a> ServerHello<'a>
{
	///Parse TLS Server Hello message.
	///
	///The parameter `enabled` controls what TLS features are enabled. This is used to enforce the TLS
	///restriction that answers can not contain algorithms or extensions not present in offers. For server
	///certificate, the corresponding offer is Client Hello, and for client certificate, the corresponding
	///offer is Certificate Request. Most features can end up being requested.
	///
	///The parameter `key_ok` is used as callback to validate any received key shares. These are KEM
	///ciphertexts.
	pub fn parse(msg: &'a [u8], enable: RemoteFeatureSet, key_ok: KeyCheck, dbg: &mut DbgT) ->
		Result<ServerHello<'a>, ServerHelloParseError>
	{
		let base = parse_base(Message::new(msg), enable, dbg).map_err(ServerHelloParseError)?;
		//This may be HRR, SH(TLS1.2) or SH(TLS1.3). Check which.
		if let Some(version) = base.supported_version {
			if base.random == HRR_MAGIC {
				parse_hello_retry_request(base, version, enable, dbg).
					map(ServerHello::HelloRetryRequest).map_err(ServerHelloParseError)
			} else {
				parse_server_hello_tls13(base, version, enable, key_ok, dbg).
					map(ServerHello::ServerHelloTls13).map_err(ServerHelloParseError)
			}
		} else {
			parse_server_hello_tls12(base, enable, dbg).map(ServerHello::ServerHelloTls12).
				map_err(ServerHelloParseError)
		}
	}
}

fn parse_hello_retry_request<'a>(msg: ServerHelloBase<'a>, version: TlsVersion, enable: RemoteFeatureSet,
	dbg: &mut DbgT) -> Result<HelloRetryRequest<'a>, _ServerHelloParseError>
{
	let exts2 = &msg.ext_data;
	//Recheck if extensions are allowed, as the previous check was for SH12/SH13/HRR, this is for HRR only.
	field!(S Extensions2
		exts::recheck_extensions(&msg.ext_data.get_mask(), ExtensionType::allowed_in_hello_retry_request)
	);
	//Cookie extension.
	let cookie = extension!(exts2 dbg COOKIE |extn, extv, dbg|{
		let c = field!(E Cookie extv.slice(LEN_COOKIE));
		debug!(E dbg extn "{clen} bytes", clen=c.len());
		Ok(c)
	});
	//Supported groups extension.
	let requested_group = extension!(exts2 dbg KEY_SHARE |extn, extv, dbg|{
		let group: NamedGroup = field!(E SelectedGroup extv.item(|g|{
			crate::ask_group(enable, g)
		}));
		debug!(E dbg extn "Server requesting group {group}");
		Ok(group)
	});
	exts::report_unknown_extensions(msg.extensions, dbg);

	Ok(HelloRetryRequest {
		tls_version: version,
		session_id: msg.session_id,
		cipher_suite: msg.cipher_suite,
		cookie,
		requested_group,
		extensions: msg.extensions,
		extension_mask: msg.ext_data.get_mask(),
	})
}

fn parse_server_hello_tls12<'a>(msg: ServerHelloBase<'a>, enable: RemoteFeatureSet, dbg: &mut DbgT) ->
	Result<ServerHelloTls12<'a>, _ServerHelloParseError>
{
	let exts2 = &msg.ext_data;
	debug!(dbg "Using TLS version 1.2");

	//Recheck if extensions are allowed, as the previous check was for SH12/SH13/HRR, this is for SH12 only.
	field!(S Extensions2
		exts::recheck_extensions(&msg.ext_data.get_mask(), ExtensionType::allowed_in_server_hello_tls12)
	);

	let acknowledge_server_name = extension!(exts2 dbg SERVER_NAME exts::ext_acknowledge).is_some();

	let max_fragment_length = extension!(exts2 dbg MAX_FRAGMENT_LENGTH|extn, extv, dbg|{
		exts::serverext_max_fragment_length(extn, extv, enable, dbg)
	});

	let acknowledge_client_certificate_url = extension!(exts2 dbg CLIENT_CERTIFICATE_URL exts::ext_acknowledge).
		is_some();

	let acknowledge_trusted_ca_keys = extension!(exts2 dbg TRUSTED_CA_KEYS exts::ext_acknowledge).is_some();

	let use_truncated_hmac = extension!(exts2 dbg TRUNCATED_HMAC exts::ext_using).is_some();

	let acknowledge_status_request = extension!(exts2 dbg STATUS_REQUEST |extn, _, dbg|{
		debug!(E dbg extn "Maybe sending certificate status");
		Ok(())
	}).is_some();

	let user_mapping_types = extension!(exts2 dbg USER_MAPPING |extn, extv, dbg|{
		// struct {
		//	 UserMappingType user_mapping_types<1..2^8-1>;
		// } UserMappingTypeList;
		//
		// - RFC4681 section 2.
		let umt = field!(E UserMappingTypes
			extv.list_of::<btls_aux_tls_iana::UserMappingType>(LEN_USER_MAPPING_TYPE_LIST)
		);
		for i in umt.iter_all() { if !crate::ask_user_mapping_type(enable, i) {
			field!(E UserMappingTypes Err(VectorError::BogusElement(i)));
		}}
		debug!(E dbg extn "{umt:?}");
		Ok(umt)
	}).unwrap_or_default();

	let client_authz = extension!(exts2 dbg CLIENT_AUTHZ |extn, extv, dbg|{
		exts::serverext_peer_authz(extn, extv, enable, dbg, crate::ask_client_authz_format)
	}).unwrap_or_default();

	let server_authz = extension!(exts2 dbg SERVER_AUTHZ |extn, extv, dbg|{
		exts::serverext_peer_authz(extn, extv, enable, dbg, crate::ask_server_authz_format)
	}).unwrap_or_default();

	let cert_type = extension!(exts2 dbg CERT_TYPE |extn, extv, dbg|{
		let c: CertificateType = field!(E CertificateType extv.item(|c|{
			crate::ask_certificate_type(enable, c)
		}));
		debug!(E dbg extn "Using certificate type {c}");
		Ok(c)
	});

	let ec_point_formats = extension!(exts2 dbg EC_POINT_FORMATS |extn, extv, dbg|{
		// struct {
		//	 ECPointFormat ec_point_format_list<1..2^8-1>
		// } ECPointFormatList;
		//
		// - RFC4492 section 5.1.2
		let epfl = field!(E EcPointFormatList
			extv.list_of::<btls_aux_tls_iana::EcPointFormat>(LEN_EC_POINT_FORMATS)
		);
		//There might be no relation to client list.
		debug!(E dbg extn "{epfl:?}");
		Ok(epfl)
	}).unwrap_or_default();

	let use_srtp = extension!(exts2 dbg USE_SRTP |extn, extv, dbg|{
		exts::serverext_use_srtp(extn, extv, enable, dbg)
	});

	let heartbeat = extension!(exts2 dbg HEARTBEAT exts::ext_heartbeat);

	let chosen_alpn_protocol =  extension!(exts2 dbg APPLICATION_LAYER_PROTOCOL_NEGOTIATION |extn, extv, dbg|{
		exts::serverext_alpn(extn, extv, enable, dbg)
	});

	let acknowledge_status_request_v2 = extension!(exts2 dbg STATUS_REQUEST_V2 |extn, _, dbg|{
		debug!(E dbg extn "Maybe sending certificate status");
		Ok(())
	}).is_some();

	let signed_certificate_timestamps = extension!(exts2 dbg SIGNED_CERTIFICATE_TIMESTAMP |extn, extv, dbg|{
		// struct {
		//	 SerializedSCT sct_list <1..2^16-1>;
		// } SignedCertificateTimestampList;
		//
		// - RFC6962 section 3.3.
		let sct = field!(E SctList
			extv.list_of::<crate::iters2::SerializedSCT<'a>>(LEN_SERIALIZED_SCT_LIST)
		);
		debug!(E dbg extn "Server sent {scnt} items", scnt=sct.count_all(true));
		Ok(sct)
	}).unwrap_or_default();
	
	let client_certificate_type = extension!(exts2 dbg CLIENT_CERTIFICATE_TYPE |extn, extv, dbg|{
		exts::serverext_peer_certificate_type(extn, extv, enable, dbg,
			crate::ask_client_certificate_type, crate::errors::Struct::ClientCertificateType)
	});

	let server_certificate_type = extension!(exts2 dbg SERVER_CERTIFICATE_TYPE |extn, extv, dbg|{
		exts::serverext_peer_certificate_type(extn, extv, enable, dbg,
			crate::ask_server_certificate_type, crate::errors::Struct::ServerCertificateType)
	});

	let use_encrypt_then_mac = extension!(exts2 dbg ENCRYPT_THEN_MAC exts::ext_using).is_some();

	let use_extended_master_secret = extension!(exts2 dbg EXTENDED_MASTER_SECRET exts::ext_using).is_some();

	let token_binding = extension!(exts2 dbg TOKEN_BINDING |extn, extv, dbg|{
		// struct {
		//	 TB_ProtocolVersion token_binding_version;
		//	 TokenBindingKeyParameters key_parameters_list<1..2^8-1>
		// } TokenBindingParameters;
		//
		// - RFC8472 section 2.
		use btls_aux_tls_iana::TokenBindingKeyParameter;
		let major: u8 = field!(E TbVerMajor extv.cp());
		let minor: u8 = field!(E TbVerMinor extv.cp());
		let kpl: TokenBindingKeyParameter = field!(E KeyParametersList2
			extv.list_one(LEN_TB_KEY_PARAMETERS_LIST, |p|{
				crate::ask_token_binding_key_parameter(enable, p)
			})
		);
		debug!(E dbg extn "version {major}.{minor} parameter {kpl}");
		Ok((major, minor, kpl))
	});

	let use_tls_lts = extension!(exts2 dbg RECORD_SIZE_LIMIT exts::ext_using).is_some();

	let record_size_limit = extension!(exts2 dbg RECORD_SIZE_LIMIT exts::ext_record_size_limit);

	let session_ticket = extension!(exts2 dbg SESSION_TICKET |extn, extv, dbg|{
		debug!(E dbg extn "{extvl} bytes", extvl=extv.length());
		Ok(extv.all())
	});

	let chosen_ekt_cipher = extension!(exts2 dbg SUPPORTED_EKT_CIPHERS |extn, extv, dbg|{
		exts::serverext_supported_ekt_ciphers(extn, extv, enable, dbg)
	});

	let transparency_info = extension!(exts2 dbg TRANSPARENCY_INFO |extn, extv, dbg|{
		// struct {
		//	 SerializedTransItem trans_item_list<1..2^16-1>;
		// } TransItemList;
		//
		// - RFC9162 section 6.3
		let ti = field!(E TransItemList
			extv.list_of::<crate::iters2::SerializedTransItem<'a>>(LEN_SERIALIZED_TRANS_ITEM_LIST)
		);
		debug!(E dbg extn "Server sent {tcnt} items", tcnt=ti.count_all(true));
		Ok(ti)
	}).unwrap_or_default();

	let cid_old = extension!(exts2 dbg CONNECTION_ID_OLD exts::ext_connection_id);
	let cid = extension!(exts2 dbg CONNECTION_ID exts::ext_connection_id);

	let external_id_hash = extension!(exts2 dbg EXTERNAL_ID_HASH exts::ext_external_id_hash);
	let external_session_id = extension!(exts2 dbg EXTERNAL_SESSION_ID exts::ext_external_session_id);

	let dnssec_chain = extension!(exts2 dbg DNSSEC_CHAIN exts::serverext_dnssec_chain);

	let renegotiated_connection = extension!(exts2 dbg RENEGOTIATION_INFO |extn, extv, dbg|{
		let rc = field!(E RenegotiatedConnection extv.slice(LEN_RENEGOTIATED_CONNECTION));
		if rc.len() > 0 {
			debug!(E dbg extn "Renegotiating <{rc}>", rc=Hexdump(rc));
		} else {
			debug!(E dbg extn "New session");
		}
		Ok(rc)
	});

	exts::report_unknown_extensions(msg.extensions, dbg);

	const DOWNGRADE_SENTINEL: [u8; 8] = [0x44, 0x4F, 0x57, 0x4E, 0x47, 0x52, 0x44, 0x01];
	//Random is always 32 octets by type.
	let sentinel = &msg.random[24..32];
	fail_if!(sentinel == &DOWNGRADE_SENTINEL, _ServerHelloParseError::DowngradeAttack);
	fail_if!(token_binding.is_some() && !use_extended_master_secret,
		_ServerHelloParseError::TokenBindingRequiresEms);
	fail_if!(cert_type.is_some() && (client_certificate_type.is_some() ||
		server_certificate_type.is_some()), _ServerHelloParseError::CertTypeConflict);
	fail_if!(cid.is_some() && cid_old.is_some(), _ServerHelloParseError::CidConflict);
	let client_ct = client_certificate_type.or(cert_type).unwrap_or(CertificateType::X509);
	let server_ct = server_certificate_type.or(cert_type).unwrap_or(CertificateType::X509);
	Ok(ServerHelloTls12 {
		tls_version: msg.server_version,
		random: msg.random,
		session_id: msg.session_id,
		cipher_suite: msg.cipher_suite,
		compression_method: msg.compression_method,
		extensions: msg.extensions,
		acknowledge_server_name,
		acknowledge_client_certificate_url,
		acknowledge_trusted_ca_keys,
		use_truncated_hmac,
		acknowledge_status_request,
		acknowledge_status_request_v2,
		chosen_client_certificate_type: client_ct,
		chosen_server_certificate_type: server_ct,
		use_encrypt_then_mac,
		use_extended_master_secret,
		signed_certificate_timestamps,
		transparency_info,
		renegotiated_connection,
		use_srtp,
		heartbeat,
		chosen_alpn_protocol,
		token_binding,
		external_id_hash,
		external_session_id,
		max_fragment_length,
		use_tls_lts,
		record_size_limit,
		chosen_ekt_cipher,
		session_ticket,
		dnssec_chain,
		client_authz,
		server_authz,
		ec_point_formats,
		user_mapping_types,
		extension_mask: msg.ext_data.get_mask(),
	})
}

fn parse_server_hello_tls13<'a>(msg: ServerHelloBase<'a>, version: TlsVersion, enable: RemoteFeatureSet,
	key_ok: KeyCheck, dbg: &mut DbgT) -> Result<ServerHelloTls13<'a>, _ServerHelloParseError>
{
	let exts2 = &msg.ext_data;
	//Check compression method.
	fail_if!(msg.compression_method != CompressionMethod::NULL,
		_ServerHelloParseError::CompressionMethodTls13(msg.compression_method)
	);
	//Recheck if extensions are allowed, as the previous check was for SH12/SH13/HRR, this is SH13 only.
	field!(S Extensions2
		exts::recheck_extensions(&msg.ext_data.get_mask(), ExtensionType::allowed_in_server_hello_tls13)
	);

	//TLS cert with extern psk extension.
	let use_cert_with_extern_psk = extension!(exts2 dbg TLS_CERT_WITH_EXTERN_PSK |extn, _, dbg|{
		debug!(E dbg extn "Will use Certificates with external PSK");
		Ok(())
	}).is_some();
	//Pre shared key extension.
	let pre_shared_key = extension!(exts2 dbg PRE_SHARED_KEY |extn, extv, dbg|{
		use crate::errors::PskIndex as PskIdxErr;
		let idx: u16 = field!(E SelectedIdentity extv.cp().map_err(PskIdxErr::Read));
		if !crate::ask_pre_shared_key(enable, idx) {
			field!(E SelectedIdentity Err(PskIdxErr::InvalidIndex(idx)));
		}
		debug!(E dbg extn "Will use PSK key index {idx}");
		Ok(idx)
	});
	//Key share extension.
	let share = extension!(exts2 dbg KEY_SHARE |extn, extv, dbg|{
		let group: NamedGroup = field!(E ServerShareGroup extv.item(|g|{
			crate::ask_group(enable, g)
		}));
		let key_exchange = field!(E ServerShareKeyExchange extv.slice(LEN_KEY_SHARE_ENTRY_KEX));
		fail_if!(!key_ok(group, key_exchange), ExtensionError::BadCiphertext(group));
		debug!(E dbg extn "Key share for group {group} ({kexlen} bytes)", kexlen=key_exchange.len());
		Ok((group, key_exchange))
	});
	//Connection ID extension.
	let cid_old = extension!(exts2 dbg CONNECTION_ID_OLD exts::ext_connection_id);
	let cid = extension!(exts2 dbg CONNECTION_ID exts::ext_connection_id);
	//Sequence number encryption algorithm.
	let sequence_number_encryption_algorithm = extension!(exts2 dbg SEQUENCE_NUMBER_ENCRYPTION_ALGORITHMS
		|extn, extv, dbg|{
		let alg: SequenceNumberEncryptionAlgorithm = field!(E SelectedAlg extv.item(|a|{
			crate::ask_seqnum_encalg(enable, a)
		}));
		debug!(E dbg extn "Using sequence number encryption {alg}");
		Ok(alg)
	});
	//Unknown extensions.
	exts::report_unknown_extensions(msg.extensions, dbg);
	fail_if!(use_cert_with_extern_psk && (pre_shared_key.is_none() || share.is_none()),
		_ServerHelloParseError::CertExternPskReq);
	fail_if!(cid.is_some() && cid_old.is_some(), _ServerHelloParseError::CidConflict);
	match (share.is_some(), pre_shared_key.is_some()) {
		//PSK+DH.
		(true, true) => if !crate::ask_psk_key_exchange_mode(enable, PskKeyExchangeMode::PSK_DHE_KE) {
			fail!(_ServerHelloParseError::BogusKeyExchangeMode(PskKeyExchangeMode::PSK_DHE_KE));
		},
		//DH only.
		(true, false) => (),	//Not special.
		//PSK only.
		(false, true) => if !crate::ask_psk_key_exchange_mode(enable, PskKeyExchangeMode::PSK_KE) {
			fail!(_ServerHelloParseError::BogusKeyExchangeMode(PskKeyExchangeMode::PSK_KE));
		},
		//None!
		(false, false) => fail!(_ServerHelloParseError::NoKeyExchange)
	}
	Ok(ServerHelloTls13 {
		tls_version: version,
		random: msg.random,
		session_id: msg.session_id,
		cipher_suite: msg.cipher_suite,
		extensions: msg.extensions,
		use_cert_with_extern_psk,
		pre_shared_key,
		share,
		cid,
		cid_old,
		sequence_number_encryption_algorithm,
		extension_mask: msg.ext_data.get_mask(),
	})
}

///Parsed Hello Retry Request message.
#[derive(Clone)]
pub struct HelloRetryRequest<'a>
{
	tls_version: TlsVersion,
	session_id: &'a [u8],
	cipher_suite: CipherSuite,
	cookie: Option<&'a [u8]>,
	requested_group: Option<NamedGroup>,
	extensions: ListOf<'a,Extension<'a>>,
	extension_mask: [u32;exts::EXTENSION_WORDS],
}

///Parsed TLS 1.3 Server Hello message.
#[derive(Clone)]
pub struct ServerHelloTls13<'a>
{
	tls_version: TlsVersion,
	random: [u8;32],
	session_id: &'a [u8],
	cipher_suite: CipherSuite,
	extensions: ListOf<'a,Extension<'a>>,
	use_cert_with_extern_psk: bool,
	pre_shared_key: Option<u16>,
	share: Option<(NamedGroup, &'a [u8])>,
	cid: Option<&'a [u8]>,
	cid_old: Option<&'a [u8]>,
	sequence_number_encryption_algorithm: Option<SequenceNumberEncryptionAlgorithm>,
	extension_mask: [u32;exts::EXTENSION_WORDS],
}

///Parsed TLS 1.2 Server Hello message.
#[derive(Clone)]
pub struct ServerHelloTls12<'a>
{
	tls_version: TlsVersion,
	random: [u8;32],
	session_id: &'a [u8],
	cipher_suite: CipherSuite,
	compression_method: CompressionMethod,
	extensions: ListOf<'a,Extension<'a>>,
	acknowledge_server_name: bool,
	acknowledge_client_certificate_url: bool,
	acknowledge_trusted_ca_keys: bool,
	use_truncated_hmac: bool,
	acknowledge_status_request: bool,
	acknowledge_status_request_v2: bool,
	chosen_client_certificate_type: CertificateType,
	chosen_server_certificate_type: CertificateType,
	use_encrypt_then_mac: bool,
	use_extended_master_secret: bool,
	user_mapping_types: ListOf<'a,btls_aux_tls_iana::UserMappingType>,
	client_authz: ListOf<'a,btls_aux_tls_iana::AuthorizationDataFormat>,
	server_authz: ListOf<'a,btls_aux_tls_iana::AuthorizationDataFormat>,
	ec_point_formats: ListOf<'a,btls_aux_tls_iana::EcPointFormat>,
	signed_certificate_timestamps: ListOf<'a,crate::iters2::SerializedSCT<'a>>,
	transparency_info: ListOf<'a,crate::iters2::SerializedTransItem<'a>>,
	renegotiated_connection: Option<&'a [u8]>,
	use_srtp: Option<(btls_aux_tls_iana::DtlsSrtpProtectionProfile, &'a [u8])>,
	heartbeat: Option<bool>,
	chosen_alpn_protocol: Option<btls_aux_tls_iana::AlpnProtocolId<'a>>,
	token_binding: Option<(u8, u8, btls_aux_tls_iana::TokenBindingKeyParameter)>,
	external_id_hash: Option<&'a [u8]>,
	external_session_id: Option<&'a [u8]>,
	max_fragment_length: Option<crate::MaximumFragmentLength>,
	use_tls_lts: bool,
	record_size_limit: Option<u16>,
	chosen_ekt_cipher: Option<btls_aux_tls_iana::EktCipher>,
	session_ticket: Option<&'a [u8]>,
	dnssec_chain: Option<exts::DnssecChain<'a>>,
	extension_mask: [u32;exts::EXTENSION_WORDS],
}

macro_rules! has_extension
{
	() => {
		///Was specified extension sent?
		pub fn has_extension(&self, ext: ExtensionType) -> bool
		{
			match exts::extension_bit(ext) {
				Some(bit) => {
					if let Some(rw) = self.extension_mask.get(bit / 32) {
						*rw & 1u32 << bit % 32 != 0
					} else {
						false
					}
				},
				None => self.extensions.iter_all().any(|e|{
					e.extension_type == ext
				}),
			}
		}
	}
}

impl<'a> HelloRetryRequest<'a>
{
	///Get the TLS version in use.
	pub fn get_tls_version(&self) -> TlsVersion { self.tls_version }
	///Get fake Session ID the server echoed back to client.
	pub fn get_session_id(&self) -> &'a [u8] { self.session_id }
	///Get the cipher suite the server selected.
	pub fn get_cipher_suite(&self) -> CipherSuite { self.cipher_suite }
	///Get the cookie the server requests the client to echo, if any.
	pub fn get_cookie(&self) -> Option<&'a [u8]> { self.cookie }
	///Get the group/KEM selected by the server.
	///
	///The client should send only one share, for this group/KEM.
	pub fn get_group(&self) -> Option<NamedGroup> { self.requested_group }
	///Get extensions.
	///
	///This can be used to parse extensions other than the explicitly supported ones.
	pub fn get_extensions(&self) -> ListOf<'a, Extension<'a>> { self.extensions }
	has_extension!();
}

impl<'a> ServerHelloTls12<'a>
{
	///Get the TLS version in use.
	pub fn get_tls_version(&self) -> TlsVersion { self.tls_version }
	///Get Server Random.
	pub fn get_random(&self) -> [u8;32] { self.random }
	///Get Session ID the server created.
	pub fn get_session_id(&self) -> &'a [u8] { self.session_id }
	///Get the cipher suite the server selected.
	pub fn get_cipher_suite(&self) -> CipherSuite { self.cipher_suite }
	///Get the compression method the server selected.
	pub fn get_compression_method(&self) -> CompressionMethod { self.compression_method }
	///Get extensions.
	///
	///This can be used to parse extensions other than the explicitly supported ones.
	pub fn get_extensions(&self) -> ListOf<'a, Extension<'a>> { self.extensions }
	///Did the server acknowledge `server_name`?
	pub fn acknowledged_server_name(&self) -> bool { self.acknowledge_server_name }
	///Get the maximum fragment size, if any.
	///
	///This is old deprecated extension. Use `record_size_limit` instead.
	pub fn get_maximum_fragment_length(&self) -> Option<crate::MaximumFragmentLength>
	{
		self.max_fragment_length
	}
	///Did the server acknowledge `client_certificate_url`?
	pub fn acknowledged_client_certificate_url(&self) -> bool { self.acknowledge_client_certificate_url }
	///Did the server acknowledge `trusted_ca_keys`?
	pub fn acknowledged_trusted_ca_keys(&self) -> bool { self.acknowledge_trusted_ca_keys }
	///Will the server use Truncated HMAC?
	pub fn use_truncated_hmac(&self) -> bool { self.use_truncated_hmac }
	///Did the server acknowledge `status_request`?
	pub fn acknowledged_status_request(&self) -> bool { self.acknowledge_status_request }
	///Get User Mapping Types the both sides support.
	///
	///This is the server-supported list intersected with client list from client hello.
	pub fn get_user_mapping_types(&self) -> ListOf<'a,btls_aux_tls_iana::UserMappingType>
	{
		self.user_mapping_types
	}
	///Get Client Authorization Data Formats both sides support.
	///
	///This is the server-supported list intersected with client list from client hello.
	pub fn get_client_authorization_data_formats(&self) -> ListOf<'a,btls_aux_tls_iana::AuthorizationDataFormat>
	{
		self.client_authz
	}
	///Get Server Authorization Data Formats both sides support.
	///
	///This is the server-supported list intersected with client list from client hello.
	pub fn get_server_authorization_data_formats(&self) -> ListOf<'a,btls_aux_tls_iana::AuthorizationDataFormat>
	{
		self.server_authz
	}
	///Get EC Point Formats the server supports.
	///
	///This list has nothing to do with the client list.
	pub fn get_ec_point_formats(&self) -> ListOf<'a,btls_aux_tls_iana::EcPointFormat> { self.ec_point_formats }
	///Get the DTLS-SRTP protection profile to use, if any.
	pub fn get_dtls_srtp_protection_profile(&self) -> Option<btls_aux_tls_iana::DtlsSrtpProtectionProfile>
	{
		self.use_srtp.map(|(x,_)|x)
	}
	///Get the DTLS-SRTP Master Key Indicator to use.
	///
	///This value is meaningless if `get_dtls_srtp_protection_profile()` returns `None`.
	pub fn get_dtls_srtp_mki(&self) -> &'a [u8] { self.use_srtp.map(|(_,y)|y).unwrap_or(b"") }
	///Get the heartbeat support of the server.
	///
	///`Some(true)` means server has granted permission for client to send heartbeat messages to the server.
	///`Some(false)` means client MUST NOT send heartbeat messages to the server, but server might be able to
	///send heartbeat to client.
	pub fn get_heartbeat(&self) -> Option<bool> { self.heartbeat }
	///Get the server-chosen ALPN Protocol ID, if any.
	///
	///If this value is `None`, the server did not pick any explicit ALPN.
	pub fn get_alpn_protocol_id(&self) -> Option<btls_aux_tls_iana::AlpnProtocolId<'a>>
	{
		self.chosen_alpn_protocol
	}
	///Did the server acknowledge `status_request_v2`?
	pub fn acknowledged_status_request_v2(&self) -> bool { self.acknowledge_status_request_v2 }
	///Get the stapled Certificate Transparency v1 Signed Certificate Timestamps the server sent.
	pub fn get_signed_certificate_timestamps(&self) -> ListOf<'a,crate::iters2::SerializedSCT<'a>>
	{
		self.signed_certificate_timestamps
	}
	///Get the server-chosen client certificate type.
	///
	///If the server did not indicate otherwise, this is `X509`. In case the server used the obsolete
	///`cert_type` extension, this reflects that value.
	pub fn get_client_certificate_type(&self) -> CertificateType { self.chosen_client_certificate_type }
	///Get the server-chosen server certificate type.
	///
	///If the server did not indicate otherwise, this is `X509`. In case the server used the obsolete
	///`cert_type` extension, this reflects that value.
	pub fn get_server_certificate_type(&self) -> CertificateType { self.chosen_server_certificate_type }
	///Will the server use Encrypt-then-MAC?
	pub fn use_encrypt_then_mac(&self) -> bool { self.use_encrypt_then_mac }
	///Will the server use Extended Master Secret?
	pub fn use_extended_master_secret(&self) -> bool { self.use_extended_master_secret }
	///Get the server-chosen Token Binding Key Parameter.
	///
	///If this is `None`, client does not support Token Binding.
	pub fn get_token_binding_key_parameter(&self) -> Option<btls_aux_tls_iana::TokenBindingKeyParameter>
	{
		self.token_binding.map(|(_,_,z)|z)
	}
	///Get Token Binding version the server will use.
	///
	///The version is given as `(major, minor)`.
	///
	///This is meaningless if `get_token_binding_key_parameter()` gave `None`.
	pub fn get_token_binding_version(&self) -> (u8, u8)
	{
		self.token_binding.map(|(x,y,_)|(x,y)).unwrap_or((0,0))
	}
	///Will the server use TLS-LTS?
	pub fn use_tls_lts(&self) -> bool { self.use_tls_lts }
	///Get maximum size of record plaintext for encrypted record the server supports.
	///
	///If standard TLS limits apply, this is `None`.
	pub fn get_record_size_limit(&self) -> Option<usize> { self.record_size_limit.map(|x|x as usize) }
	///Return the entiere raw session ticket the server sent, if any.
	///
	///Warning: This feature is severely flawed.
	pub fn get_session_ticket(&self) -> Option<&'a [u8]> { self.session_ticket }
	///Get the server-chosen EKT cipher, if any.
	pub fn get_ekt_cipher(&self) -> Option<btls_aux_tls_iana::EktCipher> { self.chosen_ekt_cipher }
	///Get the stapled Certificate Transparency v2 Transparency Items the server sent.
	pub fn get_transparency_items(&self) -> ListOf<'a,crate::iters2::SerializedTransItem<'a>>
	{
		self.transparency_info
	}
	///Get the external session ID hash, if any.
	pub fn get_external_id_hash(&self) -> Option<&'a [u8]> { self.external_id_hash }
	///Get the external session ID, if any.
	pub fn get_external_session_id(&self) -> Option<&'a [u8]> { self.external_session_id }
	///Get the DNSSec chain certifying the certificate, if any.
	pub fn get_dnssec_chain(&self) -> Option<exts::DnssecChain<'a>> { self.dnssec_chain }
	///Get ID of connection the server is trying to renegotiate.
	///
	///If server is not trying to renegotiate, `Some(b"")`.
	///
	///Warning: Server is exploitably vulernable if this is `None`. Clients SHOULD CONSIDER terminating the
	///handshake.
	pub fn get_renegotiated_connection(&self) -> Option<&'a [u8]> { self.renegotiated_connection }
	has_extension!();
}

impl<'a> ServerHelloTls13<'a>
{
	///Get the TLS version in use.
	pub fn get_tls_version(&self) -> TlsVersion { self.tls_version }
	///Get Server Random.
	pub fn get_random(&self) -> [u8;32] { self.random }
	///Get fake Session ID the server echoed back to client.
	pub fn get_session_id(&self) -> &'a [u8] { self.session_id }
	///Get the cipher suite the server selected.
	pub fn get_cipher_suite(&self) -> CipherSuite { self.cipher_suite }
	///Get extensions.
	///
	///This can be used to parse extensions other than the explicitly supported ones.
	pub fn get_extensions(&self) -> ListOf<'a, Extension<'a>> { self.extensions }
	///The server will use certificate authentication with external PSK.
	pub fn use_tls_cert_with_extern_psk(&self) -> bool { self.use_cert_with_extern_psk }
	///The Pre Shared Key index the server will use, if any.
	pub fn get_pre_shared_key_index(&self) -> Option<usize> { self.pre_shared_key.map(|x|x as usize) }
	///Get the server key share, if any.
	///
	///The first part is the group/KEM selected, the second is the KEM ciphertext.
	pub fn get_key_share(&self) -> Option<(NamedGroup, &'a [u8])> { self.share }
	///Get the CID, if any.
	pub fn get_cid(&self) -> Option<&'a [u8]> { self.cid }
	///Get the obsolete CID, if any.
	pub fn get_cid_old(&self) -> Option<&'a [u8]> { self.cid_old }
	///Get the selected sequence number encryption algorithm.
	pub fn get_sequence_number_encryption_algorithm(&self) -> Option<SequenceNumberEncryptionAlgorithm>
	{
		self.sequence_number_encryption_algorithm
	}
	has_extension!();
}

struct ServerHelloBase<'a>
{
	ext_data: exts::Extensions<'a>,
	server_version: TlsVersion,
	random: [u8;32],
	session_id: &'a [u8],
	cipher_suite: CipherSuite,
	compression_method: CompressionMethod,
	extensions: ListOf<'a,Extension<'a>>,
	supported_version: Option<TlsVersion>,
}

fn parse_base<'a>(mut msg: Message<'a>, enable: RemoteFeatureSet, dbg: &mut DbgT) ->
	Result<ServerHelloBase<'a>, _ServerHelloParseError>
{
	// struct {
	//	 ProtocolVersion server_version;
	//	 Random random;
	//	 SessionID session_id;
	//	 CipherSuite cipher_suite;
	//	 CompressionMethod compression_method;
	//	 select (extensions_present) {
	//		 case false:
	//			 struct {};
	//		 case true:
	//			 Extension extensions<0..2^16-1>;
	//	 };
	// } ServerHello;
	//
	// - RFC5246 section 7.4.1.3
	//This is special: It is fixed to TLS 1.2.
	let server_version: TlsVersion = field!(S ServerVersion msg.item(|v|v == TlsVersion::TLS_1_2));
	let random: [u8;32] = field!(S Random msg.array());
	let session_id = field!(S SessionId2 msg.slice(LEN_SESSION_ID));
	debug!(dbg "Session ID length: {slen}", slen=session_id.len());

	let cipher_suite: CipherSuite = field!(S CipherSuite msg.item(|cs|{
		crate::ask_cipher_suite(enable, cs)
	}));
	debug!(dbg "Server selected ciphersuite {cipher_suite}");
	let compression_method: CompressionMethod = field!(S CompressionMethod msg.item(|cm|{
		crate::ask_compression_method(enable, cm)
	}));
	if compression_method != CompressionMethod::NULL {
		debug!(dbg "Using compression {compression_method}");
	}
	let extensions = if msg.more() {
		field!(S Extensions msg.list_of::<Extension<'a>>(LEN_EXTENSION_LIST))
	} else {
		Default::default()
	};
	msg.assert_end(_ServerHelloParseError::JunkAfterExtensions)?;
	//Now scan extensions for disallowed ones. The extensions have to pass two checks:
	//1) They are allowed by RemoteFeatureSet, or be Cookie (which is magic).
	//2) They are allowed for HRR, SH12 or SH13.
	let exts2 = field!(S Extensions2 exts::check_extensions_block(extensions, |ext|{
		let ok_spec = ext.allowed_in_hello_retry_request() ||
			ext.allowed_in_server_hello_tls12() ||
			ext.allowed_in_server_hello_tls13();
		let ok_req = crate::ask_extension(enable, ext) || ext == ExtensionType::COOKIE;
		ok_spec && ok_req
	}));

	//Look for supported versions. We need to do this, as we have no idea if this is TLS 1.2 or 1.3, and
	//that extension is the only way to determine it.
	let supported_version = extension!(exts2 dbg SUPPORTED_VERSIONS |extn, extv, dbg|{
		let version: TlsVersion = field!(E SelectedVersion extv.item(|v|{
			crate::ask_version(enable, v)
		}));
		debug!(E dbg extn "Server selected {version}");
		Ok(version)
	});
	
	Ok(ServerHelloBase {
		ext_data: exts2,
		server_version,
		random,
		session_id,
		cipher_suite,
		compression_method,
		extensions,
		supported_version
	})
}
