use std::arch::x86_64::_rdtsc;

#[inline(never)]
fn call_sort64(x: &mut [u16;64], n: usize)
{
	btls_aux_tls_struct::sort_test::sort64(x, n)
}

#[inline(never)]
fn call_qsort(x: &mut [u16])
{
	x.sort_unstable();
}

fn main()
{
	unsafe {
		use std::io::Read;
		let mut fp = std::fs::File::open("/dev/urandom").expect("Failed to open urandom");
		let mut buf = [0u16;64];
		for l in 0..65 {
			const ITERATIONS: usize = 100000;
			let mut clocks = 0;
			let mut qclocks = 0;
			for _ in 0..ITERATIONS {
				let bbuf = std::slice::from_raw_parts_mut(buf.as_mut_ptr() as *mut u8, 128);
				fp.read_exact(bbuf).expect("Reading urandom failed");
				let mut buf2: [u16;64] = buf;
				let tmp = _rdtsc();
				call_sort64(&mut buf, l);
				clocks += _rdtsc() - tmp;
				let tmp = _rdtsc();
				call_qsort(&mut buf2[..l]);
				qclocks += _rdtsc() - tmp;
				assert_eq!(&buf[..l], &buf2[..l]);
			}
			let avg = clocks as f64 / ITERATIONS as f64;
			let qavg = qclocks as f64 / ITERATIONS as f64;
			//qavg/avg - 1
			println!("Length {l}, avg {avg} clocks (qsort {qavg} clocks), {spp}% faster",
				spp=100.0*qavg/avg-100.0);
		}
	}
}
