use crate::DbgT;
use btls_aux_fail::fail_if;
use core::marker::PhantomData;

///Error from parsing Hello Request
///
///The most notable things to query errors are the `alert()` method and the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct HelloRequestParseError(());

impl HelloRequestParseError
{
	///Get the alert code one should use for this parse error.
	pub fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		btls_aux_tls_iana::Alert::DECODE_ERROR
	}
}

impl core::fmt::Display for HelloRequestParseError
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		write!(f, "Junk after end of hello request message")
	}
}

///Error from parsing Server Hello Done
///
///The most notable things to query errors are the `alert()` method and the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct ServerHelloDoneParseError(());

impl ServerHelloDoneParseError
{
	///Get the alert code one should use for this parse error.
	pub fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		btls_aux_tls_iana::Alert::DECODE_ERROR
	}
}

impl core::fmt::Display for ServerHelloDoneParseError
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		write!(f, "Junk after end of server hello done message")
	}
}

///Error from parsing End of Early Data
///
///The most notable things to query errors are the `alert()` method and the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct EndOfEarlyDataParseError(());

impl EndOfEarlyDataParseError
{
	///Get the alert code one should use for this parse error.
	pub fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		btls_aux_tls_iana::Alert::DECODE_ERROR
	}
}

impl core::fmt::Display for EndOfEarlyDataParseError
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		write!(f, "Junk after end of end of early data message")
	}
}

///Error from parsing Finished.
///
///The most notable things to query errors are the `alert()` method and the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct FinishedParseError(Void);	//=> Not actually possible.
#[derive(Copy,Clone,Debug)]
enum Void {}

impl FinishedParseError
{
	///Get the alert code one should use for this parse error.
	pub fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		btls_aux_tls_iana::Alert::DECODE_ERROR
	}
}

impl core::fmt::Display for FinishedParseError
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		write!(f, "Junk after end of end of finished message")
	}
}


///Hello Request message
#[derive(Clone)]
pub struct HelloRequest<'a>
{
	x: PhantomData<&'a u8>
}

impl<'a> HelloRequest<'a>
{
	///Parse Hello Request message.
	pub fn parse(msg: &'a [u8], dbg: &mut DbgT) -> Result<HelloRequest<'a>, HelloRequestParseError>
	{
		// struct { } HelloRequest;
		//
		// - RFC5246 section 7.4.1.1.
		fail_if!(msg.len() > 0, HelloRequestParseError(()));
		debug!(dbg "Received");
		Ok(HelloRequest {
			x: PhantomData
		})
	}
}

///Server Hello Done message
#[derive(Clone)]
pub struct ServerHelloDone<'a>
{
	x: PhantomData<&'a u8>
}

impl<'a> ServerHelloDone<'a>
{
	///Parse Server Hello Done message.
	pub fn parse(msg: &'a [u8], dbg: &mut DbgT) -> Result<ServerHelloDone<'a>, ServerHelloDoneParseError>
	{
		// struct { } ServerHelloDone;
		//
		// - - RFC5246 section 7.4.5.
		fail_if!(msg.len() > 0, ServerHelloDoneParseError(()));
		debug!(dbg "Received");
		Ok(ServerHelloDone {
			x: PhantomData
		})
	}
}

///End of Early Data message
#[derive(Clone)]
pub struct EndOfEarlyData<'a>
{
	x: PhantomData<&'a u8>
}

impl<'a> EndOfEarlyData<'a>
{
	///Parse End of Early Data message.
	pub fn parse(msg: &'a [u8], dbg: &mut DbgT) -> Result<EndOfEarlyData<'a>, EndOfEarlyDataParseError>
	{
		// struct {} EndOfEarlyData;
		//
		// - RFC8446 section 4.5.
		fail_if!(msg.len() > 0, EndOfEarlyDataParseError(()));
		debug!(dbg "Received");
		Ok(EndOfEarlyData {
			x: PhantomData
		})
	}
}

///Finished message
#[derive(Clone)]
pub struct Finished<'a>
{
	verify_data: &'a [u8],
	x: PhantomData<&'a u8>
}

impl<'a> Finished<'a>
{
	///Get the Finished MAC.
	pub fn get_mac(&self) -> &'a [u8] { self.verify_data }
	///Parse Finished message.
	pub fn parse(msg: &'a [u8], dbg: &mut DbgT) -> Result<Finished<'a>, FinishedParseError>
	{
		// struct {
		//	opaque verify_data[Hash.length];
		// } Finished;
		//
		// - RFC8446 section 4.5.
		let verify_data = msg;
		debug!(dbg "Received");
		Ok(Finished {
			verify_data,
			x: PhantomData
		})
	}
}
