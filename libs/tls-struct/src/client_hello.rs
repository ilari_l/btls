#![allow(non_snake_case)]
use crate::DbgT;
use crate::ExtensionError;
use crate::KeyCheck;
use crate::LEN_CACHED_INFO_LIST;
use crate::LEN_CERTIFICATE_TYPE_LIST;
use crate::LEN_CIPHER_SUITE_LIST;
use crate::LEN_COMPRESSION_METHOD_LIST;
use crate::LEN_COOKIE;
use crate::LEN_CSRI2_REQLIST;
use crate::LEN_EC_POINT_FORMATS;
use crate::LEN_EKT_SUPPORTED_CIPHERS;
use crate::LEN_EXTENSION_LIST;
use crate::LEN_KEY_SHARE_LIST;
use crate::LEN_PINNING_TICKET;
use crate::LEN_PROTOCOL_LIST;
use crate::LEN_PSK_BINDER_LIST;
use crate::LEN_PSK_IDENTITY_LIST;
use crate::LEN_PSK_MODES_LIST;
use crate::LEN_RENEGOTIATED_CONNECTION;
use crate::LEN_SEQUENCE_NUMBER_ENCRYPTION_ALGORITHMS_LIST;
use crate::LEN_SESSION_ID;
use crate::LEN_SRP_I;
use crate::LEN_SRTP_MKI;
use crate::LEN_SRTP_PROTECTION_PROFILE_LIST;
use crate::LEN_TB_KEY_PARAMETERS_LIST;
use crate::LEN_TRUSTED_AUTHORITIES_LIST;
use crate::LEN_USER_MAPPING_TYPE_LIST;
use crate::LEN_VERSION_LIST;
use crate::MaximumFragmentLength;
use crate::extensions as exts;
use crate::iters2::CachedObject;
use crate::iters2::CertificateAuthority;
use crate::iters2::CertificateStatusRequestItemV2;
use crate::iters2::Extension;
use crate::iters2::IntoGenericIterator as ListOf;
use crate::iters2::IntoKeyShareIter;
use crate::iters2::IntoPreSharedKeyIter;
use crate::iters2::KeyShareEntry;
use crate::iters2::Message;
use crate::iters2::OcspStatusRequest;
use crate::iters2::PinningTicket;
use crate::iters2::ServerName;
use crate::iters2::TrustedAuthority;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::ResultExt;
use btls_aux_memory::Hexdump;
use btls_aux_tls_iana::AlpnProtocolId;
use btls_aux_tls_iana::AuthorizationDataFormat;
use btls_aux_tls_iana::CertificateCompressionAlgorithmId;
use btls_aux_tls_iana::CertificateStatusType;
use btls_aux_tls_iana::CertificateType;
use btls_aux_tls_iana::CipherSuite;
use btls_aux_tls_iana::CompressionMethod;
use btls_aux_tls_iana::DtlsSrtpProtectionProfile;
use btls_aux_tls_iana::EcPointFormat;
use btls_aux_tls_iana::EktCipher;
use btls_aux_tls_iana::ExtensionType as IanaExtension;
use btls_aux_tls_iana::PskKeyExchangeMode;
use btls_aux_tls_iana::SequenceNumberEncryptionAlgorithm;
use btls_aux_tls_iana::SignatureScheme;
use btls_aux_tls_iana::NamedGroup;
use btls_aux_tls_iana::TlsVersion;
use btls_aux_tls_iana::TokenBindingKeyParameter;
use btls_aux_tls_iana::UserMappingType;


macro_rules! field
{
	(E $location:ident $x:expr) => {
		$x.map_err(crate::errors::Struct::$location).map_err(ExtensionError::Structured)?
	};
	(S $location:ident $x:expr) => {
		$x.map_err(crate::errors::Struct::$location).map_err(_ClientHelloParseError::Structured)?
	};
}

macro_rules! extension
{
	($exts2:ident $dbg:ident $name:ident $handler:expr) => {
		$exts2.with_stdext(IanaExtension::$name, _ClientHelloParseError::JunkAfterExtension,
			_ClientHelloParseError::Extension, $dbg, $handler)?
	}
}

struct PrintName<'a>(&'a [u8]);

impl<'a> core::fmt::Display for PrintName<'a>
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		write!(f, "\"")?;
		for &b in self.0.iter() {
			if b == b'\\' {
				write!(f, "\\\\")?;
			} else if b == b'\"' {
				write!(f, "\\\"")?;
			} else if b < 32 || b > 126 {
				write!(f, "\\x{b:02x}")?;
			} else {
				let c = b as char;
				write!(f, "{c}")?;
			}
		}
		write!(f, "\"")?;
		Ok(())
	}
}

///Error from parsing Client Hello.
///
///The most notable things to query errors are the `alert()` method and the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct ClientHelloParseError(_ClientHelloParseError);

#[derive(Copy,Clone,Debug)]
enum _ClientHelloParseError
{
	JunkAfterExtensions,
	BadTls13CompressionMethod,
	NoKeyExchangeMethods,
	NeedKeyShareDheKe,
	Extension(IanaExtension, ExtensionError),
	Dependency(IanaExtension, IanaExtension),
	Conflict(IanaExtension, IanaExtension),
	Structured(crate::errors::Struct),
	JunkAfterExtension(IanaExtension),
	PskNotLast,
}

impl ClientHelloParseError
{
	///Get the alert code one should use for this parse error.
	pub fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::_ClientHelloParseError::*;
		use btls_aux_tls_iana::Alert as A;
		match self.0 {
			JunkAfterExtensions => A::DECODE_ERROR,
			BadTls13CompressionMethod => A::ILLEGAL_PARAMETER,
			Extension(_,g) => g.alert(),
			NoKeyExchangeMethods|NeedKeyShareDheKe|Dependency(_,_) => A::MISSING_EXTENSION,
			Conflict(_,_) => A::ILLEGAL_PARAMETER,
			Structured(ref x) => crate::iters2::AlertForError::alert(x),
			JunkAfterExtension(_) => A::DECODE_ERROR,
			PskNotLast => A::ILLEGAL_PARAMETER,
		}
	}
}

impl core::fmt::Display for ClientHelloParseError
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		const PSK_EXT: IanaExtension = IanaExtension::PRE_SHARED_KEY;
		use self::_ClientHelloParseError::*;
		match &self.0 {
			&JunkAfterExtensions => write!(f, "Junk after end of extensions"),
			&BadTls13CompressionMethod =>
				write!(f, "compression_methods: Non-NULL not allowed in TLS 1.3"),
			&NoKeyExchangeMethods => write!(f, "No handshake types avaialable"),
			&NeedKeyShareDheKe => write!(f, "Need key share for DHE KE"),
			&Extension(ext, ref err) => write!(f, "extension {ext}: {err}"),
			&Dependency(text, sext) => write!(f, "{text} is required by {sext}"),
			&Conflict(text, sext) => write!(f, "{text} conflicts with {sext}"),
			&Structured(ref e) => core::fmt::Display::fmt(e, f),
			&JunkAfterExtension(err) => write!(f, "Junk after end of extension {err}"),
			&PskNotLast => write!(f, "{PSK_EXT} is not the last extension"),
		}
	}
}

///Certificate Status Request
///
///This object is request for certificate status.
///
///There is no list form of this.
#[derive(Copy,Clone)]
#[non_exhaustive]
pub enum StatusRequest<'a>
{
	///No status requested.
	None,
	///OCSP single status request.
	Ocsp(OcspStatusRequest<'a>),
	///Request of unknown status type. The slice is the raw payload after the `status_type` field.
	Unknown(CertificateStatusType, &'a [u8]),
}

impl<'a> Default for StatusRequest<'a>
{
	fn default() -> StatusRequest<'a> { StatusRequest::None }
}

///Parsed TLS Client Hello.
#[derive(Clone)]
pub struct ClientHello<'a>
{
	extension_mask: [u32;exts::EXTENSION_WORDS],

	base_prefix: &'a [u8],
	binder_split: (&'a [u8], &'a [u8]),
	client_version: TlsVersion,
	random: [u8;32],
	session_id: &'a [u8],
	client_fallback: bool,
	extensions: ListOf<'a, Extension<'a>>,
	key_shares: IntoKeyShareIter<'a>,
	server_host_name: Option<&'a str>,
	server_name: ListOf<'a, ServerName<'a>>,
	maximum_fragment_length: Option<MaximumFragmentLength>,
	certificate_status_request: StatusRequest<'a>,
	certificate_status_request2: ListOf<'a,CertificateStatusRequestItemV2<'a>>,
	heartbeat: Option<bool>,
	pre_shared_keys: IntoPreSharedKeyIter<'a>,
	srp_username: Option<&'a [u8]>,
	padding_amount: usize,
	srtp_mki: &'a [u8],
	token_binding_version: (u8, u8),
	record_size_limit: Option<u16>,
	pinning_ticket: Option<Option<&'a [u8]>>,
	session_ticket: Option<&'a [u8]>,
	cookie: Option<&'a [u8]>,
	connection_id_old: Option<&'a [u8]>,
	connection_id: Option<&'a [u8]>,
	external_id_hash: Option<&'a [u8]>,
	external_session_id: Option<&'a [u8]>,
	quic_transport_parameters: Option<&'a [u8]>,
	requested_ticket_count: Option<(u8, u8)>,
	destination_port_number: Option<u16>,
	renegotiated_connection: Option<&'a [u8]>,
	supported_cipher_suites: ListOf<'a, CipherSuite>,
	supported_compression_methods: ListOf<'a, CompressionMethod>,
	supported_trusted_ca_keys: ListOf<'a,TrustedAuthority<'a>>,
	supported_user_mapping_types: ListOf<'a,UserMappingType>,
	supported_client_authz_formats: ListOf<'a,AuthorizationDataFormat>,
	supported_server_authz_formats: ListOf<'a,AuthorizationDataFormat>,
	supported_certificate_types: ListOf<'a,CertificateType>,
	supported_groups: ListOf<'a,NamedGroup>,
	supported_ec_point_formats: ListOf<'a,EcPointFormat>,
	supported_signature_algorithms: ListOf<'a,SignatureScheme>,
	supported_srtp_protection_profiles: ListOf<'a,DtlsSrtpProtectionProfile>,
	supported_alpn_protocol_ids: ListOf<'a,AlpnProtocolId<'a>>,
	supported_client_certificate_types: ListOf<'a,CertificateType>,
	supported_server_certificate_types: ListOf<'a,CertificateType>,
	supported_token_binding_key_parameters: ListOf<'a,TokenBindingKeyParameter>,
	supported_cached_objects: ListOf<'a,CachedObject<'a>>,
	supported_certificate_compression_algorithms: ListOf<'a,CertificateCompressionAlgorithmId>,
	supported_signature_algorithms_dcred: ListOf<'a,SignatureScheme>,
	supported_ekt_ciphers: ListOf<'a,EktCipher>,
	supported_versions: ListOf<'a,TlsVersion>,
	supported_psk_key_exchange_modes: ListOf<'a,PskKeyExchangeMode>,
	supported_certificate_authorities: ListOf<'a,CertificateAuthority<'a>>,
	supported_signature_algorithms_cert: ListOf<'a,SignatureScheme>,
	supported_sequence_number_encryption_algorithms: ListOf<'a,SequenceNumberEncryptionAlgorithm>,
	supports_client_certificate_url: bool,
	supports_truncated_hmac: bool,
	supports_signed_certificate_timestamp: bool,
	supports_encrypt_then_mac: bool,
	supports_extended_master_secret: bool,
	supports_tls_lts: bool,
	supports_tls_cert_with_extern_psk: bool,
	supports_early_data: bool,
	supports_post_handshake_auth: bool,
	supports_transparency_info: bool,
}

macro_rules! simple_debug
{
	(HO $f:ident $selfx:ident $var:ident) => {
		if let Some(val) = $selfx.$var {
			write!($f, ", {var}:<{val}>", var=stringify!($var), val=Hexdump(val))?;
		}
	};
	(O $f:ident $selfx:ident $var:ident) => {
		if let Some(val) = $selfx.$var {
			write!($f, ", {var}:{val:?}", var=stringify!($var))?;
		}
	};
	(B $f:ident $selfx:ident $var:ident) => {
		if $selfx.$var {
			write!($f, ", {var}", var=stringify!($var))?;
		}
	};
	(ITR $f:ident $selfx:ident $var:ident) => {
		if $selfx.$var.nontrivial() {
			write!($f, ", {var}:{val:?}", var=stringify!($var), val=$selfx.$var)?
		}
	};
	($f:ident $selfx:ident $var:ident) => {{
		write!($f, ", {var}:{val:?}", var=stringify!($var), val=$selfx.$var)?
	}};
}


impl<'a> core::fmt::Debug for ClientHello<'a>
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		write!(f, "ClientHello{{")?;
		write!(f, "client_version:{cversion}", cversion=self.client_version)?;
		write!(f, ", random:{random}", random=Hexdump(&self.random))?;
		write!(f, ", session_id:{session_id}", session_id=Hexdump(self.session_id))?;
/*
	extensions: &'a [u8],
	extension_mask: [u32;exts::EXTENSION_WORDS],
	key_shares: KeyShareIter<'a>,
*/
		if self.server_name.iter().count() > 0 {
			write!(f, ", server_name:{sname:?}", sname=self.server_name.iter())?;
		}
		simple_debug!(O f self server_host_name);
		simple_debug!(O f self maximum_fragment_length);
/*
	ext_status_request: StatusRequest<'a>,
*/
		simple_debug!(HO f self srp_username);
		if self.supported_srtp_protection_profiles.nontrivial() {
			write!(f, ", srtp_mki:<{mki}>", mki=Hexdump(self.srtp_mki))?;
		}
		simple_debug!(O f self heartbeat);
		simple_debug!(ITR f self certificate_status_request2);
		simple_debug!(f self padding_amount);
/*
	ext_token_binding__version: (u8, u8),
*/
		simple_debug!(O f self record_size_limit);
/*
	ext_ticket_pinning: Option<TicketPinningRequest<'a>>,
*/
		simple_debug!(HO f self session_ticket);
/*
	ext_pre_shared_key: PreSharedKeyIter<'a>,
*/
		simple_debug!(HO f self cookie);
		simple_debug!(HO f self connection_id_old);
		simple_debug!(HO f self connection_id);
		simple_debug!(HO f self external_id_hash);
		simple_debug!(HO f self external_session_id);
		simple_debug!(HO f self quic_transport_parameters);
		simple_debug!(O f self requested_ticket_count);
		simple_debug!(O f self destination_port_number);
		simple_debug!(HO f self renegotiated_connection);

		simple_debug!(f self supported_cipher_suites);
		simple_debug!(f self supported_compression_methods);
		simple_debug!(ITR f self supported_trusted_ca_keys);
		simple_debug!(ITR f self supported_user_mapping_types);
		simple_debug!(ITR f self supported_client_authz_formats);
		simple_debug!(ITR f self supported_server_authz_formats);
		simple_debug!(ITR f self supported_certificate_types);
		simple_debug!(ITR f self supported_groups);
		simple_debug!(ITR f self supported_ec_point_formats);
		simple_debug!(ITR f self supported_signature_algorithms);
		simple_debug!(ITR f self supported_srtp_protection_profiles);
		simple_debug!(ITR f self supported_alpn_protocol_ids);
		simple_debug!(ITR f self supported_client_certificate_types);
		simple_debug!(ITR f self supported_server_certificate_types);
		simple_debug!(ITR f self supported_token_binding_key_parameters);
		simple_debug!(ITR f self supported_cached_objects);
		simple_debug!(ITR f self supported_certificate_compression_algorithms);
		simple_debug!(ITR f self supported_signature_algorithms_dcred);
		simple_debug!(ITR f self supported_ekt_ciphers);
		simple_debug!(ITR f self supported_versions);
		simple_debug!(ITR f self supported_psk_key_exchange_modes);
		simple_debug!(ITR f self supported_certificate_authorities);
		simple_debug!(ITR f self supported_signature_algorithms_cert);

		simple_debug!(B f self supports_client_certificate_url);
		simple_debug!(B f self supports_truncated_hmac);
		simple_debug!(B f self supports_signed_certificate_timestamp);
		simple_debug!(B f self supports_encrypt_then_mac);
		simple_debug!(B f self supports_extended_master_secret);
		simple_debug!(B f self supports_tls_lts);
		simple_debug!(B f self supports_tls_cert_with_extern_psk);
		simple_debug!(B f self supports_early_data);
		simple_debug!(B f self supports_post_handshake_auth);
		simple_debug!(B f self supports_transparency_info);
		write!(f, "}}")?;
		Ok(())
	}
}

fn check_dep(etrk: &exts::Extensions, requirer: IanaExtension, requiree: IanaExtension) ->
	Result<(), _ClientHelloParseError>
{
	fail_if!(etrk.in_set(requirer) && !etrk.in_set(requiree),
		_ClientHelloParseError::Dependency(requiree, requirer));
	Ok(())
}

fn conflict(etrk: &exts::Extensions, a: IanaExtension, b: IanaExtension) ->
	Result<(), _ClientHelloParseError>
{
	fail_if!(etrk.in_set(a) && etrk.in_set(b), _ClientHelloParseError::Conflict(a, b));
	Ok(())
}

fn parse_ch<'a>(mut msg: Message<'a>, key_ok: KeyCheck, dbg: &mut DbgT) ->
	Result<ClientHello<'a>, _ClientHelloParseError>
{
	use self::_ClientHelloParseError::*;
	let o_msg = msg.as_inner();
	let orig_size = msg.length();
	// struct {
	//	 ProtocolVersion client_version;
	//	 Random random;
	//	 SessionID session_id;
	//	 CipherSuite cipher_suites<2..2^16-2>;
	//	 CompressionMethod compression_methods<1..2^8-1>;
	//	 select (extensions_present) {
	//		 case false:
	//			 struct {};
	//		 case true:
	//			 Extension extensions<0..2^16-1>;
	//	 };
	// } ClientHello;
	//
	// - RFC5246 section 7.4.1.2.

	//TLS versions before 1.2 have been withdrawn by RFC8996.
	let client_version = field!(S ClientVersion msg.item(|v:TlsVersion|{
		v.get() >= TlsVersion::TLS_1_2.get()
	}));
	debug!(dbg "Client version: {client_version}");
	let random: [u8;32] = field!(S Random msg.array());
	let session_id = field!(S SessionId2 msg.slice(LEN_SESSION_ID));
	debug!(dbg "Session ID length: {session_id_len}", session_id_len=session_id.len());
	let cipher_suites = field!(S CipherSuites msg.list_of::<CipherSuite>(LEN_CIPHER_SUITE_LIST));
	debug!(dbg "Cipher suites: {cipher_suites:?}");
	let compression_methods = field!(S CompressionMethods
		msg.list_of::<CompressionMethod>(LEN_COMPRESSION_METHOD_LIST)
	);
	debug!(dbg "Compression methods: {compression_methods:?}");
	let base_prefix_size = orig_size - msg.length();
	let base_prefix = o_msg.get(..base_prefix_size).unwrap_or(b"");
	let extensions = if msg.more() {
		field!(S Extensions msg.list_of::<crate::iters2::Extension<'a>>(LEN_EXTENSION_LIST))
	} else {
		Default::default()
	};
	msg.assert_end(JunkAfterExtensions)?;

	let mut exts2 = field!(S Extensions2 exts::check_extensions_block(extensions, |ext|{
		ext.allowed_in_client_hello_tls12() || ext.allowed_in_client_hello_tls13()
	}));
	fail_if!(exts2.psk_was_not_last(), PskNotLast);
	//Renego info SCSV does some utterly magical stuff.
	if cipher_suites.iter_all().any(|cs|cs==CipherSuite::EMPTY_RENEGOTIATION_INFO_SCSV) {
		debug!(dbg "SCSV TLS_EMPTY_RENEGOTIATION_INFO_SCSV");
		field!(S Extensions2 exts2.empty_renegotiation_info_scsv_hack());
	}

	let (server_name, server_host_name) = extension!(exts2 dbg SERVER_NAME |extn, extv, dbg|{
		let (snl, host_name) = field!(E ServerNameList extv.server_name_list());
		if let Some(host_name) = host_name { debug!(E dbg extn "Host name is {host_name}"); }
		if snl.iter().count() > 0 {
			debug!(E dbg extn "Other names are {snli:?}", snli=snl.iter());
		}
		Ok((snl,host_name))
	}).unwrap_or_default();

	let maximum_fragment_length = extension!(exts2 dbg MAX_FRAGMENT_LENGTH |extn, extv, dbg|{
		let mfl = field!(E MaximumFragmentLength MaximumFragmentLength::read(extv));
		debug!(E dbg extn "{mfl:?}");
		Ok(mfl)
	});

	let supports_client_certificate_url = extension!(exts2 dbg CLIENT_CERTIFICATE_URL exts::ext_supported).
		is_some();

	let supported_trusted_ca_keys = extension!(exts2 dbg TRUSTED_CA_KEYS |extn, extv, dbg|{
		// struct {
		//	 TrustedAuthority trusted_authorities_list<0..2^16-1>;
		// } TrustedAuthorities;
		//
		// - RFC6066 section 6.
		let tal = field!(E TrustedAuthoritiesList
			extv.list_of::<TrustedAuthority>(LEN_TRUSTED_AUTHORITIES_LIST)
		);
		debug!(E dbg extn "{tal:?}");
		Ok(tal)
	}).unwrap_or_default();

	let supports_truncated_hmac = extension!(exts2 dbg TRUNCATED_HMAC exts::ext_supported).is_some();

	let certificate_status_request = extension!(exts2 dbg STATUS_REQUEST |extn, extv, dbg|{
		let d: CertificateStatusType = field!(E StatusType extv.cp());
		let r = match d {
			CertificateStatusType::OCSP => {
				let r = field!(E RequestOcsp OcspStatusRequest::read(extv));
				debug!(E dbg extn "OCSP responders {r_resp:?} extensions <{r_exts}>",
					r_resp = r.get_responders(), r_exts = Hexdump(r.get_extensions()));
				StatusRequest::Ocsp(r)
			},
			stype => {
				debug!(E dbg extn "unknown status type {stype}");
				StatusRequest::Unknown(stype, extv.all())
			}
		};
		Ok(r)
	}).unwrap_or(StatusRequest::None);
	
	let supported_user_mapping_types = extension!(exts2 dbg USER_MAPPING |extn, extv, dbg|{
		// struct {
		//	 UserMappingType user_mapping_types<1..2^8-1>;
		// } UserMappingTypeList;
		//
		// - RFC4681 section 2.
		let umt = field!(E UserMappingTypes extv.list_of::<UserMappingType>(LEN_USER_MAPPING_TYPE_LIST));
		debug!(E dbg extn "{umt:?}");
		Ok(umt)
	}).unwrap_or_default();

	let supported_client_authz_formats = extension!(exts2 dbg CLIENT_AUTHZ exts::clientext_peer_authz).
		unwrap_or_default();

	let supported_server_authz_formats = extension!(exts2 dbg SERVER_AUTHZ exts::clientext_peer_authz).
		unwrap_or_default();

	let supported_certificate_types = extension!(exts2 dbg CERT_TYPE |extn, extv, dbg|{
		// struct {
		//	 select(ClientOrServerExtension) {
		//		 case client:
		//			 CertificateType certificate_types<1..2^8-1>;
		//		 <...>
		//	 }
		// } CertificateTypeExtension;
		//
		// - RFC6091 section 3.1.
		let ct = field!(E CertificateTypes extv.list_of::<CertificateType>(LEN_CERTIFICATE_TYPE_LIST));
		debug!(E dbg extn "{ct:?}");
		Ok(ct)
	}).unwrap_or_default();

	let supported_groups = extension!(exts2 dbg SUPPORTED_GROUPS exts::ext_supported_groups).unwrap_or_default();

	let supported_ec_point_formats = extension!(exts2 dbg EC_POINT_FORMATS |extn, extv, dbg|{
		// struct {
		//	 ECPointFormat ec_point_format_list<1..2^8-1>
		// } ECPointFormatList;
		//
		// - RFC4492 section 5.1.2
		let epfl = field!(E EcPointFormatList extv.list_of::<EcPointFormat>(LEN_EC_POINT_FORMATS));
		debug!(E dbg extn "{epfl:?}");
		Ok(epfl)
	}).unwrap_or_default();

	let srp_username = extension!(exts2 dbg SRP |extn, extv, dbg|{
		let srpi = field!(E SrpI extv.slice(LEN_SRP_I));
		debug!(E dbg extn "{srpip}", srpip=PrintName(srpi));
		Ok(srpi)
	});

	let supported_signature_algorithms =
		extension!(exts2 dbg SIGNATURE_ALGORITHMS exts::clientext_signature_algorithms).unwrap_or_default();

	let (supported_srtp_protection_profiles, srtp_mki) = extension!(exts2 dbg USE_SRTP |extn, extv, dbg|{
		// struct {
		//	 SRTPProtectionProfiles SRTPProtectionProfiles;
		//	 opaque srtp_mki<0..255>;
		// } UseSRTPData;
		//
		// SRTPProtectionProfile SRTPProtectionProfiles<2..2^16-1>;
		//
		// - RFC564 section 4.1.1.
		let protection_profiles = field!(E SrtpProtectionProfiles
			extv.list_of::<DtlsSrtpProtectionProfile>(LEN_SRTP_PROTECTION_PROFILE_LIST)
		);
		let mki = field!(E SrtpMki extv.slice(LEN_SRTP_MKI));
		debug!(E dbg extn "Protection profiles: {protection_profiles:?}");
		debug!(E dbg extn "MKI: <{mki}>", mki=Hexdump(mki));
		Ok((protection_profiles, mki))
	}).unwrap_or_default();

	let heartbeat = extension!(exts2 dbg HEARTBEAT exts::ext_heartbeat);

	let supported_alpn_protocol_ids =
		extension!(exts2 dbg APPLICATION_LAYER_PROTOCOL_NEGOTIATION |extn, extv, dbg|{
		// struct {
		//	 ProtocolName protocol_name_list<2..2^16-1>
		// } ProtocolNameList;
		//
		// - RFC7301 section 3.1.
		let pnl = field!(E ProtocolNameList extv.list_of::<AlpnProtocolId<'a>>(LEN_PROTOCOL_LIST));
		debug!(E dbg extn "{pnl:?}");
		Ok(pnl)
	}).unwrap_or_default();

	let certificate_status_request2 = extension!(exts2 dbg STATUS_REQUEST_V2 |extn, extv, dbg|{
		// struct {
		//	 CertificateStatusRequestItemV2 certificate_status_req_list<1..2^16-1>;
		// } CertificateStatusRequestListV2;
		//
		// - RFC6961 section 2.2.
		let csrl = field!(E CertificateStatusReqList
			extv.list_of::<CertificateStatusRequestItemV2<'a>>(LEN_CSRI2_REQLIST)
		);
		debug!(E dbg extn "{csrl:?}");
		Ok(csrl)
	}).unwrap_or_default();

	let supports_signed_certificate_timestamp =
		extension!(exts2 dbg SIGNED_CERTIFICATE_TIMESTAMP exts::ext_supported).is_some();

	let supported_client_certificate_types = extension!(exts2 dbg CLIENT_CERTIFICATE_TYPE |extn, extv, dbg|{
		// struct {
		//	 select(ClientOrServerExtension) {
		//		 case client:
		//			 CertificateType client_certificate_types<1..2^8-1>;
		//		 <...>
		//	 }
		// } ClientCertTypeExtension;
		//
		// - RFC7250 section 3.
		let cct = field!(E ClientCertificateTypes
			extv.list_of::<CertificateType>(LEN_CERTIFICATE_TYPE_LIST)
		);
		debug!(E dbg extn "{cct:?}");
		Ok(cct)
	}).unwrap_or_default();

	let supported_server_certificate_types = extension!(exts2 dbg SERVER_CERTIFICATE_TYPE |extn, extv, dbg|{
		// struct {
		//	 select(ClientOrServerExtension) {
		//		 case client:
		//			 CertificateType server_certificate_types<1..2^8-1>;
		//		 <...>
		//	 }
		// } ServerCertTypeExtension;
		//
		// - RFC7250 section 3.
		let sct = field!(E ServerCertificateTypes
			extv.list_of::<CertificateType>(LEN_CERTIFICATE_TYPE_LIST)
		);
		debug!(E dbg extn "{sct:?}");
		Ok(sct)
	}).unwrap_or_default();
	
	let padding = extension!(exts2 dbg PADDING |extn, extv, dbg|{
		let x = extv.all();
		debug!(E dbg extn "{xlen} bytes", xlen=x.len());
		fail_if!(x.iter().any(|&x|x!=0), ExtensionError::BadPadding);
		Ok(x.len() + 4)
	});

	let supports_encrypt_then_mac = extension!(exts2 dbg ENCRYPT_THEN_MAC exts::ext_supported).is_some();

	let supports_extended_master_secret = extension!(exts2 dbg EXTENDED_MASTER_SECRET exts::ext_supported).
		is_some();

	let (token_binding_version, supported_token_binding_key_parameters) =
		extension!(exts2 dbg TOKEN_BINDING |extn, extv, dbg|{
		// struct {
		//	 TB_ProtocolVersion token_binding_version;
		//	 TokenBindingKeyParameters key_parameters_list<1..2^8-1>
		// } TokenBindingParameters;
		//
		// - RFC8472 section 2.
		let major: u8 = field!(E TbVerMajor extv.cp());
		let minor: u8 = field!(E TbVerMinor extv.cp());
		let kpl = field!(E KeyParametersList
			extv.list_of::<TokenBindingKeyParameter>(LEN_TB_KEY_PARAMETERS_LIST)
		);
		debug!(E dbg extn "version {major}.{minor} parameters: {kpl:?}");
		Ok(((major, minor),kpl))
	}).unwrap_or_default();

	let supported_cached_objects = extension!(exts2 dbg CACHED_INFO |extn, extv, dbg|{
		// struct {
		//	 CachedObject cached_info<1..2^16-1>;
		// } CachedInformation;
		//
		// - RFC7924 section 3.
		let info = field!(E CachedInfo extv.list_of::<CachedObject<'a>>(LEN_CACHED_INFO_LIST));
		debug!(E dbg extn "{info:?}");
		Ok(info)
	}).unwrap_or_default();
	
	let supports_tls_lts = extension!(exts2 dbg TLS_LTS exts::ext_supported).is_some();

	let supported_certificate_compression_algorithms =
		extension!(exts2 dbg COMPRESS_CERTIFICATE exts::clientext_compress_certificate).unwrap_or_default();

	let record_size_limit = extension!(exts2 dbg RECORD_SIZE_LIMIT exts::ext_record_size_limit);

	let pinning_ticket = extension!(exts2 dbg TICKET_PINNING |extn, extv, dbg|{
		// struct {
		//	 select (Role) {
		//		 case client:
		//			 pinning_ticket ticket<0..2^16-1>; //omitted on 1st connection
		//		 <...>
		//	 }
		// } PinningTicketExtension;
		//
		// - RFC8672 section 3.
		let xticket: Option<PinningTicket<'a>> = field!(E Ticket extv.list_at_most_one(LEN_PINNING_TICKET));
		let xticket = xticket.map(|x|x.0);
		if let Some(xticket) = xticket {
			debug!(E dbg extn "Ticket {xticketlen} bytes", xticketlen=xticket.len());
		} else {
			debug!(E dbg extn "No ticket");
		};
		Ok(xticket)
	});

	let supports_tls_cert_with_extern_psk = extension!(exts2 dbg TLS_CERT_WITH_EXTERN_PSK exts::ext_supported).
		is_some();

	let supported_signature_algorithms_dcred =
		extension!(exts2 dbg DELEGATED_CREDENTIALS exts::clientext_signature_algorithms).unwrap_or_default();

	let session_ticket = extension!(exts2 dbg SESSION_TICKET |extn, extv, dbg|{
		//This is a mess.
		debug!(E dbg extn "{extvlen} bytes", extvlen=extv.length());
		Ok(extv.all())
	});

	let supported_ekt_ciphers = extension!(exts2 dbg SUPPORTED_EKT_CIPHERS |extn, extv, dbg|{
		// EKTCipherType supported_ciphers<1..255>;
		//
		// - RFC8870 section 5.2.1
		let sc = field!(E SupportedCiphers extv.list_of::<EktCipher>(LEN_EKT_SUPPORTED_CIPHERS));
		debug!(E dbg extn "{sc:?}");
		Ok(sc)
	}).unwrap_or_default();

	let mut __msg_left_at_binder = orig_size;		//If no binder, behave like all was binder.
	let pre_shared_keys = extension!(exts2 dbg PRE_SHARED_KEY |extn, extv, dbg|{
		use crate::iters2::PskIdentity;
		use crate::iters2::PskBinderEntry;
		// struct {
		//	 PskIdentity identities<7..2^16-1>;
		//	 PskBinderEntry binders<33..2^16-1>;
		// } OfferedPsks;
		//
		// - RFC8446 section 4.2.11.
		let identities = field!(E Identities extv.list_of::<PskIdentity>(LEN_PSK_IDENTITY_LIST));
		__msg_left_at_binder = extv.length();
		let binders = field!(E Binders extv.list_of::<PskBinderEntry>(LEN_PSK_BINDER_LIST));
		let psk = IntoPreSharedKeyIter::new(identities, binders).
			set_err(ExtensionError::PskCountMismatch)?;
		debug!(E dbg extn "{pski:?}", pski=psk.iter());
		Ok(psk)
	}).unwrap_or_default();

	let supports_early_data = extension!(exts2 dbg EARLY_DATA |extn, _, dbg|{
		debug!(E dbg extn "Will send early data");
		Ok(())
	}).is_some();

	let mut __tls_new = false;
	let mut __tls_old = true;
	let supported_versions = extension!(exts2 dbg SUPPORTED_VERSIONS |extn, extv, dbg|{
		// struct {
		//	 select (Handshake.msg_type) {
		//		 case client_hello:
		//			 ProtocolVersion versions<2..254>;
		//		 <...>
		//	 };
		// } SupportedVersions;
		//
		// - RFC8446 section 4.2.1.
		__tls_old = false;		//Maybe not.
		let v = field!(E Versions extv.list_of::<TlsVersion>(LEN_VERSION_LIST));
		debug!(E dbg extn "{v:?}");
		for version in v.iter() { match version.get() {
			0x300..=0x303 => __tls_old = true,
			0x304|0x7f00..=0x7f1c => __tls_new = true,
			_ => ()
		}}
		Ok(v)
	}).unwrap_or_default();
	
	let cookie = extension!(exts2 dbg COOKIE |extn, extv, dbg|{
		let c = field!(E Cookie extv.slice(LEN_COOKIE));
		debug!(E dbg extn "{clen} bytes", clen=c.len());
		Ok(c)
	});

	let supported_psk_key_exchange_modes = extension!(exts2 dbg PSK_KEY_EXCHANGE_MODES |extn, extv, dbg|{
		// struct {
		//	 PskKeyExchangeMode ke_modes<1..255>;
		// } PskKeyExchangeModes;
		//
		// - RFC8446 section 4.2.9.
		let km = field!(E KeModes extv.list_of::<PskKeyExchangeMode>(LEN_PSK_MODES_LIST));
		debug!(E dbg extn "{km:?}");
		Ok(km)
	}).unwrap_or_default();

	let supported_certificate_authorities =
		extension!(exts2 dbg CERTIFICATE_AUTHORITIES exts::clientext_certificate_authorities).
		unwrap_or_default();

	let supports_post_handshake_auth = extension!(exts2 dbg POST_HANDSHAKE_AUTH exts::ext_supported).is_some();

	let supported_signature_algorithms_cert =
		extension!(exts2 dbg SIGNATURE_ALGORITHMS_CERT exts::clientext_signature_algorithms);

	let key_share = extension!(exts2 dbg KEY_SHARE |extn, extv, dbg|{
		// struct {
		//	 KeyShareEntry client_shares<0..2^16-1>;
		// } KeyShareClientHello;
		//
		// - RFC8446 section 4.2.8.
		let cs = field!(E ClientShares extv.list_of::<KeyShareEntry<'a>>(LEN_KEY_SHARE_LIST));
		let mut any = false;
		for i in cs.iter_all() {
			debug!(E dbg extn "Group {grp} ({kexlen} bytes)", grp=i.group, kexlen=i.key_exchange.len());
			any = true;
		}
		if !any { debug!(E dbg extn "No shares"); }
		Ok(cs)
	});

	let supports_transparency_info = extension!(exts2 dbg TRANSPARENCY_INFO exts::ext_supported).is_some();

	let connection_id_old = extension!(exts2 dbg CONNECTION_ID_OLD exts::ext_connection_id);
	let connection_id = extension!(exts2 dbg CONNECTION_ID exts::ext_connection_id);

	let external_id_hash = extension!(exts2 dbg EXTERNAL_ID_HASH exts::ext_external_id_hash);
	let external_session_id = extension!(exts2 dbg EXTERNAL_SESSION_ID exts::ext_external_session_id);

	let quic_transport_parameters = extension!(exts2 dbg TICKET_REQUEST exts::ext_quic_transport_parameters);
		
	let requested_ticket_count = extension!(exts2 dbg TICKET_REQUEST |extn, extv, dbg|{
		let new_session_count: u8 = field!(E NewSessionCount extv.cp());
		let resumption_count: u8 = field!(E ResumptionCount extv.cp());
		debug!(E dbg extn "{new_session_count} for new, {resumption_count} for resume");
		Ok((new_session_count, resumption_count))
	});

	let destination_port_number = extension!(exts2 dbg DNSSEC_CHAIN |extn, extv, dbg|{
		let pn: u16 = field!(E PortNumber extv.cp());
		debug!(E dbg extn "Port {pn}");
		Ok(pn)
	});

	let supported_sequence_number_encryption_algorithms = extension!(exts2 dbg
		SEQUENCE_NUMBER_ENCRYPTION_ALGORITHMS |extn, extv, dbg|{
		use btls_aux_tls_iana::SequenceNumberEncryptionAlgorithm as SNEA;
		// struct {
		//	 SeqNumEncAlgs supported_algs<1..255>;
		// } SupportedSequenceNumberEncryptionAlgorithms;
		//
		// - draft-pismenny-tls-dtls-plaintext-sequence-number-01 section 3.
		let a = field!(E SupportedAlgs
			extv.list_of::<SNEA>(LEN_SEQUENCE_NUMBER_ENCRYPTION_ALGORITHMS_LIST)
		);
		debug!(E dbg extn "{a:?}");
		Ok(a)
	}).unwrap_or_default();

	//If the renego SCSV is set, then special RENEGOTIATION_INFO extension is injected which this will pick
	//up.
	let renegotiated_connection = extension!(exts2 dbg RENEGOTIATION_INFO |extn, extv, dbg|{
		let rc = field!(E RenegotiatedConnection extv.slice(LEN_RENEGOTIATED_CONNECTION));
		if rc.len() > 0 {
			debug!(E dbg extn "Renegotiating <{rc}>", rc=Hexdump(rc));
		} else {
			debug!(E dbg extn "New session");
		}
		Ok(rc)
	});

	crate::extensions::report_unknown_extensions(extensions, dbg);

	//Check extensions are allowed.
	for extension in extensions.iter_all() {
		let extn = extension.extension_type;
		if __tls_old && extn.allowed_in_client_hello_tls12() { continue; }
		if __tls_new && extn.allowed_in_client_hello_tls13() { continue; }
		fail!(Extension(extn, ExtensionError::NotAllowedInTlsVersion));
	}
	//Extension conflicts/requires.
	conflict(&exts2, IanaExtension::TLS_CERT_WITH_EXTERN_PSK, IanaExtension::EARLY_DATA)?;
	check_dep(&exts2, IanaExtension::TLS_CERT_WITH_EXTERN_PSK, IanaExtension::KEY_SHARE)?;
	check_dep(&exts2, IanaExtension::TLS_CERT_WITH_EXTERN_PSK, IanaExtension::PSK_KEY_EXCHANGE_MODES)?;
	check_dep(&exts2, IanaExtension::TLS_CERT_WITH_EXTERN_PSK, IanaExtension::PRE_SHARED_KEY)?;
	check_dep(&exts2, IanaExtension::TLS_CERT_WITH_EXTERN_PSK, IanaExtension::SIGNATURE_ALGORITHMS)?;
	check_dep(&exts2, IanaExtension::SIGNATURE_ALGORITHMS_CERT, IanaExtension::SIGNATURE_ALGORITHMS)?;
	check_dep(&exts2, IanaExtension::DELEGATED_CREDENTIALS, IanaExtension::SIGNATURE_ALGORITHMS)?;
	check_dep(&exts2, IanaExtension::SIGNATURE_ALGORITHMS, IanaExtension::SUPPORTED_GROUPS)?;
	check_dep(&exts2, IanaExtension::KEY_SHARE, IanaExtension::SUPPORTED_GROUPS)?;
	check_dep(&exts2, IanaExtension::PRE_SHARED_KEY, IanaExtension::PSK_KEY_EXCHANGE_MODES)?;

	if __tls_new {
		//Compression methods must be NULL.
		fail_if!(compression_methods.as_raw_slice() != &[0], BadTls13CompressionMethod);
		//Check that KEY_SHARE/SIGNATURE_ALGORITHMS or PRE_SHARED_KEY is present.
		let can_cert = exts2.in_set(IanaExtension::KEY_SHARE) &&
			exts2.in_set(IanaExtension::SIGNATURE_ALGORITHMS);
		let can_psk = exts2.in_set(IanaExtension::PRE_SHARED_KEY);
		fail_if!(!can_cert && !can_psk, NoKeyExchangeMethods);
	}
	//If PSK_DHE_KE is present, require KEY_SHARE.
	if supported_psk_key_exchange_modes.iter().any(|m|m==PskKeyExchangeMode::PSK_DHE_KE) {
		fail_if!(!exts2.in_set(IanaExtension::KEY_SHARE), NeedKeyShareDheKe);
	}
	if let Some(key_share) = key_share { for KeyShareEntry{group,key_exchange} in key_share {
		fail_if!(!key_ok(group, key_exchange),
			Extension(IanaExtension::KEY_SHARE, ExtensionError::BadShare(group)));
	}}

	let key_shares = match (supported_groups.nontrivial(), key_share) {
		(false, None) => Ok(IntoKeyShareIter::blank()),
		//This should not ever fail.
		(true, None) => IntoKeyShareIter::new(supported_groups, Default::default()),
		(false, Some(_)) => fail!(Dependency(IanaExtension::SUPPORTED_GROUPS, IanaExtension::KEY_SHARE)),
		(true, Some(k)) => IntoKeyShareIter::new(supported_groups, k)
	}.set_err(Extension(IanaExtension::KEY_SHARE, ExtensionError::SharesNotSubset))?;

	let fallback_hack = cipher_suites.iter_all().any(|cs|{
		cs==CipherSuite::FALLBACK_SCSV
	});
	if fallback_hack { debug!(dbg "SCSV TLS_FALLBACK_SCSV"); }

	//The binder is the absolute last thing in client hello.
	let binder_size = orig_size - __msg_left_at_binder;

	Ok(ClientHello {
		client_version,
		random,
		session_id,
		supported_cipher_suites: cipher_suites,
		client_fallback: fallback_hack,
		supported_compression_methods: compression_methods,
		extensions,
		extension_mask: exts2.get_mask(),
		key_shares,
		server_name,
		server_host_name,
		maximum_fragment_length,
		supports_client_certificate_url,
		supported_trusted_ca_keys,
		supports_truncated_hmac,
		certificate_status_request,
		supported_user_mapping_types,
		supported_client_authz_formats,
		supported_server_authz_formats,
		supported_certificate_types,
		supported_groups,
		supported_ec_point_formats,
		srp_username,
		supported_signature_algorithms,
		supported_srtp_protection_profiles,
		srtp_mki,
		heartbeat,
		supported_alpn_protocol_ids,
		certificate_status_request2,
		supports_signed_certificate_timestamp,
		supported_client_certificate_types,
		supported_server_certificate_types,
		padding_amount: padding.unwrap_or(0),
		supports_encrypt_then_mac,
		supports_extended_master_secret,
		supported_token_binding_key_parameters,
		token_binding_version,
		supported_cached_objects,
		supports_tls_lts,
		supported_certificate_compression_algorithms,
		record_size_limit,
		pinning_ticket,
		supports_tls_cert_with_extern_psk,
		supported_signature_algorithms_dcred,
		session_ticket,
		supported_ekt_ciphers,
		pre_shared_keys,
		supports_early_data,
		supported_versions,
		cookie,
		supported_psk_key_exchange_modes,
		supported_certificate_authorities,
		supported_sequence_number_encryption_algorithms,
		supports_post_handshake_auth,
		//Fall back to signature_algorithms.
		supported_signature_algorithms_cert: supported_signature_algorithms_cert.
			unwrap_or(supported_signature_algorithms),
		supports_transparency_info,
		connection_id_old,
		connection_id,
		external_id_hash,
		external_session_id,
		quic_transport_parameters,
		requested_ticket_count,
		destination_port_number,
		renegotiated_connection,
		base_prefix: base_prefix,
		binder_split: if binder_size <= o_msg.len() {
			o_msg.split_at(binder_size)
		} else {
			(b"", o_msg)
		},
	})
}

impl<'a> ClientHello<'a>
{
	///Get the non-extension parts of the Client Hello as a slice.
	///
	///This is intended for computing invariant hash for checking that client has not modified things it
	///must not modify between client hello and retry.
	pub fn get_base_prefix(&self) -> &'a [u8] { self.base_prefix }
	///Get Binder split of Client Hello.
	///
	///The first part is what of Client Hello is included in binders, and the second part is the rest. In
	///case there are no binders, the first part will be empty, and the second part will be the entiere
	///Client Hello.
	pub fn get_binder_split(&self) -> (&'a [u8], &'a [u8]) { self.binder_split }
	///Get client version (legacy).
	///
	///This is always TLS 1.2.
	pub fn get_client_version(&self) -> TlsVersion { self.client_version }
	///Get Client Random.
	pub fn get_random(&self) -> [u8;32] { self.random }
	///Get Session ID client is trying to resume.
	pub fn get_session_id(&self) -> &'a [u8] { self.session_id }
	///Get cipher suites supported by the client.
	pub fn get_cipher_suites(&self) -> ListOf<'a, CipherSuite> { self.supported_cipher_suites }
	///The client has fallen back flag.
	///
	///This is set if the client reports it has fallen back. If the server is unable to negotiate the newest
	///TLS version it supports, it SHOLD terminate the handshake.
	pub fn client_has_fallen_back(&self) -> bool { self.client_fallback }
	///Get compression methods supported by the client.
	///
	///For TLS 1.3 Client Hellos, this must be `[NULL]`.
	pub fn get_compression_methods(&self) -> ListOf<'a, CompressionMethod>
	{
		self.supported_compression_methods
	}
	///Get extensions.
	///
	///This can be used to parse extensions other than the explicitly supported ones.
	pub fn get_extensions(&self) -> ListOf<'a, Extension<'a>> { self.extensions }
	///Get server host name the client is trying to access, if any.
	///
	///If the client is trying to access the server by IP address, this is `None`.
	pub fn get_server_host_name(&self) -> Option<&'a str> { self.server_host_name }
	///Get other names of the server the client is trying to access.
	///
	///As of currently, this should never contain anything (unless one does `iter_all()`, then one gets
	///raw entry for host name).
	pub fn get_server_other_names(&self) -> ListOf<'a, ServerName<'a>> { self.server_name }
	///Get the maximum fragment length, if any.
	///
	///This is old deprecated extension. Use `record_size_limit` instead.
	pub fn get_maximum_fragment_length(&self) -> Option<MaximumFragmentLength>
	{
		self.maximum_fragment_length
	}
	///The client supports sending URL for client certificate.
	pub fn supports_client_certificate_url(&self) -> bool { self.supports_client_certificate_url }
	///Get CA keys the client trusts.
	///
	///If the client did not send this extension, this defaults to `[PreAgreed]`
	pub fn get_trusted_ca_keys(&self) -> ListOf<'a,TrustedAuthority<'a>> { self.supported_trusted_ca_keys }
	///Client supports truncated HMAC.
	pub fn supports_truncated_hmac(&self) -> bool { self.supports_truncated_hmac }
	///Client request for stapling certificate status.
	pub fn get_status_request(&self) -> StatusRequest<'a> { self.certificate_status_request }
	///Get User Mapping Types the the client supports.
	pub fn get_user_mapping_types(&self) -> ListOf<'a,UserMappingType> { self.supported_user_mapping_types }
	///Get Client Authorization Data Formats the client supports.
	pub fn get_client_authorization_data_formats(&self) -> ListOf<'a,AuthorizationDataFormat>
	{
		self.supported_client_authz_formats
	}
	///Get Server Authorization Data Formats the client supports.
	pub fn get_server_authorization_data_formats(&self) -> ListOf<'a,AuthorizationDataFormat>
	{
		self.supported_server_authz_formats
	}
	///Get Certificate Types the client supports.
	///
	///Note, that if client does not send the extension, this defaults to `[X509]`. If you need to check if
	///the extension was actually sent, use `has_extension(CERT_TYPE)`.
	///
	///This is obsolete. Use `client_certificate_types`/`server_certificate_types` instead.
	pub fn get_certificate_types(&self) -> ListOf<'a,CertificateType> { self.supported_certificate_types }
	///Get Groups/KEMs the client supports.
	///
	///If this list is empty, the client does not support forward-secret key exchanges (only supporting PSK-only
	///key exchange).
	pub fn get_groups(&self) -> ListOf<'a,NamedGroup> { self.supported_groups }
	///Get EC Point Formats the client supports.
	pub fn get_ec_point_formats(&self) -> ListOf<'a,EcPointFormat> { self.supported_ec_point_formats }
	///Get the Secure Remote Password username, if any.
	pub fn get_srp_username(&self) -> Option<&'a [u8]> { self.srp_username }
	///Get the Signature Algorithms the client supports.
	///
	///This is the whitelist for supported algorithms for signing the exchange. In case Delegated Credentials
	///are used, this is list of algorithms allowed for signing the Delegated Credential, the algorithms
	///DCs themselves can use are conntained in `get_signature_algorithms_dcred()'. These two lists might not
	///have any relation with each other.
	///
	///If this list is empty, the client does not support signature authentication, only PSK authentication.
	pub fn get_signature_algorithms(&self) -> ListOf<'a,SignatureScheme> { self.supported_signature_algorithms }
	///Get DTLS-SRTP Protection Profiles the client supports.
	///
	///If this list is empty, the client does not support DTLS-SRTP.
	pub fn get_dtls_srtp_protection_profiles(&self) -> ListOf<'a,DtlsSrtpProtectionProfile>
	{
		self.supported_srtp_protection_profiles
	}
	///Get the DTLS-SRTP Master Key Indicator the client is trying to use.
	///
	///This value is meaningless if `get_dtls_srtp_protection_profiles()` returns an empty list.
	pub fn get_dtls_srtp_mki(&self) -> &'a [u8] { self.srtp_mki }
	///Get the heartbeat support of the client.
	///
	///`Some(x)` means client is requesting permision to send `heartbeat` messages to the server, or for
	///server to send heartbeat messages to it. If `x` is true, the server MAY send heartbeat messages to
	///to the client. If `x` is false, the server MUST NOT send heartbeat messages to the client.
	pub fn get_heartbeat(&self) -> Option<bool> { self.heartbeat }
	///Get list of ALPN Protocol IDs client supprorts.
	///
	///If this list is empty, the client is trying to access the default protocol of the port. This is not
	///safe against adversary redirecting connections.
	pub fn get_alpn_protocol_ids(&self) -> ListOf<'a,AlpnProtocolId<'a>> { self.supported_alpn_protocol_ids }
	///Get client request for stapling certificate status, v2.
	///
	///If this list is empty, the client does not support stapling certificate status via v2 mechanism. It
	///still may support stapling via v1 mechanism (`get_status_request()`).
	pub fn get_status_request_v2(&self) -> ListOf<'a,CertificateStatusRequestItemV2<'a>>
	{
		self.certificate_status_request2
	}
	///The client supports stapling Certificate Transparency v1 Signed Certificate Timestamps.
	pub fn supports_signed_certificate_timestamps(&self) -> bool
	{
		self.supports_signed_certificate_timestamp
	}
	///Get Client Certificate Types the client supports.
	///
	///Note, that if client does not send the extension, this defaults to `[X509]`. If you need to check if
	///the extension was actually sent, use `has_extension(CLIENT_CERTIFICATE_TYPE)`.
	pub fn get_client_certificate_types(&self) -> ListOf<'a,CertificateType>
	{
		self.supported_client_certificate_types
	}
	///Get Server Certificate Types the client supports.
	///
	///Note, that if client does not send the extension, this defaults to `[X509]`. If you need to check if
	///the extension was actually sent, use `has_extension(SERVER_CERTIFICATE_TYPE)`.
	pub fn get_server_certificate_types(&self) -> ListOf<'a,CertificateType>
	{
		self.supported_server_certificate_types
	}
	///Get amount of padding in Client Hello.
	///
	///If no `padding` extension was sent, this is 0. Otherwise this is size of `padding` extension, including
	///the header. As consequence, the value can not be 1 to 3.
	pub fn get_padding_size(&self) -> usize { self.padding_amount }
	///The client supports Encrypt-then-MAC.
	pub fn supports_encrypt_then_mac(&self) -> bool { self.supports_encrypt_then_mac }
	///The client supports Extended Master Secret.
	///
	///Servers SHOULD NOT resume sessions unless both connections use Extended Master Secret, due to
	///risk of Triple Handshake Attack. Server SHOULD NOT allow use of TLS-extractor on connections with
	///no EMS, due to different version of the same attack.
	pub fn supports_extended_master_secret(&self) -> bool { self.supports_extended_master_secret }
	///Get list of Token Binding Key Parameters the client supports.
	///
	///If this is empty, client does not support Token Binding.
	pub fn get_token_binding_key_parameters(&self) -> ListOf<'a,TokenBindingKeyParameter>
	{
		self.supported_token_binding_key_parameters
	}
	///Get maximum Token Binding version the client supports.
	///
	///The version is given as `(major, minor)`.
	///
	///This is meaningless if `get_token_binding_key_parameters()` gives an empty list.
	pub fn get_token_binding_version(&self) -> (u8, u8) { self.token_binding_version }
	///Get the Cached Objects the client has.
	pub fn get_cached_objects(&self) -> ListOf<'a,CachedObject<'a>> { self.supported_cached_objects }
	///The client supports TLS-LTS.
	pub fn supports_tls_lts(&self) -> bool { self.supports_tls_lts }
	///Get list of Certificate Compression algorithms the client supports.
	///
	///If this list is empty, the client does not support certificate compression.
	pub fn get_certificate_compression_algorithms(&self) -> ListOf<'a,CertificateCompressionAlgorithmId>
	{
		self.supported_certificate_compression_algorithms
	}
	///Get maximum size of record plaintext for encrypted record the client supports.
	///
	///If standard TLS limits apply, this is `None`.
	pub fn get_record_size_limit(&self) -> Option<usize> { self.record_size_limit.map(|x|x as usize) }
	///Get the pinning ticket client is using.
	///
	/// * If client supports ticket pinning and has existing tikcet, returns `Some(Some(ticket))`.
	/// * If client supports ticket pinning but has no existing ticket, returns `Some(None)`.
	/// * If client does not support ticket pinning, returns `None`.
	pub fn get_pinning_ticket(&self) -> Option<Option<&'a [u8]>> { self.pinning_ticket }
	///The client supports certificate authentication with external PSK.
	pub fn supports_tls_cert_with_extern_psk(&self) -> bool { self.supports_tls_cert_with_extern_psk }
	///Get the Signature Algorithms the client supports for Delegated Credentials.
	///
	///This is the whitelist for supported algorithms for use with Delegated Credentials. The list of
	///algorithms allowed to sign DCs is separate, and returned by `get_signature_algorithms()'. These two
	///lists might not have any relation with each other.
	///
	///If this list is empty, the client does not support Delegated Credentials.
	pub fn get_signature_algorithms_dcred(&self) -> ListOf<'a,SignatureScheme>
	{
		self.supported_signature_algorithms_dcred
	}
	///Return the entiere raw session ticket the client sent, if any.
	///
	///This is for TLS 1.2 only. For similar mechanism in TLS 1.3, there is `get_pre_shared_keys()`.
	///
	///Warning: This feature is severely flawed.
	pub fn get_session_ticket(&self) -> Option<&'a [u8]> { self.session_ticket }
	///Get list of supported EKT ciphers.
	pub fn get_ekt_ciphers(&self) -> ListOf<'a,EktCipher> { self.supported_ekt_ciphers }
	///Get the pre-shared keys the client is offering.
	///
	///If this list is empty, the client does not have any PSKs to offer. It still might support PSKs, see
	///`get_psk_key_exchange_modes()`.
	pub fn get_pre_shared_keys(&self) -> IntoPreSharedKeyIter<'a> { self.pre_shared_keys }
	///Get if the client is sending Early Data.
	pub fn sending_early_data(&self) -> bool { self.supports_early_data }
	///Get list of versions supported by the client.
	///
	///If client did not send the `supported_versions` extension, this is `[TLS_1_2]`. If you need to check if
	///the extension was actually sent, use `has_extension(SUPPORTED_VERSIONS)`.
	pub fn get_tls_versions(&self) -> ListOf<'a,TlsVersion> { self.supported_versions }
	///Get the Cookie the client is returning, if any.
	pub fn get_cookie(&self) -> Option<&'a [u8]> { self.cookie }
	///Get the PSK Key Exchange Modes the client supports.
	pub fn get_psk_key_exchange_modes(&self) -> ListOf<'a,PskKeyExchangeMode>
	{
		self.supported_psk_key_exchange_modes
	}
	///Get list of Certificate Authorities the client trusts.
	///
	///If this list is empty, the list of trusted certificate authorities is assumed to be pre-agreed.
	pub fn get_certificate_authorities(&self) -> ListOf<'a,CertificateAuthority<'a>>
	{
		self.supported_certificate_authorities
	}
	///The client supports Post-Handshake Authentication.
	pub fn supports_post_handshake_authentication(&self) -> bool { self.supports_post_handshake_auth }
	///Get list of Signature Algorithms the client supports for certificate signing.
	///
	///In case the client did not send `signature_algorithms_cert` extension, this mirrors the list in
	///`signature_algorithms`.
	pub fn get_signature_algorithms_cert(&self) -> ListOf<'a,SignatureScheme>
	{
		self.supported_signature_algorithms_cert
	}
	///Get list of Sequence Number Encryption Algorithms client supports.
	///
	///If this is empty, client does not support `sequence_number_encryption_algorithms` extension.
	pub fn get_sequence_number_encryption_algorithms(&self) -> ListOf<'a,SequenceNumberEncryptionAlgorithm>
	{
		self.supported_sequence_number_encryption_algorithms
	}
	///Get list of Groups/KEMs client sent, together with public keys for each of them, if any.
	///
	///This list is always the same as one returned by `get_groups()`, but also includes public keys.
	///
	///If this list is empty, the client does not support forward-secret key exchanges (only supporting PSK-only
	///key exchange).
	pub fn get_key_shares(&self) -> IntoKeyShareIter<'a> { self.key_shares }
	///The client supports stapling Certificate Transparency v2 Trasnparency Items.
	pub fn supports_transparency_items(&self) -> bool
	{
		self.supports_transparency_info
	}
	///Get the Connection ID the client is using, if any.
	///
	///This is the old extension. `get_connection_id()` should be used instead.
	pub fn get_connection_id_old(&self) -> Option<&'a [u8]> { self.connection_id_old }
	///Get the Connection ID the client is using, if any.
	pub fn get_connection_id(&self) -> Option<&'a [u8]> { self.connection_id }
	///Get the external session ID hash, if any.
	pub fn get_external_id_hash(&self) -> Option<&'a [u8]> { self.external_id_hash }
	///Get the external session ID, if any.
	pub fn get_external_session_id(&self) -> Option<&'a [u8]> { self.external_session_id }
	///Get the raw QUIC transport parameters, if any.
	pub fn get_quic_transport_parameters(&self) -> Option<&'a [u8]> { self.quic_transport_parameters }
	///Get number of tickets the client is requesting, if any.
	///
	///The first amount is for full handshake, the second is for resumption.
	///
	/// * If contents is `Some((0,0))`, the server SHOULD assume client does not support tickets..
	/// * If contents is `Some((1,0))`, the server SHOULD assume client prefers to reuse tickets.
	pub fn get_requested_ticket_count(&self) -> Option<(u8,u8)> { self.requested_ticket_count }
	///Get port number the client is accessing, if available.
	pub fn get_destination_port_number(&self) -> Option<u16> { self.destination_port_number }
	///Get ID of connection the client is trying to renegotiate.
	///
	///If client is not trying to renegotiate, `Some(b"")`.
	///
	///Warning: Client is exploitably vulernable if this is `None`. Servers SHOULD CONSIDER terminating the
	///handshake.
	pub fn get_renegotiated_connection(&self) -> Option<&'a [u8]> { self.renegotiated_connection }
	///Parse client hello to its component parts.
	///
	///The `key_ok` method is used to check the received key shares (those are KEM public keys)..
	pub fn parse(msg: &'a [u8], key_ok: KeyCheck, mut dbg: DbgT) ->
		Result<ClientHello<'a>, ClientHelloParseError>
	{
		parse_ch(Message::new(msg), key_ok, &mut dbg).map_err(ClientHelloParseError)
	}
}

impl<'a> ClientHello<'a>
{
	///Was specified extension sent?
	pub fn has_extension(&self, ext: IanaExtension) -> bool
	{
		match crate::extensions::extension_bit(ext) {
			Some(bit) => {
				if let Some(rw) = self.extension_mask.get(bit / 32) {
					*rw & 1u32 << bit % 32 != 0
				} else {
					false
				}
			},
			None => self.extensions.iter_all().any(|e|{
				e.extension_type == ext
			})
		}
	}
}

#[cfg(test)] mod test;
