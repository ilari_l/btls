use crate::DbgT;
use crate::ExtensionError;
use crate::LEN_EXTENSION_LIST;
use crate::LEN_PINNING_PROOF;
use crate::LEN_PINNING_TICKET;
use crate::MaximumFragmentLength;
use crate::RemoteFeatureSet;
use crate::errors::Struct as EStruct;
use crate::extensions as exts;
use crate::iters2::AlertForError;
use crate::iters2::Extension;
use crate::iters2::IntoGenericIterator as ListOf;
use crate::iters2::Message;
use crate::iters2::PinningProof;
use crate::iters2::PinningTicket;
use btls_aux_fail::fail;
use btls_aux_tls_iana::CertificateType;
use btls_aux_tls_iana::ExtensionType;
use core::marker::PhantomData;

macro_rules! field
{
	(E $location:ident $x:expr) => {
		$x.map_err(crate::errors::Struct::$location).map_err(ExtensionError::Structured)?
	};
	(S $location:ident $x:expr) => {
		$x.map_err(crate::errors::Struct::$location).map_err(_EncryptedExtensionsParseError::Structured)?
	};
}

macro_rules! extension
{
	($exts2:ident $dbg:ident $name:ident $handler:expr) => {
		$exts2.with_stdext(ExtensionType::$name, _EncryptedExtensionsParseError::JunkAfterExtension,
			_EncryptedExtensionsParseError::Extension, $dbg, $handler)?
	}
}

///Error from parsing Encrypted Extensions.
///
///The most notable things to query errors are the `alert()` method and the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct EncryptedExtensionsParseError(_EncryptedExtensionsParseError);

#[derive(Copy,Clone,Debug)]
enum _EncryptedExtensionsParseError
{
	Structured(EStruct),
	Extension(ExtensionType, ExtensionError),
	JunkAfterExtensions,
	JunkAfterExtension(ExtensionType),
}

impl EncryptedExtensionsParseError
{
	///Get the alert code one should use for this parse error.
	pub fn alert(&self) -> btls_aux_tls_iana::Alert
	{
		use self::_EncryptedExtensionsParseError::*;
		use btls_aux_tls_iana::Alert as A;
		match &self.0 {
			&Structured(ref err) => err.alert(),
			&Extension(_, ref err) => err.alert(),
			&JunkAfterExtensions => A::DECODE_ERROR,
			&JunkAfterExtension(_) => A::DECODE_ERROR,
		}
	}
}

impl core::fmt::Display for EncryptedExtensionsParseError
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		use self::_EncryptedExtensionsParseError::*;
		match &self.0 {
			&Structured(ref err) => core::fmt::Display::fmt(err, f),
			&Extension(ext, ref err) => write!(f, "extension {ext}: {err}"),
			&JunkAfterExtensions => write!(f, "Junk after end of extensions"),
			&JunkAfterExtension(ext) => write!(f, "Junk after end of extension {ext}"),
		}
	}
}


fn parse_ee<'a>(mut msg: Message<'a>, enable: RemoteFeatureSet, dbg: &mut DbgT) ->
	Result<EncryptedExtensions<'a>, _EncryptedExtensionsParseError>
{
	// struct {
	//	 Extension extensions<0..2^16-1>;
	// } EncryptedExtensions;
	//
	// - RFC8446 section 4.3.1.
	use self::_EncryptedExtensionsParseError as EEPE;
	let extensions = field!(S Extensions msg.list_of::<Extension<'a>>(LEN_EXTENSION_LIST));
	msg.assert_end(EEPE::JunkAfterExtensions)?;

	let exts2 = field!(S Extensions2 exts::check_extensions_block(extensions, |ext|{
		ext.allowed_in_encrypted_extensions() && crate::ask_extension(enable, ext)
	}));

	let acknowledge_server_name = extension!(exts2 dbg SERVER_NAME exts::ext_acknowledge).is_some();

	let max_fragment_length = extension!(exts2 dbg MAX_FRAGMENT_LENGTH |extn, extv, dbg|{
		exts::serverext_max_fragment_length(extn, extv, enable, dbg)
	});

	let supported_groups = extension!(exts2 dbg SUPPORTED_GROUPS exts::ext_supported_groups).unwrap_or_default();

	let use_srtp = extension!(exts2 dbg USE_SRTP |extn, extv, dbg|{
		exts::serverext_use_srtp(extn, extv, enable, dbg)
	});

	let heartbeat = extension!(exts2 dbg HEARTBEAT exts::ext_heartbeat);

	let chosen_alpn_protocol = extension!(exts2 dbg APPLICATION_LAYER_PROTOCOL_NEGOTIATION |extn, extv, dbg|{
		exts::serverext_alpn(extn, extv, enable, dbg)
	});

	let client_certificate_type = extension!(exts2 dbg CLIENT_CERTIFICATE_TYPE |extn, extv, dbg|{
		exts::serverext_peer_certificate_type(extn, extv, enable, dbg,
			crate::ask_client_certificate_type, EStruct::ClientCertificateType)
	});

	let server_certificate_type = extension!(exts2 dbg SERVER_CERTIFICATE_TYPE |extn, extv, dbg|{
		exts::serverext_peer_certificate_type(extn, extv, enable, dbg,
			crate::ask_server_certificate_type, EStruct::ServerCertificateType)
	});

	let record_size_limit = extension!(exts2 dbg RECORD_SIZE_LIMIT exts::ext_record_size_limit);

	let ticket_pinning = extension!(exts2 dbg TICKET_PINNING |extn, extv, dbg|{
		// struct {
		//	 select (Role) {
		//		 <...>
		//		 case server:
		//			 pinning_proof proof<0..2^8-1>; //no proof on 1st connection
		//			 pinning_ticket ticket<0..2^16-1>; //omitted on ramp down
		//			 uint32 lifetime;
		//	 }
		// } PinningTicketExtension;
		//
		// - RFC8672 section 3.
		let xproof: Option<PinningProof<'a>> = field!(E Proof extv.list_at_most_one(LEN_PINNING_PROOF));
		let xticket: Option<PinningTicket<'a>> = field!(E Ticket extv.list_at_most_one(LEN_PINNING_TICKET));
		let lifetime: u32 = field!(E Lifetime extv.cp());
		let xproof = xproof.map(|x|x.0);
		let xticket = xticket.map(|x|x.0);
		match (xproof, xticket) {
			(None, Some(ticket)) => debug!(E dbg extn "Ramp up, {ticketl} byte ticket",
				ticketl=ticket.len()),
			(Some(proof), Some(ticket)) =>
				debug!(E dbg extn "Maintain, {proofl} byte proof, {ticketl} byte ticket",
					proofl=proof.len(), ticketl=ticket.len()),
			(Some(proof), None) => debug!(E dbg extn "Ramp down, {proofl} byte proof", proofl=proof.len()),
			(None, None) => fail!(ExtensionError::BadPinningTicket),
		};
		Ok((xproof, xticket, lifetime))
	});

	let chosen_ekt_cipher = extension!(exts2 dbg SUPPORTED_EKT_CIPHERS |extn, extv, dbg|{
		exts::serverext_supported_ekt_ciphers(extn, extv, enable, dbg)
	});

	let accept_early_data = extension!(exts2 dbg EARLY_DATA |extn, _, dbg|{
		debug!(E dbg extn "Server accepted early data");
		Ok(())
	}).is_some();

	let external_id_hash = extension!(exts2 dbg EXTERNAL_ID_HASH exts::ext_external_id_hash);
	let external_session_id = extension!(exts2 dbg EXTERNAL_SESSION_ID exts::ext_external_session_id);

	let quic_transport_parameters =
		extension!(exts2 dbg QUIC_TRANSPORT_PARAMETERS exts::ext_quic_transport_parameters);

	let expected_ticket_count = extension!(exts2 dbg TICKET_REQUEST |extn, extv, dbg|{
		let expected_count: u8 = field!(E ExpectedCount extv.cp());
		debug!(E dbg extn "{expected_count} tickets expected");
		Ok(expected_count)
	});

	exts::report_unknown_extensions(extensions, dbg);

	Ok(EncryptedExtensions {
		acknowledge_server_name,
		chosen_client_certificate_type: client_certificate_type.unwrap_or(CertificateType::X509),
		chosen_server_certificate_type: server_certificate_type.unwrap_or(CertificateType::X509),
		use_srtp,
		heartbeat,
		chosen_alpn_protocol,
		external_id_hash,
		external_session_id,
		max_fragment_length,
		record_size_limit,
		chosen_ekt_cipher,
		accept_early_data,
		quic_transport_parameters,
		supported_groups,
		ticket_pinning,
		expected_ticket_count,
		extensions,
		x: PhantomData
	})
}

///Parsed Encrypted Extensions message. 
#[derive(Clone)]
pub struct EncryptedExtensions<'a>
{
	acknowledge_server_name: bool,
	chosen_client_certificate_type: CertificateType,
	chosen_server_certificate_type: CertificateType,
	use_srtp: Option<(btls_aux_tls_iana::DtlsSrtpProtectionProfile, &'a [u8])>,
	heartbeat: Option<bool>,
	chosen_alpn_protocol: Option<btls_aux_tls_iana::AlpnProtocolId<'a>>,
	external_id_hash: Option<&'a [u8]>,
	external_session_id: Option<&'a [u8]>,
	max_fragment_length: Option<MaximumFragmentLength>,
	record_size_limit: Option<u16>,
	chosen_ekt_cipher: Option<btls_aux_tls_iana::EktCipher>,
	accept_early_data: bool,
	quic_transport_parameters: Option<&'a [u8]>,
	supported_groups: ListOf<'a,btls_aux_tls_iana::NamedGroup>,
	ticket_pinning: Option<(Option<&'a [u8]>, Option<&'a [u8]>, u32)>,
	expected_ticket_count: Option<u8>,
	extensions: ListOf<'a,Extension<'a>>,
	x: PhantomData<&'a u8>
}

impl<'a> EncryptedExtensions<'a>
{
	///Get extensions.
	///
	///This can be used to parse extensions other than the explicitly supported ones.
	pub fn get_extensions(&self) -> ListOf<'a, Extension<'a>> { self.extensions }
	///Did the server acknowledge `server_name`?
	pub fn acknowledged_server_name(&self) -> bool { self.acknowledge_server_name }
	///Get the maximum fragment size, if any.
	///
	///This is old deprecated extension. Use `record_size_limit` instead.
	pub fn get_maximum_fragment_length(&self) -> Option<MaximumFragmentLength>
	{
		self.max_fragment_length
	}
	///Get Groups/KEMs the server supports.
	///
	///This is purely a hint for the client for future connections. If the list is empty, the server did not
	///send the hint.
	pub fn get_groups(&self) -> ListOf<'a, btls_aux_tls_iana::NamedGroup> { self.supported_groups }
	///Get the DTLS-SRTP protection profile to use, if any.
	pub fn get_dtls_srtp_protection_profile(&self) -> Option<btls_aux_tls_iana::DtlsSrtpProtectionProfile>
	{
		self.use_srtp.map(|(x,_)|x)
	}
	///Get the DTLS-SRTP Master Key Indicator to use.
	///
	///This value is meaningless if `get_dtls_srtp_protection_profile()` returns `None`.
	pub fn get_dtls_srtp_mki(&self) -> &'a [u8] { self.use_srtp.map(|(_,y)|y).unwrap_or(b"") }
	///Get the heartbeat support of the server.
	///
	///`Some(true)` means server has granted permission for client to send heartbeat messages to the server.
	///`Some(false)` means client MUST NOT send heartbeat messages to the server, but server might be able to
	///send heartbeat to client.
	pub fn get_heartbeat(&self) -> Option<bool> { self.heartbeat }
	///Get the server-chosen ALPN Protocol ID, if any.
	///
	///If this value is `None`, the server did not pick any explicit ALPN.
	pub fn get_alpn_protocol_id(&self) -> Option<btls_aux_tls_iana::AlpnProtocolId<'a>>
	{
		self.chosen_alpn_protocol
	}
	///Get the server-chosen client certificate type.
	///
	///If the server did not indicate otherwise, this is `X509`.
	pub fn get_client_certificate_type(&self) -> CertificateType { self.chosen_client_certificate_type }
	///Get the server-chosen server certificate type.
	///
	///If the server did not indicate otherwise, this is `X509`.
	pub fn get_server_certificate_type(&self) -> CertificateType { self.chosen_server_certificate_type }
	///Get maximum size of record plaintext for encrypted record the server supports.
	///
	///If standard TLS limits apply, this is `None`.
	pub fn get_record_size_limit(&self) -> Option<usize> { self.record_size_limit.map(|x|x as usize) }
	///Get the new pinning ticket the server issued, if any.
	///
	///This is `None` if server wants to break the pin, or does not support pinning tickets in the first place.
	pub fn get_new_pinning_ticket(&self) -> Option<&'a [u8]> { self.ticket_pinning.and_then(|(x,_,_)|x) }
	///Get the pinning proof the server sent.
	///
	///This is `None` if server is not responding to pinning ticket sent by the client.
	pub fn get_pinning_proof(&self) -> Option<&'a [u8]>  { self.ticket_pinning.and_then(|(_,y,_)|y) }
	///Get the lifetime of the pinning ticket in seconds.
	///
	///The possible combinations of `get_new_pinning_ticket()` and `get_pinning_proof()` are:
	///
	/// * `None/None`: Client or server does not support ticket pinning.
	/// * `Some/None`: Client and server establish a fresh pin.
	/// * `Some/Some`: Client and server have existing pin, and server extends the pin.
	/// * `None/Some`: Client and server have existing pin, which the server breaks.
	///
	///This is meaningless if `get_new_pinning_ticket()` is `None`.
	pub fn get_pinning_ticket_lifetime(&self) -> u32 { self.ticket_pinning.map(|(_,_,z)|z).unwrap_or(0) }
	///Get the server-chosen EKT cipher, if any.
	pub fn get_ekt_cipher(&self) -> Option<btls_aux_tls_iana::EktCipher> { self.chosen_ekt_cipher }
	///Did the server acknowledge receiving early data?
	pub fn acknowledged_early_data(&self) -> bool { self.accept_early_data }
	///Get the external session ID hash, if any.
	pub fn get_external_id_hash(&self) -> Option<&'a [u8]> { self.external_id_hash }
	///Get the external session ID, if any.
	pub fn get_external_session_id(&self) -> Option<&'a [u8]> { self.external_session_id }
	///Get the raw QUIC transport parameters, if any.
	pub fn get_quic_transport_parameters(&self) -> Option<&'a [u8]> { self.quic_transport_parameters }
	///Get the ticket count the server expects to send.
	pub fn get_ticket_count(&self) -> Option<u8> { self.expected_ticket_count }
	///Parse Encrypted Extensions message.
	///
	///The parameter `enabled` controls what TLS features are enabled. This is used to enforce the TLS
	///restriction that answers can not contain algorithms or extensions not present in offers. For server
	///certificate, the corresponding offer is Client Hello, and for client certificate, the corresponding
	///offer is Certificate Request. Most features can end up being requested.
	pub fn parse(msg: &'a [u8], enable: RemoteFeatureSet, dbg: &mut DbgT) ->
		Result<EncryptedExtensions<'a>, EncryptedExtensionsParseError>
	{
		parse_ee(Message::new(msg), enable, dbg).map_err(EncryptedExtensionsParseError)
	}
}
