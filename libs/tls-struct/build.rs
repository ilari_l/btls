use btls_aux_codegen::BareLiteral;
use btls_aux_codegen::CodeBlock;
use btls_aux_codegen::mkid;
use btls_aux_codegen::mkidf;
use btls_aux_codegen::quote;
use btls_aux_codegen::qwrite;
use btls_aux_codegen::Symbol;
use std::env;
use std::fs::File;
use std::io::Write as IoWrite;
use std::path::Path;

struct SEntryExtra
{
	name: &'static str,
	argument: Option<&'static str>,
	fixed_error: Option<&'static str>,
	field: Option<&'static str>,
	display: &'static str,
}

struct SEntryWrite
{
	symbol: Symbol,
	args: CodeBlock,
	fixed_alert: Option<Symbol>,
	has_arg: bool,
	display_rhs: CodeBlock,
}

impl SEntryWrite
{
	fn symbol(&self) -> Symbol { self.symbol.clone() }
	fn args(&self) -> CodeBlock { self.args.clone() }
	fn alert_arg(&self) -> CodeBlock
	{
		match (self.has_arg, self.fixed_alert.is_none()) {
			(true, true) => quote!((ref err)),
			(true, false) => quote!((_)),
			(false, _) => quote!(),
		}
	}
	fn alert_rhs(&self) -> CodeBlock
	{
		match self.fixed_alert.as_ref() {
			Some(fe) => quote!(Alert::#fe),
			None => quote!(err.alert()),
		}
	}
	fn display_arg(&self) -> CodeBlock
	{
		match self.has_arg {
			true => quote!((ref err)),
			false => quote!()
		}
	}
	fn display_rhs(&self) -> CodeBlock { self.display_rhs.clone() }
}


//Symbol, name, type.
fn write_sentry_table(f: &mut impl IoWrite, name: &str, s: &[(&str, &str, &str)], extra: &[SEntryExtra])
{
	let tname = mkid(name);
	let mut entries: Vec<SEntryWrite> = Vec::new();
	for (symbol,fname,dtype) in s.iter() {
		let dfmt = if let Some(split) = fname.find("::") {
			format!("{fld}::{var}: {{err}}", fld=&fname[..split], var=&fname[split+2..])
		} else {
			format!("{fname}: {{err}}")
		};
		let fieldarg = BareLiteral(dtype);	//Abuse BareLiteral to write this raw.
		entries.push(SEntryWrite {
			symbol: mkid(symbol),
			args: quote!((#fieldarg)),
			fixed_alert: None,
			has_arg: true,
			display_rhs: quote!(write!(f, #dfmt)),
		});
	}
	for extra in extra.iter() {
		let fieldarg = match extra.argument {
			Some(arg) => {
				let arg = BareLiteral(arg);	//Abuse BareLiteral to write this raw.
				quote!((#arg))
			},
			None => quote!(),
		};
		if extra.argument.is_none() && extra.fixed_error.is_none() {
			panic!("At least of argument and fixed_error is required")
		}
		let fieldpfx = if let Some(field) = extra.field {
			if let Some(split) = field.find("::") {
				format!("{fld}::{var}: ", fld=&field[..split], var=&field[split+2..])
			} else {
				format!("{field}: ")
			}
		} else {
			String::new()
		};
		let drhs = if extra.argument.is_some() {
			let fstr = format!("{fieldpfx}{{}}");
			let edisplay = BareLiteral(extra.display);	//Write this raw.
			quote!(write!(f, #fstr, #edisplay))
		} else {
			let pstr = format!("{fieldpfx}{extra}", extra=extra.display);
			quote!(f.write_str(#pstr))
		};
		entries.push(SEntryWrite {
			symbol: mkid(extra.name),
			args: fieldarg,
			fixed_alert: extra.fixed_error.map(mkid),
			has_arg: extra.argument.is_some(),
			display_rhs: drhs,
		});
	}
	qwrite(f, quote!{
#[derive(Copy,Clone,Debug)]
pub(crate) enum #tname
{
	#([entries] #symbol#args,)*
}

impl AlertForError for #tname
{
	fn alert(&self) -> Alert
	{
		match self {
			#([entries] &#!REPEAT tname::#symbol#alert_arg => #alert_rhs),*
		}
	}
}

impl core::fmt::Display for #tname
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		match self {
			#([entries] &#!REPEAT tname::#symbol#display_arg => #display_rhs),*
		}
	}
}
	});
}

macro_rules! exception
{
	(VARIANT $name:ident $stype:ident) => {
		SEntryExtra {
			name: stringify!($name),
			argument: Some(stringify!($stype)),
			fixed_error: None,
			field: None,
			display: "err",
		}
	};
	(SIMPLE[$field:ident] $name:ident $alert:ident $msg:expr) => {
		SEntryExtra {
			name: stringify!($name),
			argument: None,
			fixed_error: Some(stringify!($alert)),
			field: Some(stringify!($field)),
			display: $msg,
		}
	};
	(SIMPLE $name:ident $alert:ident $msg:expr) => {
		SEntryExtra {
			name: stringify!($name),
			argument: None,
			fixed_error: Some(stringify!($alert)),
			field: None,
			display: $msg,
		}
	};
}

static EXTENSION_WHITELIST: &'static str = "
SERVER_NAME
MAX_FRAGMENT_LENGTH
CLIENT_CERTIFICATE_URL
TRUSTED_CA_KEYS
TRUNCATED_HMAC
STATUS_REQUEST
USER_MAPPING
CLIENT_AUTHZ
SERVER_AUTHZ
CERT_TYPE
SUPPORTED_GROUPS
EC_POINT_FORMATS
SRP
SIGNATURE_ALGORITHMS
USE_SRTP
HEARTBEAT
APPLICATION_LAYER_PROTOCOL_NEGOTIATION
STATUS_REQUEST_V2
SIGNED_CERTIFICATE_TIMESTAMP
CLIENT_CERTIFICATE_TYPE
SERVER_CERTIFICATE_TYPE
PADDING
ENCRYPT_THEN_MAC
EXTENDED_MASTER_SECRET
TOKEN_BINDING
CACHED_INFO
TLS_LTS
COMPRESS_CERTIFICATE
RECORD_SIZE_LIMIT
#PWD_PROTECT not included as Dragonfly/TLS is too screwed up.
#PWD_CLEAR not included as Dragonfly/TLS is too screwed up.
#PASSWORD_SALT not included as Dragonfly/TLS is too screwed up.
TICKET_PINNING
TLS_CERT_WITH_EXTERN_PSK
DELEGATED_CREDENTIALS
SESSION_TICKET
#TLMSP not implemented yet.
#
# It is about pageful of TLS syntax notation...
#
#TLMSP_PROXYING not implemented yet.
#
# Structure is:
#   uint32 hs_id
#
#TLMSP_DELEGATE not implemented yet.
#
# Structure is:
#   uint32 hs_id
#   opaque token<0...255>
#
SUPPORTED_EKT_CIPHERS
PRE_SHARED_KEY
EARLY_DATA
SUPPORTED_VERSIONS
COOKIE
PSK_KEY_EXCHANGE_MODES
CERTIFICATE_AUTHORITIES
OID_FILTERS
POST_HANDSHAKE_AUTH
SIGNATURE_ALGORITHMS_CERT
KEY_SHARE
TRANSPARENCY_INFO
CONNECTION_ID_OLD
CONNECTION_ID
EXTERNAL_ID_HASH
EXTERNAL_SESSION_ID
QUIC_TRANSPORT_PARAMETERS
TICKET_REQUEST
DNSSEC_CHAIN
SEQUENCE_NUMBER_ENCRYPTION_ALGORITHMS
RENEGOTIATION_INFO
";


fn main()
{
	let out_dir = env::var("OUT_DIR").unwrap();
	let dest_path = Path::new(&out_dir).join("autogenerated-errors.inc.rs");
	let mut f = File::create(&dest_path).unwrap();
	write_sentry_table(&mut f, "Struct", &[
		//Shared between HelloVerifyRequest message, and cookie extension.
		("Cookie", "cookie", "SliceReadError"),
		//Shared between ClientHello message, and ServerHello message.
		("Random", "random", "PrimitiveReadError"),
		("SessionId2", "session_id", "SliceReadError"),
		//Shared between many messages.
		("Extensions", "extensions", "VectorError<crate::iters2::Extension<'static>>"),
		("Extensions2", "extensions", "crate::extensions::ExtensionError2"),
		//Shared between Certificate message, and CertificateRequest message.
		("CertificateRequestContext", "certificate_request_context", "SliceReadError"),
		//Shared between client_authz extension and server_authz extension.
		("AuthzFormatList", "authz_format_list", "VectorError<btls_aux_tls_iana::AuthorizationDataFormat>"),
		//Shared between signature_algorithms extension, signature_algorithms_cert extension, and
		//delegated_credentials extension.
		("SupportedSignatureAlgorithms", "supported_signature_algorithms", "VectorError<btls_aux_tls_iana::SignatureScheme>"),
		//ClientHello message.
		("ClientVersion", "client_version", "ItemError<btls_aux_tls_iana::TlsVersion>"),
		("CipherSuites", "cipher_suites", "VectorError<btls_aux_tls_iana::CipherSuite>"),
		("CompressionMethods", "compression_methods", "VectorError<btls_aux_tls_iana::CompressionMethod>"),
		//ServerHello message.
		("ServerVersion", "server_version", "ItemError<btls_aux_tls_iana::TlsVersion>"),
		("CipherSuite", "cipher_suite", "ItemError<btls_aux_tls_iana::CipherSuite>"),
		("CompressionMethod", "compression_method", "ItemError<btls_aux_tls_iana::CompressionMethod>"),
		//Certificate message.
		("CertificateList", "certificate_list","VectorError<crate::iters2::CertificateEntry<'static>>"),
		("CertificateList2", "certificate_list","VectorError<crate::iters2::CertificateEntry2<'static>>"),
		("SubjectPublicKeyInfo", "ASN.1_subject_public_key_info", "SliceReadError"),
		("DescriptorType", "descriptor_type", "OpenPgpDescriptor"),
		("EmptyCert", "empty_cert", "OpenPgpEmptyCert"),
		("OpenPgpKeyId", "subkey_cert.open_pgp_key_id", "SliceReadError"),
		("OpenPgpCert", "subkey_cert.open_pgp_cert", "SliceReadError"),
		("OpenPgpFpKeyId", "subkey_cert_fingerprint.open_pgp_key_id", "SliceReadError"),
		("OpenPgpFpCert", "subkey_cert.open_pgp_cert_fingerprint", "SliceReadError"),
		//CertificateRequest message.
		("CertificateAuthorities", "certificate_authorities", "VectorError<crate::iters2::CertificateAuthority<'static>>"),
		("CertificateTypes2", "certificate_types", "VectorError<btls_aux_tls_iana::ClientCertificateType>"),
		//EktKey message.
		("EktKeyValue", "ekt_key_value", "SliceReadError"),
		("SrtpMasterSalt", "srtp_master_salt", "SliceReadError"),
		("EktSpi", "ekt_spi", "PrimitiveReadError"),
		("EktTtl", "ekt_ttl", "PrimitiveReadError"),
		//SupplementalData message.
		("SuppData", "supp_data", "VectorError<crate::iters2::SupplementalDataEntry<'static>>"),
		//RequestConnectionId messsage.
		("CidCount", "count", "PrimitiveReadError"),
		//HelloVerifyRequest message.
		("ServerVersion2", "server_version", "PrimitiveReadError"),
		//CertificateUrl message.
		("ChainType", "type", "PrimitiveReadError"),
		("UrlAndHashList", "url_and_hash_list", "VectorError<crate::iters2::UrlAndHash<'static>>"),
		//NewConnectionId message.
		("Usage", "usage", "PrimitiveReadError"),
		("Cids", "cids", "VectorError<crate::iters2::ConnectionId<'static>>"),
		//NewSessionTicket message.
		("TicketLifetime", "ticket_lifetime", "PrimitiveReadError"),
		("TicketAgeAdd", "ticket_age_add", "PrimitiveReadError"),
		("TicketNonce", "ticket_nonce", "SliceReadError"),
		("Ticket2", "ticket", "SliceReadError"),
		//server_name extension.
		("ServerNameList", "server_name_list", "VectorError<crate::iters2::ServerName<'static>>"),
		//max_fragment_length extension.
		("MaximumFragmentLength", "maximum_fragment_length", "MaximumFragmentLength"),
		//trusted_ca_keys extension.
		("TrustedAuthoritiesList", "trusted_authorities_list", "VectorError<crate::iters2::TrustedAuthority<'static>>"),
		//status_request extension.
		("StatusType", "status_type", "PrimitiveReadError"),
		("RequestOcsp", "request::ocsp", "crate::iters2::OcspStatusRequestError"),
		//user_mapping extension.
		("UserMappingTypes", "user_mapping_types", "VectorError<btls_aux_tls_iana::UserMappingType>"),
		//cert_type extension.
		("CertificateTypes", "certificate_types", "VectorError<btls_aux_tls_iana::CertificateType>"),
		("CertificateType", "certificate_type", "ItemError<btls_aux_tls_iana::CertificateType>"),
		//supported_groups extension.
		("NamedGroupList", "named_group_list", "VectorError<btls_aux_tls_iana::NamedGroup>"),
		//ec_point_formats extension.
		("EcPointFormatList", "ec_point_format_list", "VectorError<btls_aux_tls_iana::EcPointFormat>"),
		//srp extension.
		("SrpI", "srp_I", "SliceReadError"),
		//use_srtp extension.
		("SrtpProtectionProfiles", "srtp_protection_profiles", "VectorError<btls_aux_tls_iana::DtlsSrtpProtectionProfile>"),
		("SrtpProtectionProfiles2", "srtp_protection_profiles", "ItemError<btls_aux_tls_iana::DtlsSrtpProtectionProfile>"),
		("SrtpMki", "srtp_mki", "SliceReadError"),
		//heartbeat extension.
		("Mode", "mode", "ItemError<btls_aux_tls_iana::HeartbeatMode>"),
		//application_layer_protocol_negotiation extension.
		("ProtocolNameList", "protocol_name_list", "VectorError<btls_aux_tls_iana::AlpnProtocolId<'static>>"),
		("ProtocolNameList2", "protocol_name_list", "ItemError<btls_aux_tls_iana::AlpnProtocolId<'static>>"),
		//status_request_v2 extension.
		("CertificateStatusReqList", "certificate_status_req_list", "VectorError<crate::iters2::CertificateStatusRequestItemV2<'static>>"),
		//signed_certificate_timestamp extension.
		("SctList", "sct_list","VectorError<crate::iters2::SerializedSCT<'static>>"),
		//client_certificate_type extension.
		("ClientCertificateTypes", "client_certificate_types", "VectorError<btls_aux_tls_iana::CertificateType>"),
		("ClientCertificateType", "client_certificate_type", "ItemError<btls_aux_tls_iana::CertificateType>"),
		//server_certificate_types extension.
		("ServerCertificateTypes", "server_certificate_types", "VectorError<btls_aux_tls_iana::CertificateType>"),
		("ServerCertificateType", "server_certificate_type", "ItemError<btls_aux_tls_iana::CertificateType>"),
		//token_binding extension.
		("TbVerMajor", "token_binding_version.major", "PrimitiveReadError"),
		("TbVerMinor", "token_binding_version.minor", "PrimitiveReadError"),
		("KeyParametersList", "key_parameters_list", "VectorError<btls_aux_tls_iana::TokenBindingKeyParameter>"),
		("KeyParametersList2", "key_parameters_list", "ItemError<btls_aux_tls_iana::TokenBindingKeyParameter>"),
		//cached_info extension.
		("CachedInfo", "cached_info", "VectorError<crate::iters2::CachedObject<'static>>"),
		//compress_certificate extension.
		("Algorithms", "algorithms", "VectorError<btls_aux_tls_iana::CertificateCompressionAlgorithmId>"),
		("Algorithm2", "algorithm", "ItemError<btls_aux_tls_iana::CertificateCompressionAlgorithmId>"),
		("UncompressedLength", "uncompressed_length", "PrimitiveReadError"),
		("CompressedCertificateMessage", "compressed_certificate_message", "SliceReadError"),
		//record_size_limit extension.
		("RecordSizeLimit", "record_size_limit", "PrimitiveReadError"),
		//ticket_pinning extension.
		("Ticket", "ticket", "ItemError<crate::iters2::PinningTicket<'static>>"),
		("Lifetime", "lifetime","PrimitiveReadError"),
		("Proof", "proof","ItemError<crate::iters2::PinningProof<'static>>"),
		//delegated_credentials extension.
		("Algorithm", "algorithm", "ItemError<btls_aux_tls_iana::SignatureScheme>"),
		("Signature", "signature", "SliceReadError"),
		("ValidTime", "cred.valid_time", "PrimitiveReadError"),
		("SubjectPublicKeyInfo2", "cred.subject_public_key_info", "SliceReadError"),
		("DcCertVerifyAlgorithm", "cred.dc_cert_verify_algorithm", "ItemError<btls_aux_tls_iana::SignatureScheme>"),
		//supported_ekt_ciphers extension.
		("SupportedCiphers", "supported_ciphers", "VectorError<btls_aux_tls_iana::EktCipher>"),
		("SelectedCipher", "selected_cipher", "ItemError<btls_aux_tls_iana::EktCipher>"),
		//pre_shared_key extension.
		("Identities", "identities", "VectorError<crate::iters2::PskIdentity<'static>>"),
		("Binders", "binders", "VectorError<crate::iters2::PskBinderEntry<'static>>"),
		("SelectedIdentity", "selected_identity", "crate::errors::PskIndex"),
		//early_data extension.
		("MaxEarlyDataSize", "max_early_data_size", "PrimitiveReadError"),
		//supported_versions extension.
		("Versions", "versions", "VectorError<btls_aux_tls_iana::TlsVersion>"),
		("SelectedVersion", "selected_version", "ItemError<btls_aux_tls_iana::TlsVersion>"),
		//psk_key_exchange_modes extension.
		("KeModes", "ke_modes", "VectorError<btls_aux_tls_iana::PskKeyExchangeMode>"),
		//certificate_authorities extension.
		("Authorities", "authorities", "VectorError<crate::iters2::CertificateAuthority<'static>>"),
		//oid_filters extension.
		("Filters", "filters", "VectorError<crate::iters2::OidFilter<'static>>"),
		//key_share extension.
		("ClientShares", "client_shares", "VectorError<crate::iters2::KeyShareEntry<'static>>"),
		("SelectedGroup", "selected_group", "ItemError<btls_aux_tls_iana::NamedGroup>"),
		("ServerShareGroup", "server_share.group", "ItemError<btls_aux_tls_iana::NamedGroup>"),
		("ServerShareKeyExchange", "server_share.key_exchange", "SliceReadError"),
		//transparency_info extension.
		("TransItemList", "trans_item_list","VectorError<crate::iters2::SerializedTransItem<'static>>"),
		//connection_id extension.
		("Cid", "cid", "SliceReadError"),
		//external_id_hash extension.
		("BindingHash", "binding_hash", "SliceReadError"),
		//external_session_id extension.
		("SessionId", "session_id", "SliceReadError"),
		//ticket_request extension.
		("NewSessionCount", "new_session_count", "PrimitiveReadError"),
		("ResumptionCount", "resumption_count", "PrimitiveReadError"),
		("ExpectedCount", "expected_count","PrimitiveReadError"),
		//dnssec_chain extension.
		("PortNumber", "port_number", "PrimitiveReadError"),
		("ExtSupportLifetime", "ext_support_lifetime","PrimitiveReadError"),
		("AuthenticationChain", "authentication_chain","SliceReadError"),
		//sequence_number_encryption_algorithms extension.
		("SupportedAlgs", "supported_algs", "VectorError<btls_aux_tls_iana::SequenceNumberEncryptionAlgorithm>"),
		("SelectedAlg", "selected_alg", "ItemError<btls_aux_tls_iana::SequenceNumberEncryptionAlgorithm>"),
		//renegotiation_info extension.
		("RenegotiatedConnection", "renegotiated_connection", "SliceReadError"),
/*
*/
	], &[]);
	write_sentry_table(&mut f, "PskIndex", &[
	], &[
		exception!(VARIANT Read PrimitiveReadError),
		SEntryExtra {
			name: "InvalidIndex",
			argument: Some("u16"),
			fixed_error: Some("ILLEGAL_PARAMETER"),
			field: None,
			display: r#"&format_args!("Illegal PSK index {err}")"#,
		}
	]);
	write_sentry_table(&mut f, "MaximumFragmentLength", &[
	], &[
		exception!(VARIANT Read PrimitiveReadError),
		SEntryExtra {
			name: "BadValue",
			argument: Some("u8"),
			fixed_error: Some("ILLEGAL_PARAMETER"),
			field: None,
			display: r#"&format_args!("Illegal value {err}")"#,
		},
		SEntryExtra {
			name: "IllegalValue",
			argument: Some("crate::MaximumFragmentLength"),
			fixed_error: Some("ILLEGAL_PARAMETER"),
			field: None,
			display: r#"&format_args!("Illegal value {err:?}")"#,
		}
	]);
	write_sentry_table(&mut f, "OpenPgpDescriptor", &[
	], &[
		exception!(VARIANT Read PrimitiveReadError),
		SEntryExtra {
			name: "BadValue",
			argument: Some("u8"),
			fixed_error: Some("ILLEGAL_PARAMETER"),
			field: None,
			display: r#"&format_args!("Illegal descriptor {err}")"#,
		},
	]);
	write_sentry_table(&mut f, "OpenPgpEmptyCert", &[
	], &[
		exception!(VARIANT Read PrimitiveReadError),
		SEntryExtra {
			name: "BadValue",
			argument: Some("u32"),
			fixed_error: Some("ILLEGAL_PARAMETER"),
			field: None,
			display: r#"&format_args!("Illegal value {err}")"#,
		},
	]);


	let dest_path = Path::new(&out_dir).join("autogenerated-ext-bitmap.inc.rs");
	let mut f = File::create(&dest_path).unwrap();

	let mut known_exts = Vec::new();
	let mut ext_wnum = Vec::new();
	let mut ext_bmask = Vec::new();
	let mut ext_num = Vec::new();
	let mut ext_bit = Vec::new();
	let mut grease_syms = Vec::new();
	let mut grease_val: Vec<u16> = Vec::new();
	fn define_ext2(bit: &mut Vec<usize>, wnum: &mut Vec<usize>, bmask: &mut Vec<u32>, num: &mut Vec<CodeBlock>,
		sym: CodeBlock)
	{
		bit.push(num.len());
		wnum.push(num.len() / 32);
		bmask.push(1u32 << num.len() % 32);
		num.push(sym);
	}
	for ext in EXTENSION_WHITELIST.lines().filter(|s|s.len()>0 && !s.starts_with("#")) {
		let ext = mkid(ext);
		known_exts.push(ext.clone());
		//Use the extension whitelist instead of list in tls-iana, as the former is stable, whereas the other
		//is not and could lead to version skew.
		define_ext2(&mut ext_bit, &mut ext_wnum, &mut ext_bmask, &mut ext_num, quote!(E::#ext));
	}
	for ext in ["EXT_NPN","EXT_ALERT"].iter() {
		let ext = mkid(ext);
		define_ext2(&mut ext_bit, &mut ext_wnum, &mut ext_bmask, &mut ext_num, quote!(#ext));
	}
	//Trick: Do not count GREASE to ext_count, but count it to ext_words. This way GREASE extensions get
	//tracking bits (which are 4 bytes per 32 extensions), and so do not count to 64 extension limit, but not
	//extension descriptors, which are never used because the only purpose of extension descriptors is to read
	//the extension data, which is never needed for GREASE. Extension descriptors are also much more expensive:
	//4 bytes per extension.
	let ext_count_no_grease = ext_num.len();
	for i in 0..16 {
		//Define bits for GREASE so they do not count towards extension limit.
		let ext = mkidf!("GREASE_EXT_{i}");
		define_ext2(&mut ext_bit, &mut ext_wnum, &mut ext_bmask, &mut ext_num, quote!(#ext));
		grease_syms.push(mkidf!("GREASE_EXT_{i}"));
		grease_val.push(2570 + 4112 * i);
	}
	let ext_words = (ext_num.len() + 31) / 32;
	qwrite(&mut f, quote!{
//Some nonstandard extensions that are assigned bit numbers to offload pressure.
const EXT_NPN: E = E::new(13172);
const EXT_ALERT: E = E::new(0x4541);
#(const #grease_syms: E = E::new(#grease_val);)*

pub(crate) const EXTENSION_WORDS: usize = #ext_words;
pub(crate) const EXTENSION_COUNT: usize = #ext_count_no_grease;

pub(crate) fn extension_bit(e: E) -> Option<usize>
{
	let r = match e {
		#(#ext_num => #ext_bit,)*
		_ => return None
	};
	Some(r)
}

pub(crate) fn recheck_extensions(mask: &[u32;EXTENSION_WORDS], ok: impl Fn(E) -> bool) -> Result<(), ExtensionError2>
{
	#(if mask[#ext_wnum]&#ext_bmask != 0 && !ok(#ext_num) { fail!(ExtensionError2::Bogus(#ext_num)); })*
	Ok(())
}

fn known_extension_whitelist(e: E) -> bool
{
	match e {
		#(E::#known_exts => true,)*
		_ => false
	}
}
	});

	println!("cargo:rerun-if-changed=build.rs");	//This has no external deps to rebuild for.
}
