use btls_aux_codegen::mkidf;
use btls_aux_codegen::quote;
use btls_aux_codegen::qwrite;
use btls_aux_codegen::Symbol;
use std::fs::File;
use std::io::Read;
use std::path::Path;
use std::collections::BTreeMap;
use std::borrow::Cow;
use std::str::FromStr;

fn mangle_name<'a>(x: &'a str) -> Cow<'a, str>
{
	//Damn NSAP-PTR.
	if x.find('-').is_some() {
		let mut o = String::new();
		for c in x.chars() { if c == '-' { o.push('_'); } else { o.push(c); } }
		return Cow::Owned(o);
	}
	if x == "*" { return Cow::Borrowed("ANY"); }
	Cow::Borrowed(x)
}

struct NumEntry
{
	num: u16,
	name: String,
}

impl NumEntry
{
	fn new(entry: (&u16, &&str)) -> NumEntry
	{
		NumEntry {
			num: *entry.0,
			name: entry.1.to_string(),
		}
	}
	fn class_sym(&self) -> Symbol { mkidf!("DNSCLASS_{name}", name=mangle_name(&self.name)) }
	fn type_sym(&self) -> Symbol { mkidf!("DNSTYPE_{name}", name=mangle_name(&self.name)) }
	fn class_help(&self) -> String { format!("DNS CLASS {name}", name=self.name) }
	fn type_help(&self) -> String { format!("DNS TYPE {name}", name=self.name) }
	fn num(&self) -> u16 { self.num }
	fn name(&self) -> String { self.name.clone() }
	
}

fn main()
{
	let mut types = String::new();
	let mut classes = String::new();
	File::open("dnstypes.dat").and_then(|mut fp|fp.read_to_string(&mut types)).expect("dnstypes.dat");
	File::open("dnsclasses.dat").and_then(|mut fp|fp.read_to_string(&mut classes)).expect("dnsclasses.dat");
	let out_dir = std::env::var_os("OUT_DIR").unwrap();
	let dest_path = Path::new(&out_dir).join("autogenerated-dnstypes.inc.rs");
	let mut f = File::create(&dest_path).unwrap();

	let mut types_db = BTreeMap::new();
	let mut classes_db = BTreeMap::new();
	for line in types.lines() {
		let s = line.find(" ").expect("No space");
		let (name, num) = line.split_at(s);
		let num = u16::from_str(num.trim()).expect("Not numeric");
		types_db.insert(num, name);
	}
	for line in classes.lines() {
		let s = line.find(" ").expect("No space");
		let (num, name) = line.split_at(s);
		let num = u16::from_str(num.trim()).expect("Not numeric");
		let name = name.trim();
		classes_db.insert(num, name);
	}
	let classes: Vec<_> = classes_db.iter().map(NumEntry::new).collect();
	let types: Vec<_> = types_db.iter().map(NumEntry::new).collect();

	qwrite(&mut f, quote!{
		#([classes]
			#[doc=#class_help]
			pub const #class_sym: Class = Class(#num);
		)*
		#([types]
			#[doc=#type_help]
			pub const #type_sym: Type = Type(#num);
		)*
		#[deprecated(since="1.0.2", note="Use Class::name() instead")]
		pub fn class_name(x: Class) -> Result<&'static str, u16>
		{
			match x {
				#([classes] #class_sym => Ok(#name),)*
				y => Err(y.0)
			}
		}
		#[deprecated(since="1.0.2", note="Use Type::name() instead")]
		pub fn rrtype_name(x: Type) -> Result<&'static str, u16>
		{
			match x {
				#([types] #type_sym => Ok(#name),)*
				y => Err(y.0)
			}
		}
	});

	println!("cargo:rerun-if-changed=dnstypes.dat");
	println!("cargo:rerun-if-changed=dnsclasses.dat");
}
