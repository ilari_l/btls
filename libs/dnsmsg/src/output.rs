use crate::Dns0x20;
use crate::Extendable;
use crate::NameCompressContext;
use crate::consts::Class;
use crate::consts::DNSTYPE_OPT;
use crate::consts::Type;
use crate::message::Header;
use crate::name::Name;
use crate::tsig::tcp_fixup_length;
use crate::tsig::TsigSigner;
use crate::tsig::tsig_sign;
use crate::tsig::tsig_refuse_sign;

///Output query.
pub struct OutputQuery<'a>
{
	pub qname: &'a Name,
	pub dns0x20: Dns0x20,
	pub qtype: Type,
	pub qclass: Class,
}

///Output response.
pub struct OutputResponse<'a>
{
	pub owner: &'a Name,
	pub rtype: Type,
	pub rclass: Class,
	pub ttl: u32,
	pub data: &'a [u8],
}

///Output TSIG information.
pub struct TsigInfo<'a>
{
	pub signer: &'a dyn TsigSigner,
	pub timestamp: u64,
	pub fudge: u16,
	pub error: u16,
	pub other: &'a [u8],
	pub chain_mac: Option<(&'a [u8], bool)>,
	pub refuse_sign: bool,
}

///Output message.
pub trait OutputMessage
{
	///The header for the message.
	///
	///Note that *count fields are ignored.
	fn get_header(&self) -> Header;
	///The question for the message.
	fn get_question<'a>(&'a self, index: usize) -> Option<OutputQuery<'a>>;
	///The answers for the message.
	fn get_answer<'a>(&'a self, index: usize) -> Option<OutputResponse<'a>>;
	///The authority for the message.
	fn get_authority<'a>(&'a self, index: usize) -> Option<OutputResponse<'a>>;
	///The additional section for the message.
	fn get_additional<'a>(&'a self, index: usize) -> Option<OutputResponse<'a>>;
	///The maximum UDP packet size.
	fn get_udp_max(&self) -> u16;
	///The EDNS flags for the message.
	fn get_edns_flags(&self) -> u16;
	///The EDNS TLVs for the message.
	fn get_edns_tlvs<'a>(&'a self) -> &'a [u8];
}

fn __emit_query<E:Extendable>(q: OutputQuery, buf: &mut E, compress: &mut impl NameCompressContext,
	offset: usize) -> Result<(), E::Error>
{
	q.qname.to_wire_compressed(buf, &q.dns0x20, compress, offset)?;
	q.qtype.to_wire(buf)?;
	q.qclass.to_wire(buf)?;
	Ok(())
}

fn __emit_record<E:Extendable>(q: OutputResponse, buf: &mut E, compress: &mut impl NameCompressContext,
	offset: usize) -> Result<(), E::Error>
{
	//This handles compressing the part of name in common with QNAME.
	q.owner.to_wire_compressed(buf, &Dns0x20::new(), compress, offset)?;
	q.rtype.to_wire(buf)?;
	q.rclass.to_wire(buf)?;
	buf.extend(&q.ttl.to_be_bytes())?;
	buf.extend(&(q.data.len() as u16).to_be_bytes())?;
	buf.extend(q.data)?;
	Ok(())
}

fn fixup16(buf: &mut impl Extendable, ptr: usize, count: usize)
{
	buf.write(ptr, (count >> 8) as u8);
	buf.write(ptr+1, count as u8);
}

fn __do_iterate<T,E:Extendable>(buf: &mut E, offset: usize, get: impl Fn(usize) -> Option<T>,
	mut emit: impl FnMut(&mut E, T) -> Result<(), E::Error>) -> Result<usize, E::Error>
{
	let mut count = 0;
	while let Some(x) = get(count) {
		emit(buf, x)?;
		count += 1;
	}
	fixup16(buf, offset, count);
	Ok(count)
}

///Output a DNS message.
pub fn dns_output_message<E:Extendable>(message: &impl OutputMessage, tcp: bool, buf: &mut E,
	compress: &mut impl NameCompressContext, signer: Option<TsigInfo>) -> Result<(), E::Error>
{
	//TCP has two-byte length field.
	let offset = if tcp { buf.extend(&[0;2])?; 2 } else { 0 };
	let header = message.get_header();
	let flagsw = (header.qr as u16) << 15 | (header.opcode as u16 & 0xF) << 11 |
		(header.aa as u16) << 10 | (header.tc as u16) << 9 | (header.rd as u16) << 8 | 
		(header.ra as u16) << 7 | (header.z as u16 & 0x7) << 4 | (header.rcode & 0xF);
	buf.extend(&header.id.to_be_bytes())?;
	buf.extend(&flagsw.to_be_bytes())?;
	buf.extend(&[0;8])?;	//*count fields to be fixed up.

	__do_iterate(buf, offset+4, |i|message.get_question(i), |buf,x|__emit_query(x, buf, compress, offset))?;
	__do_iterate(buf, offset+6, |i|message.get_answer(i), |buf,x|__emit_record(x, buf, compress, offset))?;
	__do_iterate(buf, offset+8, |i|message.get_authority(i), |buf,x|__emit_record(x, buf, compress, offset))?;
	let additional = __do_iterate(buf, offset+10, |i|message.get_additional(i), |buf,x|{
		__emit_record(x, buf, compress, offset)
	})?;

	//Write EDNS.
	buf.push(0)?;
	DNSTYPE_OPT.to_wire(buf)?;
	Class(message.get_udp_max()).to_wire(buf)?;
	let ednsw = (header.rcode as u32 & 0xFF0) << 20 | (message.get_edns_flags() as u32);
	buf.extend(&ednsw.to_be_bytes())?;
	let tlvs = message.get_edns_tlvs();
	buf.extend(&(tlvs.len() as u16).to_be_bytes())?;
	buf.extend(tlvs)?;
	fixup16(buf, offset+10, additional+1);

	if let Some(signer) = signer {
		if signer.refuse_sign {
			let keyid = signer.signer.get_keyid();
			let algorithm = signer.signer.get_algorithm();
			tsig_refuse_sign(buf, tcp, keyid, algorithm, signer.timestamp, signer.fudge, signer.error,
				signer.other)?;
		} else {
			tsig_sign(buf, tcp, signer.signer, signer.timestamp, signer.fudge, signer.error,
				signer.other, signer.chain_mac)?;
		}
	}

	if tcp { tcp_fixup_length(buf); }

	Ok(())
}
