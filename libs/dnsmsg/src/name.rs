use super::Class;
use super::Dns0x20;
use super::Extendable;
use btls_aux_collections::ToOwned;
use btls_aux_fail::f_break;
use btls_aux_fail::f_return;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_memory::copy_maximum;
use btls_aux_memory::nth_bit;
use btls_aux_memory::slice_strip_first;
use btls_aux_memory::split_at;
use core::borrow::Borrow;
use core::cmp::min;
use core::cmp::Ordering;
use core::fmt::Debug;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::mem::replace;
use core::ops::Deref;

#[derive(Clone)]
struct NameLabelIter<'a>(&'a [u8], usize);

impl<'a> Iterator for NameLabelIter<'a>
{
	type Item = &'a [u8];
	fn next(&mut self) -> Option<&'a [u8]>
	{
		//Normal labels are 1-byte length (1 to 63) followed by that many bytes of actual label.
		let llen = self.0.get(self.1).map(|x|*x)? as usize;
		let nptr = self.1+llen+1;
		let slice = self.0.get(self.1+1..nptr)?;
		self.1 = nptr;
		Some(slice)
	}
}

#[derive(Clone)]
struct NameLabelIterS<'a>(&'a Name);

impl<'a> Iterator for NameLabelIterS<'a>
{
	type Item = (&'a [u8], &'a Name);
	fn next(&mut self) -> Option<(&'a [u8], &'a Name)>
	{
		//Normal labels are 1-byte length (1 to 63) followed by that many bytes of actual label.
		let name = self.0;
		let llen = name.0.get(0).map(|x|*x)? as usize;
		let nptr = llen+1;
		let slice = name.0.get(1..nptr)?;
		let name = Name::from_raw(name.0.get(nptr..)?);
		self.0 = name;
		Some((slice, name))
	}
}



fn lowercase(i: u8) -> u8 { if i.wrapping_sub(65) < 26 { i + 32  } else { i } }
fn is_digit(x: u8) -> bool { x >= 48 && x <= 57 }
fn push_master(x: &mut NameBuf, y: &mut usize, v: u8) -> Result<(), MasterNameError>
{
	fail_if!(*y >= x.0.len(), MasterNameError::TooLong);
	x.0[*y] = v;
	*y += 1;
	Ok(())
}

fn alter_master(x: &mut NameBuf, y: usize, v: u8) -> Result<(), MasterNameError>
{
	fail_if!(y >= x.0.len(), MasterNameError::TooLong);
	x.0[y] = v;
	Ok(())
}

fn print_label2(label: &[u8], f: &mut Formatter, dns0x20: Option<&Dns0x20>, mut index: usize) ->
	Result<usize, FmtError>
{
	let dns0x20 = dns0x20.map(|x|&x.0).unwrap_or(&[0;32]);
	//Increment index by 1 in order to skip the label type byte which is included in index sequence.
	index += 1;
	for &b in label.iter() {
		let c = b as char;
		let c = if nth_bit(dns0x20, index) { c.to_ascii_uppercase() } else { c };
		match b {
			46 => write!(f, "\\.")?,
			92 => write!(f, "\\\\")?,
			33..=126 => write!(f, "{c}")?,
			//Escape all others.
			_ => write!(f, "\\{b:03}")?
		}
		index += 1;
	}
	Ok(index)
}

pub(crate) fn print_name2<'a>(labels: impl Iterator<Item=&'a [u8]>, f: &mut Formatter, dns0x20: Option<&Dns0x20>) ->
	Result<(), FmtError>
{
	let mut subsequent = false;
	let mut index = 0;
	for label in labels {
		if label.len() == 0 { break; }
		if replace(&mut subsequent, true) { write!(f, ".")?; }
		index = print_label2(label, f, dns0x20, index)?;
	}
	Ok(())
}


///Error in master name.
#[derive(Copy,Clone,Debug)]
#[non_exhaustive]
pub enum MasterNameError
{
	///Name too long.
	TooLong,
	///Label too long.
	LabelTooLong,
	///Empty label.
	EmptyLabel,
	///Illegal escape sequence.
	IllegalEscape,
	///Dangling escape sequence.
	DanglingEscape,
}

impl Display for MasterNameError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::MasterNameError::*;
		match self {
			&TooLong => f.write_str("Name too long"),
			&LabelTooLong => f.write_str("Label too long"),
			&EmptyLabel => f.write_str("Empty label"),
			&IllegalEscape => f.write_str("Illegal escape sequence"),
			&DanglingEscape => f.write_str("Dangling escape sequence"),
		}
	}
}

///Print CLASS and name.
#[derive(Copy,Clone)]
pub struct ClassName<'a>(pub Class, pub &'a Name);

impl<'a> Display for ClassName<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		Display::fmt(&self.0, f)?;
		f.write_str(":")?;
		print_name2(self.1.iter(), f, None)
	}
}

///Print CLASS and name with given DNS 0x20.
#[derive(Copy,Clone)]
pub struct ClassName0x20<'a>(pub Class, pub &'a Name, pub &'a Dns0x20);

impl<'a> Display for ClassName0x20<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		Display::fmt(&self.0, f)?;
		f.write_str(":")?;
		print_name2(self.1.iter(), f, Some(self.2))
	}
}

///Trait for name compression context.
pub trait NameCompressContext
{
	///If there is no pointer associated with Name, store pointer if less than 16384 and return None. If there
	///is associated pointer with name, return that pointer.
	///
	///Always returning None gives correct result with compression disabled.
	fn exchange_ptr(&mut self, name: &Name, pointer: u16) -> Option<u16>;
}

fn try_compress<E:Extendable>(buf: &mut E, name: &Name, compress: &mut impl NameCompressContext, offset: usize) ->
	Result<bool, E::Error>
{
	if name.len() <= 1 { return Ok(false); }	//Do not try compressing root.
	let ptr = min(buf.len().saturating_sub(offset), 65535) as u16;
	//Check that compression pointer is legal before using. The compression pointers passed to
	//exchange_ptr can be illegal, and it might not check before storing those.
	if let Some(ptr) = compress.exchange_ptr(name, ptr) { if ptr < 16384 {
		buf.push(192 | (ptr >> 8) as u8)?;
		buf.push(ptr as u8)?;
		return Ok(true);
	}}
	Ok(false)
}

///Borrowed DNS name.
#[derive(Debug)]
pub struct Name([u8]);
///Owned DNS name.
#[derive(Clone)]
pub struct NameBuf([u8;256]);

impl Name
{
	//This is pub(crate) for tests.
	pub(crate) fn from_raw<'a>(x: &'a [u8]) -> &'a Name { unsafe{core::mem::transmute(x)} }
	fn count_labels(&self) -> usize
	{
		let x = &self.0;
		let mut c = 0;
		let mut i = 0;
		while i < x.len() {
			let l = x[i] as usize;
			i += l + 1;
			c += 1;
		}
		c
	}
	fn kill_labels(&self, n: usize) -> &Name
	{
		let x = &self.0;
		let mut i = 0;
		for _ in 0..n {
			let l = *f_break!(x.get(i)) as usize;
			i += l + 1;
		}
		let x = x.get(i..).unwrap_or(b"");
		Name::from_raw(x)
	}
	//Equals can just be raw comparision, since Name is assumed to be all lowercase.
	fn equals(&self, other: &Self) -> bool { &self.0 == &other.0 }
	fn __order_equal_depth(a: &Self, b: &Self) -> Ordering
	{
		let (la, pa) = a.__parent_and_label();
		let (lb, pb) = b.__parent_and_label();
		if pa.0.len() > 0 {
			//Compare parents and tiebreak by this label. Labels should be all-lowercase, so one
			//can use raw lexicographic compare.
			Self::__order_equal_depth(pa, pb).then_with(||la.cmp(lb))
		} else {
			//This should only happen if a==b==. So the names should be equal.
			Ordering::Equal
		}
	}
	fn order(&self, other: &Self) -> Ordering
	{
		let mut a = self;
		let mut b = other;
		//Order is not standard lexicographic ordering of slices, since labels are stored in least
		//significant to most significant order. Start by counting depths.
		let depth_a = a.count_labels();
		let depth_b = b.count_labels();
		//Remove first labels, so number of labels is equal.
		let tiebreak = if depth_b < depth_a {
			a = a.kill_labels(depth_a-depth_b);
			Ordering::Greater
		} else if depth_a < depth_b {
			b = b.kill_labels(depth_b-depth_a);
			Ordering::Less
		} else {
			Ordering::Equal
		};
		Self::__order_equal_depth(a, b).then(tiebreak)
	}
	fn __parent_and_label<'a>(&'a self) -> (&'a [u8], &Name)
	{
		let empty: &'static [u8] = b"";
		//This should not be empty, but in case it is...
		let llen = *f_return!(self.0.get(0), (b"", self));
		//The +1 is for label length byte.
		match split_at(&self.0, llen as usize + 1) {
			//Strip the label length byte.
			Ok((head, tail)) => (slice_strip_first(head), Name::from_raw(tail)),
			//This is not supposed to happen. Return something undefined.
			Err(_) => (&self.0, Name::from_raw(empty))
		}
	}
	fn __parent(&self) -> &Name { self.__parent_and_label().1 }
	///Is root?
	pub fn is_root(&self) -> bool { self.0.len() <= 1 }
	///Get parent of the name, or None if name has no parent (i.e., is root).
	pub fn parent(&self) -> Option<&Name>
	{
		//Empty parent is parent of root, and thus invalid.
		let x = self.__parent();
		if x.0.len() > 0 { Some(x) } else { None }
	}
	///Get wire length of the name.
	pub fn len(&self) -> usize { self.0.len() }
	///Iterate over labels in name.
	pub fn iter<'a>(&'a self) -> impl Iterator<Item=&'a [u8]>+Clone { NameLabelIter(&self.0, 0) }
	fn iter_split<'a>(&'a self) -> NameLabelIterS { NameLabelIterS(self) }
	///Write name in master form. At worst, this can take 503 bytes.
	pub fn to_master<E:Extendable>(&self, buf: &mut E) -> Result<(), E::Error>
	{
		Self::__to_master(self.iter(), buf)
	}
	pub(crate) fn __to_master<'a,E:Extendable>(name: impl Iterator<Item=&'a [u8]>+Clone, buf: &mut E) ->
		Result<(), E::Error>
	{
		let mut hint = 0;
		for label in name.clone() {
			hint += 1 + label.len();
			for &b in label.iter() { if b == b'.' || b == b'\\' { hint += 1; } }
		}
		hint = hint.saturating_sub(1);
		buf.alloc_hint(hint);
		let mut subsequent = false;
		for label in name {
			if replace(&mut subsequent, true) && label.len() > 0 { buf.push(b'.')?; }
			for &b in label.iter() {
				if b == b'.' { buf.extend(b"\\.")?; }
				else if b == b'\\' { buf.extend(b"\\\\")?; }
				else { buf.push(b)?; }
			}
		}
		Ok(())
	}
	///Write in wire form with specified Dns0x20 pattern.
	pub fn to_wire<E:Extendable>(&self, buf: &mut E, dns0x20: &Dns0x20) -> Result<(), E::Error>
	{
		let mut i = 0;
		for label in self.iter() {
			buf.push(label.len() as u8)?;
			i+=1;	//Skip label length byte.
			for &b in label.iter() {
				let b = if b.is_ascii_lowercase() && nth_bit(&dns0x20.0, i) { b - 32 } else { b };
				buf.push(b)?;
				i+=1;
			}
		}
		Ok(())
	}
	///Write in wire form with specified Dns0x20 pattern.
	pub(crate) fn to_wire_compressed<E:Extendable>(&self, buf: &mut E, dns0x20: &Dns0x20,
		compress: &mut impl NameCompressContext, offset: usize) -> Result<(), E::Error>
	{
		let mut i = 0;
		//If try_compress succeeds compressing the entiere name, we are done.
		if try_compress(buf, self, compress, offset)? { return Ok(()); }
		for (label,rest) in self.iter_split() {
			buf.push(label.len() as u8)?;
			i+=1;	//Skip label length byte.
			for &b in label.iter() {
				let b = if b.is_ascii_lowercase() && nth_bit(&dns0x20.0, i) { b - 32 } else { b };
				buf.push(b)?;
				i+=1;
			}
			//Try compressing the rest, if this succeeds, we are done.
			if try_compress(buf, rest, compress, offset)? { return Ok(()); }
		}
		Ok(())
	}
	///Equals strictly with wire labels with specified Dns0x20 pattern.
	pub(crate) fn equals_strict<'a>(&self, mut b: impl Iterator<Item=&'a [u8]>, dns0x20: &Dns0x20) -> bool
	{
		let mut a = self.iter();
		let mut i = 0;
		loop { match (a.next(), b.next()) {
			(Some(a), Some(b)) => {
				if a.len() != b.len() { return false; }
				i += 1;		//Skip label length byte.
				for (&a, &b) in a.iter().zip(b.iter()) {
					let is_alpha = a.is_ascii_lowercase();
					let uppercase = nth_bit(&dns0x20.0, i);
					let o = if is_alpha && uppercase { 32 } else { 0 };
					if a - o != b { return false; }
					i += 1;
				}
			},
			(None, None) => return true,	//End.
			_ => return false,		//One ends, another does not.
		}}
	}
	///Equals ignoring case.
	pub(crate) fn equals_ignore_case<'a>(&self, mut b: impl Iterator<Item=&'a [u8]>) -> bool
	{
		let mut a = self.iter();
		loop { match (a.next(), b.next()) {
			(Some(a), Some(b)) => {
				if a.len() != b.len() { return false; }
				for (&a, &b) in a.iter().zip(b.iter()) {
					let uppercase = b.is_ascii_uppercase();
					let o = if uppercase { 32 } else { 0 };
					if a != b + o { return false; }
				}
			},
			(None, None) => return true,	//End.
			_ => return false,		//One ends, another does not.
		}}
	}
	///Get name as raw wire-format bytes.
	pub fn as_raw(&self) -> &[u8] { &self.0 }
	///Get the name corresponding to TSIG with HMAC-SHA256.
	pub fn tsig_hmac_sha256() -> &'static Name
	{
		static P: &'static [u8] = b"\x0bhmac-sha256\x00";
		Name::from_raw(P)
	}
	///Get the name corresponding to DNS root.
	pub fn root() -> &'static Name
	{
		static P: &'static [u8] = b"\x00";
		Name::from_raw(P)
	}
	///To owned name.
	pub fn to_name_buf(&self) -> NameBuf
	{
		let mut out = NameBuf([0;256]);
		//self.0.len() should never be more than 255, but in case it is, just truncate.
		let len = copy_maximum(&mut out.0[1..], &self.0);
		out.0[0] = len as u8;
		out
	}
	///Is given name in the given zone?
	pub fn in_zone_of(&self, zone: &Name) -> bool
	{
		//Due to how addresses work, name is in zone of another iff it ends with the zone name.
		//This does handle cases like x.yz.w is not in z.w correctly.
		self.0.ends_with(&zone.0)
	}
}

impl Display for Name
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		print_name2(self.iter(), f, None)
	}
}

impl Display for NameBuf
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> { Display::fmt(self.deref(), f) }
}

impl Debug for NameBuf
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> { Debug::fmt(self.deref(), f) }
}

impl Borrow<Name> for NameBuf
{
	fn borrow(&self) -> &Name { self.deref() }
}

impl Deref for NameBuf
{
	type Target = Name;
	fn deref(&self) -> &Name
	{
		//First byte of NameBuf is length.
		let l = self.0[0] as usize;
		//1+l <= 1+255=256 = self.0.len(), so this is always in-bounds.
		let p: &[u8] = &self.0[1..1+l];
		Name::from_raw(p)
	}
}

impl ToOwned for Name
{
	type Owned = NameBuf;
	fn to_owned(&self) -> NameBuf { self.to_name_buf() }
}

impl NameBuf
{
	///Construct name from DNS master file name.
	pub fn from_master(name: &[u8]) -> Result<NameBuf, MasterNameError>
	{
		use self::MasterNameError::*;
		let mut out = NameBuf([0;256]);
		let mut oitr = 1;
		//Label.
		let mut lstart = 1;
		let mut escape = 0u8;
		push_master(&mut out, &mut oitr, 0)?;		//Placeholder for label length.
		for &i in name.iter() {
			if escape > 0 {
				if is_digit(i) {
					if escape == 1 {
						escape = i - 46;	//[2,4].
						fail_if!(escape > 4, IllegalEscape);
					} else if escape <= 4 {
						escape = 10 * escape + i - 63;	//[5,30].
						fail_if!(escape > 30, IllegalEscape);
					} else if escape < 30 || escape == 30 && i <= 53 {
						escape = escape.wrapping_mul(10).wrapping_add(i+158);
						push_master(&mut out, &mut oitr, escape)?;
						escape = 0;
					} else {
						fail!(IllegalEscape);
					}
				} else {
					fail_if!(escape > 1, IllegalEscape);
					//Push any character.
					push_master(&mut out, &mut oitr, lowercase(i))?;
					escape = 0;
				}
			} else if i == b'\\' {
				escape = 1;
			} else if i == b'.' {
				//End of label, push it.
				let llen = oitr.saturating_sub(lstart + 1);
				fail_if!(llen == 0, EmptyLabel);
				fail_if!(llen > 63, LabelTooLong);
				alter_master(&mut out, lstart, llen as u8)?;
				lstart = oitr;
				push_master(&mut out, &mut oitr, 0)?;	//Space for label length.
			} else {
				//Push it.
				push_master(&mut out, &mut oitr, lowercase(i))?;
			}
		}
		fail_if!(escape > 0, DanglingEscape);
		//Fixup length of last label. llen == 0 is allowed here. It happens if the name has trailing
		//dot to denote root zone. We allow such trailing dots.
		let llen = oitr.saturating_sub(lstart + 1);
		fail_if!(llen > 63, LabelTooLong);
		alter_master(&mut out, lstart, llen as u8)?;
		//If the last label was not root, push root.
		if llen > 0 { push_master(&mut out, &mut oitr, 0)?; }
		out.0[0] = (oitr - 1) as u8;	//Always at most 255.
		Ok(out)
	}
	///Construct name corresponding to the root.
	pub fn root() -> NameBuf
	{
		let mut out = NameBuf([0;256]);
		out.0[0] = 1;
		out
	}
	pub(crate) fn from_labels<'a>(name: impl Iterator<Item=&'a [u8]>) -> NameBuf
	{
		let mut out = NameBuf([0;256]);
		let mut oitr = 1;
		for label in name {
			//Just ignore failures from push_master, as the name should be short enough.
			push_master(&mut out, &mut oitr, label.len() as u8).ok();
			for &b in label.iter() {
				let v = if b.wrapping_sub(65) < 26 { b + 32 } else { b };
				push_master(&mut out, &mut oitr, v).ok();
			}
		}
		out.0[0] = (oitr - 1) as u8;	//Always at most 255.
		out
	}
}

macro_rules! partial_eq_and_ord
{
	($ty1:ident, $ty2:ident) => {
		impl PartialOrd<$ty2> for $ty1
		{
			fn partial_cmp(&self, rhs: &$ty2) -> Option<Ordering> { Some(self.order(rhs)) }
		}
		impl PartialEq<$ty2> for $ty1
		{
			fn eq(&self, rhs: &$ty2) -> bool { self.equals(rhs) }
		}
	}
}

macro_rules! eq_and_ord
{
	($ty1:ident) => {
		impl Ord for $ty1
		{
			fn cmp(&self, rhs: &$ty1) -> Ordering { self.order(rhs) }
		}
		impl Eq for $ty1 {}
	}
}

partial_eq_and_ord!(Name, Name);
partial_eq_and_ord!(Name, NameBuf);
partial_eq_and_ord!(NameBuf, Name);
partial_eq_and_ord!(NameBuf, NameBuf);
eq_and_ord!(Name);
eq_and_ord!(NameBuf);

#[cfg(test)]
mod tests {
use super::Name;
use super::NameBuf;
use crate::Extendable;
use btls_aux_memory::SmallVec;
use btls_aux_collections::ToString;

#[test]
fn compare_dns_names()
{
	assert!(Name::from_raw(b"\x01a\x00") < Name::from_raw(b"\x01b\x00"));
	assert!(Name::from_raw(b"\x01a\x00") < Name::from_raw(b"\x01c\x00"));
	assert!(Name::from_raw(b"\x019\x00") < Name::from_raw(b"\x01a\x00"));
	assert!(Name::from_raw(b"\x01a\x00") < Name::from_raw(b"\x02aa\x00"));
	assert!(Name::from_raw(b"\x03aaa\x01a\x00") < Name::from_raw(b"\x02aa\x01b\x00"));
	assert!(Name::from_raw(b"\x01y\x00") < Name::from_raw(b"\x01x\x01y\x00"));
	assert!(Name::from_raw(b"\x01j\x00") < Name::from_raw(b"\x01a\x01u\x00"));
}

#[test]
fn to_master()
{
	let mut out = SmallVec::<u8,512>::new();
	Name::from_raw(b"\x01x\x03\\.\x80\x03foo\x07example\x03org\x00").to_master(&mut out).unwrap();
	assert_eq!(out.slice_from_index(0), b"x.\\\\\\.\x80.foo.example.org");
}

#[test]
fn print()
{
	let n = Name::from_raw(b"\x01x\x04\\. \x7f\x03foo\x07example\x03org\x00");
	let m = n.to_string();
	assert_eq!(m, "x.\\\\\\.\\032\\127.foo.example.org");
	let n2 = NameBuf::from_master(m.as_bytes()).unwrap();
	assert!(n.equals(&n2));
}

}
