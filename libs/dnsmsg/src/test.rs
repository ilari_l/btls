use crate::Dns0x20;
use crate::Extendable;
use crate::Header;
use crate::Message;
use crate::Name;
use crate::NameBuf;
use crate::NameCompressContext;
use crate::OutputMessage;
use crate::OutputQuery;
use crate::OutputResponse;
use crate::Rdata;
use crate::tsig_sign;
//use crate::TsigInfo;
use crate::TsigSigner;
use crate::consts::DNSCLASS_IN;
use crate::consts::DNSCLASS_NONE;
use crate::consts::DNSTYPE_NULL;
use crate::consts::DNSTYPE_SOA;
use crate::consts::DNSTYPE_TXT;
use btls_aux_memory::SmallVec;
use std::collections::BTreeMap;
use std::ops::Deref;
use std::vec::Vec;

struct TestTsigSigner;
impl TsigSigner for TestTsigSigner
{
	fn get_keyid<'a>(&'a self) -> &'a Name
	{
		static P: &'static [u8] = b"\x17some-authentication-key\x07example\x00";
		Name::from_raw(P)
	}
	fn get_algorithm<'a>(&'a self) -> &'a Name { Name::tsig_hmac_sha256() }
	fn get_max_mac_length(&self) -> u16 { 32 }
	fn mac_multi(&self, buf: &mut [u8], data: &[&[u8]])
	{
		use btls_aux_hash::Hash;
		use btls_aux_hash::Sha256;
		let mac = Sha256::function().hmac_v(b"secret key material", data);
		let mlen = core::cmp::min(buf.len(), mac.len());
		(&mut buf[..mlen]).copy_from_slice(&mac[..mlen]);
	}
}


#[test]
fn test_tsig_sign()
{
	let omsg = include_bytes!("test-tsig.dnsmsg");
	let mut msg = SmallVec::<u8,1024>::new();
	msg.extend(&omsg[..257]).expect("Should have space for this");
	msg.write(11,msg.read(11)-1);		//Remove TSIG.
	tsig_sign(&mut msg, false, &TestTsigSigner, 5_000_000_000, 60, 0, b"", None).
		expect("Should have enough space"); 
	assert_eq!(msg.slice_from_index(0), &omsg[..]);
}

#[test]
fn test_with_tsig()
{
	use btls_aux_hash::Hash;
	use btls_aux_hash::Sha256;
	let msg = include_bytes!("test-tsig.dnsmsg");
	let mut msg = Message::decode(msg, |_,_,_,_,_,_|()).expect("Should have decoded");
	let header = msg.header();
	assert_eq!(header.id, 12345);		//TXID should be 12345.
	assert!(!header.qr);			//Query.
	assert_eq!(header.opcode, 5);		//UPDATE.
	assert!(!header.aa);
	assert!(!header.tc);
	assert!(!header.rd);
	assert!(!header.ra);
	assert_eq!(header.z, 0);
	assert_eq!(header.rcode, 0);
	assert_eq!(header.qdcount, 1);
	assert_eq!(header.ancount, 0);
	assert_eq!(header.nscount, 3);
	assert_eq!(header.arcount, 2);		//OPT+TSIG.

	let query: Vec<_> = msg.question().collect();
	let answer: Vec<_> = msg.answer().collect();
	let authority: Vec<_> = msg.authority().collect();
	let additional: Vec<_> = msg.additional().collect();
	assert_eq!(query.len(), 1);
	assert_eq!(answer.len(), 0);
	assert_eq!(authority.len(), 3);
	assert_eq!(additional.len(), 0);		//OPT and TSIG should have been removed.
	let zone = &query[0];
	let zonen = NameBuf::from_master(b"zone.example.org").unwrap();
	assert!(zone.qname == zonen.deref());
	assert_eq!(zone.qclass, DNSCLASS_IN);
	assert_eq!(zone.qtype, DNSTYPE_SOA);
	let ename = NameBuf::from_master(b"a-example-com.zone.example.org").unwrap();
	assert!(authority[0].owner == ename.deref());
	assert_eq!(authority[0].clazz, DNSCLASS_IN);
	assert_eq!(authority[0].rtype, DNSTYPE_TXT);
	assert_eq!(authority[0].ttl, 1);
	assert_eq!(authority[0].data.raw_data(), Some(&b"\x1bAdded example TXT record #1"[..]));
	assert!(authority[1].owner == ename.deref());
	assert_eq!(authority[1].clazz, DNSCLASS_IN);
	assert_eq!(authority[1].rtype, DNSTYPE_TXT);
	assert_eq!(authority[1].ttl, 1);
	assert_eq!(authority[1].data.raw_data(), Some(&b"\x1bAdded example TXT record #2"[..]));
	assert!(authority[2].owner == ename.deref());
	assert_eq!(authority[2].clazz, DNSCLASS_NONE);
	assert_eq!(authority[2].rtype, DNSTYPE_TXT);
	assert_eq!(authority[2].ttl, 0);
	assert_eq!(authority[2].data.raw_data(), Some(&b"\x1dRemoved example TXT record #3"[..]));
	let edns = msg.edns().expect("Expected EDNS");
	assert_eq!(edns.version, 0);
	assert_eq!(edns.flags, 0);
	assert_eq!(edns.udpmax, 1200);
	assert_eq!(edns.tlvs, &[]);
	let tsig = msg.tsig().expect("Expected TSIG");
	let keyid = NameBuf::from_master(b"some-authentication-key.example").unwrap();
	let algorithm = Name::tsig_hmac_sha256();
	assert!(tsig.keyid == keyid.deref());
	assert!(tsig.algorithm == algorithm);
	assert_eq!(tsig.body_len, 257);
	assert_eq!(tsig.time.epoch, 1);
	assert_eq!(tsig.time.time, 705_032_704);
	assert_eq!(tsig.time.fudge, 60);
	assert_eq!(tsig.original_id, 12345);
	assert_eq!(tsig.error, 0);
	assert_eq!(tsig.other, &[]);
	let mut tbs = SmallVec::<u8,1024>::new();
	msg.mangle_msgid();		//Mangle header to check this uses orig_txid.
	let has_tsig = msg.append_tsig_tbs(&mut tbs, false).expect("Should have space for this");
	assert!(has_tsig);
	let mac = Sha256::hmac(b"secret key material", tbs.slice_from_index(0));
	assert_eq!(tsig.mac, mac);
}

#[test]
fn test_from_master_simple()
{
	let name = NameBuf::from_master(b"hello.worldx").expect("Name should be valid");
	assert_eq!(name.as_raw(), b"\x05hello\x06worldx\x00");
	let name = NameBuf::from_master(b"hello.worldx.").expect("Name should be valid");
	assert_eq!(name.as_raw(), b"\x05hello\x06worldx\x00");
}

#[test]
fn test_from_master_escapes()
{
	let name = NameBuf::from_master(b"hello.world\\.").expect("Name should be valid");
	assert_eq!(name.as_raw(), b"\x05hello\x06world.\x00");
	let name = NameBuf::from_master(b"hello.world\\\\").expect("Name should be valid");
	assert_eq!(name.as_raw(), b"\x05hello\x06world\\\x00");
}

#[test]
fn test_from_master_nescapes()
{
	for i in 0..256usize {
		let n = format!("hello.world\\{i:03}");
		let name = NameBuf::from_master(n.as_bytes()).expect("Name should be valid");
		let mut r = b"\x05hello\x06world".to_vec();
		r.push(i as u8);
		r.push(0);
		assert_eq!(name.as_raw(), &r[..]);
	}
}

#[test]
fn test_from_master_bad_nescapes()
{
	for i in 256..1000usize {
		let n = format!("hello.world\\{i:03}");
		assert!(NameBuf::from_master(n.as_bytes()).is_err());
	}
	for i in 0..100usize {
		let n = format!("hello.world\\{i:02}");
		assert!(NameBuf::from_master(n.as_bytes()).is_err());
	}
	for i in 0..10usize {
		let n = format!("hello.world\\{i}");
		assert!(NameBuf::from_master(n.as_bytes()).is_err());
	}
	assert!(NameBuf::from_master(b"hello.world\\").is_err());
}

impl NameCompressContext for BTreeMap<NameBuf, u16>
{
	fn exchange_ptr(&mut self, name: &Name, pointer: u16) -> Option<u16>
	{
		if let Some(ptr) = self.get(name) { return Some(*ptr); }
		self.insert(name.to_name_buf(), pointer);
		None
	}
}

struct TestEncode1;

impl OutputMessage for TestEncode1
{
	fn get_header(&self) -> Header
	{
		Header {
			id: 12345,
			qr: true,
			opcode: 0,
			aa: false,
			tc: false,
			rd: true,
			ra: true,
			z: 0,
			rcode: 1533,
			qdcount: 9999,
			ancount: 9999,
			nscount: 9999,
			arcount: 9999
		}
	}
	fn get_question<'a>(&'a self, index: usize) -> Option<OutputQuery<'a>>
	{
		if index == 0 {
			Some(OutputQuery {
				qname: Name::from_raw(b"\x03foo\x07example\x03org\x00"),
				qclass: DNSCLASS_IN,
				qtype: DNSTYPE_NULL,
				dns0x20: Dns0x20::new(),
			})
		} else {
			None
		}
	}
	fn get_answer<'a>(&'a self, index: usize) -> Option<OutputResponse<'a>>
	{
		if index == 0 {
			Some(OutputResponse {
				owner: Name::from_raw(b"\x03bar\x03foo\x07example\x03org\x00"),
				rclass: DNSCLASS_IN,
				rtype: DNSTYPE_NULL,
				ttl: 1,
				data: &[],
			})
		} else {
			None
		}
	}
	fn get_authority<'a>(&'a self, index: usize) -> Option<OutputResponse<'a>>
	{
		if index == 0 {
			Some(OutputResponse {
				owner: Name::from_raw(b"\x07example\x03org\x00"),
				rclass: DNSCLASS_IN,
				rtype: DNSTYPE_SOA,
				ttl: 1000,
				data: &b"\x01x\x07invalid\x00\x01y\x07invalid\x00\0\0\0\x02\0\0\0\x03\0\0\0\x04\
					\0\0\0\x05\0\0\0\x06"[..],
			})
		} else {
			None
		}
	}
	fn get_additional<'a>(&'a self, _: usize) -> Option<OutputResponse<'a>> { None }
	fn get_udp_max(&self) -> u16 { 1200 }
	fn get_edns_flags(&self) -> u16 { 0 }
	fn get_edns_tlvs<'a>(&'a self) -> &'a [u8] { &[] }
}

#[test]
fn test_encode_decode_1()
{
	let mut msg = SmallVec::<u8,1024>::new();
	let mut nc: BTreeMap<NameBuf, u16> = BTreeMap::new();
	crate::output::dns_output_message(&TestEncode1, true, &mut msg, &mut nc, None).
		expect("Should have space for this");
	let wmsg = msg.slice_from_index(0);
	let msg = msg.slice_from_index(2);
	assert_eq!(wmsg[0], (msg.len() >> 8) as u8);
	assert_eq!(wmsg[1], msg.len() as u8);
	//The answer postfix should be compressed.
	assert_eq!(msg[37], 192);
	assert_eq!(msg[38], 12);
	assert_eq!(msg[49], 192);
	assert_eq!(msg[50], 16);

	//Decode the message.
	let msg = Message::decode(msg, |_,_,_,_,_,_|()).expect("Should have decoded");
	let header = msg.header();
	assert_eq!(header.id, 12345);		//TXID should be 12345.
	assert!(header.qr);
	assert_eq!(header.opcode, 0);
	assert!(!header.aa);
	assert!(!header.tc);
	assert!(header.rd);
	assert!(header.ra);
	assert_eq!(header.z, 0);
	assert_eq!(header.rcode, 1533);
	assert_eq!(header.qdcount, 1);
	assert_eq!(header.ancount, 1);
	assert_eq!(header.nscount, 1);
	assert_eq!(header.arcount, 1);		//OPT

	let query: Vec<_> = msg.question().collect();
	let answer: Vec<_> = msg.answer().collect();
	let authority: Vec<_> = msg.authority().collect();
	let additional: Vec<_> = msg.additional().collect();
	assert_eq!(query.len(), 1);
	assert_eq!(answer.len(), 1);
	assert_eq!(authority.len(), 1);
	assert_eq!(additional.len(), 0);		//OPT should have been removed.
	let zone = &query[0];
	let zonen = NameBuf::from_master(b"foo.example.org").unwrap();
	assert!(zone.qname == zonen.deref());
	assert_eq!(zone.qclass, DNSCLASS_IN);
	assert_eq!(zone.qtype, DNSTYPE_NULL);
	let ename = NameBuf::from_master(b"bar.foo.example.org").unwrap();
	assert!(answer[0].owner == ename.deref());
	assert_eq!(answer[0].clazz, DNSCLASS_IN);
	assert_eq!(answer[0].rtype, DNSTYPE_NULL);
	assert_eq!(answer[0].ttl, 1);
	assert_eq!(answer[0].data.raw_data(), Some(&b""[..]));
	let ename = NameBuf::from_master(b"example.org").unwrap();
	assert!(authority[0].owner == ename.deref());
	assert_eq!(authority[0].clazz, DNSCLASS_IN);
	assert_eq!(authority[0].rtype, DNSTYPE_SOA);
	assert_eq!(authority[0].ttl, 1000);
	if let &Rdata::Soa{mname, rname, serial, refresh, retry, expire, minimum} = &authority[0].data {
		let ename = NameBuf::from_master(b"x.invalid").unwrap();
		assert!(mname == ename.deref());
		let ename = NameBuf::from_master(b"y.invalid").unwrap();
		assert!(rname == ename.deref());
		assert_eq!(serial, 2);
		assert_eq!(refresh, 3);
		assert_eq!(retry, 4);
		assert_eq!(expire, 5);
		assert_eq!(minimum, 6);
	} else {
		panic!("Not SOA");
	}
	let edns = msg.edns().expect("Expected EDNS");
	assert_eq!(edns.version, 0);
	assert_eq!(edns.flags, 0);
	assert_eq!(edns.udpmax, 1200);
	assert_eq!(edns.tlvs, &[]);
	if msg.tsig().is_some() { panic!("Did not expect TSIG"); }
}
