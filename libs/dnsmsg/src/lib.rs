#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]
#[cfg(test)] #[macro_use] extern crate std;

use btls_aux_fail::dtry;
use btls_aux_memory::BackedVector;
use btls_aux_memory::SmallVec;


pub mod consts;
pub use crate::consts::Class;
pub use crate::consts::Type;
use crate::consts::DNSTYPE_OPT;
mod message;
pub use message::Edns;
pub use message::Header;
pub use message::Message;
pub use message::MessageParseError;
pub use message::Query;
pub use message::Tsig;
mod name;
pub use crate::name::ClassName;
pub use crate::name::ClassName0x20;
pub use crate::name::NameCompressContext;
pub use crate::name::Name;
pub use crate::name::NameBuf;
pub use crate::name::MasterNameError;
mod nameptr;
pub use crate::nameptr::ClassNamePtr;
pub use crate::nameptr::NamePtr;
pub use crate::nameptr::NameDecompressionError;
mod output;
pub use crate::output::dns_output_message;
pub use crate::output::OutputMessage;
pub use crate::output::OutputQuery;
pub use crate::output::OutputResponse;
pub use crate::output::TsigInfo;
mod rdata;
pub use crate::rdata::MessageSection;
pub use crate::rdata::Rdata;
pub use crate::rdata::Record;
pub use crate::rdata::RecordDecodeError;
pub use crate::rdata::TsigTimestamp;
use crate::rdata::write_u16;
use crate::rdata::write_u32;
mod tsig;
pub use crate::tsig::tsig_sign;
pub use crate::tsig::TsigSigner;


///Data for Dns0x20 hack.
pub struct Dns0x20([u8;32]);

impl Dns0x20
{
	pub fn new() -> Dns0x20 { Dns0x20([0;32]) }
	pub fn with(&mut self, x: impl FnOnce(&mut [u8])) { x(&mut self.0) }
}

pub trait Extendable
{
	type Error: Sized;
	fn push(&mut self, b: u8) -> Result<(), <Self as Extendable>::Error>;
	fn extend(&mut self, b: &[u8]) -> Result<(), <Self as Extendable>::Error>;
	fn len(&self) -> usize;
	fn read(&self, idx: usize) -> u8;	//SHALL return 0 on out-of-range read.
	fn write(&mut self, idx: usize, v: u8);	//SHALL ignore out-of-range writes.
	fn slice_from_index<'a>(&'a self, idx: usize) -> &'a [u8];
	fn alloc_hint(&mut self, hint: usize);
}

macro_rules! impl_extandable_vec
{
	($name:ident [$($a:tt)*] [$($b:tt)*]) => {
		impl $($a)* Extendable for $name $($b)*
		{
			type Error = ();
			fn push(&mut self, b: u8) -> Result<(), ()>
			{
				dtry!($name::push(self, b));
				Ok(())
			}
			fn extend(&mut self, b: &[u8]) -> Result<(), ()>
			{
				dtry!($name::extend(self, b));
				Ok(())
			}
			fn len(&self) -> usize { $name::len(self) }
			fn read(&self, idx: usize) -> u8 { $name::get_value(self, idx).unwrap_or(0) }
			fn write(&mut self, idx: usize, v: u8) { $name::swap_element(self, idx, v); }
			fn slice_from_index<'b>(&'b self, idx: usize) -> &'b [u8]
			{
				$name::as_inner(self).get(idx..).unwrap_or(&[])
			}
			fn alloc_hint(&mut self, _: usize) {}
		}
	}
}

impl_extandable_vec!(BackedVector [<'a>] [<'a,u8>]);
impl_extandable_vec!(SmallVec [<const N: usize>] [<u8,N>]);

pub enum Infallible {}
pub use btls_aux_collections::Vec;

impl Extendable for Vec<u8>
{
	type Error = Infallible;
	fn push(&mut self, b: u8) -> Result<(), Infallible> { Ok(Vec::push(self, b)) }
	fn extend(&mut self, b: &[u8]) -> Result<(), Infallible> { Ok(Vec::extend_from_slice(self, b)) }
	fn len(&self) -> usize { Vec::len(self) }
	fn read(&self, idx: usize) -> u8 { self.get(idx).map(|x|*x).unwrap_or(0) }
	fn write(&mut self, idx: usize, v: u8) { self.get_mut(idx).map(|p|*p=v);  }
	fn slice_from_index<'b>(&'b self, idx: usize) -> &'b [u8] { self.get(idx..).unwrap_or(&[]) }
	fn alloc_hint(&mut self, hint: usize) { Vec::reserve(self, hint) }
}

///Format a DNS query into given buffer.
///
//The maximum possible length of query is 282 bytes.
pub fn dns_format_query<E:Extendable>(name: &Name, clazz: Class, rrtype: Type, txid: u16, dns0x20: &Dns0x20,
	tcp: bool, buf: &mut E) -> Result<(), E::Error>
{
	buf.alloc_hint(27 + name.len() + if tcp { 2 } else { 0 });
	if tcp { write_u16(buf, 0)?; }				//Dummy length (fixed up later).
	//Header.
	write_u16(buf, txid)?;					//ID.
	write_u16(buf, 256)?;					//Query QUERY, Recursion desired.
	write_u16(buf, 1)?;					//1 query.
	write_u16(buf, 0)?;					//No answers.
	write_u16(buf, 0)?;					//No authority.
	write_u16(buf, 1)?;					//1 additional.
	//Query.
	name.to_wire(buf, dns0x20)?;
	rrtype.to_wire(buf)?;
	clazz.to_wire(buf)?;
	//EDNS(0)
	buf.push(0)?;
	DNSTYPE_OPT.to_wire(buf)?;				//OPT.
	write_u16(buf, 4096)?;					//4kB UDP buffer.
	write_u32(buf, 0)?;					//Flags&Extended.
	write_u16(buf, 0)?;					//Empty TLV.
	//Fixup the TCP length if any.
	if tcp {
		let msglen = buf.len() - 2;	//Length of length not included.
		buf.write(0, (msglen >> 8) as u8);
		buf.write(1, msglen as u8);
	}
	Ok(())
}

#[cfg(test)] mod test;
