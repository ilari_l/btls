use super::Extendable;
use crate::consts::Class;
use crate::consts::DNSCLASS_ANY;
use crate::consts::Type;
use crate::consts::DNSTYPE_OPT;
use crate::consts::DNSTYPE_TSIG;
use crate::nameptr::NamePtr;
use crate::nameptr::NameDecompressionError;
use crate::rdata::MessageSection;
use crate::rdata::Rdata;
use crate::rdata::read_u16;
use crate::rdata::write_slice_len16;
use crate::rdata::Record;
use crate::rdata::RecordDecodeError;
use crate::rdata::TsigTimestamp;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::fail_if_none;
use core::convert::TryInto;
use core::fmt::Debug;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;



///DNS message header.
#[derive(Copy,Clone,Debug)]
pub struct Header
{
	pub id: u16,
	pub qr: bool,
	pub opcode: u8,
	pub aa: bool,
	pub tc: bool,
	pub rd: bool,
	pub ra: bool,
	pub z: u8,
	pub rcode: u16,
	pub qdcount: u16,
	pub ancount: u16,
	pub nscount: u16,
	pub arcount: u16
}

impl Header
{
	fn decode(msg: &[u8]) -> Option<Header>
	{
		fail_if_none!(msg.len() < 12);
		let mut w = [0u16; 6];
		for i in 0..12 { w[i/2] |= (msg[i] as u16) << (8 - 8 * (i&1)); }
		let qr = w[1] & 0x8000 != 0;
		let opcode = ((w[1] >> 11) & 15) as u8;
		let aa = w[1] & 0x400 != 0;
		let tc = w[1] & 0x200 != 0;
		let rd = w[1] & 0x100 != 0;
		let ra = w[1] & 0x80 != 0;
		let z = (w[1] as u8 & 0x70) >> 4;
		let rcode = (w[1] & 0xF) as u16;
		Some(Header{id:w[0], qr, opcode, aa, tc, rd, ra, z, rcode, qdcount:w[2],
			ancount:w[3], nscount:w[4], arcount:w[5]})
	}
}

///EDNS extension block.
#[derive(Copy,Clone,Debug)]
pub struct Edns<'a>
{
	pub version: u8,
	pub flags: u16,
	pub udpmax: u16,
	pub tlvs: &'a [u8],
}

///TSIG block.
#[derive(Copy,Clone,Debug)]
pub struct Tsig<'a>
{
	pub body_len: usize,
	pub keyid: NamePtr<'a>,
	pub algorithm: NamePtr<'a>,
	pub time: TsigTimestamp,
	pub mac: &'a [u8],
	pub original_id: u16,
	pub error: u16,
	pub other: &'a [u8],
}

///Error parsing DNS message.
#[derive(Copy,Clone,Debug)]
#[non_exhaustive]
pub enum MessageParseError
{
	///Message truncated.
	Truncated,
	///Error parsing record.
	BadRecord(RecordDecodeError),
	///Error parsing QNAME.
	BadQname(NameDecompressionError),
	///TSIG not last in additional section.
	TsigNotLast,
	///TSIG does not have CLASS *.
	TsigNotClassAny,
	///TSIG does not have TTL 0.
	TsigNotTtl0,
	///DSO messages are not supported.
	DsoNotSupported,
	///Internal error.
	InternalError(&'static str),
}

impl From<RecordDecodeError> for MessageParseError
{
	fn from(e: RecordDecodeError) -> MessageParseError
	{
		match e {
			RecordDecodeError::Truncated => MessageParseError::Truncated,
			other => MessageParseError::BadRecord(other),
		}
	}
}

impl From<NameDecompressionError> for MessageParseError
{
	fn from(e: NameDecompressionError) -> MessageParseError
	{
		match e {
			NameDecompressionError::Truncated => MessageParseError::Truncated,
			other => MessageParseError::BadQname(other),
		}
	}
}
impl Display for MessageParseError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::MessageParseError::*;
		match self {
			&Truncated => f.write_str("Message truncated"),
			&BadRecord(rec) => write!(f, "Message has bad record: {rec}"),
			&BadQname(qname) => write!(f, "Message has bad QNAME: {qname}"),
			TsigNotLast => f.write_str("TSIG not the last record"),
			TsigNotClassAny => f.write_str("TSIG does not have CLASS *"),
			TsigNotTtl0 => f.write_str("TSIG does not have TTL 0"),
			DsoNotSupported => f.write_str("DSO messages are not supported"),
			&InternalError(err) => write!(f, "Internal error: {err}"),
		}
	}
}

#[derive(Clone)]
struct ResponseIterator<'a>(&'a [u8], usize, u16, bool);

impl<'a> Iterator for ResponseIterator<'a>
{
	type Item=Record<'a>;
	fn next(&mut self) -> Option<Record<'a>>
	{
		loop {
			fail_if_none!(self.2 == 0);		//End of iteration.
			let item = Record::decode_unchecked(self.0, &mut self.1)?;
			self.2 -= 1;
			if !self.3 { return Some(item); }	//Not additional.
			match item.rtype {
				//In additional section, skip any OPT on root, and TSIG. Return others.
				DNSTYPE_OPT if item.owner.is_root() => (),
				DNSTYPE_TSIG => (),
				_ => return Some(item)
			}
		}
	}
}

#[derive(Clone)]
struct QueryIterator<'a>(&'a [u8], usize, u16);

impl<'a> Iterator for QueryIterator<'a>
{
	type Item=Query<'a>;
	fn next(&mut self) -> Option<Query<'a>>
	{
		fail_if_none!(self.2 == 0);		//End of iteration.
		let item = Query::decode_unchecked(self.0, &mut self.1)?;
		self.2 -= 1;
		Some(item)
	}
}

///DNS query block.
#[derive(Copy,Clone)]
pub struct Query<'a>
{
	pub qname: NamePtr<'a>,
	pub qtype: Type,
	pub qclass: Class,
}

impl<'a> Query<'a>
{
	///Decode the query block.
	///
	///The ptr is the offset of the block, and is updated to point to the end of the block.
	///
	///The decoded records are passed to the given callback.
	pub fn decode(msg: &'a [u8], ptr: &mut usize,
		cb: &mut impl FnMut(NamePtr, Type, Class, u32, &Rdata, MessageSection)) ->
		Result<Query<'a>, RecordDecodeError>
	{
		use self::RecordDecodeError::Truncated;
		let qname = NamePtr::read(msg, ptr, !0)?;
		//Read the values.
		let qtype = Type(read_u16(msg, ptr, !0).ok_or(Truncated)?);
		let qclass = Class(read_u16(msg, ptr, !0).ok_or(Truncated)?);
		cb(qname, qtype, qclass, 0, &Rdata::Other{rtype:qtype, data: b""}, MessageSection::Query);
		Ok(Query{qname, qtype, qclass})
	}
	pub(crate) fn decode_unchecked(msg: &'a [u8], ptr: &mut usize) -> Option<Query<'a>>
	{
		let qname = NamePtr::read_unchecked(msg, ptr);
		//Read the values.
		let r = msg.get(*ptr..*ptr+4)?;		//QTYPE, QCLASS.
		let qtype = Type(u16::from_be_bytes(r[0..2].try_into().ok()?));
		let qclass = Class(u16::from_be_bytes(r[2..4].try_into().ok()?));
		*ptr += 4;
		Some(Query{qname, qtype, qclass})
	}
}

pub use btls_aux_collections::Vec;

///DNS message.
pub struct Message<'a>
{
	header: Header,
	offset_query: usize,
	offset_answer: usize,
	offset_authority: usize,
	offset_additional: usize,
	queryv: Vec<Query<'a>>,
	answerv: Vec<Record<'a>>,
	authorityv: Vec<Record<'a>>,
	additionalv: Vec<Record<'a>>,
	edns: Option<Edns<'a>>,
	tsig: Option<Tsig<'a>>,
	msg: &'a [u8],
}

impl<'a> Debug for Message<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		f.write_str("DnsMessage { ")?;
		write!(f, "Header: {hdr:?}, ", hdr=self.header)?;
		for e in QueryIterator(self.msg, self.offset_query, self.header.qdcount) {
			let Query{qclass, qname, qtype} = e;
			write!(f, "Query: {qclass}:{qname} {qtype}?, ")?;
		}
		for e in ResponseIterator(self.msg, self.offset_answer, self.header.ancount, false) {
			let Record{clazz, owner, rtype, ttl, data, ..} = e;
			write!(f, "Answer: {clazz}:{owner} {rtype} {ttl} {data:?}, ")?;
		}
		for e in ResponseIterator(self.msg, self.offset_authority, self.header.nscount, false) {
			let Record{clazz, owner, rtype, ttl, data, ..} = e;
			write!(f, "Authority: {clazz}:{owner} {rtype} {ttl} {data:?}, ")?;
		}
		for e in ResponseIterator(self.msg, self.offset_additional, self.header.arcount, true) {
			let Record{clazz, owner, rtype, ttl, data, ..} = e;
			write!(f, "Additional: {clazz}:{owner} {rtype} {ttl} {data:?}, ")?;
		}
		write!(f, "edns: {edns:?}, ", edns=self.edns)?;
		write!(f, "tsig: {tsig:?} ", tsig=self.tsig)?;
		f.write_str("}")?;
		Ok(())
	}
}

impl<'a> Message<'a>
{
	#[cfg(test)] pub(crate) fn mangle_msgid(&mut self) { self.header.id = !self.header.id; }
	///Get the message header.
	pub fn header(&self) -> Header { self.header }
	///Get the message EDNS block, if any.
	pub fn edns(&self) -> Option<Edns<'a>> { self.edns }
	///Get the message TSIG block, if any.
	pub fn tsig(&self) -> Option<Tsig<'a>> { self.tsig }
	///Get the question section as slice.
	pub fn question_v(&self) -> &[Query<'a>] { &self.queryv }
	///Get the answer section as slice.
	pub fn answer_v(&self) -> &[Record<'a>] { &self.answerv }
	///Get the authority section as slice.
	pub fn authority_v(&self) -> &[Record<'a>] { &self.authorityv }
	///Get the additional section as slice.
	pub fn additional_v(&self) -> &[Record<'a>] { &self.additionalv }
	///Get the question section as iterator.
	pub fn question(&self) -> impl Iterator<Item=Query<'a>>+Clone
	{
		QueryIterator(self.msg, self.offset_query, self.header.qdcount)
	}
	///Get the answer section as iterator.
	pub fn answer(&self) -> impl Iterator<Item=Record<'a>>+Clone
	{
		ResponseIterator(self.msg, self.offset_answer, self.header.ancount, false)
	}
	///Get the authority section as iterator.
	pub fn authority(&self) -> impl Iterator<Item=Record<'a>>+Clone
	{
		ResponseIterator(self.msg, self.offset_authority, self.header.nscount, false)
	}
	///Get the additional section as iterator.
	pub fn additional(&self) -> impl Iterator<Item=Record<'a>>+Clone
	{
		ResponseIterator(self.msg, self.offset_additional, self.header.arcount, true)
	}
	///Decode DNS message.
	///
	///The decoded records are passed to the given callback.
	pub fn decode(msg: &'a [u8], mut cb: impl FnMut(NamePtr, Type, Class, u32, &Rdata,
		MessageSection)) -> Result<Message<'a>, MessageParseError>
	{
		use self::MessageParseError::*;
		let mut header = Header::decode(msg).ok_or(Truncated)?;
		//No, DSO is not supported.
		fail_if!(header.opcode == 6, DsoNotSupported);
		let mut queryv = Vec::with_capacity(header.qdcount as usize);
		let mut answerv = Vec::with_capacity(header.ancount as usize);
		let mut authorityv = Vec::with_capacity(header.nscount as usize);
		let mut additionalv = Vec::with_capacity(header.arcount as usize);
		let mut ptr = 12;
		let offset_query = ptr;
		for _ in 0..header.qdcount {
			let q = Query::decode(msg, &mut ptr, &mut cb)?;
			queryv.push(q);
		}
		let offset_answer = ptr;
		for _ in 0..header.ancount {
			let r = Record::decode(msg, &mut ptr, MessageSection::Answer, &mut cb)?;
			answerv.push(r);
		}
		let offset_authority = ptr;
		for _ in 0..header.nscount {
			let r = Record::decode(msg, &mut ptr, MessageSection::Authority, &mut cb)?;
			authorityv.push(r);
		}
		let offset_additional = ptr;
		let mut tsig_data = None;
		let mut edns = None;
		for _ in 0..header.arcount {
			let entry = Record::decode(msg, &mut ptr, MessageSection::Additional, &mut cb)?;
			fail_if!(tsig_data.is_some(), TsigNotLast);
			if entry.rtype == DNSTYPE_TSIG {
				fail_if!(entry.clazz != DNSCLASS_ANY, TsigNotClassAny);
				fail_if!(entry.ttl != 0, TsigNotTtl0);
				tsig_data = Some((entry.offset, entry.owner, entry.data));
			} else if entry.owner.is_root() && entry.rtype == DNSTYPE_OPT {
				//EDNS OPT.
				let ext_rcode = (entry.ttl >> 24) as u16;
				header.rcode |= ext_rcode << 4;
				edns = Some(Edns {
					udpmax: entry.clazz.0,
					version: (entry.ttl >> 16) as u8,
					flags: entry.ttl as u16,
					//This is always supposed to match.
					tlvs: entry.data.raw_data().
						ok_or(InternalError("OPT should not be special"))?,
				});
			} else {
				additionalv.push(entry);
			}
		}
		let tsig = if let Some((body_len, keyid, data)) = tsig_data {
			if let Rdata::Tsig{algorithm, time, mac, original_id, error, other} = data {
				Some(Tsig{body_len, keyid, algorithm, time, mac, original_id,
					error, other})
			} else {
				fail!(InternalError("TSIG should be special"));
			}
		} else {
			None
		};
		Ok(Message{header, offset_query, offset_answer, offset_authority, offset_additional, edns,
			tsig, msg, queryv, answerv, authorityv, additionalv})
	}
	///Append to be signed part of TSIG into buffer.
	///
	///Returns `true` if the message has TSIG, otherwise appends the entiere DNS message and return `false'.
	///
	///What is pushed into the buffer is directly the data the MAC in TSIG is computed over.
	pub fn append_tsig_tbs<E:Extendable>(&self, out: &mut E, continuation: bool) -> Result<bool, E::Error>
	{
		match &self.tsig {
			&Some(ref tsig) => {
				out.alloc_hint(tsig.body_len + 8 + if !continuation {
					tsig.keyid.len() + tsig.algorithm.len() + tsig.other.len() + 10
				} else {
					0
				});
				out.extend(&tsig.original_id.to_be_bytes())?;
				out.extend(self.msg.get(2..10).unwrap_or(b""))?;
				//Remove the TSIG from count.
				let madditional = self.header.arcount.saturating_sub(1);
				out.extend(&madditional.to_be_bytes())?;
				out.extend(self.msg.get(12..tsig.body_len).unwrap_or(b""))?;
				if !continuation {
					tsig.keyid.decompress(out, false)?;
					DNSCLASS_ANY.to_wire(out)?;
					out.extend(&0u32.to_be_bytes())?;
					tsig.algorithm.decompress(out, false)?;
				}
				tsig.time.write(out)?;
				if !continuation {
					out.extend(&tsig.error.to_be_bytes())?;
					write_slice_len16(out, tsig.other)?;
				}
				Ok(true)
			},
			//If message does not have tsig, append it in its entierety and return false.
			&None => {
				out.extend(&self.msg)?;
				Ok(false)
			}
		}
	}
}
