use super::Class;
use super::Extendable;
use super::NamePtr;
use super::Type;
use super::NameDecompressionError;
//use crate::consts::DNSCLASS_ANY;
use crate::consts::DNSTYPE_A;
use crate::consts::DNSTYPE_AAAA;
use crate::consts::DNSTYPE_CNAME;
use crate::consts::DNSTYPE_MB;
use crate::consts::DNSTYPE_MD;
use crate::consts::DNSTYPE_MF;
use crate::consts::DNSTYPE_MG;
use crate::consts::DNSTYPE_MINFO;
use crate::consts::DNSTYPE_MR;
use crate::consts::DNSTYPE_MX;
use crate::consts::DNSTYPE_NS;
//use crate::consts::DNSTYPE_OPT;
use crate::consts::DNSTYPE_PTR;
use crate::consts::DNSTYPE_SOA;
use crate::consts::DNSTYPE_TSIG;
//use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::fail_if_none;
use btls_aux_memory::Hexdump;
use btls_aux_memory::map_subarray;
use core::convert::TryInto;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::mem::replace;
use core::mem::size_of;
use core::mem::transmute;


fn read_slice_u16<'a>(msg: &'a [u8], ptr: &mut usize, eptr: usize) -> Option<&'a [u8]>
{
	let len = read_u16(msg, ptr, eptr)? as usize;
	let xptr = *ptr + len;
	fail_if_none!(xptr > eptr);
	let r = msg.get(*ptr..xptr)?;
	*ptr = xptr;
	Some(r)
}

pub(crate) fn read_u16(msg: &[u8], ptr: &mut usize, eptr: usize) -> Option<u16>
{
	type X = u16;
	let xptr = *ptr + size_of::<X>();
	fail_if_none!(xptr > eptr);
	map_subarray(msg, replace(ptr, xptr), X::from_be_bytes)
}

fn read_u32(msg: &[u8], ptr: &mut usize, eptr: usize) -> Option<u32>
{
	type X = u32;
	let xptr = *ptr + size_of::<X>();
	fail_if_none!(xptr > eptr);
	map_subarray(msg, replace(ptr, xptr), X::from_be_bytes)
}

pub(crate) fn write_u16<E:Extendable>(out: &mut E, val: u16) -> Result<(), E::Error>
{
	out.extend(&val.to_be_bytes())
}

pub(crate) fn write_u32<E:Extendable>(out: &mut E, val: u32) -> Result<(), E::Error>
{
	out.extend(&val.to_be_bytes())
}

pub(crate) fn write_slice_len16<E:Extendable>(out: &mut E, item: &[u8]) -> Result<(), E::Error>
{
	out.extend(&(item.len() as u16).to_be_bytes())?;
	out.extend(item)?;
	Ok(())
}

///Error decoding record.
#[derive(Copy,Clone,Debug)]
#[non_exhaustive]
pub enum RecordDecodeError
{
	///Truncated.
	Truncated,
	///Bad owner name.
	BadOwner(NameDecompressionError),
	///Junk after end of RDATA.
	JunkAfterRdata(Type),
	///Bad rdata name.
	BadRdataName(NameDecompressionError),
	///RDATA truncated.
	RdataTruncated(Type),
}

impl From<NameDecompressionError> for RecordDecodeError
{
	fn from(e: NameDecompressionError) -> RecordDecodeError
	{
		match e {
			NameDecompressionError::Truncated => RecordDecodeError::Truncated,
			other => RecordDecodeError::BadOwner(other),
		}
	}
}

impl Display for RecordDecodeError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::RecordDecodeError::*;
		match self {
			&Truncated => f.write_str("Record truncated"),
			&RdataTruncated(rtype) => write!(f, "Record RDATA truncated for {rtype}"),
			&BadOwner(owner) => write!(f, "Record has bad owner: {owner}"),
			&JunkAfterRdata(rtype) => write!(f, "Junk after RDATA for {rtype}"),
			&BadRdataName(err) => write!(f, "Record has bad name in RDATA: {err}"),
		}
	}
}

///TSIG timestamp.
#[derive(Copy,Clone,Debug)]
pub struct TsigTimestamp
{
	///Epoch (upper 16 bits of Unix timestamp).
	pub epoch: u16,
	///Lower 32 bits of Unix timestamp.
	pub time: u32,
	///The fudge in seconds.
	pub fudge: u16,
}

impl TsigTimestamp
{
	pub(crate) fn new(ts: u64, fudge: u16) -> TsigTimestamp
	{
		TsigTimestamp {
			epoch: (ts >> 32) as u16,
			time: ts as u32,
			fudge: fudge,
		}
	}
	fn read(msg: &[u8], ptr: &mut usize, eptr: usize) -> Option<TsigTimestamp>
	{
		let epoch = read_u16(msg, ptr, eptr)?;
		let time = read_u32(msg, ptr, eptr)?;
		let fudge = read_u16(msg, ptr, eptr)?;
		Some(TsigTimestamp{epoch, time, fudge})
	}
	pub(crate) fn write<E:Extendable>(&self, out: &mut E) -> Result<(), E::Error>
	{
		out.extend(&self.epoch.to_be_bytes())?;
		out.extend(&self.time.to_be_bytes())?;
		out.extend(&self.fudge.to_be_bytes())?;
		Ok(())
	}
	pub(crate) fn as_bytes(&self) -> [u8;8]
	{
		let w = (self.epoch as u64) << 48 | (self.time as u64) << 16 | self.fudge as u64;
		w.to_be_bytes()
	}
}

///RDATA record.
#[derive(Copy,Clone,Debug)]
pub enum Rdata<'a>
{
	///CNAME, MB, MD, MF, MG, MR, NS, PTR
	SName{rtype: Type, target:NamePtr<'a>},
	///MINFO.
	Minfo{emailbx:NamePtr<'a>, rmailbx:NamePtr<'a>},
	///MX.
	Mx{priority:u16, target:NamePtr<'a>},
	///SOA.
	Soa{mname:NamePtr<'a>, rname:NamePtr<'a>, serial:u32, refresh: u32, retry: u32, expire: u32,
		minimum: u32},
	///TSIG.
	Tsig{algorithm:NamePtr<'a>, time: TsigTimestamp, mac: &'a [u8], original_id: u16,
		error: u16, other: &'a [u8]},
	///All others.
	Other{rtype:Type, data: &'a [u8]},
}

//The following RRTYPEs can have compressed labels in RRDATA:
//- CNAME (domain) ID=5
//- MB (domain) ID=7
//- MD (domain) ID=3
//- MF (domain) ID=4
//- MG (domain) ID=8
//- MINFO (domain, domain) ID=14
//- MR (domain) ID=9
//- MX (u16, domain) ID=15
//- NS (domain) ID=2
//- PTR (domain) ID=12
//- SOA (domain, domain, u32, u32, u32, u32, u32) ID=6

impl<'a> Rdata<'a>
{
	fn decode_unchecked(msg: &'a [u8], ptr: usize, len: usize, rtype: Type) -> Option<Rdata<'a>>
	{
		let mut ptr = ptr;
		let r = match rtype {
			DNSTYPE_NS|DNSTYPE_MD|DNSTYPE_MF|DNSTYPE_CNAME|DNSTYPE_MB|DNSTYPE_MG|DNSTYPE_MR|
				DNSTYPE_PTR => {
				//These have SName format.
				let target = NamePtr::read_unchecked(msg, &mut ptr);
				Rdata::SName{rtype, target}
			},
			DNSTYPE_SOA => {
				//SOA. This has 2 labels and 5 32-bit words.
				let mname = NamePtr::read_unchecked(msg, &mut ptr);
				let rname = NamePtr::read_unchecked(msg, &mut ptr);
				let r = msg.get(ptr..ptr+20)?;
				let serial = map_subarray(r, 0, u32::from_be_bytes)?;
				let refresh = map_subarray(r, 4, u32::from_be_bytes)?;
				let retry = map_subarray(r, 8, u32::from_be_bytes)?;
				let expire = map_subarray(r, 12, u32::from_be_bytes)?;
				let minimum = map_subarray(r, 16, u32::from_be_bytes)?;
				Rdata::Soa{mname,rname,serial,refresh,retry,expire,minimum}
			},
			DNSTYPE_MINFO => {
				//MInfo. This has 2 names.
				let rmailbx = NamePtr::read_unchecked(msg, &mut ptr);
				let emailbx = NamePtr::read_unchecked(msg, &mut ptr);
				Rdata::Minfo{emailbx, rmailbx}
			},
			DNSTYPE_MX => {
				//MX. This has 16-bit value and name.
				let r = msg.get(ptr..ptr+2)?;
				let priority = u16::from_be_bytes(r.try_into().ok()?);
				ptr += 2;
				let target = NamePtr::read_unchecked(msg, &mut ptr);
				Rdata::Mx{priority, target}
			},
			DNSTYPE_TSIG => {
				//TSIG: This has complex of fields, starting with name field. NamePtr::read()
				//can be used, because it is always correct to treat non-compressible field as
				//compressible one, as compressible notion is superset of non-compressible one.
				let algorithm = NamePtr::read_unchecked(msg, &mut ptr);
				let r = msg.get(ptr..ptr+10)?;	//Time fields and mac length.
				let epoch = map_subarray(r, 0, u16::from_be_bytes)?;
				let time = map_subarray(r, 2, u32::from_be_bytes)?;
				let fudge = map_subarray(r, 6, u16::from_be_bytes)?;
				let maclen = map_subarray(r, 8, u16::from_be_bytes)?;
				ptr += 10;
				let mac = msg.get(ptr..ptr+maclen as usize)?;
				ptr += maclen as usize;
				let r = msg.get(ptr..ptr+6)?;	//original_id, error and other length.
				let original_id = map_subarray(r, 0, u16::from_be_bytes)?;
				let error = map_subarray(r, 2, u16::from_be_bytes)?;
				let otherlen = map_subarray(r, 4, u16::from_be_bytes)?;
				ptr += 6;
				let other = msg.get(ptr..ptr+otherlen as usize)?;
				let time = TsigTimestamp{epoch, time, fudge};
				Rdata::Tsig{algorithm, time, mac, original_id, error, other}
			},
			rtype => {
				//Raw copy.
				Rdata::Other{rtype, data: msg.get(ptr..ptr+len)?}
			}
		};
		Some(r)
	}
	fn decode(msg: &'a [u8], ptr: usize, len: usize, rtype: Type) -> Result<Rdata<'a>, RecordDecodeError>
	{
		use self::RecordDecodeError::*;
		let eptr = ptr + len;
		let mut ptr = ptr;
		let rdata = msg.get(ptr..eptr).ok_or(Truncated)?;
		let truncated = RdataTruncated(rtype);
		let junk = JunkAfterRdata(rtype);
		let r = match rtype {
			DNSTYPE_NS|DNSTYPE_MD|DNSTYPE_MF|DNSTYPE_CNAME|DNSTYPE_MB|DNSTYPE_MG|DNSTYPE_MR|
				DNSTYPE_PTR => {
				//These have SName format.
				let target = NamePtr::read(msg, &mut ptr, eptr).map_err(BadRdataName)?;
				fail_if!(ptr != eptr, junk);
				Rdata::SName{rtype, target}
			},
			DNSTYPE_SOA => {
				//SOA. This has 2 labels and 5 32-bit words.
				let mname = NamePtr::read(msg, &mut ptr, eptr).map_err(BadRdataName)?;
				let rname = NamePtr::read(msg, &mut ptr, eptr).map_err(BadRdataName)?;
				let serial = read_u32(msg, &mut ptr, eptr).ok_or(truncated)?;
				let refresh = read_u32(msg, &mut ptr, eptr).ok_or(truncated)?;
				let retry = read_u32(msg, &mut ptr, eptr).ok_or(truncated)?;
				let expire = read_u32(msg, &mut ptr, eptr).ok_or(truncated)?;
				let minimum = read_u32(msg, &mut ptr, eptr).ok_or(truncated)?;
				fail_if!(ptr != eptr, junk);
				Rdata::Soa{mname,rname,serial,refresh,retry,expire,minimum}
			},
			DNSTYPE_MINFO => {
				//MInfo. This has 2 names.
				let rmailbx = NamePtr::read(msg, &mut ptr, eptr).map_err(BadRdataName)?;
				let emailbx = NamePtr::read(msg, &mut ptr, eptr).map_err(BadRdataName)?;
				fail_if!(ptr != eptr, junk);
				Rdata::Minfo{emailbx, rmailbx}
			},
			DNSTYPE_MX => {
				//MX. This has 16-bit value and name.
				let priority = read_u16(msg, &mut ptr, eptr).ok_or(truncated)?;
				let target = NamePtr::read(msg, &mut ptr, eptr).map_err(BadRdataName)?;
				fail_if!(ptr != eptr, junk);
				Rdata::Mx{priority, target}
			},
			DNSTYPE_TSIG => {
				//TSIG: This has complex of fields, starting with name field. NamePtr::read()
				//can be used, because it is always correct to treat non-compressible field as
				//compressible one, as compressible notion is superset of non-compressible one.
				let algorithm = NamePtr::read(msg, &mut ptr, eptr).map_err(BadRdataName)?;
				let time = TsigTimestamp::read(msg, &mut ptr, eptr).ok_or(truncated)?;
				let mac = read_slice_u16(msg, &mut ptr, eptr).ok_or(truncated)?;
				let original_id = read_u16(msg, &mut ptr, eptr).ok_or(truncated)?;
				let error = read_u16(msg, &mut ptr, eptr).ok_or(truncated)?;
				let other = read_slice_u16(msg, &mut ptr, eptr).ok_or(truncated)?;
				fail_if!(ptr != eptr, junk);
				Rdata::Tsig{algorithm, time, mac, original_id, error, other}
			},
			rtype => {
				//Raw copy.
				Rdata::Other{rtype, data: rdata}
			}
		};
		Ok(r)
	}
	pub(crate) fn raw_data(&self) -> Option<&'a [u8]>
	{
		if let &Rdata::Other{data,..} = self { Some(data) } else { None }
	}
	///Uncompress the RDATA into buffer.
	///
	///This uncompresses any compression pointers in the data.
	pub fn uncompress<E:Extendable>(&self, v: &mut E) -> Result<(), E::Error>
	{
		match self {
			&Rdata::SName{ref target,..} => {
				v.alloc_hint(target.len());
				target.decompress(v, true)?;
			},
			&Rdata::Minfo{ref rmailbx, ref emailbx} => {
				v.alloc_hint(rmailbx.len()+emailbx.len());
				rmailbx.decompress(v, true)?;
				emailbx.decompress(v, true)?;
			},
			&Rdata::Mx{priority, ref target} => {
				v.alloc_hint(target.len()+2);
				write_u16(v, priority)?;
				target.decompress(v, true)?;
			},
			&Rdata::Soa{ref mname, ref rname, serial, refresh, retry, expire, minimum} => {
				v.alloc_hint(mname.len()+rname.len()+20);
				mname.decompress(v, true)?;
				rname.decompress(v, true)?;
				write_u32(v, serial)?;
				write_u32(v, refresh)?;
				write_u32(v, retry)?;
				write_u32(v, expire)?;
				write_u32(v, minimum)?;
			},
			&Rdata::Tsig{algorithm, time, mac, original_id, error, other} => {
				v.alloc_hint(algorithm.len()+mac.len()+other.len()+16);
				algorithm.decompress(v, false)?;
				time.write(v)?;
				write_slice_len16(v, mac)?;
				write_u16(v, original_id)?;
				write_u16(v, error)?;
				write_slice_len16(v, other)?;
			}
			&Rdata::Other{data,..} => v.extend(data)?,
		}
		Ok(())
	}
}


impl<'a> Display for Rdata<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&Rdata::SName{rtype,ref target} =>
				write!(f, "{rtype} target={target}"),
			&Rdata::Minfo{ref emailbx, ref rmailbx} =>
				write!(f, "MINFO rmailbx={rmailbx} emailbx={emailbx}"),
			&Rdata::Mx{priority, ref target} => write!(f, "MX priority={priority} target={target}"),
			&Rdata::Soa{ref mname, ref rname, serial, refresh, retry, expire, minimum} =>
				write!(f, "SOA mname={mname} rname={rname} serial={serial} refresh={refresh} \
					retry={retry} expire={expire} minimum={minimum}"),
			&Rdata::Other{rtype:DNSTYPE_AAAA, data} if data.len() == 16 => {
				let mut data2 = [0;8];
				for i in 0..8 {
					data2[i] = data[2*i+0] as u16 * 256 + data[2*i+1] as u16;
				}
				f.write_str("AAAA ")?;
				print_ipv6(f, &data2)
			},
			&Rdata::Other{rtype:DNSTYPE_A, data} if data.len() == 4 => {
				let data2: &[u8;4] = unsafe{transmute(data.as_ptr())};
				f.write_str("A ")?;
				print_ipv4(f, data2)
			},
			&Rdata::Tsig{algorithm, time, mac, original_id, error, other} =>
				write!(f, "TSIG algorithm={algorithm}, time={time}(+-{fudge}), \
					original_id={original_id}, error={error}, other={other}, mac={mac}",
					fudge=time.fudge, other=Hexdump(other), mac=Hexdump(mac),
					time=(time.epoch as u64)<< 32|time.time as u64),
			&Rdata::Other{rtype, data} => write!(f, "{rtype} {rdata}", rdata=Hexdump(data)),
		}
	}
}

///Section in the message.
#[derive(Copy,Clone,PartialEq,Eq)]
pub enum MessageSection
{
	Query,
	Answer,
	Authority,
	Additional,
}

///DNS record.
#[derive(Copy,Clone)]
pub struct Record<'a>
{
	///Owner name of the record.
	pub owner: NamePtr<'a>,
	///Record type.
	pub rtype: Type,
	///Record class.
	pub clazz: Class,
	///Time to live in seconds.
	pub ttl: u32,
	///The record data.
	pub data: Rdata<'a>,
	pub(crate) offset: usize,
}

impl<'a> Record<'a>
{
	///Decode a record.
	///
	///The given pointer is updated to the end of the record and the given callback is called.
	pub fn decode(msg: &'a [u8], ptr: &mut usize, section: MessageSection,
		cb: &mut impl FnMut(NamePtr, Type, Class, u32, &Rdata, MessageSection)) ->
		Result<Record<'a>, RecordDecodeError>
	{
		use self::RecordDecodeError::Truncated;
		let offset = *ptr;
		let owner = NamePtr::read(msg, ptr, !0)?;
		//Read the values.
		let rtype = Type(read_u16(msg, ptr, !0).ok_or(Truncated)?);
		let clazz = Class(read_u16(msg, ptr, !0).ok_or(Truncated)?);
		let ttl = read_u32(msg, ptr, !0).ok_or(Truncated)?;
		let len = read_u16(msg, ptr, !0).ok_or(Truncated)? as usize;
		let data = Rdata::decode(msg, *ptr, len, rtype)?;
		*ptr += len;
		cb(owner, rtype, clazz, ttl, &data, section);
		Ok(Record{owner, rtype, clazz, ttl, data, offset})
	}
	pub(crate) fn decode_unchecked(msg: &'a [u8], ptr: &mut usize) -> Option<Record<'a>>
	{
		let offset = *ptr;
		let owner = NamePtr::read_unchecked(msg, ptr);
		//Read the values.
		let r = msg.get(*ptr..*ptr+10)?;	//RTYPE, CLASS, TTL and RDLENGTH.
		let rtype = Type(map_subarray(r, 0, u16::from_be_bytes)?);
		let clazz = Class(map_subarray(r, 2, u16::from_be_bytes)?);
		let ttl = map_subarray(r, 4, u32::from_be_bytes)?;
		let rdlength = map_subarray(r, 8, u16::from_be_bytes)? as usize;
		*ptr += 10;
		let data = Rdata::decode_unchecked(msg, *ptr, rdlength, rtype)?;
		*ptr += rdlength;
		Some(Record{owner, rtype, clazz, ttl, data, offset})
	}
}

fn maxrun(mask: u8) -> (usize, usize)
{
	for i in 0..8 {
		let j = 8 - i;
		let m = ((1u16 << j) - 1) as u8;
		for k in 0..(i+1) { if (mask >> k) & m == m { return (k, k + j); } }
	}
	(0, 0)
}

fn print_ipv4(fmt: &mut Formatter, addr: &[u8;4]) -> Result<(), FmtError>
{
	let [a,b,c,d] = *addr;
	write!(fmt, "{a}.{b}.{c}.{d}")
}

fn print_ipv6(fmt: &mut Formatter, addr: &[u16;8]) -> Result<(), FmtError>
{
	let mut m = 0;
	for i in 0..8 { if addr[i] == 0 { m |= 1 << i; } }
	let (a, b) = maxrun(m);
	for i in 0..8 {
		if i < a || i >= b {
			write!(fmt, "{hextet:x}", hextet=addr[i])?;
			if i < 7 && i + 1 != a { fmt.write_str(":")?; }
		}
		if i == a && b > a { fmt.write_str("::")?; }
	}
	Ok(())
}
