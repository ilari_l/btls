use super::Extendable;
use crate::consts::DNSCLASS_ANY;
use crate::consts::DNSTYPE_TSIG;
use crate::name::Name;
use crate::rdata::TsigTimestamp;
use crate::rdata::write_u16;
use crate::rdata::write_u32;
use crate::rdata::write_slice_len16;

///TSIG signer.
pub trait TsigSigner
{
	///Get the key ID used.
	fn get_keyid<'a>(&'a self) -> &'a Name;
	///Get the algorithm used.
	fn get_algorithm<'a>(&'a self) -> &'a Name;
	///Get the maximum MAC length.
	fn get_max_mac_length(&self) -> u16;
	///MAC concatenation of slices and write prefix of result to given buffer.
	///
	///If output is shorter than `get_max_mac_length()`, the output is truncated. If the output is longer,
	///the remaining buffer is left alone.
	fn mac_multi(&self, output: &mut [u8], data: &[&[u8]]);
}

pub(crate) fn tcp_fixup_length(msg: &mut impl Extendable)
{
	let msglen = msg.len() - 2;	//Length of length not included.
	msg.write(0, (msglen >> 8) as u8);
	msg.write(1, msglen as u8);
}

fn tsig_write_record<E:Extendable>(msg: &mut E, tcp: bool, offset: usize, keyid: &Name, algorithm: &Name,
	mac: &[u8], other: &[u8], timestamp: TsigTimestamp, error: u16) -> Result<(), E::Error>
{
	//Start extending the message in order to directly write MAC to its place.
	msg.alloc_hint(keyid.as_raw().len()+algorithm.as_raw().len()+mac.len()+other.len()+26);
	msg.extend(&keyid.as_raw())?;
	DNSTYPE_TSIG.to_wire(msg)?;
	DNSCLASS_ANY.to_wire(msg)?;
	write_u32(msg, 0)?;
	//TSIG is 16 bytes, plus algorithm, MAC and other.
	let dlen = 16 + algorithm.as_raw().len() as u16 + mac.len() as u16 + other.len() as u16;
	write_u16(msg, dlen)?;
	msg.extend(&algorithm.as_raw())?;
	timestamp.write(msg)?;
	write_slice_len16(msg, mac)?;
	let orig_txid = msg.read(offset+0) as u16 * 256 + msg.read(offset+1) as u16;
	write_u16(msg, orig_txid)?;
	write_u16(msg, error)?;
	write_slice_len16(msg, other)?;
	//Increment ARCOUNT by 1.
	msg.write(offset+11, msg.read(offset+11).wrapping_add(1));
	if msg.read(offset+11) == 0 { msg.write(offset+10, msg.read(offset+10).wrapping_add(1)); }
	//In TCP mode fix message length.
	if tcp { tcp_fixup_length(msg); }
	Ok(())
}

///Refuse to sign TSIG.
pub fn tsig_refuse_sign<E:Extendable>(msg: &mut E, tcp: bool, keyid: &Name, algorithm: &Name, timestamp: u64,
	fudge: u16, error: u16, other: &[u8]) -> Result<(), E::Error>
{
	let timestamp = TsigTimestamp::new(timestamp, fudge);
	let offset = if tcp { 2 } else { 0 };
	if msg.len() < offset + 12 { return Ok(()); }	//Malformed.
	tsig_write_record(msg, tcp, offset, keyid, algorithm, &[], other, timestamp, error)
}

///Sign TSIG.
pub fn tsig_sign<E:Extendable>(msg: &mut E, tcp: bool, signer: &dyn TsigSigner, timestamp: u64, fudge: u16,
	error: u16, other: &[u8], chain_mac: Option<(&[u8], bool)>) -> Result<(), E::Error>
{
	let keyid = signer.get_keyid();
	let algorithm = signer.get_algorithm();
	let timestamp = TsigTimestamp::new(timestamp, fudge);
	let offset = if tcp { 2 } else { 0 };
	if msg.len() < offset + 12 { return Ok(()); }	//Malformed.

	let mut tbs2 = [&[][..];11];
	let chain_mac_len;
	let dnsclass_any = DNSCLASS_ANY.to_bytes();
	let zero_ttl = 0u32.to_be_bytes();
	let tbs_ts = timestamp.as_bytes();
	let error_b = error.to_be_bytes();
	let other_len = (other.len() as u16).to_be_bytes();
	let continuation = if let Some((chain_mac, continuation)) = chain_mac {
		chain_mac_len = (chain_mac.len() as u16).to_be_bytes();
		tbs2[0] = &chain_mac_len;
		tbs2[1] = chain_mac;
		continuation	//Replies may be continuations.
	} else {
		false		//Queries are never continuations.
	};
	tbs2[2] = msg.slice_from_index(offset);
	if !continuation {
		tbs2[3] = &keyid.as_raw();
		tbs2[4] = &dnsclass_any;
		tbs2[5] = &zero_ttl;
		tbs2[6] = &algorithm.as_raw();
	}
	tbs2[7] = &tbs_ts;
	if !continuation {
		tbs2[8] = &error_b;
		tbs2[9] = &other_len;
		tbs2[10] = other;
	}
	let maclen = core::cmp::min(signer.get_max_mac_length(), 255);	//Cap MAC at 255 bytes.
	let mut mac = [0;255];
	let mac = &mut mac[..maclen as usize];
	signer.mac_multi(mac, &tbs2);
	tsig_write_record(msg, tcp, offset, keyid, algorithm, mac, other, timestamp, error)
}
