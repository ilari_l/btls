use super::Class;
use super::Dns0x20;
use super::Extendable;
use super::Name;
use super::NameBuf;
use crate::name::print_name2;
use btls_aux_fail::f_return;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use core::fmt::Debug;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::mem::replace;

#[derive(Clone)]
struct LabelIterator<'a>(&'a [u8], usize);

impl<'a> Iterator for LabelIterator<'a>
{
	type Item = &'a [u8];
	fn next(&mut self) -> Option<&'a [u8]>
	{
		//This has already been validated, so just UB if it is not valid.
		let mut llen = self.0.get(self.1).map(|x|*x)?;
		if llen >= 192 {	//Pointer jump.
			let ptrl = self.0.get(self.1+1).map(|x|*x)?;
			self.1 = (llen as usize & 63) * 256 + ptrl as usize;
			llen = self.0.get(self.1).map(|x|*x)?;
		}
		if llen == 0 {
			self.1 = !0;	//Fuse.
			Some(b"")
		} else {
			let llen = llen as usize;
			let sptr = self.1+1;
			let eptr = sptr+llen;
			let r = self.0.get(sptr..eptr)?;
			self.1 = eptr;
			Some(r)
		}
	}
}

///Error in decompressing name.
#[derive(Copy,Clone,Debug)]
#[non_exhaustive]
pub enum NameDecompressionError
{
	///Name truncated.
	Truncated,
	///Illegal compression pointer.
	IllegalPointer,
	///Two consequtive compression pointers.
	ConsequtivePointers,
	///Name too long.
	NameTooLong,
	///Target buffer too small.
	BufferTooSmall,
	///Illegal label.
	IllegalLabel(u8),
	///Name compression not allowed.
	IllegalCompression,
}

impl Display for NameDecompressionError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::NameDecompressionError::*;
		match self {
			&Truncated => f.write_str("Name truncated"),
			&IllegalPointer => f.write_str("Illegal compression ponter"),
			&ConsequtivePointers => f.write_str("Two consequtive compression pointers"),
			&NameTooLong => f.write_str("Name too long"),
			&BufferTooSmall => f.write_str("Buffer too small"),
			&IllegalLabel(ltype) => write!(f, "Illegal label type {ltype}"),
			IllegalCompression => f.write_str("Name compression not allowed"),
		}
	}
}

///Print class and name.
#[derive(Copy,Clone)]
pub struct ClassNamePtr<'a>(pub Class, pub NamePtr<'a>);

impl<'a> Display for ClassNamePtr<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		Display::fmt(&self.0, f)?;
		f.write_str(":")?;
		print_name2(self.1.iter(), f, None)
	}
}

///Name pointer.
#[derive(Copy,Clone)]
pub struct NamePtr<'a>(&'a [u8], usize);

impl<'a> NamePtr<'a>
{
	pub(crate) fn read(msg: &'a [u8], ptr: &mut usize, eptr: usize) ->
		Result<NamePtr<'a>, NameDecompressionError>
	{
		use self::NameDecompressionError::Truncated;
		use self::NameDecompressionError::NameTooLong;
		let optr = *ptr;
		let mut tlen = 0u8;
		loop {
			fail_if!(*ptr >= eptr, Truncated);
			let llen = msg.get(*ptr).map(|x|*x).ok_or(Truncated)?;
			if llen == 0 {
				//Empty label ends the name.
				*ptr += 1;
				tlen.checked_add(1).ok_or(NameTooLong)?;
				fail_if!(*ptr > eptr, Truncated);
				return Ok(NamePtr(msg, optr));
			} else if llen < 64 {
				*ptr += llen as usize + 1;
				tlen = tlen.checked_add(llen + 1).ok_or(NameTooLong)?;
			} else if llen < 192 {
				fail!(NameDecompressionError::IllegalLabel(llen));
			} else {
				//Compression pointer ends the name representation.
				let ptrl = msg.get(*ptr+1).map(|x|*x).ok_or(Truncated)?;
				let cptr = (llen as usize & 63) * 256 + ptrl as usize;
				fail_if!(cptr >= msg.len(), NameDecompressionError::IllegalPointer);
				Self::__validate_name_pointer(msg, cptr, tlen)?;
				*ptr += 2;
				fail_if!(*ptr > eptr, Truncated);
				return Ok(NamePtr(msg, optr));
			}
		}
	}
	fn __validate_name_pointer(msg: &[u8], mut ptr: usize, mut tlen: u8) -> Result<(), NameDecompressionError>
	{
		use self::NameDecompressionError::Truncated;
		use self::NameDecompressionError::NameTooLong;
		let mut last_ptr = true;
		loop {
			let llen = msg.get(ptr).map(|x|*x).ok_or(Truncated)?;
			if llen == 0 {
				//Empty label ends the name.
				tlen.checked_add(1).ok_or(NameTooLong)?;
				return Ok(());
			} else if llen < 64 {
				ptr += llen as usize + 1;
				tlen = tlen.checked_add(llen + 1).ok_or(NameTooLong)?;
				last_ptr = false;
			} else if llen < 192 {
				fail!(NameDecompressionError::IllegalLabel(llen));
			} else {
				fail_if!(last_ptr, NameDecompressionError::ConsequtivePointers);
				last_ptr = true;
				let ptrl = msg.get(ptr+1).map(|x|*x).ok_or(Truncated)?;
				ptr = (llen as usize & 63) * 256 + ptrl as usize;
				fail_if!(ptr >= msg.len(), NameDecompressionError::IllegalPointer);
			}
		}
	}
	//Only call if read() has previously succeeded at the same offset
	pub(crate) fn read_unchecked(msg: &'a [u8], ptr: &mut usize) -> NamePtr<'a>
	{
		let optr = *ptr;
		while *ptr < msg.len() {
			let b = msg[*ptr];
			if b == 0 {
				*ptr += 1;
				break;
			} else if b < 192 {
				*ptr += 1 + b as usize;
			} else {
				*ptr += 2;
				break;
			}
		}
		NamePtr(msg, optr)
	}
	///Return the decompressed length.
	pub fn len(&self) -> usize
	{
		LabelIterator(self.0, self.1).fold(0, |a,b|a+b.len()+1)
	}
	///Decompress the name pointer into owned name.
	pub fn decompress_owned(&self) -> NameBuf
	{
		NameBuf::from_labels(LabelIterator(self.0, self.1))
	}
	pub(crate) fn decompress<E:Extendable>(&self, out: &mut E, normalize: bool) -> Result<(), E::Error>
	{
		for label in LabelIterator(self.0, self.1) {
			out.push(label.len() as u8)?;
			for &b in label.iter() {
				let v = if normalize && b.wrapping_sub(65) < 26 { b + 32 } else { b };
				out.push(v)?;
			}
		}
		Ok(())
	}
	///The output is at worst 503 bytes.
	pub fn decompress_as_master<E:Extendable>(&self, out: &mut E) -> Result<(), E::Error>
	{
		Name::__to_master(LabelIterator(self.0, self.1), out)
	}
	///Does the name strictly equal another (including DNS 0x20)?
	pub fn equals_strict(&self, with: &Name, dns0x20: &Dns0x20) -> bool
	{
		with.equals_strict(LabelIterator(self.0, self.1), dns0x20)
	}
	///Is root?
	pub fn is_root(&self) -> bool
	{
		let mut ptr = self.1;
		let msg = self.0;
		let mut no_redir = false;
		loop {
			let llen = *f_return!(msg.get(ptr), false);
			//If label is uncompressed, it is only root if it is empty.
			if llen < 192 { return llen == 0; }
			//Follow pointer, but only once.
			if replace(&mut no_redir, false) { return false; }	//Two back-to-back pointers.
			let llen2 = *f_return!(msg.get(ptr+1), false);
			ptr = (llen as usize & 63) * 256 + (llen2 as usize);
		}
	}
	///Iterate over labels in name.
	pub fn iter(&self) -> impl Iterator<Item=&'a [u8]>+Clone { LabelIterator(self.0, self.1) }
}

impl<'a,'b> PartialEq<&'b Name> for NamePtr<'a>
{
	fn eq(&self, name: &&'b Name) -> bool
	{
		name.equals_ignore_case(LabelIterator(self.0, self.1))
	}
}

impl<'a> Display for NamePtr<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		//Use no dns0x20, as that way no case changes occur.
		print_name2(LabelIterator(self.0, self.1), f, None)
	}
}

impl<'a> Debug for NamePtr<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> { Display::fmt(self, f) }
}

