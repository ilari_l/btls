use super::*;
use std::vec::Vec;
use std::fs::File;
use std::io::Read;

#[test]
fn sha3_256_kat()
{
	let mut ctx = Sha3256::new();
	ctx.input(b"abc");
	let h = ctx.output();
	assert_eq!(&h[..], &[
		0x3a, 0x98, 0x5d, 0xa7, 0x4f, 0xe2, 0x25, 0xb2,
		0x04, 0x5c, 0x17, 0x2d, 0x6b, 0xd3, 0x90, 0xbd,
		0x85, 0x5f, 0x08, 0x6e, 0x3e, 0x9d, 0x52, 0x5b,
		0x46, 0xbf, 0xe2, 0x45, 0x11, 0x43, 0x15, 0x32
	][..]);
	let mut ctx = Sha3256::new();
	ctx.input(b"abc");
	ctx.reset();
	let h = ctx.output();
	assert_eq!(&h[..], &[
		0xa7, 0xff, 0xc6, 0xf8, 0xbf, 0x1e, 0xd7, 0x66,
		0x51, 0xc1, 0x47, 0x56, 0xa0, 0x61, 0xd6, 0x62,
		0xf5, 0x80, 0xff, 0x4d, 0xe4, 0x3b, 0x49, 0xfa,
		0x82, 0xd8, 0x0a, 0x4b, 0x80, 0xf8, 0x43, 0x4a
	][..]);
	let mut ctx = Sha3256::new();
	ctx.input(b"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq");
	let h = ctx.output();
	assert_eq!(&h[..], &[
		0x41, 0xc0, 0xdb, 0xa2, 0xa9, 0xd6, 0x24, 0x08,
		0x49, 0x10, 0x03, 0x76, 0xa8, 0x23, 0x5e, 0x2c,
		0x82, 0xe1, 0xb9, 0x99, 0x8a, 0x99, 0x9e, 0x21,
		0xdb, 0x32, 0xdd, 0x97, 0x49, 0x6d, 0x33, 0x76
	][..]);
	let mut ctx = Sha3256::new();
	ctx.input(b"abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmn\
		hijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu");
	let h = ctx.output();
	assert_eq!(&h[..], &[
		0x91, 0x6f, 0x60, 0x61, 0xfe, 0x87, 0x97, 0x41,
		0xca, 0x64, 0x69, 0xb4, 0x39, 0x71, 0xdf, 0xdb,
		0x28, 0xb1, 0xa3, 0x2d, 0xc3, 0x6c, 0xb3, 0x25,
		0x4e, 0x81, 0x2b, 0xe2, 0x7a, 0xad, 0x1d, 0x18
	][..]);
	let mut ctx = Sha3256::new();
	ctx.input(b"Example of ECDSA with P-256");
	let h = ctx.output();
	assert_eq!(&h[..], &[
		0x03, 0xE3, 0x9E, 0x85, 0xB2, 0x51, 0xDF, 0x09,
		0x1B, 0xE8, 0xAA, 0x2E, 0x8C, 0x88, 0x31, 0xEC,
		0xF6, 0x88, 0xE0, 0x22, 0x7E, 0x1D, 0x4C, 0x70,
		0x42, 0xE6, 0x74, 0xB9, 0xB6, 0x46, 0x5F, 0x3B
	][..]);
}


#[test]
fn sha3_384_kat()
{
	let mut ctx = Sha3384::new();
	ctx.input(b"abc");
	let h = ctx.output();
	assert_eq!(&h[..], &[
		0xec, 0x01, 0x49, 0x82, 0x88, 0x51, 0x6f, 0xc9,
		0x26, 0x45, 0x9f, 0x58, 0xe2, 0xc6, 0xad, 0x8d,
		0xf9, 0xb4, 0x73, 0xcb, 0x0f, 0xc0, 0x8c, 0x25,
		0x96, 0xda, 0x7c, 0xf0, 0xe4, 0x9b, 0xe4, 0xb2,
		0x98, 0xd8, 0x8c, 0xea, 0x92, 0x7a, 0xc7, 0xf5,
		0x39, 0xf1, 0xed, 0xf2, 0x28, 0x37, 0x6d, 0x25
	][..]);
	let mut ctx = Sha3384::new();
	ctx.input(b"abc");
	ctx.reset();
	let h = ctx.output();
	assert_eq!(&h[..], &[
		0x0c, 0x63, 0xa7, 0x5b, 0x84, 0x5e, 0x4f, 0x7d,
		0x01, 0x10, 0x7d, 0x85, 0x2e, 0x4c, 0x24, 0x85,
		0xc5, 0x1a, 0x50, 0xaa, 0xaa, 0x94, 0xfc, 0x61,
		0x99, 0x5e, 0x71, 0xbb, 0xee, 0x98, 0x3a, 0x2a,
		0xc3, 0x71, 0x38, 0x31, 0x26, 0x4a, 0xdb, 0x47,
		0xfb, 0x6b, 0xd1, 0xe0, 0x58, 0xd5, 0xf0, 0x04
	][..]);
	let mut ctx = Sha3384::new();
	ctx.input(b"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq");
	let h = ctx.output();
	assert_eq!(&h[..], &[
		0x99, 0x1c, 0x66, 0x57, 0x55, 0xeb, 0x3a, 0x4b,
		0x6b, 0xbd, 0xfb, 0x75, 0xc7, 0x8a, 0x49, 0x2e,
		0x8c, 0x56, 0xa2, 0x2c, 0x5c, 0x4d, 0x7e, 0x42,
		0x9b, 0xfd, 0xbc, 0x32, 0xb9, 0xd4, 0xad, 0x5a,
		0xa0, 0x4a, 0x1f, 0x07, 0x6e, 0x62, 0xfe, 0xa1,
		0x9e, 0xef, 0x51, 0xac, 0xd0, 0x65, 0x7c, 0x22
	][..]);
	let mut ctx = Sha3384::new();
	ctx.input(b"abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmn\
		hijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu");
	let h = ctx.output();
	assert_eq!(&h[..], &[
		0x79, 0x40, 0x7d, 0x3b, 0x59, 0x16, 0xb5, 0x9c,
		0x3e, 0x30, 0xb0, 0x98, 0x22, 0x97, 0x47, 0x91,
		0xc3, 0x13, 0xfb, 0x9e, 0xcc, 0x84, 0x9e, 0x40,
		0x6f, 0x23, 0x59, 0x2d, 0x04, 0xf6, 0x25, 0xdc,
		0x8c, 0x70, 0x9b, 0x98, 0xb4, 0x3b, 0x38, 0x52,
		0xb3, 0x37, 0x21, 0x61, 0x79, 0xaa, 0x7f, 0xc7
	][..]);
}

#[test]
fn sha3_512_kat()
{
	let mut ctx = Sha3512::new();
	ctx.input(b"abc");
	let h = ctx.output();
	assert_eq!(&h[..], &[
		0xb7, 0x51, 0x85, 0x0b, 0x1a, 0x57, 0x16, 0x8a,
		0x56, 0x93, 0xcd, 0x92, 0x4b, 0x6b, 0x09, 0x6e,
		0x08, 0xf6, 0x21, 0x82, 0x74, 0x44, 0xf7, 0x0d,
		0x88, 0x4f, 0x5d, 0x02, 0x40, 0xd2, 0x71, 0x2e,
		0x10, 0xe1, 0x16, 0xe9, 0x19, 0x2a, 0xf3, 0xc9,
		0x1a, 0x7e, 0xc5, 0x76, 0x47, 0xe3, 0x93, 0x40,
		0x57, 0x34, 0x0b, 0x4c, 0xf4, 0x08, 0xd5, 0xa5,
		0x65, 0x92, 0xf8, 0x27, 0x4e, 0xec, 0x53, 0xf0
	][..]);
	let mut ctx = Sha3512::new();
	ctx.input(b"abc");
	ctx.reset();
	let h = ctx.output();
	assert_eq!(&h[..], &[
		0xa6, 0x9f, 0x73, 0xcc, 0xa2, 0x3a, 0x9a, 0xc5,
		0xc8, 0xb5, 0x67, 0xdc, 0x18, 0x5a, 0x75, 0x6e,
		0x97, 0xc9, 0x82, 0x16, 0x4f, 0xe2, 0x58, 0x59,
		0xe0, 0xd1, 0xdc, 0xc1, 0x47, 0x5c, 0x80, 0xa6,
		0x15, 0xb2, 0x12, 0x3a, 0xf1, 0xf5, 0xf9, 0x4c,
		0x11, 0xe3, 0xe9, 0x40, 0x2c, 0x3a, 0xc5, 0x58,
		0xf5, 0x00, 0x19, 0x9d, 0x95, 0xb6, 0xd3, 0xe3,
		0x01, 0x75, 0x85, 0x86, 0x28, 0x1d, 0xcd, 0x26
	][..]);
	let mut ctx = Sha3512::new();
	ctx.input(b"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq");
	let h = ctx.output();
	assert_eq!(&h[..], &[
		0x04, 0xa3, 0x71, 0xe8, 0x4e, 0xcf, 0xb5, 0xb8,
		0xb7, 0x7c, 0xb4, 0x86, 0x10, 0xfc, 0xa8, 0x18,
		0x2d, 0xd4, 0x57, 0xce, 0x6f, 0x32, 0x6a, 0x0f,
		0xd3, 0xd7, 0xec, 0x2f, 0x1e, 0x91, 0x63, 0x6d,
		0xee, 0x69, 0x1f, 0xbe, 0x0c, 0x98, 0x53, 0x02,
		0xba, 0x1b, 0x0d, 0x8d, 0xc7, 0x8c, 0x08, 0x63,
		0x46, 0xb5, 0x33, 0xb4, 0x9c, 0x03, 0x0d, 0x99,
		0xa2, 0x7d, 0xaf, 0x11, 0x39, 0xd6, 0xe7, 0x5e
	][..]);
	let mut ctx = Sha3512::new();
	ctx.input(b"abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmn\
		hijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu");
	let h = ctx.output();
	assert_eq!(&h[..], &[
		0xaf, 0xeb, 0xb2, 0xef, 0x54, 0x2e, 0x65, 0x79,
		0xc5, 0x0c, 0xad, 0x06, 0xd2, 0xe5, 0x78, 0xf9,
		0xf8, 0xdd, 0x68, 0x81, 0xd7, 0xdc, 0x82, 0x4d,
		0x26, 0x36, 0x0f, 0xee, 0xbf, 0x18, 0xa4, 0xfa,
		0x73, 0xe3, 0x26, 0x11, 0x22, 0x94, 0x8e, 0xfc,
		0xfd, 0x49, 0x2e, 0x74, 0xe8, 0x2e, 0x21, 0x89,
		0xed, 0x0f, 0xb4, 0x40, 0xd1, 0x87, 0xf3, 0x82,
		0x27, 0x0c, 0xb4, 0x55, 0xf2, 0x1d, 0xd1, 0x85
	][..]);
}

#[test]
fn shake128_kat()
{
	let mut ctx = Shake128256::new();
	ctx.input(&[0xa3;200]);
	let h = ctx.output();
	assert_eq!(&h[..], &[
		0x13, 0x1A, 0xB8, 0xD2, 0xB5, 0x94, 0x94, 0x6B,
		0x9C, 0x81, 0x33, 0x3F, 0x9B, 0xB6, 0xE0, 0xCE,
		0x75, 0xC3, 0xB9, 0x31, 0x04, 0xFA, 0x34, 0x69,
		0xD3, 0x91, 0x74, 0x57, 0x38, 0x5D, 0xA0, 0x37,
	][..]);
}

#[test]
fn shake256_kat()
{
	let mut ctx = Shake256256::new();
	ctx.input(&[0xa3;200]);
	let h = ctx.output();
	assert_eq!(&h[..], &[
		0xCD, 0x8A, 0x92, 0x0E, 0xD1, 0x41, 0xAA, 0x04,
		0x07, 0xA2, 0x2D, 0x59, 0x28, 0x86, 0x52, 0xE9,
		0xD9, 0xF1, 0xA7, 0xEE, 0x0C, 0x1E, 0x7C, 0x1C,
		0xA6, 0x99, 0x42, 0x4D, 0xA8, 0x4A, 0x90, 0x4D,
	][..]);
	let mut ctx = Shake256384::new();
	ctx.input(&[0xa3;200]);
	let h = ctx.output();
	assert_eq!(&h[..], &[
		0xCD, 0x8A, 0x92, 0x0E, 0xD1, 0x41, 0xAA, 0x04,
		0x07, 0xA2, 0x2D, 0x59, 0x28, 0x86, 0x52, 0xE9,
		0xD9, 0xF1, 0xA7, 0xEE, 0x0C, 0x1E, 0x7C, 0x1C,
		0xA6, 0x99, 0x42, 0x4D, 0xA8, 0x4A, 0x90, 0x4D,
		0x2D, 0x70, 0x0C, 0xAA, 0xE7, 0x39, 0x6E, 0xCE,
		0x96, 0x60, 0x44, 0x40, 0x57, 0x7D, 0xA4, 0xF3,
	][..]);
	let mut ctx = Shake256512::new();
	ctx.input(&[0xa3;200]);
	let h = ctx.output();
	assert_eq!(&h[..], &[
		0xCD, 0x8A, 0x92, 0x0E, 0xD1, 0x41, 0xAA, 0x04,
		0x07, 0xA2, 0x2D, 0x59, 0x28, 0x86, 0x52, 0xE9,
		0xD9, 0xF1, 0xA7, 0xEE, 0x0C, 0x1E, 0x7C, 0x1C,
		0xA6, 0x99, 0x42, 0x4D, 0xA8, 0x4A, 0x90, 0x4D,
		0x2D, 0x70, 0x0C, 0xAA, 0xE7, 0x39, 0x6E, 0xCE,
		0x96, 0x60, 0x44, 0x40, 0x57, 0x7D, 0xA4, 0xF3,
		0xAA, 0x22, 0xAE, 0xB8, 0x85, 0x7F, 0x96, 0x1C,
		0x4C, 0xD8, 0xE0, 0x6F, 0x0A, 0xE6, 0x61, 0x0B,
	][..]);
}

#[test]
fn cshake128_kat_nocontext()
{
	let mut ctx = Shake128::with_context(b"", None);
	ctx.input(&[0xa3;200]);
	let mut h = [0;32];
	ctx.output(&mut h);
	assert_eq!(&h[..], &[
		0x13, 0x1A, 0xB8, 0xD2, 0xB5, 0x94, 0x94, 0x6B,
		0x9C, 0x81, 0x33, 0x3F, 0x9B, 0xB6, 0xE0, 0xCE,
		0x75, 0xC3, 0xB9, 0x31, 0x04, 0xFA, 0x34, 0x69,
		0xD3, 0x91, 0x74, 0x57, 0x38, 0x5D, 0xA0, 0x37,
	][..]);
}

#[test]
fn cshake256_kat_nocontext()
{
	let mut ctx = Shake256::with_context(b"", None);
	ctx.input(&[0xa3;200]);
	let mut h = [0;64];
	ctx.output(&mut h);
	assert_eq!(&h[..], &[
		0xCD, 0x8A, 0x92, 0x0E, 0xD1, 0x41, 0xAA, 0x04,
		0x07, 0xA2, 0x2D, 0x59, 0x28, 0x86, 0x52, 0xE9,
		0xD9, 0xF1, 0xA7, 0xEE, 0x0C, 0x1E, 0x7C, 0x1C,
		0xA6, 0x99, 0x42, 0x4D, 0xA8, 0x4A, 0x90, 0x4D,
		0x2D, 0x70, 0x0C, 0xAA, 0xE7, 0x39, 0x6E, 0xCE,
		0x96, 0x60, 0x44, 0x40, 0x57, 0x7D, 0xA4, 0xF3,
		0xAA, 0x22, 0xAE, 0xB8, 0x85, 0x7F, 0x96, 0x1C,
		0x4C, 0xD8, 0xE0, 0x6F, 0x0A, 0xE6, 0x61, 0x0B,
	][..]);
}

fn test_leftpad(pfx: &[u8], val: u64, encoding: &[u8])
{
	let mut out1 = [0;64];
	let mut out2 = [0;64];
	let mut ctx1 = Shake256::new();
	let mut ctx2 = Shake256::new();
	ctx1.input(pfx);
	ctx2.input(pfx);
	ctx1.0.leftpad(val);
	ctx2.input(encoding);
	ctx1.output(&mut out1);
	ctx2.output(&mut out2);
	assert_eq!(&out1[..], &out2[..]);
}

fn test_leftpad_cases(pfx: &[u8])
{
	test_leftpad(pfx, 0x0000000000000000, &[1,0]);
	test_leftpad(pfx, 0x0000000000000001, &[1,1]);
	test_leftpad(pfx, 0x0000000000000011, &[1,0x11]);
	test_leftpad(pfx, 0x00000000000000FF, &[1,255]);
	test_leftpad(pfx, 0x0000000000000100, &[2,1,0]);
	test_leftpad(pfx, 0x0000000000002211, &[2,0x22,0x11]);
	test_leftpad(pfx, 0x000000000000FFFF, &[2,255,255]);
	test_leftpad(pfx, 0x0000000000010000, &[3,1,0,0]);
	test_leftpad(pfx, 0x0000000000332211, &[3,0x33,0x22,0x11]);
	test_leftpad(pfx, 0x0000000000FFFFFF, &[3,255,255,255]);
	test_leftpad(pfx, 0x0000000001000000, &[4,1,0,0,0]);
	test_leftpad(pfx, 0x0000000044332211, &[4,0x44,0x33,0x22,0x11]);
	test_leftpad(pfx, 0x00000000FFFFFFFF, &[4,255,255,255,255]);
	test_leftpad(pfx, 0x0000000100000000, &[5,1,0,0,0,0]);
	test_leftpad(pfx, 0x0000005544332211, &[5,0x55,0x44,0x33,0x22,0x11]);
	test_leftpad(pfx, 0x000000FFFFFFFFFF, &[5,255,255,255,255,255]);
	test_leftpad(pfx, 0x0000010000000000, &[6,1,0,0,0,0,0]);
	test_leftpad(pfx, 0x0000665544332211, &[6,0x66,0x55,0x44,0x33,0x22,0x11]);
	test_leftpad(pfx, 0x0000FFFFFFFFFFFF, &[6,255,255,255,255,255,255]);
	test_leftpad(pfx, 0x0001000000000000, &[7,1,0,0,0,0,0,0]);
	test_leftpad(pfx, 0x0077665544332211, &[7,0x77,0x66,0x55,0x44,0x33,0x22,0x11]);
	test_leftpad(pfx, 0x00FFFFFFFFFFFFFF, &[7,255,255,255,255,255,255,255]);
	test_leftpad(pfx, 0x0100000000000000, &[8,1,0,0,0,0,0,0,0]);
	test_leftpad(pfx, 0x8877665544332211, &[8,0x88,0x77,0x66,0x55,0x44,0x33,0x22,0x11]);
	test_leftpad(pfx, 0xFFFFFFFFFFFFFFFF, &[8,255,255,255,255,255,255,255,255]);
}


#[test]
fn leftpad()
{
	let buf = [42;136];
	//All possible word alignments.
	for i in 40..48 { test_leftpad_cases(&buf[..i]); }
	//All possible word alignments in block split.
	for i in 128..136 { test_leftpad_cases(&buf[..i]); }
}

#[test]
fn rightpad_fail()
{
	let pfx = [42;40];
	let mut out1 = [0;64];
	let mut out2 = [0;64];
	let mut ctx1 = Shake256::new();
	let mut ctx2 = Shake256::new();
	ctx1.input(&pfx);
	ctx2.input(&pfx);
	ctx1.0.rightpad(0x0001000000000000);
	ctx2.input(&[1,0,0,0,0,0,0,7]);
	ctx1.output(&mut out1);
	ctx2.output(&mut out2);
	assert_eq!(&out1[..], &out2[..]);
}

fn test_rightpad(pfx: &[u8], val: u64, encoding: &[u8])
{
	let mut out1 = [0;64];
	let mut out2 = [0;64];
	let mut ctx1 = Shake256::new();
	let mut ctx2 = Shake256::new();
	ctx1.input(pfx);
	ctx2.input(pfx);
	ctx1.0.rightpad(val);
	ctx2.input(encoding);
	ctx1.output(&mut out1);
	ctx2.output(&mut out2);
	println!("pfx={pfx:?} encoding={encoding:?}");
	assert_eq!(&out1[..], &out2[..]);
}

fn test_rightpad_cases(pfx: &[u8])
{
	test_rightpad(pfx, 0x0000000000000000, &[0,1]);
	test_rightpad(pfx, 0x0000000000000001, &[1,1]);
	test_rightpad(pfx, 0x0000000000000011, &[0x11,1]);
	test_rightpad(pfx, 0x00000000000000FF, &[255,1]);
	test_rightpad(pfx, 0x0000000000000100, &[1,0,2]);
	test_rightpad(pfx, 0x0000000000002211, &[0x22,0x11,2]);
	test_rightpad(pfx, 0x000000000000FFFF, &[255,255,2]);
	test_rightpad(pfx, 0x0000000000010000, &[1,0,0,3]);
	test_rightpad(pfx, 0x0000000000332211, &[0x33,0x22,0x11,3]);
	test_rightpad(pfx, 0x0000000000FFFFFF, &[255,255,255,3]);
	test_rightpad(pfx, 0x0000000001000000, &[1,0,0,0,4]);
	test_rightpad(pfx, 0x0000000044332211, &[0x44,0x33,0x22,0x11,4]);
	test_rightpad(pfx, 0x00000000FFFFFFFF, &[255,255,255,255,4]);
	test_rightpad(pfx, 0x0000000100000000, &[1,0,0,0,0,5]);
	test_rightpad(pfx, 0x0000005544332211, &[0x55,0x44,0x33,0x22,0x11,5]);
	test_rightpad(pfx, 0x000000FFFFFFFFFF, &[255,255,255,255,255,5]);
	test_rightpad(pfx, 0x0000010000000000, &[1,0,0,0,0,0,6]);
	test_rightpad(pfx, 0x0000665544332211, &[0x66,0x55,0x44,0x33,0x22,0x11,6]);
	test_rightpad(pfx, 0x0000FFFFFFFFFFFF, &[255,255,255,255,255,255,6]);
	test_rightpad(pfx, 0x0001000000000000, &[1,0,0,0,0,0,0,7]);
	test_rightpad(pfx, 0x0077665544332211, &[0x77,0x66,0x55,0x44,0x33,0x22,0x11,7]);
	test_rightpad(pfx, 0x00FFFFFFFFFFFFFF, &[255,255,255,255,255,255,255,7]);
	test_rightpad(pfx, 0x0100000000000000, &[1,0,0,0,0,0,0,0,8]);
	test_rightpad(pfx, 0x8877665544332211, &[0x88,0x77,0x66,0x55,0x44,0x33,0x22,0x11,8]);
	test_rightpad(pfx, 0xFFFFFFFFFFFFFFFF, &[255,255,255,255,255,255,255,255,8]);
}

#[test]
fn rightpad()
{
	let buf = [42;136];
	//All possible word alignments.
	for i in 40..48 { test_rightpad_cases(&buf[..i]); }
	//All possible word alignments in block split.
	for i in 128..136 { test_rightpad_cases(&buf[..i]); }
}

#[test]
fn shake128()
{
	let refhash = b"\x1a\x96\x18\x2b\x50\xfb\x8c\x7e\x74\xe0\xa7\x07\x78\x8f\x55\xe9\x82\x09\xb8\xd9\x1f\xad\
		\xe8\xf3\x2f\x8d\xd5\xcf\xf7\xbf\x21\xf5";
	let mut hash = [0;32];
	let input = b"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq";
	let mut ctx = Shake128::new();
	ctx.input(input);
	ctx.output(&mut hash);
	assert_eq!(&hash[..], &refhash[..]);
}

#[test]
fn cshake128_1()
{
	let refhash = b"\xc1\xc3\x69\x25\xb6\x40\x9a\x04\xf1\xb5\x04\xfc\xbc\xa9\xd8\x2b\x40\x17\x27\x7c\xb5\xed\
		\x2b\x20\x65\xfc\x1d\x38\x14\xd5\xaa\xf5";
	let mut hash = [0;32];
	let mut input = [0;4];
	for i in 0..input.len() { input[i] = i as u8; }
	let mut ctx = Shake128::with_context(b"Email Signature", None);
	ctx.input(&input);
	ctx.output(&mut hash);
	assert_eq!(&hash[..], &refhash[..]);
}

#[test]
fn cshake128_2()
{
	let refhash = b"\xc5\x22\x1d\x50\xe4\xf8\x22\xd9\x6a\x2e\x88\x81\xa9\x61\x42\x0f\x29\x4b\x7b\x24\xfe\x3d\
		\x20\x94\xba\xed\x2c\x65\x24\xcc\x16\x6b";
	let mut hash = [0;32];
	let mut input = [0;0xc8];
	for i in 0..input.len() { input[i] = i as u8; }
	let mut ctx = Shake128::with_context(b"Email Signature", None);
	ctx.input(&input);
	ctx.output(&mut hash);
	assert_eq!(&hash[..], &refhash[..]);
}

#[test]
fn cshake256_1()
{
	let refhash = b"\xD0\x08\x82\x8E\x2B\x80\xAC\x9D\x22\x18\xFF\xEE\x1D\x07\x0C\x48\xB8\xE4\xC8\x7B\xFF\x32\
		\xC9\x69\x9D\x5B\x68\x96\xEE\xE0\xED\xD1\x64\x02\x0E\x2B\xE0\x56\x08\x58\xD9\xC0\x0C\x03\x7E\x34\
		\xA9\x69\x37\xC5\x61\xA7\x4C\x41\x2B\xB4\xC7\x46\x46\x95\x27\x28\x1C\x8C";
	let mut hash = [0;64];
	let mut input = [0;4];
	for i in 0..input.len() { input[i] = i as u8; }
	let mut ctx = Shake256::with_context(b"Email Signature", None);
	ctx.input(&input);
	ctx.output(&mut hash);
	assert_eq!(&hash[..], &refhash[..]);
}

#[test]
fn cshake256_2()
{
	let refhash = b"\x07\xDC\x27\xB1\x1E\x51\xFB\xAC\x75\xBC\x7B\x3C\x1D\x98\x3E\x8B\x4B\x85\xFB\x1D\xEF\xAF\
		\x21\x89\x12\xAC\x86\x43\x02\x73\x09\x17\x27\xF4\x2B\x17\xED\x1D\xF6\x3E\x8E\xC1\x18\xF0\x4B\x23\
		\x63\x3C\x1D\xFB\x15\x74\xC8\xFB\x55\xCB\x45\xDA\x8E\x25\xAF\xB0\x92\xBB";
	let mut hash = [0;64];
	let mut input = [0;0xc8];
	for i in 0..input.len() { input[i] = i as u8; }
	let mut ctx = Shake256::with_context(b"Email Signature", None);
	ctx.input(&input);
	ctx.output(&mut hash);
	assert_eq!(&hash[..], &refhash[..]);
}


#[test]
fn kmac128_1()
{
	let refhash = b"\xE5\x78\x0B\x0D\x3E\xA6\xF7\xD3\xA4\x29\xC5\x70\x6A\xA4\x3A\x00\xFA\xDB\xD7\xD4\x96\x28\
		\x83\x9E\x31\x87\x24\x3F\x45\x6E\xE1\x4E";
	let mut hash = [0;32];
	let mut input = [0;4];
	let mut key = [0;32];
	for i in 0..input.len() { input[i] = i as u8; }
	for i in 0..key.len() { key[i] = i as u8 + 0x40; }
	let mut ctx = Shake128::with_context(b"", Some(&key));
	ctx.input(&input);
	ctx.output(&mut hash);
	assert_eq!(&hash[..], &refhash[..]);
}

#[test]
fn kmac128_2()
{
	let refhash = b"\x3B\x1F\xBA\x96\x3C\xD8\xB0\xB5\x9E\x8C\x1A\x6D\x71\x88\x8B\x71\x43\x65\x1A\xF8\xBA\x0A\
		\x70\x70\xC0\x97\x9E\x28\x11\x32\x4A\xA5";
	let mut hash = [0;32];
	let mut input = [0;4];
	let mut key = [0;32];
	for i in 0..input.len() { input[i] = i as u8; }
	for i in 0..key.len() { key[i] = i as u8 + 0x40; }
	let mut ctx = Shake128::with_context(b"My Tagged Application", Some(&key));
	ctx.input(&input);
	ctx.output(&mut hash);
	assert_eq!(&hash[..], &refhash[..]);
}

#[test]
fn kmac128_3()
{
	let refhash = b"\x1F\x5B\x4E\x6C\xCA\x02\x20\x9E\x0D\xCB\x5C\xA6\x35\xB8\x9A\x15\xE2\x71\xEC\xC7\x60\x07\
		\x1D\xFD\x80\x5F\xAA\x38\xF9\x72\x92\x30";
	let mut hash = [0;32];
	let mut input = [0;0xc8];
	let mut key = [0;32];
	for i in 0..input.len() { input[i] = i as u8; }
	for i in 0..key.len() { key[i] = i as u8 + 0x40; }
	let mut ctx = Shake128::with_context(b"My Tagged Application", Some(&key));
	ctx.input(&input);
	ctx.output(&mut hash);
	assert_eq!(&hash[..], &refhash[..]);
}

#[test]
fn kmac256_1()
{
	let refhash = b"\x20\xC5\x70\xC3\x13\x46\xF7\x03\xC9\xAC\x36\xC6\x1C\x03\xCB\x64\xC3\x97\x0D\x0C\xFC\x78\
		\x7E\x9B\x79\x59\x9D\x27\x3A\x68\xD2\xF7\xF6\x9D\x4C\xC3\xDE\x9D\x10\x4A\x35\x16\x89\xF2\x7C\xF6\
		\xF5\x95\x1F\x01\x03\xF3\x3F\x4F\x24\x87\x10\x24\xD9\xC2\x77\x73\xA8\xDD";
	let mut hash = [0;64];
	let mut input = [0;4];
	let mut key = [0;32];
	for i in 0..input.len() { input[i] = i as u8; }
	for i in 0..key.len() { key[i] = i as u8 + 0x40; }
	let mut ctx = Shake256::with_context(b"My Tagged Application", Some(&key));
	ctx.input(&input);
	ctx.output(&mut hash);
	assert_eq!(&hash[..], &refhash[..]);
}

#[test]
fn kmac256_2()
{
	let refhash = b"\x75\x35\x8C\xF3\x9E\x41\x49\x4E\x94\x97\x07\x92\x7C\xEE\x0A\xF2\x0A\x3F\xF5\x53\x90\x4C\
		\x86\xB0\x8F\x21\xCC\x41\x4B\xCF\xD6\x91\x58\x9D\x27\xCF\x5E\x15\x36\x9C\xBB\xFF\x8B\x9A\x4C\x2E\
		\xB1\x78\x00\x85\x5D\x02\x35\xFF\x63\x5D\xA8\x25\x33\xEC\x6B\x75\x9B\x69";
	let mut hash = [0;64];
	let mut input = [0;0xc8];
	let mut key = [0;32];
	for i in 0..input.len() { input[i] = i as u8; }
	for i in 0..key.len() { key[i] = i as u8 + 0x40; }
	let mut ctx = Shake256::with_context(b"", Some(&key));
	ctx.input(&input);
	ctx.output(&mut hash);
	assert_eq!(&hash[..], &refhash[..]);
}

#[test]
fn kmac256_3()
{
	let refhash = b"\xB5\x86\x18\xF7\x1F\x92\xE1\xD5\x6C\x1B\x8C\x55\xDD\xD7\xCD\x18\x8B\x97\xB4\xCA\x4D\x99\
		\x83\x1E\xB2\x69\x9A\x83\x7D\xA2\xE4\xD9\x70\xFB\xAC\xFD\xE5\x00\x33\xAE\xA5\x85\xF1\xA2\x70\x85\
		\x10\xC3\x2D\x07\x88\x08\x01\xBD\x18\x28\x98\xFE\x47\x68\x76\xFC\x89\x65";
	let mut hash = [0;64];
	let mut input = [0;0xc8];
	let mut key = [0;32];
	for i in 0..input.len() { input[i] = i as u8; }
	for i in 0..key.len() { key[i] = i as u8 + 0x40; }
	let mut ctx = Shake256::with_context(b"My Tagged Application", Some(&key));
	ctx.input(&input);
	ctx.output(&mut hash);
	assert_eq!(&hash[..], &refhash[..]);
}

fn check_hash(buf: &[u8], correct: &[u8;64], splits: &[usize])
{
	let mut hash = [0;64];
	let mut r = Shake256::new();
	let mut last = 0;
	for &split in splits.iter() {
		r.input(&buf[last..split]);
		last = split;
	}
	r.input(&buf[last..]);
	r.output(&mut hash);
	assert_eq!(&hash[..], &correct[..]);
}

#[test]
fn input_invariants()
{
	let mut buf = [0;8192];
	Shake256::with_context(b"foo", Some(b"bar")).output(&mut buf);
	let mut refhash = [0;64];
	let mut r = Shake256::new();
	for i in 0..buf.len() { r.input(&buf[i..i+1]); }
	r.output(&mut refhash);

	check_hash(&buf, &refhash, &[]);
	for i in 1..513 {
		//Block-aligned case.
		check_hash(&buf, &refhash, &[i]);
		//Word-aligned case.
		check_hash(&buf, &refhash, &[64,i+64]);
		//unaligned case.
		check_hash(&buf, &refhash, &[67,i+67]);
	}
}

#[test]
fn shake_multiblock()
{
	let mut out = [0;136*3];
	let mut rout = [0;136*3];
	let mut int = [0;25];
	Shake256::new().output(&mut out);
	int[0] ^=  0x000000000000001f;
	int[16] ^= 0x8000000000000000;
	raw_keccak_permutation(&mut int);
	for i in 0..17 {
		let rout = &mut rout[136*0..136*1];
		(&mut rout[8*i..8*i+8]).copy_from_slice(&int[i].to_le_bytes());
	}
	raw_keccak_permutation(&mut int);
	for i in 0..17 {
		let rout = &mut rout[136*1..136*2];
		(&mut rout[8*i..8*i+8]).copy_from_slice(&int[i].to_le_bytes());
	}
	raw_keccak_permutation(&mut int);
	for i in 0..17 {
		let rout = &mut rout[136*2..136*3];
		(&mut rout[8*i..8*i+8]).copy_from_slice(&int[i].to_le_bytes());
	}
	assert_eq!(&out[..], &rout[..]);
}

#[test]
fn kmac_input()
{
	let mut ctx = Shake256::with_context(b"foo", Some(b"bar"));
	let ctx_i = ctx.0.state;
	assert_eq!(ctx.0.fill, 0);
	ctx.input(b"zotqux");
	assert_eq!(ctx.0.fill, 6);
	let ctx_i2 = ctx.0.state;
	let mut kout = [0;136];
	ctx.output(&mut kout);

	let mut int = [0;25];
	//First block contains N and S.
	int[0] ^=  0x0043414d4b20018801;
	int[1] ^=  0x000000006f6f661801;
	raw_keccak_permutation(&mut int);
	//Second block contains K.
	int[0] ^=  0x0072616218018801;
	raw_keccak_permutation(&mut int);
	let int_i = int;
	//Subsequent blocks contain data, followed by output length. Set last block flag.
	int[0] ^=  0x0000787571746f7a;
	let int_i2 = int;
	int[0] ^=  0x4004000000000000;
	int[1] ^=  0x0000000000000402;
	int[16] ^= 0x8000000000000000;
	raw_keccak_permutation(&mut int);
	let mut rout = [0;136];
	for i in 0..17 {
		(&mut rout[8*i..8*i+8]).copy_from_slice(&int[i].to_le_bytes());
	}

	assert_eq!(ctx_i, int_i);
	assert_eq!(ctx_i2, int_i2);
	assert_eq!(&kout[..], &rout[..]);
}

fn make_kmac_cshake(key: &[u8], msg1: &[u8], blklen: usize) -> Vec<u8>
{
	let mut msg2 = Vec::new();
	msg2.extend_from_slice(&[1,blklen as u8]);		//n bytes per block.
	msg2.extend_from_slice(&[1,key.len() as u8 * 8]);	//Key length in bits.
	msg2.extend_from_slice(key);				//Key itself.
	while msg2.len() < blklen { msg2.push(0); }		//Zero pad.
	msg2.extend_from_slice(msg1);
	msg2.extend_from_slice(&[3,8,2]);			//776 bits of output.
	msg2
}

#[test]
fn kmac_from_cshake256()
{
	let msg1 = b"quxzot";
	let key = b"bar";
	let ctx = b"foo";
	let mut out1 = [0;97];
	let mut out2 = [0;97];
	let mut ctx1 = Shake256::with_context(ctx, Some(key));
	let mut ctx2 = Shake256::test_cshake(b"KMAC", ctx);
	ctx1.input(msg1);
	ctx2.input(&make_kmac_cshake(key, msg1, 136));
	ctx1.output(&mut out1);
	ctx2.output(&mut out2);

	//These two should be the same.
	assert_eq!(&out1[..], &out2[..]);
}

#[test]
fn kmac_from_cshake128()
{
	let msg1 = b"quxzot";
	let key = b"bar";
	let ctx = b"foo";
	let mut out1 = [0;97];
	let mut out2 = [0;97];
	let mut ctx1 = Shake128::with_context(ctx, Some(key));
	let mut ctx2 = Shake128::test_cshake(b"KMAC", ctx);
	ctx1.input(msg1);
	ctx2.input(&make_kmac_cshake(key, msg1, 168));
	ctx1.output(&mut out1);
	ctx2.output(&mut out2);

	//These two should be the same.
	assert_eq!(&out1[..], &out2[..]);
}

#[test]
fn kmac_from_cshake160()
{
	let msg1 = b"quxzot";
	let key = b"bar";
	let ctx = b"foo";
	let mut out1 = [0;97];
	let mut out2 = [0;97];
	let mut ctx1 = Shake160::with_context(ctx, Some(key));
	let mut ctx2 = Shake160::test_cshake(b"KMAC", ctx);
	ctx1.input(msg1);
	ctx2.input(&make_kmac_cshake(key, msg1, 160));
	ctx1.output(&mut out1);
	ctx2.output(&mut out2);

	//These two should be the same.
	assert_eq!(&out1[..], &out2[..]);
}

#[test]
fn kmac_from_cshake192()
{
	let msg1 = b"quxzot";
	let key = b"bar";
	let ctx = b"foo";
	let mut out1 = [0;97];
	let mut out2 = [0;97];
	let mut ctx1 = Shake192::with_context(ctx, Some(key));
	let mut ctx2 = Shake192::test_cshake(b"KMAC", ctx);
	ctx1.input(msg1);
	ctx2.input(&make_kmac_cshake(key, msg1, 152));
	ctx1.output(&mut out1);
	ctx2.output(&mut out2);

	//These two should be the same.
	assert_eq!(&out1[..], &out2[..]);
}

#[test]
fn kmac_from_cshake224()
{
	let msg1 = b"quxzot";
	let key = b"bar";
	let ctx = b"foo";
	let mut out1 = [0;97];
	let mut out2 = [0;97];
	let mut ctx1 = Shake224::with_context(ctx, Some(key));
	let mut ctx2 = Shake224::test_cshake(b"KMAC", ctx);
	ctx1.input(msg1);
	ctx2.input(&make_kmac_cshake(key, msg1, 144));
	ctx1.output(&mut out1);
	ctx2.output(&mut out2);

	//These two should be the same.
	assert_eq!(&out1[..], &out2[..]);
}

#[test]
fn direct_out_xof_match()
{
	let mut ctx1 = Shake256::with_context(b"foo", None);
	let mut ctx2 = Shake256::with_context(b"foo", None);
	ctx1.input(&[0xa3;200]);
	ctx2.input(&[0xa3;200]);
	let mut h1 = [0;16384];
	let mut h2 = [0;16384];
	ctx1.output(&mut h1);
	let mut ctx2 = ctx2.as_stream();
	let breaks = [0,136,137,145,152,160,288,3*136,7*136,16384];
	for x in breaks.windows(2) {
		let r = &mut h2[x[0]..x[1]];
		ctx2.read(r);
	}
	assert_eq!(&h1[..], &h2[..]);
}


fn check_hash_x(buf: &[u8], correct: &[u8;64], splits: &[usize])
{
	let mut hash = [0;64];
	let mut r = Shake256::new();
	let mut last = 0;
	for &split in splits.iter() {
		r.input(&buf[last..split]);
		last = split;
	}
	r.input(&buf[last..]);
	r.output(&mut hash);
	assert_eq!(&hash[..], &correct[..]);
}

#[test]
fn test_sha3_do_post()
{
	let mut fp = File::open("/dev/urandom").expect("Failed to open /dev/urandom");
	let mut buf = [0;8192];
	let mut i = 0;
	while i < buf.len() {
		let r = fp.read(&mut buf[i..]).expect("Failed to read /dev/urandom");
		i += r;
	}
	let mut refhash = [0;64];
	let mut r = Shake256::new();
	for i in 0..buf.len() { r.input(&buf[i..i+1]); }
	r.output(&mut refhash);

	check_hash(&buf, &refhash, &[]);

	//Do check of all the cases.
	check_hash_x(&buf, &refhash, &[3,7]);
	check_hash_x(&buf, &refhash, &[3,8]);
	check_hash_x(&buf, &refhash, &[3,10]);
	check_hash_x(&buf, &refhash, &[3,11]);
	check_hash_x(&buf, &refhash, &[3,12]);
	check_hash_x(&buf, &refhash, &[3,16]);
	check_hash_x(&buf, &refhash, &[3,20]);
	check_hash_x(&buf, &refhash, &[3,136]);
	check_hash_x(&buf, &refhash, &[3,139]);
	check_hash_x(&buf, &refhash, &[3,144]);
	check_hash_x(&buf, &refhash, &[3,147]);
	check_hash_x(&buf, &refhash, &[3,160]);
	check_hash_x(&buf, &refhash, &[3,165]);
	check_hash_x(&buf, &refhash, &[8,136]);
	check_hash_x(&buf, &refhash, &[8,144]);
	check_hash_x(&buf, &refhash, &[8,160]);
	check_hash_x(&buf, &refhash, &[8,165]);

	for i in 1..513 {
		//Block-aligned case.
		check_hash_x(&buf, &refhash, &[i]);
		//Word-aligned case.
		check_hash_x(&buf, &refhash, &[64,i+64]);
		//unaligned case.
		check_hash_x(&buf, &refhash, &[67,i+67]);
	}
	println!("POST OK");
}

#[test]
fn test_sha3internal_size()
{
	assert!(core::mem::size_of::<crate::Sha3Internal>() == 208);
}
