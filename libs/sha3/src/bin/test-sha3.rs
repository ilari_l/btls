use btls_aux_sha3::raw_keccak_permutation;
use btls_aux_sha3::Shake256;
use std::time::Instant;
use std::fs::File;
use std::io::Read;

fn check_hash(buf: &[u8], correct: &[u8;64], splits: &[usize])
{
	let mut hash = [0;64];
	let mut r = Shake256::new();
	let mut last = 0;
	for &split in splits.iter() {
		r.input(&buf[last..split]);
		last = split;
	}
	r.input(&buf[last..]);
	r.output(&mut hash);
	assert_eq!(&hash[..], &correct[..]);
}

fn do_post()
{
	let mut fp = File::open("/dev/urandom").expect("Failed to open /dev/urandom");
	let mut buf = [0;8192];
	let mut i = 0;
	while i < buf.len() {
		let r = fp.read(&mut buf[i..]).expect("Failed to read /dev/urandom");
		i += r;
	}
	let mut refhash = [0;64];
	let mut r = Shake256::new();
	for i in 0..buf.len() { r.input(&buf[i..i+1]); }
	r.output(&mut refhash);

	check_hash(&buf, &refhash, &[]);

	//Do check of all the cases.
	check_hash(&buf, &refhash, &[3,7]);
	check_hash(&buf, &refhash, &[3,8]);
	check_hash(&buf, &refhash, &[3,10]);
	check_hash(&buf, &refhash, &[3,11]);
	check_hash(&buf, &refhash, &[3,12]);
	check_hash(&buf, &refhash, &[3,16]);
	check_hash(&buf, &refhash, &[3,20]);
	check_hash(&buf, &refhash, &[3,136]);
	check_hash(&buf, &refhash, &[3,139]);
	check_hash(&buf, &refhash, &[3,144]);
	check_hash(&buf, &refhash, &[3,147]);
	check_hash(&buf, &refhash, &[3,160]);
	check_hash(&buf, &refhash, &[3,165]);
	check_hash(&buf, &refhash, &[8,136]);
	check_hash(&buf, &refhash, &[8,144]);
	check_hash(&buf, &refhash, &[8,160]);
	check_hash(&buf, &refhash, &[8,165]);

	for i in 1..513 {
		//Block-aligned case.
		check_hash(&buf, &refhash, &[i]);
		//Word-aligned case.
		check_hash(&buf, &refhash, &[64,i+64]);
		//unaligned case.
		check_hash(&buf, &refhash, &[67,i+67]);
	}
	println!("POST OK");
}

fn do_rounds(f: &mut impl FnMut()) -> (f64, usize)
{
	let ts = Instant::now();
	let mut now = ts;
	let mut done = 0;
	while now.duration_since(ts).as_secs() < 5 {
		for _ in 0..1000 { f(); }
		done += 1000;
		now = Instant::now();
	}
	let dt = now.duration_since(ts);
	(dt.as_nanos() as f64 / 1_000_000_000.0, done)
}

fn mbps(scale: usize, n: usize, dt: f64) -> f64
{
	scale as f64 * n as f64 / dt / 1048576.0
}

fn diff_speed(speed: f64, base: f64) -> f64
{
	1.0 / (1.0 / speed - 1.0 / base)
}

fn main()
{
	do_post();

	let mut state = [0;25];
	let (dt, n) = do_rounds(&mut ||raw_keccak_permutation(&mut state));
	let bspeed = mbps(136, n, dt);
	println!("Raw keccak: {bspeed} MB/s");

	let mut state = Shake256::new();
	let block = [42;8];
	let (dt, n) = do_rounds(&mut ||state.input(&block));
	let speed = mbps(8, n, dt);
	println!("8 bytes (aligned): {speed} MB/s ({ds} MB/s)", ds=diff_speed(speed, bspeed));

	let mut state = Shake256::new();
	let block = [42;8];
	state.input(&block[..4]);
	let (dt, n) = do_rounds(&mut ||state.input(&block));
	let speed = mbps(8, n, dt);
	println!("8 bytes (unaligned): {speed} MB/s ({ds} MB/s)", ds=diff_speed(speed, bspeed));

	let mut state = Shake256::new();
	let block = [42;136];
	let (dt, n) = do_rounds(&mut ||state.input(&block));
	let speed = mbps(136, n, dt);
	println!("136 bytes (aligned): {speed} MB/s ({ds} MB/s)", ds=diff_speed(speed, bspeed));

	let mut state = Shake256::new();
	let block = [42;136];
	state.input(&block[..8]);
	let (dt, n) = do_rounds(&mut ||state.input(&block));
	let speed = mbps(136, n, dt);
	println!("136 bytes (word-aligned): {speed} MB/s ({ds} MB/s)", ds=diff_speed(speed, bspeed));

	let mut state = Shake256::new();
	let block = [42;136];
	state.input(&block[..4]);
	let (dt, n) = do_rounds(&mut ||state.input(&block));
	let speed = mbps(136, n, dt);
	println!("136 bytes (unaligned): {speed} MB/s ({ds} MB/s)", ds=diff_speed(speed, bspeed));
}
