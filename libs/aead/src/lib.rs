//!AEAD encryption/decryption.
//!
//!This crate contains routines for encrypting and decrypting data using various AEAD (Authenticated Encryption
//!with Associated Data) encryption methods.
//!
//!# DANGER:
//!
//!Note, that the algorithms used are not necressarily misuse-resistant, therefore the algorithms are
//!**not suitable** for encrypting data at rest (as opposed to data in motion).
#![deny(missing_docs)]
#![no_std]
#![warn(unsafe_op_in_unsafe_fn)]
#[cfg(test)] #[macro_use] extern crate std;


pub use btls_aux_aead_base::AeadError;
pub use btls_aux_aead_base::InputOutputBuffer;
use btls_aux_aead_base::LowLevelAeadContext;
use btls_aux_fail::fail_if;
use btls_aux_set::raw_bitset_from_u64;
use btls_aux_set::raw_bitset_hamming_weight;
use btls_aux_set::raw_bitset_not_empty;
use btls_aux_set::Set;
use btls_aux_set::SetItem;
use btls_aux_set::SetStorageTrait;
use btls_aux_set2::SetItem as SetItem2;
use btls_aux_set2::SetOf;
use btls_aux_memory::AliasReadSlice;
use btls_aux_memory::AliasReadWriteSlice;
use btls_aux_memory::AliasWriteSlice;
use btls_aux_memory::BackedVector;
use btls_aux_memory::bound_buffer_size;
use btls_aux_memory::NonNullMut;
use btls_aux_memory::WriteOnlyArray;
use btls_aux_tls_iana::CipherSuite;
use btls_aux_tls_iana::CipherSuiteCipher;
use core::cmp::min;


const BIT_AES_128_GCM: u32 = 0;
const BIT_AES_256_GCM: u32 = 1;
const BIT_CHACHA20_POLY1305: u32 = 2;
const BIT_AES_192_GCM: u32 = 3;
const ALGO_MASK: u64 = 15;

///Is hardware-accelerated AES supported?
///
/// # Return value:
///
/// * `true`, if hardware AES-GCM is supported.
/// * `false`, if hardware AES-GCM is not supported, a software implementation will be used.
///
/// # Notes:
///
///It makes sense to prefer Chacha20 to AES if hardware-accelerated AES is not supported (this function returns
///`false`) as it is much faster in this case. On other other hand, it makes sense to prefer AES if
///hardware-accelerated AES is available (this function returns `true`), as hardware AES is faster than softare
///Chacha20.
pub fn hw_aes_supported() -> bool { btls_aux_hwaesgcm::hw_aes_supported() }

///The largest possible encryption key size in bytes.
///
///If one wants to store raw key for any possible supported algorithm, then it is sufficient to have a buffer that
///can store up to `MAX_KEY` bytes, plus length field for the buffer.
pub const MAX_KEY: usize = 32;
///The largest possible encryption nonce size in bytes.
///
///If one wants to store nonce for any possible supported algorithm, then it is sufficient to have a buffer that
///can store up to `MAX_NONCE` bytes, plus length field for the buffer.
pub const MAX_NONCE: usize = 12;
///The largest possible message expansion in bytes.
///
///If one wants to store the ciphertext of any supported algorithm, then it is sufficient to have output buffer that
///is length of plaintext plus `MAX_EXPANSION` bytes.
pub const MAX_EXPANSION: usize = 16;

const __ALGO_CONST_MASK: u64 = 1 << BIT_AES_128_GCM | 1 << BIT_AES_256_GCM | 1 << BIT_CHACHA20_POLY1305;
///Algorithm mask for all default-enabled algorithms.
///
///Note that the actual constants are not directly exposed. Use [`ProtectorType::algo_const()`] for determining
///the constant values for various algorithms.
///
///[`ProtectorType::algo_const()`]: enum.ProtectorType.html#method.algo_const
pub const ALGO_CONST_MASK: u64 = __ALGO_CONST_MASK;

const __ALGO_CONST_MASK_NSA: u64 = 1 << BIT_AES_256_GCM;
///Algorithm mask for all algorithms enabled in NSA mode.
///
///This is similar to [`ALGO_CONST_MASK`], but pertains to the NSA mode instead of normal mode.
///
///[`ALGO_CONST_MASK`]: constant.ALGO_CONST_MASK.html
pub const ALGO_CONST_MASK_NSA: u64 = __ALGO_CONST_MASK_NSA;

impl SetItem for ProtectorType
{
	fn __sign_bit(&self) -> usize { self.algo_bit() as usize }
}

type _ProtectorTypeSet = u8;

#[allow(missing_docs)]
#[deprecated(since="2.8.1", note="Use SetOf<ProtectorType> instead")]
#[derive(Copy,Clone)]
pub struct ProtectorTypeSet(_ProtectorTypeSet);

#[allow(deprecated,missing_docs)]
impl ProtectorTypeSet
{
	pub fn nsa_mode() -> ProtectorTypeSet
	{
		ProtectorTypeSet(raw_bitset_from_u64(__ALGO_CONST_MASK_NSA))
	}
	pub fn from_mask(mask: u64) -> ProtectorTypeSet
	{
		//The optimization of count() and has_elements() using raw_bitset_hamming_weight() and
		//raw_bitset_not_empty() relies on internal set representation never having any unused bits
		//set, as these would cause ghost elements. Therefore mask the passed in mask by mask of used bits
		//in order to clear any unused bits.
		ProtectorTypeSet(raw_bitset_from_u64(mask & ALGO_MASK))
	}
}

#[allow(deprecated)]
impl Set for ProtectorTypeSet
{
	type Item = ProtectorType;
	type Storage = _ProtectorTypeSet;
	fn new() -> Self { ProtectorTypeSet(0) }
	fn __storage<'a>(&'a self) -> &'a <Self as Set>::Storage { &self.0 }
	fn __storage_mut<'a>(&'a mut self) -> &'a mut <Self as Set>::Storage  { &mut self.0 }
	fn __universe() -> &'static [ProtectorType]
	{
		use self::ProtectorType::*;
		&[Aes128Gcm, Aes256Gcm, Chacha20Poly1305, Aes192Gcm]
	}
	//Let's override these with more efficient implementations.
	fn count(&self) -> u32 {  raw_bitset_hamming_weight(&self.0) }
	fn has_elements(&self) -> bool { raw_bitset_not_empty(&self.0) }
}

//Yes, this is ridiculous, but it is required to silence the deprecation warning.
mod __dummy
{
	#![allow(deprecated)]
	use super::ProtectorTypeSet;
	use btls_aux_set::impl_set_traits;
	use btls_aux_set::Set;
	impl_set_traits!(ProtectorTypeSet);
}


impl SetItem2 for ProtectorType
{
	type Storage = _ProtectorTypeSet;
	fn __universe() -> &'static [ProtectorType]
	{
		use self::ProtectorType::*;
		&[Aes128Gcm, Aes256Gcm, Chacha20Poly1305, Aes192Gcm]
	}
	fn __universe_bits() -> _ProtectorTypeSet
	{
		let mut x = 0;
		x |= 1 << BIT_AES_128_GCM;
		x |= 1 << BIT_AES_192_GCM;
		x |= 1 << BIT_AES_256_GCM;
		x |= 1 << BIT_CHACHA20_POLY1305;
		x
	}
	fn __sign_bit(&self) -> usize { self.algo_bit() as usize }
	fn __by_internal_name(name: &str) -> Option<usize>
	{
		Some(ProtectorType::by_internal_name(name)?.algo_bit() as usize)
	}
}


///A type of AEAD algorithm.
///
///This enumeration enumerates possible AEAD algorithms. In turn, this can be used to create encryptors and decryptors
///for the given algorithm.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
#[non_exhaustive]
pub enum ProtectorType
{
	///AES (Advanced Encryption Standard), with 128-bit key, in GCM (Galois Counter Mode), a.k.a. AES-128-GCM.
	///
	///This algorithm is number `1` in IANA `"Authenticated Encryption with Associated Data (AEAD) Parameters"`
	///registry.
	Aes128Gcm,
	///AES (Advanced Encryption Standard), with 192-bit key, in GCM (Galois Counter Mode), a.k.a. AES-192-GCM.
	///
	///There is no corresponding algorithm entry in IANA `"Authenticated Encryption with Associated Data (AEAD)
	///Parameters"` registry.
	Aes192Gcm,
	///AES (Advanced Encryption Standard), with 256-bit key, in GCM (Galois Counter Mode), a.k.a. AES-256-GCM.
	///
	///This algorithm is number `2` in IANA `"Authenticated Encryption with Associated Data (AEAD) Parameters"`
	///registry.
	Aes256Gcm,
	///Chacha20 (Chacha with 20 rounds) encryption combined with Poly1305 MAC (Message Authentication Code),
	///a.k.a. Chacha20-Poly1305-AEAD.
	///
	///This algorithm is number `29` in IANA `"Authenticated Encryption with Associated Data (AEAD) Parameters"`
	///registry.
	Chacha20Poly1305,
}

macro_rules! mkxcrypt
{
	($kind:ident $key:ident) => { UnifiedKey::$kind(newctx($key)?) };
}

impl ProtectorType
{
	///Get the bitmask bit for this protector.
	///
	/// # Return value:
	///
	///Protector type bit number.
	pub fn algo_bit(&self) -> u32
	{
		use self::ProtectorType::*;
		match self {
			&Aes128Gcm => BIT_AES_128_GCM,
			&Aes256Gcm => BIT_AES_256_GCM,
			&Chacha20Poly1305 => BIT_CHACHA20_POLY1305,
			&Aes192Gcm => BIT_AES_192_GCM,
		}
	}
	fn new_unified_key(&self, key: &[u8]) -> Result<UnifiedKey, AeadError>
	{
		use self::ProtectorType::*;
		let hwaes1 = btls_aux_hwaesgcm::hw_aes_supported();
		let k = match self {
			&Aes128Gcm if hwaes1 => mkxcrypt!(Hw1Aes128Gcm key),
			&Aes192Gcm if hwaes1 => mkxcrypt!(Hw1Aes192Gcm key),
			&Aes256Gcm if hwaes1 => mkxcrypt!(Hw1Aes256Gcm key),
			&Aes128Gcm => mkxcrypt!(Aes128Gcm key),
			&Aes192Gcm => mkxcrypt!(Aes192Gcm key),
			&Aes256Gcm => mkxcrypt!(Aes256Gcm key),
			&Chacha20Poly1305 => mkxcrypt!(Chacha2 key),
		};
		Ok(k)
	}
	///Get the default-enabled protector type set.
	///
	/// # Return value:
	///
	///Protector type set.
	pub const fn default_set() -> SetOf<ProtectorType>
	{
		btls_aux_set2::set_from_raw_storage!(__ALGO_CONST_MASK as u8)
	}
	///Get the enabled protector type set for NSA mode.
	///
	/// # Return value:
	///
	///Protector type set.
	pub const fn nsa_set() -> SetOf<ProtectorType>
	{
		btls_aux_set2::set_from_raw_storage!(__ALGO_CONST_MASK_NSA as u8)
	}
	///Construct protector for TLS cipher suite.
	///
	/// # Parameters:
	///
	/// * `cs`: The TLS cipher suite.
	///
	/// # Error conditions:
	///
	/// * The given cipher suite is unknown.
	/// * The given cipher suite has unsupported protector.
	///
	/// # Return value:
	///
	///The protector the given cipher suite uses.
	pub fn by_tls_ciphersuite(cs: CipherSuite) -> Option<ProtectorType>
	{
		Self::by_cipher(cs.cipher())
	}
	///Construct protector for TLS cipher.
	///
	/// # Parameters:
	///
	/// * `c`: The TLS cipher.
	///
	/// # Error conditions:
	///
	/// * The given cipher is not supported as protector.
	///
	/// # Return value:
	///
	///The protector corresponding to given TLS cipher.
	pub fn by_cipher(c: CipherSuiteCipher) -> Option<ProtectorType>
	{
		let p = match c {
			CipherSuiteCipher::Aes128Gcm => ProtectorType::Aes128Gcm,
			CipherSuiteCipher::Aes256Gcm => ProtectorType::Aes256Gcm,
			CipherSuiteCipher::Chacha20Poly1305 => ProtectorType::Chacha20Poly1305,
			_ => return None
		};
		Some(p)
	}
	///Construct protector by internal name.
	///
	/// # Parameters:
	///
	/// * `name`: The internal name of the protector.
	///
	/// # Error conditions:
	///
	/// * The given name is unknown.
	///
	/// # Return value:
	///
	///The protector corresponding to the given name.
	///
	/// # Currently known names:
	///
	/// name|description
	/// ----|-----------
	/// `AES-128-GCM`|AES with 128-bit key in GCM mode.
	/// `AES-192-GCM`|AES with 192-bit key in GCM mode.
	/// `AES-256-GCM`|AES with 256-bit key in GCM mode.
	/// `CHACHA20-POLY1305`|Chacha20 combined with Poly1305 MAC.
	pub fn by_internal_name(name: &str) -> Option<ProtectorType>
	{
		use self::ProtectorType::*;
		//These do not use symbolic constants, because the names are different from the usual algorithm names.
		//In practicular, CHACHA20POLY1305_NAME is "Chacha20-Poly1305" != "CHACHA20-POLY1305".
		let p = match name {
			"AES-128-GCM" => Aes128Gcm,
			"AES-192-GCM" => Aes192Gcm,
			"AES-256-GCM" => Aes256Gcm,
			"CHACHA20-POLY1305" => Chacha20Poly1305,
			_ => return None
		};
		Some(p)
	}
	///Get length of encryption key.
	///
	/// # Return value:
	///
	///The size of encryption key in bytes. This value is always bounded above by [`MAX_KEY`].
	///
	///[`MAX_KEY`]: constant.MAX_KEY.html
	pub fn get_key_length(&self) -> usize
	{
		use self::ProtectorType::*;
		match self {
			&Aes128Gcm => 16,
			&Aes192Gcm => 24,
			&Aes256Gcm|&Chacha20Poly1305 => 32,
		}
	}
	///Get length of nonce.
	///
	/// # Return value:
	///
	///The size of nonce in bytes. This value is always bounded above by [`MAX_NONCE`].
	///
	///[`MAX_NONCE`]: constant.MAX_NONCE.html
	pub fn get_nonce_length(&self) -> usize
	{
		use self::ProtectorType::*;
		match self {
			&Aes128Gcm|&Aes192Gcm|&Aes256Gcm|&Chacha20Poly1305 => 12,
		}
	}
	///Get length of tag.
	///
	/// # Return value:
	///
	///The size of tag in bytes. This value is always bounded above by [`MAX_EXPANSION`].
	///
	///This function returns the size of the tag the algorithm outputs in bytes. This value is always
	///bounded above by [`MAX_EXPANSION`]. The size of the tag is also the amount of expansion from plaintext to
	///ciphertext the encryption induces.
	///
	/// # Notes:
	///
	/// * In case the encryption algorithm is blocked, this returns the combination of tag size and the largest
	///possible padding to pad the input to block size. In this case, the size of ciphertext may be smaller than
	///plaintext size plus `get_tag_length()`.
	/// * If exact ciphertext size is needed, use [`EncryptionKey::encrypted_size()`].
	///
	///[`MAX_EXPANSION`]: constant.MAX_EXPANSION.html
	///[`EncryptionKey::encrypted_size()`]:struct.EncryptionKey.html#method.encrypted_size
	pub fn get_tag_length(&self) -> usize
	{
		use self::ProtectorType::*;
		match self {
			&Aes128Gcm|&Aes192Gcm|&Aes256Gcm|&Chacha20Poly1305 => 16,
		}
	}
	///Does the protector use GCM-style nonces in `TLS 1.2`?
	///
	/// # Return value:
	///
	/// * `true`, if the low 64 bits of the nonce are explicitly included in the `TLS 1.2` record. This is waste
	///of space and invites security bugs.
	/// * `false`, if `TLS 1.3`-style implicit nonces are used `TLS 1.2` records.
	///
	/// # Notes:
	///
	/// * `TLS 1.3` always uses implicit nonces.
	pub fn is_gcm_style(&self) -> bool
	{
		use self::ProtectorType::*;
		match self {
			&Aes128Gcm|&Aes192Gcm|&Aes256Gcm => true,
			&Chacha20Poly1305 => false,
		}
	}
	///Get the security level of the encryption algorithm.
	///
	/// # Return value:
	///
	///The security level in bits.
	///
	/// # Notes:
	///
	/// * The value returned should always be eight times the value retrned by [`get_key_length()`].
	///
	///[`get_key_length()`]: #method.get_key_length
	pub fn get_security_level(&self) -> u32
	{
		use self::ProtectorType::*;
		match self {
			&Aes128Gcm => 128,
			&Aes192Gcm => 192,
			&Aes256Gcm|&Chacha20Poly1305 => 256,
		}
	}
	///Get the security rank of the encryption algorithm.
	///
	/// # Return value:
	///
	///The relative rank of the algorithm. Higher numbers correspond to algorithms considered more secure.
	///
	/// # Notes:
	///
	/// * The absolute value returned is meaningless, only relative comparisions are meaningful.
	/// * This is very much an estimate. Algorithms can prove to be much less secure than thought.
	pub fn get_security_rank(&self) -> u32
	{
		use self::ProtectorType::*;
		match self {
			&Aes128Gcm => 1,
			&Aes192Gcm => 2,
			&Aes256Gcm => 3,
			&Chacha20Poly1305 => 4,
		}
	}
	#[allow(missing_docs)]
	#[deprecated(since="2.9.0", note="Use .algo_bit() instead")]
	pub fn algo_const(&self) -> u64
	{
		let b = self.algo_bit();
		if b < 64 { 1 << b } else { 0 }
	}
	#[allow(missing_docs,deprecated)]
	#[deprecated(since="2.8.1", note="Use SetOf<ProtectorType> instead")]
	pub fn singleton_set(&self) -> ProtectorTypeSet
	{
		let mut x = ProtectorTypeSet(SetStorageTrait::__new());
		x.0.set(self.algo_bit() as usize);
		x
	}
	///Get name for this algorithm.
	///
	/// # Return value:
	///
	///A human-readable name for this protector.
	pub fn as_string(&self) -> &'static str
	{
		use self::ProtectorType::*;
		match self {
			&Aes128Gcm => btls_aux_nettle::AES128GCM_NAME,
			&Aes256Gcm => btls_aux_nettle::AES256GCM_NAME,
			&Chacha20Poly1305 => btls_aux_chacha20poly1305aead::CHACHA20POLY1305_NAME,
			&Aes192Gcm => btls_aux_nettle::AES192GCM_NAME,
		}
	}
	///Create a new encryptor.
	///
	/// # Parameters:
	///
	/// * `key`: The encryption key to use.
	///
	/// # Error conditions:
	///
	/// * The key is of incorrect length. Use [`get_key_length()`] to obtain the correct length.
	///
	/// # Return value:
	///
	///The new encryption context.
	///
	///[`get_key_length()`]: #method.get_key_length
	pub fn new_encryptor(&self, key: &[u8]) -> Result<EncryptionKey, AeadError>
	{
		self.new_unified_key(key).map(EncryptionKey)
	}
	///Create a new decryptor.
	///
	/// # Parameters:
	///
	/// * `key`: The decryption key to use.
	///
	/// # Error conditions:
	///
	/// * The key is of incorrect length. Use [`get_key_length()`] to obtain the correct length.
	///
	/// # Return value:
	///
	///The new decryption context.
	///
	///[`get_key_length()`]: #method.get_key_length
	pub fn new_decryptor(&self, key: &[u8]) -> Result<DecryptionKey, AeadError>
	{
		self.new_unified_key(key).map(DecryptionKey)
	}
}

//Internally used by mkxcrypt!() macro to implement some magical type inference.
fn newctx<T:LowLevelAeadContext>(key: &[u8]) -> Result<T, AeadError> { T::new_context(key) }

enum UnifiedKey
{
	Aes128Gcm(btls_aux_nettle::Aes128GcmContext),
	Aes192Gcm(btls_aux_nettle::Aes192GcmContext),
	Aes256Gcm(btls_aux_nettle::Aes256GcmContext),
	Chacha2(btls_aux_chacha20poly1305aead::Chacha20Poly1305),
	Hw1Aes128Gcm(btls_aux_hwaesgcm::Aes128GcmContext),
	Hw1Aes192Gcm(btls_aux_hwaesgcm::Aes192GcmContext),
	Hw1Aes256Gcm(btls_aux_hwaesgcm::Aes256GcmContext),
}

//Call the `do_action()` method on object $p with demultiplexed encryptor/decryptor (UnifiedKey) as argument
//(as &impl LowLevelAeadContext).
macro_rules! with_unified_key
{
	($iref:expr, $p:expr) => {{
		let p = $p;
		match $iref {
			&UnifiedKey::Aes128Gcm(ref x) => p.do_action(x),
			&UnifiedKey::Aes192Gcm(ref x) => p.do_action(x),
			&UnifiedKey::Aes256Gcm(ref x) => p.do_action(x),
			&UnifiedKey::Chacha2(ref x) => p.do_action(x),
			&UnifiedKey::Hw1Aes128Gcm(ref x) => p.do_action(x),
			&UnifiedKey::Hw1Aes192Gcm(ref x) => p.do_action(x),
			&UnifiedKey::Hw1Aes256Gcm(ref x) => p.do_action(x),
		}
	}}
}

///An encryptor.
///
///This ADT wraps inside the AEAD algorithm and the key used, and performs data encryption using the stored algorithm
///and the key.
///
///Objects are created using the [`ProtectorType::new_encryptor()`] method.
///
///[`ProtectorType::new_encryptor()`]: enum.ProtectorType.html#method.new_encryptor
pub struct EncryptionKey(UnifiedKey);

///A decryptor.
///
///This ADT wraps inside the AEAD algorithm and the key used, and performs data decryption using the stored algorithm
///and the key.
///
///Objects are created using the [`ProtectorType::new_decryptor()`] method.
///
/// # DANGER:
///
///The AEAD is not guaranteed to be committing: Decrypting with the wrong key is not guaranteed to fail. The attacker
///may be able to produce messages that successfully decrypt under multiple keys, maybe with different nonce and
///associated data values.
///
///[`ProtectorType::new_decryptor()`]: enum.ProtectorType.html#method.new_decryptor
pub struct DecryptionKey(UnifiedKey);

//For use with with_unified_key!() in order to call encrypted_size() with specified argument.
struct CallEncryptNeedSize(usize);
impl CallEncryptNeedSize
{
	fn do_action(self, context: &impl LowLevelAeadContext) -> Option<usize> { context.encrypted_size(self.0) }
}

//For use with with_unified_key!() in order to call max_plaintext_size() with specified argument.
struct CallDecryptNeedSize(usize);
impl CallDecryptNeedSize
{
	fn do_action(self, context: &impl LowLevelAeadContext) -> Option<usize> { context.max_plaintext_size(self.0) }
}

//For use with with_unified_key!() in order to call encrypt3() with specified arguments.
struct CallEncrypt3<'a>
{
	//The nonce.
	nonce: &'a [u8],
	//The associated data.
	ad: &'a [u8],
	//Pointer to output buffer. Must point to sufficient writable bytes. Use
	//`LowLevelAeadContext::encrypted_size()` function to calculate the amount of space needed.
	optr: NonNullMut<u8>,
	//Pointer to input buffer.
	input: AliasReadSlice<'a, u8>,
	//Length of trailer part. Located at optr + ilen.
	trailerlen: usize,
}

impl<'a> CallEncrypt3<'a>
{
	//The receiver must be as documented in the declaration of Encrypt2.
	unsafe fn do_action(self, context: &impl LowLevelAeadContext) -> Result<usize, AeadError>
	{
		//Safety: The invariants of self are exactly the preconditions of LowLevelAeadContext::encrypt3().
		let optr = self.optr.to_raw();
		let (iptr, ilen) = self.input.base_and_len();
		unsafe{context.encrypt3(self.nonce, self.ad, optr, iptr.to_raw(), ilen, self.trailerlen)}
	}
}

impl EncryptionKey
{
	#[allow(missing_docs)]
	#[deprecated(since="2.8.0", note="Use encrypt_inplace/encrypt_buffer_to_buffer instead")]
	pub fn encrypt2<'a,'b>(&self, nonce: &[u8], ad: &[u8], mut pt_ct: InputOutputBuffer<'a,'b>,
		trailerlen: usize) -> Result<usize, AeadError>
	{
		let (output, input) = pt_ct.buffers3()?;
		//Safety:
		//1) By postonditions of InputOutputBuffer::buffers2(), optr points to olen writable bytes.
		//2) By postonditions of InputOutputBuffer::buffers2(), iptr points to ilen readable bytes.
		//3) InputOutputBuffer requires the entiere output buffer to be initialized, so any subrange of
		//it is also initialized.
		//4) By postconditions of InputOutputBuffer::buffers2(), optr and iptr are either the same or do
		//not overlap at all.
		unsafe{encrypt_rawptr(&self.0, output, input, nonce, ad, trailerlen)}
	}
	///Encrypt a message in-place.
	///
	/// # Warnings about nonce seletion:
	///
	/// * The nonce never repeat for the same key, or **ALL SECURITY WILL BE LOST**.
	/// * The nonce MUST NOT be random, because there is insufficent space for reliable randomization. In
	///context of TLS, this is known as *Nonce Disrespecting Adversaries attack*
	/// * The nonce does not have to unpredictable or secret. It SHOULD be a counter.
	/// * However, the starting point of the nonce SHOULD be random, especially with 128 bit keys, as otherwise
	///the algorithm is weakened.
	/// * The exact nonces used SHOULD also be secret, which means deriving the initial nonce via a key
	///derivation function, and then only transmitting offsets to this initial nonce.
	///
	/// # Parameters:
	///
	/// * `nonce`: The nonce to use.
	/// * `ad`: The associated data.
	/// * `xt`: The buffer plaintext is read from, and ciphertext is written to.
	/// * `ptlen`: Length of plaintext in bytes.
	///
	/// # Error conditions:
	///
	/// * `nonce` is not of suitable length for the algorithm. For the correct length, use
	///[`ProtectorType::get_nonce_length()`].
	/// * `ad` is too large.
	/// * `ptlen` is too large.
	/// * `ptlen` is larger than `xt.len()`.
	/// * The output ciphertext will not fit in `xt`. For the size of the ciphertext, use
	///[`encrypted_size()`]. One may assume that the worst-case size of the ciphertext is plaintext size plus
	///[`MAX_EXPANSION`].
	///
	/// # Return value:
	///
	///Number of bytes in ciphertext.
	///
	///[`encrypted_size()`]: #method.encrypted_size
	///[`MAX_EXPANSION`]: constant.MAX_EXPANSION.html
	///[`ProtectorType::get_nonce_length()`]: enum.ProtectorType.html#method.get_nonce_length
	pub fn encrypt_inplace(&self, nonce: &[u8], ad: &[u8], xt: &mut [u8], ptlen: usize) -> Result<usize, AeadError>
	{
		let xput = AliasReadWriteSlice::new(xt);
		let (mut input, output) = xput.fork();
		let xlen = input.len();
		//Check there are enough input elements.
		input.truncate(ptlen).map_err(|_|{
			AeadError::BadBuffers(ptlen, xlen)
		})?;
		//Safety: trailerlen=0 trivially satisfies the initialization constraint.
		unsafe{encrypt_rawptr(&self.0, output, input, nonce, ad, 0)}
	}
	///Encrypt a message from buffer to buffer.
	///
	/// # Warnings about nonce seletion:
	///
	/// * The nonce never repeat for the same key, or **ALL SECURITY WILL BE LOST**.
	/// * The nonce MUST NOT be random, because there is insufficent space for reliable randomization. In
	///context of TLS, this is known as *Nonce Disrespecting Adversaries attack*
	/// * The nonce does not have to unpredictable or secret. It SHOULD be a counter.
	/// * However, the starting point of the nonce SHOULD be random, especially with 128 bit keys, as otherwise
	///the algorithm is weakened.
	/// * The exact nonces used SHOULD also be secret, which means deriving the initial nonce via a key
	///derivation function, and then only transmitting offsets to this initial nonce.
	///
	/// # Parameters:
	///
	/// * `nonce`: The nonce to use.
	/// * `ad`: The associated data.
	/// * `ct`: The buffer to write the ciphertext to.
	/// * `pt`: The buffer to read the plaintext from.
	/// * `padcontrol`: Padding control.
	///   * If `None`, no padding is used.
	///   * If `Some(b,n)`, then the message is padded with byte `b`, followed by `n` zeroes.
	///
	/// # Error conditions:
	///
	/// * `nonce` is not of suitable length for the algorithm. For the correct length, use
	///[`ProtectorType::get_nonce_length()`].
	/// * `ad` is too large.
	/// * The plaintext is too large.
	/// * The output ciphertext will not fit in `ct`. For the size of the ciphertext, use
	///[`encrypted_size()`]. One may assume that the worst-case size of the ciphertext is plaintext size plus
	///[`MAX_EXPANSION`]. Remeber to take padding (if any) into account in plaintext size.
	///
	/// # Return value:
	///
	///The filled part of ciphertext buffer.
	///
	///[`encrypted_size()`]: #method.encrypted_size
	///[`MAX_EXPANSION`]: constant.MAX_EXPANSION.html
	///[`ProtectorType::get_nonce_length()`]: enum.ProtectorType.html#method.get_nonce_length
	pub fn encrypt_buffer_to_buffer<'a>(&self, nonce: &[u8], ad: &[u8],
		ct: &'a mut (impl WriteOnlyArray<u8>+?Sized), pt: &[u8], padcontrol: Option<(u8, usize)>) ->
		Result<&'a [u8], AeadError>
	{
		let mut ct = BackedVector::new_woa(ct);
		let output = ct.unfilled_aliasing_write();
		let ptlen = pt.len();
		//If there is padding, write it into trailer part. If no padding, then the trailer is empty.
		let trailerlen = if let Some((padbyte, padlen)) = padcontrol {
			write_padcontrol(&output, ptlen, padbyte, padlen)
		} else {
			0
		};
		let input = AliasReadSlice::new(pt);
		unsafe {
			//Safety:
			//1) By postconditions of write_padcontrol(), indices [ptlen,ptlen+trailerlen) in optr are
			//   initialized.
			let len = encrypt_rawptr(&self.0, output, input, nonce, ad, trailerlen)?;
			//Safety: Postconditions of encrypt_rawptr() guarantee that first len bytes of ct have been
			//initialized.
			ct.assume_init(len);
		}
		Ok(ct.into_inner())
	}
	///Compute size of ciphertext for given plaintext size.
	///
	///This is the output size of [`encrypt_inplace()`] or [`encrypt_buffer_to_buffer()`] for the given
	///plaintext size.
	///
	/// # Parameters:
	///
	/// * `ptlen`: The size of plaintext in bytes.
	///
	/// # Error conditions:
	///
	/// * The given plaintext size is too large. On 32-bit platforms, the limit is about 2GB. For 64-bit platforms,
	///the limit for AES-GCM is about 64GB, and the limit for Chacha-Poly1305 is about 256GB.
	///
	/// # Return value:
	///
	///The number of bytes in the ciphertext.
	///
	///[`encrypt_inplace()`]: #method.encrypt_inplace
	///[`encrypt_buffer_to_buffer()`]: #method.encrypt_buffer_to_buffer
	pub fn encrypted_size(&self, ptlen: usize) -> Option<usize>
	{
		with_unified_key!(&self.0, CallEncryptNeedSize(ptlen))
	}
	///Compute maximum plaintext size.
	///
	///This is the same as [`DecryptionKey::max_plaintext_size()`], but takes encryption key instead of
	///decryption key.
	///
	///This is mostly useful for sizing ciphertext buffers in some corner-cases.
	///
	/// # Error conditions:
	///
	/// * The given size of ciphertext is not possible for the algorithm. It can be too small, too large, or
	///not aligned to block size (if the algorithm has blocks of some sort).
	///
	/// # Returns:
	///
	///The maximum size of plaintext in bytes.
	///
	///[`DecryptionKey::max_plaintext_size()`]: struct.DecryptionKey.html#method.max_plaintext_size
	pub fn max_plaintext_size(&self, ctlen: usize) -> Option<usize>
	{
		with_unified_key!(&self.0, CallDecryptNeedSize(ctlen))
	}
}

macro_rules! assert_capacity
{
	($x:ident, $y:expr) => {{
		let y = $y;
		debug_assert!($x >= y);
		$x -= y;
		let _ = $x;	//Shut up a warning.
		()
	}}
}

//Write padding.
//
//This will write padding to indices [ptlen,ptlen+padlen+1). 
//
//Return value is padlen + 1.
fn write_padcontrol(output: &AliasWriteSlice<u8>, ptlen: usize, padbyte: u8, padlen: usize) -> usize
{
	let (optr, olen) = output.base_and_len();
	let output_remain = olen.saturating_sub(ptlen);
	let mut check = olen;
	//Write the padding as trailer part. The total length of padding is padlen + 1. If there is insufficent space,
	//write whatever part of the padding fits, and then proceed to call encrypt_rawptr/( with nominal padding size:
	//The call to encrypt_rawptr() will fail as desired. The padding consists of padbyte followed by padlen zero
	//bytes.
	if output_remain > 0 {
		//Safety: output_remain > 0, which impiles olen > ptlen, which in turn impiles that optr points to
		//more than ptlen writable bytes.
		assert_capacity!(check, ptlen);
		let mut padptr = unsafe{optr.add(ptlen)};
		//Safety: Because optr points to more than plen writable bytes, padptr points to at least one writable
		//byte.
		assert_capacity!(check, 1);
		unsafe{padptr.write_adv(padbyte);}
		//Now padptr points to output_remain - 1 writable bytes, which is at least 0 because output_remain > 0
		//by branch condition above.
		//Safety: By above condition, it must hold that output_remain > 0. Therefore, ptlen < olen.
		//Furthermore, padbytes <= output_remain - 1 < output_remain. Thus ptlen+1+padbytes <=
		//ptlen+1+(output_remain-1) = pt.len()+output_remain = ptlen+(olen-ptlen) = olen. Which is length of
		//optr. Therefore the write is all in bounds.
		let padbytes = min(padlen, output_remain - 1);
		//Safety: padptr points to olen - ptlen - 1 writable bytes. Because output_remain > 0,
		//output_remain = olen - ptlen, so olen - ptlen - 1 = output_remain - 1. Now
		//padbytes <= output_remain - 1, so the write is in-range.
		assert_capacity!(check, padbytes);
		unsafe{padptr.write_bytes_adv(0, padbytes);}
	}
	padlen + 1
}

//Safety preconditions:
//
//1) indices [ilen,ilen+trailerlen) in output are initialized.
unsafe fn encrypt_rawptr(ctx: &UnifiedKey, output: AliasWriteSlice<u8>, input: AliasReadSlice<u8>, nonce: &[u8],
	ad: &[u8], trailerlen: usize) -> Result<usize, AeadError>
{
	AeadError::assert_same_or_disjoint(&output, &input)?;
	let ilen = input.len();
	let (optr, olen) = output.base_and_len();
	//In case the saturating addition of plaintext size saturates, the size will exceed the maximum plaintext
	//size, as it is capped to isize::MAX, making the encrypt call quickly fail. Also in case encrypted_size()
	//fails due to plaintext too big, the encrypt call will fail anyway.
	let ptlen = ilen.saturating_add(trailerlen);
	let need = with_unified_key!(ctx, CallEncryptNeedSize(ptlen)).unwrap_or(0);
	let maximum = bound_buffer_size(olen);
	fail_if!(need > maximum, AeadError::OutputTooBig(need, maximum));
	//The function safety preconditions are enough if olen >= encrypted_size(ilen + trailerlen). But
	//ilen + trailerlen = ptlen, so this is equivalen to olen >= encrypted_size(ptlen). Now,
	//encrypted_size(ptlen) = need, so this is equivalen to olen >= need. Now olen >= maximum, so
	//maximum >= need is enough to imply this. But if maximum >= need fails, the above check return error.
	//Therefore the safety preconditions hold.
	unsafe{with_unified_key!(ctx, CallEncrypt3{nonce, ad, optr, input, trailerlen})}
}

//For use with with_unified_key!() in order to call decrypt3() with specified arguments.
struct CallDecrypt3<'a>
{
	//The nonce.
	nonce: &'a [u8],
	//The associated data.
	ad: &'a [u8],
	//Pointer to output buffer. Must have at least ilen writable bytes. Either must not overlap with the input
	//buffer, or must be equal to input buffer.
	optr: NonNullMut<u8>,
	//The input buffer.
	input: AliasReadSlice<'a, u8>,
}

impl<'a> CallDecrypt3<'a>
{
	//The receiver must be as documented in the declaration of Decrypt2.
	unsafe fn do_action(self, context: &impl LowLevelAeadContext) -> Result<usize, AeadError>
	{
		//Safety: The invariants of self are exactly the preconditions of LowLevelAeadContext::decrypt3().
		let optr = self.optr.to_raw();
		let (iptr, ilen) = self.input.base_and_len();
		unsafe{context.decrypt3(self.nonce, self.ad, optr, iptr.to_raw(), ilen)}
	}
}

impl DecryptionKey
{
	#[allow(missing_docs)]
	#[deprecated(since="2.8.0", note="Use decrypt_inplace/decrypt_buffer_to_buffer instead")]
	pub fn decrypt2<'a,'b>(&self, nonce: &[u8], ad: &[u8], mut ct_pt: InputOutputBuffer<'a,'b>) ->
		Result<usize, AeadError>
	{
		let (output, input) = ct_pt.buffers3()?;
		decrypt_rawptr(&self.0, output, input, nonce, ad)
	}
	///Decrypt a message in-place.
	///
	/// # Parameters:
	///
	/// * `nonce`: The nonce to use.
	/// * `ad`: The associated data to use.
	/// * `xt`: The buffer to read the ciphertext from and write the plaintext to.
	///
	/// # Error conditions:
	///
	/// * The key does not match what was used when encrypting.
	/// * Nonce does not match what was used when encrypting.
	/// * Associated data does not match what was used when encrypting.
	/// * The ciphertext has been tampered with.
	/// 
	/// # Return value:
	///
	///Number of bytes of plaintext filled.
	pub fn decrypt_inplace(&self, nonce: &[u8], ad: &[u8], xt: &mut [u8]) -> Result<usize, AeadError>
	{
		let xput = AliasReadWriteSlice::new(xt);
		let (input, output) = xput.fork();
		//Safety: xt contains xlen readable/writable elements and and buffers are the same.
		decrypt_rawptr(&self.0, output, input, nonce, ad)
	}
	///Decrypt message buffer-to-buffer.
	///
	/// # Parameters:
	///
	/// * `nonce`: The nonce to use.
	/// * `ad`: The associated data to use.
	/// * `pt`: The buffer to write the plaintext to.
	/// * `ct`: The buffer to read the ciphertext from.
	///
	/// # Error conditions:
	///
	/// * The key does not match what was used when encrypting.
	/// * Nonce does not match what was used when encrypting.
	/// * Associated data does not match what was used when encrypting.
	/// * The ciphertext has been tampered with.
	/// * `pt` is not big enough. For the worst-case size of the plaintext, see [`max_plaintext_size()`].
	///One may assume that the worst-case size of the plaintext is smaller than the ciphertext.
	///
	/// # Return value:
	///
	///The filled part of plaintext buffer.
	///
	///[`max_plaintext_size()`]: #method.max_plaintext_size
	pub fn decrypt_buffer_to_buffer<'a>(&self, nonce: &[u8], ad: &[u8],
		pt: &'a mut (impl WriteOnlyArray<u8>+?Sized), ct: &[u8]) -> Result<&'a [u8], AeadError>
	{
		let mut pt = BackedVector::new_woa(pt);
		let output = pt.unfilled_aliasing_write();
		let input = AliasReadSlice::new(ct);
		let len = decrypt_rawptr(&self.0, output, input, nonce, ad)?;
		//By __decrypt(), len elements have been initialized.
		unsafe {pt.assume_init(len);}
		Ok(pt.into_inner())
	}
	///Compute maximum plaintext size.
	///
	///The maximum plaintext size is sufficient output buffer size for decrypting any ciphertext of given length
	///using [`decrypt_buffer_to_buffer()`].
	///
	/// # Parameters:
	///
	/// * `ctlen`: The length of ciphertext in bytes.
	///
	/// # Error conditions:
	///
	/// * The given size of ciphertext is not possible for the algorithm. It can be too small, too large, or
	///not aligned to block size (if the algorithm has blocks of some sort).
	///
	/// # Returns:
	///
	///The maximum size of plaintext in bytes.
	///
	///[`decrypt_buffer_to_buffer()`]: #method.decrypt_buffer_to_buffer
	pub fn max_plaintext_size(&self, ctlen: usize) -> Option<usize>
	{
		with_unified_key!(&self.0, CallDecryptNeedSize(ctlen))
	}
}

fn decrypt_rawptr(ctx: &UnifiedKey, output: AliasWriteSlice<u8>, input: AliasReadSlice<u8>, nonce: &[u8],
	ad: &[u8]) -> Result<usize, AeadError>
{
	AeadError::assert_same_or_disjoint(&output, &input)?;
	let ilen = input.len();
	let (optr, olen) = output.base_and_len();
	//In case ciphertext size is too large, just call decryption with 0 byte output buffer, it will quickly
	//fail.
	let need = with_unified_key!(ctx, CallDecryptNeedSize(ilen)).unwrap_or(0);
	let maximum = bound_buffer_size(olen);
	fail_if!(need > maximum, AeadError::OutputTooBig(need, maximum));
	//The function safety preconditions are enough if olen >= max_plaintext_size(ilen). But
	//max_plaintext_size(ilen) = need, so this is equivalen to olen >= need. Now olen >= maximum, so
	//maximum >= need is enough to imply this. But if maximum >= need fails, the above check return error.
	//Therefore the safety preconditions hold.
	unsafe{with_unified_key!(ctx, CallDecrypt3{nonce, ad, optr, input})}
}

#[cfg(test)]
mod test;
