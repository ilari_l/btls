#![allow(deprecated)]
use super::AeadError;
use super::DecryptionKey;
use super::EncryptionKey;
use super::InputOutputBuffer;
use super::ProtectorType;
use core::mem::MaybeUninit;

fn test_tail(ectx: EncryptionKey, dctx: DecryptionKey, dctx2: DecryptionKey)
{
	let mut buffer1 = vec![0;1024];
	let mut buffer2 = vec![0;1024];
	let mut data = vec![0;1024];
	let nonce1 = [1;12];
	let mut nonce2 = [1;12];
	let ad1 = [7;14];
	let mut ad2 = [7;14];
	let ad3 = [8;115];
	let mut ad4 = [8;115];
	nonce2[11] += 1;
	ad2[13] += 1;
	ad4[76] += 1;
	for (idx, value) in data.iter_mut().enumerate() { *value = (0x252243 * idx + 0x3636774) as u8; }

	//Basic multiple of 64 encryption, same buffer, no ad.
	(&mut buffer1[..128]).copy_from_slice(&data[..128]);
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Single(&mut buffer1[..144], 128), 0), Ok(144));
	assert_eq!(dctx.decrypt2(&nonce1, &[], InputOutputBuffer::Single(&mut buffer1[..144], 144)), Ok(128));
	for i in 0..128 { assert_eq!(buffer1[i], data[i]); }

	//Single buffer longer than necressary.
	(&mut buffer1[..128]).copy_from_slice(&data[..128]);
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Single(&mut buffer1[..147], 128), 0), Ok(144));
	assert_eq!(dctx.decrypt2(&nonce1, &[], InputOutputBuffer::Single(&mut buffer1[..147], 144)), Ok(128));
	for i in 0..128 { assert_eq!(buffer1[i], data[i]); }

	//Single buffer too short.
	(&mut buffer1[..128]).copy_from_slice(&data[..128]);
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Single(&mut buffer1[..143], 128), 0), Err(
		AeadError::OutputTooBig(144, 143)));
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Single(&mut buffer1[..147], 128), 0), Ok(144));
	assert_eq!(dctx.decrypt2(&nonce1, &[], InputOutputBuffer::Single(&mut buffer1[..143], 144)), Err(
		AeadError::BadBuffers(144, 143)));

	//Basic multiple of 64 encryption, same buffer, with ad.
	(&mut buffer1[..128]).copy_from_slice(&data[..128]);
	assert_eq!(ectx.encrypt2(&nonce1, &ad1, InputOutputBuffer::Single(&mut buffer1[..144], 128), 0), Ok(144));
	assert_eq!(dctx.decrypt2(&nonce1, &ad1, InputOutputBuffer::Single(&mut buffer1[..144], 144)), Ok(128));
	for i in 0..128 { assert_eq!(buffer1[i], data[i]); }

	//Basic multiple of 64 encryption, same buffer, with long ad.
	(&mut buffer1[..128]).copy_from_slice(&data[..128]);
	assert_eq!(ectx.encrypt2(&nonce1, &ad3, InputOutputBuffer::Single(&mut buffer1[..144], 128), 0), Ok(144));
	assert_eq!(dctx.decrypt2(&nonce1, &ad3, InputOutputBuffer::Single(&mut buffer1[..144], 144)), Ok(128));
	for i in 0..128 { assert_eq!(buffer1[i], data[i]); }

	//Refuses to decrypt if key mismatched.
	(&mut buffer1[..128]).copy_from_slice(&data[..128]);
	assert_eq!(ectx.encrypt2(&nonce1, &ad1, InputOutputBuffer::Single(&mut buffer1[..144], 128), 0), Ok(144));
	assert_eq!(dctx2.decrypt2(&nonce1, &ad1, InputOutputBuffer::Single(&mut buffer1[..144], 144)), Err(
		AeadError::MacCheckFailed));

	//Refuses to decrypt if ad mismatched.
	(&mut buffer1[..128]).copy_from_slice(&data[..128]);
	assert_eq!(ectx.encrypt2(&nonce1, &ad1, InputOutputBuffer::Single(&mut buffer1[..144], 128), 0), Ok(144));
	assert_eq!(dctx.decrypt2(&nonce1, &ad2, InputOutputBuffer::Single(&mut buffer1[..144], 144)), Err(
		AeadError::MacCheckFailed));

	//Refuses to decrypt if long ad mismatched.
	(&mut buffer1[..128]).copy_from_slice(&data[..128]);
	assert_eq!(ectx.encrypt2(&nonce1, &ad3, InputOutputBuffer::Single(&mut buffer1[..144], 128), 0), Ok(144));
	assert_eq!(dctx.decrypt2(&nonce1, &ad4, InputOutputBuffer::Single(&mut buffer1[..144], 144)), Err(
		AeadError::MacCheckFailed));

	//Refuses to decrypt if nonce mismatched.
	(&mut buffer1[..128]).copy_from_slice(&data[..128]);
	assert_eq!(ectx.encrypt2(&nonce1, &ad1, InputOutputBuffer::Single(&mut buffer1[..144], 128), 0), Ok(144));
	assert_eq!(dctx.decrypt2(&nonce2, &ad1, InputOutputBuffer::Single(&mut buffer1[..144], 144)), Err(
		AeadError::MacCheckFailed));

	//Refuses to decrypt if key message is corrupt.
	(&mut buffer1[..128]).copy_from_slice(&data[..128]);
	assert_eq!(ectx.encrypt2(&nonce1, &ad1, InputOutputBuffer::Single(&mut buffer1[..144], 128), 0), Ok(144));
	buffer1[57] += 1;
	assert_eq!(dctx.decrypt2(&nonce1, &ad1, InputOutputBuffer::Single(&mut buffer1[..144], 144)), Err(
		AeadError::MacCheckFailed));

	//Basic multiple of 64 encryption, different buffers.
	(&mut buffer1[..128]).copy_from_slice(&data[..128]);
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer2[..144], &buffer1[..128]),
		0), Ok(144));
	buffer1[57] += 1;
	assert_eq!(dctx.decrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer1[..128], &buffer2[..144])),
		Ok(128));
	for i in 0..128 { assert_eq!(buffer1[i], data[i]); }

	//Basic multiple of 64 encryption, overlong targets.
	(&mut buffer1[..128]).copy_from_slice(&data[..128]);
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer2[..154], &buffer1[..128]),
		0), Ok(144));
	buffer1[57] += 1;
	assert_eq!(dctx.decrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer1[..138], &buffer2[..144])),
		Ok(128));
	for i in 0..128 { assert_eq!(buffer1[i], data[i]); }

	//Basic multiple of 64 encryption, different buffers, too short.
	(&mut buffer1[..128]).copy_from_slice(&data[..128]);
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer2[..143], &buffer1[..128]),
		0), Err(AeadError::OutputTooBig(144, 143)));
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer2[..144], &buffer1[..128]),
		0), Ok(144));
	buffer1[57] += 1;
	assert_eq!(dctx.decrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer1[..127], &buffer2[..144])),
		Err(AeadError::OutputTooBig(128, 127)));
	assert_eq!(dctx.decrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer1[..128], &buffer2[..144])),
		Ok(128));
	for i in 0..128 { assert_eq!(buffer1[i], data[i]); }

	//Basic multiple of 16 encryption, same buffer, no ad, trailer does not fill block.
	(&mut buffer1[..202]).copy_from_slice(&data[..202]);
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Single(&mut buffer1[..218], 192), 10), Ok(218));
	assert_eq!(dctx.decrypt2(&nonce1, &[], InputOutputBuffer::Single(&mut buffer1[..218], 218)), Ok(202));
	for i in 0..202 { assert_eq!(buffer1[i], data[i]); }

	//Basic multiple of 16 encryption, same buffer, no ad, trailer fills block.
	(&mut buffer1[..212]).copy_from_slice(&data[..212]);
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Single(&mut buffer1[..228], 192), 20), Ok(228));
	assert_eq!(dctx.decrypt2(&nonce1, &[], InputOutputBuffer::Single(&mut buffer1[..228], 228)), Ok(212));
	for i in 0..212 { assert_eq!(buffer1[i], data[i]); }

	//Basic non-multiple of 16 encryption, same buffer, no ad.
	(&mut buffer1[..178]).copy_from_slice(&data[..178]);
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Single(&mut buffer1[..194], 178), 0), Ok(194));
	assert_eq!(dctx.decrypt2(&nonce1, &[], InputOutputBuffer::Single(&mut buffer1[..194], 194)), Ok(178));
	for i in 0..178 { assert_eq!(buffer1[i], data[i]); }

	//Basic non-multiple of 16 encryption, same buffer, no ad, trailer does not fill block.
	(&mut buffer1[..188]).copy_from_slice(&data[..188]);
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Single(&mut buffer1[..204], 178), 10), Ok(204));
	assert_eq!(dctx.decrypt2(&nonce1, &[], InputOutputBuffer::Single(&mut buffer1[..204], 204)), Ok(188));
	for i in 0..188 { assert_eq!(buffer1[i], data[i]); }

	//Basic non-multiple of 16 encryption, same buffer, no ad, trailer fills block.
	(&mut buffer1[..198]).copy_from_slice(&data[..198]);
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Single(&mut buffer1[..214], 178), 20), Ok(214));
	assert_eq!(dctx.decrypt2(&nonce1, &[], InputOutputBuffer::Single(&mut buffer1[..214], 214)), Ok(198));
	for i in 0..198 { assert_eq!(buffer1[i], data[i]); }

	//too short non-multiple of 16 encryption, same buffer, no ad, trailer fills block.
	(&mut buffer1[..198]).copy_from_slice(&data[..198]);
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Single(&mut buffer1[..213], 178), 20), Err(
		AeadError::OutputTooBig(214, 213)));
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Single(&mut buffer1[..214], 178), 20), Ok(214));
	assert_eq!(dctx.decrypt2(&nonce1, &[], InputOutputBuffer::Single(&mut buffer1[..214], 214)), Ok(198));
	for i in 0..198 { assert_eq!(buffer1[i], data[i]); }


	//Basic multiple of 16 encryption, two buffers, no ad, trailer does not fill block.
	(&mut buffer1[..192]).copy_from_slice(&data[..192]);
	(&mut buffer2[192..202]).copy_from_slice(&data[192..202]);
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer2[..218], &buffer1[..192]), 10),
		Ok(218));
	assert_eq!(dctx.decrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer1[..202], &buffer2[..218])),
		Ok(202));
	for i in 0..202 { assert_eq!(buffer1[i], data[i]); }

	//Basic multiple of 16 encryption, two buffers, no ad, trailer fills block.
	(&mut buffer1[..192]).copy_from_slice(&data[..192]);
	(&mut buffer2[192..212]).copy_from_slice(&data[192..212]);
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer2[..228], &buffer1[..192]), 20),
		Ok(228));
	assert_eq!(dctx.decrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer1[..212], &buffer2[..228])),
		Ok(212));
	for i in 0..212 { assert_eq!(buffer1[i], data[i]); }

	//Basic non-multiple of 16 encryption, two buffers, no ad.
	(&mut buffer1[..178]).copy_from_slice(&data[..178]);
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer2[..194], &buffer1[..178]), 0),
		Ok(194));
	assert_eq!(dctx.decrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer1[..178], &buffer2[..194])),
		Ok(178));
	for i in 0..178 { assert_eq!(buffer1[i], data[i]); }

	//Basic non-multiple of 16 encryption, two buffers, no ad, trailer does not fill block.
	(&mut buffer1[..178]).copy_from_slice(&data[..178]);
	(&mut buffer2[178..188]).copy_from_slice(&data[178..188]);
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer2[..204], &buffer1[..178]), 10),
		Ok(204));
	assert_eq!(dctx.decrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer1[..188], &buffer2[..204])),
		Ok(188));
	for i in 0..188 { assert_eq!(buffer1[i], data[i]); }

	//Basic non-multiple of 16 encryption, two buffers, no ad, trailer fills block.
	(&mut buffer1[..178]).copy_from_slice(&data[..178]);
	(&mut buffer2[178..198]).copy_from_slice(&data[178..198]);
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer2[..214], &buffer1[..178]), 20),
		Ok(214));
	assert_eq!(dctx.decrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer1[..198], &buffer2[..214])),
		Ok(198));
	for i in 0..198 { assert_eq!(buffer1[i], data[i]); }

	//too short non-multiple of 16 encryption, two buffers, no ad, trailer fills block.
	(&mut buffer1[..178]).copy_from_slice(&data[..178]);
	(&mut buffer2[178..198]).copy_from_slice(&data[178..198]);
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer2[..213], &buffer1[..178]), 20),
		Err(AeadError::OutputTooBig(214, 213)));
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer2[..214], &buffer1[..178]), 20),
		Ok(214));
	assert_eq!(dctx.decrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer1[..198], &buffer2[..214])),
		Ok(198));
	for i in 0..198 { assert_eq!(buffer1[i], data[i]); }

	//Basic buffer to buffer encrypt.
	let mut tmp = [MaybeUninit::<u8>::uninit(); 1024];
	(&mut buffer1[..160]).copy_from_slice(&data[..160]);
	let tmp = ectx.encrypt_buffer_to_buffer(&nonce1, &[], &mut tmp, &buffer1[..160], None).unwrap();
	assert_eq!(dctx.decrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer2[..160], tmp)), Ok(160));
	for i in 0..160 { assert_eq!(buffer2[i], data[i]); }

	//Basic buffer to buffer decrypt.
	let mut tmp = [MaybeUninit::<u8>::uninit(); 1024];
	(&mut buffer1[..160]).copy_from_slice(&data[..160]);
	assert_eq!(ectx.encrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer2[..176], &buffer1[..160]), 0),
		Ok(176));
	let tmp = dctx.decrypt_buffer_to_buffer(&nonce1, &[], &mut tmp, &buffer2[..176]).unwrap();
	assert!(tmp.len() == 160);
	for i in 0..160 { assert_eq!(tmp[i], data[i]); }

	//Buffer to buffer encrypt with extra pad.
	let mut tmp = [MaybeUninit::<u8>::uninit(); 1024];
	(&mut buffer1[..160]).copy_from_slice(&data[..160]);
	let tmp = ectx.encrypt_buffer_to_buffer(&nonce1, &[], &mut tmp, &buffer1[..160], Some((data[160], 0))).
		unwrap();
	assert_eq!(dctx.decrypt2(&nonce1, &[], InputOutputBuffer::Double(&mut buffer2[..161], tmp)), Ok(161));
	for i in 0..161 { assert_eq!(buffer2[i], data[i]); }
}

#[cfg(test)]
fn test_aes_gcm(variant: ProtectorType, keysize: usize)
{
	let key = vec![1;keysize];
	let mut key2 = vec![1;keysize];
	key2[0] = 2;
	let ectx = variant.new_encryptor(&key).unwrap();
	let dctx = variant.new_decryptor(&key).unwrap();
	let dctx2 = variant.new_decryptor(&key2).unwrap();
	assert_eq!(variant.get_key_length(), keysize);
	assert_eq!(variant.get_nonce_length(), 12);
	assert_eq!(variant.get_tag_length(), 16);
	assert_eq!(variant.is_gcm_style(), true);
	test_tail(ectx, dctx, dctx2);
}

#[test]
fn test_aes_128_gcm()
{
	let key16 = [0;16];
	let key24 = [0;24];
	let key32 = [0;32];
	assert!(ProtectorType::Aes128Gcm.new_encryptor(&key16).is_ok());
	assert!(ProtectorType::Aes128Gcm.new_encryptor(&key24).is_err());
	assert!(ProtectorType::Aes128Gcm.new_encryptor(&key32).is_err());
	assert!(ProtectorType::Aes128Gcm.new_decryptor(&key16).is_ok());
	assert!(ProtectorType::Aes128Gcm.new_decryptor(&key24).is_err());
	assert!(ProtectorType::Aes128Gcm.new_decryptor(&key32).is_err());
	test_aes_gcm(ProtectorType::Aes128Gcm, 16);
}

#[test]
fn test_aes_192_gcm()
{
	let key16 = [0;16];
	let key24 = [0;24];
	let key32 = [0;32];
	assert!(ProtectorType::Aes192Gcm.new_encryptor(&key16).is_err());
	assert!(ProtectorType::Aes192Gcm.new_encryptor(&key24).is_ok());
	assert!(ProtectorType::Aes192Gcm.new_encryptor(&key32).is_err());
	assert!(ProtectorType::Aes192Gcm.new_decryptor(&key16).is_err());
	assert!(ProtectorType::Aes192Gcm.new_decryptor(&key24).is_ok());
	assert!(ProtectorType::Aes192Gcm.new_decryptor(&key32).is_err());
	test_aes_gcm(ProtectorType::Aes192Gcm, 24);
}

#[test]
fn test_aes_256_gcm()
{
	let key16 = [0;16];
	let key24 = [0;24];
	let key32 = [0;32];
	assert!(ProtectorType::Aes256Gcm.new_encryptor(&key16).is_err());
	assert!(ProtectorType::Aes256Gcm.new_encryptor(&key24).is_err());
	assert!(ProtectorType::Aes256Gcm.new_encryptor(&key32).is_ok());
	assert!(ProtectorType::Aes256Gcm.new_decryptor(&key16).is_err());
	assert!(ProtectorType::Aes256Gcm.new_decryptor(&key24).is_err());
	assert!(ProtectorType::Aes256Gcm.new_decryptor(&key32).is_ok());
	test_aes_gcm(ProtectorType::Aes256Gcm, 32);
}

#[test]
fn test_chacha20()
{
	let key16 = [0;16];
	assert!(ProtectorType::Chacha20Poly1305.new_encryptor(&key16).is_err());
	assert!(ProtectorType::Chacha20Poly1305.new_decryptor(&key16).is_err());
	let key = [1;32];
	let mut key2 = [1;32];
	key2[0] = 2;
	let ectx = ProtectorType::Chacha20Poly1305.new_encryptor(&key).unwrap();
	let dctx = ProtectorType::Chacha20Poly1305.new_decryptor(&key).unwrap();
	let dctx2 = ProtectorType::Chacha20Poly1305.new_decryptor(&key2).unwrap();
	assert_eq!(ProtectorType::Chacha20Poly1305.get_key_length(), 32);
	assert_eq!(ProtectorType::Chacha20Poly1305.get_nonce_length(), 12);
	assert_eq!(ProtectorType::Chacha20Poly1305.get_tag_length(), 16);
	assert_eq!(ProtectorType::Chacha20Poly1305.is_gcm_style(), false);
	test_tail(ectx, dctx, dctx2);
}
