//!Various routines related to hashes and especially TLS PRF hashes.
//!
//!This crate contains implementations of various hashes and their applications to TLS PRF.
#![deny(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]

use btls_aux_fail::f_return;
use btls_aux_fail::fail_if_none;
use btls_aux_memory::copy_maximum;
use btls_aux_set2::SetItem as SetItem2;
use btls_aux_set2::SetOf;
use btls_aux_tls_iana::CipherSuite;
use btls_aux_tls_iana::CipherSuiteMac;
use core::ops::Deref;
use core::ops::DerefMut;


///Trait for byte array related to hash.
///
/// # Unsafety:
///
///Implementations MUST uphold the following invariants:
///
/// * `as_xput_ptr()` returns pointer valid for `len()` readable bytes.
/// * `as_xput_mut_ptr()` returns pointer valid for `len()` readable/writable bytes.
pub unsafe trait HashArrayT
{
	///Return all zeroes.
	fn zeroed() -> Self;
	///Return hash output as slice.
	fn as_xput_slice(&self) -> &[u8];
	///Return hash output as mutable slice.
	fn as_xput_slice_mut(&mut self) -> &mut [u8];
	///Return length of hash.
	fn xput_len(&self) -> usize;
	///Return raw pointer to hash.
	fn as_xput_ptr(&self) -> *const u8;
	///Return raw pointer to hash.
	fn as_xput_mut_ptr(&mut self) -> *mut u8;
}

///Trait for raw hash outputs.
pub trait HashOutputT: HashArrayT
{
	///Cast to arbitrary-length output.
	fn as_output_any(&self) -> HashOutput2;
}

///Trait for raw hash blocks.
pub trait HashBlockT: HashArrayT
{
	///Cast to arbitrary-length block.
	fn as_input_any(&self) -> HashBlock2;
}

#[doc(hidden)]
pub trait HashUpdate
{
	#[doc(hidden)]
	fn __update(&mut self, block: &[u8]);
}

#[doc(hidden)]
pub trait GetHashFunction
{
	#[doc(hidden)]
	fn _function() -> HashFunction2;
	#[doc(hidden)]
	fn _as_string() -> &'static str;
}

///The largest possible hash block size.
pub const MAX_HASH_BLOCK: usize = _MAX_HASH_BLOCK;
///The largest possible hash output size.
pub const MAX_HASH_OUTPUT: usize = _MAX_HASH_OUTPUT;

///Output from hash function.
#[derive(Clone)] pub struct HashOutput2(_HashOutput);
///Input block for an hash function.
///
///`[u8;N]`, where `N` is any valid length of hash input block implements the [`HashBlockT`] trait, which has the
///[`HashBlockT::as_input_any()`] method that can be used to construct this. Another way to construct this is the
///[`HashFunction2::hmac_precompress_key()`] method.
///
///This is mainly intended to hold key for HMAC in bounded amount of space, even if size of the key itself is not
///bounded.
///
///[`HashBlockT`]:trait.HashBlockT.html
///[`HashBlockT::as_input_any()`]:trait.HashBlockT.html#tymethod.as_input_any
///[`HashFunction2::hmac_precompress_key()`]:struct.HashFunction2.html#method.hmac_precompress_key
#[derive(Clone)] pub struct HashBlock2(_HashBlock);
///A hash function
#[derive(Copy,Clone,Debug,PartialEq,Eq)] pub struct HashFunction2(_HashFunction);
///Hash calculation context.
///
///This context represents hash calculation in progress. It can be cloned for forked hash.
#[derive(Clone)] pub struct HashContext2(_HashContext);

unsafe impl btls_aux_memory::TrustedInvariantLength for HashOutput2
{
	fn as_byte_slice(&self) -> &[u8] { self.0.deref() }
}

unsafe impl<'a> btls_aux_memory::TrustedInvariantLength for &'a HashOutput2
{
	fn as_byte_slice(&self) -> &[u8] { self.0.deref() }
}

impl Deref for HashOutput2
{
	type Target = [u8];
	fn deref(&self) -> &[u8] { self.0.deref() }
}

impl DerefMut for HashOutput2
{
	fn deref_mut(&mut self) -> &mut [u8] { self.0.deref_mut() }
}

impl Deref for HashBlock2
{
	type Target = [u8];
	fn deref(&self) -> &[u8] { self.0.deref() }
}

impl HashContext2
{
	///Append data to be hashed.
	///
	/// # Parameters:
	///
	/// * `data`: The data to append.
	pub fn append(&mut self, data: &[u8]) { self.0.append(data) }
	///Append data to be hashed.
	///
	///The actual data appended is the concatenation of the slices passed.
	///
	/// # Parameters:
	///
	/// * `data`: The data to append.
	pub fn append_v(&mut self, data: &[&[u8]]) { self.0.append_v(data) }
	///Get hash output for data added so far.
	///
	/// # Return value:
	///
	///The hash value.
	///
	/// # Notes:
	///
	/// * The current state is not modified (multitap hash). As consequence, multiple back-to-back calls return
	///the same hash.
	pub fn output(&self) -> HashOutput2 { HashOutput2(self.0.output()) }
	///Obtain the hash function this context is for.
	///
	/// # Return value:
	///
	///The hash function.
	pub fn function(&self) -> HashFunction2 { HashFunction2(self.0.function()) }
	///Reset the hash context to empty.
	///
	/// # Notes:
	///
	/// * The state of hash context immediately after reset is the same as hash context immediately after creation.
	pub fn reset(&mut self) { self.0.reset() }
	
}

///Type of hash function.
#[derive(Copy,Clone,Debug)]
pub struct HashType(_HashType);

impl HashType
{
	///Get the default-enabled hash type set.
	///
	/// # Return value:
	///
	///hash type set.
	pub const fn default_set() -> SetOf<HashType>
	{
		btls_aux_set2::set_from_raw_storage!(_HASH_NORMAL_MASK)
	}
	///Get the enabled hash type set for NSA mode.
	///
	/// # Return value:
	///
	///hash type set.
	pub const fn nsa_set() -> SetOf<HashType>
	{
		btls_aux_set2::set_from_raw_storage!(NSA_KEY)
	}
}

impl SetItem2 for HashType
{
	type Storage = _HashTypeSetStorage;
	fn __sign_bit(&self) -> usize { self.0.sign_bit() as usize }
	fn __universe() -> &'static [HashType] { _HFT_UNIVERSE }
	fn __universe_bits() -> _HashTypeSetStorage { _HFT_UNIVERSE_BITS }
	fn __by_internal_name(name: &str) -> Option<usize>
	{
		_HashType::by_internal_name(name).map(|x|{
			x as usize
		})
	}
}

///The OR of to_bitmask() of all default algorithms.
pub const DEFAULT_HASH_BITMASK: u64 = _DEFAULT_HASHALGO_MASK;

///Hash enable policy.
#[derive(Copy,Clone,Debug)]
pub struct HashFunctionEnabled(SetOf<HashType>);

impl HashFunctionEnabled
{
	///Construct policy that allows nothing.
	///
	/// # Return value
	///
	///The policy.
	pub fn none() -> HashFunctionEnabled { HashFunctionEnabled(SetOf::empty_set()) }
	///Construct policy with specified mask.
	///
	/// # Parameters:
	///
	/// * `mask`: Bitmask of hash functions to enable. This is bitwise OR of values from
	///[`HashFunction2::to_bitmask()`] or [`bitmask_by_name()`].
	///
	/// # Return value
	///
	///The policy.
	///
	///[`HashFunction2::to_bitmask()`]:struct.HashFunction2.html#method.to_bitmask
	///[`bitmask_by_name()`]:#method.bitmask_by_name
	pub fn from_mask(mask: u64) -> HashFunctionEnabled { HashFunctionEnabled(SetOf::from_raw_mask(mask)) }
	///Construct hash policy unsafely allowing absolutely everything.
	///
	/// # Return value
	///
	///The policy.
	pub fn unsafe_any_policy() -> HashFunctionEnabled { HashFunctionEnabled(!SetOf::empty_set()) }
	///Construct policy for NSA mode.
	///
	/// # Return value
	///
	///The policy.
	pub fn nsa_mode() -> HashFunctionEnabled { HashFunctionEnabled(HashType::nsa_set()) }
	#[allow(missing_docs)]
	#[deprecated(since="1.16.0", note="Use bit_by_name() instead")]
	pub fn bitmask_by_name(name: &str) -> Option<u64>
	{
		let item = SetOf::<HashType>::singleton_by_name(name)?.to_raw_mask();
		fail_if_none!(item == 0);
		Some(item)
	}
	///The bitmask bit for specified algorithm.
	///
	/// # Parameters:
	///
	/// * `name`: The internal name of the algorithm.
	///
	/// # Error conditions:
	///
	/// * The algorithm name is unknown.
	///
	/// # Return value:
	///
	///The bitmask bit number.
	pub fn bit_by_name(name: &str) -> Option<u32>
	{
		_HashType::by_internal_name(name)
	}
	///Construct hash policy from set of hash types.
	///
	/// # Parameters:
	///
	/// * `set`: Set of hash types to enable.
	///
	/// # Return value:
	///
	///Hash policy.
	pub fn from_set(set: SetOf<HashType>) -> HashFunctionEnabled { HashFunctionEnabled(set) }
	///Does the policy allow specified hash?
	///
	/// # Parameters:
	///
	/// * `func`: The hash function to check.
	///
	/// # Return value:
	///
	///`true` if allowed, `false` otherwise.
	pub fn allows(&self, func: HashFunction2) -> bool { self._allows(func.0) }
}

impl HashFunction2
{
	#[doc(hidden)]
	pub fn __unstable_discriminant(&self) -> __UnstableDiscriminant { self.0.__unstable_discriminant() }
	///Construct hash function used by specific TLS ciphersuite.
	///
	/// # Parameters:
	///
	/// * `cs`: The TLS cipher suite.
	///
	/// # Error conditions:
	///
	/// * The specified cipher suite is not known.
	/// * The specified cipher suite uses unsupported MAC.
	///
	/// # Return value:
	///
	///The hash function used.
	pub fn by_tls_ciphersuite(cs: CipherSuite) -> Option<HashFunction2>
	{
		Self::by_prf(cs.mac())
	}
	///Construct hash function used by used by specific MAC type.
	///
	/// # Parameters:
	///
	/// * `c`: The TLS cipher suite MAC type.
	///
	/// # Error conditions:
	///
	/// * The specified MAC is not known.
	/// * The specified MAC is unsupported.
	///
	/// # Return value:
	///
	///The hash function used.
	///
	/// # Notes:
	///
	/// * Currently the only supported MACs are `Sha256` and `Sha384`.
	pub fn by_prf(c: CipherSuiteMac) -> Option<HashFunction2>
	{
		Some(match c {
			CipherSuiteMac::Sha256 => HashFunction2(_HashFunction::Sha256),
			CipherSuiteMac::Sha384 => HashFunction2(_HashFunction::Sha384),
			_ => return None
		})
	}
	///Create HashFunction by internal name
	///
	/// # Parameters:
	///
	/// * `name`: The name of the algorithm.
	///
	/// # Error conditions:
	///
	/// * The name of the hash is not recogized.
	///
	/// # Return value:
	///
	///The hash function.
	pub fn by_internal_name(name: &str) -> Option<HashFunction2>
	{
		_HashFunction::by_internal_name(name).map(HashFunction2)
	}
	///Create a new hash computation context.
	///
	///# Return value:
	///
	///A new context with empty data and this hash function.
	pub fn context(&self) -> HashContext2 { HashContext2(self.0.context()) }
	///The input block size of the hash function.
	///
	/// # Return value:
	///
	///The blocksize is bytes.
	///
	/// # Notes:
	///
	/// * HMAC uses the input block size when padding the key.
	/// * For example, `SHA-256` gives 64 (512/8=64).
	pub fn input_length(&self) -> usize { self.0.input_length() }
	///The output size of the hash function.
	///
	/// # Return value:
	///
	///The output size in bytes.
	///
	/// # Notes:
	///
	/// * For example, `SHA-256` gives 32 (256/8=32).
	pub fn output_length(&self) -> usize { self.0.output_length() }
	///The TLS 1.3 handshake secret size of the hash function.
	///
	/// # Return value:
	///
	///The secret size in bytes.
	///
	/// # Notes:
	///
	/// * This is usually the same as `output_length()`, but might differ in some cases.
	pub fn tls13_secret_length(&self) -> usize { self.0.tls13_secret_length() }
	///The internal name of the hash.
	///
	/// # Return value:
	///
	///The internal name.
	pub fn name(&self) -> &'static str { self.0.name() }
	///The name of the hash (for human-readable purposes).
	///
	/// # Return value:
	///
	///The human-readable name.
	pub fn as_string(&self) -> &'static str { self.0.as_string() }
	///The bitmask for the algorithm.
	///
	/// # Return value:
	///
	///The bitmask that includes just this algorithm.
	///
	/// # Notes:
	///
	/// * The return value is always power of two.
	/// * The bitmasks can be passed to [`HashFunctionEnabled::from_mask()`].
	///
	///[`HashFunctionEnabled::from_mask()`]:struct.HashFunctionEnabled.html#method.from_mask
	pub fn to_bitmask(&self) -> u64 { self.0.to_bitmask() }
	///Get type of this hash.
	///
	/// # Return value:
	///
	///Type of hash.
	pub fn hash_type(&self) -> HashType { HashType(self.0.hash_type()) }
	///Construct output block that is all-zero.
	///
	///The length of output block is the same as the output length of the hash.
	///
	/// # Return values:
	///
	///The output block.
	pub fn zeroes(&self) -> HashOutput2 { HashOutput2(self.0.zeroes()) }
	///Construct TLS 1.3 handshake secret block that is all-zero.
	///
	/// # Return values:
	///
	///The block.
	pub fn zeroes_tls13_handshake(&self) -> HashOutput2 { HashOutput2(self.0.zeroes_tls13hs()) }
	///Perform one-shot computation of the hash on specified data.
	///
	/// # Parameters:
	///
	/// * `data`: The data to compute the hash on.
	///
	/// # Return value:
	///
	///The hash value.
	pub fn hash(&self, data: &[u8]) -> HashOutput2 { HashOutput2(self.0.hash(data)) }
	///Perform one-shot computation of the hash on specified data.
	///
	///The slices in the input slice are concatenated.
	///
	/// # Parameters:
	///
	/// * `data`: The data to compute the hash on.
	///
	/// # Return value:
	///
	///The hash value.
	pub fn hash_v(&self, data: &[&[u8]]) -> HashOutput2 { HashOutput2(self.0.hash_v(data)) }
	///Perform one-shot computation of the hash on specified data, given by a callback.
	///
	/// # Parameters:
	///
	/// * `data`: Callback that suppiles the data to hash via calling the [`HashAction::append()`] method on its
	///argument.
	///
	/// # Return value:
	///
	///The hash value.
	///
	///[`HashAction::append()`]:struct.HashAction.html#method.append
	pub fn hash_c(&self, data: impl FnOnce(&mut HashAction<HashActionAny>)) -> HashOutput2
	{
		HashOutput2(self.0.hash_c(data))
	}
	///Precompress the HMAC key.
	///
	///For given HMAC key of unbounded length, This function calculates equivalent key of bounded length. This
	///is possible due to the internal structure of HMAC.
	///
	/// # Parameters:
	///
	/// * `key`: The HMAC key.
	///
	/// # Return value:
	///
	///HMAC key equivalent to the `key`.
	pub fn hmac_precompress_key(&self, key: &[u8]) -> HashBlock2
	{
		HashBlock2(self.0.hmac_precompress_key(key))
	}
	///Perform one-shot computation of HMAC on specified data.
	///
	/// # Parameters:
	///
	/// * `key`: The HMAC key.
	/// * `data`: The data to compute the hash on.
	///
	/// # Return value:
	///
	///The MAC value.
	pub fn hmac(&self, key: &[u8], data: &[u8]) -> HashOutput2 { HashOutput2(self.0.hmac(key, data)) }
	///Perform one-shot computation of HMAC on specified data.
	///
	///The slices in the input slice are concatenated.
	///
	/// # Parameters:
	///
	/// * `key`: The HMAC key.
	/// * `data`: The data to compute the hash on.
	///
	/// # Return value:
	///
	///The MAC value.
	pub fn hmac_v(&self, key: &[u8], data: &[&[u8]]) -> HashOutput2 { HashOutput2(self.0.hmac_v(key, data)) }
	///Perform one-shot computation of HMAC on specified data, given by a callback.
	///
	/// # Parameters:
	///
	/// * `key`: The HMAC key.
	/// * `data`: Callback that suppiles the data to MAC via calling the [`HashAction::append()`] method on its
	///argument.
	///
	/// # Return value:
	///
	///The MAC value.
	///
	///[`HashAction::append()`]:struct.HashAction.html#method.append
	pub fn hmac_c(&self, key: &[u8], data: impl FnOnce(&mut HashAction<HashActionAny>)) -> HashOutput2
	{
		HashOutput2(self.0.hmac_c(key, data))
	}
	///Compute the `TLS 1.3` `derive-secret` function.
	///
	/// # Parameters:
	///
	/// * `key`: The input key to use.
	/// * `label`: The label to use. The slices are concatenated.
	/// * `context`: The context value to use.
	///
	/// # Return value:
	///
	///The derive-secret output.
	pub fn tls13_derive_secret(&self, key: &[u8], label: &[&str], context: &[u8]) -> HashOutput2
	{
		HashOutput2(self.0.tls13_derive_secret(key, label, context))
	}
	///Compute the `TLS 1.3` "HMAC" function.
	///
	/// # Parameters:
	///
	/// * `key`: The input key to use.
	/// * `data`: The data to MAC.
	///
	/// # Return value:
	///
	///The MAC.
	///
	/// # Notes:
	///
	/// * This method is usually the same as [`hmac()`], but is seperate in case the TLS specification specifying
	///its use in `TLS 1.3` specifies something special to replace the HMAC.
	///
	///[`hmac()`]:#method.hmac
	pub fn tls13_hmac(&self, key: &[u8], data: &[u8]) -> HashOutput2
	{
		HashOutput2(self.0.tls13_hmac(key, data))
	}
	///Compute the `TLS 1.3` `hkdf-expand-label` function.
	///
	/// # Parameters:
	///
	/// * `key`: The input key to use.
	/// * `label`: The label to use. The slices are concatenated.
	/// * `context`: The context value to use.
	/// * `output`: The buffer to store the output to.
	///
	/// # Notes:
	///
	/// * The output size is picked from the length of `output`.
	/// * This is usually based on `HKDF-expand`, but may be different in case TLS specification for its use
	///in `TLS 1.3` specifies something different.
	pub fn tls13_hkdf_expand_label(&self, key: &[u8], label: &[&str], context: &[u8], output: &mut [u8])
	{
		self.0.tls13_hkdf_expand_label(key, label, context, output)
	}
	///Compute the `TLS 1.3` `hkdf-extract` function.
	///
	/// # Parameters:
	///
	/// * `key`: The input key to use.
	/// * `salt`: The salt to use.
	///
	/// # Return value:
	///
	///Extracted key.
	///
	/// # Notes:
	///
	/// * This is usually `HKDF-extract`, but may be different in case TLS specification for its use
	///in `TLS 1.3` specifies something different.
	pub fn tls13_hkdf_extract(&self, key: &[u8], salt: &[u8]) -> HashOutput2
	{
		HashOutput2(self.0.tls13_hkdf_extract(key, salt))
	}
	///Compute the `TLS 1.2` `tls-prf` function.
	///
	/// # Parameters:
	///
	/// * `key`: The input key to use.
	/// * `label`: The label to use.
	/// * `data`: Callback that suppiles the data to PRF via calling the [`HashAction::append()`] method on its
	///argument.
	/// * `output`: The buffer to store the output to.
	///
	/// # Notes:
	///
	/// * The output size is picked from the length of `output`.
	pub fn tls_prf(&self, key: &[u8], label: &str, data: impl Fn(&mut HashAction<HashActionAny>),
		output: &mut [u8])
	{
		self.0.tls_prf(key, label, data, output)
	}
	///Is NSA hash?
	///
	/// # Return value:
	///
	///`true` if hash function is allowed in `CNSA`, `false` otherwise.
	pub fn is_nsa(&self) -> bool { self.0.is_nsa() }
	///Get maximum allowed output length of tls13_hkdf_expand_label().
	///
	/// # Return value:
	///
	///The maximum allowed output length of tls13_hkdf_expand_label(), which may be usize::MAX.
	pub fn tls13_hkdf_expand_label_max(&self) -> usize
	{
		self.0.tls13_hkdf_expand_label_max()
	}
	#[allow(missing_docs)]
	#[deprecated(since="1.14.0", note="Use tls13_hkdf_extract() instead")]
	pub fn hkdf_extract(&self, key: &[u8], salt: &[u8]) -> HashOutput2
	{
		HashOutput2(self.0.hmac(salt, key))
	}
	#[allow(missing_docs)]
	#[deprecated(since="1.14.0", note="Use tls13_hkdf_expand_label() instead")]
	pub fn hkdf_expand(&self, key: &[u8], data: impl Fn(&mut HashAction<HashActionAny>), output: &mut [u8])
	{
		self.0.hkdf_expand(key, data, output)
	}
	#[allow(missing_docs)]
	#[deprecated(since="1.11.1", note="Insane API function")]
	pub fn ecdsa_sha3_fixup(&self, h: &mut HashOutput2)
	{
		if !self.0.is_sha3() { return; }	//Do not do anything with non-SHA3.
		let h = h.deref_mut();
		//Hopefully this gets SIMD-optimized by the compiler.
		for i in 0..h.len() {
			let mut x = h[i];
			x = x >> 4 & 0x0F | x << 4 & 0xF0;
			x = x >> 2 & 0x33 | x << 2 & 0xCC;
			x = x >> 1 & 0x55 | x << 1 & 0xAA;
			h[i] = x;
		}
	}
}

#[doc(hidden)]
pub struct HashActionAny<'a,'b>(_HashActionAny<'a,'b>);

impl<'a,'b> HashUpdate for HashActionAny<'a,'b>
{
	fn __update(&mut self, block: &[u8]) { self.0.__update(block) }
}

///Append data to be hashed from callback.
///
///The hash methods that supply data to hash by callback uses this as the context type to collect the data to hash.
pub struct HashAction<'a,T:HashUpdate>(&'a mut T);

impl<'a,T:HashUpdate> HashAction<'a,T>
{
	///Append data to be hashed.
	///
	/// # Parameters:
	///
	/// * `block`: The block of data to append.
	pub fn append(&mut self, block: &[u8]) { self.0.__update(block); }
}

///Trait for hash function context for specific hash.
///
/// # Safety:
///
/// * The `Output` type MUST be at most as big as `Input` type.
/// * Don't override any implementations.
/// * It might be tempting to override `tls_prf()` for custom TLS 1.2 PRF. DO NOT DO THIS, IT WILL NOT WORK.
///Custom TLS 1.2 PRFs CAN NOT BE SUPPORTED.
pub unsafe trait Hash: HashUpdate+GetHashFunction+Clone
{
	///The type of TLS 1.3 verify_data.
	type Tls13VerifyData: HashOutputT+AsRef<[u8]>+Copy;
	///The type of TLS 1.3 handshake keys.
	type Tls13HandshakeKey: HashOutputT+AsRef<[u8]>+Copy;
	///The output type of the hash.
	type Output: HashOutputT+AsRef<[u8]>+Copy;
	///The input block type of the hash.
	type Input: HashBlockT+AsRef<[u8]>+Copy;
	#[doc(hidden)]
	fn __initialize() -> Self;
	#[doc(hidden)]
	fn __finalize(self) -> <Self as Hash>::Output;
	///Create a new hash context.
	///
	///Returns a hash context with initially no data in it.
	fn new() -> Self { Self::__initialize() }
	///Append data to be hashed.
	///
	///Parameters:
	///
	/// * `data`: The data to append.
	fn append(&mut self, data: &[u8]) { self.__update(data) }
	///Append data to be hashed.
	///
	///Parameters:
	///
	/// * `data`: The data to append. Fragments are concatenated.
	fn append_v(&mut self, data: &[&[u8]]) { for block in data.iter() { self.__update(block) } }
	///Get intermediate hash.
	///
	///Returns hash of data added to context so far.
	fn output(&self) -> <Self as Hash>::Output { self.clone().__finalize() }
	///Get block of zeroes.
	///
	///Returns one hash output block worth of zero bytes.
	fn zeroes() -> <Self as Hash>::Output { <Self as Hash>::Output::zeroed() }
	///Get TLS 1.3 handshake secret full of zeroes.
	///
	///Returns one TLS 1.3 handshake secret worth of zero bytes.
	fn zeroes_tls13_handshake() -> <Self as Hash>::Output { <Self as Hash>::Output::zeroed() }
	///Compute hash.
	///
	///Parameters:
	///
	/// * `data`: The data to hash.
	///
	///Returns the hash.
	fn hash(data: &[u8]) -> <Self as Hash>::Output { Self::hash_c(|h|h.append(data)) }
	///Compute hash.
	///
	///Parameters:
	///
	/// * `data`: The data to hash. The parts are concatenated.
	///
	///Returns the hash.
	fn hash_v(data: &[&[u8]]) -> <Self as Hash>::Output
	{
		Self::hash_c(|h|for block in data { h.append(block); })
	}
	///Compute hash.
	///
	///Parameters:
	///
	/// * `data`: The data closure.
	///
	///Returns the hash.
	fn hash_c(data: impl FnOnce(&mut HashAction<Self>)) -> <Self as Hash>::Output
	{
		let mut h = Self::__initialize();
		data(&mut HashAction(&mut h));
		h.__finalize()
	}
	///Precompress HMAC key.
	///
	///The idea is hmac_H(k, x) = hmac_H(precompress_H(k), x)), but precompress_H(k) gives fixed-length output.
	///
	///Parameters:
	///
	/// * `key`: The key to compress.
	///
	///Returns the compressed key.
	fn hmac_precompress_key(key: &[u8]) -> <Self as Hash>::Input
	{
		let mut fblock = Self::Input::zeroed();
		let bbytes = fblock.xput_len();
		if key.len() > bbytes {
			let h = Self::hash(key);
			copy_maximum(fblock.as_xput_slice_mut(), h.as_xput_slice()); 
		} else {
			copy_maximum(fblock.as_xput_slice_mut(), key);
		}
		fblock
	}
	///Compute HMAC.
	///
	///Parameters:
	///
	/// * `key`: The HMAC key to use.
	/// * `data`: The data to HMAC.
	///
	///Returns the MAC.
	fn hmac(key: &[u8], data: &[u8]) -> <Self as Hash>::Output { Self::hmac_c(key, |h|h.append(data)) }
	///Compute HMAC.
	///
	///Parameters:
	///
	/// * `key`: The HMAC key to use.
	/// * `data`: The data to HMAC. The parts are concatenated.
	///
	///Returns the MAC.
	fn hmac_v(key: &[u8], data: &[&[u8]]) -> <Self as Hash>::Output
	{
		Self::hmac_c(key, |h|for block in data { h.append(block); })
	}
	///Compute HMAC.
	///
	///Parameters:
	///
	/// * `key`: The HMAC key to use.
	/// * `data`: The data closure.
	///
	///Returns the MAC.
	fn hmac_c(key: &[u8], data: impl FnOnce(&mut HashAction<Self>)) -> <Self as Hash>::Output
	{
		//HMAC first ensures that key length is at most length of input block by hashing it if it is
		//longer (notice this is not cryptographically kosher).
		let mut fblock = Self::hmac_precompress_key(key);
		//Perform the actual HMAC.
		for b in fblock.as_xput_slice_mut().iter_mut() { *b ^= 0x36; }
		let mut inner = Self::__initialize();
		inner.__update(fblock.as_xput_slice());
		data(&mut HashAction(&mut inner));
		//opad is not 0x6a, but ipad^opad is 0x6a.
		for b in fblock.as_xput_slice_mut().iter_mut() { *b ^= 0x6A; }
		let mut outer = Self::__initialize();
		outer.__update(fblock.as_xput_slice());
		outer.__update(inner.__finalize().as_xput_slice());
		outer.__finalize()
	}
	///Compute TLS 1.3 Derive-Secret
	///
	///Parameters:
	///
	/// * `key`: The master key to use.
	/// * `label`: The label to use. The parts are concatenated. Should not exceed 255 bytes.
	/// * `context`: The context information (transcript hash). Should not exceed 255 bytes.
	///
	///Returns the computed secret.
	fn tls13_derive_secret(key: &[u8], label: &[&str], context: &[u8]) -> <Self as Hash>::Tls13HandshakeKey
	{
		let mut output = Self::Tls13HandshakeKey::zeroed();
		Self::tls13_hkdf_expand_label(key, label, context, output.as_xput_slice_mut());
		output
	}
	///Compute TLS 1.3 HMAC.
	///
	/// # Parameters:
	///
	/// * `key`: The finished key to use.
	/// * `data`: The transcript hash.
	///
	/// # Return value:
	///
	///The MAC.
	///
	/// # Notes:
	///
	/// * This is only used to compute the verify_data in Finished message (and PSK binder).
	fn tls13_hmac(key: &[u8], data: &[u8]) -> <Self as Hash>::Tls13VerifyData;
	///Compute TLS 1.3 HKDF-Expand-Label
	///
	///Parameters:
	///
	/// * `key`: The master key to use.
	/// * `label`: The label to use. The parts are concatenated. Should not exceed 255 bytes.
	/// * `context`: The context information. Should not exceed 255 bytes.
	/// * `output`: The output buffer of suitable size. Should not exceed 255 output blocks nor 65535 bytes.
	fn tls13_hkdf_expand_label(key: &[u8], label: &[&str], context: &[u8], output: &mut [u8]);
	///Get maximum allowed output length of tls13_hkdf_expand_label().
	///
	/// # Return value:
	///
	///The maximum allowed output length of tls13_hkdf_expand_label(), which may be usize::MAX.
	fn tls13_hkdf_expand_label_max() -> usize;
	///Compute TLS 1.3 HDKF-Extract
	///
	///Parameters:
	///
	/// * `key`: The master key to use.
	/// * `salt`: Salt.
	///
	///Returns the extracted key.
	fn tls13_hkdf_extract(key: &[u8], salt: &[u8]) -> <Self as Hash>::Tls13HandshakeKey;
	///Compute TLS 1.2 PRF.
	///
	///Parameters:
	///
	/// * `key`: The master key to use.
	/// * `label`: The label to use.
	/// * `data`: The context information closure.
	/// * `output`: The output buffer of suitable size.
	fn tls_prf(key: &[u8], label: &str, data: impl Fn(&mut HashAction<Self>), mut output: &mut [u8])
	{
		let mut a = Self::hmac_c(key, |x|{
			x.append(label.as_bytes());
			data(x);
		});
		loop {
			let b = Self::hmac_c(key, |x|{
				x.append(a.as_xput_slice());
				x.append(label.as_bytes());
				data(x);
			});
			output = f_return!(fill_slice(output, b.as_xput_slice()));
			a = Self::hmac(key, a.as_xput_slice());
		}
	}
	///Get handle for this hash function.
	fn function() -> HashFunction2 { Self::_function() }
	///Get name of this hash function.
	fn as_string() -> &'static str { <Self as GetHashFunction>::_as_string() }
	#[allow(missing_docs)]
	#[deprecated(since="1.14.0", note="Use tls13_hkdf_extract() instead")]
	fn hkdf_extract(key: &[u8], salt: &[u8]) -> <Self as Hash>::Output { Self::hmac(salt, key) }
	#[allow(missing_docs)]
	#[deprecated(since="1.14.0", note="Use tls13_hkdf_expand_label() instead")]
	fn hkdf_expand(key: &[u8], data: impl Fn(&mut HashAction<Self>), output: &mut [u8])
	{
		Self::__hkdf_expand(key, data, output)
	}
	#[doc(hidden)]
	fn __hkdf_expand(key: &[u8], data: impl Fn(&mut HashAction<Self>), mut output: &mut [u8])
	{
		let mut b = Self::hmac_c(key, |x|{
			data(x);
			x.append(&[1u8]);
		});
		output = f_return!(fill_slice(output, b.as_xput_slice()));
		let mut c = [1u8];
		loop {
			c[0] = c[0].wrapping_add(1);
			b = Self::hmac_c(key, |x|{
				x.append(b.as_xput_slice());
				data(x);
				x.append(&c);
			});
			output = f_return!(fill_slice(output, b.as_xput_slice()));
		}
	}
}

fn __std_tls13_hmac<H:Hash>(key: &[u8], data: &[u8]) -> <H as Hash>::Output
{
	H::hmac_c(key, |h|h.append(data))
}

fn __std_tls13_hkdf_expand_label<H:Hash>(key: &[u8], label: &[&str], context: &[u8], output: &mut [u8])
{
	let labellen = __calculate_label_len(label);
	let outlen = output.len();
	H::__hkdf_expand(key, |x|__format_hkdf_input(x, outlen, labellen, label, context),  output);
}

fn __format_hkdf_input<H:HashUpdate>(x: &mut HashAction<H>, outlen: usize, labellen: usize, label: &[&str],
	context: &[u8])
{
	x.append(&[(outlen >> 8) as u8, outlen as u8, labellen as u8]);
	for frag in label.iter() { x.append(frag.as_bytes()); }
	x.append(&[context.len() as u8]);
	x.append(context);
}

fn __calculate_label_len(label: &[&str]) -> usize
{
	label.iter().fold(0, |sofar, frag|sofar + frag.len())
}


fn __std_tls13_hkdf_expand_label_max<H:Hash>() -> usize
{
	255 * <H as Hash>::Output::zeroed().xput_len()
}

fn __std_tls13_hkdf_extract<H:Hash>(key: &[u8], salt: &[u8]) -> <H as Hash>::Output
{
	H::hmac(salt, key)
}


///Compute SHA-1 checksum of input.
///
///WARNING: SHA-1 is broken. It is not to be used except for compatibility purposes (and one has to be careful not
///to implement anything that is fundamentally broken due to SHA-1).
///
///Parameters:
///
/// * `data`: The data to checksum.
///
///Returns the SHA-1 checksum.
pub fn sha1insecure(data: &[u8]) -> [u8;20]
{
	let mut ctx = btls_aux_nettle::Sha1Ctx::new();
	ctx.input(data);
	ctx.output()
}

///Compute SHA-256 checksum of input.
///
///WARNING: SHA-256 may be broken. There are no guarantees of security of the output.
///
///Parameters:
///
/// * `data`: The data to checksum.
///
///Returns the SHA-256 checksum.
pub fn sha256(data: &[u8]) -> [u8;32] { crate::Sha256::hash(data) }


//Set this to never inline to prevent optimizations due to context, and allow it to be inspected.
///Compare two slices in constant time.
///
///The two slices compared are `a` and `b`.
///
///Note: The constant-time guarantee only applies if the slices are of the same constant length. If lengths are
///different or variable, then the comparision can take variable time. In any case, the time taken does not depend
///on the values stored in slices.
///
///If `a`==`b`, returns `true`. Otherwise returns `false`.
#[inline(never)]
pub fn slice_eq_ct(a: &[u8], b: &[u8]) -> bool
{
	//TODO: This can be implemented more efficiently. This code compiles into byte loop.
	if a.len() != b.len() { return false; }
	let mut syndrome = 0;
	for i in a.iter().zip(b.iter()) { syndrome |= *i.0 ^ *i.1; }
	syndrome==0
}

fn fill_slice<'a>(output: &'a mut [u8], with: &[u8]) -> Option<&'a mut [u8]>
{
	if output.len() <= with.len() {
		//output.len() <= bwith.len(), so in-bounds.
		output.copy_from_slice(&with[..output.len()]);
		None		//Filled.
	} else {
		//output.len() > with.len().
		let (block, noutput) = output.split_at_mut(with.len());
		block.copy_from_slice(with);
		Some(noutput)	//Rest.
	}
}

include!(concat!(env!("OUT_DIR"), "/autogenerated.inc.rs"));


#[cfg(test)]
mod test;
