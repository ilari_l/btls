use super::*;
use core::ops::Deref;


fn test_hash_function(func: HashFunction2)
{
	let mut ctx = func.context();
	assert_eq!(ctx.function(), func);
	assert!(func.input_length() <= crate::_MAX_HASH_BLOCK);
	assert!(func.output_length() <= crate::_MAX_HASH_OUTPUT);
	assert_eq!(ctx.function(), func);
	let emptyhash = ctx.output();
	let emptyhash2 = ctx.output();
	assert_eq!(emptyhash.len(), func.output_length());
	assert_eq!(emptyhash.deref(), emptyhash2.deref());
	ctx.append("foo".as_bytes());
	ctx.append("bar".as_bytes());
	let foobarhash = ctx.output();
	let mut ctx2 = ctx.clone();
	ctx.reset();
	ctx.append("foobar".as_bytes());
	let foobarhash2 = ctx.output();
	assert_eq!(foobarhash.deref(), foobarhash2.deref());
	ctx.append("baz".as_bytes());
	ctx2.append("baz".as_bytes());
	let foobarbazhash = ctx.output();
	let foobarbazhash2 = ctx2.output();
	assert_eq!(foobarbazhash.deref(), foobarbazhash2.deref());
	ctx.reset();
	let emptyhash3 = ctx.output();
	assert_eq!(emptyhash.deref(), emptyhash3.deref());
}


#[test]
fn test_sha256_hash()
{
	let func = HashFunction2(_HashFunction::Sha256);
	test_hash_function(func);
	assert_eq!(func.input_length(), 64);
	assert_eq!(func.output_length(), 32);
}

#[test]
fn test_sha384_hash()
{
	let func = HashFunction2(_HashFunction::Sha384);
	test_hash_function(func);
	assert_eq!(func.input_length(), 128);
	assert_eq!(func.output_length(), 48);
}

#[test]
fn test_sha512_hash()
{
	let func = HashFunction2(_HashFunction::Sha512);
	test_hash_function(func);
	assert_eq!(func.input_length(), 128);
	assert_eq!(func.output_length(), 64);
}

#[test]
fn test_sha3_256_hash()
{
	let func = HashFunction2(_HashFunction::Sha3256);
	test_hash_function(func);
	assert_eq!(func.input_length(), 200-2*32);
	assert_eq!(func.output_length(), 32);
}

#[test]
fn test_sha3_384_hash()
{
	let func = HashFunction2(_HashFunction::Sha3384);
	test_hash_function(func);
	assert_eq!(func.input_length(), 200-2*48);
	assert_eq!(func.output_length(), 48);
}

#[test]
fn test_sha3_512_hash()
{
	let func = HashFunction2(_HashFunction::Sha3512);
	test_hash_function(func);
	assert_eq!(func.input_length(), 200-2*64);
	assert_eq!(func.output_length(), 64);
}

#[test]
fn test_reverse()
{
	for i in 0..256u16 {
		let mut x = i as u8;
		x = x >> 4 & 0x0F | x << 4 & 0xF0;
		x = x >> 2 & 0x33 | x << 2 & 0xCC;
		x = x >> 1 & 0x55 | x << 1 & 0xAA;
		assert_eq!(x, (i as u8).reverse_bits());
	}
}
