use btls_aux_codegen::Bitmask;
use btls_aux_codegen::CodeBlock;
use btls_aux_codegen::mkid;
use btls_aux_codegen::mkidf;
use btls_aux_codegen::quote;
use btls_aux_codegen::qwrite;
use btls_aux_codegen::Symbol as TokenTree;
use std::env;
use std::cmp::max;
use std::collections::BTreeMap;
use std::collections::BTreeSet;
use std::fs::File;
use std::path::PathBuf;

pub struct H
{
	pub name: &'static str,
	pub ilen: usize,
	pub olen: usize,
	pub hname: &'static str,
	pub family: &'static str,
	pub dflt: bool,
	pub imp: &'static str,
	pub ebit: &'static str,
	pub is_sha3: bool,
	pub nsa: bool,
	_d: (),
}

impl H
{
	fn symbol(&self) -> TokenTree { mkid(self.name) }
	fn help(&self) -> String
	{
		format!("{hname}\n\nThis is the `{family}` hash function with {bits} bit output.",
			hname=self.hname, family=self.family, bits=self.olen*8)
	}
	fn is_sha3(&self) -> bool { self.is_sha3 }
	fn is_nsa(&self) -> bool { self.nsa }
	fn inner(&self) -> CodeBlock {
		let frags = tovec_map(self.imp.split('!'), mkid);
		quote!(#(#frags)::*)	//Combine with ::.
	}
	fn ilen(&self) -> usize { self.ilen }
	fn olen(&self) -> usize { self.olen }
	fn isym(&self) -> TokenTree { mkidf!("I{ilen}", ilen=self.ilen) } 
	fn osym(&self) -> TokenTree { mkidf!("O{olen}", olen=self.olen) }
	fn hname(&self) -> String { self.hname.to_string() }
	fn iname(&self) -> String { format!("{hname}(PRF)", hname=self.hname) }
	fn hash_type(&self) -> TokenTree { mkid(self.ebit) }
	fn is_normal(h: &&Self) -> bool { h.dflt }
	fn is_nsa2(h: &&Self) -> bool { h.nsa }
}

struct Size(usize);

impl Size
{
	fn new(entry: &usize) -> Size { Size(*entry) }
	fn size(&self) -> usize { self.0 }
	fn isym(&self) -> TokenTree { mkidf!("I{ilen}", ilen=self.0) } 
	fn osym(&self) -> TokenTree { mkidf!("O{olen}", olen=self.0) }
}

struct Enable(&'static str, u32);

impl Enable
{
	fn new(entry: (&&'static str, &u32)) -> Enable
	{
		Enable(entry.0, *entry.1)
	}
	fn name(&self) -> &'static str { self.0 }
	fn symbol(&self) -> TokenTree { mkid(self.0) }
	fn bit(&self) -> u32 { self.1 }
	fn mask(&self) -> u64 { if self.1 < 64 { 1u64 << self.1 } else { 0 } }
}


struct HashEnable(BTreeMap<&'static str, u32>);

impl HashEnable
{
	fn new() -> HashEnable { HashEnable(BTreeMap::new()) }
	fn add(&mut self, key: &'static str, value: impl FnOnce() -> u32)
	{
		self.0.entry(key).or_insert_with(value);
	}
	fn get(&self, label: &str) -> usize
	{
		match self.0.get(label) {
			Some(x) => *x as usize,
			None => panic!("No enable bit '{label}' found")
		}
	}
	fn to_enable(&self) -> Vec<Enable>
	{
		self.0.iter().map(Enable::new).collect()
	}
	//Abuse enable for this, even if it does not quite fit.
	fn to_hash_enable(&self) -> Vec<Enable>
	{
		let mut out = Vec::new();
		for &H{name, ebit, ..} in HASHES.iter() { out.push(Enable(name, self.get(ebit) as u32)); }
		out
	}
}

pub static HASHES: &'static [H] = &[
	H{name:"Sha256",	ilen:64,	olen:32,	hname:"SHA-256",	family:"SHA-256",	dflt:true,	imp:"btls_aux_nettle!Sha256Ctx",	ebit: "SHA256",		is_sha3: false,		nsa: false,	_d:()},
	H{name:"Sha384",	ilen:128,	olen:48,	hname:"SHA-384",	family:"SHA-384",	dflt:true,	imp:"btls_aux_nettle!Sha384Ctx",	ebit: "SHA512",		is_sha3: false,		nsa: true,	_d:()},
	H{name:"Sha512",	ilen:128,	olen:64,	hname:"SHA-512",	family:"SHA-512",	dflt:true,	imp:"btls_aux_nettle!Sha512Ctx",	ebit: "SHA512",		is_sha3: false,		nsa: false,	_d:()},
	H{name:"Sha3256",	ilen:136,	olen:32,	hname:"SHA3-256",	family:"SHA3-256",	dflt:true,	imp:"btls_aux_nettle!Sha3256Ctx2",	ebit: "SHA3",		is_sha3: true,		nsa: false,	_d:()},
	H{name:"Sha3384",	ilen:104,	olen:48,	hname:"SHA3-384",	family:"SHA3-384",	dflt:true,	imp:"btls_aux_nettle!Sha3384Ctx2",	ebit: "SHA3",		is_sha3: true,		nsa: false,	_d:()},
	H{name:"Sha3512",	ilen:72,	olen:64,	hname:"SHA3-512",	family:"SHA3-512",	dflt:true,	imp:"btls_aux_nettle!Sha3512Ctx2",	ebit: "SHA3",		is_sha3: true,		nsa: false,	_d:()},
	H{name:"Shake128256",	ilen:168,	olen:32,	hname:"SHAKE128@256",	family:"SHAKE128",	dflt:true,	imp:"btls_aux_sha3!Shake128256",	ebit: "SHAKE128",	is_sha3: true,		nsa: false,	_d:()},
	H{name:"Shake256256",	ilen:136,	olen:32,	hname:"SHAKE256@256",	family:"SHAKE256",	dflt:true,	imp:"btls_aux_sha3!Shake256256",	ebit: "SHAKE256",	is_sha3: true,		nsa: false,	_d:()},
	H{name:"Shake256384",	ilen:136,	olen:48,	hname:"SHAKE256@384",	family:"SHAKE256",	dflt:true,	imp:"btls_aux_sha3!Shake256384",	ebit: "SHAKE256",	is_sha3: true,		nsa: false,	_d:()},
	H{name:"Shake256448",	ilen:136,	olen:56,	hname:"SHAKE256@448",	family:"SHAKE256",	dflt:true,	imp:"btls_aux_sha3!Shake256448",	ebit: "SHAKE256",	is_sha3: true,		nsa: false,	_d:()},
	H{name:"Shake256512",	ilen:136,	olen:64,	hname:"SHAKE256@512",	family:"SHAKE256",	dflt:true,	imp:"btls_aux_sha3!Shake256512",	ebit: "SHAKE256",	is_sha3: true,		nsa: false,	_d:()},
];

//Some hash mask bits are reserved for backwards compat.
const RESERVED_HMASK_BITS: u64 = 1<<0|1<<1|1<<3|1<<6|1<<7;
static SPECIAL_HMASK_TABLE: &'static [(&'static str, u32)] = &[
	("SHA256", 0),
	("SHA3", 3),
	("SHA512", 1),
	("SHAKE128", 6),
	("SHAKE256", 7)
];

fn tovec_map<T,U>(itr: impl Iterator<Item=T>, func: impl Fn(T) -> U) -> Vec<U> { itr.map(func).collect() }

type _HashTypeElement = u8;

fn write_autogenerated_file2(fp: &mut File)
{
	let mut default_hashalgo_mask = 0u64;
	let mut as_bytes_size = BTreeSet::new();
	let mut ilens = BTreeSet::new();
	let mut olens = BTreeSet::new();
	let mut ebits = HashEnable::new();
	//Special sizes only for output.
	as_bytes_size.insert(20); olens.insert(20);
	//Collect enable bits. For backwards compat, some entries use hardcoded mask bits.
	let mut next_ebit = 0;
	for hash in HASHES.iter() {
		ebits.add(hash.ebit, ||{
			let mut ebit: Option<u32> = None;
			for &(ebitn, ebitv) in SPECIAL_HMASK_TABLE.iter() {
				if ebitn == hash.ebit { ebit = Some(ebitv); }
			}
			let ebit = ebit.unwrap_or_else(||{
				while RESERVED_HMASK_BITS >> next_ebit & 1 != 0 { next_ebit += 1; }
				let tmp = next_ebit;
				next_ebit += 1;
				tmp
			});
			if hash.dflt && ebit < 64 { default_hashalgo_mask |= 1 << ebit; }
			ebit
		});
	}

	//The largest posible hash output/block in bytes.
	let max_hash_output = HASHES.iter().fold(0usize, |sofar,H{olen,..}|max(sofar, *olen));
	let max_hash_block = HASHES.iter().fold(0usize, |sofar,H{ilen,..}|max(sofar, *ilen));
	for H{name, ilen, olen, ..} in HASHES.iter() {
		if *ilen < *olen || *olen > 255 {
			panic!("Unsupported hash parameters: name={name}, in={ilen}, out={olen}");
		}
		as_bytes_size.insert(*ilen);
		as_bytes_size.insert(*olen);
		ilens.insert(*ilen);
		olens.insert(*olen);
	}
	//Hash type (enable) stuff.
	let hash_types = ebits.to_enable();
	let hash_enable = ebits.to_hash_enable();
	//The sizes and corresponding symbols.
	let as_bytes_size: Vec<_> = as_bytes_size.iter().collect();
	let ilens: Vec<_> = ilens.iter().map(Size::new).collect();
	let olens: Vec<_> = olens.iter().map(Size::new).collect();
	//The universe of type bits.
	let ebit_f = |h:&H|ebits.get(h.ebit);
	let hft_normal = HASHES.iter().filter(H::is_normal).map(ebit_f);
	let hft_nsa = HASHES.iter().filter(H::is_nsa2).map(ebit_f);
	let hft_bs = Bitmask::<_HashTypeElement>::new(HASHES.iter().map(ebit_f));
	let hft_normal = hft_bs.make(hft_normal);
	let hft_nsa = hft_bs.make(hft_nsa);
	

	qwrite(fp, quote!{
const _DEFAULT_HASHALGO_MASK: u64 = #default_hashalgo_mask;
const _MAX_HASH_OUTPUT: usize = #max_hash_output;
const _MAX_HASH_BLOCK: usize = #max_hash_block;
static _HFT_UNIVERSE: &'static [HashType] = &[ #([hash_types] HashType(_HashType::#symbol),)* ];
const _HFT_UNIVERSE_BITS: _HashTypeSetStorage = #hft_bs;
const NSA_KEY: _HashTypeSetStorage = [ #(#hft_nsa),* ];
const _HASH_NORMAL_MASK: _HashTypeSetStorage = [ #(#hft_normal),* ];
type _HashTypeSetStorage = #!MASKTYPE hft_bs;

#(unsafe impl HashArrayT for [u8;#as_bytes_size]
{
	//SAFETY: u8 can be zero-initialized.
	fn zeroed() -> Self { unsafe { core::mem::zeroed() } }
	fn as_xput_slice(&self) -> &[u8] { &self[..] }
	fn as_xput_slice_mut(&mut self) -> &mut [u8] { &mut self[..] }
	fn xput_len(&self) -> usize { #as_bytes_size }
	//SAFETY: Returns #as_bytes_size() readable bytes.
	fn as_xput_ptr(&self) -> *const u8 { self.as_ptr() }
	//SAFETY: Returns #as_bytes_size() readable/writable bytes.
	fn as_xput_mut_ptr(&mut self) -> *mut u8 { self.as_mut_ptr() }
})*

#([olens]
	impl HashOutputT for [u8;#size]
	{
		fn as_output_any(&self) -> HashOutput2 { HashOutput2(_HashOutput::#osym(*self)) }
	}
)*

#([ilens]
	impl HashBlockT for [u8;#size]
	{
		fn as_input_any(&self) -> HashBlock2 { HashBlock2(_HashBlock::#isym(*self)) }
	}
)*


#[doc(hidden)]
pub enum __UnstableDiscriminant
{
	#([HASHES] #symbol,)*
}

#[derive(Copy,Clone,Debug,PartialEq,Eq)]
enum _HashFunction
{
	#([HASHES] #symbol,)*
}

#[derive(Copy,Clone,PartialEq,Eq,Debug)]
enum _HashType
{
	#([hash_types] #symbol,)*
}

impl _HashType
{
	fn sign_bit(self) -> u32
	{
		match self {
			#([hash_types] _HashType::#symbol => #bit,)*
		}
	}
	fn by_internal_name(name: &str) -> Option<u32>
	{
		match name {
			#([hash_types] #name => Some(#bit),)*
			_ => None
		}
		
	}
}

#[derive(Clone)] 
enum _HashOutput
{
	#([olens] #osym([u8;#size]),)*
}

impl _HashOutput
{
	fn deref(&self) -> &[u8]
	{
		match self {
			#([olens] &_HashOutput::#osym(ref h) => h,)*
		}
	}
	fn deref_mut(&mut self) -> &mut [u8]
	{
		match self {
			#([olens] &mut _HashOutput::#osym(ref mut h) => h,)*
		}
	}
}

#[derive(Clone)] 
enum _HashBlock
{
	#([ilens] #isym([u8;#size]),)*
}

impl _HashBlock
{
	fn deref(&self) -> &[u8]
	{
		match self {
			#([ilens] &_HashBlock::#isym(ref h) => h,)*
		}
	}
}

#[derive(Clone)] 
enum _HashContext
{
	#([HASHES] #symbol(#symbol)),*
}

enum _HashActionAny<'a,'b>
{
	#([HASHES] #symbol(&'a mut HashAction<'b,#symbol>),)*
}

impl<'a,'b> _HashActionAny<'a,'b>
{
	fn __update(&mut self, block: &[u8])
	{
		match self {
			#([HASHES] &mut _HashActionAny::#symbol(ref mut h) => h.append(block),)*
		}
	}
}

impl HashFunctionEnabled
{
	fn _allows(&self, func: _HashFunction) -> bool
	{
		self.0.is_in2(HashType(func.hash_type()))
	}
}

impl _HashContext
{
	fn append(&mut self, data: &[u8])
	{ 
		match self {
			#([HASHES] &mut _HashContext::#symbol(ref mut h) => h.append(data),)*
		}
	}
	fn append_v(&mut self, data: &[&[u8]])
	{ 
		match self {
			#([HASHES] &mut _HashContext::#symbol(ref mut h) => h.append_v(data),)*
		}
	}
	fn reset(&mut self)
	{ 
		match self {
			#([HASHES] &mut _HashContext::#symbol(ref mut h) => *h = #symbol::new()),*
		}
	}
	fn output(&self) -> _HashOutput
	{ 
		match self {
			#([HASHES] &_HashContext::#symbol(ref h) => {
				let inner = Hash::output(h);
				_HashOutput::#osym(inner)
			},)*
		}
	}
	fn function(&self) -> _HashFunction
	{ 
		match self {
			#([HASHES] &_HashContext::#symbol(_) => _HashFunction::#symbol,)*
		}
	}
}

impl _HashFunction
{
	fn __unstable_discriminant(&self) -> __UnstableDiscriminant
	{
		match self {
			#([HASHES] _HashFunction::#symbol => __UnstableDiscriminant::#symbol,)*
		}
	}
	fn hash_type(&self) -> _HashType
	{
		match self {
			#([HASHES] _HashFunction::#symbol => _HashType::#hash_type,)*
		}
	}
	fn is_sha3(&self) -> bool
	{
		match self {
			#([HASHES] _HashFunction::#symbol => #is_sha3,)*
		}
	}
	fn is_nsa(&self) -> bool
	{
		match self {
			#([HASHES] _HashFunction::#symbol => #is_nsa,)*
		}
	}
	fn tls13_hkdf_expand_label_max(&self) -> usize
	{
		match self {
			#([HASHES] _HashFunction::#symbol => <#symbol as Hash>::tls13_hkdf_expand_label_max(),)*
		}
	}
	fn to_bitmask(&self) -> u64
	{
		match self {
			#([hash_enable] _HashFunction::#symbol => #mask,)*
		}
	}
	fn by_internal_name(name: &str) -> Option<_HashFunction>
	{
		match name {
			#([HASHES] #hname => Some(_HashFunction::#symbol),)*
			_ => None
		}
	}
	fn name(self) -> &'static str
	{
		match self {
			#([HASHES] _HashFunction::#symbol => #iname,)*
		}
	}
	fn as_string(self) -> &'static str
	{
		match self {
			#([HASHES] _HashFunction::#symbol => #hname,)*
		}
	}
	fn input_length(self) -> usize
	{
		match self {
			#([HASHES] _HashFunction::#symbol => #ilen,)*
		}
	}
	fn output_length(self) -> usize
	{
		match self {
			#([HASHES] _HashFunction::#symbol => #olen,)*
		}
	}
	fn tls13_secret_length(self) -> usize
	{
		match self {
			#([HASHES] _HashFunction::#symbol => #olen,)*
		}
	}
	fn context(self) -> _HashContext
	{
		match self {
			#([HASHES] _HashFunction::#symbol => _HashContext::#symbol(#symbol::new()),)*
		}
	}
	fn zeroes(self) -> _HashOutput
	{
		match self {
			#([HASHES] _HashFunction::#symbol => _HashOutput::#osym([0;#olen]),)*
		}
	}
	fn zeroes_tls13hs(self) -> _HashOutput
	{
		match self {
			#([HASHES] _HashFunction::#symbol => _HashOutput::#osym([0;#olen]),)*
		}
	}
	fn hash(self, data: &[u8]) -> _HashOutput
	{
		match self {
			#([HASHES] _HashFunction::#symbol => {
				let inner = <#symbol as Hash>::hash(data);
				_HashOutput::#osym(inner)
			},)*
		}
	}
	fn hash_v(self, data: &[&[u8]]) -> _HashOutput
	{
		match self {
			#([HASHES] _HashFunction::#symbol => {
				let inner = <#symbol as Hash>::hash_v(data);
				_HashOutput::#osym(inner)
			},)*
		}
	}
	fn hash_c(self, data: impl FnOnce(&mut HashAction<HashActionAny>)) -> _HashOutput
	{
		match self {
			#([HASHES] _HashFunction::#symbol => {
				let inner = <#symbol as Hash>::hash_c(|idata|{
					data(&mut HashAction(&mut HashActionAny(_HashActionAny::#symbol(idata))))
				});
				_HashOutput::#osym(inner)
			},)*
		}
	}
	fn hmac_precompress_key(self, key: &[u8]) -> _HashBlock
	{
		match self {
			#([HASHES] _HashFunction::#symbol => {
				let inner = <#symbol as Hash>::hmac_precompress_key(key);
				_HashBlock::#isym(inner)
			},)*
		}
	}
	fn hmac(self, key: &[u8], data: &[u8]) -> _HashOutput
	{
		match self {
			#([HASHES] _HashFunction::#symbol => {
				let inner = <#symbol as Hash>::hmac(key, data);
				_HashOutput::#osym(inner)
			},)*
		}
	}
	fn hmac_v(self, key: &[u8], data: &[&[u8]]) -> _HashOutput
	{
		match self {
			#([HASHES] _HashFunction::#symbol => {
				let inner = <#symbol as Hash>::hmac_v(key, data);
				_HashOutput::#osym(inner)
			},)*
		}
	}
	fn hmac_c(self, key: &[u8], data: impl FnOnce(&mut HashAction<HashActionAny>)) -> _HashOutput
	{
		match self {
			#([HASHES] _HashFunction::#symbol => {
				let inner = <#symbol as Hash>::hmac_c(key, |idata|{
					data(&mut HashAction(&mut HashActionAny(_HashActionAny::#symbol(idata))))
				});
				_HashOutput::#osym(inner)
			},)*
		}
	}
	fn tls13_derive_secret(self, key: &[u8], label: &[&str], context: &[u8]) -> _HashOutput
	{
		match self {
			#([HASHES] _HashFunction::#symbol => {
				let inner = <#symbol as Hash>::tls13_derive_secret(key, label, context);
				_HashOutput::#osym(inner)
			},)*
		}
	}
	fn tls13_hkdf_expand_label(self, key: &[u8], label: &[&str], context: &[u8], output: &mut [u8])
	{
		match self {
			#([HASHES] _HashFunction::#symbol => {
				<#symbol as Hash>::tls13_hkdf_expand_label(key, label, context, output)
			},)*
		}
	}
	fn tls13_hmac(self, key: &[u8], data: &[u8]) -> _HashOutput
	{
		match self {
			#([HASHES] _HashFunction::#symbol => {
				_HashOutput::#osym(<#symbol as Hash>::tls13_hmac(key, data))
			},)*
		}
	}
	fn tls13_hkdf_extract(self, key: &[u8], salt: &[u8]) -> _HashOutput
	{
		match self {
			#([HASHES] _HashFunction::#symbol => {
				let inner = <#symbol as Hash>::tls13_hkdf_extract(key, salt);
				_HashOutput::#osym(inner)
			},)*
		}
	}
	fn hkdf_expand(self, key: &[u8], data: impl Fn(&mut HashAction<HashActionAny>), output: &mut [u8])
	{
		match self {
			#([HASHES] _HashFunction::#symbol => {
				<#symbol as Hash>::__hkdf_expand(key, |idata|{
					data(&mut HashAction(&mut HashActionAny(_HashActionAny::#symbol(idata))))
				}, output)
			},)*
		}
	}
	fn tls_prf(self, key: &[u8], label: &str, data: impl Fn(&mut HashAction<HashActionAny>),
		output: &mut [u8])
	{
		match self {
			#([HASHES] _HashFunction::#symbol => {
				<#symbol as Hash>::tls_prf(key, label, |idata|{
					data(&mut HashAction(&mut HashActionAny(_HashActionAny::#symbol(idata))))
				}, output)
			},)*
		}
	}
}

#([HASHES]
	#[doc=#help]
	#[derive(Clone)]
	pub struct #symbol(#inner);

	impl HashUpdate for #symbol
	{
		fn __update(&mut self, block: &[u8]) { self.0.input(block); }
	}
	unsafe impl Hash for #symbol
	{
		type Tls13VerifyData = [u8;#olen];
		type Tls13HandshakeKey = [u8;#olen];
		type Output = [u8;#olen];
		type Input = [u8;#ilen];
		fn __initialize() -> Self { #symbol(#inner::new()) }
		fn __finalize(self) -> [u8;#olen] { self.0.output() }
		fn tls13_hmac(key: &[u8], data: &[u8]) -> [u8;#olen] { __std_tls13_hmac::<#symbol>(key, data) }
		fn tls13_hkdf_expand_label(key: &[u8], label: &[&str], context: &[u8], output: &mut [u8])
		{
			__std_tls13_hkdf_expand_label::<#symbol>(key, label, context, output)
		}
		fn tls13_hkdf_expand_label_max() -> usize
		{
			__std_tls13_hkdf_expand_label_max::<#symbol>()
		}
		fn tls13_hkdf_extract(key: &[u8], salt: &[u8]) -> [u8;#olen]
		{
			__std_tls13_hkdf_extract::<#symbol>(key, salt)
		}
	}
	impl GetHashFunction for #symbol
	{
		fn _function() -> HashFunction2 { HashFunction2(_HashFunction::#symbol) }
		fn _as_string() -> &'static str { Self::__as_string() }
	}
	impl #symbol {
		fn __as_string() -> &'static str { #hname }
	}
)*

///A checksum function.
///
///These may or may not qualify as cryptographic hashes.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
#[non_exhaustive]
pub enum Checksum
{
	///The SHA-1 (INSECURE).
	Sha1Insecure,
	#([HASHES] #[doc=#help] #symbol),*,
}

impl Checksum
{
	///Calculate the checksum on given data.
	pub fn checksum(&self, data: &[u8]) -> Result<HashOutput2, ()>
	{
		Ok(HashOutput2(match self {
			#([HASHES] Checksum::#symbol => _HashOutput::#osym(#symbol::hash(data))),*,
			Checksum::Sha1Insecure => _HashOutput::O20(sha1insecure(data)),
		}))
	}
}

	});
}

fn main()
{
	let out_dir = PathBuf::from(env::var("OUT_DIR").unwrap());
	write_autogenerated_file2(&mut File::create(&out_dir.join("autogenerated.inc.rs")).unwrap());

	println!("cargo:rerun-if-changed=build.rs");	//This has no external deps to rebuild for.
}
