use btls_aux_codegen::Bitmask;
use btls_aux_codegen::CodeBlock;
use btls_aux_codegen::mkid;
use btls_aux_codegen::mkidf;
use btls_aux_codegen::quote;
use btls_aux_codegen::qwrite;
use btls_aux_codegen::Symbol as TokenTree;
use btls_aux_fail::fail_if_none;
use std::env;
use std::fmt::Write as FmtWrite;
use std::fs::File;
use std::ops::Deref;
use std::path::Path;

const NO_FLAGS: u32 = 0;
const ALLOW_NULL: u32 = 1;
const ALLOW_NULL_SIG: u32 = 2;
const RSA_FLOATY_KEY: u32 = 4;
const NO_X509: u32 = 512;
const CCONLY: u32 = 2048;
const TYPE_RSA_PKCS1: u32 = 8;
const TYPE_RSA_PSS: u32 = 16;
const TYPE_ECDSA: u32 = 24;
const TYPE_ED25519: u32 = 32;
const TYPE_ED448: u32 = 40;
//48 is not used.
const TYPE_MASK: u32 = 56;
const CURVE_P256: u32 = 64;
const CURVE_P384: u32 = 128;
const CURVE_P521: u32 = 192;
const CURVE_MASK: u32 = 192;
const IS_DEFAULT: u32 = 256;

//Class constants.
const CLASS_TLS12_RSA: u32 = 1;
const CLASS_TLS12_ECDSA: u32 = 2;
const CLASS_TLS13_ALLOWED: u32 = 4;
const CLASS_NOT_RSA: u32 = 8;

fn flags_to_class(flags: u32) -> u32
{
	match flags & TYPE_MASK {
		TYPE_RSA_PKCS1 => CLASS_TLS12_RSA,
		TYPE_RSA_PSS => CLASS_TLS12_RSA | CLASS_TLS13_ALLOWED,
		TYPE_ECDSA => CLASS_TLS12_ECDSA | CLASS_TLS13_ALLOWED | CLASS_NOT_RSA,
		TYPE_ED25519 => CLASS_TLS12_ECDSA | CLASS_TLS13_ALLOWED | CLASS_NOT_RSA,
		TYPE_ED448 => CLASS_TLS12_ECDSA | CLASS_TLS13_ALLOWED | CLASS_NOT_RSA,
		_ => unreachable!()
	}
}

#[derive(Copy,Clone)]
struct AuxSignatureType
{
	//Symbol.
	symbol: &'static str,
	//Internal name.
	intname: &'static str,
	//Algorithm bit number.
	algbit: usize,
	//Enable by default?
	is_dflt: bool,
}

#[derive(Copy,Clone)]
struct AuxHashFunction
{
	//Name of algorithm in btls_aux_hash.
	intname: &'static str,
	//Name of algorithm in *_OID definitions.
	oidname: &'static str,
	//Length of output in bits.
	length: usize,
	//Decription.
	desc: &'static str,
	//Postfix in signame_entry.
	postfix: &'static str,
}

macro_rules! def_hash
{
	(INT $int:ident OID $oid:ident LEN $len:tt DESC $desc:tt PF $pf:ident) => {
		AuxHashFunction{
			intname: stringify!($int),
			oidname: stringify!($oid),
			length:$len,
			desc: $desc,
			postfix: stringify!($pf),
		}
	}
}

#[derive(Copy,Clone)]
struct RSignatureInfo
{
	//TLS signature algorithm id.
	tls_id: Option<u16>,
	//Priority in TLS. Larger is more preferred. <0 hides.
	priority: i32,
	//The hash function used.
	hash: &'static str,
	//Flags.
	flags: u32,
	//JWS algorithm id, or None if not JWS compatible.
	jws_alg: Option<&'static str>,
}

macro_rules! sig_type
{
	(RSA_PKCS1) => { TYPE_RSA_PKCS1 };
	(RSA_PSS) => { TYPE_RSA_PSS };
	(ECDSA) => { TYPE_ECDSA };
	(ED25519) => { TYPE_ED25519 };
	(ED448) => { TYPE_ED448 };
}

macro_rules! def_sig
{
	(TLS $tlsid:tt $stype:ident $h:ident $prio:expr, JWS $jws:ident $($flag:ident)|*) => {
		RSignatureInfo {
			tls_id: Some($tlsid),
			hash: stringify!($h),
			flags: $($flag)|*|sig_type!($stype),
			priority: $prio,
			jws_alg: Some(stringify!($jws)),
		}
	};
	(TLS $tlsid:tt $stype:ident $h:ident $prio:expr, $($flag:ident)|*) => {
		RSignatureInfo {
			tls_id: Some($tlsid),
			hash: stringify!($h),
			flags: $($flag)|*|sig_type!($stype),
			priority: $prio,
			jws_alg: None,			//No JWS algo.
		}
	};
	(X509 $stype:ident $h:ident $prio:expr, $($flag:ident)|*) => {
		RSignatureInfo {
			tls_id: None,			//Not valid in TLS.
			hash: stringify!($h),
			flags: $($flag)|*|sig_type!($stype),
			priority: $prio,
			jws_alg: None,			//No JWS algo.
		}
	};
}

static SIGTYPES: &'static [AuxSignatureType] = &[
	AuxSignatureType{symbol:"RsaPkcs1", intname:"RSA-PKCS1", algbit:1, is_dflt:true},
	AuxSignatureType{symbol:"RsaPss", intname:"RSA-PSS", algbit:2, is_dflt:true},
	AuxSignatureType{symbol:"Ecdsa", intname:"ECDSA", algbit:3, is_dflt:true},
	AuxSignatureType{symbol:"Ed25519", intname:"ED25519", algbit:4, is_dflt:true},
	AuxSignatureType{symbol:"Ed448", intname:"ED448", algbit:5, is_dflt:true},
];

static INTERNAL_HASH: AuxHashFunction = AuxHashFunction{intname: "", oidname: "", length:0, desc:"", postfix: ""};

static HASHES: &'static [AuxHashFunction] = &[
	def_hash!(INT Sha256  OID SHA256   LEN 256 DESC "SHA-256"  PF SHA_256),
	def_hash!(INT Sha384  OID SHA384   LEN 384 DESC "SHA-384"  PF SHA_384),
	def_hash!(INT Sha512  OID SHA512   LEN 512 DESC "SHA-512"  PF SHA_512),
	def_hash!(INT Sha3256 OID SHA3_256 LEN 256 DESC "SHA3-256" PF SHA3_256),
	def_hash!(INT Sha3384 OID SHA3_384 LEN 384 DESC "SHA3-384" PF SHA3_384),
	def_hash!(INT Sha3512 OID SHA3_512 LEN 512 DESC "SHA3-512" PF SHA3_512),
];

static SIGNATURES: &'static [RSignatureInfo] = &[
	def_sig!(TLS 0x0401 RSA_PKCS1 SHA_256   100, JWS RS256 ALLOW_NULL|ALLOW_NULL_SIG),
	def_sig!(TLS 0x0420 RSA_PKCS1 SHA_256   101, ALLOW_NULL|ALLOW_NULL_SIG|NO_X509|CCONLY),
	def_sig!(TLS 0x0501 RSA_PKCS1 SHA_384   200, JWS RS384 ALLOW_NULL|ALLOW_NULL_SIG),
	def_sig!(TLS 0x0520 RSA_PKCS1 SHA_384   201, ALLOW_NULL|ALLOW_NULL_SIG|NO_X509|CCONLY),
	def_sig!(TLS 0x0601 RSA_PKCS1 SHA_512   300, JWS RS512 ALLOW_NULL|ALLOW_NULL_SIG),
	def_sig!(TLS 0x0620 RSA_PKCS1 SHA_512   301, ALLOW_NULL|ALLOW_NULL_SIG|NO_X509|CCONLY),
	def_sig!(TLS 0x0804 RSA_PSS   SHA_256   400, JWS PS256 ALLOW_NULL),
	def_sig!(TLS 0x0805 RSA_PSS   SHA_384   500, JWS PS384 ALLOW_NULL),
	def_sig!(TLS 0x0806 RSA_PSS   SHA_512   600, JWS PS512 ALLOW_NULL),
	def_sig!(TLS 0x0809 RSA_PSS   SHA_256   700, JWS PS256 ALLOW_NULL|RSA_FLOATY_KEY|NO_X509),
	def_sig!(TLS 0x080a RSA_PSS   SHA_384   800, JWS PS384 ALLOW_NULL|RSA_FLOATY_KEY|NO_X509),
	def_sig!(TLS 0x080b RSA_PSS   SHA_512   900, JWS PS512 ALLOW_NULL|RSA_FLOATY_KEY|NO_X509),
	def_sig!(TLS 0x0403 ECDSA     SHA_256  1200, JWS ES256 CURVE_P256),
	def_sig!(TLS 0x0503 ECDSA     SHA_384  1100, JWS ES384 CURVE_P384),
	def_sig!(TLS 0x0603 ECDSA     SHA_512  1000, JWS ES512 CURVE_P521),
	def_sig!(TLS 0x0807 ED25519   INTERNAL 1500, JWS EdDSA NO_FLAGS),
	def_sig!(TLS 0x0808 ED448     INTERNAL 1300, JWS EdDSA NO_FLAGS),
	def_sig!(X509 RSA_PKCS1 SHA3_256   -1, ALLOW_NULL_SIG),
	def_sig!(X509 RSA_PKCS1 SHA3_384   -1, ALLOW_NULL_SIG),
	def_sig!(X509 RSA_PKCS1 SHA3_512   -1, ALLOW_NULL_SIG),
	def_sig!(X509 RSA_PSS   SHA3_256   -1, NO_FLAGS),
	def_sig!(X509 RSA_PSS   SHA3_384   -1, NO_FLAGS),
	def_sig!(X509 RSA_PSS   SHA3_512   -1, NO_FLAGS),
];

impl RSignatureInfo
{
	fn get_sym_name(&self, asuffix: bool) -> String
	{
		let prefix = match self.flags & TYPE_MASK {
			TYPE_ECDSA => "ECDSA_",
			TYPE_ED25519 => "ED25519",
			TYPE_ED448 => "ED448",
			TYPE_RSA_PKCS1 => "RSA_PKCS1_",
			TYPE_RSA_PSS => "RSA_PSS_",
			_ => unreachable!()
		};
		let suffix = if asuffix && self.flags & RSA_FLOATY_KEY != 0 { "_FLOAT" } else { "" };
		let suffix3 = if asuffix && self.flags & CCONLY != 0 { "_CCONLY" } else { "" };
		format!("{prefix}{nsym}{suffix}{suffix3}", nsym=self.hash_nsym())
	}
	fn get_hash(&self) -> AuxHashFunction
	{
		if self.hash == "INTERNAL" { return INTERNAL_HASH; }
		for i in HASHES.iter() {
			if i.postfix == self.hash { return i.clone() }
		}
		panic!("No hash named '{hash}'", hash=self.hash);
	}
	fn hash_nsym(&self) -> String
	{
		self.get_hash().oidname.to_owned()
	}
	fn hash_nenum(&self) -> String
	{
		self.get_hash().intname.to_owned()
	}
	fn hash_desc(&self) -> String
	{
		self.get_hash().desc.to_owned()
	}
	fn hash_length(&self) -> usize
	{
		self.get_hash().length
	}
	fn __get_unique_symbol(self) -> String
	{
		let mut out = String::new();
		let prefix = match self.flags & TYPE_MASK {
			TYPE_ECDSA => "ECDSA",
			TYPE_ED25519 => "ED25519",
			TYPE_ED448 => "ED448",
			TYPE_RSA_PKCS1 => "RSA_PKCS1",
			TYPE_RSA_PSS => "RSA_PSS",
			_ => unreachable!()
		};
		write!(out, "{prefix}_{hash}", hash=self.hash).unwrap();
		if self.flags & RSA_FLOATY_KEY != 0 { write!(out, "_FLOAT").unwrap(); }
		if self.flags & CCONLY != 0 { write!(out, "_CCONLY").unwrap(); }
		out
	}
	fn get_unique_symbol(self) -> TokenTree { mkid(&self.__get_unique_symbol()) }
	fn tname(self) -> String
	{
		let mtype = self.flags & TYPE_MASK;
		let textual_name_postfix = self.hash_desc();
		let textual_name_prefix = match mtype {
			TYPE_ECDSA => "ECDSA ",
			TYPE_ED25519 => "Ed25519",
			TYPE_ED448 => "Ed448",
			TYPE_RSA_PKCS1 => "RSA PKCS#1 v1.5 ",
			TYPE_RSA_PSS => "RSA PSS ",
			_ => unreachable!()
		};
		let extra_suffix = if mtype == TYPE_RSA_PSS {
			if self.flags & RSA_FLOATY_KEY != 0 { " with PSS" } else { " with DROWN" }
		} else {
			""
		};
		format!("{textual_name_prefix}{textual_name_postfix}{extra_suffix}")
	}
}


fn filter_ecdsa_entry(list: &mut Vec<TokenTree>, rid: &RSignatureInfo, curve: u32)
{
	if rid.tls_id.is_none() { return; }
	let c = rid.flags & CURVE_MASK;
	if rid.flags & TYPE_MASK != TYPE_ECDSA { return; }	//Not interested in this.
	if c != curve && c != 0 { return; }			//Not interested in this.
	list.push(mkidf!("SIG_{sym}", sym=rid.get_sym_name(true)));
}

fn for_each_sig_if<T>(predicate: impl Fn(&RSignatureInfo) -> bool, map: impl Fn(RSignatureInfo) -> T) -> Vec<T>
{
	SIGNATURES.iter().cloned().filter(predicate).map(map).collect()
}

struct SignatureEntry
{
	ibit: usize,
	info: RSignatureInfo,
}

impl SignatureEntry
{
	fn ibit(&self) -> usize { self.ibit }
	fn debugname(&self) -> String { self.info.__get_unique_symbol() }
	fn symbol(&self) -> TokenTree { self.info.get_unique_symbol() }
	fn sigclass(&self) -> TokenTree
	{
		let entry = self.info;
		let t = match entry.flags & TYPE_MASK {
			TYPE_ECDSA => "Ecdsa",
			TYPE_ED25519 => "Ed25519",
			TYPE_ED448 => "Ed448",
			TYPE_RSA_PKCS1 if entry.flags & CCONLY != 0  => "RsaPkcs1Cc",
			TYPE_RSA_PKCS1 => "RsaPkcs1",
			TYPE_RSA_PSS if entry.flags & RSA_FLOATY_KEY != 0 => "RsaPssPss",
			TYPE_RSA_PSS => "RsaPssRsae",
			_ => unreachable!()
		};
		mkid(t)
	}
	fn tname(&self) -> String { self.info.tname() }
	fn priority(&self) -> i32 { self.info.priority }
	fn jws_alg(&self) -> CodeBlock
	{
		match &self.info.jws_alg {
			&Some(ref x) => quote!(Some(#x)),
			&None => quote!(None),
		}
	}
	fn oid(&self) -> CodeBlock
	{
		let entry = self.info;
		if entry.flags & NO_X509 == 0 {
			let sym = mkidf!("{sym}_OID", sym=entry.get_sym_name(false));
			quote!(Some(&btls_aux_autogenerated_definitions::#sym[..]))
		} else {
			quote!(None)
		}
	}
	fn major(&self) -> CodeBlock
	{
		let entry = self.info;
		let mtype = entry.flags & TYPE_MASK;
		if mtype == TYPE_RSA_PKCS1 {
			let h = mkid(&entry.hash_nenum());
			let poid = mkidf!("{sym}_OID", sym=entry.hash_nsym());
			let allow_null = entry.flags & ALLOW_NULL != 0;
			quote!(RsaPkcs1(RsaHash::#h, &btls_aux_autogenerated_definitions::#poid[..], #allow_null))
		} else if mtype == TYPE_RSA_PSS {
			let h = mkid(&entry.hash_nenum());
			quote!(RsaPss(RsaHash::#h))
		} else if mtype == TYPE_ECDSA {
			match entry.get_hash().intname {
				"Sha256" => quote!(Ecdsa(EcdsaHash::Sha256)),
				"Sha384" => quote!(Ecdsa(EcdsaHash::Sha384)),
				"Sha512" => quote!(Ecdsa(EcdsaHash::Sha512)),
				f => panic!("Illegal hash function {f} for ECDSA")
			}
		} else if mtype == TYPE_ED25519 {
			quote!(Ed25519)
		} else if mtype == TYPE_ED448 {
			quote!(Ed448)
		} else {
			panic!("Unknown signature algorithm major type!")
		}
	}
	fn tls12_type(&self) -> CodeBlock
	{
		let entry = self.info;
		if entry.tls_id.is_none() { return quote!(NotUsable); }
		match entry.flags & TYPE_MASK {
			TYPE_RSA_PKCS1 => quote!(Rsa),
			TYPE_RSA_PSS => quote!(Rsa),
			TYPE_ECDSA => quote!(Ecdsa),
			TYPE_ED25519 => quote!(Ecdsa),
			TYPE_ED448 => quote!(Ecdsa),
			_ => quote!(NotUsable)
		}
	}
	fn tls13_ok(&self) -> bool
	{
		let entry = self.info;
		entry.tls_id.is_some() && flags_to_class(entry.flags) & CLASS_TLS13_ALLOWED != 0
	}
	fn major_type(&self) -> CodeBlock
	{
		match self.info.flags & TYPE_MASK {
			TYPE_RSA_PKCS1 => quote!(Rsa),
			TYPE_RSA_PSS => quote!(Rsa),
			TYPE_ECDSA => quote!(Ecdsa),
			TYPE_ED25519 => quote!(Eddsa),
			TYPE_ED448 => quote!(Eddsa),
			_ => quote!(Unknown)
		}
	}
}

struct SignatureTlsEntry
{
	symbol: TokenTree,
	id: u16,
}

impl SignatureTlsEntry
{
	fn symbol(&self) -> TokenTree { self.symbol.clone() }
	fn id(&self) -> u16 { self.id }
}

struct SignatureX509Entry(RSignatureInfo);

impl SignatureX509Entry
{
	fn symbol(&self) -> TokenTree { self.0.get_unique_symbol() }
	fn oid(&self) -> CodeBlock
	{
		let sym = mkidf!("{sym}_OID", sym=self.0.get_sym_name(false));
		quote!(&btls_aux_autogenerated_definitions::#sym[..])
	}
}

struct SecondaryEntry
{
	symbol: TokenTree,
	bit_number: usize,
	name: String,
}

impl SecondaryEntry
{
	fn new(entry: RSignatureInfo, index: &mut usize, secondary_default_bits: &mut Vec<usize>) ->
		Option<SecondaryEntry>
	{
		let stype = entry.flags & TYPE_MASK;
		//This is not for ordinary signature types.
		fail_if_none!(matches!(stype, TYPE_RSA_PKCS1|TYPE_RSA_PSS|TYPE_ECDSA|TYPE_ED25519|TYPE_ED448));
		let bit = *index;
		if entry.flags & IS_DEFAULT != 0 { secondary_default_bits.push(bit);  }
		*index += 1;
		Some(SecondaryEntry {
			symbol: entry.get_unique_symbol(),
			bit_number: bit,
			//TODO: Better names!
			name: bit.to_string(),
		})
	}
	fn symbol(&self) -> TokenTree { self.symbol.clone() }
	fn bit_number(&self) -> usize { self.bit_number }
	fn int_name(&self) -> String { self.name.clone() }
}

/*******************************************************************************************************************/
type _SignatureMaskElement = u8;

fn write_autogenerated_file(fp: &mut File)
{
	let adefs = mkid("btls_aux_autogenerated_definitions");
	let mut sigs_ecdsa256 = Vec::new();
	let mut sigs_ecdsa384 = Vec::new();
	let mut sigs_ecdsa521 = Vec::new();
	for val in SIGNATURES.iter() {
		filter_ecdsa_entry(&mut sigs_ecdsa256, val, CURVE_P256);
		filter_ecdsa_entry(&mut sigs_ecdsa384, val, CURVE_P384);
		filter_ecdsa_entry(&mut sigs_ecdsa521, val, CURVE_P521);
	}

	let algset_bs = Bitmask::<_SignatureMaskElement>::new(0..SIGNATURES.len());
	let mut algset_tls12_rsa = algset_bs.sub();
	let mut algset_tls12_ecdsa = algset_bs.sub();
	let mut algset_tls13_ok = algset_bs.sub();
	let mut algset_not_rsa = algset_bs.sub();
	let mut algset_not_rsa_encryption = algset_bs.sub();
	let mut algset_tls_public_use = algset_bs.sub();
	let mut algset_cconly = algset_bs.sub();
	let mut algset_has_oid = algset_bs.sub();
	for (index, entry) in SIGNATURES.iter().cloned().enumerate() {
		if entry.tls_id.unwrap_or(0xFFFF) >> 9 != 127 { algset_tls_public_use.add(index); }
		//TODO: Not strictly correct with RSA_FLOATY_KEY. RSA_FLOATY_KEY should only be included if the
		//shadowed entry is !NO_X509.
		if entry.flags & NO_X509 == 0 || entry.flags & RSA_FLOATY_KEY != 0 { algset_has_oid.add(index); }
		if entry.flags & CCONLY != 0 { algset_cconly.add(index); }
		match entry.flags & TYPE_MASK {
			TYPE_RSA_PKCS1 => algset_tls12_rsa.add(index),
			TYPE_RSA_PSS => {
				algset_tls12_rsa.add(index);
				algset_tls13_ok.add(index);
				if entry.flags & RSA_FLOATY_KEY != 0 { algset_not_rsa_encryption.add(index); }
			},
			//ECDSA, Ed25519 and Ed448 happen to have the same flags.
			TYPE_ECDSA|TYPE_ED25519|TYPE_ED448 => {
				algset_tls12_ecdsa.add(index);
				algset_tls13_ok.add(index);
				algset_not_rsa.add(index);
				algset_not_rsa_encryption.add(index);
			},
			_ => unreachable!(),
		}
	}
	//Signature types.
	let signatures: Vec<_> = SIGNATURES.iter().cloned().enumerate().map(|(ibit, info)|{
		SignatureEntry{ibit, info}
	}).collect();

	let signatures_tls: Vec<_> = SIGNATURES.iter().cloned().filter_map(|entry|{
		//The entries that should be filtered out are those with tls_id=None.
		Some(SignatureTlsEntry {
			symbol: entry.get_unique_symbol(),
			id: entry.tls_id?,
		})
	}).collect();

	let signatures_x509: Vec<_> = SIGNATURES.iter().cloned().filter_map(|entry|{
		fail_if_none!(entry.flags & NO_X509 != 0);
		Some(SignatureX509Entry(entry))
	}).collect();

	let mut index = 0usize;
	let mut secondary_default_bits = Vec::new();
	let signature_secondary: Vec<_> = SIGNATURES.iter().cloned().filter_map(|entry|{
		SecondaryEntry::new(entry, &mut index, &mut secondary_default_bits)
	}).collect();

	//Recognize the hash function even if entry is marked as NO_X509.
	let recognize_hash = for_each_sig_if(|entry|entry.flags & TYPE_MASK == TYPE_RSA_PSS, |entry|{
		let hnull = entry.flags & ALLOW_NULL != 0;
		let h = mkid(&entry.hash_nenum());
		let oid = mkidf!("{nsym}_OID", nsym=entry.hash_nsym());
		let hpcond = mkid(&if hnull { "hpnoneornull" } else { "hpnone" });
		quote!(else if oid == &#adefs::#oid && #hpcond { Some(btls_aux_hash::#h::function()) })
	});
	//NOTE: If there ever are RSA-PSS entries with elementary OIDs, recognize_non_pss_x509_oid should be
	//used for those.
	let recognize_rsa_pss = for_each_sig_if(|entry|entry.flags & (TYPE_MASK|NO_X509) == TYPE_RSA_PSS, |entry|{
		let name = entry.get_unique_symbol();
		let saltlen2 = (entry.hash_length() >> 3) as usize;
		let h = mkid(&entry.hash_nenum());
		//The hash function is always non-NULL, because default is SHA-1, which is not acceptable.
		quote!(else if p.salt.unwrap_or(20) == #saltlen2 &&
			p.trailer.unwrap_or(RsaPssTrailer::Bc) == RsaPssTrailer::Bc &&
			p.hash == Some(btls_aux_hash::#h::function()) &&
			p.mask == Some(RsaPssMaskGenerator::Mgf1(btls_aux_hash::#h::function())) {
			Some(_SignatureAlgorithmX5092::#name)
		})
	});
	let recognize_non_pss_x509_oid = for_each_sig_if(|entry|{
		entry.flags & NO_X509 == 0 && entry.flags & TYPE_MASK != TYPE_RSA_PSS
	}, |entry|{
		let name = entry.get_unique_symbol();
		let hnull = entry.flags & ALLOW_NULL_SIG != 0;
		let pcond = mkid(&if hnull { "pnull" } else { "pnone" });
		let oid = mkidf!("{sym}_OID_I", sym=entry.get_sym_name(false));
		quote!(else if #pcond && oid == &#adefs::#oid { Some(_SignatureAlgorithmX5092::#name) })
	});
	//Signature types.
	let sigtype_symbol: Vec<_> = SIGTYPES.iter().map(|entry|mkid(entry.symbol)).collect();
	let sigtype_intname: Vec<_> = SIGTYPES.iter().map(|entry|entry.intname).collect();
	let sigtype_algbit: Vec<_> = SIGTYPES.iter().map(|entry|entry.algbit).collect();
	let algbit_f = |s:&AuxSignatureType|s.algbit;
	let sigtype_bs = Bitmask::<_SignatureMaskElement>::new(SIGTYPES.iter().map(algbit_f));
	let sigtype_default = sigtype_bs.make(SIGTYPES.iter().filter(|s|s.is_dflt).map(algbit_f));
	let sigtype_default_mask = sigtype_bs.make64(SIGTYPES.iter().filter(|s|s.is_dflt).map(algbit_f));
	//The secondary bitmask assignments.
	let sigalgo_count = SIGNATURES.len();
	let ecdsa_hash_names = [mkid("Sha256"),mkid("Sha384"),mkid("Sha512")];
	let rsa_hash_names = [mkid("Sha256"),mkid("Sha384"),mkid("Sha512"),
		mkid("Sha3256"),mkid("Sha3384"),mkid("Sha3512")];
	//Universe for secondaries.
	let secondary_bs = signature_secondary.iter().map(SecondaryEntry::bit_number);
	let secondary_bs = Bitmask::<_SignatureMaskElement>::new(secondary_bs);
	let secondary_default = secondary_bs.make(secondary_default_bits.iter().cloned());


	qwrite(fp, quote!{
use #adefs::RSA_KEY_OID;
use btls_aux_autogenerated_definitions::RSA_PSS_KEY_OID;
use btls_aux_autogenerated_definitions::ECDSA_KEY_OID;
use btls_aux_autogenerated_definitions::CURVE_NSA_P256;
use btls_aux_autogenerated_definitions::CURVE_NSA_P384;
use btls_aux_autogenerated_definitions::ED25519_KEY_OID;
use btls_aux_autogenerated_definitions::ED448_KEY_OID;
use btls_aux_autogenerated_definitions::CURVE_NSA_P521;
use btls_aux_hash::Hash;
use btls_aux_hash::HashFunction2;
use btls_aux_hash::HashOutput2;
use btls_aux_hash::HashOutputT;

const _SIGALGO_CONST_MASK: u64 = #sigtype_default_mask;
const _SIGNATURE_NORMAL_MASK: _SignatureTypeSet = [ #(#sigtype_default),* ];
const _SIGNATURE_SECONDARY_MASK: _SignatureAlgorithmSecondary = [ #(#secondary_default),* ];
const _SIGALGO_COUNT: usize = #sigalgo_count;
type _SignatureTypeSet = #!MASKTYPE sigtype_bs;
type _SignatureAlgorithmSecondary = #!MASKTYPE secondary_bs;
type _SignatureAlgorithmSet2 = #!MASKTYPE algset_bs;


const ALGSET_TLS12_ECDSA: _SignatureAlgorithmSet2 = #algset_tls12_ecdsa;
const ALGSET_TLS12_RSA: _SignatureAlgorithmSet2 = #algset_tls12_rsa;
const ALGSET_TLS13_OK: _SignatureAlgorithmSet2 = #algset_tls13_ok;
const ALGSET_NOT_RSA: _SignatureAlgorithmSet2 = #algset_not_rsa;
const ALGSET_NOT_RSA_ENCRYPTION: _SignatureAlgorithmSet2 = #algset_not_rsa_encryption;
const ALGSET_CCONLY: _SignatureAlgorithmSet2 = #algset_cconly;
const ALGSET_TLS_PUBLIC_USE: _SignatureAlgorithmSet2 = #algset_tls_public_use;
const ALGSET_HAS_OID: _SignatureAlgorithmSet2 = #algset_has_oid;
const ALGSET_ALL: _SignatureAlgorithmSet2 = #algset_bs;

#[derive(Clone,Debug,PartialEq,Eq)]
enum MajorSignatureAlgorithm2
{
	//The arguments in order are:
	//
	// * Hash function used.
	// * The partial has identifier OID (no headers, no trailing `NULL`).
	// * Null flag, `true` means trailing `NULL` may be present. `false` means there must be no trailing `NULL`.
	RsaPkcs1(RsaHash, &'static [u8], bool),
	RsaPss(RsaHash),
	Ecdsa(EcdsaHash),
	Ed25519,
	Ed448,
}

#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub(crate) enum EcdsaHash
{
	#(#ecdsa_hash_names,)*
}

impl EcdsaHash
{
	pub(crate) fn hashmsg(&self, msg: &[u8]) -> HashOutput2
	{
		use EcdsaHash::*;
		match *self {
			#(#ecdsa_hash_names => btls_aux_hash::#ecdsa_hash_names::hash(msg).as_output_any(),)*
		}
	}
	pub(crate) fn function(&self) -> HashFunction2
	{
		use EcdsaHash::*;
		match *self {
			#(#ecdsa_hash_names => btls_aux_hash::#ecdsa_hash_names::function(),)*
		}
	}
}

#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub(crate) enum RsaHash
{
	#(#rsa_hash_names,)*
}

impl RsaHash
{
	pub(crate) fn function(&self) -> HashFunction2
	{
		use RsaHash::*;
		match *self {
			#(#rsa_hash_names => btls_aux_hash::#rsa_hash_names::function(),)*
		}
	}
}

#[allow(non_camel_case_types)]
#[derive(Copy,Clone,PartialEq,Eq,Debug)]
enum _SignatureAlgorithmTls2
{
	#([signatures_tls] #symbol,)*
}

impl _SignatureAlgorithmTls2
{
	fn by_tls_id(id: u16) -> Option<_SignatureAlgorithmTls2>
	{
		match id {
			#([signatures_tls] #id => Some(_SignatureAlgorithmTls2::#symbol),)*
			_ => None
		}
	}
	fn tls_id(self) -> u16
	{
		match self {
			#([signatures_tls] _SignatureAlgorithmTls2::#symbol => #id,)*
		}
	}
	fn to_generic(self) -> _SignatureAlgorithm2
	{
		match self {
			#([signatures_tls] _SignatureAlgorithmTls2::#symbol => _SignatureAlgorithm2::#symbol,)*
		}
	}
	fn from_generic(x: _SignatureAlgorithm2) -> Option<Self>
	{
		//Wrapping in some() is hack to get around exhaustiveness checks.
		match Some(x) {
			#([signatures_tls]
				Some(_SignatureAlgorithm2::#symbol) => Some(_SignatureAlgorithmTls2::#symbol),
			)*
			_ => None
		}
	}
}

fn __recognize_hash_oid(oid: &[u8], params: Option<Asn1Tlv>) -> Option<HashFunction2>
{
	let (hpnone, hpnoneornull) = if let Some(params) = params {
		(false, params.tag == Asn1Tag::NULL && params.raw_p == &[])
	} else {
		(true, true)
	};
	if false { None }	//So that all entries can be else if without exception.
	#(#recognize_hash)*
	else { None }
}

#[allow(non_camel_case_types)]
#[derive(Copy,Clone,PartialEq,Eq,Debug)]
enum _SignatureAlgorithmX5092
{
	#([signatures_x509] #symbol,)*
}

impl _SignatureAlgorithmX5092
{
	pub fn __handle_rsa_pss(params: &[u8]) -> Option<_SignatureAlgorithmX5092>
	{
		let p = RsaPssParameterBlock2::from(params).ok()?;

		if false { None }	//So that all entries can be else if without exception.
		#(#recognize_rsa_pss)*
		else { None }
	}
	fn by_oid(oid: &[u8]) -> Option<_SignatureAlgorithmX5092>
	{
		let mut src = Source::new(oid);
		let oid = src.asn1_slice(Asn1Tag::OID).ok()?;
		let params = src.read_remaining();
		let pnone = params.len() == 0;
		let pnull = params == &[5,0] || pnone;
		if oid == #adefs::RSA_PSS_OID_I { Self::__handle_rsa_pss(params) }
		#(#recognize_non_pss_x509_oid)*
		else { return None; }
	}
	fn oid(self) -> &'static [u8]
	{
		match self {
			#([signatures_x509] _SignatureAlgorithmX5092::#symbol => #oid),*
		}
	}
	fn to_generic(self) -> _SignatureAlgorithm2
	{
		match self {
			#([signatures_x509] _SignatureAlgorithmX5092::#symbol => _SignatureAlgorithm2::#symbol),*
		}
	}
	fn from_generic(x: _SignatureAlgorithm2) -> Option<Self>
	{
		//Wrapping in some() is hack to get around exhaustiveness checks.
		match Some(x) {
			#([signatures_x509]
				Some(_SignatureAlgorithm2::#symbol) => Some(_SignatureAlgorithmX5092::#symbol),
			)*
			_ => None
		}
	}
}

#[allow(non_camel_case_types)]
#[derive(Copy,Clone,PartialEq,Eq)]
enum _SignatureAlgorithm2
{
	#([signatures] #symbol,)*
}

impl _SignatureAlgorithm2
{
	fn set_bit(self) -> usize
	{
		match self {
			#([signatures] _SignatureAlgorithm2::#symbol => #ibit),*
		}
	}
	fn debug_name(self) -> &'static str
	{
		match self {
			#([signatures] _SignatureAlgorithm2::#symbol => #debugname),*
		}
	}
	fn oid(self) -> Option<&'static [u8]>
	{
		match self {
			#([signatures] _SignatureAlgorithm2::#symbol => #oid),*
		}
	}
	fn get_priority(self) -> i32
	{
		match self {
			#([signatures] _SignatureAlgorithm2::#symbol => #priority),*,
		}
	}
	fn get_jws_algo(&self) -> Option<&'static str>
	{
		match self {
			#([signatures] _SignatureAlgorithm2::#symbol => #jws_alg),*,
		}
	}
	fn secondary_alg(&self) -> Option<_SignatureTypeSecondary>
	{
		match self {
			#([signature_secondary]
				_SignatureAlgorithm2::#symbol => Some(_SignatureTypeSecondary::#symbol),
			)*
			//Only some entries have secondary bitmask value.
			_ => None
		}
	}
	fn as_string(&self) -> &'static str
	{
		match self {
			#([signatures] &_SignatureAlgorithm2::#symbol => #tname,)*
		}
	}
	fn rsa_level(self, ktype: KeyIdentify2) -> Result<u8, ()>
	{
		match self {
			#([signatures] _SignatureAlgorithm2::#symbol => sig_key_match(SClass::#sigclass, ktype),)*
		}
	}
	fn tls12_type(self) -> Tls12SignatureType2
	{
		match self {
			#([signatures] _SignatureAlgorithm2::#symbol => Tls12SignatureType2::#tls12_type,)*
		}
	}
	fn major2(self) -> SignatureTypeMajor2
	{
		match self {
			#([signatures] _SignatureAlgorithm2::#symbol => SignatureTypeMajor2::#major_type,)*
		}
	}
	fn tls13_ok(self) -> bool
	{
		match self {
			#([signatures] _SignatureAlgorithm2::#symbol => #tls13_ok,)*
		}
	}
	fn major(self) -> MajorSignatureAlgorithm2
	{
		match self {
			#([signatures] _SignatureAlgorithm2::#symbol => MajorSignatureAlgorithm2::#major,)*
		}
	}
}

#[derive(Copy,Clone,PartialEq,Eq,Debug)]
enum _SignatureType
{
	#(#sigtype_symbol),*
}

impl SetItem2 for SignatureType
{
	type Storage = _SignatureTypeSet;
	fn __sign_bit(&self) -> usize
	{
		match self.0 {
			#(_SignatureType::#sigtype_symbol => #sigtype_algbit),*
		}
	}
	fn __universe() -> &'static [SignatureType]
	{
		&[#(SignatureType(_SignatureType::#sigtype_symbol)),*]
	}
	fn __universe_bits() -> _SignatureTypeSet { #sigtype_bs }
	fn __by_internal_name(name: &str) -> Option<usize>
	{
		match name {
			#(#sigtype_intname => Some(#sigtype_algbit)),*,
			_ => None
		}
	}
}

#[derive(Copy,Clone,PartialEq,Eq,Debug)]
enum _SignatureTypeSecondary
{
	#([signature_secondary] #symbol),*
}

impl SetItem2 for SignatureTypeSecondary
{
	type Storage = _SignatureAlgorithmSecondary;
	fn __sign_bit(&self) -> usize
	{
		match self.0 {
			#([signature_secondary] _SignatureTypeSecondary::#symbol => #bit_number,)*
		}
	}
	fn __universe() -> &'static [SignatureTypeSecondary]
	{
		&[
			#([signature_secondary] SignatureTypeSecondary(_SignatureTypeSecondary::#symbol),)*
		]
	}
	fn __universe_bits() -> _SignatureAlgorithmSecondary { #secondary_bs }
	fn __by_internal_name(name: &str) -> Option<usize>
	{
		match name {
			#([signature_secondary] #int_name => Some(#bit_number))*
			_ => None
		}
	}
}

impl SetItem2 for SignatureAlgorithm2
{
	type Storage = _SignatureAlgorithmSet2;
	fn __sign_bit(&self) -> usize
	{
		match self.0 {
			#([signatures] _SignatureAlgorithm2::#symbol => #ibit,)*
		}
	}
	fn __universe() -> &'static [SignatureAlgorithm2]
	{
		&[
			#([signatures] SignatureAlgorithm2(_SignatureAlgorithm2::#symbol),)*
		]
	}
	fn __universe_bits() -> _SignatureAlgorithmSet2 { #algset_bs }
	//By-name lookup is not supported.
	fn __by_internal_name(_: &str) -> Option<usize> { None }
}

static SIGNATURE_ALGORITHMS: &'static [SignatureAlgorithm2] = &[
	#([signatures] SignatureAlgorithm2(_SignatureAlgorithm2::#symbol),)*
];

});

}
/*******************************************************************************************************************/
fn write_autogenerated_file_pem(fp: &mut File)
{
	let data = &[
		("CERTIFICATE","Certificate"),
		("TRUSTED CERTIFICATE","TrustedCertificate"),
		("X509 CRL","X509Crl"),
		("CERTIFICATE REQUEST","CertificateRequest"),
		("PKCS7","Pkcs7"),
		("CMS","Cms"),
		("PRIVATE KEY","PrivateKey"),
		("RSA PRIVATE KEY","RsaPrivateKey"),
		("EC PRIVATE KEY","EcPrivateKey"),
		("ENCRYPTED PRIVATE KEY","EncryptedPrivateKey"),
		("ATTRIBUTE CERTIFICATE","AttributeCertificate"),
		("PUBLIC KEY","PublicKey"),
		("DH PARAMETERS","DhParameters"),
	];
	let mut caseline = Vec::new();
	let mut print = Vec::new();

	for &(textual, symbol2) in data.iter() {
		let sym2 = mkid(symbol2);
		print.push(quote!{#sym2 => f.write_str(#textual),});
		caseline.push(quote!{#textual => #sym2,});
	}
	let caseline = caseline.deref();
	let print = print.deref();

	qwrite(fp, quote!{
		fn construct_pem_fragment<'a>(kind: &'a [u8], value: &'a [u8]) -> Option<PemDerseqFragment2<'a>>
		{
			use self::PemFragmentKind::*;
			if kind.iter().any(|b|b.wrapping_sub(32) > 95) { return None; }
			let kind = from_utf8(kind).ok()?;
			let kind = match kind {
				#(#caseline)*
				kind => Other(kind)
			};
			Some(PemDerseqFragment2::Pem(kind, value))
		}
		impl<'a> Display for PemFragmentKind<'a>
		{
			fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
			{
				use self::PemFragmentKind::*;
				match self {
					#(#print)*
					Other(x) => Display::fmt(x, f),
				}
			}
		}
	});
}

/*******************************************************************************************************************/

fn main()
{
	let out_dir = env::var("OUT_DIR").unwrap();
	let mut f = File::create(&Path::new(&out_dir).join("autogenerated.inc.rs")).unwrap();
	write_autogenerated_file(&mut f);
	let mut f = File::create(&Path::new(&out_dir).join("autogenerated-pem.inc.rs")).unwrap();
	write_autogenerated_file_pem(&mut f);

	println!("cargo:rerun-if-changed=build.rs");	//This has no external deps to rebuild for.
}
