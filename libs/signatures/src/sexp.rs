use crate::InternalKey;
use crate::KeyFormat;
use crate::keypair::DecodingError;
use btls_aux_collections::Vec;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::ResultExt;
use btls_aux_sexp::SexprParseStream;

pub(crate) fn convert_key_from_sexpr2(native_key: &[u8]) -> Result<InternalKey, DecodingError>
{
	use crate::keypair::DecodingError::*;
	use crate::ecdsa::EcdsaDecodingError::*;
	use crate::eddsa::EddsaDecodingError::*;
	let mut stream = SexprParseStream::new(native_key);
	let ktype = stream.read_typed_str().set_err(NoKeyType)?;
	if ktype == "ecdsa" {
		static ZEROBYTE: [u8;1] = [0];
		let crv = stream.next_str().set_err(SexpListTruncated)?;
		let curve = crate::ecdsa::EcdsaCurve::by_sexp_id(crv).ok_or(UnknownCurve)?;
		let (oid, len) = (&ZEROBYTE[..], curve.get_component_bytes());
		let d = stream.next_data().set_err(SexpListTruncated)?;
		let x = stream.next_data().set_err(SexpListTruncated)?;
		let y = stream.next_data().set_err(SexpListTruncated)?;
		stream.assert_eol().set_err(SexpListTooLong)?;
		crate::ecdsa::serialize_key(oid, d, x, y, len)
	} else if ktype == "ed25519" {
		let privkey = stream.next_data().set_err(SexpListTruncated)?;
		let pubkey = stream.next_data().set_err(SexpListTruncated)?;
		stream.assert_eol().set_err(SexpListTooLong)?;
		fail_if!(privkey.len() != 32, BadEd25519d);
		fail_if!(pubkey.len() != 32, BadEd25519x);
		let mut res = Vec::new();
		res.extend_from_slice(privkey);
		res.extend_from_slice(pubkey);
		Ok(InternalKey::join(KeyFormat::Ed25519, res))
	} else if ktype == "ed448" {
		let privkey = stream.next_data().set_err(SexpListTruncated)?;
		let pubkey = stream.next_data().set_err(SexpListTruncated)?;
		stream.assert_eol().set_err(SexpListTooLong)?;
		fail_if!(privkey.len() != 57, BadEd448d);
		fail_if!(pubkey.len() != 57, BadEd448x);
		let mut res = Vec::new();
		res.extend_from_slice(privkey);
		res.extend_from_slice(pubkey);
		Ok(InternalKey::join(KeyFormat::Ed448, res))
	} else {
		fail!(SexpUnknownKeyType);
	}
}
