use super::*;
use crate::cose::convert_key_from_cwk2;
use crate::sexp::convert_key_from_sexpr2;

#[test]
fn ed25519_sexpr()
{
	let skey = include_bytes!("test-ed25519.sexp");
	let tkey = include_bytes!("test-ed25519.der");
	let (ktype, cmp) = convert_key_from_sexpr2(&skey[..]).map(|x|x.split()).unwrap();
	assert_eq!(ktype, KeyFormat::Ed25519);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn ed25519_cose()
{
	let skey = include_bytes!("test-ed25519.cose");
	let tkey = include_bytes!("test-ed25519.der");
	let (ktype, cmp) = convert_key_from_cwk2(&skey[..]).map(|x|x.split()).unwrap();
	assert_eq!(ktype, KeyFormat::Ed25519);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn ed25519_jose()
{
	let skey = include_bytes!("test-ed25519.jose");
	let tkey = include_bytes!("test-ed25519.der");
	let (ktype, cmp) = convert_key_from_jwk2(&skey[..]).map(|x|x.split()).unwrap();
	assert_eq!(ktype, KeyFormat::Ed25519);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn ed448_sexpr()
{
	let skey = include_bytes!("test-ed448.sexp");
	let tkey = include_bytes!("test-ed448.der");
	let (ktype, cmp) = convert_key_from_sexpr2(&skey[..]).map(|x|x.split()).unwrap();
	assert_eq!(ktype, KeyFormat::Ed448);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn ed448_cose()
{
	let skey = include_bytes!("test-ed448.cose");
	let tkey = include_bytes!("test-ed448.der");
	let (ktype, cmp) = convert_key_from_cwk2(&skey[..]).map(|x|x.split()).unwrap();
	assert_eq!(ktype, KeyFormat::Ed448);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn ed448_jose()
{
	let skey = include_bytes!("test-ed448.jose");
	let tkey = include_bytes!("test-ed448.der");
	let (ktype, cmp) = convert_key_from_jwk2(&skey[..]).map(|x|x.split()).unwrap();
	assert_eq!(ktype, KeyFormat::Ed448);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn p256_sexpr()
{
	let skey = include_bytes!("test-p256.sexp");
	let tkey = include_bytes!("test-p256.der");
	let (ktype, cmp) = convert_key_from_sexpr2(&skey[..]).map(|x|x.split()).unwrap();
	assert_eq!(ktype, KeyFormat::Ecdsa);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn p256_cose()
{
	let skey = include_bytes!("test-p256.cose");
	let tkey = include_bytes!("test-p256.der");
	let (ktype, cmp) = convert_key_from_cwk2(&skey[..]).map(|x|x.split()).unwrap();
	assert_eq!(ktype, KeyFormat::Ecdsa);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn p256_jose()
{
	let skey = include_bytes!("test-p256.jose");
	let tkey = include_bytes!("test-p256.der");
	let (ktype, cmp) = convert_key_from_jwk2(&skey[..]).map(|x|x.split()).unwrap();
	assert_eq!(ktype, KeyFormat::Ecdsa);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn p384_sexpr()
{
	let skey = include_bytes!("test-p384.sexp");
	let tkey = include_bytes!("test-p384.der");
	let (ktype, cmp) = convert_key_from_sexpr2(&skey[..]).map(|x|x.split()).unwrap();
	assert_eq!(ktype, KeyFormat::Ecdsa);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn p384_cose()
{
	let skey = include_bytes!("test-p384.cose");
	let tkey = include_bytes!("test-p384.der");
	let (ktype, cmp) = convert_key_from_cwk2(&skey[..]).map(|x|x.split()).unwrap();
	assert_eq!(ktype, KeyFormat::Ecdsa);
	assert_eq!(cmp, &tkey[..]);
}

#[test]
fn p384_jose()
{
	let skey = include_bytes!("test-p384.jose");
	let tkey = include_bytes!("test-p384.der");
	let (ktype, cmp) = convert_key_from_jwk2(&skey[..]).map(|x|x.split()).unwrap();
	assert_eq!(ktype, KeyFormat::Ecdsa);
	assert_eq!(cmp, &tkey[..]);
}
