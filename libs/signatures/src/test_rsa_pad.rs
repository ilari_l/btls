use super::RsaHash;
use super::validate_pkcs1;
use super::validate_pss;
use btls_aux_hash::Hash;
use btls_aux_hash::Sha256;
use btls_aux_serialization::Asn1Tag;
use btls_aux_serialization::Sink;
use core::iter::repeat;
use core::ops::Deref;
use std::vec::Vec;


#[test]
fn pkcs1_basic_verify()
{
	//Basic test, no NULL, so it is valid for both maybe-NULL and no-NULL modes.
	let hash = [5,15,11];
	let hoid = [2,6,2,1];
	let mut sig = Vec::new();
	sig.write_u16(1).unwrap();
	sig.extend(repeat(255u8).take(8));
	sig.write_u8(0).unwrap();
	sig.asn1_fn(Asn1Tag::SEQUENCE, |x|{
		x.asn1_fn(Asn1Tag::SEQUENCE, |y|{
			y.asn1_fn(Asn1Tag::OID, |z|z.write_slice(&hoid))
		})?;
		x.asn1_fn(Asn1Tag::OCTET_STRING, |y|y.write_slice(&hash))
	}).unwrap();
	assert!(validate_pkcs1(&sig, &hash, &hoid, false).is_ok());
	assert!(validate_pkcs1(&sig, &hash, &hoid, true).is_ok());
}

#[test]
fn pkcs1_verify_trailing_crap()
{
	let hash = [5,15,11];
	let hoid = [2,6,2,1];
	let mut sig = Vec::new();
	sig.write_u16(1).unwrap();
	sig.extend(repeat(255u8).take(8));
	sig.write_u8(0).unwrap();
	sig.asn1_fn(Asn1Tag::SEQUENCE, |x|{
		x.asn1_fn(Asn1Tag::SEQUENCE, |y|{
			y.asn1_fn(Asn1Tag::OID, |z|z.write_slice(&hoid))
		})?;
		x.asn1_fn(Asn1Tag::OCTET_STRING, |y|y.write_slice(&hash))
	}).unwrap();
	sig.write_u8(0).unwrap();
	assert!(validate_pkcs1(&sig, &hash, &hoid, false).is_err());
	assert!(validate_pkcs1(&sig, &hash, &hoid, true).is_err());
}

#[test]
fn pkcs1_basic_verify_null()
{
	//Basic test, with NULL, so it is valid for both maybe-NULL mode, but not no-NULL mode.
	let hash = [5,15,11];
	let hoid = [2,6,2,1];
	let mut sig = Vec::new();
	sig.write_u16(1).unwrap();
	sig.extend(repeat(255u8).take(8));
	sig.write_u8(0).unwrap();
	sig.asn1_fn(Asn1Tag::SEQUENCE, |x|{
		x.asn1_fn(Asn1Tag::SEQUENCE, |y|{
			y.asn1_fn(Asn1Tag::OID, |z|z.write_slice(&hoid))?;
			y.asn1_fn(Asn1Tag::NULL, |_|Ok(()))
		})?;
		x.asn1_fn(Asn1Tag::OCTET_STRING, |y|y.write_slice(&hash))
	}).unwrap();
	assert!(validate_pkcs1(&sig, &hash, &hoid, false).is_err());	//NULL present and not allowed.
	assert!(validate_pkcs1(&sig, &hash, &hoid, true).is_ok());
}

#[test]
fn pkcs1_basic_verify_null_not_empty()
{
	//Invalid: The NULL is not empty.
	let hash = [5,15,11];
	let hoid = [2,6,2,1];
	let mut sig = Vec::new();
	sig.write_u16(1).unwrap();
	sig.extend(repeat(255u8).take(8));
	sig.write_u8(0).unwrap();
	sig.asn1_fn(Asn1Tag::SEQUENCE, |x|{
		x.asn1_fn(Asn1Tag::SEQUENCE, |y|{
			y.asn1_fn(Asn1Tag::OID, |z|z.write_slice(&hoid))?;
			y.asn1_fn(Asn1Tag::NULL, |z|z.write_u8(0))
		})?;
		x.asn1_fn(Asn1Tag::OCTET_STRING, |y|y.write_slice(&hash))
	}).unwrap();
	let hash = [5,15,11];
	let hoid = [2,6,2,1];
	assert!(validate_pkcs1(&sig, &hash, &hoid, true).is_err());
}

#[test]
fn pkcs1_verify_not_null()
{
	//Invalid: Where there might be NULL, there is OCTET STRING.
	let hash = [5,15,11];
	let hoid = [2,6,2,1];
	let mut sig = Vec::new();
	sig.write_u16(1).unwrap();
	sig.extend(repeat(255u8).take(8));
	sig.write_u8(0).unwrap();
	sig.asn1_fn(Asn1Tag::SEQUENCE, |x|{
		x.asn1_fn(Asn1Tag::SEQUENCE, |y|{
			y.asn1_fn(Asn1Tag::OID, |z|z.write_slice(&hoid))?;
			y.asn1_fn(Asn1Tag::OCTET_STRING, |_|Ok(()))
		})?;
		x.asn1_fn(Asn1Tag::OCTET_STRING, |y|y.write_slice(&hash))
	}).unwrap();
	assert!(validate_pkcs1(&sig, &hash, &hoid, true).is_err());
}

#[test]
fn pkcs1_verify_null_extra_algid()
{
	//Invalid: Extra member in algid.
	let hash = [5,15,11];
	let hoid = [2,6,2,1];
	let mut sig = Vec::new();
	sig.write_u16(1).unwrap();
	sig.extend(repeat(255u8).take(8));
	sig.write_u8(0).unwrap();
	sig.asn1_fn(Asn1Tag::SEQUENCE, |x|{
		x.asn1_fn(Asn1Tag::SEQUENCE, |y|{
			y.asn1_fn(Asn1Tag::OID, |z|z.write_slice(&hoid))?;
			y.asn1_fn(Asn1Tag::NULL, |_|Ok(()))?;
			y.asn1_fn(Asn1Tag::OCTET_STRING, |_|Ok(()))
		})?;
		x.asn1_fn(Asn1Tag::OCTET_STRING, |y|y.write_slice(&hash))
	}).unwrap();
	assert!(validate_pkcs1(&sig, &hash, &hoid, true).is_err());
}

#[test]
fn pkcs1_verify_null_extra_seq()
{
	//Invalid: Extra memeber in top SEQUENCE.
	let hash = [5,15,11];
	let hoid = [2,6,2,1];
	let mut sig = Vec::new();
	sig.write_u16(1).unwrap();
	sig.extend(repeat(255u8).take(8));
	sig.write_u8(0).unwrap();
	sig.asn1_fn(Asn1Tag::SEQUENCE, |x|{
		x.asn1_fn(Asn1Tag::SEQUENCE, |y|{
			y.asn1_fn(Asn1Tag::OID, |z|z.write_slice(&hoid))?;
			y.asn1_fn(Asn1Tag::NULL, |_|Ok(()))
		})?;
		x.asn1_fn(Asn1Tag::OCTET_STRING, |y|y.write_slice(&hash))?;
		x.asn1_fn(Asn1Tag::OCTET_STRING, |_|Ok(()))
	}).unwrap();
	assert!(validate_pkcs1(&sig, &hash, &hoid, true).is_err());
}

#[test]
fn pkcs1_verify_null_extra_top()
{
	//Invalid: Extra member after digestinfo.
	let hash = [5,15,11];
	let hoid = [2,6,2,1];
	let mut sig = Vec::new();
	sig.write_u16(1).unwrap();
	sig.extend(repeat(255u8).take(8));
	sig.write_u8(0).unwrap();
	sig.asn1_fn(Asn1Tag::SEQUENCE, |x|{
		x.asn1_fn(Asn1Tag::SEQUENCE, |y|{
			y.asn1_fn(Asn1Tag::OID, |z|z.write_slice(&hoid))?;
			y.asn1_fn(Asn1Tag::NULL, |_|Ok(()))
		})?;
		x.asn1_fn(Asn1Tag::OCTET_STRING, |y|y.write_slice(&hash))
	}).unwrap();
	sig.asn1_fn(Asn1Tag::OCTET_STRING, |_|Ok(())).unwrap();
	assert!(validate_pkcs1(&sig, &hash, &hoid, true).is_err());
}

#[test]
fn pkcs1_verify_wrong_hash()
{
	//Invalid: Hash is wrong.
	let hash = [5,15,11];
	let whash = [5,15,12];
	let hoid = [2,6,2,1];
	let mut sig = Vec::new();
	sig.write_u16(1).unwrap();
	sig.extend(repeat(255u8).take(8));
	sig.write_u8(0).unwrap();
	sig.asn1_fn(Asn1Tag::SEQUENCE, |x|{
		x.asn1_fn(Asn1Tag::SEQUENCE, |y|{
			y.asn1_fn(Asn1Tag::OID, |z|z.write_slice(&hoid))
		})?;
		x.asn1_fn(Asn1Tag::OCTET_STRING, |y|y.write_slice(&whash))
	}).unwrap();
	assert!(validate_pkcs1(&sig, &hash, &hoid, false).is_err());
	assert!(validate_pkcs1(&sig, &hash, &hoid, true).is_err());
}

#[test]
fn pkcs1_verify_wrong_function()
{
	//Invalid: Hash function is wrong.
	let hash = [5,15,11];
	let hoid = [2,6,2,1];
	let woid = [2,6,2,2];
	let mut sig = Vec::new();
	sig.write_u16(1).unwrap();
	sig.extend(repeat(255u8).take(8));
	sig.write_u8(0).unwrap();
	sig.asn1_fn(Asn1Tag::SEQUENCE, |x|{
		x.asn1_fn(Asn1Tag::SEQUENCE, |y|{
			y.asn1_fn(Asn1Tag::OID, |z|z.write_slice(&woid))
		})?;
		x.asn1_fn(Asn1Tag::OCTET_STRING, |y|y.write_slice(&hash))
	}).unwrap();
	assert!(validate_pkcs1(&sig, &hash, &hoid, false).is_err());
	assert!(validate_pkcs1(&sig, &hash, &hoid, true).is_err());
}

#[test]
fn pkcs1_wrong_hashlen()
{
	//Invalid: Hash length is wrong (but common prefix matches)
	let hash = [5,15,11];
	let whash = [5,15,11,0];
	let hoid = [2,6,2,1];
	let mut sig = Vec::new();
	sig.write_u16(1).unwrap();
	sig.extend(repeat(255u8).take(8));
	sig.write_u8(0).unwrap();
	sig.asn1_fn(Asn1Tag::SEQUENCE, |x|{
		x.asn1_fn(Asn1Tag::SEQUENCE, |y|{
			y.asn1_fn(Asn1Tag::OID, |z|z.write_slice(&hoid))
		})?;
		x.asn1_fn(Asn1Tag::OCTET_STRING, |y|y.write_slice(&whash))
	}).unwrap();
	assert!(validate_pkcs1(&sig, &hash, &hoid, false).is_err());
	assert!(validate_pkcs1(&sig, &hash, &hoid, true).is_err());
}

#[test]
fn pkcs1_wrong_hashlen2()
{
	//Invalid: Hash length is wrong (but common prefix matches)
	let hash = [5,15,11,0];
	let whash = [5,15,11];
	let hoid = [2,6,2,1];
	let mut sig = Vec::new();
	sig.write_u16(1).unwrap();
	sig.extend(repeat(255u8).take(8));
	sig.write_u8(0).unwrap();
	sig.asn1_fn(Asn1Tag::SEQUENCE, |x|{
		x.asn1_fn(Asn1Tag::SEQUENCE, |y|{
			y.asn1_fn(Asn1Tag::OID, |z|z.write_slice(&hoid))
		})?;
		x.asn1_fn(Asn1Tag::OCTET_STRING, |y|y.write_slice(&whash))
	}).unwrap();
	assert!(validate_pkcs1(&sig, &hash, &hoid, false).is_err());
	assert!(validate_pkcs1(&sig, &hash, &hoid, true).is_err());
}

#[test]
fn pkcs1_noncanon_len()
{
	//Invalid: Non-canonical length.
	let hash = [5,15,11];
	let hoid = [2,6,2,1];
	let mut sig = Vec::new();
	sig.write_u16(1).unwrap();
	sig.extend(repeat(255u8).take(8));
	sig.write_u8(0).unwrap();
	sig.asn1_fn(Asn1Tag::SEQUENCE, |x|{
		x.asn1_fn(Asn1Tag::SEQUENCE, |y|{
			y.write_slice(&[6,129,hoid.len() as u8])?;
			y.write_slice(&hoid)
		})?;
		x.asn1_fn(Asn1Tag::OCTET_STRING, |y|y.write_slice(&hash))
	}).unwrap();
	assert!(validate_pkcs1(&sig, &hash, &hoid, false).is_err());
	assert!(validate_pkcs1(&sig, &hash, &hoid, true).is_err());
}

#[test]
fn pkcs1_noncanon_len2()
{
	//Invalid: Non-canonical length.
	let hash = [5,15,11];
	let hoid = [2,6,2,1];
	let mut sig = Vec::new();
	sig.write_u16(1).unwrap();
	sig.extend(repeat(255u8).take(8));
	sig.write_u8(0).unwrap();
	sig.asn1_fn(Asn1Tag::SEQUENCE, |x|{
		x.asn1_fn(Asn1Tag::SEQUENCE, |y|{
			y.write_slice(&[6,130,0,hoid.len() as u8])?;
			y.write_slice(&hoid)
		})?;
		x.asn1_fn(Asn1Tag::OCTET_STRING, |y|y.write_slice(&hash))
	}).unwrap();
	assert!(validate_pkcs1(&sig, &hash, &hoid, false).is_err());
	assert!(validate_pkcs1(&sig, &hash, &hoid, true).is_err());
}

#[test]
fn pkcs1_extra_pad()
{
	//Valid: More than minimum padding.
	let hash = [5,15,11];
	let hoid = [2,6,2,1];
	let mut sig = Vec::new();
	sig.write_u16(1).unwrap();
	sig.extend(repeat(255u8).take(9));
	sig.write_u8(0).unwrap();
	sig.asn1_fn(Asn1Tag::SEQUENCE, |x|{
		x.asn1_fn(Asn1Tag::SEQUENCE, |y|{
			y.asn1_fn(Asn1Tag::OID, |z|z.write_slice(&hoid))
		})?;
		x.asn1_fn(Asn1Tag::OCTET_STRING, |y|y.write_slice(&hash))
	}).unwrap();
	assert!(validate_pkcs1(&sig, &hash, &hoid, false).is_ok());
	assert!(validate_pkcs1(&sig, &hash, &hoid, true).is_ok());
}

#[test]
fn pkcs1_pad7()
{
	//Invalid: 7 bytes padding, not required 8.
	let hash = [5,15,11];
	let hoid = [2,6,2,1];
	let mut sig = Vec::new();
	sig.write_u16(1).unwrap();
	sig.extend(repeat(255u8).take(7));
	sig.write_u8(0).unwrap();
	sig.asn1_fn(Asn1Tag::SEQUENCE, |x|{
		x.asn1_fn(Asn1Tag::SEQUENCE, |y|{
			y.asn1_fn(Asn1Tag::OID, |z|z.write_slice(&hoid))
		})?;
		x.asn1_fn(Asn1Tag::OCTET_STRING, |y|y.write_slice(&hash))
	}).unwrap();
	assert!(validate_pkcs1(&sig, &hash, &hoid, false).is_err());
	assert!(validate_pkcs1(&sig, &hash, &hoid, true).is_err());
}

#[test]
fn pkcs1_wrong_pad_end()
{
	//Invalid: Paddig ends with 1, not 0.
	let hash = [5,15,11];
	let hoid = [2,6,2,1];
	let mut sig = Vec::new();
	sig.write_u16(1).unwrap();
	sig.extend(repeat(255u8).take(8));
	sig.write_u8(1).unwrap();
	sig.asn1_fn(Asn1Tag::SEQUENCE, |x|{
		x.asn1_fn(Asn1Tag::SEQUENCE, |y|{
			y.asn1_fn(Asn1Tag::OID, |z|z.write_slice(&hoid))
		})?;
		x.asn1_fn(Asn1Tag::OCTET_STRING, |y|y.write_slice(&hash))
	}).unwrap();
	assert!(validate_pkcs1(&sig, &hash, &hoid, false).is_err());
	assert!(validate_pkcs1(&sig, &hash, &hoid, true).is_err());
}

#[test]
fn pkcs1_wrong_pad_start()
{
	//Invalid: Padding part starts with 2, not 1.
	let hash = [5,15,11];
	let hoid = [2,6,2,1];
	let mut sig = Vec::new();
	sig.write_u16(2).unwrap();
	sig.extend(repeat(255u8).take(8));
	sig.write_u8(0).unwrap();
	sig.asn1_fn(Asn1Tag::SEQUENCE, |x|{
		x.asn1_fn(Asn1Tag::SEQUENCE, |y|{
			y.asn1_fn(Asn1Tag::OID, |z|z.write_slice(&hoid))
		})?;
		x.asn1_fn(Asn1Tag::OCTET_STRING, |y|y.write_slice(&hash))
	}).unwrap();
	assert!(validate_pkcs1(&sig, &hash, &hoid, false).is_err());
	assert!(validate_pkcs1(&sig, &hash, &hoid, true).is_err());
}

fn synth_signature(em: &[u8], hash: &[u8], hoid: &[u8], omitted: usize)
{
	//Synthethize and compare.
	let mut em2 = Vec::new();
	em2.write_u16(1).unwrap();
	em2.extend(repeat(255u8).take(omitted));
	em2.write_u8(0).unwrap();
	em2.asn1_fn(Asn1Tag::SEQUENCE, |x|{
		x.asn1_fn(Asn1Tag::SEQUENCE, |y|{
			y.asn1_fn(Asn1Tag::OID, |z|z.write_slice(&hoid))?;
			y.asn1_fn(Asn1Tag::NULL, |_|Ok(()))
		})?;
		x.asn1_fn(Asn1Tag::OCTET_STRING, |y|y.write_slice(&hash))
	}).unwrap();
	assert_eq!(em, &em2[..]);
}

#[test]
fn pkcs1_real_sig1()
{
	let omitted = 202;
	let mut em=vec![0, 1, 0, 48, 49, 48, 13, 6, 9, 96, 134, 72, 1, 101, 3, 4, 2, 1, 5, 0, 4, 32];
	let hash=[123, 183, 49, 239, 135, 135, 6, 28, 30, 75, 253, 116, 88, 105, 90, 49, 210, 162, 169, 227,
		92, 80, 213, 97, 18, 109, 62, 129, 253, 81, 142, 184];
	let hoid=[96, 134, 72, 1, 101, 3, 4, 2, 1];
	let allow_null=true;
	for _ in 0..omitted { em.insert(2, 255); }
	for b in hash.iter() { em.push(*b); }
	assert_eq!(em.len(), 256);
	assert!(validate_pkcs1(&em, &hash, &hoid, allow_null).is_ok());
	synth_signature(&em, &hash, &hoid, omitted);
}

#[test]
fn pkcs1_real_sig2()
{
	let omitted = 426;
	let mut em=vec![0, 1, 0, 48, 81, 48, 13, 6, 9, 96, 134, 72, 1, 101, 3, 4, 2, 3, 5, 0, 4, 64];
	let hash=[217, 213, 115, 149, 6, 231, 92, 102, 74, 35, 80, 216, 92, 194, 68, 254, 83, 153, 184, 115,
		49, 176, 152, 17, 78, 107, 146, 72, 23, 247, 182, 195, 31, 78, 209, 102, 116, 107, 106, 121, 125,
		138, 233, 132, 68, 46, 54, 62, 219, 93, 79, 37, 222, 245, 204, 66, 54, 105, 188, 56, 215, 168,
		188, 32];
	let hoid=[96, 134, 72, 1, 101, 3, 4, 2, 3];
	let allow_null=true;
	for _ in 0..omitted { em.insert(2, 255); }
	for b in hash.iter() { em.push(*b); }
	assert_eq!(em.len(), 512);
	assert!(validate_pkcs1(&em, &hash, &hoid, allow_null).is_ok());
	synth_signature(&em, &hash, &hoid, omitted);
}

#[test]
fn pkcs1_sim_sig3()
{
	let omitted = 314;
	let mut em=vec![0, 1, 0, 48, 65, 48, 13, 6, 9, 96, 134, 72, 1, 101, 3, 4, 2, 2, 5, 0, 4, 48];
	let hash=b"\x42\x84\xb5\x69\x4c\xa6\xc0\xd2\xcf\x47\x89\xa0\xb9\x5a\xc8\x02\x5c\x81\x8d\xe5\x23\x04\x36\
		\x4b\xe7\xcd\x29\x81\xb2\xd2\xed\xc6\x85\xb3\x22\x27\x7e\xc2\x58\x19\x96\x24\x13\xd8\xc9\xb2\xc1\
		\xf5";
	let hoid=[96, 134, 72, 1, 101, 3, 4, 2, 2];
	let allow_null=true;
	for _ in 0..omitted { em.insert(2, 255); }
	for b in hash.iter() { em.push(*b); }
	assert_eq!(em.len(), 384);
	assert!(validate_pkcs1(&em, hash, &hoid, allow_null).is_ok());
	synth_signature(&em, hash, &hoid, omitted);
}

#[test]
fn pkcs1_sim_sig3_without_null()
{
	let omitted = 316;
	let mut em=vec![0, 1, 0, 48, 63, 48, 11, 6, 9, 96, 134, 72, 1, 101, 3, 4, 2, 2, 4, 48];
	let hash=b"\x42\x84\xb5\x69\x4c\xa6\xc0\xd2\xcf\x47\x89\xa0\xb9\x5a\xc8\x02\x5c\x81\x8d\xe5\x23\x04\x36\
		\x4b\xe7\xcd\x29\x81\xb2\xd2\xed\xc6\x85\xb3\x22\x27\x7e\xc2\x58\x19\x96\x24\x13\xd8\xc9\xb2\xc1\
		\xf5";
	let hoid=[96, 134, 72, 1, 101, 3, 4, 2, 2];
	let allow_null=true;
	for _ in 0..omitted { em.insert(2, 255); }
	for b in hash.iter() { em.push(*b); }
	assert_eq!(em.len(), 384);
	assert!(validate_pkcs1(&em, hash, &hoid, allow_null).is_ok());
}

#[test]
fn pkcs1_sim_sig3_crap()
{
	let omitted = 313;
	let mut em=vec![0, 1, 0, 48, 65, 48, 13, 6, 9, 96, 134, 72, 1, 101, 3, 4, 2, 2, 5, 0, 4, 48];
	let hash=b"\x42\x84\xb5\x69\x4c\xa6\xc0\xd2\xcf\x47\x89\xa0\xb9\x5a\xc8\x02\x5c\x81\x8d\xe5\x23\x04\x36\
		\x4b\xe7\xcd\x29\x81\xb2\xd2\xed\xc6\x85\xb3\x22\x27\x7e\xc2\x58\x19\x96\x24\x13\xd8\xc9\xb2\xc1\
		\xf5";
	let hoid=[96, 134, 72, 1, 101, 3, 4, 2, 2];
	let allow_null=true;
	for _ in 0..omitted { em.insert(2, 255); }
	for b in hash.iter() { em.push(*b); }
	em.push(255);
	assert_eq!(em.len(), 384);
	assert!(validate_pkcs1(&em, hash, &hoid, allow_null).is_err());
}

#[test]
fn pkcs1_sim_sig3_every_corrupt_bit_matters()
{
	let omitted = 314;
	let mut em=vec![0, 1, 0, 48, 65, 48, 13, 6, 9, 96, 134, 72, 1, 101, 3, 4, 2, 2, 5, 0, 4, 48];
	let hash=b"\x42\x84\xb5\x69\x4c\xa6\xc0\xd2\xcf\x47\x89\xa0\xb9\x5a\xc8\x02\x5c\x81\x8d\xe5\x23\x04\x36\
		\x4b\xe7\xcd\x29\x81\xb2\xd2\xed\xc6\x85\xb3\x22\x27\x7e\xc2\x58\x19\x96\x24\x13\xd8\xc9\xb2\xc1\
		\xf5";
	let hoid=[96, 134, 72, 1, 101, 3, 4, 2, 2];
	let allow_null=true;
	for _ in 0..omitted { em.insert(2, 255); }
	for b in hash.iter() { em.push(*b); }
	assert_eq!(em.len(), 384);
	for i in 0..3072 {
		em[i/8] ^= 1 << i%8;
		assert!(validate_pkcs1(&em, hash, &hoid, allow_null).is_err());
		em[i/8] ^= 1 << i%8;
	}
	assert!(validate_pkcs1(&em, hash, &hoid, allow_null).is_ok());
}

#[test]
fn pkcs1_sim_sig3_size_threshold()
{
	let omitted = 7;
	let mut em=vec![0, 1, 0, 48, 65, 48, 13, 6, 9, 96, 134, 72, 1, 101, 3, 4, 2, 2, 5, 0, 4, 48];
	let hash=b"\x42\x84\xb5\x69\x4c\xa6\xc0\xd2\xcf\x47\x89\xa0\xb9\x5a\xc8\x02\x5c\x81\x8d\xe5\x23\x04\x36\
		\x4b\xe7\xcd\x29\x81\xb2\xd2\xed\xc6\x85\xb3\x22\x27\x7e\xc2\x58\x19\x96\x24\x13\xd8\xc9\xb2\xc1\
		\xf5";
	let hoid=[96, 134, 72, 1, 101, 3, 4, 2, 2];
	let allow_null=true;
	for _ in 0..omitted { em.insert(2, 255); }
	for b in hash.iter() { em.push(*b); }
	assert!(validate_pkcs1(&em, hash, &hoid, allow_null).is_err());
	em.insert(2, 255);
	assert!(validate_pkcs1(&em, hash, &hoid, allow_null).is_ok());
}

#[test]
fn pkcs1_sim_sig3_too_small()
{
}

fn pss_test_sig(msghash: &[u8; 32], salt: &[u8;32]) -> [u8;97]
{
	let hash = Sha256::function();
	let mut buf = [0;97];	//3*32+1.
	buf[31] = 1; //Separator.
	buf[96] = 0xbc;	//Trailer.
	let realhash = hash.hash_c(|x|{
		x.append(&[0;8]);
		x.append(msghash);
		x.append(salt);
	});
	(&mut buf[64..96]).copy_from_slice(&realhash);
	(&mut buf[32..64]).copy_from_slice(salt);

	let maskhash = hash.hash_c(|x|{
		x.append(&realhash);
		x.append(&[0,0,0,0]);
	});
	for i in 0..32 { buf[i] ^= maskhash.deref()[i] };

	let maskhash = hash.hash_c(|x|{
		x.append(&realhash);
		x.append(&[0,0,0,1]);
	});
	for i in 0..32 { buf[i+32] ^= maskhash.deref()[i] };

	buf[0] &= 127;	//High bit.
	buf
}

#[test]
fn pss_test_basic()
{
	let msghash = [1;32];
	let salt = [3;32];	//Picked to cause first mask bit to be 1.
	let buf = pss_test_sig(&msghash, &salt);

	assert!(validate_pss(&buf, &msghash, RsaHash::Sha256, 1).is_ok());
}

#[test]
fn pss_test_strip4()
{
	//The entiere first nibble is stripped.
	let msghash = [1;32];
	let salt = [3;32];	//Picked to cause first mask bit to be 1.
	let mut buf = pss_test_sig(&msghash, &salt);
	buf[0] &= 15;
	assert!(validate_pss(&buf, &msghash, RsaHash::Sha256, 4).is_ok());
}

#[test]
fn pss_test_strip7()
{
	//Maximum 7 bit strip.
	let msghash = [1;32];
	let salt = [3;32];	//Picked to cause first mask bit to be 1.
	let mut buf = pss_test_sig(&msghash, &salt);
	buf[0] &= 1;
	assert!(validate_pss(&buf, &msghash, RsaHash::Sha256, 7).is_ok());
}

#[test]
fn pss_test_bad_pad_end()
{
	let msghash = [1;32];
	let salt = [3;32];	//Picked to cause first mask bit to be 1.
	let mut buf = pss_test_sig(&msghash, &salt);
	buf[31] ^= 0x03;	//Change pad end to 2 instead of 1.

	assert!(validate_pss(&buf, &msghash, RsaHash::Sha256, 1).is_err());
}

#[test]
fn pss_test_bad_pad()
{
	let msghash = [1;32];
	let salt = [3;32];	//Picked to cause first mask bit to be 1.
	let mut buf = pss_test_sig(&msghash, &salt);
	buf[30] ^= 0x01;	//Change last pad to 1.

	assert!(validate_pss(&buf, &msghash, RsaHash::Sha256, 1).is_err());
}

#[test]
fn pss_test_hash_mismatch()
{
	let msghash = [1;32];
	let msghash2 = [2;32];
	let salt = [3;32];	//Picked to cause first mask bit to be 1.
	let buf = pss_test_sig(&msghash, &salt);

	assert!(validate_pss(&buf, &msghash2, RsaHash::Sha256, 1).is_err());
}

#[test]
fn pss_test_wrong_trailer()
{
	let msghash = [1;32];
	let salt = [3;32];	//Picked to cause first mask bit to be 1.
	let mut buf = pss_test_sig(&msghash, &salt);
	buf[96] = 0xbb;

	assert!(validate_pss(&buf, &msghash, RsaHash::Sha256, 1).is_err());
}
