use btls_aux_fail::fail_if;
use core::fmt::Debug;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;

///Error from base64 decoding.
#[derive(Debug)]
pub enum Base64DecodeError<W:OutputStream>
{
	///Invalid Base64 input.
	InvalidInput,
	///I/O error.
	Io(<W as OutputStream>::Error),
}

impl<W:OutputStream> Display for Base64DecodeError<W>
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&Base64DecodeError::InvalidInput => fmt.write_str("Invalid Base64 input"),
			&Base64DecodeError::Io(ref err) => write!(fmt, "Output error: {err}"),
		}
	}
}

///Output stream.
pub trait OutputStream
{
	///The error type.
	type Error: Sized+Display+Debug;
	///Write `data` into this stream.
	fn write(&mut self, data: &[u8]) -> Result<(), <Self as OutputStream>::Error>;
}

///BASE64 decoder.
///
///Note, this is different from `base64url`.
///
///This structure is created by the [`new()`](#method.new) method.
///
///After creation, one can feed more data to be decoded by [`data()`](#method.data) method and then end decoding
///using the [`end()`](#method.end) method.
pub struct Base64Decoder
{
	state: u32,
}

impl Base64Decoder
{
	///Create a new BASE64 decoder.
	pub fn new() -> Base64Decoder
	{
		Base64Decoder{state: 0}
	}
	///Decode BASE64 data fragment `fragment`, outputting the decoding result into `output`.
	///
	///The decoded output is written into a stream. The bits that are not decoded yet are saved for next block,
	///so the concatenation of results is result of concatenation. The output runs at most 3 bytes behind the
	///input.
	///
	///On success, returns `Ok(())`. Otherwise returns `Err(err)`, where `err` is the error.
	pub fn data<Output:OutputStream>(&mut self, fragment: &str, output: &mut Output) ->
		Result<(), Base64DecodeError<Output>>
	{
		use self::Base64DecodeError::*;
		for j in fragment.chars() {
			if j == ' ' || j == '\t' { continue; }
			let x = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".find(j).
				unwrap_or(255);
			self.state = {
				let val = self.state & 0xFFFFFF;
				let pol = self.state >> 24;
				let mut seen_pad = (self.state >> 26) != 0;
				//Too much padding, or non-= after =, or invalid character.
				fail_if!(pol == 4 || (pol > 4 && x != 64) || x > 64, InvalidInput);
				if x == 64 {
					if !seen_pad {
						if pol == 2 {
							let buf = [(val >> 4) as u8];
							output.write(&buf).map_err(Io)?;
						} else if pol == 3 {
							let buf = [(val >> 10) as u8, (val >> 2) as u8];
							output.write(&buf).map_err(Io)?;
						}
					}
					seen_pad = true;
				}				//Padding starts.
				let val = val << 6 | x as u32;
				let pol = pol.wrapping_add(1) & 3;
				if pol == 0 && !seen_pad {
					let buf = [(val >> 16) as u8, (val >> 8) as u8, val as u8];
					output.write(&buf).map_err(Io)?;
				}
				(seen_pad as u32) << 26 | pol << 24 | (val & 0xFFFFFF)
			};
		}
		Ok(())
	}
	///Decode end of BASE64 data into stream `output`.
	///
	///Altough this method takes no input, it can produce output by flushing the remaining pending input into
	///the output. This produces at most 2 bytes of output. This operation is fallable, as the encoding can
	///be truncated in some illegal place.
	///
	///On success, returns `Ok(())`. Otherwise returns `Err(err)`, where `err` is the error.
	pub fn end<Output:OutputStream>(&self, output: &mut Output) -> Result<(), Base64DecodeError<Output>>
	{
		use self::Base64DecodeError::*;
		let val = self.state & 0xFFFFFF;
		let pol = self.state >> 24;
		//1 mod 4 is not valid size. And if =s are present, there must be correct amount.
		fail_if!(pol == 1 || pol > 4, InvalidInput);
		if pol == 2 {
			let buf = [(val >> 4) as u8];
			output.write(&buf).map_err(Io)?;
		} else if pol == 3 {
			let buf = [(val >> 10) as u8, (val >> 2) as u8];
			output.write(&buf).map_err(Io)?;
		}
		Ok(())
	}
}
