use crate::SignatureAlgorithmEnabled2;
use crate::SignatureAlgorithmTls2;
use crate::SignatureFlags2;
use crate::SignatureKeyPair;
use crate::SignatureTls2;
use btls_aux_random::TemporaryRandomLifetimeTag;
use btls_aux_random::TemporaryRandomStream;

const ALGBIT_ED448: usize = 5;

fn do_test2(tlsid: u16, params: ())
{
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	for _ in 0..100 {
		let msg1 = "Hello, World!";
		let msg2 = "Hello, world!";
		//Allow all signatures because this is to test verifiability of the generated signature.
		let any_policy = SignatureAlgorithmEnabled2::unsafe_any_policy();
		let nrsa_flags = SignatureFlags2::NO_RSA;
		let any_flags = SignatureFlags2::CERTIFICATE;
		let xalgo = SignatureAlgorithmTls2::by_tls_id(0x808).unwrap();
		//First, generate two keys.
		let kp1r = crate::eddsa::Ed448KeyPair::keypair_generate(params, &mut rng).unwrap();
		let kp2r = crate::eddsa::Ed448KeyPair::keypair_generate(params, &mut rng).unwrap();
		//The keys should be different.
		assert!(&kp1r[..] != &kp2r[..]);
		//Then load the keys. Load keys twice to check consistency.
		let (_, kp1, pk1a) = crate::eddsa::Ed448KeyPair::keypair_load(&kp1r).unwrap();
		let (_, _, pk1b) = crate::eddsa::Ed448KeyPair::keypair_load(&kp1r).unwrap();
		let (_, kp2, pk2a) = crate::eddsa::Ed448KeyPair::keypair_load(&kp2r).unwrap();
		let (_, _, pk2b) = crate::eddsa::Ed448KeyPair::keypair_load(&kp2r).unwrap();
		assert_eq!(&pk1a[..], &pk1b[..]);
		assert_eq!(&pk2a[..], &pk2b[..]);
		//Sign some data.
		let mut signature1 = [0;160];
		let mut signature2 = [0;160];
		let signature1len = kp1.keypair_sign(msg1.as_bytes(), tlsid, &mut signature1, &mut rng).unwrap();
		let signature1 = SignatureTls2::new(xalgo, &signature1[..signature1len]);
		let signature2len = kp2.keypair_sign(msg2.as_bytes(), tlsid, &mut signature2, &mut rng).unwrap();
		let signature2 = SignatureTls2::new(xalgo, &signature2[..signature2len]);
		//Now, check the signatures.
		signature1.verify(&pk1a, msg1.as_bytes(), any_policy, nrsa_flags).unwrap();
		signature2.verify(&pk2a, msg2.as_bytes(), any_policy, nrsa_flags).unwrap();
		//The signatures should not check with wrong keys, wrong messages or wrong signatures.
		signature1.verify(&pk1a, msg2.as_bytes(), any_policy, any_flags).unwrap_err();
		signature1.verify(&pk2a, msg1.as_bytes(), any_policy, any_flags).unwrap_err();
		signature1.verify(&pk2a, msg2.as_bytes(), any_policy, any_flags).unwrap_err();
		signature2.verify(&pk1a, msg2.as_bytes(), any_policy, any_flags).unwrap_err();
		signature2.verify(&pk2a, msg1.as_bytes(), any_policy, any_flags).unwrap_err();
		signature2.verify(&pk1a, msg1.as_bytes(), any_policy, any_flags).unwrap_err();
		//The signatures should not verify with algorithms disabled.
		let ecdsa_mask = 1<<ALGBIT_ED448;
		let bad_policy = SignatureAlgorithmEnabled2::from_mask(!ecdsa_mask, !0, !0, true);
		signature1.verify(&pk1a, msg1.as_bytes(), bad_policy, any_flags).unwrap_err();
		signature2.verify(&pk2a, msg2.as_bytes(), bad_policy, any_flags).unwrap_err();
		//These should not validate in NSA mode.
		let nsa_policy = SignatureAlgorithmEnabled2::nsa_mode();
		signature1.verify(&pk1a, msg1.as_bytes(), nsa_policy, any_flags).unwrap_err();
		signature2.verify(&pk2a, msg2.as_bytes(), nsa_policy, any_flags).unwrap_err();
	}
}

#[test] fn sign_ed448_2() { do_test2(0x808, ()); }

fn do_test(tlsid: u16, key_bytes: &[u8])
{
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);

	let (_, key_bytes) = crate::sexp::convert_key_from_sexpr2(&key_bytes[..]).map(|x|x.split()).unwrap();
	let (_, keypair, pubkey) = crate::eddsa::Ed448KeyPair::keypair_load(&key_bytes).unwrap();
	let algo = SignatureAlgorithmTls2::by_tls_id(0x808).unwrap();
	let msg = "Hello, World!";
	let mut signature = [0;160];
	let signaturelen = keypair.keypair_sign(msg.as_bytes(), tlsid, &mut signature, &mut rng).unwrap();
	let signature = SignatureTls2::new(algo, &signature[..signaturelen]);
	//Allow all signatures because this is to test verifiability of the generated signature.
	signature.verify(&pubkey, msg.as_bytes(), SignatureAlgorithmEnabled2::unsafe_any_policy(),
		SignatureFlags2::NO_RSA).unwrap();
}

#[test] fn sign_ed448() { do_test(0x808, include_bytes!("tested448.sexp")); }
