use crate::Hasher;
use crate::InternalKey;
use crate::KeyFormat;
use crate::SignatureAlgorithm2;
use crate::SignatureFormat;
use crate::SignatureKeyPair;
use crate::SignatureKeyPair2;
use crate::SignatureKeyPair3;
use crate::TemporaryRandomStream;
use btls_aux_collections::Arc;
use btls_aux_collections::Cow;
use btls_aux_collections::String;
use btls_aux_collections::ToOwned;
use btls_aux_collections::Vec;
use btls_aux_fail::dtry;
use btls_aux_fail::f_continue;
use btls_aux_fail::f_return;
use btls_aux_fail::ResultExt;
use btls_aux_futures::FutureReceiver;
use btls_aux_tls_iana::SignatureScheme;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::ops::Deref;
use crate::cose::convert_key_from_cwk2;
use crate::csrgen::CsrAssemblyAdapter2;
use crate::csrgen::CsrParams;
use crate::csrgen::get_algorithm_id;
use crate::csrgen::get_raw_csr_tbs;
use crate::jose::convert_key_from_jwk2;
use crate::sexp::convert_key_from_sexpr2;
use crate::verify::SubjectPublicKeyInfo;
use crate::verify::SupportedSchemes;

#[allow(unsafe_code)]
fn transmute_u16_ss_arr<'a>(x: &'a [u16]) -> &'a [SignatureScheme]
{
	//SignatureScheme is internally u16.
	unsafe{core::mem::transmute(x)}
}

#[allow(missing_docs)]
#[deprecated(since="1.10.5", note="Use KeyPair2 instead")]
pub trait KeyPair
{
	fn sign(&self, data: &[u8], algorithm: u16, rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<Vec<u8>, ()>>;
	fn get_schemes(&self) -> SupportedSchemes;
	fn get_public_key(&self) -> SubjectPublicKeyInfo;
	fn get_key_type(&self) -> &'static str;
	fn sign_csr(&self, csr_params: &CsrParams, rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<Vec<u8>, ()>>
	{
		let algs: &[u16] = &self.get_schemes().0;
		let algs = transmute_u16_ss_arr(algs);
		let pubkey = self.get_public_key();
		let tbs = f_return!(get_raw_csr_tbs(&pubkey.0, csr_params), FutureReceiver::from(Err(())));
		let (algoid, algorithm) = f_return!(get_algorithm_id(algs, csr_params.requested_signature_algo),
			FutureReceiver::from(Err(())));
		self.sign(&tbs, algorithm.get(), rng).map(CsrAssemblyAdapter2{tbs, algoid})
	}
}

///A key pair (public key and private key).
///
///This is very similar to [`KeyPair`](trait.KeyPair.html), but with types adjusted a bit.
pub trait KeyPair2
{
	///Get signature algorithms this keypair is for.
	fn get_signature_algorithms<'a>(&'a self) -> &'a [SignatureAlgorithm2];
	///Sign a message `data` using this key.
	///
	///The algorithm used is returned by `get_signature_algorithm()`.
	fn sign_data(&self, data: &[u8], algorithms: SignatureAlgorithm2, rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<(SignatureFormat, Vec<u8>), ()>>;
	///Sign a message `msg` using this key.
	///
	///The algorithm used is returned by `get_signature_algorithm()`.
	fn sign_callback(&self, algorithms: SignatureAlgorithm2, rng: &mut TemporaryRandomStream,
		msg: &mut dyn FnMut(&mut Hasher)) -> FutureReceiver<Result<(SignatureFormat, Vec<u8>), ()>>;
	///Get the public key corresponding to this keypair.
	fn get_public_key2<'a>(&'a self) -> &'a [u8];
	///Get type of key of this keypair.
	///
	///The returned string is a human-readable description of keypair type.
	fn get_key_type2<'a>(&'a self) -> &'a str;
	///Sign a message `data` using algorithm `algorithm`.
	///
	///See [`KeyPair::sign_csr()`](trait.KeyPair2.html#method.sign_csr)
	fn sign_csr2(&self, csr_params: &CsrParams, rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<Vec<u8>, ()>>
	{
		let mut algorithm = None;
		for &algo in self.get_signature_algorithms().iter() {
			//Skip algorithms with no OID, they can not be used.
			f_continue!(algo.get_oid());
			//TODO: Smarter algorithm seletion.
			algorithm = Some(algo);
			break;
		}
		//The signature algorithm better have OID.
		let algorithm = f_return!(algorithm, FutureReceiver::from(Err(())));
		let algoid = f_return!(algorithm.get_oid(), FutureReceiver::from(Err(())));
		let pubkey = self.get_public_key2();
		let tbs = f_return!(get_raw_csr_tbs(pubkey, csr_params), FutureReceiver::from(Err(())));
		self.sign_data(&tbs, algorithm, rng).map(CsrAssemblyAdapter2{tbs, algoid})
	}
}

impl LocalKeyPairStorage
{
	///Get human-readable string for key type.
	pub fn get_key_type(&self) -> &'static str
	{
		match &self.0 {
			&_LocalKeyPairStorage::Ed25519(_, _) => "ed25519",
			&_LocalKeyPairStorage::Ed448(_, _) => "ed448",
			&_LocalKeyPairStorage::Ecdsa(crv, _, _) => crv.get_ecdsa_name(),
		}
	}
	///Load a key.
	pub fn new(data: &[u8]) -> Result<LocalKeyPairStorage, KeyLoadingError>
	{
		use self::KeyLoadingError::*;
		let (kind, data) = convert_key(data).map(|x|x.split())?;
		match kind {
			KeyFormat::Ed25519 => {
				let (_, privkey, pubkey) = crate::eddsa::Ed25519KeyPair::keypair_load(data.deref()).
					map_err(Ed25519)?;
				Ok(LocalKeyPairStorage(_LocalKeyPairStorage::Ed25519(privkey, pubkey)))
			},
			KeyFormat::Ed448 => {
				let (_, privkey, pubkey) = crate::eddsa::Ed448KeyPair::keypair_load(data.deref()).
					map_err(Ed448)?;
				Ok(LocalKeyPairStorage(_LocalKeyPairStorage::Ed448(privkey, pubkey)))
			},
			KeyFormat::Ecdsa => {
				let (variant, privkey, pubkey) =
					crate::ecdsa::EcdsaKeyPair::keypair_load(data.deref()).map_err(Ecdsa)?;
				Ok(LocalKeyPairStorage(_LocalKeyPairStorage::Ecdsa(variant, privkey, pubkey)))
			},
		}
	}
	fn __with_sign_sync(f: impl FnOnce(&mut [u8]) -> Result<usize, ()>) -> Result<Vec<u8>, ()>
	{
		let mut output = [0;256];	//Should be big enough for ECDSA.
		let siglen = dtry!(f(&mut output));
		Ok(dtry!(output.get(..siglen)).to_owned())
	}
	fn __with_sign_fmt_sync(fmt: SignatureFormat, f: impl FnOnce(&mut [u8]) -> Result<usize, ()>) ->
		Result<(SignatureFormat, Vec<u8>), ()>
	{
		let body = Self::__with_sign_sync(f)?;
		Ok((fmt, body))
	}
	fn with_sign(f: impl FnOnce(&mut [u8]) -> Result<usize, ()>) -> FutureReceiver<Result<Vec<u8>, ()>>
	{
		FutureReceiver::from(Self::__with_sign_sync(f))
	}
	fn with_sign_fmt(fmt: SignatureFormat, f: impl FnOnce(&mut [u8]) -> Result<usize, ()>) ->
		FutureReceiver<Result<(SignatureFormat, Vec<u8>), ()>>
	{
		FutureReceiver::from(Self::__with_sign_fmt_sync(fmt, f))
	}
	///Sign data `data` using algorithm `algorithm` (specified as TLS SignatureScheme) with rng `rng`.
	pub fn sign(&self, data: &[u8], algorithm: SignatureScheme, rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<Vec<u8>, ()>>
	{
		let algorithm = algorithm.get();
		Self::with_sign(|output|match &self.0 {
			&_LocalKeyPairStorage::Ed25519(ref key, _) => key.keypair_sign(data, algorithm, output, rng),
			&_LocalKeyPairStorage::Ed448(ref key, _) => key.keypair_sign(data, algorithm, output, rng),
			&_LocalKeyPairStorage::Ecdsa(_, ref key, _) => key.keypair_sign(data, algorithm, output, rng),
		})
	}
	///Get supported signature schemes.
	pub fn get_schemes(&self) -> Cow<'static, [u16]>
	{
		match &self.0 {
			&_LocalKeyPairStorage::Ed25519(ref key, _) => key.supported_tls_schemes(),
			&_LocalKeyPairStorage::Ed448(ref key, _) => key.supported_tls_schemes(),
			&_LocalKeyPairStorage::Ecdsa(_, ref key, _) => key.supported_tls_schemes(),
		}
	}
	///Get public key as X.509 SubjectPublicKeyInfo.
	pub fn get_pubkey(&self) -> Vec<u8>
	{
		match &self.0 {
			&_LocalKeyPairStorage::Ed25519(_, ref pkey) => pkey.clone(),
			&_LocalKeyPairStorage::Ed448(_, ref pkey) => pkey.clone(),
			&_LocalKeyPairStorage::Ecdsa(_, _, ref pkey) => pkey.clone(),
		}
	}
	///Get signature format.
	pub fn get_signature_format(&self) -> SignatureFormat
	{
		match &self.0 {
			&_LocalKeyPairStorage::Ed25519(ref key, _) => key.get_signature_format(),
			&_LocalKeyPairStorage::Ed448(ref key, _) => key.get_signature_format(),
			&_LocalKeyPairStorage::Ecdsa(_, ref key, _) => key.get_signature_format()
		}
	}
	///Get algorithm `sign_data()` and `sign_callback()` sign with.
	pub fn get_signature_algorithms<'a>(&'a self) -> &'a [SignatureAlgorithm2]
	{
		match &self.0 {
			&_LocalKeyPairStorage::Ed25519(ref key, _) => key.get_signature_algorithms(),
			&_LocalKeyPairStorage::Ed448(ref key, _) => key.get_signature_algorithms(),
			&_LocalKeyPairStorage::Ecdsa(_, ref key, _) => key.get_signature_algorithms(),
		}
	}
	///Sign data
	pub fn sign_data(&self, data: &[u8], algorithm: SignatureAlgorithm2, rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<(SignatureFormat, Vec<u8>), ()>>
	{
		let fmt = self.get_signature_format();
		Self::with_sign_fmt(fmt, |output|match &self.0 {
			&_LocalKeyPairStorage::Ed25519(ref key, _) => key.sign_data(data, algorithm, output, rng),
			&_LocalKeyPairStorage::Ed448(ref key, _) => key.sign_data(data, algorithm, output, rng),
			&_LocalKeyPairStorage::Ecdsa(_, ref key, _) => key.sign_data(data, algorithm, output, rng),
		})
	}
	///Sign data (callback)
	pub fn sign_callback(&self, algorithm: SignatureAlgorithm2, rng: &mut TemporaryRandomStream,
		msg: &mut dyn FnMut(&mut Hasher)) -> FutureReceiver<Result<(SignatureFormat, Vec<u8>), ()>>
	{
		let fmt = self.get_signature_format();
		Self::with_sign_fmt(fmt, |output|match &self.0 {
			&_LocalKeyPairStorage::Ed25519(ref key, _) => key.sign_callback(output, algorithm, rng, msg),
			&_LocalKeyPairStorage::Ed448(ref key, _) => key.sign_callback(output, algorithm, rng, msg),
			&_LocalKeyPairStorage::Ecdsa(_, ref key, _) =>
				key.sign_callback(output, algorithm, rng, msg),
		})
	}
}

pub(super) struct _LocalKeyPair
{
	privkey: LocalKeyPairStorage,
	pubkey: SubjectPublicKeyInfo,
	schemes: SupportedSchemes,
	keytype: &'static str,
}

impl _LocalKeyPair
{
	pub(super) fn new(data: &[u8]) -> Result<_LocalKeyPair, KeyLoadingError>
	{
		let privkey = LocalKeyPairStorage::new(data)?;
		let pubkey = privkey.get_pubkey();
		let schemes = privkey.get_schemes();
		let keytype = privkey.get_key_type();
		Ok(_LocalKeyPair{privkey:privkey, pubkey:SubjectPublicKeyInfo(Arc::new(pubkey)),
			schemes:SupportedSchemes(Arc::new(schemes)), keytype: keytype})
	}
}

///Key pair that is implemented by signing using a local private key.
///
///This implements the [`KeyPair`](trait.KeyPair.html) trait. It is created by one of:
///
/// * Load a keypair from stream: [`new()`](#method.new)
/// * Load a keypair from file: [`new_file()`](#method.new_file)
/// * Generate a transient P-256 key: [`new_transient_ecdsa_p256()`](#method.new_transient_ecdsa_p256)
///
///# DANGER:
///
///The private key is contained in the memory space of the process and thus can be leaked by memory disclosure
///vulernabilities. Such vulernabilities do not have to reside in the TLS implementation, as memory disclosure in
///the application is just as effective for attacks.
///
///The keys that are in Internal Moudle Format can be shells for remote keys. Such keys can not be dumped using
///memory disclosure. In case of remote code execution, the difficulty of dumping the key is dictated by a variety
///of factors, ranging from easy to very hard.
#[derive(Clone)]
pub struct LocalKeyPair(pub(super) Arc<_LocalKeyPair>);

impl LocalKeyPair
{
	fn _new(privkey: &[u8], name: &str) -> Result<LocalKeyPair, KeyReadError>
	{
		Ok(LocalKeyPair(Arc::new(_LocalKeyPair::new(privkey).
			map_err(|x|KeyReadError::ParseError(name.to_owned(), x))?)))
	}
	///Create a keypair out of stream `privkey` a byte slice.
	///
	///The key is called `name` in various log and error messages.
	///
	///On success, returns `Ok(keypair)` where `keypair` is the loaded keypair. Otherwise returns `Err(error)`
	///where `error` describes the error that occured.
	///
	///The following key formats are currently supported.
	///
	/// * COSE key format.
	/// * JWK format.
	/// * S-Expression format.
	pub fn new(privkey: &[u8], name: &str) -> Result<LocalKeyPair, KeyReadError>
	{
		Self::_new(privkey, name)
	}
	///Create a fresh temporary ECDSA P-256 keypair.
	///
	///On success, returns `Ok(keypair)` where `keypair` is the loaded keypair. Otherwise returns `Err(error)`
	///where `error` describes the error that occured.
	///
	///Note that this is only suitable for transient keys: There is no way to save the keypair anywhere. This
	///is mainly meant to generate keys for ACME validation requests.
	pub fn new_transient_ecdsa_p256(rng: &mut TemporaryRandomStream) ->
		Result<LocalKeyPairStorage, KeyReadError>
	{
		use self::KeyReadError::*;
		let (kp, pubkey) = crate::ecdsa::EcdsaKeyPair::new_transient_ecdsa_p256(rng).
			set_err(KeyGenerationFailed)?;
		Ok(LocalKeyPairStorage(_LocalKeyPairStorage::Ecdsa(crate::ecdsa::EcdsaCurve::NsaP256, kp,
			pubkey.clone())))
	}
}

#[allow(deprecated)]
impl KeyPair for LocalKeyPair
{
	fn sign(&self, data: &[u8], algorithm: u16, rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<Vec<u8>, ()>>
	{
		self.0.deref().privkey.sign(data, SignatureScheme::new(algorithm), rng)
	}
	fn get_schemes(&self) -> SupportedSchemes { self.0.deref().schemes.clone() }
	fn get_public_key(&self) -> SubjectPublicKeyInfo { self.0.deref().pubkey.clone() }
	fn get_key_type(&self) -> &'static str { self.0.deref().keytype }
}

impl KeyPair2 for LocalKeyPair
{
	fn get_signature_algorithms<'a>(&'a self) -> &'a [SignatureAlgorithm2]
	{
		self.0.deref().privkey.get_signature_algorithms()
	}
	fn sign_data(&self, data: &[u8], algorithm: SignatureAlgorithm2, rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<(SignatureFormat, Vec<u8>), ()>>
	{
		self.0.deref().privkey.sign_data(data, algorithm, rng)
	}
	fn sign_callback(&self, algorithm: SignatureAlgorithm2, rng: &mut TemporaryRandomStream,
		msg: &mut dyn FnMut(&mut Hasher)) -> FutureReceiver<Result<(SignatureFormat, Vec<u8>), ()>>
	{
		self.0.deref().privkey.sign_callback(algorithm, rng, msg)
	}
	fn get_public_key2<'a>(&'a self) -> &'a [u8] { self.0.deref().pubkey.0.deref().deref() }
	fn get_key_type2<'a>(&'a self) -> &'a str { self.0.deref().keytype }
}

///A local key pair inner storage.
pub struct LocalKeyPairStorage(_LocalKeyPairStorage);

//Don't put anything that isn't Send and Sync here!
enum _LocalKeyPairStorage
{
	Ed25519(crate::eddsa::Ed25519KeyPair, Vec<u8>),
	Ed448(crate::eddsa::Ed448KeyPair, Vec<u8>),
	Ecdsa(crate::ecdsa::EcdsaCurve, crate::ecdsa::EcdsaKeyPair, Vec<u8>),
}

fn convert_key<'a>(key: &'a [u8]) -> Result<InternalKey, KeyLoadingError>
{
	use self::KeyLoadingError::*;
	if key.get(0).map(|&x|x) == Some(40) {		//40 => ( from S-Expr.
		let tmp = convert_key_from_sexpr2(key).map_err(KeyDecodeError)?;
		Ok(tmp)
	} else if looks_like_json(key) {		//123 => { from JSON.
		let tmp = convert_key_from_jwk2(key).map_err(KeyDecodeError)?;
		Ok(tmp)
	} else if key.get(0).map(|&x|x&0xE0) == Some(160) {	//160-191 (CBOR major 5)
		let tmp = convert_key_from_cwk2(key).map_err(KeyDecodeError)?;
		Ok(tmp)
	} else {
		Err(From::from(UnrecognizedPrivateKeyFormat))
	}
}

fn aspace(b: u8) -> bool { b == 9 || b == 10 || b == 13 || b == 32 }

fn looks_like_json(key: &[u8]) -> bool
{
	//If the string starts with BOM, skip it.
	let idx = if key.get(..3).unwrap_or(&[]) == b"\xEF\xBB\xBF" { 3 } else { 0 };
	for c in key.get(idx..).unwrap_or(&[]).iter().cloned() {
		if aspace(c) { continue; }	//Ignore space.
		return c == 123;	//The first non-space must be '{'.
	}
	//If we reached here, the string was pure whitespace.
	false
}

//Error for loading a keypair. Hide this because it isn't reachable from anything documented.
#[doc(hidden)]
#[derive(Clone,Debug,PartialEq,Eq)]
#[non_exhaustive]
pub enum KeyLoadingError
{
	#[doc(hidden)]
	Ecdsa(crate::ecdsa::EcdsaKeyLoadingError),
	#[doc(hidden)]
	Ed25519(crate::eddsa::Ed25519KeyLoadingError),
	#[doc(hidden)]
	Ed448(crate::eddsa::Ed448KeyLoadingError),
	#[doc(hidden)]
	KeyDecodeError(DecodingError),
	#[doc(hidden)]
	UnrecognizedPrivateKeyFormat,
	#[doc(hidden)]
	UnrecognizedKeyVersion,
	#[doc(hidden)]
	UnknownPrivateKeyType,
}

///Error in decoding a keypair.
///
///This is internally created by various key conversion functions. It can be formatted into human-readable error
///message using the `{}` format operator.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum DecodingError
{
	#[doc(hidden)]
	Ecdsa(crate::ecdsa::EcdsaDecodingError),
	#[doc(hidden)]
	Eddsa(crate::eddsa::EddsaDecodingError),
	#[doc(hidden)]
	InvalidBase64,
	#[doc(hidden)]
	NotValidUtf8,
	#[doc(hidden)]
	UnknownKeyType,
	#[doc(hidden)]
	CwkInvalidCbor,
	#[doc(hidden)]
	JwkInvalidJson,
	#[doc(hidden)]
	NoKeyType,
	#[doc(hidden)]
	WkNoOkpSubtype,
	#[doc(hidden)]
	WkUnknownOkpSubtype,
	#[doc(hidden)]
	WkToplevelNotDictionary,
	#[doc(hidden)]
	SexpUnknownKeyType,
	#[doc(hidden)]
	SexpListTruncated,
	#[doc(hidden)]
	SexpListTooLong,
	#[doc(hidden)]
	WkNoParameter(&'static str, &'static str),
	#[doc(hidden)]
	WkBadParameter(&'static str, &'static str),
	#[doc(hidden)]
	IoError,
	#[doc(hidden)]
	ElementNotAsn1(usize),
	#[doc(hidden)]
	ExpectedOne,
}


///Error reading key pair
///
///This error type is internally constructed by many key reading functions.
///
///This can be turned into human-readable error message using the `{}` format operator (trait `Display`).
#[derive(Debug)]
#[non_exhaustive]
pub enum KeyReadError
{
	#[doc(hidden)]
	ParseError(String, KeyLoadingError),
	#[doc(hidden)]
	KeyGenerationFailed,
}

impl Display for KeyLoadingError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::KeyLoadingError::*;
		match self {
			&Ecdsa(ref err) => write!(fmt, "ECDSA loading: {err}"),
			&Ed25519(ref err) => write!(fmt, "Ed25519 loading: {err}"),
			&Ed448(ref err) => write!(fmt, "Ed448 loading: {err}"),
			&KeyDecodeError(err) => write!(fmt, "Error decoding key: {err}"),
			&UnrecognizedPrivateKeyFormat => fmt.write_str("Unrecognized private key format"),
			&UnrecognizedKeyVersion => fmt.write_str("Unrecognized key format version"),
			&UnknownPrivateKeyType => fmt.write_str("Unknown private key type"),
		}
	}
}

impl Display for KeyReadError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::KeyReadError::*;
		match self {
			&ParseError(ref name, ref err) => write!(fmt, "Can't parse keypair '{name}': {err}"),
			&KeyGenerationFailed => fmt.write_str("Can't generate new key"),
		}
	}
}

impl Display for DecodingError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::DecodingError::*;
		match self {
			&Ecdsa(ref x) => Display::fmt(x, fmt),
			&Eddsa(ref x) => Display::fmt(x, fmt),
			&InvalidBase64 => fmt.write_str("Invalid Base64 encoding"),
			&NotValidUtf8 => fmt.write_str("Not valid UTF-8"),
			&CwkInvalidCbor => fmt.write_str("supposed CWK contains invalid CBOR"),
			&JwkInvalidJson => fmt.write_str("supposed JWK contains invalid JSON"),
			&NoKeyType => fmt.write_str("No key type in keypair"),
			&WkNoOkpSubtype => fmt.write_str("CWK/JWK no OKP subtype"),
			&WkUnknownOkpSubtype => fmt.write_str("CWK/JWK unknown OKP subtype"),
			&UnknownKeyType => fmt.write_str("Unknown key type for keypair"),
			&WkToplevelNotDictionary => fmt.write_str("supposed CWK/JWK toplevel is not a dictionary"),
			&SexpUnknownKeyType => fmt.write_str("Unknown key type in S-Exp"),
			&SexpListTruncated => fmt.write_str("Not enough fields for key type in S-Exp"),
			&SexpListTooLong => fmt.write_str("Too many fields for key type in S-Exp"),
			&IoError => fmt.write_str("I/O error"),
			&WkNoParameter(param, ktype) =>
				write!(fmt, "CWK/JWK required parameter {param} missing in type {ktype} key"),
			&WkBadParameter(param, ktype) =>
				write!(fmt, "CWK/JWK required parameter {param} bad in type {ktype} key"),
			&ElementNotAsn1(line) =>
				write!(fmt, "PEM element starting at line {line} is not a ASN.1 value",
					line=line.saturating_add(1)),
			&ExpectedOne => fmt.write_str("Expected exactly one PEM element"),
		}
	}
}
