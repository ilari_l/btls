use btls_aux_cbor::CBOR;
use btls_aux_collections::Cow;
use btls_aux_collections::Vec;
use btls_aux_fail::dtry;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::ResultExt;
use btls_aux_json2::JSON;
use btls_aux_rope3::fragment_padded;
use btls_aux_rope3::FragmentWriteOps;
use btls_aux_rope3::rope3;
use btls_aux_sexp::SexprParseStream;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::str::from_utf8;

const NSA_P256_PART_LEN: usize = 32;
const NSA_P384_PART_LEN: usize = 48;
const NSA_P521_PART_LEN: usize = 66;
const NSA_P256_SEXP_ID: &'static str = "P256";
const NSA_P384_SEXP_ID: &'static str = "P384";
const NSA_P521_SEXP_ID: &'static str = "P521";
const NSA_P256_JWK_ID: &'static str = "P-256";
const NSA_P384_JWK_ID: &'static str = "P-384";
const NSA_P521_JWK_ID: &'static str = "P-521";

///Format of key.
#[derive(Copy,Clone,PartialEq,Eq,Debug)]
#[repr(usize)]
pub enum KeyFormat
{
	#[doc(hidden)]
	Unknown,
	///RSA key identifier.
	///
	///The internal format is as follows, in order:
	///
	/// * 2 bytes: Big-endian number of bytes in n (nlen).
	/// * nlen bytes: Big-endian RSA n.
	/// * nlen bytes: Big-endian RSA d.
	/// * (nlen+1)/2 bytes: Big-endian RSA p
	/// * (nlen+1)/2 bytes: Big-endian RSA q
	/// * (nlen+1)/2 bytes: Big-endian RSA dp
	/// * (nlen+1)/2 bytes: Big-endian RSA dq
	/// * (nlen+1)/2 bytes: Big-endian RSA qi
	/// * (remainder): Big endian RSA e.
	Rsa,
	///ECDSA key identifier.
	///
	///The internal format is as follows, in order:
	///
	/// * 1 byte: Variant identifier (0 for NSA P-256, P-384 and P-521).
	/// * c bytes: Big endian ECDSA d.
	/// * c bytes: Big endian ECDSA x.
	/// * c bytes: Big endian ECDSA y.
	///
	///Where c is 32 for P-256, 48 for P-384 and 66 for P-521.
	Ecdsa,
	///Ed25519 key identifier.
	///
	///The internal format is as follows, in order:
	///
	/// * 32 bytes: The private key.
	/// * 32 bytes: The public key.
	Ed25519,
	///Ed448 key identifier.
	///
	///The internal format is as follows, in order:
	///
	/// * 57 bytes: The private key.
	/// * 57 bytes: The public key.
	Ed448,
}

///Error in decoding a keypair.
///
///This is internally created by various key conversion functions. It can be formatted into human-readable error
///message using the `{}` format operator.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum DecodingError
{
	#[doc(hidden)]
	InvalidBase64,
	#[doc(hidden)]
	NotValidUtf8,
	#[doc(hidden)]
	UnknownKeyType,
	#[doc(hidden)]
	RsaKeyTooSmall,
	#[doc(hidden)]
	RsaKeyBadSize,
	#[doc(hidden)]
	CantSerializeRsaKey,
	#[doc(hidden)]
	RsaElementsTooLarge,
	#[doc(hidden)]
	CantSerializeEcdsaKey,
	#[doc(hidden)]
	CwkInvalidCbor,
	#[doc(hidden)]
	JwkInvalidJson,
	#[doc(hidden)]
	NoKeyType,
	#[doc(hidden)]
	NoCurve,
	#[doc(hidden)]
	UnknownCurve,
	#[doc(hidden)]
	BadEd25519d,
	#[doc(hidden)]
	BadEd25519x,
	#[doc(hidden)]
	BadEd448d,
	#[doc(hidden)]
	BadEd448x,
	#[doc(hidden)]
	WkNoOkpSubtype,
	#[doc(hidden)]
	WkUnknownOkpSubtype,
	#[doc(hidden)]
	WkToplevelNotDictionary,
	#[doc(hidden)]
	SexpUnknownKeyType,
	#[doc(hidden)]
	SexpListTruncated,
	#[doc(hidden)]
	SexpListTooLong,
	#[doc(hidden)]
	WkNoParameter(&'static str, &'static str),
	#[doc(hidden)]
	WkBadParameter(&'static str, &'static str),
	#[doc(hidden)]
	IoError,
	#[doc(hidden)]
	ElementNotAsn1(usize),
	#[doc(hidden)]
	ExpectedOne,
}

impl Display for DecodingError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::DecodingError::*;
		match self {
			&InvalidBase64 => fmt.write_str("Invalid Base64 encoding"),
			&NotValidUtf8 => fmt.write_str("Not valid UTF-8"),
			&RsaKeyTooSmall => fmt.write_str("RSA key needs to be 2048-4096 bits"),
			&RsaKeyBadSize => fmt.write_str("RSA key not multiple of 8 bits"),
			&CantSerializeRsaKey => fmt.write_str("Can't serialize RSA key"),
			&CantSerializeEcdsaKey => fmt.write_str("Can't serialize ECDSA key"),
			&RsaElementsTooLarge => fmt.write_str("An RSA element exceeds maximum size"),
			&CwkInvalidCbor => fmt.write_str("supposed CWK contains invalid CBOR"),
			&JwkInvalidJson => fmt.write_str("supposed JWK contains invalid JSON"),
			&NoKeyType => fmt.write_str("No key type in keypair"),
			&NoCurve => fmt.write_str("No curve in EC keypair"),
			&UnknownCurve => fmt.write_str("Unknown curve in EC key"),
			&BadEd25519d => fmt.write_str("Invalid length of d in Ed25519"),
			&BadEd25519x => fmt.write_str("Invalid length of x in Ed25519"),
			&BadEd448d => fmt.write_str("Invalid length of d in Ed448"),
			&BadEd448x => fmt.write_str("Invalid length of x in Ed448"),
			&WkNoOkpSubtype => fmt.write_str("CWK/JWK no OKP subtype"),
			&WkUnknownOkpSubtype => fmt.write_str("CWK/JWK unknown OKP subtype"),
			&UnknownKeyType => fmt.write_str("Unknown key type for keypair"),
			&WkToplevelNotDictionary => fmt.write_str("supposed CWK/JWK toplevel is not a dictionary"),
			&SexpUnknownKeyType => fmt.write_str("Unknown key type in S-Exp"),
			&SexpListTruncated => fmt.write_str("Not enough fields for key type in S-Exp"),
			&SexpListTooLong => fmt.write_str("Too many fields for key type in S-Exp"),
			&IoError => fmt.write_str("I/O error"),
			&WkNoParameter(param, ktype) =>
				write!(fmt, "CWK/JWK required parameter {param} missing in type {ktype} key"),
			&WkBadParameter(param, ktype) =>
				write!(fmt, "CWK/JWK required parameter {param} bad in type {ktype} key"),
			&ElementNotAsn1(line) =>
				write!(fmt, "PEM element starting at line {line} is not a ASN.1 value",
					line=line.saturating_add(1)),
			&ExpectedOne => fmt.write_str("Expected exactly one PEM element"),
		}
	}
}

///Convert a COSE key into internal format.
///
///The key to convert is `cwk_key`.
///
///On success, returns `Ok((format, intrep))`, where `format` is one of the `KeyFormat` constants, and
///`intrep` is the internal representation of the key. Otherwise returns `Err(err)`, where `err` is the error.
pub fn convert_key_from_cwk(cwk_key: &[u8]) -> Result<(KeyFormat, Cow<'static, [u8]>), DecodingError>
{
	use self::DecodingError::*;
	let cbor = CBOR::parse(cwk_key).set_err(CwkInvalidCbor)?;
	fail_if!(!cbor.is_dictionary(), WkToplevelNotDictionary);
	let kty = cbor.field(1i64).ok_or(NoKeyType)?;
	if *kty == 3i64 {
		//RSA key.
		let n = read_cose_keypart(&cbor, -1, "n", "RSA")?;
		let e = read_cose_keypart(&cbor, -2, "e", "RSA")?;
		let d = read_cose_keypart(&cbor, -3, "d", "RSA")?;
		let p = read_cose_keypart(&cbor, -4, "p", "RSA")?;
		let q = read_cose_keypart(&cbor, -5, "q", "RSA")?;
		let dp = read_cose_keypart(&cbor, -6, "dp", "RSA")?;
		let dq = read_cose_keypart(&cbor, -7, "dq", "RSA")?;
		let qi = read_cose_keypart(&cbor, -8, "qi", "RSA")?;
		return serialize_rsa_key(&n, &e, &d, &p, &q, &dp, &dq, &qi);
	} else if *kty == 2i64 {
		//ECC key.
		static ZEROBYTE: [u8;1] = [0];
		let crv = cbor.field(-1i64).ok_or(NoCurve)?;
		let (oid, len) = if *crv == 1i64 {
			(&ZEROBYTE[..], NSA_P256_PART_LEN)
		} else if *crv == 2i64 {
			(&ZEROBYTE[..], NSA_P384_PART_LEN)
		} else if *crv == 3i64 {
			(&ZEROBYTE[..], NSA_P521_PART_LEN)
		} else {
			fail!(UnknownCurve);
		};
		let d = read_cose_keypart(&cbor, -4, "d", "EC")?;
		let x = read_cose_keypart(&cbor, -2, "x", "EC")?;
		let y = read_cose_keypart(&cbor, -3, "y", "EC")?;
		return serialize_ecdsa_key(oid, &d, &x, &y, len);
	} else if *kty == 1i64 {
		//OKP key (Ed25519 or Ed448).
		let sub = cbor.field(-1i64).ok_or(WkNoOkpSubtype)?;
		let d = read_cose_keypart(&cbor, -4, "d", "OKP")?;
		let x = read_cose_keypart(&cbor, -2, "x", "OKP")?;
		if *sub == 6i64 {
			fail_if!(d.len() != 32, BadEd25519d);
			fail_if!(x.len() != 32, BadEd25519x);
			let mut res = Vec::new();
			res.extend_from_slice(&d);
			res.extend_from_slice(&x);
			return Ok((KeyFormat::Ed25519, Cow::Owned(res)));
		} else if *sub == 7i64 {
			fail_if!(d.len() != 57, BadEd448d);
			fail_if!(x.len() != 57, BadEd448x);
			let mut res = Vec::new();
			res.extend_from_slice(&d);
			res.extend_from_slice(&x);
			return Ok((KeyFormat::Ed448, Cow::Owned(res)));
		} else {
			fail!(WkUnknownOkpSubtype);
		}
	} else {
		fail!(UnknownKeyType);
	}
}

///Convert a JWK into internal format.
///
///The key to convert is `jwk_key`.
///
///On success, returns `Ok((format, intrep))`, where `format` is one of the `KeyFormat` constants, and
///`intrep` is the internal representation of the key. Otherwise returns `Err(err)`, where `err` is the error.
pub fn convert_key_from_jwk(jwk_key: &[u8]) -> Result<(KeyFormat, Cow<'static, [u8]>), DecodingError>
{
	use self::DecodingError::*;
	//This is supposed to be UTF-8.
	let mut jwk_key = from_utf8(jwk_key).set_err(NotValidUtf8)?;
	let json = JSON::parse(&mut jwk_key).set_err(JwkInvalidJson)?;
	fail_if!(!json.is_object(), WkToplevelNotDictionary);
	let kty = json.field("kty").ok_or(NoKeyType)?;
	if kty == "RSA" {
		//RSA key.
		let n = read_jose_keypart(&json, "n", "RSA")?;
		let e = read_jose_keypart(&json, "e", "RSA")?;
		let d = read_jose_keypart(&json, "d", "RSA")?;
		let p = read_jose_keypart(&json, "p", "RSA")?;
		let q = read_jose_keypart(&json, "q", "RSA")?;
		let dp = read_jose_keypart(&json, "dp", "RSA")?;
		let dq = read_jose_keypart(&json, "dq", "RSA")?;
		let qi = read_jose_keypart(&json, "qi", "RSA")?;
		return serialize_rsa_key(&n, &e, &d, &p, &q, &dp, &dq, &qi);
	} else if kty == "EC" {
		//ECC key.
		static ZEROBYTE: [u8;1] = [0];
		let crv = json.field("crv").ok_or(NoCurve)?;
		let (oid, len) = if crv == NSA_P256_JWK_ID {
			(&ZEROBYTE[..], NSA_P256_PART_LEN)
		} else if crv == NSA_P384_JWK_ID {
			(&ZEROBYTE[..], NSA_P384_PART_LEN)
		} else if crv == NSA_P521_JWK_ID {
			(&ZEROBYTE[..], NSA_P521_PART_LEN)
		} else {
			fail!(UnknownCurve);
		};
		let d = read_jose_keypart(&json, "d", "EC")?;
		let x = read_jose_keypart(&json, "x", "EC")?;
		let y = read_jose_keypart(&json, "y", "EC")?;
		return serialize_ecdsa_key(oid, &d, &x, &y, len);
	} else if kty == "OKP" {
		//OKP key (Ed25519 or Ed448).
		let sub = json.field("crv").ok_or(WkNoOkpSubtype)?;
		let d = read_jose_keypart(&json, "d", "OKP")?;
		let x = read_jose_keypart(&json, "x", "OKP")?;
		if sub == "Ed25519" {
			fail_if!(d.len() != 32, BadEd25519d);
			fail_if!(x.len() != 32, BadEd25519x);
			let mut res = Vec::new();
			res.extend_from_slice(&d);
			res.extend_from_slice(&x);
			return Ok((KeyFormat::Ed25519, Cow::Owned(res)));
		} else if sub == "Ed448" {
			fail_if!(d.len() != 57, BadEd448d);
			fail_if!(x.len() != 57, BadEd448x);
			let mut res = Vec::new();
			res.extend_from_slice(&d);
			res.extend_from_slice(&x);
			return Ok((KeyFormat::Ed448, Cow::Owned(res)));
		} else {
			fail!(WkUnknownOkpSubtype);
		}
	} else {
		fail!(UnknownKeyType);
	}
}

///Convert a native format key into internal format.
///
///The key to convert is `native_key`.
///
///On success, returns `Ok((format, intrep))`, where `format` is one of the `KeyFormat` constants, and
///`intrep` is the internal representation of the key. Otherwise returns `Err(err)`, where `err` is the error.
pub fn convert_key_from_sexpr(native_key: &[u8]) -> Result<(KeyFormat, Cow<'static, [u8]>), DecodingError>
{
	use self::DecodingError::*;
	let mut stream = SexprParseStream::new(native_key);
	let ktype = stream.read_typed_str().set_err(NoKeyType)?;
	if ktype == "rsa" || ktype == "rsa-pss" {
		let n = stream.next_data().set_err(SexpListTruncated)?;
		let e = stream.next_data().set_err(SexpListTruncated)?;
		let d = stream.next_data().set_err(SexpListTruncated)?;
		let p = stream.next_data().set_err(SexpListTruncated)?;
		let q = stream.next_data().set_err(SexpListTruncated)?;
		let dp = stream.next_data().set_err(SexpListTruncated)?;
		let dq = stream.next_data().set_err(SexpListTruncated)?;
		let qi = stream.next_data().set_err(SexpListTruncated)?;
		stream.assert_eol().set_err(SexpListTooLong)?;
		serialize_rsa_key(&n, &e, &d, &p, &q, &dp, &dq, &qi)
	} else if ktype == "ecdsa" {
		static ZEROBYTE: [u8;1] = [0];
		let crv = stream.next_str().set_err(SexpListTruncated)?;
		let (oid, len) = if crv == NSA_P256_SEXP_ID {
			(&ZEROBYTE[..], NSA_P256_PART_LEN)
		} else if crv == NSA_P384_SEXP_ID {
			(&ZEROBYTE[..], NSA_P384_PART_LEN)
		} else if crv == NSA_P521_SEXP_ID {
			(&ZEROBYTE[..], NSA_P521_PART_LEN)
		} else {
			fail!(UnknownCurve);
		};
		let d = stream.next_data().set_err(SexpListTruncated)?;
		let x = stream.next_data().set_err(SexpListTruncated)?;
		let y = stream.next_data().set_err(SexpListTruncated)?;
		stream.assert_eol().set_err(SexpListTooLong)?;
		serialize_ecdsa_key(oid, d, x, y, len)
	} else if ktype == "ed25519" {
		let privkey = stream.next_data().set_err(SexpListTruncated)?;
		let pubkey = stream.next_data().set_err(SexpListTruncated)?;
		stream.assert_eol().set_err(SexpListTooLong)?;
		fail_if!(privkey.len() != 32, BadEd25519d);
		fail_if!(pubkey.len() != 32, BadEd25519x);
		let mut res = Vec::new();
		res.extend_from_slice(privkey);
		res.extend_from_slice(pubkey);
		Ok((KeyFormat::Ed25519, Cow::Owned(res)))
	} else if ktype == "ed448" {
		let privkey = stream.next_data().set_err(SexpListTruncated)?;
		let pubkey = stream.next_data().set_err(SexpListTruncated)?;
		stream.assert_eol().set_err(SexpListTooLong)?;
		fail_if!(privkey.len() != 57, BadEd448d);
		fail_if!(pubkey.len() != 57, BadEd448x);
		let mut res = Vec::new();
		res.extend_from_slice(privkey);
		res.extend_from_slice(pubkey);
		Ok((KeyFormat::Ed448, Cow::Owned(res)))
	} else {
		fail!(SexpUnknownKeyType);
	}
}

fn serialize_rsa_key(n: &[u8], e: &[u8], d: &[u8], p: &[u8], q: &[u8], dp: &[u8], dq: &[u8], qi: &[u8]) ->
	Result<(KeyFormat, Cow<'static, [u8]>), DecodingError>
{
	use self::DecodingError::*;
	fail_if!(n.len() < 256 || n.len() > 512, RsaKeyTooSmall);
	fail_if!(n[0] < 128, RsaKeyBadSize);	//n[0] is valid, since n.len() is 256-512 by above.
	let largesize = n.len();
	let smallsize = (n.len() + 1) / 2;
	fail_if!(d.len() > largesize, RsaElementsTooLarge);
	fail_if!(p.len() > smallsize, RsaElementsTooLarge);
	fail_if!(q.len() > smallsize, RsaElementsTooLarge);
	fail_if!(dp.len() > smallsize, RsaElementsTooLarge);
	fail_if!(dq.len() > smallsize, RsaElementsTooLarge);
	fail_if!(qi.len() > smallsize, RsaElementsTooLarge);

	let t = rope3!(CONCATENATE(
		{largesize as u16},
		{fragment_padded(n, largesize)},
		{fragment_padded(d, largesize)},
		{fragment_padded(p, smallsize)},
		{fragment_padded(q, smallsize)},
		{fragment_padded(dp, smallsize)},
		{fragment_padded(dq, smallsize)},
		{fragment_padded(qi, smallsize)},
		e
	));
	Ok((KeyFormat::Rsa, Cow::Owned(FragmentWriteOps::to_vector(&t))))
}

fn serialize_ecdsa_key(oid: &[u8], d: &[u8], x: &[u8], y: &[u8], len: usize) ->
	Result<(KeyFormat, Cow<'static, [u8]>), DecodingError>
{
/*
	let mut output = Vec::with_capacity(oid.len() + 3 * len);
	output.extend_from_slice(oid);
	write_padded(&mut output, d, len);
	write_padded(&mut output, x, len);
	write_padded(&mut output, y, len);
*/
	let t = rope3!(CONCATENATE(
		oid,
		{fragment_padded(d, len)},
		{fragment_padded(x, len)},
		{fragment_padded(y, len)},
	));
	Ok((KeyFormat::Ecdsa, Cow::Owned(FragmentWriteOps::to_vector(&t))))
}

fn read_cose_keypart<'a>(dict: &'a CBOR, key: i64, keyname: &'static str,
	cwktype: &'static str) -> Result<&'a [u8], DecodingError>
{
	use self::DecodingError::*;
	decode_octets_cbor(dict.field(key).ok_or(WkNoParameter(keyname, cwktype))?).
		set_err(WkBadParameter(keyname, cwktype))
}

fn read_jose_keypart(dict: &JSON, key: &'static str, jwktype: &'static str) ->
	Result<Vec<u8>, DecodingError>
{
	use self::DecodingError::*;
	decode_base64url_json(dict.field(key).ok_or(WkNoParameter(key, jwktype))?).
		set_err(WkBadParameter(key, jwktype))
}

fn decode_octets_cbor<'a>(x: &'a CBOR) -> Result<&'a [u8], ()> { Ok(dtry!(x.as_octets())) }

fn decode_base64url_json(x: &JSON) -> Result<Vec<u8>, ()>
{
	let x = dtry!(x.as_string());
	let mut v = Vec::new();
	let mut base64_pol = 0u8;
	let mut base64_val = 0;
	for j in x.chars() {
		if j == ' ' || j == '\t' { continue; }
		let x = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".find(j).unwrap_or(64);
		if x == 64 { return Err(()); }
		base64_val = base64_val << 6 | x;
		base64_pol = base64_pol.wrapping_add(1) & 3;
		if base64_pol == 0 {
			v.push((base64_val >> 16) as u8);
			v.push((base64_val >> 8) as u8);
			v.push(base64_val as u8);
		}
	}
	if base64_pol == 1 { return Err(()); }
	if base64_pol == 2 {
		v.push((base64_val >> 4) as u8);
	} else if base64_pol == 3 {
		v.push((base64_val >> 10) as u8);
		v.push((base64_val >> 2) as u8);
	}
	Ok(v)
}
