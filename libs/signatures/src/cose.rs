use super::DecodingError;
use super::InternalKey;
use super::KeyFormat;
use btls_aux_cbor::CBOR;
use btls_aux_collections::Vec;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::ResultExt;

pub(crate) fn convert_key_from_cwk2(cwk_key: &[u8]) -> Result<InternalKey, DecodingError>
{
	use crate::keypair::DecodingError::*;
	use crate::ecdsa::EcdsaDecodingError::*;
	use crate::eddsa::EddsaDecodingError::*;
	let cbor = CBOR::parse(cwk_key).set_err(CwkInvalidCbor)?;
	fail_if!(!cbor.is_dictionary(), WkToplevelNotDictionary);
	let kty = cbor.field(1u64).ok_or(NoKeyType)?;
	if *kty == 2i64 {
		//ECC key.
		use crate::ecdsa::EcdsaCurve::*;
		static ZEROBYTE: [u8;1] = [0];
		let crv = cbor.field(-1i64).ok_or(NoCurve)?;
		let (oid, len) = if *crv == 1i64 {
			(&ZEROBYTE[..], NsaP256.get_component_bytes())
		} else if *crv == 2i64 {
			(&ZEROBYTE[..], NsaP384.get_component_bytes())
		} else if *crv == 3i64 {
			(&ZEROBYTE[..], NsaP521.get_component_bytes())
		} else {
			fail!(UnknownCurve);
		};
		let d = read_cose_keypart(&cbor, -4, "d", "EC")?;
		let x = read_cose_keypart(&cbor, -2, "x", "EC")?;
		let y = read_cose_keypart(&cbor, -3, "y", "EC")?;
		return crate::ecdsa::serialize_key(oid, &d, &x, &y, len);
	} else if *kty == 1i64 {
		//OKP key (Ed25519 or Ed448).
		let sub = cbor.field(-1i64).ok_or(WkNoOkpSubtype)?;
		let d = read_cose_keypart(&cbor, -4, "d", "OKP")?;
		let x = read_cose_keypart(&cbor, -2, "x", "OKP")?;
		if *sub == 6i64 {
			fail_if!(d.len() != 32, BadEd25519d);
			fail_if!(x.len() != 32, BadEd25519x);
			let mut res = Vec::new();
			res.extend_from_slice(&d);
			res.extend_from_slice(&x);
			return Ok(InternalKey::join(KeyFormat::Ed25519, res));
		} else if *sub == 7i64 {
			fail_if!(d.len() != 57, BadEd448d);
			fail_if!(x.len() != 57, BadEd448x);
			let mut res = Vec::new();
			res.extend_from_slice(&d);
			res.extend_from_slice(&x);
			return Ok(InternalKey::join(KeyFormat::Ed448, res));
		} else {
			fail!(WkUnknownOkpSubtype);
		}
	} else {
		fail!(UnknownKeyType);
	}
}

fn read_cose_keypart<'a>(dict: &'a CBOR, key: i64, keyname: &'static str,
	cwktype: &'static str) -> Result<&'a [u8], DecodingError>
{
	use self::DecodingError::*;
	let fval = dict.field(key).ok_or(WkNoParameter(keyname, cwktype))?;
	fval.as_octets().ok_or(WkBadParameter(keyname, cwktype))
}
