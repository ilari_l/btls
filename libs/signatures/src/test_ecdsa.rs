use crate::SignatureAlgorithmEnabled2;
use crate::SignatureAlgorithmTls2;
use crate::SignatureFlags2;
use crate::SignatureFormat;
use crate::SignatureKeyPair;
use crate::SignatureTls2;
use crate::ecdsa::EcdsaCurve;
use btls_aux_collections::Vec;
use btls_aux_json2::base64url;
use btls_aux_random::TemporaryRandomLifetimeTag;
use btls_aux_random::TemporaryRandomStream;

const ALGBIT_ECDSA: usize = 3;

fn do_test2(tlsid: u16, params: EcdsaCurve)
{
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	for _ in 0..100 {
		let msg1 = "Hello, World!";
		let msg2 = "Hello, world!";
		//Allow all signatures because this is to test verifiability of the generated signature.
		let any_policy = SignatureAlgorithmEnabled2::unsafe_any_policy();
		let nrsa_flags = SignatureFlags2::NO_RSA;
		let any_flags = SignatureFlags2::CERTIFICATE;
		let xalgo = SignatureAlgorithmTls2::by_tls_id(tlsid).unwrap();
		//First, generate two keys.
		let kp1r = crate::ecdsa::EcdsaKeyPair::keypair_generate(params, &mut rng).unwrap();
		let kp2r = crate::ecdsa::EcdsaKeyPair::keypair_generate(params, &mut rng).unwrap();
		//The keys should be different.
		assert!(&kp1r[..] != &kp2r[..]);
		//Then load the keys. Load keys twice to check consistency.
		let (curve1a, kp1, pk1a) = crate::ecdsa::EcdsaKeyPair::keypair_load(&kp1r).unwrap();
		let (curve1b, _, pk1b) = crate::ecdsa::EcdsaKeyPair::keypair_load(&kp1r).unwrap();
		let (curve2a, kp2, pk2a) = crate::ecdsa::EcdsaKeyPair::keypair_load(&kp2r).unwrap();
		let (curve2b, _, pk2b) = crate::ecdsa::EcdsaKeyPair::keypair_load(&kp2r).unwrap();
		assert_eq!(&pk1a[..], &pk1b[..]);
		assert_eq!(&pk2a[..], &pk2b[..]);
		assert_eq!(curve1a.oid(), curve1b.oid());
		assert_eq!(curve1a.oid(), params.oid());
		assert_eq!(curve2a.oid(), curve2b.oid());
		assert_eq!(curve2a.oid(), params.oid());
		//Sign some data.
		let mut signature1 = [0;160];
		let mut signature2 = [0;160];
		let signature1len = kp1.keypair_sign(msg1.as_bytes(), tlsid, &mut signature1, &mut rng).unwrap();
		let signature1 = SignatureTls2::new(xalgo, &signature1[..signature1len]);
		let signature2len = kp2.keypair_sign(msg2.as_bytes(), tlsid, &mut signature2, &mut rng).unwrap();
		let signature2 = SignatureTls2::new(xalgo, &signature2[..signature2len]);
		//Now, check the signatures.
		signature1.verify(&pk1a, msg1.as_bytes(), any_policy, nrsa_flags).unwrap();
		signature2.verify(&pk2a, msg2.as_bytes(), any_policy, nrsa_flags).unwrap();
		//The signatures should not check with wrong keys, wrong messages or wrong signatures.
		signature1.verify(&pk1a, msg2.as_bytes(), any_policy, any_flags).unwrap_err();
		signature1.verify(&pk2a, msg1.as_bytes(), any_policy, any_flags).unwrap_err();
		signature1.verify(&pk2a, msg2.as_bytes(), any_policy, any_flags).unwrap_err();
		signature2.verify(&pk1a, msg2.as_bytes(), any_policy, any_flags).unwrap_err();
		signature2.verify(&pk2a, msg1.as_bytes(), any_policy, any_flags).unwrap_err();
		signature2.verify(&pk1a, msg1.as_bytes(), any_policy, any_flags).unwrap_err();
		//The signatures should not verify with algorithms disabled.
		let ecdsa_mask = 1<<ALGBIT_ECDSA;
		let bad_policy = SignatureAlgorithmEnabled2::from_mask(!ecdsa_mask, !0, !0, true);
		signature1.verify(&pk1a, msg1.as_bytes(), bad_policy, any_flags).unwrap_err();
		signature2.verify(&pk2a, msg2.as_bytes(), bad_policy, any_flags).unwrap_err();
		//NSA mode. This is nontrival. Require SHA-384 and P-384.
		let nsa_policy = SignatureAlgorithmEnabled2::nsa_mode();
		let ok1 = signature1.verify(&pk1a, msg1.as_bytes(), nsa_policy, any_flags).is_ok();
		let ok2 = signature2.verify(&pk2a, msg2.as_bytes(), nsa_policy, any_flags).is_ok();
		assert!(ok1 == ok2);
		let ok = tlsid == 0x503 && matches!(params, EcdsaCurve::NsaP384);
		assert!(ok == ok1);
	}
}

#[test] fn sign_ecdsa_sha256_p256_2() { do_test2(0x403, EcdsaCurve::NsaP256); }
#[test] fn sign_ecdsa_sha256_p384_2() { do_test2(0x403, EcdsaCurve::NsaP384); }
#[test] fn sign_ecdsa_sha256_p521_2() { do_test2(0x403, EcdsaCurve::NsaP521); }
#[test] fn sign_ecdsa_sha384_p256_2() { do_test2(0x503, EcdsaCurve::NsaP256); }
#[test] fn sign_ecdsa_sha384_p384_2() { do_test2(0x503, EcdsaCurve::NsaP384); }
#[test] fn sign_ecdsa_sha384_p521_2() { do_test2(0x503, EcdsaCurve::NsaP521); }
#[test] fn sign_ecdsa_sha512_p256_2() { do_test2(0x603, EcdsaCurve::NsaP256); }
#[test] fn sign_ecdsa_sha512_p384_2() { do_test2(0x603, EcdsaCurve::NsaP384); }
#[test] fn sign_ecdsa_sha512_p521_2() { do_test2(0x603, EcdsaCurve::NsaP521); }


fn do_test(tlsid: u16, key_bytes: &[u8])
{
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let (_, key_bytes) = crate::sexp::convert_key_from_sexpr2(&key_bytes[..]).map(|x|x.split()).unwrap();
	let (_, keypair, pubkey) = crate::ecdsa::EcdsaKeyPair::keypair_load(&key_bytes).unwrap();
	let algo = SignatureAlgorithmTls2::by_tls_id(tlsid).unwrap();
	let msg = "Hello, World!";
	let mut signature = [0;160];
	let signaturelen = keypair.keypair_sign(msg.as_bytes(), tlsid, &mut signature, &mut rng).unwrap();
	let signature = SignatureTls2::new(algo, &signature[..signaturelen]);
	//Allow all signatures because this is to test verifiability of the generated signature.
	signature.verify(&pubkey, msg.as_bytes(), SignatureAlgorithmEnabled2::unsafe_any_policy(),
		SignatureFlags2::NO_RSA).unwrap();
}

#[test] fn sign_ecdsa_sha256_p521() { do_test(0x403, include_bytes!("testecdsa.sexp")); }
#[test] fn sign_ecdsa_sha384_p521() { do_test(0x503, include_bytes!("testecdsa.sexp")); }
#[test] fn sign_ecdsa_sha512_p521() { do_test(0x603, include_bytes!("testecdsa.sexp")); }
#[test] fn sign_ecdsa_sha256_p384() { do_test(0x403, include_bytes!("testecdsa2.sexp")); }
#[test] fn sign_ecdsa_sha384_p384() { do_test(0x503, include_bytes!("testecdsa2.sexp")); }
#[test] fn sign_ecdsa_sha512_p384() { do_test(0x603, include_bytes!("testecdsa2.sexp")); }
#[test] fn sign_ecdsa_sha256_p256() { do_test(0x403, include_bytes!("testecdsa3.sexp")); }
#[test] fn sign_ecdsa_sha384_p256() { do_test(0x503, include_bytes!("testecdsa3.sexp")); }
#[test] fn sign_ecdsa_sha512_p256() { do_test(0x603, include_bytes!("testecdsa3.sexp")); }

fn do_test_p(tlsid: u16, params: EcdsaCurve)
{
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let key_bytes = crate::ecdsa::EcdsaKeyPair::keypair_generate(params, &mut rng).unwrap();
	let (_, keypair, pubkey) = crate::ecdsa::EcdsaKeyPair::keypair_load(&key_bytes).unwrap();
	let algo = SignatureAlgorithmTls2::by_tls_id(tlsid).unwrap();
	let msg = "Hello, World!";
	let mut signature = [0;160];
	let signaturelen = keypair.keypair_sign(msg.as_bytes(), tlsid, &mut signature, &mut rng).unwrap();
	let signature = SignatureTls2::new(algo, &signature[..signaturelen]);
	//Allow all signatures because this is to test verifiability of the generated signature.
	//Check pubkey is recognized.
	assert!(crate::sanity_check_spki(&pubkey).is_ok());
	signature.verify(&pubkey, msg.as_bytes(), SignatureAlgorithmEnabled2::unsafe_any_policy(),
		SignatureFlags2::NO_RSA).unwrap();
	//FIXME: Compress key. Very hacky.
	let pubkey2 = match pubkey.len() {
		91 => {
			let mut x = Vec::new();
			x.push(48);
			x.push(57);
			x.extend_from_slice(&pubkey[2..24]);
			x.push(34);
			x.push(0);
			x.push(2|pubkey[90]&1);
			x.extend_from_slice(&pubkey[27..59]);
			x
		},
		120 => {
			let mut x = Vec::new();
			x.push(48);
			x.push(70);
			x.extend_from_slice(&pubkey[2..21]);
			x.push(50);
			x.push(0);
			x.push(2|pubkey[119]&1);
			x.extend_from_slice(&pubkey[24..72]);
			x
		},
		158 => {
			let mut x = Vec::new();
			x.push(48);
			x.push(88);
			x.extend_from_slice(&pubkey[3..22]);
			x.push(68);
			x.push(0);
			x.push(2|pubkey[157]&1);
			x.extend_from_slice(&pubkey[26..92]);
			x
		},
		len => panic!("Unsupported length {len}")
	};
	println!("{pubkey:?} => {pubkey2:?}");
	assert!(crate::sanity_check_spki(&pubkey2).is_ok());
	signature.verify(&pubkey2, msg.as_bytes(), SignatureAlgorithmEnabled2::unsafe_any_policy(),
		SignatureFlags2::NO_RSA).unwrap();
}

#[test] fn sign_ecdsa_packed_p256() { do_test_p(0x0403, EcdsaCurve::NsaP256); }
#[test] fn sign_ecdsa_packed_p384() { do_test_p(0x0503, EcdsaCurve::NsaP384); }
#[test] fn sign_ecdsa_packed_p521() { do_test_p(0x0603, EcdsaCurve::NsaP521); }


fn do_test_j(tlsid: u16, params: EcdsaCurve)
{
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let key_bytes = crate::ecdsa::EcdsaKeyPair::keypair_generate(params, &mut rng).unwrap();
	let (_, keypair, pubkey) = crate::ecdsa::EcdsaKeyPair::keypair_load(&key_bytes).unwrap();
	let format = SignatureFormat::for_public_key(&pubkey);
	let msg = "Hello, World!";
	let mut signature = [0;160];
	for _ in 0..500 {
		let signaturelen = keypair.keypair_sign(msg.as_bytes(), tlsid, &mut signature, &mut rng).unwrap();
		let signature = &signature[..signaturelen];
		let jose = format.to_jose(signature).expect("Not valid ECDSA signature");
		let cose = format.to_cose(signature).expect("Not valid ECDSA signature");
		let explen = match params {
			EcdsaCurve::NsaP256 => 64,
			EcdsaCurve::NsaP384 => 96,
			EcdsaCurve::NsaP521 => 132,
		};
		assert!(cose.len() == explen);
		let jose2 = base64url(&cose);
		assert!(jose == jose2);
	}
}

#[test] fn sign_ecdsa_jose_cose_p256() { do_test_j(0x0403, EcdsaCurve::NsaP256); }
#[test] fn sign_ecdsa_jose_cose_p384() { do_test_j(0x0503, EcdsaCurve::NsaP384); }
#[test] fn sign_ecdsa_jose_cose_p521() { do_test_j(0x0603, EcdsaCurve::NsaP521); }

#[test]
fn to_cose_100_nn()
{
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let mut x = [0;207];
	x[0] = 48;	x[1] = 129;	x[2] = 204;	//SEQUENCE 204.
	x[3] = 2;	x[4] = 100;			//INTEGER 100.
	x[105] = 2;	x[106] = 100;			//INTEGER 100.
	while x[5] == 0 || x[5] >= 128 { rng.fill_bytes(&mut x[5..105]); }
	while x[107] == 0 || x[107] >= 128 { rng.fill_bytes(&mut x[107..207]); }
	let fmt = SignatureFormat::Ecdsa(100);
	let cose = fmt.to_cose(&x).expect("Not valid ECDSA signature");
	assert_eq!(&x[5..105], &cose[..100]);
	assert_eq!(&x[107..207], &cose[100..]);
}

#[test]
fn to_cose_100_ny()
{
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let mut x = [0;208];
	x[0] = 48;	x[1] = 129;	x[2] = 205;	//SEQUENCE 205.
	x[3] = 2;	x[4] = 100;			//INTEGER 100.
	x[105] = 2;	x[106] = 101;			//INTEGER 101.
	while x[5] == 0 || x[5] >= 128 { rng.fill_bytes(&mut x[5..105]); }
	while x[108] < 128 { rng.fill_bytes(&mut x[108..208]); }
	let fmt = SignatureFormat::Ecdsa(100);
	let cose = fmt.to_cose(&x).expect("Not valid ECDSA signature");
	assert_eq!(&x[5..105], &cose[..100]);
	assert_eq!(&x[108..208], &cose[100..]);
}

#[test]
fn to_cose_100_yn()
{
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let mut x = [0;208];
	x[0] = 48;	x[1] = 129;	x[2] = 205;	//SEQUENCE 205.
	x[3] = 2;	x[4] = 101;			//INTEGER 101.
	x[106] = 2;	x[107] = 100;			//INTEGER 100.
	while x[6] < 128 { rng.fill_bytes(&mut x[6..106]); }
	while x[108] == 0 || x[108] >= 128 { rng.fill_bytes(&mut x[108..208]); }
	let fmt = SignatureFormat::Ecdsa(100);
	let cose = fmt.to_cose(&x).expect("Not valid ECDSA signature");
	assert_eq!(&x[6..106], &cose[..100]);
	assert_eq!(&x[108..208], &cose[100..]);
}

#[test]
fn to_cose_100_yy()
{
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let mut x = [0;209];
	x[0] = 48;	x[1] = 129;	x[2] = 206;	//SEQUENCE 206.
	x[3] = 2;	x[4] = 101;			//INTEGER 101.
	x[106] = 2;	x[107] = 101;			//INTEGER 101.
	while x[6] < 128 { rng.fill_bytes(&mut x[6..106]); }
	while x[109] < 128 { rng.fill_bytes(&mut x[109..209]); }
	let fmt = SignatureFormat::Ecdsa(100);
	let cose = fmt.to_cose(&x).expect("Not valid ECDSA signature");
	assert_eq!(&x[6..106], &cose[..100]);
	assert_eq!(&x[109..209], &cose[100..]);
}

#[test]
fn to_cose_100_pad()
{
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let mut x = [0;86];
	x[0] = 48;	x[1] = 84;			//SEQUENCE 84.
	x[2] = 2;	x[3] = 32;			//INTEGER 32.
	x[36] = 2;	x[37] = 48;			//INTEGER 48.
	while x[4] == 0 || x[4] >= 128 { rng.fill_bytes(&mut x[4..36]); }
	while x[38] == 0 || x[38] >= 128 { rng.fill_bytes(&mut x[38..86]); }
	let fmt = SignatureFormat::Ecdsa(100);
	let cose = fmt.to_cose(&x).expect("Not valid ECDSA signature");
	let jose = fmt.to_jose(&x).expect("Not valid ECDSA signature");
	assert_eq!(&[0;68][..], &cose[0..68]);
	assert_eq!(&x[4..36], &cose[68..100]);
	assert_eq!(&[0;52][..], &cose[100..152]);
	assert_eq!(&x[38..86], &cose[152..200]);
	let jose2 = base64url(&cose);
	assert!(jose == jose2);
}
