use super::*;
use crate::keypair::_LocalKeyPair;
use btls_aux_collections::Arc;
use btls_aux_collections::String;
use btls_aux_collections::Vec;
use btls_aux_filename::Filename;
use btls_aux_random::TemporaryRandomLifetimeTag;
use btls_aux_random::TemporaryRandomStream;
use std::fs::File;
use std::io::Read;
use std::path::Path;

fn kp_new_file(privkey: &str) -> Result<LocalKeyPair, String>
{
	let privkey = Path::new(privkey);
	let pd = Filename::from_path(privkey);
	let mut _privkey = File::open(privkey).map_err(|err|{
		format!("Can't open keypair '{pd}': {err}")
	})?;
	let mut __privkey = Vec::new();
	_privkey.read_to_end(&mut __privkey).map_err(|err|{
		format!("Can't read keypair '{pd}': {err}")
	})?;
	let kp = _LocalKeyPair::new(&__privkey).map_err(|err|{
		format!("Can't parse keypair '{pd}': {err}")
	})?;
	Ok(LocalKeyPair(Arc::new(kp)))
}

#[test]
fn test_eddsa()
{
	let scheme = 0x807;
	let algo = SignatureAlgorithmTls2::by_tls_id(scheme).unwrap();
	let any_policy = SignatureAlgorithmEnabled2::unsafe_any_policy();
	let keypair = kp_new_file("src/testkey.raw").unwrap();
	let message = b"Hello, World!";
	assert_eq!(&(keypair.get_schemes().iter().cloned().collect::<Vec<_>>()), &[scheme]);
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let s = keypair.sign(message, scheme, &mut rng).read();
	let s = s.ok().unwrap();
	let sig = s.unwrap();
	assert_eq!(sig.len(), 64);
	let key = keypair.get_public_key();
	let signature = SignatureTls2::new(algo, &sig[..]);
	signature.verify(&key.0, message, any_policy, SignatureFlags2::CERTIFICATE).unwrap();
	signature.verify(&key.0, message, any_policy, SignatureFlags2::NO_RSA).unwrap();
}

#[test]
fn test_eddsa_unknown()
{
	let scheme = 0x403;
	let keypair = kp_new_file("src/testkey.raw").unwrap();
	let message = b"Hello, World!";
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let s = keypair.sign(message, scheme, &mut rng).read();
	let s = s.ok().unwrap();	//Future settled.
	s.unwrap_err();			//Signing failed.
}

#[test]
fn test_eddsa2()
{
	let scheme = 0x808;
	let algo = SignatureAlgorithmTls2::by_tls_id(scheme).unwrap();
	let any_policy = SignatureAlgorithmEnabled2::unsafe_any_policy();
	let refsig = include_bytes!("hellow.sig");
	let keypair = kp_new_file("src/testkey2.raw").unwrap();
	let message = b"Hello, World!";
	assert_eq!(&(keypair.get_schemes().iter().cloned().collect::<Vec<_>>()), &[scheme]);
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let s = keypair.sign(message, scheme, &mut rng).read();
	let s = s.ok().unwrap();
	let sig = s.unwrap();
	assert_eq!(sig.len(), 114);
	assert_eq!(&sig[..], &refsig[..]);
	let key = keypair.get_public_key();
	let signature = SignatureTls2::new(algo, &sig[..]);
	signature.verify(&key.0, message, any_policy, SignatureFlags2::CERTIFICATE).unwrap();
	signature.verify(&key.0, message, any_policy, SignatureFlags2::NO_RSA).unwrap();
}

#[test]
fn test_eddsa2_unknown()
{
	let scheme = 0x807;
	let keypair = kp_new_file("src/testkey2.raw").unwrap();
	let message = b"Hello, World!";
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let s = keypair.sign(message, scheme, &mut rng).read();
	let s = s.ok().unwrap();	//Future settled.
	s.unwrap_err();			//Signing failed.
}

/*
fn strip_leading_zero<'a>(x: &'a [u8]) -> &'a [u8]
{
	if x[0] == 0 { &x[1..] } else { x }
}

fn extend_sexpr(dest: &mut Vec<u8>, x: &[u8])
{
	dest.extend(format!("{xlen}:", xlen=x.len()).as_bytes().iter());
	dest.extend(x.iter());
}

fn convert_file(name: &str)
{
	use std::io::Write;
	let mut contents = Vec::new();
	{
		let mut handle = File::open(name).unwrap();
		handle.read_to_end(&mut contents).unwrap();
	}
	let mut top = Source::new(&contents);
	let mut sub = top.read_asn1_value(ASN1_SEQUENCE, |_|"Error reading toplevel").unwrap().value;
	sub.read_asn1_value(ASN1_INTEGER, |_|"Error reading version").unwrap();	//Discard version.
	let mut dest = Vec::new();
	dest.push(b'(');
	dest.extend(b"3:rsa");
	extend_sexpr(&mut dest, strip_leading_zero(sub.read_asn1_value(ASN1_INTEGER,
		|_|"Error reading n").unwrap().raw_p));
	extend_sexpr(&mut dest, strip_leading_zero(sub.read_asn1_value(ASN1_INTEGER,
		|_|"Error reading e").unwrap().raw_p));
	extend_sexpr(&mut dest, strip_leading_zero(sub.read_asn1_value(ASN1_INTEGER,
		|_|"Error reading d").unwrap().raw_p));
	extend_sexpr(&mut dest, strip_leading_zero(sub.read_asn1_value(ASN1_INTEGER,
		|_|"Error reading p").unwrap().raw_p));
	extend_sexpr(&mut dest, strip_leading_zero(sub.read_asn1_value(ASN1_INTEGER,
		|_|"Error reading q").unwrap().raw_p));
	extend_sexpr(&mut dest, strip_leading_zero(sub.read_asn1_value(ASN1_INTEGER,
		|_|"Error reading dp").unwrap().raw_p));
	extend_sexpr(&mut dest, strip_leading_zero(sub.read_asn1_value(ASN1_INTEGER,
		|_|"Error reading dq").unwrap().raw_p));
	extend_sexpr(&mut dest, strip_leading_zero(sub.read_asn1_value(ASN1_INTEGER,
		|_|"Error reading qi").unwrap().raw_p));
	dest.push(b')');
	let mut handle = File::create(name).unwrap();
	handle.write_all(&dest).unwrap();
}

#[test]
fn convert_files()
{
	convert_file("src/rsa2040.der");
	convert_file("src/rsa2048.der");
	convert_file("src/rsa2055.der");
	convert_file("src/rsa2056.der");
	convert_file("src/rsa4095.der");
	convert_file("src/rsa4096.der");
	convert_file("src/rsa4097.der");
	convert_file("src/rsa4104.der");
}
*/

#[test]
fn test_jose_boms()
{
	kp_new_file("src/test-boms.jose").unwrap();
}
