use btls_aux_collections::Arc;
use btls_aux_collections::Cow;
use btls_aux_collections::Vec;
use core::slice::Iter as SliceIter;

///A SubjectPublicKeyInfo.
///
///The argument is an raw DER encoding of the PKIX SubjectPublicKeyInfo structure, including the leading SEQUENCE
///header.
#[derive(Clone)]
pub struct SubjectPublicKeyInfo(pub Arc<Vec<u8>>);

///A list of supported TLS SignatureSchemes.
///
///One can obtain an iterator over the methods by using the [`iter()`](#method.iter) method and get the length of
///the signature scheme list using the [`len()`](#method.len) method.
#[derive(Clone, Debug)]
pub struct SupportedSchemes(pub Arc<Cow<'static, [u16]>>);

impl SupportedSchemes
{
	///Get iterator over schemes in this list.
	///
	///The iterator yields `u16` items, one per supported TLS SignatureScheme.
	pub fn iter<'a>(&'a self) -> SliceIter<'a, u16> { self.0.iter() }
	///Get number of TLS SignatureSchemes in this list.
	pub fn len(&self) -> usize { self.0.len() }
}
