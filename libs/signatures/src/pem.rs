use crate::base64::Base64DecodeError;
use crate::base64::Base64Decoder;
use crate::base64::OutputStream;
use btls_aux_collections::Cow;
use btls_aux_collections::Vec;
use btls_aux_fail::fail_if;
use btls_aux_fail::fail_if_none;
use btls_aux_fail::ResultExt;
use btls_aux_serialization::is_single_asn1_structure;
use btls_aux_serialization::Source;
use core::convert::From;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::mem::replace;
use core::str::from_utf8;
use crate::keypair::DecodingError;

struct DevNull;
#[doc(hidden)]
#[derive(Debug)]
pub enum Void {}

//This can not actually be called.
impl Display for Void { fn fmt(&self, _: &mut Formatter) -> Result<(), FmtError> { Ok(()) } }

impl OutputStream for DevNull
{
	type Error = Void;
	fn write(&mut self, _: &[u8]) -> Result<(), Void> { Ok(()) }
}

impl OutputStream for Vec<u8>
{
	type Error = Void;
	fn write(&mut self, buf: &[u8]) -> Result<(), Void> { Ok(self.extend_from_slice(buf)) }
}

impl<T:OutputStream> From<Base64DecodeError<T>> for DecodingError
{
	fn from(x: Base64DecodeError<T>) -> DecodingError
	{
		match x {
			Base64DecodeError::InvalidInput => DecodingError::InvalidBase64,
			//Can not actually happen.
			Base64DecodeError::Io(_) => DecodingError::IoError,
		}
	}
}

#[allow(missing_docs)]
#[deprecated(since="1.5.0",note="Use iterate_pem_or_derseq_fragments instead")]
pub fn looks_like_pem(data: &[u8], kind: &str) -> bool
{
	//This is supposed to be UTF-8.
	let data = match from_utf8(data) { Ok(x) => x, Err(_) => return false };
	let mut found_something = false;
	let mut open_label = None;
	let mut validator = Base64Decoder::new();
	for i in data.lines() {
		if i.starts_with("-----BEGIN ") && i.ends_with("-----") {
			let xlbl = &i[10..i.len()-5];	//10 to allow space at start of kind.
			if xlbl.ends_with(kind) { found_something = true; }
			if open_label.is_some() { return false; }
			open_label = Some(xlbl);
			validator = Base64Decoder::new();
		} else if i.starts_with("-----END ") && i.ends_with("-----") {
			if validator.end(&mut DevNull).is_err() { return false; }
			let xlbl = &i[8..i.len()-5];	//8 to allow space at start of kind.
			if open_label != Some(xlbl) { return false; }
			open_label = None;
		} else if open_label.is_some() {
			if validator.data(i, &mut DevNull).is_err() { return false; }
		}
	}
	if open_label.is_some() { return false; }
	found_something
}

#[allow(missing_docs)]
#[deprecated(since="1.5.0",note="Use iterate_pem_or_derseq_fragments instead")]
pub fn decode_pem(data: &[u8], kind: &str, multi: bool) -> Result<Vec<u8>, DecodingError>
{
	use self::DecodingError::*;
	//This is supposed to be UTF-8.
	let data = from_utf8(data).set_err(NotValidUtf8)?;
	let mut out = Vec::new();
	let mut interesting = false;
	let mut found_one = false;
	let mut found_count = 0usize;
	let mut decoder = Base64Decoder::new();
	let mut last_start = 0usize;
	let mut last_startline = 0usize;
	//We skip everything we checked in looks_like_pem().
	for (linenum,i) in data.lines().enumerate() {
		if i.starts_with("-----BEGIN ") && i.ends_with("-----") {
			let xlbl = &i[10..i.len()-5];	//10 to allow space at start of kind.
			//If multi is set, we concatenate the elements.
			if xlbl.ends_with(kind) {
				if multi || !found_one { interesting = true; }
				found_count = found_count.saturating_add(1);
			}
			found_one |= interesting;
			last_start = out.len();
			last_startline = linenum;
			decoder = Base64Decoder::new();
		} else if i.starts_with("-----END ") && i.ends_with("-----") {
			//Only process if interesting. Ignore other blocks.
			if interesting {
				decoder.end(&mut out)?;
				let last_element = &out[last_start..];
				fail_if!(!is_single_asn1_structure(last_element), ElementNotAsn1(last_startline));
			}
			interesting = false;
		} else if interesting {
			decoder.data(i, &mut out)?;
		}
	}
	fail_if!(!multi && found_count != 1, ExpectedOne);
	Ok(out)
}

///PEM/DERseq fragment.
#[derive(Clone,Debug)]
pub enum PemDerseqFragment<'a>
{
	///PEM comment line.
	Comment(&'a [u8]),
	///PEM/DER fragment. PEM fragments are always owned, DER fragments are always borrowed.
	Fragment(&'a [u8], Cow<'a, [u8]>),
}

///Kind of PEM fragment.
#[derive(Clone,Debug,PartialEq,Eq)]
pub enum PemFragmentKind<'a>
{
	///PEM Certificate.
	Certificate,
	///PEM trusted certificate.
	TrustedCertificate,
	///PEM X.509 CRL.
	X509Crl,
	///PEM Certificate request.
	CertificateRequest,
	///PEM PKCS7 structure.
	Pkcs7,
	///PEM CMS structure.
	Cms,
	///PEM Private key structure.
	PrivateKey,
	///PEM RSA Private key structure.
	RsaPrivateKey,
	///PEM EC Private key structure.
	EcPrivateKey,
	///PEM Encrypted Private key structure.
	EncryptedPrivateKey,
	///PEM Attribute certificate
	AttributeCertificate,
	///PEM public key.
	PublicKey,
	///PEM DH parameters.
	DhParameters,
	#[doc(hidden)]
	Other(&'a str)
}

///PEM/DERseq fragment.
#[derive(Clone,Debug)]
pub enum PemDerseqFragment2<'a>
{
	///DER fragment.
	Der(&'a [u8]),
	///PEM comment line.
	Comment(&'a [u8]),
	///PEM fragment.
	Pem(PemFragmentKind<'a>, &'a [u8]),
	///Format Error.
	Error
}

include!(concat!(env!("OUT_DIR"), "/autogenerated-pem.inc.rs"));
///Iterate over all PEM/DERseq fragments.
pub fn iterate_pem_or_derseq_fragments<E,F:FnMut(PemDerseqFragment2)->Result<(),E>>(file: &[u8], mut f: F) ->
	Result<(), E>
{
	//Prefer PEM, fall back to DERseq.
	if validate_pem(file).is_some() {
		iterate_valid_pem_file(file, &mut f,
			|f,comment|f(PemDerseqFragment2::Comment(comment)),
			|f,kind,value|match construct_pem_fragment(kind, &value) {
			Some(frag) => f(frag),
			None => f(PemDerseqFragment2::Error)
		})?;
	} else {
		let mut src = Source::new(file);
		while src.more() { match src.asn1_sequence_out() {
			Ok(g) => f(PemDerseqFragment2::Der(g))?,
			Err(_) => return f(PemDerseqFragment2::Error)
		}}
	}
	Ok(())
}

///Struct that can be set once.
pub struct SetOnce<T>(Option<T>);

impl<T> Default for SetOnce<T>
{
	fn default() -> SetOnce<T> { SetOnce(None) }
}

impl<T> SetOnce<T>
{
	///Set value.
	pub fn set<E,F:FnOnce()->E>(&mut self, val: T, errh: F) -> Result<(),E>
	{
		if self.0.is_some() { return Err(errh()); }
		Ok(self.0 = Some(val))
	}
	///Get value.
	pub fn get<E,F:FnOnce()->E>(self, errh: F) -> Result<T,E> { self.0.ok_or_else(errh) }
	///Get value.
	pub fn get_or(self, errh: impl FnOnce()->T) -> T { self.0.unwrap_or_else(errh) }
}

fn strip_prefix_suffix<'a>(x: &'a [u8], prefix: &[u8], suffix: &[u8]) -> Option<&'a [u8]>
{
	let x = x.strip_prefix(prefix)?;
	x.strip_suffix(suffix)
}

fn validate_pem(file: &[u8]) -> Option<()>
{
	let mut open_resource = None;
	let mut any_resources = false;
	let mut validator = Base64VState::Init;
	for line in file.split(|&x|x==13||x==10) {
		if let Some(resource) = strip_prefix_suffix(line, b"-----BEGIN ", b"-----") {
			fail_if_none!(open_resource.is_some());
			open_resource = Some(resource);
		} else if let Some(resource) = strip_prefix_suffix(line, b"-----END ", b"-----") {
			fail_if_none!(!validate_base64_reset(&mut validator));
			fail_if_none!(open_resource != Some(resource));
			open_resource = None;
			any_resources = true;
		} else if open_resource.is_some() {
			//Note that -----BEGIN -line would not pass validation.
			fail_if_none!(!validate_base64(&mut validator, line))
		}
	}
	fail_if_none!(!any_resources || open_resource.is_some());
	Some(())
}

fn iterate_valid_pem_file<'a,C,E>(file: &'a [u8], context: &mut C,
	mut comment: impl FnMut(&mut C,&'a [u8]) -> Result<(), E>,
	mut fragment: impl FnMut(&mut C, &'a [u8], Vec<u8>) -> Result<(), E>) -> Result<(), E>
{
	//This is past point of no return. Decode as PEM treating any errors as UB.
	let mut resource: &'a [u8] = b"";
	let mut datablock = Vec::new();
	let mut base64_state = 0;
	for line in file.split(|&x|x==13||x==10) {
		if let Some(nresource) = strip_prefix_suffix(line, b"-----BEGIN ", b"-----") {
			resource = nresource;
			base64_state = 1;
		} else if strip_prefix_suffix(line, b"-----END ", b"-----").is_some() {
			//Decode the last block.
			if base64_state >> 18 > 0 {
				datablock.push((base64_state >> 10) as u8);
				datablock.push((base64_state >> 2) as u8);
			} else if base64_state >> 12 > 0 {
				datablock.push((base64_state >> 4) as u8);
			}
			let new = replace(&mut datablock, Vec::new());
			fragment(context, resource, new)?;
			base64_state = 0;
		} else if base64_state > 0 {
			for &b in line.iter() {
				match b {
					65..=90 => base64_state = base64_state * 64 + (b as u32 - 65),
					97..=122 => base64_state = base64_state * 64 + (b as u32 - 71),
					48..=57 => base64_state = base64_state * 64 + (b as u32 + 4),
					43 => base64_state = base64_state * 64 + 62,
					47 => base64_state = base64_state * 64 + 63,
					//Ignore invalid bytes.
					_ => (),
				}
				if base64_state >> 24 > 0 {
					datablock.push((base64_state >> 16) as u8);
					datablock.push((base64_state >> 8) as u8);
					datablock.push(base64_state as u8);
					base64_state = 1;
				}
			}
		} else if line.len() > 0 { comment(context, line)?; }
	}
	Ok(())
}

#[derive(Copy,Clone)]
enum Base64VState
{
	Init,	//Idle.
	Ch1,	//1 character.
	Ch2Z,	//2 characters, last is xx0000.
	Ch2,	//2 characters, last is !xx0000.
	Ch3Z,	//3 characters, last is xxxx00.
	Ch3,	//3 characters, last is !xxxx00.
	Ch3E,	//2 characters and equals.
	Ch4E,	//Full padded block.
}

///xxxxx xx0000
///xxxxx xxxxxx xxxx00
///xxxxx xxxxxx xxxxxx xxxxxx
fn validate_base64(state: &mut Base64VState, input: &[u8]) -> bool
{
	use self::Base64VState::*;
	for &b in input.iter() { match b {
		9|32 => (),					//Ignore whitespace.
		65..=90 => *state = match *state {
			Init => Ch1,
			Ch1 if b & 15 == 1 => Ch2Z,
			Ch1 => Ch2,
			Ch2|Ch2Z if b & 3 == 1 => Ch3Z,
			Ch2|Ch2Z => Ch3,
			Ch3|Ch3Z => Init,
			Ch3E|Ch4E => return false
		},
		97..=122 => *state = match *state {
			Init => Ch1,
			Ch1 if b & 15 == 7 => Ch2Z,
			Ch1 => Ch2,
			Ch2|Ch2Z if b & 3 == 3 => Ch3Z,
			Ch2|Ch2Z => Ch3,
			Ch3|Ch3Z => Init,
			Ch3E|Ch4E => return false
		},
		43|47..=57 => *state = match *state {
			Init => Ch1,
			Ch1 => Ch2,				//b&15 would be 12, not in range.
			Ch2|Ch2Z if b & 3 == 0 => Ch3Z,
			Ch2|Ch2Z => Ch3,
			Ch3|Ch3Z => Init,
			Ch3E|Ch4E => return false
		},
		61 => *state = match *state {			//Padding
			Ch2Z => Ch3E,				//XY=.
			Ch3Z => Ch4E,				//XYZ=.
			Ch3E => Ch4E,				//XY==.
			_ => return false,			//Others are illegal.
		},
		_ => return false				//Bad character.
	}}
	true
}

fn validate_base64_reset(state: &mut Base64VState) -> bool
{
	use self::Base64VState::*;
	matches!(replace(state, Init), Init|Ch2Z|Ch3Z|Ch4E)
}
