use super::*;
use btls_aux_collections::BTreeMap;
use btls_aux_collections::ToOwned;
use btls_aux_collections::ToString;
use btls_aux_collections::Vec;
use btls_aux_json2::JSON;
use core::convert::TryInto;
use core::ops::Deref;

#[test]
fn rfc8037_key()
{
	//This is the example from RFC8037.
	use btls_aux_hash::Hash;
	use btls_aux_hash::Sha256;
	let key = include_bytes!("ed25519-testkey.txt");
	let mut key = &key[..];
	let key = LocalKeyPair::new(&mut key, "test key").unwrap();
	let pubkey = key.get_public_key().0;
	let pubkey= crate::jose::extract_jwk_tail(&pubkey).unwrap();
	let thumb = base64url(&Sha256::hash(&pubkey.serialize_string().as_bytes()));
	assert_eq!(&thumb, "kPrK_qmxVWaYVA9wwBF6Iuo3vVzz7TxHCTwXBygrS4k");
}

fn rfc8037_sig_core(headers: JsonNode, data: &[u8]) -> SignedJws
{
	use btls_aux_random::TemporaryRandomLifetimeTag;
	let ed25519 = 0x807;
	let key = include_bytes!("ed25519-testkey.txt");
	let mut key = &key[..];
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let key = LocalKeyPair::new(&mut key, "test key").unwrap();
	match sign_jws(&headers, data, &key, ed25519, &mut rng).read() {
		Ok(Ok(x)) => x,
		_ => { assert!(false); unreachable!(); }
	}
}

const TEST_MESSAGE: &'static [u8] = b"Example of Ed25519 signing";

fn rfc8037_sig_do() -> SignedJws
{
	rfc8037_sig_core(JsonNode::Dictionary(BTreeMap::new()), TEST_MESSAGE)
}

fn rfc8037_sig_do_detached() -> SignedJws
{
	let mut hdr = BTreeMap::new();
	hdr.insert("b64".to_string(), JsonNode::Boolean(true));
	rfc8037_sig_core(JsonNode::Dictionary(hdr), TEST_MESSAGE)
}

#[test]
fn rfc8037_sig()
{
	let sig = rfc8037_sig_do();
	assert_eq!(sig.to_compact(), "eyJhbGciOiJFZERTQSJ9.RXhhbXBsZSBvZiBFZDI1NTE5IHNpZ25pbmc.hgyY0il_MGCj\
		P0JzlnLWG1PPOt7-09PGcvMg3AIbQR6dWbhijcNR4ki4iylGjg5BhVsPt9g7sVvpAr_MuM0KAg");
}

#[test]
fn rfc8037_sig_json()
{
	let sig = rfc8037_sig_do();
	let sig = sig.to_json();
	let sig = JSON::parse(&sig).expect("JSON signature does not parse!");
	assert!(sig.size() == 3);
	let protected = sig.field("protected").expect("no protected").as_string().expect("protected not string");
	assert_eq!(protected, "eyJhbGciOiJFZERTQSJ9");
	let payload = sig.field("payload").expect("no payload").as_string().expect("payload not string");
	assert_eq!(payload, "RXhhbXBsZSBvZiBFZDI1NTE5IHNpZ25pbmc");
	let signature = sig.field("signature").expect("no signature").as_string().expect("signature not string");
	assert_eq!(signature, "hgyY0il_MGCjP0JzlnLWG1PPOt7-09PGcvMg3AIbQR6dWbhijcNR4ki4iylGjg5BhVsPt9g7sVvpAr_\
		MuM0KAg");
}

#[test]
fn rfc8037_sig_detached()
{
	let sig = rfc8037_sig_do_detached();
	assert_eq!(sig.to_compact(), "eyJhbGciOiJFZERTQSJ9..hgyY0il_MGCjP0JzlnLWG1PPOt7-09PGcvMg3AIbQR6dWbhijcNR\
		4ki4iylGjg5BhVsPt9g7sVvpAr_MuM0KAg");
}

#[test]
fn rfc8037_sig_detached_json()
{
	let sig = rfc8037_sig_do_detached();
	let sig = sig.to_json();
	let sig = JSON::parse(&sig).expect("JSON signature does not parse!");
	assert!(sig.size() == 2);
	let protected = sig.field("protected").expect("no protected").as_string().expect("protected not string");
	assert_eq!(protected, "eyJhbGciOiJFZERTQSJ9");
	let signature = sig.field("signature").expect("no signature").as_string().expect("signature not string");
	assert_eq!(signature, "hgyY0il_MGCjP0JzlnLWG1PPOt7-09PGcvMg3AIbQR6dWbhijcNR4ki4iylGjg5BhVsPt9g7sVvpAr_\
		MuM0KAg");
}

fn rfc8037_sig2_detached_test(reftbs: &str, bflag: bool)
{
	use btls_aux_random::TemporaryRandomLifetimeTag;
	let key = include_bytes!("ed25519-testkey.txt");
	let mut key = &key[..];
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let key = LocalKeyPair::new(&mut key, "test key").unwrap();
	let pubkey = key.get_public_key2();
	let pubkey: &[u8;32] = pubkey[12..44].try_into().expect("Not 32 bytes???");
	let mut hdr = BTreeMap::new();
	hdr.insert("b64".to_string(), JSON::Boolean(bflag));
	let sig = match sign_jws2(&JSON::Object(hdr), TEST_MESSAGE, &key, &mut rng).read() {
		Ok(Ok(x)) => x,
		_ => { assert!(false); unreachable!(); }
	};
	let sig = sig.to_compact();
	//The signature is random, so verify it instead of comparing to a KAT.
	let xlen = if bflag { 108 } else { 144 };
	assert_eq!(sig.len(), xlen);
	assert_eq!(&sig[..xlen-87], &reftbs[..xlen-87]);
	assert!(&sig[xlen-87..xlen-86] == ".");
	let sig2 = &sig[xlen-86..xlen];
	let sig2 = decode_base64url(&sig2).expect("Decode base64 failed?");
	let sig2: &[u8;64] = sig2[..].try_into().expect("Not 64 bytes???");
	assert!(btls_aux_xed25519::ed25519_verify(pubkey, reftbs.as_bytes(), &sig2).is_ok());
}

#[test]
fn rfc8037_sig2_detached()
{
	let reftbs = "eyJhbGciOiJFZERTQSJ9.RXhhbXBsZSBvZiBFZDI1NTE5IHNpZ25pbmc";
	rfc8037_sig2_detached_test(reftbs, true);
}

#[test]
fn rfc8037_sig2_detached_raw()
{
	let reftbs = "eyJhbGciOiJFZERTQSIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19.Example of Ed25519 signing";
	rfc8037_sig2_detached_test(reftbs, false);
}

#[test]
fn serialization_test()
{
	let mut v = Vec::new();
	v.push(JsonNode::String("foo\u{7}bar".to_owned()));
	v.push(JsonNode::Number(1234));
	let x = JsonNode::Array(v);
	assert_eq!(x.serialize().deref(), r#"["foo\u0007bar",1234]"#);
}

#[test]
fn rfc7797_default()
{
	let refhdr = "eyJhbGciOiJIUzI1NiJ9";
	let reftbs = b"eyJhbGciOiJIUzI1NiJ9.JC4wMg";
	let header: BTreeMap<String, JSON> = BTreeMap::new();
	let header = JSON::Object(header);
	let msg = b"$.02";
	let alg = Some("HS256");

	let (tbs, hrng, prng, dtbs) = crate::jose::jws_sign_tbs(&header, msg, alg, true).
		expect("jws_sign_tbs failed");
	let (dtbs, detached) = dtbs.true_tbs(&tbs).expect("No tbs???");
	assert!(!detached);
	assert_eq!(tbs.as_bytes(), reftbs);
	assert_eq!(&tbs[hrng.clone()], refhdr);
	assert_eq!(&tbs[prng.clone()], "JC4wMg");
	assert_eq!(dtbs, reftbs);

	let (tbs, hrng, prng, dtbs) = crate::jose::jws_sign_tbs(&header, msg, alg, false).
		expect("jws_sign_tbs failed");
	assert_eq!(tbs.as_bytes(), reftbs);
	assert_eq!(&tbs[hrng.clone()], refhdr);
	assert_eq!(&tbs[prng.clone()], "JC4wMg");
	assert!(matches!(dtbs, crate::jose::DetachedTbs::Inline));
}

#[test]
fn rfc7797_explicit()
{
	let refhdr = "eyJhbGciOiJIUzI1NiJ9";
	let reftbs = b"eyJhbGciOiJIUzI1NiJ9.JC4wMg";
	let mut header: BTreeMap<String, JSON> = BTreeMap::new();
	header.insert("b64".to_string(), JSON::Boolean(true));
	let header = JSON::Object(header);
	let msg = b"$.02";
	let alg = Some("HS256");

	let (tbs, hrng, prng, dtbs) = crate::jose::jws_sign_tbs(&header, msg, alg, true).
		expect("jws_sign_tbs failed");
	let (dtbs, detached) = dtbs.true_tbs(&tbs).expect("No tbs???");
	assert!(detached);
	assert_eq!(tbs, refhdr);
	assert_eq!(&tbs[hrng.clone()], refhdr);
	assert_eq!(&tbs[prng.clone()], "");
	assert_eq!(dtbs, reftbs);

	let (tbs, hrng, prng, dtbs) = crate::jose::jws_sign_tbs(&header, msg, alg, false).
		expect("jws_sign_tbs failed");
	assert_eq!(tbs, refhdr);
	assert_eq!(&tbs[hrng.clone()], refhdr);
	assert_eq!(&tbs[prng.clone()], "");
	assert!(matches!(dtbs, crate::jose::DetachedTbs::DetachedBase64));
}



//eyJhbGciOiJFZERTQSJ9
//eyJhbGciOiJFZERTQSIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19
//eyJhbGciOiJIUzI1NiJ9
//eyJhbGciOiJIUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19
#[test]
fn rfc7797_unencoded()
{
	let refhdr = "eyJhbGciOiJIUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19";
	let reftbs = b"eyJhbGciOiJIUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19.$.02";
	let mut header: BTreeMap<String, JSON> = BTreeMap::new();
	header.insert("b64".to_string(), JSON::Boolean(false));
	let header = JSON::Object(header);
	let msg = b"$.02";
	let alg = Some("HS256");

	let (tbs, hrng, prng, dtbs) = crate::jose::jws_sign_tbs(&header, msg, alg, true).
		expect("jws_sign_tbs failed");
	let (dtbs, detached) = dtbs.true_tbs(&tbs).expect("No tbs???");
	assert!(detached);
	assert_eq!(tbs, refhdr);
	assert_eq!(&tbs[hrng.clone()], refhdr);
	assert_eq!(&tbs[prng.clone()], "");
	assert_eq!(dtbs, reftbs);

	let (tbs, hrng, prng, dtbs) = crate::jose::jws_sign_tbs(&header, msg, alg, false).
		expect("jws_sign_tbs failed");
	assert_eq!(tbs, refhdr);
	assert_eq!(&tbs[hrng.clone()], refhdr);
	assert_eq!(&tbs[prng.clone()], "");
	assert!(matches!(dtbs, crate::jose::DetachedTbs::DetachedRaw));
}
