use crate::extract_raw_key;
use crate::InternalKey;
use crate::KeyFormat;
use crate::KeyIdentify2;
use crate::SignatureAlgorithmTls2;
use crate::SignatureFormat;
use btls_aux_collections::BTreeMap;
use btls_aux_collections::String;
use btls_aux_collections::ToOwned;
use btls_aux_collections::ToString;
use btls_aux_collections::Vec;
use btls_aux_fail::dtry;
use btls_aux_fail::f_return;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::ResultExt;
use btls_aux_futures::FutureMapper;
use btls_aux_futures::FutureReceiver;
use btls_aux_random::TemporaryRandomStream;
use btls_aux_serialization::Asn1Tag;
use btls_aux_serialization::Source;
#[allow(deprecated)] pub use btls_aux_json::JsonWriter as JsonNode;
pub use btls_aux_json2::base64url;
use btls_aux_json2::base64url_append;
use btls_aux_json2::JSON;
use btls_aux_json2::json_object;
use btls_aux_json2::Number as JsonNumber;
use core::ops::Deref;
use core::ops::Range;
use core::str::from_utf8;
use crate::keypair::DecodingError;
#[allow(deprecated)] use crate::keypair::KeyPair;
use crate::keypair::KeyPair2;

pub(crate) fn convert_key_from_jwk2(jwk_key: &[u8]) -> Result<InternalKey, DecodingError>
{
	use crate::keypair::DecodingError::*;
	use crate::ecdsa::EcdsaDecodingError::*;
	use crate::eddsa::EddsaDecodingError::*;
	//This is supposed to be UTF-8.
	let mut jwk_key = from_utf8(jwk_key).set_err(NotValidUtf8)?;
	let json = JSON::parse(&mut jwk_key).set_err(JwkInvalidJson)?;
	fail_if!(!json.is_object(), WkToplevelNotDictionary);
	let kty = json.field("kty").ok_or(NoKeyType)?;
	if kty == "EC" {
		//ECC key.
		static ZEROBYTE: [u8;1] = [0];
		let crv = json.field("crv").ok_or(NoCurve)?;
		let curve = crv.as_string().and_then(|crv|crate::ecdsa::EcdsaCurve::by_jwk_id(crv)).
			ok_or(UnknownCurve)?;
		let (oid, len) = (&ZEROBYTE[..], curve.get_component_bytes());
		let d = read_jose_keypart(&json, "d", "EC")?;
		let x = read_jose_keypart(&json, "x", "EC")?;
		let y = read_jose_keypart(&json, "y", "EC")?;
		return crate::ecdsa::serialize_key(oid, &d, &x, &y, len);
	} else if kty == "OKP" {
		//OKP key (Ed25519 or Ed448).
		let sub = json.field("crv").ok_or(WkNoOkpSubtype)?;
		let d = read_jose_keypart(&json, "d", "OKP")?;
		let x = read_jose_keypart(&json, "x", "OKP")?;
		if sub == "Ed25519" {
			fail_if!(d.len() != 32, BadEd25519d);
			fail_if!(x.len() != 32, BadEd25519x);
			let mut res = Vec::new();
			res.extend_from_slice(&d);
			res.extend_from_slice(&x);
			return Ok(InternalKey::join(KeyFormat::Ed25519, res));
		} else if sub == "Ed448" {
			fail_if!(d.len() != 57, BadEd448d);
			fail_if!(x.len() != 57, BadEd448x);
			let mut res = Vec::new();
			res.extend_from_slice(&d);
			res.extend_from_slice(&x);
			return Ok(InternalKey::join(KeyFormat::Ed448, res));
		} else {
			fail!(WkUnknownOkpSubtype);
		}
	} else {
		fail!(UnknownKeyType);
	}
}

fn read_jose_keypart(dict: &JSON, key: &'static str, jwktype: &'static str) ->
	Result<Vec<u8>, DecodingError>
{
	use self::DecodingError::*;
	decode_base64url_json(dict.field(key).ok_or(WkNoParameter(key, jwktype))?).
		set_err(WkBadParameter(key, jwktype))
}

fn base64url_chunk<'a>(output: &'a mut [u8], input: &mut &[u8]) -> &'a [u8]
{
	static C: &[u8;64] = b"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";
	let mut i = 0;
	let mut j = 0;
	//Handle full blocks.
	while let (Some(output), Some(input)) = (output.get_mut(i..i+4), input.get(j..j+3)) {
		let cw = input[0] as usize * 65536 + input[1] as usize * 256 + input[2] as usize;
		output[0] = C[(cw >> 18) & 63];
		output[1] = C[(cw >> 12) & 63];
		output[2] = C[(cw >> 6) & 63];
		output[3] = C[cw & 63];
		i += 4;
		j += 3;
	}
	//Handle possible last partial block.
	if let (Some(output), Some(input)) = (output.get_mut(i..i+4), input.get(j..)) {
		if input.len() == 1 {
			let cw = input[0] as usize * 65536;
			output[0] = C[(cw >> 18) & 63];
			output[1] = C[(cw >> 12) & 63];
			i += 2;
			j += 1;
		} else if input.len() == 2 {
			let cw = input[0] as usize * 65536 + input[1] as usize * 256;
			output[0] = C[(cw >> 18) & 63];
			output[1] = C[(cw >> 12) & 63];
			output[2] = C[(cw >> 6) & 63];
			i += 3;
			j += 2;
		}
	}
	//Advance encoded part.
	*input = input.get(j..).unwrap_or(b"");
	output.get(..i).unwrap_or(b"")
}



fn decode_base64url_json(x: &JSON) -> Result<Vec<u8>, ()>
{
	let x = dtry!(x.as_string());
	decode_base64url(x)
}

pub(crate) fn decode_base64url(x: &str) -> Result<Vec<u8>, ()>
{
	let mut v = Vec::new();
	let mut base64_pol = 0u8;
	let mut base64_val = 0;
	for j in x.chars() {
		if j == ' ' || j == '\t' { continue; }
		let x = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".find(j).unwrap_or(64);
		if x == 64 { return Err(()); }
		base64_val = base64_val << 6 | x;
		base64_pol = base64_pol.wrapping_add(1) & 3;
		if base64_pol == 0 {
			v.push((base64_val >> 16) as u8);
			v.push((base64_val >> 8) as u8);
			v.push(base64_val as u8);
		}
	}
	if base64_pol == 1 { return Err(()); }
	if base64_pol == 2 {
		v.push((base64_val >> 4) as u8);
	} else if base64_pol == 3 {
		v.push((base64_val >> 10) as u8);
		v.push((base64_val >> 2) as u8);
	}
	Ok(v)
}


///The signed JWS.
///
///This structure is created by the [`sign_jws2()`](fn.sign_jws2.html) function.
///
///The result can be put into compact serialization by using the [`to_compact()`](#method.to_compact) method, and
///into JSON serialization by usign the [`to_json()`](#method.to_json) method.
#[derive(Clone,Debug)]
pub struct SignedJws
{
	///The data to be signed. If detached is set, this is just the base64 encoding of protected.
	tbs: String,
	///The protected JWS header, as base64url string. 
	header: Range<usize>,
	///The signed payload, as `base64url` string. This can be whatever if detached is set.
	payload: Range<usize>,
	///The signature, as `base64url` string.
	signature: String,
	///Detached content mode.
	detached: bool,
}

impl SignedJws
{
	///Emit the signed JWS as compact serialization.
	pub fn to_compact(&self) -> String
	{
		let mut x = String::with_capacity(self.tbs.len() + self.signature.len() + 2);
		x.push_str(&self.tbs);
		x.push('.');
		//In detached mode, self.tbs is just the header part, so add a second '.' in order to end the
		//body, which is empty because of detached.
		if self.detached { x.push('.'); }
		x.push_str(&self.signature);
		x
	}
	///Emit signed JWS as JSON serialization.
	pub fn to_json(&self) -> String
	{
		//Assume that protected and payload are already base64-encoded, and thus contain no characters
		//that need escaping.
		let protected = self.tbs.get(self.header.clone()).unwrap_or("");
		let payload = self.tbs.get(self.payload.clone()).unwrap_or("");
		let signature = &self.signature;
		let mut x = String::with_capacity(protected.len() + payload.len() + signature.len() + 44);
		x.push_str(r#"{"protected":""#);
		x.push_str(protected);
		x.push_str(r#"","#);
		if !self.detached {
			x.push_str(r#""payload":""#);
			x.push_str(payload);
			x.push_str(r#"","#);
		}
		x.push_str(r#""signature":""#);
		x.push_str(signature);
		x.push_str(r#""}"#);
		x
	}
}

fn __jose_map(adapter: &JwsSignAdapter, fmt: Option<SignatureFormat>, sig: Vec<u8>) -> Result<SignedJws, ()>
{
	//Override the format if specified, otherwise use adapter default format.
	let signature = fmt.unwrap_or(adapter.format).to_jose(&sig)?;
	Ok(SignedJws{
		tbs: adapter.tbs.clone(),
		header: adapter.header.clone(),
		payload: adapter.payload.clone(),
		signature: signature,
		detached: adapter.detached,
	})
}

struct JwsSignAdapter
{
	format: SignatureFormat,
	tbs: String,
	header: Range<usize>,
	payload: Range<usize>,
	detached: bool,
}

impl FutureMapper<Result<(SignatureFormat, Vec<u8>), ()>, Result<SignedJws, ()>> for JwsSignAdapter
{
	fn map(&mut self, val: Result<(SignatureFormat, Vec<u8>), ()>) -> Result<SignedJws, ()>
	{
		let (format, val) = val?;
		__jose_map(&self, Some(format), val)
	}
}

impl FutureMapper<Result<Vec<u8>, ()>, Result<SignedJws, ()>> for JwsSignAdapter
{
	fn map(&mut self, val: Result<Vec<u8>, ()>) -> Result<SignedJws, ()>
	{
		let val = val?;
		__jose_map(&self, None, val)
	}
}

pub(crate) enum DetachedTbs
{
	Inline,
	Detached(Vec<u8>),
	DetachedBase64,
	DetachedRaw,
}

impl DetachedTbs
{
	pub fn true_tbs<'a>(&'a self, tbs: &'a String) -> Option<(&'a [u8], bool)>
	{
		match self {
			&DetachedTbs::Inline => Some((tbs.as_bytes(), false)),
			&DetachedTbs::Detached(ref x) => Some((x, true)),
			//These two should not happen, since full tbs was requested.
			&DetachedTbs::DetachedBase64 => None,
			&DetachedTbs::DetachedRaw => None,
		}
	}
}

#[allow(missing_docs,deprecated)]
#[deprecated(since="1.10.5", note="Use sign_jws2() instead")]
pub fn sign_jws<K:KeyPair>(protected_header: &JsonNode, payload: &[u8], keypair: &K, algorithm: u16,
	rng: &mut TemporaryRandomStream) -> FutureReceiver<Result<SignedJws, ()>>
{
	let err = FutureReceiver::from(Err(()));
	let alg = f_return!(SignatureAlgorithmTls2::by_tls_id(algorithm), err);
	let alg = f_return!(alg.to_generic().get_jws_algo(), err);
	let pk = keypair.get_public_key();
	let format = SignatureFormat::for_public_key(&pk.0);
	let protected_header = translate_json(protected_header);
	let (tbs, header, payload, detached) = f_return!(jws_sign_tbs(&protected_header, payload, Some(alg), true),
		err);
	let (tbsx, detached) = f_return!(detached.true_tbs(&tbs), err);
	keypair.sign(tbsx, algorithm, rng).map(JwsSignAdapter{tbs, header, payload, format, detached})
}

///Sign a JWS (JSON web signature)
///
///Like [`sign_jws()`](fn.sign_jws.html), but with [`KeyPair2`] instead.
///
///If the JWS header does not have alg field, one is automatically filled with the first algorithm the key is
///capable of that is usable in JWS. If the JWS header has the alg field, that algorithm is forced. If the key is
///not capable of it, signing fails.
///
///If making a detached signature (`b64` present), this function will not allocate memory for the payload,
///instead opting to use the payload buffer passed in.
///
///[`KeyPair2`]: trait.KeyPair2.html
pub fn sign_jws2<K:KeyPair2>(protected_header: &JSON, payload: &[u8], keypair: &K,
	rng: &mut TemporaryRandomStream) -> FutureReceiver<Result<SignedJws, ()>>
{
	let err = FutureReceiver::from(Err(()));
	let mut opayload = payload;
	//If the protected header has alg, attempt to force the algorithm.
	let forced_algorithm: Option<&str> = match protected_header.field("alg") {
		Some(&JSON::String(ref a)) => Some(a),
		None => None,
		_ => return err		//Invalid header.
	};
	let algorithm = keypair.get_signature_algorithms().iter().find(|algo|{
		//Skip algorithms with no JWS algo, they can not be used.
		let jwsalg = f_return!(algo.get_jws_algo(), false);
		//JWS Algorithm is suitable if none is forced, or this is forced one.
		forced_algorithm.is_none() || forced_algorithm == Some(jwsalg)
	});
	//The signature algorithm better exist.
	let algorithm = *f_return!(algorithm, err);
	let jwsid = f_return!(algorithm.get_jws_algo(), err);
	let format = SignatureFormat::Unknown;	//Taken from signature.
	let res = f_return!(jws_sign_tbs(&protected_header, payload, Some(jwsid), false), err);
	let (tbs, header, payload, detached) = res;
	let (sig, detached) = match detached {
		DetachedTbs::Inline => (keypair.sign_data(tbs.as_bytes(), algorithm, rng), false),
		DetachedTbs::DetachedBase64 => {
			let s = keypair.sign_callback(algorithm, rng, &mut |input|{
				input.append(tbs.as_bytes());
				input.append(b".");
				if opayload.len() <= 192 {
					let mut buf = [0;256];
					input.append(base64url_chunk(&mut buf, &mut opayload));
				} else {
					let mut buf = [0;1024];
					while opayload.len() > 0 {
						input.append(base64url_chunk(&mut buf, &mut opayload));
					}
				}
			});
			(s, true)
		},
		DetachedTbs::DetachedRaw => {
			let s = keypair.sign_callback(algorithm, rng, &mut |input|{
				input.append(tbs.as_bytes());
				input.append(b".");
				input.append(opayload);
			});
			(s, true)
		},
		//This should not happen as need_full_tbs=false.
		DetachedTbs::Detached(_) => return err,
	};
	sig.map(JwsSignAdapter{tbs, header, payload, format, detached})
}

type SigRet = Result<(SignatureFormat, Vec<u8>), ()>;

///Sign a JWS (JSON web signature)
///
///Like [`sign_jws()`](fn.sign_jws.html), but with synchrous signer instead.
///
///If the `protected_header` has no `alg` field, then the `alg` field is populated from the value of the
///`algorithm` field. If this field is `None`, signing fails.
pub fn sign_jws_synchronous(protected_header: &JSON, payload: &[u8], algorithm: Option<&str>,
	signer: &mut dyn FnMut(&[u8]) -> SigRet) -> Result<SignedJws, ()>
{
	let (tbs, header, payload, detached) = jws_sign_tbs(protected_header, payload, algorithm, true)?;
	let (tbsx, detached) = dtry!(detached.true_tbs(&tbs));
	let (format, sig) = signer(tbsx)?;
	JwsSignAdapter{tbs, header, payload, format, detached}.map(Ok(sig))
}

#[allow(deprecated)]
fn translate_json(x: &JsonNode) -> JSON
{
	match x {
		&JsonNode::Null => JSON::Null,
		&JsonNode::Boolean(b) => JSON::Boolean(b),
		&JsonNode::Number(n) => JSON::Number(JsonNumber::new_signed(n)),
		&JsonNode::String(ref s) => JSON::String(s.clone()),
		&JsonNode::Array(ref x) => {
			let mut y = Vec::new();
			for z in x.iter() { y.push(translate_json(z)); }
			JSON::Array(y)
		},
		&JsonNode::Dictionary(ref x) => {
			let mut y = BTreeMap::new();
			for (k,v) in x.iter() { y.insert(k.clone(), translate_json(v)); }
			JSON::Object(y)
		}
		//I hope this is never hit.
		_ => JSON::Null
	}
}

fn add_critical_header(critical: &mut Option<Vec<JSON>>, header: &str)
{
	if let Some(crit) = critical.as_mut() {
		let done = crit.iter().any(|v|v.as_string() == Some(header));
		if !done { crit.push(JSON::from(header)); }
	} else {
		let mut v = Vec::with_capacity(1);
		v.push(JSON::from(header));
		*critical = Some(v);
	}
}

//This is pub(crate) for a few tests.
pub(crate) fn jws_sign_tbs(protected_header: &JSON, payload: &[u8], algorithm: Option<&str>, need_full_tbs: bool) ->
	Result<(String,Range<usize>,Range<usize>,DetachedTbs), ()>
{
	let mut detached = false;
	let mut raw = false;
	let mut need_b64_crit = false;
	let mut critical: Option<Vec<JSON>> = None;
	let protected_header = if let &JSON::Object(ref x) = protected_header {
		let mut arr = BTreeMap::new();
		let mut has_alg = false;
		for (k,v) in x.iter() {
			if k == "alg" {
				has_alg = true;
			} else if k == "b64" {
				detached = true;	//Impiles detached.
				raw = !dtry!(v.as_boolean());
				if !raw { continue; }	//Omit this if b64=true (default mode).
				need_b64_crit = true;	//Non-default mode => critical.
			} else if k == "crit" {
				critical = Some(dtry!(v.as_array()).to_vec());
				continue;	//Set separatedly.
			}
			arr.insert(k.clone(), v.clone());
		}
		//If there is no crit for b64, add one.
		if need_b64_crit { add_critical_header(&mut critical, "b64"); }
		//Add back critical if needed.
		if let Some(crit) = critical {
			arr.insert("crit".to_owned(), JSON::Array(crit));
		}
		if !has_alg {
			let alg = dtry!(algorithm);
			arr.insert("alg".to_owned(), JSON::from(alg));
		}
		JSON::Object(arr)
	} else {
		fail!(());
	};
	let header = protected_header.serialize_string();
	if !detached {
		jws_sign_tbs_tail_inline(header, payload)
	} else if !need_full_tbs {
		let mut tbs = String::with_capacity((4 * header.len() + 5) / 3);
		base64url_append(&mut tbs, header.as_bytes());
		let hrng = 0..tbs.len();
		let k = if raw { DetachedTbs::DetachedRaw } else { DetachedTbs::DetachedBase64 };
		Ok((tbs, hrng, 0..0, k))
	} else if raw {
		let mut tbs_header = String::with_capacity((4 * header.len() + 5) / 3);
		base64url_append(&mut tbs_header, header.as_bytes());
		let mut tbs = Vec::with_capacity(tbs_header.len() + 1 + payload.len());
		tbs.extend_from_slice(tbs_header.as_bytes());
		tbs.push(b'.');
		tbs.extend_from_slice(payload);
		jws_sign_tbs_tail_fulltbs(tbs_header, tbs)
	} else {
		//This is close enough to format the full tbs. Need to split hader afterwards.
		let (tbs, header, _, _) = jws_sign_tbs_tail_inline(header, payload)?;
		let tbs_header = tbs.get(header.clone()).unwrap_or("").to_string();
		jws_sign_tbs_tail_fulltbs(tbs_header, tbs.into_bytes())
	}
}

fn jws_sign_tbs_tail_fulltbs(header: String, tbs: Vec<u8>) -> 
	Result<(String,Range<usize>,Range<usize>,DetachedTbs), ()>
{
	let hrng = 0..header.len();
	Ok((header, hrng, 0..0, DetachedTbs::Detached(tbs)))
}

fn jws_sign_tbs_tail_inline(header: String, payload: &[u8]) ->
	Result<(String,Range<usize>,Range<usize>,DetachedTbs), ()>
{
	let mut tbs = String::with_capacity((4 * header.len() + 4 * payload.len() + 10) / 3);
	let hdrstart = 0;
	base64url_append(&mut tbs, header.as_bytes());
	let hdrend = tbs.len();
	tbs.push('.');	//.
	let pldstart = tbs.len();
	base64url_append(&mut tbs, payload);
	let pldend = tbs.len();
	Ok((tbs, hdrstart..hdrend, pldstart..pldend, DetachedTbs::Inline))
}



///Extract (public) JWK (JSON Web Key) from keypair.
///
///The [`KeyPair2`] to extract public key from is `keypair`
///
///On success, returns `Ok(jwk)`, where `jwk` is the JWK public key. Otherwise returns `Err(())`.
///
///Note that serializing the JWK obtained always gives JWK in the canonical form. One can calculate the JWK
///thumbprint directly from that value. Also note that some keypairs can have algorithms that are not supported in
///JOSE (JSON Object Signing and Encryption) at all. These keys will always fail to export.
///
///[`KeyPair2`]: trait.KeyPair2.html
pub fn extract_jwk3<K: KeyPair2>(keypair: &K) -> Result<JSON, ()>
{
	extract_jwk_tail(keypair.get_public_key2())
}

///Extract (public) JWK (JSON Web Key) from X.509 SPKI.
pub fn extract_jwk_x509spki(spki: &[u8]) -> Result<JSON, ()> { extract_jwk_tail(spki) }


pub(crate) fn extract_jwk_tail(pubkey: &[u8]) -> Result<JSON, ()>
{
	//The pubkey is in X.509 SPKI form.
	let algo = KeyIdentify2::identify(pubkey.deref());
	let keydata = extract_raw_key(pubkey.deref())?;
	//Hack: Wrap the algorithm in Some(), so we get non-exhaustive match we can throw a catch-all to.
	let r = match Some(algo) {
		Some(KeyIdentify2::RsaGeneric|KeyIdentify2::RsaPss) => {
			let mut keydata = Source::new(keydata);
			let mut keydata = dtry!(keydata.asn1(Asn1Tag::SEQUENCE));
			let rsa_n = dtry!(keydata.asn1_posint());
			let rsa_e = dtry!(keydata.asn1_posint());
			json_object!(kty => "RSA", n => rsa_n, e => rsa_e)
		},
		Some(KeyIdentify2::EcdsaP256) => make_jwk_ec(crate::ecdsa::EcdsaCurve::NsaP256, keydata)?,
		Some(KeyIdentify2::EcdsaP384) => make_jwk_ec(crate::ecdsa::EcdsaCurve::NsaP384, keydata)?,
		Some(KeyIdentify2::EcdsaP521) => make_jwk_ec(crate::ecdsa::EcdsaCurve::NsaP521, keydata)?,
		Some(KeyIdentify2::Ed25519) => make_jwk_okp("Ed25519", keydata),
		Some(KeyIdentify2::Ed448) => make_jwk_okp("Ed448", keydata),
		_ => return Err(())
	};
	Ok(r)
}

fn make_jwk_ec(crv: crate::ecdsa::EcdsaCurve, keydata: &[u8]) -> Result<JSON, ()>
{
	let _crv = crv.get_jwk_curve_name();
	let xlen = crv.get_component_bytes();
	fail_if!(keydata.len() < 1, ());
	let (_, x) = keydata.split_at(1);
	fail_if!(x.len() < xlen, ());
	let (x, y) = x.split_at(xlen);
	Ok(json_object!(kty => "EC", crv => _crv, x => x, y => y))
}

fn make_jwk_okp(ktype: &str, keydata: &[u8]) -> JSON
{
	json_object!(kty => "OKP", crv => ktype, x => keydata)
}
