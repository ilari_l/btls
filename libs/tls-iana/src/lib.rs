//!TLS IANA registery definitions.
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]
#[cfg(test)] #[macro_use] extern crate std;

pub use crate::ciphersuite::*;
use btls_aux_memory::Hexdump;
use btls_aux_serialization::Source;

mod ciphersuite;

macro_rules! deserialize_for_type
{
	($clazz:ident u8) => {
		impl $clazz {
			///Read item from source.
			pub fn read(src: &mut Source) -> Option<$clazz> { src.u8().map($clazz).ok() }
			///Read item from slice.
			pub fn read_s(src: &mut &[u8]) -> Result<$clazz, (usize, usize)>
			{
				read_s_8(src).map($clazz).ok_or((src.len(),1))
			}
		}
	};
	($clazz:ident u16) => {
		impl $clazz {
			///Read item from source.
			pub fn read(src: &mut Source) -> Option<$clazz> { src.u16().map($clazz).ok() }
			///Read item from slice.
			pub fn read_s(src: &mut &[u8]) -> Result<$clazz, (usize, usize)>
			{
				read_s_16(src).map($clazz).ok_or((src.len(),2))
			}
		}
	};
}

macro_rules! codepoint_for_type
{
	($clazz:ident $inner:ident $kind:expr, $flag:expr) => {
		deserialize_for_type!($clazz $inner);
		impl core::fmt::Debug for $clazz
		{
			fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
			{
				let kind = $kind;
				write!(f, "{kind}:")?;
				if let Some(grease) = self.__grease() {
					write!(f, "<GREASE_{grease:X}>")
				} else if let Some(name) = self.get_name() {
					write!(f, "{name}")
				} else {
					f.write_str("<?")?;
					Codepoint::__numeric_form(*self, f)?;
					f.write_str(">")
				}
			}
		}
		impl core::fmt::Display for $clazz
		{
			fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
			{
				core::fmt::Display::fmt(&self.display(), f)
			}
		}
		impl $clazz
		{
			///Get kind.
			pub const fn kind() -> &'static str { $kind }
			///Construct a value.
			pub const fn new(x: $inner) -> $clazz { $clazz(x) }
			///Construct a value by name, if possible.
			pub fn new_name(name: &str) -> Option<Self> { Self::_by_name(name) }
			///Get the raw value of the codepoint.
			pub const fn get(self) -> $inner { self.0 }
			///Get the name.
			pub fn get_name(self) -> Option<&'static str> { self._get_name() }
			///Is registered codepoint?
			pub fn is_registered(self) -> bool { self._get_name().is_some() }
			///Is private-use codepoint?
			pub fn is_private_use(self) -> bool { self._is_private_use() }
			///Is GREASE codepoint?
			pub fn is_grease(self) -> bool { self._is_grease() }
			///Print the name of the codepoint.
			pub fn display(self) -> CodepointPrinter<$clazz> { CodepointPrinter(self) }
		}
		impl MakeCodepoint for $clazz
		{
			type Inner2 = $inner;
			fn __new(v: $inner) -> Self { Self(v) }
		}
		impl Codepoint for $clazz
		{
			type Inner = $inner;
			fn __grease(self) -> Option<u8>
			{
				if self.is_grease() { Some(self.0 as u8 / 16 % 16) } else { None }
			}
			fn __get(self) -> $inner { self.0 }
			fn __get_name(self) -> Option<&'static str> { self._get_name() }
			fn __numeric_form(self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
			{
				let val = self.0;
				if $flag { write!(f, "{val:04x}h") } else { write!(f, "{val}") }
			}
		}
	}
}

/*******************************************************************************************************************/
///TLS Cipher Suite
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct CipherSuite(pub(crate) u16);
codepoint_for_type!(CipherSuite u16 "cipher_suite", true);

///TLS Client Certificate Type
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct ClientCertificateType(u8);
codepoint_for_type!(ClientCertificateType u8 "client_ceritificate_type", false);

///TLS Content Type
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct ContentType(u8);
codepoint_for_type!(ContentType u8 "content_type", false);

///TLS Alert
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct Alert(u8);
codepoint_for_type!(Alert u8 "alert", false);

impl Alert
{
	///Is close-type alert?
	///
	///Close-type alerts are not assumed to be instantly fatal to the connection.
	pub fn is_close(self) -> bool { self == Alert::CLOSE_NOTIFY || self == Alert::USER_CANCELED }
}

///TLS Handshake Type
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct HandshakeType(u8);
codepoint_for_type!(HandshakeType u8 "handshake_type", false);

///TLS Named Group
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct NamedGroup(u16);
codepoint_for_type!(NamedGroup u16 "supported_group", false);

///TLS EC point format.
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct EcPointFormat(u8);
codepoint_for_type!(EcPointFormat u8 "ec_point_format", false);

///TLS EC curve type.
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct EcCurveType(u8);
codepoint_for_type!(EcCurveType u8 "ec_curve_type", false);

impl EcCurveType
{
	///Is explicit curve?
	pub fn is_explicit(self) -> bool
	{
		self == EcCurveType::EXPLICIT_PRIME || self == EcCurveType::EXPLICIT_CHAR2
	}
}

///TLS Supplemental Data Format
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct SupplementalDataFormat(u16);
codepoint_for_type!(SupplementalDataFormat u16 "supplemental_data_format", false);

///TLS User Mapping Type
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct UserMappingType(u8);
codepoint_for_type!(UserMappingType u8 "user_mapping_type", false);

///TLS Signature Algorithm.
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct SignatureAlgorithm(u8);
codepoint_for_type!(SignatureAlgorithm u8 "signature_algorithm", false);

///TLS Hash Algorithm.
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct HashAlgorithm(u8);
codepoint_for_type!(HashAlgorithm u8 "hash_algorithm", false);

///TLS Sequence Number Encryption Algorithm.
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct SequenceNumberEncryptionAlgorithm(u16);
codepoint_for_type!(SequenceNumberEncryptionAlgorithm u16 "sequence_number_encryption_algorithm", false);

///TLS exporter label.
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct ExporterLabel(&'static str);

impl ExporterLabel
{
	///Form from string.
	pub const fn new(x: &'static str) -> ExporterLabel { ExporterLabel(x) }
	///Get raw label string.
	pub const fn get(self) -> &'static str { self.0 }
	#[deprecated(since="1.0.220601", note="Use CLIENT_AUTHENTICATOR_HANDSHAKE_CONTEXT")]
	pub const EXPORTER_CLIENT_AUTH_HSCTX: ExporterLabel =
		ExporterLabel("EXPORTER-client authenticator handshake context");
	#[deprecated(since="1.0.220601", note="Use SERVER_AUTHENTICATOR_HANDSHAKE_CONTEXT")]
	pub const EXPORTER_SERVER_AUTH_HSCTX: ExporterLabel =
		ExporterLabel("EXPORTER-server authenticator handshake context");
	#[deprecated(since="1.0.220601", note="Use CLIENT_AUTHENTICATOR_FINISHED_KEY")]
	pub const EXPORTER_CLIENT_AUTH_FKEY: ExporterLabel =
		ExporterLabel("EXPORTER-client authenticator finished key");
	#[deprecated(since="1.0.220601", note="Use SERVER_AUTHENTICATOR_FINISHED_KEY")]
	pub const EXPORTER_SERVER_AUTH_FKEY: ExporterLabel =
		ExporterLabel("EXPORTER-server authenticator finished key");
}

///TLS Authorization Data Format
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct AuthorizationDataFormat(u8);
codepoint_for_type!(AuthorizationDataFormat u8 "authorization_data_format", false);

///TLS Heartbeat Message Type.
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct HeartbeatMessageType(u8);
codepoint_for_type!(HeartbeatMessageType u8 "heartbeat_message_type", false);

impl HeartbeatMessageType
{
	///Is request message?
	pub fn is_request(self) -> bool { self == HeartbeatMessageType::HEARTBEAT_REQUEST }
}

///TLS Heartbeat Mode.
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct HeartbeatMode(u8);
codepoint_for_type!(HeartbeatMode u8 "heartbeat_mode", false);

impl HeartbeatMode
{
	///Can the peer send?
	pub fn peer_may_send(self) -> bool { self == HeartbeatMode::PEER_ALLOWED_TO_SEND }
}

///TLS Signature Scheme.
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct SignatureScheme(u16);
codepoint_for_type!(SignatureScheme u16 "signature_scheme", true);

impl SignatureScheme
{
	///Does the split form exist?
	pub fn is_split_form(self) -> bool
	{
		let (h, s) = self.split();
		h.0 >= 1 && h.0 <= 6 && s.0 >= 1 && s.0 <= 3
	}
	///Split into split form.
	///
	///If `self.is_split_form() == false`, the result is meaningless.
	pub fn split(self) -> (HashAlgorithm, SignatureAlgorithm)
	{
		(HashAlgorithm((self.0 >> 8) as u8), SignatureAlgorithm((self.0 & 0xFF) as u8))
	}
}

///TLS PSK Key Exchange Mode.
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct PskKeyExchangeMode(u8);
codepoint_for_type!(PskKeyExchangeMode u8 "psk_key_exchange", false);

///TLS KDF Identifier.
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct KdfIdentifier(u16);
codepoint_for_type!(KdfIdentifier u16 "kdf", true);

/*******************************************************************************************************************/
///TLS Extension Type.
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct ExtensionType(u16);
codepoint_for_type!(ExtensionType u16 "extension_type", false);

const EXT_POS_CLIENT_HELLO: u16 = 1<<0;
const EXT_POS_CLIENT_HELLO_TLS12: u16 = 1<<1;
const EXT_POS_HELLO_RETRY_REQUEST: u16 = 1<<2;
const EXT_POS_SERVER_HELLO_TLS12: u16 = 1<<3;
const EXT_POS_SERVER_HELLO_TLS13: u16 = 1<<4;
const EXT_POS_ENCRYPTED_EXTENSIONS: u16 = 1<<5;
const EXT_POS_CERTIFICATE_REQUEST: u16 = 1<<6;
const EXT_POS_CERTIFICATE: u16 = 1<<7;
const EXT_POS_NEW_SESSION_TICKET: u16 = 1<<8;
const EXT_POS_CLIENT_CR: u16 = 1<<9;

impl ExtensionType
{
	fn allowed_positions(self) -> u16
	{
		const CH: u16 = EXT_POS_CLIENT_HELLO;
		const CH12: u16 = EXT_POS_CLIENT_HELLO_TLS12;
		const HRR: u16 = EXT_POS_HELLO_RETRY_REQUEST;
		const SH12: u16 = EXT_POS_SERVER_HELLO_TLS12;
		const SH: u16 = EXT_POS_SERVER_HELLO_TLS13;
		const EE: u16 = EXT_POS_ENCRYPTED_EXTENSIONS;
		//Any entry valid for certificate request is also valid for client certificate request.
		const CR: u16 = EXT_POS_CERTIFICATE_REQUEST|EXT_POS_CLIENT_CR;
		const CCR: u16 = EXT_POS_CLIENT_CR;
		const CT: u16 = EXT_POS_CERTIFICATE;
		const NST: u16 = EXT_POS_NEW_SESSION_TICKET;
		if self.is_grease() { return CH|CH12|CR|NST; }
		if self._is_reserved() { return CH|CH12; }
		match self {
			//server_name is special in that it is not valid for certificate request, but is valid
			//for client certificate request.
			ExtensionType::SERVER_NAME => CH|CH12|SH12|EE|CCR,
			ExtensionType::MAX_FRAGMENT_LENGTH => CH|CH12|SH12|EE,
			ExtensionType::CLIENT_CERTIFICATE_URL => CH12|SH12,
			ExtensionType::TRUSTED_CA_KEYS => CH12|SH12,
			ExtensionType::TRUNCATED_HMAC => CH12|SH12,
			ExtensionType::STATUS_REQUEST => CH|CH12|SH12|CR|CT,
			ExtensionType::USER_MAPPING => CH12|SH12,
			ExtensionType::CLIENT_AUTHZ => CH12|SH12,
			ExtensionType::SERVER_AUTHZ => CH12|SH12,
			ExtensionType::CERT_TYPE => CH12|SH12,
			ExtensionType::SUPPORTED_GROUPS => CH|CH12|EE,
			ExtensionType::EC_POINT_FORMATS => CH12|SH12,
			ExtensionType::SRP => CH12,
			ExtensionType::SIGNATURE_ALGORITHMS => CH|CH12|CR,
			ExtensionType::USE_SRTP => CH|CH12|SH12|EE,
			ExtensionType::HEARTBEAT => CH|CH12|SH12|EE,
			ExtensionType::APPLICATION_LAYER_PROTOCOL_NEGOTIATION => CH|CH12|SH12|EE,
			ExtensionType::STATUS_REQUEST_V2 => CH12|SH12,
			ExtensionType::SIGNED_CERTIFICATE_TIMESTAMP => CH|CH12|SH12|CR|CT,
			ExtensionType::CLIENT_CERTIFICATE_TYPE => CH|CH12|SH12|EE,
			ExtensionType::SERVER_CERTIFICATE_TYPE => CH|CH12|SH12|EE,
			ExtensionType::PADDING => CH|CH12,
			ExtensionType::ENCRYPT_THEN_MAC => CH12|SH12,
			ExtensionType::EXTENDED_MASTER_SECRET => CH12|SH12,
			ExtensionType::TOKEN_BINDING => CH12|SH12,
			ExtensionType::CACHED_INFO => CH12,
			ExtensionType::TLS_LTS => CH12|SH12,
			ExtensionType::COMPRESS_CERTIFICATE => CH|CR,
			ExtensionType::RECORD_SIZE_LIMIT => CH|CH12|SH12|EE,
			ExtensionType::PWD_PROTECT => CH|CH12,
			ExtensionType::PWD_CLEAR => CH|CH12,
			//The IANA data is not trustworthy about this.
			ExtensionType::PASSWORD_SALT => CH|CH12,
			ExtensionType::TICKET_PINNING => CH|EE,
			ExtensionType::TLS_CERT_WITH_EXTERN_PSK => CH|SH,
			ExtensionType::DELEGATED_CREDENTIALS => CH|CR|CT,
			ExtensionType::SESSION_TICKET => CH12|SH12,
			ExtensionType::TLMSP => CH12,
			ExtensionType::TLMSP_PROXYING => CH12,
			ExtensionType::TLMSP_DELEGATE => CH12,
			ExtensionType::SUPPORTED_EKT_CIPHERS => CH|CH12|SH12|EE,
			ExtensionType::PRE_SHARED_KEY => CH|SH,
			ExtensionType::EARLY_DATA => CH|EE|NST,
			ExtensionType::SUPPORTED_VERSIONS => CH|CH12|SH|HRR,
			ExtensionType::COOKIE => CH|HRR,
			ExtensionType::PSK_KEY_EXCHANGE_MODES => CH,
			ExtensionType::CERTIFICATE_AUTHORITIES => CH|CH12|CR,
			ExtensionType::OID_FILTERS => CR,
			ExtensionType::POST_HANDSHAKE_AUTH => CH,
			ExtensionType::SIGNATURE_ALGORITHMS_CERT => CH|CH12|CR,
			ExtensionType::KEY_SHARE => CH|SH|HRR,
			ExtensionType::TRANSPARENCY_INFO => CH|CH12|SH12|CR|CT,
			ExtensionType::CONNECTION_ID_OLD => CH|CH12|SH12|SH,
			ExtensionType::CONNECTION_ID => CH|CH12|SH12|SH,
			ExtensionType::EXTERNAL_ID_HASH => CH|CH12|SH12|EE,
			ExtensionType::EXTERNAL_SESSION_ID => CH|CH12|SH12|EE,
			ExtensionType::QUIC_TRANSPORT_PARAMETERS => CH|EE,
			ExtensionType::TICKET_REQUEST => CH|EE,
			ExtensionType::DNSSEC_CHAIN => CH|CH12|SH12|CT,
			ExtensionType::SEQUENCE_NUMBER_ENCRYPTION_ALGORITHMS => CH|SH,
			ExtensionType::RENEGOTIATION_INFO => CH12|SH12,
			_ => CH|CH12|HRR|SH|SH12|EE|CR|CT|NST
		}
	}
	///Is allowed in TLS 1.3 Client Hello?
	///
	///Note that Client Hello can be TLS 1.2 and TLS 1.3 dual version, and extensions of both need to be
	///allowed in that case.
	pub fn allowed_in_client_hello_tls13(self) -> bool
	{
		self.allowed_positions() & EXT_POS_CLIENT_HELLO != 0
	}
	///Is allowed in TLS 1.2 Client Hello?
	///
	///Note that Client Hello can be TLS 1.2 and TLS 1.3 dual version, and extensions of both need to be
	///allowed in that case.
	pub fn allowed_in_client_hello_tls12(self) -> bool
	{
		self.allowed_positions() & EXT_POS_CLIENT_HELLO_TLS12 != 0
	}
	///Is allowed in Hello Retry Request?
	pub fn allowed_in_hello_retry_request(self) -> bool
	{
		self.allowed_positions() & EXT_POS_HELLO_RETRY_REQUEST != 0
	}
	///Is allowed in TLS 1.3 Server Hello?
	pub fn allowed_in_server_hello_tls13(self) -> bool
	{
		self.allowed_positions() & EXT_POS_SERVER_HELLO_TLS13 != 0
	}
	///Is allowed in TLS 1.2 Server Hello?
	pub fn allowed_in_server_hello_tls12(self) -> bool
	{
		self.allowed_positions() & EXT_POS_SERVER_HELLO_TLS12 != 0
	}
	///Is allowed in Encrypted Extensions?
	pub fn allowed_in_encrypted_extensions(self) -> bool
	{
		self.allowed_positions() & EXT_POS_ENCRYPTED_EXTENSIONS != 0
	}
	///Is allowed in Certificate Request?
	pub fn allowed_in_certificate_request(self) -> bool
	{
		self.allowed_positions() & EXT_POS_CERTIFICATE_REQUEST != 0
	}
	///Is allowed in Certificate?
	pub fn allowed_in_certificate(self) -> bool
	{
		self.allowed_positions() & EXT_POS_CERTIFICATE != 0
	}
	///Is allowed in New Session Ticket?
	pub fn allowed_in_new_session_ticket(self) -> bool
	{
		self.allowed_positions() & EXT_POS_NEW_SESSION_TICKET != 0
	}
	///Is allowed in Client certificate request?
	pub fn allowed_in_client_certificate_request(self) -> bool
	{
		self.allowed_positions() & EXT_POS_CLIENT_CR != 0
	}
	///Is TLS 1.3 volatile extension?
	///
	///Note that all exensions in HRR should be taken as volatile.
	pub fn is_tls13_volatile(self) -> bool
	{
		match self {
			ExtensionType::KEY_SHARE => true,
			ExtensionType::EARLY_DATA => true,
			ExtensionType::COOKIE => true,
			ExtensionType::PRE_SHARED_KEY => true,
			ExtensionType::PADDING => true,
			//Any other extension is NOT volatile, even if unknown!
			_ => false,
		}
	}
	//This is messy.
	fn _is_reserved(self) -> bool
	{
		//GREASE and private use codepoints are not taken to be reserved. These are the exceptional
		//reservations.
		self.0 == 40 || self.0 == 46
	}
}

///TLS Certificate Type
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct CertificateType(u8);
codepoint_for_type!(CertificateType u8 "certificate_type", false);

///TLS Certificate Status Type.
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct CertificateStatusType(u8);
codepoint_for_type!(CertificateStatusType u8 "certificate_status_type", false);

///TLS ALPN Protocol ID.
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct AlpnProtocolId<'a>(&'a [u8]);

impl<'a> PartialEq<str> for AlpnProtocolId<'a>
{
	fn eq(&self, x: &str) -> bool { self.0 == x.as_bytes() }
}

impl<'a> AlpnProtocolId<'a>
{
	///Wrap ALPN in wrapper.
	pub const fn new(x: &'a [u8]) -> AlpnProtocolId<'a> { AlpnProtocolId(x) }
	///Get raw label string.
	pub const fn get(self) -> &'a [u8] { self.0 }
	///Get string representation of ALPN Protocol ID.
	///
	///If given GREASE codepoint, or codepoint that is not valid UTF-8, returns `""`.
	pub fn get_string(self) -> &'a str
	{
		if self.is_grease() { return ""; }
		core::str::from_utf8(self.0).unwrap_or("")
	}
	///Is GREASE?
	pub fn is_grease(self) -> bool
	{
		self.0.len() == 2 && self.0[0] == self.0[1] && self.0[0] & 15 == 10
	}
	///Get the standard mask bit.
	pub fn get_standard(proto: &[u8]) -> Option<(AlpnProtocolId<'static>, usize)>
	{
		let pos = STANDARD_ALPN_SORTED.binary_search_by(|s|s.get().cmp(proto)).ok()?;
		let entry = STANDARD_ALPN_SORTED.get(pos)?;
		Some((*entry, pos))
	}
	///Show the name of the protocol.
	pub fn display(self) -> AlpnProtocolIdPrinter<'a> { AlpnProtocolIdPrinter(self.0) }
	///Get standard APLN entry by its bit index.
	pub fn standard_by_index(index: usize) -> Option<AlpnProtocolId<'static>>
	{
		STANDARD_ALPN_SORTED.get(index).map(|x|*x)
	}
}

impl<'a> core::fmt::Display for AlpnProtocolId<'a>
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		core::fmt::Display::fmt(&self.display(), f)
	}
}

unsafe impl<'a> btls_aux_memory::TrustedInvariantLength for AlpnProtocolId<'a>
{
	fn as_byte_slice(&self) -> &[u8] { self.get() }
}

#[test]
fn alpn_sorted()
{
	for x in STANDARD_ALPN_SORTED.windows(2) {
		let first = x[0];
		let second = x[1];
		println!("'{first}' < '{second}'");
		assert!(first.0 < second.0);
	}
}

#[test]
fn alpn_standard()
{
	for (i, &x) in STANDARD_ALPN_SORTED.iter().enumerate() {
		assert!(AlpnProtocolId::standard_by_index(i) == Some(x));
		let (x2, i2) = AlpnProtocolId::get_standard(x.get()).unwrap();
		assert!(x == x2);
		assert!(i == i2);
	}
}

#[derive(Copy,Clone,Debug)]
pub struct AlpnMaskBits([u64;ALPN_MASK_WORDS]);

impl AlpnMaskBits
{
	pub fn new() -> AlpnMaskBits { AlpnMaskBits([0;ALPN_MASK_WORDS]) }
	fn __wmask(bit: usize) -> (usize, u64) { (bit / 64, 1 << bit as u32 % 64) }
	pub fn add(&mut self, bit: usize)
	{
		let (w, b) = Self::__wmask(bit);
		if w < ALPN_MASK_WORDS { self.0[w] |= b; }
	}
	pub fn is_set(&self, bit: usize) -> bool
	{
		let (w, b) = Self::__wmask(bit);
		if w < ALPN_MASK_WORDS { self.0[w] & b != 0 } else { false }
	}
}

impl<'a> core::fmt::Debug for AlpnProtocolId<'a>
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		let alpn = self.0;
		let pfx = "alpn_protocol_id";
		if let Some(grease) = alpn_grease(alpn) {
			write!(f, "{pfx}:<GREASE_{grease:X}>")
		} else if let Some(alpn) = printable_alpn(alpn) {
			write!(f, "{pfx}:{alpn}")
		} else {
			write!(f, "{pfx}:<{data}>", data=Hexdump(self.0))
		}
	}
}

///TLS Cached Information Type
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct CachedInformationType(u8);
codepoint_for_type!(CachedInformationType u8 "cached_information_type", false);

///TLS Certificate Compression Algorithm ID
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct CertificateCompressionAlgorithmId(u16);
codepoint_for_type!(CertificateCompressionAlgorithmId u16 "certificate_compression_algorithm_id", false);

/*******************************************************************************************************************/
///TLS Compression Method
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct CompressionMethod(u8);
codepoint_for_type!(CompressionMethod u8 "compression_method", false);

///RTP EKT Cipher
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct EktCipher(u8);
codepoint_for_type!(EktCipher u8 "ekt_cipher", false);

///DTLS-SRTP Protection Profile
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct DtlsSrtpProtectionProfile(u16);
codepoint_for_type!(DtlsSrtpProtectionProfile u16 "dtls_srtp_protection_profile", true);

///Token Binding Key Parameters.
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct TokenBindingKeyParameter(u8);
codepoint_for_type!(TokenBindingKeyParameter u8 "token_binding_key_parameter", false);

///TLS Server Name Type.
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct ServerNameType(u8);
codepoint_for_type!(ServerNameType u8 "server_name_type", false);

///TLS Connection ID Usage.
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct ConnectionIdUsage(u8);
codepoint_for_type!(ConnectionIdUsage u8 "connection_id_usage", false);

///TLS Certificate Chain Type.
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct CertChainType(u8);
codepoint_for_type!(CertChainType u8 "cert_chain_type", false);

///TLS Version
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct TlsVersion(u16);
deserialize_for_type!(TlsVersion u16);

//TLS version is special.
impl TlsVersion
{
	///Get kind.
	pub const fn kind() -> &'static str { "tls_version" }
	///Construct a value.
	pub const fn new(x: u16) -> TlsVersion { TlsVersion(x) }
	///Get the raw value of the codepoint.
	pub const fn get(self) -> u16 { self.0 }
	///Print the name of the codepoint.
	pub fn display(self) -> TlsVersionPrinter { TlsVersionPrinter(self.0) }
	///Is grease?
	pub fn is_grease(self) -> bool { self.0 % 4112 == 2570 }
}

impl core::fmt::Display for TlsVersion
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		core::fmt::Display::fmt(&self.display(), f)
	}
}

impl MakeCodepoint for TlsVersion
{
	type Inner2 = u16;
	fn __new(v: u16) -> TlsVersion { TlsVersion(v) }
}

impl core::fmt::Debug for TlsVersion
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		write!(f, "tls_version:{val}", val=TlsVersionPrinter(self.0))
	}
}

/*******************************************************************************************************************/
use crate::helper::*;
///Various helper types.
///
///These types are mostly incidental types needed to implement functionality, these do not describe any TLS
///codepoint.
pub mod helper
{
	use btls_aux_memory::Hexdump;
	use btls_aux_memory::split_at;
	use core::convert::TryInto;
	use core::hash::Hash;

	///Print a TLS Version.
	pub struct TlsVersionPrinter(pub(crate) u16);

	impl core::fmt::Display for TlsVersionPrinter
	{
		fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
		{
			if self.0 % 4112 == 2570 {
				return write!(f, "<GREASE({num})>", num=self.0/16%16);
			}
			match self.0 {
				0x300 => f.write_str("SSLv3.0"),
				x@0x301..=0x3ff => write!(f, "TLSv1.{y}", y=x%256-1),
				x@0x7f00..=0x7fff => write!(f, "TLSv1.3-draft{y}", y=x%256),
				x => write!(f, "<unknown{x:04x}h>")
			}
		}
	}

	pub(crate) fn alpn_grease(x: &[u8]) -> Option<u8>
	{
		if x.len() == 2 && x[0] == x[1] && x[0] % 15 == 10 { Some(x[0] / 16 % 16) } else { None }
	}

	pub(crate) fn printable_alpn<'a>(x: &'a [u8]) -> Option<&'a str>
	{
		if !x.iter().all(|x|match x {
			//Allow alphanumerics, /, . and -.
			b'A'..=b'Z' => true,
			b'a'..=b'z' => true,
			b'0'..=b'9' => true,
			b'/'|b'.'|b'-' => true,
			_ => false
		}) { return None; }
		core::str::from_utf8(x).ok()
	}

	///Print an ALPN Protocol ID.
	pub struct AlpnProtocolIdPrinter<'a>(pub(crate) &'a [u8]);

	impl<'a> core::fmt::Display for AlpnProtocolIdPrinter<'a>
	{
		fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
		{
			let alpn = self.0;
			if let Some(grease) = alpn_grease(alpn) {
				write!(f, "<GREASE_{grease:X}>")
			} else if let Some(alpn) = printable_alpn(alpn) {
				write!(f, "{alpn}")
			} else {
				write!(f, "<{data}>", data=Hexdump(self.0))
			}
		}
	}

	pub(crate) fn read_s_8(src: &mut &[u8]) -> Option<u8>
	{
		let (head, tail) = split_at(*src, 1).ok()?;
		*src = tail;
		//head is guaranteed to be 1 byte.
		Some(u8::from_be_bytes(unsafe{head.as_ptr().cast::<[u8;1]>().read()}))
	}

	pub(crate) fn read_s_16(src: &mut &[u8]) -> Option<u16>
	{
		let (head, tail) = split_at(*src, 2).ok()?;
		*src = tail;
		//head is guaranteed to be 2 bytes.
		Some(u16::from_be_bytes(unsafe{head.as_ptr().cast::<[u8;2]>().read()}))
	}

	pub trait XRead: Sized
	{
		///Get size.
		fn size() -> usize;
		///Read.
		fn read(x: &mut &[u8]) -> Option<Self>;
		///Read unchecked.
		unsafe fn read_unchecked(x: &mut &[u8]) -> Self;
		///Cast internal value to usize.
		fn __cast_usize(self) -> usize;
	}

	macro_rules! impl_xread
	{
		($clazz:ident $size:expr) => {
			impl XRead for $clazz
			{
				#[inline(always)] fn size() -> usize { $size }
				#[inline(always)] fn read(x: &mut &[u8]) -> Option<$clazz>
				{
					if x.len() < $size { return None; }
					let (head, tail) = x.split_at($size);
					let b = head.try_into().ok()?;
					*x = tail;
					Some($clazz::from_be_bytes(b))
				}
				#[inline(always)] unsafe fn read_unchecked(x: &mut &[u8]) -> $clazz
				{
					//The alignment requirement for array is the same as the alignment requirement
					//for the base type, and base type is u8, so any alignment is ok.
					let b = unsafe{x.as_ptr().cast::<[u8;$size]>().read()};
					*x = unsafe{x.get_unchecked(b.len()..)};
					$clazz::from_be_bytes(b)
				}
				#[inline(always)] fn __cast_usize(self) -> usize { self as usize }
			}
			impl MakeCodepoint for $clazz
			{
				type Inner2 = $clazz;
				#[inline(always)] fn __new(v: $clazz) -> $clazz { v }
			}
		}
	}

	impl_xread!(u8 1);
	impl_xread!(u16 2);
	impl_xread!(u32 4);
	impl_xread!(u64 8);

	#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
	pub struct U24(u32);

	#[inline(always)] fn __read_u24(b: [u8;3]) -> U24
	{
		U24(b[0] as u32 * 65536 + b[1] as u32 * 256 + b[2] as u32)
	}

	impl XRead for U24
	{
		#[inline(always)] fn size() -> usize { 3 }
		#[inline(always)] fn read(x: &mut &[u8]) -> Option<U24>
		{
			let (head, tail) = split_at(*x, 3).ok()?;
			*x = tail;
			//head is guaranteed to be 3 bytes.
			Some(__read_u24(unsafe{head.as_ptr().cast::<[u8;3]>().read()}))
		}
		#[inline(always)] unsafe fn read_unchecked(x: &mut &[u8]) -> U24
		{
			let b = unsafe{x.as_ptr().cast::<[u8;3]>().read()};
			*x = unsafe{x.get_unchecked(b.len()..)};
			__read_u24(b)
		}
		#[inline(always)] fn __cast_usize(self) -> usize { self.0 as usize }
	}
	impl MakeCodepoint for U24
	{
		type Inner2 = U24;
		#[inline(always)] fn __new(v: U24) -> U24 { v }
	}
	

	///TLS codepoint type.
	pub trait MakeCodepoint: Copy+Clone+PartialEq+Eq+PartialOrd+Ord+Hash+Sized
	{
		///The inner type (usually u8 or u16).
		type Inner2: XRead+Copy+Sized;
		///Call `Self::new()`.
		///
		///This is trait method in order for it to be usable from generic contexts.
		fn __new(v: <Self as MakeCodepoint>::Inner2) -> Self;
	}

	///TLS codepoint type.
	pub trait Codepoint: Copy+Clone+PartialEq+Eq+PartialOrd+Ord+Hash+Sized+MakeCodepoint
	{
		///The inner type (usually u8 or u16).
		type Inner: Copy+Sized;
		///Take in a codepoint and if it is not a GREASE codepoint, return `None`.
		///
		///If the codepoint is in fact GREASE codepoint, return its number wrapped in `Some()`.
		///
		///This is used in printing GREASE codepoints.
		fn __grease(self) -> Option<u8>;
		///Call `self.get()`.
		///
		///This is trait method in order for it to be usable from generic contexts.
		fn __get(self) -> <Self as Codepoint>::Inner;
		///Call `self.get_name()`.
		///
		///This is trait method in order for it to be usable from generic contexts.
		fn __get_name(self) -> Option<&'static str>;
		///Print codepoint in its numeric form.
		fn __numeric_form(self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>;
	}

	///Print a codepoint.
	#[derive(Copy,Clone)]
	pub struct CodepointPrinter<T:Codepoint>(pub(crate) T);

	impl<T:Codepoint> core::fmt::Display for CodepointPrinter<T>
	{
		fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
		{
			if let Some(x) = Codepoint::__get_name(self.0) {
				f.write_str(x)
			} else if let Some(grease) = Codepoint::__grease(self.0) {
				write!(f, "<GREASE_{grease:X}>")
			} else {
				f.write_str("<?")?;
				Codepoint::__numeric_form(self.0, f)?;
				f.write_str(">")
			}
		}
	}
}

include!(concat!(env!("OUT_DIR"), "/autogenerated.inc.rs"));
