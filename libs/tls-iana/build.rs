use btls_aux_codegen::CodeBlock;
use btls_aux_codegen::mkid;
use btls_aux_codegen::mkidf;
use btls_aux_codegen::quote;
use btls_aux_codegen::qwrite;
use btls_aux_codegen::Symbol;
use std::env;
use std::collections::BTreeMap;
use std::fs::File;
use std::io::Write;
use std::path::Path;

struct Entry(i64, &'static str);

#[derive(PartialEq)]
enum PrivateUse
{
	None,
	L224,
	L248,
	L254,
	H255,
	NamedGroup,
	Extension,
}

struct ConstantSet
{
	ctype: &'static str,
	help: fn(&'static str, i64) -> String,
	name: fn(&'static str) -> String,
	uppercase: fn(&'static str) -> String,
	grease: bool,
	private: PrivateUse,
	entries: &'static [Entry],
}

fn null_uppercase(x: &str) -> String
{
	let mut out = String::new();
	for c in x.chars() {
		if c == '|' { break; }
		if c == '.' {
			out.push('_');	//Replace . by _.
		} else if c.is_lowercase() {
			for c in c.to_uppercase() { out.push(c); }
		} else {
			out.push(c);
		}
	}
	out
}

fn splith<'a>(x: &'a str) -> &'a str
{
	if let Some(i) = x.find("|") { &x[i+1..] } else { x }
}

macro_rules! stdhelp
{
	($name:ident $kind:expr) => {
		fn $name(name: &'static str, cp: i64) -> String
		{
			format!("{kind} #{cp}: {name}", kind=$kind, name=splith(name))
		}
	}
}

//fn null_help(_: &'static str, _: i64) -> String { String::new() }
fn null_name(x: &'static str) -> String { splith(x).to_string() }
fn cs_name(ident: &'static str) -> String { format!("TLS_{ident}") }
fn cs_help(ident: &'static str, cp: i64) -> String { format!("Cipher Suite 0x{cp:04x}: TLS_{ident}") }
fn sigsch_help(name: &'static str, cp: i64) -> String
{
	format!("Signature Scheme 0x{cp:04x}: {name}", name=splith(name))
}
stdhelp!(alert_help "Alert");
stdhelp!(adf_help "Authorization Data Format");
stdhelp!(cachedinfo_help "Cached Information Type");
stdhelp!(cca_help "Certificate Compression Algorithm Id");
stdhelp!(cm_help "Compression method");
stdhelp!(cst_help "Certificate Status Type");
stdhelp!(ctype_help "Certificate Type");
stdhelp!(cct_help "Client Certificate Type");
stdhelp!(cotype_help "Content Type");
stdhelp!(crvtype_help "EC Curve Type");
stdhelp!(ecptfmt_help "EC Point Format");
stdhelp!(usermap_help "User Mapping Type");
stdhelp!(sgrp_help "Supported Group");
stdhelp!(sdf_help "Supplemental Data Format");
stdhelp!(sigalgo_help "Signature Algorithm");
stdhelp!(hashalgo_help "Hash Algorithm");
stdhelp!(pskke_help "PSK Key Exchange Mode");
stdhelp!(hbmsg_help "Heartbeat Message Type");
stdhelp!(hbmode_help "Heartbeat Mode");
stdhelp!(hstype_help "Handshake Type");
stdhelp!(ektcipher_help "EKT Cipher Type");
stdhelp!(srtpprofile_help "DTLS-SRTP Protection Profile");
stdhelp!(tbkey_help "Token Binding Key Parameter");
stdhelp!(snametype_help "Server Name Type");
stdhelp!(conniduse_help "Connection ID Use");
stdhelp!(certchaint_help "Certificate Chain Type");
stdhelp!(ext_help "Extension Type");
stdhelp!(kdfid_help "KDF identifier");
stdhelp!(seqenc_help "Sequence Number Encryption Algorithm");

fn write_tls_versions(f: &mut impl Write)
{
	let mut tlsver_help: Vec<String> = Vec::new();
	let mut tlsver_sym: Vec<Symbol> = Vec::new();
	let mut tlsver_val: Vec<u16> = Vec::new();
	for i in 0..4 {
		tlsver_help.push(format!("TLS v1.{i}"));
		tlsver_sym.push(mkidf!("TLS_1_{i}"));
		tlsver_val.push(0x301+i);
	}
	for i in 18..29 {
		tlsver_help.push(format!("TLS v1.3-draft{i}"));
		tlsver_sym.push(mkidf!("TLS_1_3_DRAFT{i}"));
		tlsver_val.push(0x7f00+i);
	}
	qwrite(f, quote!{
impl TlsVersion
{
	///SSL v3.0
	pub const SSL_3_0: TlsVersion = TlsVersion(0x300);
	#(#[doc=#tlsver_help] pub const #tlsver_sym: TlsVersion = TlsVersion(#tlsver_val);)*
}

	});
}

fn write_constant_set(f: &mut impl Write, s: &ConstantSet)
{
	let ctype = mkid(s.ctype);
	let ctype_help: Vec<_> = s.entries.iter().map(|&Entry(id,sym)|{
		let h = (s.help)(sym, id);
		if h.len() > 0 { quote!(#[doc=#h]) } else { quote!() }
	}).collect();
	let ctype_consts: Vec<_> = s.entries.iter().map(|&Entry(_,sym)|(s.uppercase)(sym)).collect();
	let ctype_consts_sym: Vec<_> = s.entries.iter().map(|&Entry(_,sym)|mkid(&(s.uppercase)(sym))).collect();
	let ctype_id: Vec<_> = s.entries.iter().map(|&Entry(id,_)|id).collect();
	let ctype_name: Vec<_> = s.entries.iter().map(|&Entry(_,sym)|(s.name)(sym)).collect();
	let is_grease = if s.grease { quote!(self.0 % 4112 == 2570) } else { quote!(false) };
	let is_private_use = match s.private {
		PrivateUse::None => quote!(false),
		PrivateUse::L224 => quote!(self.0 >= 224),
		PrivateUse::L248 => quote!(self.0 >= 248),
		PrivateUse::L254 => quote!(self.0 >= 254),
		PrivateUse::H255 => quote!(self.0 >> 8 == 255),
		PrivateUse::NamedGroup => quote!(self.0 & 0xfffc == 0x01fc || self.0 >> 8 == 254),
		//There is a registered extension in the private use range!
		PrivateUse::Extension => quote!(self.0 >> 8 == 255 && self.0 != 0xFF01),
	};
	qwrite(f, quote!{
impl #ctype
{
	///All known constants
	pub const __CONSTANTS: &'static [&'static str] = &[#(#ctype_consts),*];
	fn _get_name(self) -> Option<&'static str>
	{
		match self.0 as i64 {
			#(#ctype_id => Some(#ctype_name),)*
			_ => None
		}
	}
	fn _is_grease(self) -> bool { #is_grease }
	fn _by_name(name: &str) -> Option<#ctype>
	{
		match name
		{
			#(#ctype_name => Some(#!REPEAT ctype(#ctype_id as _)),)*
			_ => None,
		}
	}
	fn _is_private_use(self) -> bool { #is_private_use }
	#(
		#ctype_help
		pub const #ctype_consts_sym: #!REPEAT ctype = #!REPEAT ctype(#ctype_id as _);
	)*
}

	});
}

static CLIENT_CERTIFICATE_TYPE: ConstantSet = ConstantSet {
	ctype: "ClientCertificateType",
	uppercase: null_uppercase,
	name: null_name,
	help: cct_help,
	grease: false,
	private: PrivateUse::L224,
	entries: &[
		Entry(1,"rsa_sign"),
		Entry(2,"dss_sign"),
		Entry(3,"rsa_fixed_dh"),
		Entry(4,"dss_fixed_dh"),
		Entry(5,"rsa_ephemeral_dh"),
		Entry(6,"dss_ephemeral_dh"),
		Entry(20,"fortezza_dms"),
		Entry(64,"ecdsa_sign"),
		Entry(65,"rsa_fixed_ecdh"),
		Entry(66,"ecdsa_fixed_ecdh"),
		Entry(67,"gost_sign256"),
		Entry(68,"gost_sign512"),
	],
};

static CONTENT_TYPE: ConstantSet = ConstantSet {
	ctype: "ContentType",
	uppercase: null_uppercase,
	name: null_name,
	help: cotype_help,
	grease: false,
	private: PrivateUse::None,
	entries: &[
		Entry(20,"change_cipher_spec"),
		Entry(21,"alert"),
		Entry(22,"handshake"),
		Entry(23,"application_data"),
		Entry(24,"heartbeat"),
		Entry(25,"tls12_cid"),
		Entry(26,"ack|ACK"),
	]
};

static CIPHER_SUITE: ConstantSet = ConstantSet {
	ctype: "CipherSuite",
	uppercase: null_uppercase,
	name: cs_name,
	help: cs_help,
	grease: true,
	private: PrivateUse::H255,
	entries: &[
		Entry(0x0000,"NULL_WITH_NULL_NULL"),
		Entry(0x0001,"RSA_WITH_NULL_MD5"),
		Entry(0x0002,"RSA_WITH_NULL_SHA"),
		Entry(0x0003,"RSA_EXPORT_WITH_RC4_40_MD5"),
		Entry(0x0004,"RSA_WITH_RC4_128_MD5"),
		Entry(0x0005,"RSA_WITH_RC4_128_SHA"),
		Entry(0x0006,"RSA_EXPORT_WITH_RC2_CBC_40_MD5"),
		Entry(0x0007,"RSA_WITH_IDEA_CBC_SHA"),
		Entry(0x0008,"RSA_EXPORT_WITH_DES40_CBC_SHA"),
		Entry(0x0009,"RSA_WITH_DES_CBC_SHA"),
		Entry(0x000A,"RSA_WITH_3DES_EDE_CBC_SHA"),
		Entry(0x000B,"DH_DSS_EXPORT_WITH_DES40_CBC_SHA"),
		Entry(0x000C,"DH_DSS_WITH_DES_CBC_SHA"),
		Entry(0x000D,"DH_DSS_WITH_3DES_EDE_CBC_SHA"),
		Entry(0x000E,"DH_RSA_EXPORT_WITH_DES40_CBC_SHA"),
		Entry(0x000F,"DH_RSA_WITH_DES_CBC_SHA"),
		Entry(0x0010,"DH_RSA_WITH_3DES_EDE_CBC_SHA"),
		Entry(0x0011,"DHE_DSS_EXPORT_WITH_DES40_CBC_SHA"),
		Entry(0x0012,"DHE_DSS_WITH_DES_CBC_SHA"),
		Entry(0x0013,"DHE_DSS_WITH_3DES_EDE_CBC_SHA"),
		Entry(0x0014,"DHE_RSA_EXPORT_WITH_DES40_CBC_SHA"),
		Entry(0x0015,"DHE_RSA_WITH_DES_CBC_SHA"),
		Entry(0x0016,"DHE_RSA_WITH_3DES_EDE_CBC_SHA"),
		Entry(0x0017,"DH_ANON_EXPORT_WITH_RC4_40_MD5"),
		Entry(0x0018,"DH_ANON_WITH_RC4_128_MD5"),
		Entry(0x0019,"DH_ANON_EXPORT_WITH_DES40_CBC_SHA"),
		Entry(0x001A,"DH_ANON_WITH_DES_CBC_SHA"),
		Entry(0x001B,"DH_ANON_WITH_3DES_EDE_CBC_SHA"),
		Entry(0x001E,"KRB5_WITH_DES_CBC_SHA"),
		Entry(0x001F,"KRB5_WITH_3DES_EDE_CBC_SHA"),
		Entry(0x0020,"KRB5_WITH_RC4_128_SHA"),
		Entry(0x0021,"KRB5_WITH_IDEA_CBC_SHA"),
		Entry(0x0022,"KRB5_WITH_DES_CBC_MD5"),
		Entry(0x0023,"KRB5_WITH_3DES_EDE_CBC_MD5"),
		Entry(0x0024,"KRB5_WITH_RC4_128_MD5"),
		Entry(0x0025,"KRB5_WITH_IDEA_CBC_MD5"),
		Entry(0x0026,"KRB5_EXPORT_WITH_DES_CBC_40_SHA"),
		Entry(0x0027,"KRB5_EXPORT_WITH_RC2_CBC_40_SHA"),
		Entry(0x0028,"KRB5_EXPORT_WITH_RC4_40_SHA"),
		Entry(0x0029,"KRB5_EXPORT_WITH_DES_CBC_40_MD5"),
		Entry(0x002A,"KRB5_EXPORT_WITH_RC2_CBC_40_MD5"),
		Entry(0x002B,"KRB5_EXPORT_WITH_RC4_40_MD5"),
		Entry(0x002C,"PSK_WITH_NULL_SHA"),
		Entry(0x002D,"DHE_PSK_WITH_NULL_SHA"),
		Entry(0x002E,"RSA_PSK_WITH_NULL_SHA"),
		Entry(0x002F,"RSA_WITH_AES_128_CBC_SHA"),
		Entry(0x0030,"DH_DSS_WITH_AES_128_CBC_SHA"),
		Entry(0x0031,"DH_RSA_WITH_AES_128_CBC_SHA"),
		Entry(0x0032,"DHE_DSS_WITH_AES_128_CBC_SHA"),
		Entry(0x0033,"DHE_RSA_WITH_AES_128_CBC_SHA"),
		Entry(0x0034,"DH_ANON_WITH_AES_128_CBC_SHA"),
		Entry(0x0035,"RSA_WITH_AES_256_CBC_SHA"),
		Entry(0x0036,"DH_DSS_WITH_AES_256_CBC_SHA"),
		Entry(0x0037,"DH_RSA_WITH_AES_256_CBC_SHA"),
		Entry(0x0038,"DHE_DSS_WITH_AES_256_CBC_SHA"),
		Entry(0x0039,"DHE_RSA_WITH_AES_256_CBC_SHA"),
		Entry(0x003A,"DH_ANON_WITH_AES_256_CBC_SHA"),
		Entry(0x003B,"RSA_WITH_NULL_SHA256"),
		Entry(0x003C,"RSA_WITH_AES_128_CBC_SHA256"),
		Entry(0x003D,"RSA_WITH_AES_256_CBC_SHA256"),
		Entry(0x003E,"DH_DSS_WITH_AES_128_CBC_SHA256"),
		Entry(0x003F,"DH_RSA_WITH_AES_128_CBC_SHA256"),
		Entry(0x0040,"DHE_DSS_WITH_AES_128_CBC_SHA256"),
		Entry(0x0041,"RSA_WITH_CAMELLIA_128_CBC_SHA"),
		Entry(0x0042,"DH_DSS_WITH_CAMELLIA_128_CBC_SHA"),
		Entry(0x0043,"DH_RSA_WITH_CAMELLIA_128_CBC_SHA"),
		Entry(0x0044,"DHE_DSS_WITH_CAMELLIA_128_CBC_SHA"),
		Entry(0x0045,"DHE_RSA_WITH_CAMELLIA_128_CBC_SHA"),
		Entry(0x0046,"DH_ANON_WITH_CAMELLIA_128_CBC_SHA"),
		Entry(0x0067,"DHE_RSA_WITH_AES_128_CBC_SHA256"),
		Entry(0x0068,"DH_DSS_WITH_AES_256_CBC_SHA256"),
		Entry(0x0069,"DH_RSA_WITH_AES_256_CBC_SHA256"),
		Entry(0x006A,"DHE_DSS_WITH_AES_256_CBC_SHA256"),
		Entry(0x006B,"DHE_RSA_WITH_AES_256_CBC_SHA256"),
		Entry(0x006C,"DH_ANON_WITH_AES_128_CBC_SHA256"),
		Entry(0x006D,"DH_ANON_WITH_AES_256_CBC_SHA256"),
		Entry(0x0084,"RSA_WITH_CAMELLIA_256_CBC_SHA"),
		Entry(0x0085,"DH_DSS_WITH_CAMELLIA_256_CBC_SHA"),
		Entry(0x0086,"DH_RSA_WITH_CAMELLIA_256_CBC_SHA"),
		Entry(0x0087,"DHE_DSS_WITH_CAMELLIA_256_CBC_SHA"),
		Entry(0x0088,"DHE_RSA_WITH_CAMELLIA_256_CBC_SHA"),
		Entry(0x0089,"DH_ANON_WITH_CAMELLIA_256_CBC_SHA"),
		Entry(0x008A,"PSK_WITH_RC4_128_SHA"),
		Entry(0x008B,"PSK_WITH_3DES_EDE_CBC_SHA"),
		Entry(0x008C,"PSK_WITH_AES_128_CBC_SHA"),
		Entry(0x008D,"PSK_WITH_AES_256_CBC_SHA"),
		Entry(0x008E,"DHE_PSK_WITH_RC4_128_SHA"),
		Entry(0x008F,"DHE_PSK_WITH_3DES_EDE_CBC_SHA"),
		Entry(0x0090,"DHE_PSK_WITH_AES_128_CBC_SHA"),
		Entry(0x0091,"DHE_PSK_WITH_AES_256_CBC_SHA"),
		Entry(0x0092,"RSA_PSK_WITH_RC4_128_SHA"),
		Entry(0x0093,"RSA_PSK_WITH_3DES_EDE_CBC_SHA"),
		Entry(0x0094,"RSA_PSK_WITH_AES_128_CBC_SHA"),
		Entry(0x0095,"RSA_PSK_WITH_AES_256_CBC_SHA"),
		Entry(0x0096,"RSA_WITH_SEED_CBC_SHA"),
		Entry(0x0097,"DH_DSS_WITH_SEED_CBC_SHA"),
		Entry(0x0098,"DH_RSA_WITH_SEED_CBC_SHA"),
		Entry(0x0099,"DHE_DSS_WITH_SEED_CBC_SHA"),
		Entry(0x009A,"DHE_RSA_WITH_SEED_CBC_SHA"),
		Entry(0x009B,"DH_ANON_WITH_SEED_CBC_SHA"),
		Entry(0x009C,"RSA_WITH_AES_128_GCM_SHA256"),
		Entry(0x009D,"RSA_WITH_AES_256_GCM_SHA384"),
		Entry(0x009E,"DHE_RSA_WITH_AES_128_GCM_SHA256"),
		Entry(0x009F,"DHE_RSA_WITH_AES_256_GCM_SHA384"),
		Entry(0x00A0,"DH_RSA_WITH_AES_128_GCM_SHA256"),
		Entry(0x00A1,"DH_RSA_WITH_AES_256_GCM_SHA384"),
		Entry(0x00A2,"DHE_DSS_WITH_AES_128_GCM_SHA256"),
		Entry(0x00A3,"DHE_DSS_WITH_AES_256_GCM_SHA384"),
		Entry(0x00A4,"DH_DSS_WITH_AES_128_GCM_SHA256"),
		Entry(0x00A5,"DH_DSS_WITH_AES_256_GCM_SHA384"),
		Entry(0x00A6,"DH_ANON_WITH_AES_128_GCM_SHA256"),
		Entry(0x00A7,"DH_ANON_WITH_AES_256_GCM_SHA384"),
		Entry(0x00A8,"PSK_WITH_AES_128_GCM_SHA256"),
		Entry(0x00A9,"PSK_WITH_AES_256_GCM_SHA384"),
		Entry(0x00AA,"DHE_PSK_WITH_AES_128_GCM_SHA256"),
		Entry(0x00AB,"DHE_PSK_WITH_AES_256_GCM_SHA384"),
		Entry(0x00AC,"RSA_PSK_WITH_AES_128_GCM_SHA256"),
		Entry(0x00AD,"RSA_PSK_WITH_AES_256_GCM_SHA384"),
		Entry(0x00AE,"PSK_WITH_AES_128_CBC_SHA256"),
		Entry(0x00AF,"PSK_WITH_AES_256_CBC_SHA384"),
		Entry(0x00B0,"PSK_WITH_NULL_SHA256"),
		Entry(0x00B1,"PSK_WITH_NULL_SHA384"),
		Entry(0x00B2,"DHE_PSK_WITH_AES_128_CBC_SHA256"),
		Entry(0x00B3,"DHE_PSK_WITH_AES_256_CBC_SHA384"),
		Entry(0x00B4,"DHE_PSK_WITH_NULL_SHA256"),
		Entry(0x00B5,"DHE_PSK_WITH_NULL_SHA384"),
		Entry(0x00B6,"RSA_PSK_WITH_AES_128_CBC_SHA256"),
		Entry(0x00B7,"RSA_PSK_WITH_AES_256_CBC_SHA384"),
		Entry(0x00B8,"RSA_PSK_WITH_NULL_SHA256"),
		Entry(0x00B9,"RSA_PSK_WITH_NULL_SHA384"),
		Entry(0x00BA,"RSA_WITH_CAMELLIA_128_CBC_SHA256"),
		Entry(0x00BB,"DH_DSS_WITH_CAMELLIA_128_CBC_SHA256"),
		Entry(0x00BC,"DH_RSA_WITH_CAMELLIA_128_CBC_SHA256"),
		Entry(0x00BD,"DHE_DSS_WITH_CAMELLIA_128_CBC_SHA256"),
		Entry(0x00BE,"DHE_RSA_WITH_CAMELLIA_128_CBC_SHA256"),
		Entry(0x00BF,"DH_ANON_WITH_CAMELLIA_128_CBC_SHA256"),
		Entry(0x00C0,"RSA_WITH_CAMELLIA_256_CBC_SHA256"),
		Entry(0x00C1,"DH_DSS_WITH_CAMELLIA_256_CBC_SHA256"),
		Entry(0x00C2,"DH_RSA_WITH_CAMELLIA_256_CBC_SHA256"),
		Entry(0x00C3,"DHE_DSS_WITH_CAMELLIA_256_CBC_SHA256"),
		Entry(0x00C4,"DHE_RSA_WITH_CAMELLIA_256_CBC_SHA256"),
		Entry(0x00C5,"DH_ANON_WITH_CAMELLIA_256_CBC_SHA256"),
		Entry(0x00C6,"SM4_GCM_SM3"),
		Entry(0x00C7,"SM4_CCM_SM3"),
		Entry(0x00FF,"EMPTY_RENEGOTIATION_INFO_SCSV"),
		Entry(0x1301,"AES_128_GCM_SHA256"),
		Entry(0x1302,"AES_256_GCM_SHA384"),
		Entry(0x1303,"CHACHA20_POLY1305_SHA256"),
		Entry(0x1304,"AES_128_CCM_SHA256"),
		Entry(0x1305,"AES_128_CCM_8_SHA256"),
		Entry(0x1306,"TLS_AEGIS_256_SHA384"),
		Entry(0x1307,"TLS_AEGIS_128L_SHA256"),
		Entry(0x5600,"FALLBACK_SCSV"),
		Entry(0xC001,"ECDH_ECDSA_WITH_NULL_SHA"),
		Entry(0xC002,"ECDH_ECDSA_WITH_RC4_128_SHA"),
		Entry(0xC003,"ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA"),
		Entry(0xC004,"ECDH_ECDSA_WITH_AES_128_CBC_SHA"),
		Entry(0xC005,"ECDH_ECDSA_WITH_AES_256_CBC_SHA"),
		Entry(0xC006,"ECDHE_ECDSA_WITH_NULL_SHA"),
		Entry(0xC007,"ECDHE_ECDSA_WITH_RC4_128_SHA"),
		Entry(0xC008,"ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA"),
		Entry(0xC009,"ECDHE_ECDSA_WITH_AES_128_CBC_SHA"),
		Entry(0xC00A,"ECDHE_ECDSA_WITH_AES_256_CBC_SHA"),
		Entry(0xC00B,"ECDH_RSA_WITH_NULL_SHA"),
		Entry(0xC00C,"ECDH_RSA_WITH_RC4_128_SHA"),
		Entry(0xC00D,"ECDH_RSA_WITH_3DES_EDE_CBC_SHA"),
		Entry(0xC00E,"ECDH_RSA_WITH_AES_128_CBC_SHA"),
		Entry(0xC00F,"ECDH_RSA_WITH_AES_256_CBC_SHA"),
		Entry(0xC010,"ECDHE_RSA_WITH_NULL_SHA"),
		Entry(0xC011,"ECDHE_RSA_WITH_RC4_128_SHA"),
		Entry(0xC012,"ECDHE_RSA_WITH_3DES_EDE_CBC_SHA"),
		Entry(0xC013,"ECDHE_RSA_WITH_AES_128_CBC_SHA"),
		Entry(0xC014,"ECDHE_RSA_WITH_AES_256_CBC_SHA"),
		Entry(0xC015,"ECDH_ANON_WITH_NULL_SHA"),
		Entry(0xC016,"ECDH_ANON_WITH_RC4_128_SHA"),
		Entry(0xC017,"ECDH_ANON_WITH_3DES_EDE_CBC_SHA"),
		Entry(0xC018,"ECDH_ANON_WITH_AES_128_CBC_SHA"),
		Entry(0xC019,"ECDH_ANON_WITH_AES_256_CBC_SHA"),
		Entry(0xC01A,"SRP_SHA_WITH_3DES_EDE_CBC_SHA"),
		Entry(0xC01B,"SRP_SHA_RSA_WITH_3DES_EDE_CBC_SHA"),
		Entry(0xC01C,"SRP_SHA_DSS_WITH_3DES_EDE_CBC_SHA"),
		Entry(0xC01D,"SRP_SHA_WITH_AES_128_CBC_SHA"),
		Entry(0xC01E,"SRP_SHA_RSA_WITH_AES_128_CBC_SHA"),
		Entry(0xC01F,"SRP_SHA_DSS_WITH_AES_128_CBC_SHA"),
		Entry(0xC020,"SRP_SHA_WITH_AES_256_CBC_SHA"),
		Entry(0xC021,"SRP_SHA_RSA_WITH_AES_256_CBC_SHA"),
		Entry(0xC022,"SRP_SHA_DSS_WITH_AES_256_CBC_SHA"),
		Entry(0xC023,"ECDHE_ECDSA_WITH_AES_128_CBC_SHA256"),
		Entry(0xC024,"ECDHE_ECDSA_WITH_AES_256_CBC_SHA384"),
		Entry(0xC025,"ECDH_ECDSA_WITH_AES_128_CBC_SHA256"),
		Entry(0xC026,"ECDH_ECDSA_WITH_AES_256_CBC_SHA384"),
		Entry(0xC027,"ECDHE_RSA_WITH_AES_128_CBC_SHA256"),
		Entry(0xC028,"ECDHE_RSA_WITH_AES_256_CBC_SHA384"),
		Entry(0xC029,"ECDH_RSA_WITH_AES_128_CBC_SHA256"),
		Entry(0xC02A,"ECDH_RSA_WITH_AES_256_CBC_SHA384"),
		Entry(0xC02B,"ECDHE_ECDSA_WITH_AES_128_GCM_SHA256"),
		Entry(0xC02C,"ECDHE_ECDSA_WITH_AES_256_GCM_SHA384"),
		Entry(0xC02D,"ECDH_ECDSA_WITH_AES_128_GCM_SHA256"),
		Entry(0xC02E,"ECDH_ECDSA_WITH_AES_256_GCM_SHA384"),
		Entry(0xC02F,"ECDHE_RSA_WITH_AES_128_GCM_SHA256"),
		Entry(0xC030,"ECDHE_RSA_WITH_AES_256_GCM_SHA384"),
		Entry(0xC031,"ECDH_RSA_WITH_AES_128_GCM_SHA256"),
		Entry(0xC032,"ECDH_RSA_WITH_AES_256_GCM_SHA384"),
		Entry(0xC033,"ECDHE_PSK_WITH_RC4_128_SHA"),
		Entry(0xC034,"ECDHE_PSK_WITH_3DES_EDE_CBC_SHA"),
		Entry(0xC035,"ECDHE_PSK_WITH_AES_128_CBC_SHA"),
		Entry(0xC036,"ECDHE_PSK_WITH_AES_256_CBC_SHA"),
		Entry(0xC037,"ECDHE_PSK_WITH_AES_128_CBC_SHA256"),
		Entry(0xC038,"ECDHE_PSK_WITH_AES_256_CBC_SHA384"),
		Entry(0xC039,"ECDHE_PSK_WITH_NULL_SHA"),
		Entry(0xC03A,"ECDHE_PSK_WITH_NULL_SHA256"),
		Entry(0xC03B,"ECDHE_PSK_WITH_NULL_SHA384"),
		Entry(0xC03C,"RSA_WITH_ARIA_128_CBC_SHA256"),
		Entry(0xC03D,"RSA_WITH_ARIA_256_CBC_SHA384"),
		Entry(0xC03E,"DH_DSS_WITH_ARIA_128_CBC_SHA256"),
		Entry(0xC03F,"DH_DSS_WITH_ARIA_256_CBC_SHA384"),
		Entry(0xC040,"DH_RSA_WITH_ARIA_128_CBC_SHA256"),
		Entry(0xC041,"DH_RSA_WITH_ARIA_256_CBC_SHA384"),
		Entry(0xC042,"DHE_DSS_WITH_ARIA_128_CBC_SHA256"),
		Entry(0xC043,"DHE_DSS_WITH_ARIA_256_CBC_SHA384"),
		Entry(0xC044,"DHE_RSA_WITH_ARIA_128_CBC_SHA256"),
		Entry(0xC045,"DHE_RSA_WITH_ARIA_256_CBC_SHA384"),
		Entry(0xC046,"DH_ANON_WITH_ARIA_128_CBC_SHA256"),
		Entry(0xC047,"DH_ANON_WITH_ARIA_256_CBC_SHA384"),
		Entry(0xC048,"ECDHE_ECDSA_WITH_ARIA_128_CBC_SHA256"),
		Entry(0xC049,"ECDHE_ECDSA_WITH_ARIA_256_CBC_SHA384"),
		Entry(0xC04A,"ECDH_ECDSA_WITH_ARIA_128_CBC_SHA256"),
		Entry(0xC04B,"ECDH_ECDSA_WITH_ARIA_256_CBC_SHA384"),
		Entry(0xC04C,"ECDHE_RSA_WITH_ARIA_128_CBC_SHA256"),
		Entry(0xC04D,"ECDHE_RSA_WITH_ARIA_256_CBC_SHA384"),
		Entry(0xC04E,"ECDH_RSA_WITH_ARIA_128_CBC_SHA256"),
		Entry(0xC04F,"ECDH_RSA_WITH_ARIA_256_CBC_SHA384"),
		Entry(0xC050,"RSA_WITH_ARIA_128_GCM_SHA256"),
		Entry(0xC051,"RSA_WITH_ARIA_256_GCM_SHA384"),
		Entry(0xC052,"DHE_RSA_WITH_ARIA_128_GCM_SHA256"),
		Entry(0xC053,"DHE_RSA_WITH_ARIA_256_GCM_SHA384"),
		Entry(0xC054,"DH_RSA_WITH_ARIA_128_GCM_SHA256"),
		Entry(0xC055,"DH_RSA_WITH_ARIA_256_GCM_SHA384"),
		Entry(0xC056,"DHE_DSS_WITH_ARIA_128_GCM_SHA256"),
		Entry(0xC057,"DHE_DSS_WITH_ARIA_256_GCM_SHA384"),
		Entry(0xC058,"DH_DSS_WITH_ARIA_128_GCM_SHA256"),
		Entry(0xC059,"DH_DSS_WITH_ARIA_256_GCM_SHA384"),
		Entry(0xC05A,"DH_ANON_WITH_ARIA_128_GCM_SHA256"),
		Entry(0xC05B,"DH_ANON_WITH_ARIA_256_GCM_SHA384"),
		Entry(0xC05C,"ECDHE_ECDSA_WITH_ARIA_128_GCM_SHA256"),
		Entry(0xC05D,"ECDHE_ECDSA_WITH_ARIA_256_GCM_SHA384"),
		Entry(0xC05E,"ECDH_ECDSA_WITH_ARIA_128_GCM_SHA256"),
		Entry(0xC05F,"ECDH_ECDSA_WITH_ARIA_256_GCM_SHA384"),
		Entry(0xC060,"ECDHE_RSA_WITH_ARIA_128_GCM_SHA256"),
		Entry(0xC061,"ECDHE_RSA_WITH_ARIA_256_GCM_SHA384"),
		Entry(0xC062,"ECDH_RSA_WITH_ARIA_128_GCM_SHA256"),
		Entry(0xC063,"ECDH_RSA_WITH_ARIA_256_GCM_SHA384"),
		Entry(0xC064,"PSK_WITH_ARIA_128_CBC_SHA256"),
		Entry(0xC065,"PSK_WITH_ARIA_256_CBC_SHA384"),
		Entry(0xC066,"DHE_PSK_WITH_ARIA_128_CBC_SHA256"),
		Entry(0xC067,"DHE_PSK_WITH_ARIA_256_CBC_SHA384"),
		Entry(0xC068,"RSA_PSK_WITH_ARIA_128_CBC_SHA256"),
		Entry(0xC069,"RSA_PSK_WITH_ARIA_256_CBC_SHA384"),
		Entry(0xC06A,"PSK_WITH_ARIA_128_GCM_SHA256"),
		Entry(0xC06B,"PSK_WITH_ARIA_256_GCM_SHA384"),
		Entry(0xC06C,"DHE_PSK_WITH_ARIA_128_GCM_SHA256"),
		Entry(0xC06D,"DHE_PSK_WITH_ARIA_256_GCM_SHA384"),
		Entry(0xC06E,"RSA_PSK_WITH_ARIA_128_GCM_SHA256"),
		Entry(0xC06F,"RSA_PSK_WITH_ARIA_256_GCM_SHA384"),
		Entry(0xC070,"ECDHE_PSK_WITH_ARIA_128_CBC_SHA256"),
		Entry(0xC071,"ECDHE_PSK_WITH_ARIA_256_CBC_SHA384"),
		Entry(0xC072,"ECDHE_ECDSA_WITH_CAMELLIA_128_CBC_SHA256"),
		Entry(0xC073,"ECDHE_ECDSA_WITH_CAMELLIA_256_CBC_SHA384"),
		Entry(0xC074,"ECDH_ECDSA_WITH_CAMELLIA_128_CBC_SHA256"),
		Entry(0xC075,"ECDH_ECDSA_WITH_CAMELLIA_256_CBC_SHA384"),
		Entry(0xC076,"ECDHE_RSA_WITH_CAMELLIA_128_CBC_SHA256"),
		Entry(0xC077,"ECDHE_RSA_WITH_CAMELLIA_256_CBC_SHA384"),
		Entry(0xC078,"ECDH_RSA_WITH_CAMELLIA_128_CBC_SHA256"),
		Entry(0xC079,"ECDH_RSA_WITH_CAMELLIA_256_CBC_SHA384"),
		Entry(0xC07A,"RSA_WITH_CAMELLIA_128_GCM_SHA256"),
		Entry(0xC07B,"RSA_WITH_CAMELLIA_256_GCM_SHA384"),
		Entry(0xC07C,"DHE_RSA_WITH_CAMELLIA_128_GCM_SHA256"),
		Entry(0xC07D,"DHE_RSA_WITH_CAMELLIA_256_GCM_SHA384"),
		Entry(0xC07E,"DH_RSA_WITH_CAMELLIA_128_GCM_SHA256"),
		Entry(0xC07F,"DH_RSA_WITH_CAMELLIA_256_GCM_SHA384"),
		Entry(0xC080,"DHE_DSS_WITH_CAMELLIA_128_GCM_SHA256"),
		Entry(0xC081,"DHE_DSS_WITH_CAMELLIA_256_GCM_SHA384"),
		Entry(0xC082,"DH_DSS_WITH_CAMELLIA_128_GCM_SHA256"),
		Entry(0xC083,"DH_DSS_WITH_CAMELLIA_256_GCM_SHA384"),
		Entry(0xC084,"DH_ANON_WITH_CAMELLIA_128_GCM_SHA256"),
		Entry(0xC085,"DH_ANON_WITH_CAMELLIA_256_GCM_SHA384"),
		Entry(0xC086,"ECDHE_ECDSA_WITH_CAMELLIA_128_GCM_SHA256"),
		Entry(0xC087,"ECDHE_ECDSA_WITH_CAMELLIA_256_GCM_SHA384"),
		Entry(0xC088,"ECDH_ECDSA_WITH_CAMELLIA_128_GCM_SHA256"),
		Entry(0xC089,"ECDH_ECDSA_WITH_CAMELLIA_256_GCM_SHA384"),
		Entry(0xC08A,"ECDHE_RSA_WITH_CAMELLIA_128_GCM_SHA256"),
		Entry(0xC08B,"ECDHE_RSA_WITH_CAMELLIA_256_GCM_SHA384"),
		Entry(0xC08C,"ECDH_RSA_WITH_CAMELLIA_128_GCM_SHA256"),
		Entry(0xC08D,"ECDH_RSA_WITH_CAMELLIA_256_GCM_SHA384"),
		Entry(0xC08E,"PSK_WITH_CAMELLIA_128_GCM_SHA256"),
		Entry(0xC08F,"PSK_WITH_CAMELLIA_256_GCM_SHA384"),
		Entry(0xC090,"DHE_PSK_WITH_CAMELLIA_128_GCM_SHA256"),
		Entry(0xC091,"DHE_PSK_WITH_CAMELLIA_256_GCM_SHA384"),
		Entry(0xC092,"RSA_PSK_WITH_CAMELLIA_128_GCM_SHA256"),
		Entry(0xC093,"RSA_PSK_WITH_CAMELLIA_256_GCM_SHA384"),
		Entry(0xC094,"PSK_WITH_CAMELLIA_128_CBC_SHA256"),
		Entry(0xC095,"PSK_WITH_CAMELLIA_256_CBC_SHA384"),
		Entry(0xC096,"DHE_PSK_WITH_CAMELLIA_128_CBC_SHA256"),
		Entry(0xC097,"DHE_PSK_WITH_CAMELLIA_256_CBC_SHA384"),
		Entry(0xC098,"RSA_PSK_WITH_CAMELLIA_128_CBC_SHA256"),
		Entry(0xC099,"RSA_PSK_WITH_CAMELLIA_256_CBC_SHA384"),
		Entry(0xC09A,"ECDHE_PSK_WITH_CAMELLIA_128_CBC_SHA256"),
		Entry(0xC09B,"ECDHE_PSK_WITH_CAMELLIA_256_CBC_SHA384"),
		Entry(0xC09C,"RSA_WITH_AES_128_CCM"),
		Entry(0xC09D,"RSA_WITH_AES_256_CCM"),
		Entry(0xC09E,"DHE_RSA_WITH_AES_128_CCM"),
		Entry(0xC09F,"DHE_RSA_WITH_AES_256_CCM"),
		Entry(0xC0A0,"RSA_WITH_AES_128_CCM_8"),
		Entry(0xC0A1,"RSA_WITH_AES_256_CCM_8"),
		Entry(0xC0A2,"DHE_RSA_WITH_AES_128_CCM_8"),
		Entry(0xC0A3,"DHE_RSA_WITH_AES_256_CCM_8"),
		Entry(0xC0A4,"PSK_WITH_AES_128_CCM"),
		Entry(0xC0A5,"PSK_WITH_AES_256_CCM"),
		Entry(0xC0A6,"DHE_PSK_WITH_AES_128_CCM"),
		Entry(0xC0A7,"DHE_PSK_WITH_AES_256_CCM"),
		Entry(0xC0A8,"PSK_WITH_AES_128_CCM_8"),
		Entry(0xC0A9,"PSK_WITH_AES_256_CCM_8"),
		Entry(0xC0AA,"PSK_DHE_WITH_AES_128_CCM_8"),
		Entry(0xC0AB,"PSK_DHE_WITH_AES_256_CCM_8"),
		Entry(0xC0AC,"ECDHE_ECDSA_WITH_AES_128_CCM"),
		Entry(0xC0AD,"ECDHE_ECDSA_WITH_AES_256_CCM"),
		Entry(0xC0AE,"ECDHE_ECDSA_WITH_AES_128_CCM_8"),
		Entry(0xC0AF,"ECDHE_ECDSA_WITH_AES_256_CCM_8"),
		Entry(0xC0B0,"ECCPWD_WITH_AES_128_GCM_SHA256"),
		Entry(0xC0B1,"ECCPWD_WITH_AES_256_GCM_SHA384"),
		Entry(0xC0B2,"ECCPWD_WITH_AES_128_CCM_SHA256"),
		Entry(0xC0B3,"ECCPWD_WITH_AES_256_CCM_SHA384"),
		Entry(0xC0B4,"SHA256_SHA256"),
		Entry(0xC0B5,"SHA384_SHA384"),
		Entry(0xC100,"GOSTR341112_256_WITH_KUZNYECHIK_CTR_OMAC"),
		Entry(0xC101,"GOSTR341112_256_WITH_MAGMA_CTR_OMAC"),
		Entry(0xC102,"GOSTR341112_256_WITH_28147_CNT_IMIT"),
		Entry(0xC103,"GOSTR341112_256_WITH_KUZNYECHIK_MGM_L"),
		Entry(0xC104,"GOSTR341112_256_WITH_MAGMA_MGM_L"),
		Entry(0xC105,"GOSTR341112_256_WITH_KUZNYECHIK_MGM_S"),
		Entry(0xC106,"GOSTR341112_256_WITH_MAGMA_MGM_S"),
		Entry(0xCCA8,"ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256"),
		Entry(0xCCA9,"ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256"),
		Entry(0xCCAA,"DHE_RSA_WITH_CHACHA20_POLY1305_SHA256"),
		Entry(0xCCAB,"PSK_WITH_CHACHA20_POLY1305_SHA256"),
		Entry(0xCCAC,"ECDHE_PSK_WITH_CHACHA20_POLY1305_SHA256"),
		Entry(0xCCAD,"DHE_PSK_WITH_CHACHA20_POLY1305_SHA256"),
		Entry(0xCCAE,"RSA_PSK_WITH_CHACHA20_POLY1305_SHA256"),
		Entry(0xD001,"ECDHE_PSK_WITH_AES_128_GCM_SHA256"),
		Entry(0xD002,"ECDHE_PSK_WITH_AES_256_GCM_SHA384"),
		Entry(0xD003,"ECDHE_PSK_WITH_AES_128_CCM_8_SHA256"),
		Entry(0xD005,"ECDHE_PSK_WITH_AES_128_CCM_SHA256"),
	],
};

static ALERT: ConstantSet = ConstantSet {
	ctype: "Alert",
	uppercase: null_uppercase,
	name: null_name,
	help: alert_help,
	grease: false,
	private: PrivateUse::None,
	entries: &[
		Entry(0,"close_notify"),
		Entry(10,"unexpected_message"),
		Entry(20,"bad_record_mac"),
		Entry(21,"decryption_failed"),
		Entry(22,"record_overflow"),
		Entry(30,"decompression_failure"),
		Entry(40,"handshake_failure"),
		Entry(41,"no_certificate"),
		Entry(42,"bad_certificate"),
		Entry(43,"unsupported_certificate"),
		Entry(44,"certificate_revoked"),
		Entry(45,"certificate_expired"),
		Entry(46,"certificate_unknown"),
		Entry(47,"illegal_parameter"),
		Entry(48,"unknown_ca"),
		Entry(49,"access_denied"),
		Entry(50,"decode_error"),
		Entry(51,"decrypt_error"),
		Entry(52,"too_many_cids_requested"),
		Entry(60,"export_restriction"),
		Entry(70,"protocol_version"),
		Entry(71,"insufficient_security"),
		Entry(80,"internal_error"),
		Entry(86,"inappropriate_fallback"),
		Entry(90,"user_canceled"),
		Entry(100,"no_renegotiation"),
		Entry(109,"missing_extension"),
		Entry(110,"unsupported_extension"),
		Entry(111,"certificate_unobtainable"),
		Entry(112,"unrecognized_name"),
		Entry(113,"bad_certificate_status_response"),
		Entry(114,"bad_certificate_hash_value"),
		Entry(115,"unknown_psk_identity"),
		Entry(116,"certificate_required"),
		Entry(120,"no_application_protocol"),
	],
};

static HANDSHAKE_TYPE: ConstantSet = ConstantSet {
	ctype: "HandshakeType",
	uppercase: null_uppercase,
	name: null_name,
	help: hstype_help,
	grease: false,
	private: PrivateUse::None,
	entries: &[
		Entry(0,"hello_request"),
		Entry(1,"client_hello"),
		Entry(2,"server_hello"),
		Entry(3,"hello_verify_request"),
		Entry(4,"new_session_ticket"),
		Entry(5,"end_of_early_data"),
		Entry(6,"hello_retry_request"),
		Entry(8,"encrypted_extensions"),
		Entry(9,"request_connection_id"),
		Entry(10,"new_connection_id"),
		Entry(11,"certificate"),
		Entry(12,"server_key_exchange"),
		Entry(13,"certificate_request"),
		Entry(14,"server_hello_done"),
		Entry(15,"certificate_verify"),
		Entry(16,"client_key_exchange"),
		Entry(17,"client_certificate_request"),
		Entry(20,"finished"),
		Entry(21,"certificate_url"),
		Entry(22,"certificate_status"),
		Entry(23,"supplemental_data"),
		Entry(24,"key_update"),
		Entry(25,"compressed_certificate"),
		Entry(26,"ekt_key"),
		Entry(254,"message_hash"),
	],
};

static SUPPORTED_GROUP: ConstantSet = ConstantSet {
	ctype: "NamedGroup",
	uppercase: null_uppercase,
	name: null_name,
	help: sgrp_help,
	grease: true,
	private: PrivateUse::NamedGroup,
	entries: &[
		Entry(1,"sect163k1"),
		Entry(2,"sect163r1"),
		Entry(3,"sect163r2"),
		Entry(4,"sect193r1"),
		Entry(5,"sect193r2"),
		Entry(6,"sect233k1"),
		Entry(7,"sect233r1"),
		Entry(8,"sect239k1"),
		Entry(9,"sect283k1"),
		Entry(10,"sect283r1"),
		Entry(11,"sect409k1"),
		Entry(12,"sect409r1"),
		Entry(13,"sect571k1"),
		Entry(14,"sect571r1"),
		Entry(15,"secp160k1"),
		Entry(16,"secp160r1"),
		Entry(17,"secp160r2"),
		Entry(18,"secp192k1"),
		Entry(19,"secp192r1"),
		Entry(20,"secp224k1"),
		Entry(21,"secp224r1"),
		Entry(22,"secp256k1"),
		Entry(23,"secp256r1"),
		Entry(24,"secp384r1"),
		Entry(25,"secp521r1"),
		Entry(26,"brainpoolP256r1"),
		Entry(27,"brainpoolP384r1"),
		Entry(28,"brainpoolP512r1"),
		Entry(29,"x25519"),
		Entry(30,"x448"),
		Entry(31,"brainpoolP256r1tls13"),
		Entry(32,"brainpoolP384r1tls13"),
		Entry(33,"brainpoolP512r1tls13"),
		Entry(34,"GC256A"),
		Entry(35,"GC256B"),
		Entry(36,"GC256C"),
		Entry(37,"GC256D"),
		Entry(38,"GC512A"),
		Entry(39,"GC512B"),
		Entry(40,"GC512C"),
		Entry(41,"curveSM2"),
		Entry(256,"ffdhe2048"),
		Entry(257,"ffdhe3072"),
		Entry(258,"ffdhe4096"),
		Entry(259,"ffdhe6144"),
		Entry(260,"ffdhe8192"),
		Entry(25497, "X25519Kyber768Draft00"),
		Entry(25498, "SecP256r1Kyber768Draft00"),
		Entry(65281,"arbitrary_explicit_prime_curves"),
		Entry(65282,"arbitrary_explicit_char2_curves"),
	],
};

static EC_POINT_FORMAT: ConstantSet = ConstantSet {
	ctype: "EcPointFormat",
	uppercase: null_uppercase,
	name: null_name,
	help: ecptfmt_help,
	grease: false,
	private: PrivateUse::L248,
	entries: &[
		Entry(0,"uncompressed"),
		Entry(1,"ansiX962_compressed_prime"),
		Entry(2,"ansiX962_compressed_char2"),
	],
};

static EC_CURVE_TYPE: ConstantSet = ConstantSet {
	ctype: "EcCurveType",
	uppercase: null_uppercase,
	name: null_name,
	help: crvtype_help,
	grease: false,
	private: PrivateUse::L248,
	entries: &[
		Entry(1,"explicit_prime"),
		Entry(2,"explicit_char2"),
		Entry(3,"named_curve"),
	],
};

static SUPPLEMENTAL_DATA_FORMAT: ConstantSet = ConstantSet {
	ctype: "SupplementalDataFormat",
	uppercase: null_uppercase,
	name: null_name,
	help: sdf_help,
	grease: false,
	private: PrivateUse::H255,
	entries: &[
		Entry(0,"user_mapping_data"),
		Entry(16386,"authz_data"),
	],
};

static USER_MAPPING_TYPE: ConstantSet = ConstantSet {
	ctype: "UserMappingType",
	uppercase: null_uppercase,
	name: null_name,
	help: usermap_help,
	grease: false,
	private: PrivateUse::L224,
	entries: &[
		Entry(64,"upn_domain_hint"),
	],
};

static AUTHORIZATION_DATA_FORMAT: ConstantSet = ConstantSet {
	ctype: "AuthorizationDataFormat",
	uppercase: null_uppercase,
	name: null_name,
	help: adf_help,
	grease: false,
	private: PrivateUse::L224,
	entries: &[
		Entry(0,"x509_attr_cert"),
		Entry(1,"saml_assertion"),
		Entry(2,"x509_attr_cert_url"),
		Entry(3,"saml_assertion_url"),
		Entry(64,"keynote_assertion_list"),
		Entry(65,"keynote_assertion_list_url"),
		Entry(66,"dtcp_authorization"),
	],
};

static HEARTBEAT_MESSAGE_TYPE: ConstantSet = ConstantSet {
	ctype: "HeartbeatMessageType",
	uppercase: null_uppercase,
	name: null_name,
	help: hbmsg_help,
	grease: false,
	private: PrivateUse::None,
	entries: &[
		Entry(1,"heartbeat_request"),
		Entry(2,"heartbeat_response"),
	],
};

static HEARTBEAT_MODE: ConstantSet = ConstantSet {
	ctype: "HeartbeatMode",
	uppercase: null_uppercase,
	name: null_name,
	help: hbmode_help,
	grease: false,
	private: PrivateUse::None,
	entries: &[
		Entry(1,"peer_allowed_to_send"),
		Entry(2,"peer_not_allowed_to_send"),
	],
};

static SIGNATURE_ALGORITHM: ConstantSet = ConstantSet {
	ctype: "SignatureAlgorithm",
	uppercase: null_uppercase,
	name: null_name,
	help: sigalgo_help,
	grease: false,
	private: PrivateUse::L224,
	entries: &[
		Entry(1,"rsa"),
		Entry(2,"dsa"),
		Entry(3,"ecdsa"),
	],
};

static HASH_ALGORITHM: ConstantSet = ConstantSet {
	ctype: "HashAlgorithm",
	uppercase: null_uppercase,
	name: null_name,
	help: hashalgo_help,
	grease: false,
	private: PrivateUse::L224,
	entries: &[
		Entry(0,"none"),
		Entry(1,"md5"),
		Entry(2,"sha1"),
		Entry(3,"sha224"),
		Entry(4,"sha256"),
		Entry(5,"sha384"),
		Entry(6,"sha512"),
	],
};

static SIGNATURE_SCHEME: ConstantSet = ConstantSet {
	ctype: "SignatureScheme",
	uppercase: null_uppercase,
	name: null_name,
	help: sigsch_help,
	grease: false,
	private: PrivateUse::None,	///??? Is going on with this.
	entries: &[
		Entry(0x0000,"wouldbe_null_null"),	//NULL auth is not allowed.
		Entry(0x0001,"wouldbe_rsa_pkcs1_null"),	//These three are "wouldbe", as none works with NULL.
		Entry(0x0002,"wouldbe_dss_null"),
		Entry(0x0003,"wouldbe_ecdsa_null"),
		Entry(0x0100,"wouldbe_null_md5"),
		Entry(0x0101,"tls12_rsa_pkcs1_md5"),
		Entry(0x0102,"wouldbe_dss_md5"),	//These two are "wouldbe", as DSS/ECDSA is not allowed
		Entry(0x0103,"wouldbe_ecdsa_md5"),	//with MD5.
		Entry(0x0200,"wouldbe_null_sha1"),
		Entry(0x0201,"rsa_pkcs1_sha1"),
		Entry(0x0202,"tl12_dss_sha1"),
		Entry(0x0203,"ecdsa_sha1"),
		Entry(0x0300,"wouldbe_null_sha224"),
		Entry(0x0301,"tls12_rsa_pkcs1_sha224"),
		Entry(0x0302,"tls12_dss_sha224"),
		Entry(0x0303,"tls12_ecdsa_sha224"),
		Entry(0x0400,"wouldbe_null_sha256"),
		Entry(0x0401,"rsa_pkcs1_sha256"),
		Entry(0x0402,"tls12_dss_sha256"),
		Entry(0x0403,"ecdsa_secp256r1_sha256"),
		Entry(0x0420,"rsa_pkcs1_sha256_legacy"),
		Entry(0x0500,"wouldbe_null_sha384"),
		Entry(0x0501,"rsa_pkcs1_sha384"),
		Entry(0x0502,"tls12_dss_sha384"),
		Entry(0x0503,"ecdsa_secp384r1_sha384"),
		Entry(0x0520,"rsa_pkcs1_sha384_legacy"),
		Entry(0x0600,"wouldbe_null_sha512"),
		Entry(0x0601,"rsa_pkcs1_sha512"),
		Entry(0x0602,"tls12_dss_sha512"),
		Entry(0x0603,"ecdsa_secp521r1_sha512"),
		Entry(0x0620,"rsa_pkcs1_sha512_legacy"),
		Entry(0x0704,"eccsi_sha256"),
		Entry(0x0705,"iso_ibs1"),
		Entry(0x0706,"iso_ibs2"),
		Entry(0x0707,"iso_chinese_ibs"),
		Entry(0x0708,"sm2sig_sm3"),
		Entry(0x0709,"gostr34102012_256a"),
		Entry(0x070a,"gostr34102012_256b"),
		Entry(0x070b,"gostr34102012_256c"),
		Entry(0x070c,"gostr34102012_256d"),
		Entry(0x070d,"gostr34102012_512a"),
		Entry(0x070e,"gostr34102012_512b"),
		Entry(0x070f,"gostr34102012_512c"),
		Entry(0x0804,"rsa_pss_rsae_sha256"),
		Entry(0x0805,"rsa_pss_rsae_sha384"),
		Entry(0x0806,"rsa_pss_rsae_sha512"),
		Entry(0x0807,"ed25519"),
		Entry(0x0808,"ed448"),
		Entry(0x0809,"rsa_pss_pss_sha256"),
		Entry(0x080a,"rsa_pss_pss_sha384"),
		Entry(0x080b,"rsa_pss_pss_sha512"),
		Entry(0x081a,"ecdsa_brainpoolP256r1tls13_sha256"),
		Entry(0x081b,"ecdsa_brainpoolP384r1tls13_sha384"),
		Entry(0x081c,"ecdsa_brainpoolP512r1tls13_sha512"),
		Entry(0x0840,"gostr34102012_256"),
		Entry(0x0841,"gostr34102012_512"),
	],
};

static PSK_KEY_EXCHANGE_MODE: ConstantSet = ConstantSet {
	ctype: "PskKeyExchangeMode",
	uppercase: null_uppercase,
	name: null_name,
	help: pskke_help,
	grease: false,
	private: PrivateUse::L254,
	entries: &[
		Entry(0,"psk_ke"),
		Entry(1,"psk_dhe_ke"),
	],
};

static KDF_IDENTIFIER: ConstantSet = ConstantSet {
	ctype: "KdfIdentifier",
	uppercase: null_uppercase,
	name: null_name,
	help: kdfid_help,
	grease: false,
	private: PrivateUse::H255,
	entries: &[
		Entry(1,"HKDF_SHA256"),
		Entry(2,"HKDF_SHA384"),
	],
};

static EXTENSION_TYPE: ConstantSet = ConstantSet {
	ctype: "ExtensionType",
	uppercase: null_uppercase,
	name: null_name,
	help: ext_help,
	grease: true,
	private: PrivateUse::Extension,
	entries: &[
		Entry(0,"server_name"),
		Entry(1,"max_fragment_length"),
		Entry(2,"client_certificate_url"),
		Entry(3,"trusted_ca_keys"),
		Entry(4,"truncated_hmac"),
		Entry(5,"status_request"),
		Entry(6,"user_mapping"),
		Entry(7,"client_authz"),
		Entry(8,"server_authz"),
		Entry(9,"cert_type"),
		Entry(10,"supported_groups"),
		Entry(11,"ec_point_formats"),
		Entry(12,"srp"),
		Entry(13,"signature_algorithms"),
		Entry(14,"use_srtp"),
		Entry(15,"heartbeat"),
		Entry(16,"application_layer_protocol_negotiation"),
		Entry(17,"status_request_v2"),
		Entry(18,"signed_certificate_timestamp"),
		Entry(19,"client_certificate_type"),
		Entry(20,"server_certificate_type"),
		Entry(21,"padding"),
		Entry(22,"encrypt_then_mac"),
		Entry(23,"extended_master_secret"),
		Entry(24,"token_binding"),
		Entry(25,"cached_info"),
		Entry(26,"tls_lts"),
		Entry(27,"compress_certificate"),
		Entry(28,"record_size_limit"),
		Entry(29,"pwd_protect"),
		Entry(30,"pwd_clear"),
		Entry(31,"password_salt"),
		Entry(32,"ticket_pinning"),
		Entry(33,"tls_cert_with_extern_psk"),
		Entry(34,"delegated_credentials"),
		Entry(35,"session_ticket"),
		Entry(36,"tlmsp"),
		Entry(37,"tlmsp_proxying"),
		Entry(38,"tlmsp_delegate"),
		Entry(39,"supported_ekt_ciphers"),
		Entry(41,"pre_shared_key"),
		Entry(42,"early_data"),
		Entry(43,"supported_versions"),
		Entry(44,"cookie"),
		Entry(45,"psk_key_exchange_modes"),
		Entry(47,"certificate_authorities"),
		Entry(48,"oid_filters"),
		Entry(49,"post_handshake_auth"),
		Entry(50,"signature_algorithms_cert"),
		Entry(51,"key_share"),
		Entry(52,"transparency_info"),
		Entry(53,"connection_id_old"),
		Entry(54,"connection_id"),
		Entry(55,"external_id_hash"),
		Entry(56,"external_session_id"),
		Entry(57,"quic_transport_parameters"),
		Entry(58,"ticket_request"),
		Entry(59,"dnssec_chain"),
		Entry(60,"sequence_number_encryption_algorithms"),
		Entry(65281,"renegotiation_info"),
	],
};

static CERTIFICATE_TYPE: ConstantSet = ConstantSet {
	ctype: "CertificateType",
	uppercase: null_uppercase,
	name: null_name,
	help: ctype_help,
	grease: false,
	private: PrivateUse::L224,
	entries: &[
		Entry(0,"X509"),
		Entry(1,"OpenPGP"),
		Entry(2,"raw_public_key|Raw Public Key"),	//It is this way in the registry.
		Entry(3,"ieee_1609_dot_2|1609Dot2"),
	],
};

static CERTIFICATE_STATUS_TYPE: ConstantSet = ConstantSet {
	ctype: "CertificateStatusType",
	uppercase: null_uppercase,
	name: null_name,
	help: cst_help,
	grease: false,
	private: PrivateUse::None,
	entries: &[
		Entry(1,"ocsp"),
		Entry(2,"ocsp_multi"),
	],
};

static CACHED_INFORMATION_TYPE: ConstantSet = ConstantSet {
	ctype: "CachedInformationType",
	uppercase: null_uppercase,
	name: null_name,
	help: cachedinfo_help,
	grease: false,
	private: PrivateUse::L224,
	entries: &[
		Entry(1,"cert"),
		Entry(2,"cert_req"),
	],
};

static CERTIFICATE_COMPRESSION_ALGORITHM_ID: ConstantSet = ConstantSet {
	ctype: "CertificateCompressionAlgorithmId",
	uppercase: null_uppercase,
	name: null_name,
	help: cca_help,
	grease: false,
	private: PrivateUse::None,
	entries: &[
		Entry(1,"zlib"),
		Entry(2,"brotli"),
		Entry(3,"zstd"),
	],
};

static COMPRESSION_METOD: ConstantSet = ConstantSet {
	ctype: "CompressionMethod",
	uppercase: null_uppercase,
	name: null_name,
	help: cm_help,
	grease: false,
	private: PrivateUse::L224,
	entries: &[
		Entry(0,"null"),
		Entry(1,"deflate"),
	],
};

static EKT_CIPHER: ConstantSet = ConstantSet {
	ctype: "EktCipher",
	uppercase: null_uppercase,
	name: null_name,
	help: ektcipher_help,
	grease: false,
	private: PrivateUse::None,
	entries: &[
		Entry(1,"aeskw_128"),
		Entry(2,"aeskw_256"),
	],
};

static DTLS_SRTP_PROTECTION_PROFILE: ConstantSet = ConstantSet {
	ctype: "DtlsSrtpProtectionProfile",
	uppercase: null_uppercase,
	name: null_name,
	help: srtpprofile_help,
	grease: false,
	private: PrivateUse::None,
	entries: &[
		Entry(0x0001, "SRTP_AES128_CM_HMAC_SHA1_80"),
		Entry(0x0002, "SRTP_AES128_CM_HMAC_SHA1_32"),
		Entry(0x0005, "SRTP_NULL_HMAC_SHA1_80"),
		Entry(0x0006, "SRTP_NULL_HMAC_SHA1_32"),
		Entry(0x0007, "SRTP_AEAD_AES_128_GCM"),
		Entry(0x0008, "SRTP_AEAD_AES_256_GCM"),
		Entry(0x0009, "DOUBLE_AEAD_AES_128_GCM_AEAD_AES_128_GCM"),
		Entry(0x000A, "DOUBLE_AEAD_AES_256_GCM_AEAD_AES_256_GCM"),
		Entry(0x000B, "SRTP_ARIA_128_CTR_HMAC_SHA1_80"),
		Entry(0x000C, "SRTP_ARIA_128_CTR_HMAC_SHA1_32"),
		Entry(0x000D, "SRTP_ARIA_256_CTR_HMAC_SHA1_80"),
		Entry(0x000E, "SRTP_ARIA_256_CTR_HMAC_SHA1_32"),
		Entry(0x000F, "SRTP_AEAD_ARIA_128_GCM"),
		Entry(0x0010, "SRTP_AEAD_ARIA_256_GCM"),
	],
};

static TOKEN_BINDING_KEY_PARAMETER: ConstantSet = ConstantSet {
	ctype: "TokenBindingKeyParameter",
	uppercase: null_uppercase,
	name: null_name,
	help: tbkey_help,
	grease: false,
	private: PrivateUse::None,
	entries: &[
		Entry(0, "rsa2048_pkcs1.5"),
		Entry(1, "rsa2048_pss"),
		Entry(2, "ecdsap256"),
	],
};

static SERVER_NAME_TYPE: ConstantSet = ConstantSet {
	ctype: "ServerNameType",
	uppercase: null_uppercase,
	name: null_name,
	help: snametype_help,
	grease: false,
	private: PrivateUse::None,
	entries: &[
		Entry(0,"host_name"),
	],
};

static CONNECTION_ID_USAGE: ConstantSet = ConstantSet {
	ctype: "ConnectionIdUsage",
	uppercase: null_uppercase,
	name: null_name,
	help: conniduse_help,
	grease: false,
	private: PrivateUse::None,
	entries: &[
		Entry(0,"cid_immediate"),
		Entry(1,"cid_spare"),
	],
};

static CERT_CHAIN_TYPE: ConstantSet = ConstantSet {
	ctype: "CertChainType",
	uppercase: null_uppercase,
	name: null_name,
	help: certchaint_help,
	grease: false,
	private: PrivateUse::None,
	entries: &[
		Entry(0,"individual_certs"),
		Entry(1,"pkipath"),
	],
};

static SEQUENCE_NUMBER_ENCRYPTION_ALGORITHM: ConstantSet = ConstantSet {
	ctype: "SequenceNumberEncryptionAlgorithm",
	uppercase: null_uppercase,
	name: null_name,
	help: seqenc_help,
	grease: false,
	private: PrivateUse::None,
	entries: &[
		Entry(0,"default_cipher"),
		Entry(1,"plaintext"),
	],
};

static EXPORTER_LABELS: &'static str = r#"
client EAP encryption
ttls keying material
ttls challenge
EXTRACTOR-dtls_srtp
EXPORTER_DTLS_OVER_SCTP
EXPORTER-ETSI-TC-M2M-Bootstrap
EXPORTER-ETSI-TC-M2M-Connection
TLS_MK_Extr
EXPORTER_GBA_Digest
EXPORTER: teap session key seed
EXPORTER-oneM2M-Bootstrap
EXPORTER-oneM2M-Connection
EXPORTER-oneM2M-ESCertKE¦ONEM2M_ES_CERT_KE
EXPORTER-Token-Binding
EXPORTER-BBF-Dying-Gasp
EXPORTER-network-time-security
EXPORTER_3GPP_N32_MASTER¦TGPP_N32_MASTER
EXPORTER-ACE-MQTT-Sign-Challenge
EXPORTER_EAP_TLS_Key_Material
EXPORTER_EAP_TLS_Method-Id
EXPORTER-BBF-USP-Record
EXPORTER-client authenticator handshake context
EXPORTER-server authenticator handshake context
EXPORTER-client authenticator finished key
EXPORTER-server authenticator finished key
EXPORTER-Channel-Binding
EXPORTER: Inner Methods Compound Keys
EXPORTER: Session Key Generating Function
EXPORTER: Extended Session Key Generating Function
TEAPbindkey@ietf.org|TEAPBINDKEY_AT_IETF_DOT_ORG
"#;

const EXPORTER_STRIP_PREFIX: &'static [&'static str] = &[
	"EXTRACTOR-",
	"EXPORTER_",
	"EXPORTER-",
	"EXPORTER: ",
];

fn write_exporter_labels(f: &mut File)
{
	let exporter_consts: Vec<CodeBlock> = EXPORTER_LABELS.lines().filter_map(|label|{
		//Skip empty lines.
		if label.len() == 0 { return None; }
		if let Some(p) = label.find("¦") {
			let sym = mkid(&label[p+2..]);
			let val = &label[..p];
			return Some(quote!(pub const #sym: ExporterLabel = ExporterLabel(#val);));
		}
		let mut label2 = label;
		for pfx in EXPORTER_STRIP_PREFIX.iter() {
			if let Some(label3) = label2.strip_prefix(pfx) {
				label2 = label3;
				break;
			}
		}
		let mut label3 = String::new();
		for (_, c) in label2.char_indices() {
			if c.is_alphanumeric() {
				label3.extend(c.to_uppercase());
			} else {
				label3.push('_');
			}
		}
		let label3 = mkid(&label3);
		Some(quote!(pub const #label3: ExporterLabel = ExporterLabel(#label);))
	}).collect();
	qwrite(f, quote!{
impl ExporterLabel
{
	#(#exporter_consts)*
}

	});
}

static STANDARD_ALPN: &'static str = r#"
http/0.9¦HTTP_09¦HTTP/0.9
http/1.0¦HTTP_10¦HTTP/1.0
http/1.1¦HTTP_11¦HTTP/1.1
spdy/1¦SPDY_1¦SPDY/1
spdy/2¦SPDY_2¦SPDY/2
spdy/3¦SPDY_3¦SPDY/3
stun.turn¦STUN_TURN¦Traversal Using Relays around NAT (TURN)
stun.nat-discovery¦STUN_NAT_DISCOVERY¦NAT discovery using Session Traversal Utilities for NAT (STUN)
h2¦HTTP_2¦HTTP/2 over TLS
h2c¦HTTP_2_CLEAR¦HTTP/2 over TCP
webrtc¦WEBRTC¦WebRTC Media and Data
c-webrtc¦CONFIDENTIAL_WEBRTC¦Confidential WebRTC Media and Data
ftp¦FTP¦FTP
imap¦IMAP¦IMAP
pop3¦POP3¦POP3
managesieve¦MANAGESIEVE¦ManageSieve
coap¦COAP¦CoAP
xmpp-client¦XMPP_CLIENT¦XMPP jabber:client namespace
xmpp-server¦XMPP_SERVER¦XMPP jabber:server namespace
acme-tls/1¦ACME_TLS¦acme-tls/1
mqtt¦MQTT¦OASIS Message Queuing Telemetry Transport (MQTT)
dot¦DNS_OVER_TLS¦DNS-over-TLS
ntske/1¦NETWORK_TIME_SECURITY_1¦Network Time Security Key Establishment, version 1
sunrpc¦SUNRPC¦SunRPC
h3¦HTTP_3¦HTTP/3
smb¦SMB2¦SMB2
irc¦IRC¦IRC
nntp¦NNTP_READ¦NNTP (reading)
nnsp¦NNTP_TRANSIT¦NNTP (transit)
doq¦DNS_OVER_QUIC¦DoQ
sip/2¦SIP_2¦SIP
tds/8.0¦TDS_80¦TDS/8.0
dicom¦DICOM¦DICOM
"#;

fn write_alpn_labels(f: &mut File)
{
	let mut set = BTreeMap::new();
	let alpn_consts: Vec<CodeBlock> = STANDARD_ALPN.lines().filter_map(|label|{
		if label.len() == 0 { return None; }
		let mut itr = label.splitn(3, "¦");
		let cp = itr.next().expect("codepoint");
		let sym = itr.next().expect("symbol");
		let desc = itr.next().expect("description");
		set.insert(cp,sym);
		let help = format!("Application Layer Protocol: {desc}");
		let sym = mkid(sym);
		Some(quote!(
			#[doc=#help]
			pub const #sym: AlpnProtocolId<'static> = AlpnProtocolId(#cp.as_bytes());
		))
	}).collect();
	let alpn_set: Vec<_> = set.iter().map(|(_,sym)|mkid(sym)).collect();
	let setlen  = set.len();
	let setwords = (set.len() + 63) / 64;
	qwrite(f, quote!{
impl<'a> AlpnProtocolId<'a>
{
	#(#alpn_consts)*
}
const STANDARD_ALPN_COUNT: usize = #setlen;
const ALPN_MASK_WORDS: usize = #setwords;
static STANDARD_ALPN_SORTED: [AlpnProtocolId<'static>;STANDARD_ALPN_COUNT] = [
	#(AlpnProtocolId::#alpn_set),*
];

	});
}

fn main()
{
	let out_dir = env::var("OUT_DIR").unwrap();
	let dest_path = Path::new(&out_dir).join("autogenerated.inc.rs");
	let mut f = File::create(&dest_path).unwrap();
	write_constant_set(&mut f, &CLIENT_CERTIFICATE_TYPE);
	write_constant_set(&mut f, &CIPHER_SUITE);
	write_constant_set(&mut f, &CONTENT_TYPE);
	write_constant_set(&mut f, &ALERT);
	write_constant_set(&mut f, &HANDSHAKE_TYPE);
	write_constant_set(&mut f, &SUPPORTED_GROUP);
	write_constant_set(&mut f, &EC_POINT_FORMAT);
	write_constant_set(&mut f, &EC_CURVE_TYPE);
	write_constant_set(&mut f, &SUPPLEMENTAL_DATA_FORMAT);
	write_constant_set(&mut f, &USER_MAPPING_TYPE);
	write_constant_set(&mut f, &AUTHORIZATION_DATA_FORMAT);
	write_constant_set(&mut f, &HEARTBEAT_MESSAGE_TYPE);
	write_constant_set(&mut f, &HEARTBEAT_MODE);
	write_constant_set(&mut f, &SIGNATURE_ALGORITHM);
	write_constant_set(&mut f, &HASH_ALGORITHM);
	write_constant_set(&mut f, &SIGNATURE_SCHEME);
	write_constant_set(&mut f, &PSK_KEY_EXCHANGE_MODE);
	write_constant_set(&mut f, &KDF_IDENTIFIER);
	write_constant_set(&mut f, &EXTENSION_TYPE);
	write_constant_set(&mut f, &CERTIFICATE_TYPE);
	write_constant_set(&mut f, &CERTIFICATE_STATUS_TYPE);
	write_constant_set(&mut f, &CACHED_INFORMATION_TYPE);
	write_constant_set(&mut f, &CERTIFICATE_COMPRESSION_ALGORITHM_ID);
	write_constant_set(&mut f, &COMPRESSION_METOD);
	write_constant_set(&mut f, &EKT_CIPHER);
	write_constant_set(&mut f, &DTLS_SRTP_PROTECTION_PROFILE);
	write_constant_set(&mut f, &TOKEN_BINDING_KEY_PARAMETER);
	write_constant_set(&mut f, &SERVER_NAME_TYPE);
	write_constant_set(&mut f, &CONNECTION_ID_USAGE);
	write_constant_set(&mut f, &CERT_CHAIN_TYPE);
	write_constant_set(&mut f, &SEQUENCE_NUMBER_ENCRYPTION_ALGORITHM);
	write_tls_versions(&mut f);
	write_exporter_labels(&mut f);
	write_alpn_labels(&mut f);


	println!("cargo:rerun-if-changed=build.rs");	//This has no external deps to rebuild for.
}
