use std::cmp::Ordering;
use std::ptr::copy_nonoverlapping;

///Copy data from src to dst. The maximum size dst can grow is max. Returns dst.len() + src.len() compared to
///max and number of elements copied.
pub fn slice_to_vector<T:Copy>(dst: &mut Vec<T>, src: &[T], max: usize) -> (Ordering, usize)
{
	if dst.len() > max { return (Ordering::Greater, 0); }
	let limit = max - dst.len();
	let l = if limit < src.len() {
		dst.extend_from_slice(&src[..limit]);
		limit
	} else {
		dst.extend_from_slice(src);
		src.len()
	};
	(src.len().cmp(&limit), l)
}

///Discard first n elements of vec.
pub fn discard_vector<T:Copy>(vec: &mut Vec<T>, n: usize)
{
	if n >= vec.len() { return vec.clear(); }
	let left = vec.len() - n;
	let base = vec.as_mut_ptr();
	unsafe{copy_nonoverlapping(base.add(n), base, left)};
	vec.truncate(left);
}

pub struct RxRed
{
	backing: Vec<u8>,
	consumed: usize,
	reserved_size: usize,
	eof: bool,
}

impl RxRed
{
	pub fn new() -> RxRed
	{
		RxRed {
			backing: Vec::new(),
			consumed: 0,
			reserved_size: 0,
			eof: false,
		}
	}
	pub fn at_end(&self) -> bool { self.eof && self.consumed == self.backing.len() }
	pub fn is_empty(&self) -> bool { self.consumed == self.backing.len() }
	pub fn prepare_write(&mut self, n: usize) -> &mut [u8]
	{
		if self.eof { return &mut self.backing[0..0]; }
		self.reserved_size = self.backing.len();
		self.backing.resize(self.reserved_size+n, 0);
		&mut self.backing[self.reserved_size..]
	}
	pub fn commit_write(&mut self, n: usize) { self.backing.truncate(self.reserved_size + n); }
	pub fn commit_eof(&mut self) { self.backing.truncate(self.reserved_size); self.eof = true; }
	pub fn peek_all(&self) -> &[u8]
	{
		self.backing.get(self.consumed..).unwrap_or(b"")
	}
	pub fn pending_eof(&self) -> bool { self.eof }
	pub fn consume(&mut self, l: usize)
	{
		self.consumed += l;
		if self.consumed >= self.backing.len() {
			self.backing.clear();
			self.consumed = 0;
		}
	}
	pub fn read(&mut self, buf: &mut [u8]) -> Option<usize>
	{
		if self.consumed >= self.backing.len() {
			return if self.eof { None } else { Some(0) }
		}
		let r = &self.backing[self.consumed..];
		let l = if r.len() < buf.len() {
			(&mut buf[..r.len()]).copy_from_slice(r);
			r.len()
		} else {
			let l = buf.len();
			buf.copy_from_slice(&r[..l]);
			l
		};
		self.consume(l);
		Some(l)
	}
}

