use super::Transport;
use super::vec_utils::discard_vector;
use mini_tls_core::unstable::Client;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use std::io::Error;
use std::io::ErrorKind;
use std::mem::replace;

#[derive(Clone)]
pub(crate) struct CapturedError(Result<i32, (ErrorKind,String)>);

impl CapturedError
{
	fn capture(error: &Error) -> CapturedError
	{
		let error = match error.raw_os_error() {
			Some(code) => Ok(code),
			None => Err((error.kind(), error.to_string()))
		};
		CapturedError(error)
	}
	fn release(&self) -> Error
	{
		match &self.0 {
			&Ok(code) => Error::from_raw_os_error(code),
			&Err((kind, ref msg)) => Error::new(kind, msg.clone())
		}
	}
}

const MAX_PACKET: usize = 16645;

pub(crate) struct RecordDiscard(usize);

pub(crate) enum RxRecordResult<'a>
{
	Eof(bool),	//Clean?
	Wait,
	TooBig,
	Record(&'a [u8], RecordDiscard),
}

pub(crate) struct Transport2<TS:Transport>
{
	//The underlying transport.
	transport: TS,
	//The error last read blocked on.
	read_blocked: Option<CapturedError>,
	//The error last write blocked on.
	write_blocked: Option<CapturedError>,
	//The error that crashed the transport.
	transport_error: Option<CapturedError>,
	//Urgent data, and amount of urgent data sent so far.
	urgent: Option<Vec<u8>>,
	urgent_ptr: usize,
	//Normal send buffer, and amount of normal data sent so far.
	write: Vec<u8>,
	write_ptr: usize,
	//Normal receive buffer, and amount of data received so far.
	read: Vec<u8>,
	read_ptr: usize,
	read_eof: bool,
	
}

impl<TS:Transport> Transport2<TS>
{
	pub(crate) fn new(transport: TS) -> Transport2<TS>
	{
		Transport2 {
			transport: transport,
			read_blocked: None,
			write_blocked: None,
			transport_error: None,
			urgent: None,
			urgent_ptr: 0,
			write: Vec::new(),
			write_ptr: 0,
			read: Vec::new(),
			read_ptr: 0,
			read_eof: false,
		}
	}
	pub(crate) fn transport_flags(&self) -> u32
	{
		let mut flags = 0;
		if self.read_blocked.is_some() { flags |= super::STATUS_READ_BLOCKED; }
		if self.write_blocked.is_some() { flags |= super::STATUS_WRITE_BLOCKED; }
		if self.transport_error.is_some() { flags |= super::STATUS_TRANSPORT_ERROR; }
		flags
	}
	pub(crate) fn unblock_read(&mut self) { self.read_blocked = None; }
	pub(crate) fn unblock_write(&mut self) { self.write_blocked = None; }
	pub(crate) fn set_error(&mut self, err: Error) -> Result<(), Error>
	{
		self.transport_error = Some(CapturedError::capture(&err));
		Err(err)
	}
	pub(crate) fn check_error(&self) -> Result<(), Error>
	{
		if let &Some(ref terr) = &self.transport_error { fail!(terr.release()); }
		Ok(())
	}
	//This retries Interrupted and records other errors.
	pub(crate) fn read(&mut self, buf: &mut [u8]) -> Result<usize, Error>
	{
		self.__check_read_errors()?;
		fail_if!(buf.len()==0, Error::new(ErrorKind::InvalidInput, "Can not read to empty buffer"));
		loop { match self.transport.read(buf) {
			Ok(r) => return Ok(r),
			Err(ref e) if e.kind() == ErrorKind::Interrupted => (),
			Err(ref e) if e.kind() == ErrorKind::WouldBlock => {
				let e = CapturedError::capture(e);
				self.read_blocked = Some(e.clone());
				fail!(e.release());
			},
			Err(e) => {
				self.transport_error = Some(CapturedError::capture(&e));
				fail!(e)
			}
		}}
			
	}
	//This retries short writes. If write is short, that means stream got into blocked state.
	pub fn write(&mut self, mut buf: &[u8]) -> Result<usize, Error>
	{
		self.__check_write_errors()?;
		let origsz = buf.len();
		while buf.len() > 0 { match self.__write(buf) {
			Ok(n) => if n < buf.len() { buf = &buf[n..] } else { return Ok(origsz); },
			//No need to do anything with Interrupted, as __write() retries those.
			//Report first write being blocked as 0 byte short write.
			Err(ref e) if e.kind() == ErrorKind::WouldBlock => return Ok(origsz - buf.len()),
			Err(e) => fail!(e)
		}}
		Ok(0)
	}
	//Will not return Ok(()) while there is urgent data.
	pub(crate) fn write_urgent(&mut self) -> Result<(), Error>
	{
		//Transmitting stuff has priority over reporting errors to application. This is in order
		//for applications to not have to worry about properly transmitting alerts. But this is
		//a generic flight transmission facility, not just alert transmission facility.
		//Need to temporarily take the field to prevent borrowck errors.
		while let Some(urgent) = self.urgent.take() {
			if self.urgent_ptr < urgent.len() {
				let urgent2 = &urgent[self.urgent_ptr..];
				let r = self.write(urgent2);
				if let Ok(r) = r { self.urgent_ptr += r; }
				self.urgent = Some(urgent);
				r?;
				//Loop back. If write was short, the next write will report WouldBlock,
				//which the previous r? will return. If not, then next iteration clears
				//stuff to send, and proceds with the handshake.
			}
			//No need to put it back in else branch, there should not have been pending
			//stuff anyway.
		}
		Ok(())
	}
	pub(crate) fn has_urgent_data(&self) -> bool { self.urgent.is_some() }
	pub(crate) fn transmit_urgent_data(&mut self, flight: Vec<u8>) -> Result<(), Error>
	{
		self.urgent = Some(flight);
		self.urgent_ptr = 0;
		//Attempt immedate transmission, but report errors a bit differently: WouldBlock is not reported,
		//only fatal transport errors.
		match self.write_urgent() {
			Ok(_) => Ok(()),
			Err(ref e) if e.kind() == ErrorKind::WouldBlock => Ok(()),
			Err(e) => Err(e)
		}
	}
	pub(crate) fn write_tcp(&mut self) -> Result<(), Error>
	{
		//Loop to guarantee the entiere buffer either gets written or WouldBlock is returned. The
		//position is tracked in write_ptr, so this call is restarable.
		while self.write_ptr < self.write.len() {
			//temporarily swap buffer because borrowck.
			let tmp = replace(&mut self.write, Vec::new());
			let r = self.write(&tmp[self.write_ptr..]);
			self.write = tmp;
			self.write_ptr += r?;
		}
		self.write.clear();
		self.write_ptr = 0;
		Ok(())
	}
	pub(crate) fn prepare_write(&mut self, size: usize) -> &mut [u8]
	{
		self.write.resize(size, 0);
		&mut self.write
	}
	pub(crate) fn commit_write(&mut self, n: usize)
	{
		self.write.truncate(n);
		self.write_ptr = 0;
	}
	pub(crate) fn read_tcp(&mut self) -> Result<(), Error>
	{
		//Compact the read buffer.
		if self.read_ptr > 0 {
			discard_vector(&mut self.read, self.read_ptr);
			self.read_ptr = 0;
		}
		if self.read_eof { return Ok(()); }	//Do not read once EOF.
		let old_size = self.read.len();
		if old_size >= MAX_PACKET { return Ok(()); }
		self.read.resize(MAX_PACKET, 0);
		//temporarily swap buffer because borrowck.
		let mut tmp = replace(&mut self.read, Vec::new());
		let r = self.read(&mut tmp[old_size..]);
		self.read = tmp;
		let amt = if let Ok(r) = r { r } else { 0 };
		self.read.resize(old_size + amt, 0);	//Trim read buffer to contain actually valid data.
		if r.as_ref().ok() == Some(&0) { self.read_eof = true; }
		r.map(|_|())
	}
	pub(crate) fn next_record<'a>(&'a self) -> RxRecordResult<'a>
	{
		if self.read_ptr >= self.read.len() {
			return if self.read_eof { RxRecordResult::Eof(true) } else { RxRecordResult::Wait };
		}
		let remainder = &self.read[self.read_ptr..];
		let len = Client::first_record_size(remainder);
		if len > remainder.len() {
			//If receive bufer is stuck with packet above max, erroring out is the only way to avoid
			//deadlock.
			if len > MAX_PACKET { return RxRecordResult::TooBig; }
			return if self.read_eof { RxRecordResult::Eof(false) } else { RxRecordResult::Wait };
		}
		RxRecordResult::Record(&remainder[..len], RecordDiscard(len))
	}
	pub(crate) fn discard_record(&mut self, d: RecordDiscard)
	{
		self.read_ptr += d.0;
	}
	pub(crate) fn raise_write_block(&self) -> Result<(), Error> { self.__check_write_errors() }
	fn __check_write_errors(&self) -> Result<(), Error>
	{
		if let Some(transport_error) = self.transport_error.as_ref() { fail!(transport_error.release()); }
		if let Some(write_blocked) = self.write_blocked.as_ref() { fail!(write_blocked.release()); }
		Ok(())
	}
	fn __check_read_errors(&self) -> Result<(), Error>
	{
		if let Some(transport_error) = self.transport_error.as_ref() { fail!(transport_error.release()); }
		if let Some(read_blocked) = self.read_blocked.as_ref() { fail!(read_blocked.release()); }
		Ok(())
	}
	//This retries Interrupted and records other errors.
	fn __write(&mut self, buf: &[u8]) -> Result<usize, Error>
	{
		self.__check_write_errors()?;
		loop { match self.transport.write(buf) {
			Ok(r) => return Ok(r),
			Err(ref e) if e.kind() == ErrorKind::Interrupted => (),
			Err(ref e) if e.kind() == ErrorKind::WouldBlock => {
				let e = CapturedError::capture(e);
				self.write_blocked = Some(e.clone());
				fail!(e.release());
			},
			Err(e) => {
				self.transport_error = Some(CapturedError::capture(&e));
				fail!(e)
			}
		}}
			
	}
}

///Helper trait to call Transport methods.
pub trait CallTransport
{
	///Call Transport::write.
	fn write(&mut self, buf: &[u8]) -> Result<usize,Error>;
	///Call Transport::read.
	fn read(&mut self, buf: &mut [u8]) -> Result<usize,Error>;
}

impl CallTransport for Box<dyn Transport+Send+Sync+'static>
{
	fn write(&mut self, buf: &[u8]) -> Result<usize,Error> { self.as_mut().write(buf) }
	fn read(&mut self, buf: &mut [u8]) -> Result<usize,Error> { self.as_mut().read(buf) }
}

impl CallTransport for Box<dyn Transport+'static>
{
	fn write(&mut self, buf: &[u8]) -> Result<usize,Error> { self.as_mut().write(buf) }
	fn read(&mut self, buf: &mut [u8]) -> Result<usize,Error> { self.as_mut().read(buf) }
}

