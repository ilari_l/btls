#![warn(unsafe_op_in_unsafe_fn)]
use crate::transport_utils::Transport2;
use crate::transport_utils::RxRecordResult;
use crate::vec_utils::slice_to_vector;
use crate::vec_utils::discard_vector;
use crate::vec_utils::RxRed;
use btls_aux_fail::f_break;
use btls_aux_fail::f_continue;
use btls_aux_fail::fail;
use btls_aux_memory::Attachment;
use btls_aux_memory::ByteStringLines;
use btls_aux_memory::split_at;
use btls_aux_memory::split_attach_first;
use btls_aux_serialization::Asn1Tag;
use btls_aux_serialization::Source;
use btls_aux_signatures::SignatureAlgorithmSet2;
use btls_aux_tls_iana::Alert;
use btls_aux_tls_iana::AlpnProtocolId;
pub use mini_tls_core::HostKey;
use mini_tls_core::unstable::*;
use std::collections::BTreeMap;
use std::io::Error;
use std::io::ErrorKind;
use std::io::Read;
use std::io::Write;

macro_rules! fail_io
{
	($kind:ident $arg:expr) => {
		return Err(std::io::Error::new(std::io::ErrorKind::$kind, $arg))
	};
	($kind:ident $($args:tt)*) => {
		return Err(std::io::Error::new(std::io::ErrorKind::$kind, format!($($args)*)))
	};
}

mod transport_utils;
mod vec_utils;

///Transport interface.
///
///Transport interface provodes low-level interface to receive and transmit data via whatever transport is used
///(usually TCP).
///
///The transport may be blocked for read, blocked for write or in error state.
///
/// * If `write()` returns a short write, it is immediately called again to write the rest.
/// * If `read()` returns a short read, it will be called again if there is insufficient data to make further
///progress eiher via completing current flight, or reading any application data.
/// * If `read()` returns 0 bytes, it is assumed that transport will never produce data again. If this leads
///to TLS error depends on if stream can close cleanly at that point.
/// * If `read()` returns `WouldBlock` class error, the transport is blocked for read. In this state, no further
///read calls will be issued until transport is unblocked using the `unblock_read()` method.
/// * If `write()` returns `WouldBlock` class error, the transport is blocked for write. In this state, no further
///write calls will be issued until transport is unblocked using the `unblock_write()` method.
/// * If `read()` or `write()` call returns `Interrupted` class error, they are immediately called again.
/// * If `read()` or `write()` call returns error other than `WouldBlock` or `Interrupted` class, the transport
///enters error state. In this state, no further `read()` nor `write()` calls will be issued. This state is
///terminal. It is not possible to exit it.
///
///`Transport` is implemented for `std::net::TcpStream`, so one may use that as transport for simple half-duplex
///applications. On Unix, it is also implemented for `std::os::unix::net::UnixStream`.
///
pub trait Transport
{
	///Write data to transport.
	///
	///This method takes the buffer `buf` and writes a prefix of it into transport interface. The return
	///value is the same as in `std::io::Write::write()`.
	fn write(&mut self, buf: &[u8]) -> Result<usize, Error>;
	///Read data from transport.
	///
	///This method reads data from transport into buffer `buf`, filling a prefix of it. The return
	///value is the same as in `std::io::Read::read()`.
	fn read(&mut self, buf: &mut [u8]) -> Result<usize, Error>;
}

impl Transport for std::net::TcpStream
{
	fn read(&mut self, buf: &mut [u8]) -> Result<usize, Error>
	{
		Read::read(self, buf)
	}
	fn write(&mut self, buf: &[u8]) -> Result<usize, Error>
	{
		Write::write(self, buf).and_then(|x|{Write::flush(self)?; Ok(x)})
	}
}

#[cfg(unix)]
impl Transport for std::os::unix::net::UnixStream
{
	fn read(&mut self, buf: &mut [u8]) -> Result<usize, Error>
	{
		Read::read(self, buf)
	}
	fn write(&mut self, buf: &[u8]) -> Result<usize, Error>
	{
		Write::write(self, buf).and_then(|x|{Write::flush(self)?; Ok(x)})
	}
}

///Error type used by HTTP liner read errors.
#[derive(Copy,Clone,Debug)]
pub struct HttpLineReadError(_HttpLineReadError);
#[derive(Copy,Clone,Debug)]
enum _HttpLineReadError
{
	BadLineEnd,
	StartWhitespace,
	IllegalByte(u8),
	LineTooLong,
}

impl From<_HttpLineReadError> for HttpLineReadError
{
	fn from(x: _HttpLineReadError) -> HttpLineReadError { HttpLineReadError(x) }
}

macro_rules! http_error
{
	(IllegalByte $arg:expr) => {
		fail!(Error::new(ErrorKind::Other, HttpLineReadError(_HttpLineReadError::IllegalByte($arg))))
	};
	($err:ident) => {
		fail!(Error::new(ErrorKind::Other, HttpLineReadError(_HttpLineReadError::$err)))
	};
}

impl std::error::Error for HttpLineReadError {}

impl std::fmt::Display for HttpLineReadError
{
	fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error>
	{
		use self::_HttpLineReadError::*;
		match &self.0 {
			&BadLineEnd => write!(f, "CR not followed by LF"),
			&StartWhitespace => write!(f, "Line starts with whitespace"),
			&IllegalByte(b) => write!(f, "Line contains illegal byte {b}"),
			&LineTooLong => write!(f, "Line is too long"),
		}
	}
}

///X.509 Trust anchor.
///
///This object is a X.509 trust anchor, also known as root certificate.
#[derive(Clone)]
pub struct TrustAnchor
{
	name: Vec<u8>,
	pubkey: Vec<u8>,
}

impl TrustAnchor
{
	///Construct trust anchors from file.
	///
	///The loaded trust anchors MUST be selfsigned and in validity period.
	///
	///The file may be in either PEM or (m)DER format.
	pub fn from_file<P:AsRef<std::path::Path>>(filename: P) -> Result<Vec<TrustAnchor>, Error>
	{
		Self::__from_file(filename.as_ref())
	}
	///Construct trust anchors from something implementing `std::io::Read`.
	///
	///The loaded trust anchors MUST be selfsigned and in validity period.
	///
	///The buffer may be in either PEM or (m)DER format.
	pub fn from_read(resource: &mut dyn Read) -> Result<Vec<TrustAnchor>, Error>
	{
		let mut x = Vec::new();
		resource.read_to_end(&mut x)?;
		Self::__from_memory(&x)
	}
	fn __from_file(filename: &std::path::Path) -> Result<Vec<TrustAnchor>, Error>
	{
		let mut file = std::fs::File::open(filename)?;
		Self::from_read(&mut file)
	}
	fn __from_memory(buffer: &[u8]) -> Result<Vec<TrustAnchor>, Error>
	{
		let mut out = Vec::new();
		if Self::__try_pem(buffer, &mut out) { return Ok(out); }
		//Then try (M)DER.
		if Self::__try_mder(buffer, &mut out) { return Ok(out); }
		fail_io!(Other "File is not in (M)DER nor PEM format")
	}
	fn __try_mder(mut buffer: &[u8], out: &mut Vec<TrustAnchor>) -> bool
	{
		let ocount = out.len();
		while let Ok((_, tail)) = split_attach_first(buffer, b"0", Attachment::Right) {
			//In case the certificate parse fails, preload buffer with remainder.
			buffer = f_break!(tail.get(1..));	//How can this fail???
			let mut cert = Source::new(tail);
			//Try reading certificate and see if it looks even remotely sane.
			let (outer, mut value) = f_continue!(cert.asn1_sequence_inout());
			f_continue!(value.asn1_slice(Asn1Tag::SEQUENCE));
			f_continue!(value.asn1_slice(Asn1Tag::SEQUENCE));
			f_continue!(value.asn1_slice(Asn1Tag::BIT_STRING));
			f_continue!(value.expect_end());
			//Yes, looks certificateish, parse it, and relock if it parses.
			if Self::__add_certificate(outer, out) { buffer = cert.read_remaining(); }
		}
		out.len() > ocount
	}
	fn __try_pem(buffer: &[u8], out: &mut Vec<TrustAnchor>) -> bool
	{
		let mut any = false;
		let mut cert: Option<Vec<u8>> = None;
		for line in ByteStringLines::new(buffer) {
			let sb = Self::__pem_start_line(line);
			let eb = Self::__pem_end_line(line);
			if let &mut Some(ref mut cert2) = &mut cert {
				if eb == Some(b"CERTIFICATE") {
					Self::__process_base64(&cert2, out);
					any = true;
					cert = None;
				} else if sb == Some(b"CERTIFICATE") {
					//Malformed, re-init.
					*cert2 = Vec::new();
				} else if sb.is_some() || eb.is_some() {
					//Malformed.
					cert = None;
				} else {
					cert2.extend_from_slice(line);
				}
			} else if sb == Some(b"CERTIFICATE") {
				cert = Some(Vec::new());
			}
		}
		any
	}
	fn __pem_start_line(line: &[u8]) -> Option<&[u8]>
	{
		let line = line.strip_prefix(b"-----BEGIN ")?;
		let line = line.strip_suffix(b"-----")?;
		Some(line)
	}
	fn __pem_end_line(line: &[u8]) -> Option<&[u8]>
	{
		let line = line.strip_prefix(b"-----END ")?;
		let line = line.strip_suffix(b"-----")?;
		Some(line)
	}
	fn __process_base64(buffer: &[u8], out: &mut Vec<TrustAnchor>)
	{
		let mut tmp = Vec::with_capacity(buffer.len() - buffer.len() / 4);
		let mut phase = 0;
		let mut v = 0u32;
		for &b in buffer.iter() { match Self::__base64val(b) {
			x@0..=63 => {
				v |= x << 18 - 6 * phase;
				phase += 1;
				if phase == 4 {
					//v.to_be_bytes() is 4 bytes.
					tmp.extend_from_slice(&v.to_be_bytes()[1..4]);
					v = 0;
					phase = 0;
				}
			},
			_ => ()
		}}
		if phase == 2 {
			v >>= 16;
			//v.to_be_bytes() is 4 bytes.
			tmp.extend_from_slice(&v.to_be_bytes()[3..4]);
		} else if phase == 3 {
			v >>= 8;
			//v.to_be_bytes() is 4 bytes.
			tmp.extend_from_slice(&v.to_be_bytes()[2..4]);
		}
		Self::__add_certificate(&tmp, out);
	}
	fn __base64val(b: u8) -> u32
	{
		match b {
			b@48..=57 => b as u32 + 4,
			b@65..=90 => b as u32 - 65,
			b@97..=122 => b as u32 - 71,
			b'+' => 62,
			b'/' => 63,
			b'=' => 64,
			_ => 65
		}
	}
	fn __add_certificate(cert: &[u8], out: &mut Vec<TrustAnchor>) -> bool
	{
		let pcrt = match btls_aux_x509certparse::ParsedCertificate2::from(cert) {
			Ok(x) => x,
			Err(_) => return false
		};
		//Final checks.
		if !pcrt.issuer.same_as(pcrt.subject) { return true; }
		let now = btls_aux_time::Timestamp::now();
		if now < pcrt.not_before || now > pcrt.not_after { return true; }
		out.push(TrustAnchor {
			name: pcrt.subject.as_raw_subject().to_vec(),
			pubkey: pcrt.pubkey.to_vec(),
		});
		true
	}
	///Get the X.509 Name of the trust anchor.
	pub fn get_name(&self) -> &[u8] { &self.name }
	///Get the X.509 SubjectPublicKeyInfo of the trust anchor.
	pub fn get_subject_public_key_info(&self) -> &[u8] { &self.pubkey }
}


///Configuration.
///
///This structure contains various configuration that is shared among many connections.
#[derive(Clone)]
pub struct Configuration
{
	trust_anchors: BTreeMap<Vec<u8>, Vec<u8>>,
	alpn_list: Vec<Vec<u8>>,
	mandatory: u32,
	allow_cr: bool,
}

impl Configuration
{
	///Create new default configuration.
	///
	///By default:
	///
	/// * There are no trust anchors.
	/// * ALPN extension is not sent.
	/// * There are no optional mandatory features.
	pub fn new() -> Configuration
	{
		Configuration {
			alpn_list: Vec::new(),
			mandatory: 0,
			trust_anchors: BTreeMap::new(),
			allow_cr: false,
		}
	}
	///Add ALPN protocol to list of supported protocols.
	///
	///Adding any ALPN protocls causes the ALPN extension to be sent.
	pub fn add_alpn_protocol(&mut self, alpn: AlpnProtocolId)
	{
		if alpn.is_grease() { return; }		//Do not add GREASE ALPNs.
		self.alpn_list.push(alpn.get().to_vec());
	}
	///Make ALPN mandatory.
	///
	///If ALPN is mandatory and the server does not return ALPN extension, the handshake fails.
	///
	///Note that setting ALPN to be mandatory without adding any ALPN protcols to list of supported protocols
	///causes handshake to always fail.
	pub fn mandatory_alpn(&mut self) { self.mandatory |= MANDATORY_ALPN; }
	///Make Extended Master Secret mandatory.
	///
	///If Extended Master Secret is mandatory, and the server does not use EMS or TLS 1.3, the handshake
	///fails.
	pub fn mandatory_extended_master_secret(&mut self) { self.mandatory |= MANDATORY_EMS; }
	///Make TLS 1.3 mandatory.
	///
	///If TLS 1.3 is mandatory, if server does not support TLS 1.3, the handshake fails.
	pub fn mandatory_tls13(&mut self) { self.mandatory |= MANDATORY_TLS13; }
	///Make Top-Secret grade crypto mandatory.
	///
	///This increases key exchange and encryption requirements to at least 192 bits.
	///
	///The following algorithms are supported in this mode:
	///
	/// * AES-256-GCM
	/// * CHACHA20-POLY1305-AEAD
	/// * ECDH with NIST P-384
	/// * ECDH with NIST P-521
	/// * X448
	///
	pub fn mandatory_high_encryption(&mut self) { self.mandatory |= MANDATORY_HIGH_ENC; }
	///Make NSA mode mandatory.
	pub fn mandatory_nsa_mode(&mut self) { self.mandatory |= MANDATORY_NSA; }
	///Allow certificate requests.
	///
	///This causes handshake to be blocked if certificate request arrives, instead of auto-rejecting the
	///request.
	pub fn allow_certificate_requests(&mut self) { self.allow_cr = true; }
	///Add a trust anchor.
	///
	///For connection to be accepted, the server's certificate must be (indirectly) signed by a trust anchor.
	pub fn add_trust_anchor(&mut self, ta: &TrustAnchor)
	{
		self.trust_anchors.insert(ta.name.clone(), ta.pubkey.clone());
	}
	///Create a new connection.
	///
	///The per-connection parameters include:
	///
	/// * The underlying transport `transport`.
	/// * The host name `host_name`. If IP address is to be used, prefix the IP address textual form with
	///`!`.
	///   * If host name is not an IP address, `server_name` extension is sent. For IP addresses, this extension
	///is omitted.
	/// * The host key whitelist. Any matching host keys will skip certificate validation. If any entries
	///are on the whitelist, `server_certificate_type` extension with `Raw Public Key` type is sent, so the
	///server can skip sending its certificate (instead of sending just its host key).
	///
	///Note that rustc is unable to infer the type parameter `TS`, so use `connect::<ThreadSafe>` or
	///`connect::<NotThreadSafe>`.
	///
	///There is a hack to perform ACME TLS-ALPN-01 validations:
	///
	/// * For ALPN list, add just `AlpnProtocolId::ACME_TLS`.
	/// * For `hostkey_whitelist`, include one `Sha256` entry containing hash of key authorization.
	/// * Handshake the resulting connection. Observe no errors (besides normal `WouldBlock`).
	pub fn connect<TS:Transport>(&self, transport: TS, host_name: &str,
		hostkey_whitelist: &[HostKey]) -> Connection<TS>
	{
		let inner = Client::new(ClientParameters {
			trust_anchors: self.trust_anchors.clone(),
			hostkey_whitelist: hostkey_whitelist.to_vec(),
			alpns: self.alpn_list.clone(),
			server_name: host_name.to_string(),
			mandatory: self.mandatory,
		});
		Connection {
			inner: inner,
			transport: Transport2::new(transport),
			blocked_certificate: None,
			blocked_signature: None,
			handshake_error: None,
			eof_sent: false,
			rx_red: RxRed::new(),
			tx_red: Vec::new(),
			allow_cr: self.allow_cr,
		}
	}
}

///Status flag: Read blocked.
///
///If this is set, no reading of transport will occur. Any operation trying to read from connection will fail
///with error of kind `WouldBlock`.
///
///To clear this status flag, call `unblock_read()`.
pub const STATUS_READ_BLOCKED: u32 = 1<<0;
///Status flag: Write blocked.
///
///If this is set, no writing of transport will occur. Any operation trying to write to connection will fail
///with error of kind `WouldBlock`.
///
///To clear this status flag, call `unblock_write()`.
pub const STATUS_WRITE_BLOCKED: u32 = 1<<1;
///Handshake complete flag.
///
///This flag is set if handshake has completed.
pub const STATUS_HANDSHAKE_COMPLETE: u32 = 1<<2;
///Transport error flag.
///
///This flag is set if underlying transport has failed. There is no way to recover.
pub const STATUS_TRANSPORT_ERROR: u32 = 1<<3;
///Ready to transmit data flag.
///
///This flag is set if data can be transmitted.
pub const STATUS_WRITE_READY: u32 = 1<<4;
///Ready to receive data flag.
///
///This flag is set if data can be received.
pub const STATUS_READ_READY: u32 = 1<<5;
///Blocked for client certificate.
///
///This flag is set if handshake is blocked because client certificate is needed.
pub const STATUS_BLOCKED_CERTIFICATE: u32 = 1<<6;
///Blocked for client certificate verify.
///
///This flag is set if handshake is blocked because client certificate signature is needed.
pub const STATUS_BLOCKED_SIGNATURE: u32 = 1<<7;
///Connection failed flag.
///
///This flag is set if connection has failed. It is always set if STATUS_TRANSPORT_ERROR is set, but it can be
///set for other reasons too (for example, due to certificate validation failure).
pub const STATUS_CONNECTION_FAILED: u32 = 1<<8;
///Read EOF flag.
///
///This flag is set if remote end has closed the stream.
pub const STATUS_READ_EOF: u32 = 1<<9;
///Clean shutdown flag.
///
///This flag is set if connection has been shut down gracefully.
pub const STATUS_CLEAN_SHUTDOWN: u32 = 1<<10;
///Handshaking flag.
///
///This flag is set when handshake is in progress.
pub const STATUS_HANDSHAKING: u32 = 1<<11;
///ALPN available flag.
///
///This flag is set when ALPN is available.
pub const STATUS_AVAILABLE_ALPN: u32 = 1<<12;
///TLS-Extractor available flag.
///
///This flag is set when `TLS-EXTRACTOR` is available.
pub const STATUS_AVAILABLE_EXTRACTOR: u32 = 1<<13;
///Handshake blocked flag.
///
///This flag is set whenever `STATUS_BLOCKED_*` flag is set.
pub const STATUS_BLOCKED: u32 = 1<<14;

///Print status flags.
pub struct PrintStatusFlags(pub u32);

fn show_flag(v: u32, m: u32, name: &str, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
{
	if v & m != 0 {
		if v & m - 1 != 0 { f.write_str(",")?; }
		f.write_str(name)?;
	}
	Ok(())
}

impl core::fmt::Display for PrintStatusFlags
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		show_flag(self.0, STATUS_READ_BLOCKED, "READ_BLOCK", f)?;
		show_flag(self.0, STATUS_WRITE_BLOCKED, "WRITE_BLOCK", f)?;
		show_flag(self.0, STATUS_HANDSHAKE_COMPLETE, "HS_DONE", f)?;
		show_flag(self.0, STATUS_TRANSPORT_ERROR, "TS_ERROR", f)?;
		show_flag(self.0, STATUS_WRITE_READY, "WR_READY", f)?;
		show_flag(self.0, STATUS_READ_READY, "RD_READY", f)?;
		show_flag(self.0, STATUS_BLOCKED_CERTIFICATE, "BLOCK_CERT", f)?;
		show_flag(self.0, STATUS_BLOCKED_CERTIFICATE, "BLOCK_SIG", f)?;
		show_flag(self.0, STATUS_CONNECTION_FAILED, "CONN_FAILED", f)?;
		show_flag(self.0, STATUS_READ_EOF, "EOF", f)?;
		show_flag(self.0, STATUS_CLEAN_SHUTDOWN, "SHUTDOWN", f)?;
		show_flag(self.0, STATUS_HANDSHAKING, "HANDSHAKE", f)?;
		show_flag(self.0, STATUS_AVAILABLE_ALPN, "ALPN", f)?;
		show_flag(self.0, STATUS_AVAILABLE_EXTRACTOR, "EXTRACTOR", f)?;
		show_flag(self.0, STATUS_BLOCKED, "BLOCKED", f)?;
		Ok(())
	}
}

///Any transport.
///
///This type is useful for "washing away" the underlying type of transport.
pub struct AnyTransport(Box<dyn Transport+'static>);

impl AnyTransport
{
	///Create new object.
	pub fn new(transport: impl Transport+'static) -> AnyTransport
	{
		AnyTransport(Box::new(transport))
	}
}

impl Transport for AnyTransport
{
	fn read(&mut self, x: &mut [u8]) -> Result<usize, Error> { self.0.read(x) }
	fn write(&mut self, x: &[u8]) -> Result<usize, Error> { self.0.write(x) }
}

///Any thread-safe transport.
///
///This type is useful for "washing away" the underlying type of transport.
pub struct AnyTransportTS(Box<dyn Transport+Send+Sync+'static>);

impl AnyTransportTS
{
	///Create new object.
	pub fn new(transport: impl Transport+Send+Sync+'static) -> AnyTransport
	{
		AnyTransport(Box::new(transport))
	}
}

impl Transport for AnyTransportTS
{
	fn read(&mut self, x: &mut [u8]) -> Result<usize, Error> { self.0.read(x) }
	fn write(&mut self, x: &[u8]) -> Result<usize, Error> { self.0.write(x) }
}

///Connection.
///
///This is client end of a TLS connection. Data can be read and written by using the `std::io` `Read` and
///`Write` traits.
///
///Notes on read/write behavior:
///
/// * Writes are record-buffered. To ensure that data is promptly sent, call the `Write::flush()` method.
/// * If there is pending application data, `Read::read()` will not attempt to read the connection, instead, it
///will satisfy requrest from buffer, regardless if the read will be short.
/// * read/write both return `WouldBlock` status from the transport.
/// * If connection has not yet been handshaked, read/write will drive handshake to completion.
/// * The status flags `STATUS_READ_BLOCKED` and `STATUS_WRITE_BLOCKED` can be used as signals to add the file
///descriptor to `epoll()`, even in edge triggered mode, as those are only set after `WouldBlock` has been seen.
///
///If one does not want the type of transport to leak through, one can use the `AnyTransport` and `AnyTransportTS`
///types. This has cost of extra layer of indirection.
pub struct Connection<TS:Transport>
{
	//The underlying transport.
	transport: Transport2<TS>,
	//The underlying record layer and handshake state machine.
	inner: Client,
	//Connection error message (for all non-I/O errors).
	handshake_error: Option<String>,
	//Pending certificate request.
	blocked_certificate: Option<ClientCertificateRequest>,
	//Pending signature request.
	blocked_signature: Option<ClientSignatureRequest>,
	//Buffers.
	eof_sent: bool,
	rx_red: RxRed,
	tx_red: Vec<u8>,
	//Allow certificate requets.
	allow_cr: bool,
}

impl<TS:Transport> Connection<TS>
{
	fn __handle_fatal_error(&mut self, e: (Vec<u8>, String)) -> Result<(), Error>
	{
		//Can assume urgent data field is empty, as code should not call anywhere that can call here
		//if it is not. transmit_urgent_data() attempts immedate send. If this transports all, return
		//the error so connection properly fails. If not, return Ok(()) so the alert will be sent before
		//the connection fails.
		self.handshake_error = Some(e.1.clone());
		self.transport.transmit_urgent_data(e.0)?;
		if !self.transport.has_urgent_data() { fail_io!(Other e.1); }
		Ok(())
	}
	fn __raise_fatal_error(&mut self, a: Alert, m: String) -> Result<(), Error>
	{
		let alert = self.inner.transmit_fatal_alert(a);
		self.__handle_fatal_error((alert, m))?;
		return Ok(());
	}
	fn __flush_tx_red(&mut self) -> Result<(), Error>
	{
		//First, if there is something in tx_black, try to flush it to wire. This provodes backpressure
		//to regulate the send rate.
		self.transport.write_tcp()?;
		let overhead = self.inner.get_max_overhead();
		if self.tx_red.len() == 0 { return Ok(()); }
		let buf = self.transport.prepare_write(overhead+self.tx_red.len());
		match self.inner.transmit_record(&self.tx_red, buf) {
			Ok((inc, outc)) => {
				self.transport.commit_write(outc);
				discard_vector(&mut self.tx_red, inc);
			},
			Err(err) => {
				//This error is unrecoverable, as it is unlikely that the encryptor will ever
				//work, so no alert can be sent.
				let err = format!("Failed to transmit record: {err}");
				self.handshake_error = Some(err.clone());
				fail!(Error::new(ErrorKind::Other, err));
			},
		}
		//Flush the TX black again to transmit the data just encrypted.
		self.transport.write_tcp()
	}
}

impl<TS:Transport> Connection<TS>
{
	///Get status flags.
	///
	///The following status flags exist:
	///
	/// * `STATUS_READ_BLOCKED`: The underlying transport is blocked for read.
	/// * `STATUS_WRITE_BLOCKED`: The underlying transprot is blocked for write.
	/// * `STATUS_HANDSHAKE_COMPLETE`: The handshake is complete.
	/// * `STATUS_TRANSPORT_ERROR`: The underlying transport is in error state.
	/// * `STATUS_CONNECTION_FAILED`: The connection has failed.
	/// * `STATUS_HANDSHAKING`: Handshake is in progress.
	/// * `STATUS_READ_READY`: Data may be read.
	/// * `STATUS_WRITE_READY`: Data may be written.
	/// * `STATUS_READ_EOF`: The peer has indicated it will not send more data.
	/// * `STATUS_CLEAN_SHUTDOWN`: The connection has been cleanly shut down (mutual secure EOF).
	/// * `STATUS_BLOCKED_CERTIFICATE`: The connection is blocked because missing certificate.
	/// * `STATUS_BLOCKED_SIGNATURE`: The connection is blocked because missing signature.
	/// * `STATUS_AVAILABLE_ALPN`: `ALPN` is available.
	/// * `STATUS_AVAILABLE_EXTRACTOR`: `TLS-EXTRACTOR` is available.
	/// * `STATUS_BLOCKED`: `STATUS_BLOCKED_CERTIFICATE` or `STATUS_BLOCKED_SIGNATURE`.
	pub fn get_status(&self) -> u32
	{
		let mut flags = 0;
		let rflags = self.inner.get_status_flags();
		flags |= self.transport.transport_flags();
		if rflags.fatal_error { flags |= STATUS_CONNECTION_FAILED; }
		if RxTxPhase::Application == rflags.tx { flags |= STATUS_WRITE_READY; }
		if RxTxPhase::Application == rflags.rx { flags |= STATUS_READ_READY; }
		if RxTxPhase::Shutdown == rflags.rx { flags |= STATUS_READ_EOF; }
		if rflags.rx == rflags.tx && rflags.rx == RxTxPhase::Shutdown { flags |= STATUS_CLEAN_SHUTDOWN; }
		match rflags.handshake {
			HandshakePhase::Running => flags |= STATUS_HANDSHAKING,
			HandshakePhase::Blocked => {
				flags |= STATUS_BLOCKED;
				if self.blocked_certificate.is_some() { flags |= STATUS_BLOCKED_CERTIFICATE; }
				if self.blocked_signature.is_some() { flags |= STATUS_BLOCKED_SIGNATURE; }
			},
			HandshakePhase::Complete => flags |= STATUS_HANDSHAKE_COMPLETE,
			HandshakePhase::AcmeComplete => return STATUS_CLEAN_SHUTDOWN,
			HandshakePhase::Failed => flags |= STATUS_CONNECTION_FAILED,
		}
		let cinfo = self.inner.get_connection_info();
		if cinfo.client_get_alpn().is_some() { flags |= STATUS_AVAILABLE_ALPN; }
		if rflags.extractor_ready { flags |= STATUS_AVAILABLE_EXTRACTOR; }
		flags
	}
	///Clear STATUS_READ_BLOCKED.
	pub fn unblock_read(&mut self) { self.transport.unblock_read(); }
	///Clear STATUS_WRITE_BLOCKED.
	pub fn unblock_write(&mut self) { self.transport.unblock_write(); }
	///Do send end-of-data.
	///
	///After sending end-of-data, no further data can be sent. If there is any pending unflushed data,
	///that data is flushed before sending end of the data.
	///
	///If this returns WouldBlock, call again (after unblocking writes) until it does not.
	pub fn write_eof(&mut self) -> Result<(), Error>
	{
		if !self.__check_tx_ready() { fail_io!(Other "Can not send EOF without complete handshake"); }
		//Definitely do not want any pending application data when sending EOF.
		while self.tx_red.len() > 0 { self.__flush_tx_red()?; }
		if !self.eof_sent {
			self.eof_sent = true;
			let eof = self.inner.transmit_eof();
			self.transport.transmit_urgent_data(eof)?;
		}
		//If not completed last time, try transmitting rest of EOF. This is needed even on the first
		//round, as transmit_urgent_data() can silently truncate the data it sends.
		self.transport.write_urgent()
	}
	fn __do_transmit_flight(&mut self) -> Result<(), Error>
	{
		match self.inner.transmit_flight() {
			Ok(TransmitReturn::Transmit(data)) => self.transport.transmit_urgent_data(data)?,
			Ok(TransmitReturn::Blocked(b)) => {
				match b {
					//Autoreject certificate requests.
					ClientBlocked::RequestCertificate(_) if !self.allow_cr => {
						self.inner.get_handshake().reject_certificate_request();
					},
					ClientBlocked::RequestCertificate(x) => self.blocked_certificate = Some(x),
					ClientBlocked::RequestSignature(x) => self.blocked_signature = Some(x),
				};
				fail_io!(WouldBlock "Handshake blocked");
			},
			//This is unknown error, as __do_transmit_flight() should not have been called with
			//pending failure.
			Ok(TransmitReturn::Error) => fail_io!(Other "Handshake failed with unknown error"),
			Err(x) => return self.__handle_fatal_error(x)
		}
		Ok(())
	}
	fn __do_unblocked_handshake_rx(&mut self) -> Result<(), Error>
	{
		//Never need to care about data returned: Records are processed one at a time,
		//and record that finishes the handshake in receive direction can never carry
		//data, and data is disallowed before that.
		match self.transport.next_record() {
			RxRecordResult::Eof(_) => {
				//EOF during handshake is a fatal transport error. Especially since this means
				//that all the needed handshake messages will never arrive.
				return self.transport.set_error(Error::new(ErrorKind::UnexpectedEof,
					"Received EOF during handshake"));
			},
			RxRecordResult::TooBig => return self.__raise_fatal_error(Alert::RECORD_OVERFLOW,
				format!("Received record too large")),
			//Fill the buffer more. The execution will loop back to call to this function.
			RxRecordResult::Wait => self.transport.read_tcp()?,
			RxRecordResult::Record(record, rsize) => {
				//rx_red acts as a scratch buffer.
				let buf = self.rx_red.prepare_write(record.len());
				let r = self.inner.receive_record(record, buf);
				self.transport.discard_record(rsize);
				self.rx_red.commit_write(0);
				match r {
					Ok(None) => self.rx_red.commit_eof(),
					Ok(Some(r)) => if r > 0 {
						//This should not happen: Data should not arrive before handshake
						//completes enough for this to stop getting called.
						return self.__raise_fatal_error(Alert::UNEXPECTED_MESSAGE,
							format!("Application data received during hs"));
					},
					Err(e) => self.__handle_fatal_error(e)?
				}
			},
		}
		Ok(())
	}
	fn __check_tx_ready(&self) -> bool
	{
		let rstatus = self.inner.get_status_flags();
		matches!(rstatus.tx, RxTxPhase::Application|RxTxPhase::Shutdown)
	}
	///Drive the handshake towards completion.
	///
	///This method is useful for two things:
	///
	/// * If operating in nonblocking mode: After handshake has completed, if transport indicates readability,
	///one can attempt to read the connection, and similarly for writability and writing.
	/// * If using multiple ALPN values, handshake needs to be far enough to use `get_alpn_protocol()` to
	///get the used protocol.
	pub fn handshake(&mut self) -> Result<(), Error>
	{
		//Loop this until handshake finishes, something throws would blocked or error.
		loop {
			self.transport.write_urgent()?;
			//If handshake has failed, return the error.
			if let Some(err) = self.handshake_error.as_ref() { fail_io!(Other err.clone()); }
			self.transport.check_error()?;
			let status = self.inner.get_status_flags();
			//These five conditions are mutually exclusive.
			if status.handshake == HandshakePhase::Complete {
				return Ok(());		//Done!
			} else if status.handshake == HandshakePhase::AcmeComplete {
				return Ok(());		//Done!
			} else if status.handshake == HandshakePhase::Failed {
				//This is unknown error, because some call should have returned Err(_), which
				//then would get written to handshake_error, causing the error exit above.
				fail_io!(Other "Handshake failed with unknown error");
			} else if status.tx == RxTxPhase::Handshake || status.handshake == HandshakePhase::Blocked {
				//Only exit loop on error.
				self.__do_transmit_flight()?;
			} else if status.rx == RxTxPhase::Handshake {
				//Only exit loop on error.
				self.__do_unblocked_handshake_rx()?;
			} else {
				fail_io!(Other "Bad record layer status {status:?}");
			}
		}
	}
	///Get the ALPN protocol active on connection.
	///
	///This is the ALPN explicity designated by server, or `None` if no protocol has been designated by the
	///server.
	///
	///One should check that connection status flag `STATUS_AVAILABLE_ALPN` is set first, as otherwise this
	///will always return `None`.
	pub fn get_alpn<'a>(&'a self) -> Option<AlpnProtocolId<'a>>
	{
		Some(AlpnProtocolId::new(self.inner.get_connection_info().client_get_alpn()??))
	}
	///Invoke TLS-EXTRACTOR.
	///
	///Note that this is only available if Extended Master Secret or TLS version 1.3 has been negotiated.
	///For this, it is useful to call `mandatory_extended_master_secret()` in connection if this API is to be
	///used. Calling it otherwise results an error.
	///
	///The handshake also has to have reached far enough. Use status flag `STATUS_AVAILABLE_EXTRACTOR`
	///to determine this.
	///
	///The material filled in buffer is suitable for both keying symmetric ciphers and signing/MACing for
	///authentication. It is not possible for Man-in-the-Middle to cause both endpoints to compute the same
	///outputs.
	///
	///Note that while in TLS 1.2, absent context (`None`) and empty context (`Some(b""))` are not the same,
	///in TLS 1.3 they are. For this reason, TLS 1.3 requires each label to specify if there is context or
	///not.
	///
	///In TLS 1.3, the size of output is limited to 255 hash output blocks (usually 8,160 bytes), and results
	///of different sizes are independent. In TLS 1.2, there is no limit on output length, but shorter outputs
	///are prefixes of longer outputs.
	pub fn tls_extractor(&self, label: &str, context: Option<&[u8]>, buffer: &mut [u8]) -> Result<(), ()>
	{
		self.inner.tls_extractor(label, context, buffer)
	}
	///Query certificate request parameters.
	///
	///This only makes sense if `STATUS_BLOCKED_CERTIFICATE` is set. Otherwise this always returns `None`.
	pub fn blocked_certificate(&self) -> Option<BlockedCertificate>
	{
		self.blocked_certificate.as_ref().map(|b|BlockedCertificate {
			signature_algorithms: SignatureAlgorithmSet2::from_set_of(b.signatures),
			signature_algorithms_cert: SignatureAlgorithmSet2::from_set_of(b.signatures_cert),
			_x: std::marker::PhantomData
		})
	}
	///Query signature parameters.
	///
	///This only makes sense if `STATUS_BLOCKED_SIGNATURE` is set. Otherwise this always returns `None`.
	pub fn blocked_signature(&self) -> Option<BlockedSignature>
	{
		self.blocked_signature.as_ref().map(|b|BlockedSignature {
			pubkey_sha256: b.pubkey_hash,
			data_to_sign: &b.tbs,
			_x: ()
		})
	}
	///Reject certificate request.
	///
	///This clears the `STATUS_BLOCKED_CERTIFICATE` flag, and only makes sense with it set.
	///
	///Note that this is automatically called if `allow_certificate_requests()` has not been set in
	///configuration, and certificate request arrives.
	pub fn unblock_certificate_reject(&mut self)
	{
		self.inner.get_handshake().reject_certificate_request();
		self.blocked_certificate = None;
	}
	///Accept certificate request and send certificate.
	///
	///This clears the `STATUS_BLOCKED_CERTIFICATE` flag, and only makes sense with it set.
	///
	///Note that this does not immediately set `STATUS_BLOCKED_SIGNATURE`. One must proceed with handshake
	///for it to get blocked with that. So it is *not* possible to immedately follow up with call to
	///`blocked_signature()`.
	pub fn unblock_certificate_accept<T:AsRef<[u8]>>(&mut self, certificate: T, chain: &[T]) ->
		Result<(), Error>
	{
		self.inner.get_handshake().accept_certificate_request(certificate, chain).
			map_err(|e|Error::new(ErrorKind::Other, e))?;
		self.blocked_certificate = None;
		Ok(())
	}
	///Send signature.
	///
	///The algorithm used MUST be one of the algorithms in `signature_algorithms` set in certificate request.
	///
	///This clears the `STATUS_BLOCKED_SIGNATURE` flag, and only makes sense with it set.
	pub fn unblock_signature(&mut self, algo: btls_aux_tls_iana::SignatureScheme, sig: &[u8])
	{
		self.inner.get_handshake().sign_client_certificate(algo, sig);
		self.blocked_signature = None;
	}
	///Read HTTP textual line.
	///
	///If the line length exceeds the buffer, returns an error.
	///
	///The line is not allowed to contain any control characters except HT, and is not allowed to start with
	///a whitespace.
	pub fn read_http_textual_line(&mut self, line: &mut [u8]) -> Result<Option<usize>, Error>
	{
		//Drive handshake to completion, if it has not already.
		self.handshake()?;
		//Loop until line is complete.
		let mut scanpos = 0;
		let mut last_cr = false;
		loop {
			//Rx Red has the first priority to satisfy the request. For error idempotency, nothing
			//can be removed from rx_red until line has completed. If at EOF condition, signal that.
			if self.rx_red.at_end() { return Ok(None); }
			let rx_red = self.rx_red.peek_all();
			while scanpos < rx_red.len() {
				let b = rx_red[scanpos];
				if last_cr {
					//CRLF => End line.
					if b == 10 { break; }
					http_error!(BadLineEnd);
				}
				match b {
					//Whitespace not allowed at start of line.
					9|32 => if scanpos == 0 { http_error!(StartWhitespace); },
					//CR.
					13 => last_cr = true,
					0..=31|127 => http_error!(IllegalByte b),
					//Others are ok.
					_ => ()
				}
				//If last_cr is not set, copy the byte into buffer. Line is too long if this
				//fails.
				if !last_cr {
					if scanpos >= line.len() { http_error!(LineTooLong); }
					line[scanpos] = b;
				}
				scanpos += 1;
			}
			//If scanpos is within rx_red, that means line completed. The character at scanpos is
			//always LF, so consume it too. The actual line length is scanpos-1 (scanpos is always.
			//positive).
			if scanpos < rx_red.len() {
				self.rx_red.consume(scanpos + 1);
				return Ok(Some(scanpos-1));
			}
			//If pending EOF is set, this line will never complete, and it is not empty either.
			//Signal an error.
			if self.rx_red.pending_eof() { fail_io!(UnexpectedEof "Line truncated by EOF"); }
			//There is no complete line. Need more input.
			match self.transport.next_record() {
				RxRecordResult::Eof(_) => {
					//Do usual check on if EOF is allowed here.
					if let Err(err) = self.inner.receive_eof() {
						self.transport.set_error(Error::new(ErrorKind::UnexpectedEof,
							err))?;
					}
					//Otherwise ignore the TCP EOF condition. The RX Red EOF handling does
					//pick on TLS-level EOF.
				},
				RxRecordResult::TooBig => {
					let msg = format!("Received record too large");
					self.__raise_fatal_error(Alert::RECORD_OVERFLOW, msg)?;
					self.transport.raise_write_block()?;
					//It is actually impossible to reach here. Either __raise_fatal_error
					//completes the write and returns an error, or it does not because of
					//pending block, and then raise_write_block() raises an error.
				},
				//Need more data, damnit.
				RxRecordResult::Wait => self.transport.read_tcp()?,
				RxRecordResult::Record(record, rsize) => {
					//Direct read is never possible, rx_red is always acting as buffer.
					let buf = self.rx_red.prepare_write(record.len());
					let r =  self.inner.receive_record(record, buf);
					if let Ok(Some(r)) = r { self.rx_red.commit_write(r); }
					match r {
						Ok(Some(_)) => (),
						Ok(None) => self.rx_red.commit_eof(),
						Err(e) => {
							self.__handle_fatal_error(e)?;
							self.transport.raise_write_block()?;
							//See comment in TooBig branch.
						}
					};
					self.transport.discard_record(rsize);
				}
			}
		}
	}
}

///Signature parameters.
///
///This structure carries information about proving possession of the client certificate private key.
///In practicular, what data to sign, and with what key.
#[derive(Clone)]
pub struct BlockedSignature<'a>
{
	///SHA-256 hash of the public key.
	pub pubkey_sha256: [u8;32],
	///The data to sign.
	pub data_to_sign: &'a [u8],
	_x: ()
}

///Certificate request parameters.
///
///This structure carries information about a request for client to send a client certificate. In practicular, what
///are the acceptable signature algorithms.
#[derive(Clone)]
pub struct BlockedCertificate<'a>
{
	///The acceptable proof of posession signature algorithms.
	///
	///This list is absolute: Only these algorithms are acceptable, the rest are not.
	pub signature_algorithms: SignatureAlgorithmSet2,
	///The supported certificate signature algorithms.
	///
	///This list is advisory: The server may support other algorithms not expressible with TLS signature
	///schemes, or not actually need to verify the certificate signature. In practicular, signature algorithms
	///of any self-signed certificates should be ignored.
	pub signature_algorithms_cert: SignatureAlgorithmSet2,
	_x: std::marker::PhantomData<&'a u8>,
}

fn __advance_output<'a>(buf: &'a mut [u8], r: usize, count: &mut usize) -> &'a mut [u8]
{
	let (head, tail) = match split_at(buf, r) { Ok(x) => x, Err(x) => (x, &mut [][..]) };
	*count += head.len();
	tail
}

impl<TS:Transport> Read for Connection<TS>
{
	fn read(&mut self, mut buf: &mut [u8]) -> Result<usize, Error>
	{
		let mut count = 0;
		//Drive handshake to completion, if it has not already.
		self.handshake()?;
		//Any data in rx_red is first to be read.
		match self.rx_red.read(buf) {
			Some(r) => buf = __advance_output(buf, r, &mut count),
			None => return Ok(0)	//EOF.
		}
		if buf.len() == 0 { return Ok(count); }
		//Then loop over records, returning first data.
		loop { match self.transport.next_record() {
			RxRecordResult::Eof(_) => {
				//Check close is allowed here.
				if let Err(err) = self.inner.receive_eof() {
					self.transport.set_error(Error::new(ErrorKind::UnexpectedEof, err))?;
				}
				//This is EOF, so zero is correct value if nothing has been read so
				//far. If this is nonzero, next call will get the EOF.
				return Ok(count);
			},
			RxRecordResult::TooBig => {
				let msg = format!("Received record too large");
				self.__raise_fatal_error(Alert::RECORD_OVERFLOW, msg)?;
				self.transport.raise_write_block()?;
				//It is actually impossible to reach here. Either __raise_fatal_error completes
				//the write and returns an error, or it does not because of pending block, and
				//then raise_write_block() raises an error.
			},
			//If there is no complete record, but we got data, return so far.
			RxRecordResult::Wait if count > 0 => return Ok(count),
			RxRecordResult::Wait if self.rx_red.at_end() => return Ok(0),
			//Fill the buffer more. There are no complete records in buffer and no data
			//to read.
			RxRecordResult::Wait => self.transport.read_tcp()?,
			RxRecordResult::Record(record, rsize) => {
				match if self.rx_red.is_empty() && buf.len() >= record.len() {
					//Direct read.
					let r = self.inner.receive_record(record, buf);
					if let Ok(Some(r)) = r { buf = __advance_output(buf, r, &mut count); }
					r
				} else {
					//rx_red acts as a scratch buffer.
					let buf = self.rx_red.prepare_write(record.len());
					let r =  self.inner.receive_record(record, buf);
					if let Ok(Some(r)) = r { self.rx_red.commit_write(r); }
					r
				} {
					Ok(Some(_)) => (),
					Ok(None) => self.rx_red.commit_eof(),
					Err(e) => {
						self.__handle_fatal_error(e)?;
						self.transport.raise_write_block()?;
						//See comment in TooBig branch.
					}
				};
				self.transport.discard_record(rsize);
				//Try reading from rx_red.
				match self.rx_red.read(buf) {
					Some(r) => buf = __advance_output(buf, r, &mut count),
					None => return Ok(count)	//EOF.
				}
				//If buffer has been emptied, read is done.
				if buf.len() == 0 { return Ok(count); }
			},
		}}
	}
}

impl<TS:Transport> Write for Connection<TS>
{
	fn write(&mut self, mut buf: &[u8]) -> Result<usize, Error>
	{
		//Drive handshake to completion, if it has not already.
		if !self.__check_tx_ready() { self.handshake()?; }
		if self.eof_sent { return Ok(0); }
		let mtu = self.inner.get_mtu();
		let mut count = 0;
		while buf.len() > 0 {
			let (c, n) = slice_to_vector(&mut self.tx_red, buf, mtu);
			buf = buf.get(n..).unwrap_or(&[]);	//Rest of buffer.
			count += n;
			if c.is_lt() { return Ok(count); }	//Not full.
			//If __flush_tx_red() fails, it fails so badly that it is not just catastrophic,
			//but so badly that no error can be sent.
			match self.__flush_tx_red() {
				Ok(_) => (),
				Err(e) => if e.kind() == ErrorKind::WouldBlock && count > 0 {
					return Ok(count);	//Accepted so far.
				} else {
					fail!(e);	//EWOULDBLOCK or I/O error.
				}
			}
		}
		Ok(count)
	}
	fn flush(&mut self) -> Result<(), Error>
	{
		//Drive handshake to completion, if it has not already.
		if !self.__check_tx_ready() { self.handshake()?; }
		self.__flush_tx_red()?;		//Ignore delayed error / no.
		Ok(())
	}
}



#[cfg(test)] mod test;
