use super::*;
use btls_aux_memory::EscapeByteString;
use std::io::Write;
use std::io::Read;


fn __is_send(_: &dyn Send) {}
fn __is_sync(_: &dyn Sync) {}

#[allow(unreachable_code)]
fn __thread_safe_connection_is_thread_safe()
{
	let _c: &Connection<std::net::TcpStream> = return;
	__is_send(_c);
	__is_sync(_c);
}

#[allow(unreachable_code)]
fn __thread_safe_connection_is_thread_safe2()
{
	let _c: &Connection<AnyTransportTS> = return;
	__is_send(_c);
	__is_sync(_c);
}

static TEST_VALIDATOR: HostKey = HostKey::Sha256([
	0xa5,0x80,0x09,0x30,0x00,0x7a,0x4d,0x78,0x20,0xad,0xf6,0xbf,0xee,0x67,0x5b,0xad,
	0x39,0xfa,0xe0,0x58,0x5c,0xa9,0x0a,0xbe,0x70,0x78,0x7a,0x34,0x5c,0x8e,0xc9,0xe5
]);


#[test]
fn lsnes_tas_bot_mder()
{
	let tcp = std::net::TcpStream::connect(("lsnes.tas.bot",453)).unwrap();
	let mut config = Configuration::new();
	let mut talist: &[u8] = include_bytes!("isrg-roots.mder");
	let talist = TrustAnchor::from_read(&mut talist).unwrap();
	for ta in talist.iter() { config.add_trust_anchor(ta); }
	let mut conn = config.connect(tcp, "lsnes.tas.bot", &[/*TEST_VALIDATOR.clone()*/]);
	conn.handshake().expect("Handshake failed");
println!("Handshake done!");
	let req = "GET / HTTP/1.1\r\nhost:lsnes.tas.bot\r\nconnection:close\r\n\r\n";
	conn.write_all(req.as_bytes()).expect("Write failed");
	//conn.flush().expect("Flussh failed");
	conn.write_eof().expect("Send eof failed");
	loop {
		let mut buf = [0;512];
		let r = conn.read(&mut buf).expect("Read failed");
		if r == 0 { break; }
		print!("{buf}", buf=String::from_utf8_lossy(&buf[..r]));
	}
	println!("Final status flags: {fstatus:x}", fstatus=conn.get_status());
}

#[test]
fn lsnes_tas_bot()
{
	let tcp = std::net::TcpStream::connect(("lsnes.tas.bot",453)).unwrap();
	let mut config = Configuration::new();
	let talist = TrustAnchor::from_file("/etc/ssl/certs/ISRG_Root_X1.pem").unwrap();
	for ta in talist.iter() { config.add_trust_anchor(ta); }
	let mut conn = config.connect(tcp, "lsnes.tas.bot", &[/*TEST_VALIDATOR.clone()*/]);
	conn.handshake().expect("Handshake failed");
println!("Handshake done!");
	let req = "GET / HTTP/1.1\r\nhost:lsnes.tas.bot\r\nconnection:close\r\n\r\n";
	conn.write_all(req.as_bytes()).expect("Write failed");
	//conn.flush().expect("Flussh failed");
	conn.write_eof().expect("Send eof failed");
	loop {
		let mut buf = [0;512];
		let r = conn.read(&mut buf).expect("Read failed");
		if r == 0 { break; }
		print!("{buf}", buf=EscapeByteString(&buf[..r]));
	}
	println!("Final status flags: {fstatus:x}", fstatus=conn.get_status());
}

#[test]
fn lsnes_tas_bot_with_hostkey()
{
	let tcp = std::net::TcpStream::connect(("lsnes.tas.bot",453)).unwrap();
	let config = Configuration::new();
	let mut conn = config.connect(tcp, "lsnes.tas.bot", &[TEST_VALIDATOR.clone()]);
	conn.handshake().expect("Handshake failed");
println!("Handshake done!");
	let req = "GET / HTTP/1.1\r\nhost:lsnes.tas.bot\r\nconnection:close\r\n\r\n";
	conn.write_all(req.as_bytes()).expect("Write failed");
	//conn.flush().expect("Flussh failed");
	conn.write_eof().expect("Send eof failed");
	loop {
		let mut buf = [0;512];
		let r = conn.read(&mut buf).expect("Read failed");
		if r == 0 { break; }
		print!("{buf}", buf=EscapeByteString(&buf[..r]));
	}
	println!("Final status flags: {fstatus:x}", fstatus=conn.get_status());
}
