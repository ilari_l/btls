namelist = [];
fp = open("err-desc.dat","r");
for line in fp:
	line = line.strip().split("\t");
	if len(line) != 2: continue;
	namelist.append(line[0]);
namelist.sort();

for name in namelist:
	print("#ifdef "+name);
	print("if(errcode == "+name+") return \""+name+"\";");
	print("#endif");
