extern crate cc;
use cc::Build;
use std::collections::BTreeMap;
use std::collections::BTreeSet;
use std::env::var;
use std::fmt::Write as FmtWrite;
use std::fs::File;
use std::io::Read;
use std::io::Write as IoWrite;
use std::path::PathBuf;
use std::str::FromStr;

static ERROR_DESC: &'static str = include_str!("err-desc.dat");
static ERRORS_ALPHA: &'static str = include_str!("linux-err.alpha.dat");
static ERRORS_MIPS: &'static str = include_str!("linux-err.mips.dat");
static ERRORS_PARISC: &'static str = include_str!("linux-err.parisc.dat");
static ERRORS_SPARC: &'static str = include_str!("linux-err.sparc.dat");
static ERRORS_GENERIC: &'static str = include_str!("linux-err.other.dat");
static LINUX_ALPHA: &'static str = "alpha";
static LINUX_MIPS: &'static str = "mips mipsel mips64 mips64el mipsisa32r6 mipsisa32r6el mipsisa64r6 mipsisa64r6el";
static LINUX_PARISC: &'static str = "hppa";
static LINUX_SPARC: &'static str = "sparc sparcv9 sparc64 sparcel";


fn strip(x: &str) -> &str
{
	let y = x.as_bytes();
	let mut s = 0;
	let mut e = x.len();
	while s < y.len() && y[s].is_ascii_whitespace() { s += 1; }
	while e > s && y[e-1].is_ascii_whitespace() { e -= 1; }
	&x[s..e]
}

fn parse_numeric(x: &str) -> Option<i32>
{
	if let Some(x) = x.strip_prefix("0x") {
		i32::from_str_radix(x, 16).ok()
	} else if let Some(x) = x.strip_prefix("0") {
		i32::from_str_radix(x, 8).ok()
	} else {
		i32::from_str_radix(x, 10).ok()
	}
}

fn copy_entry(m: &mut BTreeMap<String, i32>, from: &str, to: &str)
{
	if m.contains_key(to) { return; }
	if let Some(&v) = m.get(from) { m.insert(to.to_string(), v); };
}


fn copy_entry2(m: &mut BTreeMap<String, i32>, a: &str, b: &str)
{
	copy_entry(m, a, b);
	copy_entry(m, b, a);
}

fn fixup_error_map(m: &mut BTreeMap<String, i32>)
{
	//Some aliases.
	copy_entry2(m, "EDEADLK", "EDEADLOCK");
	copy_entry2(m, "ECONNREFUSED", "EREFUSED");
	copy_entry2(m, "ECANCELED", "ECANCELLED");
	//Now, assign codes for all not found.
	let mut c = 314_159_265;
	for_each_error_symbol(|sym|if !m.contains_key(sym) {
		m.insert(sym.to_string(), c);
		c += 1;
	});
}

fn for_each_error_symbol(mut f: impl FnMut(&'static str))
{
	for sym in ERROR_DESC.lines() {
		let sym: Vec<_> = sym.split("\t").collect();
		if sym.len() != 2 { continue; }
		let sym = sym[0];
		f(sym);
	}
}

fn to_string_strip_non_ascii(x: &[u8]) -> String
{
	x.iter().filter(|&&x|x<128).map(|&x|x as char).collect()
}

fn get_error_map() -> BTreeMap<String, i32>
{
	let mut out = BTreeMap::new();
	let out_dir = var("OUT_DIR").unwrap();
	let mut f = File::create(&PathBuf::from(&out_dir).join("errors.c")).unwrap();
	writeln!(f, "#include <errno.h>").unwrap();
	for_each_error_symbol(|sym|writeln!(f, "const int err_{} = {};", sym, sym).unwrap());
	drop(f);
	let mut config = Build::new();
	config.file(&format!("{}/errors.c", out_dir));
	let data = config.expand();
	//Just strip anything that is not ASCII, as non-ASCII stuff can only be in comments or strings, neither of
	//which need to be preserved.
	let data = to_string_strip_non_ascii(&data);
	let mut data2 = String::new();
	for line in data.lines() {
		let line = strip(line);
		if line == "" || line.starts_with("#") { continue; }
		data2.push_str(line);
		if line.ends_with(";") { data2.push('\n'); }
	}
	for line in data2.lines() {
		if let Some(line) = line.strip_prefix("const int err_") {
			if let Some(p) = line.find("=") {
				//The -1 is to strip the ending ;.
				let name = strip(&line[..p]);
				let value = strip(&line[p+1..line.len()-1]);
				if let Some(value) = parse_numeric(value) {
					out.insert(name.to_string(), value);
				}
			}
		}
	}
	fixup_error_map(&mut out);
	out
}

fn get_error_desc_map() -> BTreeMap<&'static str, &'static str>
{
	let mut out = BTreeMap::new();
	for line in ERROR_DESC.lines() {
		let line = strip(line);
		if let Some(p) = line.find("\t") {
			out.insert(&line[..p], &line[p+1..]);
		}
	}
	out
}

fn lookup_help(n: &str) -> String
{
	let mut f = File::open("err-desc.dat").unwrap();
	let mut content = String::new();
	f.read_to_string(&mut content).unwrap();
	for line in content.lines() {
		if let Some(p) = line.find("\t") {
			let sym = &line[..p];
			let help = &line[p+1..];
			if n == sym { return help.to_string(); }
		}
	}
	format!("Unknown error {}", n)
}

fn flatten<T:Clone>(x: &[Vec<T>]) -> Vec<T>
{
	let mut out = Vec::new();
	for i in x.iter() {
		for j in i.iter() {
			out.push(j.clone());
		}
	}
	out
}

fn to_arch(elem: &str) -> String
{
	format!(r#"target_arch="{}""#, elem)
}

fn to_arch_list<T:AsRef<str>>(array: &[T]) -> String
{
	if array.len() == 1 {
		to_arch(array[0].as_ref())
	} else {
		let mut s = "any(".to_string();
		s.push_str(&to_arch(array[0].as_ref()));
		for i in array.iter() {
			s.push(',');
			s.push_str(&to_arch(i.as_ref()));
		}
		s.push(')');
		s
	}
}

fn write_arch_errors(fp: &mut File, errors: &'static str, archs: &'static str,
	desc: &BTreeMap<&'static str, &'static str>)
{
	let archs: String = if archs == "" {
		let a: Vec<&'static str> = LINUX_ALPHA.split_whitespace().collect();
		let b: Vec<&'static str> = LINUX_MIPS.split_whitespace().collect();
		let c: Vec<&'static str> = LINUX_PARISC.split_whitespace().collect();
		let d: Vec<&'static str> = LINUX_SPARC.split_whitespace().collect();
		let archs = flatten(&[a, b, c, d]);
		format!("not({})", to_arch_list(&archs))
	} else {
		let archs: Vec<&'static str> = archs.split_whitespace().collect();
		to_arch_list(&archs)
	};
	let mut symbols: BTreeSet<&'static str> = BTreeSet::new();
	let mut shortdesc: BTreeMap<i32, &'static str> = BTreeMap::new();
	let mut longdesc: BTreeMap<i32, &'static str> = BTreeMap::new();
	let mut const_defs = String::new();
	for line in errors.lines() {
		let line: Vec<&'static str> = strip(line).split_whitespace().collect();
		if line.len() != 2 { continue; }
		let desc = desc.get(line[1]).expect("No description");
		let val = i32::from_str(line[0]).expect("Not numeric");
		shortdesc.insert(val, line[1]);
		longdesc.insert(val, desc);
		symbols.insert(line[1]);

		writeln!(const_defs, "\t///{}", desc).unwrap();
		writeln!(const_defs, "\tpub const {}: OsError = OsError({});", line[1], val).unwrap();

	}
	let mut short_descs = String::new();
	let mut long_descs = String::new();
	for ((&val, short), (_, long)) in shortdesc.iter().zip(longdesc.iter()) {
		writeln!(short_descs, "\t\t\t{} => Some(\"{}\"),", val, short).unwrap();
		writeln!(long_descs, "\t\t\t{} => Some(r####\"{}\"####),", val, long).unwrap();
	}

	write!(fp, "\
#[cfg(all(target_os=\"linux\",{archs}))]
impl OsError
{{
{const_defs}
	fn get_short_desc(n: i32) -> Option<&'static str>
	{{
		match n {{
{short_descs}
			_ => None
		}}
	}}
	fn get_long_desc(n: i32) -> Option<&'static str>
	{{
		match n {{
{long_descs}
			_ => None
		}}
	}}
}}
", archs=archs, const_defs=const_defs, short_descs=short_descs, long_descs=long_descs).unwrap();
	let mut gsymbols: BTreeSet<&'static str> = BTreeSet::new();
	for_each_error_symbol(|sym|{gsymbols.insert(sym);});
	let mut fail = 0;
	for item in gsymbols.difference(&symbols) {
		fail += 1;
		eprintln!("Error {} is in global but not local list", item);
	}
	for item in symbols.difference(&gsymbols) {
		fail += 1;
		eprintln!("Error {} is in local but not global list", item);
	}
	if fail > 0 { panic!("Error list mismatch"); }
}

fn main()
{
	let errmap = get_error_map();
	let descmap = get_error_desc_map();
	let out_dir = var("OUT_DIR").unwrap();
	let mut f = File::create(&PathBuf::from(&out_dir).join("autogenerated-errors-linux-alpha.inc.rs")).unwrap();
	write_arch_errors(&mut f, ERRORS_ALPHA, LINUX_ALPHA, &descmap);
	let mut f = File::create(&PathBuf::from(&out_dir).join("autogenerated-errors-linux-mips.inc.rs")).unwrap();
	write_arch_errors(&mut f, ERRORS_MIPS, LINUX_MIPS, &descmap);
	let mut f = File::create(&PathBuf::from(&out_dir).join("autogenerated-errors-linux-parisc.inc.rs")).unwrap();
	write_arch_errors(&mut f, ERRORS_PARISC, LINUX_PARISC, &descmap);
	let mut f = File::create(&PathBuf::from(&out_dir).join("autogenerated-errors-linux-sparc.inc.rs")).unwrap();
	write_arch_errors(&mut f, ERRORS_SPARC, LINUX_SPARC, &descmap);
	let mut f = File::create(&PathBuf::from(&out_dir).join("autogenerated-errors-linux.inc.rs")).unwrap();
	write_arch_errors(&mut f, ERRORS_GENERIC, "", &descmap);
	let mut f = File::create(&PathBuf::from(&out_dir).join("autogenerated-errors-not-linux.inc.rs")).unwrap();
	let reverse: BTreeMap<i32, String> = errmap.iter().map(|(n,&v)|(v, n.to_string())).collect();

	let mut const_defs = String::new();
	for (name, value) in errmap.iter() {
		writeln!(const_defs, "\t///{}", lookup_help(name)).unwrap();
		writeln!(const_defs, "\tpub const {}: OsError = OsError({});", name, value).unwrap();
	}
	let mut short_descs = String::new();
	let mut long_descs = String::new();
	for (value, name) in reverse.iter() {
		writeln!(short_descs, "\t\t\t{} => Some(\"{}\"),", value, name).unwrap();
		writeln!(long_descs, "\t\t\t{} => Some(r####\"{}\"####),", value, lookup_help(name)).unwrap();
	}
	write!(f, "\
#[cfg(not(target_os=\"linux\"))]
impl OsError
{{
{const_defs}
	fn get_short_desc(n: i32) -> Option<&'static str>
	{{
		match n {{
{short_descs}
			_ => None
		}}
	}}
	fn get_long_desc(n: i32) -> Option<&'static str>
	{{
		match n {{
{long_descs}
			_ => None
		}}
	}}
}}
", const_defs=const_defs, short_descs=short_descs, long_descs=long_descs).unwrap();

	let mut config = Build::new();
	config.include(env!("CARGO_MANIFEST_DIR"));
	config.file("src/errno.c");
	config.compile("liberrno.a");
}


