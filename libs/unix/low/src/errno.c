#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>

void unix__call_strerror_r(int errcode, char* buf, size_t bufsize)
{
	if(!buf || !bufsize) return;	//Don't crash even if somebody sends invalid buffer.
	//Don't UB if strerror_r fails.
	buf[0] = 0;
	//The return value of this thing is not important, right?
	strerror_r(errcode, buf, bufsize);
}

int unix__read_last_os_error()
{
	return errno;
}

const char* unix__errsym_reverse_lookup(int errcode)
{
	#include "src/errsym-reverse.inc"
	return NULL;
}

DIR* unix__call_fdopendir(int fd)
{
	return fdopendir(fd);
}

const char* unix__call_readdir(DIR* dirp)
{
	errno = 0;	//NULL can be EOF or error.
	//Assume implementation is not insane and readdir is thread-safe between different streams.
	struct dirent* ent = readdir(dirp);
	return ent ? ent->d_name : NULL;
}

void unix__call_closedir(DIR* dirp)
{
	closedir(dirp);
}

char* unix__call_getenv(const char* name)
{
	return getenv(name);
}
