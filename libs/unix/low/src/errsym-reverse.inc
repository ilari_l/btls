#ifdef E2BIG
if(errcode == E2BIG) return "E2BIG";
#endif
#ifdef EACCES
if(errcode == EACCES) return "EACCES";
#endif
#ifdef EADDRINUSE
if(errcode == EADDRINUSE) return "EADDRINUSE";
#endif
#ifdef EADDRNOTAVAIL
if(errcode == EADDRNOTAVAIL) return "EADDRNOTAVAIL";
#endif
#ifdef EADV
if(errcode == EADV) return "EADV";
#endif
#ifdef EAFNOSUPPORT
if(errcode == EAFNOSUPPORT) return "EAFNOSUPPORT";
#endif
#ifdef EAGAIN
if(errcode == EAGAIN) return "EAGAIN";
#endif
#ifdef EALREADY
if(errcode == EALREADY) return "EALREADY";
#endif
#ifdef EBADCOOKIE
if(errcode == EBADCOOKIE) return "EBADCOOKIE";
#endif
#ifdef EBADE
if(errcode == EBADE) return "EBADE";
#endif
#ifdef EBADF
if(errcode == EBADF) return "EBADF";
#endif
#ifdef EBADFD
if(errcode == EBADFD) return "EBADFD";
#endif
#ifdef EBADHANDLE
if(errcode == EBADHANDLE) return "EBADHANDLE";
#endif
#ifdef EBADMSG
if(errcode == EBADMSG) return "EBADMSG";
#endif
#ifdef EBADR
if(errcode == EBADR) return "EBADR";
#endif
#ifdef EBADRQC
if(errcode == EBADRQC) return "EBADRQC";
#endif
#ifdef EBADSLT
if(errcode == EBADSLT) return "EBADSLT";
#endif
#ifdef EBADTYPE
if(errcode == EBADTYPE) return "EBADTYPE";
#endif
#ifdef EBFONT
if(errcode == EBFONT) return "EBFONT";
#endif
#ifdef EBUSY
if(errcode == EBUSY) return "EBUSY";
#endif
#ifdef ECANCELED
if(errcode == ECANCELED) return "ECANCELED";
#endif
#ifdef ECANCELLED
if(errcode == ECANCELLED) return "ECANCELLED";
#endif
#ifdef ECHILD
if(errcode == ECHILD) return "ECHILD";
#endif
#ifdef ECHRNG
if(errcode == ECHRNG) return "ECHRNG";
#endif
#ifdef ECOMM
if(errcode == ECOMM) return "ECOMM";
#endif
#ifdef ECONNABORTED
if(errcode == ECONNABORTED) return "ECONNABORTED";
#endif
#ifdef ECONNREFUSED
if(errcode == ECONNREFUSED) return "ECONNREFUSED";
#endif
#ifdef ECONNRESET
if(errcode == ECONNRESET) return "ECONNRESET";
#endif
#ifdef EDEADLK
if(errcode == EDEADLK) return "EDEADLK";
#endif
#ifdef EDEADLOCK
if(errcode == EDEADLOCK) return "EDEADLOCK";
#endif
#ifdef EDESTADDRREQ
if(errcode == EDESTADDRREQ) return "EDESTADDRREQ";
#endif
#ifdef EDOM
if(errcode == EDOM) return "EDOM";
#endif
#ifdef EDOTDOT
if(errcode == EDOTDOT) return "EDOTDOT";
#endif
#ifdef EDQUOT
if(errcode == EDQUOT) return "EDQUOT";
#endif
#ifdef EEXIST
if(errcode == EEXIST) return "EEXIST";
#endif
#ifdef EFAULT
if(errcode == EFAULT) return "EFAULT";
#endif
#ifdef EFBIG
if(errcode == EFBIG) return "EFBIG";
#endif
#ifdef EHOSTDOWN
if(errcode == EHOSTDOWN) return "EHOSTDOWN";
#endif
#ifdef EHOSTUNREACH
if(errcode == EHOSTUNREACH) return "EHOSTUNREACH";
#endif
#ifdef EHWPOISON
if(errcode == EHWPOISON) return "EHWPOISON";
#endif
#ifdef EIDRM
if(errcode == EIDRM) return "EIDRM";
#endif
#ifdef EILSEQ
if(errcode == EILSEQ) return "EILSEQ";
#endif
#ifdef EINIT
if(errcode == EINIT) return "EINIT";
#endif
#ifdef EINPROGRESS
if(errcode == EINPROGRESS) return "EINPROGRESS";
#endif
#ifdef EINTR
if(errcode == EINTR) return "EINTR";
#endif
#ifdef EINVAL
if(errcode == EINVAL) return "EINVAL";
#endif
#ifdef EIO
if(errcode == EIO) return "EIO";
#endif
#ifdef EIOCBQUEUED
if(errcode == EIOCBQUEUED) return "EIOCBQUEUED";
#endif
#ifdef EISCONN
if(errcode == EISCONN) return "EISCONN";
#endif
#ifdef EISDIR
if(errcode == EISDIR) return "EISDIR";
#endif
#ifdef EISNAM
if(errcode == EISNAM) return "EISNAM";
#endif
#ifdef EJUKEBOX
if(errcode == EJUKEBOX) return "EJUKEBOX";
#endif
#ifdef EKEYEXPIRED
if(errcode == EKEYEXPIRED) return "EKEYEXPIRED";
#endif
#ifdef EKEYREJECTED
if(errcode == EKEYREJECTED) return "EKEYREJECTED";
#endif
#ifdef EKEYREVOKED
if(errcode == EKEYREVOKED) return "EKEYREVOKED";
#endif
#ifdef EL2HLT
if(errcode == EL2HLT) return "EL2HLT";
#endif
#ifdef EL2NSYNC
if(errcode == EL2NSYNC) return "EL2NSYNC";
#endif
#ifdef EL3HLT
if(errcode == EL3HLT) return "EL3HLT";
#endif
#ifdef EL3RST
if(errcode == EL3RST) return "EL3RST";
#endif
#ifdef ELIBACC
if(errcode == ELIBACC) return "ELIBACC";
#endif
#ifdef ELIBBAD
if(errcode == ELIBBAD) return "ELIBBAD";
#endif
#ifdef ELIBEXEC
if(errcode == ELIBEXEC) return "ELIBEXEC";
#endif
#ifdef ELIBMAX
if(errcode == ELIBMAX) return "ELIBMAX";
#endif
#ifdef ELIBSCN
if(errcode == ELIBSCN) return "ELIBSCN";
#endif
#ifdef ELNRNG
if(errcode == ELNRNG) return "ELNRNG";
#endif
#ifdef ELOOP
if(errcode == ELOOP) return "ELOOP";
#endif
#ifdef EMEDIUMTYPE
if(errcode == EMEDIUMTYPE) return "EMEDIUMTYPE";
#endif
#ifdef EMFILE
if(errcode == EMFILE) return "EMFILE";
#endif
#ifdef EMLINK
if(errcode == EMLINK) return "EMLINK";
#endif
#ifdef EMSGSIZE
if(errcode == EMSGSIZE) return "EMSGSIZE";
#endif
#ifdef EMULTIHOP
if(errcode == EMULTIHOP) return "EMULTIHOP";
#endif
#ifdef ENAMETOOLONG
if(errcode == ENAMETOOLONG) return "ENAMETOOLONG";
#endif
#ifdef ENAVAIL
if(errcode == ENAVAIL) return "ENAVAIL";
#endif
#ifdef ENETDOWN
if(errcode == ENETDOWN) return "ENETDOWN";
#endif
#ifdef ENETRESET
if(errcode == ENETRESET) return "ENETRESET";
#endif
#ifdef ENETUNREACH
if(errcode == ENETUNREACH) return "ENETUNREACH";
#endif
#ifdef ENFILE
if(errcode == ENFILE) return "ENFILE";
#endif
#ifdef ENIX
if(errcode == ENIX) return "ENIX";
#endif
#ifdef ENOANO
if(errcode == ENOANO) return "ENOANO";
#endif
#ifdef ENOBUFS
if(errcode == ENOBUFS) return "ENOBUFS";
#endif
#ifdef ENOCSI
if(errcode == ENOCSI) return "ENOCSI";
#endif
#ifdef ENODATA
if(errcode == ENODATA) return "ENODATA";
#endif
#ifdef ENODEV
if(errcode == ENODEV) return "ENODEV";
#endif
#ifdef ENOENT
if(errcode == ENOENT) return "ENOENT";
#endif
#ifdef ENOEXEC
if(errcode == ENOEXEC) return "ENOEXEC";
#endif
#ifdef ENOGRACE
if(errcode == ENOGRACE) return "ENOGRACE";
#endif
#ifdef ENOKEY
if(errcode == ENOKEY) return "ENOKEY";
#endif
#ifdef ENOLCK
if(errcode == ENOLCK) return "ENOLCK";
#endif
#ifdef ENOLINK
if(errcode == ENOLINK) return "ENOLINK";
#endif
#ifdef ENOMEDIUM
if(errcode == ENOMEDIUM) return "ENOMEDIUM";
#endif
#ifdef ENOMEM
if(errcode == ENOMEM) return "ENOMEM";
#endif
#ifdef ENOMSG
if(errcode == ENOMSG) return "ENOMSG";
#endif
#ifdef ENONET
if(errcode == ENONET) return "ENONET";
#endif
#ifdef ENOPKG
if(errcode == ENOPKG) return "ENOPKG";
#endif
#ifdef ENOPROTOOPT
if(errcode == ENOPROTOOPT) return "ENOPROTOOPT";
#endif
#ifdef ENOSPC
if(errcode == ENOSPC) return "ENOSPC";
#endif
#ifdef ENOSR
if(errcode == ENOSR) return "ENOSR";
#endif
#ifdef ENOSTR
if(errcode == ENOSTR) return "ENOSTR";
#endif
#ifdef ENOSYM
if(errcode == ENOSYM) return "ENOSYM";
#endif
#ifdef ENOSYS
if(errcode == ENOSYS) return "ENOSYS";
#endif
#ifdef ENOTBLK
if(errcode == ENOTBLK) return "ENOTBLK";
#endif
#ifdef ENOTCONN
if(errcode == ENOTCONN) return "ENOTCONN";
#endif
#ifdef ENOTDIR
if(errcode == ENOTDIR) return "ENOTDIR";
#endif
#ifdef ENOTEMPTY
if(errcode == ENOTEMPTY) return "ENOTEMPTY";
#endif
#ifdef ENOTNAM
if(errcode == ENOTNAM) return "ENOTNAM";
#endif
#ifdef ENOTRECOVERABLE
if(errcode == ENOTRECOVERABLE) return "ENOTRECOVERABLE";
#endif
#ifdef ENOTSOCK
if(errcode == ENOTSOCK) return "ENOTSOCK";
#endif
#ifdef ENOTSUPP
if(errcode == ENOTSUPP) return "ENOTSUPP";
#endif
#ifdef ENOTSYNC
if(errcode == ENOTSYNC) return "ENOTSYNC";
#endif
#ifdef ENOTTY
if(errcode == ENOTTY) return "ENOTTY";
#endif
#ifdef ENOTUNIQ
if(errcode == ENOTUNIQ) return "ENOTUNIQ";
#endif
#ifdef ENXIO
if(errcode == ENXIO) return "ENXIO";
#endif
#ifdef EOPNOTSUPP
if(errcode == EOPNOTSUPP) return "EOPNOTSUPP";
#endif
#ifdef EOVERFLOW
if(errcode == EOVERFLOW) return "EOVERFLOW";
#endif
#ifdef EOWNERDEAD
if(errcode == EOWNERDEAD) return "EOWNERDEAD";
#endif
#ifdef EPERM
if(errcode == EPERM) return "EPERM";
#endif
#ifdef EPFNOSUPPORT
if(errcode == EPFNOSUPPORT) return "EPFNOSUPPORT";
#endif
#ifdef EPIPE
if(errcode == EPIPE) return "EPIPE";
#endif
#ifdef EPROCLIM
if(errcode == EPROCLIM) return "EPROCLIM";
#endif
#ifdef EPROTO
if(errcode == EPROTO) return "EPROTO";
#endif
#ifdef EPROTONOSUPPORT
if(errcode == EPROTONOSUPPORT) return "EPROTONOSUPPORT";
#endif
#ifdef EPROTOTYPE
if(errcode == EPROTOTYPE) return "EPROTOTYPE";
#endif
#ifdef ERANGE
if(errcode == ERANGE) return "ERANGE";
#endif
#ifdef ERECALLCONFLICT
if(errcode == ERECALLCONFLICT) return "ERECALLCONFLICT";
#endif
#ifdef EREFUSED
if(errcode == EREFUSED) return "EREFUSED";
#endif
#ifdef EREMCHG
if(errcode == EREMCHG) return "EREMCHG";
#endif
#ifdef EREMDEV
if(errcode == EREMDEV) return "EREMDEV";
#endif
#ifdef EREMOTE
if(errcode == EREMOTE) return "EREMOTE";
#endif
#ifdef EREMOTEIO
if(errcode == EREMOTEIO) return "EREMOTEIO";
#endif
#ifdef EREMOTERELEASE
if(errcode == EREMOTERELEASE) return "EREMOTERELEASE";
#endif
#ifdef ERESTART
if(errcode == ERESTART) return "ERESTART";
#endif
#ifdef ERFKILL
if(errcode == ERFKILL) return "ERFKILL";
#endif
#ifdef EROFS
if(errcode == EROFS) return "EROFS";
#endif
#ifdef ERREMOTE
if(errcode == ERREMOTE) return "ERREMOTE";
#endif
#ifdef ESERVERFAULT
if(errcode == ESERVERFAULT) return "ESERVERFAULT";
#endif
#ifdef ESHUTDOWN
if(errcode == ESHUTDOWN) return "ESHUTDOWN";
#endif
#ifdef ESOCKTNOSUPPORT
if(errcode == ESOCKTNOSUPPORT) return "ESOCKTNOSUPPORT";
#endif
#ifdef ESPIPE
if(errcode == ESPIPE) return "ESPIPE";
#endif
#ifdef ESRCH
if(errcode == ESRCH) return "ESRCH";
#endif
#ifdef ESRMNT
if(errcode == ESRMNT) return "ESRMNT";
#endif
#ifdef ESTALE
if(errcode == ESTALE) return "ESTALE";
#endif
#ifdef ESTRPIPE
if(errcode == ESTRPIPE) return "ESTRPIPE";
#endif
#ifdef ETIME
if(errcode == ETIME) return "ETIME";
#endif
#ifdef ETIMEDOUT
if(errcode == ETIMEDOUT) return "ETIMEDOUT";
#endif
#ifdef ETOOMANYREFS
if(errcode == ETOOMANYREFS) return "ETOOMANYREFS";
#endif
#ifdef ETOOSMALL
if(errcode == ETOOSMALL) return "ETOOSMALL";
#endif
#ifdef ETXTBSY
if(errcode == ETXTBSY) return "ETXTBSY";
#endif
#ifdef EUCLEAN
if(errcode == EUCLEAN) return "EUCLEAN";
#endif
#ifdef EUNATCH
if(errcode == EUNATCH) return "EUNATCH";
#endif
#ifdef EUSERS
if(errcode == EUSERS) return "EUSERS";
#endif
#ifdef EWOULDBLOCK
if(errcode == EWOULDBLOCK) return "EWOULDBLOCK";
#endif
#ifdef EXDEV
if(errcode == EXDEV) return "EXDEV";
#endif
#ifdef EXFULL
if(errcode == EXFULL) return "EXFULL";
#endif
