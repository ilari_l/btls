use super::__do_readlink;
use super::CString;
use super::FileDescriptor;
use super::Gid;
use super::OsError;
use super::OwnedByteString;
use super::RawMalloc;
use super::ReturnValue;
use super::Stat;
use super::Uid;
use super::with_uninitialized;
use crate::fd::DirectoryFilename;
use crate::fd::FileDescriptorB;
use crate::fd::RawFdT;
use btls_aux_fail::fail_if_none;
use btls_aux_fail::ResultExt;
use core::mem::transmute;
use core::ops::Deref;
use libc::c_char;
use libc::c_void;
use crate::user::ErrorMessage;

define_bitfield_type!([doc=r#"Flags for `access(2)`."#];AccessFlags i32);
impl AccessFlags
{
	define_symbol!([doc=r#"File exists."#];F_OK: AccessFlags as F);
	define_symbol!([doc=r#"File is readable."#];R_OK: AccessFlags as R);
	define_symbol!([doc=r#"File is writable."#];W_OK: AccessFlags as W);
	define_symbol!([doc=r#"File is executable."#];X_OK: AccessFlags as X);
}

///Object to write ACLs from.
pub trait AclWrite
{
	///Get the permissions of user.
	fn get_user(&self) -> AclPermEntry;
	///Get the users entries.
	fn get_users<F>(&self, f: F) where F: FnMut(Uid, AclPermEntry);
	///Get the permissions of group.
	fn get_group(&self) -> AclPermEntry;
	///Get the groups entries.
	fn get_groups<F>(&self, f: F) where F: FnMut(Gid, AclPermEntry);
	///Get the permissions of others.
	fn get_other(&self) -> AclPermEntry;
}

///Object to read ACLs from.
pub trait AclRead
{
	///Set the permissions of user.
	fn set_user(&mut self, perm: AclPermEntry);
	///Set the permissions of an user in users.
	fn set_users(&mut self, uid: Uid, perm: AclPermEntry);
	///Set the permissions of group.
	fn set_group(&mut self, perm: AclPermEntry);
	///Set the permissions of an group in groups.
	fn set_groups(&mut self, gid: Gid, perm: AclPermEntry);
	///Set the permissions of others.
	fn set_other(&mut self, perm: AclPermEntry);
}

#[derive(Copy,Clone,Debug)]
pub struct AclPermEntry
{
	r: bool,
	w: bool,
	x: bool,
}

impl AclPermEntry
{
	pub const NONE: AclPermEntry = AclPermEntry { r: false, w: false, x: false };
	pub const X: AclPermEntry = AclPermEntry { r: false, w: false, x: true };
	pub const R: AclPermEntry = AclPermEntry { r: true, w: false, x: false };
	pub const RX: AclPermEntry = AclPermEntry { r: true, w: false, x: true };
	pub const RW: AclPermEntry = AclPermEntry { r: true, w: true, x: false };
	pub const RWX: AclPermEntry = AclPermEntry { r: true, w: true, x: true };
	pub fn new() -> AclPermEntry { AclPermEntry { r: false, w: false, x: false } }
	pub fn from_numeric(f: u8) -> AclPermEntry
	{
		AclPermEntry {
			r: f & 4 != 0,
			w: f & 2 != 0,
			x: f & 1 != 0,
		}
	}
	pub fn to_numeric(&self) -> u8
	{
		(if self.r { 4 } else { 0 }) | (if self.w { 2 } else { 0 }) | (if self.x { 1 } else { 0 })
	}
	pub fn set_r(&mut self, f: bool) { self.r = f; }
	pub fn set_w(&mut self, f: bool) { self.w = f; }
	pub fn set_x(&mut self, f: bool) { self.x = f; }
	pub fn get_r(&self) -> bool { self.r }
	pub fn get_w(&self) -> bool { self.w }
	pub fn get_x(&self) -> bool { self.x }
}

///Handle to shared object.
#[derive(Debug)]
pub struct DlHandle(*mut c_void);

impl Drop for DlHandle
{
	fn drop(&mut self)
	{
		//These objects have no meaning on anything but Linux, so they do not do anything on drop.
		if_has_dlfcn!({
			//Can not do anything with this failing, so no need to check.
			//Assumes self.0 is valid handle => structure invariant.
			unsafe{libc::dlclose(self.0)};
		}, ());
	}
}

impl DlHandle
{
	pub(crate) fn new(ptr: *mut c_void) -> DlHandle { DlHandle(ptr) }
	//Assumes symbol is valid NUL-terminated symbol name. Not thread safe.
	pub(crate) unsafe fn dlsym(&self, symbol: *const c_char) -> Result<RawSymbolT, ErrorMessage>
	{
		//Object lookup only makes sense on Linux.
		if_has_dlfcn!({
			//Unconditionally safe.
			unsafe{libc::dlerror();}	//Reset error in case NULL symbol.
			//Assumes self.0 is valid handle => structure invariant.
			//Assumes symbol is valid NUL-terminated symbol name -> function precondition.
			let sym = unsafe{libc::dlsym(self.0, symbol)};
			if sym.is_null() {
				//Unconditionally safe.
				let e = unsafe{libc::dlerror()};
				if e.is_null() {
					Ok(sym)		//Yes, a NULL symbol.
				} else {
					Err(unsafe{ErrorMessage::new(e)})
				}
			} else {
				Ok(sym)
			}
		}, {
			let _ = symbol;
			Err(ErrorMessage::new_str("Dynamic library loading not supported"))
		})
	}
}

///Raw symbol type.
pub type RawSymbolT = *mut c_void;

define_bitfield_type!([doc=r#"Flags for `dlopen(3)`."#];DlopenFlags i32);
impl DlopenFlags
{
	define_symbol!([doc=r#"Bind the symbols immediately on load."#];RTLD_NOW: DlopenFlags as NOW LINUX_ONLY);
}

//This is special enough so that helper macros can not be used.
///Flags for `open(2)`.
#[derive(Copy,Clone,Debug)]
pub struct OpenFlags(pub(crate) i32, pub(crate) bool);

impl ::core::ops::BitOr for OpenFlags
{
	type Output = OpenFlags;
	fn bitor(self, other: Self) -> Self { OpenFlags(self.0 | other.0, self.1 || other.1) }
}
impl OpenFlags
{
	///Open for append.
	pub const O_APPEND: OpenFlags = OpenFlags(libc::O_APPEND, false);
	///Automatically close on exec.
	pub const O_CLOEXEC: OpenFlags = OpenFlags(libc::O_CLOEXEC, false);
	///Create a file, with 600 privileges.
	pub const O_CREAT: OpenFlags = OpenFlags(libc::O_CREAT, false);
	///Create a file, with 666 privileges masked by umask.
	pub const O_CREAT_PUBLIC: OpenFlags = OpenFlags(libc::O_CREAT, true);
	///Open in nonbloking mode.
	pub const O_NONBLOCK: OpenFlags = OpenFlags(libc::O_NONBLOCK, false);
	///Open for read-only.
	pub const O_RDONLY: OpenFlags = OpenFlags(libc::O_RDONLY, false);
	///Open for write-only.
	pub const O_WRONLY: OpenFlags = OpenFlags(libc::O_WRONLY, false);
	///Open for read/write.
	pub const O_RDWR: OpenFlags = OpenFlags(libc::O_RDWR, false);
	///Truncate if already exists.
	pub const O_TRUNC: OpenFlags = OpenFlags(libc::O_TRUNC, false);
	///Fail if file exists.
	pub const O_EXCL: OpenFlags = OpenFlags(libc::O_EXCL, false);
	///Open as directory.
	pub const O_DIRECTORY: OpenFlags = OpenFlags(libc::O_DIRECTORY, false);
}

///A path to file.
pub struct Path([u8]);

impl Path
{
	///To owned.
	///
	///The error (if it happens) is always `ENOMEM`.
	pub fn to_owned(&self) -> Result<PathBuf, OsError>
	{
		Ok(PathBuf(OwnedByteString::from_slice(&self.0)?))
	}
	///Get path to root directory.
	pub fn root() -> &'static Path
	{
		//That &str and &Path are layout-compatible is required by libstd.
		unsafe{transmute("/")}
	}
	///Get path to /dev/null.
	pub fn dev_null() -> &'static Path
	{
		//That &str and &Path are layout-compatible is required by libstd.
		unsafe{transmute("/dev/null")}
	}
	///Get path to /dev/urandom.
	pub fn dev_urandom() -> &'static Path
	{
		//That &str and &Path are layout-compatible is required by libstd.
		unsafe{transmute("/dev/urandom")}
	}
	///Get the raw path.
	pub fn to_inner<'a>(&'a self) -> &'a [u8] { &self.0 }
	///Get path to specified file.
	///
	///If the path specified contains a NUL byte, returns `None`.
	pub fn new<'a>(p: &'a [u8]) -> Option<&'a Path>
	{
		fail_if_none!(p.iter().cloned().any(|b|b==0));
		//Transmuting in the Some() is allowed by option optimization.
		//That &str and &Path are layout-compatible is required by libstd. Furthermore null pointer
		//optimization means Option<&Path> is layout-compatible too.
		unsafe{transmute(p)}
	}
	///Call `open(2)` on the specified path.
	pub fn open(&self, flags: OpenFlags) -> Result<FileDescriptor, OsError>
	{
		//There are no fds.
		unsafe{Self::openat(None, self, flags)}
	}
	///Call `chmod(2)` on the specified path.
	pub fn chmod(&self, mode: u32) -> Result<(), OsError>
	{
		//There are no fds.
		unsafe{Self::fchmodat(None, self, mode, 0)}
	}
	///Call `chown(2)` on the specified path.
	pub fn chown(&self, uid: Uid, gid: Gid) -> Result<(), OsError>
	{
		//There are no fds.
		unsafe{Self::fchownat(None, self, uid, gid, 0)}
	}
	///Call `unlink(2)` on the specified path.
	pub fn unlink(&self) -> Result<(), OsError>
	{
		//There are no fds.
		unsafe{Self::unlinkat(None, self)}
	}
	///Call `chdir(2)` on the specified path.
	pub fn chdir(&self) -> Result<(), OsError>
	{
		let path = self.__as_cstr()?;
		//Assumes path is live => True by definition.
		unsafe{libc::chdir(path.as_ptr()).errno()}
	}
	///Call `mkdir(2)` on the specified path.
	pub fn mkdir(&self, public: bool) -> Result<(), OsError>
	{
		//There are no fds.
		unsafe{Self::mkdirat(None, self, public)}
	}
	///Call `stat(2)` on the specified path.
	pub fn stat(&self) -> Result<Stat, OsError>
	{
		//There are no fds.
		unsafe{Self::fstatat(None, self, 0)}
	}
	///Call `lstat(2)` on the specified path.
	pub fn lstat(&self) -> Result<Stat, OsError>
	{
		//There are no fds.
		unsafe{Self::fstatat(None, self, libc::AT_SYMLINK_NOFOLLOW)}
	}
	///Call `access(2)` on the specified path.
	pub fn access(&self, flags: AccessFlags) -> Result<(), OsError>
	{
		let path = self.__as_cstr()?;
		//Assumes path is live => True by definition.
		unsafe{libc::access(path.as_ptr(), flags.0).errno()}
	}
	///Call `getfacl` on the specified path.
	pub fn getfacl(&self, acl: &mut impl AclRead) -> Result<(), OsError>
	{
		crate::sys::getfacl(self, acl)
	}
	///Call `setfacl` on the specified path.
	///
	///Only supported on Linux, otherwise returns ENOSYS.
	pub fn setfacl(&self, acl: &impl AclWrite) -> Result<(), OsError>
	{
		crate::sys::setfacl(self, acl)
	}
	///Call if_nametoindex(3).
	pub fn interface_index(&self) -> Option<u32>
	{
		let name = self.__as_cstr().ok()?;
		//Assumes name is live => True by definition.
		let r = unsafe{libc::if_nametoindex(name.as_ptr())};
		if r != 0 { Some(r) } else { None }
	}
	///Call inotify_add_watch(2).
	///
	///Only supported on Linux, otherwise returns ENOSYS.
	pub unsafe fn inotify_add_watch(&self, fd: &crate::fd::FileDescriptorB, mask: crate::fd::InotifyEventType) ->
		Result<crate::fd::InotifyWatchDescriptor, OsError>
	{
		unsafe{crate::sys::inotify_add_watch2(self, fd.as_raw_fd(), mask)}
	}
	///Call dlopen(3) (Linux only).
	///
	///# Safety:
	///
	///Not thread safe.
	pub unsafe fn dlopen(&self, flags: DlopenFlags) -> Result<DlHandle, ErrorMessage>
	{
		if_has_dlfcn!({
			let path = self.__as_cstr().
				set_err(ErrorMessage::new_str("Failed to null-terminate file name"))?;
			//Need to hold lock to serialize access to dlerror().
			let _lock = crate::DlfcnLock::new();
			//Assumes path is live => True by definition.
			let h = unsafe{libc::dlopen(path.as_ptr(), flags.to_inner())};
			if h.is_null() {
				Err(unsafe{ErrorMessage::new(libc::dlerror())})
			} else {
				Ok(DlHandle::new(h))
			}
		}, {
			if false {
				//Squash some warnings.
				let _ = flags;
				let _ = DlHandle::new(::core::ptr::null_mut());
				let _ = unsafe{ErrorMessage::new(::core::ptr::null_mut())};
			}
			Err(ErrorMessage::new_str("Dynamic library loading not implemented"))
		})
	}
	///Call dlsym(3)
	///
	///# Safety:
	///
	///Not thread safe.
	pub unsafe fn dlsym(&self, library: &DlHandle) -> Result<RawSymbolT, ErrorMessage>
	{
		let path = self.__as_cstr().
			set_err(ErrorMessage::new_str("Failed to null-terminate symbol name"))?;
		//Need to hold lock to serialize access to dlerror().
		let _lock = crate::DlfcnLock::new();
		unsafe{library.dlsym(path.as_ptr())}
	}
	///Call `getxattr(2)`.
	pub fn getxattr(&self, name: &Path) -> Result<OwnedByteString, OsError>
	{
		crate::sys::getxattr(self, name)
	}
	///Call `setxattr(2)`.
	pub fn setxattr(&self, name: &Path, value: &[u8], flags: SetxattrFlags) -> Result<(), OsError>
	{
		crate::sys::setxattr(self, name, value, flags)
	}
	///Call `lgetxattr(2)`.
	pub fn lgetxattr(&self, name: &Path) -> Result<OwnedByteString, OsError>
	{
		crate::sys::lgetxattr(self, name)
	}
	///Call `lsetxattr(2)`.
	pub fn lsetxattr(&self, name: &Path, value: &[u8], flags: SetxattrFlags) -> Result<(), OsError>
	{
		crate::sys::lsetxattr(self, name, value, flags)
	}
	///Call symlink(2)
	pub fn symlink(&self, target: &Self) -> Result<(), OsError>
	{
		//The fds (there are none) are open
		unsafe{Self::symlinkat(None, self, target)}
	}
	///Call renameat(2) with AT_FDCWD.
	pub fn rename(&self, newpath: &Self) -> Result<(), OsError>
	{
		//The fds (there are none) are open
		unsafe{Self::renameat(None, self, None, newpath)}
	}
	///Call readlink(2).
	pub fn readlink(&self) -> Result<DirectoryFilename, OsError>
	{
		//The fds (there are none) are open
		unsafe{Self::readlinkat(None, self)}
	}
	///Call renameat(2).
	///
	///Unsafety is having the file descriptors open.
	pub unsafe fn renameat(olddir: Option<FileDescriptorB>, oldpath: &Self, newdir: Option<FileDescriptorB>,
		newpath: &Self) -> Result<(), OsError>
	{
		let (olddir, oldpath) = oldpath.__at_pair(olddir)?;
		let (newdir, newpath) = newpath.__at_pair(newdir)?;
		unsafe{libc::renameat(olddir, oldpath.as_ptr(), newdir, newpath.as_ptr())}.errno()
	}
	pub(crate) unsafe fn fstatat(dirfd: Option<FileDescriptorB>, path: &Self, flags: libc::c_int) ->
		Result<Stat, OsError>
	{
		let (dirfd, path) = path.__at_pair(dirfd)?;
		//The stat is guaranteed to initialize its buffer if it suceeds.
		//Assumes path is live => True by definition.
		//with_uninitialized guarantees st is sizeo_of(stat) bytes of writable memory.
		let r = unsafe {
			with_uninitialized(|st|libc::fstatat(dirfd, path.as_ptr(), st, flags).errno())?
		};
		Ok(Stat(r))
	}
	pub(crate) unsafe fn openat(dirfd: Option<FileDescriptorB>, path: &Self, flags: OpenFlags) ->
		Result<FileDescriptor, OsError>
	{
		let (dirfd, path) = path.__at_pair(dirfd)?;
		let mode = if flags.1 { 438 } else { 384 };
		let fd = unsafe{libc::openat(dirfd, path.as_ptr(), flags.0, mode)}.errno_sval()?;
		Ok(unsafe{FileDescriptor::new(fd)})
	}
	pub(crate) unsafe fn unlinkat(dirfd: Option<FileDescriptorB>, path: &Self) -> Result<(), OsError>
	{
		let (dirfd, path) = path.__at_pair(dirfd)?;
		unsafe{libc::unlinkat(dirfd, path.as_ptr(), 0)}.errno()
	}
	pub(crate) unsafe fn fchmodat(dirfd: Option<FileDescriptorB>, path: &Self, mode: u32, flags: libc::c_int) ->
		Result<(), OsError>
	{
		if mode > 511 { return Err(OsError::EINVAL); }
		let (dirfd, path) = path.__at_pair(dirfd)?;
		unsafe{libc::fchmodat(dirfd, path.as_ptr(), mode as libc::mode_t, flags)}.errno()
	}
	pub(crate) unsafe fn fchownat(dirfd: Option<FileDescriptorB>, path: &Self, uid: Uid, gid: Gid,
		flags: libc::c_int) -> Result<(), OsError>
	{
		let (dirfd, path) = path.__at_pair(dirfd)?;
		unsafe{libc::fchownat(dirfd, path.as_ptr(), uid.to_inner(), gid.to_inner(), flags)}.errno()
	}
	pub(crate) unsafe fn mkdirat(dirfd: Option<FileDescriptorB>, path: &Self, public: bool) -> Result<(), OsError>
	{
		let mode = if public { 493 } else { 448 };
		let (dirfd, path) = path.__at_pair(dirfd)?;
		unsafe{libc::mkdirat(dirfd, path.as_ptr(), mode)}.errno()
	}
	pub(crate) unsafe fn symlinkat(dirfd: Option<FileDescriptorB>, path: &Self, target: &Self) ->
		Result<(), OsError>
	{
		let (dirfd, path) = path.__at_pair(dirfd)?;
		let target = target.__as_cstr()?;
		unsafe{libc::symlinkat(target.as_ptr(), dirfd, path.as_ptr())}.errno()
	}
	pub(crate) unsafe fn readlinkat(dirfd: Option<FileDescriptorB>, path: &Self) ->
		Result<DirectoryFilename, OsError>
	{
		let (dirfd, path) = path.__at_pair(dirfd)?;
		__do_readlink(|buf|unsafe {
			let buflen = buf.len();
			let pathptr = path.as_ptr();
			let bufptr = buf.as_mut_ptr().cast();
			libc::readlinkat(dirfd, pathptr, bufptr, buflen).errno_val()
		})
	}
	fn __at_pair(&self, dir: Option<FileDescriptorB>) -> Result<(RawFdT, CString), OsError>
	{
		let dir = dir.map(|fd|fd.as_raw_fd()).unwrap_or(libc::AT_FDCWD);
		let path = self.__as_cstr()?;
		Ok((dir, path))
	}
	///The only possible error is ENOMEM.
	pub(crate) fn __as_cstr(&self) -> Result<CString, OsError>
	{
		CString::new_c_string_unchecked(&self.0)
	}
}

///A owned path to file.
pub struct PathBuf(OwnedByteString);

impl Deref for PathBuf
{
	type Target = Path;
	fn deref(&self) -> &Path
	{
		let inner: &[u8] = self.0.deref();
		unsafe{transmute(inner)}
	}
}



///Flags for setxattr
#[derive(Copy,Clone,PartialEq,Eq)]
pub enum SetxattrFlags
{
	///Create or Replace.
	Default,
	///Create only
	CreateOnly,
	///Replace only
	ReplaceOnly,
}



///Thread umask.
#[derive(Copy,Clone,Debug)]
pub struct Umask(libc::mode_t);

impl Umask
{
	///Umask 077 (private)
	pub const PRIVATE: Umask = Umask(63);	//go-rwx
	///Umask 022 (public)
	pub const PUBLIC:  Umask = Umask(18);	//go-w
	//Assume this never fails.
	///Replace the umask of current thread with given one and return the old umask.
	pub fn swap(self) -> Umask { Umask(unsafe{libc::umask(self.0)}) }
}

///Byte buffer that has been allocated from heap.
pub struct HeapByteBuffer(RawMalloc<u8>);

impl core::ops::Deref for HeapByteBuffer
{
	type Target = [u8];
	fn deref(&self) -> &[u8]
	{
		let base = self.0.get_base();
		let size = self.0.get_size();
		unsafe{base.make_slice(size)}
	}
}

///Get the current working directory.
pub fn getcwd() -> Option<HeapByteBuffer>
{
	unsafe {
		const INIT_SIZE: usize = 256;
		let mut bufsz = INIT_SIZE;
		//bufsz > 0.
		let mut buf: RawMalloc<u8> = RawMalloc::new(bufsz)?;
		while libc::getcwd(buf.get_base_mut().to_raw().cast(), bufsz).is_null() {
			//Failed! If ERANGE, enlarge buffer.
			let err = OsError::new();
			fail_if_none!(err != OsError::ERANGE);
			//Buffer was not big enough. bufsz > 0 .
			bufsz <<= 1;
			buf.resize(bufsz)?;
		}
		let bufsz = buf.get_base().strlen();
		//Free excess memory. This is a downsize, so this can not fail.
		buf.resize(bufsz);		
		Some(HeapByteBuffer(buf))
	}
}
