use crate::AclPermEntry;
use crate::AclRead;
use crate::AclWrite;
use crate::EpollCreateFlags;
use crate::EpollCtlCmd;
use crate::EpollEvent;
use crate::EpollEventType;
use crate::ExtendedTime;
use crate::Gid;
use crate::InotifyInitFlags;
use crate::LeapSecondStatus;
use crate::OsError;
use crate::OwnedByteString;
use crate::Path;
use crate::Pid;
use crate::RawFdT;
use crate::RawPidT;
use crate::RawMalloc;
use crate::ReturnValue;
use crate::SetxattrFlags;
use crate::Signal;
use crate::SignalFdFlags;
use crate::SignalSet;
use crate::structure_from_bytes;
use crate::Uid;
use crate::fd::Address;
use crate::fd::CmsgKind;
use crate::fd::CmsgSerializer;
use crate::fd::do_backward_copy;
use crate::fd::do_forward_copy;
use crate::fd::FileDescriptor;
use crate::fd::SocketFamily;
use crate::fd::SocketFlags;
use crate::fd::SocketType;
use crate::fd::TypeAdapter;
use crate::fd::UnixPath;
use crate::fs::OpenFlags;
use btls_aux_fail::fail_if;
use btls_aux_memory::asciiz_pointer;
use btls_aux_memory::asciiz_slice;
use btls_aux_memory::ObjectExt;
use core::cmp::min;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::mem::MaybeUninit;
use core::mem::size_of;
use core::mem::transmute;
use core::mem::zeroed;
use core::ops::Deref;
use core::slice::from_raw_parts;
use core::str::from_utf8_unchecked;
use core::str::FromStr;
use libc::c_char;
use libc::c_int;
use libc::c_void;
use libc::getenv;


#[doc(hidden)]
pub fn __dummy_epoll_event_compatible() -> bool { crate::fd::EpollEvent::compatible() }

extern {
	fn inotify_init1(flags: c_int) -> c_int;
	fn inotify_add_watch(fd: c_int, pathname: *const c_char, mask: u32) -> c_int;
}

#[derive(Copy,Clone)]
#[repr(C)]
struct RawAclEntry
{
	etype: u16,
	perm: u16,
	id: u32,
}

impl SetxattrFlags
{
	fn to_libc(self) -> i32
	{
		match self {
			SetxattrFlags::Default => 0,
			SetxattrFlags::CreateOnly => libc::XATTR_CREATE,
			SetxattrFlags::ReplaceOnly => libc::XATTR_REPLACE,
		}
	}
}

impl Pid
{
	//Assume gettid() can not fail.
	///Get PID of current thread (Linux only).
	pub fn current_thread() -> Pid
	{
		//Unconditionally safe.
		Pid::new(unsafe{libc::syscall(libc::SYS_gettid) as RawPidT})
	}
}

impl OwnedByteString
{
	fn blank() -> OwnedByteString { OwnedByteString(RawMalloc::__blank()) }
}

impl OpenFlags
{
	///Open as path.
	pub const O_PATH: OpenFlags = OpenFlags(libc::O_PATH, false);
}

impl CmsgKind {
	///Process credentials (Linux only).
	pub const SCM_CREDENTIALS: CmsgKind = CmsgKind(libc::SOL_SOCKET, libc::SCM_CREDENTIALS);
	///Transfer file descriptors (Linux only).
	pub const SCM_RIGHTS: CmsgKind = CmsgKind(libc::SOL_SOCKET, libc::SCM_RIGHTS);
}

//Working with this trait only makes sense on Linux.
impl TypeAdapter for EpollEvent
{
	type Internal = libc::epoll_event;
	fn compatible() -> bool
	{
		//Due to layout, there can only be padding between fields, so the layout matches if the size
		//matches. And the layout is fixed by the Linux ABI, which is stable. Libc can not really change
		//the Linux ABI, so this is only meant to catch a new architecture with packed epoll_event.
		size_of::<libc::epoll_event>() == size_of::<EpollEvent>()
	}
	fn forward(t: &mut libc::epoll_event, s: &EpollEvent)
	{
		t.events = s.get_events().into_inner() as _;
		t.u64 = s.get_token();
	}
	fn backward(t: &mut EpollEvent, s: &libc::epoll_event)
	{
		t.set_events(EpollEventType::wrap(s.events as _));
		t.set_token(s.u64);
	}
}

static NOTIFY_SOCKET: &'static [u8] = b"NOTIFY_SOCKET\0";

pub(crate) fn systemd_is_notify() -> bool
{
	//Assumes that argument to getenv is NUL-terminated, which it is.
	unsafe{!getenv(NOTIFY_SOCKET.as_ptr().cast()).is_null()}
}

pub(crate) fn systemd_notify_startup() -> Result<(), OsError>
{
	//Assumes that argument to getenv is NUL-terminated, which it is.
	let notify_sock = unsafe{getenv(NOTIFY_SOCKET.as_ptr().cast())};
	fail_if!(notify_sock.is_null(), OsError::ENODEV);
	let notify_sock = unsafe{asciiz_pointer(notify_sock)};
	//Turn into address.
	let notify_sock = if let Some(notify_sock) = notify_sock.strip_prefix(b"@") {
		Address::Abstract(UnixPath::Borrowed(notify_sock))
	} else {
		Address::Unix(UnixPath::Borrowed(notify_sock))
	};
	//Send the message acknowledging startup.
	let mut control = [MaybeUninit::uninit();104];
	let mut control = CmsgSerializer::new2(&mut control);
	control.add(CmsgKind::SCM_CREDENTIALS, Ucred::new().to_inner())?;
	let flags = SocketFlags::CLOEXEC;
	let socket = FileDescriptor::socket2(SocketFamily::UNIX, SocketType::DGRAM, Default::default(), flags)?;
	static MESSAGE: &'static [u8] = b"READY=1";
	socket.sendmsg(Some(&notify_sock), &[MESSAGE], Some(&control), Default::default())?;
	//Okay, done.
	Ok(())
}

pub(crate) fn unshare(flags: i32) -> Result<(), OsError>
{
	//Uncoditionally safe.
	unsafe{libc::unshare(flags).errno()}
}

pub(crate) fn tgkill(pid: Pid, tid: Pid, sig: Signal) -> Result<(), OsError>
{
	//No invariants.
	unsafe{::libc::syscall(::libc::SYS_tgkill, pid.to_inner(), tid.to_inner(), sig.to_inner()).errno()}
}

pub(crate) fn set_subreaper(status: bool)-> Result<(), OsError>
{
	let status = if status { 1u32 } else { 0u32 };
	//Assumes that status is long => true by definition.
	unsafe{libc::prctl(libc::PR_SET_CHILD_SUBREAPER, status as libc::c_ulong).errno()}
}


fn acl_copy_u16(buf: &mut [MaybeUninit<u8>], ptr: &mut usize, val: u16) -> Result<(), OsError>
{
	if buf.len().saturating_sub(*ptr) < 2 { return Err(OsError::ENOSPC); }
	//Val is 2 bytes and buf has space for at least two more by above. The value is native-endian.
	unsafe{buf.as_mut_ptr().add(*ptr).cast::<u16>().write_unaligned(val)};
	*ptr += 2;
	Ok(())
}

fn acl_copy_u32(buf: &mut [MaybeUninit<u8>], ptr: &mut usize, val: u32) -> Result<(), OsError>
{
	if buf.len().saturating_sub(*ptr) < 4 { return Err(OsError::ENOSPC); }
	//Val is 4 bytes and buf has space for at least four more by above. The value is native-endian.
	unsafe{buf.as_mut_ptr().add(*ptr).cast::<u32>().write_unaligned(val)};
	*ptr += 4;
	Ok(())
}

fn acl_copy_entry(buf: &mut [MaybeUninit<u8>], ptr: &mut usize, etype: u16, perm: u8, id: u32) -> Result<(), OsError>
{
	acl_copy_u16(buf, ptr, etype)?;
	acl_copy_u16(buf, ptr, perm as u16)?;
	acl_copy_u32(buf, ptr, id)
}

fn encode_acls<'a>(buf: &'a mut [MaybeUninit<u8>], acl: &impl AclWrite) -> Result<&'a [u8], OsError>
{
	let mut ptr = 0usize;
	let mut mask = acl.get_group().to_numeric();
	let mut err = Ok(());
	//Encode the ACL buffer.
	acl_copy_u32(buf, &mut ptr, 2)?;	//Magic?
	acl_copy_entry(buf, &mut ptr, 1, acl.get_user().to_numeric(), 0xFFFFFFFF)?;
	acl.get_users(|uid, perm|{
		if err.is_err() { return; }
		mask |= perm.to_numeric();
		err = acl_copy_entry(buf, &mut ptr, 2, perm.to_numeric(), uid.to_inner());
	});
	err?;
	acl_copy_entry(buf, &mut ptr, 4, acl.get_group().to_numeric(), 0xFFFFFFFF)?;
	acl.get_groups(|gid, perm|{
		if err.is_err() { return; }
		mask |= perm.to_numeric();
		err = acl_copy_entry(buf, &mut ptr, 8, perm.to_numeric(), gid.to_inner());
	});
	err?;
	acl_copy_entry(buf, &mut ptr, 16, mask, 0xFFFFFFFF)?;
	acl_copy_entry(buf, &mut ptr, 32, acl.get_other().to_numeric(), 0xFFFFFFFF)?;
	//Assumes buf has been initialized up to ptr => true by definition of ptr.
	//Assumes ptr <= buf.len() => true by definition of ptr.
	Ok(unsafe{transmute(&buf[..ptr])})
}

fn __with_getxattr(name: &Path, func: impl Fn(*const i8, *mut c_void, usize) -> isize) ->
	Result<OwnedByteString, OsError>
{
	let name = name.__as_cstr()?;
	let mut dummy = [0u8];
	loop {
		let len = func(name.as_ptr(), dummy.as_mut_ptr().cast(), 0).errno_val()?;
		//Special case for empty attribute.
		if len == 0 { return Ok(OwnedByteString::blank()); }
		let mut ret = unsafe{OwnedByteString::alloc(len)?};
		let __ret = &mut ret.0;
		let bbase = __ret.get_base_mut();
		let bsize = __ret.get_size();
		let len = func(name.as_ptr(), bbase.to_raw().cast(), bsize).errno_val()?;
		if len <= __ret.get_size() {
			//Fits, return this. If allocation is too big, truncate it. This is downsize, so this can
			//not fail.
			__ret.resize(len);
			return Ok(ret);
		}
		//Does not fit (raced with something modifying the xattr), try again.
	}
}

fn __with_setxattr(name: &Path, value: &[u8], flags: SetxattrFlags,
	func: impl Fn(*const i8, *const c_void, usize, i32) -> i32) -> Result<(), OsError>
{
	let name = name.__as_cstr()?;
	let flags = flags.to_libc();
	func(name.as_ptr(), value.as_ptr().cast(), value.len(), flags).errno()
}

unsafe fn with_uninit_read<'a,E,F>(buffer: &'a mut [MaybeUninit<u8>], initializer: F) ->
	Result<&'a [u8], E> where F: FnOnce(*mut u8, usize) -> Result<usize, E>
{
	let buflen = buffer.len();
	let amt = initializer(buffer.as_mut_ptr().cast(), buflen)?;
	let amt = min(buffer.len(), amt);
	//Assume specified amount has gotten inititialized.
	Ok(unsafe{from_raw_parts(buffer.as_mut_ptr().cast(), amt)})
}

pub(crate) fn setfacl(selfx: &Path, acl: &impl AclWrite) -> Result<(), OsError>
{
	let mut buf = [MaybeUninit::uninit();2048];
	let buf = encode_acls(&mut buf, acl)?;
	let path = selfx.__as_cstr()?;
	let attribute = b"system.posix_acl_access\0";
	//Assumes path is live => True by definition.
	//Assumes attribute is live => True by definition.
	//Assumes buf points to buf.len() bytes => True by definition.
	unsafe{libc::setxattr(path.as_ptr(), attribute.as_ptr().cast(), buf.as_ptr().cast(), buf.len(), 0)}.errno()
}

pub(crate) unsafe fn fsetfacl(fd: RawFdT, acl: &impl AclWrite) -> Result<(), OsError>
{
	let mut buf = [MaybeUninit::uninit();2048];
	let buf = encode_acls(&mut buf, acl)?;
	let attribute = b"system.posix_acl_access\0";
	//Assumes attribute is live => True by definition.
	//Assumes buf points to buf.len() bytes => True by definition.
	unsafe{libc::fsetxattr(fd, attribute.as_ptr().cast(), buf.as_ptr().cast(), buf.len(), 0)}.errno()
}

pub(crate) fn getfacl(selfx: &Path, acl: &mut impl AclRead) -> Result<(), OsError>
{
	let mut buf = [MaybeUninit::uninit();2048];
	let path = selfx.__as_cstr()?;
	let attribute = b"system.posix_acl_access\0";
	//Assumes path is live => True by definition.
	//Assumes attribute is live => True by definition.
	//Assumes buf points to buf.len() bytes => True by definition.
	let buf = unsafe {
		with_uninit_read(&mut buf, |buf, buflen|{
			libc::getxattr(path.as_ptr(), attribute.as_ptr().cast(), buf.cast(), buflen).errno_val()
		})
	}?;
	if buf.len() < 4 { return Err(OsError::EBADMSG); }
	let (magic, buf) = buf.split_at(4);
	if unsafe{magic.as_ptr().cast::<u32>().read_unaligned()} != 2  {
		return Err(OsError::EBADMSG);
	}
	//For each ACL entry.
	for i in 0..buf.len() / size_of::<RawAclEntry>() {
		let offset = i * size_of::<RawAclEntry>();		//<buf.len().
		//Assumes buf.as_ptr().offset(offset) is valid for at least size_of(RawAclEntry)
		//bytes => offset + size_of<RawAclEntry> = (i+1)*size_of(RawAclEntry) <=
		//buf.len() / size_of(RawAclEntry) * size_of(RawAclEntry) <= buf.len(), so true.
		let rent = unsafe{structure_from_bytes::<RawAclEntry>(buf.as_ptr().add(offset))};
		let perm = AclPermEntry::from_numeric(rent.perm as u8);
		match rent.etype {
			1 => acl.set_user(perm),
			2 => acl.set_users(Uid::new(rent.id), perm),
			4 => acl.set_group(perm),
			8 => acl.set_groups(Gid::new(rent.id), perm),
			32 => acl.set_other(perm),
			_ => (),
		}
	}
	Ok(())
}

pub(crate) unsafe fn inotify_add_watch2(selfx: &Path, fd: RawFdT,
	mask: crate::fd::InotifyEventType) -> Result<crate::fd::InotifyWatchDescriptor, OsError>
{
	let path = selfx.__as_cstr()?;
	//Assumes path is live => True by definition.
	let wd = unsafe{inotify_add_watch(fd, path.as_ptr(), mask.to_inner())}.errno_sval()?;
	Ok(crate::fd::InotifyWatchDescriptor(wd))
}

pub(crate) fn getxattr(selfx: &Path, name: &Path) -> Result<OwnedByteString, OsError>
{
	let path = selfx.__as_cstr()?;
	__with_getxattr(name, |a,b,c|unsafe{libc::getxattr(path.as_ptr(), a, b, c)})
}

pub(crate) fn setxattr(selfx: &Path, name: &Path, value: &[u8], flags: SetxattrFlags) -> Result<(), OsError>
{
	let path = selfx.__as_cstr()?;
	__with_setxattr(name, value, flags, |a,b,c,d|unsafe {
		libc::setxattr(path.as_ptr(), a, b, c, d)
	})
}

pub(crate) fn lgetxattr(selfx: &Path, name: &Path) -> Result<OwnedByteString, OsError>
{
	let path = selfx.__as_cstr()?;
	__with_getxattr(name, |a,b,c|unsafe{libc::lgetxattr(path.as_ptr(), a, b, c)})
}

pub(crate) fn lsetxattr(selfx: &Path, name: &Path, value: &[u8], flags: SetxattrFlags) -> Result<(), OsError>
{
	let path = selfx.__as_cstr()?;
	__with_setxattr(name, value, flags, |a,b,c,d|unsafe {
		libc::lsetxattr(path.as_ptr(), a, b, c, d)
	})
}

pub(crate) unsafe fn fgetxattr(fd: RawFdT, name: &Path) -> Result<OwnedByteString, OsError>
{
	__with_getxattr(name, |a,b,c|unsafe {
		libc::fgetxattr(fd, a, b, c)
	})
}

pub(crate) unsafe fn fsetxattr(fd: RawFdT, name: &Path, value: &[u8], flags: SetxattrFlags) -> Result<(), OsError>
{
	__with_setxattr(name, value, flags, |a,b,c,d|unsafe {
		libc::fsetxattr(fd, a, b, c, d)
	})
}

pub(crate) fn inotify_init(flags: InotifyInitFlags) -> Result<FileDescriptor, OsError>
{
	//Uncondtionally safe.
	let fd = unsafe{inotify_init1(flags.to_inner())}.errno_sval()?;
	//The above just opened this fd.
	Ok(unsafe{FileDescriptor::new(fd)})
}

pub(crate) fn epoll_create(flags: EpollCreateFlags) -> Result<FileDescriptor, OsError>
{
	//Uncondtionally safe.
	let fd = unsafe{libc::epoll_create1(flags.to_inner() as _)}.errno_sval()?;
	//The above just opened this fd.
	Ok(unsafe{FileDescriptor::new(fd)})
}

pub(crate) fn signalfd(signals: SignalSet, flags: SignalFdFlags) -> Result<FileDescriptor, OsError>
{
	//Uncondtionally safe.
	let fd = unsafe{libc::signalfd(-1, signals.as_raw(), flags.to_inner())}.errno_sval()?;
	//The above just opened this fd.
	Ok(unsafe{FileDescriptor::new(fd)})
}

pub(crate) unsafe fn seek(fd: RawFdT, offset: i64, whence: c_int) -> Result<u64, OsError>
{
	unsafe{libc::lseek64(fd, offset, whence)}.errno_val()
}

pub(crate) unsafe fn set_passcred(fd: RawFdT, status: bool) -> Result<(), OsError>
{
	let val: c_int = if status { 1 } else { 0 };
	//Assumes fd is open => Precondition of function.
	//Assumes val is at least size_of_val(val) bytes => True by definition.
	unsafe{crate::fd::__do_setsockopt(fd, libc::SOL_SOCKET, libc::SO_PASSCRED, val)}
}

pub(crate) unsafe fn epoll_ctl(selfx: RawFdT, fd: RawFdT, cmd: EpollCtlCmd) -> Result<(), OsError>
{
	let (op, mut arg) = match cmd {
		EpollCtlCmd::Add(events, token) => (libc::EPOLL_CTL_ADD, libc::epoll_event {
			events: events.to_inner() as u32,
			u64: token,
		}),
		EpollCtlCmd::Modify(events, token) => (libc::EPOLL_CTL_MOD, libc::epoll_event {
			events: events.to_inner() as u32,
			u64: token,
		}),
		EpollCtlCmd::Delete => (libc::EPOLL_CTL_DEL, libc::epoll_event {
			events: 0,
			u64: 0,
		}),
	};
	//Assumes selfx is open => Precondition of function.
	//Assumes arg is at least size_of(epoll_event) bytes => True by definition.
	unsafe{libc::epoll_ctl(selfx, op, fd, arg.pointer_to_mut())}.errno()
}

pub(crate) unsafe fn epoll_wait(fd: RawFdT, events: &mut [EpollEvent], timeout: i32) -> Result<u32, OsError>
{
	//Assumes EpollEvent properly implements TypeAdapter => True by definition.
	let (eventptr, eventcount, release) = unsafe{do_forward_copy(events)}?;
	//Assumes fd is open => Precondition of function.
	//Assumes eventptr is at least size_of(eventcount) elements => True by definition of
	//do_forward_copy.
	//Assumes (eventptr, eventcount) is valid => release is live across call => true by
	//definition.
	let r = unsafe{libc::epoll_wait(fd, eventptr, eventcount as _, timeout as _)}.errno_val()?;
	//Assumes eventptr is from do_forward_copy() with the same events => True by above.
	//Assumes eventptr is valid => release is live across call => true by definition.
	unsafe{do_backward_copy(events, eventptr);}
	drop(release);
	Ok(r)
}

pub(crate) fn getrandom2(buffer: &mut [u8], flags: u32) -> Result<usize, OsError>
{
	unsafe {
		//Assumes buffer points to at least buffer.len() writable bytes => True by defition.
		let buflen = buffer.len();
		let bufptr = buffer.as_mut_ptr();
		(libc::syscall(libc::SYS_getrandom, bufptr, buflen, flags) as libc::ssize_t).errno_val()
	}
}

pub(crate) fn unix_path_offset() -> usize { 2 }	//By Linux userspace ABI.

///Process credentials (Linux only).
#[derive(Copy,Clone)]
pub struct Ucred(::libc::ucred);

impl Ucred
{
	///Get credentials of current process.
	pub fn new() -> Ucred
	{
		Ucred(::libc::ucred {
			//Assume none of these can fail. These all are Uncoditionally safe.
			pid: unsafe{::libc::getpid()},
			gid: unsafe{::libc::getegid()},
			uid: unsafe{::libc::geteuid()},
		})
	}
	///Construct process credentials from raw bytes of message.
	pub fn from_bytes(bytes: &[u8]) -> Result<Ucred, UcredError>
	{
		let csize = size_of::<::libc::ucred>();
		fail_if!(bytes.len() != csize, UcredError(bytes.len()));
		//Assumes: Ucred is omnivalid and csize bytes => true by definition.
		Ok(unsafe{structure_from_bytes::<Ucred>(bytes.as_ptr())})
	}
	///Cast Ucred to inner `libc::ucred`.
	pub fn to_inner(self) -> ::libc::ucred { self.0 }
	///Get the PID of process.
	pub fn pid(self) -> Pid { Pid::new(self.0.pid) }
	///Get the UID of process.
	pub fn uid(self) -> Uid { Uid::new(self.0.uid) }
	///Get the GID of process.
	pub fn gid(self) -> Gid { Gid::new(self.0.gid) }
}

///Error deserializing process credentials (Linux only).
#[derive(Copy,Clone,Debug)]
pub struct UcredError(usize);

impl Display for UcredError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "Bad SCM_CREDENTIALS size ({got}!={expected})",
			got=self.0, expected=size_of::<::libc::ucred>())
	}
}

#[repr(C)]
#[derive(Copy,Clone)]
pub struct SignalEvent(::libc::signalfd_siginfo);

impl Deref for SignalEvent
{
	type Target = ::libc::signalfd_siginfo;
	fn deref(&self) -> &::libc::signalfd_siginfo { &self.0 }
}

impl ExtendedTime
{
	pub(crate) fn __now() -> Result<ExtendedTime, OsError>
	{
		//timex can be zero-init.
		let mut tx: libc::timex = unsafe{core::mem::zeroed()};
		let ret = unsafe{libc::adjtimex(tx.pointer_to_mut()).errno_val()?};
		//Nice trap.
		let scale: u32 = if tx.status & 0x2000 != 0 { 1 } else { 1_000 };
		let sec = tx.time.tv_sec as i64;
		let nsec = tx.time.tv_usec as u32 * scale;
		Ok(ExtendedTime{
			leap_second: match ret {
				1 => LeapSecondStatus::WillInsert,
				2 => LeapSecondStatus::WillDelete,
				3 => LeapSecondStatus::InProgress,
				_ => LeapSecondStatus::Normal
			},
			timestamp: sec,
			nanoseconds: nsec,
		})
	}
}

fn ref2ptr_mut<T>(x: &mut T) -> *mut T { x as *mut T }

pub(crate) fn get_linux_version() -> u32
{
	fn is_numeric_or_dot(b: u8) -> bool { matches!(b, 46|48..=57) }
	let mut s: libc::utsname = unsafe{zeroed()};
	let r = unsafe{libc::uname(ref2ptr_mut(&mut s))};
	//If uname fails, return unknown.
	if r < 0 { return 0; }
	let sysname = asciiz_slice(&s.sysname);
	//If sysname is something crazy, return unknown.
	if sysname != b"Linux" { return 0; }
	//Try parsing the release.
	let release = asciiz_slice(&s.release);
	let mut rlen = 0;
	while rlen < release.len() && is_numeric_or_dot(release[rlen]) { rlen += 1; }
	//This is guaranteed to work.
	let release = unsafe{from_utf8_unchecked(&release[..rlen])};
	let mut itr = release.split('.');
	let major = u8::from_str(itr.next().unwrap_or("0")).unwrap_or(0);
	let minor = u8::from_str(itr.next().unwrap_or("0")).unwrap_or(0);
	let patch = u16::from_str(itr.next().unwrap_or("0")).unwrap_or(0);
	(major as u32) << 24 | (minor as u32) << 16 | patch as u32
}

#[test]
fn linux_version()
{
	assert!(get_linux_version() >= 0x03020000);
}

#[test]
fn linux_version2()
{
	use crate::LinuxVersion;
	const MIN_VERSION: LinuxVersion = LinuxVersion::version2(3, 2);
	const FUTURE_VERSION: LinuxVersion = LinuxVersion::version3(132, 53, 44366);
	assert!(LinuxVersion::current() >= MIN_VERSION);
	assert!(LinuxVersion::current() < FUTURE_VERSION);
}
