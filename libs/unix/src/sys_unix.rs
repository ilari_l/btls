use crate::AclRead;
use crate::AclWrite;
use crate::EpollCreateFlags;
use crate::EpollCtlCmd;
use crate::EpollEvent;
use crate::FileDescriptor;
use crate::InotifyInitFlags;
use crate::OsError;
use crate::OwnedByteString;
use crate::Path;
use crate::Pid;
use crate::RawFdT;
use crate::ReturnValue;
use crate::SetxattrFlags;
use crate::Signal;
use crate::SignalFdFlags;
use crate::SignalSet;
use core::mem::zeroed;
use libc::c_int;
use libc::sockaddr_un;

#[doc(hidden)]
pub fn __dummy_epoll_event_compatible() -> bool { false }

impl OsError
{
	define_symbol!([doc=r#"Bad file descriptor."#];EBADF: OsError as EBADFD);
	define_symbol!([doc=r#"No data available."#];ENODATA: OsError VAL 5000);
}


pub(crate) fn systemd_is_notify() -> bool { false }

pub fn systemd_notify_startup() -> Result<(), OsError>
{
	Err(OsError::ENOSYS)
}

pub(crate) fn unshare(_: i32) -> Result<(), OsError>
{
	Err(OsError::ENOSYS)
}

pub(crate) fn tgkill(_: Pid, _: Pid, _: Signal) -> Result<(), OsError>
{
	Err(OsError::ENOSYS)
}

pub(crate) fn set_subreaper(_: bool)-> Result<(), OsError>
{
	Err(OsError::ENOSYS)
}

pub(crate) fn setfacl(_: &Path, _: &impl AclWrite) -> Result<(), OsError>
{
	Err(OsError::ENOSYS)
}

pub(crate) unsafe fn fsetfacl(_: RawFdT, _: &impl AclWrite) -> Result<(), OsError>
{
	Err(OsError::ENOSYS)
}

pub(crate) fn getfacl(_: &Path, _: &mut impl AclRead) -> Result<(), OsError>
{
	Err(OsError::EOPNOTSUPP)
}

pub(crate) unsafe fn inotify_add_watch2(_: &Path, _: RawFdT, _: crate::fd::InotifyEventType) ->
	Result<crate::fd::InotifyWatchDescriptor, OsError>
{
	Err(OsError::ENOSYS)
}

pub(crate) fn getxattr(_: &Path, _: &Path) -> Result<OwnedByteString, OsError>
{
	Err(OsError::ENOSYS)
}

pub(crate) fn setxattr(_: &Path, _: &Path, _: &[u8], _: SetxattrFlags) -> Result<(), OsError>
{
	Err(OsError::ENOSYS)
}

pub(crate) fn lgetxattr(_: &Path, _: &Path) -> Result<OwnedByteString, OsError>
{
	Err(OsError::ENOSYS)
}

pub(crate) fn lsetxattr(_: &Path, _: &Path, _: &[u8], _: SetxattrFlags) -> Result<(), OsError>
{
	Err(OsError::ENOSYS)
}

pub(crate) unsafe fn fgetxattr(_: RawFdT, _: &Path) -> Result<OwnedByteString, OsError>
{
	Err(OsError::ENOSYS)
}

pub(crate) unsafe fn fsetxattr(_: RawFdT, _: &Path, _: &[u8], _: SetxattrFlags) -> Result<(), OsError>
{
	Err(OsError::ENOSYS)
}

pub(crate) fn inotify_init(_: InotifyInitFlags) -> Result<FileDescriptor, OsError>
{
	Err(OsError::ENOSYS)
}

pub(crate) fn epoll_create(_: EpollCreateFlags) -> Result<FileDescriptor, OsError>
{
	Err(OsError::ENOSYS)
}

pub(crate) fn signalfd(_: SignalSet, _: SignalFdFlags) -> Result<FileDescriptor, OsError>
{
	Err(OsError::ENOSYS)
}

pub(crate) unsafe fn seek(fd: RawFdT, offset: i64, whence: c_int) -> Result<u64, OsError>
{
	libc::lseek(fd, offset as _, whence).errno_val()
}

pub(crate) unsafe fn set_passcred(_: RawFdT, _: bool) -> Result<(), OsError>
{
	Err(OsError::EOPNOTSUPP)
}

pub(crate) unsafe fn epoll_ctl(_: RawFdT, _: RawFdT, _: EpollCtlCmd) -> Result<(), OsError>
{
	Err(OsError::ENOSYS)
}

pub(crate) unsafe fn epoll_wait(_: RawFdT, _: &mut [EpollEvent], _: i32) -> Result<u32, OsError>
{
	Err(OsError::ENOSYS)
}

pub(crate) fn getrandom2(_: &mut [u8], _: u32) -> Result<usize, OsError>
{
	Err(OsError::ENOSYS)
}

pub(crate) fn unix_path_offset() -> usize
{
	let xaddr: sockaddr_un = unsafe{zeroed()};
	(xaddr.sun_path.as_ptr() as usize).wrapping_sub(&xaddr as *const _ as usize)
}

//Just something random to make it compile.
#[repr(C)] #[derive(Copy,Clone)] pub struct SignalEvent([u8;128]);
