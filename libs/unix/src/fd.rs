use super::AclWrite;
use super::compatible_fields;
use super::Gid;
use super::offof;
use super::OsError;
use super::OwnedByteString;
use super::Path;
use super::RawMalloc;
use super::ReturnValue;
use super::SetxattrFlags;
use super::Stat;
use super::structure_from_bytes;
use super::Uid;
use super::with_uninitialized;
use super::with_uninitialized_read;
use crate::fs::OpenFlags;
use crate::sys::SignalEvent;
use btls_aux_fail::fail_if;
use btls_aux_fail::fail_if_none;
use btls_aux_fail::ResultExt;
use btls_aux_memory::asciiz_slice;
use btls_aux_memory::BackedVector;
use btls_aux_memory::ObjectExt;
use btls_aux_memory::RawPointerFrom;
use core::cmp::min;
use core::convert::TryInto;
use core::fmt::Debug;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::mem::forget;
use core::mem::MaybeUninit;
use core::mem::size_of;
use core::mem::transmute;
use core::mem::zeroed;
use core::ops::Deref;
use core::ptr::copy_nonoverlapping;
use core::ptr::null;
use core::ptr::null_mut;
use core::slice::from_raw_parts;
use core::slice::from_raw_parts_mut;
use libc::c_int;
use libc::c_void;
use libc::cmsghdr;
use libc::iovec;
use libc::msghdr;
use libc::sa_family_t;
use libc::sockaddr;
use libc::sockaddr_in;
use libc::sockaddr_in6;
use libc::sockaddr_un;
use libc::socklen_t;
use libc::pollfd;
use crate::pid::SignalSet;


//Invariant: Field 0 points to beginning of owned allocation made using malloc() or calloc() and Field 1 is the
//size of that allocation in elements. All the allocation is usable and initialized.
///Fixed buffer in heap.
pub struct HeapBuffer(RawMalloc<u8>);

//The heap buffer is usable across threads, despite having raw pointer inside it (which is normally not safe
//to use across threads).
unsafe impl Send for HeapBuffer {}
unsafe impl Sync for HeapBuffer {}

impl Deref for HeapBuffer
{
	type Target = [u8];
	fn deref(&self) -> &[u8]
	{
		//By structure invariant, the owned memory area starts from self.0 and extends for self.1 bytes.
		//Furthermore all of allocation is intialized. Thus one can create an u8 slice starting at self.0.
		//and running self.1 elements.
		let base = self.0.get_base();
		let size = self.0.get_size();
		unsafe{base.make_slice(size)}
	}
}

impl HeapBuffer
{
	///Create a new buffer with specified contents.
	///
	///The only possible failure code is `ENOMEM`, which is given if there is insufficient heap available to
	///allocate the buffer.
	pub fn new(content: &[u8]) -> Result<HeapBuffer, OsError>
	{
		unsafe {
			//Allocate enough memory to hold content.len() * size_of(u8) = content.len() bytes. If
			//pointer returned is NULL, that means allocation failed. Return ENOMEM in this case.
			let mut ptr = malloc_failed!(RawMalloc::<u8>::new(content.len()));
			//malloc() returns pointer to fresh alocation at least indicated length, so this is valid.
			if ptr.is_nontrivial() { ptr.get_base_mut().write_slice(content); }
			//The structure invariant assumes that:
			//1) Field 0 points to beginning of allocation. Malloc() returns pointer to beginning of
			//allocation, so this is true.
			//2) Field 1 is number of elements in allocation. The size passed to malloc() is
			//content.len() and sizeof(u8)=1, so number of elements in allocation is content.len(). So
			//this is true.
			//3) Field 0 points to at least field.1 initialized elements. The copy_elements()
			//above initialized content.len() elements (as content is assumed to have content.len()
			//initialized elements). So this is met.
			Ok(HeapBuffer(ptr))
		}
	}
}

const UNIX_BUF_SIZE: usize = 108;

///Unix socket path.
///
///# Note:
///
///Unix socket paths used for non-abstract sockets must not contain NUL bytes.
pub enum UnixPath<'a>
{
	///Use slice as unix socket path.
	Borrowed(&'a [u8]),
	///Use array as unix socket path.
	///
	///The scalar is the size of the path, and must be at most 108.
	Array([u8;UNIX_BUF_SIZE], usize),
	///Use heap buffer as unix socket path.
	Owned(HeapBuffer),
}

impl<'a> PartialEq for UnixPath<'a>
{
	fn eq(&self, other: &Self) -> bool { self.deref().eq(other.deref()) }
}

impl<'a> Eq for UnixPath<'a> {}

impl<'a> Debug for UnixPath<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "UnixPath({p:?})", p=self.deref())
	}
}

impl<'a> UnixPath<'a>
{
	///Construct unix path from slice, borrowing it.
	pub fn borrowed(x: &'a [u8]) -> UnixPath<'a> { UnixPath::Borrowed(x) }
	///Construct unixpath from slice, copying the slice to owned path.
	pub fn owned_bytes(x: &[u8]) -> UnixPath<'static>
	{
		//Fits into static buffer?
		if x.len() <= UNIX_BUF_SIZE {
			let mut arr = [0;UNIX_BUF_SIZE];
			//copy_from_slice() assumes that LHS and RHS both have the same number of elements.
			//By definition number of elements in x is x.len(), thus slicing first x.len() elements of
			//arr gives x.len() elements. So there are same number of elements on both sides.
			//Furthermore, by if above, x.len() <= UNIX_BUF_SIZE = arr.len(). Thus slice
			//arr[..x.len()] is valid thing to do.
			(&mut arr[..x.len()]).copy_from_slice(x);
			UnixPath::Array(arr, x.len())
		} else {
			//Panic on OOM.
			match HeapBuffer::new(x) {
				Ok(y) => UnixPath::Owned(y),
				Err(_) => panic!("Out of memory")
			}
		}
	}
	///Construct unix path from possibly NUL-terminated slice, copying the slice to owned path.
	pub fn owned(x: &[u8]) -> UnixPath<'static>
	{
		Self::owned_bytes(asciiz_slice(x))
	}
	///Take borrowed unix socket path and make it owned.
	pub fn to_owned(&self) -> UnixPath<'static> { UnixPath::owned(self.deref()) }
}

impl<'a> Clone for UnixPath<'a>
{
	fn clone(&self) -> UnixPath<'a>
	{
		match self {
			&UnixPath::Borrowed(x) => UnixPath::Borrowed(x),
			&UnixPath::Array(x, y) => UnixPath::Array(x, y),
			//Panic on OOM.
			&UnixPath::Owned(ref x) => match HeapBuffer::new(x.deref()) {
				Ok(y) => UnixPath::Owned(y),
				Err(_) => panic!("Out of memory")
			}
		}
	}
}

impl<'a> Deref for UnixPath<'a>
{
	type Target = [u8];
	fn deref(&self) -> &[u8]
	{
		match self {
			&UnixPath::Borrowed(x) => x,
			//min(x.len(), y) <= x.len(), and therefore the slice of x is in bounds.
			&UnixPath::Array(ref x, y) => &x[..min(x.len(), y)],
			&UnixPath::Owned(ref x) => x.deref()
		}
	}
}

///IPv4 socket address.
#[derive(Clone,Debug,PartialEq,Eq)]
pub struct AddressIpv4
{
	///The IPv4 address.
	pub addr: [u8;4],
	///Port number (native endian).
	pub port: u16,
}

///IPv6 socket address.
#[derive(Clone,Debug,PartialEq,Eq)]
pub struct AddressIpv6
{
	///The IPv6 address.
	pub addr: [u8;16],
	///Port number (native endian).
	pub port: u16,
	///Flow ID (native endian). Only 20 low bits are used.
	pub flow: u32,
	///Scope ID (native endian). 0 means global scope.
	///
	///To obtain scope ID of interface, use [`Path::interface_index()`]
	///
	///[`Path::interface_index()`]: struct.Path.html#method.interface_index
	pub scope: u32,
}

fn copy_to_uninit<T:Copy>(tgt: &mut [MaybeUninit<u8>], src: &T) -> usize
{
	unsafe {
		let len = min(size_of::<T>(), tgt.len());
		//len is smaller than size_of::<T>(), so read part fits.
		//len is smaller than tgt.len(), so write part fits.
		//The pointers do not overlap because tgt is &mut.
		let sptr: *const u8 = T::pointer_to(src).cast();
		let dptr: *mut u8 = tgt.as_mut_ptr().cast();
		copy_nonoverlapping(sptr, dptr, len);
		len
	}
}

///Socket address
#[derive(Clone,Debug,PartialEq,Eq)]
pub enum Address<'a>
{
	///IPv4 address.
	Ipv4(AddressIpv4),
	///IPv6 address.
	Ipv6(AddressIpv6),
	///Unix domain socket.
	Unix(UnixPath<'a>),
	#[cfg(target_os = "linux")]
	///Abstract socket (Linux only).
	Abstract(UnixPath<'a>),
}

impl<'a> Address<'a>
{
	///Get the address family of the socket.
	///
	///This is meant for creating a socket with correct address family.
	pub fn family(&self) -> SocketFamily
	{
		match self {
			&Address::Ipv4(_) => SocketFamily::INET,
			&Address::Ipv6(_) => SocketFamily::INET6,
			&Address::Unix(_) => SocketFamily::UNIX,
			#[cfg(target_os = "linux")]
			&Address::Abstract(_) => SocketFamily::UNIX,
		}
	}
	fn to_low(&self) -> Result<AddressStorage, OsError>
	{
		let mut s = AddressStorage::new_empty();
		s.1 = match self {
			&Address::Ipv4(ref addr) => unsafe {
				//All zeroes is valid value for sockaddr_in.
				let mut xaddr: sockaddr_in = zeroed();
				xaddr.sin_family = SocketFamily::INET.to_short();
				xaddr.sin_port = addr.port.to_be();
				//This assumes that both addr.addr and xaddr.sin_addr are 4 octets.
				xaddr.sin_addr = transmute(addr.addr);
				//size_of_val(xddr) <= s.0.len()
				copy_to_uninit(&mut s.0, &xaddr) as socklen_t
			},
			&Address::Ipv6(ref addr) => unsafe {
				//All zeroes is valid value for sockaddr_in6.
				let mut xaddr: sockaddr_in6 = zeroed();
				xaddr.sin6_family = SocketFamily::INET6.to_short();
				xaddr.sin6_port = addr.port.to_be();
				xaddr.sin6_flowinfo = addr.flow.to_be();
				xaddr.sin6_scope_id = addr.scope;
				//This assumes that both addr.addr and xaddr.sin6_addr are 16 octets.
				xaddr.sin6_addr = transmute(addr.addr);
				//size_of_val(xddr) <= s.0.len()
				copy_to_uninit(&mut s.0, &xaddr) as socklen_t
			},
			&Address::Unix(ref addr) => unsafe {
				let addr = addr.deref();
				//All zeroes is valid value for sockaddr_un.
				let mut xaddr: sockaddr_un = zeroed();
				xaddr.sun_family = SocketFamily::UNIX.to_short();
				//Check that the name fits.
				if addr.len() + 1 > xaddr.sun_path.len() {
					return Err(OsError::ENAMETOOLONG);
				}
				//The check above guarantees this fits.
				let dst: *mut u8 = xaddr.sun_path.as_mut_ptr().cast();
				copy_nonoverlapping(addr.as_ptr(), dst, addr.len());
				//size_of_val(xddr) <= s.0.len()
				copy_to_uninit(&mut s.0, &xaddr) as socklen_t
			},
			#[cfg(target_os = "linux")]
			&Address::Abstract(ref addr) => unsafe {
				let addr = addr.deref();
				//Assume Linux here to simplify this a lot.
				//Do check againgst buffer size and not size of xaddr address field.
				if s.0.len() < addr.len() + 3 {
					return Err(OsError::ENAMETOOLONG);
				}
				let ptr = s.0.as_mut_ptr();
				ptr.cast::<u16>().write_unaligned(SocketFamily::UNIX.to_short());
				ptr.add(2).cast::<u8>().write_unaligned(0);
				copy_nonoverlapping(addr.as_ptr(), ptr.add(3).cast(), addr.len());
				//The address length is one (for NUL) plus length of real address. All this has
				//been initialized above.
				addr.len() as socklen_t + 3
			},
		};
		Ok(s)
	}
}

//The maximum possible address size is size of sockaddr_storage. At least Linux can not do anything sensible
//with more space than that (even through it will not fail).
const ADDR_STORAGE_SIZE: usize = size_of::<libc::sockaddr_storage>();

struct AddressStorage([MaybeUninit<u8>;ADDR_STORAGE_SIZE], socklen_t);

impl AddressStorage
{
	fn new_empty() -> AddressStorage
	{
		AddressStorage([MaybeUninit::uninit();ADDR_STORAGE_SIZE], 0)
	}
	fn new_full() -> AddressStorage
	{
		AddressStorage([MaybeUninit::uninit();ADDR_STORAGE_SIZE], ADDR_STORAGE_SIZE as socklen_t)
	}
	fn to_high(&self) -> Option<Address<'static>>
	{
		let slen = self.1 as usize;
		fail_if_none!(slen < size_of::<sa_family_t>());
		//Assumes:
		//1) self.0 is valid for at least size_of(sa_family_t) bytes => 128 > 4.
		let af = unsafe{self.0.as_ptr().cast::<sa_family_t>().read_unaligned()};
		Some(if af == SocketFamily::INET.to_short() && slen == size_of::<sockaddr_in>() { unsafe {
			//Assumes:
			//1) self.0 is valid for at least size_of(sockaddr_in) bytes => Checked above.
			let xaddr = self.0.as_ptr().cast::<sockaddr_in>().read_unaligned();
			Address::Ipv4(AddressIpv4 {
				addr: transmute(xaddr.sin_addr),
				port: xaddr.sin_port.to_be(),	//This swaps or not.
			})
		}} else if af == SocketFamily::INET6.to_short() && slen == size_of::<sockaddr_in6>() { unsafe {
			//All zeroes is valid value for sockaddr_in6.
			//Assumes:
			//1) self.0 is valid for at least size_of(sockaddr_in6) bytes => Checked above.
			let xaddr = self.0.as_ptr().cast::<sockaddr_in6>().read_unaligned();
			Address::Ipv6(AddressIpv6 {
				addr: transmute(xaddr.sin6_addr),
				port: xaddr.sin6_port.to_be(),		//These swap or not.
				flow: xaddr.sin6_flowinfo.to_be(),
				scope: xaddr.sin6_scope_id,
			})
		}} else if af == SocketFamily::UNIX.to_short() {
			//This one is annoying: Linux truncates the socket address length.
			let offset = crate::sys::unix_path_offset();
			let addr: &[u8] = unsafe{transmute(self.0.get(offset..slen)?)};
			//Abstract sockets start with NUL byte.
			#[cfg(target_os = "linux")] { if let Some(addr) = addr.strip_prefix(&[0]) {
				//If address starts with NUL, it is abstract address (NUL not part of name).
				return Some(Address::Abstract(UnixPath::owned_bytes(addr)));
			}}
			Address::Unix(UnixPath::owned(addr))
		} else {
			return None;	//What kind of address is this?
		})
	}
	fn to_const_sockaddr(&self) -> (*const sockaddr, socklen_t)
	{
		(self.0.as_ptr().cast(), self.1)
	}
	fn to_mut_sockaddr(&mut self) -> (*mut libc::sockaddr, *mut socklen_t)
	{
		(self.0.as_mut_ptr().cast(), self.1.pointer_to_mut())
	}
}

///Kind of socket control message.
#[derive(Copy,Clone,PartialEq,Eq,PartialOrd,Ord,Debug)]
pub struct CmsgKind(pub(crate) c_int, pub(crate) c_int);

//TODO: Is this correct on various *BSD systems?
const CMSG_ALIGN: usize = size_of::<usize>();

///Parse socket control message.
pub struct CmsgParser<'a>(&'a mut [MaybeUninit<u8>], Option<usize>);

impl<'a> CmsgParser<'a>
{
	///Construct a new socket control message parser, giving a placeholder for the control message.
	pub fn new2(buffer: &'a mut [MaybeUninit<u8>]) -> CmsgParser<'a>
	{
		CmsgParser(align_cmsg(buffer), None)
	}
	///Get pointer into placeholder for control message.
	pub fn as_mut_ptr(&mut self) -> *mut libc::c_void { self.0.as_ptr() as _ }
	///Get length of placeoholder for control message.
	pub fn len(&self) -> usize { self.0.len() }
	pub(crate) fn set_len(&mut self, newlen: usize) { self.1 = Some(newlen); }
	///Iterate over all control messages in block of given length, passing each in form `(type, payload)` to
	///given callback.
	pub fn iterate<E>(self, mut f: impl FnMut(CmsgKind, &[u8]) -> Result<(), E>) -> Result<(), E>
	{
		//There is nothing to iterate until set_len() has been called by recvmsg(2).
		let length = match self.1 { Some(x) => x, None => return Ok(()) };
		let mut itr = 0;
		let chdrsz = size_of::<cmsghdr>();
		let control = self.0;
		//Take minimum, so control safe to access up to controllen.
		let controllen = min(control.len(), length);
		while itr < controllen {
			//By loop condition, controllen > itr.
			if controllen - itr < chdrsz { break; } //no space.
			//Assumes itr + size_of(cmsghdr) <= control.len(). This was checked above.
			let chdr = unsafe{structure_from_bytes::<cmsghdr>(control[itr..].as_ptr() as _)};
			let chdrlen = chdr.cmsg_len as usize;	//This might not be usize.
			//cmsg_len includes the header, so it has to be at least that. Additionally, there must be
			//space for chdrlen.
			if chdrlen < chdrsz || controllen - itr < chdrlen { break; }
			//Assumes that:
			//1) chdrlen >= chdrsz => Checked above.
			//2) itr+chdrlen <= control.len() => Checked above.
			//3) The control is initialized up to at least itr+chdrlen => It is initialized up to
			//controllen >= itr+chdrlen.
			let payload: &[u8] = unsafe{transmute(&control[itr+chdrsz..itr+chdrlen])};
			f(CmsgKind(chdr.cmsg_level, chdr.cmsg_type), payload)?;
			//Assumes that CMSG_ALIGN != 0. It is currently 4 or 8.
			itr = itr + (chdrlen + CMSG_ALIGN - 1) / CMSG_ALIGN * CMSG_ALIGN;
		}
		Ok(())
	}
}

///Serialize a control message block.
pub struct CmsgSerializer<'a>(&'a mut [MaybeUninit<u8>], usize);

impl<'a> CmsgSerializer<'a>
{
	///Create a new serializer for control message block from the scratch buffer.
	pub fn new2(x: &'a mut [MaybeUninit<u8>]) -> CmsgSerializer<'a>
	{
		CmsgSerializer(align_cmsg(x), 0)
	}
	///Add a control message of specified type and payload to the block.
	pub fn add<T:Copy>(&mut self, mtype: CmsgKind, payload: T) -> Result<(), OsError>
	{
		//Check that the result fits!
		if self.1 > self.0.len() { return Err(OsError::ENOSPC); }
		//Assumes self.1 < self.0.len(). This was checked above.
		let buffer = &mut self.0[self.1..];
		let hlen = size_of::<cmsghdr>();
		let plen = size_of::<T>();
		let eltsz = hlen + plen;
		//Assumes that CMSG_ALIGN != 0. It is currently 4 or 8.
		let pad = (CMSG_ALIGN - eltsz % CMSG_ALIGN)  % CMSG_ALIGN;
		let used = eltsz + pad;
		if buffer.len() < used { return Err(OsError::ENOSPC); }
		//Assumes that used <= buffer.len(). This was checked above.
		let buffer = &mut buffer[..used];
		//The cmsghdr can have variable fields. cmsghdr can be all zeroes.
		let mut header: cmsghdr = unsafe{zeroed()};
		//eltsz should be small enough to fit whatever type cmsg_len is.
		header.cmsg_len = eltsz as _;
		header.cmsg_level = mtype.0;
		header.cmsg_type = mtype.1;
		let (bheader, buffer) = buffer.split_at_mut(hlen);
		let (bpayload, bpad) = buffer.split_at_mut(plen);
		unsafe {
			//Assumes that:
			//1) bheader is at least size_of(cmsghdr) bytes => It is hlen = size_of(cmsghdr) bytes.
			bheader.as_mut_ptr().cast::<cmsghdr>().write_unaligned(header);
			//Assumes that:
			//1) bpayload is at least size_of(T) bytes => It is plen = size_of(T) bytes.
			bpayload.as_mut_ptr().cast::<T>().write_unaligned(payload);
		}
		for b in bpad.iter_mut() { *b = MaybeUninit::new(0); }
		self.1 += used;
		Ok(())
	}
	///Get raw pointer into the constructed control message.
	///
	///This is mutable because many functions take it that way, it should not be modified!
	fn as_ptr(&self) -> *mut libc::c_void
	{
		//If control message is empty, pass as NULL/0.
		if self.1 > 0 { self.0.as_ptr() as _ } else { null_mut() }
	}
	///Get length of constructed control message.
	fn len(&self) -> usize { self.1 }
}

define_bitfield_type!([doc=r#"Flags for `epoll_create(2)`."#];EpollCreateFlags i32);
impl EpollCreateFlags
{
	define_symbol!([doc=r#"Automatically close the epoll descriptor on exec."#];
		CLOEXEC: EpollCreateFlags VAL 0x00080000);
}

///Command for `epoll_ctl(2)`.
#[derive(Copy,Clone)]
pub enum EpollCtlCmd
{
	///Add event.
	Add(EpollEventType, u64),
	///Modify or rearm event.
	Modify(EpollEventType, u64),
	///Delete event.
	Delete,
}


//EpollEvent is special: It is packed on x86-64 (but not on any other arch!).
#[cfg_attr(target_arch = "x86_64", repr(packed))]
#[derive(Copy,Clone)]
#[repr(C)]
///`Epoll(7)` event type.
pub struct EpollEvent
{
	events: EpollEventType,
	token: u64,
}

impl EpollEvent
{
	///Create a new Epoll event with specified type and token.
	pub fn new(events: EpollEventType, token: u64) -> EpollEvent
	{
		EpollEvent {
			events: events,
			token: token,
		}
	}
	///Get the epoll event type associated with this event.
	pub fn get_events(&self) -> EpollEventType { self.events }
	///Get the token associated with this event.
	pub fn get_token(&self) -> u64 { self.token }
	#[cfg(target_os = "linux")]
	pub(crate) fn set_events(&mut self, ev: EpollEventType) { self.events = ev; }
	#[cfg(target_os = "linux")]
	pub(crate) fn set_token(&mut self, tok: u64) { self.token = tok; }
}

define_bitfield_type!([doc=r#"Event flags for `epoll(7)`"#];EpollEventType u32);
impl EpollEventType
{
	define_symbol!([doc=r#"Normal data is available."#];IN: EpollEventType VAL 0x00000001);
	define_symbol!([doc=r#"Out-of-Band data available."#];PRI: EpollEventType VAL 0x00000002);
	define_symbol!([doc=r#"Available for write."#];OUT: EpollEventType VAL 0x00000004);
	define_symbol!([doc=r#"File is in error state."#];ERR: EpollEventType VAL 0x00000008);
	define_symbol!([doc=r#"Pipe or socket hung up."#];HUP: EpollEventType VAL 0x00000010);
	define_symbol!([doc=r#"Invalid file descriptor."#];NVAL: EpollEventType VAL 0x00000020);
	define_symbol!([doc=r#"Normal data is available."#];RDNORM: EpollEventType VAL 0x00000040);
	define_symbol!([doc=r#"Out-of-Band data is available."#];RDBAND: EpollEventType VAL 0x00000080);
	define_symbol!([doc=r#"Available for normal write."#];WRNORM: EpollEventType VAL 0x00000100);
	define_symbol!([doc=r#"Available for Out-of-Band write."#];WRBAND: EpollEventType VAL 0x00000200);
	define_symbol!([doc=r#"Available message."#];MSG: EpollEventType VAL 0x00000400);
	//800 and 1000 do not seem to be defined.
	define_symbol!([doc=r#"Pipe or socket hung up."#];RDHUP: EpollEventType VAL 0x00002000);
	define_symbol!([doc=r#"Only signal event on one watch."#];EXCLUSIVE: EpollEventType VAL 0x10000000);
	define_symbol!([doc=r#"Prevent system from sleeping."#];WAKEUP: EpollEventType VAL 0x20000000);
	define_symbol!([doc=r#"Auto-disable event after triggering."#];ONESHOT: EpollEventType VAL 0x40000000);
	define_symbol!([doc=r#"Event is Edge-Triggered."#];ET: EpollEventType VAL 0x80000000);
	pub fn any_of(&self, compare: EpollEventType) -> bool { self.0 & compare.0 != 0 }
	#[cfg(target_os = "linux")]
	pub(crate) fn wrap(v: u32) -> EpollEventType { EpollEventType(v) }
	#[cfg(target_os = "linux")]
	pub(crate) fn into_inner(self) -> u32 { self.0 }
}


///Watch descriptor for `inotify(7)`.
#[derive(Copy,Clone,Debug,PartialEq,PartialOrd,Eq,Ord)]
pub struct InotifyWatchDescriptor(pub(crate) i32);

///`Inotify(7)` event.
#[derive(Copy,Clone,Debug)]
pub struct InotifyEvent<'a>
{
	watch_desc: InotifyWatchDescriptor,
	mask: InotifyEventType,
	cookie: u32,
	name: &'a [u8]
}

impl<'a> InotifyEvent<'a>
{
	pub(crate) fn new(array: &'a [u8], ptr: &mut usize) -> Result<Option<InotifyEvent<'a>>, ()>
	{
		if *ptr >= array.len() { return Ok(None); }		//No more entries.
		let array = &array[*ptr..];				//This is in-range by check above.
		let hsize = size_of::<InotifyHeader>();
		if array.len() < hsize { return Err(()); }		//Incomplete entry.
		//Assumes:
		//1) hdr is at least hsize bytes => hsize = size(InotifyHeader) =>
		//   true by definition.
		let hdr = unsafe {array.as_ptr().cast::<InotifyHeader>().read_unaligned()};
		let elen = hdr.len as usize + size_of::<InotifyHeader>();
		if elen < hsize || elen > array.len() { return Err(()); }	//Malformed entry.
		let mut name = &array[hsize..elen];				//Raw name.
		*ptr += elen;							//Next entry.
		//Strip any trailing NULs off the name.
		while name.len() > 0 && name[name.len()-1] == 0 { name = &name[..name.len()-1]; }
		Ok(Some(InotifyEvent {
			watch_desc: InotifyWatchDescriptor(hdr.wd),
			mask: InotifyEventType(hdr.mask),
			cookie: hdr.cookie,
			name: name,
		}))
	}
	///Get the watch descriptor (as returned by `inotify_add_watch(2)`).
	pub fn get_watch_descriptor(&self) -> InotifyWatchDescriptor { self.watch_desc }
	///Get type of the event.
	pub fn get_event_mask(&self) -> InotifyEventType { self.mask }
	///Get event cookie (used to match both halves of move event).
	pub fn get_cookie(&self) -> u32 { self.cookie }
	///Get the name of file.
	pub fn get_name(&self) -> &'a [u8] { self.name }
}

define_bitfield_type!([doc=r#"Event flags for `inotify(7)`."#];InotifyEventType u32);
impl InotifyEventType
{
	define_symbol!([doc=r#"File was accessed."#];ACCESS: InotifyEventType VAL 0x00000001);
	define_symbol!([doc=r#"File contents were modified."#];MODIFY: InotifyEventType VAL 0x00000002);
	define_symbol!([doc=r#"File attributes were changed."#];ATTRIB: InotifyEventType VAL 0x00000004);
	define_symbol!([doc=r#"File open for write was closed."#];CLOSE_WRITE: InotifyEventType VAL 0x00000008);
	define_symbol!([doc=r#"File open for read only was closed."#];
		CLOSE_NOWRITE: InotifyEventType VAL 0x00000010);
	define_symbol!([doc=r#"File was opened."#];OPEN: InotifyEventType VAL 0x00000020);
	define_symbol!([doc=r#"File was moved from."#];MOVED_FROM: InotifyEventType VAL 0x00000040);
	define_symbol!([doc=r#"File was moved to."#];MOVED_TO: InotifyEventType VAL 0x00000080);
	define_symbol!([doc=r#"File created."#];CREATE: InotifyEventType VAL 0x00000100);
	define_symbol!([doc=r#"File deleted."#];DELETE: InotifyEventType VAL 0x00000200);
	define_symbol!([doc=r#"The watched object was deleted."#];DELETE_SELF: InotifyEventType VAL 0x00000400);
	define_symbol!([doc=r#"The watched object was moved."#];MOVE_SELF: InotifyEventType VAL 0x00000800);
	define_symbol!([doc=r#"Filesystem was unmounted."#];UNMOUNT: InotifyEventType VAL 0x00002000);
	define_symbol!([doc=r#"Event queue overflow."#];Q_OVERFLOW: InotifyEventType VAL 0x00004000);
	define_symbol!([doc=r#"Watch was deleted."#];IGNORED: InotifyEventType VAL 0x00008000);
}

#[repr(C)]
#[derive(Debug)]
pub(crate)struct InotifyHeader
{
	wd: c_int,
	mask: u32,
	cookie: u32,
	len: u32
}

define_bitfield_type!([doc=r#"Flags for `inotify_init1(2)`."#];InotifyInitFlags c_int);
impl InotifyInitFlags
{
	//Use the arch-universal Linux constant values, since this is meaningful only on Linux.
	define_symbol!([doc=r#"Create the inotify descriptor in nonblocking mode"#];
		NONBLOCK: InotifyInitFlags VAL 0x00000800);
	define_symbol!([doc=r#"Close the inotify descriptor on exec."#];
		CLOEXEC: InotifyInitFlags VAL 0x00080000);
}

define_bitfield_type!([doc=r#"Flags for `signalfd(2)`."#];SignalFdFlags c_int);
impl SignalFdFlags
{
	//Use the arch-universal Linux constant values, since this is meaningful only on Linux.
	define_symbol!([doc=r#"Create the signalfd descriptor in nonblocking mode"#];
		NONBLOCK: SignalFdFlags VAL 0x00000800);
	define_symbol!([doc=r#"Close the signalfd descriptor on exec."#];
		CLOEXEC: SignalFdFlags VAL 0x00080000);
}

define_bitfield_type!([doc=r#"Protection for `mmap(2)`."#];MmapProtection c_int);
impl MmapProtection
{
	define_symbol!([doc=r#"No access."#];PROT_NONE: MmapProtection as NONE);
	define_symbol!([doc=r#"Read access."#];PROT_READ: MmapProtection as READ);
	define_symbol!([doc=r#"Read access."#];PROT_WRITE: MmapProtection as WRITE);
	define_symbol!([doc=r#"Read access."#];PROT_EXEC: MmapProtection as EXEC);
}

define_bitfield_type!([doc=r#"Flags for `mmap(2)`."#];MmapFlags c_int);
impl MmapFlags
{
	define_symbol!([doc=r#"Shared mapping."#];MAP_SHARED: MmapFlags as SHARED);
	define_symbol!([doc=r#"Private mapping."#];MAP_PRIVATE: MmapFlags as PRIVATE);
	define_symbol!([doc=r#"Fixed mapping."#];MAP_FIXED: MmapProtection as FIXED);
}

#[derive(Copy,Clone,Debug,PartialEq,Eq)]
///Target for seeking.
pub enum SeekTarget
{
	///Specified offset in file.
	Set(u64),
	///Specified offset from current position.
	Delta(i64),
	///Specified offset in file, couting backwards from the end.
	SetEnd(u64),
}

//This is not correct for Linux on Alpha and PA-RISC, but those are dead dead dead, and completely unsupported by
//Rust.
define_bitfield_type!([doc=r#"Flags for `socket(2)`."#];SocketFlags i32);
impl SocketFlags
{
	define_symbol!([doc=r#"Make socket nonblocking."#];O_NONBLOCK: SocketFlags as NONBLOCK);
	define_symbol!([doc=r#"Make socket close on exec."#];O_CLOEXEC: SocketFlags as CLOEXEC);
}

define_bitfield_type!([doc=r#"Flags for `pipe(2)`."#];PipeFlags i32);
impl PipeFlags
{
	define_symbol!([doc=r#"Make pipe nonblocking."#];O_NONBLOCK: PipeFlags as NONBLOCK);
	define_symbol!([doc=r#"Make pipe close on exec."#];O_CLOEXEC: PipeFlags as CLOEXEC);
}

define_bitfield_type!([doc=r#"
Flags for `send(2)`, `recv(2)`, `recvfrom(2)`, `sendto(2)`, `recvmsg(2)` or `sendmsg(2)`.
"#];SendRecvFlags i32);

define_enum_type!([doc=r#"The address family of socket."#];SocketFamily i32 NODEFAULT);
impl SocketFamily
{
	fn to_short(self) -> sa_family_t { self.0 as sa_family_t }
	define_symbol!([doc=r#"IPv4."#];AF_INET: SocketFamily as INET);
	define_symbol!([doc=r#"IPv6."#];AF_INET6: SocketFamily as INET6);
	define_symbol!([doc=r#"Unix domain sockets."#];AF_UNIX: SocketFamily as UNIX);
	///Invalid address family.
	pub const INVALID: SocketFamily = SocketFamily(-1);
}

impl Display for SocketFamily
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> { Display::fmt(&self.0, f) }
}

define_enum_type!([doc=r#"The protocol of socket."#];SocketProtocol i32);
define_enum_type!([doc=r#"The type of socket"#];SocketType i32 NODEFAULT);
impl SocketType
{
	define_symbol!([doc=r#"Datagram socket (packets; unreliable)."#];SOCK_DGRAM: SocketType as DGRAM);
	define_symbol!([doc=r#"Seqpacket socket (packets; reliable)."#];SOCK_SEQPACKET: SocketType as SEQPACKET);
	define_symbol!([doc=r#"Stream socket (bytes; reliable)."#];SOCK_STREAM: SocketType as STREAM);
}

define_enum_type!([doc=r#"The operation for `shutdown(2)`"#];ShutdownFlags i32 NODEFAULT);
impl ShutdownFlags
{
	define_symbol!([doc=r#"Shutdown read side of socket."#];SHUT_RD: ShutdownFlags as RD);
	define_symbol!([doc=r#"Shutdown both sides of socket."#];SHUT_RDWR: ShutdownFlags as RDWR);
	define_symbol!([doc=r#"Shutdown write side of socket."#];SHUT_WR: ShutdownFlags as WR);
}

define_enum_type!([doc=r#"A special file descriptor`"#];SpecialFd i32 NODEFAULT);
impl SpecialFd
{
	///STDIN.
	pub const STDIN: SpecialFd = SpecialFd(0);
	///STDOUT.
	pub const STDOUT: SpecialFd = SpecialFd(1);
	///STDERR.
	pub const STDERR: SpecialFd = SpecialFd(2);
}

///Directory Filename.
pub struct DirectoryFilename(super::CString);

impl DirectoryFilename
{
	pub(crate) fn new(s: super::CString) -> DirectoryFilename { DirectoryFilename(s) }
}

impl Deref for DirectoryFilename
{
	type Target = [u8];
	fn deref(&self) -> &[u8] { self.0.as_slice() }
}

///Directory stream.
pub struct DirectoryStream(super::Dirstream);

impl Iterator for DirectoryStream
{
	type Item = Result<DirectoryFilename, OsError>;
	fn next(&mut self) -> Option<Result<DirectoryFilename, OsError>>
	{
		Some(self.0.next()?.map(DirectoryFilename))
	}
}

fn fixup_flags1(x: &FileDescriptor, flags: i32, nonblock: i32, cloexec: i32) ->
	Result<(), OsError>
{
	if_linux!({}, {
		if flags & nonblock != 0 { x.set_nonblock(true)?; }
		if flags & cloexec != 0 { x.set_cloexec(true)?; }
	});
	Ok(())
}

fn fixup_flags2(r: &FileDescriptor, w: &FileDescriptor, flags: i32, nonblock: i32, cloexec: i32) ->
	Result<(), OsError>
{
	if_linux!({}, {
		if flags & nonblock != 0 { r.set_nonblock(true)?; w.set_nonblock(true)?; }
		if flags & cloexec != 0 { r.set_cloexec(true)?; w.set_cloexec(true)?; }
	});
	Ok(())
}

///Type of raw file descriptor.
pub type RawFdT = c_int;

///A file descriptor.
///
///The file descriptor referenced will be closed on drop.
#[derive(Debug)]
pub struct FileDescriptor(FileDescriptorB);

impl Drop for FileDescriptor
{
	fn drop(&mut self)
	{
		//This structure owns self.0, so should close it when dropped.
		unsafe{self.0.close()}
	}
}

impl Display for FileDescriptor
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> { Display::fmt(&self.0, f) }
}

impl FileDescriptor
{
	///Create a new file descriptor from raw file descriptor.
	///
	///# Safety:
	///
	///The file descriptor must be open and the new object takes ownership of it.
	pub unsafe fn new(fd: RawFdT) -> FileDescriptor { FileDescriptor(FileDescriptorB(fd)) }
	///Is special file descriptor?
	///
	///STDIN, STDOUT and STDERR count as special, other file descriptors do not.
	pub fn is_special(&self) -> bool { self.0.is_special() }
	///Get the raw file descriptor.
	pub fn as_raw_fd(&self) -> RawFdT { self.0.as_raw_fd() }
	///Drop this file descriptor without closing and return raw file descriptor.
	pub fn into_raw_fd(self) -> RawFdT { self.into_fdb().as_raw_fd() }
	///Get the associated borrowed file descriptor.
	pub fn as_fdb(&self) -> FileDescriptorB { self.0 }
	///Drop this file descriptor without closing and return borrowed file descriptor.
	pub fn into_fdb(self) -> FileDescriptorB
	{
		let fd = self.0;
		forget(self);
		fd
	}
	///Call fdopendir(3) and return stream.
	pub fn into_dirstream(self) -> Result<DirectoryStream, OsError>
	{
		Ok(DirectoryStream(super::Dirstream::new(self)?))
	}
	///Call mkdirat(2).
	pub fn mkdirat(&self, path: &Path, public: bool) -> Result<(), OsError>
	{
		//The file is open.
		unsafe{self.as_fdb().mkdirat(path, public)}
	}
	///Call openat(2) and return file descriptor.
	pub fn openat(&self, path: &Path, flags: OpenFlags) -> Result<FileDescriptor, OsError>
	{
		//The file is open.
		unsafe{self.as_fdb().openat(path, flags)}
	}
	///Call unlinkat(2).
	pub fn unlinkat(&self, path: &Path) -> Result<(), OsError>
	{
		//The file is open.
		unsafe{self.as_fdb().unlinkat(path)}
	}
	///Call fstatat(2) with flags=0.
	pub fn statat(&self, path: &Path) -> Result<Stat, OsError>
	{
		//The file is open.
		unsafe{self.as_fdb().statat(path)}
	}
	///Call fstatat(2) with flags=AT_SYMLINK_NOFOLLOW.
	pub fn lstatat(&self, path: &Path) -> Result<Stat, OsError>
	{
		//The file is open.
		unsafe{self.as_fdb().lstatat(path)}
	}
	///Call fchmodat(2) with flags=0.
	pub fn chmodat(&self, path: &Path, mode: u32) -> Result<(), OsError>
	{
		//The file is open.
		unsafe{self.as_fdb().chmodat(path, mode)}
	}
	///Call fchownat(2) with flags=0.
	pub fn chownat(&self, path: &Path, uid: Uid, gid: Gid) -> Result<(), OsError>
	{
		//The file is open.
		unsafe{self.as_fdb().chownat(path, uid, gid)}
	}
	///Call fchownat(2) with flags=AT_SYMLINK_NOFOLLOW.
	pub fn lchownat(&self, path: &Path, uid: Uid, gid: Gid) -> Result<(), OsError>
	{
		//The file is open.
		unsafe{self.as_fdb().lchownat(path, uid, gid)}
	}
	///Call symlinkat(2).
	pub fn symlinkat(&self, path: &Path, target: &Path) -> Result<(), OsError>
	{
		//The file is open.
		unsafe{self.as_fdb().symlinkat(path, target)}
	}
	///Call readlinkat(2).
	pub fn readlinkat(&self, path: &Path) -> Result<DirectoryFilename, OsError>
	{
		//The file is open.
		unsafe{self.as_fdb().readlinkat(path)}
	}
	///Call renameat(2) within directory.
	pub fn renameat_indir(&self, oldpath: &Path, newpath: &Path) ->
		Result<(), OsError>
	{
		//The file is open.
		unsafe{self.as_fdb().renameat(oldpath, self.as_fdb(), newpath)}
	}
	///Call pipe(2) and return the file descriptors. The first is read end, the second is write end.
	pub fn pipe() -> Result<(FileDescriptor, FileDescriptor), OsError>
	{
		Self::pipe2(Default::default())
	}
	///Call pipe(2) and return the file descriptors. The first is read end, the second is write end.
	pub fn pipe2(flags: PipeFlags) -> Result<(FileDescriptor, FileDescriptor), OsError>
	{
		let mut fd = [0;2];
		let t = if_linux!({
			//Assumes that fd is array of at least 2 c_ints => True by definition.
			unsafe{libc::pipe2(fd.as_mut_ptr(), flags.0)}
		}, {
			//Assumes that fd is array of at least 2 c_ints => True by definition.
			unsafe{libc::pipe(fd.as_mut_ptr())}
		});
		t.errno()?;
		let r = FileDescriptor(FileDescriptorB(fd[0]));
		let w = FileDescriptor(FileDescriptorB(fd[1]));
		fixup_flags2(&r, &w, flags.0, PipeFlags::NONBLOCK.0, PipeFlags::CLOEXEC.0)?;
		Ok((r, w))
	}
	///Call socketpair(2) and return the file descriptors.
	pub fn socketpair(domain: SocketFamily, stype: SocketType, protocol: SocketProtocol) ->
		Result<(FileDescriptor, FileDescriptor), OsError>
	{
		Self::socketpair2(domain, stype, protocol, Default::default())
	}
	///Call socketpair(2) and return the file descriptors.
	pub fn socketpair2(domain: SocketFamily, stype: SocketType, protocol: SocketProtocol, flags: SocketFlags) ->
		Result<(FileDescriptor, FileDescriptor), OsError>
	{
		let mut fd = [0;2];
		let t = if_linux!({
			//Assumes that fd is array of at least 2 c_ints => True by definition.
			unsafe{libc::socketpair(domain.0, stype.0|flags.0, protocol.0, fd.as_mut_ptr())}
		}, {
			//Assumes that fd is array of at least 2 c_ints => True by definition.
			unsafe{libc::socketpair(domain.0, stype.0, protocol.0, fd.as_mut_ptr())}
		});
		t.errno()?;
		let r = FileDescriptor(FileDescriptorB(fd[0]));
		let w = FileDescriptor(FileDescriptorB(fd[1]));
		fixup_flags2(&r, &w, flags.0, SocketFlags::NONBLOCK.0, SocketFlags::CLOEXEC.0)?;
		Ok((r, w))
	}
	///Call socket(2) and return the file descriptor.
	pub fn socket(domain: SocketFamily, stype: SocketType, protocol: SocketProtocol) ->
		Result<FileDescriptor, OsError>
	{
		Self::socket2(domain, stype, protocol, SocketFlags::default())
	}
	///Call socket(2) and return the file descriptor.
	pub fn socket2(domain: SocketFamily, stype: SocketType, protocol: SocketProtocol, flags: SocketFlags) ->
		Result<FileDescriptor, OsError>
	{
		//Uncondtionally safe.
		let fd = if_linux!({
			let stype = stype.0 | flags.0;
			unsafe{libc::socket(domain.0, stype, protocol.0)}
		}, {
			unsafe{libc::socket(domain.0, stype.0, protocol.0)}
		});
		let fd = FileDescriptor(FileDescriptorB(fd.errno_sval()?));
		fixup_flags1(&fd, flags.0, SocketFlags::NONBLOCK.0, SocketFlags::CLOEXEC.0)?;
		Ok(fd)
	}
	///Call dup(2). Note that it is only possible to duplicate into STDIN/STDOUT/STDERR.
	pub fn dup2(&self, tgt: SpecialFd) -> Result<(), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.dup2(tgt)}
	}
	///Call read(2).
	pub fn read(&self, buf: &mut [u8]) -> Result<usize, OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.read(buf)}
	}
	///Call read(2). Buffer can be initially uninitialized.
	pub fn read_uninit2<'a>(&self, buf: &'a mut [MaybeUninit<u8>]) -> Result<&'a [u8], OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.read_uninit2(buf)}
	}
	///Call read(2), interpretting as inotify records.
	pub fn read_inotify<F>(&self, f: F) -> Result<(), OsError> where F: FnMut(InotifyEvent)
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.read_inotify(f)}
	}
	///Call read(2), interpretting as siginfo records.
	pub fn read_siginfo<F>(&self, f: F) -> Result<(), OsError> where F: FnMut(SignalEvent)
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.read_siginfo(f)}
	}
	///Call readv(2)
	pub fn readv(&self, buf: &[&mut [u8]]) -> Result<usize, OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.readv(buf)}
	}
	///Call write(2)
	pub fn write(&self, buf: &[u8]) -> Result<usize, OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.write(buf)}
	}
	///Call writev(2)
	pub fn writev(&self, buf: &[&[u8]]) -> Result<usize, OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.writev(buf)}
	}
	///Call recv(2)
	pub fn recv(&self, buf: &mut [u8], flags: SendRecvFlags) -> Result<usize, OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.recv(buf, flags)}
	}
	///Call send(2)
	pub fn send(&self, buf: &[u8], flags: SendRecvFlags) -> Result<usize, OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.send(buf, flags)}
	}
	///Call recvfrom(2).
	pub fn recvfrom(&self, buf: &mut [u8], flags: SendRecvFlags) ->
		Result<(usize, Option<Address<'static>>), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.recvfrom(buf, flags)}
	}
	///Call sendto(2).
	pub fn sendto(&self, buf: &[u8], to: &Address, flags: SendRecvFlags) -> Result<usize, OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.sendto(buf, to, flags)}
	}
	///Call sendmsg(2).
	pub fn sendmsg(&self, to: Option<&Address>, iov: &[&[u8]], control: Option<&CmsgSerializer>,
		flags: SendRecvFlags) -> Result<usize, OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.sendmsg(to, iov, control, flags)}
	}
	///Call recvmsg(2).
	pub fn recvmsg(&self, get_addr: bool, iov: &[&mut [u8]], control: Option<&mut CmsgParser>,
		flags: SendRecvFlags) -> Result<(usize, Option<Address<'static>>), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.recvmsg(get_addr, iov, control, flags)}
	}
	///Call fstat(2)
	pub fn stat(&self) -> Result<Stat, OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.stat()}
	}
	///Call fsync(2)
	pub fn sync(&self) -> Result<(), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.sync()}
	}
	///Call lseek(2)
	pub fn seek(&self, position: SeekTarget) -> Result<u64, OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.seek(position)}
	}
	///Call shutdown(2)
	pub fn shutdown(&self, how: ShutdownFlags) -> Result<(), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.shutdown(how)}
	}
	///Call `getsockopt(..., SOL_SOCKET, SO_ERROR, ...)`.
	///
	///# Notes:
	///
	///The outer error is from getsockopt() itself, the inner error is the connect error.
	pub fn so_error(&self) -> Result<Result<(), OsError>, OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.so_error()}
	}
	///Call `getsockopt(..., SOL_SOCKET, SO_TYPE, ...)`
	pub fn so_type(&self) -> Result<SocketType, OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.so_type()}
	}
	///Set or clear O_CLOEXEC on the file descriptor.
	pub fn set_cloexec(&self, status: bool) -> Result<(), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.set_cloexec(status)}
	}
	///Set or clear O_NONBLOCK on the file descriptor.
	pub fn set_nonblock(&self, status: bool) -> Result<(), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.set_nonblock(status)}
	}
	///Call `setsockopt(..., SOL_SOCKET, SO_REUSEADDR, ...)`
	pub fn set_reuseaddr(&self, status: bool) -> Result<(), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.set_reuseaddr(status)}
	}
	///Call `setsockopt(..., IPPROTO_IPV6, IPV6_V6ONLY, ...)`
	pub fn set_v6only(&self, status: bool) -> Result<(), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.set_v6only(status)}
	}
	///Call `setsockopt(..., IPPROTO_TCP, TCP_NODELAY, ...)`
	pub fn set_tcp_nodelay(&self, status: bool) -> Result<(), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.set_tcp_nodelay(status)}
	}
	///Call accept(2).
	pub fn accept(&self) -> Result<(FileDescriptor, Option<Address<'static>>), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.accept2(Default::default())}
	}
	///Call accept(2).
	pub fn accept2(&self, flags: SocketFlags) -> Result<(FileDescriptor, Option<Address<'static>>), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.accept2(flags)}
	}
	///Call bind(2)
	pub fn bind(&self, to: &Address) -> Result<(), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.bind(to)}
	}
	///Call connect(2).
	///
	///# Notes:
	///
	///Instead of returning Err(EINPROGRESS), this returns Ok(false). Ok(true) means synchronously completed
	///connection.
	pub fn connect(&self, to: &Address) -> Result<bool, OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.connect(to)}
	}
	///Call getsockname(2).
	pub fn local_address(&self) -> Result<Option<Address<'static>>, OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.local_address()}
	}
	///Call getpeername(2).
	pub fn remote_address(&self) -> Result<Option<Address<'static>>, OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.remote_address()}
	}
	///Call listen(2).
	pub fn listen(&self, backlog: u32) -> Result<(), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.listen(backlog)}
	}
	///Call isatty(2).
	pub fn isatty(&self) -> bool
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.isatty()}
	}
	///Call setsockopt(..., SOL_SOCKET, SO_PASSCRED, ...).
	///
	///Only supported on Linux, otherwise returns EOPNOTSUPP.
	pub fn set_passcred(&self, status: bool) -> Result<(), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.set_passcred(status)}
	}
	///Call epoll_create1(2).
	///
	///Only supported on Linux, otherwise returns ENOSYS.
	pub fn epoll_create(flags: EpollCreateFlags) -> Result<FileDescriptor, OsError>
	{
		crate::sys::epoll_create(flags)
	}
	///Call epoll_ctl(2).
	///
	///Only supported on Linux, otherwise returns ENOSYS.
	pub fn epoll_ctl(&self, fd: &FileDescriptor, cmd: EpollCtlCmd) -> Result<(), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		//Assumes that fd.0 is open => True by definition.
		unsafe{self.0.epoll_ctl(fd.0, cmd)}
	}
	///Call epoll_ctl(2).
	///
	///Only supported on Linux, otherwise returns ENOSYS.
	///
	///# Safety:
	///
	///The target file descriptor must be open.
	pub unsafe fn epoll_ctlb(&self, fd: FileDescriptorB, cmd: EpollCtlCmd) -> Result<(), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		////Assumes that fd is open => Precondition for the function.
		unsafe{self.0.epoll_ctl(fd, cmd)}
	}
	///Call epoll_wait(2).
	///
	///Only supported on Linux, otherwise returns ENOSYS.
	pub fn epoll_wait(&self, events: &mut [EpollEvent], timeout: i32) -> Result<u32, OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.epoll_wait(events, timeout)}
	}
	///Call inotify_init1(2).
	///
	///Only supported on Linux, otherwise returns ENOSYS.
	pub fn inotify_init(flags: InotifyInitFlags) -> Result<FileDescriptor, OsError>
	{
		crate::sys::inotify_init(flags)
	}
	///Call signalfd(2).
	///
	///Only supported on Linux, otherwise returns ENOSYS.
	pub fn signalfd(signals: SignalSet, flags: SignalFdFlags) -> Result<FileDescriptor, OsError>
	{
		crate::sys::signalfd(signals, flags)
	}
	///Call `fsetfacl` on the specified fd.
	///
	///Only supported on Linux, otherwise returns ENOSYS.
	pub fn setfacl(&self, acl: &impl AclWrite) -> Result<(), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.setfacl(acl)}
	}
	///Call `fchmod(2)`.
	pub fn chmod(&self, mode: u32) -> Result<(), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.chmod(mode)}
	}
	///Call `fchown(2)`.
	pub fn chown(&self, uid: Uid, gid: Gid) -> Result<(), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.chown(uid, gid)}
	}
	///Call `mmap(2)`.
	pub unsafe fn mmap(&self,addr: *mut u8, size: usize, prot: MmapProtection, flags: MmapFlags,
		offset: u64) -> Result<*mut u8, OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.mmap(addr, size, prot, flags, offset)}
	}
	///Call `fgetxattr(2)`.
	pub fn getxattr(&self, name: &Path) -> Result<OwnedByteString, OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.getxattr(name)}
	}
	///Call `fsetxattr(2)`.
	pub fn setxattr(&self, name: &Path, value: &[u8], flags: SetxattrFlags) -> Result<(), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.setxattr(name, value, flags)}
	}
	///Call `ftruncate(2)`.
	pub fn truncate(&self, size: u64) -> Result<(), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{self.0.truncate(size)}
	}
}

unsafe fn __do_getsockopt<T:Sized>(fd: RawFdT, level: i32, optname: i32, val: &mut T) ->
	Result<libc::socklen_t, OsError>
{
	let mut len = size_of::<T>() as libc::socklen_t;
	unsafe {
		let val: *mut T = val.pointer_to_mut();
		libc::getsockopt(fd, level, optname, val.cast(), len.pointer_to_mut()).errno()?;
		Ok(len)
	}
}

pub(crate) unsafe fn __do_setsockopt<T:Sized>(fd: RawFdT, level: i32, optname: i32, val: T) -> Result<(), OsError>
{
	let len = size_of::<T>() as libc::socklen_t;
	unsafe {
		let val: *const T = val.pointer_to();
		libc::setsockopt(fd, level, optname, val.cast(), len).errno()
	}
}

//Note that FileDescriptorB::close() refuses to close these, so these are safe to assume are open.
///The file descriptor for standard input.
pub const STDIN: FileDescriptor = FileDescriptor(FileDescriptorB(0));
///The file descriptor for standard output.
pub const STDOUT: FileDescriptor = FileDescriptor(FileDescriptorB(1));
///The file descriptor for standard error.
pub const STDERR: FileDescriptor = FileDescriptor(FileDescriptorB(2));


///Borrowed file descriptor.
///
///These file descriptors are not automatically closed.
///
///# Safety:
///
///Most methods are unsafe, because there is no way to know that the file descriptor is actually open and still
///refering to the original file.
#[derive(Copy,Clone,Debug,PartialEq,Eq,PartialOrd,Ord,Hash)]
#[repr(C)]
pub struct FileDescriptorB(RawFdT);

impl Display for FileDescriptorB
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> { Display::fmt(&self.0, f) }
}

impl FileDescriptorB
{
	///Create a borrowed file descriptor wrapping specified raw file descriptor.
	pub fn new(fd: RawFdT) -> FileDescriptorB { FileDescriptorB(fd) }
	///Make `FileDescriptor` out of borrowed file descriptor. The file will be closed when the returned file
	///descriptor goes out of scope.
	pub unsafe fn autoclose(self) -> FileDescriptor { FileDescriptor(self) }
	///Call close(2).
	///
	///# Notes:
	///
	///STDIN/STDOUT/STDERR can not be closed. To close STDOUT, dup2 /dev/null on top of it.
	pub unsafe fn close(&self)
	{
		if self.is_special() { return; }	//No closing STDIN/STDOUT/STDERR.
		//It is unclear what happens if close() gives an error. Assume that the file got closed anyway.
		//Assumes self.0 is open => Precondition of function.
		unsafe{libc::close(self.0);}
	}
	///Is special file descriptor?
	///
	///STDIN, STDOUT and STDERR count as special, other file descriptors do not.
	pub fn is_special(&self) -> bool { self.0 <= 2 }
	///Get underlying raw file descriptor.
	pub fn as_raw_fd(&self) -> RawFdT { self.0 }
	///Call dup(2). Note that it is only possible to duplicate into STDIN/STDOUT/STDERR.
	pub unsafe fn dup2(&self, tgt: SpecialFd) -> Result<(), OsError>
	{
		//It is only possible to dup2 into STDIN/STDOUT/STDERR.
		//Assumes self.0 is open => Precondition of function.
		unsafe{libc::dup2(self.0, tgt.0)}.errno()?;
		//O_CLOEXEC MUST be cleared since fds 0/1/2 must always inherit.
		unsafe{FileDescriptorB(tgt.0).set_cloexec(false)}?;
		Ok(())
	}
	///Call mkdirat(2).
	pub unsafe fn mkdirat(&self, path: &Path, public: bool) -> Result<(), OsError>
	{
		unsafe{Path::mkdirat(Some(*self), path, public)}
	}
	///Call openat(2) and return file descriptor.
	pub unsafe fn openat(&self, path: &Path, flags: OpenFlags) -> Result<FileDescriptor, OsError>
	{
		//The file is open.
		unsafe{Path::openat(Some(*self), path, flags)}
	}
	///Call unlinkat(2).
	pub unsafe fn unlinkat(&self, path: &Path) -> Result<(), OsError>
	{
		//The file is open.
		unsafe{Path::unlinkat(Some(*self), path)}
	}
	///Call fstatat(2) with flags=0.
	pub unsafe fn statat(&self, path: &Path) -> Result<Stat, OsError>
	{
		unsafe{Path::fstatat(Some(*self), path, 0)}
	}
	///Call fstatat(2) with flags=AT_SYMLINK_NOFOLLOW.
	pub unsafe fn lstatat(&self, path: &Path) -> Result<Stat, OsError>
	{
		unsafe{Path::fstatat(Some(*self), path, libc::AT_SYMLINK_NOFOLLOW)}
	}
	///Call fchmodat(2) with flags=0.
	pub unsafe fn chmodat(&self, path: &Path, mode: u32) -> Result<(), OsError>
	{
		unsafe{Path::fchmodat(Some(*self), path, mode, 0)}
	}
	///Call fchownat(2) with flags=0.
	pub unsafe fn chownat(&self, path: &Path, uid: Uid, gid: Gid) -> Result<(), OsError>
	{
		unsafe{Path::fchownat(Some(*self), path, uid, gid, 0)}
	}
	///Call fchownat(2) with flags=AT_SYMLINK_NOFOLLOW.
	pub unsafe fn lchownat(&self, path: &Path, uid: Uid, gid: Gid) -> Result<(), OsError>
	{
		unsafe{Path::fchownat(Some(*self), path, uid, gid, libc::AT_SYMLINK_NOFOLLOW)}
	}
	///Call symlinkat(2).
	pub unsafe fn symlinkat(&self, path: &Path, target: &Path) -> Result<(), OsError>
	{
		unsafe{Path::symlinkat(Some(*self), path, target)}
	}
	///Call readlinkat(2).
	pub unsafe fn readlinkat(&self, path: &Path) -> Result<DirectoryFilename, OsError>
	{
		unsafe{Path::readlinkat(Some(*self), path)}
	}
	///Call renameat(2)..
	pub unsafe fn renameat(&self, oldpath: &Path, newdir: FileDescriptorB, newpath: &Path) ->
		Result<(), OsError>
	{
		unsafe{Path::renameat(Some(*self), oldpath, Some(newdir), newpath)}
	}
	///Call read(2).
	pub unsafe fn read(&self, buf: &mut [u8]) -> Result<usize, OsError>
	{
		//Assumes self.0 is open => Precondition of function.
		//Assumes buf is at least buf.len() bytes of writable memory => True by definition.
		unsafe{
			let buflen = buf.len();
			let bufptr = buf.as_mut_ptr().cast();
			libc::read(self.0, bufptr, buflen).errno_val()
		}
	}
	///Call read(2). Buffer can be initially uninitialized.
	pub unsafe fn read_uninit2<'a>(&self, buf: &'a mut [MaybeUninit<u8>]) -> Result<&'a [u8], OsError>
	{
		let mut buf = BackedVector::new(buf);
		let amt = unsafe{
			let buflen = buf.remaining_capacity();
			let bufptr = buf.unfilled_pointer().cast();
			libc::read(self.0, bufptr, buflen).errno_val()?
		};
		unsafe{buf.assume_init(amt);}
		Ok(buf.into_inner())
	}
	///Call read(2), interpretting as inotify records.
	pub unsafe fn read_inotify<F>(&self, mut f: F) -> Result<(), OsError> where F: FnMut(InotifyEvent)
	{
		let mut buf = [MaybeUninit::uninit();8192];
		//Assumes self.0 is open => Precondition of function.
		let buf = unsafe{self.read_uninit2(&mut buf)}?;
		let mut ptr = 0;
		loop { match InotifyEvent::new(buf, &mut ptr) {
			Ok(Some(ev)) => f(ev),
			Ok(None) => break,
			Err(_) => return Err(OsError::EBADMSG)
		}}
		Ok(())
	}
	///Call read(2), interpretting as signalfd siginfo record.
	pub unsafe fn read_siginfo<F>(&self, mut f: F) -> Result<(), OsError> where F: FnMut(SignalEvent)
	{
		//Assumes read() initializes amount it indicates => true by definition.
		let item = unsafe {
			with_uninitialized_read(OsError::EBADMSG, |buf, buflen|{
				libc::read(self.0, buf.cast(), buflen).errno_val()
			})?
		};
		f(item);
		Ok(())
	}
	///Call write(2).
	pub unsafe fn write(&self, buf: &[u8]) -> Result<usize, OsError>
	{
		//Assumes self.0 is open => Precondition of function.
		//Assumes buf is at least buf.len() bytes of readable memory => True by definition.
		unsafe{
			let buflen = buf.len();
			let bufptr = buf.as_ptr().cast();
			libc::write(self.0, bufptr, buflen).errno_val()
		}
	}
	///Call readv(2).
	pub unsafe fn readv(&self, iov: &[&mut [u8]]) -> Result<usize, OsError>
	{
		//Assumes compatible_iovec is compatible function for slice to iovec.
		let (iov, iovlen, release) = unsafe {
			do_forward_copy_fn(iov, compatible_iovec, translate_mut_iovec)?
		};
		//Assumes self.0 is open => Precondition of function.
		//Assumes iov has at least iovlen elements => True by definition.
		//Assumes (iov,iovlen) are valid => release is live across call => True by definition.
		let r = unsafe{libc::readv(self.0, iov, iovlen as _)}.errno_val();
		drop(release);
		r
	}
	///Call writev(2).
	pub unsafe fn writev(&self, iov: &[&[u8]]) -> Result<usize, OsError>
	{
		//Assumes compatible_iovec is compatible function for slice to iovec.
		let (iov, iovlen, release) = unsafe {
			do_forward_copy_fn(iov, compatible_iovec, translate_const_iovec)?
		};
		//Assumes self.0 is open => Precondition of function.
		//Assumes iov has at least iovlen elements => True by definition.
		//Assumes (iov,iovlen) are valid => release is live across call => True by definition.
		let r = unsafe{libc::writev(self.0, iov, iovlen as _)}.errno_val();
		drop(release);
		r
	}
	///Call recv(2).
	pub unsafe fn recv(&self, buf: &mut [u8], flags: SendRecvFlags) -> Result<usize, OsError>
	{
		//Assumes self.0 is open => Precondition of function.
		//Assumes buf is at least buf.len() bytes of writable memory => True by definition.
		unsafe{libc::recv(self.0, buf.as_mut_ptr().cast(), buf.len(), flags.0)}.errno_val()
	}
	///Call send(2).
	pub unsafe fn send(&self, buf: &[u8], flags: SendRecvFlags) -> Result<usize, OsError>
	{
		//Assumes self.0 is open => Precondition of function.
		//Assumes buf is at least buf.len() bytes of readable memory => True by definition.
		unsafe{libc::send(self.0, buf.as_ptr().cast(), buf.len(), flags.0)}.errno_val()
	}
	///Call fsync(2).
	pub unsafe fn sync(&self) -> Result<(), OsError>
	{
		//Assumes self.0 is open => Precondition of function.
		let r = unsafe{libc::fsync(self.0)}.errno()?;
		Ok(r)
	}
	///Call recvfrom(2).
	pub unsafe fn recvfrom(&self, buf: &mut [u8], flags: SendRecvFlags) ->
		Result<(usize, Option<Address<'static>>), OsError>
	{
		let mut low = AddressStorage::new_full();
		let size = {
			let (sock, slen) = low.to_mut_sockaddr();
			//Assumes self.0 is open => Precondition of function.
			//Assumes buf is at least buf.len() bytes of writable memory => True by definition.
			//Assumes (sock,slen) are valid => low is live across the call => True by definition.
			unsafe {
				let buflen = buf.len();
				let bufptr = buf.as_mut_ptr().cast();
				libc::recvfrom(self.0, bufptr, buflen, flags.0, sock, slen).errno_val()?
			}
		};
		let addr = low.to_high();
		Ok((size, addr))
	}
	///Call sendto(2).
	pub unsafe fn sendto(&self, buf: &[u8], to: &Address, flags: SendRecvFlags) -> Result<usize, OsError>
	{
		let to = to.to_low()?;
		let (sock, slen) = to.to_const_sockaddr();
		//Assumes self.0 is open => Precondition of function.
		//Assumes buf is at least buf.len() bytes of readable memory => True by definition.
		//Assumes (sock,slen) are valid => to is live across the call => True by definition.
		unsafe{
			let buflen = buf.len();
			let bufptr = buf.as_ptr().cast();
			libc::sendto(self.0, bufptr, buflen, flags.0, sock, slen).errno_val()
		}
	}
	///Call sendmsg(2).
	pub unsafe fn sendmsg(&self, to: Option<&Address>, iov: &[&[u8]], control: Option<&CmsgSerializer>,
		flags: SendRecvFlags) -> Result<usize, OsError>
	{
		//All zeroes is valid msghdr.
		let mut msg: msghdr = unsafe{zeroed()};
		//Assumes compatible_iovec is compatible function for slice to iovec.
		let (iov, iovlen, release) = unsafe {
			do_forward_copy_fn(iov, compatible_iovec, translate_const_iovec)?
		};
		msg.msg_iov = iov;
		msg.msg_iovlen = iovlen as _;
		if let Some(control) = control {
			msg.msg_control = control.as_ptr();
			msg.msg_controllen = control.len() as _;
		}
		//Fill the structure here, as it has to be alive on call to sendmsg.
		let to_low = match to { Some(x) => Some(x.to_low()?), None => None };	//Convert to low.
		if let Some(to_low) = to_low.as_ref() {
			msg.msg_name = to_low.0.as_ptr().cast2();
			msg.msg_namelen = to_low.1 as _;
		}
		//Assumes self.0 is open => Precondition of function.
		//Assumes iov has at least iovlen elements => True by definition.
		//Assumes (iov,iovlen) are valid => release is live across call => True by definition.
		//Assumes (name,namelen) are valid => to_low is live across call => True by definition.
		//Assumes (control,controllen) are valid => control is live across call => True by definition.
		let r = unsafe{libc::sendmsg(self.0, msg.pointer_to(), flags.0)}.errno_val();
		drop(release);
		drop(to_low);
		r
	}
	///Call recvmsg(2).
	pub unsafe fn recvmsg(&self, get_addr: bool, iov: &[&mut [u8]], mut control: Option<&mut CmsgParser>,
		flags: SendRecvFlags) -> Result<(usize, Option<Address<'static>>), OsError>
	{
		//All zeroes is valid msghdr.
		let mut msg: msghdr = unsafe{zeroed()};
		//Assumes compatible_iovec is compatible function for slice to iovec.
		let (iov, iovlen, _release) = unsafe {
			do_forward_copy_fn(iov, compatible_iovec, translate_mut_iovec)?
		};
		msg.msg_iov = iov;
		msg.msg_iovlen = iovlen as _;
		if let Some(control) = control.as_mut() {
			msg.msg_control = control.as_mut_ptr();
			msg.msg_controllen = control.len() as _;
		}
		let r = if get_addr {
			//Fill temporary addres for msg_name/msg_namelen.
			let mut to_low = AddressStorage::new_full();
			msg.msg_name = to_low.0.as_ptr().cast2();
			msg.msg_namelen = to_low.1 as _;
			//Assumes self.0 is open => Precondition of function.
			//Assumes iov has at least iovlen elements => True by definition.
			//Assumes (iov,iovlen) are valid => release is live across call => True by definition.
			//Assumes (name,namelen) are valid => to_low is live across call => True by definition.
			//Assumes (control,controllen) are valid => control is live across call => True by
			//definition.
			let r = unsafe{libc::recvmsg(self.0, msg.pointer_to_mut(), flags.0)}.errno_val();
			r.map(|r|{
				to_low.1 = msg.msg_namelen as _;
				(r, to_low.to_high())
			})
		} else {
			//Assumes self.0 is open => Precondition of function.
			//Assumes iov has at least iovlen elements => True by definition.
			//Assumes (iov,iovlen) are valid => release is live across call => True by definition.
			//Assumes (control,controllen) are valid => control is live across call => True by
			//definition.
			let r = unsafe{libc::recvmsg(self.0, msg.pointer_to_mut(), flags.0)}.errno_val();
			//No address is available, return None.
			r.map(|r|(r, None))
		};
		if r.is_ok() {
			//Set length for control, so it can be iterated.
			if let Some(control) = control.as_mut() { control.set_len(msg.msg_controllen as usize); }
		}
		r
	}
	///Call fstat(2).
	pub unsafe fn stat(&self) -> Result<Stat, OsError>
	{
		//The fstat is guaranteed to initialize its buffer if it suceeds.
		//Assumes self.0 is open => Precondition of function.
		//with_uninitialized guarantees st is sizeo_of(stat) bytes of writable memory.
		let r = unsafe{with_uninitialized(|st|libc::fstat(self.0, st).errno())}?;
		Ok(Stat(r))
	}
	///Call lseek(2)
	pub unsafe fn seek(&self, position: SeekTarget) -> Result<u64, OsError>
	{
		let (offset, whence) = match position {
			SeekTarget::Set(x) => (cast_to_i64(x, false)?, libc::SEEK_SET),
			SeekTarget::Delta(x) => (x, libc::SEEK_CUR),
			SeekTarget::SetEnd(x) => (cast_to_i64(x, true)?, libc::SEEK_END),
		};
		//Assumes self.0 is open => Precondition of function.
		//Assumes that lseek is LFS on anything non-linux. It better be, or the OS is pure crap.
		unsafe{crate::sys::seek(self.0, offset, whence)}
	}
	///Call shutdown(2).
	pub unsafe fn shutdown(&self, how: ShutdownFlags) -> Result<(), OsError>
	{
		//Assumes self.0 is open => Precondition of function.
		unsafe{libc::shutdown(self.0, how.0)}.errno()
	}
	///Call `getsockopt(..., SOL_SOCKET, SO_ERROR, ...)`.
	///
	///# Notes:
	///
	///The outer error is from `getsockopt()` itself, the inner error is the connect error.
	pub unsafe fn so_error(&self) -> Result<Result<(), OsError>, OsError>
	{
		let mut code = 0i32;
		//Assumes self.0 is open => Precondition of function.
		//Assumes code is at least 4 bytes => True by definition.
		unsafe{__do_getsockopt(self.0, libc::SOL_SOCKET, libc::SO_ERROR, &mut code)}?;
		//This is bit special: This is getsockopt succeeding, but returning a connection failure.
		Ok(if code != 0 { Err(OsError::from_raw(code)) } else { Ok(()) })
	}
	///Call `getsockopt(..., SOL_SOCKET, SO_TYPE, ...)`
	pub unsafe fn so_type(&self) -> Result<SocketType, OsError>
	{
		let mut code = 0i32;
		//Assumes self.0 is open => Precondition of function.
		//Assumes code is at least 4 bytes => True by definition.
		unsafe{__do_getsockopt(self.0, libc::SOL_SOCKET, libc::SO_TYPE, &mut code)}?;
		Ok(SocketType(code))
	}
	///Set or clear O_CLOEXEC on the file descriptor.
	pub unsafe fn set_cloexec(&self, status: bool) -> Result<(), OsError>
	{
		use libc::fcntl;
		use libc::FD_CLOEXEC;
		//Assumes self.0 is open => Precondition of function.
		let mut flags = unsafe{fcntl(self.0, libc::F_GETFD)}.errno_sval()?;
		if status {
			if flags & FD_CLOEXEC != 0 { return Ok(()); }	//Already close_on_exec.
			flags |= FD_CLOEXEC;
		} else {
			if flags & FD_CLOEXEC == 0 { return Ok(()); }	//Already !close_on_exec.
			flags &= !FD_CLOEXEC;
		}
		//Assumes self.0 is open => Precondition of function.
		//Assumes flags is at least 4 bytes => True by definition.
		unsafe{fcntl(self.0, libc::F_SETFD, flags)}.errno()
	}
	///Set or clear O_NONBLOCK on the file descriptor.
	pub unsafe fn set_nonblock(&self, status: bool) -> Result<(), OsError>
	{
		use libc::fcntl;
		use libc::O_NONBLOCK;
		//Assumes self.0 is open => Precondition of function.
		let mut flags = unsafe{fcntl(self.0, libc::F_GETFL)}.errno_sval()?;
		if status {
			if flags & O_NONBLOCK != 0 { return Ok(()); }	//Already nonblocking.
			flags |= O_NONBLOCK;
		} else {
			if flags & O_NONBLOCK == 0 { return Ok(()); }	//Already blocking.
			flags &= !O_NONBLOCK;
		}
		//Assumes self.0 is open => Precondition of function.
		//Assumes flags is at least 4 bytes => True by definition.
		unsafe{fcntl(self.0, libc::F_SETFL, flags)}.errno()
	}
	///Call `setsockopt(..., SOL_SOCKET, SO_REUSEADDR, ...)`
	pub unsafe fn set_reuseaddr(&self, status: bool) -> Result<(), OsError>
	{
		let val: c_int = if status { 1 } else { 0 };
		//Assumes self.0 is open => Precondition of function.
		//Assumes val is at least size_of_val(val) bytes => True by definition.
		unsafe{__do_setsockopt(self.0, libc::SOL_SOCKET, libc::SO_REUSEADDR, val)}
	}
	///Call `setsockopt(..., IPPROTO_IPV6, IPV6_V6ONLY, ...)`
	pub unsafe fn set_v6only(&self, status: bool) -> Result<(), OsError>
	{
		let val: c_int = if status { 1 } else { 0 };
		//Assumes self.0 is open => Precondition of function.
		//Assumes val is at least size_of_val(val) bytes => True by definition.
		unsafe{__do_setsockopt(self.0, libc::IPPROTO_IPV6, libc::IPV6_V6ONLY, val)}
	}
	///Call `setsockopt(..., IPPROTO_TCP, TCP_NODELAY, ...)`
	pub unsafe fn set_tcp_nodelay(&self, status: bool) -> Result<(), OsError>
	{
		let val: c_int = if status { 1 } else { 0 };
		//Assumes self.0 is open => Precondition of function.
		//Assumes val is at least size_of_val(val) bytes => True by definition.
		unsafe{__do_setsockopt(self.0, libc::IPPROTO_TCP, libc::TCP_NODELAY, val)}
	}
	///Call getsockname(2).
	pub unsafe fn local_address(&self) -> Result<Option<Address<'static>>, OsError>
	{
		let mut low = AddressStorage::new_full();
		{
			let (sock, slen) = low.to_mut_sockaddr();
			//Assumes self.0 is open => Precondition of function.
			//Assumes (sock,slen) are valid => low is live across the call => True by definition.
			unsafe{libc::getsockname(self.0, sock, slen)}.errno()?
		};
		Ok(low.to_high())
	}
	///Call getpeername(2).
	pub unsafe fn remote_address(&self) -> Result<Option<Address<'static>>, OsError>
	{
		let mut low = AddressStorage::new_full();
		{
			let (sock, slen) = low.to_mut_sockaddr();
			//Assumes self.0 is open => Precondition of function.
			//Assumes (sock,slen) are valid => low is live across the call => True by definition.
			unsafe{libc::getpeername(self.0, sock, slen)}.errno()?
		};
		Ok(low.to_high())
	}
	///Call accept(2).
	pub unsafe fn accept(&self) -> Result<(FileDescriptor, Option<Address<'static>>), OsError>
	{
		unsafe{self.accept2(Default::default())}
	}
	///Call accept(2).
	pub unsafe fn accept2(&self, flags: SocketFlags) -> Result<(FileDescriptor, Option<Address<'static>>), OsError>
	{
		let mut low = AddressStorage::new_full();
		let fd = {
			let (sock, slen) = low.to_mut_sockaddr();
			//Assumes self.0 is open => Precondition of function.
			//Assumes (sock,slen) are valid => low is live across the call => True by definition.
			if_linux!({
				unsafe{libc::accept4(self.0, sock, slen, flags.0)}
			}, {
				unsafe{libc::accept(self.0, sock, slen)}
			})
		};
		let fd = FileDescriptor(FileDescriptorB(fd.errno_sval()?));
		let addr = low.to_high();
		Ok((fd, addr))
	}
	///Call bind(2).
	pub unsafe fn bind(&self, to: &Address) -> Result<(), OsError>
	{
		let to = to.to_low()?;
		let (sock, slen) = to.to_const_sockaddr();
		//Assumes self.0 is open => Precondition of function.
		//Assumes (sock,slen) are valid => to is live across the call => True by definition.
		unsafe{libc::bind(self.0, sock, slen)}.errno()
	}
	///Call connect(2).
	///
	///# Notes:
	///
	///Instead of returning Err(EINPROGRESS), this returns Ok(false). Ok(true) means synchronously completed
	///connection.
	pub unsafe fn connect(&self, to: &Address) -> Result<bool, OsError>
	{
		let to = to.to_low()?;
		let (sock, slen) = to.to_const_sockaddr();
		//Assumes self.0 is open => Precondition of function.
		//Assumes (sock,slen) are valid => to is live across the call => True by definition.
		let r = unsafe{libc::connect(self.0, sock, slen)}.errno();
		if r == Err(OsError::EINPROGRESS) { return Ok(false); }		//In progress.
		r?;	//Return other errors.
		Ok(true)	//Connected.
	}
	///Call listen(2).
	pub unsafe fn listen(&self, backlog: u32) -> Result<(), OsError>
	{
		let backlog = min(backlog, 0x7FFFFFFF) as i32;
		//Assumes self.0 is open => Precondition of function.
		unsafe{libc::listen(self.0, backlog)}.errno()
	}
	///Call isatty(2).
	pub unsafe fn isatty(&self) -> bool { unsafe{libc::isatty(self.0) != 0} }
	///Call `setsockopt(..., SOL_SOCKET, SO_PASSCRED, ...)`.
	///
	///Only supported on Linux, otherwise returns EOPNOTSUPP.
	pub unsafe fn set_passcred(&self, status: bool) -> Result<(), OsError>
	{
		unsafe{crate::sys::set_passcred(self.as_raw_fd(), status)}
	}
	///Call epoll_ctl(2).
	///
	///Only supported on Linux, otherwise returns ENOSYS.
	pub unsafe fn epoll_ctl(&self, fd: FileDescriptorB, cmd: EpollCtlCmd) -> Result<(), OsError>
	{
		unsafe{crate::sys::epoll_ctl(self.as_raw_fd(), fd.as_raw_fd(), cmd)}
	}
	///Call epoll_wait(2).
	///
	///Only supported on Linux, otherwise returns ENOSYS.
	pub unsafe fn epoll_wait(&self, events: &mut [EpollEvent], timeout: i32) -> Result<u32, OsError>
	{
		unsafe{crate::sys::epoll_wait(self.as_raw_fd(), events, timeout)}
	}
	///Call `fsetfacl` on the specified fd.
	///
	///Only supported on Linux, otherwise returns ENOSYS.
	pub unsafe fn setfacl(&self, acl: &impl AclWrite) -> Result<(), OsError>
	{
		unsafe{crate::sys::fsetfacl(self.as_raw_fd(), acl)}
	}
	///Call `fchmod(2)`.
	pub unsafe fn chmod(&self, mode: u32) -> Result<(), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{libc::fchmod(self.0, mode as _)}.errno()
	}
	///Call `fchown(2)`.
	pub unsafe fn chown(&self, uid: Uid, gid: Gid) -> Result<(), OsError>
	{
		//Assumes that self.0 is open => True by definition.
		unsafe{libc::fchown(self.0, uid.to_inner(), gid.to_inner())}.errno()
	}
	///Call `mmap(2)`.
	pub unsafe fn mmap(&self, addr: *mut u8, size: usize, prot: MmapProtection, flags: MmapFlags,
		offset: u64) -> Result<*mut u8, OsError>
	{
		let ret = unsafe {
			libc::mmap(addr.cast(), size, prot.to_inner(), flags.to_inner(), self.0, offset as _)
		};
		if ret == libc::MAP_FAILED { return Err(OsError::new()); }
		Ok(ret.cast())
	}
	///Call `fgetxattr(2)`.
	pub unsafe fn getxattr(&self, name: &Path) -> Result<OwnedByteString, OsError>
	{
		unsafe{crate::sys::fgetxattr(self.as_raw_fd(), name)}
	}
	///Call `fsetxattr(2)`.
	pub unsafe fn setxattr(&self, name: &Path, value: &[u8], flags: SetxattrFlags) -> Result<(), OsError>
	{
		unsafe{crate::sys::fsetxattr(self.as_raw_fd(), name, value, flags)}
	}
	///Call `ftruncate(2)`.
	pub unsafe fn truncate(&self, size: u64) -> Result<(), OsError>
	{
		let size = size.try_into().set_err(OsError::EFBIG)?;
		unsafe{libc::ftruncate(self.0, size)}.errno()
	}
}

define_bitfield_type!([doc=r#"Event flags for `poll(2)` or `ppoll(2)`."#];PollFlags i16);
impl PollFlags
{
	define_symbol!([doc=r#"File is in error state."#];POLLERR: PollFlags as ERR);
	define_symbol!([doc=r#"Pipe or socket hung up."#];POLLHUP: PollFlags as HUP);
	define_symbol!([doc=r#"Data available for read."#];POLLIN: PollFlags as IN);
	define_symbol!([doc=r#"File is available for write."#];POLLOUT: PollFlags as OUT);
	///Is in error state?
	pub fn is_err(&self) -> bool { self.0 & libc::POLLERR != 0 }
	///Is invalid?
	pub fn is_invalid(&self) -> bool { self.0 & libc::POLLNVAL != 0 }
	///Is hung up?
	pub fn is_hup(&self) -> bool { self.0 & libc::POLLHUP != 0 }
	///Is data available for read?
	pub fn is_in(&self) -> bool { self.0 & libc::POLLIN != 0 }
	///Is data available for read or hung up?
	pub fn is_in_or_hup(&self) -> bool { self.0 & (libc::POLLIN|libc::POLLHUP) != 0 }
	///Is available for output?
	pub fn is_out(&self) -> bool { self.0 & libc::POLLOUT != 0 }
	///Is no event?
	pub fn is_none(&self) -> bool { self.0 == 0 }
}

///File descriptor to poll.
#[repr(C)]
#[derive(Copy,Clone,Debug)]
pub struct PollFd
{
	///The file descriptor.
	pub fd: FileDescriptorB,
	///The event flags.
	pub events: PollFlags,
	///Event flags returned.
	pub revents: PollFlags,
}

impl PollFd
{
	///Call poll(2).
	pub unsafe fn poll(fds: &mut [PollFd], timeout: i32) -> Result<u32, OsError>
	{
		let timeout = timeout as c_int;
		//Assumes PollFd properly implements TypeAdapter => True by definition.
		let (fds2, nfds, release) = unsafe{do_forward_copy(fds)}?;
		//Assumes fds2 are open => Precondition of function.
		//Assumes fds is at least size_of(nfds) elements => True by definition of do_forward_copy.
		//Assumes (fds2, nfds) is valid => release is live across call => true by definition.
		let ret = unsafe{libc::poll(fds2, nfds as _, timeout)}.errno_val()?;
		//Assumes fds2 is from do_forward_copy() with the same events => True by above.
		//Assumes fds2 is valid => release is live across call => true by definition.
		unsafe{do_backward_copy(fds, fds2);}
		drop(release);
		Ok(ret)
	}
	///Call ppoll(2).
	pub unsafe fn ppoll(fds: &mut [PollFd], timeout: i32, sigmask: Option<SignalSet>) ->
		Result<u32, OsError>
	{
		let mut ntimeout: libc::timespec = unsafe{zeroed()};
		let timeout = if timeout >= 0 {
			ntimeout.tv_sec = (timeout / 1000) as _;
			ntimeout.tv_nsec = (timeout % 1000 * 1000000) as _;
			ntimeout.pointer_to()
		} else {
			null()
		};
		let sigmask = if let Some(sigmask) = sigmask.as_ref() { sigmask.as_raw() } else { null() };
		//Assumes PollFd properly implements TypeAdapter => True by definition.
		let (fds2, nfds, release) = unsafe{do_forward_copy(fds)}?;
		//Assumes fds2 are open => Precondition of function.
		//Assumes fds is at least size_of(nfds) elements => True by definition of do_forward_copy.
		//Assumes (fds2, nfds) is valid => release is live across call => true by definition.
		//Assumes sigmask points to size_of(sigmask_t) bytes => True by definition.
		let ret = unsafe{libc::ppoll(fds2, nfds as _, timeout, sigmask)}.errno_val()?;
		//Assumes fds2 is from do_forward_copy() with the same events => True by above.
		//Assumes fds2 is valid => release is live across call => true by definition.
		unsafe{do_backward_copy(fds, fds2);}
		drop(release);
		Ok(ret)
	}
}

define_bitfield_type!([doc=r#"Flags for `getrandom(2)`."#];GetRandomFlags u32);

///Call getrandom(2).
///
///Only supported on Linux, otherwise returns ENOSYS.
pub fn getrandom(buffer: &mut [u8], flags: GetRandomFlags) -> Result<usize, OsError>
{
	crate::sys::getrandom2(buffer, flags.0)
}

///Call `mmap(2)` on anonymous file.
///
///Offset is always 0, MAP_ANONYMOUS is always added.
pub unsafe fn mmap(addr: *mut u8, size: usize, prot: MmapProtection, flags: MmapFlags) -> Result<*mut u8, OsError>
{
	let flags = flags.to_inner() | libc::MAP_ANONYMOUS;
	let ret = unsafe{libc::mmap(addr.cast(), size, prot.to_inner(), flags, -1, 0)};
	if ret == libc::MAP_FAILED { return Err(OsError::new()); }
	Ok(ret.cast())
}

///Call `munmap(2)`.
pub unsafe fn munmap(addr: *mut u8, size: usize) -> Result<(), OsError>
{
	unsafe{libc::munmap(addr.cast(), size)}.errno()
}

#[test]
fn test_syscall_bad()
{
	let r = unsafe{(libc::syscall(12345, 0) as libc::ssize_t).errno_val()};
	assert_eq!(r, Err(OsError::ENOSYS));
}

///Ensure that file descriptors 0-2 (STDIN/STDOUT/STDERR) are open.
///
///Returns estimate for number of open files.
pub fn file_descriptors_ok() -> u32
{
	unsafe {
		let fname = b"/dev/null\0";
		let mut fd = -1;
		while fd < 3 {
			//Assumes fname is NUL-terminated by array => True by definition.
			//This should be run before there is threads, and fds 0-2 should definitely be inherited.
			fd = libc::open(fname.as_ptr().cast(), libc::O_RDWR);
			//Uncondtionally safe.
			if fd < 0 { libc::_exit(57); }	//BAD: Can not open /dev/null.
		}
		//Assumes fd is open and can be closed => True by above.
		libc::close(fd);	//Close the now extraneous file.
		fd as u32		//Estimate of files open.
	}
}

pub(crate) struct CallFree(*mut c_void);

impl Drop for CallFree
{
	//This owns self.0, so it can be freed on drop.
	//Assumes self.0 is valid allocation => Invariant of structure.
	fn drop(&mut self) { unsafe{libc::free(self.0)}; }
}


pub(crate) trait TypeAdapter
{
	type Internal;
	fn compatible() -> bool;
	fn forward(t: &mut <Self as TypeAdapter>::Internal, s: &Self);
	fn backward(t: &mut Self, s: &<Self as TypeAdapter>::Internal);
}

unsafe fn do_forward_copy_fn<Target,Source>(input: &[Source], direct: fn() -> bool,
	translate: fn(&mut Target, &Source)) -> Result<(*mut Target, usize, Option<CallFree>), OsError>
{
	if direct() {
		//In direct operation, just transmute the buffer.
		//Assumes Source and Target have the same layout => Definition of direct() = true.
		Ok((input.as_ptr().cast2(), input.len(), None))
	} else {
		//In indirect operation, allocate a buffer to hold the results.
		let input2: *mut Target = unsafe{libc::calloc(input.len(), size_of::<Target>()).cast()};
		fail_if!(input2.is_null(), OsError::ENOMEM);
		//Assumes input2 is valid allocation => True by definition.
		let dropper = CallFree(input2.cast());
		//Assumes input2 is at least input.len() Targets => True by definition.
		let input2 = unsafe{from_raw_parts_mut(input2, input.len())};
		for (t,s) in input2.iter_mut().zip(input.iter()) { translate(t, s); }
		Ok((input2.as_mut_ptr(), input.len(), Some(dropper)))
	}
}

pub(crate) unsafe fn do_forward_copy<A:TypeAdapter>(input: &[A]) ->
	Result<(*mut <A as TypeAdapter>::Internal, usize, Option<CallFree>), OsError>
{
	//Assumes A::compatible() is valid direct function => True by definition.
	unsafe{do_forward_copy_fn(input, A::compatible, A::forward)}
}

pub(crate) unsafe fn do_backward_copy<A:TypeAdapter>(input: &mut [A], results: *mut <A as TypeAdapter>::Internal)
{
	if A::compatible() {
		//In direct mode, there is nothing to do.
	} else {
		//In indirect mode, copy back the results.
		//Assumes results is at least input.len() Targets => Precondition of function.
		let results = unsafe{from_raw_parts(results, input.len())};
		for (t, s) in input.iter_mut().zip(results.iter()) { A::backward(t, s); }
	}
}

impl TypeAdapter for PollFd
{
	type Internal = libc::pollfd;
	fn compatible() -> bool
	{
		//This should be optimized into true.
		//Assumes that PollFd and pollfd can be all zeroes.
		let a: pollfd = unsafe{zeroed()};
		let b: PollFd = unsafe{zeroed()};
		compatible_fields(&a, &b, &a.fd, &b.fd) && compatible_fields(&a, &b, &a.events, &b.events) &&
			compatible_fields(&a, &b, &a.revents, &b.revents)
	}
	fn forward(t: &mut <Self as TypeAdapter>::Internal, s: &PollFd)
	{
		t.fd = s.fd.as_raw_fd();
		t.events = s.events.0;
		t.revents = s.revents.0;
	}
	fn backward(t: &mut PollFd, s: &<Self as TypeAdapter>::Internal)
	{
		t.revents = PollFlags(s.revents);
	}
}

fn compatible_iovec() -> bool
{
	let u = size_of::<usize>();
	//Assumes that iovec can be all zeroes.
	let tmp: iovec = unsafe{zeroed()};
	if size_of::<iovec>() != 2 * u { return false; }
	if size_of::<&[u8]>() != 2 * u { return false; }
	if offof(&tmp, &tmp.iov_base) != 0 { return false; }
	if offof(&tmp, &tmp.iov_len) != u { return false; }
	return true;
}

#[doc(hidden)]
pub fn __dummy_iovec_compatible() -> bool { compatible_iovec() && PollFd::compatible() }

fn translate_const_iovec(t: &mut libc::iovec, s: &&[u8])
{
	t.iov_base = s.as_ptr().cast2();
	t.iov_len = s.len() as _;
}

fn translate_mut_iovec(t: &mut libc::iovec, s: &&mut [u8])
{
	t.iov_base = s.as_ptr().cast2();
	t.iov_len = s.len() as _;
}

fn align_cmsg(control: &mut [MaybeUninit<u8>]) -> &mut [MaybeUninit<u8>]
{
	let mut offset = 0;
	while offset < control.len() && (control.as_ptr() as usize + offset) % CMSG_ALIGN != 0 {
		offset += 1;
	}
	&mut control[offset..]
}

fn cast_to_i64(mut value: u64, negative: bool) -> Result<i64, OsError>
{
	if negative {
		if value > 9223372036854775808 { return Err(OsError::EINVAL); }
		value = (!value).wrapping_add(1);	//2s complement.
	} else {
		if value > 9223372036854775807 { return Err(OsError::EINVAL); }
	}
	Ok(value as i64)
}

#[cfg(test)]
fn test_fd_flags(fd: i32, nonblock: bool, cloexec: bool)
{
	let f1 = unsafe{libc::fcntl(fd, libc::F_GETFL)}.errno_sval().expect("F_GETFL failed");
	let f2 = unsafe{libc::fcntl(fd, libc::F_GETFD)}.errno_sval().expect("F_GETFD failed");
	let f1e = libc::O_NONBLOCK * nonblock as i32;
	let f2e = libc::FD_CLOEXEC * cloexec as i32;
	assert!(f1 & libc::O_NONBLOCK == f1e);
	assert!(f2 & libc::FD_CLOEXEC == f2e);
}

#[test]
fn socket_set_nonblock()
{
	let flags = SocketFlags::NONBLOCK;
	let fd = FileDescriptor::socket2(SocketFamily::UNIX, SocketType::STREAM, Default::default(), flags).
		expect("Failed to create socket");
	test_fd_flags(fd.as_raw_fd(), true, false);
}

#[test]
fn socket_set_cloexec()
{
	let flags = SocketFlags::CLOEXEC;
	let fd = FileDescriptor::socket2(SocketFamily::UNIX, SocketType::STREAM, Default::default(), flags).
		expect("Failed to create socket");
	test_fd_flags(fd.as_raw_fd(), false, true);
}

#[test]
fn pipe_set_nonblock()
{
	let flags = PipeFlags::NONBLOCK;
	let (f1, f2) = FileDescriptor::pipe2(flags).expect("Failed to create pipe");
	test_fd_flags(f1.as_raw_fd(), true, false);
	test_fd_flags(f2.as_raw_fd(), true, false);
}

#[test]
fn pipe_set_cloexec()
{
	let flags = PipeFlags::CLOEXEC;
	let (f1, f2) = FileDescriptor::pipe2(flags).expect("Failed to create pipe");
	test_fd_flags(f1.as_raw_fd(), false, true);
	test_fd_flags(f2.as_raw_fd(), false, true);
}
