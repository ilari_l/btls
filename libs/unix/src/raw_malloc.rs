//use btls_aux_fail::fail_if_none;
use btls_aux_memory::NonNull;
use btls_aux_memory::NonNullMut;
use core::mem::forget;
use core::mem::size_of;

pub(crate) struct RawMalloc<T>
{
	base: NonNullMut<T>,
	size: usize,
}

impl<T> Drop for RawMalloc<T>
{
	fn drop(&mut self)
	{
		//The pointer is valid only if this is nontrivial.
		if self.is_nontrivial() { unsafe{libc::free(self.base.to_raw().cast())}; }
	}
}

impl<T> RawMalloc<T>
{
	pub(crate) fn new(size: usize) -> Option<RawMalloc<T>>
	{
		let need = size.checked_mul(size_of::<T>())?;
		if need == 0 {
			//This allocation is always trivial.
			return Some(RawMalloc{
				base: NonNullMut::dangling(),
				size: size,
			})
		}
		let base = unsafe{libc::malloc(need)};
		let base = NonNullMut::new(base.cast::<T>())?;
		Some(RawMalloc{base, size})
	}
	pub(crate) fn __blank() -> RawMalloc<T>
	{
		//This allocation is always trivial.
		RawMalloc {
			base: NonNullMut::dangling(),
			size: 0,
		}
	}
	pub(crate) fn resize(&mut self, newsize: usize) -> Option<()>
	{
		//Special empty case, or no change.
		if size_of::<T>() == 0 || self.size == newsize {
			self.size = newsize;
			return Some(());
		}
		let need = newsize.checked_mul(size_of::<T>())?;
		let base = match (self.size > 0, newsize > 0) {
			(false, false) => NonNullMut::dangling(),
			(false, true) => {
				//Just malloc new buffer.
				let base = unsafe{libc::malloc(need)};
				NonNullMut::new(base.cast::<T>())?
			},
			(true, false) => {
				//Just free the buffer.
				unsafe{libc::free(self.base.to_raw().cast())};
				NonNullMut::dangling()
			},
			(true, true) => {
				let base = unsafe{libc::realloc(self.base.to_raw().cast(), need)};
				//This is pretty special if it fails: If failure occurs when downsizing, ignore
				//it, using the old buffer (we are just holding some extra memory). But if failure
				//occurs in upsizing, return ENOMEM.
				if !base.is_null() {
					NonNullMut::new(base.cast::<T>())?
				} else if newsize <= self.size {
					self.base
				} else {
					return None;
				}
			},
		};
		self.base = base;
		self.size = newsize;
		Some(())
	}
	pub(crate) fn is_nontrivial(&self) -> bool
	{
		//Nontrivial iff size and element size is positive.
		self.size > 0 && size_of::<T>() > 0
	}
	pub(crate) fn get_base_mut(&mut self) -> NonNullMut<T> { self.base }
	pub(crate) fn get_base(&self) -> NonNull<T> { self.base.to_const() }
	pub(crate) fn get_size(&self) -> usize { self.size }
	pub(crate) fn leak(self) -> NonNullMut<T>
	{
		let ret = self.base;
		forget(self);
		ret
	}
}
