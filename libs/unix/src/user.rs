use super::CString;
use super::OsError;
use super::RawMalloc;
use super::ReturnValue;
use btls_aux_fail::f_return;
use btls_aux_fail::fail_if_none;
use btls_aux_memory::asciiz_pointer;
use btls_aux_memory::NonNull;
use btls_aux_memory::NonNullMut;
use btls_aux_memory::ObjectExt;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::marker::PhantomData;
use core::mem::transmute;
use core::mem::zeroed;
use core::str::from_utf8;
use core::str::from_utf8_unchecked;
use core::str::FromStr;
use libc::c_char;
use libc::c_int;
use libc::gid_t;
use libc::uid_t;

///Raw GID type.
pub type RawGidT = gid_t;
///Raw UID type.
pub type RawUidT = uid_t;

///A Group ID.
#[derive(Copy,Clone,Hash,Debug,PartialOrd,Ord,PartialEq,Eq)]
pub struct Gid(RawGidT);

impl Gid
{
	///Create new object describing GID 0.
	pub fn root() -> Gid { Gid(0) }
	///Create a new object describing GID `g`.
	pub(crate) fn new(g: RawGidT) -> Gid { Gid(g) }
	///Create a new object describing Group/GID 'g'.
	pub fn from_str(g: &str) -> Option<Gid>
	{
		//If g starts with #, interpret as numeric only.
		let g = if g.starts_with("#") {
			RawGidT::from_str(&g[1..]).ok()
		} else {
			Self::__new_str_dbonly(g).or_else(||RawGidT::from_str(g).ok())
		}?;
		g.checked_add(1)?;	//The largest possible gid is not valid!
		return Some(Gid(g))
	}
	fn __new_str_dbonly(g: &str) -> Option<RawGidT>
	{
		let name = CString::new_c_string(g.as_bytes()).ok()?;
		unsafe {
			//Need to hold lock to serialize access to getgrnam().
			let _lock = crate::DlfcnLock::new();
			//Asumes name is valid NUL-terminated string => True by definition.
			let entry = libc::getgrnam(name.as_ptr());
			fail_if_none!(entry.is_null());		//Not found.
			//Assumes entry is valid and not NULL => True by check above and definition of getgrnam.
			Some((*entry).gr_gid)
		}
	}
	//Assume get{,e}gid() can not fail.
	///Call getgid(2).
	pub fn get_real() -> Gid
	{
		//Unconditionally safe.
		unsafe{Gid(libc::getgid())}
	}
	///Call getegid(2).
	pub fn get_effective() -> Gid
	{
		//Unconditionally safe.
		unsafe{Gid(libc::getegid())}
	}
	///Call setgid(2).
	pub fn change(self) -> Result<(), OsError>
	{ 
		//Unconditionally safe.
		unsafe{libc::setgid(self.0).errno()}
	}
	///Call setregid(2).
	pub fn change_re(real: Gid, effective: Gid) -> Result<(), OsError>
	{ 
		//Unconditionally safe.
		unsafe{setregid(real.0, effective.0).errno()}
	}
	///Call setgroups(2).
	pub fn change_extra(array: &[Gid]) -> Result<(), OsError>
	{ 
		//The layout of Gid and RawGidT is compatible. Assumes array is of array.len() elements => true by
		//definition.
		unsafe{libc::setgroups(array.len() as _, transmute(array.as_ptr())).errno()}
	}
	///Call getgrnam(3).
	pub fn record_by_name(name: &[u8]) -> Option<GroupRecord>
	{
		let name = CString::new_c_string(name).ok()?;
		unsafe {
			//Need to hold lock to serialize access to getgrnam().
			let _lock = crate::DlfcnLock::new();
			//Asumes name is valid NUL-terminated string => True by definition.
			let entry = libc::getgrnam(name.as_ptr());
			fail_if_none!(entry.is_null());		//Not found.
			//Assumes entry is valid and not NULL => True by check above and definition of getgrnam.
			Some(GroupRecord::new(&*entry))
		}
	}
	///Get the raw GID.
	pub fn to_inner(self) -> RawGidT { self.0 }
}

impl Display for Gid
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> { Display::fmt(&self.0, f) }
}

///Records about a group.
#[derive(Copy,Clone)]
pub struct GroupRecord(Gid);

impl GroupRecord
{
	fn new(g: &libc::group) -> GroupRecord
	{
		GroupRecord(Gid(g.gr_gid))
	}
	///Get the GID of the group.
	pub fn gid(&self) -> Gid { self.0 }
}

define_enum_type!([doc=r#"The limit to use for `getrlimit(2)` and `setrlimit(2)`."#];Rlimit i32);
impl Rlimit
{
	//RLIMIT_NOFILE seems to be i32 or u32 depending on platform.
	define_symbol!([doc=r#"Maximum number of file descriptors."#];
		NOFILE: Rlimit VAL ::libc::RLIMIT_NOFILE as i32);
	///Call `getrlimit(2)`. Of return values, first is soft, second is hard.
	pub fn get(self) -> Result<(u64, u64), OsError>
	{
		//All zeroes is valid rlimit.
		let mut rlim: libc::rlimit = unsafe{zeroed()};
		//Assumes rlim is valid rlimit => true by definition.
		unsafe{libc::getrlimit(self.0 as _, rlim.pointer_to_mut())}.errno()?;
		Ok((rlim.rlim_cur as u64, rlim.rlim_max as u64))
	}
}

//FreeBSD has these, but not defined in libc.
extern "C" {
	fn setregid(real: gid_t, effective: gid_t) -> c_int;
	fn setreuid(real: uid_t, effective: uid_t) -> c_int;
}

///User ID.
#[derive(Copy,Clone,Hash,Debug,PartialOrd,Ord,PartialEq,Eq)]
pub struct Uid(RawUidT);

impl Uid
{
	///Get object describing the root user (UID0).
	pub fn root() -> Uid { Uid(0) }
	///Get object describing UID `u`.
	pub(crate) fn new(u: RawUidT) -> Uid { Uid(u) }
	///Create a new object describing User/UID 'u'.
	pub fn from_str(u: &str) -> Option<Uid>
	{
		//If u starts with #, interpret as numeric only.
		let u = if u.starts_with("#") {
			RawUidT::from_str(&u[1..]).ok()
		} else {
			Self::__new_str_dbonly(u).or_else(||RawUidT::from_str(u).ok())
		}?;
		u.checked_add(1)?;	//The largest possible uid is not valid!
		return Some(Uid(u))
	}
	fn __new_str_dbonly(u: &str) -> Option<RawUidT>
	{
		let name = CString::new_c_string(u.as_bytes()).ok()?;
		unsafe {
			//Need to hold lock to serialize access to getgrnam().
			let _lock = crate::DlfcnLock::new();
			//Asumes name is valid NUL-terminated string => True by definition.
			let entry = libc::getpwnam(name.as_ptr());
			fail_if_none!(entry.is_null());		//Not found.
			//Assumes entry is valid and not NULL => True by check above and definition of getpwnam.
			Some((*entry).pw_uid)
		}
	}
	//Assume get{,e}uid() can not fail.
	///Call `getuid(2)`.
	pub fn get_real() -> Uid
	{
		//Unconditionally safe.
		unsafe{Uid(libc::getuid())}
	}
	///Call `geteuid(2)`.
	pub fn get_effective() -> Uid
	{
		//Unconditionally safe.
		unsafe{Uid(libc::geteuid())}
	}
	///Does the specified UID describe the superuser (UID0)?
	pub fn is_super(&self) -> bool { self.0 == 0 }
	///Call `setuid(2)`.
	pub fn change(self) -> Result<(), OsError>
	{
		//Unconditionally safe.
		unsafe{libc::setuid(self.0).errno()}
	}
	///Call `setreuid(2)`.
	pub fn change_re(real: Uid, effective: Uid) -> Result<(), OsError>
	{ 
		//Unconditionally safe.
		unsafe{setreuid(real.0, effective.0).errno()}
	}
	///Call getpwuid(3).
	pub fn record(self) -> Option<UserRecord>
	{
		unsafe {
			//Need to hold lock to serialize access to getpwnam().
			let _lock = crate::DlfcnLock::new();
			//Unconditionally safe.
			let entry = libc::getpwuid(self.0);
			fail_if_none!(entry.is_null());		//Not found.
			//Assumes entry is valid and not NULL => True by check above and definition of getpwuid.
			Self::__to_record(&*entry)
		}
	}
	///Call getpwnam(3).
	pub fn record_by_name(name: &[u8]) -> Option<UserRecord>
	{
		let name = CString::new_c_string(name).ok()?;
		unsafe {
			//Need to hold lock to serialize access to getpwnam().
			let _lock = crate::DlfcnLock::new();
			//Asumes name is valid NUL-terminated string => True by definition.
			let entry = libc::getpwnam(name.as_ptr());
			fail_if_none!(entry.is_null());		//Not found.
			//Assumes entry is valid and not NULL => True by check above and definition of getpwnam.
			Self::__to_record(&*entry)
		}
	}
	//Assumes entry is valid or NULL.
	unsafe fn __to_record(entry: &libc::passwd) -> Option<UserRecord>
	{
		let uid = Uid(entry.pw_uid);
		let gid = Gid(entry.pw_gid);
		//Assumes entry->pw_name is valid NULL-terminated string.
		let extra_gids = unsafe{Self::__grouplist(entry.pw_name, gid)}?;
		//Assumes entry->pw_dir is NUL-terminated => true by definition.
		let homedir = unsafe{strdup(entry.pw_dir)}?;
		//Assumes entry->pw_nam is NUL-terminated => true by definition.
		let name = unsafe{strdup(entry.pw_name)}?;

		Some(UserRecord{uid, gid, homedir, extra_gids, name})
	}
	unsafe fn __grouplist(name: *const c_char, primary: Gid) -> Option<MallocBuffer<Gid>>
	{
		let mut space = 32usize as c_int;
		let mut buffer = unsafe{MallocBuffer::new(space as usize)}?;
		loop {
			//Assumes buffer is good for space entries => true by invariant.
			let obuf = buffer.as_mut_ptr::<RawGidT>();
			let r = unsafe{libc::getgrouplist(name, primary.0, obuf.to_raw(), space.pointer_to_mut())};
			if r >= 0 && (r as usize) <= buffer.0.get_size() {
				//Successful, remove primary and return the list.
				let reduce = {
					//Assumes: r is smaller than capacity => true by above check.
					unsafe{buffer.set_length(r as usize);}
					let buffer2 = unsafe{buffer.as_mut_slice()};
					let mut pindex = None;
					for (idx, gid) in buffer2.iter().enumerate() {
						if *gid == primary { pindex = Some(idx); }
					}
					if let Some(pindex) = pindex {
						//Swap pindex and last. pindex is smaller than buffer length by
						//construction of loop above.
						let last = buffer2.len() - 1;
						let tmp = buffer2[pindex];
						buffer2[pindex] = buffer2[last];
						buffer2[last] = tmp;
						true
					} else {
						false
					}
				};
				//If reduce is set, remove the last element.
				if reduce { buffer.1 = buffer.1.saturating_sub(1); }
				return Some(buffer);
			} else {
				//Resize buffer for next iteration. This frees the old one via drop.
				buffer = unsafe{MallocBuffer::new(space as usize)}?;
			}
		}
	}
	///Get the raw UID.
	pub fn to_inner(self) -> RawUidT { self.0 }
}

unsafe fn strdup(s: *const c_char) -> Option<MallocBuffer<u8>>
{
	let s = f_return!(NonNull::new(s), MallocBuffer::from_slice(b"(null)"));
	unsafe{MallocBuffer::from_slice(asciiz_pointer(s))}
}

impl Display for Uid
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> { Display::fmt(&self.0, f) }
}

struct MallocBuffer<T:Sized+Copy>(RawMalloc<T>, usize, PhantomData<T>);

impl<T:Sized+Copy> MallocBuffer<T>
{
	unsafe fn as_mut_slice(&mut self) -> &mut [T]
	{
		unsafe{self.0.get_base_mut().make_slice_mut(self.1)}
	}
	//Assumes self has been fully initialized.
	unsafe fn as_slice(&self) -> &[T]
	{
		unsafe{self.0.get_base().make_slice(self.1)}
	}
	//Assumes len is valid length.
	unsafe fn set_length(&mut self, len: usize) { if len <= self.0.get_size() { self.1 = len; } }
	//Does not initialize the buffer.
	unsafe fn new(len: usize) -> Option<MallocBuffer<T>>
	{
		let mem = RawMalloc::<T>::new(len)?;
		Some(MallocBuffer(mem, 0, PhantomData))
	}
	fn from_slice(slice: &[T]) -> Option<MallocBuffer<T>>
	{
		unsafe {
			let mut buf = Self::new(slice.len())?;
			//Assume the above gives slice.len() elements. And mark the elements as initialized.
			if buf.0.is_nontrivial() { buf.0.get_base_mut().write_slice(slice); }
			buf.1 = slice.len();
			Some(buf)
		}
	}
	fn as_mut_ptr<U:Sized>(&mut self) -> NonNullMut<U> { self.0.get_base_mut().cast() }
}

///Record about user.
pub struct UserRecord
{
	uid: Uid,
	gid: Gid,
	homedir: MallocBuffer<u8>,
	extra_gids: MallocBuffer<Gid>,
	name:  MallocBuffer<u8>
}

impl UserRecord
{
	///Get the UID of the user.
	pub fn uid(&self) -> Uid { self.uid }
	///Get the primary GID of the user.
	pub fn gid(&self) -> Gid { self.gid }
	///Get the other GIDs of the user.
	pub fn extra_gids<'a>(&'a self) -> &'a [Gid] { unsafe{self.extra_gids.as_slice()} }
	///Get the home directory of the user.
	pub fn homedir<'a>(&'a self) -> &'a [u8] { unsafe{self.homedir.as_slice()} }
	///Get the name of the user.
	pub fn name<'a>(&'a self) -> &'a [u8] { unsafe{self.name.as_slice()} }
}

///A freeform error message.
///
///The only thing to do with it is to print it using the `Display` implementation (`{}` format operator).
pub struct ErrorMessage(Result<MallocBuffer<u8>, &'static str>);

impl Display for ErrorMessage
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self.0.as_ref() {
			Err(m) => f.write_str(m)?,
			Ok(self0) => {
				//Assumes Self0 has beeen fully initialized => structure invariant.
				let msg = unsafe{self0.as_slice()};
				let mut itr = 0;
				while itr < msg.len() {
					let msg = &msg[itr..];
					let stride = from_utf8(msg).map(|x|x.len()).
						unwrap_or_else(|e|e.valid_up_to());
					if stride > 0 {
						//Valid UTF-8 for stride bytes by definition of valid_up_to().
						unsafe{f.write_str(from_utf8_unchecked(&msg[..stride]))?};
						itr += stride;
					} else {
						//msg[0] is always valid as itr < msg.len().
						write!(f, "<{b:02x}>", b=msg[0])?;
					}
				}
			}
		}
		Ok(())
	}
}

impl ErrorMessage
{
	pub(crate) fn new_str(ptr: &'static str) -> ErrorMessage { ErrorMessage(Err(ptr)) }
	//Assumes ptr is NUL-terminated string.
	pub(crate) unsafe fn new(ptr: *const c_char) -> ErrorMessage
	{
		let ptr = f_return!(NonNull::new(ptr), ErrorMessage(Err("(null)")));
		match unsafe{MallocBuffer::<u8>::from_slice(asciiz_pointer(ptr))} {
			Some(msg) => ErrorMessage(Ok(msg)),
			None => ErrorMessage(Err("Out of memory"))
		}
	}
}

///libc char type.
pub type CCharT = c_char;
