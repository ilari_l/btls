use btls_aux_memory::EscapeByteString;
use btls_aux_unix::OsError;
use btls_aux_unix::Path;
use btls_aux_unix::SetxattrFlags;
use std::os::unix::ffi::OsStrExt;


fn main()
{
	let args: Vec<_> = std::env::args_os().skip(1).collect();
	if args.len() < 2 || args.len() > 3 { panic!("Expected 2 or 3 arguments"); }
	let filename = Path::new(args[0].as_bytes()).expect("Invalid filename");
	let attrname = Path::new(args[1].as_bytes()).expect("Invalid attrname");
	if args.len() == 2 {
		match filename.lgetxattr(attrname) {
			Err(OsError::ENODATA) => println!("<unset>"),
			Err(err) => eprintln!("lgetxattr() failed: {err}"),
			Ok(value) => println!("{value}", value=EscapeByteString(&value)),
		}
	} else {
		let attrval = args[2].as_bytes();
		filename.lsetxattr(attrname, attrval, SetxattrFlags::Default).expect("lsetxattr() failed");
	}
}
