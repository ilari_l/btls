use crate::OsError;
use crate::PrintBuffer2;
use crate::RawMalloc;
use btls_aux_memory::NonNull;
use core::fmt::Arguments;
use core::fmt::Write;
use core::mem::MaybeUninit;
use crate::fd::STDERR;
use libc::c_int;

///Facility to log.
#[derive(Copy,Clone,Debug)]
pub enum LogFacility
{
	///Authorization messages.
	Authorization,
	///Authorization messages (public).
	AuthorizationPublic,
	///Cron daemon.
	Cron,
	///Generic daemon.
	Daemon,
	///FTP daemon.
	FTP,
	///Local type #0.
	Local0,
	///Local type #1.
	Local1,
	///Local type #2.
	Local2,
	///Local type #3.
	Local3,
	///Local type #4.
	Local4,
	///Local type #5.
	Local5,
	///Local type #6.
	Local6,
	///Local type #7.
	Local7,
	///Printer subsystem.
	LinePrinter,
	///Mail subsystem.
	Mail,
	///Usenet NEWS subsystem.
	News,
	///User messages.
	User,
	///UUCP subsystem.
	UUCP,
}

///Level of log message.
#[derive(Copy,Clone,Debug)]
pub enum LogLevel
{
	///System is unusable.
	Emergency,
	///Immediate action required.
	Alert,
	///Critical error.
	Critical,
	///Error.
	Error,
	///Warning.
	Warning,
	///Important message.
	Notice,
	///Informational message.
	Informational,
	///Debugging message.
	Debug,
}

impl LogLevel
{
	///Is at least of severity `c`?
	pub fn at_least(self, c: LogLevel) -> bool { self.priority() >= c.priority() }
	fn priority(self) -> u32
	{
		match self {
			LogLevel::Emergency => 7,
			LogLevel::Alert => 6,
			LogLevel::Critical => 5,
			LogLevel::Error => 4,
			LogLevel::Warning => 3,
			LogLevel::Notice => 2,
			LogLevel::Informational => 1,
			LogLevel::Debug => 0,
		}
	}
	fn systemd_log_priority(self) -> char
	{
		match self {
			LogLevel::Emergency => '0',
			LogLevel::Alert => '1',
			LogLevel::Critical => '2',
			LogLevel::Error => '3',
			LogLevel::Warning => '4',
			LogLevel::Notice => '5',
			LogLevel::Informational => '6',
			LogLevel::Debug => '7',
		}
	}
	fn btls_log_priority(self) -> char
	{
		match self {
			LogLevel::Emergency => 'G',
			LogLevel::Alert => 'A',
			LogLevel::Critical => 'C',
			LogLevel::Error => 'E',
			LogLevel::Warning => 'W',
			LogLevel::Notice => 'N',
			LogLevel::Informational => 'I',
			LogLevel::Debug => 'D',
		}
	}
	///Get textual prefix for log message priority.
	pub fn textual_log_priority(self) -> &'static str
	{
		match self {
			LogLevel::Emergency => "Emergency",
			LogLevel::Alert => "Alert",
			LogLevel::Critical => "Critical",
			LogLevel::Error => "Error",
			LogLevel::Warning => "Warning",
			LogLevel::Notice => "Notice",
			LogLevel::Informational => "Informational",
			LogLevel::Debug => "Debug",
		}
	}
}

define_bitfield_type!([doc=r#"Flags for `openlog(3)`."#];OpenlogFlags c_int);
impl OpenlogFlags
{
	define_symbol!([doc=r#"Log to console if there is an error with `syslog(3)`."#];
		LOG_CONS: OpenlogFlags as CONS);
	define_symbol!([doc=r#"Open connection immediately."#];LOG_NDELAY: OpenlogFlags as NDELAY);
	define_symbol!([doc=r#"Do not wait for children (no effect on Linux)."#];LOG_NOWAIT: OpenlogFlags as NOWAIT);
	define_symbol!([doc=r#"Delay opening connection until `syslog(3)`."#];LOG_ODELAY: OpenlogFlags as ODELAY);
	define_symbol!([doc=r#"Include PID in log message."#];LOG_PID: OpenlogFlags as PID);
}

///Log target.
#[derive(Copy,Clone,Debug)]
pub enum Logger
{
	///STDERR, printed as plain messages.
	StderrPlain,
	///STDERR, printed using internal log protocol.
	StderrInternal,
	///STDERR, printed using SystemD log protocol.
	StderrSystemD,
	///Syslog.
	Syslog,
}

impl Logger
{
	///Call `openlog(3)`.
	pub fn openlog(ident: &[u8], option: OpenlogFlags, facility: LogFacility)
	{
		let facility = trans_facility(facility);
		//Need to allocate ident, as it is assumed unfreed. The +1 guarantees need > 0.
		let need = ident.len() + 1;
		let ident2 = RawMalloc::<u8>::new(need);
		let ident2 = if let Some(ident2) = ident2 { unsafe{
			//Leak the memory, as openlog takes ownership of the memory.
			let ident2 = ident2.leak();
			//Assumes malloc hands the memory, which is big enough for both ident and NUL.
			ident2.write_slice(ident);
			ident2.add(ident.len()).write(0);
			//Nothing frees ident2, it is leaked.
			ident2.to_const()
		}} else {
			static DUMMY: &'static [u8] = b"(ENOMEM)\0";
			NonNull::new_slice_base(DUMMY)
		};
		//Assumes ident2 is nul-terminated string => true by construction above.
		unsafe{libc::openlog(ident2.to_raw().cast(), option.0, facility)};
		//Just leak the ident2, do not care about the case where logger is reinitialized.
	}
	///Log a message with facility `facility` (default if `None`) priority `priority` and message `msg`.
	///
	///Note that only the Syslog logger supports facilities, the others just ignore them.
	pub fn log(&self, facility: Option<LogFacility>, priority: LogLevel, msg: Arguments)
	{
		match *self {
			Logger::StderrPlain => call_syslog_stderr_pfx(priority.textual_log_priority(), msg),
			Logger::StderrInternal => call_syslog_stderr(priority.btls_log_priority(), msg),
			Logger::StderrSystemD => call_syslog_stderr(priority.systemd_log_priority(), msg),
			Logger::Syslog => {
				let mut buf = [MaybeUninit::uninit();4096];
				let mut buf = PrintBuffer2::new(&mut buf);
				write!(buf, "{msg}").ok();
				buf.force_last_nul();
				let buf = buf.into_inner_bytes();
				let facility = facility.map(|f|trans_facility(f)).unwrap_or(0);
				let priority = trans_level(priority);
				unsafe{
					let fmt: *const libc::c_char = b"%s\0".as_ptr().cast();
					//Assumes that buf is nul-terminated => True by force_last_nul() above.
					libc::syslog(facility | priority, fmt, buf.as_ptr());
				}
			}
		}
	}
}

fn trans_level(priority: LogLevel) -> c_int
{
	match priority {
		LogLevel::Emergency => libc::LOG_EMERG,
		LogLevel::Alert => libc::LOG_ALERT,
		LogLevel::Critical => libc::LOG_CRIT,
		LogLevel::Error => libc::LOG_ERR,
		LogLevel::Warning => libc::LOG_WARNING,
		LogLevel::Notice => libc::LOG_NOTICE,
		LogLevel::Informational => libc::LOG_INFO,
		LogLevel::Debug => libc::LOG_DEBUG,
	}
}

fn trans_facility(facility: LogFacility) -> c_int
{
	match facility {
		LogFacility::Authorization => libc::LOG_AUTHPRIV,
		LogFacility::AuthorizationPublic => libc::LOG_AUTH,
		LogFacility::Cron => libc::LOG_CRON,
		LogFacility::Daemon => libc::LOG_DAEMON,
		LogFacility::FTP => libc::LOG_FTP,
		LogFacility::Local0 => libc::LOG_LOCAL0,
		LogFacility::Local1 => libc::LOG_LOCAL1,
		LogFacility::Local2 => libc::LOG_LOCAL2,
		LogFacility::Local3 => libc::LOG_LOCAL3,
		LogFacility::Local4 => libc::LOG_LOCAL4,
		LogFacility::Local5 => libc::LOG_LOCAL5,
		LogFacility::Local6 => libc::LOG_LOCAL6,
		LogFacility::Local7 => libc::LOG_LOCAL7,
		LogFacility::LinePrinter => libc::LOG_LPR,
		LogFacility::Mail => libc::LOG_MAIL,
		LogFacility::News => libc::LOG_NEWS,
		LogFacility::User => libc::LOG_USER,
		LogFacility::UUCP => libc::LOG_UUCP,
	}
}

fn call_syslog_stderr(lchar: char, msg: Arguments)
{
	let mut buf = [MaybeUninit::uninit();4096];
	let mut buf = PrintBuffer2::new(&mut buf);
	write!(buf, "<{lchar}>{msg}").ok();
	buf.force_last_linefeed();
	flush_stderr(buf.into_inner_bytes());
}

fn call_syslog_stderr_pfx(pfx: &str, msg: Arguments)
{
	let mut buf = [MaybeUninit::uninit();4096];
	let mut buf = PrintBuffer2::new(&mut buf);
	write!(buf, "{pfx}: {msg}").ok();
	buf.force_last_linefeed();
	flush_stderr(buf.into_inner_bytes());
}

fn flush_stderr(buf: &[u8])
{
	let mut itr = 0;
	while itr < buf.len() {
		match STDERR.write(&buf[itr..]) {
			Ok(r) => itr += r,
			Err(e) if e == OsError::EINTR => continue,		//Try again.
			Err(_) => return					//Error.
		}
	}
}
