#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]
#[cfg(test)] #[macro_use] extern crate std;
use btls_aux_fail::fail_if;
use btls_aux_memory::asciiz_pointer;
use btls_aux_memory::NonNull;
use btls_aux_memory::ObjectExt;
use btls_aux_memory::PrintBuffer as PrintBuffer2;
pub use btls_aux_memory::SmallVec;
use btls_aux_unix_cimpl::__Dirstream;
use btls_aux_unix_cimpl::call_closedir;
use btls_aux_unix_cimpl::call_fdopendir;
use btls_aux_unix_cimpl::call_readdir;
pub use btls_aux_unix_cimpl::OsError;
pub use btls_aux_unix_cimpl::PrintShortOsError;
use core::cmp::Ordering;
use core::fmt::Debug;
use core::mem::MaybeUninit;
use core::mem::size_of;
use core::mem::size_of_val;
use core::mem::zeroed;
use core::ops::Deref;
use core::ptr::null_mut;
pub use crate::fd::*;
pub use crate::fs::*;
use libc::c_char;
pub use crate::pid::*;
use crate::raw_malloc::RawMalloc;
pub use crate::sys::*;
pub use crate::syslog::*;
pub use crate::user::*;

#[cfg(target_os="linux")] 
///If on Linux, do `$a`, otherwise do `$b`.
///
///Note that `$b` is parsed even on Linux, but never executed. `$a` is not even parsed on non-Linux.
#[macro_export] macro_rules! if_linux { ($a:expr, $b:expr) => { if true { $a } else { $b } }; }
#[cfg(not(target_os="linux"))]
///If on Linux, do `$a`, otherwise do `$b`.
///
///Note that `$b` is parsed even on Linux, but never executed. `$a` is not even parsed on non-Linux.
#[macro_export] macro_rules! if_linux { ($a:expr, $b:expr) => { $b }; }

//DLFCN is known to exist on Linux, FreeBSD, OpenBSD and NetBSD.
#[cfg(any(target_os="linux",target_os="freebsd",target_os="openbsd",target_os="netbsd"))]
macro_rules! if_has_dlfcn { ($a:expr, $b:expr) => { if true { $a } else { $b } }; }
#[cfg(not(any(target_os="linux",target_os="freebsd",target_os="openbsd",target_os="netbsd")))]
macro_rules! if_has_dlfcn { ($a:expr, $b:expr) => { $b }; }

macro_rules! define_enum_type
{
	($([$comments:meta])*;$name:ident $inner:ident NODEFAULT) => {
		#[derive(Copy,Clone,Debug,PartialEq,Eq,PartialOrd,Ord,Hash)]
		$(#[$comments])*
		pub struct $name($inner);
		impl $name
		{
			///Return the raw value of the constant.
			pub fn to_inner(self) -> $inner { self.0 }
		}
	};
	($([$comments:meta])*;$name:ident $inner:ident) => {
		define_enum_type!($([$comments])*;$name $inner NODEFAULT);
		impl Default for $name
		{
			fn default() -> $name { $name(0) }
		}
	}
}

macro_rules! define_bitfield_type
{
	($([$comments:meta])*;$name:ident $inner:ident) => {
		define_enum_type!($([$comments])*;$name $inner);
		impl ::core::ops::BitOr for $name
		{
			type Output = $name;
			fn bitor(self, other: Self) -> Self { $name(self.0 | other.0) }
		}
		impl ::core::ops::BitOrAssign for $name
		{
			fn bitor_assign(&mut self, other: Self) { self.0 |= other.0; }
		}
	}
}

macro_rules! define_symbol {
	($([$comments:meta])*;$name:ident : $xtype:ident VAL $val:expr) => {
		$(#[$comments])* pub const $name: $xtype = $xtype($val);
	};
	($([$comments:meta])*;$name:ident : $xtype:ident as $name2:ident) => {
		$(#[$comments])* pub const $name2: $xtype = $xtype(libc::$name);
	};
	($([$comments:meta])*;$name:ident : $xtype:ident as $name2:ident LINUX_ONLY) => {
		#[cfg(target_os="linux")]
		$(#[$comments])* pub const $name2: $xtype = $xtype(libc::$name);
		#[cfg(not(target_os="linux"))]
		$(#[$comments])* pub const $name2: $xtype = $xtype(0);
	};
	($([$comments:meta])*;$name:ident : $xtype:ident) => {
		$(#[$comments])* pub const $name: $xtype = $xtype(libc::$name);
	};
}

macro_rules! malloc_failed { ($x:expr) => { $x.ok_or(OsError::ENOMEM)? } }

mod fd;
mod fs;
mod pid;
mod raw_malloc;
mod syslog;
mod user;

#[cfg(not(target_os = "linux"))] #[path="sys_unix.rs"] mod sys;
#[cfg(target_os = "linux")]#[path="sys_linux.rs"] mod sys;

const XSMALL_CSTR_SIZE: usize = 256;
const SMALL_CSTR_SIZE: usize = 1024;

enum CString
{
	XSmall([MaybeUninit<u8>;XSMALL_CSTR_SIZE]),
	Small([MaybeUninit<u8>;SMALL_CSTR_SIZE]),
	Large(RawMalloc<u8>),
}

impl CString
{
	fn new_c_string_unchecked(x: &[u8]) -> Result<CString, OsError>
	{
		if x.len() < XSMALL_CSTR_SIZE {
			let mut buf = SmallVec::<u8,XSMALL_CSTR_SIZE>::new();
			//Assumes that x.len() < XSMALL_CSTR_SIZE, so the NUL-terminator will fit.
			buf.extend(x);
			buf.push(0);
			Ok(CString::XSmall(buf.into_inner()))
		} else if x.len() < SMALL_CSTR_SIZE {
			let mut buf = SmallVec::<u8,SMALL_CSTR_SIZE>::new();
			//Assumes that x.len() < SMALL_CSTR_SIZE, so the NUL-terminator will fit.
			buf.extend(x);
			buf.push(0);
			Ok(CString::Small(buf.into_inner()))
		} else { unsafe {
			let need = malloc_failed!(x.len().checked_add(1));
			let mut buf = malloc_failed!(RawMalloc::new(need));
			//need = x.len() + 1, so NUL-terminator will fit.
			let __buf = buf.get_base_mut();
			__buf.write_slice(x);
			__buf.add(x.len()).write(0);
			Ok(CString::Large(buf))
		}}
	}
	fn new_c_string(x: &[u8]) -> Result<CString, OsError>
	{
		//Check that x does not contain NUL.
		fail_if!(x.iter().cloned().any(|b|b==0), OsError::EINVAL);
		CString::new_c_string_unchecked(x)
	}
	fn as_ptr(&self) -> *const c_char
	{
		match self {
			&CString::XSmall(ref x) => x.as_ptr() as _,
			&CString::Small(ref x) => x.as_ptr() as _,
			&CString::Large(ref x) => x.get_base().to_raw().cast(),
		}
	}
	fn as_slice<'a>(&'a self) -> &'a [u8]
	{
		unsafe{asciiz_pointer(self.as_ptr())}
	}
}

fn ___do_readlink(mut func: impl FnMut(&mut [MaybeUninit<u8>]) -> Result<usize, OsError>) -> Result<CString, OsError>
{
	let mut buf = [MaybeUninit::uninit();XSMALL_CSTR_SIZE];
	let amt = func(&mut buf)?;
	if amt < XSMALL_CSTR_SIZE {
		//Ok, NUL-terminate and return.
		buf[amt] = MaybeUninit::new(0);
		return Ok(CString::XSmall(buf));
	}
	//Not extra small.
	let mut buf = [MaybeUninit::uninit();SMALL_CSTR_SIZE];
	let amt = func(&mut buf)?;
	if amt < SMALL_CSTR_SIZE {
		//Ok, NUL-terminate and return.
		buf[amt] = MaybeUninit::new(0);
		return Ok(CString::Small(buf));
	}
	//Not small.
	let mut alloc = 16 * SMALL_CSTR_SIZE / 10;
	let mut buf = RawMalloc::<u8>::new(alloc).ok_or(OsError::ENOMEM)?;
	loop { unsafe {
		alloc = 16 * alloc / 10;
		buf.resize(alloc).ok_or(OsError::ENOMEM)?;
		let base = buf.get_base_mut().cast::<MaybeUninit::<u8>>();
		let amt = func(base.make_slice_mut(alloc))?;
		if amt < alloc {
			//Ok, NUL-terminate and return.
			buf.get_base_mut().add(amt).write(0);
			return Ok(CString::Large(buf));
		}
	}}
}

fn __do_readlink(func: impl FnMut(&mut [MaybeUninit<u8>]) -> Result<usize, OsError>) ->
	Result<DirectoryFilename, OsError>
{
	___do_readlink(func).map(DirectoryFilename::new)
}

unsafe fn with_uninitialized<T,E,F>(initializer: F) -> Result<T, E> where F: FnOnce(*mut T) -> Result<(), E>
{
	let mut tmp = MaybeUninit::uninit();
	initializer(tmp.as_mut_ptr())?;
	Ok(unsafe{tmp.assume_init()})
}

unsafe fn with_uninitialized_read<T,E,F>(incomplete: E, initializer: F) -> Result<T, E> where
	F: FnOnce(*mut u8, usize) -> Result<usize, E>
{
	let mut tmp = MaybeUninit::<T>::uninit();
	let bcount = size_of::<T>();
	let amt = initializer(tmp.as_mut_ptr().cast(), bcount)?;
	if amt < bcount { return Err(incomplete); }
	Ok(unsafe{tmp.assume_init()})
}


//Uses size_of::<T>() bytes from source.
#[inline(always)] pub(crate) unsafe fn structure_from_bytes<T:Copy>(src: *const u8) -> T
{
	//Assumes:
	//1) src is valid for at least size_of(T) bytes => Precondition of function.
	//2) T is valid to copy => Guaranteed by the T: Copy bound.
	unsafe{src.cast::<T>().read_unaligned()}
}

struct Dirstream(*mut __Dirstream);

//Dirstream is safe to send between threads, but not to share between threads.
unsafe impl Send for Dirstream {}

impl Drop for Dirstream
{
	//Object can only be dropped once.
	fn drop(&mut self) { unsafe{call_closedir(self.0);} }
}

impl Iterator for Dirstream
{
	//Can not return temporary memory, so make CString.
	type Item = Result<CString, OsError>;
	fn next(&mut self) -> Option<Result<CString, OsError>>
	{
		unsafe {
			//The stream is open since self exists.
			match NonNull::new(call_readdir(self.0)) {
				Some(ent) => {
					let ent = ent.cstr_as_slice();
					Some(CString::new_c_string_unchecked(ent))		//Successful.
				},
				//This can return no error on end of directory.
				None => OsError::new_maybe_none().map(Err)
			}
		}
	}
}

impl Dirstream
{
	fn new(fd: FileDescriptor) -> Result<Dirstream, OsError>
	{
		unsafe {
			let fd = fd.into_fdb();
			let strm = call_fdopendir(fd.as_raw_fd());
			if strm.is_null() {
				let error = OsError::new();			//Save error, as close overwrites it.
				fd.close();					//Drop the owned fd.
				Err(error)
			} else {
				Ok(Dirstream(strm))				//Successful.
			}
		}
	}
}

///File metadata.
#[derive(Copy,Clone)]
pub struct Stat(libc::stat);

impl Stat
{
	///Get UID of file owner.
	pub fn uid(&self) -> Uid { Uid::new(self.0.st_uid) }
	///Get GID of file owner.
	pub fn gid(&self) -> Gid { Gid::new(self.0.st_gid) }
	///Get size of file in bytes.
	pub fn size(&self) -> u64 { self.0.st_size as u64 }
	///Get device file is on.
	pub fn device(&self) -> u64 { self.0.st_dev as u64 }
	///Get inode number of file.
	pub fn inode(&self) -> u64 { self.0.st_ino as u64 }
	///Get modification time of file (first is seconds, second is sub-second nanoseconds).
	pub fn mtime(&self) -> (i64, u32) { (self.0.st_mtime as i64, self.0.st_mtime_nsec as u32) }
	///Get access time of file (first is seconds, second is sub-second nanoseconds).
	pub fn atime(&self) -> (i64, u32) { (self.0.st_atime as i64, self.0.st_atime_nsec as u32) }
	///Get creation time of file (first is seconds, second is sub-second nanoseconds).
	pub fn ctime(&self) -> (i64, u32) { (self.0.st_ctime as i64, self.0.st_ctime_nsec as u32) }
	///Get type of file.
	pub fn format(&self) -> StatModeFormat { StatModeFormat(self.0.st_mode & libc::S_IFMT) }
	///Get file permission bits (includes setuid/setgid/sticky bits).
	pub fn permissions(&self) -> u32 { self.0.st_mode as u32 & 4095 }
	///Get number of hard links to file.
	pub fn links(&self) -> u64 { self.0.st_nlink as u64 }
	///Get the referenced device for character/block device file.
	pub fn rdevice(&self) -> u64 { self.0.st_rdev as u64 }
	///Get the optimal block size for the file.
	pub fn blocksize(&self) -> u64 { self.0.st_blksize as u64 }
	///Get number of allocated blocks in file.
	pub fn blockcount(&self) -> u64 { self.0.st_blocks as u64 }
}

///`stat(2)` file types.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub struct StatModeFormat(libc::mode_t);
impl StatModeFormat {
	define_symbol!([doc=r#"File is a directory"#];S_IFDIR: StatModeFormat as DIR);
	define_symbol!([doc=r#"File is a regular file"#];S_IFREG: StatModeFormat as REG);
	define_symbol!([doc=r#"File is a symbolic link"#];S_IFLNK: StatModeFormat as LNK);
	define_symbol!([doc=r#"File is a Unix domain socket"#];S_IFSOCK: StatModeFormat as SOCK);
}

trait ReturnValue: Sized+Copy
{
	type Output: Sized;
	fn __to_value(self) -> Option<<Self as ReturnValue>::Output>;
	fn errno_val(self) -> Result<<Self as ReturnValue>::Output, OsError>
	{
		self.__to_value().ok_or_else(||OsError::new())
	}
	fn errno(self) -> Result<(), OsError> { self.__to_value().map(|_|()).ok_or_else(||OsError::new()) }
	fn errno_sval(self) -> Result<Self, OsError>
	{
		self.__to_value().ok_or_else(||OsError::new())?;
		Ok(self)
	}
}

macro_rules! def_returnval {
	($xtype:ident $assoc:ident) => {
		impl ReturnValue for $xtype
		{
			type Output = $assoc;
			fn __to_value(self) -> Option<$assoc> { if self < 0 { None } else { Some(self as _) } }
		}
	}
}

def_returnval!(i8 u8);
def_returnval!(i16 u16);
def_returnval!(i32 u32);
def_returnval!(i64 u64);
def_returnval!(isize usize);

extern {
	fn unix__call_getenv(name: *const c_char) -> *const c_char;
}

///Owned byte string.
pub struct OwnedByteString(RawMalloc<u8>);

impl OwnedByteString
{
	//fn blank() -> OwnedByteString { OwnedByteString(RawMalloc::__blank()) }
	unsafe fn alloc(len: usize) -> Result<OwnedByteString, OsError>
	{
		Ok(OwnedByteString(malloc_failed!(RawMalloc::<u8>::new(len))))
	}
	fn from_slice(val: &[u8]) -> Result<OwnedByteString, OsError>
	{
		unsafe {
			let mut ptr = Self::alloc(val.len())?;
			let __ptr = &mut ptr.0;
			//The buffer is valid.
			if __ptr.is_nontrivial() { __ptr.get_base_mut().write_slice(val); }
			Ok(ptr)
		}
	}
	unsafe fn new(val: NonNull<c_char>) -> Result<OwnedByteString, OsError>
	{
		unsafe{Self::from_slice(asciiz_pointer(val))}
	}
}

impl Deref for OwnedByteString
{
	type Target = [u8];
	//The pointer is always valid base, as dangling pointer is valid for size 0.
	fn deref(&self) -> &[u8]
	{
		let base = self.0.get_base();
		let size = self.0.get_size();
		unsafe{base.make_slice(size)}
	}
}

///Call `getenv(3)`.
///
///Error ENOENT -> No such environment variable.
///Error ENOMEM -> Out of memory.
pub fn getenv(name: &[u8]) -> Result<OwnedByteString, OsError>
{
	const MAXSZ: usize = 255;
	const NEED: usize = MAXSZ+1;	//+1 is for NUL terminator.
	let mut namebuf = SmallVec::<u8,NEED>::new();
	//Check that name does not contain = nor NULL, nor is longer than MAXSZ. Illegal names just do not
	//exist.
	let is_illegal = name.iter().any(|&c|matches!(c, 0|b'='));
	fail_if!(is_illegal || name.len() > MAXSZ, OsError::ENOENT);
	//Assume these fit.
	namebuf.extend(name);
	namebuf.push(0);
	//Read the variable.
	let env_val = unsafe{unix__call_getenv(namebuf.as_ptr().cast())};
	let env_val = NonNull::new(env_val).ok_or(OsError::ENOENT)?;
	unsafe{OwnedByteString::new(env_val)}
}

///Call `setsid(2)`
pub fn setsid() -> Result<(), OsError>
{
	//Unconditionally safe.
	unsafe{libc::setsid()}.errno()
}

///Is Systemd notify type daemon?
pub fn systemd_is_notify() -> bool
{
	crate::sys::systemd_is_notify()
}

///Notify Systemd that startup is complete. Should only be called if systemd_is_notify() returned true.
pub fn systemd_notify_startup() -> Result<(), OsError>
{
	crate::sys::systemd_notify_startup()
}

///Call `usleep(2)`
pub fn usleep(secs: u32) -> Result<(), OsError>
{
	//Unconditionally safe.
	unsafe{libc::usleep(secs).errno()}
}

///Get current Unix timestamp with millisecond granularity.
///
///The return value is unsigned, since time is always after 1970.
pub fn unix_time_milliseconds() -> u64
{
	//zeroes is valid value for timeval.
	let mut now: libc::timeval = unsafe{zeroed()};
	//Assumes now is valid for size_of(timeval) bytes => True by definition.
	unsafe{libc::gettimeofday(now.pointer_to_mut(), null_mut())};
	(now.tv_sec as u64).saturating_mul(1000).saturating_add(now.tv_usec as u64 / 1000)
}

///Leap second status.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum LeapSecondStatus
{
	///No leap second today.
	Normal,
	///Leap second will be inserted at end of day.
	WillInsert,
	///Leap second will be deleted at end of day.
	WillDelete,
	///Leap second in progress.
	InProgress,
}

///Extended time.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub struct ExtendedTime
{
	///Leap second status.
	pub leap_second: LeapSecondStatus,
	///Unix timestamp.
	pub timestamp: i64,
	///Nanoseconds past whole second.
	pub nanoseconds: u32,
}

impl ExtendedTime
{
	///Get current time.
	///
	/// # Error conditions:
	///
	/// * The call failed somehow.
	///
	/// # Return value:
	///
	///Current time.
	pub fn now() -> Result<ExtendedTime, OsError> { Self::__now() }
}

#[cfg(not(target_os="linux"))]
impl ExtendedTime
{
	fn __now() -> Result<ExtendedTime, OsError>
	{
		//zeroes is valid value for timeval.
		let mut now: libc::timeval = unsafe{zeroed()};
		//Assumes now is valid for size_of(timeval) bytes => True by definition.
		unsafe{libc::gettimeofday(now.pointer_to_mut(), null_mut()).errno()?};
		Ok(ExtendedTime {
			//No leap second status available.
			leap_second: LeapSecondStatus::Normal,
			timestamp: now.tv_sec,
			nanoseconds: now.tv_usec as u32 * 1000,
		})
	}
}

///Get current day number and nanosecond of the day.
pub fn get_date_and_time() -> Result<(i64, Option<u32>, u32), OsError>
{
	let ts = ExtendedTime::now()?;
	let day = ts.timestamp.div_euclid(86400);
	let mut sec = Some(ts.timestamp.rem_euclid(86400) as u32);
	if ts.leap_second == LeapSecondStatus::InProgress { sec = None; }
	Ok((day, sec, ts.nanoseconds))
}


define_bitfield_type!([doc=r#"Flags for `unshare(2)` (Linux only)."#];UnshareFlags i32);
impl UnshareFlags
{
	define_symbol!([doc=r#"Unshare file descriptor table."#];FILES: UnshareFlags VAL 0x00000400);
	pub fn unshare(self) -> Result<(), OsError> { crate::sys::unshare(self.0) }
}

fn offof<B,T>(b: &B, x: &T) -> usize
{
        (x.pointer_to() as usize).wrapping_sub(b.pointer_to() as usize)
}

fn compatible_fields<A,B,AF,BF>(a: &A, b: &B, af: &AF, bf: &BF) -> bool
{
	offof(a, af) == offof(b, bf) && size_of_val(af) == size_of_val(bf)
}

//TODO: Lock/Unlock. Otherwise it is unsafe to call dlopen/dlsym concurrently.
struct DlfcnLock;
impl DlfcnLock
{
	fn new() -> DlfcnLock { DlfcnLock }
}

///Linux version.
#[derive(Copy,Clone,Debug)]
pub struct LinuxVersion(u32);

impl LinuxVersion
{
	///Version of currently running kernel.
	pub fn current() -> LinuxVersion
	{
		if_linux!({
			LinuxVersion(crate::sys::get_linux_version())
		}, {
			LinuxVersion(0)
		})
	}
	///Construct 2-digit version.
	pub const fn version2(a: u8, b: u8) -> LinuxVersion
	{
		LinuxVersion((a as u32) << 24 | (b as u32) << 16)
	}
	///Construct 3-digit version.
	pub const fn version3(a: u8, b: u8, c: u16) -> LinuxVersion
	{
		LinuxVersion((a as u32) << 24 | (b as u32) << 16 | c as u32)
	}
}

//Manually impl equals and relational ops, in order for it to not be structurally equal (which would allow
//using it in match.
impl PartialEq for LinuxVersion
{
	fn eq(&self, other: &Self) -> bool { self.0 == other.0 }
}

impl Eq for LinuxVersion {}

impl PartialOrd for LinuxVersion
{
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> { self.0.partial_cmp(&other.0) }
}

impl Ord for LinuxVersion
{
	fn cmp(&self, other: &Self) -> Ordering { self.0.cmp(&other.0) }
}

