use super::OsError;
use super::ReturnValue;
use btls_aux_memory::ObjectExt;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::mem::size_of;
use core::mem::transmute;
use core::mem::zeroed;
use core::ptr::null;
use core::slice::from_raw_parts_mut;
use libc::c_char;
use libc::c_int;
use libc::pid_t;
use libc::SIG_DFL;
use libc::SIG_IGN;
use libc::sigset_t;



///A signal handler.
pub enum SignalHandler
{
	///Ignore the signal.
	Ignore,
	///Apply default handling.
	Default,
	///Call function on signal.
	Function(unsafe extern "C" fn(RawSignalT))
}

///Raw signal number.
pub type RawSignalT = c_int;

///A signal number.
#[derive(Copy,Clone,Hash,Debug,PartialOrd,Ord,PartialEq,Eq)]
pub struct Signal(c_int);
impl Signal
{
	///Create a signal with specified number.
	///
	///Normal signals are in range 0-31.
	pub fn new(num: u8)  -> Signal { Signal(num as c_int) }
	//Assumes that h is valid signal handler.
	pub unsafe fn set_handler(self, h: SignalHandler) -> SignalHandler
	{
		let h: usize = match h {
			SignalHandler::Ignore => SIG_IGN,
			SignalHandler::Default => SIG_DFL,
			SignalHandler::Function(f) => unsafe{transmute(f)},
		};
		//Assume this never fails. Assumes function precondition.
		let h = unsafe{libc::signal(self.0, h)};
		match h {
			SIG_IGN => SignalHandler::Ignore,
			SIG_DFL => SignalHandler::Default,
			f => SignalHandler::Function(unsafe{transmute(f)})
		}
	}
	define_symbol!([doc=r#"Signal SIGABRT."#];SIGABRT: Signal as ABRT);
	define_symbol!([doc=r#"Signal SIGALRM."#];SIGALRM: Signal as ALRM);
	define_symbol!([doc=r#"Signal SIGBUS."#]; SIGBUS:  Signal as BUS);
	define_symbol!([doc=r#"Signal SIGCHLD."#];SIGCHLD: Signal as CHLD);
	define_symbol!([doc=r#"Signal SIGCONT."#];SIGCONT: Signal as CONT);
	define_symbol!([doc=r#"Signal SIGFPE."#]; SIGFPE:  Signal as FPE);
	define_symbol!([doc=r#"Signal SIGHUP."#]; SIGHUP:  Signal as HUP);
	define_symbol!([doc=r#"Signal SIGILL."#]; SIGILL:  Signal as ILL);
	define_symbol!([doc=r#"Signal SIGINT."#]; SIGINT:  Signal as INT);
	define_symbol!([doc=r#"Signal SIGKILL."#];SIGKILL: Signal as KILL);
	define_symbol!([doc=r#"Signal SIGPIPE."#];SIGPIPE: Signal as PIPE);
	define_symbol!([doc=r#"Signal SIGPROF."#];SIGPROF: Signal as PROF);
	define_symbol!([doc=r#"Signal SIGQUIT."#];SIGQUIT: Signal as QUIT);
	define_symbol!([doc=r#"Signal SIGSEGV."#];SIGSEGV: Signal as SEGV);
	define_symbol!([doc=r#"Signal SIGSTOP."#];SIGSTOP: Signal as STOP);
	define_symbol!([doc=r#"Signal SIGSYS."#]; SIGSYS:  Signal as SYS);
	define_symbol!([doc=r#"Signal SIGTERM."#];SIGTERM: Signal as TERM);
	define_symbol!([doc=r#"Signal SIGTRAP."#];SIGTRAP: Signal as TRAP);
	define_symbol!([doc=r#"Signal SIGTSTP."#];SIGTSTP: Signal as TSTP);
	define_symbol!([doc=r#"Signal SIGTTIN."#];SIGTTIN: Signal as TTIN);
	define_symbol!([doc=r#"Signal SIGTTOU."#];SIGTTOU: Signal as TTOU);
	define_symbol!([doc=r#"Signal SIGURG."#]; SIGURG:  Signal as URG);
	define_symbol!([doc=r#"Signal SIGUSR1."#];SIGUSR1: Signal as USR1);
	define_symbol!([doc=r#"Signal SIGUSR2."#];SIGUSR2: Signal as USR2);
	define_symbol!([doc=r#"Signal SIGVTALRM."#];SIGVTALRM: Signal as VTALRM);
	define_symbol!([doc=r#"Signal SIGXCPU."#];SIGXCPU: Signal as XCPU);
	define_symbol!([doc=r#"Signal SIGXFSZ."#];SIGXFSZ: Signal as XFSZ);
}

//Solarish
#[cfg(any(target_os="solaris",target_os="illumos"))]
impl Signal
{
	define_symbol!([doc=r#"Signal SIGIO."#];SIGIO: Signal as IO);
	define_symbol!([doc=r#"Signal SIGWINCH."#];SIGWINCH: Signal as WINCH);
	define_symbol!([doc=r#"Signal SIGEMT."#];SIGEMT: Signal as EMT);
	define_symbol!([doc=r#"Signal SIGPWR."#];SIGPWR: Signal as PWR);
}

//BSD.
#[cfg(any(target_os="freebsd",target_os="dragonfly",target_os="openbsd",target_os="netbsd"))]
impl Signal
{
	define_symbol!([doc=r#"Signal SIGIO."#];SIGIO: Signal as IO);
	define_symbol!([doc=r#"Signal SIGWINCH."#];SIGWINCH: Signal as WINCH);
	define_symbol!([doc=r#"Signal SIGEMT."#];SIGEMT: Signal as EMT);
}

//Generic Linux.
#[cfg(any(target_os="linux",target_os="l4re"))]
impl Signal
{
	define_symbol!([doc=r#"Signal SIGIO."#];SIGIO: Signal as IO);
	define_symbol!([doc=r#"Signal SIGWINCH."#];SIGWINCH: Signal as WINCH);
}

//Linux on Mips.
#[cfg(all(any(target_os="linux",target_os="l4re"),target_arch="mips"))]
impl Signal
{
	define_symbol!([doc=r#"Signal SIGPWR."#];SIGPWR: Signal as PWR);
}

//Linux on grab bag of archs.
#[cfg(all(any(target_os="linux",target_os="l4re"),any(
	target_arch="arm",
	target_arch="aarch64",
	target_arch="hexagon",
	target_arch="loongarch64",
	target_arch="m68k",
	target_arch="powerpc",
	target_arch="powerpc64",
	target_arch="riscv32",
	target_arch="riscv64",
	target_arch="s390x",
	target_arch="x86",
	target_arch="x86_64"
)))]
impl Signal
{
	define_symbol!([doc=r#"Signal SIGPWR."#];SIGPWR: Signal as PWR);
	define_symbol!([doc=r#"Signal SIGSTKFLT."#];SIGSTKFLT: Signal as STKFLT);
}

impl Display for Signal
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> { Display::fmt(&self.0, f) }
}

impl Signal
{
	pub fn to_inner(self) -> i32 { self.0 }
}

///Set of signals.
#[derive(Copy,Clone)]
pub struct SignalSet(sigset_t);

impl SignalSet
{
	///Create a new empty set of signals.
	pub fn new() -> SignalSet
	{
		unsafe {
			//sigset_t can be all zeroes.
			let mut set: sigset_t = zeroed();
			//Assumes that set is sigset_t => True by definition.
			libc::sigemptyset(set.pointer_to_mut());
			SignalSet(set)
		}
	}
	///Create set of every signal except the unblockable ones (KILL and STOP).
	pub fn blockables() -> SignalSet
	{
		let mut set = SignalSet::new();
		for i in 0..32 {
			match Signal::new(i) { Signal::KILL|Signal::STOP => (), i => set.add(i) };
		}
		set
	}
	///Get the current blocked signals set.
	pub fn blocked_signals() -> Result<SignalSet, OsError>
	{
		//Do this by blocking empty set of signals, which is no-op, but returns the former mask.
		SignalSet::new().block_signals()
	}
	///Add a signal to the set.
	pub fn add(&mut self, sig: Signal)
	{
		unsafe {
			//Assumes that self.as_raw_mut() is sigset_t => True by definition.
			libc::sigaddset(self.as_raw_mut(), sig.0);
		}
	}
	///Delete a signal from the set.
	pub fn delete(&mut self, sig: Signal)
	{
		unsafe {
			//Assumes that self.as_raw_mut() is sigset_t => True by definition.
			libc::sigdelset(self.as_raw_mut(), sig.0);
		}
	}
	///Is specified signal `sig` in the set?
	pub fn is_in(&self, sig: Signal) -> bool
	{
		unsafe {
			//Assumes that self.as_raw_mut() is sigset_t => True by definition.
			libc::sigismember(self.as_raw(), sig.0) > 0
		}
	}
	pub(crate) fn as_raw(&self) -> *const sigset_t { self.0.pointer_to() }
	pub(crate) fn as_raw_mut(&mut self) -> *mut sigset_t { self.0.pointer_to_mut() }
	///Set the current thread signal mask.
	pub fn set_signals(&self) -> Result<SignalSet, OsError> { self.__signal_mask_op(libc::SIG_SETMASK) }
	///Block specified signals in current thread.
	pub fn block_signals(&self) -> Result<SignalSet, OsError> { self.__signal_mask_op(libc::SIG_BLOCK) }
	///Unblock specified signals in current thread.
	pub fn unblock_signals(&self) -> Result<SignalSet, OsError> { self.__signal_mask_op(libc::SIG_UNBLOCK) }
	fn __signal_mask_op(&self, op: c_int) -> Result<SignalSet, OsError>
	{
		unsafe {
			//sigset_t can be all zeroes.
			let mut oldset: sigset_t = zeroed();
			//Assumes that self.as_raw() is sigset_t => True by definition.
			//Assumes that oldset is sigset_t => True by definition.
			libc::pthread_sigmask(op, self.as_raw(), oldset.pointer_to_mut()).errno()?;
			Ok(SignalSet(oldset))
		}
	}
}

///Raw PID type.
pub type RawPidT = pid_t;

///A process ID.
#[derive(Copy,Clone,Hash,Debug,PartialOrd,Ord,PartialEq,Eq)]
pub struct Pid(RawPidT);

impl Pid
{
	///Create new object describing PID `p`.
	pub fn new(p: RawPidT) -> Pid { Pid(p) }
	//Assume getpid() can not fail.
	///Get PID of current process.
	pub fn current() -> Pid
	{
		//Unconditionally safe.
		Pid(unsafe{libc::getpid()})
	}
	///Call `kill(2)`.
	pub fn kill(self, sig: Signal) -> Result<(), OsError>
	{
		//Unconditionally safe.
		unsafe{libc::kill(self.0, sig.0)}.errno()
	}
	///Call `kill(2)` with `pid=0`.
	pub fn kill_process_group(sig: Signal) -> Result<(), OsError>
	{
		//Unconditionally safe.
		unsafe{libc::kill(0, sig.0)}.errno()
	}
	///Parent returns Ok(Some(pid)) on success, child returns Ok(None) on success. On error, returns Err(err)).
	///Call `fork(2)`.
	///
	///# Safety:
	///
	///Note that no other thread is copied, from child point of view, all other threads have suddenly terminated. 
	pub fn fork() -> Result<Option<Pid>, OsError>
	{
		//Unconditionally safe.
		let r = unsafe{libc::fork()}.errno_val()? as RawPidT;
		Ok(if r > 0 { Some(Pid(r)) } else { None })
	}
	///Get the raw PID value.
	pub fn to_inner(self) -> RawPidT { self.0 }
	///Call `exit(3)`.
	pub fn exit(status: u8) -> !
	{
		//Unconditionally safe.
		unsafe{libc::exit(status as _)}
	}
	///Call `_exit(3)`.
	pub fn _exit(status: u8) -> !
	{
		//Unconditionally safe.
		unsafe{libc::_exit(status as _)}
	}
	///Call `execv(3)`.
	pub fn execv(prog: &[u8], args: &[&[u8]]) -> OsError
	{
		unsafe{Self::__exec(prog, args, |prog, args|{
			//Assumes that prog and args are suitable => Guaranteed by definition of __exec.
			libc::execv(transmute(prog), transmute(args)).errno()
		})}
	}
	///Call `execvp(3)`.
	pub fn execvp(prog: &[u8], args: &[&[u8]]) -> OsError
	{
		unsafe{Self::__exec(prog, args, |prog, args|{
			//Assumes that prog and args are suitable => Guaranteed by definition of __exec.
			libc::execvp(transmute(prog), transmute(args)).errno()
		})}
	}
	//Call `tgkill(2)`.
	pub fn tgkill(&self, tid: Pid, sig: Signal) -> Result<(), OsError>
	{
		crate::sys::tgkill(*self, tid, sig)
	}
	unsafe fn __exec(prog: &[u8], args: &[&[u8]], f: impl FnOnce(*const u8, *const *const u8) ->
		Result<(), OsError>) -> OsError
	{
		//Check no NULs, and calculate needed memory.
		if prog.iter().any(|b|*b == 0) { return OsError::EINVAL; }
		for arg in args.iter() { if arg.iter().any(|b|*b == 0) { return OsError::EINVAL; } }
		let vmem = size_of::<*const c_char>() * (args.len() + 1);
		let mut smem = prog.len() + 1;
		for arg in args.iter() { smem += arg.len() + 1 };
		//Allocate memory.
		let m = unsafe{libc::calloc(vmem + smem, size_of::<u8>()).cast::<u8>()};
		if m.is_null() { return OsError::ENOMEM; }
		//Block because v and s can not be live when memory is freed in error case.
		let r = {
			//Split memory to vector and strings. The block size is size_of(*const char*)*(args.len()+1)+
			//smem, so both slices fit. Additionally assumes that memory is aligned enough for pointers,
			//this is guaranteed by malloc.
			let v = unsafe{from_raw_parts_mut(m.cast::<*const u8>(), args.len()+1)};
			let s = unsafe{from_raw_parts_mut(m.add(vmem).cast::<u8>(), smem)};
			let mut sitr = 0usize;
			//Copy the name and arguments. v is of size args.len()+1, so this all is in range.
			//Additionally s is big enough for all the strings.
			let pp = Self::__copy_stringz(prog, s, &mut sitr);
			for (idx, arg) in args.iter().enumerate() {
				v[idx] = Self::__copy_stringz(arg, s, &mut sitr);
			}
			v[args.len()] = null();
			//Ok, invoke the exec.
			f(pp, v.as_ptr())
		};
		//Failed. Release the memory. m points to beginning of memory block.
		unsafe{libc::free(m.cast());}
		//Use ENOSYS as default error.
		match r { Ok(_) => OsError::ENOSYS, Err(e) => e }
	}
	//Assumes that smem is big enough to append the src, together with terminal NUL.
	fn __copy_stringz(src: &[u8], smem: &mut [u8], sitr: &mut usize) -> *const u8
	{
		let smem = &mut smem[*sitr..];
		//These slices are of the same length by definition.
		(&mut smem[..src.len()]).copy_from_slice(src);
		smem[src.len()] = 0;
		*sitr += src.len() + 1;
		smem.as_ptr()
	}
}

impl Display for Pid
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> { Display::fmt(&self.0, f) }
}

///Process(es) to wait for.
pub trait ToWaitPid
{
	///Get raw PID of process to wait.
	fn to_wait_pid(&self) -> RawPidT;
}


impl ToWaitPid for Pid
{
	fn to_wait_pid(&self) -> RawPidT { self.0 }
}

///Wait any child of this process.
#[derive(Copy,Clone,Debug)]
pub struct WaitAnyChild;

impl ToWaitPid for WaitAnyChild
{
	fn to_wait_pid(&self) -> RawPidT { -1 }
}

///Wait any process in the same process group as this process.
#[derive(Copy,Clone,Debug)]
pub struct WaitProcessGroup;

impl ToWaitPid for WaitProcessGroup
{
	fn to_wait_pid(&self) -> RawPidT { 0 }
}

///Process exit status.
#[derive(Copy,Clone,Debug)]
pub struct WaitStatus(RawPidT, i32);

impl WaitStatus
{
	///Get PID of exited process.
	pub fn pid(self) -> Pid { Pid(self.0) }
	///Get exit status of process (if process exited).
	pub fn exit_status(self) -> Option<i32>
	{
		unsafe {
			//Unconditionally safe.
			if libc::WIFEXITED(self.1) { Some(libc::WEXITSTATUS(self.1)) } else { None }
		}
	}
	///Get the termination signal of process (if process terminated).
	pub fn term_sig(self) -> Option<Signal>
	{
		unsafe {
			//Unconditionally safe.
			if libc::WIFSIGNALED(self.1) { Some(Signal(libc::WTERMSIG(self.1))) } else { None }
		}
	}
	///Call `wait(2)`.
	pub fn wait_any() -> Result<WaitStatus, OsError>
	{
		unsafe {
			let mut status: i32 = 0;
			//Assumes status is c_int => True by definition.
			let r = libc::wait(status.pointer_to_mut()).errno_sval()?;
			Ok(WaitStatus(r, status))
		}
	}
	///Set or clear the `CHILD_SUBREAPER` process flag.
	pub fn set_subreaper(status: bool)-> Result<(), OsError>
	{
		crate::sys::set_subreaper(status)
	}
	///Wait for specified child to exit.
	///
	///On success, returns Ok(Some(status)), if WNOHANG was specified and none is ready, returns Ok(None). If
	///Error occured, returns Err(err).
	pub fn wait_pid(pid: impl ToWaitPid, options: WaitPidFlags) -> Result<Option<WaitStatus>, OsError>
	{
		unsafe {
			let mut status: i32 = 0;
			//Assumes status is c_int => True by definition.
			let r = libc::waitpid(pid.to_wait_pid(), status.pointer_to_mut(), options.0).errno_sval()?;
			Ok(if r == 0 { None } else { Some(WaitStatus(r, status)) })
		}
	}
}

define_bitfield_type!([doc=r#"Flags for `waitpid(2)`."#];WaitPidFlags i32);
impl WaitPidFlags
{
	define_symbol!([doc=r#"Do not wait for process status to change."#];WNOHANG: WaitPidFlags as NOHANG);
}
