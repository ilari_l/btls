//!Routines for serializing and unserializing TLS and ASN.1 DER data.
//!
//!This crate provodes methods for serializing and unserializing TLS structures and ASN.1 DER.
//!
//!The most important items are:
//!
//! * [`Source`](struct.Source.html): A data source.
//! * [`Sink`](trait.Sink.html): A data sink.
#![no_std]
#![deny(unsafe_code)]
#![deny(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]
#[cfg(test)] #[macro_use] extern crate std;

mod sink;
pub use self::sink::*;
mod source;
pub use self::source::*;

#[cfg(test)]
mod test;
