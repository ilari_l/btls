#![allow(unsafe_code)]
use btls_aux_fail::f_return;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_memory::copy_maximum;
use core::fmt::Display;
use core::fmt::Formatter;
use core::fmt::Error as FmtError;
use core::cmp::min;
use core::mem::replace;
use core::slice::from_raw_parts;


#[path="source-obsolete.rs"] mod obsolete;
pub use self::obsolete::*;


///Length of vector.
#[derive(Copy,Clone,Debug)]
pub struct VectorLength(_VectorLength);
#[derive(Copy,Clone,Debug)]
enum _VectorLength
{
	Constant(u32),
	Variable{min: u32, max: u32, granularity: u32},
}

impl VectorLength
{
	///max for 8-bit field.
	pub const MAX_8BIT: u32 = 0xFF;
	///max for 16-bit field.
	pub const MAX_16BIT: u32 = 0xFFFF;
	///max for 24-bit field.
	pub const MAX_24BIT: u32 = 0xFFFFFF;
	///max for 32-bit field.
	pub const MAX_32BIT: u32 = 0xFFFFFFFF;
	///Create a new vector length that is constant.
	pub const fn constant(size: u32) -> VectorLength
	{
		VectorLength(_VectorLength::Constant(size))
	}
	///Create a new vector length that is variable.
	///
	///Any value in given range (inclusive) can be used.
	pub const fn variable(min: u32, max: u32) -> VectorLength
	{
		VectorLength(_VectorLength::Variable{min, max, granularity: 0})
	}
	///Create a new vector length that is variable, with given unit.
	///
	///The value in given range (inclusive) must be multiple of size of given type.
	pub const fn variable_ty<T:Sized>(min: u32, max: u32) -> VectorLength
	{
		use core::mem::size_of;
		VectorLength(_VectorLength::Variable{min, max, granularity: size_of::<T>() as u32})
	}
	///Get number of length bytes.
	///
	///The value returned is 0 for constant lengths. For variable lengths, the length can be at most 4.
	pub fn length_bytes(&self) -> usize
	{
		match self.0 {
			_VectorLength::Constant(_) => 0,
			_VectorLength::Variable{max, ..} => match max {
				0..=255 => 1,
				256..=65535 => 2,
				65536..=16777215 => 3,
				16777216..=4294967295 => 4,
			}
		}
	}
	///Get constant length.
	///
	///Return value is undefined if `length_bytes()` is not 0.
	pub fn constant_length(&self) -> usize
	{
		match self.0 { _VectorLength::Constant(y) => y as usize, _ => 0 }
	}
	///Is number of bytes acceptable?
	pub fn acceptable_length(&self, l: usize) -> bool
	{
		if l > 0xFFFFFFFF { return false; }
		let l = l as u32;
		match self.0 {
			_VectorLength::Constant(y) => l == y,
			_VectorLength::Variable{min, max, granularity} =>
				l >= min && l <= max && (granularity < 2 || l % granularity == 0)
		}
	}
}

impl Display for VectorLength
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self.0 {
			_VectorLength::Constant(y) => write!(f, "={y}"),
			_VectorLength::Variable{min, max, granularity} => if granularity > 1 {
				write!(f, "[{min},{max}]/{granularity}")
			} else {
				write!(f, "[{min},{max}]")
			}
		}
	}
}

///Error type of read2 methods.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
#[non_exhaustive]
pub enum Read2Error
{
	///End of data.
	EndOfData,
	///Insufficient data for length. First is emount requested, second is amount provoded.
	LengthXRun(usize, usize),
	///Insufficient data for value. First is emount requested, second is amount provoded.
	ValueXRun(usize, usize),
	///Value too small. First is amount used, second is lower limit.
	ValueTooSmall(usize,usize),
	///Value too large. First is amount used, second is upper limit.
	ValueTooLarge(usize,usize),
	///Value bad. First is amount used, second is granularity.
	ValueBad(usize,usize),
	///ASN.1 not DER.
	Asn1NotDer,
	///Insufficient data for ASN.1 tag.
	Asn1TagXRun,
	///ASN.1 tag too big.
	Asn1Tag2Big,
	///ASN.1 unexpected tag.
	Asn1UnexpectedTag(Asn1Tag, Asn1Tag),
	///Insufficient data for ASN.1 length.
	Asn1LengthXRun,
	///ASN.1 length too big.
	Asn1Length2Big,
	///ASN.1 BIT STRING empty.
	Asn1NoBitStringDiscard,
	///ASN.1 BIT STRING discard bad.
	Asn1BadBitStringDiscard(u8),
	///ASN.1 BIT STRING padding bad.
	Asn1BadBitStringPadding(u8, u8),
	///ASN.1 empty integer.
	Asn1EmptyInteger,
	///ASN.1 integer negative.
	Asn1IntegerNegative,
	///Expected end of item, got junk.
	ExpectedEnd,
	///Impossible error.
	Impossible,
}

impl Display for Read2Error
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&Read2Error::EndOfData => f.write_str("End of data"),
			&Read2Error::LengthXRun(s,a) => write!(f, "Length is {s} bytes, but only {a} remain"),
			&Read2Error::ValueXRun(s,a) => write!(f, "Value is {s} bytes, but only {a} remain"),
			&Read2Error::ValueTooSmall(s,l) => write!(f, "Value is {s} bytes, but at least {l}"),
			&Read2Error::ValueTooLarge(s,l) => write!(f, "Value is {s} bytes, but at most {l}"),
			&Read2Error::ValueBad(s,l) => write!(f, "Value is {s} bytes, but multiple of {l}"),
			&Read2Error::Asn1NotDer => f.write_str("ASN.1 not DER"),
			&Read2Error::Asn1TagXRun => f.write_str("ASN.1 tag truncated"),
			&Read2Error::Asn1Tag2Big => f.write_str("ASN.1 tag too big"),
			&Read2Error::Asn1UnexpectedTag(s,l) => write!(f, "Unexpected tag {s}, expected {l}"),
			&Read2Error::Asn1LengthXRun => f.write_str("ASN.1 length truncated"),
			&Read2Error::Asn1Length2Big => f.write_str("ASN.1 length too big"),
			&Read2Error::Asn1NoBitStringDiscard => f.write_str("ASN.1 Bit string discard missing"),
			&Read2Error::Asn1BadBitStringDiscard(discard) =>
				write!(f, "ASN.1 Bad BitString discard {discard}"),
			&Read2Error::Asn1BadBitStringPadding(discard, padding) =>
				write!(f, "ASN.1 Bad BitString padding {padding} (discard {discard})"),
			&Read2Error::Asn1EmptyInteger => f.write_str("ASN.1 INTEGER empty"),
			&Read2Error::Asn1IntegerNegative => f.write_str("ASN.1 INTEGER not positive"),
			&Read2Error::ExpectedEnd => f.write_str("Expected end of item, got junk"),
			&Read2Error::Impossible => f.write_str("Impossible error"),
		}
	}
}

///Source that provodes data out of byte slice.
///
///This structure reads byte slice as various data types, in order.
///
///This is especially useful for pasing TLS syntax or ASN.1 DER.
///
///Note that methods enforce ASN.1 DER strictly. Also, the maximum allowed length of any ASN.1 element is restricted
///to 16,777,215 bytes for security reasons (it is hazardous to parse elements any bigger than this).
#[derive(Copy,Clone,Debug)]
pub struct Source<'a>(&'a [u8],usize, usize);


///Parsed ASN.1 DER TLV
///
///This structure represents a parsed ASN.1 TLV.
#[derive(Copy,Clone,Debug)]
pub struct Asn1Tlv<'a>
{
	///The tag of the TLV.
	pub tag: Asn1Tag,
	///`Source` constructed over the value of the TLV.
	pub value: Source<'a>,
	///Raw value of the TLV
	pub raw_p: &'a [u8],
	///Raw representation of the TLV, including the tag and length.
	pub outer: &'a [u8],
}

///ASN.1 tag
#[derive(Copy,Clone,Eq,PartialEq,Debug)]
pub enum Asn1Tag
{
	///Universal tag.
	///
	/// * The 1st parameter is the tag number.
	/// * The 2nd parameter is the constructed flag.
	///
	///# Notes:
	///
	/// * This class contains various well-known tags, including:
	///   * 1: Boolean
	///   * 2: Integer
	///   * 3: Bit String
	///   * 4: Octet String
	///   * 5: NULL
	///   * 6: OID
	///   * 7: Object Descriptor
	///   * 8: Instance Of
	///   * 9: Real
	///   * 10: Enumerated
	///   * 11: Embedded PDV
	///   * 12: UTF-8 String
	///   * 13: Relative OID
	///   * 16: Sequence
	///   * 17: Set
	///   * 18: Numeric String
	///   * 19: Printable String
	///   * 20: Telex String
	///   * 21: Video String
	///   * 22: IA5 String
	///   * 23: UTC Time
	///   * 24: Generalized Time
	///   * 25: Graphics String
	///   * 26: ISO646 String
	///   * 27: General String
	///   * 28: Universal String
	///   * 29: Character String
	///   * 30: BMP String
	Universal(u64, bool),
	///Application-specific tag
	///
	/// * The 1st parameter is the tag number.
	/// * The 2nd parameter is the constructed flag.
	Application(u64, bool),
	///Context-specific tag
	///
	/// * The 1st parameter is the tag number.
	/// * The 2nd parameter is the constructed flag.
	ContextSpecific(u64, bool),
	///Private-use tag
	///
	/// * The 1st parameter is the tag number.
	/// * The 2nd parameter is the constructed flag.
	Private(u64, bool)
}

impl Asn1Tag
{
	///The tag value for ASN.1 BOOLEAN type
	pub const BOOLEAN: Asn1Tag = Asn1Tag::Universal(1, false);
	///The tag value for ASN.1 INTEGER type
	pub const INTEGER: Asn1Tag = Asn1Tag::Universal(2, false);
	///The tag value for ASN.1 BIT STRING type
	pub const BIT_STRING: Asn1Tag = Asn1Tag::Universal(3, false);
	///The tag value for ASN.1 OCTET STRING type
	pub const OCTET_STRING: Asn1Tag = Asn1Tag::Universal(4, false);
	///The tag value for ASN.1 NULL type
	pub const NULL: Asn1Tag = Asn1Tag::Universal(5, false);
	///The tag value for ASN.1 OBJECT IDENTIFIER type
	pub const OID: Asn1Tag = Asn1Tag::Universal(6, false);
	///The tag value for ASN.1 OBJECT DESCRIPTOR type
	pub const OBJECT_DESCRIPTOR: Asn1Tag = Asn1Tag::Universal(7, false);
	///The tag value for ASN.1 INSTANCE_OF type
	pub const INSTANCE_OF: Asn1Tag = Asn1Tag::Universal(8, true);
	///The tag value for ASN.1 REAL type
	pub const REAL: Asn1Tag = Asn1Tag::Universal(9, false);
	///The tag value for ASN.1 ENUMERATED type
	pub const ENUMERATED: Asn1Tag = Asn1Tag::Universal(10, false);
	///The tag value for ASN.1 EMBEDDED PDV type
	pub const EMBEDDED_PDV: Asn1Tag = Asn1Tag::Universal(11, true);
	///The tag value for ASN.1 UTF-8 STRING type
	pub const UTF8_STRING: Asn1Tag = Asn1Tag::Universal(12, false);
	///The tag value for ASN.1 UTF-8 STRING type
	pub const RELATIVE_OID: Asn1Tag = Asn1Tag::Universal(13, false);
	//14 and 15 are unknown.
	///The tag value for ASN.1 SEQUENCE type
	pub const SEQUENCE: Asn1Tag = Asn1Tag::Universal(16, true);
	///The tag value for ASN.1 SET type
	pub const SET: Asn1Tag = Asn1Tag::Universal(17, true);
	///The tag value for ASN.1 NUMERIC STRING type
	pub const NUMERIC_STRING: Asn1Tag = Asn1Tag::Universal(18, true);
	///The tag value for ASN.1 PRINTABLE STRING type
	pub const PRINTABLE_STRING: Asn1Tag = Asn1Tag::Universal(19, false);
	///The tag value for ASN.1 TELEX STRING type
	pub const TELEX_STRING: Asn1Tag = Asn1Tag::Universal(20, false);
	///The tag value for VIDEO STRING type
	pub const VIDEO_STRING: Asn1Tag = Asn1Tag::Universal(21, false);
	///The tag value for ASN.1 IA5 STRING type
	pub const IA5_STRING: Asn1Tag = Asn1Tag::Universal(22, false);
	///The tag value for ASN.1 UTC TIME type
	pub const UTC_TIME: Asn1Tag = Asn1Tag::Universal(23, false);
	///The tag value for ASN.1 GENERALIZED TIME type
	pub const GENERALIZED_TIME: Asn1Tag = Asn1Tag::Universal(24, false);
	///The tag value for ASN.1 GRAPHICS STRING type
	pub const GRAPHICS_STRING: Asn1Tag = Asn1Tag::Universal(25, false);
	///The tag value for ASN.1 ISO646 STRING type
	pub const ISO646_STRING: Asn1Tag = Asn1Tag::Universal(26, false);
	///The tag value for ASN.1 GENERAL STRING type
	pub const GENERAL_STRING: Asn1Tag = Asn1Tag::Universal(27, false);
	///The tag value for ASN.1 UNIVERSAL STRING type
	pub const UNIVERSAL_STRING: Asn1Tag = Asn1Tag::Universal(28, false);
	///The tag value for ASN.1 CHARACTER STRING type
	pub const CHARACTER_STRING: Asn1Tag = Asn1Tag::Universal(29, false);
	///The tag value for ASN.1 BMP STRING type
	pub const BMP_STRING: Asn1Tag = Asn1Tag::Universal(30, false);
	///The tag value for ASN.1 `[0]` constructed
	pub const CTX0_C: Asn1Tag = Asn1Tag::ContextSpecific(0, true);
	///The tag value for ASN.1 `[1]` constructed
	pub const CTX1_C: Asn1Tag = Asn1Tag::ContextSpecific(1, true);
	///The tag value for ASN.1 `[2]` constructed
	pub const CTX2_C: Asn1Tag = Asn1Tag::ContextSpecific(2, true);
	///The tag value for ASN.1 `[3]` constructed
	pub const CTX3_C: Asn1Tag = Asn1Tag::ContextSpecific(3, true);
	///The tag value for ASN.1 `[0]` primitive
	pub const CTX0_P: Asn1Tag = Asn1Tag::ContextSpecific(0, false);
	///The tag value for ASN.1 `[1]` primitive
	pub const CTX1_P: Asn1Tag = Asn1Tag::ContextSpecific(1, false);
	///The tag value for ASN.1 `[2]` primitive
	pub const CTX2_P: Asn1Tag = Asn1Tag::ContextSpecific(2, false);
	///The tag value for ASN.1 `[3]` primitive
	pub const CTX3_P: Asn1Tag = Asn1Tag::ContextSpecific(3, false);
}

impl Display for Asn1Tag
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		match *self {
			Asn1Tag::BOOLEAN => fmt.write_str("BOOLEAN"),
			Asn1Tag::INTEGER => fmt.write_str("INTEGER"),
			Asn1Tag::BIT_STRING => fmt.write_str("BIT STRING"),
			Asn1Tag::OCTET_STRING => fmt.write_str("OCTET STRING"),
			Asn1Tag::NULL => fmt.write_str("NULL"),
			Asn1Tag::OID => fmt.write_str("OID"),
			Asn1Tag::OBJECT_DESCRIPTOR => fmt.write_str("OBJECT DESCRIPTOR"),
			Asn1Tag::INSTANCE_OF => fmt.write_str("INSTANCE OF"),
			Asn1Tag::REAL => fmt.write_str("REAL"),
			Asn1Tag::ENUMERATED => fmt.write_str("ENUMERATED"),
			Asn1Tag::EMBEDDED_PDV => fmt.write_str("EMBEDDED PDV"),
			Asn1Tag::UTF8_STRING => fmt.write_str("UTF8 STRING"),
			Asn1Tag::RELATIVE_OID => fmt.write_str("RELATIVE OID"),
			Asn1Tag::SEQUENCE => fmt.write_str("SEQUENCE"),
			Asn1Tag::SET => fmt.write_str("SET"),
			Asn1Tag::NUMERIC_STRING => fmt.write_str("NUMERIC STRING"),
			Asn1Tag::PRINTABLE_STRING => fmt.write_str("PRINTABLE STRING"),
			Asn1Tag::TELEX_STRING => fmt.write_str("TELEX STRING"),
			Asn1Tag::VIDEO_STRING => fmt.write_str("VIDEO STRING"),
			Asn1Tag::IA5_STRING => fmt.write_str("IA5 STRING"),
			Asn1Tag::UTC_TIME => fmt.write_str("UTC TIME"),
			Asn1Tag::GENERALIZED_TIME => fmt.write_str("GENERALIZED TIME"),
			Asn1Tag::GRAPHICS_STRING => fmt.write_str("GRAPHICS STRING"),
			Asn1Tag::ISO646_STRING => fmt.write_str("ISO646 STRING"),
			Asn1Tag::GENERAL_STRING => fmt.write_str("GENERAL STRING"),
			Asn1Tag::UNIVERSAL_STRING => fmt.write_str("UNIVERSAL STRING"),
			Asn1Tag::CHARACTER_STRING => fmt.write_str("CHARACTER STRING"),
			Asn1Tag::BMP_STRING => fmt.write_str("BMP STRING"),
			Asn1Tag::Universal(8, false) => fmt.write_str("[Invalid tag: INSTANCE OF but primitive???]"),
			Asn1Tag::Universal(11, false) =>
				fmt.write_str("[Invalid tag: EMBEDDED PDV but primitive???]"),
			Asn1Tag::Universal(16, false) => fmt.write_str("[Invalid tag: SEQUENCE but primitive???]"),
			Asn1Tag::Universal(17, false) => fmt.write_str("[Invalid tag: SET but primitive???]"),
			Asn1Tag::Universal(tag, false) => write!(fmt, "[Universal {tag} primitive]"),
			Asn1Tag::Universal(tag, true) => write!(fmt, "[Universal {tag} constructed]"),
			Asn1Tag::Application(tag, false) => write!(fmt, "[Application {tag} primitive]"),
			Asn1Tag::Application(tag, true) => write!(fmt, "[Application {tag} constructed]"),
			Asn1Tag::ContextSpecific(tag, false) => write!(fmt, "[{tag} primitive]"),
			Asn1Tag::ContextSpecific(tag, true) => write!(fmt, "[{tag} constructed]"),
			Asn1Tag::Private(tag, false) => write!(fmt, "[Private {tag} primitive]"),
			Asn1Tag::Private(tag, true) => write!(fmt, "[Private {tag} constructed]"),
		}
	}
}

fn __read_asn1_tag<'a>(x: &'a [u8]) -> Result<(Asn1Tag, usize, &'a [u8]), Read2Error>
{
	let f = *x.get(0).ok_or(Read2Error::Asn1TagXRun)?;
	let mut l = 1usize;
	let v = if f & 31 == 31 {
		let mut v = 0u64;
		loop {
			let b = *x.get(l).ok_or(Read2Error::Asn1TagXRun)?;
			l += 1;
			//Leading zeroes are not allowed, and if v is not 57 bits, it will overflow.
			fail_if!(v == 0 && b == 128, Read2Error::Asn1NotDer);
			fail_if!(v >= 1 << 57, Read2Error::Asn1Tag2Big);
			v = v << 7 | b as u64 & 127;
			if b < 128 { break; }
		}
		//This can only be used for tags >30.
		fail_if!(v < 31, Read2Error::Asn1NotDer);
		v
	} else {
		f as u64 & 31
	};
	//Proof of safety: x.get(l-1) has been executed successfully, so x.len() >= l.
	let tail = unsafe{x.get_unchecked(l..)};
	let constructed = f & 0x20 != 0;
	let v = match f {
		0x00..=0x3F => Asn1Tag::Universal(v, constructed),
		0x40..=0x7F => Asn1Tag::Application(v, constructed),
		0x80..=0xBF => Asn1Tag::ContextSpecific(v, constructed),
		0xC0..=0xFF => Asn1Tag::Private(v, constructed),
	};
	Ok((v, l, tail))
}

fn __read_asn1_length<'a>(x: &'a [u8]) -> Result<(usize, usize, &'a [u8]), Read2Error>
{
	let f = *x.get(0).ok_or(Read2Error::Asn1LengthXRun)?;
	//Proof of safety: x.get(0) succesful, so x.len() >= 1.
	let x = unsafe{x.get_unchecked(1..)};
	let r = match f {
		b@0..=0x7F => (b as usize, 1, x),
		0x80 => fail!(Read2Error::Asn1NotDer),
		0x81 => {
			fail_if!(1 > x.len(), Read2Error::LengthXRun(1, x.len()));
			fail_if!(x[0] < 128, Read2Error::Asn1NotDer);
			let v = x[0] as usize;
			//Proof of safety: x.len() >= 1.
			(v, 2, unsafe{x.get_unchecked(1..)})
		},
		0x82 => {
			fail_if!(2 > x.len(), Read2Error::LengthXRun(2, x.len()));
			fail_if!(x[0] == 0, Read2Error::Asn1NotDer);
			let v = x[0] as usize * 256 + x[1] as usize;
			//Proof of safety: x.len() >= 2.
			(v, 3, unsafe{x.get_unchecked(2..)})
		},
		0x83 => {
			fail_if!(3 > x.len(), Read2Error::LengthXRun(3, x.len()));
			fail_if!(x[0] == 0, Read2Error::Asn1NotDer);
			let v = x[0] as usize * 65536 + x[1] as usize * 256 + x[2] as usize;
			//Proof of safety: x.len() >= 3.
			(v, 4, unsafe{x.get_unchecked(3..)})
		},
		b@0x84..=0xFF => {
			let b = b as usize - 0x80;
			fail_if!(b > x.len(), Read2Error::LengthXRun(b, x.len()));
			fail_if!(x[0] == 0, Read2Error::Asn1NotDer);
			//This is for some length that is too large.
			fail!(Read2Error::Asn1Length2Big);
		}
	};
	Ok(r)
}

fn __read_asn1<'a,T>(array: &'a [u8], ptr: &mut usize, gbase: usize, tchk: Option<Asn1Tag>,
	output: impl Fn(Asn1Tag, &'a [u8], Source<'a>) -> Result<T, Read2Error>) -> Result<T, Read2Error>
{
	fail_if!(*ptr >= array.len(), Read2Error::EndOfData);
	//Proof of safety: *ptr <= array.len().
	let slice = unsafe{array.get_unchecked(*ptr..)};
	let oslice = slice;
	let (gtag, offset1, slice) = __read_asn1_tag(slice)?;
	if let Some(tag) = tchk { fail_if!(gtag != tag, Read2Error::Asn1UnexpectedTag(gtag, tag)); }
	let (length, offset2, slice) = __read_asn1_length(slice)?;
	fail_if!(length > slice.len(), Read2Error::ValueXRun(length, slice.len()));
	//Proof of safety: length <= slice.len().
	let slice = unsafe{slice.get_unchecked(..length)};
	//Special checks for BIT STRING.
	if gtag == Asn1Tag::BIT_STRING {
		//Special checks on bit string. The discard has to be present, has to be at most 7,
		//and corresponding padding bits have to be clear.
		let discard = *slice.get(0).ok_or(Read2Error::Asn1NoBitStringDiscard)?;
		fail_if!(discard > 7, Read2Error::Asn1BadBitStringDiscard(discard));
		//We can only reach here if slice.len() > 0, and discard < 8.
		let pad = slice[slice.len()-1] & ((1 << discard) - 1);
		fail_if!(pad != 0, Read2Error::Asn1BadBitStringPadding(discard, pad));
	} else if gtag == Asn1Tag::INTEGER {
		//Check that integer does not have redundant zero nor is zero length.
		if *slice.get(0).ok_or(Read2Error::Asn1EmptyInteger)? == 0 {
			//Second byte missing is allowed (0). But it can not have high bit unset.
			fail_if!(slice.len() >= 2 && slice[1] < 128, Read2Error::Asn1NotDer);
		}
	} else if gtag == Asn1Tag::OID {
		let (&t,_) = slice.split_last().ok_or(Read2Error::Asn1NotDer)?;
		fail_if!(t >= 128, Read2Error::Asn1NotDer);
	}
	let gpos = gbase + *ptr + offset1 + offset2;
	let length = length + offset1 + offset2;
	//offset1 + offset2 is exactly what is removed by __read_asn1_tag and __read_asn1_length.
	let oslice = unsafe{oslice.get_unchecked(..length)};
	let out = output(gtag, oslice, Source(slice, 0, gpos))?;
	*ptr += length;
	Ok(out)
}

trait ReadBe
{
	type Output;
	//p shall point to at least 1 element, but MAY be unaligned.
	unsafe fn read_be(p: *const Self) -> <Self as ReadBe>::Output;
}

macro_rules! primitive_impl_read_be
{
	($clazz:ident) => {
		impl ReadBe for $clazz
		{
			type Output = $clazz;
			unsafe fn read_be(p: *const Self) -> $clazz { unsafe{p.read_unaligned().to_be()} }
		}
	}
}

primitive_impl_read_be!(u8);
primitive_impl_read_be!(u16);
primitive_impl_read_be!(u32);
primitive_impl_read_be!(u64);

#[repr(C)] struct U24([u8;3]);

impl ReadBe for U24
{
	type Output = u32;
	unsafe fn read_be(p: *const Self) -> u32
	{
		let x = unsafe{p.read_unaligned().0};
		(x[0] as u32)*65536+(x[1] as u32)*256+(x[2] as u32)
	}
}

fn __get_ptr(array: &[u8], ptr: &mut usize, amt: usize) -> Result<*const u8, Read2Error>
{
	fail_if!(*ptr >= array.len(), Read2Error::EndOfData);
	let remaining = array.len() - *ptr;
	fail_if!(amt > remaining, Read2Error::ValueXRun(amt, remaining));
	//array.len() >= *ptr + amt.
	let p = unsafe{array.as_ptr().add(*ptr)};
	*ptr += amt;
	Ok(p)
}

fn __read_integer<T:ReadBe>(array: &[u8], ptr: &mut usize) -> Result<<T as ReadBe>::Output, Read2Error>
{
	let sz = core::mem::size_of::<T>();
	let ptr = __get_ptr(array, ptr, sz)?;
	//__get_ptr() returns pointer valid for at least sz bytes if it succeeds.
	Ok(unsafe{T::read_be(ptr.cast())})
}

fn __read_remaining<'a>(array: &'a [u8], ptr: &mut usize) -> &'a [u8]
{
	let pt = replace(ptr, array.len());
	//The minimum <= self.0.len(), so in range.
	&array[min(pt, array.len())..]
}

impl<'a> Source<'a>
{
	///Create a new source from slice `slice`.
	pub fn new<'b>(slice: &'b [u8]) -> Source<'b> { Source(slice, 0, 0) }
	///Is there more data?
	pub fn more(&self) -> bool { self.1 < self.0.len() }
	///Return the entiere source as slice.
	pub fn as_slice(&self) -> &'a [u8] { self.0 }
	///Read everything remaining as slice.
	///
	///After this method is used, [`more()`](#method.more) will return `false`.
	pub fn read_remaining(&mut self) -> &'a [u8] { __read_remaining(self.0, &mut self.1) }
	///Return the current position in source.
	///
	///Note that only this source is considered, the starting position in supersource is ignored.
	pub fn position(&self) -> usize { self.1 }
	///Return the current global position in source.
	///
	///This method returns the current global read position in the source. The supersources are included.
	pub fn global_position(&self) -> usize { self.2.wrapping_add(self.1) }
}

impl<'a> Source<'a>
{
	fn __cancel_on_error<T,E>(&mut self, f: impl FnOnce(&mut Self) -> Result<T,E>) -> Result<T,E>
	{
		let s = self.1;
		f(self).map_err(|e|{self.1 = s; e})
	}
	///Expect end of item.
	pub fn expect_end(self) -> Result<(), Read2Error>
	{
		fail_if!(self.1 < self.0.len(), Read2Error::ExpectedEnd);
		Ok(())
	}
	///Read an array out of source.
	pub fn array(&mut self, output: &mut [u8]) -> Result<(), Read2Error>
	{
		fail_if!(self.1 >= self.0.len(), Read2Error::EndOfData);
		let src = &self.0[self.1..];
		fail_if!(output.len() > src.len(), Read2Error::ValueXRun(output.len(), src.len()));
		self.1 += copy_maximum(output, src);
		Ok(())
	}
	///Read an source out of source.
	pub fn source(&mut self, length: VectorLength) -> Result<Source<'a>, Read2Error>
	{
		self.__cancel_on_error(|this|{
			let (length, mut min, mut max, granularity) = match length.0 {
				_VectorLength::Constant(x) => (x as usize, x as usize, x as usize, 1),
				_VectorLength::Variable{min, max, granularity} => {
					let t = match max {
						0..=255 => this.u8().map(|x|x as usize),
						256..=65535 => this.u16().map(|x|x as usize),
						65536..=16777215 => this.u24().map(|x|x as usize),
						16777216..=4294967295 => this.u32().map(|x|x as usize),
					};
					match t {
						Ok(x) => (x as usize, min as usize, max as usize,
							granularity as usize),
						//ValueXRun becomes LengthXRun.
						Err(Read2Error::ValueXRun(s,a)) =>
							fail!(Read2Error::LengthXRun(s,a)),
						//All other errors are identity-mapped.
						Err(x) => fail!(x)
					}
				}
			};
			//No need to restore position, as there has only been one call.
			//If some joker sets granularity=0, treat it like granularity=1.
			//Adjust bounds if granularity is nontrivial.
			if granularity > 1 {
				let ming = min % granularity;
				let maxg = max % granularity;
				if ming > 0 { min += granularity - ming; }
				if maxg > 0 { max -= granularity; }
			}
			fail_if!(length < min, Read2Error::ValueTooSmall(length, min));
			fail_if!(length > max, Read2Error::ValueTooLarge(length, max));
			if granularity > 1 {
				fail_if!(length % granularity != 0, Read2Error::ValueBad(length, granularity));
			}
			//We know this.1 < this.0.len().
			let remaining = this.0.len() - this.1;
			fail_if!(length > remaining, Read2Error::ValueXRun(length, remaining));
			//Proof of safety:
			//By definition of this.0.as_ptr() and this.0.len(), this.0.as_ptr() points to this.0.len()
			//readable elements. By above, this.1 < this.0.len() and therefore
			//this.0.as_ptr().add(this.1) points to this.0.len() - this.1 readable elements. By above,
			//remaining = this.0.len() - this.1 and remaining >= length, and therefore
			//this.0.as_ptr().add(this.1) points to length readable elements.
			let input = unsafe{from_raw_parts(this.0.as_ptr().add(this.1), length)};
			let gpos = this.2.wrapping_add(this.1);
			this.1 += length;
			Ok(Source(input, 0, gpos))
		})
	}
	///Read an slice out of source.
	pub fn slice(&mut self, length: VectorLength) -> Result<&'a [u8], Read2Error>
	{
		self.source(length).map(|x|x.0)
	}
	///Read a big-endian 8-bit value out source.
	pub fn u8(&mut self) -> Result<u8, Read2Error>
	{
		__read_integer::<u8>(self.0, &mut self.1)
	}
	///Read a big-endian 16-bit value out source.
	pub fn u16(&mut self) -> Result<u16, Read2Error>
	{
		__read_integer::<u16>(self.0, &mut self.1)
	}
	///Read a big-endian 24-bit value out source.
	pub fn u24(&mut self) -> Result<u32, Read2Error>
	{
		__read_integer::<U24>(self.0, &mut self.1)
	}
	///Read a big-endian 32-bit value out source.
	pub fn u32(&mut self) -> Result<u32, Read2Error>
	{
		__read_integer::<u32>(self.0, &mut self.1)
	}
	///Read a big-endian 64-bit value out source.
	pub fn u64(&mut self) -> Result<u64, Read2Error>
	{
		__read_integer::<u64>(self.0, &mut self.1)
	}
	///Read ASN.1 element with fixed type.
	pub fn asn1(&mut self, tag: Asn1Tag) -> Result<Source<'a>, Read2Error>
	{
		__read_asn1(self.0, &mut self.1, self.2, Some(tag), |_,_,s|Ok(s))
	}
	///Read optional ASN.1 element with fixed type.
	pub fn asn1_optional(&mut self, tag: Asn1Tag) -> Result<Option<Source<'a>>, Read2Error>
	{
		fail_if!(self.1 > self.0.len(), Read2Error::EndOfData);
		if self.1 == self.0.len() { return Ok(None); }		//Clean end.
		let saved = self.1;
		match self.asn1(tag) {
			Ok(x) => Ok(Some(x)),
			Err(Read2Error::Asn1UnexpectedTag(_,_)) => {
				self.1 = saved;		//Cancel read so caller can try again.
				Ok(None)
			},
			Err(e) => Err(e)
		}
	}
	///Read ASN.1 slice with fixed type.
	pub fn asn1_slice(&mut self, tag: Asn1Tag) -> Result<&'a [u8], Read2Error>
	{
		__read_asn1(self.0, &mut self.1, self.2, Some(tag), |_,_,s|Ok(s.0))
	}
	///Read ASN.1 element with any type.
	pub fn asn1_any(&mut self) -> Result<(Asn1Tag, Source<'a>), Read2Error>
	{
		__read_asn1(self.0, &mut self.1, self.2, None, |t,_,s|Ok((t, s)))
	}
	///Read outer ASN.1 element with any type.
	pub fn asn1_any_out(&mut self) -> Result<&'a [u8], Read2Error>
	{
		__read_asn1(self.0, &mut self.1, self.2, None, |_,o,_|Ok(o))
	}
	///Discard ASN.1 element with any type.
	pub fn asn1_discard(&mut self) -> Result<(), Read2Error>
	{
		__read_asn1(self.0, &mut self.1, self.2, None, |_,_,_|Ok(()))
	}
	///Read ASN.1 TLV.
	pub fn asn1_tlv(&mut self) -> Result<Asn1Tlv<'a>, Read2Error>
	{
		__read_asn1(self.0, &mut self.1, self.2, None, |tag,outer,value|Ok(Asn1Tlv{
			tag,
			outer,
			value,
			raw_p: value.0,
		}))
	}
	///Read outer and inner of ASN.1 SEQUENCE.
	pub fn asn1_sequence_inout(&mut self) -> Result<(&'a [u8], Source<'a>), Read2Error>
	{
		__read_asn1(self.0, &mut self.1, self.2, Some(Asn1Tag::SEQUENCE), |_,o,s|Ok((o, s)))
	}
	///Read outer of ASN.1 SEQUENCE.
	pub fn asn1_sequence_out(&mut self) -> Result<&'a [u8], Read2Error>
	{
		__read_asn1(self.0, &mut self.1, self.2, Some(Asn1Tag::SEQUENCE), |_,o,_|Ok(o))
	}
	///Read ASN.1 positive integer.
	pub fn asn1_posint(&mut self) -> Result<&'a [u8], Read2Error>
	{
		__read_asn1(self.0, &mut self.1, self.2, Some(Asn1Tag::INTEGER), |_,_,s|{
			let s = s.0;
			let (&f, t) = s.split_first().ok_or(Read2Error::Asn1IntegerNegative)?;
			if f == 0 && t.len() > 0 { return Ok(t); }
			fail_if!(f == 0 || f >= 128, Read2Error::Asn1IntegerNegative);
			Ok(s)
		})
	}
	///Read optional critical flag.
	pub fn asn1_extension_is_critical(&mut self) -> Result<bool, Read2Error>
	{
		fail_if!(self.1 > self.0.len(), Read2Error::EndOfData);
		if self.1 == self.0.len() { return Ok(false); }		//End of data, not critical.
		//Proof of safety: self.1 <= self.0.len().
		let slice = unsafe{self.0.get_unchecked(self.1..)};
		let tag = *slice.get(0).ok_or(Read2Error::Asn1TagXRun)?;
		if tag != 1 { return Ok(false); }			//Not boolean,  not critical.
		let length = *slice.get(1).ok_or(Read2Error::Asn1LengthXRun)?;
		fail_if!(length != 1, Read2Error::Asn1NotDer);
		let flag = *slice.get(2).ok_or(Read2Error::ValueXRun(1, 0))?;
		fail_if!(flag != 255, Read2Error::Asn1NotDer);		//Can't be 0 because OPTIONAL.
		self.1 += 3;
		Ok(true)
	}
}

///Is the slice `slice` a single ASN.1 structure?
///
///Note that only the top-level ASN.1 TLV is considered.
pub fn is_single_asn1_structure(slice: &[u8]) -> bool
{
	let mut src = Source::new(slice);
	f_return!(src.asn1_discard(), false);
	!src.more()
}


#[cfg(test)] const __MAX8: u32 = 255;
#[cfg(test)] const __MAX16: u32 = 65535;
#[cfg(test)] const __MAX24: u32 = 16777215;


#[test]
fn source_as_slice()
{
	let x = [6,7,3];
	let y = Source::new(&x);
	let z = y.as_slice();
	assert_eq!(&x, z);
}

#[test]
fn source_at_end()
{
	let x = [6,7,3];
	let mut y = Source::new(&x);
	assert!(y.more());
	y.u8().unwrap();
	assert!(y.more());
	y.u8().unwrap();
	assert!(y.more());
	y.u8().unwrap();
	assert!(!y.more());
}

#[test]
fn source_read_slice()
{
	let x = [6, 7, 3, 6, 8, 3, 2, 6, 8, 4, 3, 2, 1, 7, 3, 0];
	let mut y = Source::new(&x);
	assert!(y.more());
	let z1 = y.slice(VectorLength::constant(4)).unwrap();
	assert!(y.more());
	let z2 = y.slice(VectorLength::constant(8)).unwrap();
	assert!(y.more());
	let z3 = y.slice(VectorLength::constant(4)).unwrap();
	assert!(!y.more());
	y.slice(VectorLength::constant(1)).unwrap_err();
	assert_eq!(z1, &x[0..4]);
	assert_eq!(z2, &x[4..12]);
	assert_eq!(z3, &x[12..16]);
}

#[test]
fn source_read_slice_len()
{
	let x1 = [3, 1, 2, 3, 4];
	let x2 = [0, 3, 2, 3, 4, 5];
	let x3 = [0, 0, 3, 3, 4, 5, 6];
	let mut y1 = Source::new(&x1);
	let mut y2 = Source::new(&x2);
	let mut y3 = Source::new(&x3);
	let z1 = y1.slice(VectorLength::variable(0, __MAX8)).unwrap();
	let z2 = y2.slice(VectorLength::variable(0, __MAX16)).unwrap();
	let z3 = y3.slice(VectorLength::variable(0, __MAX24)).unwrap();
	assert_eq!(&z1, &[1,2,3]);
	assert_eq!(&z2, &[2,3,4]);
	assert_eq!(&z3, &[3,4,5]);
	assert_eq!(y1.u8().unwrap(), 4);
	assert_eq!(y2.u8().unwrap(), 5);
	assert_eq!(y3.u8().unwrap(), 6);
}

#[test]
fn source_read_slice_len_long()
{
	let mut x = [0;70000];
	x[0] = 1;
	x[1] = 2;
	x[2] = 3;
	let mut y1 = Source::new(&x);
	let mut y2 = Source::new(&x);
	let mut y3 = Source::new(&x);
	assert_eq!(y1.slice(VectorLength::variable(0, __MAX8)).unwrap().len(), 0x01);
	assert_eq!(y2.slice(VectorLength::variable(0, __MAX16)).unwrap().len(), 0x0102);
	assert_eq!(y3.slice(VectorLength::variable(0, __MAX24)).unwrap().len(), 0x010203);
	assert_eq!(y1.position(), 0x02);
	assert_eq!(y2.position(), 0x0104);
	assert_eq!(y3.position(), 0x010206);
}

#[test]
fn source_read_remaining()
{
	let x = [6, 7, 3, 6, 8, 3, 2, 6, 8, 4, 3, 2, 1, 7, 3, 0];
	let mut y = Source::new(&x);
	assert!(y.more());
	y.slice(VectorLength::constant(5)).unwrap();
	assert!(y.more());
	let z1 = y.read_remaining();
	assert!(!y.more());
	let z2 = y.read_remaining();
	assert_eq!(z1, &x[5..]);
	assert_eq!(z2, &[]);
}

#[test]
fn source_read_source()
{
	let x = [6, 7, 3, 6, 8, 3, 2, 6, 8, 4, 3, 2, 1, 7, 3, 0];
	let mut y = Source::new(&x);
	assert!(y.more());
	let mut z1 = y.source(VectorLength::constant(4)).unwrap();
	assert!(y.more());
	let mut z2 = y.source(VectorLength::constant(8)).unwrap();
	assert!(y.more());
	let mut z3 = y.source(VectorLength::constant(4)).unwrap();
	assert!(!y.more());
	y.source(VectorLength::constant(1)).unwrap_err();
	assert_eq!(z1.read_remaining(), &x[0..4]);
	assert_eq!(z2.read_remaining(), &x[4..12]);
	assert_eq!(z3.read_remaining(), &x[12..16]);
}

#[test]
fn source_read_source_len()
{
	let x1 = [3, 1, 2, 3, 4];
	let x2 = [0, 3, 2, 3, 4, 5];
	let x3 = [0, 0, 3, 3, 4, 5, 6];
	let mut y1 = Source::new(&x1);
	let mut y2 = Source::new(&x2);
	let mut y3 = Source::new(&x3);
	let mut z1 = y1.source(VectorLength::variable(0, __MAX8)).unwrap();
	let mut z2 = y2.source(VectorLength::variable(0, __MAX16)).unwrap();
	let mut z3 = y3.source(VectorLength::variable(0, __MAX24)).unwrap();
	assert_eq!(z1.read_remaining(), &[1,2,3]);
	assert_eq!(z2.read_remaining(), &[2,3,4]);
	assert_eq!(z3.read_remaining(), &[3,4,5]);
	assert_eq!(y1.u8().unwrap(), 4);
	assert_eq!(y2.u8().unwrap(), 5);
	assert_eq!(y3.u8().unwrap(), 6);
}

#[test]
fn source_read_source_len_long()
{
	let mut x = [0;70000];
	x[0] = 1;
	x[1] = 2;
	x[2] = 3;
	let mut y1 = Source::new(&x);
	let mut y2 = Source::new(&x);
	let mut y3 = Source::new(&x);
	assert_eq!(y1.source(VectorLength::variable(0, __MAX8)).unwrap().as_slice().len(), 0x01);
	assert_eq!(y2.source(VectorLength::variable(0, __MAX16)).unwrap().as_slice().len(), 0x0102);
	assert_eq!(y3.source(VectorLength::variable(0, __MAX24)).unwrap().as_slice().len(), 0x010203);
	assert_eq!(y1.position(), 0x02);
	assert_eq!(y2.position(), 0x0104);
	assert_eq!(y3.position(), 0x010206);
}

#[test]
fn source_read_ux()
{
	let x = [
		2, 6,
		7, 3, 2, 6,
		8, 4, 3, 3, 6, 7,
		2, 4, 5, 3, 2, 1, 7, 8,
		7, 3, 7, 2, 1, 3, 5, 6, 8, 4, 4, 7, 8, 6, 4, 0
	];
	let mut y = Source::new(&x);
	assert_eq!(y.u8().unwrap(), 0x02);
	assert_eq!(y.u8().unwrap(), 0x06);
	assert_eq!(y.u16().unwrap(), 0x0703);
	assert_eq!(y.u16().unwrap(), 0x0206);
	assert_eq!(y.u24().unwrap(), 0x080403);
	assert_eq!(y.u24().unwrap(), 0x030607);
	assert_eq!(y.u32().unwrap(), 0x02040503);
	assert_eq!(y.u32().unwrap(), 0x02010708);
	assert_eq!(y.u64().unwrap(), 0x0703070201030506);
	assert_eq!(y.u64().unwrap(), 0x0804040708060400);
	y.u8().unwrap_err();
	y.u16().unwrap_err();
	y.u24().unwrap_err();
	y.u32().unwrap_err();
	y.u64().unwrap_err();
}

#[test]
fn source_read_ux_one()
{
	let x = [0];
	let mut y = Source::new(&x);
	y.u16().unwrap_err();
	y.u24().unwrap_err();
	y.u32().unwrap_err();
	y.u64().unwrap_err();
}

#[test]
fn source_read_ux_one_shy()
{
	let x = [1,2,3,4,5,6,7];
	let mut y = Source::new(&x);
	let mut y1 = Source::new(&x);
	y.u64().unwrap_err();
	y.u32().unwrap();
	y.u32().unwrap_err();
	y.u16().unwrap();
	y.u16().unwrap_err();
	y1.slice(VectorLength::constant(8)).unwrap_err();
	y1.source(VectorLength::constant(8)).unwrap_err();
}

#[test]
fn asn1_basic_oid()
{
	let x = [6, 4, 82, 5, 29, 17];
	let mut y = Source::new(&x);
	let z = y.asn1_tlv().unwrap();
	assert_eq!(z.tag, Asn1Tag::OID);
	assert_eq!(z.value.as_slice(), &x[2..6]);
	assert_eq!(z.raw_p, &x[2..6]);
}

#[test]
fn asn1_basic_sequence()
{
	let x = [0x30, 4, 82, 5, 29, 17];
	let mut y = Source::new(&x);
	let z = y.asn1_tlv().unwrap();
	assert_eq!(z.tag, Asn1Tag::SEQUENCE);
	assert_eq!(z.value.as_slice(), &x[2..6]);
	assert_eq!(z.raw_p, &x[2..6]);
}

#[test]
fn asn1_one_shy()
{
	let x = [0x30, 4, 82, 5, 29];
	let mut y = Source::new(&x);
	assert_eq!(y.asn1_any().unwrap_err(), Read2Error::ValueXRun(4, 3));
}

#[test]
fn asn1_extended_tag()
{
	let x = [0xFF, 0x1F, 4, 0, 5, 29, 17, 0];
	let mut y = Source::new(&x);
	let (ztag, zvalue) = y.asn1_any().unwrap();
	assert_eq!(ztag, Asn1Tag::Private(31, true));
	assert_eq!(zvalue.as_slice(), &x[3..7]);
}

#[test]
fn asn1_extended_tag_under()
{
	let x = [0xFF, 0x1E, 4, 0, 5, 29, 17, 0];
	let mut y = Source::new(&x);
	assert_eq!(y.asn1_any().unwrap_err(), Read2Error::Asn1NotDer);
}

#[test]
fn asn1_extended_tag_2()
{
	let x = [0x9F, 0xFE, 0x7F, 4, 0, 5, 29, 17, 0];
	let mut y = Source::new(&x);
	let (ztag, zvalue) = y.asn1_any().unwrap();
	assert_eq!(ztag, Asn1Tag::ContextSpecific(16255, false));
	assert_eq!(zvalue.as_slice(), &x[4..8]);
}

#[test]
fn asn1_extended_tag_2_0x81()
{
	let x = [0x9F, 0x81, 0x00, 4, 0, 5, 29, 17, 0];
	let mut y = Source::new(&x);
	let (ztag, zvalue) = y.asn1_any().unwrap();
	assert_eq!(ztag, Asn1Tag::ContextSpecific(128, false));
	assert_eq!(zvalue.as_slice(), &x[4..8]);
}

#[test]
fn asn1_extended_tag_2_under()
{
	let x = [0xFF, 0x80, 0x7F, 4, 0, 5, 29, 17, 0];
	let mut y = Source::new(&x);
	assert_eq!(y.asn1_any().unwrap_err(), Read2Error::Asn1NotDer);
}

#[test]
fn asn1_extended_tag_3()
{
	let x = [0x9F, 0xFD, 0xFE, 0x7F, 4, 0, 5, 29, 17, 0];
	let mut y = Source::new(&x);
	let (ztag, zvalue) = y.asn1_any().unwrap();
	assert_eq!(ztag, Asn1Tag::ContextSpecific(2064255, false));
	assert_eq!(zvalue.as_slice(), &x[5..9]);
}

#[test]
fn asn1_extended_tag_3_0x8180()
{
	let x = [0x9F, 0x81, 0x80, 0x00, 4, 0, 5, 29, 17, 0];
	let mut y = Source::new(&x);
	let (ztag, zvalue) = y.asn1_any().unwrap();
	assert_eq!(ztag, Asn1Tag::ContextSpecific(16384, false));
	assert_eq!(zvalue.as_slice(), &x[5..9]);
}

#[test]
fn asn1_extended_tag_3_under()
{
	let x = [0xFF, 0x80, 0xFF, 0x7F, 4, 0, 5, 29, 17, 0];
	let mut y = Source::new(&x);
	assert_eq!(y.asn1_any().unwrap_err(), Read2Error::Asn1NotDer);
}

#[test]
fn asn1_extended_tag_max()
{
	let x = [0xDF, 0x81, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F, 4, 0, 5, 29, 17, 0];
	let mut y = Source::new(&x);
	let (ztag, zvalue) = y.asn1_any().unwrap();
	assert_eq!(ztag, Asn1Tag::Private(0xFFFFFFFFFFFFFFFF, false));
	assert_eq!(zvalue.as_slice(), &x[12..16]);
}

#[test]
fn asn1_extended_tag_over()
{
	let x = [0xDF, 0x82, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x00, 4, 0, 5, 29, 17, 0];
	let mut y = Source::new(&x);
	assert_eq!(y.asn1_any().unwrap_err(), Read2Error::Asn1Tag2Big);
}


#[test]
fn asn1_taglen_1()
{
	let mut x = [0;128+3];
	x[0] = 0x30;
	x[1] = 0x81;
	x[2] = 0x80;
	x[3] = 0x55;
	x[2+128] = 0xAA;
	let mut y = Source::new(&x);
	let (ztag, zvalue) = y.asn1_any().unwrap();
	assert_eq!(ztag, Asn1Tag::SEQUENCE);
	assert_eq!(zvalue.as_slice().len(), 0x80);
	assert_eq!(zvalue.as_slice(), &x[3..]);
}

#[test]
fn asn1_taglen_1_under()
{
	let mut x = [0;127+3];
	x[0] = 0x30;
	x[1] = 0x81;
	x[2] = 0x7F;
	x[3] = 0x55;
	x[2+127] = 0xAA;
	let mut y = Source::new(&x);
	assert_eq!(y.asn1_any().unwrap_err(), Read2Error::Asn1NotDer);
}

#[test]
fn asn1_taglen_2()
{
	let mut x = [0;259+4];
	x[0] = 0x30;
	x[1] = 0x82;
	x[2] = 0x01;
	x[3] = 0x03;
	x[4] = 0x55;
	x[3+259] = 0xAA;
	let mut y = Source::new(&x);
	let (ztag, zvalue) = y.asn1_any().unwrap();
	assert_eq!(ztag, Asn1Tag::SEQUENCE);
	assert_eq!(zvalue.as_slice().len(), 0x0103);
	assert_eq!(zvalue.as_slice(), &x[4..]);
}

#[test]
fn asn1_taglen_2_under()
{
	let mut x = [0;255+4];
	x[0] = 0x30;
	x[1] = 0x82;
	x[2] = 0x00;
	x[3] = 0xFF;
	x[4] = 0x55;
	x[3+255] = 0xAA;
	let mut y = Source::new(&x);
	assert_eq!(y.asn1_any().unwrap_err(), Read2Error::Asn1NotDer);
}

#[test]
fn asn1_taglen_3()
{
	let mut x = [0;66052+5];
	x[0] = 0x30;
	x[1] = 0x83;
	x[2] = 0x01;
	x[3] = 0x02;
	x[4] = 0x04;
	x[5] = 0x55;
	x[4+66052] = 0xAA;
	let mut y = Source::new(&x);
	let (ztag, zvalue) = y.asn1_any().unwrap();
	assert_eq!(ztag, Asn1Tag::SEQUENCE);
	assert_eq!(zvalue.as_slice().len(), 0x010204);
	assert_eq!(zvalue.as_slice(), &x[5..]);
}

#[test]
fn asn1_taglen_3_under()
{
	let mut x = [0;65535+5];
	x[0] = 0x30;
	x[1] = 0x83;
	x[2] = 0x00;
	x[3] = 0xFF;
	x[4] = 0xFF;
	x[5] = 0x55;
	x[4+65535] = 0xAA;
	let mut y = Source::new(&x);
	assert_eq!(y.asn1_any().unwrap_err(), Read2Error::Asn1NotDer);
}

#[test]
fn asn1_taglen_4()
{
	use std::vec::Vec;
	let mut x = Vec::with_capacity(0x01020304+6);
	x.push(0x30);
	x.push(0x84);
	x.push(0x01);
	x.push(0x02);
	x.push(0x03);
	x.push(0x04);
	for i in 0..0x01020304 { x.push(i as u8); }
	let mut y = Source::new(&x);
	assert_eq!(y.asn1_any().unwrap_err(), Read2Error::Asn1Length2Big);
}

#[test]
fn asn1_taglen_0()
{
	let x = [0x30, 0x80];
	let mut y = Source::new(&x);
	assert_eq!(y.asn1_any().unwrap_err(), Read2Error::Asn1NotDer);
}

#[test]
fn asn1_integer_bad()
{
	let x = [0x02, 0x02, 0x00, 0x7F];
	let mut y = Source::new(&x);
	assert_eq!(y.asn1_any().unwrap_err(), Read2Error::Asn1NotDer);
}

#[test]
fn asn1_integer_bad2()
{
	let x = [0x02, 0x02, 0x00, 0x00, 0x80];
	let mut y = Source::new(&x);
	assert_eq!(y.asn1_any().unwrap_err(), Read2Error::Asn1NotDer);
}

#[test]
fn asn1_integers_edge()
{
	let x = [0x02, 0x01, 0x7F, 0x02, 0x02, 0x00, 0x80, 0x02, 0x02, 0x00, 0xFF, 0x02, 0x02, 0x01, 0x00,
		0x02, 0x01, 0x80, 0x02, 0x01, 0xFF];
	let mut y = Source::new(&x);
	assert_eq!(y.asn1_slice(Asn1Tag::INTEGER).unwrap(), &[0x7F]);
	assert_eq!(y.asn1_slice(Asn1Tag::INTEGER).unwrap(), &[0x00, 0x80]);
	assert_eq!(y.asn1_slice(Asn1Tag::INTEGER).unwrap(), &[0x00, 0xFF]);
	assert_eq!(y.asn1_slice(Asn1Tag::INTEGER).unwrap(), &[0x01, 0x00]);
	assert_eq!(y.asn1_slice(Asn1Tag::INTEGER).unwrap(), &[0x80]);
	assert_eq!(y.asn1_slice(Asn1Tag::INTEGER).unwrap(), &[0xFF]);
}

#[test]
fn asn1_integers_positive()
{
	let x = [0x02, 0x01, 0x00, 0x02, 0x01, 0x01, 0x02, 0x01, 0x7F, 0x02, 0x01, 0x80, 0x02, 0x01, 0xFF,
		0x02, 0x02, 0x00, 0x80, 0x02, 0x02, 0x00, 0xFF, 0x02, 0x02, 0x01, 0x00];
	let mut y = Source::new(&x);
	assert_eq!(y.asn1_posint().unwrap_err(), Read2Error::Asn1IntegerNegative);
	assert_eq!(y.asn1_slice(Asn1Tag::INTEGER).unwrap(), &[0x00]);
	assert_eq!(y.asn1_posint().unwrap(), &[0x01]);
	assert_eq!(y.asn1_posint().unwrap(), &[0x7F]);
	assert_eq!(y.asn1_posint().unwrap_err(), Read2Error::Asn1IntegerNegative);
	assert_eq!(y.asn1_slice(Asn1Tag::INTEGER).unwrap(), &[0x80]);
	assert_eq!(y.asn1_posint().unwrap_err(), Read2Error::Asn1IntegerNegative);
	assert_eq!(y.asn1_slice(Asn1Tag::INTEGER).unwrap(), &[0xFF]);
	assert_eq!(y.asn1_posint().unwrap(), &[0x80]);
	assert_eq!(y.asn1_posint().unwrap(), &[0xFF]);
	assert_eq!(y.asn1_posint().unwrap(), &[0x01, 0x00]);
}

#[test]
fn global_offset()
{
	let x = [0x30, 0x03, 0x30, 0x00, 0x02];
	let mut y = Source::new(&x);
	let mut z = y.asn1(Asn1Tag::SEQUENCE).unwrap();
	z.asn1(Asn1Tag::SEQUENCE).unwrap();
	assert_eq!(z.position(), 2);
	assert_eq!(z.global_position(), 4);
	assert!(z.asn1(Asn1Tag::SEQUENCE).is_err());
}

#[test]
fn global_offset_nesting()
{
	let x = [2,1,5,5,1,6,4,1,42];
	let mut y = Source::new(&x);
	y.slice(VectorLength::variable(0,255)).unwrap();
	let mut z = y.source(VectorLength::variable(0,255)).unwrap();
	assert!(z.position() == 0);
	assert!(z.global_position() == 4);
	z.slice(VectorLength::variable(0,255)).unwrap();
	let mut w = z.asn1(Asn1Tag::OCTET_STRING).unwrap();
	assert!(w.position() == 0);
println!("gpos={gpos}", gpos=w.global_position());
	assert!(w.global_position() == 8);
	assert!(w.read_remaining() == &[42]);
	assert!(w.position() == 1);
	assert!(w.global_position() == 9);
}
