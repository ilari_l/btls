#![allow(deprecated)]
#![allow(missing_docs)]
use super::__read_asn1;
use super::__read_integer;
use super::Asn1Tag;
use super::Asn1Tlv;
use super::Read2Error;
use super::Source;
use super::U24;
use btls_aux_fail::dtry;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::ResultExt;
use core::cmp::min;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::mem::replace;
use core::ops::Range;
use core::ops::RangeTo;


#[deprecated(since="1.5.0", note="Use VectorLength instead")]
pub const MAX8: usize = 255;
#[deprecated(since="1.5.0", note="Use VectorLength instead")]
pub const MAX16: usize = 65535;
#[deprecated(since="1.5.0", note="Use VectorLength instead")]
pub const MAX24: usize = 16777215;

#[deprecated(since="1.5.0", note="Use VectorLength instead")]
pub trait LengthTrait
{
	fn get_length<'a>(&self, src: &mut Source<'a>) -> Result<usize, ()>;
	fn get_bytes(&self) -> usize;
}

impl LengthTrait for usize
{
	fn get_length<'a>(&self, _: &mut Source<'a>) -> Result<usize, ()> { Ok(*self) }
	fn get_bytes(&self) -> usize { 0 }
}

impl LengthTrait for Range<usize>
{
	fn get_length<'a>(&self, src: &mut Source<'a>) -> Result<usize, ()>
	{
		range_get_length_common(self.start, self.end, src)
	}
	fn get_bytes(&self) -> usize { range_get_bytes_common(self.end) }
}

impl LengthTrait for RangeTo<usize>
{
	fn get_length<'a>(&self, src: &mut Source<'a>) -> Result<usize, ()>
	{
		range_get_length_common(0, self.end, src)
	}
	fn get_bytes(&self) -> usize { range_get_bytes_common(self.end) }
}


fn range_get_length_common<'a>(start: usize, end: usize, src: &mut Source<'a>) -> Result<usize, ()>
{
	let len = match range_get_bytes_common(end) {
		1 => dtry!(src.u8()) as usize,
		2 => dtry!(src.u16()) as usize,
		3 => dtry!(src.u24()) as usize,
		_ => fail!(())
	};
	fail_if!(len < start || len > end, ());
	Ok(len)
}

fn range_get_bytes_common(end: usize) -> usize
{
	match end {
		0..=255 => 1,
		256..=65535 => 2,
		65536..=16777215 => 3,
		_ => 0
	}
}


#[deprecated(since="1.5.0", note="Use Read2Error instead")]
#[derive(Copy,Clone,Eq,PartialEq,Debug)]
#[non_exhaustive]
pub enum Asn1Error
{
	#[doc(hidden)]
	Truncated,
	#[doc(hidden)]
	NotDer,
	#[doc(hidden)]
	LengthTooBig,
	#[doc(hidden)]
	TagTooBig,
	#[doc(hidden)]
	UnexpectedType(Asn1Tag, Asn1Tag),
	#[doc(hidden)]
	ReadZero,
	#[doc(hidden)]
	NoBitStringDiscard,
	#[doc(hidden)]
	BadBitStringDiscard(u8),
	#[doc(hidden)]
	BadBitStringPadding(u8, u8),
}

impl Display for Asn1Error
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::Asn1Error::*;
		match self {
			&Truncated => fmt.write_str("Truncated"),
			&NotDer => fmt.write_str("Not valid DER"),
			&LengthTooBig => fmt.write_str("Value too big"),
			&TagTooBig => fmt.write_str("Tag too big"),
			&UnexpectedType(got,expected) =>
				write!(fmt, "Unexpected type (got {got}, epxpected {expected})"),
			&ReadZero => fmt.write_str("Internal error: Array element empty"),
			&NoBitStringDiscard => fmt.write_str("Bit string discard missing"),
			&BadBitStringDiscard(discard) => write!(fmt, "Bad BitString discard {discard}"),
			&BadBitStringPadding(discard, pad) =>
				write!(fmt, "Bad BitString padding {pad} (discard {discard})"),
		}
	}
}

#[deprecated(since="1.5.0", note="Use asn1_*() instead")]
pub trait Asn1TagMatch
{
	fn tmatch(&self, val: Asn1Tag) -> bool;
	fn expected(&self) -> Asn1Tag;
}

impl Asn1TagMatch for Asn1Tag
{
	fn tmatch(&self, val: Asn1Tag) -> bool { *self == val }
	fn expected(&self) -> Asn1Tag { *self }
}

#[deprecated(since="1.5.0", note="Use asn1_*() instead")]
#[derive(Copy,Clone,Debug)]
pub struct AnyAsn1Tag;


impl Asn1TagMatch for AnyAsn1Tag
{
	fn tmatch(&self, _: Asn1Tag) -> bool { true }
	fn expected(&self) -> Asn1Tag { Asn1Tag::Universal(0, false) }		//Never called.
}

impl Read2Error
{
	fn to_asn1_error(self) -> Asn1Error
	{
		match self {
			Read2Error::EndOfData => Asn1Error::Truncated,
			Read2Error::LengthXRun(_,_) => Asn1Error::Truncated,
			Read2Error::ValueXRun(_,_) => Asn1Error::Truncated,
			Read2Error::Asn1NotDer => Asn1Error::NotDer,
			Read2Error::Asn1TagXRun => Asn1Error::Truncated,
			Read2Error::Asn1Tag2Big => Asn1Error::TagTooBig,
			Read2Error::Asn1UnexpectedTag(s,l) => Asn1Error::UnexpectedType(s,l),
			Read2Error::Asn1LengthXRun => Asn1Error::Truncated,
			Read2Error::Asn1Length2Big => Asn1Error::LengthTooBig,
			Read2Error::Asn1NoBitStringDiscard => Asn1Error::NoBitStringDiscard,
			Read2Error::Asn1BadBitStringDiscard(x) => Asn1Error::BadBitStringDiscard(x),
			Read2Error::Asn1BadBitStringPadding(x, y) => Asn1Error::BadBitStringPadding(x,y),
			Read2Error::Asn1EmptyInteger => Asn1Error::NotDer,
			Read2Error::Asn1IntegerNegative => Asn1Error::NotDer,
			//Can these three be generated for ASN.1 anyway???
			Read2Error::ValueTooSmall(_,_) => Asn1Error::NoBitStringDiscard,
			Read2Error::ValueTooLarge(_,_) => Asn1Error::NoBitStringDiscard,
			Read2Error::ValueBad(_,_) => Asn1Error::NoBitStringDiscard,
			Read2Error::ExpectedEnd => Asn1Error::NoBitStringDiscard,
			Read2Error::Impossible => Asn1Error::NoBitStringDiscard,
		}
	}
}

impl<'a> Source<'a>
{
	#[deprecated(since="1.3.0", note="Use .more() or .assert_at_end() instead")]
	pub fn at_end(&self) -> bool { !self.more() }
	#[deprecated(since="1.5.0", note="Use .expect_end instead")]
	pub fn assert_at_end<E>(self, err: impl FnOnce() -> E) -> Result<(), E>
	{
		if self.more() { return Err(err()); }
		Ok(())
	}
	#[deprecated(since="1.5.0", note="No replacement")]
	pub fn slice_marker(&self) -> SliceMarker { SliceMarker(self.1) }
	#[deprecated(since="1.5.0", note="Use .array() instead")]
	pub fn read_array<E:Sized+Clone>(&mut self, buf: &mut [u8], err: E) -> Result<(), E>
	{
		let base = self.1;
		let eptr = base.checked_add(buf.len()).ok_or(err.clone())?;
		fail_if!(eptr > self.0.len(), err);
		//self.0.len() >= eptr >= base, so the slice is in bound. Additionally eptr - base = buf.len(),
		//so the both sides of copy are of equal length.
		buf.copy_from_slice(&self.0[base..eptr]);
		self.1 = eptr;
		Ok(())
	}
	#[deprecated(since="1.5.0", note="Use .slice() instead")]
	pub fn read_slice<L:LengthTrait,E:Sized+Clone>(&mut self, len: L, err: E) -> Result<&'a [u8], E>
	{
		self.read_source(len, err).map(|x|x.0)
	}
	#[deprecated(since="1.5.0", note="Use .source() instead")]
	pub fn read_source<L:LengthTrait,E:Sized+Clone>(&mut self, len: L, err: E) -> Result<Source<'a>, E>
	{
		//Save position across call, but use new position in calculations. This is in case some call
		//fails: We want to keep position unchanged.
		let saved = self.1;
		let len = len.get_length(self).map_err(|_|err.clone())?;
		let base = replace(&mut self.1, saved);
		let eptr = base.checked_add(len).ok_or(err.clone())?;
		fail_if!(eptr > self.0.len() || base > eptr, err);
		//self.0.len() >= eptr >= base, so the slice is in bound.
		let gpos = self.2.wrapping_add(base);
		let res = &self.0[base..eptr];
		self.1 = eptr;
		Ok(Source(res, 0, gpos))
	}
	#[deprecated(since="1.5.0", note="Use .source() and loop instead")]
	pub fn read_source_fn<F, T: Sized, E: Sized+Clone, L:LengthTrait>(&mut self, length: L, repeat: bool,
		mut closure: F, readfail: E, blankret: T) -> Result<T, E> where F: FnMut(&mut Source<'a>) ->
		Result<T,E>
	{
		let mut tmp = self.read_source(length, readfail.clone())?;
		if repeat {
			let mut retval = Ok(blankret);
			while tmp.more() { retval = Ok(closure(&mut tmp)?); }
			retval
		} else {
			closure(&mut tmp)
		}
	}
	#[deprecated(since="1.5.0", note="Use .u8() instead")]
	pub fn read_u8<E:Sized+Clone>(&mut self, err: E) -> Result<u8, E>
	{
		__read_integer::<u8>(self.0, &mut self.1).set_err(err)
	}
	#[deprecated(since="1.5.0", note="Use .u16() instead")]
	pub fn read_u16<E:Sized+Clone>(&mut self, err: E) -> Result<u16, E>
	{
		__read_integer::<u16>(self.0, &mut self.1).set_err(err)
	}
	#[deprecated(since="1.5.0", note="Use .u24() instead")]
	pub fn read_u24<E:Sized+Clone>(&mut self, err: E) -> Result<u32, E>
	{
		__read_integer::<U24>(self.0, &mut self.1).set_err(err)
	}
	#[deprecated(since="1.5.0", note="Use .u32() instead")]
	pub fn read_u32<E:Sized+Clone>(&mut self, err: E) -> Result<u32, E>
	{
		__read_integer::<u32>(self.0, &mut self.1).set_err(err)
	}
	#[deprecated(since="1.5.0", note="Use .u64() instead")]
	pub fn read_u64<E:Sized+Clone>(&mut self, err: E) -> Result<u64, E>
	{
		__read_integer::<u64>(self.0, &mut self.1).set_err(err)
	}
	#[deprecated(since="1.5.0", note="Use .asn1_discard() instead")]
	pub fn discard_asn1_tlv(&mut self) -> Result<(), Asn1Error>
	{
		__read_asn1(self.0, &mut self.1, self.2, None, |_,_,_|Ok(())).map_err(|x|x.to_asn1_error())
	}
	#[deprecated(since="1.5.0", note="Use .asn1_*() instead")]
	pub fn read_asn1_value<Tag:Asn1TagMatch, E, EM>(&mut self, xtag: Tag, maperr: EM) ->
		Result<Asn1Tlv<'a>, E> where EM: Fn(Asn1Error) -> E
	{
		__read_asn1(self.0, &mut self.1, self.2, None, |tag,outer,value|{
			fail_if!(!xtag.tmatch(tag), Read2Error::Asn1UnexpectedTag(tag, xtag.expected()));
			Ok(Asn1Tlv{
				tag,
				outer,
				value,
				raw_p: value.0,
			})
		}).map_err(|e|maperr(e.to_asn1_error()))
	}
	#[deprecated(since="1.5.0", note="Use .asn1_optional() instead")]
	pub fn read_asn1_value_opt_fn<T, E, Fc, Fe>(&mut self, tag: Asn1Tag, wrong_type: T, mut cb: Fc,
		errmap: Fe) -> Result<T, E> where Fc: FnMut(Source<'a>) -> Result<T, E>, Fe: Fn(Asn1Error) -> E
	{
		if self.1 >= self.0.len() { return Ok(wrong_type) };	//At end.
		let r = __read_asn1(self.0, &mut self.1, self.2, Some(tag), |_,_,v|Ok(v));
		match r {
			Ok(x) => cb(x),
			Err(Read2Error::Asn1UnexpectedTag(_,_)) => Ok(wrong_type),
			Err(e) => Err(errmap(e.to_asn1_error())),
		}
	}
}

#[deprecated(since="1.5.0", note="No replacment")]
pub struct SliceMarker(usize);

impl SliceMarker
{
	#[deprecated(since="1.5.0", note="No replacment")]
	pub fn commit<'a>(self, src: &Source<'a>) -> &'a [u8]
	{
		let max = min(src.1, src.0.len());
		let min = min(self.0, max);
		//min <= max <= src.0.len(), so this is always in-range.
		&src.0[min..max]
	}
}

impl<'a> Asn1Tlv<'a>
{
	#[deprecated(since="1.5.0", note="Use straight-line code instead")]
	pub fn call<T:Sized, F>(&self, mut closure: F) -> T where F: FnMut(&mut Source<'a>) -> T
	{
		closure(&mut self.value.clone())
	}
	#[deprecated(since="1.5.0", note="Use explicit loop instead")]
	pub fn fold<T:Sized, E:Sized, F>(&self, initial: T, mut closure: F) -> Result<T, E>
		where F: FnMut(&mut Source<'a>, T) -> Result<T, E>
	{
		let mut itr = self.value.clone();
		let mut state: T = initial;
		while itr.more() { state = closure(&mut itr, state)?; }
		Ok(state)
	}
}

#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_BOOLEAN: Asn1Tag = Asn1Tag::BOOLEAN;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_INTEGER: Asn1Tag = Asn1Tag::INTEGER;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_BIT_STRING: Asn1Tag = Asn1Tag::BIT_STRING;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_OCTET_STRING: Asn1Tag = Asn1Tag::OCTET_STRING;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_NULL: Asn1Tag = Asn1Tag::NULL;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_OID: Asn1Tag = Asn1Tag::OID;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_OBJECT_DESCRIPTOR: Asn1Tag = Asn1Tag::OBJECT_DESCRIPTOR;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_INSTANCE_OF: Asn1Tag = Asn1Tag::INSTANCE_OF;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_REAL: Asn1Tag = Asn1Tag::REAL;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_ENUMERATED: Asn1Tag = Asn1Tag::ENUMERATED;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_EMBEDDED_PDV: Asn1Tag = Asn1Tag::EMBEDDED_PDV;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_UTF8_STRING: Asn1Tag = Asn1Tag::UTF8_STRING;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_RELATIVE_OID: Asn1Tag = Asn1Tag::RELATIVE_OID;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_SEQUENCE: Asn1Tag = Asn1Tag::SEQUENCE;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_SET: Asn1Tag = Asn1Tag::SET;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_NUMERIC_STRING: Asn1Tag = Asn1Tag::NUMERIC_STRING;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_PRINTABLE_STRING: Asn1Tag = Asn1Tag::PRINTABLE_STRING;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_TELEX_STRING: Asn1Tag = Asn1Tag::TELEX_STRING;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_VIDEO_STRING: Asn1Tag = Asn1Tag::VIDEO_STRING;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_IA5_STRING: Asn1Tag = Asn1Tag::IA5_STRING;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_UTC_TIME: Asn1Tag = Asn1Tag::UTC_TIME;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_GENERALIZED_TIME: Asn1Tag = Asn1Tag::GENERALIZED_TIME;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_GRAPHICS_STRING: Asn1Tag = Asn1Tag::GRAPHICS_STRING;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_ISO646_STRING: Asn1Tag = Asn1Tag::ISO646_STRING;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_GENERAL_STRING: Asn1Tag = Asn1Tag::GENERAL_STRING;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_UNIVERSAL_STRING: Asn1Tag = Asn1Tag::UNIVERSAL_STRING;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_CHARACTER_STRING: Asn1Tag = Asn1Tag::CHARACTER_STRING;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_BMP_STRING: Asn1Tag = Asn1Tag::BMP_STRING;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_CTX0_C: Asn1Tag = Asn1Tag::CTX0_C;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_CTX1_C: Asn1Tag = Asn1Tag::CTX1_C;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_CTX2_C: Asn1Tag = Asn1Tag::CTX2_C;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_CTX3_C: Asn1Tag = Asn1Tag::CTX3_C;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_CTX0_P: Asn1Tag = Asn1Tag::CTX0_P;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_CTX1_P: Asn1Tag = Asn1Tag::CTX1_P;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_CTX2_P: Asn1Tag = Asn1Tag::CTX2_P;
#[deprecated(since="1.5.0", note="Use Asn1Tag associated consts")]
pub const ASN1_CTX3_P: Asn1Tag = Asn1Tag::CTX3_P;


#[test]
fn source_read_source_fn()
{
	let mut x = [0;70000];
	x[0] = 1;
	x[1] = 2;
	x[2] = 3;
	let mut y1 = Source::new(&x);
	let mut y2 = Source::new(&x);
	let mut y3 = Source::new(&x);
	y1.read_source_fn(..MAX8, false, |x|{assert_eq!(x.as_slice().len(), 0x01); Ok(())}, (), ()).unwrap();
	y2.read_source_fn(..MAX16, false, |x|{assert_eq!(x.as_slice().len(), 0x0102); Ok(())}, (), ()).unwrap();
	y3.read_source_fn(..MAX24, false, |x|{assert_eq!(x.as_slice().len(), 0x010203); Ok(())}, (), ()).unwrap();
	assert_eq!(y1.position(), 0x02);
	assert_eq!(y2.position(), 0x0104);
	assert_eq!(y3.position(), 0x010206);
}

#[test]
fn source_slice_raw()
{
	let x = [1,2,3,8,5,6,7];
	let mut y = Source::new(&x);
	y.u16().unwrap();
	let marker = y.slice_marker();
	assert_eq!(y.position(), 2);
	y.u32().unwrap();
	let z = marker.commit(&y);
	assert_eq!(y.position(), 6);
	y.u8().unwrap();
	assert!(!y.more());
	assert_eq!(y.position(), 7);
	assert_eq!(z, &x[2..6]);
}
