#![allow(unsafe_code)]

extern crate btls_aux_unix;
use self::btls_aux_unix::FileDescriptor;
use self::btls_aux_unix::InotifyEventType;
use self::btls_aux_unix::InotifyInitFlags;
use self::btls_aux_unix::OsError;
use self::btls_aux_unix::Path as UnixPath;
use self::btls_aux_unix::PollFd;
use self::btls_aux_unix::PollFlags;
use self::btls_aux_unix::unix_time_milliseconds;
use super::*;
use std::io::Error as IoError;
use std::io::ErrorKind as IoErrorKind;
use std::io::Result as IoResult;
use std::path::Path;
use std::os::unix::ffi::OsStrExt;
use std::thread::sleep;
use std::time::Duration;


///A inotify watcher.
///
///The [`supported_events()`](#method.supported_events) gives [`ACCESSED`] | [`MODIFIED`] | [`METADATA_CHANGED`] |
///[`CLOSED_WRITABLE`] | [`CLOSED_READONLY`] | [`OPENED`] | [`MOVED_FROM`] | [`MOVED_TO`] | [`CREATED`] |
///[`DELETED`] | [`WATCH_DELETED`] | [`WATCH_MOVED`]
///
///[`ACCESSED`]: constant.ACCESSED.html
///[`MODIFIED`]: constant.MODIFIED.html
///[`METADATA_CHANGED`]: constant.METADATA_CHANGED.html
///[`CLOSED_WRITABLE`]: constant.CLOSED_WRITABLE.html
///[`CLOSED_READONLY`]: constant.CLOSED_READONLY.html
///[`OPENED`]: constant.OPENED.html
///[`MOVED_FROM`]: constant.MOVED_FROM.html
///[`MOVED_TO`]: constant.MOVED_TO.html
///[`CREATED`]: constant.CREATED.html
///[`DELETED`]: constant.DELETED.html
///[`WATCH_DELETED`]: constant.WATCH_DELETED.html
///[`WATCH_MOVED`]: constant.WATCH_MOVED.html
pub struct InotifyWatcher
{
	fd: FileDescriptor,
	sink: Box<dyn InotifyEventSink+Send>,
}

impl InotifyTrait for InotifyWatcher
{
	fn supported_events() -> u32
	{
		ACCESSED|MODIFIED|METADATA_CHANGED|CLOSED_WRITABLE|CLOSED_READONLY|OPENED|MOVED_FROM|MOVED_TO|
			CREATED|DELETED|WATCH_DELETED|WATCH_MOVED
	}
	fn new<P:AsRef<Path>>(path: P, sink: Box<dyn InotifyEventSink+Send>, events: u32) -> IoResult<Self>
	{
		//Events supported?
		if events & !InotifyWatcher::supported_events() != 0 {
			return Err(IoError::new(IoErrorKind::InvalidInput, "Unsupported inotify flags"))
		}
		let p = path.as_ref();
		let p = p.as_os_str().as_bytes();
		let p = UnixPath::new(p).
			ok_or_else(||IoError::new(IoErrorKind::InvalidInput, "Given path contains NUL"))?;
		let fd = FileDescriptor::inotify_init(InotifyInitFlags::CLOEXEC|InotifyInitFlags::NONBLOCK).
			map_err(|err|IoError::from_raw_os_error(err.to_inner()))?;
		//For Linux, the mapping of flags is identity.
		let mut events2 = Default::default();
		if events & ACCESSED != 0 { events2 |= InotifyEventType::ACCESS; }
		if events & MODIFIED != 0 { events2 |= InotifyEventType::MODIFY; }
		if events & METADATA_CHANGED != 0 { events2 |= InotifyEventType::ATTRIB; }
		if events & CLOSED_WRITABLE != 0 { events2 |= InotifyEventType::CLOSE_WRITE; }
		if events & CLOSED_READONLY != 0 { events2 |= InotifyEventType::CLOSE_NOWRITE; }
		if events & OPENED != 0 { events2 |= InotifyEventType::OPEN; }
		if events & MOVED_FROM != 0 { events2 |= InotifyEventType::MOVED_FROM; }
		if events & MOVED_TO != 0 { events2 |= InotifyEventType::MOVED_TO; }
		if events & CREATED != 0 { events2 |= InotifyEventType::CREATE; }
		if events & DELETED != 0 { events2 |= InotifyEventType::DELETE; }
		if events & WATCH_DELETED != 0 { events2 |= InotifyEventType::DELETE_SELF; }
		if events & WATCH_MOVED != 0 { events2 |= InotifyEventType::MOVE_SELF; }
		unsafe {p.inotify_add_watch(&fd.as_fdb(), events2)}.
			map_err(|err|IoError::from_raw_os_error(err.to_inner()))?;
		Ok(InotifyWatcher{fd: fd, sink: sink })
	}
	fn wait(&mut self, timeout: Option<u32>)
	{
		//Transform timeout to absolute time.
		let timeout = timeout.map(|delta|unix_time_milliseconds() + delta as u64);
		let mut got_break = false;
		let mut run_once = false;
		while !got_break {
			let left = timeout.map(|tout|tout.saturating_sub(unix_time_milliseconds())).
				unwrap_or(60000) as i32;
			//We run this loop at least once, even if timeout is 0.
			if left <= 0 && run_once { return; }
			run_once = true;
			let mut pfd = [PollFd{
				fd: self.fd.as_fdb(),
				events: PollFlags::IN,
				revents: Default::default()
			}];
			if let Err(errmsg) =  unsafe{PollFd::poll(&mut pfd, left)} {
				self.sink.log_message(&format!("Error polling for inotify: {errmsg}"));
				sleep(Duration::from_millis(left as u64));
				return;
			};
			//If not readable, loop. In case of timeout, we break at start of loop.
			if !pfd[0].revents.is_in() { continue; }
			//We read all events, until WouldBlock.
			loop {
				if let Err(err) = self.fd.read_inotify(|ev|{
					//The mapping is identity.
					let mask = ev.get_event_mask().to_inner();
					got_break |= self.sink.inotify_event(ev.get_name(), mask);
				}) {
					if err == OsError::EAGAIN || err == OsError::EWOULDBLOCK { break; }
					self.sink.log_message(&format!("Error reading for inotify: {err}"));
					break;
				};
			}
		}
	}
}
