//!Various routines related to `Inotify`.
//!
//!This crate contains implementations of various routines related to using `Inotify` and its possible equivalents
//!in other operating systems.
//!
//!The most important items are:
//!
//! * Watch for inotify events: [`InotifyWatcher`](struct.InotifyWatcher.html)
#![forbid(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]
#![deny(unsafe_code)]

use std::io::Result as IoResult;
use std::path::Path;

///File was accessed.
pub const ACCESSED: u32 = 1;
///File was modified.
pub const MODIFIED: u32 = 2;
///File metadata was changed.
pub const METADATA_CHANGED: u32 = 4;
///File open for write was closed.
pub const CLOSED_WRITABLE: u32 = 8;
///File not open for write was closed.
pub const CLOSED_READONLY: u32 = 16;
///File was closed.
pub const CLOSED: u32 = CLOSED_WRITABLE | CLOSED_READONLY;
///File was opened.
pub const OPENED: u32 = 32;
///File was moved from this.
pub const MOVED_FROM: u32 = 64;
///File was moved to this.
pub const MOVED_TO: u32 = 128;
///File was moved.
pub const MOVED: u32 = MOVED_FROM | MOVED_TO;
///File was created.
pub const CREATED: u32 = 256;
///File was deleted.
pub const DELETED: u32 = 512;
///Watched object was deleted.
pub const WATCH_DELETED: u32 = 1024;
///Watched object was moved.
pub const WATCH_MOVED: u32 = 2048;
///Filesystem unmounted (spontaneous).
pub const UNMOUNTED: u32 = 8192;
///Queue overflow, events lost (spontaneous).
pub const OVERFLOW: u32 = 16384;
///File was ignored (spontaneous).
pub const IGNORED: u32 = 32768;


///Inotify event sink.
///
///The events occuring during monitoring get dumped to this sink.
pub trait InotifyEventSink
{
	///Make a log message `message`.
	///
	///Log messages are used to notify about various abnormal events. All the messages should be considered
	///at minimum warnings, preferably errors.
	fn log_message(&self, message: &str);
	///An event described by `mask` happened to file `filename`!
	///
	///If the return value is `true`, abort the wait and return from the `wait()` method after current batch
	///of events has been processed. If the return value is `false`, continue waiting.
	fn inotify_event(&self,  filename: &[u8], event: u32) -> bool;
}

///An Inotify poller
///
///Each object represents poller for inotify. Objects are created by the [`new()`](#tymethod.new) method.
///
///One can wait for events using the [`wait()`](#tymethod.wait) method.
pub trait InotifyTrait: Sized
{
	///Create a new Inotify poller for path `path`, watching for events in `events` and reporting results
	///to `sink`.
	///
	///If `events` contains unsupported event types (including any events labeled as "spontaneous"), this
	///method will fail. For supported events, see [`supported_events()`](#method.supported_events).
	///
	///On success, returns `Ok(poller)`, where `poller` is the poller object. Otherwise returns `Err(err)`
	///where `err` is the error.
	fn new<P:AsRef<Path>>(path: P, sink: Box<dyn InotifyEventSink+Send>, events: u32) -> IoResult<Self>;
	///Wait until poller detects something interesting, or timeout `timeout` (in milliseconds) expires. If
	///`timeout` is `None`, this method can wait forever.
	fn wait(&mut self, timeout: Option<u32>);
	///Get mask of supported events (default 0).
	fn supported_events() -> u32 { 0 }
}

#[cfg(target_os = "linux")]
#[path="linux.rs"]
mod inner;

#[cfg(not(target_os = "linux"))]
#[path="dummy.rs"]
mod inner;

pub use inner::*;

#[cfg(test)]
mod test;
