use super::*;
use std::io::Error as IoError;
use std::io::ErrorKind as IoErrorKind;
use std::io::Result as IoResult;
use std::path::Path;

///A inotify watcher.
///
///This is a dummy watcher that can never be created.
pub enum InotifyWatcher {}	//This can actually never be created!

impl InotifyTrait for InotifyWatcher
{
	fn new<P:AsRef<Path>>(_path: P, _sink: Box<InotifyEventSink+Send>, _events: u32) -> IoResult<Self>
	{
		Err(IoError::new(IoErrorKind::InvalidInput, "Inotify is not supported"))
	}
	fn wait(&mut self, _timeout: Option<u32>)
	{
		unreachable!();	//The receiver can not exist.
	}
}
