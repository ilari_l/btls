//!Time routines.
//!
//!This crate provodes time related methods:
//!
//!The most important items are:
//!
//! * [`Timestamp`](struct.Timestamp.html): Timestamp.
//! * [`TimeInterface`](trait.TimeInterface.html): Interface that provodes timestamps.
//! * [`TimeNow`](struct.TimeNow.html): Implementation of [`TimeInterface`](trait.TimeInterface.html) that returns
//!the current time (only available if feature `stdlib` is enabled).
//! * [`PointInTime`](struct.PointInTime.html): Point in time relative to some time that is not affected by jumps in
//!the clock (only available if feature `stdlib` is enabled).
#![no_std]
#![forbid(unsafe_code)]
#![deny(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]

#[cfg(test)] #[macro_use] extern crate std;
use btls_aux_fail::dtry;
use btls_aux_fail::f_return;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::ResultExt;
use core::cmp::min;
use core::fmt::Display;
use core::fmt::Formatter;
use core::fmt::Error as FmtError;
use core::ops::Add;
use core::ops::Range;

///Timestamp type.
///
///This structure contains a second-resolution timestamp. The range of the timestamp goes from big bang to
///to over 290 billion years CE.
#[derive(Copy,Clone,PartialEq,PartialOrd,Eq,Ord,Debug)]
pub struct Timestamp(i64);

impl Timestamp
{
	///Create new timestamp carrying time that has unix epoch offset `ts`.
	///
	///The offset is denoted as seconds since 1st Jaunuary 1970 midnight UTC, without leap seconds.
	pub fn new(ts: i64) -> Timestamp { Timestamp(ts) }
	#[allow(missing_docs)] #[deprecated(since="2.3.0", note="Use Timestamp::unix_time() instead")]
	pub fn into_inner(self) -> i64 { self.0 }
	///Get unix time corresponding to timestamp.
	pub fn unix_time(self) -> i64 { self.0 }
	///Get number of day the timestamp is on.
	///
	///Day 0 is 1st January 1970.
	pub fn get_day(self) -> i64 { self.0.div_euclid(86400) }
	///Negative infinity.
	///
	///This method returns the timestamp that is as far into the past as possible.
	pub fn negative_infinity() -> Timestamp { Timestamp(i64::min_value()) }
	///Positive infinity.
	///
	///This method returns the timestamp that is as far into the future as possible.
	pub fn positive_infinity() -> Timestamp { Timestamp(i64::max_value()) }
	///First of this timestamp and `another`.
	pub fn first(self, another: Timestamp) -> Timestamp { if self < another { self } else { another } }
	///Last of this timestamp and `another`.
	pub fn last(self, another: Timestamp) -> Timestamp { if self > another { self } else { another } }
	///Get time difference between this timestamp and timestamp `another`.
	///
	///The difference is returned in seconds. If the `another` is actually before this timestamp, returns 0.
	pub fn delta(self, another: Timestamp) -> u64
	{
		if another.0 >= self.0 {
			let base = self.0;
			let baseoff = (!base).wrapping_add(1) as u64;
			baseoff.wrapping_add(another.0 as u64)
		} else {
			0
		}
	}
	///Is this timestamp in specified range `r`?
	pub fn in_range(self, r: &Range<Timestamp>) -> bool { r.contains(&self) }
}

///Interface: Get timestamp.
pub trait TimeInterface
{
	///Get the relevant timestamp.
	///
	///This method returns the relevant timestamp for the receiver. This timestamp may be current time or some
	///fixed time represented by it.
	fn get_time(&self) -> Timestamp;
}

impl TimeInterface for Timestamp
{
	fn get_time(&self) -> Timestamp { *self }
}

impl Add<i64> for Timestamp
{
	type Output = Timestamp;
	fn add(self, x: i64) -> Timestamp { Timestamp(self.0 + x) }
}

impl Default for Timestamp
{
	fn default() -> Timestamp { Timestamp(0) }
}

#[cfg(feature="stdlib")]
mod stdc {
	extern crate std;
	use self::std::cmp::Eq;
	use self::std::cmp::min;
	use self::std::cmp::Ord;
	use self::std::cmp::Ordering;
	use self::std::cmp::PartialEq;
	use self::std::cmp::PartialOrd;
	use self::std::i64::MAX as I64_MAX;
	use self::std::time::Duration;
	use self::std::time::Instant;
	use self::std::time::SystemTime;
	use self::std::time::UNIX_EPOCH;
	#[cfg(not(unix))] use super::ShowTime;
	use super::TimeInterface;
	use super::Timestamp;

	#[cfg(not(unix))]
	impl ShowTime
	{
		///Current time.
		pub fn now() -> ShowTime
		{
			//This does not take leap seconds into account, but there is no interface to do so.
			let (r, s) = match SystemTime::now().duration_since(UNIX_EPOCH) {
				Ok(x) => (x.as_secs() as i64, x.subsec_nanos()),
				Err(x) => {
					let x = x.duration();
					let mut r = x.as_secs() as i64;
					let mut s = x.subsec_nanos();
					//The subsecond nanos go backwards on negative time.
					if s > 0 {
						r += 1;
						s = 1_000_000_000 - s;
					}
					(-r, s)
				}
			};
			ShowTime(r.div_euclid(86400), Some(r.rem_euclid(86400) as u32), s)
		}
	}

	impl Timestamp
	{
		///Get current time.
		pub fn now() -> Timestamp
		{
			let r = match SystemTime::now().duration_since(UNIX_EPOCH) {
				Ok(x) => x.as_secs() as i64,
				Err(x) => -(x.duration().as_secs() as i64)
			};
			Timestamp(r)
		}
	}

	#[allow(missing_docs)] #[deprecated(since="2.3.0", note="Use Timestamp::now() instead")]
	pub struct TimeNow;

	#[allow(deprecated)]
	impl TimeInterface for TimeNow
	{
		fn get_time(&self) -> Timestamp { Timestamp::now() }
	}

	fn bias64(val: i64) -> u64 { (val as u64).wrapping_add(1u64 << 63) }
	fn unbias64(val: u64) -> i64 { val.wrapping_sub(1u64 << 63) as i64 }

	///An unit of time.
	#[derive(Copy,Clone,Debug)]
	pub enum TimeUnit
	{
		///Indicated number of seconds.
		Seconds(u64),
		///Indicated number of milliseconds.
		Milliseconds(u64),
		///Indicated number of microseconds.
		Microseconds(u64),
		///Indicated number of nanoseconds.
		Nanoseconds(u64),
		///Duration.
		Duration(Duration),
	}

	impl TimeUnit
	{
		fn as_nanoseconds(self) -> u64
		{
			match self {
				TimeUnit::Seconds(x) => x.saturating_mul(1000000000),
				TimeUnit::Milliseconds(x) => x.saturating_mul(1000000),
				TimeUnit::Microseconds(x) => x.saturating_mul(1000),
				TimeUnit::Nanoseconds(x) => x,
				TimeUnit::Duration(x) => x.as_secs().saturating_mul(1000000000).
					saturating_add(x.subsec_nanos() as u64),
			}
		}
	}

	///A point in time.
	///
	///Points in time are not affected by any clock jumps. However, they are inherently volatile and can not
	///be persisted by any means.
	///
	///Note that point of time is represented as a base time and offset. The offset saturates at ~292 years.
	///This should however be enough due to volatility, as program is unlikely to be running for longer than
	///292 years.
	#[derive(Copy,Clone,Debug)]
	pub struct PointInTime
	{
		base: Instant,
		delta: i64,	//In nanoseconds.
	}

	impl PointInTime
	{
		///Create a point in time that is current time.
		pub fn now() -> PointInTime
		{
			PointInTime {
				base: Instant::now(),
				delta: 0,
			}
		}
		///Create a point in time that is amount of time `delta` from current time.
		pub fn from_now(delta: TimeUnit) -> PointInTime
		{
			PointInTime {
				base: Instant::now(),
				delta: min(delta.as_nanoseconds(), I64_MAX as u64) as i64,
			}
		}
		fn nanoseconds_until(self) -> Option<u64>
		{
			let now = Instant::now();
			let dt = match self.base.cmp(&now) {
				Ordering::Less => {
					let dt = now - self.base;
					let ns = dt.as_secs().saturating_mul(1000000000).
						saturating_add(dt.subsec_nanos() as u64);
					//delta - ns.
					unbias64(bias64(self.delta).saturating_sub(ns))
				},
				Ordering::Equal => self.delta,
				Ordering::Greater => {
					let dt = self.base - now;
					let ns = dt.as_secs().saturating_mul(1000000000).
						saturating_add(dt.subsec_nanos() as u64);
					//delta + ns.
					unbias64(bias64(self.delta).saturating_add(ns))
				},
			};
			if dt >= 0 { Some(dt as u64) } else { None }
		}
		///Is the specified point in time in the past?
		pub fn in_past(self) -> bool { self.nanoseconds_until().is_none() }
		///Advance point in time by amount `delta`.
		///
		///Note that advancing point of time makes it occur *earlier*.
		pub fn advance(self, delta: TimeUnit) -> PointInTime
		{
			PointInTime {
				base: self.base,
				delta: unbias64(bias64(self.delta).saturating_sub(delta.as_nanoseconds()))
			}
		}
		///Retrard point time by amount `delta`.
		///
		///Note that advancing point of time makes it occur *later*.
		pub fn retrard(self, delta: TimeUnit) -> PointInTime
		{
			PointInTime {
				base: self.base,
				delta: unbias64(bias64(self.delta).saturating_add(delta.as_nanoseconds()))
			}
		}
		///Get point in time that has offset as specified by this point in time, but with base specified by
		///`basetime`.
		///
		///The usual use is rebasing a new deadline on start time of operation in order to obtain a deadline
		///that is fixed amount from start of operation, regardless of how much time has passed after start
		///of the operation.
		pub fn rebase(self, base: PointInTime) -> PointInTime
		{
			PointInTime {
				base: base.base,
				delta: self.delta,
			}
		}
		///Get duration until point in time.
		///
		///If event is in the past, returns `None`. Otherwise returns `Some(x)`, where `x` is the duration
		///(capped to ~292 years).
		pub fn time_to(self) -> Option<Duration>
		{
			self.nanoseconds_until().map(|dt|Duration::new(dt / 1000000000, (dt % 1000000000) as u32))
		}
	}

	impl PartialEq for PointInTime
	{
		fn eq(&self, another: &PointInTime) -> bool { self.cmp(another).is_eq() }
	}

	impl Eq for PointInTime
	{
	}

	impl PartialOrd for PointInTime
	{
		fn partial_cmp(&self, another: &PointInTime) -> Option<Ordering> { Some(self.cmp(another)) }
	}

	impl Ord for PointInTime
	{
		fn cmp(&self, another: &PointInTime) -> Ordering
		{
			let deltadiff = self.delta.saturating_sub(another.delta);
			let dt = match self.base.cmp(&another.base) {
				Ordering::Less => {
					let dt = another.base - self.base;
					let ns = dt.as_secs().saturating_mul(1000000000).
						saturating_add(dt.subsec_nanos() as u64);
					//delta - ns.
					unbias64(bias64(deltadiff).saturating_sub(ns))
				},
				Ordering::Equal => deltadiff,
				Ordering::Greater => {
					let dt = self.base - another.base;
					let ns = dt.as_secs().saturating_mul(1000000000).
						saturating_add(dt.subsec_nanos() as u64);
					//delta + ns.
					unbias64(bias64(deltadiff).saturating_add(ns))
				},
			};
			dt.cmp(&0i64)
		}
	}

	///Something to pass as timer target.
	pub trait TimerTarget
	{
		///Convert to point in time.
		fn as_pit(self) -> Option<PointInTime>;
	}

	#[allow(deprecated)]
	impl TimerTarget for TimeNow
	{
		fn as_pit(self) -> Option<PointInTime> { Some(PointInTime::now()) }
	}

	impl TimerTarget for PointInTime
	{
		fn as_pit(self) -> Option<PointInTime> { Some(self) }
	}

	impl TimerTarget for Option<PointInTime>
	{
		fn as_pit(self) -> Option<PointInTime> { self }
	}

	impl TimerTarget for TimeUnit
	{
		fn as_pit(self) -> Option<PointInTime> { Some(PointInTime::from_now(self)) }
	}

	impl TimerTarget for Option<TimeUnit>
	{
		fn as_pit(self) -> Option<PointInTime> { self.map(|dt|PointInTime::from_now(dt)) }
	}

	impl TimerTarget for Duration
	{
		fn as_pit(self) -> Option<PointInTime> { Some(PointInTime::from_now(TimeUnit::Duration(self))) }
	}

	impl TimerTarget for Option<Duration>
	{
		fn as_pit(self) -> Option<PointInTime>
		{
			self.map(|dt|PointInTime::from_now(TimeUnit::Duration(dt)))
		}
	}

	///A timer.
	#[derive(Clone)]
	pub struct Timer(Option<PointInTime>);

	impl Timer
	{
		///Create a disarmed timer.
		pub fn new() -> Timer { Timer(None) }
		///Create a new timer.
		pub fn new_target<TT:TimerTarget>(target: TT) -> Timer { Timer(target.as_pit()) }
		///Disarm the timer.
		pub fn disarm(&mut self) { self.0 = None; }
		///Rearm the timer.
		pub fn rearm<TT:TimerTarget>(&mut self, target: TT) { *self = Timer::new_target(target); }
		///Check if armed.
		pub fn is_armed(&self) -> bool { self.0.is_some() }
		///Check if unarmed.
		pub fn is_unarmed(&self) -> bool { self.0.is_none() }
		///Get the point in time timer expires at.
		pub fn expires(&self) -> Option<PointInTime> { self.0 }
		///Check if timer has expired, and disarm if yes.
		pub fn triggered(&mut self, now: PointInTime) -> bool
		{
			//The condition must be >= because otherwise code could get stuck in loop where event
			//does not fire, but next event occurs immediately, so the code calls again into timer
			//interrupt, leading to infinite loop.
			if self.0.map(|ts|now >= ts).unwrap_or(false) {
				self.0 = None;
				true
			} else {
				false
			}
		}
	}
}
#[cfg(feature="stdlib")]
pub use stdc::*;

///Error in parsing timestamp.
///
///This structure is created as error type by [`interpret_timestamp()`](fn.interpret_timestamp.html). It can be
///formatted in human-readable form by using the `{}` format operator.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
#[non_exhaustive]
pub enum TimestampParseError
{
	///The date is invalid.
	///
	///This error occurs if a date is invalid (invalid day in month, invalid month or invalid year).
	///
	/// * The 1st parameter is the year.
	/// * The 2nd parameter is the month.
	/// * The 3rd parameter is the day.
	InvalidDate(i64, usize, i64),
	///The time is invalid.
	///
	///This error occurs if time is invalid (invalid hour number, invalid minute number or invalid second
	///number).
	///
	/// * The 1st parameter is the hour.
	/// * The 2nd parameter is the minute.
	/// * The 3rd parameter is the second.
	InvalidTime(u32, u32, u32),
	///Unrecognized time format.
	///
	///This error occurs if timestamp isn't either UTCTime nor GeneralizedTime.
	UnrecognizedFormat,
	///UTCTime notation is bad.
	///
	///This error occurs if UTCTime value can not be parsed into its components.
	BadUtcTime,
	///GeneralizedTime notation is bad.
	///
	///This error occurs if GeneralizedTime value can not be parsed into its components.
	BadGeneralizedTime,
}

impl Display for TimestampParseError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::TimestampParseError::*;
		match self {
			&InvalidDate(year, month, day) => write!(fmt, "Invalid date {year}-{month}-{day}"),
			&InvalidTime(hour, minute, second) => write!(fmt, "Invalid time {hour}:{minute}:{second}"),
			&UnrecognizedFormat => fmt.write_str("Unrecognized time format"),
			&BadUtcTime => fmt.write_str("Bad UtcTime notation"),
			&BadGeneralizedTime => fmt.write_str("Bad GeneralizedTime notation"),
		}
	}
}

macro_rules! digit10
{
	($x:expr) => { match $x.wrapping_sub(0x30) { y if y < 10 => y as u32, _ => fail!(()) } };
	//These are at most 9, so not even close to overflow.
	($x:expr, $y:expr) => { digit10!($x) * 10 + digit10!($y) };
}

///Read timestamp of form yymmddhhmmssZ
///
///The timestamp to read is `payload`.
///
///Range of years is 1950 to 2049.
///
///On success, returns `Ok((year, month, day, hour, minute, second))`, where the parameters are the time components.
///If the string can not be parsed as a date, returns `Err(())`.
pub fn read_utc_timestamp(payload: &[u8]) -> Result<(u32, u32, u32, u32, u32, u32), ()>
{
	//payload.len() != 13 short-circuits uless 12 < payload.len() == 13
	fail_if!(payload.len() != 13 || payload[12] != 90, ());
	//These won't come even near overflowing. And payload.len() == 13 means indices up to 12 are in range.
	let year = digit10!(payload[0], payload[1]);
	let month = digit10!(payload[2], payload[3]);
	let day = digit10!(payload[4], payload[5]);
	let hour = digit10!(payload[6], payload[7]);
	let minute = digit10!(payload[8], payload[9]);
	let second = digit10!(payload[10], payload[11]);
	let ybase = if year >= 50 { 1900u32 } else { 2000u32 };
	Ok((ybase + year, month, day, hour, minute, second))
}

///Read timestamp of form yyyymmddhhmmssZ
///
///The timestamp to read is `payload`.
///
///Range of years is 1 to 9999.
///
///On success, returns `Ok((year, month, day, hour, minute, second))`, where the parameters are the time components.
///If the string can not be parsed as a date, returns `Err(())`.
pub fn read_general_timestamp(payload: &[u8]) -> Result<(u32, u32, u32, u32, u32, u32), ()>
{
	//payload.len() != 15 short-circuits uless 14 < payload.len() == 15
	fail_if!(payload.len() != 15 || payload[14] != 90, ());
	//And payload.len() == 15 means indices up to 14 are in range.
	let century = digit10!(payload[0], payload[1]);
	let year = digit10!(payload[2], payload[3]);
	let month = digit10!(payload[4], payload[5]);
	let day = digit10!(payload[6], payload[7]);
	let hour = digit10!(payload[8], payload[9]);
	let minute = digit10!(payload[10], payload[11]);
	let second = digit10!(payload[12], payload[13]);
	//The year is max 9999, => no overflow.
	Ok((century*100+year, month, day, hour, minute, second))
}

///Time format.
#[derive(Copy,Clone,PartialEq,Eq,Debug)]
#[non_exhaustive]
pub enum TimeFormat
{
	///ASN.1 UTCTime.
	UTCTime,
	///ASN.1 GeneralizedTime.
	GeneralizedTime,
	///Unknown.
	Unknown,
}

///Interpret an ASN.1 timestamp.
///
///The timestamp is `payload` and it is of type `tag`.
///
///The type can be either UTCTime or GeneralizedTime.
///
///On success, returns the parsed timestamp. If the timestamp can not be parsed, returns `Err(err)`, where `err`
///describes the error.
pub fn interpret_timestamp<'a>(tag: TimeFormat, payload: &[u8]) -> Result<Timestamp, TimestampParseError>
{
	use self::TimestampParseError::*;
	let (year, month, day, hour, minute, mut second) = if tag == TimeFormat::UTCTime {
		//UTCTime: YYMMDDHHMMSS"Z"
		read_utc_timestamp(payload).set_err(BadUtcTime)?
	} else if tag == TimeFormat::GeneralizedTime {
		//GeneralizedTime: YYYYMMDDHHMMSS"Z"
		read_general_timestamp(payload).set_err(BadGeneralizedTime)?
	} else { fail!(UnrecognizedFormat); };
	fail_if!(year < 1 || year > 9999, InvalidDate(year as i64, month as usize, day as i64));
	//23:59:60 might be valid. Remap that to 23:59:59, as not_after is >, not >=.
	fail_if!(hour > 23 || minute > 59 || second > 60, InvalidTime(hour, minute, second));
	fail_if!((hour != 23 || minute != 59) && second == 60, InvalidTime(hour, minute, second));
	if second == 60 { second = 59; }
	//These won't come even near overflowing, since max year is 9999 and min is 1.
	let time_of_day = (hour as i64).wrapping_mul(3600).wrapping_add((minute as i64).wrapping_mul(60)).
		wrapping_add(second as i64);
	let daynum = number_of_day(year as i64, month as u8, day as u8).
		set_err(InvalidDate(year as i64, month as usize, day as i64))?;
	Ok(Timestamp::new(daynum.wrapping_mul(86400).wrapping_add(time_of_day)))
}

///Convert year, month and day into a day number.
///
///The year is `year`, the month is `month` and the day is `day`.
///
///The scale is such that new years' of 1970 is 0, and each day has number one greater than previous day. To convert
///to Modified Julian Day (MJD), add 40587 to the return value.
///
///Leap day cycle is assumed to loop every 400 years, and year 0 does not exist (the day before 1st January 1CE is
///31st December 1BCE).
///
///The range of dates accepted is `27th May 25,252,734,927,764,586 BCE` (this is before the big bang) to
///`27th July 25,252,734,927,768,524 CE`.
///
///On success, returns `Ok(daynum)` where `daynum` is the number of day. If the specified date is invalid, returns
///`Err(())`.
pub fn number_of_day(year: i64, month: u8, day: u8) -> Result<i64, ()>
{
	//Compute number of day in year.
	let month = month as usize;
	let day = day as i64;
	fail_if!(year == 0 || day <= 0, ());					//There is no year 0 or day 0.
	let year = year + if year < 0 { 1 } else { 0 };				//Pack years without gaps.
	let is_leap = year % 4 == 0 && (year % 400 == 0 || year % 100 != 0);	//Is leap year?
	let delta = if !is_leap && month > 2 { 1 } else { 0 };			//Missing leap day.
	fail_if!(!is_leap && month == 2 && day == 29, ());			//No leap day.
	const MONTHSTART: [i64;13] = [0,31,60,91,121,152,182,213,244,274,305,335,366];
	//If month is not good, either of these is going to be OOB.
	let snmonth = *dtry!(MONTHSTART.get(month));
	let stmonth = *dtry!(MONTHSTART.get(month.wrapping_sub(1)));
	fail_if!(day > snmonth - stmonth, ());					//1<=month<=12, indexes 0...12.
	let yday = stmonth + day - 1 - delta;					//Day of year.

	let mut daynum = 0i64;
	let mut year = year;
	//Supercycle jump: Start from negative 2^45 supercycles, and proceed up to positive 2^45 supercycles.
	for i in 0..46 {
		let i = 45 - i;
		if year < 1970 - (400 << i) {	//This won't overflow.
			year += 400 << i;
			daynum = dtry!(daynum.checked_sub(146097 << i));
		}
	}
	for i in 0..46 {
		let i = 45 - i;
		if year >= 1970 + (400 << i) {	//This won't overflow.
			year -= 400 << i;
			daynum = dtry!(daynum.checked_add(146097 << i));
		}
	}
	//Now year is in range 1570 to 2369. Compensate for missing leap years
	if year < 2000 { daynum = dtry!(daynum.checked_add(min((2000 - year) / 100, 3))); }
	if year > 2000 { daynum = dtry!(daynum.checked_sub((year - 2001) / 100)); }
	//Cycle jump: Start from negative 2^8 cycles and proceed up to positive 2^8 cycles. Due to compensation
	//above, all cycles can be assumed to be 1461 days.
	for i in 0..9 {
		let i = 8 - i;
		if year < 1970 - (4 << i) {
			year += 4 << i;
			daynum = dtry!(daynum.checked_sub(1461 << i));
		}
	}
	for i in 0..9 {
		let i = 8 - i;
		if year >= 1970 + (4 << i) {
			year -= 4 << i;
			daynum = dtry!(daynum.checked_add(1461 << i));
		}
	}
	//Now year is in range 1966 to 1973. Jump years.
	if year == 1966 { daynum = dtry!(daynum.checked_sub(365)); year += 1 }
	if year == 1967 { daynum = dtry!(daynum.checked_sub(365)); year += 1 }
	if year == 1968 { daynum = dtry!(daynum.checked_sub(366)); year += 1 }
	if year == 1973 { daynum = dtry!(daynum.checked_add(366)); year -= 1 }
	if year == 1972 { daynum = dtry!(daynum.checked_add(365)); year -= 1 }
	if year == 1971 { daynum = dtry!(daynum.checked_add(365)); year -= 1 }
	//Now the year has been reduced to 1969 or 1970.
	if year == 1970 {
		daynum = dtry!(daynum.checked_add(yday));
	} else {
		daynum = dtry!(daynum.checked_sub(365 - yday));
	}
	Ok(daynum)
}

///Convert a day number into a date.
///
///The day number to convert is `daynum`.
///
///The scale is such that new years' of 1970 is 0, and each day has number one greater than previous day. To convert
///Modified Julian Day (MJD), substract 40587 first.
///
///The range of dates generated is `27th May 25,252,734,927,764,586 BCE` (this is before the big bang) to
///`27th July 25,252,734,927,768,524 CE`.
///
///Returns tuple `(year, month, day)` , where `year` is the year, `month` is the month and `day` is the day of the
///month.
pub fn daynumber_into_date(daynum: i64) -> (i64, u8, u8)
{
	//Skip supercycles (146097 days). Force daynum to be positive in range 0-146096. Also shift by 10957
	//days to get timebase to be 1.1.2000.
	let sccount = daynum / 146097;
	let mut yearbase = 2000i64 + sccount * 400;
	let mut daynum = daynum - 146097 * sccount - 10957;
	while daynum < 0 {	//This can happen at most twice.
		yearbase -= 400;
		daynum += 146097;
	}
	//Compensate for missing leap days.
	if daynum >= 36584 { daynum += 1; }
	if daynum >= 73109 { daynum += 1; }
	if daynum >= 109634 { daynum += 1; }
	//Skip cycles (1461 days). This forces daynum to range 0-1460.
	let ccount = daynum / 1461;
	yearbase += ccount * 4;
	daynum -= ccount * 1461;
	//Compensate for missing leap days.
	if daynum >= 425 { daynum += 1; }
	if daynum >= 791 { daynum += 1; }
	if daynum >= 1157 { daynum += 1; }
	//Skip years (366 days). This forces daynum to range 0-365.
	let ycount = daynum / 366;
	yearbase += ycount;
	daynum -= ycount * 366;
	//"Year 0"
	if yearbase <= 0 { yearbase -= 1; }
	//Compensate for the missing days.
	if daynum >= 60 { daynum += 2; }
	if daynum >= 123 { daynum += 1; }
	if daynum >= 185 { daynum += 1; }
	if daynum >= 278 { daynum += 1; }
	if daynum >= 340 { daynum += 1; }
	let mut month = daynum / 31;
	daynum -= month * 31;
	//Add 1 to day and month, since numbers start from 1.
	month += 1;
	daynum += 1;
	(yearbase, month as u8, daynum as u8)
}

///Decode timestamp to human-readable format.
///
///The timestamp to decode is the first field. The second field is deprecated and does nothing. This is intended to
///be printed by using the `{}` formatting operator.
pub struct DecodeTsForUser(pub Timestamp, pub bool);

///Decode timestamp to human-readable format.
///
///Similar as [`DecodeTsForUser`](struct.DecodeTsForUser.html), but uses different format.
///
///The timestamp to decode is the first field. This is intended to be printed by using the `{}` formatting operator.
pub struct DecodeTsForUser2(pub Timestamp);


impl Display for DecodeTsForUser
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		Display::fmt(&ShowTime::from_ts(self.0).format1(), fmt)
	}
}

impl Display for DecodeTsForUser2
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		Display::fmt(&ShowTime::from_ts(self.0).format2(), fmt)
	}
}

#[derive(Copy,Clone)]
enum TimeFmt
{
	F1,
	F2,
	F3,
}

///Show current time.
#[derive(Copy,Clone)]
pub struct ShowTime(i64, Option<u32>, u32);

impl ShowTime
{
	fn from_ts(ts: Timestamp) -> ShowTime
	{
		ShowTime::from_i64(ts.0)
	}
	///Create from UNIX timestamp.
	pub fn from_i64(ts: i64) -> ShowTime
	{
		ShowTime(ts.div_euclid(86400), Some(ts.rem_euclid(86400) as u32), 0)
	}
	///Get number of day.
	pub fn number_of_day(&self) -> i64 { self.0 }
	///Get time of day.
	pub fn time_of_day(&self) -> (u8, u8, u8)
	{
		if let Some(ts) = self.1 {
			let a = ts / 3600;
			let b = ts / 60 % 60;
			let c = ts % 60;
			(a as u8, b as u8, c as u8)
		} else {
			(23, 59, 60)	//Leap second!!!
		}
	}
	///Get subsecond nanos.
	pub fn subsec_nanos(&self) -> u32 { self.2 }
	///Format 1 (yyyymmddThhmmssZ).
	pub fn format1(self) -> ShowTimeDisplay
	{
		ShowTimeDisplay(self.0, self.1, self.2, TimeFmt::F1)
	}
	///Format 2 (yyyy-mm-dd hh:mm:ss UTC).
	pub fn format2(self) -> ShowTimeDisplay
	{
		ShowTimeDisplay(self.0, self.1, self.2, TimeFmt::F2)
	}
	///Format 3 (yyyy-mm-dd hh:mm:ss.mmm UTC).
	pub fn format3(self) -> ShowTimeDisplay
	{
		ShowTimeDisplay(self.0, self.1, self.2, TimeFmt::F3)
	}
}

#[cfg(unix)]
impl ShowTime
{
	///Current time.
	pub fn now() -> ShowTime
	{
		let (a, b, c) = btls_aux_unix::get_date_and_time().unwrap_or((0,None,0));
		ShowTime(a, b, c)
	}
}


///Current time printer.
#[derive(Copy,Clone)]
pub struct ShowTimeDisplay(i64, Option<u32>, u32, TimeFmt);

impl Display for ShowTimeDisplay
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		let (year,month,day) = daynumber_into_date(self.0);
		let (hour, minute, second) = if let Some(t) = self.1 {
			(t / 3600, t / 60 % 60, t % 60)
		} else {
			(23, 59, 60)	//Leap second!
		};
		let msec = self.2 / 1_000_000;
		match self.3 {
			TimeFmt::F1 => write!(fmt, "{year:04}{month:02}{day:02}T{hour:02}{minute:02}{second:02}Z"),
			TimeFmt::F2 =>
				write!(fmt, "{year:04}-{month:02}-{day:02} {hour:02}:{minute:02}:{second:02} UTC"),
			TimeFmt::F3 => write!(fmt, "{year:04}-{month:02}-{day:02} \
				{hour:02}:{minute:02}:{second:02}.{msec:03} UTC"),
		}
	}
}

///Get length of wait to specified unix timestamp.
///
/// # Parameters:
///
/// * ts: The timestamp
///
/// # Returns:
///
///The length of wait in nanoseconds.
#[cfg(unix)]
pub fn wait_for_unix_ts(ts: i64) -> u64
{
	use btls_aux_unix::ExtendedTime;
	use btls_aux_unix::LeapSecondStatus;
	use core::convert::TryInto;
	let ts2 = f_return!(ExtendedTime::now(), 0);
	//Time in past?
	if ts <= ts2.timestamp { return 0; }
	let is_boundary = ts2.timestamp.div_euclid(86400) != ts.div_euclid(86400);
	let mut adjust = 0i64;
	//If waiting over day boundary, if leap second will be inserted, add a extra second to wait. If leap second
	//will be deleted, remove second from wait. If leap second is in progress, do nothing, as the wait length
	//is already correct.
	if is_boundary && ts2.leap_second == LeapSecondStatus::WillInsert { adjust = 1; }
	if is_boundary && ts2.leap_second == LeapSecondStatus::WillDelete { adjust = -1; }
	let ns = ts.saturating_sub(ts2.timestamp).saturating_add(adjust) as u128 * 1_000_000_000 -
		ts2.nanoseconds as u128;
	ns.try_into().unwrap_or(u64::MAX)
}


#[cfg(test)]
mod test;
