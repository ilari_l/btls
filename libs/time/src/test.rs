use super::*;
use core::i64::MIN as I64_MIN;
use core::i64::MAX as I64_MAX;
use std::string::ToString;


fn daynum_roundtrip(y: i64, m: u8, d: u8)
{
	let n = number_of_day(y, m, d).unwrap();
	let (y2, m2, d2) = daynumber_into_date(n);
	let x1 = format!("{y}-{m}-{d}");
	let x2 = format!("{y2}-{m2}-{d2}");
	assert_eq!(&x1, &x2);
}

#[test]
fn daynum_roundtrips1()
{
	daynum_roundtrip(-10005, 2, 29);
	const YEARS: [i64;14] = [2000, 2001, 2002, 2003, 2004, 2100, 2200, 2300, 2400, -10000, -10001, 10000,
		1996, 1999];
	for i in YEARS.iter() { for j in 1..13 { daynum_roundtrip(*i, j, 1); } }
	const LYEARS: [i64;12] = [2000, 2004, 2008, 2012, 2016, 2104, 2204, 2304, 2400, -10001, -10005, 10000];
	for i in LYEARS.iter() { daynum_roundtrip(*i, 2, 29); }
}

#[test]
fn format_leap_second()
{
	let ls = ShowTime(17166, None, 123456789);
	assert_eq!(ls.format1().to_string(), "20161231T235960Z");
	assert_eq!(ls.format2().to_string(), "2016-12-31 23:59:60 UTC");
	assert_eq!(ls.format3().to_string(), "2016-12-31 23:59:60.123 UTC");
}

#[test]
fn last_normal_second()
{
	let ls = ShowTime(17166, Some(86399), 123456789);
	assert_eq!(ls.format1().to_string(), "20161231T235959Z");
	assert_eq!(ls.format2().to_string(), "2016-12-31 23:59:59 UTC");
	assert_eq!(ls.format3().to_string(), "2016-12-31 23:59:59.123 UTC");
}

#[test]
fn end_of_year()
{
	assert_eq!(number_of_day(2000, 12, 31).unwrap() + 1, number_of_day(2001, 1, 1).unwrap());
}

#[test]
fn end_of_year_negative()
{
	assert_eq!(number_of_day(1900, 12, 31).unwrap() + 1, number_of_day(1901, 1, 1).unwrap());
}

#[test]
fn skip_year0()
{
	assert_eq!(number_of_day(-1, 12, 31).unwrap() + 1, number_of_day(1, 1, 1).unwrap());
}

#[test]
fn year_end_1977()
{
	//This was a special case causing problems in one buggy version.
	assert_eq!(number_of_day(1977, 12, 31).unwrap() + 1, number_of_day(1978, 1, 1).unwrap());
}

#[test]
fn year_end_2769()
{
	//The supercycle analog of the last test.
	assert_eq!(number_of_day(2769, 12, 31).unwrap() + 1, number_of_day(2770, 1, 1).unwrap());
}

#[test]
fn year_end()
{
	for i in 1..3000 {
		assert_eq!(number_of_day(i, 12, 31).unwrap() + 1, number_of_day(i+1, 1, 1).unwrap());
	}
	for i in -3000..-1 {
		assert_eq!(number_of_day(i, 12, 31).unwrap() + 1, number_of_day(i+1, 1, 1).unwrap());
	}
}

#[test]
fn year_progression()
{
	//Test that lengths of years 7000 BCE to 6999 CE are correct.
	for i in 1..7000 {
		let days = if i % 4 == 0 && (i % 400 == 0 || i % 100 != 0) { 366 } else { 365 };
		assert_eq!(number_of_day(i, 1, 1).unwrap() + days, number_of_day(i+1, 1, 1).unwrap());
	}
	for i in -7000..-1 {
		let days = if (i + 1) % 4 == 0 && ((i + 1) % 400 == 0 || (i + 1) % 100 != 0) { 366 } else { 365 };
		assert_eq!(number_of_day(i, 1, 1).unwrap() + days, number_of_day(i+1, 1, 1).unwrap());
	}
	assert_eq!(number_of_day(-1, 1, 1).unwrap() + 366, number_of_day(1, 1, 1).unwrap());
}

#[test]
fn month_progression_leap()
{
	const DAYS: [u8;12] = [31,29,31,30,31,30,31,31,30,31,30,31];
	const YEARS: [i64;9] = [1600,2000,2400,2004,2016,2020,4020,-1,-401];
	for i in 1..12 {
		for j in YEARS.iter() {
			assert_eq!(number_of_day(*j, i as u8, DAYS[i-1]).unwrap() + 1, number_of_day(*j,
				(i+1) as u8, 1).unwrap());
		}
	}
}

#[test]
fn month_progression_not_leap()
{
	const DAYS: [u8;12] = [31,28,31,30,31,30,31,31,30,31,30,31];
	const YEARS: [i64;12] = [2001,2002,2003,2005,2100,2200,2300,9300,-4,-101,-201,-301];
	for i in 1..12 {
		for j in YEARS.iter() {
			assert_eq!(number_of_day(*j, i as u8, DAYS[i-1]).unwrap() + 1, number_of_day(*j,
				(i+1) as u8, 1).unwrap());
		}
	}
}

#[test]
fn reject_day_too_big_leap()
{
	const DAYS: [u8;12] = [31,29,31,30,31,30,31,31,30,31,30,31];
	const YEARS: [i64;9] = [1600,2000,2400,2004,2016,2020,4020,-1,-401];
	for i in 1..12 {
		for j in YEARS.iter() {
			assert!(number_of_day(*j, i as u8, DAYS[i-1]+1).is_err());
		}
	}
}

#[test]
fn reject_day_too_big_nonleap()
{
	const DAYS: [u8;12] = [31,28,31,30,31,30,31,31,30,31,30,31];
	const YEARS: [i64;12] = [2001,2002,2003,2005,2100,2200,2300,9300,-4,-101,-201,-301];
	for i in 1..12 {
		for j in YEARS.iter() {
			assert!(number_of_day(*j, i as u8, DAYS[i-1]+1).is_err());
		}
	}
}

#[test]
fn probe_days_2000()
{
	const DAYS: [u8;12] = [31,29,31,30,31,30,31,31,30,31,30,31];
	for i in 1..13 {
		for j in 1..DAYS[i-1] {
			assert_eq!(number_of_day(2000, i as u8, j).unwrap() + 1, number_of_day(2000, i as u8,
				j+1).unwrap());
		}
	}
}

#[test]
fn invalid_months()
{
	assert!(number_of_day(2000, 0, 1).is_err());
	assert!(number_of_day(2000, 13, 1).is_err());
	assert!(number_of_day(0, 1, 1).is_err());
}

#[test]
fn epoch()
{
	assert_eq!(number_of_day(1970, 1, 1).unwrap(), 0);
}

#[test]
fn first_day()
{
	assert_eq!(number_of_day(-25252734927764586, 6, 7).unwrap(), I64_MIN);
}

#[test]
fn last_day()
{
	assert_eq!(number_of_day(25252734927768524, 7, 27).unwrap(), I64_MAX);
}

#[test]
fn past_first_day()
{
	assert!(number_of_day(-25252734927764586, 6, 6).is_err());
	const OFFSETS: [i64;11] = [1,2,3,4,8,12,16,400,800,1200,1600];
	for i in OFFSETS.iter() {
		assert!(number_of_day(-25252734927764586-*i, 6, 7).is_err());
	}
}

#[test]
fn past_last_day()
{
	assert!(number_of_day(25252734927768524, 7, 28).is_err());
	const OFFSETS: [i64;11] = [1,2,3,4,8,12,16,400,800,1200,1600];
	for i in OFFSETS.iter() {
		assert!(number_of_day(25252734927768524+*i, 7, 27).is_err());
	}
}

#[test]
fn julian_offset()
{
	//The offset is 2400001, because the half days combine to full day.
	assert_eq!(number_of_day(-4714, 11, 24).unwrap() + 2400001, number_of_day(1858, 11, 17).unwrap());
}

#[test]
fn julian_offset_1970_mod()
{
	assert_eq!(number_of_day(1858, 11, 17).unwrap() + 40587, number_of_day(1970, 1, 1).unwrap());
}

#[test]
fn timestamp_sub_normal()
{
	assert_eq!(Timestamp::new(123).delta(Timestamp::new(245)), 122);
}

#[test]
fn timestamp_sub_opposite()
{
	assert_eq!(Timestamp::new(245).delta(Timestamp::new(123)), 0);
}

#[test]
fn timestamp_sub_verylarge()
{
	assert_eq!(Timestamp::new(-0x6000000000000000).delta(Timestamp::new(0x6000000000000000)),
		0xC000000000000000);
}

#[test]
fn test_generalized_time_limits()
{
	assert_eq!(number_of_day(1, 1, 1).unwrap(), -719162);
	assert_eq!(number_of_day(9999, 12, 31).unwrap(), 2932896);
	assert_eq!(interpret_timestamp(TimeFormat::GeneralizedTime, b"00010101000000Z").unwrap().unix_time(),
		-62135596800);
	assert_eq!(interpret_timestamp(TimeFormat::GeneralizedTime, b"99991231235959Z").unwrap().unix_time(),
		253402300799);
}

#[cfg(unix)]
#[test]
fn test_wait_unix_ts()
{
	let ts = std::time::SystemTime::now().duration_since(std::time::UNIX_EPOCH).expect("Time travel???");
	let ts2 = ts + std::time::Duration::from_secs(5);
	let wait = crate::wait_for_unix_ts(ts2.as_secs() as i64);
	assert!(wait >= 3_900_000_000 && wait <= 5_000_000_000);
}
