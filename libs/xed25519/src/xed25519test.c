//gcc -g -O3 -o xed25519 xed25519test.c ed25519-donna/ed25519.c -DUSE_64BIT_MATH -DVALGRIND_CT_TEST -DED25519_REFHASH -DED25519_CUSTOMRANDOM -DED25519_SUFFIX=_xed25519
#ifdef VALGRIND_CT_TEST
#include "valgrind/memcheck.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

void curved25519_scalarmult_basepoint_xed25519(unsigned char* p, const unsigned char* s);
int curved25519_scalarmult_xed25519(unsigned char* o, const unsigned char* s, const unsigned char* b);
void ed25519_publickey_xed25519(const unsigned char* sk, unsigned char* pk);
void ed25519_sign_xed25519(const unsigned char* msg, size_t msglen, const unsigned char* sk, const unsigned char* pk, unsigned char* sig);
int ed25519_sign_open_xed25519(const unsigned char* msg, size_t msglen, const unsigned char* pk, const unsigned char* sig);

void ed25519_randombytes_cb_xed25519(unsigned char* buf, size_t buflen)
{
	for(size_t i = 0; i < buflen; i++) buf[i] = 0;		//Not actually random...
}


void call_only_once_xed25519(void(*fn)())
{
	static int flag = 0;
	if(!flag) fn();
	flag = 1;
}

void zeroize_xed25519(uint8_t* buf, size_t size)
{
	for(size_t i = 0; i < size; i++)
		buf[i] = 0;
}

void print(uint8_t* buf, size_t size)
{
	for(size_t i = 0; i < size; i++)
		printf("%02x", buf[i]);
	printf("\n");
}

int main()
{
	uint8_t x25519_sk[32];
	uint8_t x25519_pk[32];
	uint8_t x25519_pk2[32];
	uint8_t x25519_shared[32];
	uint8_t ed25519_sk[32];
	uint8_t ed25519_pk[32];
	uint8_t ed25519_msg[256];
	uint8_t ed25519_sig[64];
	VALGRIND_MAKE_MEM_UNDEFINED(x25519_sk, 32);
	VALGRIND_MAKE_MEM_UNDEFINED(x25519_pk, 32);
	VALGRIND_MAKE_MEM_UNDEFINED(x25519_pk2, 32);
	VALGRIND_MAKE_MEM_UNDEFINED(x25519_shared, 32);
	VALGRIND_MAKE_MEM_UNDEFINED(ed25519_pk, 32);
	VALGRIND_MAKE_MEM_UNDEFINED(ed25519_sk, 32);
	VALGRIND_MAKE_MEM_UNDEFINED(ed25519_msg, 256);
	VALGRIND_MAKE_MEM_UNDEFINED(ed25519_sig, 64);
	curved25519_scalarmult_basepoint_xed25519(x25519_pk, x25519_sk);
	curved25519_scalarmult_xed25519(x25519_shared, x25519_sk, x25519_pk2);
	VALGRIND_MAKE_MEM_DEFINED(x25519_pk, 32);			//To shut up spurious errors.
	VALGRIND_MAKE_MEM_DEFINED(x25519_shared, 32);		//To shut up spurious errors.
	print(x25519_pk, 32);
	print(x25519_shared, 32);
	ed25519_publickey_xed25519(ed25519_pk, ed25519_sk);
	ed25519_sign_xed25519(ed25519_msg, 256, ed25519_sk, ed25519_pk, ed25519_sig);
	VALGRIND_MAKE_MEM_DEFINED(ed25519_pk, 32);		//To shut up spurious errors.
	VALGRIND_MAKE_MEM_DEFINED(ed25519_sig, 64);		//To shut up spurious errors.
	print(ed25519_pk, 32);
	print(ed25519_sig, 64);
}

#endif
