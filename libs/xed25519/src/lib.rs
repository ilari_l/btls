//!Low level implementation of Ed25519 and X25519.
//!
//!This crate provodes a low-level implementations of the Ed25519 signature algorithm, and the X25519 key
//!agreement.
//!
//!The most important items are:
//!
//! * Generate a X25519 public key from private key: [`x25519_generate`](fn.x25519_generate.html)
//! * Perform a X25519 key agreement: [`x25519_agree`](fn.x25519_agree.html)
//! * Generate a Ed25519 public key from private key: [`ed25519_pubkey`](fn.ed25519_pubkey.html)
//! * Sign a message using Ed25519: [`ed25519_sign`](fn.ed25519_sign.html)
//! * Verify a message using Ed25519: [`ed25519_verify`](fn.ed25519_verify.html)
#![forbid(missing_docs)]
#![allow(unsafe_code)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]

#[cfg(test)] #[macro_use] extern crate std;
use btls_aux_xed25519_cimpl::curved25519_scalarmult_basepoint_xed25519;
use btls_aux_xed25519_cimpl::curved25519_scalarmult_xed25519;
use btls_aux_xed25519_cimpl::ed25519_publickey_xed25519;
use btls_aux_xed25519_cimpl::ed25519_sign_open_xed25519;
use btls_aux_xed25519_cimpl::ed25519_sign_xed25519;
use btls_aux_xed25519_cimpl::ed25519_sign_callback_xed25519;
use btls_aux_xed25519_cimpl::ed25519_verify_callback_xed25519;
pub use btls_aux_xed25519_cimpl::Hasher;


///Generate the X25519 public key corresponding to a private key.
///
///The private key is `privkey` and the result is written into `pubkey`. The input private key should be randomly
///and uniformly chosen over all octet strings of appropriate length.
///
///Note that despite this function having the same function signature as [`ed25519_pubkey()`](fn.ed25519_pubkey.html)
///this function does not produce the same result.
pub fn x25519_generate(pubkey: &mut [u8;32], privkey: &[u8;32])
{
	unsafe{curved25519_scalarmult_basepoint_xed25519(pubkey.as_mut_ptr(), privkey.as_ptr())};
}

///Perform X25519 key agreement.
///
///The private key is `privkey`, the peer public key is `peerpub` and the result is written in `shared`.
///
///The key agreement has the the following properties:
///
/// * It is comutative with respect to exchange of participants. That is, the result of key agreement between
///private key A and public key B, and private key B and public key A, is identical.
/// * It is computationally very hard to compute the key exchange result from just the public keys (without a
///quantum computer).
///
///Use [`x25519_generate`](fn.x25519_generate.html) to compute public key from private key.
///
///Note that after performing the key agreement, one should check that the result is not all zeroes. The all-zeroes
///output can be triggered by certain invalid public keys. While secure key exchange protocols can withstand such
///failures, more poorly done ones (e.g., the original `TLS 1.2` key exchange) are broken in such circumstances.
pub fn x25519_agree(shared: &mut [u8;32], privkey: &[u8;32], peerpub: &[u8;32])
{
	unsafe{curved25519_scalarmult_xed25519(shared.as_mut_ptr(), privkey.as_ptr(), peerpub.as_ptr())};
}

///Generate the `Ed25519` public key corresponding to a private key.
///
///The private key is `privkey` and the result is written into `pubkey`. The input private key should be randomly
///and uniformly chosen over all octet strings of appropriate length.
///
///Note that despite this function having the same function signature as
///[`x25519_generate()`](fn.x25519_generate.html) this function does not produce the same result.
pub fn ed25519_pubkey(pubkey: &mut [u8; 32], privkey: &[u8; 32])
{
	unsafe{ed25519_publickey_xed25519(privkey.as_ptr(), pubkey.as_mut_ptr())};
}

///Sign a piece of data using Ed25519 private key.
///
///The private key is `privkey`, the corresponding public key is `pubkey`, the message is `message` and the
///signature is written to `signature`.
///
///The message can be of any length up to `2^125-1` octets.
///
///Use [`ed25519_pubkey`](fn.ed25519_pubkey.html) to compute public keys from private keys.
///
///Note that if the private key and public key passed do not correspond to one another, this function will silently
///compute incorrect signature.
///
///Also note that these signature may be forged using a quantum computer.
pub fn ed25519_sign(privkey: &[u8; 32], pubkey: &[u8; 32], message: &[u8], signature: &mut [u8;64])
{
	unsafe{ed25519_sign_xed25519(message.as_ptr(), message.len(), privkey.as_ptr(), pubkey.as_ptr(),
		signature.as_mut_ptr())};
}

///Like `ed25519_sign()`, but takes the message via callback.
///
///Also, instead of deterministically generating the urnonce, this takes 64 random bytes that *MUST BE FULLY
///RANDOMLY SET FOR EACH SIGNATURE OR ALL SECURITY IS LOST*.
pub fn ed25519_sign_callback(privkey: &[u8; 32], pubkey: &[u8; 32], signature: &mut [u8;64], random: &[u8; 64],
	message: &mut dyn FnMut(&mut Hasher))
{
	unsafe{ed25519_sign_callback_xed25519(privkey.as_ptr(), pubkey.as_ptr(), signature.as_mut_ptr(),
		random.as_ptr(), message)};
}

///Verify an Ed25519 signature on piece of data using public key.
///
///The public key is `pubkey`, the message is `message` and the purported signature is `signature`.
///
///On success, returns `Ok(())`. If the signature does not verify, returns `Err(())`.
///
///Note that the signature does not bind the public key due to existence of 8 weak keys. That is, there can be
///multiple messages and public keys that produce the same signature. However, any such signature must purport to
///be using one of the weak keys, which are listed below:
///
/// * 0000000000000000000000000000000000000000000000000000000000000000
/// * 0000000000000000000000000000000000000000000000000000000000000080
/// * 0100000000000000000000000000000000000000000000000000000000000000
/// * 26E8958FC2B227B045C3F489F2EF98F0D5DFAC05D3C63339B13802886D53FC05
/// * 26E8958FC2B227B045C3F489F2EF98F0D5DFAC05D3C63339B13802886D53FC85
/// * C7176A703D4DD84FBA3C0B760D10670F2A2053FA2C39CCC64EC7FD7792AC037A
/// * C7176A703D4DD84FBA3C0B760D10670F2A2053FA2C39CCC64EC7FD7792AC03FA
/// * ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F
///
///Note that flipping the most significant bit of the last octet and/or the least significant bit of the first octet
///in any weak key either gives another weak key, or an invalid key where there are no accepted signatures. Such
///masking reduces the above set to 4 partially masked keys.
pub fn ed25519_verify(pubkey: &[u8; 32], message: &[u8], signature: &[u8;64]) -> Result<(), ()>
{
	let r = unsafe{ed25519_sign_open_xed25519(message.as_ptr(), message.len(), pubkey.as_ptr(),
		signature.as_ptr())};
	if r < 0 { Err(()) } else { Ok(()) }
}

///Like `ed25519_verify()`, but takes the message via callback.
pub fn ed25519_verify_callback(pubkey: &[u8; 32], signature: &[u8;64], message: &mut dyn FnMut(&mut Hasher)) ->
	Result<(), ()>
{
	let r = unsafe{ed25519_verify_callback_xed25519(pubkey.as_ptr(), signature.as_ptr(), message)};
	if r < 0 { Err(()) } else { Ok(()) }
}


#[cfg(test)]
mod test;
