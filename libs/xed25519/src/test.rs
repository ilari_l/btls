use super::*;
use std::borrow::ToOwned;
use std::fs::File;
use std::io::Read;
use std::vec::Vec;

#[test]
fn reference_x25519_key_exchange()
{
	let mut base = [0;32];
	base[0] = 9;
	let apriv2 = include_bytes!("kex_a.priv");
	let bpriv2 = include_bytes!("kex_b.priv");
	let apubref = include_bytes!("kex_a.pub");
	let bpubref = include_bytes!("kex_b.pub");
	let shrref = include_bytes!("kex_ab.shr");
	let mut apriv = apriv2.clone();
	let mut bpriv = bpriv2.clone();
	//There is no internal clamp.
	apriv[0] &= 248; apriv[31] &= 127; apriv[31] |= 64;
	bpriv[0] &= 248; bpriv[31] &= 127; bpriv[31] |= 64;
	let mut apub = [0;32];
	let mut bpub = [0;32];
	let mut apub2 = [0;32];
	let mut bpub2 = [0;32];
	let mut ashr = [0;32];
	let mut bshr = [0;32];
	x25519_generate(&mut apub2, &apriv);
	x25519_generate(&mut bpub2, &bpriv);
	x25519_agree(&mut apub, &apriv, &base);
	x25519_agree(&mut bpub, &bpriv, &base);
	assert_eq!(&apub[..], &apub2[..]);
	assert_eq!(&bpub[..], &bpub2[..]);
	assert_eq!(&apub[..], &apubref[..]);
	assert_eq!(&bpub[..], &bpubref[..]);
	x25519_agree(&mut ashr, &apriv, &bpub);
	x25519_agree(&mut bshr, &bpriv, &apub);
	assert_eq!(&ashr[..], &bshr[..]);
	assert_eq!(&ashr[..], &shrref[..]);
}

fn test_ed25519_sig(pubkey: [u8;32], msg: &[u8], sig: [u8;64])
{
	assert!(ed25519_verify(&pubkey, msg, &sig).is_ok());
	let mut nsg = msg[..].to_owned();
	for i in 0..8*nsg.len() {
		nsg[i/8] ^= 1 << i % 8;
		assert!(ed25519_verify(&pubkey, &nsg, &sig).is_err());
		nsg[i/8] ^= 1 << i % 8;
	}
	let mut bubkey = pubkey;
	for i in 0..8*bubkey.len() {
		bubkey[i/8] ^= 1 << i % 8;
		assert!(ed25519_verify(&bubkey, msg, &sig).is_err());
		bubkey[i/8] ^= 1 << i % 8;
	}
	let mut bsig = sig;
	for i in 0..8*bsig.len() {
		bsig[i/8] ^= 1 << i % 8;
		assert!(ed25519_verify(&pubkey, msg, &bsig).is_err());
		bsig[i/8] ^= 1 << i % 8;
	}
}

#[test]
fn reference_ed25519_signature()
{
	let privkey = include_bytes!("sig_a.priv");
	let pubkeyref = include_bytes!("sig_a.pub");
	let mut pubkey = [0;32];
	let mut sig = [0; 64];
	let msg = include_bytes!("sig_a.msg");
	let sigref = include_bytes!("sig_a.sig");
	ed25519_pubkey(&mut pubkey, privkey);
	assert_eq!(&pubkey[..], &pubkeyref[..]);
	ed25519_sign(privkey, &pubkey, msg, &mut sig);
	assert_eq!(&sig[..], &sigref[..]);
	test_ed25519_sig(pubkey, msg, sig);
}

fn test_ed25519_once()
{
	let mut privkey = [0;32];
	let mut pubkey = [0;32];
	let mut sig = [0;64];
	let mut msg = Vec::new();

	//Random keys.
	File::open("/dev/urandom").and_then(|mut fp|{
		let mut msglen = [0];
		fp.read_exact(&mut msglen)?;
		msg.resize(msglen[0] as usize, 0);
		fp.read_exact(&mut msg)?;
		fp.read_exact(&mut privkey)
	}).unwrap();
	ed25519_pubkey(&mut pubkey, &privkey);
	ed25519_sign(&privkey, &pubkey, &msg, &mut sig);
	test_ed25519_sig(pubkey, &msg, sig);
}

fn test_key_agreement_once()
{
	let mut base = [0;32];
	base[0] = 9;
	let mut apriv = [0;32]; let mut bpriv = [0;32]; let mut cpriv = [0;32];
	let mut apub = [0;32]; let mut bpub = [0;32]; let mut cpub = [0;32];
	let mut apub2 = [0;32]; let mut bpub2 = [0;32]; let mut cpub2 = [0;32];
	let mut abshr = [0;32]; let mut bashr = [0;32];
	let mut acshr = [0;32]; let mut cashr = [0;32];
	let mut bcshr = [0;32]; let mut cbshr = [0;32];
	//Random keys.
	File::open("/dev/urandom").and_then(|mut fp|{
		fp.read_exact(&mut apriv)?;
		fp.read_exact(&mut bpriv)?;
		fp.read_exact(&mut cpriv)
	}).unwrap();
	//Public keys. Calculate both using generate and agree. The two better match up!
	x25519_generate(&mut apub2, &apriv);
	x25519_generate(&mut bpub2, &bpriv);
	x25519_generate(&mut cpub2, &cpriv);
	x25519_agree(&mut apub, &apriv, &base);
	x25519_agree(&mut bpub, &bpriv, &base);
	x25519_agree(&mut cpub, &cpriv, &base);
	assert_eq!(&apub[..], &apub2[..]);
	assert_eq!(&bpub[..], &bpub2[..]);
	assert_eq!(&cpub[..], &cpub2[..]);
	//Calculate all pairwise products. The sides better commute, but the keys better not!
	x25519_agree(&mut abshr, &apriv, &bpub);
	x25519_agree(&mut bashr, &bpriv, &apub);
	x25519_agree(&mut acshr, &apriv, &cpub);
	x25519_agree(&mut cashr, &cpriv, &apub);
	x25519_agree(&mut bcshr, &bpriv, &cpub);
	x25519_agree(&mut cbshr, &cpriv, &bpub);
	assert_eq!(&abshr[..], &bashr[..]); assert_eq!(&acshr[..], &cashr[..]); assert_eq!(&bcshr[..], &cbshr[..]);
	assert_ne!(&abshr[..], &acshr[..]); assert_ne!(&abshr[..], &bcshr[..]); assert_ne!(&acshr[..], &bcshr[..]);
}

#[test]
fn x25519_3wise_1k()
{
	for _ in 0..1000 { test_key_agreement_once(); }
}

#[test]
fn ed25519_50()
{
	for _ in 0..50 { test_ed25519_once(); }
}

#[test]
fn test_callback_one_rngdep()
{
	let mut sig1 = [0;64];
	let mut sig2 = [0;64];
	let mut sig3 = [0;64];
	let mut rng = [0;64];
	let mut privkey = [0;32];
	let mut pubkey = [0;32];
	let mut msg = [0;700];
	//Random keys.
	File::open("/dev/urandom").and_then(|mut fp|{
		fp.read_exact(&mut msg)?;
		fp.read_exact(&mut privkey)?;
		fp.read_exact(&mut rng)?;
		Ok(())
	}).unwrap();
	ed25519_pubkey(&mut pubkey, &privkey);
	ed25519_sign_callback(&privkey, &pubkey, &mut sig1, &rng, &mut |h|h.append(&msg));
	msg[0] += 1;
	ed25519_sign_callback(&privkey, &pubkey, &mut sig2, &rng, &mut |h|h.append(&msg));
	rng[0] += 1;
	ed25519_sign_callback(&privkey, &pubkey, &mut sig3, &rng, &mut |h|h.append(&msg));
	//Sig1 should have the same first 32 bytes as sig2, but different from sig3. The last 32 bytes should
	//differ.
	assert!(&sig1[..32] == &sig2[..32]);
	assert!(&sig1[..32] != &sig3[..32]);
	assert!(&sig1[32..] != &sig2[32..]);
	assert!(&sig1[32..] != &sig3[32..]);
	assert!(&sig2[32..] != &sig3[32..]);
}

#[test]
fn test_callback_one_a()
{
	let mut sig = [0;64];
	let mut rng = [0;64];
	let mut privkey = [0;32];
	let mut pubkey = [0;32];
	let mut msg = [0;700];
	//Random keys.
	File::open("/dev/urandom").and_then(|mut fp|{
		fp.read_exact(&mut msg)?;
		fp.read_exact(&mut privkey)?;
		fp.read_exact(&mut rng)?;
		Ok(())
	}).unwrap();
	ed25519_pubkey(&mut pubkey, &privkey);
	ed25519_sign_callback(&privkey, &pubkey, &mut sig, &rng, &mut |h|h.append(&msg));
	test_ed25519_sig(pubkey, &msg, sig);
}

#[test]
fn test_callback_one_b()
{
	let mut sig = [0;64];
	let mut privkey = [0;32];
	let mut pubkey = [0;32];
	let mut msg = [0;700];
	//Random keys.
	File::open("/dev/urandom").and_then(|mut fp|{
		fp.read_exact(&mut msg)?;
		fp.read_exact(&mut privkey)?;
		Ok(())
	}).unwrap();
	ed25519_pubkey(&mut pubkey, &privkey);
	ed25519_sign(&privkey, &pubkey, &msg, &mut sig);
	assert!(ed25519_verify_callback(&pubkey, &mut sig, &mut |h|h.append(&msg)).is_ok());
	assert!(ed25519_verify_callback(&pubkey, &mut sig, &mut |h|h.append(&msg[..699])).is_err());
}

#[test]
fn test_splitting()
{
	let mut sig = [0;64];
	let mut privkey = [0;32];
	let mut pubkey = [0;32];
	let mut msg = [0;7000];
	//Random keys.
	File::open("/dev/urandom").and_then(|mut fp|{
		fp.read_exact(&mut msg)?;
		fp.read_exact(&mut privkey)?;
		Ok(())
	}).unwrap();
	ed25519_pubkey(&mut pubkey, &privkey);
	ed25519_sign(&privkey, &pubkey, &msg, &mut sig);
	assert!(ed25519_verify_callback(&pubkey, &mut sig, &mut |h|{
		h.append(&msg[..127]);
		h.append(&msg[127..383]);
		h.append(&msg[383..512]);
		h.append(&msg[512..1024]);
		h.append(&msg[1024..]);
	}).is_ok());
}
