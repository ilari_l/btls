#include "valgrind/memcheck.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

void ed25519_randombytes_cb_xed25519(uint8_t* p, size_t l)
{
	(void)p; (void)l;
	abort();
}

void curved25519_scalarmult_basepoint_xed25519(uint8_t* p, const uint8_t* s);
int curved25519_scalarmult_xed25519(uint8_t* o, const uint8_t* s, const uint8_t* b);
void ed25519_publickey_xed25519(const uint8_t* sk, uint8_t* pk);
void ed25519_sign_xed25519(const uint8_t* msg, size_t msglen, const uint8_t* sk,
	const uint8_t* pk, uint8_t* sig);

void print(uint8_t* buf, size_t size)
{
	for(size_t i = 0; i < size; i++)
		printf("%02x", buf[i]);
	printf("\n");
}

int main()
{
	uint8_t x25519_sk[32];
	uint8_t x25519_pk[32];
	uint8_t x25519_pk2[32];
	uint8_t x25519_shared[32];
	uint8_t ed25519_sk[32];
	uint8_t ed25519_pk[32];
	uint8_t ed25519_msg[256];
	uint8_t ed25519_sig[64];
	VALGRIND_MAKE_MEM_UNDEFINED(x25519_sk, 32);
	VALGRIND_MAKE_MEM_UNDEFINED(x25519_pk, 32);
	VALGRIND_MAKE_MEM_UNDEFINED(x25519_pk2, 32);
	VALGRIND_MAKE_MEM_UNDEFINED(x25519_shared, 32);
	VALGRIND_MAKE_MEM_UNDEFINED(ed25519_pk, 32);
	VALGRIND_MAKE_MEM_UNDEFINED(ed25519_sk, 32);
	VALGRIND_MAKE_MEM_UNDEFINED(ed25519_msg, 256);
	VALGRIND_MAKE_MEM_UNDEFINED(ed25519_sig, 64);
	curved25519_scalarmult_basepoint_xed25519(x25519_pk, x25519_sk);
	curved25519_scalarmult_xed25519(x25519_shared, x25519_sk, x25519_pk2);
	VALGRIND_MAKE_MEM_DEFINED(x25519_pk, 32);			//To shut up spurious errors.
	VALGRIND_MAKE_MEM_DEFINED(x25519_shared, 32);		//To shut up spurious errors.
	print(x25519_pk, 32);
	print(x25519_shared, 32);
	ed25519_publickey_xed25519(ed25519_pk, ed25519_sk);
	ed25519_sign_xed25519(ed25519_msg, 256, ed25519_sk, ed25519_pk, ed25519_sig);
	VALGRIND_MAKE_MEM_DEFINED(ed25519_pk, 32);		//To shut up spurious errors.
	VALGRIND_MAKE_MEM_DEFINED(ed25519_sig, 64);		//To shut up spurious errors.
	print(ed25519_pk, 32);
	print(ed25519_sig, 64);
}
