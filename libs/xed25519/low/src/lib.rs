//!Low-level bindings to curve25519-donna and ed25519-donna libraries, offering high performance implementation of
//!X25519 and Ed25519 cryptographic primitives.
//!
//!This is its own crate because this crate is a "highlander"; there can be only one crate in the entiere program.
//!Thus it must remain semver-compatible, which is much easier if the crate does as little as possible.
//!
//!It is only meant to be used by the [`btls-aux-xed25519`](../btls_aux_xed25519/index.html) crate.
#![no_std]
#![allow(unsafe_code)]
#![allow(improper_ctypes)]
#![allow(unknown_lints)]
#![allow(bare_trait_objects)]
use core::mem::transmute;

#[doc(hidden)]
#[no_mangle]
pub unsafe extern fn ed25519_randombytes_cb_xed25519(_ptr: *mut u8, _len: usize)
{
	//This should never be called (only batch verification needs it and we do not support batch verification),
	//and as such, does nothing.
}

//Dirty trick: This is the context structure for the hash, so it can be directly manipulated via Rust code.
#[repr(C)]
struct HashContext
{
	h: [u64;8],
	t: [u64;2],
	leftover: u32,
	buffer: [u8;128],
}

//The SHA-512 round constants I.
static SHA512_RK1: [u64;16] = [
	0x428a2f98d728ae22, 0x7137449123ef65cd, 0xb5c0fbcfec4d3b2f, 0xe9b5dba58189dbbc,
	0x3956c25bf348b538, 0x59f111f1b605d019, 0x923f82a4af194f9b, 0xab1c5ed5da6d8118,
	0xd807aa98a3030242, 0x12835b0145706fbe, 0x243185be4ee4b28c, 0x550c7dc3d5ffb4e2,
	0x72be5d74f27b896f, 0x80deb1fe3b1696b1, 0x9bdc06a725c71235, 0xc19bf174cf692694,
];

//The SHA-512 round constants II.
static SHA512_RK2: [u64;16] = [
	0xe49b69c19ef14ad2, 0xefbe4786384f25e3, 0x0fc19dc68b8cd5b5, 0x240ca1cc77ac9c65,
	0x2de92c6f592b0275, 0x4a7484aa6ea6e483, 0x5cb0a9dcbd41fbd4, 0x76f988da831153b5,
	0x983e5152ee66dfab, 0xa831c66d2db43210, 0xb00327c898fb213f, 0xbf597fc7beef0ee4,
	0xc6e00bf33da88fc2, 0xd5a79147930aa725, 0x06ca6351e003826f, 0x142929670a0e6e70,
];

//The SHA-512 round constants III.
static SHA512_RK3: [u64;16] = [
	0x27b70a8546d22ffc, 0x2e1b21385c26c926, 0x4d2c6dfc5ac42aed, 0x53380d139d95b3df,
	0x650a73548baf63de, 0x766a0abb3c77b2a8, 0x81c2c92e47edaee6, 0x92722c851482353b,
	0xa2bfe8a14cf10364, 0xa81a664bbc423001, 0xc24b8b70d0f89791, 0xc76c51a30654be30,
	0xd192e819d6ef5218, 0xd69906245565a910, 0xf40e35855771202a, 0x106aa07032bbd1b8,
];

//The SHA-512 round constants IV.
static SHA512_RK4: [u64;16] = [
	0x19a4c116b8d2d0c8, 0x1e376c085141ab53, 0x2748774cdf8eeb99, 0x34b0bcb5e19b48a8,
	0x391c0cb3c5c95a63, 0x4ed8aa4ae3418acb, 0x5b9cca4f7763e373, 0x682e6ff3d6b2b8a3,
	0x748f82ee5defb2fc, 0x78a5636f43172f60, 0x84c87814a1f0ab72, 0x8cc702081a6439ec,
	0x90befffa23631e28, 0xa4506cebde82bde9, 0xbef9a3f7b2c67915, 0xc67178f2e372532b,
];

//The SHA-512 round constants V.
static SHA512_RK5: [u64;16] = [
	0xca273eceea26619c, 0xd186b8c721c0c207, 0xeada7dd6cde0eb1e, 0xf57d4f7fee6ed178,
	0x06f067aa72176fba, 0x0a637dc5a2c898a6, 0x113f9804bef90dae, 0x1b710b35131c471b,
	0x28db77f523047d84, 0x32caab7b40c72493, 0x3c9ebe0a15c9bebc, 0x431d67c49c100d4c,
	0x4cc5d4becb3e42b6, 0x597f299cfc657e2a, 0x5fcb6fab3ad6faec, 0x6c44198c4a475817
];

#[inline(always)]
fn majority(a: u64, b: u64, c: u64) -> u64 { (a & b) ^ (a & c) ^ (b & c) }
#[inline(always)]
fn choose(k: u64, a: u64, b: u64) -> u64 { (k & a) | ((!k) & b) }
#[inline(always)]
fn sigma0(num: u64) -> u64 { num.rotate_right(28) ^ num.rotate_right(34) ^ num.rotate_right(39) }
#[inline(always)]
fn sigma1(num: u64) -> u64 { num.rotate_right(14) ^ num.rotate_right(18) ^ num.rotate_right(41) }
#[inline(always)]
fn esigma0(num: u64) -> u64 { num.rotate_right(1) ^ num.rotate_right(8) ^ (num >> 7) }
#[inline(always)]
fn esigma1(num: u64) -> u64 { num.rotate_right(19) ^ num.rotate_right(61) ^ (num >> 6) }

#[inline(always)]
#[allow(non_snake_case)]
fn sha512_drnd(s: &mut [u64;8], A: usize, B: usize, C: usize, D: usize, E: usize, F: usize, G: usize, H: usize,
	W: u64, K: u64)
{
	let x = s[H].wrapping_add(K).wrapping_add(W).wrapping_add(sigma1(s[E])).
		wrapping_add(choose(s[E], s[F], s[G]));
	s[D] = s[D].wrapping_add(x);
	s[H] = x.wrapping_add(sigma0(s[A])).wrapping_add(majority(s[A], s[B], s[C]));
}

#[inline(always)]
#[allow(non_snake_case)]
fn sha512_wrnd(W0: u64, W1: u64, W2: u64, W3: u64) -> u64
{
	let xsigma0 = esigma0(W1);
	let xsigma1 = esigma1(W3);
	W0.wrapping_add(xsigma0).wrapping_add(xsigma1).wrapping_add(W2)
}

#[allow(non_snake_case)]
fn do_sha2_16_rounds_1(s: &mut [u64;8], DB: &mut [u64;16], RK: &[u64;16])
{
	sha512_drnd(s,0,1,2,3,4,5,6,7,DB[ 0], RK[ 0]);
	sha512_drnd(s,7,0,1,2,3,4,5,6,DB[ 1], RK[ 1]);
	sha512_drnd(s,6,7,0,1,2,3,4,5,DB[ 2], RK[ 2]);
	sha512_drnd(s,5,6,7,0,1,2,3,4,DB[ 3], RK[ 3]);
	sha512_drnd(s,4,5,6,7,0,1,2,3,DB[ 4], RK[ 4]);
	sha512_drnd(s,3,4,5,6,7,0,1,2,DB[ 5], RK[ 5]);
	sha512_drnd(s,2,3,4,5,6,7,0,1,DB[ 6], RK[ 6]);
	sha512_drnd(s,1,2,3,4,5,6,7,0,DB[ 7], RK[ 7]);
	sha512_drnd(s,0,1,2,3,4,5,6,7,DB[ 8], RK[ 8]);
	sha512_drnd(s,7,0,1,2,3,4,5,6,DB[ 9], RK[ 9]);
	sha512_drnd(s,6,7,0,1,2,3,4,5,DB[10], RK[10]);
	sha512_drnd(s,5,6,7,0,1,2,3,4,DB[11], RK[11]);
	sha512_drnd(s,4,5,6,7,0,1,2,3,DB[12], RK[12]);
	sha512_drnd(s,3,4,5,6,7,0,1,2,DB[13], RK[13]);
	sha512_drnd(s,2,3,4,5,6,7,0,1,DB[14], RK[14]);
	sha512_drnd(s,1,2,3,4,5,6,7,0,DB[15], RK[15]);
}

#[allow(non_snake_case)]
fn do_sha2_16_rounds_2(s: &mut [u64;8], DB: &mut [u64;16], RK: &[u64;16])
{
	DB[ 0] = sha512_wrnd(DB[ 0], DB[ 1], DB[ 9], DB[14]); sha512_drnd(s,0,1,2,3,4,5,6,7,DB[ 0], RK[ 0]);
	DB[ 1] = sha512_wrnd(DB[ 1], DB[ 2], DB[10], DB[15]); sha512_drnd(s,7,0,1,2,3,4,5,6,DB[ 1], RK[ 1]);
	DB[ 2] = sha512_wrnd(DB[ 2], DB[ 3], DB[11], DB[ 0]); sha512_drnd(s,6,7,0,1,2,3,4,5,DB[ 2], RK[ 2]);
	DB[ 3] = sha512_wrnd(DB[ 3], DB[ 4], DB[12], DB[ 1]); sha512_drnd(s,5,6,7,0,1,2,3,4,DB[ 3], RK[ 3]);
	DB[ 4] = sha512_wrnd(DB[ 4], DB[ 5], DB[13], DB[ 2]); sha512_drnd(s,4,5,6,7,0,1,2,3,DB[ 4], RK[ 4]);
	DB[ 5] = sha512_wrnd(DB[ 5], DB[ 6], DB[14], DB[ 3]); sha512_drnd(s,3,4,5,6,7,0,1,2,DB[ 5], RK[ 5]);
	DB[ 6] = sha512_wrnd(DB[ 6], DB[ 7], DB[15], DB[ 4]); sha512_drnd(s,2,3,4,5,6,7,0,1,DB[ 6], RK[ 6]);
	DB[ 7] = sha512_wrnd(DB[ 7], DB[ 8], DB[ 0], DB[ 5]); sha512_drnd(s,1,2,3,4,5,6,7,0,DB[ 7], RK[ 7]);
	DB[ 8] = sha512_wrnd(DB[ 8], DB[ 9], DB[ 1], DB[ 6]); sha512_drnd(s,0,1,2,3,4,5,6,7,DB[ 8], RK[ 8]);
	DB[ 9] = sha512_wrnd(DB[ 9], DB[10], DB[ 2], DB[ 7]); sha512_drnd(s,7,0,1,2,3,4,5,6,DB[ 9], RK[ 9]);
	DB[10] = sha512_wrnd(DB[10], DB[11], DB[ 3], DB[ 8]); sha512_drnd(s,6,7,0,1,2,3,4,5,DB[10], RK[10]);
	DB[11] = sha512_wrnd(DB[11], DB[12], DB[ 4], DB[ 9]); sha512_drnd(s,5,6,7,0,1,2,3,4,DB[11], RK[11]);
	DB[12] = sha512_wrnd(DB[12], DB[13], DB[ 5], DB[10]); sha512_drnd(s,4,5,6,7,0,1,2,3,DB[12], RK[12]);
	DB[13] = sha512_wrnd(DB[13], DB[14], DB[ 6], DB[11]); sha512_drnd(s,3,4,5,6,7,0,1,2,DB[13], RK[13]);
	DB[14] = sha512_wrnd(DB[14], DB[15], DB[ 7], DB[12]); sha512_drnd(s,2,3,4,5,6,7,0,1,DB[14], RK[14]);
	DB[15] = sha512_wrnd(DB[15], DB[ 0], DB[ 8], DB[13]); sha512_drnd(s,1,2,3,4,5,6,7,0,DB[15], RK[15]);
}

impl HashContext
{
	#[inline(never)]
	fn compress(&mut self)
	{
		//T is only incremented on compress.
		self.t[0] += 1024; if self.t[0] == 0 { self.t[1] += 1; }
		//Load b as big-endian.
		let mut b: [u64;16] = unsafe{transmute(self.buffer)};
		for i in 0..16 { b[i] = b[i].to_be(); }
		let mut h: [u64;8] = self.h;
		do_sha2_16_rounds_1(&mut h, &mut b, &SHA512_RK1);
		do_sha2_16_rounds_2(&mut h, &mut b, &SHA512_RK2);
		do_sha2_16_rounds_2(&mut h, &mut b, &SHA512_RK3);
		do_sha2_16_rounds_2(&mut h, &mut b, &SHA512_RK4);
		do_sha2_16_rounds_2(&mut h, &mut b, &SHA512_RK5);
		for i in 0..8 { self.h[i] = self.h[i].wrapping_add(h[i]); }
	}
}

type HCb = extern fn(*mut (), *const u8, usize);
type Cb = extern fn(HCb, *mut (), *mut ());

struct WrapCb<'a>(&'a mut FnMut(&mut Hasher));

///Message hasher.
pub struct Hasher(*mut ());

impl Hasher
{
	#[inline(never)]
	fn append_slow(ctx: &mut HashContext, mut mfrag: &[u8])
	{ unsafe {
		//Assume buffer got filled.
		let sz = 128 - ctx.leftover as usize;
		if sz > mfrag.len() { return; }		//Should never happen.
		let (head, tail) = mfrag.split_at(sz);
		core::ptr::copy_nonoverlapping(head.as_ptr(),
			ctx.buffer.as_mut_ptr().offset(ctx.leftover as isize), head.len());
		mfrag = tail;
		ctx.compress();
		//Whole blocks.
		while mfrag.len() >= 128 {
			let (head, tail) = mfrag.split_at(128);
			core::ptr::copy_nonoverlapping(head.as_ptr(), ctx.buffer.as_mut_ptr(), 128);
			mfrag = tail;
			ctx.compress();
		}
		//Tail
		core::ptr::copy_nonoverlapping(mfrag.as_ptr(), ctx.buffer.as_mut_ptr(), mfrag.len());
		ctx.leftover = mfrag.len() as u32;
	}}
	///Append data.
	#[inline(always)]
	pub fn append(&mut self, mfrag: &[u8])
	{
		let ctx: &mut HashContext = unsafe{transmute(self.0)};
		if ctx.leftover as usize + mfrag.len() < 128 { unsafe{
			core::ptr::copy_nonoverlapping(mfrag.as_ptr(),
				ctx.buffer.as_mut_ptr().offset(ctx.leftover as isize), mfrag.len());
			ctx.leftover += mfrag.len() as u32;
		}} else {
			Self::append_slow(ctx, mfrag);
		}
	}
}

extern fn hasher_trampoline(_: HCb, hctx: *mut (), ctx: *mut ())
{
	let ctx: &mut WrapCb = unsafe{transmute(ctx)};
	let mut h = Hasher(hctx);
	(*ctx.0)(&mut h);
}

///Do Ed25519 signing. Both sk and pk are 32 bytes, sig and rng are 64 bytes.
pub unsafe fn ed25519_sign_callback_xed25519(sk: *const u8, pk: *const u8, sig: *mut u8,
	rng: *const u8, msg: &mut FnMut(&mut Hasher))
{
	let mut msg = WrapCb(msg);
	let msg = &mut msg;
	sign_callback(sk, pk, sig, rng, hasher_trampoline, transmute(msg))
}

///Do Ed25519 verification. Pk is 32 bytes, sig is 64 bytes. Returns 0 if signature
//verifies, -1 if it does not.
pub unsafe fn ed25519_verify_callback_xed25519(pk: *const u8, sig: *const u8, msg: &mut FnMut(&mut Hasher)) -> i32
{
	let mut msg = WrapCb(msg);
	let msg = &mut msg;
	verify_callback(pk, sig, hasher_trampoline, transmute(msg))
}

extern
{
	///Do X25519 basepoint scalarmult. p and s are 32 bytes.
	pub fn curved25519_scalarmult_basepoint_xed25519(p: *mut u8, s: *const u8);
	///Do X25519 variable-base scalarmult. p, s and b are 32 bytes.
	pub fn curved25519_scalarmult_xed25519(o: *mut u8, s: *const u8, b: *const u8) -> i32;
	///Do Ed25519 public from private key genration. Both sk and pk are 32 bytes.
	pub fn ed25519_publickey_xed25519(sk: *const u8, pk: *mut u8);
	///Do Ed25519 signing. Both sk and pk are 32 bytes, sig is 64 bytes. Msg is msglen bytes.
	pub fn ed25519_sign_xed25519(msg: *const u8, msglen: usize, sk: *const u8, pk: *const u8, sig: *mut u8);
	///Do Ed25519 verification. Pk is 32 bytes, sig is 64 bytes. Msg is msglen bytes. Returns 0 if signature
	//verifies, -1 if it does not.
	pub fn ed25519_sign_open_xed25519(msg: *const u8, msglen: usize, pk: *const u8, sig: *const u8) -> i32;
	#[link_name="ed25519_sign_with_callback_xed25519"]
	fn sign_callback(sk: *const u8, pk: *const u8, sig: *mut u8, rng: *const u8, hc: Cb, hcctx: *mut ());
	#[link_name="ed25519_verify_with_callback_xed25519"]
	fn verify_callback(pk: *const u8, sig: *const u8, hc: Cb, hcctx: *mut ()) -> i32;
}
