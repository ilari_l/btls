extern crate cc;
use cc::Build;

fn main()
{
	{
		//Try general nettle test.
		let mut config = Build::new();
		config.file("src/ed25519-donna/ed25519.c");
		config.define("ED25519_REFHASH", None);		//SHA-512 is not invoked on lots of data.
		config.define("ED25519_CUSTOMRANDOM", None);	//Use the defined random routine.
		config.define("ED25519_SUFFIX", Some("_xed25519"));	//Less symbol collisions.
		config.flag("-Wno-implicit-fallthrough");               //Shut up.
		config.compile("libxed25519.a");
	}
}
