extern crate btls_aux_random;
use btls_aux_random::RandomStream;
//use std::io::Write;
//use std::io::stdout;
use std::env::args;
use std::str::FromStr;
use std::time::SystemTime;

#[inline(never)]
fn do_something(_foo: &[u8])
{
}

fn main()
{
	let mut block = 1usize;
	for (index, i) in args().enumerate() { if index == 1 { block = 1 << u32::from_str(&i).unwrap(); } }
	const ROUNDSIZE: usize = 16384;
	const ROUNDS: usize = 131072;
	let mut tmp = [0u8; ROUNDSIZE];
	let mut rs = RandomStream::new();
	let t1 = SystemTime::now();
	for _ in 0..ROUNDS {
		let mut i = 0;
		while i < tmp.len() {
			rs.fill_bytes(&mut tmp[i..i+block]);
			i = i+block;
		}
		do_something(&tmp);
	}
	let t2 = SystemTime::now();
	let dt = t2.duration_since(t1).unwrap();
	let dt = dt.as_secs() as f64 + dt.subsec_nanos() as f64 / 1000000000.0;
	let size = ROUNDSIZE * ROUNDS;
	println!("Generated {mb_size}MB in {dt}s, {speed}MB/s ({nscall} ns/call)",
		mb_size=size>>20, speed=(ROUNDSIZE*ROUNDS) as f64/dt/1000000.0,
		nscall=dt*1000000000.0/(size/block) as f64);
}
