extern crate btls_aux_random;
use btls_aux_random::RandomStream;
use std::io::Write;
use std::io::stdout;

fn main()
{
	let mut tmp = [0u8; 16384];
	let mut rs = RandomStream::new();
	loop {
		rs.fill_bytes(&mut tmp);
		stdout().write_all(&tmp).unwrap();
	}
}
