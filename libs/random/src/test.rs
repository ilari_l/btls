use super::RandomStream;
use super::internal;
use std::io::Write;
use std::io::stderr;

#[test]
fn distribution_evenness()
{
	const ROUNDS: usize = 65536;
	let mut count = [0usize;256];
	let mut tmp = [0u8; 1024];
	let mut rs = RandomStream::new();
	for _ in 0..ROUNDS {
		rs.fill_bytes(&mut tmp);
		for i in tmp.iter() {
			count[*i as usize] += 1;
		}
	}
	let expected = (ROUNDS * tmp.len() / 256) as f64;
	let mut maxf = 0.0;
	let mut maxb = 0;
	for (idx, i) in count.iter().enumerate() {
		let f = ((*i as f64) / expected - 1.0).abs();
		assert!(f < 0.01);
		if f > maxf { maxf = f; maxb = idx; }
	}
	writeln!(stderr(), "Max f={maxf} (index={maxb})").unwrap();
}

#[test]
fn two_streams()
{
	//Basic sanity check, test that the things are actually distinct!
	let mut tmp1 = [0u8; 1024];
	let mut tmp2 = [0u8; 1024];
	let mut rs1 = RandomStream::new();
	let mut rs2 = RandomStream::new();
	rs1.fill_bytes(&mut tmp1);
	rs2.fill_bytes(&mut tmp2);
	assert!(&tmp1[512..544] != &tmp2[512..544]);
}

#[test]
fn stream_consistency()
{
	//use std::fs::File;
	//use std::io::Write;
	let ref1 = include_bytes!("randomstream1.bin");
	let ref2 = include_bytes!("randomstream2.bin");
	let key = [0;48];
	let rekey = [1;256];
	let mut buf = [0;8192];
	let mut stream = internal::RandomStream::new_from_state(&key);
	stream.fill_bytes(&mut buf);
	assert_eq!(&buf[..], &ref1[..]);
	stream.reseed(&rekey).unwrap();
	stream.fill_bytes(&mut buf);
	assert_eq!(&buf[..], &ref2[..]);
	//let mut x = File::create("randomstream1.bin").unwrap();
	//x.write_all(&mut buf);
	//let mut x = File::create("randomstream2.bin").unwrap();
	//x.write_all(&mut buf);
	//assert!(false);
}

#[test]
fn stream_consistency2()
{
	//use std::fs::File;
	//use std::io::Write;
	let ref1 = include_bytes!("randomstream3.bin");
	let ref2 = include_bytes!("randomstream4.bin");
	let key = [255;48];
	let rekey = [1;256];
	let mut buf = [0;8192];
	let mut stream = internal::RandomStream::new_from_state(&key);
	stream.fill_bytes(&mut buf);
	assert_eq!(&buf[..], &ref1[..]);
	//let mut x = File::create("randomstream3.bin").unwrap();
	//x.write_all(&mut buf);
	stream.reseed(&rekey).unwrap();
	stream.fill_bytes(&mut buf);
	assert_eq!(&buf[..], &ref2[..]);
	//let mut x = File::create("randomstream4.bin").unwrap();
	//x.write_all(&mut buf);
	//assert!(false);
}

#[test]
fn seed_bytes()
{
	let bytes1 = [0;256];
	let mut bytes2 = [0;256];
	let mut buf1 = [0;16384];
	let mut buf2 = [0;16384];
	let mut buf3 = [0;16384];
	bytes2[251] = 1;
	let mut gen1 = RandomStream::new_with_seed(&bytes1);
	let mut gen2 = RandomStream::new_with_seed(&bytes2);
	let mut gen3 = RandomStream::new_with_seed(&bytes2);
	gen1.fill_bytes(&mut buf1);
	gen2.fill_bytes(&mut buf2);
	gen3.fill_bytes(&mut buf3);
	assert!(&buf1[..] != &buf2[..]);
	assert!(&buf2[..] == &buf3[..]);
}
