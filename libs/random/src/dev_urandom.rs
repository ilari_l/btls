#![allow(unsafe_code)]
use btls_aux_collections::Box;
use btls_aux_collections::Once;
use btls_aux_unix::FileDescriptor;
use btls_aux_unix::OpenFlags;
use btls_aux_unix::OsError;
use btls_aux_unix::Path;
use core::ptr::null_mut;
use core::sync::atomic::AtomicPtr;
use core::sync::atomic::Ordering;

pub fn getrandom(buf: &mut [u8])
{
	static RNG_FD: AtomicPtr<FileDescriptor> = AtomicPtr::new(null_mut());
	static INIT: Once = Once::new();
	INIT.call_once(||{
		let mut b = [0;1];
		if btls_aux_unix::getrandom(&mut b, Default::default()) != Err(OsError::ENOSYS) {
			//Getrandom is supported, use it.
			return;
		}
		//If /dev/urandom open fails, just try again... Forever.
		while RNG_FD.load(Ordering::Acquire).is_null() {
			let flags = OpenFlags::O_RDONLY|OpenFlags::O_CLOEXEC;
			if let Ok(rngfd) = Path::dev_urandom().open(flags) {
				RNG_FD.store(Box::into_raw(Box::new(rngfd)), Ordering::Release);
			}
		}
	});
	let rngfd = RNG_FD.load(Ordering::Relaxed);
	let rng = if rngfd.is_null() { None } else { unsafe{Some((*rngfd).as_fdb()) } };
	let mut fill = 0;
	//Retry all short or failed reads... Forever.
	while fill < buf.len() {
		let buf = &mut buf[fill..];
		
		let r = if let Some(fd) = rng {
			unsafe{fd.read(buf)}
		} else  {
			btls_aux_unix::getrandom(buf, Default::default())
		};
		if let Ok(amt) = r { fill += amt as usize; }
	}
}
