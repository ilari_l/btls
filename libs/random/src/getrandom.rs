pub fn getrandom(buf: &mut [u8])
{
	let mut fill = 0;
	//Retry all short or failed reads... Forever.
	while fill < buf.len() {
		if let Ok(amt) = btls_aux_unix::getrandom(&mut buf[fill..], Default::default()) {
			fill += amt;
		}
	}
}
