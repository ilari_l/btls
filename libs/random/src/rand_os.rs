#![allow(unsafe_code)]
use btls_aux_collections::Box;
use rand_core::RngCore;
use rand_os::OsRng;
use btls_aux_collections::Once;
use core::mem::transmute;
use core::ptr::null_mut;
use core::sync::atomic::AtomicPtr;
use core::sync::atomic::Ordering;


pub fn getrandom(buf: &mut [u8])
{
	static RNG_FD: AtomicPtr<OsRng> = AtomicPtr::new(null_mut());
	static INIT: Once = Once::new();
	INIT.call_once(||{
		//Retry forever.
		while RNG_FD.load(Ordering::Acquire).is_null() {
			if let Ok(rng_fd) = OsRng::new() {
				RNG_FD.store(Box::into_raw(Box::new(rng_fd)), Ordering::Release);
			}
		}
	});
	let rngfd = RNG_FD.load(Ordering::Relaxed);
	let rngfd: &mut OsRng = unsafe{transmute(rngfd)};
	//Retry forever.
	while rngfd.try_fill_bytes(buf).is_err() {}
}
