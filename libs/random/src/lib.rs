//!CSPRNG
//!
//!This crate contains a Cryptographically Secure Pseudo Random Number Generator.
//!
//!The important items include:
//!
//! * Cryptographically Secure Pseudo Random Number Generator stream: [`RandomStream`](struct.RandomStream.html).
//! * Short-lived Cryptographically Secure Pseudo Random Number Generator stream: [`TemporaryRandomStream`]
//! * Fill buffer with Cryptographically Secure Pseudo Random Number Generator: [`secure_random()`]
//!
//![`secure_random()`]: fn.secure_random.html
//![`TemporaryRandomStream`]: struct.TemporaryRandomStream.html
#![deny(unsafe_code)]
#![forbid(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]
#[cfg(test)] extern crate std;
#[cfg(all(target_os="linux",not(feature="dev_urandom")))]
#[path="getrandom.rs"] mod inner;
#[cfg(all(target_os="linux",feature="dev_urandom"))]
#[path="dev_urandom.rs"] mod inner;
#[cfg(not(target_os="linux"))]
#[path="rand_os.rs"] mod inner;
use core::marker::PhantomData;


//This was exposed, but is now deprecated
#[doc(hidden)]
#[derive(Clone,Debug,PartialEq,Eq)]
#[non_exhaustive]
pub enum RngError
{
	///Random number generator failed!
	RngFailure,
}

///Fill buffer with random bytes.
///
///The buffer `output` is overwritten entierely with uniform random bytes. The bytes used for filling are
///cryptographically secure. This function is both thread-safe and fork-safe. However, it is not safe to call in
///signal handlers.
pub fn secure_random(output: &mut [u8]) { inner::getrandom(output) }

#[cfg(feature="fast_drbg")]
mod internal
{
	use super::secure_random;
	use btls_aux_chacha20poly1305aead::FastKeDrbg2;
	//768 byte state => N=11.
	pub struct RandomStream(FastKeDrbg2<11>);

	impl RandomStream
	{
		pub fn new() -> RandomStream
		{
			let mut buf = [0; 48];
			secure_random(&mut buf);
			let state = Self::new_from_state(&buf);
			state
		}
		pub fn new_from_state(buf: &[u8; 48]) -> RandomStream
		{
			let state = loop{ if let Ok(s) = FastKeDrbg2::new(buf) { break s; } };
			RandomStream(state)
		}
		pub fn fill_bytes(&mut self, x: &mut [u8]) { self.0.extract(x) }
		pub fn reseed(&mut self, seed: &[u8]) -> Result<(), ()> { Ok(self.0.reseed(seed)) }
	}
}

#[cfg(not(feature="fast_drbg"))]
mod internal
{
extern crate btls_aux_sha3;
use core::cmp::min;
use super::secure_random;
use self::btls_aux_sha3::Shake256384;

pub struct RandomStream
{
	output: [u32; 192],
	used: usize,
}

//Return 1 if x is 0, 0 otherwise.
fn iszero(x: u32) -> u32
{
	let x = x | (x >> 16);
	let x = x | (x >> 8);
	let x = x | (x >> 4);
	let x = x | (x >> 2);
	let x = x | (x >> 1);
	let x = x & 1;
	1 - x
}

macro_rules! Qround
{
	($s:expr, $a:expr, $b:expr, $c:expr, $d:expr) => {{
		$s[$a] = $s[$a].wrapping_add($s[$b]); $s[$d] ^= $s[$a]; $s[$d] = $s[$d].rotate_left(16);
		$s[$c] = $s[$c].wrapping_add($s[$d]); $s[$b] ^= $s[$c]; $s[$b] = $s[$b].rotate_left(12);
		$s[$a] = $s[$a].wrapping_add($s[$b]); $s[$d] ^= $s[$a]; $s[$d] = $s[$d].rotate_left(8);
		$s[$c] = $s[$c].wrapping_add($s[$d]); $s[$b] ^= $s[$c]; $s[$b] = $s[$b].rotate_left(7);
	}}
}

impl RandomStream
{
	fn refill(&mut self)
	{
		let mut tmp = [0u32; 16];
		(&mut tmp[4..]).copy_from_slice(&self.output[..12]);
		(&mut tmp[..4]).copy_from_slice(&[0x61707865, 0x3320646e, 0x79622d32, 0x6b206574]);
		for b in 0..self.output.len() / 16 {
			let oslice = &mut self.output[16*b..][..16];
			oslice.copy_from_slice(&tmp);
			for _ in 0..10 {
				Qround!(oslice, 0, 4, 8,12);
				Qround!(oslice, 1, 5, 9,13);
				Qround!(oslice, 2, 6,10,14);
				Qround!(oslice, 3, 7,11,15);
				Qround!(oslice, 0, 5,10,15);
				Qround!(oslice, 1, 6,11,12);
				Qround!(oslice, 2, 7, 8,13);
				Qround!(oslice, 3, 4, 9,14);
			}
			for i in 0..16 { oslice[i] = oslice[i].wrapping_add(tmp[i]); }
			//Advance the state. We only increment two words for compatiblity.
			tmp[12] = tmp[12].wrapping_add(1);
			tmp[13] = tmp[13].wrapping_add(iszero(tmp[12]));
		}
		self.used = 48;		//The next state itself uses 48 bytes.
	}
	pub fn new() -> RandomStream
	{
		let mut buf = [0; 48];
		secure_random(&mut buf);
		let state = Self::new_from_state(&buf);
		state
	}
	pub fn new_from_state(buf: &[u8; 48]) -> RandomStream
	{
		let mut state = RandomStream {
			output: [0; 192],
			used: 0,
		};
		for i in 0..48 { state.output[i >> 2] |= (buf[i] as u32) << ((i & 3) << 3); }
		state.refill();
		state
	}
	pub fn fill_bytes(&mut self, x: &mut [u8])
	{
		let mut itr = 0;
		while itr < x.len() {
			//used < 48, itr < x.len().
			let block = min(self.output.len() * 4 - self.used, x.len() - itr);
			let xb = &mut x[itr..][..block];			//itr+block <= x.len().
			for i in 0..block {
				let idx = i + self.used;
				xb[i] = (self.output[idx >> 2] >> ((idx & 3) << 3)) as u8;
				//Zero bytes that have been used.
				self.output[idx >> 2] &= !(255 << 8 * (idx & 3));
			}
			itr += block;
			self.used += block;
			if self.used == self.output.len() * 4 { self.refill() }
		}
	}
	pub fn reseed(&mut self, seed: &[u8]) -> Result<(), ()>
	{
		//25 element array for Keccak. Perform Shake256(state|i|seed, 384)
		let mut nstate = [0;48];
		for i in 0..48 { nstate[i] = (self.output[i/4] >> i % 4 * 8) as u8; }
		let mut xstate = Shake256384::new();
		xstate.input(&nstate);
		xstate.input(&(self.used as u64).to_le_bytes());
		xstate.input(seed);
		let nstate = xstate.output();
		for i in 0..12 { self.output[i] = 0; }
		for i in 0..48 { self.output[i/4] |= (nstate[i] as u32) << i % 4 * 8; } 
		self.refill();
		//Ok.
		Ok(())
	}
}
}

///A CSPRNG stream.
///
///This structure is a fast random number stream, initialized once and then used to extract random numbers
///quickly.
///
///This stream is much faster than [`secure_random()`], but does not handle multithreading nor multiprocessing
///(see section DANGER). It is not safe for signal handlers either.
///
///This structure can be created by the [`new()`](#method.new) and [`new_with_seed()`] methods.
///
///# DANGER:
///
///This structure is NOT SAFE for multithreading or forking. Do not transfer it across forks. Failing this likely
///leads to predictable output, leading to **catastrophic** system failure.
///
///[`secure_random()`]: fn.secure_random.html
///[`new_with_seed()`]: #method.new_with_seed
pub struct RandomStream(internal::RandomStream);

impl RandomStream
{
	///Create a new random stream, initializing it off the system Random Number Generator.
	pub fn new() -> RandomStream { RandomStream(internal::RandomStream::new()) }
	///Create a new random stream with seed `seed`.
	///
	///Note that the output of the generator is DETERMINISTIC w.r.t. the provoded seed.
	pub fn new_with_seed(seed: &[u8]) -> RandomStream
	{
		//Always use zeroes as initial key, then reseed it.
		let key = [0;48];
		let mut s = RandomStream(internal::RandomStream::new_from_state(&key));
		while s.0.reseed(seed).is_err() {};
		s
	}
	///Fill buffer `output` with random bytes from this stream.
	///
	///This method fills the output buffer with with cryptographically secure random bytes from the stream.
	///
	///This operation does not deplete "entropy" in the state. If the state has high entropy, recovering the
	///state from the generator output remains hard, no matter how long stream is extracted (within practical
	///limits). This holds even against attacker with access to a large quantum computer.
	pub fn fill_bytes(&mut self, output: &mut [u8]) { self.0.fill_bytes(output) }
	///Reseed the PRNG with seed `seed`.
	///
	///The seed SHOULD have at minimum 128 bits of entropy. Reseeding with too little entropy is INSECURE.
	///
	///Should always return `Ok(())`. If a bug occurs, may also return `Err(())`.
	pub fn reseed(&mut self, seed: &[u8]) -> Result<(), ()> { self.0.reseed(seed) }
	///Reseed the PRNG with system entropy.
	///
	///Should always return `Ok(())`. If a bug occurs, may also return `Err(())`.
	pub fn reseed_system(&mut self) -> Result<(), ()>
	{
		let mut seed = [0;48];
		secure_random(&mut seed);
		let x = self.0.reseed(&seed);
		x
	}
}

///A Lifetime tag.
///
///These objects are created manually as `TemporaryRandomLifetimeTag`. There are no fields.
///
///These tags are used by [`TemporaryRandomStream`](struct.TemporaryRandomStream.html) in order to limit the lifetime
///of the random number generator (as it is intended to be just temporary).
pub struct TemporaryRandomLifetimeTag;

///A CSPRNG stream expansion of seed.
///
///This generator is very fast, even when generating short outputs. It also is forward secure and backtracking
///resistant at granularity of individual requests.
///
///This is only meant for short-duration streams. It captures the lifetime of the seed (altough it actually does not
///capture the seed itself). It is neither `Send` nor `Sync`.
///
///This structure can be created by using the [`new()`](#method.new) or [`new_system()`](#method.new_system) methods.
pub struct TemporaryRandomStream<'a>(internal::RandomStream, PhantomData<(&'a u8, *mut u8)>);

impl<'a> TemporaryRandomStream<'a>
{
	///Create a new random stream with seed `seed`.
	///
	///Note that the output of the generator is DETERMINISTIC w.r.t. the provoded seed. So for the output to be
	///random, the seed should contain full 384 bits of entropy.
	pub fn new(seed: &'a [u8;48]) -> TemporaryRandomStream<'a>
	{
		TemporaryRandomStream(internal::RandomStream::new_from_state(seed), PhantomData)
	}
	///Create a new random stream seeded from the system RNG.
	///
	///This pulls the 384 bits needed to initialize the generator out of operating system RNG. This includes
	///`sys_getrandom()`, `/dev/urandom` and other possible sources.
	pub fn new_system(_d: &'a TemporaryRandomLifetimeTag) -> TemporaryRandomStream<'a>
	{
		let mut buf = [0; 48];
		secure_random(&mut buf);
		let state = TemporaryRandomStream(internal::RandomStream::new_from_state(&buf), PhantomData);
		state
	}
	///Fill buffer `output` with random bytes from the stream.
	///
	///This method fills the output buffer with with cryptographically secure random bytes from the stream.
	///This operation does not deplete "entropy" in the state. If the state has high entropy, recovering the
	///state from the generator output remains hard, no matter how long stream is extracted (within practical
	///limits). This holds even against attacker with access to a large quantum computer.
	pub fn fill_bytes(&mut self, output: &mut [u8]) { self.0.fill_bytes(output) }
	///Call reseed on the underlying RNG.
	///
	///See [`RandomStream::reseed`](struct.RandomStream.html#method.reseed) for more details.
	pub fn reseed(&mut self, seed: &[u8])
	{
		//Ignore return, this should always succeed.
		self.0.reseed(seed).ok();
	}
}

#[cfg(test)]
mod test;
