use super::*;
use btls_aux_collections::Box;
use btls_aux_collections::BTreeMap;
use btls_aux_collections::ToOwned;
use btls_aux_collections::ToString;
use btls_aux_collections::Vec;
use core::cmp::Ordering;
use core::iter::FromIterator;
use core::str::from_utf8;

fn assert_cbor_rel(x: &CBOR, y: &CBOR, ordering: Ordering)
{
	assert_eq!(x.partial_cmp(y), Some(ordering));
	assert_eq!(y.partial_cmp(x), Some(ordering.reverse()));
	assert_eq!(x.cmp(y), ordering);
	assert_eq!(y.cmp(x), ordering.reverse());
	assert_eq!(x.eq(y), ordering.is_eq());
	assert_eq!(x.ne(y), ordering.is_ne());
}

#[test]
fn cbor_compares()
{
	//Various CBOR values in order, for testing.
	let list = [
		CBOR::Integer(0), CBOR::Integer(1), CBOR::Integer(6), CBOR::Integer(22),
		CBOR::Integer(0xFFFFFFFFFFFFFFFF), CBOR::NegInteger(0), CBOR::NegInteger(1),
		CBOR::NegInteger(15), CBOR::NegInteger(0xFFFFFFFFFFFFFFFF), CBOR::Octets(Vec::new()),
		CBOR::Octets(vec![0]), CBOR::Octets(vec![1]), CBOR::Octets(vec![2]),
		CBOR::Octets(vec![255]), CBOR::Octets(vec![0,0]), CBOR::Octets(vec![0,1]),
		CBOR::Octets(vec![1,0]), CBOR::Octets(vec![255,0]), CBOR::Octets(vec![255,255]),
		CBOR::Octets(vec![255,255,0]), CBOR::Octets(vec![255,255,255]),
		CBOR::String(format!("")), CBOR::String(format!(" ")), CBOR::String(format!("!")),
		CBOR::String(format!("0")), CBOR::String(format!("1")), CBOR::String(format!("A")),
		CBOR::String(format!("Z")), CBOR::String(format!("0x")), CBOR::String(format!("Z ")),
		CBOR::String(format!("ZZ")), CBOR::String(format!("ZZZZZZZ")), CBOR::Array(Vec::new()),
		CBOR::Array(vec![CBOR::Integer(0)]),
		CBOR::Array(vec![CBOR::Integer(1)]),
		CBOR::Array(vec![CBOR::Integer(2)]),
		CBOR::Array(vec![CBOR::Double(0xFFFFFFFFFFFFFFFF)]),
		CBOR::Array(vec![CBOR::Integer(1), CBOR::Integer(0)]),
		CBOR::Array(vec![CBOR::Integer(1), CBOR::Integer(1)]),
		CBOR::Array(vec![CBOR::Integer(1), CBOR::Double(0xFFFFFFFFFFFFFFFF)]),
		CBOR::Array(vec![CBOR::Integer(2), CBOR::Integer(0)]),
		CBOR::Array(vec![CBOR::Integer(2), CBOR::Integer(1), CBOR::Integer(6)]),
		CBOR::Array(vec![CBOR::Integer(2), CBOR::Integer(1), CBOR::Integer(7)]),
		CBOR::Dictionary(BTreeMap::new()),
		CBOR::Dictionary(BTreeMap::from_iter([(CBOR::Integer(0), CBOR::Integer(0))].iter().
			cloned())),
		CBOR::Dictionary(BTreeMap::from_iter([(CBOR::Integer(0), CBOR::Integer(1))].iter().
			cloned())),
		CBOR::Dictionary(BTreeMap::from_iter([(CBOR::Integer(1), CBOR::Integer(0))].iter().
			cloned())),
		CBOR::Dictionary(BTreeMap::from_iter([(CBOR::Integer(2), CBOR::Integer(0))].iter().
			cloned())),
		CBOR::Dictionary(BTreeMap::from_iter([(CBOR::Double(0), CBOR::Integer(0))].iter().
			cloned())),
		CBOR::Dictionary(BTreeMap::from_iter([(CBOR::Integer(2), CBOR::Integer(0)),
			(CBOR::Integer(0), CBOR::Integer(0))].iter().cloned())),
		CBOR::Dictionary(BTreeMap::from_iter([(CBOR::Integer(2), CBOR::Integer(0)),
			(CBOR::Integer(0), CBOR::Integer(1))].iter().cloned())),
		CBOR::Dictionary(BTreeMap::from_iter([(CBOR::Integer(2), CBOR::Integer(0)),
			(CBOR::Integer(0), CBOR::Integer(1)),
			(CBOR::Integer(7), CBOR::Integer(5))].iter().cloned())),
		CBOR::Dictionary(BTreeMap::from_iter([(CBOR::Integer(2), CBOR::Integer(0)),
			(CBOR::Integer(1), CBOR::Integer(0)),
			(CBOR::Integer(6), CBOR::Integer(4))].iter().cloned())),
		CBOR::Tag(0, Box::new(CBOR::Integer(0))),
		CBOR::Tag(0, Box::new(CBOR::Double(0xFFFFFFFFFFFFFFFF))),
		CBOR::Tag(1, Box::new(CBOR::Integer(0))),
		CBOR::Tag(1, Box::new(CBOR::Double(0xFFFFFFFFFFFFFFFF))),
		CBOR::Tag(2, Box::new(CBOR::Integer(0))),
		CBOR::Simple(0), CBOR::Simple(1), CBOR::Simple(2), CBOR::Simple(20),
		CBOR::Simple(21), CBOR::Simple(22), CBOR::Simple(23), CBOR::Simple(32),
		CBOR::Simple(254), CBOR::Simple(255), CBOR::HalfFloat(0), CBOR::HalfFloat(1),
		CBOR::HalfFloat(2), CBOR::HalfFloat(0xFFFF), CBOR::Float(1),
		CBOR::Float(2), CBOR::Float(0xFEFFFFFF), CBOR::Double(1),
		CBOR::Double(5), CBOR::Double(0xFFEFFFFFFFFFFFFF),
	];
	for i in 0..list.len() {
		for j in 0..i { assert_cbor_rel(&list[i], &list[j], Ordering::Greater); }
		assert_cbor_rel(&list[i], &list[i], Ordering::Equal);
		for j in i+1..list.len() { assert_cbor_rel(&list[i], &list[j], Ordering::Less); }
	}
}

fn promote_half_single(x: u16) -> u32
{
	let p = HalfParts::decompose(x);
	let mantissa = (p.mantissa as u32) << 13;
	let r = if p.exp <= -15 && mantissa == 0 {
		//Zero. This needs to have exponent -127.
		SingleParts::new(p.sign, 0, -127)
	} else if p.exp <= -15 {
		//Non-zero subnormals. These need to be adjusted.
		let lz = mantissa.leading_zeros().saturating_sub(8) as i32;
		SingleParts::new(p.sign, (mantissa << lz) & 0x7FFFFF, -14 - lz)
	} else if p.exp >= 16 {
		//Infinite and NaN. These need to have exponent = 128.
		SingleParts::new(p.sign, mantissa, 128)
	} else {
		//Normal number.
		SingleParts::new(p.sign, mantissa, p.exp)
	};
	r.compose()
}

#[test]
fn promote_stepwise()
{
	for i in 0..65536u32 {
		let i = i as u16;
		let a = promote_half(i);
		let b = promote_single(promote_half_single(i));
		assert_eq!(a, b);
	}
}

#[test]
fn promote_half_eq()
{
	for i in 0..65536u32 {
		let i = i as u16;
		let a = CBOR::HalfFloat(i);
		let b = CBOR::Float(promote_half_single(i));
		let c = CBOR::Double(promote_half(i));
		let z = [a,b,c];
		for x in z.iter() { for y in z.iter() { assert!(x == y); }}
	}
}

#[test]
fn promote_single_eq()
{
	for i in 0..50_000u32 {
		let i = i.wrapping_mul(i).wrapping_add(i.wrapping_mul(3_742_248_533));
		let b = CBOR::Float(i);
		let c = CBOR::Double(promote_single(i));
		let z = [b,c];
		for x in z.iter() { for y in z.iter() { assert!(x == y); }}
	}
}

fn not_demotable_tail(a: CBOR, b: CBOR, c: CBOR, d: CBOR, e: CBOR)
{
	assert!(a < b);
	assert!(a < c);
	assert!(a < d);
	assert!(a == e);
	assert!(a.as_double_float_p() > b.as_double_float_p());
	assert!(a.as_double_float_p() > c.as_double_float_p());
	assert!(a.as_double_float_p() < d.as_double_float_p());
}

#[test]
fn not_demotable_single()
{
	let a = CBOR::HalfFloat(1);
	let b = CBOR::Float(1);
	let c = CBOR::Float(0x337FFFFF);
	let d = CBOR::Float(0x33800001);
	let e = CBOR::Float(0x33800000);
	not_demotable_tail(a,b,c,d,e);
}

#[test]
fn not_demotable_double1()
{
	let a = CBOR::HalfFloat(1);
	let b = CBOR::Double(1);
	let c = CBOR::Double(0x3e6FFFFFFFFFFFFF);
	let d = CBOR::Double(0x3e70000000000001);
	let e = CBOR::Double(0x3e70000000000000);
	not_demotable_tail(a,b,c,d,e);
}

#[test]
fn not_demotable_double2()
{
	let a = CBOR::Float(1);
	let b = CBOR::Double(1);
	let c = CBOR::Double(0x369FFFFFFFFFFFFF);
	let d = CBOR::Double(0x36A0000000000001);
	let e = CBOR::Double(0x36A0000000000000);
	not_demotable_tail(a,b,c,d,e);
}

#[test]
fn nan_is_equal()
{
	let z = [
		CBOR::HalfFloat(0x7C01),CBOR::HalfFloat(0x7C02),CBOR::HalfFloat(0x7E00),CBOR::HalfFloat(0x7FFF),
		CBOR::HalfFloat(0xFC01),CBOR::HalfFloat(0xFC02),CBOR::HalfFloat(0xFE00),CBOR::HalfFloat(0xFFFF),
		CBOR::Float(0x7F800001), CBOR::Float(0x7F800002), CBOR::Float(0x7FC00000), CBOR::Float(0x7FFFFFFF),
		CBOR::Float(0xFF800001), CBOR::Float(0xFF800002), CBOR::Float(0xFFC00000), CBOR::Float(0xFFFFFFFF),
		CBOR::Double(0x7FF0000000000001), CBOR::Double(0x7FF0000000000002), CBOR::Double(0x7FF8000000000000),
		CBOR::Double(0x7FFFFFFFFFFFFFFF), CBOR::Double(0xFFF0000000000001), CBOR::Double(0xFFF0000000000002),
		CBOR::Double(0xFFF8000000000000), CBOR::Double(0xFFFFFFFFFFFFFFFF),
	];
	for x in z.iter() { for y in z.iter() { assert!(x == y); }}
}

#[test]
fn parse_cbor_abc_ind()
{
	assert_eq!(CBOR::parse(&[0x7F, 0x63, 65, 66, 67, 0xFF][..]).unwrap(), CBOR::String(format!("ABC")));
}

#[test]
fn parse_cbor_array_cascade()
{
	let cbor = CBOR::parse(&[0x81,0x81,0x81,0x81,0x00][..]).unwrap();
	let cbor = &cbor.as_array().unwrap()[0];
	let cbor = &cbor.as_array().unwrap()[0];
	let cbor = &cbor.as_array().unwrap()[0];
	let cbor = &cbor.as_array().unwrap()[0];
	assert_eq!(cbor, &CBOR::Integer(0));
}

#[test]
fn parse_cbor_tag_cascade()
{
	let cbor = CBOR::parse(&[0xc1,0xc1,0xc1,0xc1,0x00][..]).unwrap();
	let cbor = cbor.as_tag().unwrap().1;
	let cbor = cbor.as_tag().unwrap().1;
	let cbor = cbor.as_tag().unwrap().1;
	let cbor = cbor.as_tag().unwrap().1;
	assert_eq!(cbor, &CBOR::Integer(0));
}

#[test]
fn parse_cbor_sequence()
{
	let c = [0x01,0x02,0x03,0x04];
	assert_eq!(CBOR::parse_multi(&c[0..]).unwrap(), (1, CBOR::Integer(1)));
	assert_eq!(CBOR::parse_multi(&c[1..]).unwrap(), (1, CBOR::Integer(2)));
	assert_eq!(CBOR::parse_multi(&c[2..]).unwrap(), (1, CBOR::Integer(3)));
	assert_eq!(CBOR::parse_multi(&c[3..]).unwrap(), (1, CBOR::Integer(4)));
}

fn parse_failed(cbor: &[u8], msg: &str)
{
	assert_eq!(CBOR::parse(cbor).unwrap_err().to_string(), msg);
}



#[test]
fn parse_cbor()
{
	//Empty.
	parse_failed(&[], "CBOR error at position 0: Expected Value, got End of Input");
	parse_failed(&[0x00, 0x01], "CBOR error at position 1: Trailing garbage");
	//Positive integers.
	assert_eq!(CBOR::parse(&[0x00][..]).unwrap(), CBOR::Integer(0));
	assert_eq!(CBOR::parse(&[0x01][..]).unwrap(), CBOR::Integer(1));
	assert_eq!(CBOR::parse(&[0x17][..]).unwrap(), CBOR::Integer(23));
	parse_failed(&[0x18], "CBOR error at position 1: Expected at least 1 bytes more, got End of Input");
	assert_eq!(CBOR::parse(&[0x18, 0x00][..]).unwrap(), CBOR::Integer(0));
	assert_eq!(CBOR::parse(&[0x18, 0x17][..]).unwrap(), CBOR::Integer(23));
	assert_eq!(CBOR::parse(&[0x18, 0x18][..]).unwrap(), CBOR::Integer(24));
	assert_eq!(CBOR::parse(&[0x18, 0xFF][..]).unwrap(), CBOR::Integer(255));
	parse_failed(&[0x19, 0x01], "CBOR error at position 2: Expected at least 1 bytes more, got End of Input");
	assert_eq!(CBOR::parse(&[0x19, 0x00, 0x00][..]).unwrap(), CBOR::Integer(0));
	assert_eq!(CBOR::parse(&[0x19, 0x00, 0xFF][..]).unwrap(), CBOR::Integer(255));
	assert_eq!(CBOR::parse(&[0x19, 0x01, 0x00][..]).unwrap(), CBOR::Integer(256));
	assert_eq!(CBOR::parse(&[0x19, 0x01, 0x03][..]).unwrap(), CBOR::Integer(259));
	assert_eq!(CBOR::parse(&[0x19, 0xFF, 0xFE][..]).unwrap(), CBOR::Integer(0xFFFE));
	assert_eq!(CBOR::parse(&[0x19, 0xFF, 0xFF][..]).unwrap(), CBOR::Integer(0xFFFF));
	parse_failed(&[0x1A, 0x01, 0x00, 0x00],
		"CBOR error at position 4: Expected at least 1 bytes more, got End of Input");
	assert_eq!(CBOR::parse(&[0x1A, 0x00, 0x00, 0x00, 0x00][..]).unwrap(), CBOR::Integer(0));
	assert_eq!(CBOR::parse(&[0x1A, 0x00, 0x00, 0x00, 0x01][..]).unwrap(), CBOR::Integer(1));
	assert_eq!(CBOR::parse(&[0x1A, 0x00, 0x00, 0x00, 0xFF][..]).unwrap(), CBOR::Integer(255));
	assert_eq!(CBOR::parse(&[0x1A, 0x00, 0x00, 0x01, 0x00][..]).unwrap(), CBOR::Integer(256));
	assert_eq!(CBOR::parse(&[0x1A, 0x01, 0x05, 0x08, 0x14][..]).unwrap(), CBOR::Integer(0x01050814));
	assert_eq!(CBOR::parse(&[0x1A, 0xFF, 0xFE, 0xFD, 0xFC][..]).unwrap(), CBOR::Integer(0xFFFEFDFC));
	assert_eq!(CBOR::parse(&[0x1A, 0xFF, 0xFF, 0xFF, 0xFF][..]).unwrap(), CBOR::Integer(0xFFFFFFFF));
	parse_failed(&[0x1B, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,],
		"CBOR error at position 8: Expected at least 1 bytes more, got End of Input");
	assert_eq!(CBOR::parse(&[0x1B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00][..]).unwrap(),
		CBOR::Integer(0));
	assert_eq!(CBOR::parse(&[0x1B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01][..]).unwrap(),
		CBOR::Integer(1));
	assert_eq!(CBOR::parse(&[0x1B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF][..]).unwrap(),
		CBOR::Integer(255));
	assert_eq!(CBOR::parse(&[0x1B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00][..]).unwrap(),
		CBOR::Integer(256));
	assert_eq!(CBOR::parse(&[0x1B, 0x16, 0x22, 0x10, 0x02, 0x01, 0x05, 0x08, 0x14][..]).unwrap(),
		CBOR::Integer(0x1622100201050814));
	assert_eq!(CBOR::parse(&[0x1B, 0xFF, 0xFE, 0xFD, 0xFC, 0xFB, 0xFA, 0xF9, 0xF8][..]).unwrap(),
		CBOR::Integer(0xFFFEFDFCFBFAF9F8));
	assert_eq!(CBOR::parse(&[0x1B, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF][..]).unwrap(),
		CBOR::Integer(0xFFFFFFFFFFFFFFFF));
	parse_failed(&[0x1C], "CBOR error at position 0: Unknown opcode 1c");
	parse_failed(&[0x1D], "CBOR error at position 0: Unknown opcode 1d");
	parse_failed(&[0x1E], "CBOR error at position 0: Unknown opcode 1e");
	parse_failed(&[0x1F], "CBOR error at position 0: Unknown opcode 1f");
	parse_failed(&[0x1F, 0x00, 0xFF], "CBOR error at position 0: Unknown opcode 1f");
	//Negative integers.
	assert_eq!(CBOR::parse(&[0x20][..]).unwrap(), CBOR::NegInteger(0));
	assert_eq!(CBOR::parse(&[0x21][..]).unwrap(), CBOR::NegInteger(1));
	assert_eq!(CBOR::parse(&[0x37][..]).unwrap(), CBOR::NegInteger(23));
	assert_eq!(CBOR::parse(&[0x38, 0x01][..]).unwrap(), CBOR::NegInteger(0x01));
	assert_eq!(CBOR::parse(&[0x38, 0x18][..]).unwrap(), CBOR::NegInteger(0x18));
	assert_eq!(CBOR::parse(&[0x38, 0xFF][..]).unwrap(), CBOR::NegInteger(0xFF));
	assert_eq!(CBOR::parse(&[0x39, 0x01, 0x03][..]).unwrap(), CBOR::NegInteger(0x0103));
	assert_eq!(CBOR::parse(&[0x3A, 0x01, 0x03, 0x05, 0x07][..]).unwrap(), CBOR::NegInteger(0x01030507));
	assert_eq!(CBOR::parse(&[0x3B, 0x01, 0x03, 0x05, 0x07, 0x09, 0x0B, 0x0D, 0x0F][..]).unwrap(),
		CBOR::NegInteger(0x01030507090B0D0F));
	parse_failed(&[0x3C], "CBOR error at position 0: Unknown opcode 3c");
	parse_failed(&[0x3D], "CBOR error at position 0: Unknown opcode 3d");
	parse_failed(&[0x3E], "CBOR error at position 0: Unknown opcode 3e");
	parse_failed(&[0x3F], "CBOR error at position 0: Unknown opcode 3f");
	parse_failed(&[0x3F, 0x00, 0xFF], "CBOR error at position 0: Unknown opcode 3f");
	//Octet strings.
	assert_eq!(CBOR::parse(&[0x40][..]).unwrap(), CBOR::Octets(vec![]));
	assert_eq!(CBOR::parse(&[0x41, 0x05][..]).unwrap(), CBOR::Octets(vec![5]));
	parse_failed(&[0x42, 0x05], "CBOR error at position 2: Expected at least 1 bytes more, got End of Input");
	assert_eq!(CBOR::parse(&[0x42, 0x05, 0x09][..]).unwrap(), CBOR::Octets(vec![5, 9]));
	assert_eq!(CBOR::parse(&[0x44, 0x05, 0x09, 6, 2][..]).unwrap(), CBOR::Octets(vec![5, 9, 6, 2]));
	assert_eq!(CBOR::parse(&[0x58, 3, 0x09, 6, 2][..]).unwrap(), CBOR::Octets(vec![9, 6, 2]));
	assert_eq!(CBOR::parse(&[0x59, 0, 3, 0x09, 6, 2][..]).unwrap(), CBOR::Octets(vec![9, 6, 2]));
	assert_eq!(CBOR::parse(&[0x5A, 0, 0, 0, 3, 0x09, 6, 2][..]).unwrap(), CBOR::Octets(vec![9, 6, 2]));
	assert_eq!(CBOR::parse(&[0x5B, 0, 0, 0, 0, 0, 0, 0, 3, 0x09, 6, 2][..]).unwrap(),
		CBOR::Octets(vec![9, 6, 2]));
	parse_failed(&[0x5C], "CBOR error at position 0: Unknown opcode 5c");
	parse_failed(&[0x5D], "CBOR error at position 0: Unknown opcode 5d");
	parse_failed(&[0x5E], "CBOR error at position 0: Unknown opcode 5e");
	parse_failed(&[0x5F], "CBOR error at position 1: Expected Octets or Break, got End of Input");
	assert_eq!(CBOR::parse(&[0x5F, 0xFF][..]).unwrap(), CBOR::Octets(vec![]));
	assert_eq!(CBOR::parse(&[0x5F, 0x40, 0xFF][..]).unwrap(), CBOR::Octets(vec![]));
	assert_eq!(CBOR::parse(&[0x5F, 0x41, 2, 0xFF][..]).unwrap(), CBOR::Octets(vec![2]));
	assert_eq!(CBOR::parse(&[0x5F, 0x42, 2, 6, 0xFF][..]).unwrap(), CBOR::Octets(vec![2, 6]));
	assert_eq!(CBOR::parse(&[0x5F, 0x42, 2, 6, 0x43, 1, 2, 3, 0xFF][..]).unwrap(),
		CBOR::Octets(vec![2, 6, 1, 2, 3]));
	assert_eq!(CBOR::parse(&[0x5F, 0x42, 2, 6, 0x40, 0x43, 1, 2, 3, 0xFF][..]).unwrap(),
		CBOR::Octets(vec![2, 6, 1, 2, 3]));
	parse_failed(&[0x5F, 0x42, 2, 6, 0x60, 0x43, 1, 2, 3, 0xFF],
		"CBOR error at position 4: Expected Octets or Break, got opcode 60");
	//Strings.
	assert_eq!(CBOR::parse(&[0x60][..]).unwrap(), CBOR::String(format!("")));
	assert_eq!(CBOR::parse(&[0x61, 0x41][..]).unwrap(), CBOR::String(format!("A")));
	parse_failed(&[0x62, 0x41], "CBOR error at position 2: Expected at least 1 bytes more, got End of Input");
	assert_eq!(CBOR::parse(&[0x62, 0x41, 0x44][..]).unwrap(), CBOR::String(format!("AD")));
	assert_eq!(CBOR::parse(&[0x78, 3, 65, 66, 67][..]).unwrap(), CBOR::String(format!("ABC")));
	assert_eq!(CBOR::parse(&[0x79, 0, 3, 65, 66, 67][..]).unwrap(), CBOR::String(format!("ABC")));
	assert_eq!(CBOR::parse(&[0x7A, 0, 0, 0, 3, 65, 66, 67][..]).unwrap(), CBOR::String(format!("ABC")));
	assert_eq!(CBOR::parse(&[0x7B, 0, 0, 0, 0, 0, 0, 0, 3, 65, 66, 67][..]).unwrap(),
		CBOR::String(format!("ABC")));
	assert_eq!(CBOR::parse(&[0x78, 3, 0xE2, 0xA2, 0x83][..]).unwrap(),
		CBOR::String(char::from_u32(0x2883).unwrap().to_string()));
	assert_eq!(CBOR::parse(&[0x78, 5, 33, 0xE2, 0xA2, 0x83, 36][..]).unwrap(),
		CBOR::String(format!("!{c}$", c=char::from_u32(0x2883).unwrap())));
	parse_failed(&[0x7C], "CBOR error at position 0: Unknown opcode 7c");
	parse_failed(&[0x7D], "CBOR error at position 0: Unknown opcode 7d");
	parse_failed(&[0x7E], "CBOR error at position 0: Unknown opcode 7e");
	parse_failed(&[0x7F], "CBOR error at position 1: Expected Text or Break, got End of Input");
	assert_eq!(CBOR::parse(&[0x7F, 0xFF][..]).unwrap(), CBOR::String(format!("")));
	parse_failed(&[0x7F, 0x63, 65, 66, 67],
		"CBOR error at position 5: Expected Text or Break, got End of Input");
	assert_eq!(CBOR::parse(&[0x7F, 0x63, 65, 66, 67, 0xFF][..]).unwrap(), CBOR::String(format!("ABC")));
	assert_eq!(CBOR::parse(&[0x7F, 0x62, 65, 66, 0x61, 67, 0xFF][..]).unwrap(),
		CBOR::String(format!("ABC")));
	assert_eq!(CBOR::parse(&[0x7F, 0x62, 65, 66, 0x60, 0x61, 67, 0xFF][..]).unwrap(),
		CBOR::String(format!("ABC")));
	parse_failed(&[0x7F, 0x62, 65, 66, 0x40, 0x61, 67, 0xFF],
		"CBOR error at position 4: Expected Text or Break, got opcode 40");
	parse_failed(&[0x7F, 0x62, 0xC2, 0x7f, 0x61, 0x83, 0xFF], "CBOR error at position 3: Text not UTF-8");
	parse_failed(&[0x7F, 0x62, 0xC2, 0xC2, 0x61, 0x83, 0xFF], "CBOR error at position 3: Text not UTF-8");
	parse_failed(&[0x7F, 0x62, 0xE2, 0xA2, 0x61, 0x83, 0xFF], "CBOR error at position 4: Text not UTF-8");
	parse_failed(&[0x7F, 0x63, 0xE2, 0xA2, 0x7f, 0x61, 0x83, 0xFF], "CBOR error at position 4: Text not UTF-8");
	parse_failed(&[0x7F, 0x63, 0xF2, 0xA2, 0xA2, 0x61, 0x83, 0xFF], "CBOR error at position 5: Text not UTF-8");
	parse_failed(&[0x7F, 0x64, 0xF2, 0xA2, 0xA2, 0x7f, 0x61, 0x83, 0xFF],
		"CBOR error at position 5: Text not UTF-8");
	//Arrays
	assert_eq!(CBOR::parse(&[0x80][..]).unwrap(), CBOR::Array(vec![]));
	assert_eq!(CBOR::parse(&[0x81, 0x04][..]).unwrap(), CBOR::Array(vec![CBOR::Integer(4)]));
	assert_eq!(CBOR::parse(&[0x81, 0xC2, 0x24][..]).unwrap(), CBOR::Array(vec![CBOR::Tag(2,
		Box::new(CBOR::NegInteger(4)))]));
	parse_failed(&[0x83, 0x04, 0x07, 0x18],
		"CBOR error at position 4: Expected at least 1 bytes more, got End of Input");
	assert_eq!(CBOR::parse(&[0x83, 0x04, 0x07, 0x02][..]).unwrap(),
		CBOR::Array(vec![CBOR::Integer(4), CBOR::Integer(7), CBOR::Integer(2)]));
	assert_eq!(CBOR::parse(&[0x83, 0x04, 0x07, 0x18, 0x19][..]).unwrap(),
		CBOR::Array(vec![CBOR::Integer(4), CBOR::Integer(7), CBOR::Integer(0x19)]));
	assert_eq!(CBOR::parse(&[0x98, 3, 0x04, 0x07, 0x18, 0x19][..]).unwrap(),
		CBOR::Array(vec![CBOR::Integer(4), CBOR::Integer(7), CBOR::Integer(0x19)]));
	assert_eq!(CBOR::parse(&[0x99, 0, 3, 0x04, 0x07, 0x18, 0x19][..]).unwrap(),
		CBOR::Array(vec![CBOR::Integer(4), CBOR::Integer(7), CBOR::Integer(0x19)]));
	assert_eq!(CBOR::parse(&[0x9A, 0, 0, 0, 3, 0x04, 0x07, 0x18, 0x19][..]).unwrap(),
		CBOR::Array(vec![CBOR::Integer(4), CBOR::Integer(7), CBOR::Integer(0x19)]));
	assert_eq!(CBOR::parse(&[0x9B, 0, 0, 0, 0, 0, 0, 0, 3, 0x04, 0x07, 0x18, 0x19][..]).unwrap(),
		CBOR::Array(vec![CBOR::Integer(4), CBOR::Integer(7), CBOR::Integer(0x19)]));
	parse_failed(&[0x9B, 1, 0, 0, 0, 0, 0, 0, 0], "CBOR error at position 9: Expected Value, got End of Input");
	parse_failed(&[0x81, 1, 0], "CBOR error at position 2: Trailing garbage");
	parse_failed(&[0x9C], "CBOR error at position 0: Unknown opcode 9c");
	parse_failed(&[0x9D], "CBOR error at position 0: Unknown opcode 9d");
	parse_failed(&[0x9E], "CBOR error at position 0: Unknown opcode 9e");
	parse_failed(&[0x9F], "CBOR error at position 1: Expected Value or Break, got End of Input");
	parse_failed(&[0x9F, 0x04, 0x07, 0x18, 0x19],
		"CBOR error at position 5: Expected Value or Break, got End of Input");
	assert_eq!(CBOR::parse(&[0x9F, 0x04, 0x07, 0x18, 0x19, 0xFF][..]).unwrap(),
		CBOR::Array(vec![CBOR::Integer(4), CBOR::Integer(7), CBOR::Integer(0x19)]));
	//Dictionaries
	assert_eq!(CBOR::parse(&[0xA0][..]).unwrap(),
		CBOR::Dictionary(BTreeMap::from_iter([].iter().cloned())));
	assert_eq!(CBOR::parse(&[0xA1, 0x03, 0x05][..]).unwrap(),
		CBOR::Dictionary(BTreeMap::from_iter([(CBOR::Integer(3), CBOR::Integer(5))].iter().
		cloned())));
	parse_failed(&[0xA2, 0x03, 0x05, 0x03, 0x05], "CBOR error at position 4: Duplicate directory key");
	assert_eq!(CBOR::parse(&[0xA2, 0x03, 0x05, 0x04, 0x05][..]).unwrap(),
		CBOR::Dictionary(BTreeMap::from_iter([(CBOR::Integer(3), CBOR::Integer(5)),
		(CBOR::Integer(4), CBOR::Integer(5))].iter().cloned())));
	assert_eq!(CBOR::parse(&[0xA2, 0xC2, 0x03, 0x05, 0xC1, 0x03, 0x06][..]).unwrap(),
		CBOR::Dictionary(BTreeMap::from_iter([(CBOR::Tag(1, Box::new(CBOR::Integer(3))),
		CBOR::Integer(6)), (CBOR::Tag(2, Box::new(CBOR::Integer(3))), CBOR::Integer(5))].
		iter().cloned())));
	assert_eq!(CBOR::parse(&[0xB8, 2, 0x03, 0x05, 0x04, 0x07][..]).unwrap(),
		CBOR::Dictionary(BTreeMap::from_iter([(CBOR::Integer(3), CBOR::Integer(5)),
		(CBOR::Integer(4), CBOR::Integer(7))].iter().cloned())));
	assert_eq!(CBOR::parse(&[0xB9, 0, 2, 0x03, 0x05, 0x04, 0x07][..]).unwrap(),
		CBOR::Dictionary(BTreeMap::from_iter([(CBOR::Integer(3), CBOR::Integer(5)),
		(CBOR::Integer(4), CBOR::Integer(7))].iter().cloned())));
	assert_eq!(CBOR::parse(&[0xBA, 0, 0, 0, 2, 0x03, 0x05, 0x04, 0x07][..]).unwrap(),
		CBOR::Dictionary(BTreeMap::from_iter([(CBOR::Integer(3), CBOR::Integer(5)),
		(CBOR::Integer(4), CBOR::Integer(7))].iter().cloned())));
	assert_eq!(CBOR::parse(&[0xBB, 0, 0, 0, 0, 0, 0, 0, 2, 0x03, 0x05, 0x04, 0x07][..]).unwrap(),
		CBOR::Dictionary(BTreeMap::from_iter([(CBOR::Integer(3), CBOR::Integer(5)),
		(CBOR::Integer(4), CBOR::Integer(7))].iter().cloned())));
	parse_failed(&[0xA1, 1], "CBOR error at position 2: Expected Value, got End of Input");
	parse_failed(&[0xA1, 1, 0, 4], "CBOR error at position 3: Trailing garbage");
	parse_failed(&[0xBC], "CBOR error at position 0: Unknown opcode bc");
	parse_failed(&[0xBD], "CBOR error at position 0: Unknown opcode bd");
	parse_failed(&[0xBE], "CBOR error at position 0: Unknown opcode be");
	parse_failed(&[0xBF], "CBOR error at position 1: Expected Value or Break, got End of Input");
	parse_failed(&[0xBF,0x00], "CBOR error at position 2: Expected Value, got End of Input");
	parse_failed(&[0xBF, 0xC2, 0x03, 0x05, 0xC1, 0x03, 0x06],
		"CBOR error at position 7: Expected Value or Break, got End of Input");
	assert_eq!(CBOR::parse(&[0xBF, 0xC2, 0x03, 0x05, 0xC1, 0x03, 0x06, 0xFF][..]).unwrap(),
		CBOR::Dictionary(BTreeMap::from_iter([(CBOR::Tag(1, Box::new(CBOR::Integer(3))),
		CBOR::Integer(6)), (CBOR::Tag(2, Box::new(CBOR::Integer(3))), CBOR::Integer(5))].
		iter().cloned())));
	//Tags
	assert_eq!(CBOR::parse(&[0xC0, 0x01][..]).unwrap(), CBOR::Tag(0, Box::new(CBOR::Integer(1))));
	assert_eq!(CBOR::parse(&[0xC1, 0x03][..]).unwrap(), CBOR::Tag(1, Box::new(CBOR::Integer(3))));
	assert_eq!(CBOR::parse(&[0xD7, 0x03][..]).unwrap(), CBOR::Tag(23, Box::new(CBOR::Integer(3))));
	assert_eq!(CBOR::parse(&[0xD8, 0x01, 0x03][..]).unwrap(), CBOR::Tag(1, Box::new(
		CBOR::Integer(3))));
	assert_eq!(CBOR::parse(&[0xD9, 0x01, 0x03, 0x04][..]).unwrap(), CBOR::Tag(0x0103, Box::new(
		CBOR::Integer(4))));
	assert_eq!(CBOR::parse(&[0xDA, 0x01, 0x03, 0x05, 0x07, 0x04][..]).unwrap(), CBOR::Tag(0x01030507,
		Box::new(CBOR::Integer(4))));
	assert_eq!(CBOR::parse(&[0xDB, 0x01, 0x03, 0x05, 0x07, 0x09, 0x0B, 0x0D, 0x0F, 0x04][..]).unwrap(),
		CBOR::Tag(0x01030507090B0D0F, Box::new(CBOR::Integer(4))));
	parse_failed(&[0xDC], "CBOR error at position 0: Unknown opcode dc");
	parse_failed(&[0xDD], "CBOR error at position 0: Unknown opcode dd");
	parse_failed(&[0xDE], "CBOR error at position 0: Unknown opcode de");
	parse_failed(&[0xDF], "CBOR error at position 0: Unknown opcode df");
	parse_failed(&[0xDF, 0x00, 0xFF], "CBOR error at position 0: Unknown opcode df");
	//Simples
	assert_eq!(CBOR::parse(&[0xE0][..]).unwrap(), CBOR::Simple(0));
	assert_eq!(CBOR::parse(&[0xE1][..]).unwrap(), CBOR::Simple(1));
	assert_eq!(CBOR::parse(&[0xF4][..]).unwrap(), CBOR::Simple(20));
	assert_eq!(CBOR::parse(&[0xF5][..]).unwrap(), CBOR::Simple(21));
	assert_eq!(CBOR::parse(&[0xF6][..]).unwrap(), CBOR::Simple(22));
	assert_eq!(CBOR::parse(&[0xF7][..]).unwrap(), CBOR::Simple(23));
	parse_failed(&[0xF8, 0x1F], "CBOR error at position 0: Bad simple 31");
	assert_eq!(CBOR::parse(&[0xF8, 0x20][..]).unwrap(), CBOR::Simple(32));
	assert_eq!(CBOR::parse(&[0xF8, 0x21][..]).unwrap(), CBOR::Simple(33));
	assert_eq!(CBOR::parse(&[0xF8, 0xFF][..]).unwrap(), CBOR::Simple(255));
	//HalfFloats
	assert_eq!(CBOR::parse(&[0xF9, 0x00, 0x00][..]).unwrap(),
		CBOR::HalfFloat(0));
	assert_eq!(CBOR::parse(&[0xF9, 0x00, 0xFF][..]).unwrap(),
		CBOR::HalfFloat(0xFF));
	assert_eq!(CBOR::parse(&[0xF9, 0x01, 0x03][..]).unwrap(),
		CBOR::HalfFloat(0x0103));
	assert_eq!(CBOR::parse(&[0xF9, 0xFF, 0xFF][..]).unwrap(),
		CBOR::HalfFloat(0xFFFF));
	//Floats
	assert_eq!(CBOR::parse(&[0xFA, 0x00, 0x00, 0x00, 0x00][..]).unwrap(),
		CBOR::Float(0));
	assert_eq!(CBOR::parse(&[0xFA, 0x00, 0x00, 0xFF, 0xFF][..]).unwrap(),
		CBOR::Float(0xFFFF));
	assert_eq!(CBOR::parse(&[0xFA, 0x01, 0x03, 0x05, 0x07][..]).unwrap(),
		CBOR::Float(0x01030507));
	assert_eq!(CBOR::parse(&[0xFA, 0xFF, 0xFF, 0xFF, 0xFF][..]).unwrap(),
		CBOR::Float(0xFFFFFFFF));
	//Doubles
	assert_eq!(CBOR::parse(&[0xFB, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00][..]).unwrap(),
		CBOR::Double(0));
	assert_eq!(CBOR::parse(&[0xFB, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF][..]).unwrap(),
		CBOR::Double(0xFFFFFFFF));
	assert_eq!(CBOR::parse(&[0xFB, 0x01, 0x03, 0x05, 0x07, 0x09, 0x0B, 0x0D, 0x0F][..]).unwrap(),
		CBOR::Double(0x01030507090B0D0F));
	assert_eq!(CBOR::parse(&[0xFB, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF][..]).unwrap(),
		CBOR::Double(0xFFFFFFFFFFFFFFFF));
	//Invalid.
	parse_failed(&[0xFC], "CBOR error at position 0: Unknown opcode fc");
	parse_failed(&[0xFD], "CBOR error at position 0: Unknown opcode fd");
	parse_failed(&[0xFE], "CBOR error at position 0: Unknown opcode fe");
	parse_failed(&[0xFF], "CBOR error at position 0: Opcode ff only allowed inside opcodes 5f, 7f, 9f and bf");
}

#[test]
fn test_utf8_bad()
{
	//2 bytes.
	parse_failed(&[0x64, 0xc2, 0x7f, 0x80, 0x80], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x64, 0xc2, 0xc0, 0x80, 0x80], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x64, 0xdf, 0x7f, 0x80, 0x80], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x64, 0xdf, 0xc0, 0x80, 0x80], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x64, 0xe0, 0x7f, 0x80, 0x80], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x64, 0xe0, 0xc0, 0x80, 0x80], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x64, 0xed, 0x7f, 0x80, 0x80], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x64, 0xed, 0xc0, 0x80, 0x80], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x64, 0xee, 0x7f, 0x80, 0x80], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x64, 0xee, 0xc0, 0x80, 0x80], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x64, 0xef, 0x7f, 0x80, 0x80], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x64, 0xef, 0xc0, 0x80, 0x80], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x64, 0xf0, 0x7f, 0x80, 0x80], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x64, 0xf0, 0xc0, 0x80, 0x80], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x64, 0xf4, 0x7f, 0x80, 0x80], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x64, 0xf4, 0xc0, 0x80, 0x80], "CBOR error at position 2: Text not UTF-8");
	//3 bytes.
	parse_failed(&[0x64, 0xe0, 0xa0, 0x7f, 0x80], "CBOR error at position 3: Text not UTF-8");
	parse_failed(&[0x64, 0xe0, 0xa0, 0xc0, 0x80], "CBOR error at position 3: Text not UTF-8");
	parse_failed(&[0x64, 0xed, 0x9f, 0x7f, 0x80], "CBOR error at position 3: Text not UTF-8");
	parse_failed(&[0x64, 0xed, 0x9f, 0xc0, 0x80], "CBOR error at position 3: Text not UTF-8");
	parse_failed(&[0x64, 0xee, 0x80, 0x7f, 0x80], "CBOR error at position 3: Text not UTF-8");
	parse_failed(&[0x64, 0xee, 0x80, 0xc0, 0x80], "CBOR error at position 3: Text not UTF-8");
	parse_failed(&[0x64, 0xef, 0xbf, 0x7f, 0x80], "CBOR error at position 3: Text not UTF-8");
	parse_failed(&[0x64, 0xef, 0xbf, 0xc0, 0x80], "CBOR error at position 3: Text not UTF-8");
	parse_failed(&[0x64, 0xf0, 0x90, 0x7f, 0x80], "CBOR error at position 3: Text not UTF-8");
	parse_failed(&[0x64, 0xf0, 0x90, 0xc0, 0x80], "CBOR error at position 3: Text not UTF-8");
	parse_failed(&[0x64, 0xf4, 0x8f, 0x7f, 0x80], "CBOR error at position 3: Text not UTF-8");
	parse_failed(&[0x64, 0xf4, 0x8f, 0xc0, 0x80], "CBOR error at position 3: Text not UTF-8");
	//4 bytes.
	parse_failed(&[0x64, 0xf0, 0x90, 0x80, 0x7f], "CBOR error at position 4: Text not UTF-8");
	parse_failed(&[0x64, 0xf0, 0x90, 0x80, 0xc0], "CBOR error at position 4: Text not UTF-8");
	parse_failed(&[0x64, 0xf4, 0x8f, 0xbf, 0x7f], "CBOR error at position 4: Text not UTF-8");
	parse_failed(&[0x64, 0xf4, 0x8f, 0xbf, 0xc0], "CBOR error at position 4: Text not UTF-8");
}

#[test]
fn test_utf8_edge()
{
	//UTF-8 Phase change edges
	parse_failed(&[0x62, 0xc1, 0xbf], "CBOR error at position 1: Text not UTF-8");
	assert_eq!(CBOR::parse(&[0x62,0xc2,0x80][..]).unwrap(), CBOR::String(format!("\u{80}")));
	parse_failed(&[0x63, 0xe0, 0x9f, 0xbf], "CBOR error at position 2: Text not UTF-8");
	assert_eq!(CBOR::parse(&[0x63,0xe0,0xa0,0x80][..]).unwrap(), CBOR::String(format!("\u{800}")));
	assert_eq!(CBOR::parse(&[0x63,0xed,0x9f,0xbf][..]).unwrap(), CBOR::String(format!("\u{d7ff}")));
	parse_failed(&[0x63, 0xed, 0xa0, 0x80], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x63, 0xed, 0xbf, 0xbf], "CBOR error at position 2: Text not UTF-8");
	assert_eq!(CBOR::parse(&[0x63,0xee,0x80,0x80][..]).unwrap(), CBOR::String(format!("\u{e000}")));
	parse_failed(&[0x64, 0xf0, 0x8f, 0xbf, 0xbf], "CBOR error at position 2: Text not UTF-8");
	assert_eq!(CBOR::parse(&[0x64,0xf0,0x90,0x80,0x80][..]).unwrap(), CBOR::String(format!("\u{10000}")));
	assert_eq!(CBOR::parse(&[0x64,0xf4,0x8f,0xbf,0xbf][..]).unwrap(), CBOR::String(format!("\u{10ffff}")));
	parse_failed(&[0x64, 0xf4, 0x90, 0x80, 0x80], "CBOR error at position 2: Text not UTF-8");
	//1 byte Truncations of above.
	parse_failed(&[0x61, 0xc1], "CBOR error at position 1: Text not UTF-8");
	parse_failed(&[0x61, 0xc2], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x61, 0xdf], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x61, 0xe0], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x61, 0xed], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x61, 0xee], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x61, 0xef], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x61, 0xf0], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x61, 0xf4], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x61, 0xf5], "CBOR error at position 1: Text not UTF-8");
	//2 byte truncations of above.
	parse_failed(&[0x62, 0xe0, 0x9f], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x62, 0xe0, 0xa0], "CBOR error at position 3: Text not UTF-8");
	parse_failed(&[0x62, 0xed, 0x9f], "CBOR error at position 3: Text not UTF-8");
	parse_failed(&[0x62, 0xed, 0xa0], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x62, 0xed, 0xbf], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x62, 0xee, 0x80], "CBOR error at position 3: Text not UTF-8");
	parse_failed(&[0x62, 0xef, 0xbf], "CBOR error at position 3: Text not UTF-8");
	parse_failed(&[0x62, 0xf0, 0x8f], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x62, 0xf0, 0x90], "CBOR error at position 3: Text not UTF-8");
	parse_failed(&[0x62, 0xf4, 0x8f], "CBOR error at position 3: Text not UTF-8");
	parse_failed(&[0x62, 0xf4, 0x90], "CBOR error at position 2: Text not UTF-8");
	//3 byte truncations of above.
	parse_failed(&[0x63, 0xf0, 0x8f, 0xbf], "CBOR error at position 2: Text not UTF-8");
	parse_failed(&[0x63, 0xf0, 0x90, 0x80], "CBOR error at position 4: Text not UTF-8");
	parse_failed(&[0x63, 0xf4, 0x8f, 0xbf], "CBOR error at position 4: Text not UTF-8");
	parse_failed(&[0x63, 0xf4, 0x90, 0x80], "CBOR error at position 2: Text not UTF-8");
}


#[test]
fn overlong_bom()
{
	let input = [0xF0, 0x8F, 0xBB, 0xBF];
	assert!(from_utf8(&input).is_err());
}


#[test]
fn cbor_casts()
{
	assert_eq!(CBOR::NULL.as_null(), Some(()));
	assert_eq!(CBOR::UNDEFINED.as_null(), None);
	assert_eq!(CBOR::FALSE.as_null(), None);
	assert_eq!(CBOR::TRUE.as_null(), None);

	assert_eq!(CBOR::NULL.as_undefined(), None);
	assert_eq!(CBOR::UNDEFINED.as_undefined(), Some(()));
	assert_eq!(CBOR::FALSE.as_undefined(), None);
	assert_eq!(CBOR::TRUE.as_undefined(), None);

	assert_eq!(CBOR::NULL.as_boolean(), None);
	assert_eq!(CBOR::UNDEFINED.as_boolean(), None);
	assert_eq!(CBOR::FALSE.as_boolean(), Some(false));
	assert_eq!(CBOR::TRUE.as_boolean(), Some(true));

	assert_eq!(CBOR::NULL.as_unsigned(), None);
	assert_eq!(CBOR::FALSE.as_unsigned(), None);
	assert_eq!(CBOR::Integer(0).as_unsigned(), Some(0));
	assert_eq!(CBOR::Integer(1).as_unsigned(), Some(1));
	assert_eq!(CBOR::Integer(1234).as_unsigned(), Some(1234));
	assert_eq!(CBOR::Integer(9223372036854775808).as_unsigned(), Some(9223372036854775808));
	assert_eq!(CBOR::NegInteger(0).as_unsigned(), None);

	assert_eq!(CBOR::NULL.as_integer(), None);
	assert_eq!(CBOR::FALSE.as_integer(), None);
	assert_eq!(CBOR::Integer(0).as_integer(), Some(0));
	assert_eq!(CBOR::Integer(1).as_integer(), Some(1));
	assert_eq!(CBOR::Integer(1234).as_integer(), Some(1234));
	assert_eq!(CBOR::Integer(9223372036854775808).as_integer(), None);
	assert_eq!(CBOR::NegInteger(0).as_integer(), Some(-1));
	assert_eq!(CBOR::NegInteger(9223372036854775807).as_integer(), Some(-9223372036854775808i64));

	assert_eq!(CBOR::Integer(0).as_unsigned_negative(), None);
	assert_eq!(CBOR::NegInteger(0).as_unsigned_negative(), Some(0));
	assert_eq!(CBOR::NegInteger(9223372036854775807).as_unsigned_negative(), Some(9223372036854775807));

	assert_eq!(CBOR::Integer(0).as_octets(), None);
	assert_eq!(CBOR::parse(&[0x40]).unwrap().as_octets().unwrap().len(), 0);
	assert_eq!(CBOR::parse(&[0x43, 102, 111, 111]).unwrap().as_octets().unwrap().len(), 3);
	assert_eq!(CBOR::parse(&[0x60]).unwrap().as_octets().is_none(), true);
	assert_eq!(CBOR::parse(&[0x63, 102, 111, 111]).unwrap().as_octets().is_none(), true);

	assert_eq!(CBOR::Integer(0).as_string(), None);
	assert_eq!(CBOR::parse(&[0x40]).unwrap().as_string().is_none(), true);
	assert_eq!(CBOR::parse(&[0x43, 102, 111, 111]).unwrap().as_string().is_none(), true);
	assert_eq!(CBOR::parse(&[0x60]).unwrap().as_string().unwrap().len(), 0);
	assert_eq!(CBOR::parse(&[0x63, 102, 111, 111]).unwrap().as_string().unwrap().len(), 3);

	assert_eq!(CBOR::Integer(0).as_array().is_none(), true);
	assert_eq!(CBOR::parse(&[0x40]).unwrap().as_array().is_none(), true);
	assert_eq!(CBOR::parse(&[0x43, 102, 111, 111]).unwrap().as_array().is_none(), true);
	assert_eq!(CBOR::parse(&[0x80]).unwrap().as_array().unwrap().len(), 0);
	assert_eq!(CBOR::parse(&[0x83, 0x13, 0x24, 0x01]).unwrap().as_array().unwrap().len(), 3);

	assert_eq!(CBOR::parse(&[0x80]).unwrap().as_dictionary().is_none(), true);
	assert_eq!(CBOR::parse(&[0x83, 0x13, 0x24, 0x01]).unwrap().as_dictionary().is_none(), true);
	assert_eq!(CBOR::parse(&[0xA0]).unwrap().as_dictionary().unwrap().len(), 0);
	assert_eq!(CBOR::parse(&[0xA2, 0x13, 0x24, 0x01, 0x17]).unwrap().as_dictionary().unwrap().len(), 2);

	assert_eq!(CBOR::parse(&[0xA0]).unwrap().as_tag().is_none(), true);
	assert_eq!(CBOR::parse(&[0xC3,0xF4]).unwrap().as_tag(), Some((3u64, &CBOR::FALSE)));

	assert_eq!(CBOR::parse(&[0xF9, 0x12, 0x34]).unwrap().as_half_float(), Some(0x1234));
	assert_eq!(CBOR::parse(&[0xFA, 0x12, 0x34, 0x56, 0x78]).unwrap().as_half_float(), None);
	assert_eq!(CBOR::parse(&[0xFB, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0]).unwrap().as_half_float(),
		None);

	assert_eq!(CBOR::parse(&[0xF9, 0x12, 0x34]).unwrap().as_single_float(), None);
	assert_eq!(CBOR::parse(&[0xFA, 0x12, 0x34, 0x56, 0x78]).unwrap().as_single_float(), Some(0x12345678));
	assert_eq!(CBOR::parse(&[0xFB, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0]).unwrap().
		as_single_float(), None);

	assert_eq!(CBOR::parse(&[0xF9, 0x12, 0x34]).unwrap().as_double_float(), None);
	assert_eq!(CBOR::parse(&[0xFA, 0x12, 0x34, 0x56, 0x78]).unwrap().as_double_float(), None);
	assert_eq!(CBOR::parse(&[0xFB, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0]).unwrap().
		as_double_float(), Some(0x123456789ABCDEF0));
}

#[test]
fn cbor_from_integer()
{
	assert_eq!(CBOR::from_integer(-2), CBOR::NegInteger(1));
	assert_eq!(CBOR::from_integer(-1), CBOR::NegInteger(0));
	assert_eq!(CBOR::from_integer(0), CBOR::Integer(0));
	assert_eq!(CBOR::from_integer(1), CBOR::Integer(1));
}

fn test_half(s: bool, e: i32, m: u16, eref: i32, mref: u64)
{
	let vref = (s as u64) << 63 | ((eref + 1023) as u64) << 52 | mref;
	let vbref = ((s as u16) << 15) | ((e + 15) as u16) << 10 | m;
	let v = promote_half(vbref);
	assert_eq!(v, vref);
	let vbref = if vbref == 0xFE00 { 0x7E00 } else { vbref };	//Canonical NaN.
	assert_eq!(try_demote_double_to_half(v), Some(vbref));
}

fn test_single(s: bool, e: i32, m: u32, eref: i32, mref: u64)
{
	let vref = (s as u64) << 63 | ((eref + 1023) as u64) << 52 | mref;
	let vbref = ((s as u32) << 31) | ((e + 127) as u32) << 23 | m;
	let v = promote_single(vbref);
	assert_eq!(v, vref);
	let vbref = if vbref == 0xFFC00000 { 0x7FC00000 } else { vbref };	//Canonical NaN.
	assert_eq!(try_demote_double_to_single(v), Some(vbref));
}

#[test]
fn test_promote_single()
{
	const S64: u64 = 0xFFFFFC0000000;
	const M64: u64 = 0xFFFFFE0000000;
	test_single(false, -127, 0, -1023, 0);			//+0.
	test_single(true, -127, 0, -1023, 0);			//-0.
	test_single(false, -127, 1, -149, 0);			//+epsilon.
	test_single(true, -127, 1, -149, 0);			//-epsilon.
	test_single(false, -127, 0x7FFFFF, -127, S64);		//+biggest subnormal.
	test_single(true, -127, 0x7FFFFF, -127, S64);		//-biggest subnormal.
	test_single(false, -126, 0, -126, 0);			//+smallest non-subnormal.
	test_single(true, -126, 0, -126, 0);			//-smallest non-subnormal.
	test_single(false, 0, 0, 0, 0);				//+1
	test_single(true, 0, 0, 0, 0);				//-1
	test_single(false, 127, 0x7FFFFF, 127, M64);		//+biggest finite.
	test_single(true, 127, 0x7FFFFF, 127, M64);		//-biggest finite.
	test_single(false, 128, 0, 1024, 0);			//+infinite.
	test_single(true, 128, 0, 1024, 0);			//-infinite.
	test_single(false, 128, 0x400000, 1024, 1<<51);		//+NaN
	test_single(true, 128, 0x400000, 1024, 1<<51);		//+NaN
}

#[test]
fn test_promote_half()
{
	const S64: u64 = 0xFF80000000000;
	const M64: u64 = 0xFFC0000000000;
	test_half(false, -15, 0, -1023, 0);		//+0.
	test_half(true, -15, 0, -1023, 0);		//-0.
	test_half(false, -15, 1, -24, 0);		//+epsilon.
	test_half(true, -15, 1, -24, 0);		//-epsilon.
	test_half(false, -15, 0x3FF, -15, S64);		//+biggest subnormal.
	test_half(true, -15, 0x3FF, -15, S64);		//-biggest subnormal.
	test_half(false, -14, 0, -14, 0);		//+smallest non-subnormal.
	test_half(true, -14, 0, -14, 0);		//-smallest non-subnormal.
	test_half(false, 0, 0, 0, 0);			//+1
	test_half(true, 0, 0, 0, 0);			//-1
	test_half(false, 15, 0x3FF, 15, M64);		//+biggest finite.
	test_half(true, 15, 0x3FF, 15, M64);		//-biggest finite.
	test_half(false, 16, 0, 1024, 0);		//+infinite.
	test_half(true, 16, 0, 1024, 0);		//-infinite.
	test_half(false, 16, 0x200, 1024, 1<<51);	//+NaN
	test_half(true, 16, 0x200, 1024, 1<<51);	//+NaN
}



#[test]
fn test_cbor_serialize()
{
	//Integers.
	assert_eq!(CBOR::from(0u64).serialize_to_vec().deref(), &[0x00]);
	assert_eq!(CBOR::from(0i64).serialize_to_vec().deref(), &[0x00]);
	assert_eq!(CBOR::from(1u64).serialize_to_vec().deref(), &[0x01]);
	assert_eq!(CBOR::from(1i64).serialize_to_vec().deref(), &[0x01]);
	assert_eq!(CBOR::from(23u64).serialize_to_vec().deref(), &[0x17]);
	assert_eq!(CBOR::from(24u64).serialize_to_vec().deref(), &[0x18, 0x18]);
	assert_eq!(CBOR::from(0x34u64).serialize_to_vec().deref(), &[0x18, 0x34]);
	assert_eq!(CBOR::from(255u64).serialize_to_vec().deref(), &[0x18, 0xFF]);
	assert_eq!(CBOR::from(256u64).serialize_to_vec().deref(), &[0x19, 1, 0]);
	assert_eq!(CBOR::from(0x1234u64).serialize_to_vec().deref(), &[0x19, 0x12, 0x34]);
	assert_eq!(CBOR::from(65535u64).serialize_to_vec().deref(), &[0x19, 0xFF, 0xFF]);
	assert_eq!(CBOR::from(65536u64).serialize_to_vec().deref(), &[0x1A, 0, 1, 0, 0]);
	assert_eq!(CBOR::from(0x12345678u64).serialize_to_vec().deref(), &[0x1A, 0x12, 0x34, 0x56, 0x78]);
	assert_eq!(CBOR::from(4294967295u64).serialize_to_vec().deref(), &[0x1A, 0xFF, 0xFF, 0xFF, 0xFF]);
	assert_eq!(CBOR::from(4294967296u64).serialize_to_vec().deref(), &[0x1B, 0, 0, 0, 1, 0, 0, 0, 0]);
	assert_eq!(CBOR::from(0x123456789ABCDEF0u64).serialize_to_vec().deref(),
		&[0x1B, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0]);
	assert_eq!(CBOR::from(0x123456789ABCDEF0i64).serialize_to_vec().deref(),
		&[0x1B, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0]);
	assert_eq!(CBOR::from(9223372036854775807i64).serialize_to_vec().deref(),
		&[0x1B, 0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]);
	assert_eq!(CBOR::from(18446744073709551615u64).serialize_to_vec().deref(),
		&[0x1B, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]);
	//Negative Integers.
	assert_eq!(CBOR::from(-1i64).serialize_to_vec().deref(), &[0x20]);
	assert_eq!(CBOR::from(-2i64).serialize_to_vec().deref(), &[0x21]);
	assert_eq!(CBOR::from(-24i64).serialize_to_vec().deref(), &[0x37]);
	assert_eq!(CBOR::from(-25i64).serialize_to_vec().deref(), &[0x38, 0x18]);
	assert_eq!(CBOR::from(-0x35i64).serialize_to_vec().deref(), &[0x38, 0x34]);
	assert_eq!(CBOR::from(-256i64).serialize_to_vec().deref(), &[0x38, 0xFF]);
	assert_eq!(CBOR::from(-257i64).serialize_to_vec().deref(), &[0x39, 1, 0]);
	assert_eq!(CBOR::from(-0x1235i64).serialize_to_vec().deref(), &[0x39, 0x12, 0x34]);
	assert_eq!(CBOR::from(-65536i64).serialize_to_vec().deref(), &[0x39, 0xFF, 0xFF]);
	assert_eq!(CBOR::from(-65537i64).serialize_to_vec().deref(), &[0x3A, 0, 1, 0, 0]);
	assert_eq!(CBOR::from(-0x12345679i64).serialize_to_vec().deref(), &[0x3A, 0x12, 0x34, 0x56, 0x78]);
	assert_eq!(CBOR::from(-4294967296i64).serialize_to_vec().deref(), &[0x3A, 0xFF, 0xFF, 0xFF, 0xFF]);
	assert_eq!(CBOR::from(-4294967297i64).serialize_to_vec().deref(), &[0x3B, 0, 0, 0, 1, 0, 0, 0, 0]);
	assert_eq!(CBOR::from(-0x123456789ABCDEF1i64).serialize_to_vec().deref(),
		&[0x3B, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0]);
	assert_eq!(CBOR::from(-9223372036854775807i64).serialize_to_vec().deref(),
		&[0x3B, 0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFE]);
	assert_eq!(CBOR::from(-9223372036854775808i64).serialize_to_vec().deref(),
		&[0x3B, 0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]);
	assert_eq!(CBOR::NegInteger(18446744073709551615).serialize_to_vec().deref(),
		&[0x3B, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]);
	//Octet strings.
	assert_eq!(CBOR::from(&b""[..]).serialize_to_vec().deref(), &[0x40]);
	assert_eq!(CBOR::from(&b"X"[..]).serialize_to_vec().deref(), b"AX");
	assert_eq!(CBOR::from(&b"ABCDEFGHIJKLMNOPQRSTUVZ"[..]).serialize_to_vec().deref(),
		b"WABCDEFGHIJKLMNOPQRSTUVZ");
	assert_eq!(CBOR::from(&b"ABCDEFGHIJKLMNOPQRSTUVWZ"[..]).serialize_to_vec().deref(),
		&b"X\x18ABCDEFGHIJKLMNOPQRSTUVWZ"[..]);
	assert_eq!(CBOR::from(&b"ABCDEFGHIJKLMNOPQRSTUVWXYZ012345"[..]).serialize_to_vec().deref(),
		&b"X ABCDEFGHIJKLMNOPQRSTUVWXYZ012345"[..]);
	assert_eq!(CBOR::from(&b"ABCDEFGHIJKLMNOPQRSTUVWXYZ012345".to_vec()).serialize_to_vec().deref(),
		&b"X ABCDEFGHIJKLMNOPQRSTUVWXYZ012345"[..]);
	assert_eq!(CBOR::from(b"ABCDEFGHIJKLMNOPQRSTUVWXYZ012345".to_vec()).serialize_to_vec().deref(),
		&b"X ABCDEFGHIJKLMNOPQRSTUVWXYZ012345"[..]);
	//Text strings.
	assert_eq!(CBOR::from("").serialize_to_vec().deref(), &[0x60]);
	assert_eq!(CBOR::from("X").serialize_to_vec().deref(), b"aX");
	assert_eq!(CBOR::from("ABCDEFGHIJKLMNOPQRSTUVZ").serialize_to_vec().deref(),
		b"wABCDEFGHIJKLMNOPQRSTUVZ");
	assert_eq!(CBOR::from("ABCDEFGHIJKLMNOPQRSTUVWZ").serialize_to_vec().deref(),
		&b"x\x18ABCDEFGHIJKLMNOPQRSTUVWZ"[..]);
	assert_eq!(CBOR::from("ABCDEFGHIJKLMNOPQRSTUVWXYZ012345").serialize_to_vec().deref(),
		&b"x ABCDEFGHIJKLMNOPQRSTUVWXYZ012345"[..]);
	assert_eq!(CBOR::from(&"ABCDEFGHIJKLMNOPQRSTUVWXYZ012345".to_owned()).serialize_to_vec().deref(),
		&b"x ABCDEFGHIJKLMNOPQRSTUVWXYZ012345"[..]);
	assert_eq!(CBOR::from("ABCDEFGHIJKLMNOPQRSTUVWXYZ012345".to_owned()).serialize_to_vec().deref(),
		&b"x ABCDEFGHIJKLMNOPQRSTUVWXYZ012345"[..]);
	//Arrays.
	assert_eq!(CBOR::Array(Vec::new()).serialize_to_vec().deref(), &[0x80]);
	assert_eq!(CBOR::from(&[
		CBOR::from(23u64),
	][..]).serialize_to_vec().deref(), &[0x81, 0x17]);
	assert_eq!(CBOR::from(&[
		CBOR::from(23u64), CBOR::from(255u64),
	][..]).serialize_to_vec().deref(), &[0x82, 0x17, 0x18, 0xFF]);
	assert_eq!(CBOR::from(&[
		CBOR::from("A"), CBOR::from("B"), CBOR::from("C"), CBOR::from("D"),
		CBOR::from("E"), CBOR::from("F"), CBOR::from("G"), CBOR::from("H"),
		CBOR::from("I"), CBOR::from("J"), CBOR::from("K"), CBOR::from("L"),
		CBOR::from("M"), CBOR::from("N"), CBOR::from("O"), CBOR::from("P"),
		CBOR::from("Q"), CBOR::from("R"), CBOR::from("S"), CBOR::from("T"),
		CBOR::from("U"), CBOR::from("V"), CBOR::from("W"), CBOR::from("X"),
		CBOR::from("Y"), CBOR::from("Z"),
	][..]).serialize_to_vec().deref(), &b"\x98\x1AaAaBaCaDaEaFaGaHaIaJaKaLaMaNaOaPaQaRaSaTaUaVaWaXaYaZ"[..]);
	assert_eq!(CBOR::from(&[
		CBOR::from(&[CBOR::from("ZOT"), CBOR::from("QUX")][..]),
	][..]).serialize_to_vec().deref(), b"\x81\x82cZOTcQUX");
	assert_eq!(CBOR::from(&[
		CBOR::from(&[CBOR::from("ZOT"), CBOR::from("QUX")][..]),
	].to_vec()).serialize_to_vec().deref(), b"\x81\x82cZOTcQUX");
	assert_eq!(CBOR::from([
		CBOR::from(&[CBOR::from("ZOT"), CBOR::from("QUX")][..]),
	].to_vec()).serialize_to_vec().deref(), b"\x81\x82cZOTcQUX");
	//Dictionaries.
	assert_eq!(CBOR::Dictionary(BTreeMap::new()).serialize_to_vec().deref(), &[0xA0]);
	assert_eq!(CBOR::from(&[
		(CBOR::from(23u64), CBOR::from(true)),
	][..]).serialize_to_vec().deref(), &[0xA1, 0x17, 0xF5]);
	assert_eq!(CBOR::from(&[
		(CBOR::from(23u64), CBOR::from(true)),
		(CBOR::from(22u64), CBOR::from(false)),
	][..]).serialize_to_vec().deref(), &[0xA2, 0x16, 0xF4, 0x17, 0xF5]);
	assert_eq!(CBOR::from(&[
		(CBOR::from(23u64), CBOR::Simple(23)), (CBOR::from(22u64), CBOR::Simple(22)),
		(CBOR::from(21u64), CBOR::Simple(21)), (CBOR::from(20u64), CBOR::Simple(20)),
		(CBOR::from(19u64), CBOR::Simple(19)), (CBOR::from(18u64), CBOR::Simple(18)),
		(CBOR::from(17u64), CBOR::Simple(17)), (CBOR::from(16u64), CBOR::Simple(16)),
		(CBOR::from(15u64), CBOR::Simple(15)), (CBOR::from(14u64), CBOR::Simple(14)),
		(CBOR::from(13u64), CBOR::Simple(13)), (CBOR::from(12u64), CBOR::Simple(12)),
		(CBOR::from(11u64), CBOR::Simple(11)), (CBOR::from(10u64), CBOR::Simple(10)),
		(CBOR::from(9u64), CBOR::Simple(9)), (CBOR::from(8u64), CBOR::Simple(8)),
		(CBOR::from(7u64), CBOR::Simple(7)), (CBOR::from(6u64), CBOR::Simple(6)),
		(CBOR::from(5u64), CBOR::Simple(5)), (CBOR::from(4u64), CBOR::Simple(4)),
		(CBOR::from(3u64), CBOR::Simple(3)), (CBOR::from(2u64), CBOR::Simple(2)),
		(CBOR::from(1u64), CBOR::Simple(1)), (CBOR::from(0u64), CBOR::Simple(0)),
	][..]).serialize_to_vec().deref(), &[0xB8, 0x18, 0x00, 0xE0, 0x01, 0xE1, 0x02, 0xE2, 0x03, 0xE3,
		0x04, 0xE4, 0x05, 0xE5, 0x06, 0xE6, 0x07, 0xE7, 0x08, 0xE8, 0x09, 0xE9, 0x0A, 0xEA, 0x0B, 0xEB,
		0x0C, 0xEC, 0x0D, 0xED, 0x0E, 0xEE, 0x0F, 0xEF, 0x10, 0xF0, 0x11, 0xF1, 0x12, 0xF2, 0x13, 0xF3,
		0x14, 0xF4, 0x15, 0xF5, 0x16, 0xF6, 0x17, 0xF7][..]);
	//Tags.
	assert_eq!(CBOR::from((0u64, CBOR::from(false))).serialize_to_vec().deref(), &[0xC0, 0xF4]);
	assert_eq!(CBOR::from((1u64, CBOR::from(false))).serialize_to_vec().deref(), &[0xC1, 0xF4]);
	assert_eq!(CBOR::from((23u64, CBOR::from(false))).serialize_to_vec().deref(), &[0xD7, 0xF4]);
	assert_eq!(CBOR::from((24u64, CBOR::from(false))).serialize_to_vec().deref(), &[0xD8, 0x18, 0xF4]);
	assert_eq!(CBOR::from((0x34u64, CBOR::from(false))).serialize_to_vec().deref(), &[0xD8, 0x34, 0xF4]);
	assert_eq!(CBOR::from((255u64, CBOR::from(false))).serialize_to_vec().deref(), &[0xD8, 0xFF, 0xF4]);
	assert_eq!(CBOR::from((256u64, CBOR::from(false))).serialize_to_vec().deref(), &[0xD9, 1, 0, 0xF4]);
	assert_eq!(CBOR::from((0x1234u64, CBOR::from(false))).serialize_to_vec().deref(),
		&[0xD9, 0x12, 0x34, 0xF4]);
	assert_eq!(CBOR::from((65535u64, CBOR::from(false))).serialize_to_vec().deref(),
		&[0xD9, 0xFF, 0xFF, 0xF4]);
	assert_eq!(CBOR::from((65536u64, CBOR::from(false))).serialize_to_vec().deref(),
		&[0xDA, 0, 1, 0, 0, 0xF4]);
	assert_eq!(CBOR::from((0x12345678u64, CBOR::from(false))).serialize_to_vec().deref(),
		&[0xDA, 0x12, 0x34, 0x56, 0x78, 0xF4]);
	assert_eq!(CBOR::from((4294967295u64, CBOR::from(false))).serialize_to_vec().deref(),
		&[0xDA, 0xFF, 0xFF, 0xFF, 0xFF, 0xF4]);
	assert_eq!(CBOR::from((4294967296u64, CBOR::from(false))).serialize_to_vec().deref(),
		&[0xDB, 0, 0, 0, 1, 0, 0, 0, 0, 0xF4]);
	assert_eq!(CBOR::from((0x123456789ABCDEF0u64, CBOR::from(false))).serialize_to_vec().deref(),
		&[0xDB, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0, 0xF4]);
	assert_eq!(CBOR::from((18446744073709551615u64, CBOR::from(false))).serialize_to_vec().deref(),
		&[0xDB, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xF4]);
	assert_eq!(CBOR::from((0u64, CBOR::from((2u64, CBOR::from(false))))).serialize_to_vec().deref(),
		&[0xC0, 0xC2, 0xF4]);
	//Simples.
	assert_eq!(CBOR::Simple(0).serialize_to_vec().deref(), &[0xE0]);
	assert_eq!(CBOR::Simple(23).serialize_to_vec().deref(), &[0xF7]);
	assert_eq!(CBOR::Simple(32).serialize_to_vec().deref(), &[0xF8, 0x20]);
	assert_eq!(CBOR::Simple(255).serialize_to_vec().deref(), &[0xF8, 0xFF]);
	//FP.
	assert_eq!(CBOR::HalfFloat(0x7C00).serialize_to_vec().deref(), &[0xF9, 0x7C, 0x00]);
	assert_eq!(CBOR::HalfFloat(0xFC00).serialize_to_vec().deref(), &[0xF9, 0xFC, 0x00]);
	assert_eq!(CBOR::HalfFloat(0x7FFF).serialize_to_vec().deref(), &[0xF9, 0x7E, 0x00]);
	assert_eq!(CBOR::HalfFloat(0xFFFF).serialize_to_vec().deref(), &[0xF9, 0x7E, 0x00]);
	assert_eq!(CBOR::Float(0x7F800000).serialize_to_vec().deref(), &[0xF9, 0x7C, 0x00]);
	assert_eq!(CBOR::Float(0xFF800000).serialize_to_vec().deref(), &[0xF9, 0xFC, 0x00]);
	assert_eq!(CBOR::Float(0x7FFFFFFF).serialize_to_vec().deref(), &[0xF9, 0x7E, 0x00]);
	assert_eq!(CBOR::Float(0xFFFFFFFF).serialize_to_vec().deref(), &[0xF9, 0x7E, 0x00]);
	assert_eq!(CBOR::Double(0x7FF0000000000000).serialize_to_vec().deref(), &[0xF9, 0x7C, 0x00]);
	assert_eq!(CBOR::Double(0xFFF0000000000000).serialize_to_vec().deref(), &[0xF9, 0xFC, 0x00]);
	assert_eq!(CBOR::Double(0x7FFFFFFFFFFFFFFF).serialize_to_vec().deref(), &[0xF9, 0x7E, 0x00]);
	assert_eq!(CBOR::Double(0xFFFFFFFFFFFFFFFF).serialize_to_vec().deref(), &[0xF9, 0x7E, 0x00]);
	assert_eq!(CBOR::from(0.0f32).serialize_to_vec().deref(), &[0xF9, 0x00, 0x00]);
	assert_eq!(CBOR::from(0.0f64).serialize_to_vec().deref(), &[0xF9, 0x00, 0x00]);
	assert_eq!(CBOR::from(1.0f32).serialize_to_vec().deref(), &[0xF9, 0x3C, 0x00]);
	assert_eq!(CBOR::from(1.0f64).serialize_to_vec().deref(), &[0xF9, 0x3C, 0x00]);
	assert_eq!(CBOR::from(-1.0f32).serialize_to_vec().deref(), &[0xF9, 0xBC, 0x00]);
	assert_eq!(CBOR::from(-1.0f64).serialize_to_vec().deref(), &[0xF9, 0xBC, 0x00]);
	assert_eq!(CBOR::from(2049.0f32).serialize_to_vec().deref(), &[0xFA, 0x45, 0x00, 0x10, 0x00]);
	assert_eq!(CBOR::from(2049.0f64).serialize_to_vec().deref(), &[0xFA, 0x45, 0x00, 0x10, 0x00]);
	assert_eq!(CBOR::from(-2049.0f32).serialize_to_vec().deref(), &[0xFA, 0xC5, 0x00, 0x10, 0x00]);
	assert_eq!(CBOR::from(-2049.0f64).serialize_to_vec().deref(), &[0xFA, 0xC5, 0x00, 0x10, 0x00]);
	assert_eq!(CBOR::from(16777217.0f64).serialize_to_vec().deref(),
		&[0xFB, 0x41, 0x70, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00]);
	assert_eq!(CBOR::from(-16777217.0f64).serialize_to_vec().deref(),
		&[0xFB, 0xC1, 0x70, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00]);
}

fn test_cbor_roundtrip(cbor: &[u8])
{
	let cbor2 = CBOR::parse(cbor).unwrap();
	let cbor3 = cbor2.serialize_to_vec();
	assert_eq!(cbor,&cbor3[..]);
}

#[test]
fn insane_alloc_32()
{
	let cbor = [0x5a,0xff,0xfe,0xfd,0xfc,0x00];
	CBOR::parse(&cbor).unwrap_err();
	let cbor = [0x7a,0xff,0xfe,0xfd,0xfc,0x00];
	CBOR::parse(&cbor).unwrap_err();
}

#[test]
fn insane_alloc_64()
{
	let cbor = [0x5b,0xff,0xfe,0xfd,0xfc,0xfb,0xfa,0xf9,0xf8,0x00];
	CBOR::parse(&cbor).unwrap_err();
	let cbor = [0x7b,0xff,0xfe,0xfd,0xfc,0xfb,0xfa,0xf9,0xf8,0x00];
	CBOR::parse(&cbor).unwrap_err();
}

#[test]
fn multipart_octet_string()
{
	let cbor = [0x5f,0x43,b'f',b'o',b'o',0x46,b'b',b'a',b'r',b'z',b'o',b't',0xff];
	let cbor2 = CBOR::parse(&cbor).unwrap();
	let cbor3 = cbor2.serialize_to_vec();
	assert_eq!(&cbor3[..], b"\x49foobarzot");
}

#[test]
fn multipart_text_string()
{
	let cbor = [0x7f,0x63,b'f',b'o',b'o',0x66,b'b',b'a',b'r',b'z',b'o',b't',0xff];
	let cbor2 = CBOR::parse(&cbor).unwrap();
	let cbor3 = cbor2.serialize_to_vec();
	assert_eq!(&cbor3[..], b"\x69foobarzot");
}

#[test]
fn multipart_utf8()
{
	CBOR::parse(&[0x7f,0x61,0xc2,0x61,0x80,0xff]).unwrap_err();
}

#[test]
fn cbor_opcodes_nest_5f()
{
	for i in 0..256 {
		let opc = i as u8;
		let major = opc / 32;
		let minor = opc % 32;
		if (minor==28||minor==29||minor==30||major==2||opc==0xFF) && opc!=0x5F { continue; }
		let e = super::parse_cbor(&[0x5f,opc,0,0,0,0,0,0,0,0]).unwrap_err();
		let rf = format!("CBOR error at position 1: Expected Octets or Break, got opcode {opc:02x}");
		assert_eq!(e.to_string(), rf);
	}
}

#[test]
fn cbor_opcodes_nest_7f()
{
	for i in 0..256 {
		let opc = i as u8;
		let major = opc / 32;
		let minor = opc % 32;
		if (minor==28||minor==29||minor==30||major==3||opc==0xFF) && opc!=0x7F { continue; }
		let e = super::parse_cbor(&[0x7f,opc,0,0,0,0,0,0,0,0]).unwrap_err();
		let rf = format!("CBOR error at position 1: Expected Text or Break, got opcode {opc:02x}");
		assert_eq!(e.to_string(), rf);
	}
}

#[test]
fn cbor_bad_opcodes()
{
	for i in 0..8 { for j in 28..31 {
		//Check that it is blowing up on opcode at offset 2.
		let opc = 32*i+j;
		let e = super::parse_cbor(&[0x82,0x00,opc]).unwrap_err();
		let rf = format!("CBOR error at position 2: Unknown opcode {opc:02x}");
		assert_eq!(e.to_string(), rf);
	}}
	//Check that it is blowing up on opcode at offset 2.
	let e = super::parse_cbor(&[0x82,0x00,0x1f]).unwrap_err();
	let rf = format!("CBOR error at position 2: Unknown opcode 1f");
	assert_eq!(e.to_string(), rf);
	let e = super::parse_cbor(&[0x82,0x00,0x3f]).unwrap_err();
	let rf = format!("CBOR error at position 2: Unknown opcode 3f");
	assert_eq!(e.to_string(), rf);
	let e = super::parse_cbor(&[0x82,0x00,0xdf]).unwrap_err();
	let rf = format!("CBOR error at position 2: Unknown opcode df");
	assert_eq!(e.to_string(), rf);
}


#[test]
fn test_cbor_opcodes()
{
	for i in 0..24 { test_cbor_roundtrip(&[i]); }
	test_cbor_roundtrip(&[0x18,0x24]);
	test_cbor_roundtrip(&[0x19,0x24,0x25]);
	test_cbor_roundtrip(&[0x1a,0x24,0x25,0x26,0x27]);
	test_cbor_roundtrip(&[0x1b,0x24,0x25,0x26,0x27,0x28,0x29,0x30,0x31]);
	for i in 0..24 { test_cbor_roundtrip(&[i+32]); }
	test_cbor_roundtrip(&[0x38,0x24]);
	test_cbor_roundtrip(&[0x39,0x24,0x25]);
	test_cbor_roundtrip(&[0x3a,0x24,0x25,0x26,0x27]);
	test_cbor_roundtrip(&[0x3b,0x24,0x25,0x26,0x27,0x28,0x29,0x30,0x31]);
	
	for i in 0..24 { test_cbor_roundtrip(&[i+192,0]); }
	test_cbor_roundtrip(&[0xd8,0x24,0]);
	test_cbor_roundtrip(&[0xd9,0x24,0x25,0]);
	test_cbor_roundtrip(&[0xda,0x24,0x25,0x26,0x27,0]);
	test_cbor_roundtrip(&[0xdb,0x24,0x25,0x26,0x27,0x28,0x29,0x30,0x31,0]);
	for i in 0..24 { test_cbor_roundtrip(&[i+224]); }
	for i in 32..255 { test_cbor_roundtrip(&[0xf8,i]); }
	test_cbor_roundtrip(&[0xf9,0x24,0x25]);
	test_cbor_roundtrip(&[0xfa,0x24,0x25,0x26,0x27]);
	test_cbor_roundtrip(&[0xfb,0x24,0x25,0x26,0x27,0x28,0x29,0x30,0x31]);
}

#[test]
fn test_insane_nest()
{
	let mut cbor = vec![0xC0; MAX_DEPTH];
	cbor.push(0);
	let _x = CBOR::parse(&cbor).expect("Should have parsed");
}

#[test]
fn test_insane_nest2()
{
	let mut cbor = vec![0x81; MAX_DEPTH];
	cbor.push(0);
	let _x = CBOR::parse(&cbor).expect("Should have parsed");
}

#[test]
fn test_insane_nest3()
{
	let mut cbor = Vec::new();
	for _ in 0..MAX_DEPTH { cbor.extend_from_slice(&[0xA1, 0x01]); }
	cbor.push(0);
	let _x = CBOR::parse(&cbor).expect("Should have parsed");
}

#[test]
fn cbor_from_integer_edge_cases()
{
	assert_eq!(CBOR::from_integer(-9223372036854775808), CBOR::NegInteger(9223372036854775807));
	assert_eq!(CBOR::from_integer(-9223372036854775807), CBOR::NegInteger(9223372036854775806));
	assert_eq!(CBOR::from_integer(9223372036854775806), CBOR::Integer(9223372036854775806));
	assert_eq!(CBOR::from_integer(9223372036854775807), CBOR::Integer(9223372036854775807));
}

#[test]
fn cbor_as_integer_edge_cases()
{
	assert_eq!(None, CBOR::NegInteger(9223372036854775808).as_integer());
	assert_eq!(Some(-9223372036854775808), CBOR::NegInteger(9223372036854775807).as_integer());
	assert_eq!(Some(-9223372036854775807), CBOR::NegInteger(9223372036854775806).as_integer());
	assert_eq!(Some(9223372036854775806), CBOR::Integer(9223372036854775806).as_integer());
	assert_eq!(Some(9223372036854775807), CBOR::Integer(9223372036854775807).as_integer());
	assert_eq!(None, CBOR::Integer(9223372036854775808).as_integer());
}

/*
//FIXME: Fix the drop code to not cause stack overflow, so extremely deeply nested CBOR can be parsed.
#[test]
fn test_insane_nest_real()
{
	let mut cbor = vec![0xC0; 1_000_000];
	cbor.push(0);
	let x = CBOR::parse(&cbor).expect("Should have parsed");
}
*/
