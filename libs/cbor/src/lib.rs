//!CBOR encoder/decoder
//!
//!This crate implements a CBOR encoder and decoder.
#![forbid(unsafe_code)]
#![deny(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]

#[cfg(test)] #[macro_use] extern crate std;
use btls_aux_collections::Box;
use btls_aux_collections::BTreeMap;
use btls_aux_collections::String;
use btls_aux_collections::ToOwned;
use btls_aux_collections::Vec;
use btls_aux_fail::dtry;
use btls_aux_fail::f_break;
use btls_aux_fail::f_return;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::fail_if_none;
use btls_aux_memory::split_at;
use btls_aux_memory::split_head_tail;
use core::cmp::Ord;
use core::cmp::Ordering;
use core::cmp::PartialEq;
use core::cmp::PartialOrd;
use core::convert::TryFrom;
use core::convert::TryInto;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::ops::Deref;
use core::str::from_utf8;

///Major error class.
///
///This enumeration conveys the major classification of an error.
///
///Objects of this type are constructed by the [`ParseError::classify()`] method.
///
///[`ParseError::classify()`]: struct.ParseError.html#method.classify
#[derive(Copy,Clone,PartialEq,Eq,Debug)]
pub enum ErrorClass
{
	///CBOR truncated.
	///
	///This error might be fixed by supplying more data.
	Truncated,
	///Invalid CBOR.
	BadFormat,
	///A dictionary contains a duplicate key.
	DuplicateKey,
	///Junk after end of CBOR.
	GarbageAfterEnd,
	///CBOR exceeds internal limitations of the implementation.
	///
	///E.g., Limitation on nesting depth was exceeded.
	InternalLimitation,
	///The CBOR triggered a BUG in the implementation.
	///
	///This should never happen.
	InternalError,
}

///Print Error parsing CBOR.
///
///This structure implements `Display` for printing human-readable error message using the `{}` format placeholder.
///This differs from [`ParseError`] in that the offset of the error is not printed.
///
///Objects of this type are crated by the [`ParseError::message()`] method.
///
///[`ParseError`]: struct.ParseError.html
///[`ParseError::message()`]: struct.ParseError.html#method.message
#[derive(Copy,Clone,Debug)]
pub struct ParseErrorPrint(_ParseError);

///Error parsing CBOR.
///
///This structure implements `Display` for printing human-readable error message using the `{}` format placeholder.
#[derive(Copy,Clone,Debug)]
pub struct ParseError(usize,_ParseError);
#[derive(Copy,Clone,Debug)]
enum _ParseError
{
	Truncated,
	TruncatedBreak,
	TruncatedOctets,
	TruncatedText,
	UnexpectedEnd2(u64),
	BadSimple(u8),
	UnknownOpcode(u8),
	BadOpcode5F(u8),
	BadOpcode7F(u8),
	BadOpcodeFF,
	NotUtf8,
	Garbage,
	DuplicateKey,
	TooDeep,
}

impl Display for _ParseError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::_ParseError::*;
		match *self {
			Truncated => write!(f, "Expected Value, got End of Input"),
			TruncatedBreak => write!(f, "Expected Value or Break, got End of Input"),
			TruncatedOctets => write!(f, "Expected Octets or Break, got End of Input"),
			TruncatedText => write!(f, "Expected Text or Break, got End of Input"),
			UnexpectedEnd2(s) => write!(f, "Expected at least {s} bytes more, got End of Input"),
			BadSimple(b) => write!(f, "Bad simple {b}"),
			UnknownOpcode(b) => write!(f, "Unknown opcode {b:02x}"),
			BadOpcode5F(b) => write!(f, "Expected Octets or Break, got opcode {b:02x}"),
			BadOpcode7F(b) => write!(f, "Expected Text or Break, got opcode {b:02x}"),
			BadOpcodeFF => write!(f, "Opcode ff only allowed inside opcodes 5f, 7f, 9f and bf"),
			NotUtf8 => write!(f, "Text not UTF-8"),
			Garbage => write!(f, "Trailing garbage"),
			DuplicateKey => write!(f, "Duplicate directory key"),
			TooDeep => write!(f, "Too deep nested structure"),
		}
	}
}

impl ParseError
{
	///Get the major class of the error.
	///
	/// # Return value:
	///
	///The error class.
	pub fn classify(&self) -> ErrorClass
	{
		use self::_ParseError::*;
		match self.1 {
			Truncated|TruncatedBreak|TruncatedText|TruncatedOctets => ErrorClass::Truncated,
			UnexpectedEnd2(_) => ErrorClass::Truncated,
			BadOpcode5F(_)|BadOpcode7F(_)|BadOpcodeFF => ErrorClass::BadFormat,
			BadSimple(_)|UnknownOpcode(_)|NotUtf8 => ErrorClass::BadFormat,
			DuplicateKey => ErrorClass::DuplicateKey,
			Garbage => ErrorClass::GarbageAfterEnd,
			TooDeep => ErrorClass::InternalLimitation,
		}
	}
	///Get offset of error.
	///
	/// # Return value:
	///
	///The offset in bytes where the error happened.
	///
	/// # Notes:
	///
	/// * The offset may point to middle of UTF-8 codepoint in case of invalid UTF-8 codepoint.
	/// * The offset may point to end of input in case the input is truncated.
	pub fn offset(&self) -> usize { self.0 }
	///Get printer for error that only prints the message part.
	///
	/// # Return value:
	///
	///Object that can be printed using the `{}` format placeholder.
	///
	/// # Notes:
	///
	/// * The difference to printing the `ParseError` directly is that the error location is not printed.
	pub fn message(&self) -> ParseErrorPrint { ParseErrorPrint(self.1) }
}

impl Display for ParseError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "CBOR error at position {pos}: {err}", pos=self.0, err=self.1)
	}
}

impl From<(usize,_ParseError)> for ParseError
{
	fn from(x: (usize, _ParseError)) -> ParseError { ParseError(x.0, x.1) }
}

enum ParseContextItem
{
	Array(Vec<CBOR>,Option<u64>),
	Dictionary(BTreeMap<CBOR,CBOR>,Option<u64>),
	DictionaryKey(BTreeMap<CBOR,CBOR>,Option<u64>,CBOR),
	Octets(Vec<u8>),
	Text(String),
	Tag(u64),
}

//Return value from push_value_iteration().
enum PushIterationRet
{
	//The entiere CBOR has been parsed.
	Break(CBOR),
	//Pushing the item will not complete any value.
	BreakNone,
	//A subvalue in CBOR has been parsed, and needs to be pushed into its parent.
	Continue(CBOR),
	//Duplicate key found.
	DuplicateKey,
}

fn push_value_iteration(stack: &mut Vec<ParseContextItem>, item: CBOR) -> PushIterationRet
{
	use ParseContextItem as PCI;
	use PushIterationRet as PIR;
	//The loop is to guarantee equivalent of tail calls, since a large number of contexts can complete at
	//once.
	//If stack is empty, it means this value is the only one in the entiere CBOR, so return it as the
	//completed CBOR. Otherwise the value is processed in topmost context, most likely replacing it with
	//another context.
	let topmost = f_return!(stack.pop(), PIR::Break(item));
	match topmost {
		PCI::Array(mut a,None) => {
			//Indeterminate array: Push the value into array, and push the array back on stack as
			//indeterminate array.
			a.push(item);
			stack.push(PCI::Array(a,None));
		},
		PCI::Array(mut a,Some(n)) => {
			//Determinate array. Push the value into array. If this is the last element (n=1), perform
			//a cascading push in order to push the just completed array element into its parent.
			//Otherwise push it back on stack, but with one less length.
			a.push(item);
			if n <= 1 {
				//Tail call.
				return PIR::Continue(CBOR::Array(a));
			} else {
				stack.push(PCI::Array(a,Some(n-1)));
			}
		},
		PCI::Dictionary(d,x) => {
			//Dictionary, waiting for key. Push dictionary key state, together with the new key to
			//insert. The item count is retained, since it is only decremented when pushing value.
			stack.push(PCI::DictionaryKey(d,x,item));
		},
		PCI::DictionaryKey(mut d,None,k) => {
			//Indeterminate dictionary, waiting for value. If the key is already in the dictionary,
			//fail, as duplicates are not allowed. Otherwise insert the new item into the dictionary.
			//Then push indeterminate dictionary, now waiting for a key.
			if d.contains_key(&k) { return PIR::DuplicateKey; }
			d.insert(k,item);
			stack.push(PCI::Dictionary(d,None));
		},
		PCI::DictionaryKey(mut d,Some(n),k) => {
			//Determinate dictionary, waiting for value. If the key is already in the dictionary,
			//fail, as duplicates are not allowed. Otherwise insert the new item into the dictionary.
			//If this is the last pair (n=1), perform a cascading push in order to push the just
			//completed array element into its parent. Otherwise push state to go back to waiting for
			//a key with one less pair to add.
			if d.contains_key(&k) { return PIR::DuplicateKey; }
			d.insert(k,item);
			if n <= 1 {
				//Tail call.
				return PIR::Continue(CBOR::Dictionary(d));
			} else {
				stack.push(PCI::Dictionary(d,Some(n-1)));
			}
		},
		//A tag. Since tags always have exactly one child, this always triggers a cascading push.
		PCI::Tag(tag) => return PIR::Continue(CBOR::Tag(tag, Box::new(item))),
		//There would be states for Octets and Text, but this function is not supposed to be called in
		//those states.
		_ => (),
	};
	//If a value is not completed, do not cascade and return no item.
	PIR::BreakNone
}


fn push_value(stack: &mut Vec<ParseContextItem>, mut item: CBOR, p: usize) -> Result<Option<CBOR>,ParseError>
{
	//This potentially needs to execute a number of times in order to perform a cascading push: One value
	//completing provodes value for its parent that also completes.
	loop {
		match push_value_iteration(stack, item) {
			PushIterationRet::Break(x) => return Ok(Some(x)),
			PushIterationRet::BreakNone => return Ok(None),
			PushIterationRet::Continue(x) => item = x,
			PushIterationRet::DuplicateKey => fail!((p, _ParseError::DuplicateKey)),
		}
	}
}

const MAX_DEPTH: usize = 2048;

fn parse_cbor(input: &[u8]) -> Result<(usize,CBOR), ParseError>
{
	use _ParseError as _PE;
	use ParseContextItem as PCI;
	let ilen = input.len();
	let mut stream = CborTokenStream::new(input);
	let mut stack: Vec<PCI> = Vec::new();
	let mut cbor: Option<CBOR> = None;
	let next_level = |stack: &mut Vec<PCI>, item: PCI| -> Option<CBOR> {
		stack.push(item);
		None
	};
	//If cbor is complete, break out in order to be able to parse CBOR sequences.
	while cbor.is_none() {
		//Position in stream, and peeked opcode (for some errors).
		let p = stream.offset();
		let popc = stream.peek_next_opcode().unwrap_or(0);
		//One needs to react differently on topmost item in parse stack being Octets, Text or something
		//else, as the first two are restricted what child items are allowed.
		let k = match stack.last() {
			Some(PCI::Octets(_)) => {
				let valid_op = popc == 255 || popc >= 0x40 && popc <= 0x5B;
				fail_if!(stream.0.len() > 0 && !valid_op, (p,_PE::BadOpcode5F(popc)));
				2
			},
			Some(PCI::Text(_)) => {
				let valid_op = popc == 255 || popc >= 0x60 && popc <= 0x7B;
				fail_if!(stream.0.len() > 0 && !valid_op, (p,_PE::BadOpcode7F(popc)));
				3
			},
			_ => 0,
		};
		//On end of stream, break from loop (this will return an error).
		let token = f_break!(stream.next());
		let token = match token {
			Ok(x) => x,
			//In case of unknown opcode, the stream still takes 1 byte.
			//Some states have special unknown opcode errors.
			Err(CborStreamError::Opcode(op)) => fail!((p,_PE::UnknownOpcode(op))),
			//Expected more logically occurs at end of input.
			Err(CborStreamError::ExpectedMore(amt)) => fail!((ilen,_PE::UnexpectedEnd2(amt))),
			Err(CborStreamError::NotUtf8(offset)) => {
				//p points to start of faulty string, and offset is offset in that string.
				let p = stream.offset();
				fail!((p+offset,_ParseError::NotUtf8));
			},
			//In case of bad simple, the stream still takes 2 bytes.
			Err(CborStreamError::BadSimple(s)) => fail!((p,_PE::BadSimple(s))),
		};
		let item = match token {
			//Stop is only allowed with indeterminates, including octets and string. Effect is
			//pretty much nested push.
			CborToken::Stop => {
				let item = match stack.pop() {
					Some(PCI::Array(a,None)) => CBOR::Array(a),
					Some(PCI::Dictionary(d,None)) => CBOR::Dictionary(d),
					Some(PCI::Octets(o)) => CBOR::Octets(o),
					Some(PCI::Text(s)) => CBOR::String(s),
					_ => fail!((p, _PE::BadOpcodeFF))
				};
				Some(item)
			},
			//Octets is allowed if topmost item in state stack is not a string. In case the topmost
			//Item is octets, there is indeterminate octets in progress, and the segment needs to be
			//appended. Otherwise this is Octets value that needs to be pushed.
			CborToken::Octets(t) if k==0||k==2 =>
				if let Some(&mut PCI::Octets(ref mut out)) = stack.last_mut() {
				out.extend_from_slice(t);
				None
			} else {
				Some(CBOR::Octets(t.to_owned()))
			},
			//String is allowed if topmost item in state stack is not an octets. In case the topmost
			//Item is string, there is indeterminate string in progress, and the segment needs to be
			//appended. Otherwise this is String value that needs to be pushed.
			CborToken::Text(t) if k==0||k==3 =>
				if let Some(&mut PCI::Text(ref mut out)) = stack.last_mut() {
				out.push_str(t);
				None
			} else {
				Some(CBOR::String(t.to_owned()))
			},
			//If there is text or octets on top of the stack, only Stop, and determinate octets/string
			//are allowed.
			_ if k == 2 => fail!((p, _PE::BadOpcode5F(popc))),
			_ if k == 3 => fail!((p, _PE::BadOpcode7F(popc))),
			CborToken::Integer(v) => Some(CBOR::Integer(v)),
			CborToken::NegInteger(v) => Some(CBOR::NegInteger(v)),
			//Empty array needs to be pushed as value, since it does not have child nodes.
			CborToken::Array(Some(0)) => Some(CBOR::Array(Vec::new())),
			//Empty dictionary needs to be pushed as value, since it does not have child nodes.
			CborToken::Dictionary(Some(0)) => Some(CBOR::Dictionary(BTreeMap::new())),
			CborToken::HalfFloat(x) => Some(CBOR::HalfFloat(x)),
			CborToken::Float(x) => Some(CBOR::Float(x)),
			CborToken::Double(x) => Some(CBOR::Double(x)),
			CborToken::Simple(t) => Some(CBOR::Simple(t)),
			//These start a new nesting level.
			CborToken::OctetsInd => next_level(&mut stack, PCI::Octets(Vec::new())),
			CborToken::TextInd => next_level(&mut stack, PCI::Text(String::new())),
			CborToken::Array(x) => next_level(&mut stack, PCI::Array(Vec::new(),x)),
			CborToken::Dictionary(x) => next_level(&mut stack, PCI::Dictionary(BTreeMap::new(),x)),
			CborToken::Tag(n) => next_level(&mut stack, PCI::Tag(n)),
			//This is not supposed to be possible. The only possibilities are k=3 String, which triggers
			//an error above, k=2 Octets, which also triggers an error, or something that is not k 0, 2
			//nor 3, which just is not possible, since k is always one of those three.
			_ => fail!((p, _PE::UnknownOpcode(popc)))
		};
		if let Some(item) = item { cbor = push_value(&mut stack, item, p)?; }
		//This limit is there because dropping resulting CBOR will overflow stack if the CBOR has too
		//deep nesting.
		fail_if!(stack.len() > MAX_DEPTH, (p, _PE::TooDeep));
	}
	//The length of stuff read, or location of error at end of input.
	let p = stream.offset();
	let cbor = cbor.ok_or_else(||{
		let err = match stack.last() {
			Some(PCI::Array(_,None)) => _PE::TruncatedBreak,
			Some(PCI::Dictionary(_,None)) => _PE::TruncatedBreak,
			Some(PCI::Octets(_)) => _PE::TruncatedOctets,
			Some(PCI::Text(_)) => _PE::TruncatedText,
			//The remaining cases want arbitrary values.
			_ => _PE::Truncated
		};
		(p, err)
	})?;
	Ok((p, cbor))
}

struct HalfParts
{
	sign: bool,
	exp: i32,
	mantissa: u16,
}

impl HalfParts
{
	fn decompose(x: u16) -> HalfParts
	{
		HalfParts {
			sign: x >> 15 != 0,
			exp: (x >> 10) as i32 % 32 - 15,
			mantissa: x & 0x3FF,
		}
	}
	fn compose(&self) -> u16 { (self.sign as u16) << 15 | ((self.exp + 15) as u16) << 10 | self.mantissa }
	fn new(sign: bool, mantissa: u16, exp: i32) -> HalfParts { HalfParts { sign, mantissa, exp } }
}

#[derive(Debug)]
struct SingleParts
{
	sign: bool,
	exp: i32,
	mantissa: u32,
}

impl SingleParts
{
	fn decompose(x: u32) -> SingleParts
	{
		SingleParts {
			sign: x >> 31 != 0,
			exp: (x >> 23) as i32 % 256 - 127,
			mantissa: x & 0x7FFFFF,
		}
	}
	fn compose(&self) -> u32 { (self.sign as u32) << 31 | ((self.exp + 127) as u32) << 23 | self.mantissa }
	fn new(sign: bool, mantissa: u32, exp: i32) -> SingleParts { SingleParts { sign, mantissa, exp } }
}

#[derive(Debug)]
struct DoubleParts
{
	sign: bool,
	exp: i32,
	mantissa: u64,
}

impl DoubleParts
{
	fn decompose(x: u64) -> DoubleParts
	{
		DoubleParts {
			sign: x >> 63 != 0,
			exp: (x >> 52) as i32 % 2048 - 1023,
			mantissa: x & 0xFFFFFFFFFFFFF,
		}
	}
	fn compose(&self) -> u64 { (self.sign as u64) << 63 | ((self.exp + 1023) as u64) << 52 | self.mantissa }
	fn new(sign: bool, mantissa: u64, exp: i32) -> DoubleParts { DoubleParts { sign, mantissa, exp } }
}

fn promote_from_parts(sign: bool, exponent: i32, mantissa: u64, subnormal: i32, infinity: i32) -> u64
{
	let r = if exponent <= subnormal && mantissa == 0 {
		//Zero. This needs to have exponent -1023.
		DoubleParts::new(sign, 0, -1023)
	} else if exponent <= subnormal {
		//Non-zero subnormals. These need to be adjusted.
		let lz = mantissa.leading_zeros().saturating_sub(11) as i32;
		DoubleParts::new(sign, (mantissa << lz) & 0xFFFFFFFFFFFFF, subnormal - lz + 1)
	} else if exponent >= infinity {
		//Infinite and NaN. These need to have exponent = 1024.
		DoubleParts::new(sign, mantissa, 1024)
	} else {
		//Normal number.
		DoubleParts::new(sign, mantissa, exponent)
	};
	r.compose()
}

fn promote_single(x: u32) -> u64
{
	let p = SingleParts::decompose(x);
	promote_from_parts(p.sign, p.exp, (p.mantissa as u64) << 29, -127, 128)
}

fn promote_half(x: u16) -> u64
{
	let p = HalfParts::decompose(x);
	promote_from_parts(p.sign, p.exp, (p.mantissa as u64) << 42, -15, 16)
}

fn try_demote_double_to_single(x: u64) -> Option<u32>
{
	let p = DoubleParts::decompose(x);
	let r = if p.exp <= -1023 && p.mantissa == 0 {
		//Zero.
		SingleParts::new(p.sign, 0, -127)
	} else if p.exp <= -150 {
		return None;						//Unrepresentable.
	} else if p.exp <= -127 {
		//Subnormals.
		let emantissa = p.mantissa | 1 << 52;
		let shift = (-p.exp) as u32 - 97;
		fail_if_none!(emantissa & ((1 << shift) - 1) != 0);	//Would be inexact.
		SingleParts::new(p.sign, (emantissa >> shift) as u32, -127)
	} else if p.exp <= 127 {
		//Normals
		fail_if_none!(p.mantissa & ((1 << 29) - 1) != 0);	//Would be inexact.
		SingleParts::new(p.sign, (p.mantissa >> 29) as u32, p.exp)
	} else if p.exp <= 1023 {
		return None;						//Unrepresentable.
	} else if p.mantissa == 0 {
		//Infinity.
		SingleParts::new(p.sign, 0, 128)
	} else {
		//NaN. Use canonical mantissa.
		SingleParts::new(false, 0x400000, 128)
	};
	Some(r.compose())
}

fn try_demote_double_to_half(x: u64) -> Option<u16>
{
	let p = DoubleParts::decompose(x);
	let r = if p.exp <= -1023 && p.mantissa == 0 {
		//Zero.
		HalfParts::new(p.sign, 0, -15)
	} else if p.exp <= -25 {
		return None;						//Unrepresentable.
	} else if p.exp <= -15 {
		//Subnormals.
		let emantissa = p.mantissa | 1 << 52;
		let shift = (-p.exp) as u32 + 28;
		fail_if_none!(emantissa & ((1 << shift) - 1) != 0);	//Would be inexact.
		HalfParts::new(p.sign, (emantissa >> shift) as u16, -15)
	} else if p.exp <= 15 {
		//Normals
		fail_if_none!(p.mantissa & ((1 << 42) - 1) != 0);	//Would be inexact.
		HalfParts::new(p.sign, (p.mantissa >> 42) as u16, p.exp)
	} else if p.exp <= 1023 {
		return None;						//Unrepresentable.
	} else if p.mantissa == 0 {
		//Infinity
		HalfParts::new(p.sign, 0, 16)
	} else {
		//NaN. Use canonical mantissa.
		HalfParts::new(false, 0x200, 16)
	};
	Some(r.compose())
}

fn try_demote_single_to_half(x: u32) -> Option<u16>
{
	//First promote single to double, then demote that to half. This avoids having to implement demotion
	//to halfs twice: once from floats and once from doubles.
	try_demote_double_to_half(promote_single(x))
}

fn decode_negint(z: i64) -> i64 { -z-1 }

struct OptionEmptyIterator<T,U:Iterator<Item=T>>(Option<U>);

impl<T,U:Iterator<Item=T>> Iterator for OptionEmptyIterator<T,U>
{
	type Item = T;
	fn next(&mut self) -> Option<T>
	{
		self.0.as_mut().and_then(|i|{
			i.next()
		})
	}
}

#[doc(hidden)]
pub enum Void {}


macro_rules! b
{
	($b:tt $sym:expr) => { ($sym >> 8*$b) as u8 }
}

macro_rules! wcborn
{
	(0 $tag:expr, $n:expr) => { [32 * $tag + $n as u8] };
	(1 $tag:expr, $n:expr) => { [32 * $tag + 24, b!(0 $n)] };
	(2 $tag:expr, $n:expr) => { [32 * $tag + 25, b!(1 $n), b!(0 $n)] };
	(4 $tag:expr, $n:expr) => { [32 * $tag + 26, b!(3 $n), b!(2 $n), b!(1 $n), b!(0 $n)] };
	(8 $tag:expr, $n:expr) => {
		[32 * $tag + 27, b!(7 $n), b!(6 $n), b!(5 $n), b!(4 $n), b!(3 $n), b!(2 $n), b!(1 $n), b!(0 $n)]
	};
}

///Trait for byte string sink.
///
///This can be used as a target for serializing a CBOR structure into byte string.
pub trait BytesWriterTarget
{
	///Type of error from write.
	///
	///If the sink can never fail, the error type should be uninhabited, signaling that errors can never happen.
	type Error: Sized;
	///Append given bytes into sink.
	///
	/// # Parameters:
	///
	/// * `frag`: The bytes to append.
	///
	/// # Error conditions:
	///
	/// * The bytes can not be appended for some reason.
	///
	/// # Notes:
	///
	/// * Any error returned is bubbled up from [`CBOR::serialize()`] call.
	///
	///[`CBOR::serialize()`]: enum.CBOR.html#method.serialize
	fn write(&mut self, frag: &[u8]) -> Result<(), <Self as BytesWriterTarget>::Error>;
	#[doc(hidden)]
	fn write_number(&mut self, major: u8, num: u64) -> Result<(), <Self as BytesWriterTarget>::Error>
	{
		if num < 24 {
			self.write(&wcborn!(0 major, num))
		} else if num < 256 {
			self.write(&wcborn!(1 major, num))
		} else if num < 65536 {
			self.write(&wcborn!(2 major, num))
		} else if num < 4294967296 {
			self.write(&wcborn!(4 major, num))
		} else {
			self.write(&wcborn!(8 major, num))
		}
	}
}

impl BytesWriterTarget for Vec<u8>
{
	type Error = Void;
	fn write(&mut self, frag: &[u8]) -> Result<(), Void> { Ok(self.extend_from_slice(frag)) }
}

impl<'a> BytesWriterTarget for btls_aux_memory::BackedVector<'a,u8>
{
	type Error = ();
	fn write(&mut self, frag: &[u8]) -> Result<(), ()>
	{
		dtry!(self.extend(frag));
		Ok(())
	}
}

impl<const N:usize> BytesWriterTarget for btls_aux_memory::SmallVec<u8,N>
{
	type Error = ();
	fn write(&mut self, frag: &[u8]) -> Result<(), ()>
	{
		dtry!(self.extend(frag));
		Ok(())
	}
}

///A CBOR value node.
///
///This is a single CBOR value, that is, one of:
///
/// * Integer
/// * Negative integer
/// * Octet string (not in JSON)
/// * Text string
/// * Array
/// * Dictionary
/// * Tag (not in JSON)
/// * `Null`
/// * `Undefined` (not in JSON)
/// * Boolean
/// * Simple item (not in JSON)
/// * Half-precision floating point (not in JSON)
/// * Single-precision floating point (not in JSON)
/// * Double-precision floating point
///
///Through CBOR is mostly compatible with JSON, the CBOR has data types not present in JSON.
///
///Note that the `Ordering` implemented by this enumeration is undefined and does not equal the canonical CBOR
///ordering. However, the ordering is total and self-consistent, so `BTreeMap` is happy to have CBOR values as
///keys.
///
///Also unlike the case with JSON, CBOR can be directly serialized. This is because CBOR uses binary
///representation for floating-point numbers, separate from integers.
#[derive(Clone,Debug)]
#[non_exhaustive]
pub enum CBOR
{
	///Nonnegative integer.
	///
	/// # Arguments:
	///
	/// 1. `value`: The integer value.
	Integer(u64),
	///Negative integer.
	///
	/// # Arguments:
	///
	/// 1. `value`: The 2s complement of the value, 
	///
	/// # Notes:
	///
	/// * There is bias of `1`. That is, `0` represents value of `-1`, and 2^64-1 represents vale of `-2^64`.
	NegInteger(u64),
	///Octet string.
	///
	/// # Arguments:
	///
	/// 1. `value`: The octet string value.
	///
	/// # Notes:
	///
	/// * This type is 8-bit clean, it can represent arbitrary octet string. JSON has no native octet
	///string type, in JSON, octet strings are commonly represented as base64url encoding of the octet string,
	///encoded as a text string.
	Octets(Vec<u8>),
	///`UTF-8` string.
	///
	/// # Arguments:
	///
	/// 1. `value`: The UTF-8 string value.
	///
	/// # Notes:
	///
	/// * CBOR requires text strings to be valid UTF-8, so arbitrary valid text string can be
	///represented.
	String(String),
	///Array
	///
	/// # Arguments:
	///
	/// 1. `value`: The array of CBOR values in array, in order, without gaps.
	Array(Vec<CBOR>),
	///Dictionary (map).
	///
	/// # Arguments:
	///
	/// 1. `value`: The dictionary of CBOR values in in dictionary, with no extra or missing entries.
	///
	/// # Notes:
	///
	/// * Arbitrary CBOR values are supported as keys, including arrays or other dictionaries. Note that JSON does
	///not support keys that are not text strings.
	Dictionary(BTreeMap<CBOR, CBOR>),
	///A tagged value.
	///
	/// # Arguments:
	///
	/// 1. `tagnumber`: The number of CBOR tag.
	/// 1. `tagged`: The tagged value.
	///
	/// # Notes:
	/// 
	/// * JSON does not have equivalent data type.
	Tag(u64, Box<CBOR>),
	///Simple value
	///
	/// # Arguments:
	///
	/// 1. `value`: The simple value. Always between `0` and `23` or `32` and `255`, inclusive.
	///
	/// # Notes:
	///
	/// * Values 24 to 31 are **not** allowed.
	/// * This incluses `false`, `true`, `null` and `undefined`  (this is not in JSON). These are represeted as
	///simple values, numbers 20, 21, 22 and 23 (constatnts [`CBOR::FALSE`], [`CBOR::TRUE`], [`CBOR::NULL`] and
	///[`CBOR::UNDEFINED`]), respectively.
	///
	///[`CBOR::FALSE`]: #associatedconstant.FALSE
	///[`CBOR::TRUE`]: #associatedconstant.TRUE
	///[`CBOR::NULL`]: #associatedconstant.NULL
	///[`CBOR::UNDEFINED`]: #associatedconstant.UNDEFINED
	Simple(u8),
	///Half-precision (16 bit) floating-point value.
	///
	/// # Arguments:
	///
	/// 1. `value`: The raw IEEE standard encoding of the floating-point number.
	///
	/// # Notes:
	///
	/// * In JSON, this corresponds to subset of the number type.
	HalfFloat(u16),
	///Single-precision (32 bit) floating-point value.
	///
	/// # Arguments:
	///
	/// 1. `value`: The raw IEEE standard encoding of the floating-point number.
	///
	/// # Notes:
	///
	/// * In JSON, this corresponds to subset of the number type.
	Float(u32),
	///Double-precision (64 bit) floating-point value.
	///
	/// # Arguments:
	///
	/// 1. `value`: The raw IEEE standard encoding of the floating-point number.
	///
	/// # Notes:
	///
	/// * In JSON, this corresponds to subset of the number type.
	Double(u64),
}

//FIXME: Dropping a deeply nested CBOR object can trigger a stack overflow!

impl CBOR
{
	///The CBOR `FALSE` simple value.
	pub const FALSE: CBOR = CBOR::Simple(20);
	///The CBOR `TRUE` simple value.
	pub const TRUE: CBOR = CBOR::Simple(21);
	///The CBOR `NULL` simple value.
	pub const NULL: CBOR = CBOR::Simple(22);
	///The CBOR `UNDEFINED` simple value.
	///
	///There is no corresponding value in JSON.
	pub const UNDEFINED: CBOR = CBOR::Simple(23);
	///Is NULL?
	///
	/// # Return value:
	///
	///`true` if value is NULL, `false` otherwise.
	pub fn is_null(&self) -> bool { matches!(self, &CBOR::Simple(22)) }
	///Is Undefined?
	///
	/// # Return value:
	///
	///`true` if value is undefined, `false` otherwise.
	pub fn is_undefined(&self) -> bool { matches!(self, &CBOR::Simple(23)) }
	///Is Boolean?
	///
	/// # Return value:
	///
	///`true` if value is boolean, `false` otherwise.
	pub fn is_boolean(&self) -> bool { matches!(self, &CBOR::Simple(20)|&CBOR::Simple(21)) }
	///Is Integer?
	///
	/// # Return value:
	///
	///`true` if value is integer, `false` otherwise.
	pub fn is_integer(&self) -> bool { matches!(self, &CBOR::Integer(_)|&CBOR::NegInteger(_)) }
	///Is Octets?
	///
	/// # Return value:
	///
	///`true` if value is byte string, `false` otherwise.
	pub fn is_octets(&self) -> bool { matches!(self, &CBOR::Octets(_)) }
	///Is String?
	///
	/// # Return value:
	///
	///`true` if value is (text) string, `false` otherwise.
	pub fn is_string(&self) -> bool { matches!(self, &CBOR::String(_)) }
	///Is Array?
	///
	/// # Return value:
	///
	///`true` if value is array, `false` otherwise.
	pub fn is_array(&self) -> bool { matches!(self, &CBOR::Array(_)) }
	///Is Dictionary?
	///
	/// # Return value:
	///
	///`true` if value is dictionary, `false` otherwise.
	pub fn is_dictionary(&self) -> bool { matches!(self, &CBOR::Dictionary(_)) }
	///Is Tag?
	///
	/// # Return value:
	///
	///`true` if value is tag, `false` otherwise.
	pub fn is_tag(&self) -> bool { matches!(self, &CBOR::Tag(_, _)) }
	///Is Simple?
	///
	/// # Return value:
	///
	///`true` if value is simple, `false` otherwise.
	pub fn is_simple(&self) -> bool { matches!(self, &CBOR::Simple(_)) }
	///Is float?
	///
	/// # Return value:
	///
	///`true` if value is float (of any size), `false` otherwise.
	pub fn is_float(&self) -> bool
	{
		matches!(self, &CBOR::HalfFloat(_)|&CBOR::Float(_)|&CBOR::Double(_))
	}
	///Construct CBOR node with integer value.
	///
	/// # Parameters:
	///
	/// * `x`: The integer value.
	///
	/// # Return value:
	///
	///CBOR value with given integer value.
	///
	/// # Notes:
	///
	/// * This can not construct the full integer range, only about half of it.
	pub fn from_integer(x: i64) -> CBOR
	{
		if x < 0 { CBOR::NegInteger((-(x + 1)) as u64) } else { CBOR::Integer(x as u64) }
	}
	///Cast CBOR node into non-negative integer.
	///
	/// # Error conditions:
	///
	/// * The value is not nonnegative integer.
	/// * The value is floating-point number, even if integer.
	///
	/// # Return value:
	///
	///The integer value.
	pub fn as_unsigned<'a>(&'a self) -> Option<u64>
	{
		if let &CBOR::Integer(x) = self { Some(x) } else { None }
	}
	///Cast CBOR node into integer.
	///
	/// # Error conditions:
	///
	/// * The value is not integer in range of `i64`.
	/// * The value is floating-point number, even if integer.
	///
	/// # Return value:
	///
	///The integer value.
	pub fn as_integer<'a>(&'a self) -> Option<i64>
	{
		match self {
			&CBOR::Integer(x) => i64::try_from(x).ok(),
			&CBOR::NegInteger(x) => i64::try_from(x).ok().map(decode_negint),
			_ => None
		}
	}
	///Cast CBOR node into negative integer.
	///
	/// # Error conditions:
	///
	/// * The value is not negative integer.
	/// * The value is floating-point number, even if integer.
	///
	/// # Return value:
	///
	///Negation of the integer value, minus 1.
	///
	/// # Notes:
	///
	/// * The returned value is offset by 1: return value of `0` means `-1`, `1` means `-2` and so on up to
	///`2^64-1` meaning `-2^64`.
	pub fn as_unsigned_negative<'a>(&'a self) -> Option<u64>
	{
		if let &CBOR::NegInteger(x) = self { Some(x) } else { None }
	}
	///Cast CBOR node into octet string.
	///
	/// # Error conditions:
	///
	/// * The value is not a byte string.
	///
	/// # Return value:
	///
	///The byte string value.
	pub fn as_octets<'a>(&'a self) -> Option<&'a [u8]>
	{
		if let &CBOR::Octets(ref x) = self { Some(&x[..]) } else { None }
	}
	///Cast CBOR node into text string.
	///
	/// # Error conditions:
	///
	/// * The value is not a (text) string.
	///
	/// # Return value:
	///
	///The string value.
	pub fn as_string<'a>(&'a self) -> Option<&'a str>
	{
		if let &CBOR::String(ref x) = self { Some(x.deref()) } else { None }
	}
	///Cast CBOR node into array.
	///
	/// # Error conditions:
	///
	/// * The value is not an array.
	///
	/// # Return value:
	///
	///The array value.
	pub fn as_array<'a>(&'a self) -> Option<&'a [CBOR]>
	{
		if let &CBOR::Array(ref x) = self { Some(&x[..]) } else { None }
	}
	///Cast CBOR node into dictionary.
	///
	/// # Error conditions:
	///
	/// * The value is not a dictionary.
	///
	/// # Return value:
	///
	///The dictionary value.
	pub fn as_dictionary<'a>(&'a self) -> Option<&'a BTreeMap<CBOR, CBOR>>
	{
		if let &CBOR::Dictionary(ref x) = self { Some(x) } else { None }
	}
	///Cast CBOR node into tag.
	///
	/// # Error conditions:
	///
	/// * The value is not a tag.
	///
	/// # Return value:
	///
	///A tuple:
	///
	/// 1. `tagval`: Tag value.
	/// 1. `obj`: The tagged object.
	pub fn as_tag<'a>(&'a self) -> Option<(u64, &'a CBOR)>
	{
		if let &CBOR::Tag(t, ref x) = self { Some((t, x)) } else { None }
	}
	///Cast CBOR node into simple.
	///
	/// # Error conditions:
	///
	/// * The value is not a simple.
	///
	/// # Return value:
	///
	///The simple value.
	pub fn as_simple<'a>(&'a self) -> Option<u8>
	{
		if let &CBOR::Simple(x) = self { Some(x) } else { None }
	}
	///Cast CBOR node into null.
	///
	/// # Error conditions:
	///
	/// * The value is not a null.
	pub fn as_null<'a>(&'a self) -> Option<()>
	{
		if let &CBOR::Simple(22) = self { Some(()) } else { None }
	}
	///Cast CBOR node into undefined.
	///
	/// # Error conditions:
	///
	/// * The value is not an undefined.
	pub fn as_undefined<'a>(&'a self) -> Option<()>
	{
		if let &CBOR::Simple(23) = self { Some(()) } else { None }
	}
	///Cast CBOR node into boolean.
	///
	/// # Error conditions:
	///
	/// * The value is not a boolean.
	///
	/// # Return value:
	///
	///The boolean value.
	pub fn as_boolean<'a>(&'a self) -> Option<bool>
	{
		match self {
			&CBOR::Simple(20) => Some(false),
			&CBOR::Simple(21) => Some(true),
			_ => None
		}
	}
	///Cast CBOR node into half-float.
	///
	/// # Error conditions:
	///
	/// * The value is not a half-float.
	///
	/// # Return value:
	///
	///The half-float value, bitcasted into integer.
	///
	/// # Notes:
	///
	/// * single-float and double-float values are not demoted, even if such demotion would be exact.
	pub fn as_half_float<'a>(&'a self) -> Option<u16>
	{
		if let &CBOR::HalfFloat(x) = self { Some(x) } else { None }
	}
	///Cast CBOR node into single-float.
	///
	/// # Error conditions:
	///
	/// * The value is not a single-float.
	///
	/// # Return value:
	///
	///The single-float value, bitcasted into integer.
	///
	/// # Notes:
	///
	/// * half-float and double-float values are not promoted/demoted, even if such demotion would be exact.
	pub fn as_single_float<'a>(&'a self) -> Option<u32>
	{
		if let &CBOR::Float(x) = self { Some(x) } else { None }
	}
	///Cast CBOR node into double-float.
	///
	/// # Error conditions:
	///
	/// * The value is not a dobule-float.
	///
	/// # Return value:
	///
	///The dobule-float value, bitcasted into integer.
	///
	/// # Notes:
	///
	/// * half-float and single-float values are not promoted.
	pub fn as_double_float<'a>(&'a self) -> Option<u64>
	{
		if let &CBOR::Double(x) = self { Some(x) } else { None }
	}
	///Cast CBOR node into double-float, promoting half-floats and single-floats.
	///
	/// # Error conditions:
	///
	/// * The value is not a float.
	///
	/// # Return value:
	///
	///The (possibly-promoted) dobule-float value, bitcasted into integer.
	///
	/// # Note:
	///
	/// * Integers are not converted, even if conversion would be exact.
	pub fn as_double_float_p<'a>(&'a self) -> Option<u64>
	{
		match self {
			&CBOR::HalfFloat(x) => Some(promote_half(x)),
			&CBOR::Float(x) => Some(promote_single(x)),
			&CBOR::Double(x) => Some(x),
			_ => None
		}
	}
	///Cast CBOR node into double-float, promoting half-floats and single-floats, followed by bitcast to `f64`.
	///
	/// # Error conditions:
	///
	/// * The value is not a float.
	///
	/// # Return value:
	///
	///The (possibly-promoted) dobule-float value.
	///
	/// # Note:
	///
	/// * Integers are not converted, even if conversion would be exact.
	/// * The returned value may be zero, infinity (both plus or minus) or `NaN`.
	/// * This is the same as [`as_double_float_p()`], but with value returned as `f64`.
	///
	///[`as_double_float_p()`]: #method.as_double_float_p
	pub fn as_double_float_v<'a>(&'a self) -> Option<f64>
	{
		self.as_double_float_p().map(|x|{
			f64::from_bits(x)
		})
	}
	///Get value at index of array.
	///
	/// # Parameters:
	///
	/// * `n`: The zero-based index of element.
	///
	/// # Error conditions:
	///
	/// * The value is not an array.
	/// * The specified index is out of range.
	///
	/// # Return value:
	///
	///The value at given index.
	pub fn nth<'a>(&'a self, n: usize) -> Option<&'a CBOR>
	{
		self.as_array().and_then(|a|{
			a.get(n)
		})
	}
	///Get length of array.
	///
	/// # Return value:
	///
	///The number of elements in array, or 0 if value is not an array.
	pub fn array_len(&self) -> usize
	{
		self.as_array().map_or(0, |a|{
			a.len()
		})
	}
	///Iterate over array.
	///
	/// # Return value:
	///
	/// * If the value is array, the iterator over array elements.
	/// * Othewise empty iterator.
	///
	/// # Notes:
	///
	/// * The iterator always yields [`array_len()`](#method.array_len) items.
	pub fn iter_array<'a>(&'a self) -> impl Iterator<Item=&'a CBOR>
	{
		let acontent = self.as_array().map(|a|{
			a.iter()
		});
		OptionEmptyIterator(acontent)
	}
	///Get dictionary field with specified name.
	///
	/// # Parameters:
	///
	/// * `name`: The dictionary key to get value for.
	///
	/// # Error conditions:
	///
	/// * The value is not a dictionary.
	/// * The specified key is not in dictionary.
	///
	/// # Return value:
	///
	///The value at given key of the dictionary.
	pub fn field<'a,T:ToCbor>(&'a self, name: T) -> Option<&'a CBOR>
	{
		let name = name.to_cbor();
		self.as_dictionary().and_then(|a|{
			a.get(&name)
		})
	}
	///Return an iterator over all field names.
	///
	/// # Return value:
	///
	///The iterator over dictionary keys, or empty iterator if value is not a dictionary.
	pub fn field_names<'a>(&'a self) -> impl Iterator<Item=&'a CBOR>
	{
		let dkeys = self.as_dictionary().map(|d|{
			d.keys()
		});
		OptionEmptyIterator(dkeys)
	}
	///Iterate over dictionary.
	///
	/// # Return value:
	///
	/// * If value is dictionary, the iterator over dictionary key-value pairs (first keys, then values).
	/// * Othewise empty iterator.
	pub fn iter_object<'a>(&'a self) -> impl Iterator<Item=(&'a CBOR, &'a CBOR)>
	{
		let ovals = self.as_dictionary().map(|a|{
			a.iter()
		});
		OptionEmptyIterator(ovals)
	}
	///Parse octet string as CBOR structure.
	///
	/// # Parameters:
	///
	/// * `data`: The data to parse.
	///
	/// # Error conditions:
	///
	/// * The data can not be parsed as CBOR. This includes the case where there is trailing junk
	///after a single CBOR structure.
	///
	/// # Return value:
	///
	///The parsed CBOR value.
	pub fn parse(data: &[u8]) -> Result<CBOR, ParseError>
	{
		let (len,cbor) = parse_cbor(data)?;
		fail_if!(len < data.len(), (len, _ParseError::Garbage));
		Ok(cbor)
	}
	///Parse first CBOR structure from octet string.
	///
	/// # Parameters:
	///
	/// * `data`: The data to parse.
	///
	/// # Error conditions:
	///
	/// * The data can not be parsed as CBOR. However, any trailing junk is ignored.
	///
	/// # Return value:
	///
	///A tuple:
	///
	/// 1. `consumed`: Number of bytes consumed.
	/// 1. `parsed`: The parsed CBOR value.
	///
	/// # Notes:
	///
	/// * This is meant for parsing CBOR sequences: Parse a sequence, remove the parsed part from string and then
	///parse next one, until running out of sequence.
	/// * This is similar to [`parse()`], except it ignores trailing junk and also returns length of parsed part.
	///
	///[`parse()`]: #method.parse
	pub fn parse_multi(data: &[u8]) -> Result<(usize, CBOR), ParseError> { parse_cbor(data) }
	///Serialize the CBOR value.
	///
	/// # Return value:
	///
	///The serialized CBOR bytes.
	pub fn serialize_to_vec(&self) -> Vec<u8>
	{
		let mut out = Vec::new();
		//The error type is !.
		if let Err(e) = self.serialize(&mut out) { match e {} };
		out
	}
	///Serialize the CBOR value.
	///
	/// # Parameters
	///
	/// * `to`: The sink to write the value to.
	///
	/// # Error conditions:
	///
	/// * Some append operation on sink fails.
	///
	/// # Notes:
	///
	/// * This is similar to [`serialize_to_vec()`](#method.serialize_to_vec), but serializes to a sink instead
	///of serializing to a byte vector.
	/// * This can be used to serialize CBOR sequences to byte vectors: Repeatedly call this method on `Vec<u8>`,
	///starting with empty vector.
	pub fn serialize<B:BytesWriterTarget>(&self, to: &mut B) -> Result<(), <B as BytesWriterTarget>::Error>
	{
		match self {
			&CBOR::Integer(x) => to.write_number(0, x),
			&CBOR::NegInteger(x) => to.write_number(1, x),
			&CBOR::Octets(ref x) => {
				to.write_number(2, x.len() as u64)?;
				to.write(x.deref())
			},
			&CBOR::String(ref x) => {
				to.write_number(3, x.len() as u64)?;
				to.write(x.as_bytes())
			},
			&CBOR::Array(ref x) => {
				to.write_number(4, x.len() as u64)?;
				for element in x.iter() { element.serialize(to)?; }
				Ok(())
			},
			&CBOR::Dictionary(ref x) => {
				to.write_number(5, x.len() as u64)?;
				for (key, value) in x.iter() {
					key.serialize(to)?;
					value.serialize(to)?;
				}
				Ok(())
			},
			&CBOR::Tag(tag, ref x) => {
				to.write_number(6, tag)?;
				x.serialize(to)
			},
			&CBOR::Simple(tag) => to.write_number(7, tag as u64),
			//Half floats above 0x7C00 are NaN, which is canonically 0x7E00.
			&CBOR::HalfFloat(x) => to.write(&wcborn!(2 7, canonicalize_hf(x))),
			//Float needs no special NaN handling, since NaNs are guaranteed to demote to halfs.
			&CBOR::Float(x) => if let Some(x) = try_demote_single_to_half(x) {
				to.write(&wcborn!(2 7, x))
			} else {
				to.write(&wcborn!(4 7, x))
			},
			//Double needs no special NaN handling, since NaNs are guaranteed to demote to halfs.
			&CBOR::Double(x) => if let Some(x) = try_demote_double_to_half(x) {
				to.write(&wcborn!(2 7, x))
			} else if let Some(x) = try_demote_double_to_single(x) {
				to.write(&wcborn!(4 7, x))
			} else {
				to.write(&wcborn!(8 7, x))
			},
		}
	}
	///Create a node from given value.
	///
	/// # Parameter:
	///
	/// * `v`: The value to use.
	///
	/// # Return value:
	///
	///The newly created CBOR value.
	///
	/// # Notes: 
	///
	/// * The source type to node type mappings are:
	///   * `bool` -> `Boolean`
	///   * `i64` -> `Integer`
	///   * `u64` -> `Integer`
	///   * `String` -> `String`
	///   * `&String` -> `String`
	///   * `&str` -> `String`
	///   * `Vec<u8>` -> `Octets`
	///   * `&Vec<u8>` -> `Octets`
	///   * `&[u8]` -> `Octets`
	///   * `&[CBOR]` -> `Array`
	///   * `Vec<CBOR>` -> `Array`
	///   * `&[(CBOR, CBOR)]` -> `Dictionary`
	///   * `(u64, CBOR)` -> `Tag`
	///   * `f32` -> `SingleFloat`
	///   * `f64` -> `DoubleFloat`
	pub fn from(v: impl ToCbor) -> CBOR { v.to_cbor() }
	fn priority(&self) -> u8
	{
		//Each discriminant must give a different value.
		match self {
			&CBOR::Integer(_) => 0x00,
			&CBOR::NegInteger(_) => 0x20,
			&CBOR::Octets(_) => 0x40,
			&CBOR::String(_) => 0x60,
			&CBOR::Array(_) => 0x80,
			&CBOR::Dictionary(_) => 0xA0,
			&CBOR::Tag(_, _) => 0xC0,
			&CBOR::Simple(_) => 0xE0,
			&CBOR::HalfFloat(_) => 0xF9,
			&CBOR::Float(_) => 0xFA,
			&CBOR::Double(_) => 0xFB,
		}
	}
}

macro_rules! eq_chain
{
	($val:expr) => {{ let tmp = $val; if tmp.is_ne() { return tmp; } }}
}

fn canonicalize_hf(x: u16) -> u16
{
	//If x is NaN, use canonical NaN of 0x7E00, otherwise return unmodified.
	if x & 0x7FFF > 0x7C00 { 0x7E00 } else { x }
}

fn order_floats_hh(x: u16, y: u16) -> Ordering
{
	//Canonicalize x and y for the case where both are NaN.
	canonicalize_hf(x).cmp(&canonicalize_hf(y))
}

fn order_floats_hs(x: u16, y: u32) -> Ordering
{
	//In case x is NaN, it needs to be canonicalized. If y is NaN, it will always demote. In case y fails
	//to demote, x < y.
	if let Some(y) = try_demote_single_to_half(y) { canonicalize_hf(x).cmp(&y) } else { Ordering::Less }
}

fn order_floats_hd(x: u16, y: u64) -> Ordering
{
	//In case x is NaN, it needs to be canonicalized. If y is NaN, it will always demote. In case y fails
	//to demote, x < y.
	if let Some(y) = try_demote_double_to_half(y) { canonicalize_hf(x).cmp(&y) } else { Ordering::Less }
}

fn order_floats_ss(x: u32, y: u32) -> Ordering
{
	//Try demoting both. NaNs always demote. If one fails to demote, it is the greater one.
	match (try_demote_single_to_half(x), try_demote_single_to_half(y)) {
		(Some(x), Some(y)) => x.cmp(&y),
		(Some(_), None) => Ordering::Greater,
		(None, Some(_)) => Ordering::Less,
		(None, None) => x.cmp(&y),
	}
}

fn order_floats_sd(x: u32, y: u64) -> Ordering
{
	//Try demoting y into single. If this works, do ss ordering. If it fails, y has to be greater.
	if let Some(y) = try_demote_double_to_single(y) { order_floats_ss(x, y) } else { Ordering::Less }
}

fn order_floats_dd(x: u64, y: u64) -> Ordering
{
	//Try demoting both to single. If this works, do ss ordering. If one fails to demote, it is greater one.
	//NaNs always demote.
	match (try_demote_double_to_single(x), try_demote_double_to_single(y)) {
		(Some(x), Some(y)) => order_floats_ss(x, y),
		(Some(_), None) => Ordering::Greater,
		(None, Some(_)) => Ordering::Less,
		(None, None) => x.cmp(&y),
	}
}

fn dereference<T:Copy>(x: &T) -> T { *x }

impl Ord for CBOR
{
	fn cmp(&self, other: &Self) -> Ordering
	{
		//Implement the canonical CBOR order.
		match (self, other) {
			//Order integers according to numeric value.
			(&CBOR::Integer(ref x), &CBOR::Integer(ref y)) => x.cmp(y),
			//Order integers according to numeric value.
			(&CBOR::NegInteger(ref x), &CBOR::NegInteger(ref y)) => x.cmp(y),
			//Order octets first by length, tiebreaking by lexicographic comparision.
			(&CBOR::Octets(ref x), &CBOR::Octets(ref y)) => {
				eq_chain!(x.len().cmp(&y.len()));
				x.cmp(y)
			},
			//Order strings first by length, tiebreaking by lexicographic comparision.
			(&CBOR::String(ref x), &CBOR::String(ref y)) => {
				eq_chain!(x.len().cmp(&y.len()));
				x.cmp(y)
			},
			//Order arrays first by length, tiebreaking by lexicographic comparision.
			(&CBOR::Array(ref x), &CBOR::Array(ref y)) => {
				eq_chain!(x.len().cmp(&y.len()));
				//x and y are guaranteed to have equal length.
				for (i, j) in x.iter().zip(y.iter()) { eq_chain!(i.cmp(j)); }
				Ordering::Equal
			},
			//Order dictionaries first by length, tiebreaking by lexicographic comparision. For each
			//entry, key is compared before value.
			(&CBOR::Dictionary(ref x), &CBOR::Dictionary(ref y)) => {
				eq_chain!(x.len().cmp(&y.len()));
				//x and y are guaranteed to have equal length.
				for ((k1, v1), (k2, v2)) in x.iter().zip(y.iter()) {
					eq_chain!(k1.cmp(k2));
					eq_chain!(v1.cmp(v2));
				}
				Ordering::Equal
			},
			//Order tags first by value, tiebreaking by comparing tagged values.
			(&CBOR::Tag(t1, ref x), &CBOR::Tag(t2, ref y)) => {
				eq_chain!(t1.cmp(&t2));
				x.cmp(y)
			},
			//Order simples according to numeric value.
			(&CBOR::Simple(ref x), &CBOR::Simple(ref y)) => x.cmp(y),
			//Order floats nontrivially.
			(&CBOR::HalfFloat(x), &CBOR::HalfFloat(y)) => order_floats_hh(x, y),
			(&CBOR::HalfFloat(x), &CBOR::Float(y)) => order_floats_hs(x, y),
			(&CBOR::HalfFloat(x), &CBOR::Double(y)) => order_floats_hd(x, y),
			(&CBOR::Float(x), &CBOR::HalfFloat(y)) => order_floats_hs(y, x).reverse(),
			(&CBOR::Float(x), &CBOR::Float(y)) => order_floats_ss(x, y),
			(&CBOR::Float(x), &CBOR::Double(y)) => order_floats_sd(x, y),
			(&CBOR::Double(x), &CBOR::HalfFloat(y)) => order_floats_hd(y, x).reverse(),
			(&CBOR::Double(x), &CBOR::Float(y)) => order_floats_sd(y, x).reverse(),
			(&CBOR::Double(x), &CBOR::Double(y)) => order_floats_dd(x, y),
			//Different types: Order according to priority.
			(ref x, ref y) => x.priority().cmp(&y.priority())
		}
	}
}

impl PartialEq for CBOR
{
	fn eq(&self, other: &Self) -> bool { self.cmp(other).is_eq() }
}

impl PartialOrd for CBOR
{
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> { Some(self.cmp(other)) }
}

impl PartialEq<i64> for CBOR
{
	fn eq(&self, x: &i64) -> bool { self.as_integer() == Some(*x) }
}

impl PartialEq<u64> for CBOR
{
	fn eq(&self, x: &u64) -> bool { self.as_unsigned() == Some(*x) }
}

impl PartialEq<str> for CBOR
{
	fn eq(&self, x: &str) -> bool { self.as_string() == Some(x) }
}

impl PartialEq<[u8]> for CBOR
{
	fn eq(&self, x: &[u8]) -> bool { self.as_octets() == Some(x) }
}

impl Eq for CBOR {}

///Turn something into CBOR node.
///
///This is used to implement the [`CBOR::from()`] method.
///
///[`CBOR::from()`]: enum.CBOR.html#method.from
pub trait ToCbor
{
	///Construct CBOR from this value.
	///
	/// # Return value:
	///
	///The constructed CBOR value.
	fn to_cbor(self) -> CBOR;
}

impl ToCbor for bool
{
	fn to_cbor(self) -> CBOR { if self { CBOR::TRUE } else { CBOR::FALSE } }
}

impl ToCbor for i64
{
	fn to_cbor(self) -> CBOR
	{
		if self >= 0 { CBOR::Integer(self as u64) } else { CBOR::NegInteger((!self) as u64) }
	}
}

impl ToCbor for u64
{
	fn to_cbor(self) -> CBOR { CBOR::Integer(self) }
}

impl ToCbor for String
{
	fn to_cbor(self) -> CBOR { CBOR::String(self) }
}

impl<'a> ToCbor for &'a String
{
	fn to_cbor(self) -> CBOR { CBOR::String(self.clone()) }
}

impl<'a> ToCbor for &'a str
{
	fn to_cbor(self) -> CBOR { CBOR::String(self.to_owned()) }
}

impl ToCbor for Vec<u8>
{
	fn to_cbor(self) -> CBOR { CBOR::Octets(self) }
}

impl<'a> ToCbor for &'a Vec<u8>
{
	fn to_cbor(self) -> CBOR { CBOR::Octets(self.clone()) }
}

impl<'a> ToCbor for &'a [u8]
{
	fn to_cbor(self) -> CBOR { CBOR::Octets(self.to_owned()) }
}

impl<'a> ToCbor for &'a [CBOR]
{
	fn to_cbor(self) -> CBOR { CBOR::Array(self.to_owned()) }
}

impl ToCbor for Vec<CBOR>
{
	fn to_cbor(self) -> CBOR { CBOR::Array(self) }
}

impl<'a> ToCbor for &'a Vec<CBOR>
{
	fn to_cbor(self) -> CBOR { CBOR::Array(self.clone()) }
}

impl<'a> ToCbor for &'a [(CBOR, CBOR)]
{
	fn to_cbor(self) -> CBOR { CBOR::Dictionary(self.iter().cloned().collect()) }
}

impl<'a> ToCbor for (u64, CBOR)
{
	fn to_cbor(self) -> CBOR { CBOR::Tag(self.0, Box::new(self.1)) }
}

impl<'a> ToCbor for f32
{
	fn to_cbor(self) -> CBOR { CBOR::Float(self.to_bits()) }
}

impl<'a> ToCbor for f64
{
	fn to_cbor(self) -> CBOR { CBOR::Double(self.to_bits()) }
}


///Streaming token for CBOR.
///
///These are constructed by iterating [`CborTokenStream`](struct.CborTokenStream.html).
#[derive(Copy,Clone,Debug)]
pub enum CborToken<'a>
{
	///Positive integer.
	///
	/// # Arguments:
	///
	/// 1. `value`: The integer value.
	Integer(u64),
	///Negative integer.
	///
	/// # Arguments:
	///
	/// 1. `value`: The 2s complement of the value, 
	///
	/// # Notes:
	///
	/// * There is bias of `1`. That is, `0` represents value of `-1`, and 2^64-1 represents vale of `-2^64`.
	NegInteger(u64),
	///Octet string.
	///
	/// # Arguments:
	///
	/// 1. `value`: The octet string value or fragment of it.
	///
	/// # Notes:
	///
	/// * The value is fragment if `Octets` occurs inside `OctetsInd` to `Stop`, otherwise it is full value.
	Octets(&'a [u8]),
	///Start indeterminate-length octet string.
	OctetsInd,
	///Text string.
	///
	/// # Arguments:
	///
	/// 1. `value`: The UTF-8 string value or fragment of it.
	///
	/// # Notes:
	///
	/// * The value is fragment if `Text` occurs inside `TextInd` to `Stop`, otherwise it is full value.
	Text(&'a str),
	///Start indeterminate-length text string.
	TextInd,
	///Start array of length.
	///
	/// # Arguments:
	///
	/// 1. `length`: The length of array in elements. `None` means indeterminate-length array ended by `Stop`.
	Array(Option<u64>),
	///Start dictionary of length.
	///
	/// # Arguments:
	///
	/// 1. `length`: The length of dictionary in element pairs. `None` means indeterminate-length dictionary
	///ended by `Stop`.
	Dictionary(Option<u64>),
	///Tag.
	///
	/// # Arguments:
	///
	/// 1. `tagnumber`: The number of CBOR tag.
	Tag(u64),
	///Simple.
	///
	/// # Arguments:
	///
	/// 1. `value`: The simple value. Always between `0` and `23` or `32` and `255`, inclusive.
	Simple(u8),
	///Half-float.
	///
	/// # Arguments:
	///
	/// 1. `value`: The raw IEEE standard encoding of the floating-point number.
	HalfFloat(u16),
	///Single-float.
	///
	/// # Arguments:
	///
	/// 1. `value`: The raw IEEE standard encoding of the floating-point number.
	Float(u32),
	///Double-float.
	///
	/// # Arguments:
	///
	/// 1. `value`: The raw IEEE standard encoding of the floating-point number.
	Double(u64),
	///Stop (end indeterminate-length value).
	Stop,
}

///Streaming token error for CBOR.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
#[non_exhaustive]
pub enum CborStreamError
{
	///Unrecognized opcode.
	///
	/// # Arguments:
	///
	/// 1. `opcode`: The bad opcode.
	///
	/// # Notes: 
	///
	/// * The Possible bad opcodes are: 0x1C-0x1F, 0x3C-0x3F, 0x5C-0x5E, 0x7C-0x7E, 0x9C-0x9E, 0xBC-0xBE,
	///0xDC-0xDF and 0xFC-0xFE.
	Opcode(u8),
	///Expected more bytes.
	///
	///
	/// # Arguments:
	///
	/// 1. `amount`: Number of more bytes that were expected, but are not present.
	ExpectedMore(u64),
	///Bad simple.
	///
	/// # Arguments:
	///
	/// 1. `item`: The number of bad simple item (always 24-31).
	BadSimple(u8),
	///String not UTF-8.
	///
	/// # Arguments:
	///
	/// 1. `offset`: The byte offset in string where UTF-8 decoding goes bad.
	NotUtf8(usize),
}

impl Display for CborStreamError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::CborStreamError::*;
		match *self {
			Opcode(opc) => write!(f, "Uknown opcode {opc}"),
			ExpectedMore(amt) => write!(f, "Expected at least {amt} more bytes"),
			BadSimple(simple) => write!(f, "Bad simple type {simple}"),
			NotUtf8(pos) => write!(f, "String is not valid UTF-8 (offset {pos})"),
		}
	}
}

macro_rules! stream_err
{
	(0 $err:ident) => { return Some(Err(CborStreamError::$err)) };
	(1 $err:ident $arg:expr) => { return Some(Err(CborStreamError::$err($arg))) };
	(R1 $err:ident $arg:expr) => { return Err(CborStreamError::$err($arg)) };
}

macro_rules! eat_stream
{
	(RAW $stream:expr, $argument:expr) => {{
		let stream = $stream;
		let argument = $argument;
		//Perform saturating cast of argument into usize. If argument overflows 32-bit usize, this is
		//guaranteed to produce value big enough to cause split to fail.
		let slen = stream.len();
		let argsz: usize = argument.try_into().unwrap_or(usize::MAX);
		//If this fails, argument > slen, because argument >= argsz > slen.
		split_at(stream, argsz).map_err(|_|{
			argument-slen as u64
		})
	}};
	(DERR $stream:expr, $argument:expr) => {
		match eat_stream!(RAW $stream, $argument) {
			Ok(t) => t,
			Err(excess) => stream_err!(R1 ExpectedMore excess)
		}
	};
	(OERR $stream:expr, $argument:expr) => {{
		match eat_stream!(RAW $stream, $argument) {
			Ok(t) => t,
			Err(excess) => stream_err!(1 ExpectedMore excess)
		}
	}};
}

///Stream of CBOR tokens.
///
///The token stream is accessed via the `Iterator` trait. The type iterated over is
///`Result<CborToken<'a>, CborStreamError>`. See [`CborToken`] and [`CborStreamError`].
///
///Note that after an error occurs, return value is undefined (but no illegal operations occur). Also note that
///since this is a token stream, no attempt at checking inter-token dependicies is made. E.g., token stream can
///contain `Stop` tokens outside any indeterminate-length map or array, which is just plain illegal in CBOR.
///
///[`CborToken`]: enum.CborToken.html
///[`CborStreamError`]: enum.CborStreamError.html
#[derive(Clone)]
pub struct CborTokenStream<'a>(&'a [u8], usize);

impl<'a> CborTokenStream<'a>
{
	///Create new stream of CBOR tokens.
	///
	/// # Parameters:
	///
	/// * `x`: The CBOR to lex.
	///
	/// # Return value:
	///
	///The CBOR stream.
	///
	/// # Notes:
	///
	/// * This does not do any interpretation of the stream, so this operation can not fail.
	pub fn new(x: &'a [u8]) -> CborTokenStream<'a> { CborTokenStream(x, x.as_ptr() as usize) }
	///Get current offset to stream.
	///
	/// # Return value:
	///
	///The byte offset in stream currently being parsed.
	pub fn offset(&self) -> usize { (self.0.as_ptr() as usize).wrapping_sub(self.1) }
	///Get the remaining stream.
	///
	/// # Return value:
	///
	///The remaining part of the stream, as byte slice.
	///
	/// # Notes:
	///
	/// * This is useful for parsing parts of larger CBOR using the main CBOR parser [`CBOR`].
	///
	///[`CBOR`]: enum.CBOR.html
	pub fn remaining(&self) -> &'a [u8] { self.0 }
	///Skip specified number of bytes in stream.
	///
	/// # Parameters:
	///
	/// * `n`: Number of bytes to discard. If too large, it is capped to maximum possible value (discard all).
	///
	/// # Notes:
	///
	/// * This is useful for acknowledging parsing parts of larger CBOR using the main CBOR parser [`CBOR`].
	///
	///[`CBOR`]: enum.CBOR.html
	pub fn discard_bytes(&mut self, n: usize) { self.0 = self.0.get(n..).unwrap_or_default(); }
	///Peek next opcode.
	///
	/// # Error conditions:
	///
	/// * The stream has reached the end.
	///
	/// # Return value: 
	///
	///The next opcode. One of (the remaining are illegal):
	///
	/// value|type
	/// -----|----
	/// 0x00-0x1B|Unsigned integer
	/// 0x20-0x3B|Negative integers
	/// 0x40-0x5B,0x5F|Byte string
	/// 0x60-0x7B,0x7F|Text string
	/// 0x80-0x9B,0x9F|Array
	/// 0xA0-0xBB,0xBF|Dictionary
	/// 0xC0-0xDB|Tag
	/// 0xE0-0xF8|Simple
	/// 0xF9|Half float
	/// 0xFA|Single float
	/// 0xFB|Dobule float
	/// 0xFF|Stop
	pub fn peek_next_opcode(&self) -> Option<u8> { self.0.get(0).map(dereference) }
	///Is at end?
	///
	/// # Return value:
	///
	///`true` if the stream is at the end, `false` otherwise.
	///
	/// # Notes:
	///
	/// * This is the same as `self.peek_next_opcode().is_none()`.
	pub fn at_end(&self) -> bool { self.0.is_empty() }
	fn __iter_next_def_major2(&mut self, argument: u64) -> Result<CborToken<'a>, CborStreamError>
	{
		let (head, tail) = eat_stream!(DERR self.0, argument);
		self.0 = tail;
		Ok(CborToken::Octets(head))
	}
	fn __iter_next_def_major3(&mut self, argument: u64) -> Result<CborToken<'a>, CborStreamError>
	{
		let (head, tail) = eat_stream!(DERR self.0, argument);
		match from_utf8(head) {
			Ok(head) => {
				//Only advance on success, so error can be relative to start of
				//octet string.
				self.0 = tail;
				Ok(CborToken::Text(head))
			},
			Err(e) => {
				let mut o = e.valid_up_to();
				let b1 = head.get(o+0).map_or(0, dereference) as u32;
				let b2 = head.get(o+1).map_or(0, dereference) as u32;
				let b3 = head.get(o+2).map_or(0, dereference) as u32;
				let b4 = head.get(o+3).map_or(0, dereference) as u32;
				let d = b1 * 16777216 + b2 * 65536 + b3 * 256 + b4;
				match d {
					0x00000000..=0xC1FFFFFF => (),
					0xF5000000..=0xFFFFFFFF => (),
					//Overlong encodings and surrogate range. Here fault is
					//always attributed to second byte.
					0xE0000000..=0xE09FFFFF => o += 1,
					0xEDA00000..=0xEDBFFFFF => o += 1,
					0xF0000000..=0xF08FFFFF => o += 1,
					0xF4900000..=0xF4FFFFFF => o += 1,
					0xE0A00000..=0xF48FFFFF if d>>22&3!=2 => o += 1,
					0xF0900000..=0xF48FFFFF if d>>14&3!=2 => o += 2,
					//If none of the above, the fault must be in the last byte.
					0xC2000000..=0xDFFFFFFF => o += 1,
					0xE0A00000..=0xEFFFFFFF => o += 2,
					0xF0900000..=0xF48FFFFF => o += 3,
				}
				stream_err!(R1 NotUtf8 o);
			},
		}
	}
}

macro_rules! iter_err
{
	($e:expr) => { match $e { Ok(x) => x, Err(e) => return Some(Err(e)) } };
}

impl<'a> Iterator for CborTokenStream<'a>
{
	type Item = Result<CborToken<'a>, CborStreamError>;
	fn next(&mut self) -> Option<Result<CborToken<'a>, CborStreamError>>
	{
		//Fetch the opcode. If there is no opcode, end the stream.
		let (opcode, tail) = split_head_tail(self.0)?;
		self.0 = tail;
		let major = opcode >> 5;
		let minor = opcode & 31;
		let (argument, tail) = match minor {
			0..=23 => (minor as u64, self.0),
			24 => {
				let (head, tail) = eat_stream!(OERR self.0, 1u64);
				let head = head.try_into().ok()?;
				(u8::from_be_bytes(head) as u64, tail)
			},
			25 => {
				let (head, tail) = eat_stream!(OERR self.0, 2u64);
				let head = head.try_into().ok()?;
				(u16::from_be_bytes(head) as u64, tail)
			},
			26 => {
				let (head, tail) = eat_stream!(OERR self.0, 4u64);
				let head = head.try_into().ok()?;
				(u32::from_be_bytes(head) as u64, tail)
			},
			27 => {
				let (head, tail) = eat_stream!(OERR self.0, 8u64);
				let head = head.try_into().ok()?;
				(u64::from_be_bytes(head), tail)
			},
			31 => (0, self.0),	//Argument is not meaningful.
			_ => stream_err!(1 Opcode opcode)
		};
		self.0 = tail;
		let r = match major {
			0 if minor == 31 => stream_err!(1 Opcode opcode),
			1 if minor == 31 => stream_err!(1 Opcode opcode),
			2 if minor == 31 => CborToken::OctetsInd,
			3 if minor == 31 => CborToken::TextInd,
			4 if minor == 31 => CborToken::Array(None),
			5 if minor == 31 => CborToken::Dictionary(None),
			6 if minor == 31 => stream_err!(1 Opcode opcode),
			7 if minor == 24 => {
				if argument < 32 { stream_err!(1 BadSimple argument as u8); }
				CborToken::Simple(argument as u8)
			},
			7 if minor == 25 => CborToken::HalfFloat(argument as u16),
			7 if minor == 26 => CborToken::Float(argument as u32),
			7 if minor == 27 => CborToken::Double(argument),
			7 if minor == 31 => CborToken::Stop,
			0 => CborToken::Integer(argument),
			1 => CborToken::NegInteger(argument),
			//2 with minor < 31 and 3 with minor < 31 have additional bytes.
			2 => iter_err!(self.__iter_next_def_major2(argument)),
			3 => iter_err!(self.__iter_next_def_major3(argument)),
			4 => CborToken::Array(Some(argument)),
			5 => CborToken::Dictionary(Some(argument)),
			6 => CborToken::Tag(argument),
			7 => CborToken::Simple(minor),
			_ => stream_err!(1 Opcode opcode)
		};
		Some(Ok(r))
	}
}

#[cfg(test)]
mod test;
