//!Filename types
#![deny(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]
#[cfg(any(feature="std",test))] #[macro_use] extern crate std;
use btls_aux_collections::Arc;
use btls_aux_collections::Cow;
use btls_aux_collections::Rc;
use btls_aux_collections::String;
use btls_aux_collections::ToOwned;
use btls_aux_collections::Vec;
use btls_aux_fail::fail_if_none;
use btls_aux_memory::Attachment;
use btls_aux_memory::EscapeByteString;
use btls_aux_memory::split_at;
use btls_aux_memory::split_attach_first;
use btls_aux_memory::split_attach_last;
use btls_aux_memory::strip_prefix;
use btls_aux_memory::strip_suffix;
#[cfg(unix)] use btls_aux_unix::Path as UnixPath;
use core::borrow::Borrow;
use core::cmp::Ordering;
use core::fmt::Arguments;
use core::fmt::Debug;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::fmt::Write as FmtWrite;
use core::hash::Hash;
use core::hash::Hasher;
use core::mem::transmute_copy;
use core::ops::Deref;
use core::ops::Range;
use core::str::from_utf8;
#[cfg(feature="std")] use std::ffi::OsStr;
#[cfg(feature="std")] use std::ffi::OsString;
#[cfg(feature="std")] use std::io::Write;
#[cfg(feature="std")] use std::io::Error;
#[cfg(feature="std")] use std::path::Path;
#[cfg(feature="std")] use std::path::PathBuf;

unsafe trait CastAsByteSlice {}
unsafe impl CastAsByteSlice for [u8] {}
unsafe impl CastAsByteSlice for Filename {}
unsafe impl CastAsByteSlice for FilenameMatchKey {}
#[cfg(feature="std")] unsafe impl CastAsByteSlice for OsStr {}
#[cfg(feature="std")] unsafe impl CastAsByteSlice for Path {}
unsafe trait CastAsByteSliceFrom {}
unsafe impl<T:CastAsByteSlice+?Sized> CastAsByteSliceFrom for T {}
unsafe impl CastAsByteSliceFrom for str {}

fn cast_byte_slice<'a,A:CastAsByteSliceFrom+?Sized+'a,B:CastAsByteSlice+?Sized+'a>(x: &'a A) -> &'a B
{
	//Transmute is not UB enough.
	unsafe{transmute_copy::<&'a A, &'a B>(&x)}
}

unsafe trait CastAsByteVector {}
unsafe impl CastAsByteVector for Vec<u8> {}
unsafe impl CastAsByteVector for FilenameBuf {}
#[cfg(feature="std")] unsafe impl CastAsByteVector for OsString {}
#[cfg(feature="std")] unsafe impl CastAsByteVector for PathBuf {}
unsafe trait CastAsByteVectorFrom {}
unsafe impl<T:CastAsByteVector+?Sized> CastAsByteVectorFrom for T {}
unsafe impl CastAsByteVectorFrom for String {}

fn cast_byte_vector<A:CastAsByteVectorFrom,B:CastAsByteVector>(x: A) -> B
{
	//Transmute is not UB enough.
	let y = unsafe{transmute_copy::<A, B>(&x)};
	//Prevent drop of x, as all resources were transferred to y, and dropping it would lead to double-drop.
	core::mem::forget(x);
	y
}


//This type has pretty odd validity invariant:
// - It must be layout-compatible with OsStr.
// - It must be layout-compatible with str.
// - API must not allow introducing non-UTF-8.
// - API must not allow introducing non-WTF-8.
///Borrowed file name.
pub struct Filename([u8]);

//This type has pretty odd validity invariant:
// - It must be layout-compatible with OsString.
// - It must be layout-compatible with String.
// - API must not allow introducing non-UTF-8.
// - API must not allow introducing non-WTF-8.
///Owned file name.
#[derive(Clone)]
pub struct FilenameBuf(Vec<u8>);

///Reference-Counted file name.
#[derive(Clone)]
pub struct FilenameRc(Rc<FilenameBuf>);

///Atomic reference-Counted file name.
#[derive(Clone)]
pub struct FilenameArc(Arc<FilenameBuf>);

///Character in filename.
#[derive(Copy,Clone,PartialEq,Debug)]
pub enum FilenameChar
{
	///Specified unicode character.
	Unicode(char),
	///Non-unicode character.
	Other(u16),
}

///Range.
pub trait SliceIndexRange
{
	///Start
	fn start(&self) -> usize;
	///End, if any.
	fn end(&self) -> Option<usize>;
}

///Iterator over characters in Filename.
pub struct FilenameChars<'a>(&'a Filename, usize);
///Iterator over (index, character) pairs in Filename.
pub struct FilenameCharIndices<'a>(&'a Filename, usize);
///Show filename.
pub struct FilenameShow<'a>(&'a Filename, bool);

impl<'a> Iterator for FilenameChars<'a>
{
	type Item = FilenameChar;
	fn next(&mut self) -> Option<FilenameChar>
	{
		let (ch, next) = self.0.char_at2(self.1)?;
		self.1 = next;
		Some(ch)
	}
}

impl<'a> Iterator for FilenameCharIndices<'a>
{
	type Item = (usize, FilenameChar);
	fn next(&mut self) -> Option<(usize, FilenameChar)>
	{
		let idx = self.1;
		let (ch, next) = self.0.char_at2(idx)?;
		self.1 = next;
		Some((idx, ch))
	}
}

impl<'a> Display for FilenameShow<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		let escape = if self.1 { '\\' } else { '/' };
		for c in self.0.chars() { match c {
			FilenameChar::Unicode('/') if !self.1 => write!(f, "//")?,
			FilenameChar::Unicode('\\') if self.1 => write!(f, "\\\\")?,
			FilenameChar::Unicode(c) => write!(f, "{c}")?,
			FilenameChar::Other(c) => write!(f, "{escape}{c:03x}")?,
		}}
		Ok(())
	}
}

impl SliceIndexRange for Range<usize>
{
	fn start(&self) -> usize { self.start }
	fn end(&self) -> Option<usize> { Some(self.end) }
}

impl SliceIndexRange for core::ops::RangeFrom<usize>
{
	fn start(&self) -> usize { self.start }
	fn end(&self) -> Option<usize> { None }
}

impl SliceIndexRange for core::ops::RangeTo<usize>
{
	fn start(&self) -> usize { 0 }
	fn end(&self) -> Option<usize> { Some(self.end) }
}

impl SliceIndexRange for core::ops::RangeFull
{
	fn start(&self) -> usize { 0 }
	fn end(&self) -> Option<usize> { None }
}

fn seq2(a: u8, b: u8) -> bool
{
	if b >> 6 != 2 || a & 0xE0 != 0xC0 { return false; }
	let x = a << 2;
	x >= 8
}
fn seq3(a: u8, b: u8, c: u8) -> bool
{
	if (b|c) >> 6 != 2 || a & 0xF0 != 0xE0 { return false; }
	let x = a << 3 | b >> 5;
	x >= 5
}
fn seq3u(a: u8, b: u8, c: u8) -> bool
{
	if (b|c) >> 6 != 2 || a & 0xF0 != 0xE0 { return false; }
	let x = a << 3 | b >> 5;
	x >= 5 && x != 109	//x=109 is surrogates.
}

fn seq4(a: u8, b: u8, c: u8, d: u8) -> bool
{
	if (b|c|d) >> 6 != 2 || a & 0xF8 != 0xF0 { return false; }
	let x = a << 4 | b >> 4;
	x >= 9 && x < 73
}

fn preduce(s: u64) -> u64 { (s >> 55) * 55 + (s & 0x7FFFFFFFFFFFFF) }

struct ReverseIterator<T,I:DoubleEndedIterator<Item=T>>(I);

impl<T,I:DoubleEndedIterator<Item=T>> Iterator for ReverseIterator<T,I>
{
	type Item = T;
	fn next(&mut self) -> Option<T> { self.0.next_back() }
}

fn __check_valid_filename_windows(x: &[u8]) -> bool
{
	let mut state = 0;
	for &b in x.iter() {
		state = match (state, b) {
			//0 Initial.
			(0, 0x00..=0x7F) => 0,
			(0, 0xC2..=0xDF) => 1,
			(0, 0xE0) => 4,
			(0, 0xE1..=0xEF) => 2,
			(0, 0xF0) => 5,
			(0, 0xF1..=0xF3) => 3,
			(0, 0xF4) => 6,
			(0, _) => return false,
			//1 Any 1 continuation.
			(1, 0x80..=0xBF) => 0,
			//2 Any 2 continuations.
			(2, 0x80..=0xBF) => 1,
			//3 Any 3 continuations.
			(3, 0x80..=0xBF) => 2,
			//4 High continuation, contunuation.
			(4, 0xA0..=0xBF) => 1,
			//5 Not low quadrant, two continuations.
			(5, 0x90..=0xBF) => 2,
			//6 Low quadrant, two continuations.
			(6, 0x80..=0x8F) => 2,
			//Any other is illegal.
			(_, _) => return false
		};
	}
	state == 0
}

#[cfg(unix)] fn check_valid_filename(_: &[u8]) -> bool { true }
#[cfg(not(unix))] fn check_valid_filename(x: &[u8]) -> bool { __check_valid_filename_windows(x) }

fn find_last_match(haystack: &[u8], needle: &[u8]) -> Option<Range<usize>>
{
	//Special case if needle.len() == 0: Produce l..l.
	if needle.len() == 0 { return Some(haystack.len()..haystack.len()); }
	//Special case if needle.len() == 1: Naive scan.
	if needle.len() == 1 {
		let c = needle[0];
		return haystack.iter().enumerate().rfind(|(_,&v)|v==c).map(|(i,_)|i..i+1);
	}
	const N: u64 = 0x7FFFFFFFFFFFC9;
	let mshift = haystack.len().checked_sub(needle.len())?;
	let mut needle_h = 0;
	let mut haystack_h = 0;
	let mut killer = 1;
	for (&b, &c) in ReverseIterator(needle.iter()).zip(ReverseIterator(haystack.iter())) {
		needle_h = preduce(needle_h << 8 | b as u64);
		haystack_h = preduce(haystack_h << 8 | c as u64);
		killer = preduce(killer << 8);
	}
	if needle_h >= N { needle_h -= N; }
	if killer >= N { killer -= N; }
	for i in 0..mshift {
		let i = mshift - i;
		let j = i + needle.len();
		if needle_h == haystack_h || needle_h + N == haystack_h {
			if &haystack[i..j] == needle { return Some(i..j); }
		}
		haystack_h = preduce(haystack_h << 8 | haystack[i-1] as u64);
		haystack_h = preduce(haystack_h + 2*N - preduce(killer * haystack[j-1] as u64));
	}
	if needle_h == haystack_h || needle_h + N == haystack_h {
		if &haystack[..needle.len()] == needle { return Some(0..needle.len()); }
	}
	None
}

fn find_first_match(haystack: &[u8], needle: &[u8]) -> Option<Range<usize>>
{
	//Special case if needle.len() == 0: Produce 0..0.
	if needle.len() == 0 { return Some(0..0); }
	//Special case if needle.len() == 1: Naive scan.
	if needle.len() == 1 {
		let c = needle[0];
		return haystack.iter().enumerate().find(|(_,&v)|v==c).map(|(i,_)|i..i+1);
	}
	const N: u64 = 0x7FFFFFFFFFFFC9;
	let mshift = haystack.len().checked_sub(needle.len())?;
	let mut needle_h = 0;
	let mut haystack_h = 0;
	let mut killer = 1;
	for (&b, &c) in needle.iter().zip(haystack.iter()) {
		needle_h = preduce(needle_h << 8 | b as u64);
		haystack_h = preduce(haystack_h << 8 | c as u64);
		killer = preduce(killer << 8);
	}
	if needle_h >= N { needle_h -= N; }
	if killer >= N { killer -= N; }
	for i in 0..mshift {
		let j = i + needle.len();
		if needle_h == haystack_h || needle_h + N == haystack_h {
			if &haystack[i..j] == needle { return Some(i..j); }
		}
		haystack_h = preduce(haystack_h << 8 | haystack[j] as u64);
		haystack_h = preduce(haystack_h + 2*N - preduce(killer * haystack[i] as u64));
	}
	if needle_h == haystack_h || needle_h + N == haystack_h {
		if &haystack[mshift..] == needle { return Some(mshift..haystack.len()); }
	}
	None
}

#[cfg(unix)]
fn __is_char_boundary(x: &[u8], idx: usize) -> bool { idx <= x.len() }

#[cfg(not(unix))]
fn __is_char_boundary(x: &[u8], idx: usize) -> bool
{
	//There are no character boundaries out of range.
	if idx > x.len() { return false; }
	//Taking nonexistent bytes as 0 does not change the results.
	let s1 = x.get(idx.wrapping_sub(3)).map(|x|*x).unwrap_or(0);
	let s2 = x.get(idx.wrapping_sub(2)).map(|x|*x).unwrap_or(0);
	let s3 = x.get(idx.wrapping_sub(1)).map(|x|*x).unwrap_or(0);
	let s4 = x.get(idx.wrapping_add(0)).map(|x|*x).unwrap_or(0);
	let s5 = x.get(idx.wrapping_add(1)).map(|x|*x).unwrap_or(0);
	let s6 = x.get(idx.wrapping_add(2)).map(|x|*x).unwrap_or(0);
	//Codepoint is not at boundary exactly when there is some sequence spanning both sides of current
	//position.
	if seq2(s3, s4) { return false; }
	if seq3(s2, s3, s4) { return false; }
	if seq3(s3, s4, s5) { return false; }
	if seq4(s1, s2, s3, s4) { return false; }
	if seq4(s2, s3, s4, s5) { return false; }
	if seq4(s3, s4, s5, s6) { return false; }
	true
}

impl Filename
{
	///Cast OsStr into Filename. The lifetime is preserved.
	#[cfg(feature="std")]
	pub fn from_os_str<'a>(s: &'a OsStr) -> &'a Filename { cast_byte_slice(s) }
	///Cast str into Filename. The lifetime is preserved.
	pub fn from_str<'a>(s: &'a str) -> &'a Filename { cast_byte_slice(s) }
	///Cast Path into Filename. The lifetime is preserved.
	#[cfg(feature="std")]
	pub fn from_path<'a>(s: &'a Path) -> &'a Filename { cast_byte_slice(s) }
	///Cast filename part of Path into Filename. The lifetime is preserved.
	#[cfg(feature="std")]
	pub fn from_path_file_name<'a>(s: &'a Path) -> Option<&'a Filename>
	{
		Some(Filename::from_os_str(s.file_name()?))
	}
	///Cast arbitrary byte slice into Filename. The lifetime is preserved. Available on Unix only.
	#[cfg(unix)] pub fn from_bytes<'a>(s: &'a [u8]) -> &'a Filename { cast_byte_slice(s) }
	///Cast arbitrary byte slice into Filename with validity check. The lifetime is preserved.
	pub fn from_bytes_checked<'a>(s: &'a [u8]) -> Option<&'a Filename>
	{
		fail_if_none!(!check_valid_filename(s));
		Some(cast_byte_slice(s))
	}
	///Cast Filename into OsStr. The lifetime is preserved.
	#[cfg(feature="std")]
	pub fn into_os_str<'a>(&'a self) -> &'a OsStr { cast_byte_slice(self) }
	///Cast Filename into Path. The lifetime is preserved.
	#[cfg(feature="std")]
	pub fn into_path<'a>(&'a self) -> &'a Path { cast_byte_slice(self) }
	///Cast Filename into match key. The lifetime is preserved.
	pub fn into_match_key<'a>(&'a self) -> &'a FilenameMatchKey { cast_byte_slice(self) }
	///Cast Filename into UnixPath. The lifetime is preserved.
	#[cfg(unix)] pub fn into_unix_path<'a>(&'a self) -> Option<&'a UnixPath> { UnixPath::new(&self.0) }
	///Check if path is bad.
	pub fn is_bad_path(&self) -> bool { self.0.iter().any(|&x|x==0) }
	///Cast filename into byte slice. The lifetime is preserved.
	pub fn into_bytes<'a>(&'a self) -> &'a [u8] { cast_byte_slice(self) }
	///Cast Filename into str if possible. The lifetime is preserved. If filename contains non-UTF-8, returns
	///None.
	pub fn into_str<'a>(&'a self) -> Option<&'a str> { from_utf8(&self.0).ok() }
	///Write filename into io::Write.
	#[cfg(feature="std")]
	pub fn write_to_io(&self, mut s: impl Write) -> Result<(), Error> { s.write_all(&self.0) }
	///Length of filename in bytes.
	pub fn len(&self) -> usize { self.0.len() }
	#[allow(missing_docs)] #[deprecated(since="1.4.2", note="Use is_char_boundary() instead")]
	pub fn is_char_boudary(&self, idx: usize) -> bool { __is_char_boundary(&self.0, idx) }
	///Is specified index at char boundary?
	pub fn is_char_boundary(&self, idx: usize) -> bool { __is_char_boundary(&self.0, idx) }
	///Slice filename at char boundaries.
	pub fn get<'a>(&'a self, range: impl SliceIndexRange) -> Option<&'a Filename>
	{
		let start = range.start();
		let end = range.end().unwrap_or(self.len());
		fail_if_none!(!self.is_char_boundary(start) || !self.is_char_boundary(end));
		//This should never fail, since is_char_boundary() checks for bounds, but just in case.
		let slice = self.0.get(start..end)?;
		Some(cast_byte_slice(slice))
	}
	///Split at, if possible.
	pub fn split_at<'a>(&'a self, idx: usize) -> Option<(&'a Filename, &'a Filename)>
	{
		fail_if_none!(!self.is_char_boundary(idx));
		let (head, tail) = split_at(&self.0, idx).ok()?;
		Some((cast_byte_slice(head), cast_byte_slice(tail)))
	}
	///Get character at, if any.
	pub fn char_at(&self, idx: usize) -> Option<FilenameChar>
	{
		self.char_at2(idx).map(|x|x.0)
	}
	///Get next character index, if any.
	pub fn next_char_at(&self, idx: usize) -> Option<usize>
	{
		self.char_at2(idx).map(|x|x.1)
	}
	///Get character and index of next, if any.
	pub fn char_at2(&self, idx: usize) -> Option<(FilenameChar,usize)>
	{
		//If out of range or not character boundary, no character here.
		fail_if_none!(idx >= self.len() || !self.is_char_boundary(idx));
		//We can assume UTF-8 sequence starts here, or that this is non-unicode. Taking nonexistent bytes
		//as 0 does not change results.
		let a = self.0.get(idx.wrapping_add(0)).map(|x|*x).unwrap_or(0);
		let b = self.0.get(idx.wrapping_add(1)).map(|x|*x).unwrap_or(0);
		let c = self.0.get(idx.wrapping_add(2)).map(|x|*x).unwrap_or(0);
		let d = self.0.get(idx.wrapping_add(3)).map(|x|*x).unwrap_or(0);
		use FilenameChar::*;
		Some(match a {
			0x00..=0x7F => {
				let x = a as u32;
				(Unicode(char::from_u32(x).unwrap_or('\0')), idx+1)
			},
			0xC0..=0xDF if seq2(a,b) => {
				let x = (a as u32&0x1F)<<6|(b as u32&0x3F);
				(Unicode(char::from_u32(x).unwrap_or('\0')), idx+2)
			},
			0xE0..=0xEF if seq3u(a,b,c) => {
				let x = (a as u32&0x0F)<<12|(b as u32&0x3F)<<6|(c as u32&0x3F);
				(Unicode(char::from_u32(x).unwrap_or('\0')), idx+3)
			},
			0xF0..=0xF4 if seq4(a,b,c,d) => {
				let x = (a as u32&0x07)<<18|(b as u32&0x3F)<<12|(c as u32&0x3F)<<6|(d as u32&0x3F);
				(Unicode(char::from_u32(x).unwrap_or('\0')), idx+4)
			},
			0xED if seq3(a,b,c) => {
				let x = (b as u16&0x1F)<<6|(c as u16&0x3F);
				(Other(0x800|x), idx+3)
			},
			other => (Other(other as u16), idx+1)
		})
	}
	///Iterate over characters in filename.
	pub fn chars<'a>(&'a self) -> FilenameChars<'a> { FilenameChars(self, 0) }
	///Iterate over (index, character) pairs in filename.
	pub fn char_indices<'a>(&'a self) -> FilenameCharIndices<'a> { FilenameCharIndices(self, 0) }
	///Add extension to filename.
	pub fn add_extension(&self, ext: &str) -> FilenameBuf
	{
		let mut x = Vec::with_capacity(self.len() + ext.len() + 1);
		x.extend_from_slice(&self.0);
		x.push(b'.');
		x.extend_from_slice(ext.as_bytes());
		FilenameBuf(x)
	}
	///Find first instance of specified substring in filename.
	pub fn find(&self, needle: &str) -> Option<Range<usize>> { find_first_match(&self.0, needle.as_bytes()) }
	///Find last instance of specified substring in filename.
	pub fn rfind(&self, needle: &str) -> Option<Range<usize>> { find_last_match(&self.0, needle.as_bytes()) }
	///Find first instance of specified character in filename.
	pub fn findch(&self, ch: char) -> Option<Range<usize>>
	{
		find_first_match(&self.0, ch.encode_utf8(&mut [0;4]).as_bytes())
	}
	///Find last instance of specified character in filename.
	pub fn rfindch(&self, ch: char) -> Option<Range<usize>>
	{
		find_last_match(&self.0, ch.encode_utf8(&mut [0;4]).as_bytes())
	}
	///Get extension in filename, if any.
	///
	///The extension is delimited by the last dot in filename, if said dot is not in the beginning of the
	///filename. 
	pub fn extension<'a>(&'a self) -> Option<&'a Filename> { self.split_name_extension().1 }
	///Get extension in filename, if any.
	///
	///This differs from `extension()` in that it returns None if the extension is not entierely valid UTF-8.
	///Note that there are no requirements on the file stem.
	pub fn extension_unicode<'a>(&'a self) -> Option<&'a str> { self.extension().and_then(|x|x.into_str()) }
	///Get stem in filename.
	///
	///The stem is the part of filename that is not extension.
	pub fn stem<'a>(&'a self) -> &'a Filename { self.split_name_extension().0 }
	///Get stem and extension of the filename.
	pub fn split_name_extension<'a>(&'a self) -> (&'a Filename, Option<&'a Filename>)
	{
		match split_attach_last(&self.0, b".", Attachment::Center) {
			Ok((b"", _)) => (self, None),	//Hidden file, No stem.
			Ok((head, tail)) => (cast_byte_slice(head), Some(cast_byte_slice(tail))),
			Err(_) => (self, None)		//No extension.
		}
	}
	///Does the filename start with the spcified string?
	pub fn starts_with(&self, x: &str) -> bool { self.0.starts_with(x.as_bytes()) }
	///Does the filename end with the spcified string?
	pub fn ends_with(&self, x: &str) -> bool { self.0.ends_with(x.as_bytes()) }
	///Does the filename have specified prefix?
	///
	///If no, returns None, otherwise returns `Some(rest)`, where `rest` is the remainder of the filename.
	pub fn match_prefix<'a>(&'a self, pfx: &Filename) -> Option<&'a Filename>
	{
		let tail = strip_prefix(&self.0, &pfx.0)?;
		Some(cast_byte_slice(tail))
	}
	///Does the filename have specified suffix?
	///
	///If no, returns None, otherwise returns `Some(rest)`, where `rest` is the remainder of the filename.
	pub fn match_suffix<'a>(&'a self, sfx: &Filename) -> Option<&'a Filename>
	{
		let head = strip_suffix(&self.0, &sfx.0)?;
		Some(cast_byte_slice(head))
	}
	#[allow(missing_docs)]
	#[deprecated(since="1.5.0", note="Print the filename directly")]
	pub fn show<'a>(&'a self) -> FilenameShow<'a> { FilenameShow(self, false) }
	#[allow(missing_docs)]
	#[deprecated(since="1.5.0", note="Print the filename directly")]
	pub fn show2<'a>(&'a self) -> FilenameShow<'a> { FilenameShow(self, true) }
	///Return filename escaped into unicode string.
	pub fn escape_unicode<'a>(&'a self) -> Cow<'a, str>
	{
		//If filename is unicode with no / in it, borrow it.
		if let Some(n) = self.into_str() { if n.find('/').is_none() { return Cow::Borrowed(n); }}
		let mut out = String::new();
		for c in self.chars() { match c {
			//Since / should not be present, use that to escape non-unicode.
			FilenameChar::Unicode('/') => out.push_str("//"),
			FilenameChar::Unicode(c) => out.push(c),
			//The highest non-unicode codepoint is 0xFFF.
			FilenameChar::Other(c) => {write!(out, "/{c:03x}").ok();},
		}}
		Cow::Owned(out)
	}
	///Warp in reference count.
	pub fn to_rc(&self) -> FilenameRc { self.to_owned().into_rc() }
	///Warp in atomic reference count.
	pub fn to_arc(&self) -> FilenameArc { self.to_owned().into_arc() }
}

impl Debug for Filename
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "Filename({fname})", fname=FilenameShow(self, true))
	}
}

impl Display for Filename
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		Display::fmt(&EscapeByteString(&self.0), f)
	}
}

trait AsRefOsStr
{
	fn as_raw_bytes(&self) -> &[u8];
}

impl AsRefOsStr for str
{
	fn as_raw_bytes(&self) -> &[u8] { self.as_bytes() }
}

#[cfg(feature="std")]
impl AsRefOsStr for OsStr
{
	fn as_raw_bytes(&self) -> &[u8] { cast_byte_slice(self) }
}

#[cfg(feature="std")]
impl AsRefOsStr for Path
{
	fn as_raw_bytes(&self) -> &[u8] { cast_byte_slice(self) }
}

impl AsRefOsStr for Filename
{
	fn as_raw_bytes(&self) -> &[u8] { cast_byte_slice(self) }
}

impl AsRefOsStr for String
{
	fn as_raw_bytes(&self) -> &[u8] { self.as_bytes() }
}

#[cfg(feature="std")]
impl AsRefOsStr for OsString
{
	fn as_raw_bytes(&self) -> &[u8] { cast_byte_slice(self.deref()) }
}

#[cfg(feature="std")]
impl AsRefOsStr for PathBuf
{
	fn as_raw_bytes(&self) -> &[u8] { cast_byte_slice(self.deref()) }
}

impl AsRefOsStr for FilenameBuf
{
	fn as_raw_bytes(&self) -> &[u8] { cast_byte_slice(self.deref()) }
}

macro_rules! def_partial
{
	(SINGLE $xtype:ident) => {
		impl Eq for $xtype {}
		impl Ord for $xtype
		{
			fn cmp(&self, rhs: &$xtype) -> Ordering { self.as_raw_bytes().cmp(rhs.as_raw_bytes()) }
		}
		impl Hash for $xtype
		{
			fn hash<S:Hasher>(&self, state: &mut S) { self.as_raw_bytes().hash(state) }
		}
		impl PartialEq<&str> for $xtype
		{
			fn eq(&self, rhs: &&str) -> bool { self.as_raw_bytes() == rhs.as_bytes() }
		}
	};
	(PAIR $left:ident $right:ident) => {
		impl PartialEq<$right> for $left
		{
			fn eq(&self, rhs: &$right) -> bool
			{
				self.as_raw_bytes() == rhs.as_raw_bytes()
			}
		}
		impl PartialOrd<$right> for $left
		{
			fn partial_cmp(&self, rhs: &$right) -> Option<Ordering>
			{
				self.as_raw_bytes().partial_cmp(rhs.as_raw_bytes())
			}
		}
	};
	(SINGLES $right:ident) => {
		def_partial!(SINGLE $right);
	};
	(SINGLES $right:ident,$($rtail:ident),*) => {
		def_partial!(SINGLE $right);
		def_partial!(SINGLES $($rtail),*);
	};
	(RCARTESIAN $left:ident $right:ident) => {
		def_partial!(PAIR $left $right);
	};
	(RCARTESIAN $left:ident $right:ident,$($rtail:ident),*) => {
		def_partial!(PAIR $left $right);
		def_partial!(RCARTESIAN $left $($rtail),*);
	};
	(CARTESIAN $left:ident ; $($right:ident),*) => {
		def_partial!(RCARTESIAN $left $($right),*);
	};
	(CARTESIAN $left:ident,$($ltail:ident),* ; $($right:ident),*) => {
		def_partial!(RCARTESIAN $left $($right),*);
		def_partial!(CARTESIAN $($ltail),* ; $($right),*);
	};
	(COMMUTATOR $($left:ident),* ; $($right:ident),*) => {
		def_partial!(CARTESIAN $($left),* ; $($right),*);
		def_partial!(CARTESIAN $($right),* ; $($left),*);
	};
}

def_partial!(CARTESIAN Filename,FilenameBuf,FilenameRc,FilenameArc ; Filename,FilenameBuf,FilenameRc,FilenameArc);
#[cfg(feature="std")] def_partial!(COMMUTATOR Filename,FilenameBuf ; OsStr,OsString);
#[cfg(feature="std")] def_partial!(COMMUTATOR Filename,FilenameBuf ; Path,PathBuf);
def_partial!(COMMUTATOR Filename,FilenameBuf ; str,String);
def_partial!(SINGLES Filename,FilenameBuf,FilenameRc,FilenameArc);


impl ToOwned for Filename
{
	type Owned = FilenameBuf;
	fn to_owned(&self) -> FilenameBuf { FilenameBuf(self.0.to_owned()) }
}

impl FilenameBuf
{
	///Cast OsString into FilenameBuf.
	#[cfg(feature="std")]
	pub fn from_os_string(s: OsString) -> FilenameBuf { cast_byte_vector(s) }
	///Cast String into FilenameBuf.
	pub fn from_string(s: String) -> FilenameBuf { cast_byte_vector(s) }
	///Cast PathBuf into FilenameBuf.
	#[cfg(feature="std")]
	pub fn from_pathbuf<'a>(s: PathBuf) -> FilenameBuf { cast_byte_vector(s) }
	///Cast arbitrary byte Vector into FilenameBuf. Available on Unix only.
	#[cfg(unix)] pub fn from_bytesbuf(s: Vec<u8>) -> FilenameBuf { cast_byte_vector(s) }
	///Cast arbitrary byte Vector into FilenameBuf with validity check.
	pub fn from_bytesbuf_checked(s: Vec<u8>) -> Result<FilenameBuf,Vec<u8>>
	{
		if !check_valid_filename(&s) { return Err(s); }
		Ok(cast_byte_vector(s))
	}
	///Cast FilenameBuf into OsString.
	#[cfg(feature="std")]
	pub fn into_os_string(self) -> OsString { cast_byte_vector(self) }
	///Cast FilenameBuf into PathBuf.
	#[cfg(feature="std")]
	pub fn into_pathbuf(self) -> PathBuf { cast_byte_vector(self) }
	///Cast FilenameBuf into String if possible. If filename contains non-UTF-8, returns
	///Err with the original name.
	pub fn into_string(self) -> Result<String,FilenameBuf>
	{
		String::from_utf8(self.0).map_err(|e|FilenameBuf(e.into_bytes()))
	}
	///Cast filename into byte vector.
	pub fn into_bytesbuf(self) -> Vec<u8> { cast_byte_vector(self) }
	///Push to end of filename.
	pub fn push(&mut self, x: &str) { self.0.extend_from_slice(x.as_bytes()) }
	///Truncate filename.
	pub fn truncate(&mut self, x: usize) -> bool
	{
		//On windows, have to check the truncation point is valid. On unix, can truncate whereever.
		#[cfg(not(unix))] {
			if x >= self.0.len() { return false; }
			let c = self.0[x];
			if c >= 0x80 && c <= 0xBF { return false; }
		}
		self.0.truncate(x);
		true
	}
	///Push to end of filename.
	pub fn push_fname(&mut self, fname: &Filename) { self.0.extend_from_slice(&fname.0) }
	///Push to end of filename.
	pub fn push_fmt(&mut self, f: Arguments)
	{
		let mut x = PushFmtHelper(&mut self.0);
		write!(x, "{f}").ok();		//Can't fail.
	}
	///Warp in reference count.
	pub fn into_rc(self) -> FilenameRc { FilenameRc(Rc::new(self)) }
	///Warp in atomic reference count.
	pub fn into_arc(self) -> FilenameArc { FilenameArc(Arc::new(self)) }
}

struct PushFmtHelper<'a>(&'a mut Vec<u8>);

impl<'a> FmtWrite for PushFmtHelper<'a>
{
	fn write_str(&mut self, s: &str) -> Result<(), FmtError>
	{
		self.0.extend_from_slice(s.as_bytes());
		Ok(())
	}
}

impl Debug for FilenameBuf
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "FilenameBuf({fname})", fname=FilenameShow(self, true))
	}
}

impl Display for FilenameBuf
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		Display::fmt(self.deref(), f)
	}
}

impl Deref for FilenameBuf
{
	type Target = Filename;
	fn deref(&self) -> &Filename
	{	
		let t: &[u8] = self.0.deref();	//Assert type of result.
		cast_byte_slice(t)
	}
}

impl Borrow<Filename> for FilenameBuf
{
	fn borrow(&self) -> &Filename { self.deref() }
}

impl FilenameRc
{
}

impl Deref for FilenameRc
{
	type Target = Filename;
	fn deref(&self) -> &Filename { self.0.deref().deref() }
}

impl Display for FilenameRc
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		Display::fmt(self.deref(), f)
	}
}

impl Borrow<Filename> for FilenameRc
{
	fn borrow(&self) -> &Filename { self.deref() }
}

impl FilenameArc
{
}

impl Deref for FilenameArc
{
	type Target = Filename;
	fn deref(&self) -> &Filename { self.0.deref().deref() }
}

impl Display for FilenameArc
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		Display::fmt(self.deref(), f)
	}
}

impl Borrow<Filename> for FilenameArc
{
	fn borrow(&self) -> &Filename { self.deref() }
}



///Match filename key.
pub struct FilenameMatchKey([u8]);


///Match filename.
#[derive(Clone,Debug)]
pub struct FilenameMatch(Vec<u8>);

impl FilenameMatch
{
	///Construct filename match from escaped filename.
	pub fn new(mut pattern: &str) -> FilenameMatch
	{
		let mut out = Vec::new();
		loop {
			match split_attach_first(pattern, "/", Attachment::Center) {
				Ok((head, tail)) => {
					out.extend_from_slice(head.as_bytes());
					pattern = if let Some(tail) = tail.strip_prefix("/") {
						out.push(b'/');
						tail
					} else if let Ok((t, tail)) = split_at(tail, 3) {
						if let Ok(v) = u16::from_str_radix(t, 16) {
							if v & 0xF80 == 0x80 {
								out.push(v as u8);
							} else if v & 0x800 == 0x800 {
								out.push(0xED);
								out.push(0x80 + (v >> 6) as u8);
								out.push(0x80 + (v & 0x3F) as u8);
							}
						}
						tail
					} else {
						tail
					}
				},
				Err(t) => {
					out.extend_from_slice(t.as_bytes());
					break;
				}
			}
		}
		FilenameMatch(out)
	}
	///Return Display instance to print the filename.
	pub fn show<'a>(&'a self) -> FilenameShow<'a>
	{
		let t = cast_byte_slice(self.0.deref());	//Can be invalid if not transmuted further.
		FilenameShow(t, false)
	}
}

impl Deref for FilenameMatch
{
	type Target = FilenameMatchKey;
	fn deref(&self) -> &FilenameMatchKey
	{	
		cast_byte_slice(self.0.deref())
	}
}

impl Borrow<FilenameMatchKey> for FilenameMatch
{
	fn borrow(&self) -> &FilenameMatchKey { self.deref() }
}


impl PartialEq<FilenameMatch> for FilenameMatch
{
	fn eq(&self, rhs: &FilenameMatch) -> bool { self.0.deref() == rhs.0.deref() }
}
impl PartialOrd<FilenameMatch> for FilenameMatch
{
	fn partial_cmp(&self, rhs: &FilenameMatch) -> Option<Ordering> { self.0.deref().partial_cmp(rhs.0.deref()) }
}
impl Eq for FilenameMatch {}
impl Ord for FilenameMatch
{
	fn cmp(&self, rhs: &FilenameMatch) -> Ordering { self.0.deref().cmp(rhs.0.deref()) }
}

impl PartialEq<FilenameMatchKey> for FilenameMatchKey
{
	fn eq(&self, rhs: &FilenameMatchKey) -> bool { self.0 == rhs.0 }
}
impl Eq for FilenameMatchKey {}
impl PartialOrd<FilenameMatchKey> for FilenameMatchKey
{
	fn partial_cmp(&self, rhs: &FilenameMatchKey) -> Option<Ordering> { self.0.partial_cmp(&rhs.0) }
}
impl Ord for FilenameMatchKey
{
	fn cmp(&self, rhs: &FilenameMatchKey) -> Ordering { self.0.cmp(&rhs.0) }
}

unsafe impl btls_aux_memory::TrustedInvariantLength for Filename
{
	fn as_byte_slice(&self) -> &[u8] { self.0.as_byte_slice() }
}

unsafe impl<'a> btls_aux_memory::TrustedInvariantLength for &'a Filename
{
	fn as_byte_slice(&self) -> &[u8] { self.0.as_byte_slice() }
}

unsafe impl btls_aux_memory::TrustedInvariantLength for FilenameBuf
{
	fn as_byte_slice(&self) -> &[u8] { &self.0 }
}

unsafe impl<'a> btls_aux_memory::TrustedInvariantLength for &'a FilenameBuf
{
	fn as_byte_slice(&self) -> &[u8] { &self.0 }
}

unsafe impl btls_aux_memory::TrustedInvariantLength for FilenameRc
{
	fn as_byte_slice(&self) -> &[u8] { self.0.as_byte_slice() }
}

unsafe impl<'a> btls_aux_memory::TrustedInvariantLength for &'a FilenameRc
{
	fn as_byte_slice(&self) -> &[u8] { self.0.as_byte_slice() }
}

unsafe impl btls_aux_memory::TrustedInvariantLength for FilenameArc
{
	fn as_byte_slice(&self) -> &[u8] { self.0.as_byte_slice() }
}

unsafe impl<'a> btls_aux_memory::TrustedInvariantLength for &'a FilenameArc
{
	fn as_byte_slice(&self) -> &[u8] { self.0.as_byte_slice() }
}




#[test]
fn iterate_all_unicode()
{
	let mut v = Vec::new();
	for i in 0..0x110000 {
		if i >> 11 == 27 { continue; }
		if i < 128 { v.push(i as u8); }
		else if i < 2048 {
			v.push(0xC0|(i>>6) as u8);
			v.push(0x80|(i&0x3F) as u8);
		} else if i < 65536 {
			v.push(0xE0|(i>>12) as u8);
			v.push(0x80|(i>>6&0x3F) as u8);
			v.push(0x80|(i&0x3F) as u8);
		} else {
			v.push(0xF0|(i>>18) as u8);
			v.push(0x80|(i>>12&0x3F) as u8);
			v.push(0x80|(i>>6&0x3F) as u8);
			v.push(0x80|(i&0x3F) as u8);
		}
	}
	let v = FilenameBuf(v);
	let mut k = v.chars();
	for i in 0..0x110000 {
		if i >> 11 == 27 { continue; }
		let j = k.next().expect("Expected character");
		match j {
			FilenameChar::Other(_) => panic!("Other?"),
			FilenameChar::Unicode(c) => assert_eq!(c as u32, i),
		}
	}
}

#[test]
fn iterate_wtf()
{
	let mut v = Vec::new();
	for i in 0xD800..0xE000 {
		v.push(0xED);
		v.push(0x80|(i>>6&0x3F) as u8);
		v.push(0x80|(i&0x3F) as u8);
	}
	let v = FilenameBuf(v);
	let mut k = v.chars();
	for i in 0x800..0x1000 {
		let j = k.next().expect("Expected character");
		match j {
			FilenameChar::Other(c) => assert_eq!(c as u32, i),
			FilenameChar::Unicode(_) => panic!("Unicode?"),
		}
	}
}

#[test]
fn iterate_bytes()
{
	let mut v = Vec::new();
	for i in 0x80..0x100 {
		v.push(i as u8);
	}
	let v = FilenameBuf(v);
	let mut k = v.chars();
	for i in 0x80..0x100 {
		let j = k.next().expect("Expected character");
		match j {
			FilenameChar::Other(c) => assert_eq!(c as u32, i),
			FilenameChar::Unicode(_) => panic!("Unicode?"),
		}
	}
}

#[test]
fn matches_exact()
{
	let p = Filename::from_str("foo");
	assert_eq!(p.find("foo"), Some(0..3));
	assert_eq!(p.rfind("foo"), Some(0..3));
}

#[test]
fn matches_twice()
{
	let p = Filename::from_str("foofoo");
	assert_eq!(p.find("foo"), Some(0..3));
	assert_eq!(p.rfind("foo"), Some(3..6));
}

#[test]
fn matches_twice_long()
{
	let p = Filename::from_str("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz");
	let y = "abcdefghijklmnopqrstuvwxyz";
	assert_eq!(p.find(y), Some(0..26));
	assert_eq!(p.rfind(y), Some(26..52));
}

#[test]
fn matches_twice_short()
{
	let p = Filename::from_str("aXbXc");
	assert_eq!(p.find("X"), Some(1..2));
	assert_eq!(p.rfind("X"), Some(3..4));
}

#[test]
fn matches_twice_short2()
{
	let p = Filename::from_str("aXYbXYc");
	assert_eq!(p.find("XY"), Some(1..3));
	assert_eq!(p.rfind("XY"), Some(4..6));
}
/*
#[test]
fn matches_overlapping()
{
	let x = b"zzzzzzzzzz";
	let y = b"zzzzzzzz";
	let mut z = Vec::new();
	find_matches(x, y, |m|{z.push(m);true});
	assert_eq!(z.len(), 3);
	assert_eq!(z[0], 0..8);
	assert_eq!(z[1], 1..9);
	assert_eq!(z[2], 2..10);
}
*/
#[test]
fn test_split_name_extension()
{
	let x = Filename::from_str("test.dat");
	let (y, z) = x.split_name_extension();
	assert!(y == "test");
	assert!(z.expect("Extension missing") == "dat");
}

#[test]
fn test_split_name_extension2()
{
	let x = Filename::from_str("test,dat");
	let (y, z) = x.split_name_extension();
	assert!(y == "test,dat");
	assert!(z.is_none());
}

#[test]
fn test_split_name_extension3()
{
	let x = Filename::from_str(".test,dat");
	let (y, z) = x.split_name_extension();
	assert!(y == ".test,dat");
	assert!(z.is_none());
}

#[test]
fn test_split_name_extension4()
{
	let x = Filename::from_str("test.tar.gz");
	let (y, z) = x.split_name_extension();
	assert!(y == "test.tar");
	assert!(z.expect("Extension missing") == "gz");
}

#[test]
fn test_split_name_extension5()
{
	let x = Filename::from_str(".test.tar.gz");
	let (y, z) = x.split_name_extension();
	assert!(y == ".test.tar");
	assert!(z.expect("Extension missing") == "gz");
}

#[test]
fn test_filename_match_1()
{
	let x = FilenameMatch::new("hello");
	assert_eq!(x.0, b"hello");
}

#[test]
fn test_filename_match_2()
{
	let x = FilenameMatch::new("hello//world");
	assert_eq!(x.0, b"hello/world");
}

#[test]
fn test_filename_match_3()
{
	let x = FilenameMatch::new("hello/0f6world");
	assert_eq!(x.0, b"hello\xf6world");
}

#[test]
fn test_filename_match_4()
{
	let x = FilenameMatch::new("hello/9f6world");
	assert_eq!(x.0, b"hello\xed\xa7\xb6world");
}

#[cfg(test)]
fn test_char_1(i: u32) -> [u8;3]
{
	[1,i as u8, 0]
}

#[cfg(test)]
fn test_char_2(i: u32) -> [u8;4]
{
	[1,0xC0 | (i >> 6) as u8, 0x80 | (i & 63) as u8, 0]
}

#[cfg(test)]
fn test_char_3(i: u32) -> [u8;5]
{
	[1,0xE0 | (i >> 12) as u8, 0x80 | (i >> 6 & 63) as u8, 0x80 | (i & 63) as u8, 0]
}

#[cfg(test)]
fn test_char_4(i: u32) -> [u8;6]
{
	[1,0xF0 | (i >> 18) as u8, 0x80 | (i >> 12 & 63) as u8, 0x80 | (i >> 6 & 63) as u8, 0x80 | (i & 63) as u8, 0]
}

#[test]
fn test_valid_chars_windows()
{
	for i in 0..=127 {
		assert!(__check_valid_filename_windows(&test_char_1(i)));
		assert!(!__check_valid_filename_windows(&test_char_2(i)));
		assert!(!__check_valid_filename_windows(&test_char_3(i)));
		assert!(!__check_valid_filename_windows(&test_char_4(i)));
	}
	for i in 128..=2047 {
		assert!(__check_valid_filename_windows(&test_char_2(i)));
		assert!(!__check_valid_filename_windows(&test_char_3(i)));
		assert!(!__check_valid_filename_windows(&test_char_4(i)));
		assert!(!__check_valid_filename_windows(&test_char_2(i)[1..2]));
	}
	for i in 2048..=65535 {
		assert!(__check_valid_filename_windows(&test_char_3(i)));
		assert!(!__check_valid_filename_windows(&test_char_4(i)));
		assert!(!__check_valid_filename_windows(&test_char_3(i)[1..3]));
	}
	for i in 65536..=0x10FFFF {
		assert!(__check_valid_filename_windows(&test_char_4(i)));
		assert!(!__check_valid_filename_windows(&test_char_4(i)[1..4]));
	}
	assert!(!__check_valid_filename_windows(&test_char_4(0x110000)));
}
