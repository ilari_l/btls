use btls_aux_filename::Filename;
use std::ops::Range;
use std::str::from_utf8;

#[inline(never)]
fn do_bench(haystack: &str, needle: &str) -> Option<Range<usize>>
{
	let haystack = Filename::from_str(haystack);
	haystack.rfind(needle)
}

fn main()
{
	let n = [42;262144];
	let n = from_utf8(&n).expect("Not UTF-8???");
	for _ in 0..10000 { do_bench(n, "x"); }
}
