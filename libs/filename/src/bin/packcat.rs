use btls_aux_filename::Filename;
use std::env::args_os;
use std::fmt::Display;
use std::fs::File;
use std::io::ErrorKind as IoErrorKind;
use std::io::Read;
use std::io::stdout;
use std::io::Write;
use std::ops::Deref;
use std::os::unix::process::ExitStatusExt;
use std::path::Path;
use std::process::Command;
use std::process::exit;

#[derive(Copy,Clone,Debug)]
enum Compressed
{
	Gzip,
	Bzip2,
	Lzip,
	Lzma,
	Zstd,
	Xz,
	Brotli,
}

impl Compressed
{
	fn from_extension(ext: &str) -> Option<Compressed>
	{
		use self::Compressed::*;
		Some(match ext {
			"gz" => Gzip,
			"bz2" => Bzip2,
			"lz" => Lzip,
			"lzip" => Lzip,
			"lzma" => Lzma,
			"zst" => Zstd,
			"zstd" => Zstd,
			"xz" => Xz,
			"br" => Brotli,
			"brotli" => Brotli,
			_ => return None
		})
	}
	fn decompressor(&self) -> Vec<&'static str>
	{
		match self {
			&Compressed::Gzip => vec!["gzip","-d"],
			&Compressed::Bzip2 => vec!["bzip2","-d"],
			&Compressed::Lzip => vec!["lzip","-d"],
			&Compressed::Lzma => vec!["lzma","-d"],
			&Compressed::Zstd => vec!["zstd","-d"],
			&Compressed::Xz => vec!["xz","-d"],
			&Compressed::Brotli => vec!["brotli","-d"],
		}
	}
}

macro_rules! error
{
	($error:ident $($rest:tt)*) => {{
		*$error = true;
		eprintln!($($rest)*);
	}}
}

fn do_file(path: &Path, error: &mut bool)
{
	let path_disp = Filename::from_path(path);
	let basename = match path.file_name() {
		Some(bn) => Filename::from_os_str(bn),
		//This returns None for .., which is definitely a directory.
		None => return error!(error "{path_disp}: Is a directory")
	};
	if path.is_dir() {
		return error!(error "{path_disp}: Is a directory");
	} else if !path.is_file() {
		return error!(error "{path_disp}: Does not exist or not a regular file");
	}
	let mut cmd = Vec::new();
	if let Some(extension) = basename.extension_unicode() {
		if let Some(fmt) = Compressed::from_extension(extension) { cmd = fmt.decompressor(); }
	}
	match File::open(path) {
		Ok(fp) => run_decompressor(&cmd, fp, &path_disp, error),
		Err(err) => return error!(error"Error opening {path_disp}: {err}")
	};
}

fn run_decompressor(scmd: &[&str], mut file: File, fname: &impl Display, error: &mut bool)
{
	if scmd.len() == 0 {
		//Just show the file.
		let mut buf = [0;65536];
		loop {
			let buf = match file.read(&mut buf) {
				Ok(0) => break,			//EOF.
				Ok(amt) => &buf[..amt],		//Data.
				Err(err) => match err.kind() {
					IoErrorKind::Interrupted => &[],
					IoErrorKind::WouldBlock => &[],
					_ => return error!(error "Error reading {fname}: {err}")
				}
			};
			let mut itr = 0;
			while itr < buf.len() {
				let buf = &buf[itr..];
				match stdout().write(&buf) {
					Ok(amt) => itr += amt,
					Err(err) => match err.kind() {
						IoErrorKind::Interrupted => (),
						IoErrorKind::WouldBlock => (),
						_ => return error!(error "Error writing stdout: {err}")
					}
				}
			}
		}
	} else {
		let mut cmd = Command::new(scmd[0]);
		for arg in scmd.iter().skip(1) { cmd.arg(arg); }
		cmd.stdin(file);
		match cmd.status() {
			Ok(stat) => if stat.success() {
				//Ok.
			} else if let Some(code) = stat.code() {
				error!(error "Command {cmd:?}: Failed with code {code}!");
			} else if let Some(sig) = stat.signal() {
				//Do not show errors from signal 13, as it is SIGPIPE, which kills the processes
				//if our stdout gets closed.
				if sig != 13 {
					error!(error "Command {cmd:?}: Failed with signal {sig}!");
				}
			} else {
				error!(error "Command {cmd:?}: Failed with unknown error!");
			}
			Err(err) => return error!(error "Error executing {cmd:?}: {err}")
		}
	}
}


fn main()
{
	let args = args_os();
	let mut error = false;
	for arg in args.skip(1) { do_file(Path::new(arg.deref()), &mut error); }
	if error { exit(1); }	//Exit unsuccessfully.
}
