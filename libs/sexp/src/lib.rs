//!Canonical S-Expression parser.
//!
//!This crate provodes a parser for canonical S-Expressions.
//!
//!Currently, display hints are not supported.
//!
//!The most important items are:
//!
//! * [`SexprParseStream`](struct.SexprParseStream.html), which parses S-Expressions.
#![forbid(unsafe_code)]
#![forbid(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]

use btls_aux_fail::dtry;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use core::str::from_utf8;

///A canonical S-Expression online parser.
///
///This structure is created using the [`new()`](#method.new) method.
#[derive(Copy,Clone, Debug)]
pub struct SexprParseStream<'a>
{
	data: &'a [u8],
	ptr: usize,
	depth: usize,
	root_status: u8,
}

///A token in S-Expression.
#[derive(Copy,Clone, PartialEq, Eq, Debug)]
pub enum SexprNode<'a>
{
	///Start of list token.
	StartList,
	///End of list token.
	EndList,
	///Raw octet string token. The argument is the raw token value.
	Data(&'a [u8]),
}

impl<'a> SexprParseStream<'a>
{
	fn next_token(&mut self) -> Result<SexprNode<'a>, bool>
	{
		fail_if!(self.ptr == self.data.len(), false);
		fail_if!(self.root_status == 2, true);
		let ntype = *self.data.get(self.ptr).ok_or(true)?;
		if ntype == 40 { //Start of list.
			if self.root_status == 0 { self.root_status = 1; }
			self.ptr = self.ptr.checked_add(1).ok_or(true)?;
			self.depth = self.depth.checked_add(1).ok_or(true)?;
			return Ok(SexprNode::StartList);
		} else if ntype == 41 { //End of list.
			if self.depth == 1 && self.root_status == 1 { self.root_status = 2; }
			self.ptr = self.ptr.checked_add(1).ok_or(true)?;
			self.depth = self.depth.checked_sub(1).ok_or(true)?;
			return Ok(SexprNode::EndList);
		} else if ntype >= 48 && ntype <= 57 { //Data.
			if self.depth == 0 { self.root_status = 2; }
			let mut len = 0usize;
			let mut nptr = self.ptr;
			loop {
				fail_if!(nptr == self.data.len(), true);
				let ch = *self.data.get(nptr).ok_or(true)?;
				if ch == 58 { break; }	//End on :
				fail_if!(ch < 48 || ch > 57, true);	//Only numbers and : valid.
				let dch = (ch - 48) as usize;
				len = len.checked_mul(10).ok_or(true)?.checked_add(dch).ok_or(true)?;
				nptr = nptr.checked_add(1).ok_or(true)?;
			}
			//Zero leading is only valid for '0'.
			fail_if!(ntype == 48 && nptr.saturating_sub(self.ptr) != 1, true);
			//Advance to start of data.
			nptr = nptr.checked_add(1).ok_or(true)?;
			let eptr = nptr.checked_add(len).ok_or(true)?;
			let data = self.data.get(nptr..eptr).ok_or(true)?;
			self.ptr = eptr;
			return Ok(SexprNode::Data(data))
		} else {
			fail!(true);
		}
	}
}

impl<'a> Iterator for SexprParseStream<'a>
{
	type Item = Result<SexprNode<'a>, ()>;
	fn next(&mut self) -> Option<Result<SexprNode<'a>, ()>>
	{
		match self.next_token() {
			Ok(x) => Some(Ok(x)),
			Err(true) => Some(Err(())),
			Err(false) => None
		}
	}
}

impl<'a> SexprParseStream<'a>
{
	///Create a new S-Expression parse stream parsing canonical S-Expression `data`.
	pub fn new(data: &'a [u8]) -> SexprParseStream<'a>
	{
		SexprParseStream {
			data: data,
			ptr: 0,
			depth: 0,
			root_status: 0
		}
	}
	///Read start of list, and the first item in the list as octet string.
	///
	///If the next two tokens are start-of-list, octets-`token`, returns `Ok(token)`. Otherwise returns
	//`Err(())`.
	pub fn read_typed(&mut self) -> Result<&'a [u8], ()>
	{
		fail_if!(self.next().unwrap_or(Err(()))? != SexprNode::StartList, ());
		let ltype = if let SexprNode::Data(x) = self.next().unwrap_or(Err(()))? { x } else { fail!(()); };
		Ok(ltype)
	}
	///Read start of list, and the first item in the list as text string.
	///
	///This differs from the [`read_typed()`](#method.read_typed) in that the read token is translated into
	///UTF-8.
	///
	///If the next two tokens are start-of-list, string-`token`, returns `Ok(token)`. Otherwise returns
	//`Err(())`.
	pub fn read_typed_str(&mut self) -> Result<&'a str, ()>
	{
		dtry!(NORET from_utf8(self.read_typed()?))
	}
	///Read a raw octet string.
	///
	///On success, returns `Ok(token)`, where `token` is the octet string read. If the token is not a octet
	///string, returns `Err(())`.
	pub fn next_data(&mut self) -> Result<&'a [u8], ()>
	{
		match self.next().unwrap_or(Err(())) { Ok(SexprNode::Data(x)) => Ok(x), _ => fail!(()) }
	}
	///Read end of list.
	///
	///If the next token is end of list, returns `Ok(())`. Otherwise returns `Err(())`.
	pub fn assert_eol(&mut self) -> Result<(), ()>
	{
		match self.next().unwrap_or(Err(())) { Ok(SexprNode::EndList) => Ok(()), _ => fail!(()) }
	}
	///Read a text string.
	///
	///This differs from [`next_data()`](#method.next_data) in that the octet string is translated to UTF-8.
	///
	///On success, returns `Ok(token)`, where `token` is the text string read. If the token is not a text
	///string, returns `Err(())`.
	pub fn next_str(&mut self) -> Result<&'a str, ()>
	{
		dtry!(NORET from_utf8(self.next_data()?))
	}
	///Read a list containing a pair of raw octet strings.
	///
	///This method reads four tokens from the stream. The first is start of list, the two middle ones
	///are octet strings and the last one is end of list.
	///
	///On success, returns `Ok(str1, str2)`, where `str1` is the first octet string read, and `str2` is the
	///second octet string read. Otherwise returns `Err(())`.
	pub fn read_pair(&mut self) -> Result<(&'a [u8], &'a [u8]), ()>
	{
		if self.next().unwrap_or(Err(()))? != SexprNode::StartList { fail!(()); }
		let first = if let SexprNode::Data(x) = self.next().unwrap_or(Err(()))? { x } else { fail!(()); };
		let second = if let SexprNode::Data(x) = self.next().unwrap_or(Err(()))? { x } else { fail!(()); };
		if self.next().unwrap_or(Err(()))? != SexprNode::EndList { fail!(()); }
		Ok((first, second))
	}
	///Read a list containing a pair of text and raw octet strings.
	///
	///This method is the same as [`read_pair()`](#method.read_pair), but instead of returning the first string
	///as octet string, the method returns the string as UTF-8 text.
	///
	///On success, returns `Ok(str1, str2)`, where `str1` is the first text string read, and `str2` is the
	///second text string read. Otherwise returns `Err(())`.
	pub fn read_pair_strn(&mut self) -> Result<(&'a str, &'a [u8]), ()>
	{
		self.read_pair().and_then(|(x,y)|Ok((dtry!(from_utf8(x)), y)))
	}
	///Read a list containing a pair of text  strings.
	///
	///This method is the same as [`read_pair()`](#method.read_pair), but instead of returning the strings
	///as octet strings, the method returns the strings as UTF-8 text.
	///
	///On success, returns `Ok(str1, str2)`, where `str1` is the first text string read, and `str2` is the
	///second text string read. Otherwise returns `Err(())`.
	pub fn read_pair_str(&mut self) -> Result<(&'a str, &'a str), ()>
	{
		self.read_pair().and_then(|(x,y)|Ok((dtry!(from_utf8(x)), dtry!(from_utf8(y)))))
	}
}

#[cfg(test)]
mod test;
