use super::*;

#[test]
fn test_read_typed()
{
	let input = "(3:foo4:quux(6:barqux3:zot))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.read_typed(), Ok(&b"foo"[..]));
	assert_eq!(strm.next_data(), Ok(&b"quux"[..]));
	assert_eq!(strm.read_typed(), Ok(&b"barqux"[..]));
	assert_eq!(strm.next_data(), Ok(&b"zot"[..]));
	strm.assert_eol().unwrap();
	strm.assert_eol().unwrap();
	assert_eq!(strm.next(), None);
}

#[test]
fn test_read_typed_str()
{
	let input = "(3:foo4:quux(6:barqux3:zot))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.read_typed_str(), Ok("foo"));
	assert_eq!(strm.next_str(), Ok("quux"));
	assert_eq!(strm.read_typed_str(), Ok("barqux"));
	assert_eq!(strm.next_str(), Ok("zot"));
	strm.assert_eol().unwrap();
	strm.assert_eol().unwrap();
	assert_eq!(strm.next(), None);
}

#[test]
fn test_read_pair()
{
	let input = "((3:foo6:barqux)(4:quux3:zot))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair(), Ok((&b"foo"[..], &b"barqux"[..])));
	assert_eq!(strm.read_pair(), Ok((&b"quux"[..], &b"zot"[..])));
	assert_eq!(strm.next(), Some(Ok(SexprNode::EndList)));
	assert_eq!(strm.next(), None);
}

#[test]
fn test_read_pair_strn()
{
	let input = "((3:foo6:barqux)(4:quux3:zot))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair_strn(), Ok(("foo", &b"barqux"[..])));
	assert_eq!(strm.read_pair_strn(), Ok(("quux", &b"zot"[..])));
	assert_eq!(strm.next(), Some(Ok(SexprNode::EndList)));
	assert_eq!(strm.next(), None);
}

#[test]
fn test_read_pair_str()
{
	let input = "((3:foo6:barqux)(4:quux3:zot))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair_str(), Ok(("foo", "barqux")));
	assert_eq!(strm.read_pair_str(), Ok(("quux", "zot")));
	assert_eq!(strm.next(), Some(Ok(SexprNode::EndList)));
	assert_eq!(strm.next(), None);
}

#[test]
fn test_read_pair_1elem()
{
	let input = "((3:foo))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair(), Err(()));
}

#[test]
fn test_read_pair_2elem()
{
	let input = "((3:foo6:barqux))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair(), Ok((&b"foo"[..], &b"barqux"[..])));
}

#[test]
fn test_read_pair_3elem()
{
	let input = "((3:foo6:barqux3:zot))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair(), Err(()));
}

#[test]
fn test_read_pair_overlen1()
{
	let input = "((32:foo6:barqux))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair(), Err(()));
}


#[test]
fn test_read_pair_overlen2()
{
	let input = "((3:foo66:barqux))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair(), Err(()));
}
