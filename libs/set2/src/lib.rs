#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]
use btls_aux_fail::f_return;
pub use core::fmt::Debug as __RustBuiltinDebug;
pub use core::fmt::Error as __RustBuiltinFmtError;
pub use core::fmt::Formatter as __RustBuiltinFormatter;
use core::marker::PhantomData;
use core::mem::replace;
//use core::mem::size_of;

///Cast key.
///
///In the Keycast/Upcast/Downcast hierarchy, this is the weakest requirement: Ability to cast a key, to possibly
///an element of set. Any type implementing either Upcast or Downcast automatically implements Keycast.
pub trait Keycast<Key>: Sized
{
	///keycast an item.
	///
	///This method may fail and return `None` if the key does not specify a valid instance of set element
	///type. For any type implementing Upcast, this method is guaranteed to never fail.
	fn __keycast(item: &Key) -> Option<Self>;
}

///Upcast item.
///
///In the Keycast/Upcast/Downcast hierarchy, this is the middle requirement: Ability to always cast a key to an
///element of set. Any type implementing Downcast automatically implements Upcast (and in turn Keycast). Similarly
///any type implementing this trait automatically also implements Keycast.
///
///Note that a type may legimately be Upcast but not Downcast. For example in case where there is some additional
///fields in source type that can not be reconstructed from base.
pub trait Upcast<Source>: Keycast<Source>
{
	///Upcast an item.
	///
	///Unlike key cast, upcasts are guaranteed to succeed.
	fn __upcast(item: &Source) -> Self;
}

impl<T,U:Upcast<T>> Keycast<T> for U
{
	fn __keycast(item: &T) -> Option<Self> { Some(U::__upcast(item)) }
}

///Downcast item.
///
///In the Keycast/Upcast/Downcast hierarchy, this is the strongest requirement: Ability to possibly cast a key
///to its subtype. It impiles reverse upcast, as subtypes can be casted to their supertypes. Any type implementing
///this trait automatically implements both Upcast and Keycast.
pub trait Downcast<Target>: Upcast<Target>
{
	///Upcast an item.
	///
	///Unlike key cast, upcasts are guaranteed to succeed.
	fn __upcast(item: &Target) -> Self;
	///Downcast an item.
	///
	///If the set item is not in subtype, this fails and returns `None`.
	fn __downcast(&self) -> Option<Target>;
}

impl<T:Clone> Downcast<T> for T
{
	fn __upcast(item: &T) -> T { item.clone() }
	fn __downcast(&self) -> Option<T> { Some(self.clone()) }
}

impl<T,U:Downcast<T>> Upcast<T> for U
{
	fn __upcast(item: &T) -> Self { Downcast::__upcast(item) }
}

mod __sealed
{
	pub struct __Sealed;
}

///Types that can be used for set storage.
///
///This trait is sealed. It is implemented exactly for:
///
/// * u8, u16, u32, u64 and u128.
/// * Arrays of u8, u16, u32, u64 and u128.
pub trait SetStorage: Copy
{
	fn bits() -> usize;
	fn new() -> Self;
	fn clear(&mut self, b: usize);
	fn set(&mut self, b: usize);
	fn complement(&mut self, b: usize);
	fn test(&self, b: usize) -> bool;
	fn test_and_clear(&mut self, b: usize) -> bool
	{
		let r = self.test(b);
		self.clear(b);
		r
	}
	fn test_and_set(&mut self, b: usize) -> bool
	{
		let r = self.test(b);
		self.set(b);
		r
	}
	fn test_and_complement(&mut self, b: usize) -> bool
	{
		let r = self.test(b);
		self.complement(b);
		r
	}
	fn xbitor(a: &Self, b: &Self) -> Self;
	fn xbitand(a: &Self, b: &Self) -> Self;
	fn xbitxor(a: &Self, b: &Self) -> Self;
	fn xbitsub(a: &Self, b: &Self) -> Self;
	fn xshl(a: &Self, b: usize) -> Self;
	fn hamming_weight(&self) -> u32;
	fn is_nonzero(&self) -> bool;
	fn to_u64(&self) -> u64;
	fn from_u64(m: u64) -> Self;
	unsafe fn __sealed(_: __sealed::__Sealed);
}

macro_rules! splitbits
{
	($b:ident, $N:ident, $bits:tt, $val:expr) => {{
		let w = $b / $bits;
		let b = $b % $bits;
		if w >= $N { return $val; }
		(w, b)
	}}
}

macro_rules! bitop
{
	($selfx:ident, $o:ident, $N:ident, $opty:ident, $opfn:ident) => {{
		let mut res: Self = <Self as SetStorage>::new();
		for i in 0..$N { res[i] = core::ops::$opty::$opfn($selfx[i], $o[i]); }
		res
	}}
}

macro_rules! impl_set_storage
{
	($xtype:ident, $bits:tt) => {
		impl SetStorage for $xtype
		{
			fn bits() -> usize { $bits }
			fn new() -> Self { 0 }
			fn clear(&mut self, b: usize)
			{
				if b >= $bits { return; }
				*self &= !(1 << b);
			}
			fn set(&mut self, b: usize)
			{
				if b >= $bits { return; }
				*self |= 1 << b;
			}
			fn complement(&mut self, b: usize)
			{
				if b >= $bits { return; }
				*self ^= 1 << b;
			}
			fn test(&self, b: usize) -> bool
			{
				if b >= $bits { return false; }
				*self >> b & 1 != 0
			}
			fn xbitor(a: &Self, b: &Self) -> Self { *a | *b }
			fn xbitand(a: &Self, b: &Self) -> Self { *a & *b }
			fn xbitxor(a: &Self, b: &Self) -> Self { *a ^ *b }
			fn xbitsub(a: &Self, b: &Self) -> Self { *a & !*b }
			fn xshl(a: &Self, b: usize) -> Self
			{
				//Do not modulo the shift amount.
				if b < $bits { *a << b } else { 0 }
			}
			fn hamming_weight(&self) -> u32 { self.count_ones() }
			fn is_nonzero(&self) -> bool { *self != 0}
			fn to_u64(&self) -> u64 { *self as u64 }
			fn from_u64(m: u64) -> Self { m as $xtype }
			unsafe fn __sealed(_: __sealed::__Sealed) {}
		}
		impl<const N:usize> SetStorage for [$xtype;N]
		{
			fn bits() -> usize { $bits * N }
			fn new() -> Self { [0;N] }
			fn clear(&mut self, b: usize)
			{
				let (w,b) = splitbits!(b, N, $bits, ());
				self[w] &= !(1 << b);
			}
			fn set(&mut self, b: usize)
			{
				let (w,b) = splitbits!(b, N, $bits, ());
				self[w] |= 1 << b;
			}
			fn complement(&mut self, b: usize)
			{
				let (w,b) = splitbits!(b, N, $bits, ());
				self[w] |= 1 << b;
			}
			fn test(&self, b: usize) -> bool
			{
				let (w,b) = splitbits!(b, N, $bits, false);
				self[w] >> b & 1 != 0
			}
			fn xbitor(a: &Self, b: &Self) -> Self { bitop!(a, b, N, BitOr, bitor) }
			fn xbitand(a: &Self, b: &Self) -> Self { bitop!(a, b, N, BitAnd, bitand) }
			fn xbitxor(a: &Self, b: &Self) -> Self { bitop!(a, b, N, BitXor, bitxor) }
			fn xbitsub(a: &Self, b: &Self) -> Self
			{
				let mut res: Self = <Self as SetStorage>::new();
				for i in 0..N { res[i] = a[i] & !b[i]; }
				res
			}
			fn xshl(a: &Self, b: usize) -> Self
			{
				let mut res: Self = <Self as SetStorage>::new();
				if b == 0 {
					res.copy_from_slice(a);
				} else if b >= $bits * N {
					//Overflows to 0.
				} else if b % $bits == 0 {
					//Optimized whole-word shift.
					let s = b / $bits;
					for i in s..N { res[i-s] = a[i]; }
				} else {
					let s = b / $bits;	//s < N !
					let t = b % $bits;	//t != 0 !
					for i in s..N-1 {	//N > 0 !
						res[i-s] |= a[i] << t;
						res[i-s] |= a[i+1] >> $bits - t;
					}
					res[N-1-s] |= a[N-1] << t;
				}
				res
			}
			fn hamming_weight(&self) -> u32 { self.iter().fold(0,|x,&y|x+y.count_ones()) }
			fn is_nonzero(&self) -> bool { self.iter().any(|&x|x!=0) }
			fn to_u64(&self) -> u64
			{
				let mut sh = 0;
				let mut m = 0;
				for i in 0..N {
					m |= (self[i] as u64) << sh;
					sh += $bits;
					if sh >= 64 { break; }
				}
				m
			}
			fn from_u64(m: u64) -> Self
			{
				//These all can be zero-init.
				let mut res: Self = <Self as SetStorage>::new();
				let mut sh = 0;
				for i in 0..N {
					res[i] = (m >> sh) as $xtype;
					sh += $bits;
					if sh >= 64 { break; }
				}
				res
			}
			unsafe fn __sealed(_: __sealed::__Sealed) {}
		}
	}
}

impl_set_storage!(u8, 8);
impl_set_storage!(u16, 16);
impl_set_storage!(u32, 32);
impl_set_storage!(u64, 64);
impl_set_storage!(u128, 128);


///Type of set item.
pub trait SetItem: __RustBuiltinDebug+Sized+Clone+'static
{
	///Storage type.
	type Storage: SetStorage;
	///Get number of sign bit for item.
	fn __sign_bit(&self) -> usize;
	///Get the universe of set.
	fn __universe() -> &'static [Self];
	///Get the bitset with all possible bits set.
	fn __universe_bits() -> <Self as SetItem>::Storage;
	///Get sign bit number by internal name.
	fn __by_internal_name(name: &str) -> Option<usize>;
}

///Create a set from raw storage.
///
///This is a macro because trait bounds on const fn are unstable.
#[macro_export]
macro_rules! set_from_raw_storage
{
	($x:expr) => {
		$crate::SetOf {
			storage: $x
		}
	}
}

///Set of T.
#[derive(Copy,Clone)]
pub struct SetOf<T:SetItem>
{
	//This is public because trait bounds on const fn are unstable.
	#[doc(hidden)]
	pub storage: <T as SetItem>::Storage,
}

impl<T:SetItem> SetOf<T>
{
	///Create empty set.
	pub fn empty_set() -> SetOf<T>
	{
		SetOf {
			storage: <T as SetItem>::Storage::new(),
		}
	}
	///Create new singleton set.
	pub fn singleton<S>(item: S) -> SetOf<T> where T: Upcast<S>
	{
		let item = <T as Upcast<S>>::__upcast(&item);
		let mut s = Self::empty_set();
		s.storage.set(item.__sign_bit());
		s
	}
	///Create new singleton set by name.
	pub fn singleton_by_name(name: &str) -> Option<SetOf<T>>
	{
		let bit = <T as SetItem>::__by_internal_name(name)?;
		let mut s = Self::empty_set();
		s.storage.set(bit);
		Some(s)
	}
	///Create from iterator.
	pub fn from_iterator<S,I:Iterator<Item=S>>(iter: I) -> Self where T: Upcast<S>
	{
		let mut t = <T as SetItem>::Storage::new();
		for item in iter {
			let casted_item = <T as Upcast<S>>::__upcast(&item);
			t.set(casted_item.__sign_bit());
		}
		SetOf {
			storage: t,
		}
	}
	///Create from mask.
	pub fn from_raw_mask(mask: u64) -> SetOf<T>
	{
		Self::from_raw_mask_offset(mask, 0)
	}
	///Create from mask, with offset.
	pub fn from_raw_mask_offset(mask: u64, offset: u32) -> SetOf<T>
	{
		let mask = <T as SetItem>::Storage::from_u64(mask);
		let mask = <T as SetItem>::Storage::xshl(&mask, offset as usize);
		//Ensure there are no weird set bits.
		let mask = <T as SetItem>::Storage::xbitand(&mask, &<T as SetItem>::__universe_bits());
		SetOf {
			storage: mask,
		}
	}
	///Is member of set?
	///
	///If keycast fails, item is reported to not be in the set.
	pub fn is_in<S>(&self, item: &S) -> bool where T: Keycast<S>
	{
		let item = f_return!(<T as Keycast<S>>::__keycast(item), false);
		self.storage.test(item.__sign_bit())
	}
	///Like `is_in()`, but takes value instead of reference.
	pub fn is_in2<S:Sized>(&self, item: S) -> bool where T: Keycast<S> { self.is_in(&item) }
	///Add a member to set.
	pub fn add<S>(&mut self, item: &S) where T: Upcast<S>
	{
		let item = <T as Upcast<S>>::__upcast(item);
		self.storage.set(item.__sign_bit());
	}
	///Lookup member.
	///
	///This differs from `is_in()` in that on sucess the matching item is returned, and on failure, `None`
	///is returned.
	pub fn lookup<S>(&self, item: &S) -> Option<T> where T: Keycast<S>
	{
		if let Some(item) = <T as Keycast<S>>::__keycast(item) {
			if self.storage.test(item.__sign_bit()) { return Some(item); }
		}
		None
	}
	///Count number of elements.
	pub fn count(&self) -> u32 { self.storage.hamming_weight() }
	///Has elements?
	pub fn has_elements(&self) -> bool { self.storage.is_nonzero() }
	///Get iterator over items.
	///
	///This performs automatic downcast and filtering on items before returning.
	///If this downcast fails, the item is omitted.
	///
	///Due to Rust limitations, the return type is exposed in full instead of being just
	///impl Iterator<Item=V>+'a. One should not assume anything about the exact return type, other than that
	///it implements Iterator<Item=V>, and Clone, and lives for 'a.
	pub fn iter<'a,V>(&'a self) -> __DowncastIterator<'a, V, T> where T: Downcast<V>
	{
		__DowncastIterator(<T as SetItem>::__universe().iter(), self, PhantomData)
	}
	///Transform into raw mask.
	pub fn to_raw_mask(self) -> u64 { self.storage.to_u64() }
	///Raw test and remove of element described by bit b.
	pub fn raw_test_and_remove(&mut self, b: usize) -> bool { self.storage.test_and_clear(b) }
}

impl<T:SetItem> __RustBuiltinDebug for SetOf<T>
{
	fn fmt(&self, f: &mut __RustBuiltinFormatter) -> Result<(), __RustBuiltinFmtError>
	{
		let t = &self.storage;
		let mut first = true;
		f.write_str("{")?;
		for alg in <T as SetItem>::__universe().iter() { if t.test(alg.__sign_bit()) {
			if !replace(&mut first, false) { f.write_str(", ")?; }
			__RustBuiltinDebug::fmt(alg, f)?;
		}}
		f.write_str("}")
	}
}

impl<T:SetItem> core::ops::BitAnd<SetOf<T>> for SetOf<T>
{
	type Output = Self;
	fn bitand(self, other: Self) -> Self
	{
		SetOf {
			storage: T::Storage::xbitand(&self.storage, &other.storage),
		}
	}
}

impl<T:SetItem> core::ops::BitAndAssign<SetOf<T>> for SetOf<T>
{
	fn bitand_assign(&mut self, other: Self)
	{
		self.storage = T::Storage::xbitand(&self.storage, &other.storage);
	}
}

impl<T:SetItem> core::ops::BitOr<SetOf<T>> for SetOf<T>
{
	type Output = Self;
	fn bitor(self, other: Self) -> Self
	{
		SetOf {
			storage: T::Storage::xbitor(&self.storage, &other.storage),
		}
	}
}

impl<T:SetItem> core::ops::BitOrAssign<SetOf<T>> for SetOf<T>
{
	fn bitor_assign(&mut self, other: Self)
	{
		self.storage = T::Storage::xbitor(&self.storage, &other.storage);
	}
}

impl<T:SetItem> core::ops::BitXor<SetOf<T>> for SetOf<T>
{
	type Output = Self;
	fn bitxor(self, other: Self) -> Self
	{
		SetOf {
			storage: T::Storage::xbitxor(&self.storage, &other.storage),
		}
	}
}

impl<T:SetItem> core::ops::BitXorAssign<SetOf<T>> for SetOf<T>
{
	fn bitxor_assign(&mut self, other: Self)
	{
		self.storage = T::Storage::xbitxor(&self.storage, &other.storage);
	}
}

impl<T:SetItem> core::ops::Sub<SetOf<T>> for SetOf<T>
{
	type Output = Self;
	fn sub(self, other: Self) -> Self
	{
		SetOf {
			storage: T::Storage::xbitsub(&self.storage, &other.storage),
		}
	}
}

impl<T:SetItem> core::ops::SubAssign<SetOf<T>> for SetOf<T>
{
	fn sub_assign(&mut self, other: Self)
	{
		self.storage = T::Storage::xbitsub(&self.storage, &other.storage);
	}
}

impl<T:SetItem> core::ops::Not for SetOf<T>
{
	type Output = Self;
	fn not(self) -> Self
	{
		SetOf {
			storage: T::Storage::xbitsub(&T::__universe_bits(), &self.storage),
		}
	}
}

impl<T:SetItem> core::ops::BitAnd<Option<SetOf<T>>> for SetOf<T>
{
	type Output = Self;
	fn bitand(self, o: Option<SetOf<T>>) -> Self
	{
		if let Some(o) = o { self & o } else { self }
	}
}

#[doc(hidden)]
pub struct __DowncastIterator<'a,V,T:Downcast<V>+SetItem>(core::slice::Iter<'a,T>, &'a SetOf<T>, PhantomData<V>);

//Manually impl Clone in order to get proper bounds (e.g., V does not have to be clone).
impl<'a,V,T:Downcast<V>+SetItem> Clone for __DowncastIterator<'a,V,T>
{
	fn clone(&self) -> Self
	{
		__DowncastIterator(self.0.clone(), self.1, PhantomData)
	}
}

impl<'a,V,T:Downcast<V>+SetItem> Iterator for __DowncastIterator<'a,V,T>
{
	type Item=V;
	fn next(&mut self) -> Option<V>
	{
		let t = &self.1.storage;
		loop {
			let item = self.0.next()?;
			if t.test(item.__sign_bit()) { if let Some(item) = <T as Downcast<V>>::__downcast(&item) {
				return Some(item);
			}}
		}
	}
}

#[cfg(test)]
mod test;
