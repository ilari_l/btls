use super::*;

#[test]
fn shift_int_zero()
{
	assert_eq!(SetStorage::xshl(&0x42u32, 0), 0x42u32);
}

#[test]
fn shift_int()
{
	assert_eq!(SetStorage::xshl(&0x42u32, 4), 0x420u32);
}

#[test]
fn shift_int_overflow()
{
	assert_eq!(SetStorage::xshl(&0x1u32, 32), 0);
}

#[test]
fn shift_array_zero()
{
	assert_eq!(SetStorage::xshl(&[2u8, 1u8, 42u8], 0), [2u8, 1u8, 42u8]);
}

#[test]
fn shift_array_whole()
{
	assert_eq!(SetStorage::xshl(&[2u8, 1u8, 42u8], 16), [42u8, 0u8, 0u8]);
}

#[test]
fn shift_array_fractional()
{
	assert_eq!(SetStorage::xshl(&[2u8, 1u8, 42u8], 11), [9u8, 80u8, 0u8]);
}

#[test]
fn shift_array_overflow()
{
	assert_eq!(SetStorage::xshl(&[0xFFu8, 0xFFu8, 0xFFu8], 24), [0u8, 0u8, 0u8]);
}
