extern crate btls_aux_serialization;
use btls_aux_codegen::BString;
use btls_aux_codegen::mkid;
use btls_aux_codegen::quote;
use btls_aux_codegen::qwrite;
use std::collections::HashMap;
use btls_aux_codegen::Symbol;
use std::collections::BTreeSet;
use std::env::var;
use std::mem::MaybeUninit;
use std::mem::transmute;
use std::path::Path;
use std::str::FromStr;


struct StateCodeMapping
{
	code_to_state: HashMap<u64, u8>,
	state_to_code: Vec<u64>,
}

impl StateCodeMapping
{
	fn new() -> StateCodeMapping
	{
		let mut r = StateCodeMapping {
			code_to_state: HashMap::new(),
			state_to_code: Vec::new(),
		};
		//Due to how code works, the first 19 states have to be certain way.
		static MAGIC_STATES: [u64;19] = [2,4,5,6,8,9,10,11,12,13,14,1,3,7,15,31,63,127,255];
		for &code in MAGIC_STATES.iter() { r.register(code); }
		r
	}
	fn register(&mut self, code: u64)
	{
		if self.code_to_state.contains_key(&code) { return; }
		//Not found, add.
		assert!(self.code_to_state.len() < 256);
		let state = self.state_to_code.len() as u8;
		self.code_to_state.insert(code, state);
		self.state_to_code.push(code);
	}
	fn enumerate_states<'a>(&'a self) -> impl Iterator<Item=(usize, u64)>+'a
	{
		self.state_to_code.iter().copied().enumerate()
	}
	fn get(&self, code: u64) -> Option<u8>
	{
		self.code_to_state.get(&code).copied()
	}
}

struct DumbHuffman
{
	table: [String;256],
	reverse: HashMap<u32, u8>,
	sc: StateCodeMapping,
}

impl DumbHuffman
{
	fn new() -> DumbHuffman
	{
		static HUFFMAN_LENGTHS: &'static str = include_str!("hpack-huffman2.txt");
		let mut sc = StateCodeMapping::new();
		let mut length_sym: Vec<u16> = Vec::new();
		let table: MaybeUninit<[MaybeUninit<String>;256]> = MaybeUninit::uninit();
		let mut table = unsafe{table.assume_init()};
		let mut reverse: HashMap<u32, u8> = HashMap::new();
		for line in HUFFMAN_LENGTHS.lines() {
			let len = u16::from_str(line).expect("Bad length");
			let sym = length_sym.len() as u16;
			assert!(len >= 4 && len < 32);
			length_sym.push(len * 256 + sym);
		}
		assert!(length_sym.len() == 256);
		//Sort length-sym first by length and then by symobl.
		length_sym.sort();
		let mut code = 0;
		let mut codelen = 0;
		for &t in length_sym.iter() {
			let symbol = t as usize % 256;
			let length = t as usize >> 8;
			while length > codelen {
				code <<= 1;
				codelen += 1;
			}
			let mut s = String::with_capacity(length);
			for i in 0..codelen {
				let v = code >> codelen - i - 1 & 1;
				s.push((48 + v as u8) as char);
			}
			table[symbol] = MaybeUninit::new(s);
			let pcode = code | 1 << codelen;
			reverse.insert(pcode, symbol as u8);
			//Register prefixes as states. Note that this loop needs to run in order of increasing
			//length.
			for i in (1..codelen).rev() { sc.register(pcode as u64 >> i); }
			code += 1;
			assert!(code < 1 << codelen);
		}
		assert!(codelen == 30);
		assert!(code == 0x3FFF_FFFF);
		//MaybeUninit<String> and String have the same layout.
		DumbHuffman { table: unsafe{transmute(table)}, reverse, sc }
	}
	fn __test_table(&self) -> [String;256]
	{
		self.table.clone()
	}
	fn __encode_table(&self) -> ([u8;256], [u32;256])
	{
		let mut length = [0;256];
		let mut code = [0;256];
		for (i,s) in self.table.iter().enumerate() {
			length[i] = s.len() as u8;
			code[i] = u32::from_str_radix(s, 2).expect("Bad binary");
		}
		(length, code)
	}
	fn __decode_table(&self) -> (u64, [u16;4096])
	{
		let mut transitions = [0xFFFF;4096];
		for (i, bcode) in self.sc.enumerate_states() {
			for j in 0..16 {
				let tnum = j * 256 + i;
				let fcode = bcode * 16 + j as u64;
				if let Some(state) = self.sc.get(fcode) {
					//If fcode is in states, this is no-emit transition.
					transitions[tnum] = 0xF00 + state as u16;
				} else {
					//fcode should be of form where there is a code, followed by some bits
					//(might be 0 bits).
					for k in 0..5 {
						let prefix = fcode >> k;
						let suffix = fcode & (1 << k) - 1 | 1 << k;
						//The prefix can be at most 31 bits long. However, the above code
						//can produce 33 bit codes.
						if prefix > 0xFFFF_FFFF { continue; }
						let prefix = prefix as u32;
						if let Some(&emit) = self.reverse.get(&prefix) {
							//Okay, emit this.
							let nstate = self.sc.get(suffix).expect("Bad suffix");
							transitions[tnum] = nstate as u16 * 256 + emit as u16;
							break;
						}
					}
				}
			}
		}
		//Fill the missing entries.
		let mut nbad = 0xF00;
		for i in 0..4096 {
			if transitions[i] == 0xFFFF {
				transitions[i] = nbad;
				nbad += 1;
			}
		}
		//Compute doubles magic constant.
		let mut magic = 0;
		for state in 0..15 {
			for psym in 0..8 {
				let tnum = 512 * psym + state;
				if transitions[tnum] < 0xF00 { magic += 1 << 4*state; }
			}
			//Promote 8 to F, just for style.
			if magic >> 4*state & 15 == 8 { magic += 7 << 4*state; }
		}
		assert!(magic == 0x6000fffffff7ff5);
		(magic, transitions)
	}
	fn __force_encode(&self, input: &[u8]) -> Vec<u8>
	{
		let mut x = String::new();
		for &b in input.iter() { x.push_str(&self.table[b as usize]); }
		while x.len() % 8 != 0 { x.push('1'); }
		let mut y = Vec::new();
		for i in 0..x.len()/8 {
			y.push(u8::from_str_radix(&x[8*i..8*i+8], 2).expect("Not binary?"));
		}
		y
	}
	fn force_hpack_header(&self, input: &[u8]) -> Vec<u8>
	{
		let hinput = self.__force_encode(input);
		let mut out = Vec::new();
		assert!(hinput.len() < 127);
		out.push(hinput.len() as u8 + 128);
		out.extend_from_slice(&hinput);
		out
	}
	fn shorter_with_header(&self, input: &[u8]) -> Vec<u8>
	{
		let hinput = self.__force_encode(input);
		let mut out = Vec::new();
		if hinput.len() < input.len() {
			assert!(hinput.len() < 127);
			out.push(hinput.len() as u8 + 128);
			out.extend_from_slice(&hinput);
		} else {
			assert!(input.len() < 127);
			out.push(input.len() as u8);
			out.extend_from_slice(&input);
		}
		out
	}
	fn decode(&self, input: &[u8]) -> Vec<u8>
	{
		assert!(input.len() > 0 && input.len() < 128);
		assert!(input.len() == input[0] as usize % 128 + 1);
		if input[0] < 128 {
			input[1..].to_vec()
		} else {
			let mut out = Vec::new();
			let mut acc = 1u32;
			for i in 1..input.len() {
				let mut b = input[i];
				for _ in 0..8 {
					acc = acc << 1 | b as u32 >> 7;
					b <<= 1;
					if let Some(&symbol) = self.reverse.get(&acc) {
						out.push(symbol);
						acc = 1;
					}
				}
			}
			assert!(matches!(acc, 1|3|7|15|31|63|127|255));
			out
		}
	}
}

fn stringcompressor_ok(huffman: &DumbHuffman, data: &[u8], reference: &[u8])
{
	//println!("stringcompressor_ok: data={data:?}, reference={reference:?}");
	let data2 = huffman.decode(data);
	assert_eq!(data2, reference);
}

static HTTP2_STATIC_TABLE: &[(&'static str, &'static str)] = &[
	(":authority", ""),
	(":method", "GET"),
	(":method", "POST"),
	(":path", "/"),
	(":path", "/index.html"),
	(":scheme", "http"),
	(":scheme", "https"),
	(":status", "200"),
	(":status", "204"),
	(":status", "206"),
	(":status", "304"),
	(":status", "400"),
	(":status", "404"),
	(":status", "500"),
	("accept-charset", ""),
	("accept-encoding", "gzip, deflate"),
	("accept-language", ""),
	("accept-ranges", ""),
	("accept", ""),
	("access-control-allow-origin", ""),
	("age", ""),
	("allow", ""),
	("authorization", ""),
	("cache-control", ""),
	("content-disposition", ""),
	("content-encoding", ""),
	("content-language", ""),
	("content-length", ""),
	("content-location", ""),
	("content-range", ""),
	("content-type", ""),
	("cookie", ""),
	("date", ""),
	("etag", ""),
	("expect", ""),
	("expires", ""),
	("from", ""),
	("host", ""),
	("if-match", ""),
	("if-modified-since", ""),
	("if-none-match", ""),
	("if-range", ""),
	("if-unmodified-since", ""),
	("last-modified", ""),
	("link", ""),
	("location", ""),
	("max-forwards", ""),
	("proxy-authenticate", ""),
	("proxy-authorization", ""),
	("range", ""),
	("referer", ""),
	("refresh", ""),
	("retry-after", ""),
	("server", ""),
	("set-cookie", ""),
	("strict-transport-security", ""),
	("transfer-encoding", ""),
	("user-agent", ""),
	("vary", ""),
	("via", ""),
	("www-authenticate", ""),
];

fn int_hash(data: &[u8]) -> u32
{
	let mut id = 0u32;
	for j in data.iter() { id = (id << 8).wrapping_add(id).wrapping_add(*j as u32); }
	id
}

struct Entry
{
	uid: u32,
	cid: u32,
	uncompressed: Vec<u8>,
	compressed: Vec<u8>,
	compressed_np: Vec<u8>,
	_packed: bool,
}

impl Entry
{
	fn uid(&self) -> u32 { self.uid }
	fn cid(&self) -> u32 { self.cid }
	fn uncompressed(&self) -> &[u8] { &self.uncompressed }
	fn compressed(&self) -> &[u8] { &self.compressed }
	fn compressed_np(&self) -> &[u8] { &self.compressed_np }
}

struct NEntry
{
	hash: u32,
	raw: Vec<u8>,
	symbol: Symbol,
}

fn sym_for_header(src: &str) -> Symbol
{
	let mut capitalize = true;
	let mut trx = String::new();
	for c in src.chars() {
		if c == ':' {
			//These are magic.
			trx.push_str("Http2MagicHeader");
		} else if c == '-' {
			capitalize = true;
		} else if c.is_ascii_alphabetic() {
			let c = if capitalize { c.to_ascii_uppercase() } else { c.to_ascii_lowercase() };
			trx.push(c);
			capitalize = false;
		} else if c.is_ascii_digit() {
			trx.push(c);
			capitalize = false;
		} else {
			panic!("Unsupported character '{c}' in header name");
		}
	}
	mkid(&trx)
}

struct HuffmanCacheEntry
{
	id: u32,
	from: BString,
	to: BString,
}

impl HuffmanCacheEntry
{
	fn id(&self) -> u32 { self.id }
	fn from(&self) -> BString { self.from.clone() }
	fn to(&self) -> BString { self.to.clone() }
}

fn build_huffman_cache(entries: &mut [Entry], id: fn(&Entry) -> u32, from: fn(&Entry) -> &[u8],
	to: fn(&Entry) -> &[u8]) -> Vec<HuffmanCacheEntry>
{
	entries.sort_by_key(id);	//Sort by id.
	entries.iter().map(|entry|{
		HuffmanCacheEntry {
			id: id(entry),
			from: BString(from(entry).to_vec()),
			to: BString(to(entry).to_vec()),
		}
	}).collect()
}

struct HeaderNameOnlyMapEntry
{
	name: BString,
	symbol: Symbol,
	number: u8,
}

impl HeaderNameOnlyMapEntry
{
	fn name(&self) -> BString { self.name.clone() }
	fn symbol(&self) -> Symbol { self.symbol.clone() }
	fn code(&self) -> u8 { self.number }
}

fn build_header_nameonly_map() -> Vec<HeaderNameOnlyMapEntry>
{
	let mut nomap: Vec<(&'static str, u8)> = Vec::new();
	let mut lastname = "";
	for (index, &(name, _)) in HTTP2_STATIC_TABLE.iter().enumerate() {
		//There are entries with the same name, skip over those.
		if name == lastname { continue; }
		nomap.push((name, index as u8 + 65));
		lastname = name;
	}
	nomap.iter().map(|&(name, number)|{
		HeaderNameOnlyMapEntry {
			name: BString(name.as_bytes().to_vec()),
			symbol: sym_for_header(name),
			number: number,
		}
	}).collect()
}

struct HeaderNameValueMapEntry
{
	name: BString,
	symbol: Symbol,
	value: BString,
	number: u8,
}

impl HeaderNameValueMapEntry
{
	fn name(&self) -> BString { self.name.clone() }
	fn symbol(&self) -> Symbol { self.symbol.clone() }
	fn value(&self) -> BString { self.value.clone() }
	fn code(&self) -> u8 { self.number }
}

fn build_header_name_value_map() -> Vec<HeaderNameValueMapEntry>
{
	let mut nvmap: Vec<(&'static str, &'static str, u8)> = Vec::new();
	for (index, &(name, value)) in HTTP2_STATIC_TABLE.iter().enumerate() {
		nvmap.push((name, value, index as u8 + 129));
	}
	nvmap.iter().map(|&(name,value,number)|{
		HeaderNameValueMapEntry {
			name: BString(name.as_bytes().to_vec()),
			symbol: sym_for_header(name),
			value: BString(value.as_bytes().to_vec()),
			number: number,
		}
	}).collect()
}

fn build_entries_map(huffman: &DumbHuffman, headers: &[&str]) -> Vec<Entry>
{
	let mut used = ::std::collections::HashMap::new();
	let mut usedc = ::std::collections::HashMap::new();
	let mut entries = Vec::new();
	for src in headers.iter() {
		let target = huffman.shorter_with_header(src.as_bytes());
		stringcompressor_ok(&huffman, &target, src.as_bytes());
		//TODO: This is not correct if target length can be above 127. But currently targets can not be
		//that long.
		assert!(target.len() < 128);
		let target_np = &target[1..];
		let id = int_hash(src.as_bytes());
		let cid = int_hash(target_np);
		if let Some(oitem) = used.get(&id) {
			eprintln!("Collision between '{oitem}' and '{src}' for id {id}");
			assert!(false);
		}
		if let Some(oitem) = usedc.get(&cid) {
			eprintln!("Collision between '{oitem}' and '{src}' for cid {cid}");
			assert!(false);
		}
		used.insert(id, src);
		usedc.insert(cid, src);
		let targete = target.to_vec();
		let targete_np = target_np.to_vec();
		let uncompressed = src.as_bytes().to_vec();
		entries.push(Entry {
			uid: id,
			cid: cid,
			uncompressed: uncompressed,
			compressed: targete,
			compressed_np: targete_np,
			_packed: target[0] & 128 > 0
		});
	}
	entries
}

struct HeaderNameEntry
{
	symbol: Symbol,
	raw: String,
	braw: BString,
	doc: String,
	id_num: u16,
	strlen: usize,
	hpack_enc: BString,
	http2_magic: bool,
}

impl HeaderNameEntry
{
	fn new(huffman: &DumbHuffman, src: &str, id_next: u16) -> HeaderNameEntry
	{
		let target = huffman.shorter_with_header(src.as_bytes());
		let trx = sym_for_header(src);
		HeaderNameEntry {
			hpack_enc: BString(target),
			symbol: trx.clone(),
			raw: src.to_string(),
			strlen: src.len(),
			braw: BString(src.as_bytes().to_vec()),
			id_num: id_next,
			doc: format!("HTTP Header `{src}`"),
			http2_magic: src.starts_with(":"),
		}
	}
	fn symbol(&self) -> Symbol { self.symbol.clone() }
	fn raw(&self) -> String { self.raw.clone() }
	fn braw(&self) -> BString { self.braw.clone() }
	fn doc(&self) -> String { self.doc.clone() }
	fn id_num(&self) -> u16 { self.id_num }
	fn strlen(&self) -> usize { self.strlen }
	fn hpack_enc(&self) -> BString { self.hpack_enc.clone() }
	fn http2_magic(&self) -> bool { self.http2_magic }
}

struct HeaderNameMap
{
	headers: Vec<HeaderNameEntry>,
	id_max: u16,
	x_forwarded: Vec<Symbol>,
	name_entries: Vec<NEntry>,
}

fn build_header_name_map(huffman: &DumbHuffman, headernames: &[&str]) -> HeaderNameMap
{
	let mut headers: Vec<HeaderNameEntry> = Vec::new();
	let mut id_max = 0u16;
	let mut id_next = 1u16;
	let mut x_forwarded = Vec::new();
	let mut name_entries = Vec::new();
	for src in headernames.iter() {
		let trx = sym_for_header(src);
		headers.push(HeaderNameEntry::new(huffman, src, id_next));
		if src.starts_with("x-forwarded-") { x_forwarded.push(trx.clone()); }
		id_max = id_next;
		id_next += 1;
		//Name cache.
		if src.len() < 127 {
			//Uncompressed.
			let mut raw = Vec::new();
			raw.push(src.len() as u8);
			raw.extend_from_slice(src.as_bytes());
			let hash = int_hash(&raw);
			let symbol = trx.clone();
			name_entries.push(NEntry{hash, raw, symbol});
			//Compressed. This needs to be forced encode!
			let packed = huffman.force_hpack_header(src.as_bytes());
			let raw = packed.clone();
			let hash = int_hash(&raw);
			let symbol = trx.clone();
			name_entries.push(NEntry{hash, raw, symbol});
		}
	}
	HeaderNameMap{headers, id_max, x_forwarded, name_entries}
}

struct NCacheMapEntry
{
	hash: u32,
	raw: BString,
	symbol: Symbol,
}

impl NCacheMapEntry
{
	fn hash(&self) -> u32 { self.hash }
	fn raw(&self) -> BString { self.raw.clone() }
	fn symbol(&self) -> Symbol { self.symbol.clone() }
}

fn build_ncache_map(name_entries: &mut [NEntry]) -> Vec<NCacheMapEntry>
{
	name_entries.sort_by_key(|k|k.hash);	//Sort by hash.
	let ret: Vec<_> = name_entries.iter().map(|i|{
		NCacheMapEntry {
			hash: i.hash,
			raw: BString(i.raw.clone()),
			symbol: i.symbol.clone(),
		}
	}).collect();
	//All the hashes should be different.
	for i in 1..ret.len() { assert!(ret[i-1].hash != ret[i].hash); }
	ret
}

struct StaticMapEntry
{
	number: usize,
	name: Symbol,
	value: BString,
}

impl StaticMapEntry
{
	fn number(&self) -> usize { self.number }
	fn name(&self) -> Symbol { self.name.clone() }
	fn value(&self) -> BString { self.value.clone() }
}

fn build_static_map() -> Vec<StaticMapEntry>
{
	HTTP2_STATIC_TABLE.iter().enumerate().map(|(i,&(name,value))|{
		StaticMapEntry {
			number: i+1,
			name: sym_for_header(name),
			value: BString(value.as_bytes().to_vec()),
		}
	}).collect()
}

fn append_file_contents(to: &mut String, name: &str)
{
	let mut seen2 = BTreeSet::new();
	let mut dups = false;
	let offset = to.lines().count();
	use ::std::io::Read as IR;
	::std::fs::File::open(name).and_then(|mut fp|{
		fp.read_to_string(to)
	}).unwrap();
	//Add linefeed if missing.
	if !to.ends_with("\n") { to.push_str("\n"); }
	for (num, line) in to.lines().enumerate() {
		if !line.starts_with("#") && line.len() > 0 && !seen2.insert(line) {
			eprintln!("Duplicate line '{line}' at {name}:{num}", num=num+1-offset);
			dups = true;
		} else if dups {
			eprintln!("Line {num} is OK", num=num+1-offset);
		}
	}
	if dups { panic!("Found duplicates"); }
}

fn main()
{
	let huffman = DumbHuffman::new();
	let mut content = String::new();
	append_file_contents(&mut content, "header-names-http.dat");
	append_file_contents(&mut content, "header-names-permanent.dat");
	append_file_contents(&mut content, "header-names-provisional.dat");
	append_file_contents(&mut content, "header-names-other.dat");
	//These are magical.
	content.push_str("\
:status
:method
:scheme
:path
:authority
:protocol
");
	let nlen = content.lines().count();
	append_file_contents(&mut content, "stdheaders.dat");
	let mut headers = Vec::new();
	let mut subname_len = 0;
	let mut seen = BTreeSet::new();
	for (n, line) in content.lines().enumerate() {
		if !line.starts_with("#") && line.len() > 0 && seen.insert(line) {
			headers.push(line);
			//If from header-names.dat, add 1 to subname count.
			if n < nlen { subname_len += 1; }
		}
	}

	let mut entries = build_entries_map(&huffman, &headers);
	let headers = build_header_name_map(&huffman, &headers[..subname_len]);
	let HeaderNameMap{headers, id_max, x_forwarded, mut name_entries} = headers;
	let huffcache_c = build_huffman_cache(&mut entries, Entry::uid, Entry::uncompressed, Entry::compressed);
	let huffcache_d = build_huffman_cache(&mut entries, Entry::cid, Entry::compressed_np, Entry::uncompressed);
	let hdr_names = build_header_nameonly_map();
	let hdr_values = build_header_name_value_map();
	let static_map = build_static_map();
	let ncache = build_ncache_map(&mut name_entries);


	let out_dir = var("OUT_DIR").unwrap();
	let dest_path = Path::new(&out_dir).join("autogenerated-huffman-cache.inc.rs");
	let mut fp = ::std::fs::File::create(&dest_path).unwrap();

	let (double_magic, decode_table) = huffman.__decode_table();
	let (encode_length, encode_table) = huffman.__encode_table();
	let test_table = huffman.__test_table();

	qwrite(&mut fp, quote!{
static HUFFMAN_CACHE: &'static [(u32,&'static [u8], &'static [u8])] = &[
	#([huffcache_c](#id, #from, #to),)*
];

static HUFFMAN_CACHE2: &'static [(u32,&'static [u8], &'static [u8])] = &[
	#([huffcache_d](#id, #from, #to),)*
];

static HUFFMAN_DECTAB2: [u16;4096] = [#(#decode_table),*];
static HUFFMAN_DECDBL_MAGIC2: u64 = #double_magic;
static HUFFMAN_ENCTAB3: [u32;256] = [#(#encode_table),*];
static HUFFMAN_LENTAB2: [u8;256] = [#(#encode_length),*];
#[cfg(test)] static HUFFMAN_TESTTAB2: [&'static str;256] = [#(#test_table),*];

static HEADER_NAME_CACHE: &'static [(u32,&'static [u8], StandardHttpHeader)] = &[
	#([ncache](#hash, #raw, StandardHttpHeader::#symbol)),*
];

const MAX_STANDARD_HEADER_ID: u16 = #id_max;

///Standard HTTP headers.
#[non_exhaustive]
#[derive(Copy,Clone,PartialEq,Eq,Debug)]
#[repr(u16)]
pub enum StandardHttpHeader
{
	#([headers] #[doc=#doc] #symbol = #id_num,)*
}

impl StandardHttpHeader
{
	///Construct from numeric id.
	///
	///It is guaranteed that id 0 is not valid.
	pub fn from_id(id: u16) -> Option<StandardHttpHeader>
	{
		if id == 0 || id > MAX_STANDARD_HEADER_ID { return None; }
		//The above guarantees the index is valid.
		Some(unsafe{core::mem::transmute(id)})
	}
	///Construct from raw header name.
	pub fn from_str(x: &str) -> Option<StandardHttpHeader> { Self::from(x.as_bytes()) }
	///Construct from raw header name.
	pub fn from(x: &[u8]) -> Option<StandardHttpHeader>
	{
		match x {
			#([headers] #braw => Some(StandardHttpHeader::#symbol),)*
			_ => None
		}
	}
	///Transform to string.
	pub fn to_str(self) -> &'static str
	{
		match self {
			#([headers] StandardHttpHeader::#symbol => #raw),*
		}
	}
	fn strlen(self) -> usize
	{
		match self {
			#([headers] StandardHttpHeader::#symbol => #strlen),*
		}
	}
	///Is HTTP/2 magic header.
	pub fn is_http2_magic(self) -> bool
	{
		match self {
			#([headers] StandardHttpHeader::#symbol => #http2_magic),*
		}
	}
	///Check if given header is dangerous, and should be filtered out from requests.
	pub fn is_dangerous_request(self) -> bool
	{
		match self {
			StandardHttpHeader::XSrcIp => true,
			StandardHttpHeader::XRealIp => true,
			StandardHttpHeader::ClientCert => true,
			StandardHttpHeader::ClientCertChain => true,
			#(StandardHttpHeader::#x_forwarded => true,)*
			_ => false,
		}
	}
	///Check if given header is dangerous, and should be filtered out from responses.
	pub fn is_dangerous_response(self) -> bool
	{
		match self {
			StandardHttpHeader::ExpectCt => true,
			StandardHttpHeader::PublicKeyPins => true,
			StandardHttpHeader::PublicKeyPinsReportOnly => true,
			StandardHttpHeader::StrictTransportSecurity => true,
			StandardHttpHeader::Nel => true,
			StandardHttpHeader::ReportTo => true,
			_ => false,
		}
	}
	fn hpack_encoding(self) -> &'static [u8]
	{
		match self {
			#([headers] StandardHttpHeader::#symbol => #hpack_enc),*
		}
	}
}

impl Display for StandardHttpHeader
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			#([headers] &StandardHttpHeader::#symbol => f.write_str(#raw),)*
		}
	}
}

///Encode a header.
pub fn hpack_encode_header<S:Sink>(output: &mut S, name: &[u8], value: &[u8]) -> Result<(), ()>
{
	match (name, value) {
		#([hdr_values](#name, #value) => output.write_u8(#code)?,)*
		#([hdr_names](#name, _) => encode_value(output, #code, value)?,)*
		(_, _) => {
			output.write_u8(0)?;
			encode_one_single(output, name)?;
			encode_one_single(output, value)?;
		}
	}
	Ok(())
}

///Encode a header.
pub fn hpack_encode_header_std<S:Sink>(output: &mut S, name: StandardHttpHeader, value: &[u8]) -> Result<(), ()>
{
	match (name, value) {
		#([hdr_values](StandardHttpHeader::#symbol, #value) => output.write_u8(#code)?,)*
		#([hdr_names](StandardHttpHeader::#symbol, _) => encode_value(output, #code, value)?,)*
		(_, _) => {
			output.write_u8(0)?;
			encode_one_single_std(output, name)?;
			encode_one_single(output, value)?;
		}
	}
	Ok(())
}

fn get_static_name(index: usize) -> Result<StandardHttpHeader, Error>
{
	let ret = match index.saturating_add(1) {
		#([static_map] #number => StandardHttpHeader::#name,)*
		i => fail!(Error(_Error::IntNotStatic(i))),
	};
	Ok(ret)
}

fn get_static_value(index: usize) -> Result<&'static [u8], Error>
{
	let ret = match index.saturating_add(1) {
		#([static_map] #number => #value as &[u8],)*
		i => fail!(Error(_Error::IntNotStatic(i))),
	};
	Ok(ret)
}
	});

	println!("cargo:rerun-if-changed=header-names-http.dat");
	println!("cargo:rerun-if-changed=header-names-permanent.dat");
	println!("cargo:rerun-if-changed=header-names-provisional.dat");
	println!("cargo:rerun-if-changed=header-names-other.dat");
	println!("cargo:rerun-if-changed=stdheaders.dat");
}
