//!HTTP/2
#![allow(unsafe_code)]
#![forbid(unused_must_use)]
#![deny(missing_docs)]

use crate::http::AuthorityKind;
use crate::http::ElementDispatch as ElementDispatchTrait;
use crate::http::EncodeHeaders;
use crate::http::HeaderBlockState;
use crate::http::HeaderFault;
use crate::http::HeaderName;
use crate::http::HttpUtils;
use crate::http::ReceiveBuffer;
use crate::http::Sink;
use crate::http::Source;
use crate::http::StandardHttpHeader;
use crate::http::StreamAbort;
use crate::http::StreamState as StreamState2;
use crate::http::StreamType;
use crate::http::UnsafeHeaderName;
use crate::http::UnsafeHeaderValue;
use crate::http::UnsafeHeaderValueC;
use crate::http2_frame::Frame;
use crate::http2_frame::FrameParseError;
use crate::http2_frame::FLAG_EOH;
use crate::http2_frame::FLAG_EOS;
use crate::http2_frame::FLAG_PAD;
use crate::http2_frame::FLAG_PRIORITY;
use crate::http2_frame::FTYPE_CONTINUATION;
use crate::http2_frame::FTYPE_DATA;
use crate::http2_frame::FTYPE_GOAWAY;
use crate::http2_frame::FTYPE_HEADERS;
use crate::http2_frame::FTYPE_SETTINGS;
use crate::http2_frame::HDR_LEN;
pub use crate::http2_frame::NO_PADDING;
pub use crate::http2_frame::PaddingAmount;
use crate::http2_frame::PingFrame;
use crate::http2_frame::Priority;
use crate::http2_frame::PriorityFrame;
use crate::http2_frame::RstStreamFrame;
pub use crate::http2_frame::StreamId;
pub use crate::http2_frame::StreamIdNonZero;
use crate::http2_frame::StreamState;
pub use crate::http2_frame::WindowIncrement;
use crate::http2_frame::WindowUpdateFrame;
use crate::http2_hpack::Context;
use crate::http2_hpack::Decoder;
use crate::http2_hpack::Error as HpackError;
pub use crate::http2_hpack::hpack_encode_header as encode_header;
pub use crate::http2_hpack::hpack_encode_header_std as encode_header_std;
use crate::http2_hpack::hpack_encode_path_concat2;
use crate::http2_hpack::PushElement;
use btls_aux_collections::BTreeMap;
use btls_aux_fail::f_return;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::fail_if_none;
use btls_aux_memory::copy_maximum;
use btls_aux_serialization::Sink as DSink;
#[cfg(test)] use btls_aux_serialization::SliceSink;
use core::cmp::max;
use core::cmp::min;
use core::fmt::Debug;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::fmt::Write as FmtWrite;
use core::mem::replace;
use core::ops::Deref;
#[cfg(test)] use std::string::String;
#[cfg(test)] use std::vec::Vec;
#[cfg(test)] use std::borrow::Cow;
#[cfg(test)] use std::borrow::ToOwned;

const MAX_FRAME_SIZE: usize = 16384;
const MAX_BUFFER: usize = MAX_FRAME_SIZE+HDR_LEN;
const MAX_SETTINGS: usize = 512;
const MAX_DEBUG: usize = 8192;

macro_rules! add_iterator { ($a:expr, $b:expr) => { $a.checked_add($b).ok_or(INTERR_ITER_OF)? } }
macro_rules! increment_iterator { ($a:expr, $b:expr) => { $a = add_iterator!($a, $b); } }

macro_rules! copy_data_to_frame_buffer
{
	($selfx:ident, $data:ident, $itr:ident) => {{
		let dst = $selfx.buffer.get_mut(($selfx.buffer_use as usize)..($selfx.next_frame_at as usize)).
			ok_or("Failed to fill frame buffer")?;
		let src = $data.get($itr..).unwrap_or(&[]);
		let amt = copy_maximum(dst, src);
		$itr = $itr.checked_add(amt).ok_or(INTERR_ITER_OF)?;
		$selfx.buffer_use = $selfx.buffer_use.checked_add(amt as u16).ok_or(INTERR_BUFP_OF)?;
		()
	}}
}

macro_rules! compute_frame_size
{
	($len:expr) => {{
		let len = $len;
		(len as usize).checked_add(HDR_LEN).and_then(|x|{
			if len as usize <= MAX_FRAME_SIZE { Some(x) } else { None }
		}).ok_or(IllegalFrameSize(len))?
	}}
}

macro_rules! read_frame_size
{
	($selfx:ident) => {{
		let len = read_be24(&$selfx.buffer, 0).ok_or("Failed to read frame length")?;
		let flen = compute_frame_size!(len);
		$selfx.next_frame_at = flen as u16;
		()
	}}
}


static INTERR_ITER_OF: &'static str = "Iterator overflow";
static INTERR_BUFP_OF: &'static str = "Buffer pointer overflow";

///Error from parse.
///
///All errors are fatal to the connection.
#[derive(Copy,Clone,Debug)]
pub struct Error(_Error);

#[derive(Copy,Clone,Debug)]
enum _Error
{
	BadPreface,
	BadFrame(FrameParseError),
	BadHpack(HpackError),
	Goaway(u32),
	IllegalFrameSize(u32),
	IllegalPushPromise,
	IllegalSettingValue(u16, u32),
	IllegalStreamId(u32),
	OverflowConnectionWindow,
	OverflowStreamWindow(u32),
	StreamError,
	UnderflowConnectionWindow,
	UnderflowStreamWindow(u32),
	StreamClosed(u32),
	UnexpectedClose,
	InternalError(&'static str),
}

impl Display for Error
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::_Error::*;
		match self.0 {
			BadPreface => write!(f, "Received bad connection preface"),
			BadFrame(err) => write!(f, "Received bad frame: {err}"),
			BadHpack(err) => write!(f, "Bad HPACK data: {err}"),
			Goaway(code) => write!(f, "Received GOAWAY with code {code}"),
			IllegalFrameSize(size) => write!(f, "Illegal frame size {size}"),
			IllegalPushPromise => write!(f, "Illegal PUSH_PROMISE frame"),
			IllegalSettingValue(key,value) => write!(f, "Illegal value {value} for setting {key}"),
			IllegalStreamId(sid) => write!(f, "Illegal stream id {sid} for new stream"),
			OverflowConnectionWindow => write!(f, "Window overflow on connection"),
			OverflowStreamWindow(sid) => write!(f, "Window overflow on stream {sid}"),
			StreamClosed(sid) => write!(f, "Stream {sid} already closed"),
			StreamError => write!(f, "I/O error"),
			UnderflowConnectionWindow => write!(f, "Window underflow on connection"),
			UnderflowStreamWindow(sid) => write!(f, "Window underflow on stream {sid}"),
			UnexpectedClose => write!(f, "Unexpected end of connection"),
			InternalError(err) => write!(f, "Internal Error: {err}"),
		}
	}
}

impl Error
{
	///Get error code.
	pub fn error_code(&self) -> Option<u32>
	{
		use self::_Error::*;
		Some(match self.0 {
			BadPreface => return None,
			BadFrame(_) => 1,
			BadHpack(_) => 9,
			Goaway(_) => return None,
			IllegalFrameSize(_) => 1,
			IllegalPushPromise => 1,
			IllegalSettingValue(_,_) => 1,
			IllegalStreamId(_) => 1,
			OverflowConnectionWindow => 3,
			OverflowStreamWindow(_) => 3,
			StreamClosed(_) => 5,
			StreamError => 2,
			UnderflowConnectionWindow => 3,
			UnderflowStreamWindow(_) => 3,
			UnexpectedClose => 2,
			InternalError(_) => 2,
		})
	}
}

impl From<&'static str> for Error
{
	fn from(e: &'static str) -> Error { Error(_Error::InternalError(e)) }
}

impl From<_Error> for Error
{
	fn from(e: _Error) -> Error { Error(e) }
}

///Element from HTTP/2 parsing.
#[derive(Debug)]
#[non_exhaustive]
pub enum Element<'b,'a:'b>
{
	///A block of data was received.
	Data(&'b mut RespondWith<'a>, &'b [u8]),
	///Version was received.
	Version(&'b mut RespondWith<'a>, &'b [u8]),
	///Status was received.
	Status(&'b mut RespondWith<'a>, u16),
	///Reason was received.
	Reason(&'b mut RespondWith<'a>, &'b [u8]),
	///Method was received.
	Method(&'b mut RespondWith<'a>, &'b [u8]),
	///Scheme was received.
	Scheme(&'b mut RespondWith<'a>, &'b [u8]),
	///Authority was received.
	Authority(&'b mut RespondWith<'a>, &'b [u8]),
	///Raw authority was received.
	AuthorityRaw(&'b mut RespondWith<'a>, &'b [u8]),
	///Content-Length was received.
	ContentLength(&'b mut RespondWith<'a>, u64),
	///Path was received.
	Path(&'b mut RespondWith<'a>, &'b [u8]),
	///Raw path was received.
	PathRaw(&'b mut RespondWith<'a>, &'b [u8]),
	///Protocol was received.
	Protocol(&'b mut RespondWith<'a>, &'b [u8]),
	///Query was received.
	Query(&'b mut RespondWith<'a>, &'b [u8]),
	///An header was received.
	Header(&'b mut RespondWith<'a>, &'b [u8], &'b [u8]),
	///A standard header was received.
	HeaderStd(&'b mut RespondWith<'a>, StandardHttpHeader, &'b [u8]),
	///End of interim headers was received, more headers next..
	EndOfHeadersInterim(&'b mut RespondWith<'a>),
	///End of headers was received, body next.
	EndOfHeaders(&'b mut RespondWith<'a>),
	///A trailer was received. Also signifies that the data has ended.
	Trailer(&'b mut RespondWith<'a>, &'b [u8], &'b [u8]),
	///A standard trailer was received. Also signifies that the data has ended.
	TrailerStd(&'b mut RespondWith<'a>, StandardHttpHeader, &'b [u8]),
	///A middler was received.
	Middler(&'b mut RespondWith<'a>, &'b [u8], &'b [u8]),
	///A standard middler was received.
	MiddlerStd(&'b mut RespondWith<'a>, StandardHttpHeader, &'b [u8]),
	///An end of middlers block.
	EndOfMiddlers(&'b mut RespondWith<'a>),
	///Start of message was received.
	StartOfMessage(&'b mut RespondWith<'a>),
	///End of message was received. May be sent already during headers.
	EndOfMessage(&'b mut RespondWith<'a>),
	///Header fault was received. This request has failed. Next event is about another message.
	HeadersFaulty(&'b mut RespondWith<'a>, HeaderFault),
	///Error abort. was received. The stream should be reset.
	Abort(&'b mut RespondWith<'a>, StreamAbort),
	///Sent reset on stream.
	SentReset(u32, u32),
	///Received reset on stream.
	ReceivedReset(u32, u32),
	///Receive repriorizaiton request `(reqid, dependency, exclusive, weight)`.
	Repriorize(u32, u32, bool, u8),
	///Reap stream.
	ReapStream(u32),
	///SETTINGS item
	Setting(u16, u32),
	///SETTINGS complete
	SettingsComplete(&'b mut Acker<'a>),
	///SETTINGS ACK.
	SettingsAck,
	///PING.
	Ping(&'b mut Acker<'a>, u64),
	///PONG.
	Pong(u64),
	///Unknown frame.
	Unknown(u32, u8, u8, &'a [u8]),
	///GOAWAY.
	Goaway(u32, &'a [u8]),
	///Start header block. This is always sent for every header block received, before anything else.
	StartHeaderBlock,
	///Trace header fragment `(fragment, soh, eoh)`.
	TraceHeaderFragment(u32, &'a [u8], bool, bool),
	///Trace header.
	TraceHeader(u32, &'a [u8], &'a [u8]),
}

///Transmit control operation error.
#[non_exhaustive]
pub enum TxError<E:Sized+Debug+Display>
{
	///Expected CONTINUATION.
	ExpectedContinuation,
	///Frame would be too big.
	FrameTooBig,
	///Not enough credits.
	NotEnoughCredits,
	///Stream is not open.
	StreamNotOpen,
	///Open stream failed.
	StreamOpenFailed,
	///Unexpected CONTINUATION.
	UnexpectedContinuation,
	///I/O error.
	Io(E),
	///Internal error
	InternalError(&'static str),
	///Connection window underflow.
	UnderflowConnectionWindow,
	///Stream window underflow.
	UnderflowStreamWindow(u32),
}

impl<E:Sized+Debug+Display> TxError<E>
{
	///Is stream not open error?
	pub fn stream_not_open(&self) -> bool { matches!(self, &TxError::StreamNotOpen) }
	///Extract I/O error.
	pub fn extract_io_error(self) -> Result<E, TxError<E>>
	{
		match self { TxError::Io(e) => Ok(e), other => Err(other) }
	}
}

impl<E:Sized+Debug+Display> Display for TxError<E>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::TxError::*;
		match self {
			&ExpectedContinuation => f.write_str("Expected CONTINUATION frame"),
			&FrameTooBig => f.write_str("Frame is too big"),
			&NotEnoughCredits => f.write_str("Not enough credits to send frame"),
			&StreamNotOpen => f.write_str("Stream is not open"),
			&StreamOpenFailed => f.write_str("Opening stream failed"),
			&UnexpectedContinuation => f.write_str("Unexpected CONTINUATION frame"),
			&Io(ref err) => write!(f, "Write error: {err}"),
			&InternalError(err) => write!(f, "Internal error: {err}"),
			&UnderflowConnectionWindow => f.write_str("Connection window underflow"),
			&UnderflowStreamWindow(sid) => write!(f, "Stream window underflow on {sid}"),
		}
	}
}

impl<S:Sized+Debug+Display> From<&'static str> for TxError<S>
{
	fn from(e: &'static str) -> TxError<S> { TxError::InternalError(e) }
}

///Element sink.
pub trait ElementSink
{
	///Sink an element.
	fn sink<'b,'a:'b>(&mut self, elem: Element<'b,'a>);
	///Is protocol supported?
	fn protocol_supported(&self, protocol: &[u8]) -> bool;
	#[doc(hidden)]
	fn reap_stream(&mut self, sid: u32) { self.sink(Element::ReapStream(sid)); }
}

struct ElementDispatch<'a,'b,S:ElementSink+'a>(&'a mut S, RespondWith<'b>);

impl<'a,'b,S:ElementSink> ElementDispatchTrait for ElementDispatch<'a,'b,S>
{
	fn abort(&mut self, cause: StreamAbort) { self.0.sink(Element::Abort(&mut self.1, cause)); }
	fn authority(&mut self, x: &[u8]) { self.0.sink(Element::Authority(&mut self.1, x)); }
	fn content_length(&mut self, x: u64) { self.0.sink(Element::ContentLength(&mut self.1, x)); }
	fn path(&mut self, x: &[u8]) { self.0.sink(Element::Path(&mut self.1, x)); }
	fn query(&mut self, x: &[u8]) { self.0.sink(Element::Query(&mut self.1, x)); }
	fn scheme(&mut self, x: &[u8]) { self.0.sink(Element::Scheme(&mut self.1, x)); }
	fn protocol(&mut self, x: &[u8]) { self.0.sink(Element::Protocol(&mut self.1, x)); }
	fn method(&mut self, x: &[u8]) { self.0.sink(Element::Method(&mut self.1, x)); }
	fn version(&mut self, x: &[u8]) { self.0.sink(Element::Version(&mut self.1, x)); }
	fn reason(&mut self, x: &[u8]) { self.0.sink(Element::Reason(&mut self.1, x)); }
	fn status(&mut self, x: u16) { self.0.sink(Element::Status(&mut self.1, x)); }
	fn middler(&mut self, n: HeaderName, v: &[u8])
	{
		let e = match n {
			HeaderName::Standard(n) => Element::MiddlerStd(&mut self.1, n, v),
			HeaderName::Custom(n) => Element::Middler(&mut self.1, n, v),
		};
		self.0.sink(e);
	}
	fn end_of_middlers(&mut self) { self.0.sink(Element::EndOfMiddlers(&mut self.1)); }
	fn trailer(&mut self, n: HeaderName, v: &[u8])
	{
		let e = match n {
			HeaderName::Standard(n) => Element::TrailerStd(&mut self.1, n, v),
			HeaderName::Custom(n) => Element::Trailer(&mut self.1, n, v),
		};
		self.0.sink(e);
	}
	fn header(&mut self, n: HeaderName, v: &mut [u8])
	{
		let e = match n {
			HeaderName::Standard(n) => Element::HeaderStd(&mut self.1, n, v),
			HeaderName::Custom(n) => Element::Header(&mut self.1, n, v),
		};
		self.0.sink(e);
	}
	fn protocol_supported(&self, x: &[u8]) -> bool { self.0.protocol_supported(x) }
	fn data(&mut self, x: &[u8]) { self.0.sink(Element::Data(&mut self.1, x)); }
	fn start_of_message(&mut self) { self.0.sink(Element::StartOfMessage(&mut self.1)); }
	fn end_of_message(&mut self) { self.0.sink(Element::EndOfMessage(&mut self.1)); }
	fn end_of_headers(&mut self) { self.0.sink(Element::EndOfHeaders(&mut self.1)); }
	fn end_of_headers_interim(&mut self) { self.0.sink(Element::EndOfHeadersInterim(&mut self.1)); }
	fn reap_stream(&mut self, id: u32) { self.0.sink(Element::ReapStream(id)); }
	fn path_raw(&mut self, x: &[u8]) { self.0.sink(Element::PathRaw(&mut self.1, x)); }
	fn authority_raw(&mut self, x: &[u8]) { self.0.sink(Element::AuthorityRaw(&mut self.1, x)); }
	fn trace_header_fragment(&mut self, fragment: &[u8], soh: bool, eoh: bool)
	{
		self.0.sink(Element::TraceHeaderFragment(self.1.get_sid().into_inner(), fragment, soh, eoh));
	}
	fn trace_header(&mut self, name: &[u8], value: &[u8])
	{
		self.0.sink(Element::TraceHeader(self.1.get_sid().into_inner(), name, value));
	}
}

//These can be at most 65534.
const MAX_NAME: usize = 256;
const MAX_VALUE: usize = 4096;

struct BytesToHeaders
{
	error: bool,
	value: bool,
	nbuf: [u8;MAX_NAME],
	nbuflen: u16,
	vbuf: [u8;MAX_VALUE],
	vbuflen: u16,
	use_std: bool,
	stdname: Option<StandardHttpHeader>,
}

macro_rules! try_in_recv
{
	($selfx:expr, $push:expr, $e:expr, $sstate:expr, $fault:expr) => {
		match $e {
			Some(x) => x,
			None => {
				HttpUtils::header_fault($push, $sstate, $fault);
				return $selfx.error = true;
			}
		}
	}
}

fn slice_fitting<'a>(target: &'a mut [u8], start: usize, len: usize) -> (usize, &'a mut [u8])
{
	let end = start.saturating_add(len);
	//Indices forced in-bounds. start2 and end2 are both <= target.len(), so in bounds for access. Furthermore,
	//start2 <= end2, because otherwise start > end, which impiles len < 0, which is impossible.
	let start2 = min(start, target.len());
	let end2 = min(end, target.len());
	(end, &mut target[start2..end2])
}

fn copy_fitting(target: &mut [u8], source: &[u8], start: usize) -> usize
{
	let (end, target) = slice_fitting(target, start, source.len());
	copy_maximum(target, source);
	end
}

impl BytesToHeaders
{
	fn new() -> BytesToHeaders
	{
		BytesToHeaders {
			error: false,
			value: false,
			nbuf: [0;MAX_NAME],
			nbuflen: 0,
			vbuf: [0;MAX_VALUE],
			vbuflen: 0,
			use_std: false,
			stdname: None,
		}
	}
	fn reset(&mut self)
	{
		self.error = false;
		self.value = false;
		self.nbuflen = 0;
		self.vbuflen = 0;
		self.stdname = None;
	}
	fn receive(&mut self, e: PushElement, push: &mut impl ElementDispatchTrait, state: &mut HeaderBlockState,
		sstate: &mut StreamState2, trace_flag: bool)
	{
		if self.error { return; }
		match e {
			PushElement::StdName(n) => {
				//This is only possible in Name state and impiles EndOfString.
				if self.value {
					let fault = HeaderFault::InternalError("StdName in value state");
					HttpUtils::header_fault(push, sstate, fault);
					return self.error = true;
				}
				self.stdname = Some(n);
				self.value = true;
			},
			//It is guraranteed that self.value can not change during Bytes(...).
			PushElement::Bytes(b) if !self.value => {
				let end = copy_fitting(&mut self.nbuf, b, self.nbuflen as usize);
				self.nbuflen = min(end, 65535) as u16;
			},
			PushElement::Bytes(b) => {
				let end = copy_fitting(&mut self.vbuf, b, self.vbuflen as usize);
				self.vbuflen = min(end, 65535) as u16;
			},
			PushElement::EndOfString if !self.value => {
				self.value = true;
			},
			PushElement::EndOfString => {
				self.value = false;
				let nlen = self.nbuflen as usize;
				let vlen = self.vbuflen as usize;
				let nlen_u8 = min(nlen, 255) as u8;
				let vbuflen = self.vbuf.len();
				let rvalue = self.vbuf.get_mut(..vlen);
				let (name, vfault) = if let Some(stdname) = self.stdname {
					let vfault = HeaderFault::HeaderValueTooLongStd(stdname, vlen, 65535,
						vbuflen);
					(UnsafeHeaderName::standard(stdname), vfault)
				} else {
					let mut sample = [0;32];
					let samplelen = min(min(nlen, sample.len()), self.nbuf.len());
					//samplelen is bounded by sample.len(), self.nbuf.len() and self.nbuflen.
					(&mut sample[..samplelen]).copy_from_slice(&self.nbuf[..samplelen]);
					let name = try_in_recv!(self, push, self.nbuf.get_mut(..nlen), sstate,
						HeaderFault::HeaderNameTooLong(sample, nlen_u8, nlen, 65535,
						self.nbuf.len()));
					let vfault = HeaderFault::HeaderValueTooLong(sample, nlen_u8, vlen, 65535,
						vbuflen);
					(UnsafeHeaderName::new(name), vfault)
				};
				let value = try_in_recv!(self, push, rvalue, sstate, vfault);
				//Reset.
				self.nbuflen = 0;
				self.vbuflen = 0;
				self.stdname = None;
				//Tracing: Decoded headers.
				if trace_flag { push.trace_header(name.as_raw(), value); }
//println!("Header reconstructed: Name='{name}', Value='{value}'", name=::std::string::escape_bytes(name),
//	value=::std::string::escape_bytes(value));
				let sname = name.as_standard();
				let value = UnsafeHeaderValue::new(value);
				if sname == Some(StandardHttpHeader::Http2MagicHeaderStatus) {
					HttpUtils::status(push, state, sstate, value.to_const());
				} else if sname == Some(StandardHttpHeader::Http2MagicHeaderMethod) {
					HttpUtils::method(push, state, sstate, value.to_const());
				} else if sname == Some(StandardHttpHeader::Http2MagicHeaderScheme) {
					HttpUtils::scheme(push, state, sstate, value);
				} else if sname == Some(StandardHttpHeader::Http2MagicHeaderPath) {
					HttpUtils::path(push, state, sstate, value);
				} else if sname == Some(StandardHttpHeader::Http2MagicHeaderAuthority) {
					//Always non-special, since this is valid alone.
					HttpUtils::authority(push, state, sstate, value,
						AuthorityKind::Http2Authority);
				} else if sname == Some(StandardHttpHeader::Http2MagicHeaderProtocol) {
					//This generates stream abort anyway if it fails.
					HttpUtils::protocol(push, state, sstate, value).ok();
				} else {
					//Upgrade is one of the banned headers.
					if let Some(name) = sname { if name.is_banned_in_http2() {
						let fault = HeaderFault::HeaderBannedInH2(name);
						HttpUtils::header_fault(push, sstate, fault);
						self.error = true;
						return;
					}}
					HttpUtils::header(push, state, sstate, name, value, true, self.use_std);
				}
			},
			//We do not care about next transition this outputs.
			PushElement::EndOfCollection => {HttpUtils::end_headers(push, state, sstate, self.use_std);},
		}
	}
}

impl StandardHttpHeader
{
	fn is_banned_in_http2(self) -> bool
	{
		matches!(self, StandardHttpHeader::Connection|StandardHttpHeader::ProxyConnection|
			StandardHttpHeader::KeepAlive|StandardHttpHeader::TransferEncoding|
			StandardHttpHeader::Upgrade)
	}
}

const FLAG_CLOSE_TX: u8 = 1;
const FLAG_CLOSE_RX: u8 = 2;
const FLAG_CLOSED: u8 = 3;
const FLAG_CLOSE_AUX: u8 = 4;
const FLAG_CLOSED_FULL: u8 = 7;

//This should have never been exported.
#[doc(hidden)]
#[derive(Copy,Clone)]
pub struct StreamEntry
{
	///The state.
	state: StreamState2,
	///The receive credit.
	rx_credit: i32,
	///The transmit credit.
	tx_credit: i32,
	///Closed flags.
	closed: u8,
	//Called start_of_message.
	called_start_of_message: bool,
}

impl StreamEntry
{
	fn _new(server: bool, dflt_tx_credit: i32) -> StreamEntry
	{
		let smode = if server { StreamType::Http2Reply } else { StreamType::Http2Request };
		StreamEntry {
			state: StreamState2::new2(smode),
			rx_credit: 65535,		//HTTP/2 default.
			tx_credit: dflt_tx_credit,
			closed: 0,
			called_start_of_message: false,
		}
	}
}

const MAX_STREAMS: u32 = 100;

///The supported settings.
pub const HTTP2_SUPPORTED_SETTINGS: &'static [(u16, u32)] = &[
	(1, crate::http2_hpack::MAX_TABLESIZE as u32),	//Maximum header table size.
	(2, 0),						//Push not supported.
	(3, MAX_STREAMS),				//Maximum number of streams.
	(4, 65535),					//Leave this always at default.
	(5, MAX_FRAME_SIZE as u32),			//Maximum frame size.
	//SETTINGS_MAX_HEADER_LIST_SIZE is not used.
	(8, 1),						//Extended protocol is supported.
];

struct StreamTable
{
	server: bool,
	hmap: BTreeMap<u32, StreamEntry>,
	max_streams: u32,
}

impl StreamTable
{
	fn new(server: bool) -> StreamTable
	{
		StreamTable {
			server: server,
			hmap: BTreeMap::new(),
			max_streams: MAX_STREAMS,
		}
	}
	fn lookup(&mut self, strmid: StreamIdNonZero) -> Option<&mut StreamEntry>
	{
		self.hmap.get_mut(&strmid.into_inner())
	}
	fn lookup_const(&self, strmid: StreamIdNonZero) -> Option<&StreamEntry>
	{
		self.hmap.get(&strmid.into_inner())
	}
	fn delete(&mut self, strmid: StreamIdNonZero) -> bool { self.hmap.remove(&strmid.into_inner()).is_some() }
	fn create(&mut self, strmid: StreamIdNonZero, max_even: &mut u32, max_odd: &mut u32, draining: bool,
		dflt_tx_credit: i32) -> bool
	{
		let strmid = strmid.into_inner();
		if draining { return false; }	//No stream creation when draining.
		if self.hmap.len() >= self.max_streams as usize { return false; }	//No space.
		if self.hmap.contains_key(&strmid) { return false; }			//Already exists.
		self.hmap.insert(strmid, StreamEntry::_new(!self.server, dflt_tx_credit));
		if strmid % 2 == 0 { *max_even = max(*max_even, strmid); }
		if strmid % 2 != 0 { *max_odd = max(*max_odd, strmid); }
		true
	}
	fn unlink_some(&mut self) -> Option<(u32, StreamEntry)>
	{
		let strmid = *self.hmap.keys().next()?;
		let entry = self.hmap.remove(&strmid)?;
		Some((strmid, entry))
	}
	fn is_empty(&self) -> bool { self.hmap.len() == 0 }
	fn delta_all_tx_windows(&mut self, delta: i32)
	{
		for (_, v) in self.hmap.iter_mut() { v.tx_credit = v.tx_credit.saturating_add(delta); }
	}
}

macro_rules! subtract_to_zero
{
	($tgt:expr, $amt:expr, $err:expr) => {
		$tgt = $tgt.checked_sub($amt).ok_or($err)?;
		fail_if!($tgt < 0, $err);
	}
}

macro_rules! close_dir
{
	($selfx:ident, $strmid:expr, $flag:expr, $push:expr) => {{
		let delete = if let Some(strm) = $selfx.streams.lookup($strmid) {
			strm.closed |= $flag;
			strm.closed & FLAG_CLOSED == FLAG_CLOSED
		} else {
			false
		};
		if delete {
			$selfx.streams.delete($strmid);
			$push.reap_stream($strmid.into_inner());
		}
	}}
}

struct DebugPrinter<'a>(&'a mut [u8], usize);

impl<'a> FmtWrite for DebugPrinter<'a>
{
	fn write_str(&mut self, s: &str) -> Result<(), FmtError>
	{
		let dst = f_return!(self.0.get_mut(self.1..), Ok(()));
		self.1 += copy_maximum(dst, s.as_bytes());
		Ok(())
	}
}

///Event that happens during sending of frame on stream.
pub trait SendEvent
{
	///Stream has been reaped.
	fn reaped(&mut self);
	///Stream has been aborted with specified code.
	fn aborted(&mut self, code: u32);
}

///Ack stuff.
#[derive(Debug)]
pub struct Acker<'a>(&'a mut StreamId, Option<u64>);	//tx_continuation_strm, ping_id

impl<'a> Acker<'a>
{
	///Acknowledge this settings or PING.
	pub fn ack<S:Sink>(&mut self, sink: &mut S) -> Result<(), TxError<S::Error>>
	{
		let mut buffer = [0;17];
		fail_if!(self.0.into_nonzero().is_some(), TxError::ExpectedContinuation);
		let blen = if let Some(code) = self.1 {
			Frame::Ping(PingFrame{
				code: code,
				ack: true,
			}).serialize(&mut buffer).ok()
		} else {
			Frame::SettingsAck.serialize(&mut buffer).ok()
		};
		match blen {
			Some(l) if l <= buffer.len() => sink.write(&[&buffer[..l]]).map_err(TxError::Io)?,
			_ => fail!("Unexpectedly large acknowledgement frame")
		}
		Ok(())
	}
}

///Respond with headers, data and resets on a stream.
#[derive(Debug)]
pub struct RespondWith<'a>
{
	sid: StreamId,
	peer_frame_limit: usize,
	conn_tx_credit: &'a mut i32,
	strm_tx_credit: &'a mut i32,
	strm_closed: &'a mut u8,
	tx_continuation_strm: &'a mut StreamId,
	reaped_id: &'a mut StreamId,	//Set to sid if stream is reaped.
}

impl<'a> RespondWith<'a>
{
	///Get the sid for stream.
	pub fn get_sid(&self) -> StreamId { self.sid }
	///Get largest amount of data that can be sent.
	pub fn get_max_data(&self) -> usize { max(min(*self.conn_tx_credit, *self.strm_tx_credit), 0) as usize }
	///Send DATA frame.
	pub fn send_data_frame<R:SendEvent,S:Sink>(&mut self, sink: &mut S, push: &mut R, eos: bool,
		padding: PaddingAmount, payload: &[u8]) -> Result<bool, TxError<S::Error>>
	{
		static PADDING: [u8;255] = [0;255];
		fail_if!(self.tx_continuation_strm.into_nonzero().is_some(), TxError::ExpectedContinuation);
		let flags = if eos { FLAG_EOS } else { 0 } | if padding.present() { FLAG_PAD } else { 0 };
		let len = payload.len().checked_add(padding.amount()).ok_or(TxError::FrameTooBig)?;
		fail_if!(len > self.peer_frame_limit, TxError::FrameTooBig);
		//Can not send on closed stream.
		if *self.strm_closed & FLAG_CLOSE_TX != 0 { return Ok(false); }
		//Debit credits.
		let amt = len as i32;
		match (self.conn_tx_credit.checked_sub(amt), self.strm_tx_credit.checked_sub(amt)) {
			(Some(c), Some(s)) if c >= 0 && s >= 0 => {
				*self.conn_tx_credit = c;
				*self.strm_tx_credit = s;
			},
			(_, _) => fail!(TxError::NotEnoughCredits),
		}
		if eos {
			*self.strm_closed |= FLAG_CLOSE_TX | FLAG_CLOSE_AUX;
			//Due to the above check on FLAG_CLOSE_TX, this can only happen once.
			if *self.strm_closed & FLAG_CLOSED == FLAG_CLOSED {
				*self.reaped_id = self.sid;
				push.reaped();
			}
		}
		//Okay.
		let head = ConnectionInner::make_header(len, FTYPE_DATA, flags, self.sid.into_inner());
		let pbyte = [padding.amount().wrapping_sub(1) as u8];
		let pbyte = if padding.present() { &pbyte[..] } else { &[] };
		let padding = padding.amount().saturating_sub(1);	//0 => 0, otherwise -1.
		let padding = PADDING.get(..padding).ok_or("Too much padding")?;
		sink.write(&[&head[..], pbyte, payload, padding]).map_err(TxError::Io).map(|_|true)
	}
	fn send_headers_tail<S:Sink>(&mut self, sink: &mut S, push: &mut impl SendEvent,
		eos: bool, eoh: bool, padding: PaddingAmount, priority: Option<Priority>, payload: &[u8],
		flags: u8, len: usize) -> Result<bool, TxError<S::Error>>
	{
		static PADDING: [u8;255] = [0;255];
		//The stream already exists, we never need to create it.
		let sid = self.sid.into_nonzero().ok_or("Stream handle to connection itself")?;
		if *self.strm_closed & FLAG_CLOSE_TX != 0 { return Ok(false); }
		//In case !eoh, the continuations do not check the status and can only be given on correct stream.
		if eos {
			*self.strm_closed |= FLAG_CLOSE_TX | FLAG_CLOSE_AUX;
			//Due to the above check on FLAG_CLOSE_TX, this can only happen once.
			if *self.strm_closed & FLAG_CLOSED == FLAG_CLOSED {
				*self.reaped_id = self.sid;
				push.reaped();
			}
		}
		if !eoh { *self.tx_continuation_strm = sid.into_stream_id(); }
		let head = ConnectionInner::make_header(len, FTYPE_HEADERS, flags, sid.into_inner());
		let pbyte = [padding.amount().wrapping_sub(1) as u8];
		let pbyte = if padding.present() { &pbyte[..] } else { &[] };
		let mut priotmp = [0;5];
		let priority = if let Some(priority) = priority {
			(&mut priotmp[..4]).copy_from_slice(&priority.dependency.into_inner().to_be_bytes());
			if priority.exclusive { priotmp[0] |= 128; }
			priotmp[4] = priority.weight;
			&priotmp[..]
		} else {
			&[]
		};
		let padding = padding.amount().saturating_sub(1);	//0 => 0, otherwise -1.
		let padding = PADDING.get(..padding).ok_or("Too much padding")?;
		sink.write(&[&head[..], pbyte, priority, payload, padding]).map_err(TxError::Io).map(|_|true)
	}
	///Send HEADERS frame.
	pub fn send_headers_frame<R:SendEvent,S:Sink>(&mut self, sink: &mut S, push: &mut R,
		eos: bool, eoh: bool, padding: PaddingAmount, priority: Option<Priority>, payload: &[u8]) ->
		Result<bool, TxError<S::Error>>
	{
		fail_if!(self.tx_continuation_strm.into_nonzero().is_some(), TxError::ExpectedContinuation);
		let mut flags = 0u8;
		let mut len = payload.len();
		if eos { flags |= FLAG_EOS; }
		if eoh { flags |= FLAG_EOH; }
		if padding.present() {
			flags |= FLAG_PAD;
			len = len.checked_add(padding.amount()).ok_or(TxError::FrameTooBig)?;
		}
		if priority.is_some() {
			flags |= FLAG_PRIORITY;
			len = len.checked_add(5).ok_or(TxError::FrameTooBig)?;
		}
		fail_if!(len > self.peer_frame_limit, TxError::FrameTooBig);

		self.send_headers_tail(sink, push, eos, eoh, padding, priority, payload, flags, len)
	}
	///Send PRIORITY frame.
	pub fn send_priority_frame<S:Sink>(&mut self, sink: &mut S, prio: Priority) ->
		Result<bool, TxError<S::Error>>
	{
		fail_if!(self.tx_continuation_strm.into_nonzero().is_some(), TxError::ExpectedContinuation);
		let sid = self.sid.into_nonzero().ok_or("Stream handle to connection itself")?;
		//Can not send on closed stream.
		if *self.strm_closed & FLAG_CLOSE_RX != 0 { return Ok(false); }
		//Okay.
		let mut buffer = [0;14];
		let blen = Frame::Priority(PriorityFrame{
			strmid: sid,
			priority: prio,
		}).serialize(&mut buffer).ok();
		match blen {
			Some(l) if l <= buffer.len() => sink.write(&[&buffer[..l]]).map_err(TxError::Io)?,
			_ => fail!("Unexpectedly large PRIORITY frame")
		};
		Ok(true)
	}
	///Send RST_STREAM frame.
	pub fn send_rst_stream_frame<R:SendEvent,S:Sink>(&mut self, sink: &mut S, push: &mut R, error: u32) ->
		Result<bool, TxError<S::Error>>
	{
		fail_if!(self.tx_continuation_strm.into_nonzero().is_some(), TxError::ExpectedContinuation);
		let sid = self.sid.into_nonzero().ok_or("Stream handle to connection itself")?;
		if *self.strm_closed & FLAG_CLOSED_FULL == FLAG_CLOSED_FULL { return Ok(false); }
		*self.strm_closed = FLAG_CLOSED_FULL;
		*self.reaped_id = self.sid;
		push.aborted(error);
		push.reaped();
		//Okay.
		let mut buffer = [0;13];
		let blen = Frame::RstStream(RstStreamFrame{
			strmid: sid,
			error: error,
		}).serialize(&mut buffer).ok();
		match blen {
			Some(l) if l <= buffer.len() => sink.write(&[&buffer[..l]]).map_err(TxError::Io)?,
			_ => fail!("Unexpectedly large RST_STREAM frame")
		};
		Ok(true)
	}
	///Send CONTINUATION frame.
	pub fn send_continuation_frame<S:Sink>(&mut self, sink: &mut S, eoh: bool, fragment: &[u8]) ->
		Result<(), TxError<S::Error>>
	{
		fail_if!(self.tx_continuation_strm.into_nonzero().is_none(), TxError::UnexpectedContinuation);
		fail_if!(fragment.len() > self.peer_frame_limit, TxError::FrameTooBig);
		let strmid = self.tx_continuation_strm.into_inner();
		let mut flags = 0u8;
		if eoh {
			flags |= FLAG_EOH;
			*self.tx_continuation_strm = StreamId::zero();
		}
		let head = ConnectionInner::make_header(fragment.len(), FTYPE_CONTINUATION, flags, strmid);
		sink.write(&[&head[..], fragment]).map_err(TxError::Io)
	}
}

struct ConnectionInner
{
	state: StreamState,
	decoder: Decoder,
	decoder_ctx: Context,
	hreceiver: BytesToHeaders,
	hblock: HeaderBlockState,
	eos_continue: bool,
	rx_credit: i32,
	tx_credit: i32,
	dflt_tx_credit: i32,
	max_id_odd: u32,
	max_id_even: u32,
	streams: StreamTable,
	draining: bool,
	server: bool,
	tx_continuation_strm: StreamId,
	peer_frame_limit: usize,
	trace_flag: bool,
}

impl ConnectionInner
{
	fn new(server: bool) -> ConnectionInner
	{
		ConnectionInner {
			state: StreamState::new(),
			decoder: Decoder::new(),
			decoder_ctx: Context::new(),
			eos_continue: false,
			hreceiver: BytesToHeaders::new(),
			hblock: HeaderBlockState::new(),
			max_id_even: 0,
			max_id_odd: 0,
			rx_credit: 65535,		//HTTP/2 default.
			tx_credit: 65535,		//HTTP/2 default.
			dflt_tx_credit: 65535,		//HTTP/2 default.
			streams: StreamTable::new(server),
			draining: false,
			server: server,
			tx_continuation_strm: StreamId::zero(),
			peer_frame_limit: 16384,	//HTTP/2 default.
			trace_flag: false,		//Tracing is off by default.
		}
	}
	fn is_drained(&self) -> bool { self.draining && self.streams.is_empty() }
	fn handle_fragment2(frag: &[u8], eoh: bool, decoder: &mut Decoder, ctx: &mut Context,
		mut cb: impl FnMut(PushElement)) -> Result<(), Error>
	{
		let cb = &mut cb;
		decoder.push(frag, ctx, |e|cb(e)).map_err(_Error::BadHpack)?;
		if eoh {
			let odecoder = replace(decoder, Decoder::new());	//Reset for next batch.
			odecoder.end(|e|cb(e)).map_err(_Error::BadHpack)?;
		}
		Ok(())
	}
	fn handle_fragment(&mut self, strmid: StreamIdNonZero, frag: &[u8], eoh: bool,
		soh: bool, push: &mut impl ElementSink) -> Result<(), Error>
	{
		let mut dummy = StreamId::zero();
		if let Some(strm) = self.streams.lookup(strmid) {
			//Set EOS flag if present.
			if self.eos_continue { strm.state.set_eos(); }
			//The stream must be open for reading.
			fail_if!(strm.closed & FLAG_CLOSE_RX != 0, _Error::StreamClosed(strmid.into_inner()));
			//Construct event sink.
			let mut push = ElementDispatch(push, RespondWith {
				sid: strmid.into_stream_id(),
				peer_frame_limit: self.peer_frame_limit,
				conn_tx_credit: &mut self.tx_credit,
				strm_tx_credit: &mut strm.tx_credit,
				strm_closed: &mut strm.closed,
				tx_continuation_strm: &mut self.tx_continuation_strm,
				//We do not need this here, since it will be reaped below anyway.
				reaped_id: &mut dummy,
			});
			let target = &mut self.hreceiver;
			let state = &mut self.hblock;
			let sstate = &mut strm.state;
			if soh && !sstate.main_headers_received() {
				//Push version, as normally HTTP/2 does not have it.
				HttpUtils::version(&mut push, state, sstate, UnsafeHeaderValueC::fixed("HTTP/2"));
			}
			let trace_flag = self.trace_flag;
			if trace_flag { push.trace_header_fragment(frag, soh, eoh); }
			Self::handle_fragment2(frag, eoh, &mut self.decoder, &mut self.decoder_ctx,
				|e|target.receive(e, &mut push, state, sstate, trace_flag))?;
		} else {
			//Do decode, discarding the answers.
			Self::handle_fragment2(frag, eoh, &mut self.decoder,
				&mut self.decoder_ctx, |_|())?;
		}
		//Clean up stream if needed.
		if eoh && self.eos_continue { close_dir!(self, strmid, FLAG_CLOSE_RX, push); }
		Ok(())
	}
	fn get_max_data_on_stream(&self, strmid: StreamIdNonZero) -> usize
	{
		//If stream is not found or it is closed for sending, return 0.
		let strm = f_return!(self.streams.lookup_const(strmid), 0);
		if strm.closed & FLAG_CLOSE_TX != 0 { return 0; }
		//Return minimum of connection credit and stream credit, floored to 0.
		max(min(self.tx_credit, strm.tx_credit), 0) as usize
	}
	fn _need_create_stream(&self, id: StreamIdNonZero) -> Result<bool, Error>
	{
		let id = id.into_inner();
		let is_odd = id % 2 == 1;
		let bound = if is_odd { self.max_id_odd } else { self.max_id_even };
		//Server opens even streams, client opens odd streams, so if new even stream is received by server,
		//or new odd stream by client, that is an error.
		fail_if!(id > bound && is_odd != self.server, Error(_Error::IllegalStreamId(id)));
		Ok(id > bound)
	}
	fn make_header(len: usize, ftype: u8, flags: u8, strmid: u32) -> [u8;9]
	{
		[
			(len >> 16) as u8, (len >> 8) as u8, len as u8,
			ftype, flags,
			(strmid >> 24) as u8, (strmid >> 16) as u8, (strmid >> 8) as u8, strmid as u8,
		]
	}
	fn send_settings_frame<S:Sink>(&mut self, sink: &mut S, list: impl Iterator<Item=(u16,u32)>) ->
		Result<(), TxError<S::Error>>
	{
		let mut payload = [0;6*MAX_SETTINGS];
		let mut ptr = 0usize;
		static E2BIG: &'static str = "Too many SETTINGS";
		for (k,v) in list {
			let end = ptr.checked_add(6).ok_or(E2BIG)?;
			let x = payload.get_mut(ptr..end).ok_or(E2BIG)?;
			(&mut x[0..2]).copy_from_slice(&k.to_be_bytes());
			(&mut x[2..6]).copy_from_slice(&v.to_be_bytes());
			ptr = end;
		}
		let payload = payload.get(..ptr).ok_or(E2BIG)?;
		let head = Self::make_header(payload.len(), FTYPE_SETTINGS, 0, 0);
		sink.write(&[&head[..], payload]).map_err(TxError::Io)
	}
	fn send_settings_frame_ack<S:Sink>(&mut self, sink: &mut S) -> Result<(), TxError<S::Error>>
	{
		let mut buffer = [0;9];
		fail_if!(self.tx_continuation_strm.into_nonzero().is_some(), TxError::ExpectedContinuation);
		let blen = Frame::SettingsAck.serialize(&mut buffer).ok();
		match blen {
			Some(l) if l <= buffer.len() => sink.write(&[&buffer[..l]]).map_err(TxError::Io)?,
			_ => fail!("Unexpectedly large SETTINGS(ACK) frame")
		};
		Ok(())
	}
	fn send_pingpong_frame<S:Sink>(&mut self, sink: &mut S, code: u64, pong: bool) ->
		Result<(), TxError<S::Error>>
	{
		let mut buffer = [0;17];
		fail_if!(self.tx_continuation_strm.into_nonzero().is_some(), TxError::ExpectedContinuation);
		let blen = Frame::Ping(PingFrame{
			code: code,
			ack: pong,
		}).serialize(&mut buffer).ok();
		match blen {
			Some(l) if l <= buffer.len() => sink.write(&[&buffer[..l]]).map_err(TxError::Io)?,
			_ => fail!("Unexpectedly large PING frame")
		};
		Ok(())
	}
	fn send_goaway_frame<S:Sink>(&mut self, sink: &mut S, maxstrm: StreamId, code: u32,
		debug: &impl Display) -> Result<(), TxError<S::Error>>
	{
		let mut mid = [0;8];
		let mut debug2 = [0u8;MAX_DEBUG];
		let debuglen = {
			let mut dtgt = DebugPrinter(&mut debug2, 0);
			write!(dtgt, "{debug}").ok();
			//This loop will eventually end because each iteration decrements dtgt.1 by 1, and
			//eventually it will hit 0.
			while dtgt.1 > 0 && *dtgt.0.get(dtgt.1).unwrap_or(&0) >> 6 == 2 {
				dtgt.1 = dtgt.1.checked_sub(1).ok_or("Target pointer underflow")?;
			}
			dtgt.1
		};
		let debug2 = debug2.get(..debuglen).ok_or("Debug information too long")?;
		(&mut mid[0..4]).copy_from_slice(&maxstrm.into_inner().to_be_bytes());
		(&mut mid[4..8]).copy_from_slice(&code.to_be_bytes());
		let len = debug2.len().checked_add(8).ok_or("MAX_DEBUG + 8 overflows?")?;
		let head = Self::make_header(len, FTYPE_GOAWAY, 0, 0);
		sink.write(&[&head[..], &mid[..], debug2]).map_err(TxError::Io)
	}
	fn send_window_update_frame<S:Sink>(&mut self, sink: &mut S, strmid: StreamId, increment: WindowIncrement) ->
		Result<(), TxError<S::Error>>
	{
		let mut buffer = [0;13];
		fail_if!(self.tx_continuation_strm.into_nonzero().is_some(), TxError::ExpectedContinuation);
		//Adjust window.
		if let Some(strmid) = strmid.into_nonzero() {
			if let Some(strm) = self.streams.lookup(strmid) {
				fail_if!(strm.closed & FLAG_CLOSE_RX != 0, TxError::StreamNotOpen);
				strm.rx_credit = strm.rx_credit.checked_add(increment.amount()).
					ok_or(TxError::UnderflowStreamWindow(strmid.into_inner()))?;
			} else {
				fail!(TxError::StreamNotOpen);
			}
		} else {
			self.rx_credit = self.rx_credit.checked_add(increment.amount()).
				ok_or(TxError::UnderflowConnectionWindow)?;
		}
		let blen = Frame::WindowUpdate(WindowUpdateFrame{
			strmid: strmid,
			increment: increment,
		}).serialize(&mut buffer).ok();
		match blen {
			Some(l) if l <= buffer.len() => sink.write(&[&buffer[..l]]).map_err(TxError::Io)?,
			_ => fail!("Unexpectedly large WINDOW_UPDATE frame")
		};
		Ok(())
	}
	fn push_frame(&mut self, f: &[u8], push: &mut impl ElementSink, no_more_streams: bool) -> Result<(), Error>
	{
		use self::_Error::*;
		fail_if!(f.len() < HDR_LEN, "Received frame too short to contain header");
		let (header, f) = f.split_at(HDR_LEN);
		let ftype = header[3];
		let flags = header[4];
		let mut t = [0;4]; t.copy_from_slice(&header[5..9]);
		let strmid = u32::from_be_bytes(t);
		let rawsize = f.len();
		let f = Frame::parse(strmid, f, ftype, flags, &mut self.state).
			map_err(_Error::BadFrame)?;
		//None of these need reaping, because this is receive, so we can manipulate the stream table.
		let mut dummystrm = StreamId::zero();
		match f {
			Frame::Data(f) => {
				//The connection window is always debited, and size is size of frame content,
				//not size of payload
				subtract_to_zero!(self.rx_credit, rawsize as i32, OverflowConnectionWindow);
				if let Some(strm) = self.streams.lookup(f.strmid) {
					//The stream must be open for reading.
					fail_if!(strm.closed & FLAG_CLOSE_RX != 0,
						StreamClosed(f.strmid.into_inner()));
					//Debit the stream.
					subtract_to_zero!(strm.rx_credit, rawsize as i32,
						OverflowStreamWindow(f.strmid.into_inner()));
					let mut push = ElementDispatch(push, RespondWith {
						sid: f.strmid.into_stream_id(),
						peer_frame_limit: self.peer_frame_limit,
						conn_tx_credit: &mut self.tx_credit,
						strm_tx_credit: &mut strm.tx_credit,
						strm_closed: &mut strm.closed,
						tx_continuation_strm: &mut self.tx_continuation_strm,
						reaped_id: &mut dummystrm,
					});
					//Forward the data.
					HttpUtils::data(&mut push, &mut strm.state, f.payload.deref());
					if f.eos { HttpUtils::end_of_message(&mut push, &mut strm.state); }
				};
				if f.eos { close_dir!(self, f.strmid, FLAG_CLOSE_RX, push); }
				//It is never actually necressary to send closed stream error. This is because either
				//we closed this stream, and the other side will eventually get it, or the other
				//side has closed it, in which case this would be error anyway, but we can not
				//close connection, because we can not prove the error due to missing stream
				//state.
			},
			Frame::Headers(f) => {
				//The start of header block always has to be frst.
				push.sink(Element::StartHeaderBlock);
				//Reset the header block state, since new header block starts.
				self.hblock = HeaderBlockState::new();
				if self._need_create_stream(f.strmid)? {
					if no_more_streams || !self.streams.create(f.strmid, &mut self.max_id_even,
						&mut self.max_id_odd, self.draining, self.dflt_tx_credit) {
						//Now, this is odd action. One needs to construct RespondWith that
						//is valid enough to reset the stream, but does not allow any other
						//operations. For this, set close status to FLAG_CLOSED, but not
						//FLAG_CLOSED_FULL used by reset.
						let mut tmp_status = FLAG_CLOSED;
						let (mut dummy1, mut dummy2, mut dummy3) = (0, 0, StreamId::zero());
						push.sink(Element::Abort(&mut RespondWith {
							sid: f.strmid.into_stream_id(),
							peer_frame_limit: 0,
							conn_tx_credit: &mut dummy1,
							strm_tx_credit: &mut dummy2,
							strm_closed: &mut tmp_status,
							tx_continuation_strm: &mut dummy3,
							reaped_id: &mut dummystrm,
						}, StreamAbort::Refused));
					}
					//Still attempt receive even without creating the stream, in order to keep
					//HPACK state in sync.
				}
				//Call start_of_message even if this stream was peviously created and this
				//is the first received HEADERS.
				if let Some(strm) = self.streams.lookup(f.strmid) {
					if !strm.called_start_of_message {
						(ElementDispatch(push, RespondWith {
							sid: f.strmid.into_stream_id(),
							peer_frame_limit: self.peer_frame_limit,
							conn_tx_credit: &mut self.tx_credit,
							strm_tx_credit: &mut strm.tx_credit,
							strm_closed: &mut strm.closed,
							tx_continuation_strm: &mut self.tx_continuation_strm,
							reaped_id: &mut dummystrm,
						})).start_of_message();
						strm.called_start_of_message = true;
					}
				}
				self.eos_continue = f.eos;
				self.hreceiver.reset();
				self.handle_fragment(f.strmid, f.fragment.deref(), f.eoh, true, push)?;
				//If PRIORITY was specified, forward it.
				if let (Some(priority), Some(strm)) = (f.priority, self.streams.lookup(f.strmid)) {
					if strm.closed & FLAG_CLOSE_TX == 0 {
						push.sink(Element::Repriorize(f.strmid.into_inner(),
							priority.dependency.into_inner(),
							priority.exclusive, priority.weight));
					}
				}
			},
			Frame::Priority(f) => if let Some(strm) = self.streams.lookup(f.strmid) {
				if strm.closed & FLAG_CLOSE_TX == 0 {
					push.sink(Element::Repriorize(f.strmid.into_inner(),
						f.priority.dependency.into_inner(),
						f.priority.exclusive, f.priority.weight));
				}
			},
			Frame::RstStream(f) => if self.streams.delete(f.strmid) {
				push.sink(Element::ReceivedReset(f.strmid.into_inner(), f.error));
				push.sink(Element::ReapStream(f.strmid.into_inner()));
			},
			Frame::Settings(f) => {
				for s in f.data.chunks(6) {
					fail_if!(s.len() < 6, InternalError("Failed to read setting"));
					let mut k = [0;2]; k.copy_from_slice(&s[0..2]);
					let mut v = [0;4]; v.copy_from_slice(&s[2..6]);
					let k = u16::from_be_bytes(k);
					let v = u32::from_be_bytes(v);
					match k {
						//SETTINGS_HEADER_TABLE_SIZE.
						1 => fail_if!((v as i32) < 0, IllegalSettingValue(k, v)),
						//SETTINGS_ENABLE_PUSH.
						2 => fail_if!(v > 1 || (!self.server && v != 0),
							IllegalSettingValue(k, v)),
						//SETTINGS_MAX_CONCURRENT_STREAMS.
						3 => (),
						//SETTINGS_INITIAL_WINDOW_SIZE
						4 => {
							let v2 = v as i32;
							fail_if!(v2 < 0, IllegalSettingValue(k, v));
							let delta = v2.checked_sub(self.dflt_tx_credit).
								ok_or(InternalError("Window adjustment overflow"))?;
							self.streams.delta_all_tx_windows(delta);
							self.dflt_tx_credit = v2;
						},
						//SETTINGS_MAX_FRAME_SIZE
						5 => {
							fail_if!(v < 16384 || v > 16777215,
								IllegalSettingValue(k, v));
							self.peer_frame_limit = v as usize;
						},
						_ => (),
					}
					push.sink(Element::Setting(k, v));
				}
				push.sink(Element::SettingsComplete(&mut Acker(&mut self.tx_continuation_strm, None)));
			},
			Frame::SettingsAck => push.sink(Element::SettingsAck),
			Frame::PushPromise(_) => fail!(IllegalPushPromise),
			Frame::Ping(f) => {
				let mut acker = Acker(&mut self.tx_continuation_strm, Some(f.code));
				push.sink(if f.ack {
					Element::Pong(f.code)
				} else {
					Element::Ping(&mut acker, f.code)
				})
			},
			Frame::Goaway(f) => {
				//Immediately tear down all streams and close connection.
				if f.error != 0 {
					//This loop will eventually finish, because each iteration removes one
					//element from streams list, and eventually it will run empty.
					while let Some((strmid, mut strm)) = self.streams.unlink_some() {
						//The structure should never be passed anywhere, so just fill it with
						//values that are valid enough to pass type checks.
						let (mut dummy1, mut dummy2, mut dummy3, mut dummy4) =
							(0, 0, 0, StreamId::zero());
						let mut d = ElementDispatch(push, RespondWith {
							sid: StreamId::new(strmid).unwrap_or(StreamId::zero()),
							peer_frame_limit: 0,
							conn_tx_credit: &mut dummy1,
							strm_tx_credit: &mut dummy2,
							strm_closed: &mut dummy3,
							tx_continuation_strm: &mut dummy4,
							reaped_id: &mut dummystrm,
						});
						//Aborting anyway, so can ignore the errors.
						HttpUtils::abort(&mut d, &mut strm.state,
							StreamAbort::Goaway(f.error)).ok();
					}
					push.sink(Element::Goaway(f.error, f.debug.deref()));
					fail!(Goaway(f.error));
				}
				self.draining = true;
			},
			Frame::WindowUpdate(f) => if let Some(strmid) = f.strmid.into_nonzero() {
				if let Some(strm) = self.streams.lookup(strmid) {
					//Credit the stream.
					strm.tx_credit = strm.tx_credit.checked_add(f.increment.amount()).
						ok_or(UnderflowStreamWindow(strmid.into_inner()))?;
				}
				//We do not send close error as it is not necressary and we can not
				//prove the other end misbehaved either.
			} else {
				//Credit the connection.
				self.tx_credit = self.tx_credit.checked_add(f.increment.amount()).
					ok_or(UnderflowConnectionWindow)?;
			},
			Frame::Continuation(f) => self.handle_fragment(f.strmid, f.fragment.deref(),
				f.eoh, false, push)?,
			Frame::Unknown(s, t, f, p) => push.sink(Element::Unknown(s.into_inner(), t, f, p.deref())),
		}
		Ok(())
	}
	fn abort_all(&mut self, push: &mut impl ElementSink) -> bool
	{
		let mut any = false;
		//element from streams list, and eventually it will run empty.
		while let Some((strmid, mut strm)) = self.streams.unlink_some() {
			any |= true;
			//The structure should never be passed anywhere, so just fill it with values that are
			//valid enough to pass type checks.
			let (mut dummy1, mut dummy2, mut dummy3, mut dummy4, mut dummy5) =
				(0, 0, 0, StreamId::zero(), StreamId::zero());
			let mut d = ElementDispatch(push, RespondWith {
				sid: StreamId::new(strmid).unwrap_or(StreamId::zero()),
				peer_frame_limit: 0,
				conn_tx_credit: &mut dummy1,
				strm_tx_credit: &mut dummy2,
				strm_closed: &mut dummy3,
				tx_continuation_strm: &mut dummy4,
				reaped_id: &mut dummy5,
			});
			//We are aborting anyway, so can ignore errors.
			HttpUtils::abort(&mut d, &mut strm.state, StreamAbort::ConnectionLost).ok();
		}
		any
	}
}

///I/O or stream error.
#[derive(Debug)]
pub enum IoOrStreamError<E:Sized+Debug>
{
	///Stream error.
	Stream(Error),
	///I/O error.
	Io(E),
}

///HTTP/2 receive stream.
pub struct Connection
{
	buffer: [u8;MAX_BUFFER],
	buffer_use: u16,
	next_frame_at: u16,
	is_server: bool,
	preface: u8,
	inner: ConnectionInner,
	error: bool,
	no_more_streams: bool,
}

impl Connection
{
	///Create a new stream.
	pub fn new(server: bool) -> Connection
	{
		Connection {
			buffer: [0;MAX_BUFFER],
			buffer_use: 0,
			next_frame_at: 3,	//Size of length.
			preface: 0,
			is_server: server,
			inner: ConnectionInner::new(server),
			error: false,
			no_more_streams: false,
		}
	}
	///Set use-std flag.
	///
	///This flag causes all standard headers to be sent via HeaderStd/MiddlerStd/TrailerStd elements.
	pub fn set_use_std_flag(&mut self)
	{
		self.inner.hreceiver.use_std = true;
	}
	///Get the amount of RX credit a stream has (or connection in case streamid is 0).
	pub fn get_stream_rx_credit(&self, strmid: StreamId) -> Option<i32>
	{
		let strmid = f_return!(strmid.into_nonzero(), Some(self.inner.rx_credit));
		if let Some(strm) = self.inner.streams.lookup_const(strmid) {
			fail_if_none!(strm.closed & FLAG_CLOSE_RX != 0);	//Closed stream.
			Some(strm.rx_credit)
		} else {
			None	//Stream does not even exist.
		}
	}
	///Get the maximum allowed amount of sent data on given stream.
	pub fn get_max_data_on_stream(&self, strmid: StreamIdNonZero) -> usize
	{
		self.inner.get_max_data_on_stream(strmid)
	}
	///Send SETTINGS frame.
	pub fn send_settings_frame<S:Sink,I:Iterator<Item=(u16,u32)>>(&mut self, sink: &mut S, list: I) ->
		Result<(), TxError<S::Error>>
	{
		self.inner.send_settings_frame(sink, list)
	}
	///Send SETTINGS(ACK) frame
	pub fn send_settings_frame_ack<S:Sink>(&mut self, sink: &mut S) -> Result<(), TxError<S::Error>>
	{
		self.inner.send_settings_frame_ack(sink)
	}
	///Send GOAWAY frame.
	pub fn send_goaway_frame<S:Sink,D:Display>(&mut self, sink: &mut S, maxstrm: StreamId, code: u32,
		debug: &D) -> Result<(), TxError<S::Error>>
	{
		//No more streams allowed.
		self.no_more_streams = true;
		self.inner.send_goaway_frame(sink, maxstrm, code, debug)
	}
	///Send PING frame.
	pub fn send_ping_frame<S:Sink>(&mut self, sink: &mut S, code: u64) -> Result<(), TxError<S::Error>>
	{
		self.inner.send_pingpong_frame(sink, code, false)
	}
	///Send PING(ACK) frame.
	pub fn send_pong_frame<S:Sink>(&mut self, sink: &mut S, code: u64) -> Result<(), TxError<S::Error>>
	{
		self.inner.send_pingpong_frame(sink, code, true)
	}
	///Send WINDOW_UPDATE frame.
	pub fn send_window_update_frame<S:Sink>(&mut self, sink: &mut S, strmid: StreamId,
		increment: WindowIncrement) -> Result<(), TxError<S::Error>>
	{
		self.inner.send_window_update_frame(sink, strmid, increment)
	}
	///Get handle to stream.
	pub fn stream_handle<T:Sized,F>(&mut self, id: StreamIdNonZero, failret: T, func: F) -> T
		where F: FnOnce(RespondWith) -> T
	{
		let mut reaped_id = StreamId::zero();
		let r = if let Some(strm) = self.inner.streams.lookup(id) {
			let r = RespondWith {
				sid: id.into_stream_id(),
				peer_frame_limit: self.inner.peer_frame_limit,
				conn_tx_credit: &mut self.inner.tx_credit,
				strm_tx_credit: &mut strm.tx_credit,
				strm_closed: &mut strm.closed,
				tx_continuation_strm: &mut self.inner.tx_continuation_strm,
				reaped_id: &mut reaped_id,
			};
			func(r)
		} else {
			failret
		};
		//Reap the stream if needed.
		if let Some(strmid) = replace(&mut reaped_id, StreamId::zero()).into_nonzero() {
			self.inner.streams.delete(strmid);
		}
		r
	}
	///Receive data.
	pub fn receive<S:Source, R:ElementSink>(&mut self, source: &mut S, push: &mut R) ->
		Result<bool, IoOrStreamError<S::Error>>
	{
		//Do not proceed on I/O error.
		fail_if!(self.error, IoOrStreamError::Stream(Error(_Error::StreamError)));
		let mut buf = S::Buffer::new();
		let r = match source.read(&mut buf) {
			Ok(true) => self._push(buf.borrow(), push).map(|_|true),
			Ok(false) => if self.inner.abort_all(push) {
				Err(Error(_Error::UnexpectedClose))
			} else {
				Ok(false)
			},
			Err(e) => {
				self.error = true;
				fail!(IoOrStreamError::Io(e));
			}
		};
		source.reclaim_buffer(buf);
		//Ignore return value, because we are already erroring out.
		if r.is_err() { self.inner.abort_all(push); }
		if self.inner.is_drained() { return Ok(false); }
		r.map_err(IoOrStreamError::Stream)
	}
	///Set tracing flag.
	pub fn set_trace_flag(&mut self, trace_flag: bool)
	{
		self.inner.trace_flag = trace_flag;
	}
	fn _push(&mut self, data: &[u8], push: &mut impl ElementSink) -> Result<(), Error>
	{
		use self::_Error::*;
		let mut itr = 0;
		//HTTP/2 preface.
		static PREFIX: &'static [u8] = b"PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n";
		if self.is_server && self.preface < PREFIX.len() as u8 {
			//This loop will end because itr increments by 1 each round, and will eventually hit 24.
			while itr < data.len() && self.preface as usize + itr < PREFIX.len() {
				fail_if!(data.get(itr) != PREFIX.get(self.preface as usize + itr), BadPreface);
				itr += 1;
			}
			self.preface += itr as u8;	//Will be smaller than PREFIX.len() <= 255.
		}
		//If self.buffer_use > 0 and self.next_frame_at == 3, length is incomplete and needs to be filled.
		//It must be that self.buffer_use < 3, because self.buffer_use < self.next_frame_at is invariant
		//upon entry.
		if self.buffer_use > 0 && self.next_frame_at == 3 {
			copy_data_to_frame_buffer!(self, data, itr);
			//Got enough data to read the frame size? If no, end read because we need more data.
			if self.buffer_use == self.next_frame_at { read_frame_size!(self); } else { return Ok(()); }
		}
		//Now if self.buffer_use > 0, the frame body is incomplete and needs to be filled. The previous
		//block ends with self.next_frame_at being at least 9. So self.buffer_use < self.next_frame_at
		//holds here too.
		if self.buffer_use > 0 {
			copy_data_to_frame_buffer!(self, data, itr);
			//Got enough data? If no, end read because we need more data. If yes, dispatch the frame and
			//clear the buffer.
			if self.buffer_use == self.next_frame_at {
				//OK frame complete.
				let f = self.buffer.get(..self.buffer_use as usize).
					ok_or("Frame too big to fit buffer")?;
				self.inner.push_frame(f, push, self.no_more_streams)?;
				//Reset.
				self.buffer_use = 0;
				self.next_frame_at = 3;
			} else {
				return Ok(());
			}
			
		}
		//Read entiere frames. Here it must be that self.buffer_use == 0. Each iteration, the length
		//decrements by at least 9.
		let mut rounds = 0;
		let maxrounds = (data.len() + 7) / 8;
		while let Some(len) = read_be24(data, itr) {
			rounds += 1;
			fail_if!(rounds > maxrounds, "Lockup in frame borrow loop");
			let flen = compute_frame_size!(len);
			if let Some(frame) = data.get(itr..itr+flen) {
				self.inner.push_frame(frame, push, self.no_more_streams)?;
				increment_iterator!(itr, flen);
			} else {
				break;	//No more complete frames.
			}
		}
		//If itr < data.len(), there is pending frame tail that needs to be stored. However, it may be
		//longer than 3 bytes, in which case the length needs to be computed. However, it is guaranteed
		//that self.buffer_use == 0.
		if itr < data.len() {
			copy_data_to_frame_buffer!(self, data, itr);
			//Got enough data to read the frame size? If no, end read because we need more data.
			if self.buffer_use == self.next_frame_at { read_frame_size!(self); } else { return Ok(()); }
			//Fill the body. This can never read enough, because if it did, the above blocks would have
			//got it.
			copy_data_to_frame_buffer!(self, data, itr);
			fail_if!(self.buffer_use < self.next_frame_at, "Should not have filled the buffer");
		}
		//Shut up the waraning.
		let _ = itr;
		Ok(())
	}
	///Set the maximum stream count.
	pub fn set_max_streams(&mut self, max_streams: u32) { self.inner.streams.max_streams = max_streams; }
	///Get the maximum stream count.
	pub fn get_max_streams(&self) -> u32 { self.inner.streams.max_streams }
}

fn read_be24(x: &[u8], y: usize) -> Option<u32>
{
	let z = y.checked_add(3)?;
	let x = x.get(y..z)?;
	Some(x[0] as u32 * 65536 + x[1] as u32 * 256 + x[2] as u32)
}

///Encode HTTP/2 headers.
pub struct HeaderEncoder;

impl EncodeHeaders for HeaderEncoder
{
	//Encode request line.
	fn request_line<S:DSink>(output: &mut S, method: &[u8], scheme: &[u8], authority: &[u8], path: &[u8],
		query: &[u8]) -> Result<(), ()>
	{
		output.write_u8(32)?;	//Compression window 0.
		encode_header_std(output, StandardHttpHeader::Http2MagicHeaderMethod, method)?;
		encode_header_std(output, StandardHttpHeader::Http2MagicHeaderScheme, scheme)?;
		encode_header_std(output, StandardHttpHeader::Http2MagicHeaderAuthority, authority)?;
		hpack_encode_path_concat2(output, path, query)?;
		Ok(())
	}
	///Encode status-line.
	fn status_line<S:DSink>(output: &mut S, status: u16, reason: &[u8]) -> Result<(), ()>
	{
		let status2 = [(48 + status / 100 % 10) as u8, (48 + status / 10 % 10) as u8,
			(48 + status % 10) as u8];
		output.write_u8(32)?;	//Compression window 0.
		encode_header_std(output, StandardHttpHeader::Http2MagicHeaderStatus, &status2)?;
		encode_header_std(output, StandardHttpHeader::XHttp2Reason, reason)?;
		Ok(())
	}
	///Encode a header.
	fn header<S:DSink>(output: &mut S, name: &[u8], value: &[u8]) -> Result<(), ()>
	{
		encode_header(output, name, value)
	}
	///Encode a header.
	fn header_std<S:DSink>(output: &mut S, name: StandardHttpHeader, value: &[u8]) -> Result<(), ()>
	{
		encode_header_std(output, name, value)
	}
	///Encode end of block.
	fn end<S:DSink>(_: &mut S) -> Result<(), ()>
	{
		//Do nothing.
		Ok(())
	}
	///Is H2?
	fn is_h2() -> bool { true }
}

#[cfg(test)]
struct ActionList(Vec<String>);

#[cfg(test)]
impl ElementSink for ActionList
{
	fn protocol_supported(&self, proto: &[u8]) -> bool
	{
		if proto == b"websocket" { return true; }
		false
	}
	fn sink<'b,'a:'b>(&mut self, elem: Element<'b,'a>)
	{
		use crate::escape_bytes;
		match elem {
			Element::Data(reqid, x) => {
				let sid = reqid.get_sid().into_inner();
				let x = escape_bytes(x);
				self.0.push(format!("[{sid}]Data: '{x}'"))
			},
			Element::Status(reqid, x) => {
				let sid = reqid.get_sid().into_inner();
				self.0.push(format!("[{sid}]Status {x}"))
			},
			Element::Method(reqid, x) => {
				let sid = reqid.get_sid().into_inner();
				let x = escape_bytes(x);
				self.0.push(format!("[{sid}]Method {x}"))
			},
			Element::Scheme(reqid, x) => {
				let sid = reqid.get_sid().into_inner();
				let x = escape_bytes(x);
				self.0.push(format!("[{sid}]Scheme {x}"))
			},
			Element::Authority(reqid, x) => {
				let sid = reqid.get_sid().into_inner();
				let x = escape_bytes(x);
				self.0.push(format!("[{sid}]Authority {x}"))
			},
			Element::AuthorityRaw(reqid, x) => {
				let sid = reqid.get_sid().into_inner();
				let x = escape_bytes(x);
				self.0.push(format!("[{sid}]AuthorityRaw {x}"))
			},
			Element::ContentLength(reqid, x) => {
				let sid = reqid.get_sid().into_inner();
				self.0.push(format!("[{sid}]Content-Length {x}"))
			},
			Element::Path(reqid, x) => {
				let sid = reqid.get_sid().into_inner();
				let x = escape_bytes(x);
				self.0.push(format!("[{sid}]Path {x}"))
			},
			Element::PathRaw(reqid, x) => {
				let sid = reqid.get_sid().into_inner();
				let x = escape_bytes(x);
				self.0.push(format!("[{sid}]PathRaw {x}"))
			},
			Element::Protocol(reqid, x) => {
				let sid = reqid.get_sid().into_inner();
				let x = escape_bytes(x);
				self.0.push(format!("[{sid}]Protocol {x}"))
			},
			Element::Query(reqid, x) => {
				let sid = reqid.get_sid().into_inner();
				let x = escape_bytes(x);
				self.0.push(format!("[{sid}]Query {x}"))
			},
			Element::Reason(reqid, x) => {
				let sid = reqid.get_sid().into_inner();
				let x = escape_bytes(x);
				self.0.push(format!("[{sid}]Reason {x}"))
			},
			Element::Version(reqid, x) => {
				let sid = reqid.get_sid().into_inner();
				let x = escape_bytes(x);
				self.0.push(format!("[{sid}]Version {x}"))
			},
			Element::Header(reqid, x, y) => {
				let sid = reqid.get_sid().into_inner();
				let x = escape_bytes(x);
				let y = escape_bytes(y);
				self.0.push(format!("[{sid}]Header: '{x}: {y}'"))
			},
			Element::Middler(reqid, x, y) => {
				let sid = reqid.get_sid().into_inner();
				let x = escape_bytes(x);
				let y = escape_bytes(y);
				self.0.push(format!("[{sid}]Middler: '{x}: {y}'"))
			},
			Element::EndOfHeadersInterim(reqid) => {
				let sid = reqid.get_sid().into_inner();
				self.0.push(format!("[{sid}]End of interim headers"))
			},
			Element::EndOfHeaders(reqid) => {
				let sid = reqid.get_sid().into_inner();
				self.0.push(format!("[{sid}]End of headers"))
			},
			Element::EndOfMessage(reqid) => {
				let sid = reqid.get_sid().into_inner();
				self.0.push(format!("[{sid}]End of message"))
			},
			Element::EndOfMiddlers(reqid) => {
				let sid = reqid.get_sid().into_inner();
				self.0.push(format!("[{sid}]End of middlers"))
			},
			Element::StartOfMessage(reqid) => {
				let sid = reqid.get_sid().into_inner();
				self.0.push(format!("[{sid}]Start of message"))
			},
			Element::StartHeaderBlock => self.0.push(format!("Start header block")),
			Element::Trailer(reqid, x, y) => {
				let sid = reqid.get_sid().into_inner();
				let x = escape_bytes(x);
				let y = escape_bytes(y);
				self.0.push(format!("[{sid}]Trailer: '{x}: {y}'"))
			},
			Element::Repriorize(reqid, x, y, z) =>self.0.push(format!("[{reqid}]Priority({x},{y},{z})'")),
			Element::HeadersFaulty(reqid, x) => {
				let sid = reqid.get_sid().into_inner();
				self.0.push(format!("[{sid}]Headers faulty({x})"))
			},
			Element::Abort(reqid, x) => {
				let sid = reqid.get_sid().into_inner();
				self.0.push(format!("[{sid}]Abort({x})"))
			},
			Element::Ping(_, x) => self.0.push(format!("PING {x}")),
			Element::Pong(x) => self.0.push(format!("PONG {x}")),
			Element::Goaway(x, y) => {
				let y = escape_bytes(y);
				self.0.push(format!("GOAWAY {x}/{y}"))
			},
			Element::Setting(x, y) => self.0.push(format!("SETTING {x}={y}")),
			Element::SettingsComplete(_) => self.0.push(format!("SETTINGS COMPLETE")),
			Element::SettingsAck => self.0.push(format!("SETTINGS ACK")),
			Element::Unknown(x, y, z, w) => self.0.push(format!(
				"Unknown frame: strm={x}, type={y}, flags={z}, data={w:?}")),
			_ => self.0.push(format!("UNKNOWN[{elem:?}]")),
		}
	}
}

#[cfg(test)]
fn string_list(x: &[String], y: &[&str])
{
	let mut p = 0;
	for (a, b) in x.iter().zip(y.iter().cloned()) {
		if a.deref() != b {
			println!("Parser emitted '{a}', reference solution has '{b}' (position {p})");
			assert!(false);
		}
		p += 1;
	}
	if x.len() < y.len() {
		let next = y[x.len()];
		println!("Parser ended, reference solution still has '{next}'");
		assert!(false);
	} else if x.len() > y.len() {
		let next = &x[y.len()];
		println!("Parser emitted '{next}', but reference solution has nothing");
		assert!(false);
	}
}

#[cfg(test)]
struct Buffer(Vec<u8>);

#[cfg(test)]
impl ReceiveBuffer for Buffer
{
	fn new() -> Self { Buffer(Vec::new()) }
	fn borrow<'a>(&'a mut self) -> &'a mut [u8] { &mut self.0 }
}

#[cfg(test)]
struct StaticString<'a>(&'a [u8], bool);

#[cfg(test)]
impl<'a> Source for StaticString<'a>
{
	type Buffer = Buffer;
	type Error = ();
	fn read(&mut self, buf: &mut Buffer) -> Result<bool, ()>
	{
		buf.0 = self.0.to_owned();
		let flag = replace(&mut self.1, false);
		Ok(flag)
	}
}

#[cfg(test)]
fn strsrc<'a>(x: &'a [u8]) -> StaticString<'a> { StaticString(x, true) }


#[cfg(test)]
fn send_frame(s: &mut Connection, a: &mut ActionList, frame: Frame)
{
	let mut buf = [0;17000];
	let len = frame.serialize(&mut buf).unwrap();
	s.receive(&mut strsrc(&buf[..len]), a).unwrap();
}

#[cfg(test)]
fn send_frame_settings(s: &mut Connection, a: &mut ActionList)
{
	send_frame(s, a, Frame::Settings(crate::http2_frame::SettingsFrame{
		data: Cow::Borrowed(&b"\x00\x03\x00\x00\x00\x80"[..]),
	}));
}

#[cfg(test)]
fn send_frame_headers(s: &mut Connection, a: &mut ActionList, strmid: u32, frag: &[u8], eos: bool)
{
	send_frame(s, a, Frame::Headers(crate::http2_frame::HeadersFrame{
		strmid: StreamIdNonZero::new(strmid).unwrap(),
		eoh: true,
		eos: eos,
		priority: None,
		padding: crate::http2_frame::PaddingAmount::new(0).unwrap(),
		fragment: Cow::Borrowed(frag),
	}));
}

#[cfg(test)]
fn send_frame_data(s: &mut Connection, a: &mut ActionList, strmid: u32, frag: &[u8], eos: bool)
{
	send_frame(s, a, Frame::Data(crate::http2_frame::DataFrame{
		strmid: StreamIdNonZero::new(strmid).unwrap(),
		eos: eos,
		payload: Cow::Borrowed(frag),
		padding: crate::http2_frame::PaddingAmount::new(0).unwrap(),
	}));
}


#[test]
fn connect_to_server()
{
	let mut a = ActionList(Vec::new());
	let mut s = Connection::new(true);
	s.receive(&mut strsrc(b"PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n\0\0\x06\x04\0\0\0\0\0\0\x03\0\0\0\x80"), &mut a).
		unwrap();
	string_list(&a.0, &[
		"SETTING 3=128",
		"SETTINGS COMPLETE",
	]);
}

#[test]
fn connect_to_server_split()
{
	let mut a = ActionList(Vec::new());
	let mut s = Connection::new(true);
	s.receive(&mut strsrc(b"PRI *"), &mut a).unwrap();
	s.receive(&mut strsrc(b" HTTP/2.0\r"), &mut a).unwrap();
	s.receive(&mut strsrc(b"\n\r\nSM\r\n\r\n\0"), &mut a).unwrap();
	s.receive(&mut strsrc(b"\0\x06\x04\0\0\0\0"), &mut a).unwrap();
	s.receive(&mut strsrc(b"\0\0\x03\0\0\0\x80"), &mut a).unwrap();
	string_list(&a.0, &[
		"SETTING 3=128",
		"SETTINGS COMPLETE",
	]);
}

#[test]
fn simple_request()
{
	let mut a = ActionList(Vec::new());
	let mut s = Connection::new(true);
	s.receive(&mut strsrc(b"PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n"), &mut a).unwrap();
	send_frame_settings(&mut s, &mut a);
	send_frame_headers(&mut s, &mut a, 1, b"\x82\x01\x03foo\x85\x87", true);
	string_list(&a.0, &[
		"SETTING 3=128",
		"SETTINGS COMPLETE",
		"Start header block",
		"[1]Start of message",
		"[1]Version HTTP/2",
		"[1]Method GET",
		"[1]AuthorityRaw foo",
		"[1]Authority foo",
		"[1]PathRaw /index.html",
		"[1]Path /index.html",
		"[1]Scheme https",
		"[1]End of message",
	]);
}

#[test]
fn host_smuggling()
{
	let mut a = ActionList(Vec::new());
	let mut s = Connection::new(true);
	s.receive(&mut strsrc(b"PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n"), &mut a).unwrap();
	send_frame_settings(&mut s, &mut a);
	send_frame_headers(&mut s, &mut a, 1, b"\x82\x01\x03foo\x85\x87\x0f\x17\x03bar", true);
	string_list(&a.0, &[
		"SETTING 3=128",
		"SETTINGS COMPLETE",
		"Start header block",
		"[1]Start of message",
		"[1]Version HTTP/2",
		"[1]Method GET",
		"[1]AuthorityRaw foo",
		"[1]Authority foo",
		"[1]PathRaw /index.html",
		"[1]Path /index.html",
		"[1]Scheme https",
		"[1]Abort(Illegal authority: Inconsistent with previous value)",
	]);
}

#[test]
fn eat_host()
{
	let mut a = ActionList(Vec::new());
	let mut s = Connection::new(true);
	s.receive(&mut strsrc(b"PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n"), &mut a).unwrap();
	send_frame_settings(&mut s, &mut a);
	send_frame_headers(&mut s, &mut a, 1, b"\x82\x01\x03foo\x85\x87\x0f\x17\x03foo", true);
	string_list(&a.0, &[
		"SETTING 3=128",
		"SETTINGS COMPLETE",
		"Start header block",
		"[1]Start of message",
		"[1]Version HTTP/2",
		"[1]Method GET",
		"[1]AuthorityRaw foo",
		"[1]Authority foo",
		"[1]PathRaw /index.html",
		"[1]Path /index.html",
		"[1]Scheme https",
		"[1]End of message",
	]);
}

#[test]
fn two_hosts()
{
	let mut a = ActionList(Vec::new());
	let mut s = Connection::new(true);
	s.receive(&mut strsrc(b"PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n"), &mut a).unwrap();
	send_frame_settings(&mut s, &mut a);
	send_frame_headers(&mut s, &mut a, 1, b"\x82\x01\x03foo\x85\x87\x0f\x17\x03foo\x0f\x17\x03foo", true);
	string_list(&a.0, &[
		"SETTING 3=128",
		"SETTINGS COMPLETE",
		"Start header block",
		"[1]Start of message",
		"[1]Version HTTP/2",
		"[1]Method GET",
		"[1]AuthorityRaw foo",
		"[1]Authority foo",
		"[1]PathRaw /index.html",
		"[1]Path /index.html",
		"[1]Scheme https",
		"[1]Abort(Multiple Host headers)",
	]);
}

#[test]
fn simple_get_payload()
{
	let mut a = ActionList(Vec::new());
	let mut s = Connection::new(true);
	s.receive(&mut strsrc(b"PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n"), &mut a).unwrap();
	send_frame_settings(&mut s, &mut a);
	send_frame_headers(&mut s, &mut a, 1, b"\x82\x01\x03foo\x85\x87", false);
	send_frame_data(&mut s, &mut a, 1, b"foo", false);
	send_frame_headers(&mut s, &mut a, 1, b"\x00\x03bar\x04test", true);
	string_list(&a.0, &[
		"SETTING 3=128",
		"SETTINGS COMPLETE",
		"Start header block",
		"[1]Start of message",
		"[1]Version HTTP/2",
		"[1]Method GET",
		"[1]AuthorityRaw foo",
		"[1]Authority foo",
		"[1]PathRaw /index.html",
		"[1]Path /index.html",
		"[1]Scheme https",
		"[1]End of headers",
		"[1]Data: 'foo'",
		"Start header block",
		"[1]Trailer: 'bar: test'",
		"[1]End of message",
	]);
}

#[test]
fn simple_get_payload_middlers()
{
	let mut a = ActionList(Vec::new());
	let mut s = Connection::new(true);
	s.receive(&mut strsrc(b"PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n"), &mut a).unwrap();
	send_frame_settings(&mut s, &mut a);
	send_frame_headers(&mut s, &mut a, 1, b"\x82\x01\x03foo\x85\x87", false);
	send_frame_data(&mut s, &mut a, 1, b"foo", false);
	send_frame_headers(&mut s, &mut a, 1, b"\x00\x03bar\x04test", false);
	send_frame_data(&mut s, &mut a, 1, b"qux", false);
	send_frame_headers(&mut s, &mut a, 1, b"\x00\x03zot\x04test", true);
	string_list(&a.0, &[
		"SETTING 3=128",
		"SETTINGS COMPLETE",
		"Start header block",
		"[1]Start of message",
		"[1]Version HTTP/2",
		"[1]Method GET",
		"[1]AuthorityRaw foo",
		"[1]Authority foo",
		"[1]PathRaw /index.html",
		"[1]Path /index.html",
		"[1]Scheme https",
		"[1]End of headers",
		"[1]Data: 'foo'",
		"Start header block",
		"[1]Middler: 'bar: test'",
		"[1]End of middlers",
		"[1]Data: 'qux'",
		"Start header block",
		"[1]Trailer: 'zot: test'",
		"[1]End of message",
	]);
}

#[test]
fn request_error_sane()
{
	let mut a = ActionList(Vec::new());
	let mut s = Connection::new(true);
	s.receive(&mut strsrc(b"PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n"), &mut a).unwrap();
	send_frame_settings(&mut s, &mut a);
	send_frame_headers(&mut s, &mut a, 1, b"\x82\x85\x87", false);
	send_frame_data(&mut s, &mut a, 1, b"foo", true);
	send_frame_headers(&mut s, &mut a, 3, b"\x82\x01\x03foo\x85\x87", true);
	string_list(&a.0, &[
		"SETTING 3=128",
		"SETTINGS COMPLETE",
		"Start header block",
		"[1]Start of message",
		"[1]Version HTTP/2",
		"[1]Method GET",
		"[1]PathRaw /index.html",
		"[1]Path /index.html",
		"[1]Scheme https",
		"[1]Abort(No authority)",
		"Start header block",
		"[3]Start of message",
		"[3]Version HTTP/2",
		"[3]Method GET",
		"[3]AuthorityRaw foo",
		"[3]Authority foo",
		"[3]PathRaw /index.html",
		"[3]Path /index.html",
		"[3]Scheme https",
		"[3]End of message",
	]);
}

#[test]
fn response_error_sane()
{
	let mut a = ActionList(Vec::new());
	let mut s = Connection::new(false);
	send_frame_settings(&mut s, &mut a);
	send_frame_headers(&mut s, &mut a, 2, b"\x88\x00\x11transfer-encoding\x03foo\x5c\x019", false);
	send_frame_data(&mut s, &mut a, 2, b"foo", true);
	send_frame_headers(&mut s, &mut a, 4, b"\x88", false);
	send_frame_data(&mut s, &mut a, 4, b"foo", true);
	string_list(&a.0, &[
		"SETTING 3=128",
		"SETTINGS COMPLETE",
		"Start header block",
		"[2]Start of message",
		"[2]Version HTTP/2",
		"[2]Status 200",
		"[2]Abort(Header transfer-encoding not allowed in HTTP/2)",
		"Start header block",
		"[4]Start of message",
		"[4]Version HTTP/2",
		"[4]Status 200",
		"[4]End of headers",
		"[4]Data: 'foo'",
		"[4]End of message",
	]);
}

#[test]
fn simple_get_response_multiple_headers_not_interim()
{
	let mut a = ActionList(Vec::new());
	let mut s = Connection::new(false);
	send_frame_settings(&mut s, &mut a);
	send_frame_headers(&mut s, &mut a, 2, b"\x88", false);
	send_frame_headers(&mut s, &mut a, 2, b"\x88", false);
	send_frame_data(&mut s, &mut a, 1, b"foo", true);
	string_list(&a.0, &[
		"SETTING 3=128",
		"SETTINGS COMPLETE",
		"Start header block",
		"[2]Start of message",
		"[2]Version HTTP/2",
		"[2]Status 200",
		"[2]End of headers",
		"Start header block",
		"[2]Abort(Illegal header after final headers)",
	]);
}

#[test]
fn simple_get_response_interim()
{
	let mut a = ActionList(Vec::new());
	let mut s = Connection::new(false);
	send_frame_settings(&mut s, &mut a);
	send_frame_headers(&mut s, &mut a, 2, b"\x08\x03100", false);
	send_frame_headers(&mut s, &mut a, 2, b"\x88", false);
	send_frame_data(&mut s, &mut a, 2, b"foo", true);
	string_list(&a.0, &[
		"SETTING 3=128",
		"SETTINGS COMPLETE",
		"Start header block",
		"[2]Start of message",
		"[2]Version HTTP/2",
		"[2]Status 100",
		"[2]End of interim headers",
		"Start header block",
		"[2]Version HTTP/2",
		"[2]Status 200",
		"[2]End of headers",
		"[2]Data: 'foo'",
		"[2]End of message",
	]);
}

#[test]
fn simple_get_response_length()
{
	let mut a = ActionList(Vec::new());
	let mut s = Connection::new(false);
	send_frame_settings(&mut s, &mut a);
	send_frame_headers(&mut s, &mut a, 2, b"\x88\x5c\x013", false);
	send_frame_data(&mut s, &mut a, 2, b"foo", true);
	string_list(&a.0, &[
		"SETTING 3=128",
		"SETTINGS COMPLETE",
		"Start header block",
		"[2]Start of message",
		"[2]Version HTTP/2",
		"[2]Status 200",
		"[2]Content-Length 3",
		"[2]Header: 'content-length: 3'",
		"[2]End of headers",
		"[2]Data: 'foo'",
		"[2]End of message",
	]);
}

#[test]
fn test_websockets_basic_protocol()
{
	let mut a = ActionList(Vec::new());
	let mut s = Connection::new(false);
	send_frame_settings(&mut s, &mut a);
	send_frame_headers(&mut s, &mut a, 2, b"\x02\x07CONNECT\x00\x09:protocol\x09websocket\x01\x03foo\x84",
		false);
	send_frame_data(&mut s, &mut a, 2, b"foo", false);
	send_frame_data(&mut s, &mut a, 2, b"bar", true);
	string_list(&a.0, &[
		"SETTING 3=128",
		"SETTINGS COMPLETE",
		"Start header block",
		"[2]Start of message",
		"[2]Version HTTP/2",
		"[2]Method CONNECT",
		"[2]Protocol websocket",
		"[2]AuthorityRaw foo",
		"[2]Authority foo",
		"[2]PathRaw /",
		"[2]Path /",
		"[2]End of headers",
		"[2]Data: 'foo'",
		"[2]Data: 'bar'",
		"[2]End of message",
	]);
}

#[test]
fn test_unknown_basic_protocol()
{
	let mut a = ActionList(Vec::new());
	let mut s = Connection::new(false);
	send_frame_settings(&mut s, &mut a);
	send_frame_headers(&mut s, &mut a, 2, b"\x02\x07CONNECT\x00\x09:protocol\x03foo\x01\x03foo\x84",
		false);
	send_frame_data(&mut s, &mut a, 2, b"foo", false);
	send_frame_data(&mut s, &mut a, 2, b"bar", true);
	string_list(&a.0, &[
		"SETTING 3=128",
		"SETTINGS COMPLETE",
		"Start header block",
		"[2]Start of message",
		"[2]Version HTTP/2",
		"[2]Method CONNECT",
		"[2]Abort(Unsupported protocol)",
	]);
}

#[cfg(test)]
fn headercompressor_ok(data: &[u8], reference: &[&[u8]])
{
	let mut itr1 = 0;
	let mut itr2 = 0;
	let mut ctx = Context::new();
	let mut d = Decoder::new();
	d.push(data, &mut ctx, |sym|{
		match sym {
			PushElement::StdName(s) => {
				assert_eq!(s.to_str().as_bytes(), reference[itr1]);
				itr1 += 1;
			},
			PushElement::Bytes(b) => for &b in b.iter() {
				assert_eq!(b, reference[itr1][itr2]);
				itr2 += 1;
			},
			PushElement::EndOfString => {
				assert_eq!(itr2, reference[itr1].len());
				itr1 += 1;
				itr2 = 0;
			},
			PushElement::EndOfCollection => assert!(false),
		};
	}).unwrap();
	let mut last: PushElement<'static> = PushElement::EndOfString;
	d.end(|v|last=v).unwrap();
	assert_eq!(itr1, reference.len());
	assert_eq!(last, PushElement::EndOfCollection);
}


#[test]
fn encode_request()
{
	let mut s = [0;512];
	let slen = {
		let mut s = SliceSink::new(&mut s);
		HeaderEncoder::request_line(&mut s, b"GET", b"https", b"foo.example", b"/bar", b"?zot").unwrap();
		HeaderEncoder::header(&mut s, b"abc", b"xyzw").unwrap();
		HeaderEncoder::end(&mut s).unwrap();
		s.written()
	};
	let s = &s[..slen];
	headercompressor_ok(s, &[b":method", b"GET", b":scheme", b"https", b":authority", b"foo.example",
		b":path", b"/bar?zot", b"abc", b"xyzw"]);
}

#[test]
fn encode_reply()
{
	let mut s = [0;512];
	let slen = {
		let mut s = SliceSink::new(&mut s);
		HeaderEncoder::status_line(&mut s, 245, b"xyzzy").unwrap();
		HeaderEncoder::header(&mut s, b"abc", b"xyzw").unwrap();
		HeaderEncoder::end(&mut s).unwrap();
		s.written()
	};
	let s = &s[..slen];
	headercompressor_ok(s, &[b":status", b"245", b"x-http2-reason", b"xyzzy", b"abc", b"xyzw"]);
}
