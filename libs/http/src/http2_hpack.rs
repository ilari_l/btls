//!HTTP/2 HPACK.
#![forbid(unused_must_use)]
#![forbid(missing_docs)]

use btls_aux_fail::f_return;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_serialization::Sink;
#[cfg(test)] use btls_aux_serialization::SliceSink;
use core::cmp::min;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::ops::Add;
use core::ops::Sub;
#[cfg(test)] use std::vec::Vec;
#[cfg(test)] use std::string::String;
#[cfg(test)] use std::cell::RefCell;


#[doc(hidden)]
pub const MAX_TABLESIZE: usize = 4096;		//MUST be at least 4096 and at most 65536.
const MAX_ENTRIES: usize = MAX_TABLESIZE / 32;

#[allow(dead_code)] #[deny(const_err)]
const ASSERT_MAX_TABLESIZE_MIN: usize = MAX_TABLESIZE - 4096;
#[allow(dead_code)] #[deny(const_err)]
const ASSERT_MAX_TABLESIZE_MAX: usize = 65536 - MAX_TABLESIZE;

///Error in HPACK processing.
///
///All errors are fatal to the connection.
#[derive(Copy,Clone,Debug)]
pub struct Error(_Error);

#[derive(Copy,Clone,Debug)]
enum _Error
{
	IllegalHuffmanSequence,
	IllegalOpcode128,
	IllegalReference2(usize, usize),
	IllegalTableSize(usize, usize),
	IntBadSliceLeft,
	IntegerTooBig,
	IntIteratorOF,
	IntIteratorOOR,
	IntNotStatic(usize),
	IntReadTooMuch,
	TruncatedHeaderBlock,
	TruncatedHuffman,
}

impl Display for Error
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::_Error::*;
		match self.0 {
			//The only illegal sequence is one with EOS in it.
			IllegalHuffmanSequence => write!(f, "Illegal EOS in huffman coded sequence"),
			IllegalOpcode128 => write!(f, "Illegal HPACK opcode 128"),
			IllegalReference2(idx, limit) => write!(f, "Illegal reference to index {idx} >= {limit}"),
			IllegalTableSize(size, limit) => write!(f, "Illegal compression window {size} > {limit}"),
			IntBadSliceLeft => write!(f, "Internal error: Can't slice smaller of left or buffer"),
			IntegerTooBig => write!(f, "Integer encoding too big"),
			IntIteratorOF => write!(f, "Internal error: Iterator overflow"),
			IntIteratorOOR => write!(f, "Internal error: Iterator out of range"),
			IntNotStatic(num) => write!(f, "Internal error: Entry {num} should be static, but is not"),
			IntReadTooMuch => write!(f, "Internal error: Read too much"),
			TruncatedHeaderBlock => write!(f, "Header block truncated"),
			TruncatedHuffman => write!(f, "Truncated huffman coded sequence"),
		}
	}
}

#[derive(Copy,Clone,PartialEq,Eq,Debug)]
struct ModTS(u16);

impl ModTS
{
	fn get(&self, arr: &[u8;MAX_TABLESIZE]) -> u8 { arr[self.0 as usize % MAX_TABLESIZE] }
	fn put(&self, arr: &mut [u8;MAX_TABLESIZE], val: u8) { arr[self.0 as usize % MAX_TABLESIZE] = val; }
}

impl Add<u32> for ModTS
{
	type Output = ModTS;
	fn add(self, rhs: u32) -> ModTS
	{
		let max = MAX_TABLESIZE as u32;
		let nval = (self.0 as u32 + rhs) % max;
		ModTS(nval as u16)
	}
}

#[derive(Copy,Clone,PartialEq,Eq,Debug)]
struct ModTE(u16);

impl ModTE
{
	fn get(&self, arr: &[ModTS;MAX_ENTRIES]) -> ModTS { arr[self.0 as usize % MAX_ENTRIES] }
	fn put(&self, arr: &mut [ModTS;MAX_ENTRIES], val: ModTS) { arr[self.0 as usize % MAX_ENTRIES] = val; }
}

impl Add<u16> for ModTE
{
	type Output = ModTE;
	fn add(self, rhs: u16) -> ModTE
	{
		let max = MAX_ENTRIES as u32;
		let nval = (self.0 as u32 + rhs as u32) % max;
		ModTE(nval as u16)
	}
}

impl Sub<u16> for ModTE
{
	type Output = ModTE;
	fn sub(self, rhs: u16) -> ModTE
	{
		let max = MAX_ENTRIES as u32;
		let nval = (self.0 as u32 + max * 65536 - rhs as u32) % max;
		ModTE(nval as u16)
	}
}

fn tab_load16(table: &[u8;MAX_TABLESIZE], offset: ModTS) -> u16
{
	(offset+0).get(table) as u16 + ((offset+1).get(table) as u16) * 256
}

fn tab_store16(table: &mut [u8;MAX_TABLESIZE], offset: ModTS, val: u16)
{
	(offset+0).put(table, val as u8);
	(offset+1).put(table, (val >> 8) as u8);
}

#[cfg(test)]
fn tab_store8inc(table: &mut [u8;MAX_TABLESIZE], offset: &mut ModTS, val: u8)
{
	offset.put(table, val);
	*offset = *offset + 1;
}

fn encode_one_single(output: &mut impl Sink, s: &[u8]) -> Result<(), ()>
{
	//First check the cache.
	let mut magic = 0u32;
	for c in s.iter() { magic = (magic << 8).wrapping_add(magic).wrapping_add(*c as u32); }
	if let Ok(eidx) = HUFFMAN_CACHE.binary_search_by(|k|k.0.cmp(&magic)) {
		match HUFFMAN_CACHE.get(eidx) {
			Some(&(_, o, r)) if s == o => return output.write_slice(r),
			_ => (),	//No match.
		}
	}
	__encode_hpack_single(output, s)
}

fn encode_one(output: &mut impl Sink, s: &[&[u8]]) -> Result<(), ()>
{
	let slen = s.iter().fold(0usize,|x,y|x+y.len());
	let hlen = bits_to_bytes(s.iter().fold(0usize,|x,y|x+hufflen_bits(y)));
	if slen > hlen {
		write_string_length_field(output, hlen, true)?;
		let mut accumulator = 1;
		for e in s.iter() { encode_huffman_str(output, &mut accumulator, e)?; }
		write_huffman_tail(output, accumulator)
	} else {
		write_string_length_field(output, slen, false)?;
		for e in s.iter() { output.write_slice(e)?; }
		Ok(())
	}
}


fn hufflen_bits(s: &[u8]) -> usize
{
	s.iter().fold(0usize, |x,&y|x+HUFFMAN_LENTAB2[y as usize] as usize)
}

fn bits_to_bytes(ans: usize) -> usize { (ans + 7) / 8 }

fn encode_huffman_str(output: &mut impl Sink, accumulator: &mut u8, s: &[u8]) -> Result<(), ()>
{
	let mut master_bits = 7 - accumulator.leading_zeros();
	let mut master_accum = *accumulator as u64 & ((1 << master_bits) - 1);
	for &c in s.iter() {
		let code = HUFFMAN_ENCTAB3[c as usize] as u64;
		let cbits = HUFFMAN_LENTAB2[c as usize] as u32;
		//println!("Encode {c} as {code:b}({cbits})");
		master_accum = master_accum << cbits | code;
		master_bits += cbits;
		//println!("Accumulated {master_accum:b}({master_bits})");
		//If 32 bits is full, flush it to output.
		if master_bits >= 32 {
			master_bits -= 32;
			let flush = (master_accum >> master_bits) as u32;
			output.write_slice(&flush.to_be_bytes())?;
			//println!("Deaccumulated {master_accum:b}({master_bits}) flush {flush:032b}");
		}
	}
	//Flush any complete bytes to output.
	while master_bits >= 8 {
		master_bits -= 8;
		let flush = (master_accum >> master_bits) as u8;
		output.write_u8(flush)?;
		//println!("Deaccumulated {master_accum:b}({master_bits}) flush {flush:08b}");
	}
	*accumulator = master_accum as u8;
	*accumulator &= (1 << master_bits) - 1;
	*accumulator |= 1 << master_bits;
	Ok(())
}

fn write_string_length_field(output: &mut impl Sink, slen: usize, huffman: bool) -> Result<(), ()>
{
	if slen < 127 {
		output.write_u8(slen as u8 | if huffman { 128 } else { 0 })?;
	} else {
		let mut x = slen - 127;
		output.write_u8(if huffman { 255 } else { 127 })?;
		while x >= 128 {
			output.write_u8(128 | x as u8)?;
			x >>= 7;
		}
		output.write_u8(x as u8)?;
	}
	Ok(())
}

fn write_huffman_tail(output: &mut impl Sink, accumulator: u8) -> Result<(), ()>
{
	//If incomplete character, emit last character.
	match accumulator {
		2..=3 => output.write_u8((accumulator << 7) | 0x7F),
		4..=7 => output.write_u8((accumulator << 6) | 0x3F),
		8..=15 => output.write_u8((accumulator << 5) | 0x1F),
		16..=31 => output.write_u8((accumulator << 4) | 0xF),
		32..=63 => output.write_u8((accumulator << 3) | 7),
		64..=127 => output.write_u8((accumulator << 2) | 3),
		128..=255 => output.write_u8((accumulator << 1) | 1),
		_ => Ok(()),	//Last byte complete.
	}
}

//This is exposed only for benchmarking.
#[doc(hidden)]
pub fn __encode_hpack_hard(output: &mut impl Sink, s: &[u8]) -> Result<(), ()>
{
	let mut accumulator = 1;
	encode_huffman_str(output, &mut accumulator, s)?;
	write_huffman_tail(output, accumulator)
}

//This is exposed only for benchmarking.
#[doc(hidden)]
pub fn __encode_hpack_single(output: &mut impl Sink, s: &[u8]) -> Result<(), ()>
{
	let slen = s.len();
	let hlen = bits_to_bytes(hufflen_bits(s));
	if slen > hlen {
		write_string_length_field(output, hlen, true)?;
		let mut accumulator = 1;
		encode_huffman_str(output, &mut accumulator, s)?;
		write_huffman_tail(output, accumulator)
	} else {
		write_string_length_field(output, slen, false)?;
		output.write_slice(s)
	}
}

const ENTRY_HEADER_LEN: u32 = 32;
const ENTRY_NLEN_OFF: u32 = 0;
const ENTRY_VLEN_OFF: u32 = 2;
const ENTRY_NID_OFF: u32 = 4;

///HPACK decompression context.
///
///This structure contains the parts of HPACK state that are kept from request to request within lifetime of a
///connection.
///
///This structure does not provode any interesting methods, it is exclusively used by passing it to
///[`Decoder`](struct.Decoder.html).
pub(crate) struct Context
{
	buffer: [u8;MAX_TABLESIZE],
	entries: [ModTS;MAX_ENTRIES],
	first: ModTE,			//Number of first entry.
	count: u16,			//Count of entries.
	tptr: ModTS,			//The tail pointer.
	wptr: ModTS,			//The write/head pointer.
	window: u16,			//Window size.
	eptr: ModTS,			//Entry pointer.
}

impl Context
{
	///Create a new context.
	pub fn new() -> Context
	{
		Context {
			buffer: [0;MAX_TABLESIZE],
			entries: [ModTS(0);MAX_ENTRIES],
			first: ModTE(0),
			count: 0,
			tptr: ModTS(0),
			wptr: ModTS(0),
			eptr: ModTS(0),
			window: 4096,	//HTTP/2 default.
		}
	}
	fn get_entry(&self, entry: usize) -> Result<(Option<StandardHttpHeader>, ModTS, u32, ModTS, u32), Error>
	{
		//Check that the entry is valid!
		if entry >= self.count as usize {
			let real_index = entry.saturating_add(62);
			let real_limit = self.count as usize + 62;
			fail!(Error(_Error::IllegalReference2(real_index, real_limit)));
		}
		//Load the name pointer and name length.
		let entry = (self.first+entry as u16).get(&self.entries);
		let namelen = tab_load16(&self.buffer, entry + ENTRY_NLEN_OFF) as u32;
		let valuelen = tab_load16(&self.buffer, entry + ENTRY_VLEN_OFF) as u32;
		let nid = StandardHttpHeader::from_id(tab_load16(&self.buffer, entry + ENTRY_NID_OFF));
		let name = entry + ENTRY_HEADER_LEN;
		let value = name + namelen;
		Ok((nid, name, namelen, value, valuelen))
	}
	fn push_string<F>(&self, base: ModTS, length: u32, sink: &mut F) where F: FnMut(PushElement)
	{
		let mut base2 = base;
		let mut itr = 0;
		while itr < length {
			//Invariant: base.0 < MAX_TABLESIZE. And itr < length by check above.
			let maxlen = min(MAX_TABLESIZE as u32 - base2.0 as u32, length as u32 - itr);
			//base2.0 + maxlen <= MAX_TABLESIZE.
			sink(PushElement::Bytes(&self.buffer[base2.0 as usize..][..maxlen as usize]));
			base2 = base2 + maxlen;
			itr += maxlen;
		}
		sink(PushElement::EndOfString);
	}
	fn count_stuff(&self) -> usize
	{
		//If tptr == wptr, one can tell if is 0 or MAX_TABLESIZE by count.
		if self.tptr == self.wptr { return if self.count > 0 { MAX_TABLESIZE } else { 0 } };
		//Otherwise, it is wptr-tptr, modulo MAX_TABLESIZE.
		(self.wptr.0 as usize + MAX_TABLESIZE - self.tptr.0 as usize) % MAX_TABLESIZE
	}
	fn __prepare_pushb(&mut self, len: usize) -> Option<u16>
	{
		//amt > 65535 does not make sense, as it will cause complete eviction of window, and subsequent
		//exit from function.
		let amt = min(len, 65535) as u16;
		//Evict to size. If c.len() > self.window, the correct thing is evict everything. If every entry
		//has gotten evicted, then skip this, as this entry is just too big.
		self.evict_to_size(self.window.saturating_sub(amt) as usize);
		if self.count == 0 { return None; }
		Some(amt)
	}
	fn __commit_pushb(&mut self, off: u32, amt: u16)
	{
		//Count into entry size.
		let e = tab_load16(&self.buffer, self.eptr + off);
		tab_store16(&mut self.buffer, self.eptr + off, e.wrapping_add(amt));
	}
	fn pushb_m(&mut self, c: &[u8], off: u32)
	{
		if let Some(amt) = self.__prepare_pushb(c.len()) {
			self.__write(c);
			self.__commit_pushb(off, amt);
		}
	}
	fn pushb_m_dummy(&mut self, len: usize, off: u32)
	{
		if let Some(amt) = self.__prepare_pushb(len) {
			self.__write_uninit(len as u32);
			self.__commit_pushb(off, amt);
		}
	}
	//Make sure there is space for the entry! Does not adjust any size (but adjusts wptr)!
	fn __write(&mut self, mut c: &[u8])
	{
		//Copy in fragments.
		while c.len() > 0 {
			//Invariant: self.wptr.0 < MAX_TABLESIZE.
			let mc = min(MAX_TABLESIZE - self.wptr.0 as usize, c.len());
			//self.wptr.0+mc <= MAX_TABLESIZE == self.buffer.len(). mc <= c.len()
			(&mut self.buffer[self.wptr.0 as usize..][..mc]).copy_from_slice(&c[..mc]);
			self.wptr = self.wptr + mc as u32;
			c = &c[mc..];
		}
	}
	//Make sure there is space for the entry! Does not adjust any size (but adjusts wptr)!
	fn __write_uninit(&mut self, amt: u32)
	{
		self.wptr = self.wptr + amt;
	}
	fn __push_name<F>(&self, stdname: Option<StandardHttpHeader>, name: ModTS, namelen: u32, sink: &mut F)
		where F: FnMut(PushElement)
	{
		match stdname {
			Some(name) => sink(PushElement::StdName(name)),
			None => self.push_string(name, namelen, sink),
		}
	}
	fn evict_to_size(&mut self, limit: usize)
	{
		let mut size = self.count_stuff();
		while size > limit {
			let osize = size;

			//tptr increments by the entry, count decreases by 1 (the array is backwards!)
			let len = ENTRY_HEADER_LEN + tab_load16(&self.buffer, self.tptr + ENTRY_NLEN_OFF) as u32 +
				tab_load16(&self.buffer, self.tptr + ENTRY_VLEN_OFF) as u32;
			self.tptr = self.tptr + len;
			self.count -= 1;
			
			size = self.count_stuff();
			//These are internal error conditions. There is no way to signal them. Panicing or
			//aborting is not possible because it would be catastrophic bug.
			if size >= osize || (size > 0 && self.count == 0) { return; }
		}
	}
}

impl ContextTrait for Context
{
	fn new_entry(&mut self)
	{
		//If window is <32, no entries can be created, otherwise possibly evict entry to make space.
		if self.window < ENTRY_HEADER_LEN as u16 { return; }
		//Evict entry to make space for header.
		self.evict_to_size(self.window.saturating_sub(ENTRY_HEADER_LEN as u16) as usize);
		//Write the entry for the new header. This is located at first-1, which becomes new first, and
		//count increases by 1.
		self.count += 1;
		self.first = self.first - 1;
		self.first.put(&mut self.entries, self.wptr);
		self.eptr = self.wptr;
		//Write 32 byte entry header. Only 6 first bytes are used, so can write junk as rest. The full 32
		//byte header must exist for accounting purposes.
		self.__write(&[0;6]);
		self.__write_uninit(ENTRY_HEADER_LEN-6);
		//OK, ready.
	}
	fn readback_name<F>(&self, entry: usize, sink: &mut F) -> Result<(), Error>
		where F: FnMut(PushElement)
	{
		let (stdname, name, namelen, _, _) = self.get_entry(entry)?;
		self.__push_name(stdname, name, namelen, sink);
		Ok(())
	}
	fn readback_name_value<F>(&self, entry: usize, sink: &mut F) -> Result<(), Error>
		where F: FnMut(PushElement)
	{
		let (stdname, name, namelen, value, valuelen) = self.get_entry(entry)?;
		self.__push_name(stdname, name, namelen, sink);
		self.push_string(value, valuelen, sink);
		Ok(())
	}
	fn push_name_m(&mut self, c: &[u8]) { self.pushb_m(c, ENTRY_NLEN_OFF) }
	fn push_value_m(&mut self, c: &[u8]) { self.pushb_m(c, ENTRY_VLEN_OFF) }
	fn change_window_size(&mut self, winsize: usize) -> Result<(), Error>
	{
		//Fastpath to evict everything.
		if winsize == 0 {
			self.tptr = ModTS(0);
			self.wptr = ModTS(0);
			self.first = ModTE(0);
			self.count = 0;
		} else {
			fail_if!(winsize > MAX_TABLESIZE, Error(_Error::IllegalTableSize(winsize, MAX_TABLESIZE)));
			self.evict_to_size(winsize);
		}
		self.window = winsize as u16;
		Ok(())
	}
	fn new_entry_with_stdname(&mut self, name: StandardHttpHeader)
	{
		self.new_entry();
		tab_store16(&mut self.buffer, self.eptr + ENTRY_NID_OFF, name as u16);
		//Can not directly use __write_uninit(), as this may evict stuff. This dummy name field needs to
		//be there for accounting purposes.
		self.pushb_m_dummy(name.strlen(), ENTRY_NLEN_OFF);
	}
	///Start a new entry, copying name of existing entry.
	fn new_entry_copyname(&mut self, entry: usize) -> Result<(), Error>
	{
		//Check that the entry is valid!
		if entry >= self.count as usize {
			let real_index = entry.saturating_add(62);
			let real_limit = self.count as usize + 62;
			fail!(Error(_Error::IllegalReference2(real_index, real_limit)));
		}
		//Load the name pointer and name length.
		let entry = (self.first+entry as u16).get(&self.entries);
		let namelen = tab_load16(&self.buffer, entry + ENTRY_NLEN_OFF);
		let nid = tab_load16(&self.buffer, entry + ENTRY_NID_OFF);
		//Evict entries for the new entry. This may free the entry we are working on, but since eviction
		//does not change main table memory, can just UAF in this case. The entry existing in window means
		//there is sufficient space for at least its name.
		self.evict_to_size((self.window - ENTRY_HEADER_LEN as u16 - namelen) as usize);
		//Create new entry. This can modify main table memory, but because write position can not be inside
		//header or name of entry to copy (eviction does not modify pointers, and entries can not overlap),
		//this at worst overwrites the header. Afterwards, overwrite the name length field back to correct
		//value. Also overwrite the nid field.
		self.new_entry();
		tab_store16(&mut self.buffer, self.eptr + ENTRY_NLEN_OFF, namelen);
		tab_store16(&mut self.buffer, self.eptr + ENTRY_NID_OFF, nid);
		//Now, it may happen that entry == self.eptr. In that case, the name is already in the correct
		//place. Otherwise we need to copy the name from old entry (possibly UAF) into the new. However,
		//the source and target of copy may overlap. Moreover, the overlap is always target-before-source,
		//so copying in forward direction is always correct. The copy can also be skipped if nid != 0,
		//as in that case the name does not contain anything valid anyway (it is just there for accounting).
		//However, regardless of anything, the space needs to be marked used.
		self.__write_uninit(namelen as u32);
		if nid == 0 && entry != self.eptr {
			let mut sidx = entry + ENTRY_HEADER_LEN;
			let mut tidx = self.eptr + ENTRY_HEADER_LEN;
			for _ in 0..namelen {
				let t = sidx.get(&self.buffer);
				tidx.put(&mut self.buffer, t);
				sidx = sidx + 1;
				tidx = tidx + 1;
			}
		}
		Ok(())
	}
}

trait ContextTrait
{
	fn new_entry(&mut self);
	fn readback_name<F>(&self, entry: usize, sink: &mut F) -> Result<(), Error>
		where F: FnMut(PushElement);
	fn readback_name_value<F>(&self, entry: usize, sink: &mut F) -> Result<(), Error>
		where F: FnMut(PushElement);
	fn change_window_size(&mut self, winsize: usize) -> Result<(), Error>;
	fn new_entry_with_stdname(&mut self, name: StandardHttpHeader);
	fn push_name_m(&mut self, c: &[u8]);
	fn push_value_m(&mut self, c: &[u8]);
	fn new_entry_copyname(&mut self, entry: usize) -> Result<(), Error>;
}

fn with_source<T>(source: &[u8], itr: &mut usize, evolve: impl FnOnce(&[u8]) -> Result<(T, usize), Error>) ->
	Result<T, Error>
{
	let source = source.get(*itr..).ok_or(Error(_Error::IntIteratorOOR))?;
	let (state, amt) = evolve(source)?;
	*itr = itr.checked_add(amt).ok_or(Error(_Error::IntIteratorOF))?;
	Ok(state)
}

fn emit_string(source: &[u8], output: &mut impl FnMut(PushElement))
{
	output(PushElement::Bytes(source));
	output(PushElement::EndOfString);
}

#[derive(Copy,Clone,PartialEq,Debug)]
enum ReadExtended
{
	NoStore,
	Resize,
	Store,
	Index
}

macro_rules! read_and_inc
{
	($src:expr, $itr:ident, $mknewstate:expr) => {{
		match $src.get($itr) {
			Some(&x) => {
				$itr = $itr.checked_add(1).ok_or(Error(_Error::IntIteratorOF))?;
				x
			},
			None => return Ok((($mknewstate)(), $itr)),
		}
	}}
}

macro_rules! hpack_nextstate
{
	(IDLE $selfx:ident) => {{ $selfx = _Decoder::Init; continue; }};
	(STORE $selfx:ident) => {{ $selfx = _Decoder::ValueStore(StringDecoder2::Init); continue; }};
	(NOSTORE $selfx:ident) => {{ $selfx = _Decoder::ValueNoStore(StringDecoder2::Init); continue; }};
	(STORENV $selfx:ident) => {{ $selfx = _Decoder::NameCache(false); continue; }};
	(NOSTORENV $selfx:ident) => {{ $selfx = _Decoder::NameCache(true); continue; }};
	(EXTEND  $selfx:ident $id:ident) => {{
		$selfx = _Decoder::Extended(ReadExtended::$id, 0, 0); continue;
	}};
	(STREND $selfx:ident) => {{ $selfx = StringDecoder2::End; continue; }};
	(STRCPY $selfx:ident $amt:expr) => {{ $selfx = StringDecoder2::Copying($amt); continue; }};
	(STRHUFF $selfx:ident $amt:expr) => {{
		$selfx = StringDecoder2::HuffmanCached($amt); continue;
	}};
	(STREXTEND  $selfx:ident $huff:expr) => {{
		$selfx = StringDecoder2::Extended($huff, 0, 0); continue;
	}};
}


//This is exposed only for benchmarking.
#[doc(hidden)]
//Returns (buf_consumed, out_consumed).
//On error, out_consumed is still returned.
pub fn unpack_huffman_frag(state: &mut u16, buf: &[u8], out: &mut [u8]) -> Result<(usize, usize), (usize, Error)>
{
	let mut ritr = 0;
	let mut witr = 0;
	//Need to produce pending byte from last round? If yes, and there is space, reset the pending byte from state.
	if *state > 0xFF {
		if witr < out.len() {
			out[witr] = (*state >> 8) as u8;
			witr += 1;
			*state &= 0xFF;
		} else {
			//No space to do anything.
			return Ok((0,0));
		}
	}
	//The witr+1 is because this can produce two bytes per round, and consumption of input is only at
	//byte granularity.
	while ritr < buf.len() {
		let b = buf[ritr];
		let bl = b as u64 >> 1 & 7;

		let w = HUFFMAN_DECTAB2[b as usize / 16 * 256 + *state as usize % 256];
		if bl < HUFFMAN_DECDBL_MAGIC2 >> w/256%16*4 & 15 {
			//This is double.
			if witr >= out.len() { break; }		//No space to emit the bytes.
			out[witr] = w as u8;
			let w = HUFFMAN_DECTAB2[b as usize % 16 * 256 + w as usize / 256 % 256];
			if witr + 1 >= out.len() {
				//No space to emit the second byte. It is guaranteed that the second byte is one of
				//with at most 7 bit code, which does not include zero. Put the pending byte as high
				//byte, and state as low byte of the new state.
				*state = (w >> 8) | (w << 8);
				ritr += 1;
				witr += 1;
				break;
			}
			out[witr+1] = w as u8;
			*state = w >> 8;
			ritr += 1;
			witr += 2;
		} else if w < 3840 {
			//This is first single. The second transition can never be invalid in this case.
			if witr >= out.len() { break; }					//No space.
			out[witr] = w as u8;
			*state = HUFFMAN_DECTAB2[b as usize % 16 * 256 + w as usize / 256 % 256] & 0xFF;
			ritr += 1;
			witr += 1;
		} else if w & 0xFF < 15 {
			fail!((witr, Error(_Error::IllegalHuffmanSequence)));
		} else {
			//Second single or none.
			let w = HUFFMAN_DECTAB2[b as usize % 16 * 256 + w as usize % 256];
			if w < 3840 {
				if witr >= out.len() { break; }		//No space.
				out[witr] = w as u8; witr += 1; *state = w >> 8;
			}
			else if w & 0xFF >= 15 { *state = w & 0xFF; }
			else { fail!((witr, Error(_Error::IllegalHuffmanSequence))); }
			ritr += 1;
		}
	}
	Ok((ritr, witr))
}


///Pushed element.
#[derive(Copy,Clone,PartialEq,Eq,Debug)]
pub(crate) enum PushElement<'a>
{
	///Standard name.
	StdName(StandardHttpHeader),
	///Multiple bytes.
	Bytes(&'a [u8]),
	///End of string.
	EndOfString,
	///End of collection.
	EndOfCollection,
}


#[derive(Copy,Clone,Debug,PartialEq)]
enum StringDecoder2
{
	Init,
	Copying(u64),
	Huffman(u16, u64),
	HuffmanCached(u64),
	Extended(bool, u64, u32),
	End,
}

impl StringDecoder2
{
	fn evolve(mut self, source: &[u8], push: &mut impl FnMut(PushElement)) -> Result<(Self, usize), Error>
	{
		let mut itr = 0;
		loop { match self {
			StringDecoder2::Init => {
				match read_and_inc!(source, itr, ||StringDecoder2::Init) {
					b@0..=126 => hpack_nextstate!(STRCPY self b as u64),
					127 => hpack_nextstate!(STREXTEND self false),
					b@128..=254 => hpack_nextstate!(STRHUFF self b as u64 - 128),
					_ => hpack_nextstate!(STREXTEND self true),
				}
			},
			StringDecoder2::Extended(huff, mut val, mut shift) => {
				loop {
					fail_if!(shift > 56, Error(_Error::IntegerTooBig));
					let s = read_and_inc!(source, itr, ||
						StringDecoder2::Extended(huff, val, shift));
					val |= ((s & 127) as u64) << shift;
					shift += 7;
					if s >> 7 == 0 { break; }
				}
				val += 127;
				if huff {
					hpack_nextstate!(STRHUFF self val);
				} else {
					hpack_nextstate!(STRCPY self val);
				}
			},
			StringDecoder2::Copying(mut left) => {
				if left == 0 {
					push(PushElement::EndOfString);
					hpack_nextstate!(STREND self);
				}
				let source = source.get(itr..).ok_or(Error(_Error::IntIteratorOOR))?;
				let source = if left <= source.len() as u64  {
					source.get(..left as usize).ok_or(Error(_Error::IntBadSliceLeft))?
				} else {
					source
				};
				//End here if no possible progress.
				if source.len() == 0 { return Ok((self, itr)); }
				push(PushElement::Bytes(source));
				itr = itr.checked_add(source.len()).ok_or(Error(_Error::IntIteratorOF))?;
				left = left.checked_sub(source.len() as u64).ok_or(Error(_Error::IntReadTooMuch))?;
				self = StringDecoder2::Copying(left);
			},
			StringDecoder2::HuffmanCached(left) => {
				//Only if there is enough stuff in buffer.
				if let Some(reference) = source.get(itr..itr+left as usize) {
					//Compute the cache hash.
					let magic = int_hash(reference);
					if let Some(u) = HUFFMAN_CACHE2.binary_search_by(|k|k.0.cmp(&magic)).
						ok().and_then(|eidx|HUFFMAN_CACHE2.get(eidx)).
						and_then(|&(_, c, u)|if c==reference { Some(u) } else { None }) {
						//Ok, cache hit, emit u.
						push(PushElement::Bytes(u));
						push(PushElement::EndOfString);
						itr += reference.len();
						hpack_nextstate!(STREND self);
					}
				}
				//If not found, continue to normal huffman decoding. The initial state is always
				//11.
				self = StringDecoder2::Huffman(11, left);
			},
			StringDecoder2::Huffman(mut hstate, mut left) => {
				if left == 0 {
					fail_if!(hstate.wrapping_sub(11) >= 8, Error(_Error::TruncatedHuffman));
					push(PushElement::EndOfString);
					hpack_nextstate!(STREND self);
				}
				let source = source.get(itr..).ok_or(Error(_Error::IntIteratorOOR))?;
				let mut source = if left <= source.len() as u64  {
					source.get(..left as usize).ok_or(Error(_Error::IntBadSliceLeft))?
				} else {
					source
				};
				//End here if no possible progress.
				if source.len() == 0 { return Ok((self, itr)); }
				//While progress can be made...
				let mut out = [0;256];
				let mut processed = 0;
				//hstate > 255 means there is a buffered byte, need to iterate in order to receive
				//that byte. This does not consume any input.
				while source.len() > 0 || hstate > 255 {
					//Process fragment of huffman code.
					match unpack_huffman_frag(&mut hstate, source, &mut out) {
						Ok((inc, outc)) => {
							push(PushElement::Bytes(&out[..outc]));
							source = &source[inc..];
							processed += inc;
						},
						Err((outc, err)) => {
							//Still emit the decoded parts before erroring (there
							//is one test that relies on this).
							push(PushElement::Bytes(&out[..outc]));
							return Err(err);
						}
					}
				}
				itr += processed;			//Acknowledge processed.
				left -= processed as u64;		//Remove from remaining input.
				self = StringDecoder2::Huffman(hstate, left);
			},
			//This state is invariant.
			StringDecoder2::End => return Ok((self, itr))
		}}
	}
	fn terminal(&self) -> bool
	{
		if let StringDecoder2::End = *self { true } else { false }
	}
}

fn int_hash(data: &[u8]) -> u32
{
	let mut id = 0u32;
	for j in data.iter() { id = (id << 8).wrapping_add(id).wrapping_add(*j as u32); }
	id
}


///HPACK stream decoder
///
///This structure handles decoding HPACK streams.
#[derive(Clone)]
pub(crate) struct Decoder(_Decoder);

#[derive(Copy,Clone,PartialEq,Debug)]
enum _Decoder
{
	Init,
	Extended(ReadExtended, usize, u32),
	NameCache(bool),	//nostore.
	NameNoStore(StringDecoder2),
	ValueNoStore(StringDecoder2),
	NameStore(StringDecoder2),
	ValueStore(StringDecoder2),
}

impl _Decoder
{
	fn __process_name_cache_inner(source: &[u8], itr: &mut usize, nostore: bool, ctx: &mut impl ContextTrait,
		push: &mut impl FnMut(PushElement)) -> bool
	{
		//Caching does not advance itr unless it hits.
		//Also, this is performance optimization: It does not matter if it ocassionally
		//misses valid cache entries.
		let l = *f_return!(source.get(*itr), false) as usize & 127;
		if l >= 127 { return false; }		//Assume all cached are <127 bytes.

		let end = *itr + l + 1;		//+1 for length byte.
		//Don't wait for the rest, just miss.
		let source = f_return!(source.get(*itr..end), false);
		let hash = int_hash(source);
		let index = f_return!(HEADER_NAME_CACHE.binary_search_by(|k|k.0.cmp(&hash)), false);
		//By binary_search_by(), this is always in-range.
		//index 1 is textual name, index 2 is StandardHttpHeader.
		let entry = unsafe{HEADER_NAME_CACHE.get_unchecked(index)};
		if entry.1 != source { return false; }		//Hash hits but entry is wrong.

		//Hit!
		*itr = end;
		push(PushElement::StdName(entry.2));
		if !nostore { ctx.new_entry_with_stdname(entry.2); }
		true
	}
	fn __process_name_cache(source: &[u8], itr: &mut usize, nostore: bool, ctx: &mut impl ContextTrait,
		push: &mut impl FnMut(PushElement)) -> _Decoder
	{
		let sinit = StringDecoder2::Init;
		match (Self::__process_name_cache_inner(source,itr, nostore, ctx, push), nostore) {
			(true, true) => _Decoder::ValueNoStore(sinit),
			(true, false) => _Decoder::ValueStore(sinit),
			(false, true) => _Decoder::NameNoStore(sinit),
			(false, false) => {
				ctx.new_entry();
				_Decoder::NameStore(sinit)
			},
		}
	}
	fn evolve(mut self, source: &[u8], ctx: &mut impl ContextTrait, push: &mut impl FnMut(PushElement)) ->
		Result<(Self, usize), Error>
	{
		let mut itr = 0;
		while itr < source.len() { match self {
			_Decoder::Init => {
				match read_and_inc!(source, itr, ||_Decoder::Init) {
					0|16 => hpack_nextstate!(NOSTORENV self),
					b@1..=14|b@17..=30 => {
						let name = get_static_name(b as usize - 1 & 15)?;
						push(PushElement::StdName(name));
						hpack_nextstate!(NOSTORE self);
					},
					15|31 => hpack_nextstate!(EXTEND self NoStore),
					b@32..=62 => {
						ctx.change_window_size(b as usize - 32)?;
						hpack_nextstate!(IDLE self);
					},
					63 => hpack_nextstate!(EXTEND self Resize),
					64 => {
						hpack_nextstate!(STORENV self);
					},
					b@65..=125 => {
						let name = get_static_name(b as usize - 1 & 63)?;
						push(PushElement::StdName(name));
						ctx.new_entry_with_stdname(name);
						hpack_nextstate!(STORE self);
					}
					126 => {
						ctx.readback_name(0, push)?;
						ctx.new_entry_copyname(0)?;
						hpack_nextstate!(STORE self);
					},
					127 => hpack_nextstate!(EXTEND self Store),
					128 => fail!(Error(_Error::IllegalOpcode128)),
					b@129..=189 => {
						let name = get_static_name(b as usize - 1 & 127)?;
						push(PushElement::StdName(name));
						let value = get_static_value(b as usize - 1 & 127)?;
						emit_string(value, push);
						hpack_nextstate!(IDLE self);
					}
					b@190..=254 => {
						ctx.readback_name_value(b as usize - 190, push)?;
						hpack_nextstate!(IDLE self);
					},
					_ => hpack_nextstate!(EXTEND self Index),
				}
			},
			_Decoder::Extended(cat, mut val, mut shift) => {
				loop {
					let s = read_and_inc!(source, itr, ||
						_Decoder::Extended(cat, val, shift));
					val |= ((s & 127) as usize) << shift;
					//We do not increment shift past 21, so overly large values, generate
					//values that are too big, but do not overflow.
					if shift < 21 { shift += 7; }
					if s >> 7 == 0 { break; }
				}
				match cat {
					ReadExtended::NoStore if val < 47 => {
						let name = get_static_name(val + 14)?;
						push(PushElement::StdName(name));
						hpack_nextstate!(NOSTORE self);
					},
					ReadExtended::NoStore => {
						ctx.readback_name(val - 47, push)?;
						hpack_nextstate!(NOSTORE self);
					},
					ReadExtended::Resize => {
						ctx.change_window_size(val+31)?;
						hpack_nextstate!(IDLE self);
					},
					ReadExtended::Store => {
						ctx.readback_name(val + 1, push)?;
						ctx.new_entry_copyname(val + 1)?;
						hpack_nextstate!(STORE self);
					},
					ReadExtended::Index => {
						ctx.readback_name_value(val + 65, push)?;
						hpack_nextstate!(IDLE self);
					}
				}
			},
			_Decoder::NameCache(nostore) => {
				self = Self::__process_name_cache(source, &mut itr, nostore, ctx, push);
			},
			_Decoder::NameNoStore(mut state) => {
				state = with_source(source, &mut itr, |source|state.evolve(source, push))?;
				if state.terminal() { hpack_nextstate!(NOSTORE self); }
				self = _Decoder::NameNoStore(state);
			},
			_Decoder::ValueNoStore(mut state) => {
				state = with_source(source, &mut itr, |source|state.evolve(source, push))?;
				if state.terminal() { hpack_nextstate!(IDLE self); }
				self = _Decoder::ValueNoStore(state);
			},
			_Decoder::NameStore(mut state) => {
				state = with_source(source, &mut itr, |source|state.evolve(source, &mut |sym|{
					push(sym);
					match sym {
						PushElement::StdName(s) => ctx.push_name_m(s.to_str().as_bytes()),
						PushElement::Bytes(b) => ctx.push_name_m(b),
						//These two might come, but are handled sepatedly.
						PushElement::EndOfString => (),
						PushElement::EndOfCollection => (),
					}
				}))?;
				if state.terminal() { hpack_nextstate!(STORE self); }
				self = _Decoder::NameStore(state);
			},
			_Decoder::ValueStore(mut state) => {
				state = with_source(source, &mut itr, |source|state.evolve(source, &mut |sym|{
					push(sym);
					match sym {
						//Da fuq? This is not supposed to be used for values.
						PushElement::StdName(s) => ctx.push_value_m(s.to_str().as_bytes()),
						PushElement::Bytes(b) => ctx.push_value_m(b),
						//These two might come, but are handled sepatedly.
						PushElement::EndOfString => (),
						PushElement::EndOfCollection => (),
					}
				}))?;
				if state.terminal() { hpack_nextstate!(IDLE self); }
				self = _Decoder::ValueStore(state);
			},
		}}
		Ok((self, itr))
	}
	fn end(self, mut push: impl FnMut(PushElement<'static>)) -> Result<(), Error>
	{
		if self == _Decoder::Init {
			Ok(push(PushElement::EndOfCollection))
		} else {
			fail!(Error(_Error::TruncatedHeaderBlock));
		}
	}
}

impl Decoder
{
	///Create a new decoder.
	pub fn new() -> Decoder
	{
		Decoder(_Decoder::Init)
	}
	///Send data `source` to be decoded.
	///
	///The request-to-request context is `ctx` and the results are sent to function `push`.
	///
	///If this is called multiple times with no intervening `end()` call, the blocks are logically concatenated.
	///
	///On success, returns `Ok(())`. On error, returns `Err(err)`, where `err` is the error message.
	///All errors are fatal, and context is possibly corrupt if error occurs, so connection has to be torn
	///down.
	pub fn push<F>(&mut self, source: &[u8], ctx: &mut Context, push: F) ->
		Result<(), Error> where F: FnMut(PushElement)
	{
		self._push(source, ctx, push)
	}
	///Send end of headers indication.
	///
	///The results are sent to function `push`.
	///
	///On success, returns `Ok(())`. On error, returns `Err(err)`, where `err` is the error message.
	///All errors are fatal, and context is possibly corrupt if error occurs, so connection has to be torn
	///down.
	pub fn end<F>(self, push: F) -> Result<(), Error> where F: FnMut(PushElement<'static>)
	{
		self.0.end(push)
	}
	fn _push(&mut self, source: &[u8], ctx: &mut impl ContextTrait, mut push: impl FnMut(PushElement)) ->
		Result<(), Error>
	{
		let mut itr = 0;
		let push = &mut push;
		while itr < source.len() {
			self.0 = with_source(source, &mut itr, |source|self.0.evolve(source, ctx, push))?
		}
		Ok(())
	}
}

include!(concat!(env!("OUT_DIR"), "/autogenerated-huffman-cache.inc.rs"));

fn encode_value(output: &mut impl Sink, p: u8, s: &[u8]) -> Result<(), ()>
{
	output.write_u8(p)?;
	encode_one_single(output, s)
}

fn encode_one_single_std(output: &mut impl Sink, s: StandardHttpHeader) -> Result<(), ()>
{
	output.write_slice(s.hpack_encoding())
	//encode_one_single(output, s.to_str().as_bytes())
}


///Encode a concatenation of two as path header.
pub fn hpack_encode_path_concat2<S:Sink>(output: &mut S, v1: &[u8], v2: &[u8]) -> Result<(), ()>
{
	output.write_u8(68)?;	//:path.
	encode_one(output, &[v1, v2])
}

/*******************************************************************************************************************/
#[cfg(test)]
fn get_static_name_test(index: usize) -> &'static [u8]
{
	get_static_name(index).unwrap().to_str().as_bytes()
}

#[cfg(test)]
fn headercompressor_ok<C:ContextTrait>(data: &[u8], reference: &[&[u8]], ctx: &mut C)
{
	let mut itr1 = 0;
	let mut itr2 = 0;
	let mut d = Decoder::new();
	d._push(data, ctx, |sym|{
		let refn = reference.get(itr1).map(|x|&x[itr2..]);
		println!("reference={refn:?}, sym={sym:?}");
		match sym {
			PushElement::StdName(s) => {
				assert_eq!(s.to_str().as_bytes(), reference[itr1]);
				itr1 += 1;
			},
			PushElement::Bytes(b) => for &b in b.iter() {
				assert_eq!(b, reference[itr1][itr2]);
				itr2 += 1;
			},
			PushElement::EndOfString => {
				assert_eq!(itr2, reference[itr1].len());
				itr1 += 1;
				itr2 = 0;
			},
			PushElement::EndOfCollection => assert!(false),
		};
	}).unwrap();
	let mut last: PushElement<'static> = PushElement::EndOfString;
	d.end(|v|last=v).unwrap();
	assert_eq!(itr1, reference.len());
	assert_eq!(last, PushElement::EndOfCollection);
}

#[test]
fn test_headercompressor()
{
	headercompressor_ok(b"\x00\x03foo\x06barzot", &[b"foo", b"barzot"], &mut Context::new());
	headercompressor_ok(b"\x01\x06barzot", &[b":authority", b"barzot"], &mut Context::new());
	headercompressor_ok(b"\x1f\x2e\x06barzot", &[b"www-authenticate", b"barzot"], &mut Context::new());
}

#[test]
fn test_headercompressor_rfc7541_huffman_example_c4()
{
	let mut ctx = Context::new();
	headercompressor_ok(b"\x82\x86\x84\x41\x8c\xf1\xe3\xc2\xe5\xf2\x3a\x6b\xa0\xab\x90\xf4\xff",
		&[b":method", b"GET", b":scheme", b"http", b":path", b"/", b":authority", b"www.example.com"],
		&mut ctx);
	assert_eq!(ctx.count_stuff(), 57);
	headercompressor_ok(b"\x82\x86\x84\xbe\x58\x86\xa8\xeb\x10\x64\x9c\xbf",
		&[b":method", b"GET", b":scheme", b"http", b":path", b"/",  b":authority", b"www.example.com",
		b"cache-control", b"no-cache"], &mut ctx);
	assert_eq!(ctx.count_stuff(), 110);
	headercompressor_ok(b"\x82\x87\x85\xbf\x40\x88\x25\xa8\x49\xe9\x5b\xa9\x7d\x7f\x89\x25\xa8\x49\xe9\x5b\
		\xb8\xe8\xb4\xbf", &[b":method", b"GET", b":scheme", b"https", b":path", b"/index.html",
		b":authority", b"www.example.com", b"custom-key", b"custom-value"], &mut ctx);
	assert_eq!(ctx.count_stuff(), 164);
}

#[test]
fn test_headercompressor_rfc7541_nohuffman_example_c5()
{
	let mut ctx = Context::new();
	ctx.change_window_size(256).unwrap();
	headercompressor_ok(b"H\x03302X\x07privatea\x1dMon, 21 Oct 2013 20:13:21 GMTn\x17https://www.example.com",
		&[b":status", b"302", b"cache-control", b"private", b"date", b"Mon, 21 Oct 2013 20:13:21 GMT",
		b"location", b"https://www.example.com"], &mut ctx);
	assert_eq!(ctx.count_stuff(), 222);
	headercompressor_ok(b"H\x03307\xc1\xc0\xbf", &[b":status", b"307", b"cache-control", b"private", b"date",
		b"Mon, 21 Oct 2013 20:13:21 GMT", b"location", b"https://www.example.com"], &mut ctx);
	assert_eq!(ctx.count_stuff(), 222);
	headercompressor_ok(b"\x88\xc1a\x1dMon, 21 Oct 2013 20:13:22 GMT\xc0Z\x04gzipw8foo=ASDJKHQKBZXOQWEOPIUAX\
		QWEOIU; max-age=3600; version=1", &[b":status", b"200", b"cache-control", b"private", b"date",
		b"Mon, 21 Oct 2013 20:13:22 GMT", b"location", b"https://www.example.com", b"content-encoding",
		b"gzip", b"set-cookie", b"foo=ASDJKHQKBZXOQWEOPIUAXQWEOIU; max-age=3600; version=1" ], &mut ctx);
	assert_eq!(ctx.count_stuff(), 215);
}

#[test]
fn test_headercompressor_rfc7541_nohuffman_example_c3()
{
	let mut ctx = Context::new();
	ctx.change_window_size(256).unwrap();
	headercompressor_ok(b"\x82\x86\x84A\x0fwww.example.com", &[b":method", b"GET", b":scheme", b"http",
		b":path", b"/", b":authority", b"www.example.com"], &mut ctx);
	assert_eq!(ctx.count_stuff(), 57);
	headercompressor_ok(b"\x82\x86\x84\xbeX\x08no-cache", &[b":method", b"GET", b":scheme", b"http",
		b":path", b"/", b":authority", b"www.example.com", b"cache-control", b"no-cache"], &mut ctx);
	assert_eq!(ctx.count_stuff(), 110);
	headercompressor_ok(b"\x82\x87\x85\xbf@\x0acustom-key\x0ccustom-value", &[b":method", b"GET",
		b":scheme", b"https", b":path", b"/index.html", b":authority", b"www.example.com",
		b"custom-key", b"custom-value"], &mut ctx);
	assert_eq!(ctx.count_stuff(), 164);
}

#[test]
fn test_headercompressor_cache()
{
	headercompressor_ok(b"\x00\x07:method\x03GET", &[b":method", b"GET"], &mut Context::new());
	headercompressor_ok(b"\x00\x85\xb9IS9\xe4\x03GET", &[b":method", b"GET"], &mut Context::new());
}

#[test]
fn test_headercompressor_send_stdname_plain()
{
	let mut c = Context::new();
	let mut d = Decoder::new();
	d._push(b"\x00\x07:method", &mut c, |sym|{
		match sym {
			PushElement::StdName(StandardHttpHeader::Http2MagicHeaderMethod) => (),
			sym => panic!("Bad symbol: {sym:?}")
		}
	}).unwrap();
}

#[test]
fn test_headercompressor_send_stdname_huffman()
{
	let mut c = Context::new();
	let mut d = Decoder::new();
	d._push(b"\x00\x85\xb9IS9\xe4", &mut c, |sym|{
		match sym {
			PushElement::StdName(StandardHttpHeader::Http2MagicHeaderMethod) => (),
			sym => panic!("Bad symbol: {sym:?}")
		}
	}).unwrap();
}

#[test]
fn tab_loadsave()
{
	let mut t = [0;MAX_TABLESIZE];
	assert_eq!(tab_load16(&t, ModTS(5)), 0);
	tab_store16(&mut t, ModTS(5), 258);
	assert_eq!(tab_load16(&t, ModTS(5)), 258);
	assert_eq!(ModTS(3).get(&t), 0);
	let mut ptr = ModTS(3);
	tab_store8inc(&mut t, &mut ptr, 42);
	assert_eq!(ptr.0, 4);
	assert_eq!(ModTS(3).get(&t), 42);
}

#[test]
fn modts_add()
{
	assert_eq!(ModTS(MAX_TABLESIZE as u16 -1) + 2, ModTS(1));
	assert_eq!(ModTS(MAX_TABLESIZE as u16 -1) + (MAX_TABLESIZE as u32 -1), ModTS(MAX_TABLESIZE as u16 -2));
	assert_eq!(ModTS(2) + 3, ModTS(5));
}

#[test]
fn modts_tabaccess()
{
	let mut t = [0;MAX_TABLESIZE];
	assert_eq!(ModTS(1).get(&t), 0);
	ModTS(MAX_TABLESIZE as u16 + 1).put(&mut t, 1);
	assert_eq!(ModTS(1).get(&t), 1);
	assert_eq!(ModTS(2).get(&t), 0);
	assert_eq!(ModTS(MAX_TABLESIZE as u16 + 1).get(&t), 1);
}

#[test]
fn modte_add()
{
	assert_eq!(ModTE(MAX_ENTRIES as u16 -1) + 2, ModTE(1));
	assert_eq!(ModTE(MAX_ENTRIES as u16 -1) + (MAX_ENTRIES as u16 -1), ModTE(MAX_ENTRIES as u16 -2));
	assert_eq!(ModTE(2) + 3, ModTE(5));
}

#[test]
fn modte_sub()
{
	assert_eq!(ModTE(3) - 2, ModTE(1));
	assert_eq!(ModTE(3) - 4, ModTE(MAX_ENTRIES as u16 - 1));
	assert_eq!(ModTE(3) - 5, ModTE(MAX_ENTRIES as u16 - 2));
	assert_eq!(ModTE(MAX_ENTRIES as u16 - 2) - (MAX_ENTRIES as u16 - 1), ModTE(MAX_ENTRIES as u16 - 1));
}

#[test]
fn modte_tabaccess()
{
	let mut t = [ModTS(0);MAX_ENTRIES];
	assert_eq!(ModTE(1).get(&t), ModTS(0));
	ModTE(MAX_ENTRIES as u16 + 1).put(&mut t, ModTS(1));
	assert_eq!(ModTE(1).get(&t), ModTS(1));
	assert_eq!(ModTE(2).get(&t), ModTS(0));
	assert_eq!(ModTE(MAX_ENTRIES as u16 + 1).get(&t), ModTS(1));
}

#[cfg(test)]
fn handle_byte(b: u8, reference: &[u8], itr: &mut usize)
{
	assert_eq!(b, reference[*itr]);
	*itr += 1;
}

#[cfg(test)]
fn stringcompressor_ok(data: &[u8], reference: &[u8])
{
	let mut itr = 0;
	let d = StringDecoder2::Init;
	let (_, a) = d.evolve(data, &mut |sym|{
		match sym {
			PushElement::StdName(s) => assert_eq!(s.to_str().as_bytes(), reference),
			PushElement::Bytes(b) => for &b in b.iter() {
				handle_byte(b, reference, &mut itr);
			},
			PushElement::EndOfString => assert_eq!(itr, reference.len()),
			PushElement::EndOfCollection => assert!(false),
		};
	}).unwrap();
	assert_eq!(a, data.len());
}

#[cfg(test)]
fn stringcompressor_error(data: &[u8], reference: &[u8], soft: bool)
{
	let mut itr = 0;
	let d = StringDecoder2::Init;
	let r = d.evolve(data, &mut |sym|{
		match sym {
			//This is not valid as it impiles end of string.
			PushElement::StdName(_) => assert!(false),
			PushElement::Bytes(b) => for &b in b.iter() {
				handle_byte(b, reference, &mut itr);
			},
			//We should not get end of string.
			PushElement::EndOfString => assert!(false),
			PushElement::EndOfCollection => assert!(false),
		};
	});
	if soft { assert!(r.is_ok()); } else { assert!(r.is_err()); }
	assert_eq!(itr, reference.len());
}

#[test]
fn test_bad_eos()
{
	stringcompressor_ok(&[0x84, 0xFF,0xFF,0xF6,0x7F], &[199]);
	stringcompressor_error(&[0x83, 0xFF,0xFF,0xF6], &[], false);
	stringcompressor_error(&[0x84, 0xFF,0xFF,0xF6,0x7E], &[199], false);
	stringcompressor_error(&[0x84, 0xFF,0xFF,0xF6,0x7D], &[199], false);
	stringcompressor_error(&[0x84, 0xFF,0xFF,0xF6,0x7C], &[199], false);
	stringcompressor_error(&[0x85, 0xFF,0xFF,0xF6,0x7F,0xFF], &[199], false);
	stringcompressor_error(&[0x86, 0xFF,0xFF,0xF6,0x7F,0xFF,0xFF], &[199], false);
	stringcompressor_error(&[0x87, 0xFF,0xFF,0xF6,0x7F,0xFF,0xFF,0xFE], &[199], false);
	stringcompressor_error(&[0x87, 0xFF,0xFF,0xF6,0x7F,0xFF,0xFF,0xFF], &[199], false);
}

#[test]
fn test_toolong_strlenlen()
{
	stringcompressor_error(&[0x7f, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0], &[], false);
	stringcompressor_error(&[0x7f, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0], &[], true);
	stringcompressor_error(&[0xff, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0], &[], false);
	stringcompressor_error(&[0xff, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0], &[], true);
}

#[test]
fn test_huffman_decompressor()
{
	stringcompressor_ok(&[0x81, 0x17], b"2");
	stringcompressor_ok(&[0x82, 0x10,0x7F], b"21");
	stringcompressor_ok(&[0x80], &[]);
	stringcompressor_ok(&[0x81, 0x1F], &[97]);
	stringcompressor_ok(&[0x84, 0xFF, 0xFF, 0xFF, 0xFB], &[22]);
}

#[test]
fn test_stringcompressor()
{
	stringcompressor_ok(b"\x06Hello!", b"Hello!");
	stringcompressor_ok(b"\x7eAwJdrDinOteEjnQVE2q3eiRNiPzbanigtGPjEPBgy24FEg2VlhTQT1qXBlldWInkvbSuyW5N7YeR\
		4EOtmlaDZbEO4XoMBDjCyaG8ReHivSXxvKnQOfceTNowadMRZ4", b"AwJdrDinOteEjnQVE2q3eiRNiPzbanigtGPjEPB\
		gy24FEg2VlhTQT1qXBlldWInkvbSuyW5N7YeR4EOtmlaDZbEO4XoMBDjCyaG8ReHivSXxvKnQOfceTNowadMRZ4");
	stringcompressor_ok(b"\x7f\x03AwJdrDinOteEjnQVE2q3eiRNiPzbanigtGPjEPBgy24FEg2VlhTQT1qXBlldWInkvbSuyW5N7YeR\
		4EOtmlaDZbEO4XoMBDjCyaG8ReHivSXxvKnQOfceTNowadMRZ4XYZW", b"AwJdrDinOteEjnQVE2q3eiRNiPzbanigtGPjEPB\
		gy24FEg2VlhTQT1qXBlldWInkvbSuyW5N7YeR4EOtmlaDZbEO4XoMBDjCyaG8ReHivSXxvKnQOfceTNowadMRZ4XYZW");
	stringcompressor_ok(b"\x82\x64\x02", b"302");
	stringcompressor_ok(b"\x85\xae\xc3\x77\x1a\x4b", b"private");
	stringcompressor_ok(b"\x96\xd0\x7a\xbe\x94\x10\x54\xd4\x44\xa8\x20\x05\x95\x04\x0b\x81\x66\xe0\x82\xa6\
		\x2d\x1b\xff", b"Mon, 21 Oct 2013 20:13:21 GMT");
	stringcompressor_ok(b"\x91\x9d\x29\xad\x17\x18\x63\xc7\x8f\x0b\x97\xc8\xe9\xae\x82\xae\x43\xd3",
		b"https://www.example.com");
	stringcompressor_ok(&[255,3,253,250,251,252,253,248,253,252,251,252,253,251,252,251,249,252,251,252,253,
		248,249,248,250,249,252,251,251,252,252,252,252,251,249,252,253,251,250,251,252,248,249,253,250,
		250,249,249,253,249,250,253,251,253,249,252,248,253,253,253,248,250,251,253,250,253,253,249,250,
		251,250,250,251,253,248,251,250,253,250,251,251,248,253,252,251,248,252,252,252,249,250,250,248,
		251,253,253,252,253,250,253,251,249,250,253,252,249,253,248,250,249,249,250,253,249,248,252,253,
		250,251,249,248,252,252,249,253,248,248,252,249,248,248,251,], b"Z,;XZ&ZX;XZ;X;*X;XZ&*&,*X;;XXXX;*\
		XZ;,;X&*Z,,**Z*,Z;Z*X&ZZZ&,;Z,ZZ*,;,,;Z&;,Z,;;&ZX;&XXX*,,&;ZZXZ,Z;*,ZX*Z&,**,Z*&XZ,;*&XX*Z&&X*&&;");
}

#[test]
fn huffman_encoding()
{
	//These prefixes have 0 to 7 bit alignments.
	static PREFIXES: [&'static [u8]; 8] = [b"&", b"#0", b"!", b"\'", b"#", b"0", b" ", b":"];
	//15-16(120-128) -> 112 (13 suffix chars)
	let mut src = [0;16];
	let mut target = [0;15];
	for i in 0..8 { for j in 0..256 {
		let plen = PREFIXES[i].len();
		(&mut src[..plen]).copy_from_slice(PREFIXES[i]);
		src[plen] = j as u8;
		for k in 1..14 { src[plen+k] = 48; }
		let src = &src[..plen+14];
		let tlen = {
			let mut ss = SliceSink::new(&mut target);
			encode_one(&mut ss, &[src]).unwrap();
			ss.written()
		};
		let target = &target[..tlen];
		stringcompressor_ok(target, src);
	}}

}

#[test]
fn huffman_encoding_single()
{
	//These prefixes have 0 to 7 bit alignments.
	static PREFIXES: [&'static [u8]; 8] = [b"&", b"#0", b"!", b"\'", b"#", b"0", b" ", b":"];
	//15-16(120-128) -> 112 (13 suffix chars)
	let mut src = [0;16];
	let mut target = [0;15];
	for i in 0..8 { for j in 0..256 {
		let plen = PREFIXES[i].len();
		(&mut src[..plen]).copy_from_slice(PREFIXES[i]);
		src[plen] = j as u8;
		for k in 1..14 { src[plen+k] = 48; }
		let src = &src[..plen+14];
		let tlen = {
			let mut ss = SliceSink::new(&mut target);
			encode_one_single(&mut ss, src).unwrap();
			ss.written()
		};
		let target = &target[..tlen];
		stringcompressor_ok(target, src);
	}}

}

#[test]
fn x_http2_reason()
{
	let src = b"x-http2-reason";
	let mut target = [0;15];
	let tlen = {
		let mut ss = SliceSink::new(&mut target);
		encode_one(&mut ss, &[src]).unwrap();
		ss.written()
	};
	let target = &target[..tlen];
	assert_eq!(target, &[138, 242, 180, 233, 77, 98, 90, 194, 141, 7, 171][..]);
}

#[test]
fn x_http2_reason_single()
{
	let src = b"x-http2-reason";
	let mut target = [0;15];
	let tlen = {
		let mut ss = SliceSink::new(&mut target);
		encode_one_single(&mut ss, src).unwrap();
		ss.written()
	};
	let target = &target[..tlen];
	assert_eq!(target, &[138, 242, 180, 233, 77, 98, 90, 194, 141, 7, 171][..]);
}

#[test]
fn code_sanity_check()
{
	//Each code should appear exactly once!
	let mut c = vec![0u32;4096];
	for i in 0..4096 { c[HUFFMAN_DECTAB2[i] as usize] += 1; }
	for j in 0..4096 { assert_eq!(c[j], 1); }
}

#[cfg(test)]
struct EventLog(RefCell<Vec<String>>);

#[cfg(test)]
impl ContextTrait for EventLog
{
	fn new_entry(&mut self)
	{
		self.0.borrow_mut().push(format!("new_entry()"));
	}
	fn readback_name<F>(&self, entry: usize, _: &mut F) -> Result<(), Error>
		where F: FnMut(PushElement)
	{
		self.0.borrow_mut().push(format!("readback_name({entry})"));
		Ok(())
	}
	fn readback_name_value<F>(&self, entry: usize, _: &mut F) -> Result<(), Error>
		where F: FnMut(PushElement)
	{
		self.0.borrow_mut().push(format!("readback_name_value({entry})"));
		Ok(())
	}
	fn push_name_m(&mut self, c: &[u8])
	{
		for &c in c.iter() { self.0.borrow_mut().push(format!("push_name({c})")); }
	}
	fn push_value_m(&mut self, c: &[u8])
	{
		for &c in c.iter() { self.0.borrow_mut().push(format!("push_value({c})")); }
	}
	fn change_window_size(&mut self, winsize: usize) -> Result<(), Error>
	{
		self.0.borrow_mut().push(format!("change_window_size({winsize})"));
		Ok(())
	}
	fn new_entry_with_stdname(&mut self, name: StandardHttpHeader)
	{
		self.0.borrow_mut().push(format!("new_entry_with_name({name})"));
	}
	///Start a new entry, copying name of existing entry.
	fn new_entry_copyname(&mut self, entry: usize) -> Result<(), Error>
	{
		self.0.borrow_mut().push(format!("new_entry_copyname({entry})"));
		Ok(())
	}
}

#[cfg(test)]
struct TError;

#[cfg(test)]
impl ContextTrait for TError
{
	fn new_entry(&mut self)
	{
		unreachable!();	//This is NOT supposed to be called.
	}
	fn readback_name<F>(&self, _: usize, _: &mut F) -> Result<(), Error> where F: FnMut(PushElement)
	{
		unreachable!();	//This is NOT supposed to be called.
	}
	fn readback_name_value<F>(&self, _: usize, _: &mut F) -> Result<(), Error> where F: FnMut(PushElement)
	{
		unreachable!();	//This is NOT supposed to be called.
	}
	fn push_name_m(&mut self, _: &[u8])
	{
		unreachable!();	//This is NOT supposed to be called.
	}
	fn push_value_m(&mut self, _: &[u8])
	{
		unreachable!();	//This is NOT supposed to be called.
	}
	fn new_entry_with_stdname(&mut self, _: StandardHttpHeader)
	{
		unreachable!();	//This is NOT supposed to be called.
	}
	fn change_window_size(&mut self, _: usize) -> Result<(), Error>
	{
		unreachable!();	//This is NOT supposed to be called.
	}
	fn new_entry_copyname(&mut self, _: usize) -> Result<(), Error>
	{
		unreachable!();	//This is NOT supposed to be called.
	}
}

#[cfg(test)]
fn opcode_decode2(data: &[u8], log: &[&str])
{
	let mut d = Decoder::new();
	let mut ctx = EventLog(RefCell::new(Vec::new()));
	d._push(data, &mut ctx, |_|()).unwrap();
	let mut i = 0;
	while i < ctx.0.borrow().len() && i < log.len() {
		assert_eq!(&ctx.0.borrow()[i], log[i]);
		i += 1;
	}
	assert_eq!(ctx.0.borrow().len(), log.len());
}

#[test]
fn read_opcodes()
{
	opcode_decode2(&[15,47], &["readback_name(0)"]);
	opcode_decode2(&[31,47], &["readback_name(0)"]);
	opcode_decode2(&[126], &["readback_name(0)", "new_entry_copyname(0)"]);
	opcode_decode2(&[190], &["readback_name_value(0)"]);
	opcode_decode2(&[127,0], &["readback_name(1)", "new_entry_copyname(1)"]);
	opcode_decode2(&[254], &["readback_name_value(64)"]);
	opcode_decode2(&[255,0], &["readback_name_value(65)"]);
	opcode_decode2(&[15,130,1], &["readback_name(83)"]);
	opcode_decode2(&[31,130,1], &["readback_name(83)"]);
	opcode_decode2(&[63,130,1], &["change_window_size(161)"]);
	opcode_decode2(&[127,130,1], &["readback_name(131)", "new_entry_copyname(131)"]);
	opcode_decode2(&[255,130,1], &["readback_name_value(195)"]);
}

#[test]
fn read_opcodes_named()
{
	headercompressor_ok(b"\x00\x03foo\x04test", &[b"foo", b"test"], &mut TError);
	headercompressor_ok(b"\x10\x03foo\x04test", &[b"foo", b"test"], &mut TError);
	opcode_decode2(b"\x40\x03foo\x04test", &["new_entry()", "push_name(102)", "push_name(111)", "push_name(111)",
		"push_value(116)", "push_value(101)", "push_value(115)", "push_value(116)"]);
}


#[test]
fn static_scan()
{
	use crate::escape_bytes;
	for i in 0..14 {
		headercompressor_ok(&[1+i as u8, 0], &[get_static_name_test(i), b""], &mut TError);
		headercompressor_ok(&[17+i as u8, 0], &[get_static_name_test(i), b""], &mut TError);
	}
	for i in 14..61 {
		headercompressor_ok(&[15,i as u8 - 14, 0], &[get_static_name_test(i), b""], &mut TError);
		headercompressor_ok(&[31,i as u8 - 14, 0], &[get_static_name_test(i), b""], &mut TError);
	}
	for i in 0..61 {
		let name = escape_bytes(get_static_name_test(i));
		opcode_decode2(&[65+i as u8], &[&format!("new_entry_with_name({name})")]);
		headercompressor_ok(&[129+i as u8], &[get_static_name_test(i), get_static_value(i).unwrap()],
			&mut TError);
	}

}


#[test]
fn read_opcodes_overflow()
{
	opcode_decode2(&[15,128,128,128,128,128,128,128,128,128,128,128,128,127], &["readback_name(266338257)"]);
	opcode_decode2(&[31,128,128,128,128,128,128,128,128,128,128,128,128,127], &["readback_name(266338257)"]);
	opcode_decode2(&[63,128,128,128,128,128,128,128,128,128,128,128,128,127],
		&["change_window_size(266338335)"]);
	opcode_decode2(&[127,128,128,128,128,128,128,128,128,128,128,128,128,127],
		&["readback_name(266338305)", "new_entry_copyname(266338305)"]);
	opcode_decode2(&[255,128,128,128,128,128,128,128,128,128,128,128,128,127],
		&["readback_name_value(266338369)"]);
}

#[cfg(test)]
fn test_call_new_stdname(context: &mut Context, name: StandardHttpHeader)
{
	let entry = context.wptr;
	context.new_entry_with_stdname(name);
	assert_eq!(tab_load16(&context.buffer, entry + ENTRY_NLEN_OFF), name.strlen() as u16);
	assert_eq!(tab_load16(&context.buffer, entry + ENTRY_VLEN_OFF), 0u16);
	assert_eq!(tab_load16(&context.buffer, entry + ENTRY_NID_OFF), name as u16);
	assert_eq!(context.wptr, entry + (name.strlen() as u32 + ENTRY_HEADER_LEN));
}

#[test]
fn context_write_offset()
{
	let mut context = Context::new();
	assert_eq!(context.wptr, ModTS(0));
	for i in 1..MAX_STANDARD_HEADER_ID+1 {
		test_call_new_stdname(&mut context, StandardHttpHeader::from_id(i).expect("bad id?"));
		context.push_value_m(b"foo");
		assert_eq!(tab_load16(&context.buffer, context.eptr + ENTRY_VLEN_OFF), 3u16);
	}
}

#[cfg(test)]
fn make_copyback_test(std: bool, not_quite: bool)
{
	let mut context = Context::new();
	for i in 1..33 {	//32 entries.
		let almost = (not_quite && i == 32) as usize;
		let vpad = b"ABCDEFGHIJKLMNOPQRSTUVWXYZ012345ABCDEFGHIJKLMNOPQRSTUVWXYZ012345\
			ABCDEFGHIJKLMNOPQRSTUVWXYZ012345";
		let n = StandardHttpHeader::from_id(i).expect("bad id?");
		if std {
			context.new_entry_with_stdname(n);
		} else {
			context.new_entry();
			context.push_name_m(n.to_str().as_bytes());
		}
		context.push_value_m(&vpad[..96-n.strlen()-almost]);
	}
	assert_eq!(context.count, 32);		//32 headers.
	let wp = if not_quite { 4095 } else { 0 };
	assert_eq!(context.wptr, ModTS(wp));	//The write pointer has done (almost) full loop.

	let entry = context.wptr;
	context.new_entry_copyname(31).expect("Should have this entry");
	//The copied entry should be standard header with id 1 or nonstandard header, and the value should be
	//cleared. There should still be 32 headers.
	assert_eq!(context.count, 32);
	let n1 = StandardHttpHeader::from_id(1).expect("bad id?");
	assert_eq!(tab_load16(&context.buffer, entry + ENTRY_NLEN_OFF), n1.strlen() as u16);
	assert_eq!(tab_load16(&context.buffer, entry + ENTRY_VLEN_OFF), 0u16);
	assert_eq!(tab_load16(&context.buffer, entry + ENTRY_NID_OFF), std as u16);
	assert_eq!(context.wptr, entry + (n1.strlen() as u32 + ENTRY_HEADER_LEN));
	//The name should be preserved, if it is nonstandard.
	if !std {
		let n1s = n1.to_str().as_bytes();
		let mut nptr = entry + ENTRY_HEADER_LEN;
		for i in 0..n1s.len() {
			assert_eq!(n1s[i], nptr.get(&context.buffer));
			nptr = nptr + 1;
		}
	}
}

#[test]
fn copyback_nid_full_loop()
{
	make_copyback_test(true, false);
}

#[test]
fn copyback_nid_full_loop_nonstandard()
{
	make_copyback_test(false, false);
}

#[test]
fn copyback_nid_almost_full_loop()
{
	make_copyback_test(true, true);
}

#[test]
fn copyback_nid_almost_full_loop_nonstandard()
{
	make_copyback_test(false, true);
}

#[cfg(test)]
fn uncompress_roundtrip(data: &[u8])
{
	let mut cstr = String::new();
	//Concatenate all codes and pad to byte with 1s.
	for &i in data { cstr.push_str(HUFFMAN_TESTTAB2[i as usize]); }
	while cstr.len() % 8 != 0 { cstr.push('1'); }

	let slen = cstr.len()/8;
	let mut output = Vec::new();
	if slen < 127 {
		output.push(slen as u8 | 128);
	} else {
		let mut x = slen - 127;
		output.push(255);
		while x >= 128 {
			output.push(128 | x as u8);
			x >>= 7;
		}
		output.push(x as u8);
	}
	for i in 0..cstr.len()/8 {
		let b = &cstr[8*i..8*i+8];
		output.push(u8::from_str_radix(b, 2).expect("Bad binary number"));
	}
	stringcompressor_ok(&output[..], data);
}

#[test]
fn roundtrip_random()
{
	use std::io::Read;
	let mut b = [0;256];
	let mut fp = std::fs::File::open("/dev/urandom").expect("Open");
	for _ in 0..1000 {
		fp.read_exact(&mut b).expect("Read");
		uncompress_roundtrip(&b);
	}
}


#[test]
fn roundtrip_bytes()
{
	let mut b = [0;256];
	for i in 0..256 { b[i] =  i as u8; }
	uncompress_roundtrip(&b);
}

#[cfg(test)]
fn predict_double(ostate: usize, b: usize) -> bool
{
	//Intermediate states <= 10 always produce doubles.
	let w = HUFFMAN_DECTAB2[b / 16 * 256 + ostate];
	let l = b as u64 >> 1 & 7;
	let c = HUFFMAN_DECDBL_MAGIC2 >> w/256%16*4 & 15;
	l < c
}

#[test]
fn huffman_doubles()
{
	let mut wrong = 0;
	for ostate in 0..256usize {
		for b in 0..256usize {
			let mut is = true;
			let mut state = ostate as u8;
			let w = HUFFMAN_DECTAB2[b / 16 * 256 + state as usize];
			if w < 3840 {
				state = (w >> 8) as u8;
			} else if w & 0xFF >= 15 {
				is = false;
			} else {
				continue;
			}
			let w = HUFFMAN_DECTAB2[b % 16 * 256 + state as usize];
			if w < 3840 { } else if w & 0xFF >= 15 { is = false; } else { continue; }
			if is != predict_double(ostate, b) {
				let tgt = HUFFMAN_DECTAB2[b / 16 * 256 + ostate];
				println!("({ostate},{b:02x}): {is} ({tgt:03x})");
				wrong += 1;
			}
		}
	}
	assert!(wrong == 0);
}

#[test]
fn test_huffman_frags()
{
	for state in 0..256 {
		for byte in 0..256 {
			let mut state1 = state as u16;
			let mut state2 = state as u8;
			let mut bad = false;
			let mut oitr2 = 0;
			let mut out1 = [0;2];
			let mut out2 = [0;2];
			let r = unpack_huffman_frag(&mut state1, &[byte as u8], &mut out1);
			let w = HUFFMAN_DECTAB2[byte / 16 * 256 + state2 as usize];
			if w < 3840 {
				out2[oitr2] = w as u8;
				oitr2 += 1;
				state2 = (w >> 8) as u8;
			} else if w & 0xFF >= 15 {
				state2 = w as u8;
			} else {
				bad = true;
			}
			let w = HUFFMAN_DECTAB2[byte % 16 * 256 + state2 as usize];
			if !bad && w < 3840 {
				out2[oitr2] = w as u8;
				oitr2 += 1;
				state2 = (w >> 8) as u8;
			} else if !bad && w & 0xFF >= 15 {
				state2 = w as u8;
			} else {
				bad = true;
			}
			match r {
				Ok((inc, outc)) => {
					assert!(state1 == state2 as u16);
					assert!(!bad);
					assert!(inc == 1);
					assert_eq!(&out1[..outc], &out2[..oitr2]);
				},
				Err((outc, _)) => {
					assert!(bad);
					assert_eq!(&out1[..outc], &out2[..oitr2]);
				}
			}
		}
	}
}

#[test]
fn huffman_split_double()
{
	use std::collections::BTreeMap;
	let mut zero: BTreeMap<(u16, u8), u16> = BTreeMap::new();
	let mut one: BTreeMap<(u16, u8), (u8, u16)> = BTreeMap::new();
	let mut two: BTreeMap<(u16, u8), ([u8;2], u16)> = BTreeMap::new();
	for s in 0..256 {
		for b in 0..256 {
			let s = s as u16;
			let b = b as u8;
			let mut out = [0;2];
			let mut s2 = s;
			let b2 = [b];
			match unpack_huffman_frag(&mut s2, &b2, &mut out) {
				Ok((1, 2)) => {two.insert((s, b), (out, s2));},
				Ok((1, 1)) => {one.insert((s, b), (out[0], s2));},
				Ok((1, 0)) => {zero.insert((s, b), s2);},
				_ => (),
			}
		}
	}
	for (&(s, b), &refstate) in zero.iter() {
		let mut out = [0;1];
		let mut s2 = s;
		let b2 = [b];
		match unpack_huffman_frag(&mut s2, &b2, &mut out) {
			Ok((1, 0)) => assert_eq!(s2, refstate),
			x => panic!("unpack_huffman_frag(1): {x:?}")
		}
		match unpack_huffman_frag(&mut s2, &[], &mut out) {
			Ok((0, 0)) => assert_eq!(s2, refstate),
			x => panic!("unpack_huffman_frag(2): {x:?}")
		}
	}
	for (&(s, b), &(refout, refstate)) in one.iter() {
		let mut out = [0;1];
		let mut s2 = s;
		let b2 = [b];
		match unpack_huffman_frag(&mut s2, &b2, &mut out) {
			Ok((1, 1)) => {
				assert_eq!(out[0], refout);
				assert_eq!(s2, refstate);
			},
			x => panic!("unpack_huffman_frag(3): {x:?}")
		}
		match unpack_huffman_frag(&mut s2, &[], &mut out) {
			Ok((0, 0)) => assert_eq!(s2, refstate),
			x => panic!("unpack_huffman_frag(4): {x:?}")
		}
	}
	for (&(s, b), &(refout, refstate)) in two.iter() {
		let mut out = [0;1];
		let mut s2 = s;
		let b2 = [b];
		match unpack_huffman_frag(&mut s2, &b2, &mut out) {
			Ok((1, 1)) => assert_eq!(out[0], refout[0]),
			x => panic!("unpack_huffman_frag(5): {x:?}")
		}
		match unpack_huffman_frag(&mut s2, &[], &mut out) {
			Ok((0, 1)) => {
				assert_eq!(out[0], refout[1]);
				assert_eq!(s2, refstate);
			},
			x => panic!("unpack_huffman_frag(6): {x:?}")
		}
	}
}

#[test]
fn encode_reg_headers()
{
	static HEADERS: [&'static str; 329] = ["a-im", "accept", "accept-additions", "accept-charset",
		"accept-datetime", "accept-encoding", "accept-features", "accept-language", "accept-patch",
		"accept-post", "accept-ranges", "age", "allow", "alpn", "also-control", "alt-svc", "alt-used",
		"alternate-recipient", "alternates", "apply-to-redirect-ref", "approved",
		"arc-authentication-results", "arc-message-signature", "arc-seal", "archive", "archived-at",
		"article-names", "article-updates", "authentication-control", "authentication-info",
		"authentication-results", "authorization", "auto-submitted", "autoforwarded", "autosubmitted",
		"base", "bcc", "body", "c-ext", "c-man", "c-opt", "c-pep", "c-pep-info", "cache-control",
		"cal-managed-id", "caldav-timezones", "cancel-key", "cancel-lock", "cc", "cdn-loop", "close",
		"comments", "connection", "content-alternative", "content-base", "content-description",
		"content-disposition", "content-duration", "content-encoding", "content-features", "content-id",
		"content-identifier", "content-language", "content-length", "content-location", "content-md5",
		"content-range", "content-return", "content-script-type", "content-style-type",
		"content-transfer-encoding", "content-translation-type", "content-type", "content-version",
		"control", "conversion", "conversion-with-loss", "cookie", "cookie2", "dasl", "dav",
		"dl-expansion-history", "date", "date-received", "default-style", "deferred-delivery",
		"delivery-date", "delta-base", "depth", "derived-from", "destination", "differential-id", "digest",
		"discarded-x400-ipms-extensions", "discarded-x400-mts-extensions", "disclose-recipients",
		"disposition-notification-options", "disposition-notification-to", "distribution", "dkim-signature",
		"downgraded-bcc", "downgraded-cc", "downgraded-disposition-notification-to",
		"downgraded-final-recipient", "downgraded-from", "downgraded-in-reply-to", "downgraded-mail-from",
		"downgraded-message-id", "downgraded-original-recipient", "downgraded-rcpt-to",
		"downgraded-references", "downgraded-reply-to", "downgraded-resent-bcc", "downgraded-resent-cc",
		"downgraded-resent-from", "downgraded-resent-reply-to", "downgraded-resent-sender",
		"downgraded-resent-to", "downgraded-return-path", "downgraded-sender", "downgraded-to", "early-data",
		"encoding", "encrypted", "etag", "expect", "expect-ct", "expires", "expiry-date", "ext",
		"followup-to", "forwarded", "from", "generate-delivery-report", "getprofile", "hobareg", "host",
		"http2-settings", "im", "if", "if-match", "if-modified-since", "if-none-match", "if-range",
		"if-schedule-tag-match", "if-unmodified-since", "importance", "in-reply-to",
		"include-referred-token-binding-id", "incomplete-copy", "injection-date", "injection-info",
		"keep-alive", "keywords", "label", "language", "last-modified", "latest-delivery-time", "lines",
		"link", "list-archive", "list-help", "list-id", "list-owner", "list-post", "list-subscribe",
		"list-unsubscribe", "list-unsubscribe-post", "location", "lock-token", "man", "max-forwards",
		"memento-datetime", "message-context", "message-id", "message-type", "meter", "mime-version",
		"mmhs-exempted-address", "mmhs-extended-authorisation-info", "mmhs-subject-indicator-codes",
		"mmhs-handling-instructions", "mmhs-message-instructions", "mmhs-codress-message-indicator",
		"mmhs-originator-reference", "mmhs-primary-precedence", "mmhs-copy-precedence", "mmhs-message-type",
		"mmhs-other-recipients-indicator-to", "mmhs-other-recipients-indicator-cc",
		"mmhs-acp127-message-identifier", "mmhs-originator-plad", "mt-priority", "negotiate", "newsgroups",
		"nntp-posting-date", "nntp-posting-host", "obsoletes", "opt", "optional-www-authenticate",
		"ordering-type", "organization", "origin", "original-encoded-information-types", "original-from",
		"original-message-id", "original-recipient", "original-sender", "originator-return-address",
		"original-subject", "oscore", "overwrite", "p3p", "path", "pep", "pics-label", "pep-info",
		"position", "posting-version", "pragma", "prefer", "preference-applied",
		"prevent-nondelivery-report", "priority", "profileobject", "protocol", "protocol-info",
		"protocol-query", "protocol-request", "proxy-authenticate", "proxy-authentication-info",
		"proxy-authorization", "proxy-features", "proxy-instruction", "public", "public-key-pins",
		"public-key-pins-report-only", "range", "received", "received-spf", "redirect-ref", "references",
		"referer", "relay-version", "replay-nonce", "reply-by", "reply-to", "require-recipient-valid-since",
		"resent-bcc", "resent-cc", "resent-date", "resent-from", "resent-message-id", "resent-reply-to",
		"resent-sender", "resent-to", "retry-after", "return-path", "safe", "schedule-reply", "schedule-tag",
		"sec-token-binding", "sec-websocket-accept", "sec-websocket-extensions", "sec-websocket-key",
		"sec-websocket-protocol", "sec-websocket-version", "security-scheme", "see-also", "sender",
		"sensitivity", "server", "set-cookie", "set-cookie2", "setprofile", "slug", "soapaction",
		"solicitation", "status-uri", "strict-transport-security", "subject", "summary", "sunset",
		"supersedes", "surrogate-capability", "surrogate-control", "tcn", "te", "timeout",
		"tls-report-domain", "tls-report-submitter", "to", "topic", "trailer", "transfer-encoding", "ttl",
		"urgency", "uri", "upgrade", "user-agent", "variant-vary", "vary", "vbr-info", "via",
		"www-authenticate", "want-digest", "warning", "x-forwarded-for", "x-forwarded-host",
		"x-forwarded-proto", "x-forwarded-server", "x-real-ip", "x400-content-identifier",
		"x400-content-return", "x400-content-type", "x400-mts-identifier", "x400-originator",
		"x400-received", "x400-recipients", "x400-trace", "x-content-type-options", "x-frame-options",
		"x-http2-reason", "xref",
		//Nonstandard stuff.
		"content-security-policy", "default-src 'none'", "nosniff", "text/plain;charset=utf-8",
		"INTERNAL ERROR: DOUBLE FAULT"
	];
	let mut used = ::std::collections::HashMap::new();
	use std::fmt::Write as FW;
	for src in HEADERS.iter() {
		//Compress with both encode_one_single() and __encode_hpack_single() in order to check that the
		//cached values are correct.
		let mut target = Vec::new();
		let mut target2 = Vec::new();
		encode_one_single(&mut target, src.as_bytes()).unwrap();
		__encode_hpack_single(&mut target2, src.as_bytes()).unwrap();
		assert_eq!(target, target2);
		//unpack it with unpack_huffman_frag() in order to test correctness without caching.
		if target[0] >= 128 {
			let mut check = [0;256];
			let mut state = 11;
			let (inc, outc) = unpack_huffman_frag(&mut state, &target[1..], &mut check).
				expect("huffman decompress failed");
			assert!(inc + 1 == target.len());
			assert_eq!(src.as_bytes(), &check[..outc]);
			assert!(state >= 11 && state < 19);
		} else {
			assert!(target[0] == src.len() as u8);
			assert_eq!(src.as_bytes(), &target[1..]);
		}
		stringcompressor_ok(&target, src.as_bytes());
		let mut id = 0u32;
		for j in src.as_bytes().iter() {
			id = (id << 8).wrapping_add(id).wrapping_add(*j as u32);
		}
		if let Some(oitem) = used.get(&id) {
			println!("Collision between '{oitem}' and '{src}' for id {id}");
			assert!(false);
		}
		used.insert(id, src);
		let mut targete = String::new();
		for c in target.iter().cloned() { write!(targete, "\\x{c:02x}").unwrap(); }
	}
}

#[test]
fn standard_header_ids()
{
	for i in 1..MAX_STANDARD_HEADER_ID+1 {
		let j = StandardHttpHeader::from_id(i).expect("Bad id?");
		let k = j.to_str();
		let l = StandardHttpHeader::from_str(k).expect("Bad name?");
		assert!(j == l);
	}
}

#[test]
fn encode_static_table()
{
	for i in 0..61 {
		let name2 = get_static_name(i).expect("index out of range");
		let value = get_static_value(i).expect("index out of range");
		let name = name2.to_str().as_bytes();
		if value.len() > 0 {
			let mut a: Vec<u8> = Vec::new();
			let mut b: Vec<u8> = Vec::new();
			hpack_encode_header(&mut a, name, value).unwrap();
			hpack_encode_header_std(&mut b, name2, value).unwrap();
			assert_eq!(a, b);
			assert_eq!(&a, &[i as u8 + 129]);
		}
		//Blacklist the following entries from the test, because these have the same name as the previous,
		//so get confused with it.
		if i == 2 || i == 4 || i == 6 || i >= 8 && i <= 13 { continue; }
		let mut a: Vec<u8> = Vec::new();
		let mut b: Vec<u8> = Vec::new();
		hpack_encode_header(&mut a, name, b"x").unwrap();
		hpack_encode_header_std(&mut b, name2, b"x").unwrap();
		assert_eq!(a, b);
		assert_eq!(&a, &[i as u8 + 65, 1, 120]);
	}
}
