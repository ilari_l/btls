#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]

#[cfg(test)] #[macro_use] extern crate std;
#[cfg(test)] extern crate btls_aux_random;
#[cfg(test)]use btls_aux_collections::String;


pub mod http;
pub mod http1;
pub mod http2;
mod http2_frame;
mod http2_hpack;
pub mod status;

//For testing.
#[doc(hidden)] pub use crate::http2_hpack::__encode_hpack_hard;
#[doc(hidden)] pub use crate::http2_hpack::__encode_hpack_single;
#[doc(hidden)] pub use crate::http2_hpack::unpack_huffman_frag as __unpack_huffman_frag;
#[doc(hidden)] pub use crate::http2_hpack::hpack_encode_header as __hpack_encode_header;

#[cfg(test)]
fn escape_bytes(x: &[u8]) -> String
{
	use btls_aux_memory::EscapeByteString;
	use btls_aux_collections::ToString;
	EscapeByteString(x).to_string()
}
