//!HTTP/2 frame parsing.
#![forbid(unused_must_use)]
#![forbid(missing_docs)]

use btls_aux_collections::Cow;
use btls_aux_fail::dtry;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::fail_if_none;
use btls_aux_fail::ResultExt;
use btls_aux_serialization::Sink;
use btls_aux_serialization::SinkMarker;
use btls_aux_serialization::SliceSink;
use btls_aux_serialization::Source;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::fmt::Write as FmtWrite;
use core::ops::Deref;

struct LengthCounter(usize);

impl Sink for LengthCounter
{
	fn write_slice(&mut self, s: &[u8]) -> Result<(), ()>
	{
		self.0 += s.len();
		Ok(())
	}
	fn _alter(&mut self, _: usize, _: u8)
	{
		//This does not change length.
	}
	fn _readback(&self, _: usize) -> u8
	{
		0	//Can not be properly answered.
	}
	fn written(&self) -> usize { self.0 }
	fn _pop(&mut self, amount: usize) { self.0 = self.0.saturating_sub(amount); }
	fn callback_after(&self, _: SinkMarker, _: &mut dyn FnMut(&[u8])->())
	{
		//Do not actually call the callback.
	}
}

///The length of HTTP/2 frame header.
pub const HDR_LEN: usize = 9;

///Frame type DATA.
pub const FTYPE_DATA: u8 = 0;
///Frame type HEADERS.
pub const FTYPE_HEADERS: u8 = 1;
///Frame type PRIORITY.
pub const FTYPE_PRIORITY: u8 = 2;
///Frame type RST_STREAM.
pub const FTYPE_RST_STREAM: u8 = 3;
///Frame type SETTINGS.
pub const FTYPE_SETTINGS: u8 = 4;
///Frame type PUSH_PROMISE.
pub const FTYPE_PUSH_PROMISE: u8 = 5;
///Frame type PING.
pub const FTYPE_PING: u8 = 6;
///Frame type GOWAWAY.
pub const FTYPE_GOAWAY: u8 = 7;
///Frame type WINDOW_UPDATE.
pub const FTYPE_WINDOW_UPDATE: u8 = 8;
///Frame type CONTINUATION.
pub const FTYPE_CONTINUATION: u8 = 9;

///ACK flag.
pub const FLAG_ACK: u8 = 1;
///EOS flag.
pub const FLAG_EOS: u8 = 1;
///EOH flag
pub const FLAG_EOH: u8 = 4;
///PAD flag.
pub const FLAG_PAD: u8 = 8;
///PRIORITY flag.
pub const FLAG_PRIORITY: u8 = 32;

const STREAM_MASK: u32 = 0x7FFFFFFF;

///A stream ID, possibly 0.
#[derive(Copy,Clone,Debug,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct StreamId(u32);

impl StreamId
{
	///Return stream id 0.
	pub fn zero() -> StreamId { StreamId(0) }
	///Create a new stream id with value `v`.
	///
	///If stream ID is valid (in range 0 to `2^31-1`), returns `Some(w)`, where `w` is stream ID with value
	///`v`. If stream ID is invalid (bigger than `2^31-1`), returns `None`.
	pub fn new(v: u32) -> Option<StreamId>
	{
		fail_if_none!(v > STREAM_MASK);
		Some(StreamId(v))
	}
	///Get the wrapped stream ID.
	pub fn into_inner(self) -> u32 { self.0 }
	///Return true if this is stream 0, false otherwise.
	pub fn is_zero(self) -> bool { self.0 == 0 }
	///Convert a nonzero stream id `v` into `StreamIdNonZero`.
	///
	///If the stream ID is nonzero, returns `Some(s)`, where `s` is the same as `v`. Otherwise returns `None`.
	pub fn into_nonzero(self) -> Option<StreamIdNonZero>
	{
		fail_if_none!(self.0 == 0);
		Some(StreamIdNonZero(self.0))
	}
	///Write the stream id into stream `target`.
	pub fn write<S:Sink>(&self, target: &mut S) -> Result<(), ()> { target.write_u32(self.0) }
}

impl Display for StreamId
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> { Display::fmt(&self.0, f) }
}

///A nonzero stream ID.
#[derive(Copy,Clone,Debug,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct StreamIdNonZero(u32);

impl StreamIdNonZero
{
	///Create a new stream id with value `v`.
	///
	///If stream ID is valid (in range 1 to `2^31-1`), returns `Some(w)`, where `w` is stream ID with value
	///`v`. If stream ID is invalid (0 or bigger than `2^31-1`), returns `None`.
	pub fn new(v: u32) -> Option<StreamIdNonZero>
	{
		fail_if_none!(v == 0 || v > STREAM_MASK);
		Some(StreamIdNonZero(v))
	}
	///Get the wrapped stream ID.
	pub fn into_inner(self) -> u32 { self.0 }
	///Return true if frame number is even, false otherwise.
	pub fn is_even(self) -> bool { self.0 % 2 == 0 }
	///Convert to stream id maybe 0.
	pub fn into_stream_id(self) -> StreamId { StreamId(self.0) }
	///Write the stream id into stream `target`.
	pub fn write<S:Sink>(&self, target: &mut S) -> Result<(), ()> { target.write_u32(self.0) }
}

impl Display for StreamIdNonZero
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> { Display::fmt(&self.0, f) }
}

///No padding.
pub const NO_PADDING: PaddingAmount = PaddingAmount(0);

///An amount of padding
#[derive(Copy,Clone,Debug,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct PaddingAmount(u16);

impl PaddingAmount
{
	///Create a new amount of padding with value `v`.
	///
	///If amount of padding is valid (in range 0 to 256), returns `Some(w)`, where `w` is amount with value
	///`v`. If amount is invalid (bigger than 256), returns `None`.
	pub fn new(v: u32) -> Option<PaddingAmount>
	{
		fail_if_none!(v > 256);
		Some(PaddingAmount(v as u16))
	}
	///Get the wrapped amount.
	pub fn amount(self) -> usize { self.0 as usize }
	///Padding is present?
	pub fn present(&self) -> bool { self.0 > 0 }
	fn write_amount(&self, target: &mut impl Sink) -> Result<(), ()>
	{
		if self.0 > 0 { target.write_u8((self.0 - 1) as u8) } else { Ok(()) }
	}
	fn write_padding(&self, target: &mut impl Sink) -> Result<(), ()>
	{
		for _ in 1..self.0 { target.write_u8(0)?; }
		Ok(())
	}
}

///An amount of window increment
#[derive(Copy,Clone,Debug,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub struct WindowIncrement(i32);

impl WindowIncrement
{
	///Create a new amount of increment with value `v`.
	///
	///If amount of increment is valid (in range 1 to `2^31-1`), returns `Some(w)`, where `w` is amount with
	///value `v`. If amount is invalid (0 or bigger than `2^31-1`), returns `None`.
	pub fn new(v: u32) -> Option<WindowIncrement>
	{
		let v = v as i32;
		fail_if_none!(v <= 0);
		Some(WindowIncrement(v))
	}
	///Get the wrapped amount.
	pub fn amount(self) -> i32 { self.0 }
	///Increase window size by this.
	pub fn increase_by(&self, amt: &mut i32) -> Result<(), ()>
	{
		*amt = dtry!(amt.checked_add(self.0));
		Ok(())
	}
}

///Error in parsing a frame.
#[derive(Copy,Clone,Debug)]
pub struct FrameParseError(_FrameParseError);

#[derive(Copy,Clone,Debug)]
enum _FrameParseError
{
	///Illegal stream ID.
	IllegalStreamId(u64, u32),
	///Other errors (stream ID is legal).
	Other(u64, u32, u8, u8, StreamId, FrameParseErrorKind),
}

impl Display for FrameParseError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&FrameParseError(_FrameParseError::IllegalStreamId(seqno, sid)) =>
				write!(f, "Frame #{seqno}: Illegal stream ID {sid}"),
			&FrameParseError(_FrameParseError::Other(seqno, len, ftype, flags, sid, err)) =>
				write!(f, "Frame #{seqno}[length={len}, type={ftype}, flags={flags:02x}, \
					stream={sid}]: {err}"),
		}
	}
}

#[derive(Copy,Clone,Debug)]
enum FrameParseErrorKind
{
	EmptyHeaderFragment,			//Header fragment is empty.
	ExpectedContinuation(StreamId),		//Expected CONTINUATION frame for specified stream.
	ExpectedSettings,			//Expected SETTINGS frame.
	IllegalFrameSize,			//Illegal size for frame.
	IllegalFrameStream,			//Illegal stream for frame.
	IllegalLastStreamId(u32),		//Illegal last stream ID.
	IllegalPromisedStream(u32),		//Illegal promised stream ID.
	IllegalWindowIncrement(i32),		//Illegal window increment.
	TruncatedPadding,			//Frame with padding has incomplete padding.
	TruncatedPriority,			//HEADERS frame has incomplete priority field.
	UnexpectedContinuation,			//unexpected continuation.
}

impl Display for FrameParseErrorKind
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::FrameParseErrorKind::*;
		match self {
			&EmptyHeaderFragment => write!(f, "Empty header fragment"),
			&ExpectedContinuation(sid) => write!(f, "Expected CONTINUATION on stream {sid}"),
			&ExpectedSettings => write!(f, "Expected SETTINGS frame"),
			&IllegalFrameSize => write!(f, "Illegal size for frame type"),
			&IllegalFrameStream => write!(f, "Illegal frame type on stream"),
			&IllegalLastStreamId(sid) => write!(f, "Illegal last stream id {sid}"),
			&IllegalPromisedStream(sid) => write!(f, "Illegal promised stream id {sid}"),
			&IllegalWindowIncrement(sid) => write!(f, "Illegal window increment {sid}"),
			&TruncatedPadding => write!(f, "Truncated frame padding"),
			&TruncatedPriority => write!(f, "Truncated priority field in HEADERS"),
			&UnexpectedContinuation => write!(f, "Unexpected CONTINUATION"),
		}
	}
}


fn unpad<'a>(data: &'a [u8]) -> Result<&'a [u8], ()>
{
	fail_if!(data.len() < 1, ());
	let (pad, data) = data.split_at(1);
	let pad = pad[0] as usize;
	let dend = dtry!(data.len().checked_sub(pad));
	dtry!(NORET data.get(..dend))
}

fn priority(d: &mut Source) -> Result<Priority, ()>
{
	let r = dtry!(d.u32());
	let w = dtry!(d.u8());
	Ok(Priority {
		exclusive: r >> 31 != 0,
		//This can not actually fail.
		dependency: dtry!(StreamId::new(r & STREAM_MASK)),
		weight: w
	})
}

fn write_frame<S:Sink>(target: &mut S, ftype: u8, flags: u8, strmid: StreamId,
	body: impl FnOnce(&mut S) -> Result<(), ()>) -> Result<usize, ()>
{
	target.write_u24(0)?;
	target.write_u8(ftype)?;
	target.write_u8(flags)?;
	strmid.write(target)?;
	body(target)?;
	let plen = target.written() - HDR_LEN;
	target.alter_u24(0, plen as u32)?;
	Ok(target.written())
}

///HTTP/2 DATA frame.
#[derive(Clone,PartialEq,Eq,Debug)]
pub struct DataFrame<'a>
{
	///The stream id.
	pub strmid: StreamIdNonZero,
	///The data payload.
	pub payload: Cow<'a, [u8]>,
	///True means that the stream ends here, false means there may be more data on this stream.
	pub eos: bool,
	///The amount of padding inserted.
	pub padding: PaddingAmount,
}

impl<'a> DataFrame<'a>
{
	fn parse(strmid: StreamId, d: &'a [u8], flags: u8) -> Result<DataFrame<'a>, FrameParseErrorKind>
	{
		use self::FrameParseErrorKind::*;
		//Unpad if there is padding.
		let (d, padding) = if flags & FLAG_PAD != 0 {
			let p = *d.get(0).unwrap_or(&0);
			let d = unpad(d).set_err(TruncatedPadding)?;
			(d, PaddingAmount(p as u16 + 1))
		} else {
			(d, PaddingAmount(0))
		};
		Ok(DataFrame {
			strmid: strmid.into_nonzero().ok_or(IllegalFrameStream)?,
			payload: Cow::Borrowed(d),
			eos: flags & FLAG_EOS != 0,
			padding: padding,
		})
	}
	fn _serialize(&self, mut target: impl Sink) -> Result<usize, ()>
	{
		let mut flags = 0;
		if self.eos { flags |= FLAG_EOS; }
		if self.padding.present() { flags |= FLAG_PAD; }
		write_frame(&mut target, FTYPE_DATA, flags, self.strmid.into_stream_id(), |target|{
			self.padding.write_amount(target)?;
			target.write_slice(self.payload.deref())?;
			self.padding.write_padding(target)
		})
	}
}

///HTTP/2 priority information.
#[derive(Copy,Clone,PartialEq,Eq,Debug)]
pub struct Priority
{
	///The exclusive flag.
	pub exclusive: bool,
	///The stream this stream depends on, 0 means this is top-level stream.
	pub dependency: StreamId,
	///The weight of this stream - 1.
	pub weight: u8,
}

impl Priority
{
	fn serialize(&self, target: &mut impl Sink) -> Result<(), ()>
	{
		let r = self.dependency.into_inner() | if self.exclusive { 1 << 31 } else { 0 };
		target.write_u32(r)?;
		target.write_u8(self.weight)
	}
}

///HTTP/2 HEADERS frame.
#[derive(Clone,PartialEq,Eq,Debug)]
pub struct HeadersFrame<'a>
{
	///The stream ID this frame is on.
	pub strmid: StreamIdNonZero,
	///The optional priority data for the stream, if any.
	pub priority: Option<Priority>,
	///Header block fragment (always the first one).
	pub fragment: Cow<'a, [u8]>,
	///True means the stream ends here, false means there may be more data.
	pub eos: bool,
	///True means this is the only fragment in header block, false means there are more fragments.
	pub eoh: bool,
	///Amount of padding inserted.
	pub padding: PaddingAmount,
}

impl<'a> HeadersFrame<'a>
{
	fn parse(strmid: StreamId, d: &'a [u8], flags: u8) -> Result<HeadersFrame<'a>, FrameParseErrorKind>
	{
		use self::FrameParseErrorKind::*;
		//Unpad if there is padding.
		let (d, padding) = if flags & FLAG_PAD != 0 {
			let p = *d.get(0).unwrap_or(&0);
			let d = unpad(d).set_err(TruncatedPadding)?;
			(d, PaddingAmount(p as u16 + 1))
		} else {
			(d, PaddingAmount(0))
		};
		//Priority.
		let (priority, d) = if flags & FLAG_PRIORITY != 0 {
			let mut d = Source::new(d);
			let priority = priority(&mut d).set_err(TruncatedPriority)?;
			(Some(priority), d.read_remaining())
		} else {
			(None, d)
		};
		fail_if!(d.len() == 0, EmptyHeaderFragment);
		Ok(HeadersFrame {
			strmid: strmid.into_nonzero().ok_or(IllegalFrameStream)?,
			priority: priority,
			fragment: Cow::Borrowed(d),
			eos: flags & FLAG_EOS != 0,
			eoh: flags & FLAG_EOH != 0,
			padding: padding,
		})
	}
	fn _serialize(&self, mut target: impl Sink) -> Result<usize, ()>
	{
		fail_if!(self.fragment.len() == 0, ());
		let mut flags = 0;
		if self.eos { flags |= FLAG_EOS; }
		if self.eoh { flags |= FLAG_EOH; }
		if self.padding.present() { flags |= FLAG_PAD; }
		if self.priority.is_some() { flags |= FLAG_PRIORITY; }
		write_frame(&mut target, FTYPE_HEADERS, flags, self.strmid.into_stream_id(), |target|{
			self.padding.write_amount(target)?;
			if let Some(priority) = self.priority { priority.serialize(target)?; }
			target.write_slice(self.fragment.deref())?;
			self.padding.write_padding(target)
		})
	}
}

///HTTP/2 PRIORITY frame.
#[derive(Copy,Clone,PartialEq,Eq,Debug)]
pub struct PriorityFrame
{
	///The stream ID this frame is on.
	pub strmid: StreamIdNonZero,
	///The new priority data for this frame.
	pub priority: Priority
}

impl PriorityFrame
{
	fn parse(strmid: StreamId, d: &[u8]) -> Result<PriorityFrame, FrameParseErrorKind>
	{
		use self::FrameParseErrorKind::*;
		let mut d = Source::new(d);
		let priority = priority(&mut d).set_err(IllegalFrameSize)?;
		d.expect_end().set_err(IllegalFrameSize)?;
		Ok(PriorityFrame{
			strmid: strmid.into_nonzero().ok_or(IllegalFrameStream)?,
			priority: priority,
		})
	}
	fn _serialize(&self, mut target: impl Sink) -> Result<usize, ()>
	{
		write_frame(&mut target, FTYPE_PRIORITY, 0, self.strmid.into_stream_id(), |target|{
			self.priority.serialize(target)
		})
	}
}

///HTTP/2 RST_STREAM frame.
#[derive(Copy,Clone,PartialEq,Eq,Debug)]
pub struct RstStreamFrame
{
	///The stream ID this frame is on.
	pub strmid: StreamIdNonZero,
	///The HTTP/2 reason error code for abort.
	pub error: u32,
}

impl RstStreamFrame
{
	fn parse(strmid: StreamId, d: &[u8]) -> Result<RstStreamFrame, FrameParseErrorKind>
	{
		use self::FrameParseErrorKind::*;
		let mut d = Source::new(d);
		let code = d.u32().set_err(IllegalFrameSize)?;
		d.expect_end().set_err(IllegalFrameSize)?;
		Ok(RstStreamFrame{
			strmid: strmid.into_nonzero().ok_or(IllegalFrameStream)?,
			error: code,
		})
	}
	fn _serialize(&self, mut target: impl Sink) -> Result<usize, ()>
	{
		write_frame(&mut target, FTYPE_RST_STREAM, 0, self.strmid.into_stream_id(), |target|{
			target.write_u32(self.error)
		})
	}
}

///HTTP/2 SETTINGS frame.
#[derive(Clone,PartialEq,Eq,Debug)]
pub struct SettingsFrame<'a>
{
	///The settings payload. Must be multiple of 6 in length.
	pub data: Cow<'a, [u8]>
}

impl<'a> SettingsFrame<'a>
{
	fn parse(d: &'a [u8]) -> Result<SettingsFrame<'a>, FrameParseErrorKind>
	{
		use self::FrameParseErrorKind::*;
		fail_if!(d.len() % 6 != 0, IllegalFrameSize);
		Ok(SettingsFrame{
			data: Cow::Borrowed(d),
		})
	}
	fn _serialize(&self, mut target: impl Sink) -> Result<usize, ()>
	{
		fail_if!(self.data.len() % 6 != 0, ());
		write_frame(&mut target, FTYPE_SETTINGS, 0, StreamId::zero(), |target|{
			target.write_slice(self.data.deref())
		})
	}
}

///HTTP/2 PUSH_PROMISE frame.
#[derive(Clone,PartialEq,Eq,Debug)]
pub struct PushPromiseFrame<'a>
{
	///The stream ID this frame is on.
	pub strmid: StreamIdNonZero,
	///The ID of the promised stream.
	pub promised_id: StreamIdNonZero,
	///Header block fragment (always the first one).
	pub fragment: Cow<'a, [u8]>,
	///True means this is the only fragment in header block, false means there are more fragments.
	pub eoh: bool,
	///Amount of padding inserted.
	pub padding: PaddingAmount,
}

impl<'a> PushPromiseFrame<'a>
{
	fn parse(strmid: StreamId, d: &'a [u8], flags: u8) -> Result<PushPromiseFrame<'a>, FrameParseErrorKind>
	{
		use self::FrameParseErrorKind::*;
		//Unpad if there is padding.
		let (d, padding) = if flags & FLAG_PAD != 0 {
			let p = *d.get(0).unwrap_or(&0);
			let d = unpad(d).set_err(TruncatedPadding)?;
			(d, PaddingAmount(p as u16 + 1))
		} else {
			(d, PaddingAmount(0))
		};
		let mut d = Source::new(d);
		let promised_id = d.u32().set_err(IllegalFrameSize)?;
		let d = d.read_remaining();
		fail_if!(d.len() == 0, EmptyHeaderFragment);
		Ok(PushPromiseFrame {
			strmid: strmid.into_nonzero().ok_or(IllegalFrameStream)?,
			promised_id: StreamIdNonZero::new(promised_id).ok_or(IllegalPromisedStream(promised_id))?,
			fragment: Cow::Borrowed(d),
			eoh: flags & FLAG_EOH != 0,
			padding: padding,
		})
	}
	fn _serialize(&self, mut target: impl Sink) -> Result<usize, ()>
	{
		fail_if!(self.fragment.len() == 0, ());
		let mut flags = 0;
		if self.eoh { flags |= FLAG_EOH; }
		if self.padding.present() { flags |= FLAG_PAD; }
		write_frame(&mut target, FTYPE_PUSH_PROMISE, flags, self.strmid.into_stream_id(), |target|{
			self.padding.write_amount(target)?;
			self.promised_id.write(target)?;
			target.write_slice(self.fragment.deref())?;
			self.padding.write_padding(target)
		})
	}
}

///HTTP/2 PING frame.
#[derive(Copy,Clone,PartialEq,Eq,Debug)]
pub struct PingFrame
{
	///The ping code.
	pub code: u64,
	///True if this is ping acknowledgement, false if this is actual ping.
	pub ack: bool,
}

impl PingFrame
{
	fn parse(d: &[u8], flags: u8) -> Result<PingFrame, FrameParseErrorKind>
	{
		use self::FrameParseErrorKind::*;
		let mut d = Source::new(d);
		let code = d.u64().set_err(IllegalFrameSize)?;
		d.expect_end().set_err(IllegalFrameSize)?;
		Ok(PingFrame{
			code: code,
			ack: flags & FLAG_ACK != 0
		})
	}
	fn _serialize(&self, mut target: impl Sink) -> Result<usize, ()>
	{
		write_frame(&mut target, FTYPE_PING, if self.ack { FLAG_ACK } else { 0 }, StreamId::zero(), |target|{
			target.write_u64(self.code)
		})
	}
}

///HTTP/2 GOAWAY frame.
#[derive(Clone,PartialEq,Eq,Debug)]
pub struct GoawayFrame<'a>
{
	///The last processed stream id.
	pub last_stream_id: StreamId,
	///The reason code for terminating the connection.
	pub error: u32,
	///Additional debugging information.
	pub debug: Cow<'a, [u8]>,
}

impl<'a> GoawayFrame<'a>
{
	fn parse(d: &'a [u8]) -> Result<GoawayFrame<'a>, FrameParseErrorKind>
	{
		use self::FrameParseErrorKind::*;
		let mut d = Source::new(d);
		let lastid = d.u32().set_err(IllegalFrameSize)?;
		let code = d.u32().set_err(IllegalFrameSize)?;
		let debug = d.read_remaining();
		Ok(GoawayFrame {
			last_stream_id: StreamId::new(lastid).ok_or(IllegalLastStreamId(lastid))?,
			error: code,
			debug: Cow::Borrowed(debug),
		})
	}
	fn _serialize(&self, mut target: impl Sink) -> Result<usize, ()>
	{
		write_frame(&mut target, FTYPE_GOAWAY, 0, StreamId::zero(), |target|{
			self.last_stream_id.write(target)?;
			target.write_u32(self.error)?;
			target.write_slice(self.debug.deref())
		})
	}
}

///HTTP/2 WINDOW_UPDATE frame.
#[derive(Copy,Clone,PartialEq,Eq,Debug)]
pub struct WindowUpdateFrame
{
	///The stream ID this frame is on. May be zero to mean the entiere connection.
	pub strmid: StreamId,
	///The window size increment, always positive.
	pub increment: WindowIncrement,
}

impl WindowUpdateFrame
{
	fn parse(strmid: StreamId, d: &[u8]) -> Result<WindowUpdateFrame, FrameParseErrorKind>
	{
		use self::FrameParseErrorKind::*;
		let mut d = Source::new(d);
		let increment = d.u32().set_err(IllegalFrameSize)?;
		d.expect_end().set_err(IllegalFrameSize)?;
		Ok(WindowUpdateFrame{
			strmid: strmid,
			increment: WindowIncrement::new(increment).ok_or(IllegalWindowIncrement(increment as i32))?,
		})
	}
	fn _serialize(&self, mut target: impl Sink) -> Result<usize, ()>
	{
		write_frame(&mut target, FTYPE_WINDOW_UPDATE, 0, self.strmid, |target|{
			target.write_u32(self.increment.amount() as u32)
		})
	}
}

///HTTP/2 CONTINUATION frame.
#[derive(Clone,PartialEq,Eq,Debug)]
pub struct ContinuationFrame<'a>
{
	///The stream ID this frame is on.
	pub strmid: StreamIdNonZero,
	///The header fragment (always second or later).
	pub fragment: Cow<'a, [u8]>,
	///True means this is the last fragment in header block, false means there are more fragments.
	pub eoh: bool,
}

impl<'a> ContinuationFrame<'a>
{
	fn parse(strmid: StreamId, d: &'a [u8], flags: u8) -> Result<ContinuationFrame<'a>, FrameParseErrorKind>
	{
		use self::FrameParseErrorKind::*;
		fail_if!(d.len() == 0, EmptyHeaderFragment);
		Ok(ContinuationFrame {
			strmid: strmid.into_nonzero().ok_or(IllegalFrameStream)?,
			fragment: Cow::Borrowed(d),
			eoh: flags & FLAG_EOH != 0,
		})
	}
	fn _serialize(&self, mut target: impl Sink) -> Result<usize, ()>
	{
		fail_if!(self.fragment.len() == 0, ());
		let mut flags = 0;
		if self.eoh { flags |= FLAG_EOH; }
		write_frame(&mut target, FTYPE_CONTINUATION, flags, self.strmid.into_stream_id(), |target|{
			target.write_slice(self.fragment.deref())
		})
	}
}

///HTTP/2 stream state.
#[derive(Clone,PartialEq,Eq,Debug)]
pub struct StreamState
{
	frame_seqno: u64,
	expect_continue: StreamId,	//0 means not expecting continuation.
}

impl StreamState
{
	///Create a new stream state.
	pub fn new() -> StreamState
	{
		StreamState {
			frame_seqno: 0,
			expect_continue: StreamId(0),
		}
	}
}

struct WriteWrapper<'b,S:Sink+'b>(&'b mut S);

impl<'b,S:Sink+'b> FmtWrite for WriteWrapper<'b,S>
{
	fn write_str(&mut self, s: &str) -> Result<(), FmtError>
	{
		self.0.write_slice(s.as_bytes()).set_err(FmtError)
	}
}

///HTTP/2 frame.
#[derive(Clone,PartialEq,Eq,Debug)]
pub enum Frame<'a>
{
	///DATA frame.
	Data(DataFrame<'a>),
	///HEADERS frame.
	Headers(HeadersFrame<'a>),
	///PRIORITY frame.
	Priority(PriorityFrame),
	///RST_STREAM frame.
	RstStream(RstStreamFrame),
	///SETTINGS frame (non-ack).
	Settings(SettingsFrame<'a>),
	///SETTINGS frame (ack).
	SettingsAck,
	///PUSH_PROMISE frame.
	PushPromise(PushPromiseFrame<'a>),
	///PING frame.
	Ping(PingFrame),
	///GOAWAY frame.
	Goaway(GoawayFrame<'a>),
	///WINDOW_UPDATE frame.
	WindowUpdate(WindowUpdateFrame),
	///CONTINUATION frame.
	Continuation(ContinuationFrame<'a>),
	///Unknown frame type. In order, the parameters are.
	///
	/// * Stream id (possibly 0)
	/// * Frame type (always 10 or larger).
	/// * Flags
	/// * Payload.
	Unknown(StreamId, u8, u8, Cow<'a, [u8]>),
}

impl<'a> Frame<'a>
{
	#[cfg(test)]
	pub fn length(&self) -> usize { self._serialize(LengthCounter(0)).unwrap_or(0) }
	///Serialize this frame into buffer `target`.
	///
	///On success, returns `Ok(())`. If frame is illegal or there is insufficient space in buffer, returns
	///`Err(())`.
	pub fn serialize(&self, target: &mut [u8]) -> Result<usize, ()> { self._serialize(SliceSink::new(target)) }
	fn _serialize(&self, target: impl Sink) -> Result<usize, ()>
	{
		match self {
			&Frame::Data(ref x) => x._serialize(target),
			&Frame::Headers(ref x) => x._serialize(target),
			&Frame::Priority(ref x) => x._serialize(target),
			&Frame::RstStream(ref x) => x._serialize(target),
			&Frame::Settings(ref x) => x._serialize(target),
			&Frame::SettingsAck => Self::_serialize_settings_ack(target),
			&Frame::PushPromise(ref x) => x._serialize(target),
			&Frame::Ping(ref x) => x._serialize(target),
			&Frame::Goaway(ref x) => x._serialize(target),
			&Frame::WindowUpdate(ref x) => x._serialize(target),
			&Frame::Continuation(ref x) => x._serialize(target),
			&Frame::Unknown(_, 0..=9, _, _) => Err(()),
			&Frame::Unknown(s, t, f, ref x) =>  Self::_serialize_unknown(target, s, t, f, x.deref()),
		}
	}
	fn _serialize_settings_ack(mut target: impl Sink) -> Result<usize, ()>
	{
		target.write_u24(0)?;
		target.write_u8(FTYPE_SETTINGS)?;
		target.write_u8(FLAG_ACK)?;
		target.write_u32(0)?;
		Ok(target.written())
	}
	fn _serialize_unknown(mut target: impl Sink, s: StreamId, t: u8, f: u8, x: &[u8]) -> Result<usize, ()>
	{
		target.write_u24(x.len() as u32)?;
		target.write_u8(t)?;
		target.write_u8(f)?;
		s.write(&mut target)?;
		target.write_slice(x.deref())?;
		Ok(target.written())
	}
	///Parse a HTTP/2 frame. The frame is on stream `strmid`, has type `ftype`, flags `flags` and payload `d`.
	///
	///`expect_continue` is a state flag that shoupld be initially set to `None` and `first_frame` is another
	///state flag that should initially be set to `true`.
	///
	///On success, returns `Ok(frame)`, where `frame` the parsed frame. Otherwise returns `Err(err)`, where
	///`err` is the error. All errors are fatal to the connection.
	pub fn parse(strmid: u32, d: &'a [u8], ftype: u8, flags: u8, state: &mut StreamState) ->
		Result<Frame<'a>, FrameParseError>
	{
		let strmid = StreamId::new(strmid).
			ok_or(FrameParseError(_FrameParseError::IllegalStreamId(state.frame_seqno, strmid)))?;
		let r = Self::_parse(strmid, d, ftype, flags, state).
			map_err(|e|FrameParseError(_FrameParseError::Other(state.frame_seqno, d.len() as u32, ftype,
			flags, strmid, e)))?;
		state.frame_seqno = state.frame_seqno.wrapping_add(1);
		//Skip 0 as it is problematic if frame numbers wrap around.
		if state.frame_seqno == 0 { state.frame_seqno = 1; }
		Ok(r)
	}
	fn _parse(strmid: StreamId, d: &'a [u8], ftype: u8, flags: u8, state: &mut StreamState) ->
		Result<Frame<'a>, FrameParseErrorKind>
	{
		use self::FrameParseErrorKind::*;
		//The first frame MUST be settings.
		if state.frame_seqno == 0 {
			if ftype == FTYPE_SETTINGS && flags & FLAG_ACK == 0 {
				fail_if!(!strmid.is_zero(), IllegalFrameStream);
				return Ok(Frame::Settings(SettingsFrame::parse(d)?));
			} else {
				fail!(ExpectedSettings);
			}
		}
		//CONTINUATION is special.
		if !state.expect_continue.is_zero() {
			if ftype == FTYPE_CONTINUATION && strmid == state.expect_continue {
				let r = ContinuationFrame::parse(strmid, d, flags)?;
				//If EOH, clear expect_continue, as next will not be continuation.
				if r.eoh { state.expect_continue = StreamId::zero(); }
				return Ok(Frame::Continuation(r));
			} else {
				fail!(ExpectedContinuation(state.expect_continue));
			}
		}
		let r = match ftype {
			FTYPE_DATA => Frame::Data(DataFrame::parse(strmid, d, flags)?),
			FTYPE_HEADERS => {
				let r = HeadersFrame::parse(strmid, d, flags)?;
				//If !EOH, set expect_continue, as next will be continuation.
				if !r.eoh { state.expect_continue = r.strmid.into_stream_id(); }
				Frame::Headers(r)
			},
			FTYPE_PRIORITY => Frame::Priority(PriorityFrame::parse(strmid, d)?),
			FTYPE_RST_STREAM => Frame::RstStream(RstStreamFrame::parse(strmid, d)?),
			FTYPE_SETTINGS if flags & FLAG_ACK != 0 => {
				fail_if!(!strmid.is_zero(), IllegalFrameStream);
				fail_if!(d.len() != 0, IllegalFrameSize);
				Frame::SettingsAck
			},
			FTYPE_SETTINGS => {
				fail_if!(!strmid.is_zero(), IllegalFrameStream);
				Frame::Settings(SettingsFrame::parse(d)?)
			},
			FTYPE_PUSH_PROMISE => {
				let r = PushPromiseFrame::parse(strmid, d, flags)?;
				//If !EOH, set expect_continue, as next will be continuation.
				if r.eoh { state.expect_continue = r.strmid.into_stream_id(); }
				Frame::PushPromise(r)
			},
			FTYPE_PING => {
				fail_if!(!strmid.is_zero(), IllegalFrameStream);
				Frame::Ping(PingFrame::parse(d, flags)?)
			},
			FTYPE_GOAWAY => {
				fail_if!(!strmid.is_zero(), IllegalFrameStream);
				Frame::Goaway(GoawayFrame::parse(d)?)
			},
			FTYPE_WINDOW_UPDATE => Frame::WindowUpdate(WindowUpdateFrame::parse(strmid, d)?),
			FTYPE_CONTINUATION => {
				//This is not legal anyway, because we handled expected continations above.
				fail_if!(!strmid.is_zero(), IllegalFrameStream);
				fail!(UnexpectedContinuation);
			},
			_ => Frame::Unknown(strmid, ftype, flags, Cow::Borrowed(d)),
		};
		Ok(r)
	}
}

/*******************************************************************************************************************/
#[cfg(test)]
fn read24be(x: &[u8], y: usize) -> u32
{
	let x = &x[y..y+3];
	x[0] as u32 * 65536 + x[1] as u32 * 256 + x[2] as u32
}

#[cfg(test)]
fn read32be(x: &[u8], y: usize) -> u32
{
	let mut t = [0;4];
	t.copy_from_slice(&x[y..y+4]);
	u32::from_be_bytes(t)
}

#[cfg(test)]
fn read64be(x: &[u8], y: usize) -> u64
{
	let mut t = [0;8];
	t.copy_from_slice(&x[y..y+8]);
	u64::from_be_bytes(t)
}

#[cfg(test)]
fn fails_parse(strmid: u32, ftype: u8, flags: u8, d: &[u8])
{
	let expect_continue = match ftype {
		9 => StreamId(strmid),
		_ => StreamId::zero(),
	};
	let mut s = StreamState{frame_seqno: 1, expect_continue: expect_continue};
	Frame::parse(strmid, d, ftype, flags, &mut s).unwrap_err();
}

#[cfg(test)]
fn serialize_rejected(f: Frame)
{
	use ::std::vec::Vec;
	assert_eq!(f.length(), 0);
	let mut e = Vec::new();
	e.resize(20000, 0);	//Should be enough.
	assert!(f.serialize(&mut e).is_err());
}

#[cfg(test)]
fn roundtrip_frame_ok(f: Frame)
{
	use ::std::vec::Vec;
	//Use frame number 1 because 0 is special.And allow CONTINUATION.
	let expect_continue = match &f {
		&Frame::Continuation(ref c) => c.strmid.into_stream_id(),
		_ => StreamId::zero(),
	};
	let mut s = StreamState{frame_seqno: 1, expect_continue: expect_continue};
	let mut e = Vec::new();
	let flen = f.length();
	e.resize(flen, 0);
	assert_eq!(f.serialize(&mut e).unwrap(), flen);
	assert_eq!(read24be(&e, 0), (flen-9) as u32);
	let ftype = e[3];
	let flags = e[4];
	let strmid = read32be(&e,5);
	let f2 = Frame::parse(strmid, &e[9..], ftype, flags, &mut s).unwrap();
	assert_eq!(f, f2);
}

#[cfg(test)]
fn random_data_frame1() -> DataFrame<'static>
{
	use ::btls_aux_random::secure_random;
	use ::std::borrow::ToOwned;
	let mut raw = [0;16];
	loop {
		secure_random(&mut raw);
		let strmid = read32be(&raw, 0);
		if strmid == 0 || strmid > 0x7FFFFFFF { continue; }
		return DataFrame{
			strmid: StreamIdNonZero(strmid),
			payload: Cow::Owned((&raw[6..]).to_owned()),
			eos: raw[4] & 1 != 0,
			padding: PaddingAmount(if raw[4] & 2 != 0 { raw[5] as u16 + 1 } else { 0 }),
		}
	}
}

#[cfg(test)]
fn random_data_frame2() -> DataFrame<'static>
{
	use ::btls_aux_random::secure_random;
	let mut raw = [0;6];
	loop {
		secure_random(&mut raw);
		let strmid = read32be(&raw, 0);
		if strmid == 0 || strmid > 0x7FFFFFFF { continue; }
		return DataFrame{
			strmid: StreamIdNonZero(strmid),
			payload: Cow::Borrowed(b""),
			eos: raw[4] & 1 != 0,
			padding: PaddingAmount(if raw[4] & 2 != 0 { raw[5] as u16 + 1 } else { 0 }),
		}
	}
}

#[cfg(test)]
fn random_headers_frame() -> HeadersFrame<'static>
{
	use ::btls_aux_random::secure_random;
	use ::std::borrow::ToOwned;
	let mut raw = [0;21];
	loop {
		secure_random(&mut raw);
		let strmid = read32be(&raw, 0);
		if strmid == 0 || strmid > 0x7FFFFFFF { continue; }
		let dstrmid = read32be(&raw, 6);
		if dstrmid > 0x7FFFFFFF { continue; }
		return HeadersFrame{
			strmid: StreamIdNonZero(strmid),
			fragment: Cow::Owned((&raw[11..]).to_owned()),
			priority: if raw[4] & 8 != 0 {
				Some(Priority{
					dependency: StreamId(dstrmid),
					exclusive: raw[4] & 16 != 0,
					weight: raw[10],
				})
			} else {
				None
			},
			eos: raw[4] & 1 != 0,
			eoh: raw[4] & 4 != 0,
			padding: PaddingAmount(if raw[4] & 2 != 0 { raw[5] as u16 + 1 } else { 0 }),
		}
	}
}

#[cfg(test)]
fn random_priority_frame() -> PriorityFrame
{
	use ::btls_aux_random::secure_random;
	let mut raw = [0;10];
	loop {
		secure_random(&mut raw);
		let strmid = read32be(&raw, 0);
		if strmid == 0 || strmid > 0x7FFFFFFF { continue; }
		let dstrmid = read32be(&raw, 5);
		if dstrmid > 0x7FFFFFFF { continue; }
		return PriorityFrame{
			strmid: StreamIdNonZero(strmid),
			priority: Priority{
				dependency: StreamId(dstrmid),
				exclusive: raw[4] & 1 != 0,
				weight: raw[9],
			}
		}
	}
}

#[cfg(test)]
fn random_rst_stream_frame() -> RstStreamFrame
{
	use ::btls_aux_random::secure_random;
	let mut raw = [0;8];
	loop {
		secure_random(&mut raw);
		let strmid = read32be(&raw, 0);
		if strmid == 0 || strmid > 0x7FFFFFFF { continue; }
		return RstStreamFrame{
			strmid: StreamIdNonZero(strmid),
			error: read32be(&raw, 4),
		}
	}
}

#[cfg(test)]
fn random_settings_frame() -> SettingsFrame<'static>
{
	use ::btls_aux_random::secure_random;
	use ::std::borrow::ToOwned;
	let mut raw = [0;18];
	secure_random(&mut raw);
	return SettingsFrame{
		data: Cow::Owned((&raw[..]).to_owned()),
	}
}

#[cfg(test)]
fn random_push_promise_frame() -> PushPromiseFrame<'static>
{
	use ::btls_aux_random::secure_random;
	use ::std::borrow::ToOwned;
	let mut raw = [0;20];
	loop {
		secure_random(&mut raw);
		let strmid = read32be(&raw, 0);
		if strmid == 0 || strmid > 0x7FFFFFFF { continue; }
		let dstrmid = read32be(&raw, 6);
		if dstrmid == 0 || dstrmid > 0x7FFFFFFF { continue; }
		return PushPromiseFrame{
			strmid: StreamIdNonZero(strmid),
			promised_id: StreamIdNonZero(dstrmid),
			fragment: Cow::Owned((&raw[10..]).to_owned()),
			eoh: raw[4] & 1 != 0,
			padding: PaddingAmount(if raw[4] & 2 != 0 { raw[5] as u16 + 1 } else { 0 }),
		}
	}
}

#[cfg(test)]
fn random_ping_frame() -> PingFrame
{
	use ::btls_aux_random::secure_random;
	let mut raw = [0;9];
	secure_random(&mut raw);
	return PingFrame{
		code: read64be(&raw, 0),
		ack: raw[8] & 1 != 0,
	}
}

#[cfg(test)]
fn random_goaway_frame1() -> GoawayFrame<'static>
{
	use ::btls_aux_random::secure_random;
	let mut raw = [0;8];
	loop {
		secure_random(&mut raw);
		let strmid = read32be(&raw, 0);
		if strmid > 0x7FFFFFFF { continue; }
		return GoawayFrame{
			last_stream_id: StreamId(strmid),
			error: read32be(&raw, 4),
			debug: Cow::Borrowed(b""),
		}
	}
}

#[cfg(test)]
fn random_goaway_frame2() -> GoawayFrame<'static>
{
	use ::btls_aux_random::secure_random;
	use ::std::borrow::ToOwned;
	let mut raw = [0;18];
	loop {
		secure_random(&mut raw);
		let strmid = read32be(&raw, 0);
		if strmid > 0x7FFFFFFF { continue; }
		return GoawayFrame{
			last_stream_id: StreamId(strmid),
			error: read32be(&raw, 4),
			debug: Cow::Owned((&raw[8..]).to_owned()),
		}
	}
}

#[cfg(test)]
fn random_window_update_frame() -> WindowUpdateFrame
{
	use ::btls_aux_random::secure_random;
	let mut raw = [0;8];
	loop {
		secure_random(&mut raw);
		let strmid = read32be(&raw, 0);
		if strmid > 0x7FFFFFFF { continue; }
		let increment = read32be(&raw, 0) as i32;
		if increment <= 0 { continue; }
		return WindowUpdateFrame{
			strmid: StreamId(strmid),
			increment: WindowIncrement(increment),
		}
	}
}

#[cfg(test)]
fn random_continuation_frame() -> ContinuationFrame<'static>
{
	use ::btls_aux_random::secure_random;
	use ::std::borrow::ToOwned;
	let mut raw = [0;15];
	loop {
		secure_random(&mut raw);
		let strmid = read32be(&raw, 0);
		if strmid == 0 || strmid > 0x7FFFFFFF { continue; }
		return ContinuationFrame{
			strmid: StreamIdNonZero(strmid),
			fragment: Cow::Owned((&raw[5..]).to_owned()),
			eoh: raw[4] & 1 != 0,
		}
	}
}

#[cfg(test)]
fn random_unknown_frame() -> Frame<'static>
{
	use ::btls_aux_random::secure_random;
	use ::std::borrow::ToOwned;
	let mut raw = [0;16];
	loop {
		secure_random(&mut raw);
		let strmid = read32be(&raw, 0);
		if strmid > 0x7FFFFFFF { continue; }
		let ftype = raw[4];
		if ftype < 10 { continue; }
		return Frame::Unknown(StreamId(strmid), ftype, raw[5], Cow::Owned((&raw[6..]).to_owned()));
	}
}

#[test]
fn test_random_frames()
{
	for _ in 0..1000 { roundtrip_frame_ok(Frame::Data(random_data_frame1())); }
	for _ in 0..1000 { roundtrip_frame_ok(Frame::Data(random_data_frame2())); }
	for _ in 0..1000 { roundtrip_frame_ok(Frame::Headers(random_headers_frame())); }
	for _ in 0..1000 { roundtrip_frame_ok(Frame::Priority(random_priority_frame())); }
	for _ in 0..1000 { roundtrip_frame_ok(Frame::RstStream(random_rst_stream_frame())); }
	for _ in 0..1000 { roundtrip_frame_ok(Frame::Settings(random_settings_frame())); }
	roundtrip_frame_ok(Frame::SettingsAck);	//Only one settings ACK.
	for _ in 0..1000 { roundtrip_frame_ok(Frame::PushPromise(random_push_promise_frame())); }
	for _ in 0..1000 { roundtrip_frame_ok(Frame::Ping(random_ping_frame())); }
	for _ in 0..1000 { roundtrip_frame_ok(Frame::Goaway(random_goaway_frame1())); }
	for _ in 0..1000 { roundtrip_frame_ok(Frame::Goaway(random_goaway_frame2())); }
	for _ in 0..1000 { roundtrip_frame_ok(Frame::WindowUpdate(random_window_update_frame())); }
	for _ in 0..1000 { roundtrip_frame_ok(Frame::Continuation(random_continuation_frame())); }
	for _ in 0..1000 { roundtrip_frame_ok(random_unknown_frame()); }
}

#[test]
fn reject_zerolength_fragments_serialize()
{
	serialize_rejected(Frame::Headers(HeadersFrame{
		strmid: StreamIdNonZero(1),
		fragment: Cow::Borrowed(b""),
		priority: None,
		eos: false,
		eoh: true,
		padding: PaddingAmount(0),
	}));
	serialize_rejected(Frame::PushPromise(PushPromiseFrame{
		strmid: StreamIdNonZero(1),
		promised_id: StreamIdNonZero(2),
		fragment: Cow::Borrowed(b""),
		eoh: true,
		padding: PaddingAmount(0),
	}));
	serialize_rejected(Frame::Continuation(ContinuationFrame{
		strmid: StreamIdNonZero(1),
		fragment: Cow::Borrowed(b""),
		eoh: true,
	}));
}


#[test]
fn fail_parse_data()
{
	//Zero stream.
	fails_parse(0, FTYPE_DATA, 0, b"foobar");
	//Stream out of range.
	fails_parse(0x80000000, FTYPE_DATA, 0, b"foobar");
}

#[test]
fn fail_parse_headers()
{
	//Zero stream.
	fails_parse(0, FTYPE_HEADERS, 0, b"foobar");
	//Stream out of range.
	fails_parse(0x80000000, FTYPE_HEADERS, 0, b"foobar");
	//Empty fragment.
	fails_parse(1, FTYPE_HEADERS, 0, b"");
}

#[test]
fn fail_parse_priority()
{
	//Zero stream.
	fails_parse(0, FTYPE_PRIORITY, 0, b"12345");
	//Stream out of range.
	fails_parse(0x80000000, FTYPE_PRIORITY, 0, b"12345");
	//Length out of range
	fails_parse(1, FTYPE_PRIORITY, 0, b"1234");
	fails_parse(1, FTYPE_PRIORITY, 0, b"123456");
}

#[test]
fn fail_parse_rst_stream()
{
	//Zero stream.
	fails_parse(0, FTYPE_RST_STREAM, 0, b"1234");
	//Stream out of range.
	fails_parse(0x80000000, FTYPE_RST_STREAM, 0, b"1234");
	//Length out of range
	fails_parse(1, FTYPE_RST_STREAM, 0, b"123");
	fails_parse(1, FTYPE_RST_STREAM, 0, b"12345");
}

#[test]
fn fail_parse_settings()
{
	//Bad stream.
	fails_parse(1, FTYPE_SETTINGS, 0, b"123456");
	//Length out of range
	fails_parse(0, FTYPE_RST_STREAM, 0, b"1235");
	fails_parse(0, FTYPE_RST_STREAM, 0, b"1234567");
}

#[test]
fn fail_parse_push_promise()
{
	//Zero stream.
	fails_parse(0, FTYPE_PUSH_PROMISE, 0, b"12345");
	//Stream out of range.
	fails_parse(0x80000000, FTYPE_PUSH_PROMISE, 0, b"12345");
	//Promised stream out of range
	fails_parse(1, FTYPE_PUSH_PROMISE, 0, b"\x00\x00\x00\x00X");
	fails_parse(1, FTYPE_PUSH_PROMISE, 0, b"\x80\x00\x00\x00X");
	//Empty fragment.
	fails_parse(1, FTYPE_PUSH_PROMISE, 0, b"1234");
}

#[test]
fn fail_parse_ping()
{
	//Bad stream.
	fails_parse(1, FTYPE_PING, 0, b"12345678");
	//Length out of range
	fails_parse(0, FTYPE_PING, 0, b"1234567");
	fails_parse(0, FTYPE_PING, 0, b"123456789");
}


#[test]
fn fail_parse_goaway()
{
	//Bad stream.
	fails_parse(1, FTYPE_GOAWAY, 0, b"12345678");
	//Last stream out of range
	fails_parse(0, FTYPE_GOAWAY, 0, b"\x80\x00\x00\x00XYZW");
}

#[test]
fn fail_parse_window_update()
{
	//Stream out of range.
	fails_parse(0x80000000, FTYPE_WINDOW_UPDATE, 0, b"1234");
	//Bad length.
	fails_parse(1, FTYPE_WINDOW_UPDATE, 0, b"123");
	fails_parse(1, FTYPE_WINDOW_UPDATE, 0, b"12345");
	//Bad increment.
	fails_parse(1, FTYPE_WINDOW_UPDATE, 0, b"\x00\x00\x00\x00");
	fails_parse(1, FTYPE_WINDOW_UPDATE, 0, b"\x80\x00\x00\x00");
	fails_parse(1, FTYPE_WINDOW_UPDATE, 0, b"\xff\xff\xff\xff");
}

#[test]
fn fail_parse_continuation()
{
	//Zero stream.
	fails_parse(0, FTYPE_CONTINUATION, 0, b"1234");
	//Stream out of range.
	fails_parse(0x80000000, FTYPE_CONTINUATION, 0, b"1234");
	//Empty fragment.
	fails_parse(1, FTYPE_CONTINUATION, 0, b"");
}
