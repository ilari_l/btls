use btls_aux_http::__encode_hpack_single;
use btls_aux_http::__encode_hpack_hard;
use btls_aux_http::__unpack_huffman_frag;
use btls_aux_serialization::Sink;
use btls_aux_serialization::SliceSink;

fn rdtsc() -> u64 { unsafe{std::arch::x86_64::_rdtsc()} }

fn main()
{
	const ROUNDS: usize = 100_000;
	let args: Vec<_> = std::env::args_os().skip(1).collect();
	let tstr: &std::ffi::OsStr = &args[0];
	let tstr: &[u8] = unsafe{std::mem::transmute(tstr)};
	let mut target = [0;32768];
	let mut cum_cycles = 0u64;
	let mut tlen = 0;
	for _ in 0..ROUNDS {
		let mut target = SliceSink::new(&mut target);
		let t = rdtsc();
		__encode_hpack_single(&mut target, tstr).expect("Encode hpack failed");
		cum_cycles += rdtsc() - t;
		tlen = target.written();
	}
	println!("Enc: Avg {avgc} cycles/round, {slen} -> {tlen} bytes",
		avgc=cum_cycles as f64 / ROUNDS as f64, slen=tstr.len());
	let mut target2 = SliceSink::new(&mut target);
	__encode_hpack_hard(&mut target2, tstr).expect("Encode hpack failed");
	let len = target2.written();
	let target2b = &target[..len];
	let mut cum_cycles = 0u64;
	let mut target3 = vec![0;tstr.len()];
	for _ in 0..ROUNDS {
		let mut state = 11;
		let t = rdtsc();
		let (inb, outb) = __unpack_huffman_frag(&mut state, target2b, &mut target3).
			expect("Decode hpack failed");
		cum_cycles += rdtsc() - t;
		assert!(state >= 11 && state <= 18);
		assert_eq!(inb, target2b.len());
		assert_eq!(outb, tstr.len());
		assert_eq!(&target3, tstr);
	}
	println!("Dec: Avg {avgc} cycles/round", avgc=cum_cycles as f64 / ROUNDS as f64);
}
