//!HTTP/1.x
#![allow(unsafe_code)]
#![forbid(unused_must_use)]
#![forbid(missing_docs)]
use crate::http::AuthorityKind;
use crate::http::ElementDispatch as ElementDispatchTrait;
use crate::http::EncodeHeaders;
use crate::http::equals_case_insensitive;
use crate::http::HeaderBlockState;
use crate::http::HeaderFault;
use crate::http::HeaderName;
use crate::http::HttpUtils;
use crate::http::is_tchar;
use crate::http::is_xdigit;
use crate::http::ReceiveBuffer;
use crate::http::Source;
use crate::http::StandardHttpHeader;
use crate::http::StreamAbort;
use crate::http::StreamState;
use crate::http::StreamType;
use crate::http::StreamTransition;
use crate::http::UnsafeHeaderName;
use crate::http::UnsafeHeaderValue;
use crate::http::UnsafeHeaderValueC;
use btls_aux_fail::f_break;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_memory::Attachment;
use btls_aux_memory::copy_if_fits;
use btls_aux_memory::split_attach_first;
use btls_aux_memory::strip_prefix;
use btls_aux_memory::trim_ascii;
use btls_aux_serialization::Sink as DSink;
#[cfg(test)] use btls_aux_serialization::SliceSink;
use core::cmp::min;
use core::fmt::Debug;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::mem::replace;
use core::mem::swap;
use core::str::from_utf8;
#[cfg(test)] use crate::http::partial_decode_authority;
#[cfg(test)] use crate::http::partial_decode_path;
#[cfg(test)] use crate::http::partial_decode_query;
#[cfg(test)] use crate::http::PartialDecodeError;
#[cfg(test)] use std::borrow::ToOwned;
#[cfg(test)] use std::vec::Vec;
#[cfg(test)] use std::string::String;
#[cfg(test)] use core::ops::Deref;

struct Http1LineFragment<'a>(&'a mut [u8]);

impl<'a> Http1LineFragment<'a>
{
	//Http1LineFragment does not contain control characters, so can use new_unsafe().
	fn as_raw(&self) -> &[u8] { self.0 }
	fn to_value(self) -> UnsafeHeaderValue<'a> { UnsafeHeaderValue::new_unsafe(self.0) }
	fn to_valuec(self) -> UnsafeHeaderValueC<'a> { UnsafeHeaderValueC::new_unsafe(self.0) }
	fn read(&self, idx: usize) -> Option<u8> { Some(*self.0.get(idx)?) }
	fn len(&self) -> usize { self.0.len() }
	fn equals(&self, s: &str) -> bool { self.0 == s.as_bytes() }
	fn starts_with(&self, s: &str) -> bool { self.0.starts_with(s.as_bytes()) }
	fn contains_colon_slash_slash(&self) -> bool { contains_colon_slash_slash(self.0) }
	fn split_proxyform(self) -> Option<(Http1LineFragment<'a>, Http1LineFragment<'a>,
		Option<Http1LineFragment<'a>>)>
	{
		//Find first ':' and split on it. Second part should start with //. Take part after this.
		let (x, y) = split_attach_first(self.0, b":", Attachment::Center).ok()?;
		let y = strip_prefix(y, b"//")?;
		//Find first '/' and split on it. The / if exists gets stuck on last part.
		let (y, z) = match split_attach_first(y, b"/", Attachment::Right) {
			Ok((y, z)) => (y, Some(Http1LineFragment(z))),
			Err(y) => (y, None)
		};
		//Splitting can not introduce new characters.
		Some((Http1LineFragment(x), Http1LineFragment(y), z))
	}
	fn split_header_value(self) -> Result<(Http1LineFragment<'a>, Http1LineFragment<'a>), &'a [u8]>
	{
		//Find first ':' and split on it. Trim whitespace on second part.
		let (x, y) = split_attach_first(self.0, b":", Attachment::Center).map_err(|x|&x[..])?;
		let y = trim_ascii(y);
		Ok((Http1LineFragment(x), Http1LineFragment(y)))
	}
	fn to_name(self) -> Result<UnsafeHeaderName<'a>, Error>
	{
		fail_if!(self.len() == 0, _Error::IllegalHeaderName(pfbf(&self)));
		fail_if!(!self.0.iter().cloned().all(is_tchar), _Error::IllegalHeaderName(pfbf(&self)));
		//Above checks guarantee the name is valid. But it needs lowercasing first, as it may have uppercase
		//letters.
		self.0.make_ascii_lowercase();
		Ok(UnsafeHeaderName::new_unsafe(self.0))
	}
}

const BUFFER_SIZE: usize = 16384;

fn total_line_length(l: &[u8], soffset: usize, state: u8) -> Result<usize, Result<u8, u8>>
{
	//If last one had CR at the end, start with that.
	let mut v = if state != 0 { 13u16 } else { 0u16 };
	let l = l.get(soffset..).ok_or(Ok(state))?;	//Ignore beginning.
	for (i, c) in l.iter().cloned().enumerate() {
		//Check character is legal. Characters 127 and <32 (except 9, 10 and 13) are illegal.
		v = (v << 8) | c as u16;
		match (v >> 8, c) {
			//CRLF sequence.
			(13, 10) => return i.checked_add(1).ok_or(Ok(255)),	//CRLF sequence.
			//13 is illegal unless followed by 10 (checked above).
			(13, _) => return Err(Err(13)),
			//0-8,11,12,14-31 and 127 are always illegal. 10 is illegal unless preceeded by 13, which
			//was checked above.
			(_,0..=8|10..=12|14..=31|127) => return Err(Err(c)),
			//Ok.
			(_, _) => ()
		};
		//CRLF sequence: Return i + 1 (for this byte). The starting offset is ignored.
		if v == 0x0d0a { return i.checked_add(1).ok_or(Ok(255)); }
	}
	//Return if last character was CR or something else.
	Err(if v & 0xFF == 13 { Ok(1) } else { Ok(0) })
}

macro_rules! split_off_data
{
	($a:ident $b:expr) => {{
		let tmp3 = $b;
		let mut tmp4 = &mut [][..];
		swap(&mut $a, &mut tmp4);
		fail_if!(tmp4.len() < tmp3 , INTERR_ITER_OOR);
		let (tmp1, tmp2) = tmp4.split_at_mut(tmp3);
		$a = tmp2;
		tmp1
	}}
}

macro_rules! append_to_buffer
{
	($a:ident, $selfx:expr) => {{
		let toolong = ||_Error::LineTooLong;
		let buffer = $selfx.buffer.get_mut($selfx.buffer_use..).ok_or_else(toolong)?;
		copy_if_fits(buffer, $a).ok_or_else(toolong)?;
		$selfx.buffer_use = $selfx.buffer_use.checked_add($a.len()).ok_or_else(toolong)?;
	}}
}


macro_rules! try_complete_line
{
	($a:ident $selfx:expr) => {{
		match total_line_length($a, 0, $selfx.buffer_state) {
			Ok(l) => {
				let tmp = split_off_data!($a l);
				append_to_buffer!(tmp, $selfx);
				$selfx.buffer_state = 0;
				true
			},
			Err(Err(b)) => fail!(_Error::IllegalByte(b)),	//Illegal byte
			Err(Ok(255)) => fail!(_Error::LineTooLong),	//Much too long.
			Err(Ok(x)) => {
				let alen = $a.len();
				let tmp = split_off_data!($a alen);
				append_to_buffer!(tmp, $selfx);
				$selfx.buffer_state = x;
				false
			}
		}
	}}
}

macro_rules! next_line_offset
{
	($a:expr,$off:expr) => {{
		use self::_Error::*;
		match total_line_length($a, $off, 0) {
			Ok(l) => Some($off.checked_add(l).ok_or(LineTooLong)?),
			Err(Err(b)) => fail!(_Error::IllegalByte(b)),	//Illegal byte
			Err(Ok(255)) => fail!(_Error::LineTooLong),	//Much too long.
			Err(Ok(_)) => None
		}
	}}
}


macro_rules! split_off_one_line
{
	($a:ident $selfx:expr) => {{
		use self::_Error::*;
		if $selfx.buffer_use > 0 {
			//If self.buffer_use > 0, we have to copy stuff from data buffer until buffer is completed
			//at CRLF and then return buffer. If buffer does not complete, return no data.
			if try_complete_line!($a $selfx) {
				let size = replace(&mut $selfx.buffer_use, 0).checked_sub(2).ok_or(IllegalLineEnd)?;
				Some(Http1LineFragment($selfx.buffer.get_mut(..size).ok_or(INTERR_ITER_OOR)?))
			} else {
				if $a.len() > 0 { Err(INTERR_INPUT_NOT_CONSUMED)?; }
				None
			}
		} else {
			//Otherwise if data contains CRLF, split the first line and return that. If there is no
			//CRLF, save it in buffer and return no data. Since buffer is empty, buffer state is always
			//0.
			match total_line_length($a, 0, $selfx.buffer_state) {
				Ok(copied) => {
					let tosave = split_off_data!($a copied);
					$selfx.buffer_state = 0;
					let size = tosave.len().checked_sub(2).ok_or(IllegalLineEnd)?;
					Some(Http1LineFragment(tosave.get_mut(..size).ok_or(IllegalLineEnd)?))
				},
				//Treat illegal characters 10 and 13 specially.
				Err(Err(10|13)) => fail!(IllegalLineEnd),
				Err(Err(b)) => fail!(IllegalByte(b)),
				Err(Ok(255)) => fail!(IllegalLineEnd),
				Err(Ok(x)) => {
					//All of $a got consumed by the operation.
					let alen = $a.len();
					let a = split_off_data!($a alen);
					$selfx.buffer_state = x;
					$selfx.buffer_use = a.len();
					copy_if_fits(&mut $selfx.buffer, a).ok_or(LineTooLong)?;
					if $a.len() > 0 { Err(INTERR_INPUT_NOT_CONSUMED)?; }
					None
				}
			}
		}
	}}
}

macro_rules! set_header_flags
{
	($line:ident $end_headers:ident $end_pass:ident) => {{
		if $line == b"" || $line == b"\r\n" {
			$end_headers = true;
		} else if $line.get(..5).map(|x|equals_case_insensitive(x, b"host:")).unwrap_or(false) {
			$end_pass = true;
		}
	}}
}

macro_rules! flush_headers
{
	($selfx:ident $buf:expr, $buflen:expr, $push:ident) => {{
		let mut offset = 0;
		while offset < $buflen {
			if let Some(offset2) = next_line_offset!($buf, offset) {
				let offset3 = offset2;
				let offset2 = offset2.checked_sub(2).ok_or(_Error::IllegalLineEnd)?;
				let line = Http1LineFragment($buf.get_mut(offset..offset2).
					ok_or(_Error::IllegalLineEnd)?);
				if line.len() == 0 { break; }
				let (k, v) = split_header!($selfx $push line);
				//This is only used with requests, so upgrade is always special.
				HttpUtils::header($push, &mut $selfx.hstate, &mut $selfx.sstate, k, v, false,
					$selfx.use_std);
				offset = offset3;
			} else {
				fail!(_Error::IllegalLineEnd);	//We already checked this.
			}
		}
	}}
}

fn contains_colon_slash_slash(s: &[u8]) -> bool
{
	let mut m = 0;
	for c in s.iter().cloned() {
		m = match (m, c) {
			(_, b':') => 1,
			(1, b'/') => 2,
			(2, b'/') => return true,
			(_, _) => 0,
		};
	}
	false
}

fn write_byte_if_inrange(x: &mut [u8], pos: &mut usize, val: u8)
{
	x.get_mut(*pos).map(|x|*x=val);
	*pos += 1;
}

fn hexchar(x: u8) -> u8 { x + if x > 9 { 87 } else { 48 } }

#[derive(Copy)]
struct PrintableInput([u8;256]);

fn pfb(x: &[u8]) -> PrintableInput { PrintableInput::from_bytes(x) }
//Just restriction on characters so can Immediately unwrap.
fn pfbf(x: &Http1LineFragment) -> PrintableInput { PrintableInput::from_bytes(x.0) }

impl PrintableInput
{
	fn from_bytes(x: &[u8]) -> PrintableInput
	{
		let mut written = 1;
		let mut p = PrintableInput([0;256]);
		for c in x.iter().cloned() {
			if c >= 32 && c <= 126 && c != b'<' && c != b'>' && c != b'%' {
				write_byte_if_inrange(&mut p.0, &mut written, c);
			} else {
				write_byte_if_inrange(&mut p.0, &mut written, b'%');
				write_byte_if_inrange(&mut p.0, &mut written, hexchar(c/16));
				write_byte_if_inrange(&mut p.0, &mut written, hexchar(c%16));
			}
		}
		if written > 256 {
			//Overwrite the end.
			written = 251;
			write_byte_if_inrange(&mut p.0, &mut written, b'<');
			for _ in 0..3 { write_byte_if_inrange(&mut p.0, &mut written, b'.'); }
			write_byte_if_inrange(&mut p.0, &mut written, b'>');
		}
		p.0[0] = (written - 1) as u8;
		p
	}
}

//Define Clone oneself, as this is too long.
impl Clone for PrintableInput { fn clone(&self) -> Self { *self }}

impl Display for PrintableInput
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		//This can never overflow, because self.0 is array of bytes, which are at most 255, and thus
		//the array access can be to at most 256, which is the array size.
		let data = &self.0[1..][..self.0[0] as usize];
		if let Ok(data) = from_utf8(data) { f.write_str(data) } else { f.write_str("<unprintable>") }
	}
}

impl Debug for PrintableInput
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> { Display::fmt(self, f) }
}

///Error from parse.
///
///All errors are fatal to the connection.
#[derive(Copy,Clone,Debug)]
pub struct Error(_Error);

#[derive(Copy,Clone,Debug)]
enum _Error
{
	//Fatal error parsing headers. Currently this is only given for Content-Length mismatches.
	FatalHeaderError(&'static str),
	//Fatal error parsing headers. Currently this is only given for Content-Length mismatches.
	FatalHeaderErrorF(HeaderFault),
	///Middlers are not supported in HTTP/1.x.
	Middlers,
	//No authority header present.
	NoAuthority,
	//Illegal byte in text parts. This is only given for bytes 0-8,10-31 and 127 (10 and 13 are possible if
	//misused).
	IllegalByte(u8),
	//The chunk end line was not empty.
	IllegalChunkEnd,
	//Can not parse chunk header.
	IllegalChunkHeader,
	//Header continuation (splitting one header onto multiple lines) used.
	IllegalContinuation,
	//Header with illegal name.
	IllegalHeaderName(PrintableInput),
	//Header line with no ':' in it.
	IllegalHeaderSyntax(PrintableInput),
	//Bad line ending.
	IllegalLineEnd,
	//Bad request line (not 3 space-separated components).
	IllegalRequestLine(PrintableInput),
	//Bad status line (not at least 3 space-separated components).
	IllegalStatusLine(PrintableInput),
	//NAK upgrade routine misued.
	IlleglUpgradeNak,
	//Inter-read buffer overrun.
	LineTooLong,
	//I/O error has happened previously.
	StreamError,
	//Unexpected end of stream.
	UnexpectedClose,
	//Request or response claims to be HTTP/0.9.
	UnsupportedHttp09,
	//Request or response has unsupported HTTP version.
	UnsupportedHttpVersion(PrintableInput),
	//Internal error (a bug).
	InternalError(&'static str),
}

impl Display for Error
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::_Error::*;
		match self.0 {
			FatalHeaderError(err) => write!(f, "Fatal header error: {err}"),
			FatalHeaderErrorF(ref err) => Display::fmt(err, f),
			NoAuthority => write!(f, "No virtual host specified"),
			Middlers => write!(f, "Middlers are not supported in HTTP/1.x"),
			IllegalByte(byte) => write!(f, "Illegal byte {byte}"),
			IllegalChunkEnd => write!(f, "Illegal chunk ending"),
			IllegalChunkHeader => write!(f, "Illegal chunk header"),
			IllegalContinuation => write!(f, "Illegal continuation"),
			IllegalHeaderName(name) => write!(f, "Illegal header name <{name}>"),
			IllegalHeaderSyntax(line) => write!(f, "Illegal header syntax <{line}>"),
			IllegalLineEnd => write!(f, "Illegal line ending"),
			IllegalRequestLine(line) => write!(f, "Illegal request line <{line}>"),
			IllegalStatusLine(line) => write!(f, "Illegal status line <{line}>"),
			IlleglUpgradeNak => write!(f, "Illegal upgrade NAK"),
			LineTooLong => write!(f, "Line too long"),
			StreamError => write!(f, "I/O error"),
			UnexpectedClose => write!(f, "Unexpected connection close"),
			UnsupportedHttp09 => write!(f, "HTTP/0.9 is not supported"),
			UnsupportedHttpVersion(version) => write!(f, "Unsupported HTTP version <{version}>"),
			InternalError(err) => write!(f, "Internal Error: {err}"),
		}
	}
}

impl From<&'static str> for Error
{
	fn from(e: &'static str) -> Error { Error(_Error::InternalError(e)) }
}

impl From<_Error> for Error
{
	fn from(e: _Error) -> Error { Error(e) }
}

impl Error
{
	fn into_stream_error(&self) -> StreamAbort
	{
		use self::_Error::*;
		match self.0 {
			//This actually does not matter because abort should close the stream first.
			FatalHeaderError(_) => StreamAbort::InternalError("FatalHeaderError"),
			FatalHeaderErrorF(_) => StreamAbort::InternalError("FatalHeaderErrorF"),
			NoAuthority => StreamAbort::ProtocolError("No virtual host specified"),
			Middlers => StreamAbort::ProtocolError("Middlers are not supported in HTTP/1.x"),
			IllegalByte(_) => StreamAbort::ProtocolError("Illegal byte"),
			IllegalChunkEnd => StreamAbort::ProtocolError("Illegal chunk ending"),
			IllegalChunkHeader => StreamAbort::ProtocolError("Illegal chunk header"),
			IllegalContinuation => StreamAbort::ProtocolError("Illegal continuation"),
			IllegalHeaderName(_) => StreamAbort::ProtocolError("Illegal header name"),
			IllegalHeaderSyntax(_) => StreamAbort::ProtocolError("Illegal header syntax"),
			IllegalLineEnd => StreamAbort::ProtocolError("Illegal line ending"),
			IllegalRequestLine(_) => StreamAbort::ProtocolError("Illegal request line"),
			IllegalStatusLine(_) => StreamAbort::ProtocolError("Illegal status line"),
			IlleglUpgradeNak => StreamAbort::ProtocolError("Illegal upgade NAK"),
			LineTooLong => StreamAbort::ProtocolError("Line too long"),
			StreamError => StreamAbort::ConnectionLost,
			UnexpectedClose => StreamAbort::ConnectionLost,
			UnsupportedHttp09 => StreamAbort::ProtocolError("HTTP/0.9 is not supported"),
			UnsupportedHttpVersion(_) => StreamAbort::ProtocolError("Unsupported HTTP version"),
			InternalError(x) => StreamAbort::InternalError(x),
		}
	}
}

static INTERR_ITER_OOR: &'static str = "Iterator out of range";
static INTERR_DLEFT_UF: &'static str = "Amount of data left underflow";
static INTERR_AURL_FAILED: &'static str = "Failed to split absolute URL";
static INTERR_INPUT_NOT_CONSUMED: &'static str = "Expected to consume all input, but did not";

///Element from HTTP/1.x parsing.
#[derive(Copy,Clone,Debug)]
#[non_exhaustive]
pub enum Element<'a>
{
	///Content-Length header was received.
	ContentLength(u64),
	///A block of data was received.
	Data(&'a [u8]),
	///Version was received.
	Version(&'a [u8]),
	///Status was received.
	Status(u16),
	///Reason was received.
	Reason(&'a [u8]),
	///Method was received.
	Method(&'a [u8]),
	///Scheme was received.
	Scheme(&'a [u8]),
	///Authority was received.
	Authority(&'a [u8]),
	///Raw authority was received.
	AuthorityRaw(&'a [u8]),
	///Path was received.
	Path(&'a [u8]),
	///Raw path was received.
	PathRaw(&'a [u8]),
	///Protocol was received.
	Protocol(&'a [u8]),
	///Query was received.
	Query(&'a [u8]),
	///An header was received.
	Header(&'a [u8], &'a [u8]),
	///A standard header was received.
	HeaderStd(StandardHttpHeader, &'a [u8]),
	///Designate header as connection header (use_std only).
	ConnectionHeader(&'a [u8]),
	///Designate standard header as connection header (use_std only).
	ConnectionHeaderStd(StandardHttpHeader),
	///End of interim headers was received, more headers next..
	EndOfHeadersInterim,
	///End of headers was received, body next.
	EndOfHeaders,
	///A trailer was received. Also signifies that the data has ended.
	Trailer(&'a [u8], &'a [u8]),
	///A standard trailer was received.
	TrailerStd(StandardHttpHeader, &'a [u8]),
	///Start of message, will be followed by headers.
	StartOfMessage,
	///End of message was received. May be sent already during headers.
	EndOfMessage,
	///Header fault was received. This request has failed. Next event is about another message.
	HeadersFaulty(HeaderFault),
	///Error abort. was received. The connection should be torn down, if it is not already.
	Abort(StreamAbort),
}


///Element sink.
pub trait ElementSink
{
	///Sink an element.
	fn sink<'a>(&mut self, elem: Element<'a>);
	///Is protocol supported?
	fn protocol_supported(&self, protocol: &[u8]) -> bool;
}

struct ElementDispatch<'a,S:ElementSink+'a>(&'a mut S);

impl<'a,S:ElementSink> ElementDispatchTrait for ElementDispatch<'a,S>
{
	fn abort(&mut self, cause: StreamAbort) { self.0.sink(Element::Abort(cause)); }
	fn authority(&mut self, x: &[u8]) { self.0.sink(Element::Authority(x)); }
	fn content_length(&mut self, x: u64) { self.0.sink(Element::ContentLength(x)); }
	fn path(&mut self, x: &[u8]) { self.0.sink(Element::Path(x)); }
	fn query(&mut self, x: &[u8]) { self.0.sink(Element::Query(x)); }
	fn scheme(&mut self, x: &[u8]) { self.0.sink(Element::Scheme(x)); }
	fn protocol(&mut self, x: &[u8]) { self.0.sink(Element::Protocol(x)); }
	fn method(&mut self, x: &[u8]) { self.0.sink(Element::Method(x)); }
	fn version(&mut self, x: &[u8]) { self.0.sink(Element::Version(x)); }
	fn reason(&mut self, x: &[u8]) { self.0.sink(Element::Reason(x)); }
	fn status(&mut self, x: u16) { self.0.sink(Element::Status(x)); }
	fn trailer(&mut self, n: HeaderName, v: &[u8])
	{
		let e = match n {
			HeaderName::Standard(n) => Element::TrailerStd(n, v),
			HeaderName::Custom(n) => Element::Trailer(n, v),
		};
		self.0.sink(e);
	}
	fn header(&mut self, n: HeaderName, v: &mut [u8])
	{
		let e = match n {
			//Connection is very special in use_std. Report it as sequence of header names, using
			//standard ones if possible.
			HeaderName::Standard(StandardHttpHeader::Connection) => {
				for part in v.split_mut(|&x|x==b',') {
					let part = trim_ascii(part);
					part.make_ascii_lowercase();
					if let Some(name) = StandardHttpHeader::from(part) {
						self.0.sink(Element::ConnectionHeaderStd(name));
					} else {
						self.0.sink(Element::ConnectionHeader(part));
					}
				}
				return;
			},
			HeaderName::Standard(n) => Element::HeaderStd(n, v),
			HeaderName::Custom(n) => Element::Header(n, v),
		};
		self.0.sink(e);
	}
	fn protocol_supported(&self, x: &[u8]) -> bool { self.0.protocol_supported(x) }
	fn data(&mut self, x: &[u8]) { self.0.sink(Element::Data(x)); }
	fn start_of_message(&mut self) { self.0.sink(Element::StartOfMessage); }
	fn end_of_message(&mut self) { self.0.sink(Element::EndOfMessage); }
	fn end_of_headers(&mut self) { self.0.sink(Element::EndOfHeaders); }
	fn end_of_headers_interim(&mut self) { self.0.sink(Element::EndOfHeadersInterim); }
	fn reap_stream(&mut self, _: u32) {}	//This never happens.
	fn path_raw(&mut self, x: &[u8]) { self.0.sink(Element::PathRaw(x)); }
	fn authority_raw(&mut self, x: &[u8]) { self.0.sink(Element::AuthorityRaw(x)); }
}

#[derive(Copy,Clone,Debug)]
enum State
{
	Idle(bool),		//Idle, between requests or responses. Flag is if there have been requests.
	HeaderLine,		//First line of headers.
	Headers,		//Processing header block.
	Headers2,		//Processing header block after host.
	Raw(bool),		//Raw copy as data. Flag is if any data has been received.
	BodyToClose,		//payload delimited by close.
	BodyLength(u64),	//Body with specified length left.
	Chunk(u64),		//Chunk with specified length left.
	ChunkEnd,		//CRLF after chunk.
	ChunkLine,		//Chunk size line.
	Trailers,		//Processing trailer block.
}

///HTTP/1.x receive stream.
pub struct Stream
{
	buffer: [u8;BUFFER_SIZE],
	buffer_use: usize,
	buffer_puse: usize,
	buffer_state: u8,
	state: State,
	processing_reply: bool,
	next_request_head: bool,
	next_reply: bool,
	hstate: HeaderBlockState,
	sstate: StreamState,
	dfltscheme: &'static [u8],
	error: bool,
	status101: bool,
	use_std: bool,
}

impl Clone for Stream
{
	fn clone(&self) -> Self
	{
		Stream {
			buffer: self.buffer,
			buffer_use: self.buffer_use,
			buffer_puse: self.buffer_puse,
			buffer_state: self.buffer_state,
			state: self.state,
			processing_reply: self.processing_reply,
			hstate: self.hstate,
			sstate: self.sstate,
			dfltscheme: self.dfltscheme,
			next_request_head: self.next_request_head,
			next_reply: self.next_reply,
			error: self.error,
			status101: self.status101,
			use_std: self.use_std,
		}
	}
}

fn errhsyntax(header: &[u8]) -> Error { Error(_Error::IllegalHeaderSyntax(pfb(header))) }

fn do_reqline_split<'a>(line: Http1LineFragment<'a>, limit3: bool) ->
	Result<(Http1LineFragment<'a>, Http1LineFragment<'a>, Http1LineFragment<'a>), &'a [u8]>
{
	let line = line.0;
	//There must be exactly two spaces.
	let (space1, space2) = {
		let mut space_indices = line.iter().cloned().enumerate().filter(|&(_,c)|c==32).map(|(i,_)|i);
		let space1 = match space_indices.next() { Some(i) => i, None => return Err(line) };
		let space2 = match space_indices.next() { Some(i) => i, None => return Err(line) };
		if space_indices.next().is_some() && limit3 { return Err(line); }
		(space1, space2)
	};
	//The indices are always in range.
	let (a, line) = line.split_at_mut(space1);
	let (_, line) = line.split_at_mut(1);
	let (b, line) = line.split_at_mut(space2-space1-1);
	let (_, c) = line.split_at_mut(1);
	//Splitting can never introduce characters, so safe to rewrap.
	let a = Http1LineFragment(a);
	let b = Http1LineFragment(b);
	let c = Http1LineFragment(c);
	Ok((a,b,c))
}

fn is_valid_http_version(version: &[u8]) -> bool
{
	if version.len() != 8 || !version.starts_with(b"HTTP/1.") { return false; }
	version[7].saturating_sub(48) <= 9
}

macro_rules! split_header
{
	($selfx:ident $push:ident $line:expr) => {{
		let f = $line.read(0).unwrap_or(0);
		fail_if!(f == 9 || f == 32, _Error::IllegalContinuation);
		match $line.split_header_value() {
			Ok((k, v)) => (k.to_name()?, v.to_value()),
			Err(e) => return Err(errhsyntax(e))
		}
	}}
}

enum Scheme<'a>
{
	Default(&'static [u8]),
	Explicit(UnsafeHeaderValue<'a>),
	None,
}

enum Path<'a>
{
	Explicit(UnsafeHeaderValue<'a>),
	Asterisk,
	Root,
	None,
}

///I/O or stream error.
#[derive(Debug)]
pub enum IoOrStreamError<E:Sized+Debug>
{
	///Stream error.
	Stream(Error),
	///I/O error.
	Io(E),
}

impl Stream
{
	///Create a new stream.
	pub fn new(dfltscheme: &'static [u8]) -> Stream
	{
		Stream {
			buffer: [0;BUFFER_SIZE],
			buffer_use: 0,
			buffer_puse: 0,
			buffer_state: 0,
			state: State::Idle(false),
			processing_reply: false,
			hstate: HeaderBlockState::new(),
			sstate: StreamState::new2(StreamType::Http1Request),
			next_request_head: false,
			next_reply: false,
			dfltscheme: dfltscheme,
			error: false,
			status101: false,
			use_std: false,
		}
	}
	///Set use-std flag.
	///
	///This flag causes all standard headers to be sent via HeaderStd/MiddlerStd/TrailerStd elements. It also
	///causes Connection header to be sent using ConnectionHeader/ConnectionHeaderStd elements.
	pub fn set_use_std_flag(&mut self)
	{
		self.use_std = true;
	}
	///Set ready for reply. This is only meaningful for client side.
	///
	///In ready for reply state, the next message will be read as HTTP response. It will be reply to HEAD
	///(always no body, even if normally indicated) if `head` is true, otherwise it will have body or not
	///as normal.
	///
	///This must be set in idle state.
	pub fn set_reply(&mut self, head: bool)
	{
		self.next_request_head = head;
		self.next_reply = true;
	}
	///Push data.
	pub fn push<S:Source, R:ElementSink>(&mut self, source: &mut S, push: &mut R) ->
		Result<bool, IoOrStreamError<<S as Source>::Error>>
	{
		//Do not proceed on I/O error.
		fail_if!(self.error, IoOrStreamError::Stream(Error(_Error::StreamError)));
		let mut d = ElementDispatch(push);
		let mut buf = S::Buffer::new();
		let r = match source.read(&mut buf) {
			Ok(true) => self._push(buf.borrow(), &mut d).map(|_|true),
			Ok(false) => self._push_end(&mut d).map(|_|false),
			Err(e) => {
				self.error = true;
				fail!(IoOrStreamError::Io(e));
			}
		};
		source.reclaim_buffer(buf);
		//Ignore return value, because we are already erroring out.
		if let Err(e) = r { HttpUtils::abort(&mut d, &mut self.sstate, e.into_stream_error()).ok(); }
		r.map_err(|e|IoOrStreamError::Stream(e))
	}
	///NAK upgrade. There is impiled end-of-message.
	pub fn nak_upgrade(&mut self) -> Result<(), Error>
	{
		if let State::Raw(false) = self.state {
			self.state = State::Idle(true);
		} else {
			fail!(_Error::IlleglUpgradeNak);
		}
		Ok(())
	}
	fn _push_end(&mut self, push: &mut impl ElementDispatchTrait) -> Result<(), Error>
	{
		use self::_Error::*;
		match self.state {
			State::Idle(_) => Ok(()),	//Just close the connection.
			//In raw and body to close states, emit end of message.
			State::Raw(_)|State::BodyToClose => Ok(HttpUtils::end_of_message(push, &mut self.sstate)),
			//Close is unexpected in the remaing state.
			_ => Err(Error(UnexpectedClose)),
		}
	}
	fn __end_headers_next_transition(&mut self, push: &mut impl ElementDispatchTrait) -> Result<State, Error>
	{
		use self::_Error::*;
		Ok(match HttpUtils::end_headers(push, &mut self.hstate, &mut self.sstate, self.use_std) {
			StreamTransition::Upgrade => State::Raw(false),
			StreamTransition::InterimComplete => State::HeaderLine,
			StreamTransition::NextMessage => State::Idle(true),
			StreamTransition::BodyToClose => State::BodyToClose,
			StreamTransition::AsChunked => State::ChunkLine,
			StreamTransition::FixedLength(l) => State::BodyLength(l),
			StreamTransition::HardError(e) => fail!(FatalHeaderError(e)),
			StreamTransition::HardErrorF(e) => fail!(FatalHeaderErrorF(e)),
			StreamTransition::HardErrorA => fail!(NoAuthority),
			StreamTransition::Middlers => fail!(Middlers),
		})
	}
	fn _push_headers<'a>(&mut self, mut data: &'a mut [u8], push: &mut impl ElementDispatchTrait) ->
		Result<&'a mut [u8], Error>
	{
		loop {
			let line = f_break!(split_off_one_line!(data self));
			//Empty line ends the header block.
			if line.len() == 0 {
				self.state = self.__end_headers_next_transition(push)?;
				break;
			}
			let (k, v) = split_header!(self push line);
			//This handles host magically. Handle upgrade specially if processing
			//request or reply with status 101.
			HttpUtils::header(push, &mut self.hstate, &mut self.sstate, k, v,
				self.processing_reply && !self.status101, self.use_std);
		}
		Ok(data)
	}
	//This is special: It can need to buffer multiple header lines in order to get the authority.
	fn _push_headers_special<'a>(&mut self, mut data: &'a mut [u8], push: &mut impl ElementDispatchTrait) ->
		Result<&'a mut [u8], Error>
	{
		use self::_Error::*;
		let cache_offset = if self.buffer_puse < self.buffer_use {
			//If the last buffered line is not complete, try to complete it.
			if try_complete_line!(data self) {
				Some(replace(&mut self.buffer_puse, self.buffer_use))
			} else {
				return Ok(data);	//Not complete.
			}
		} else {
			None			//No lines were completed.
		};
		//At this point, buffer is always entiere lines, and data always starts at line
		//boundary. However, cache_offset may point to part of buffer containing just
		//completed line. Scan cache_offset if any, followed by lines of data.
		let mut end_pass = false;
		let mut end_headers = false;
		if let Some(cache_offset) = cache_offset {
			let last = self.buffer_use.checked_sub(2).ok_or(IllegalLineEnd)?;
			let line = self.buffer.get(cache_offset..last).ok_or(INTERR_ITER_OOR)?;
			set_header_flags!(line end_headers end_pass);
		}
		//Scan the buffer passed for host header or end of headers.
		let mut loffset = 0;
		let mut loffset2 = 0;
		while !end_pass && !end_headers {
			if let Some(offset) = next_line_offset!(data, loffset) {
				let line = data.get(loffset..offset).ok_or(IllegalLineEnd)?;
				set_header_flags!(line end_headers end_pass);
				loffset2 = loffset;	//Loffset2 steps one behind.
				loffset = offset;
			} else {
				//Not found, copy the entiere data into temporary storage, so it will
				//be considered next round. buffer_puse is set to buffer_use+loffset,
				//as that is the last complete line.
				let buffer = self.buffer.get_mut(self.buffer_use..).ok_or(LineTooLong)?;
				copy_if_fits(buffer, data).ok_or(LineTooLong)?;
				self.buffer_puse = self.buffer_use.checked_add(loffset).ok_or(LineTooLong)?;
				self.buffer_use = self.buffer_use.checked_add(data.len()).ok_or(LineTooLong)?;
				//All the data has been spent.
				return Ok(&mut []);
			}
		}
		//The host should be flushed first. If any data lines have been read, it is
		//presumably last of those, otherwise it is the completed line if any.
		let mut flush_from_buffer = self.buffer_use;
		let mut flush_from_data = loffset;
		let line = if loffset2 < loffset {
			let offset = loffset.checked_sub(2).ok_or(IllegalLineEnd)?;
			//Do not submit the last line of data, in order to avoid submitting host twice.
			if loffset2 < offset { flush_from_data = loffset2; }
			data.get_mut(loffset2..offset).ok_or(IllegalLineEnd)?
		} else if let Some(cache_offset) = cache_offset {
			let last = self.buffer_use.checked_sub(2).ok_or(IllegalLineEnd)?;
			//Do not submit last line of buffer, in order to avoid submitting host twice. This code can
			//assume buffer since cache_offset only contains one line, as any further lines would still
			//be in data.
			if cache_offset < last { flush_from_buffer = cache_offset; }
			self.buffer.get_mut(cache_offset..last).ok_or(INTERR_ITER_OOR)?
		} else {
			//This should not happen!
			fail!(IllegalLineEnd);
		};
		if line.len() > 0 {
			let line = Http1LineFragment(line);
			let (k, v) = split_header!(self push line);
			//This handles host magically. And this is always request, so
			//treat upgrade specially.
			HttpUtils::header(push, &mut self.hstate, &mut self.sstate, k, v, false, self.use_std);
		}
		//Flush the buffered headers, and appropriate part of data.
		let sdata = split_off_data!(data loffset);
		flush_headers!(self &mut self.buffer, flush_from_buffer, push);
		flush_headers!(self sdata, flush_from_data, push);
		self.buffer_puse = 0;
		self.buffer_use = 0;
		//Next transition.
		self.state = if end_headers {
			self.__end_headers_next_transition(push)?
		} else {
			State::Headers2
		};
		Ok(data)
	}
	fn _push_request_line<'a>(&mut self, mut data: &'a mut [u8], push: &mut impl ElementDispatchTrait) ->
		Result<&'a mut [u8], Error>
	{
		use self::_Error::*;
		if let Some(line) = split_off_one_line!(data self) {
			//Reset header state.
			self.hstate = HeaderBlockState::new();
			//The request line supposedly has 3 components.
			let (method, url, version) = do_reqline_split(line, true).
				map_err(|line|IllegalRequestLine(pfb(line)))?;
			let is_options = method.as_raw() == b"OPTIONS";
			fail_if!(version.equals("HTTP/0.9 "), UnsupportedHttp09);
			fail_if!(!is_valid_http_version(version.as_raw()), UnsupportedHttpVersion(pfbf(&version)));
			HttpUtils::method(push, &mut self.hstate, &mut self.sstate, method.to_valuec());
			HttpUtils::version(push, &mut self.hstate, &mut self.sstate, version.to_valuec());
			let (scheme, authority, path) = if url.equals("*") {
				//Asterisk form.
				(Scheme::Default(self.dfltscheme), None, Path::Asterisk)
			} else if url.starts_with("/") {
				//Absolute form.
				(Scheme::Default(self.dfltscheme), None, Path::Explicit(url.to_value()))
			} else if url.contains_colon_slash_slash() {
				//Proxy form. Failure is internal error, as contains_colon_slash_slash is
				//supposed to guarantee the parse is possible.
				let (scheme, rawauth, path) = url.split_proxyform().
					ok_or(InternalError(INTERR_AURL_FAILED))?;
				//The default path is either Root or Asterisk depending on the request method!
				let dpath = if is_options { Path::Asterisk } else { Path::Root };
				let path = path.map(|y|Path::Explicit(y.to_value())).unwrap_or(dpath);
				(Scheme::Explicit(scheme.to_value()), Some(rawauth), path)
			} else {
				//authority form.
				(Scheme::None, Some(url), Path::None)
			};
			let has_auth = authority.is_some();
			if let Some(authority) = authority {
				//The header-line one is always special.
				HttpUtils::authority(push, &mut self.hstate, &mut self.sstate, authority.to_value(),
					AuthorityKind::Http1Absolute);
			}
			match scheme {
				Scheme::Explicit(x) =>
					HttpUtils::scheme(push, &mut self.hstate, &mut self.sstate, x),
				Scheme::Default(x) => HttpUtils::scheme_default(push, &mut self.hstate,
					&mut self.sstate, x),
				Scheme::None => (),
			};
			match path {
				Path::Explicit(x) => HttpUtils::path(push, &mut self.hstate, &mut self.sstate, x),
				Path::Asterisk  =>
					HttpUtils::path_asterisk(push, &mut self.hstate, &mut self.sstate),
				Path::Root => HttpUtils::path_root(push, &mut self.hstate, &mut self.sstate),
				Path::None => (),
			};
			//If we have authority, start outputting normally, otherwise wait for authority.
			self.state = if has_auth { State::Headers2 } else { State::Headers };
		}
		Ok(data)
	}
	fn _push_status_line<'a>(&mut self, mut data: &'a mut [u8], push: &mut impl ElementDispatchTrait) ->
		Result<&'a mut [u8], Error>
	{
		use self::_Error::*;
		if let Some(line) = split_off_one_line!(data self) {
			//Reset header state.
			self.hstate = HeaderBlockState::new();
			fail_if!(line.starts_with("HTTP/0.9 "), UnsupportedHttp09);
			//do_reqline_split is close enough for parsing status-line, since it ignres 3rd
			//and further spaces.
			let (version, status, reason) = do_reqline_split(line, false).
				map_err(|line|IllegalStatusLine(pfb(line)))?;
			//The version field must contain "HTTP/1.X", where X is digit.
			fail_if!(!is_valid_http_version(version.as_raw()), UnsupportedHttpVersion(pfbf(&version)));
			//Send all the pseudo-headers associated with status-line.
			self.status101 = status.equals("101");
			HttpUtils::version(push, &mut self.hstate, &mut self.sstate, version.to_valuec());
			HttpUtils::status(push, &mut self.hstate, &mut self.sstate, status.to_valuec());
			HttpUtils::reason(push, &mut self.hstate, &mut self.sstate, reason.to_valuec());
			//Start normal header processing.
			self.state = State::Headers2;
		}
		Ok(data)
	}
	fn _push_trailers<'a>(&mut self, mut data: &'a mut [u8], push: &mut impl ElementDispatchTrait) ->
		Result<&'a mut [u8], Error>
	{
		if let Some(line) = split_off_one_line!(data self) {
			//If line length is positive, this is a trailer line, otherwise it ends trailers.
			if line.len() > 0 {
				let (k, v) = split_header!(self push line);
				//In trailers, upgrade is never magical.
				HttpUtils::header(push, &mut self.hstate, &mut self.sstate, k, v, true, self.use_std);
			} else {
				//Back for next request/reply.
				HttpUtils::end_of_message(push, &mut self.sstate);
				self.state = State::Idle(true);
			}
		}
		Ok(data)
	}
	fn _push_chunk_header<'a>(&mut self, mut data: &'a mut [u8]) -> Result<&'a mut [u8], Error>
	{
		use self::_Error::*;
		if let Some(line) = split_off_one_line!(data self) {
			//If there is ;, truncate on that.
			let mut l = line.0.split(|x|*x==b';');
			//Check this consists of only hexadecimal digits.
			let line = l.next().ok_or(IllegalChunkHeader)?;
			fail_if!(!line.iter().cloned().all(is_xdigit) || line.len() == 0,
				IllegalChunkHeader);
			let left = from_utf8(line).ok().and_then(|line|u64::from_str_radix(line, 16).ok()).
				ok_or(IllegalChunkHeader)?;
			self.state = if left == 0 { State::Trailers } else { State::Chunk(left) };
		}
		Ok(data)
	}
	fn _push_chunk<'a>(&mut self, mut data: &'a mut [u8], mut left: u64,
		push: &mut impl ElementDispatchTrait) -> Result<&'a mut [u8], Error>
	{
		let slen = min(data.len() as u64, left) as usize;
		let source = split_off_data!(data slen);
		HttpUtils::data(push, &mut self.sstate, source);
		left = left.checked_sub(source.len() as u64).ok_or(INTERR_DLEFT_UF)?;
		self.state = if left == 0 { State::ChunkEnd } else { State::Chunk(left) };
		Ok(data)
	}
	fn _push_chunk_end<'a>(&mut self, mut data: &'a mut [u8]) -> Result<&'a mut [u8], Error>
	{
		use self::_Error::*;
		if let Some(line) = split_off_one_line!(data self) {
			fail_if!(line.len() != 0, IllegalChunkEnd);
			self.state = State::ChunkLine;
		}
		Ok(data)
	}
	fn _push_body_length<'a>(&mut self, mut data: &'a mut [u8], mut left: u64,
		push: &mut impl ElementDispatchTrait) -> Result<&'a mut [u8], Error>
	{
		let slen = min(data.len() as u64, left) as usize;
		let source = split_off_data!(data slen);
		HttpUtils::data(push, &mut self.sstate, source);
		left = left.checked_sub(source.len() as u64).ok_or(INTERR_DLEFT_UF)?;
		self.state = if left == 0 {
			//Immediately cycle to next request.
			HttpUtils::end_of_message(push, &mut self.sstate);
			State::Idle(true)
		} else {
			State::BodyLength(left)
		};
		Ok(data)
	}
	fn _push_idle<'a>(&mut self, mut data: &'a mut [u8], between_requests: bool,
		push: &mut impl ElementDispatchTrait) -> Result<&'a mut [u8], Error>
	{
		if between_requests {
			//Skip CR and LF.
			let mut amt = 0;
			while amt < data.len() {
				if data[amt] == 13 || data[amt] == 10 { amt += 1; } else { break; }
			}
			data = &mut data[amt..];
			if data.len() == 0 { return Ok(data); }		//No data.
		}
		//If we are client and next_request_head is set, there is no body.
		let reply = self.next_reply;
		let nobody = self.next_request_head && reply;
		self.processing_reply = reply;
		let smode = match (reply, nobody) {
			(false, _) => StreamType::Http1Request,
			(true, false) => StreamType::Http1Reply,
			(true, true) => StreamType::Http1Head,
		};
		self.sstate = StreamState::new2(smode);
		self.state = State::HeaderLine;
		//Start of message.
		push.start_of_message();
		Ok(data)
	}
	fn _push(&mut self, mut data: &mut [u8], push: &mut impl ElementDispatchTrait) -> Result<(), Error>
	{
		while data.len() > 0 { match self.state {
			//State Raw is used for raw data transport after upgrade. All data is just copied with no
			//further interpretation. There should be no data in text buffers.
			//State BodyToClose means that HTTP is transferring a body delimited by close. It will
			//only end at end of connection. There should be no data in text buffers.
			State::Raw(ref mut flag) => {
				*flag = true;	//Signal that there has been data.
				HttpUtils::data(push, &mut self.sstate, data);
				break;
			},
			State::BodyToClose => {
				HttpUtils::data(push, &mut self.sstate, data);
				break;
			},
			//State BodyLength is used for Content-Length delimited body transport. The argument is
			//number of bytes left until body ends. There should be no data in text buffers.
			State::BodyLength(left) => data = self._push_body_length(data, left, push)?,
			//State Chunk is used for T-E: chunked delimited body transport. The argument is
			//number of bytes left until chunk ends. There should be no data in text buffers.
			State::Chunk(left) => data = self._push_chunk(data, left, push)?,
			//State::Idle is used for idle connection between requests.
			State::Idle(between_requests) => data = self._push_idle(data, between_requests, push)?,
			//State Chunked is used for the empty line at end of chunk. The text buffer should be empty.
			State::ChunkEnd => data = self._push_chunk_end(data)?,
			//State Headers2 is the state for normal header lines after request-line/status-line.
			//However, it is not used if request did not contain authority until host header line
			//is received (however, if request-line did have authority, this is used Immediately after).
			//There are no pending lines in buffer.
			State::Headers2 => data = self._push_headers(data, push)?,
			//State ChunkLine is used for reading the chunk header line. The text buffer should be
			//empty.
			State::ChunkLine => data = self._push_chunk_header(data)?,
			//State Trailers is used for reading the trailers after end of chunked body.
			State::Trailers => data = self._push_trailers(data, push)?,
			//State HeaderLine is used for reading the status-line. The buffer should be empty.
			State::HeaderLine if self.processing_reply =>
				data = self._push_status_line(data, push)?,
			//State HeaderLine is used for reading the request-line. The buffer should be empty.
			State::HeaderLine => data = self._push_request_line(data, push)?,
			//State::Headers is used for reading request headers from request-line that did not contain
			//hostname until host header is received. It is not used for replies, if request-line
			//did contain authority, or if non-empty host has already been received. This is pretty
			//darn special: It can need to buffer multiple rows.
			State::Headers => data = self._push_headers_special(data, push)?,
		}}
		Ok(())
	}
}

fn is_control_or_space_or_colon(x: &u8) -> bool { *x <= 32 || *x == 58 || *x == 127 }
fn is_control_or_space(x: &u8) -> bool { *x <= 32 || *x == 127 }
fn is_control(x: &u8) -> bool { (*x < 32 && *x != 9) || *x == 127 }

///Encode HTTP/1 headers.
pub struct HeaderEncoder;

impl EncodeHeaders for HeaderEncoder
{
	//Encode request line.
	fn request_line<S:DSink>(output: &mut S, method: &[u8], _: &[u8], authority: &[u8], path: &[u8],
		query: &[u8]) -> Result<(), ()>
	{
		//Do basic legality check on method, authority, path and query. Especially that they can not cross
		//component borders.
		fail_if!(method.iter().any(is_control_or_space), ());
		fail_if!(authority.iter().any(is_control_or_space), ());
		fail_if!(path.iter().any(is_control_or_space), ());
		fail_if!(query.iter().any(is_control_or_space), ());

		output.write_slice(method)?;
		output.write_u8(b' ')?;
		output.write_slice(path)?;
		output.write_slice(query)?;
		output.write_slice(b" HTTP/1.1\r\nhost:")?;
		output.write_slice(authority)?;
		output.write_u16(0xd0a)?;
		Ok(())
	}
	///Encode status-line.
	fn status_line<S:DSink>(output: &mut S, status: u16, reason: &[u8]) -> Result<(), ()>
	{
		//Do basic legality check on status. Especially that they can not cross component borders.
		fail_if!(reason.iter().any(is_control), ());

		let status2 = [(48 + status / 100 % 10) as u8, (48 + status / 10 % 10) as u8,
			(48 + status % 10) as u8, 32];
		output.write_slice(b"HTTP/1.1 ")?;
		output.write_slice(&status2)?;
		output.write_slice(reason)?;
		output.write_u16(0xd0a)?;
		Ok(())
	}
	///Encode a header.
	fn header<S:DSink>(output: &mut S, name: &[u8], value: &[u8]) -> Result<(), ()>
	{
		//Do basic legality check on header. Especially that they can not cross component borders.
		fail_if!(name.iter().any(is_control_or_space_or_colon), ());
		fail_if!(value.iter().any(is_control), ());

		output.write_slice(name)?;
		output.write_u8(b':')?;
		output.write_slice(value)?;
		output.write_u16(0xd0a)?;
		Ok(())
	}
	///Encode end of block.
	fn end<S:DSink>(output: &mut S) -> Result<(), ()>
	{
		output.write_u16(0xd0a)
	}
	///Is H2?
	fn is_h2() -> bool { false }
}

/*******************************************************************************************************************/



#[cfg(test)]
fn test_pdecode<F>(x: &[u8], func: F, y: &[u8])
	where F: for <'a> Fn(&'a mut [u8]) -> Result<&'a mut [u8], PartialDecodeError>
{
	let mut x2: Vec<u8> = x.to_owned();
	assert_eq!(func(&mut x2).unwrap(), y);
}

#[cfg(test)]
fn test_pdecode_fail<F>(x: &[u8], func: F)
	where F: for <'a> Fn(&'a mut [u8]) -> Result<&'a mut [u8], PartialDecodeError>
{
	let mut x2: Vec<u8> = x.to_owned();
	assert!(func(&mut x2).is_err());
}

#[test]
fn test_partial_decode_path()
{
	test_pdecode(b"ABCDEFGHIJKLMNOPQRSTUVWXYZ", partial_decode_path, b"ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	test_pdecode(b"a%62c", partial_decode_path, b"abc");
	test_pdecode(b"a%25c", partial_decode_path, b"a%25c");
	test_pdecode(b"a%2Fc", partial_decode_path, b"a/c");
	test_pdecode(b"a/c", partial_decode_path, b"a/c");
	test_pdecode(b"ac%30", partial_decode_path, b"ac0");
	test_pdecode(b"%30", partial_decode_path, b"0");
	test_pdecode(b"%30%2F%3a%3F%40", partial_decode_path, b"0/:%3f@");
	test_pdecode_fail(b"[hello]", partial_decode_path);
	test_pdecode_fail(b"a%4gc", partial_decode_path);
	test_pdecode_fail(b"a%g5c", partial_decode_path);
	test_pdecode_fail(b"a%4", partial_decode_path);
	test_pdecode_fail(b"a%", partial_decode_path);
}

#[test]
fn test_partial_decode_query()
{
	test_pdecode(b"ABCDEFGHIJKLMNOPQRSTUVWXYZ", partial_decode_query, b"ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	test_pdecode(b"a%62c", partial_decode_query, b"abc");
	test_pdecode(b"a%42c", partial_decode_query, b"aBc");
	test_pdecode(b"a%25c", partial_decode_query, b"a%25c");
	test_pdecode(b"a%2Fc", partial_decode_query, b"a/c");
	test_pdecode(b"a/c", partial_decode_query, b"a/c");
	test_pdecode(b"ac%30", partial_decode_query, b"ac0");
	test_pdecode(b"%30", partial_decode_query, b"0");
	test_pdecode(b"%30%2F%3a%3F%40", partial_decode_query, b"0/:?@");
	test_pdecode_fail(b"[hello]", partial_decode_query);
	test_pdecode_fail(b"a%4gc", partial_decode_query);
	test_pdecode_fail(b"a%g5c", partial_decode_query);
	test_pdecode_fail(b"a%4", partial_decode_query);
	test_pdecode_fail(b"a%", partial_decode_query);
}

#[test]
fn test_partial_decode_authority()
{
	test_pdecode(b"ABCDEFGHIJKLMNOPQRSTUVWXYZ", partial_decode_authority, b"abcdefghijklmnopqrstuvwxyz");
	test_pdecode(b"a%62c", partial_decode_authority, b"abc");
	test_pdecode(b"a%42c", partial_decode_authority, b"abc");
	test_pdecode(b"a%25c", partial_decode_authority, b"a%25c");
	test_pdecode(b"a%2Fc", partial_decode_authority, b"a%2fc");
	test_pdecode(b"ac%30", partial_decode_authority, b"ac0");
	test_pdecode(b"%30", partial_decode_authority, b"0");
	test_pdecode(b"%5BHELLO%5D", partial_decode_authority, b"%5bhello%5d");
	test_pdecode(b"%5BHELLO%5D:", partial_decode_authority, b"%5bhello%5d:");
	test_pdecode(b"[hello]", partial_decode_authority, b"[hello]");
	test_pdecode(b"[hello]:", partial_decode_authority, b"[hello]:");
	test_pdecode(b"[%48ELLO]:", partial_decode_authority, b"[hello]:");
	test_pdecode(b"[HELLO]:", partial_decode_authority, b"[hello]:");
	test_pdecode(b"%3A:", partial_decode_authority, b"%3a:");
	test_pdecode(b"%3A:7", partial_decode_authority, b"%3a:7");
	test_pdecode(b"%30%2F%3a%3F%40", partial_decode_authority, b"0%2f%3a%3f%40");
	test_pdecode(b"foo%3abar:123", partial_decode_authority, b"foo%3abar:123");
	test_pdecode(b"[::1]:12345", partial_decode_authority, b"[::1]:12345");
	test_pdecode(b"[v7.%3a]:12345", partial_decode_authority, b"[v7.:]:12345");
	test_pdecode_fail(b"[hello", partial_decode_authority);
	test_pdecode_fail(b"hello]", partial_decode_authority);
	test_pdecode_fail(b"a/c", partial_decode_authority);
	test_pdecode_fail(b"a%4gc", partial_decode_authority);
	test_pdecode_fail(b"a%g5c", partial_decode_authority);
	test_pdecode_fail(b"a%4", partial_decode_authority);
	test_pdecode_fail(b"a%", partial_decode_authority);
}

#[test]
fn test_contains_colon_slash_slash()
{
	assert!(contains_colon_slash_slash(b"http://foo"));
	assert!(contains_colon_slash_slash(b"https://foo"));
	assert!(contains_colon_slash_slash(b"foo:/bar://zot"));
	assert!(!contains_colon_slash_slash(b"foo:/bar:/zot"));
	assert!(!contains_colon_slash_slash(b"foo:/x/zot"));
	assert!(!contains_colon_slash_slash(b"foo:x//zot"));
	assert!(contains_colon_slash_slash(b"foo:/x/zot://qux"));
	assert!(contains_colon_slash_slash(b"foo:/x/zot://"));
}


#[cfg(test)]
fn test_split(x: &[u8], ch: &[u8], y: &[u8], z: Option<&[u8]>)
{
	use core::ops::DerefMut;
	let mut x2: Vec<u8> = x.to_owned();
	let (y2, z2) = match split_attach_first(x2.deref_mut(), ch, Attachment::Right) {
		Ok((a,b)) => (a, Some(b)),
		Err(a) => (a, None)
	};
	assert_eq!(y2, y);
	assert_eq!(z2.map(|x|&x[..]), z);
}

#[test]
fn test_split_on_char()
{
	test_split(b"/bar", b"/", b"", Some(&b"/bar"[..]));
	test_split(b"foo", b"/", b"foo", None);
	test_split(b"foo/bar", b"/", b"foo", Some(&b"/bar"[..]));
	test_split(b"foo/", b"/", b"foo", Some(&b"/"[..]));
}

/*
#[cfg(test)]
fn test_mode_permuted<F>(server: bool, headers: &mut [(&[u8], &[u8])], mode: BodyMode, heapsize: usize, cb: &mut F)
	where F: FnMut(&mut HeaderAccumulator)
{
	if heapsize == 1 {
		let mut a = HeaderAccumulator::new(server);
		cb(&mut a);
		for &(k, v) in headers.iter() { a.set_header(k, v).unwrap(); }
		assert_eq!(a.bodymode().unwrap(), mode);
		return;
	}
	for i in 0..heapsize {
		test_mode_permuted(server, headers, mode, heapsize - 1, cb);
		let first = if heapsize % 2 == 1 { 0 } else { i };
		let last = headers.len() - 1;
		let x = headers[first];
		headers[first] = headers[last];
		headers[last] = x;
	}
}

#[cfg(test)]
fn test_mode<F>(server: bool, headers: &[(&[u8], &[u8])], mode: BodyMode, mut cb: F)
	where F: FnMut(&mut HeaderAccumulator)
{
	let mut headers2: Vec<(&[u8], &[u8])> = headers.to_owned();
	test_mode_permuted(server, &mut headers2, mode, headers.len(), &mut cb);
}

#[test]
fn test_modes()
{
	static HOST: (&'static [u8], &'static [u8]) = (b"host", b"foo");
	static CL: (&'static [u8], &'static [u8]) = (b"content-length", b"123459");
	static GZIP: (&'static [u8], &'static [u8]) = (b"transfer-encoding", b"gzip");
	static CHUNKED: (&'static [u8], &'static [u8]) = (b"transfer-encoding", b"gzip, \tchunked\t ");
	static CHUNKED2: (&'static [u8], &'static [u8]) = (b"transfer-encoding", b" \tchunked\t ");
	//Request default.
	test_mode(true, &[HOST], BodyMode::None, |_|());
	//Response default.
	test_mode(false, &[], BodyMode::ToClose, |_|());
	//Lengths.
	test_mode(true, &[HOST, CL], BodyMode::Fixed(123459), |_|());
	//Lengths + gzip
	test_mode(true, &[HOST, CL, GZIP], BodyMode::ToClose, |_|());
	test_mode(false, &[CL, GZIP], BodyMode::ToClose, |_|());
	//Lengths + chunked
	test_mode(true, &[HOST, CL, CHUNKED], BodyMode::Chunked, |_|());
	test_mode(true, &[HOST, CL, CHUNKED2], BodyMode::Chunked, |_|());
	test_mode(true, &[HOST, CHUNKED], BodyMode::Chunked, |_|());
	test_mode(true, &[HOST, CHUNKED2], BodyMode::Chunked, |_|());
	test_mode(false, &[CL, CHUNKED], BodyMode::Chunked, |_|());
	test_mode(false, &[CL, CHUNKED2], BodyMode::Chunked, |_|());
	test_mode(false, &[CHUNKED], BodyMode::Chunked, |_|());
	test_mode(false, &[CHUNKED2], BodyMode::Chunked, |_|());
	//Nobody
	test_mode(true, &[HOST, CHUNKED2], BodyMode::None, |x|x.set_force_no_body());
	test_mode(false, &[CHUNKED2], BodyMode::None, |x|x.set_force_no_body());
	//Upgrade.
	test_mode(true, &[HOST, CHUNKED2], BodyMode::Raw, |x|{x.set_interrim();x.set_upgrade();
		x.set_force_no_body()});
	test_mode(false, &[CHUNKED2], BodyMode::Raw, |x|{x.set_interrim();x.set_upgrade();x.set_force_no_body()});
	//Interim.
	test_mode(true, &[HOST, CHUNKED2], BodyMode::Interim, |x|{x.set_interrim();x.set_force_no_body()});
	test_mode(false, &[CHUNKED2], BodyMode::Interim, |x|{x.set_interrim();x.set_force_no_body()});
}

#[test]
fn mode_error()
{
	static GZIP_ERR: (&'static [u8], &'static [u8]) = (b"transfer-encoding", b"chunked,gzip");
	let mut a = HeaderAccumulator::new(false);
	assert!(a.set_header(GZIP_ERR.0, GZIP_ERR.1).is_err());
	let mut a = HeaderAccumulator::new(true);
	assert!(a.set_header(GZIP_ERR.0, GZIP_ERR.1).is_err());
}
*/

#[cfg(test)]
struct ActionList(Vec<String>);

#[cfg(test)]
impl ElementSink for ActionList
{
	fn protocol_supported(&self, proto: &[u8]) -> bool
	{
		if proto == b"websocket" { return true; }
		false
	}
	fn sink<'a>(&mut self, elem: Element<'a>)
	{
		use crate::escape_bytes;
		match elem {
			Element::Data(x) => {
				let x = escape_bytes(x);
				self.0.push(format!("Data: '{x}'"))
			},
			Element::Status(x) => self.0.push(format!("Status {x}")),
			Element::Method(x) => {
				let x = escape_bytes(x);
				self.0.push(format!("Method {x}"))
			},
			Element::Scheme(x) => {
				let x = escape_bytes(x);
				self.0.push(format!("Scheme {x}"))
			},
			Element::Authority(x) => {
				let x = escape_bytes(x);
				self.0.push(format!("Authority {x}"))
			},
			Element::AuthorityRaw(x) => {
				let x = escape_bytes(x);
				self.0.push(format!("AuthorityRaw {x}"))
			},
			Element::Path(x) => {
				let x = escape_bytes(x);
				self.0.push(format!("Path {x}"))
			},
			Element::PathRaw(x) => {
				let x = escape_bytes(x);
				self.0.push(format!("PathRaw {x}"))
			},
			Element::ContentLength(x) => self.0.push(format!("Content-Length {x}")),
			Element::Protocol(x) => {
				let x = escape_bytes(x);
				self.0.push(format!("Protocol {x}"))
			},
			Element::Query(x) => {
				let x = escape_bytes(x);
				self.0.push(format!("Query {x}"))
			},
			Element::Reason(x) => {
				let x = escape_bytes(x);
				self.0.push(format!("Reason {x}"))
			},
			Element::Version(x) => {
				let x = escape_bytes(x);
				self.0.push(format!("Version {x}"))
			},
			Element::Header(x, y) => {
				let x = escape_bytes(x);
				let y = escape_bytes(y);
				self.0.push(format!("Header: '{x}: {y}'"))
			},
			Element::EndOfHeadersInterim => self.0.push(format!("End of interim headers")),
			Element::EndOfHeaders => self.0.push(format!("End of headers")),
			Element::EndOfMessage => self.0.push(format!("End of message")),
			Element::Trailer(x, y) => {
				let x = escape_bytes(x);
				let y = escape_bytes(y);
				self.0.push(format!("Trailer: '{x}: {y}'"))
			},
			Element::HeadersFaulty(x) => self.0.push(format!("Headers faulty({x})")),
			Element::Abort(x) => self.0.push(format!("Abort({x})")),
			Element::StartOfMessage => self.0.push(format!("Start of message")),
			//These should not be used.
			Element::HeaderStd(x,y) => {
				let y = escape_bytes(y);
				self.0.push(format!("HeaderStd: {x}: '{y}'"))
			},
			Element::TrailerStd(x,y) => {
				let y = escape_bytes(y);
				self.0.push(format!("TrailerStd: {x}: '{y}'"))
			},
			Element::ConnectionHeader(x) => {
				let x = escape_bytes(x);
				self.0.push(format!("Connection: '{x}'"))
			},
			Element::ConnectionHeaderStd(x) => self.0.push(format!("ConnectionStd: {x}")),
			//_ => self.0.push(format!("UNKNOWN")),
		}
	}
}

#[cfg(test)]
fn string_list(x: &[String], y: &[&str])
{
	let mut p = 0;
	for (a, b) in x.iter().zip(y.iter().cloned()) {
		if a.deref() != b {
			println!("Parser emitted '{a}', reference solution has '{b}' (position {p})");
			assert!(false);
		}
		p += 1;
	}
	if x.len() < y.len() {
		let next = y[x.len()];
		println!("Parser ended, reference solution still has '{next}'");
		assert!(false);
	} else if x.len() > y.len() {
		let next = &x[y.len()];
		println!("Parser emitted '{next}', but reference solution has nothing");
		assert!(false);
	}
}

#[cfg(test)]
struct Buffer(Vec<u8>);

#[cfg(test)]
impl ReceiveBuffer for Buffer
{
	fn new() -> Self { Buffer(Vec::new()) }
	fn borrow<'a>(&'a mut self) -> &'a mut [u8] { &mut self.0 }
}

#[cfg(test)]
struct StaticString(&'static [u8], bool);

#[cfg(test)]
impl Source for StaticString
{
	type Buffer = Buffer;
	type Error = ();
	fn read(&mut self, buf: &mut Buffer) -> Result<bool, ()>
	{
		buf.0 = self.0.to_owned();
		let flag = replace(&mut self.1, false);
		Ok(flag)
	}
}

#[cfg(test)]
struct EOF;

#[cfg(test)]
impl Source for EOF
{
	type Buffer = Buffer;
	type Error = ();
	fn read(&mut self, _: &mut Buffer) -> Result<bool, ()> { Ok(false) }
}

#[cfg(test)]
fn strsrc(x: &'static [u8]) -> StaticString { StaticString(x, true) }



#[test]
fn get_proxy_empty_path()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.push(&mut strsrc(b"GET httpx://example.org HTTP/1.1\r\nHost: \r\n\r\n"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"AuthorityRaw example.org",
		"Authority example.org",
		"Scheme httpx",
		"PathRaw /",
		"Path /",
		"End of message",
	]);
}

#[test]
fn options_proxy_empty_path()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.push(&mut strsrc(b"OPTIONS httpx://example.org HTTP/1.1\r\nHost: \r\n\r\n"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method OPTIONS",
		"Version HTTP/1.1",
		"AuthorityRaw example.org",
		"Authority example.org",
		"Scheme httpx",
		"PathRaw *",
		"Path *",
		"End of message",
	]);
}

#[test]
fn simple_get_proxy()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.push(&mut strsrc(b"GET httpx://example.org/foo HTTP/1.1\r\nHost: \r\n\r\n"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"AuthorityRaw example.org",
		"Authority example.org",
		"Scheme httpx",
		"PathRaw /foo",
		"Path /foo",
		"End of message",
	]);
}

#[test]
fn simple_get_proxy_duplicate()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.push(&mut strsrc(b"GET httpx://example.org/foo HTTP/1.1\r\nHost: example.org\r\n\r\n"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"AuthorityRaw example.org",
		"Authority example.org",
		"Scheme httpx",
		"PathRaw /foo",
		"Path /foo",
		"End of message",
	]);
}

#[test]
fn simple_get_proxy_duplicate_uppercase()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.push(&mut strsrc(b"GET httpx://example.org/foo HTTP/1.1\r\nHost: EXAMPLE.ORG\r\n\r\n"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"AuthorityRaw example.org",
		"Authority example.org",
		"Scheme httpx",
		"PathRaw /foo",
		"Path /foo",
		"End of message",
	]);
}

#[test]
fn simple_get_proxy_smuggling()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	let x = s.push(&mut strsrc(b"GET httpx://example.org/foo HTTP/1.1\r\nHost: example.net\r\n\r\n"), &mut a);
	assert_eq!(format!("{x:?}"), "Err(Stream(Error(FatalHeaderErrorF(IllegalAuthority(Inconsistent)))))");
}

#[test]
fn simple_get_proxy_lowercase()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.push(&mut strsrc(b"GET httpx://EXAMPLE.ORG/foo HTTP/1.1\r\nHost: \r\n\r\n"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	assert_eq!(a.0[3], "AuthorityRaw EXAMPLE.ORG");
	assert_eq!(a.0[4], "Authority example.org");
}

#[test]
fn simple_get_normal()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nHost: example.org\r\n\r\n"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"Scheme https",
		"PathRaw /foo",
		"Path /foo",
		"AuthorityRaw example.org",
		"Authority example.org",
		"End of message",
	]);
}

#[test]
fn simple_get_two_hosts_match()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	let x = s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nHost: example.org\r\nHost: example.org\r\n\r\n"), &mut a);
	assert_eq!(format!("{x:?}"), "Err(Stream(Error(FatalHeaderErrorF(MultipleHost))))");
}

#[test]
fn simple_get_two_hosts_dont_match()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	let x = s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nHost: example.org\r\nHost: example.net\r\n\r\n"), &mut a);
	assert_eq!(format!("{x:?}"), "Err(Stream(Error(FatalHeaderErrorF(MultipleHost))))");
}

#[test]
fn simple_get_normal_lowercase()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nHost: EXAMPLE.ORG\r\n\r\n"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"Scheme https",
		"PathRaw /foo",
		"Path /foo",
		"AuthorityRaw EXAMPLE.ORG",
		"Authority example.org",
		"End of message",
	]);
}

#[test]
fn get_split_host()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nfoo:bar\r\nHost: ex"), &mut a).unwrap();
	s.push(&mut strsrc(b"ample.org\r\n\r\n"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"Scheme https",
		"PathRaw /foo",
		"Path /foo",
		"AuthorityRaw example.org",
		"Authority example.org",
		"Header: 'foo: bar'",
		"End of message",
	]);
}

#[test]
fn get_split_previous()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nfoo:"), &mut a).unwrap();
	s.push(&mut strsrc(b"bar\r\nHost: example.org\r\n\r\n"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"Scheme https",
		"PathRaw /foo",
		"Path /foo",
		"AuthorityRaw example.org",
		"Authority example.org",
		"Header: 'foo: bar'",
		"End of message",
	]);
}

#[test]
fn get_split_next()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nfoo:bar\r\nHost: example.org\r\nx:"), &mut a).unwrap();
	s.push(&mut strsrc(b"y\r\n\r\n"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"Scheme https",
		"PathRaw /foo",
		"Path /foo",
		"AuthorityRaw example.org",
		"Authority example.org",
		"Header: 'foo: bar'",
		"Header: 'x: y'",
		"End of message",
	]);
}

#[test]
fn get_missing_host()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	let x = s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nfoo:bar\r\n\r\n"), &mut a);
	assert_eq!(format!("{x:?}"), "Err(Stream(Error(NoAuthority)))");
}

#[test]
fn proxy_get_missing_host()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	let x = s.push(&mut strsrc(b"GET https://example.org/foo HTTP/1.1\r\nfoo:bar\r\n\r\n"), &mut a);
	assert_eq!(format!("{x:?}"), "Err(Stream(Error(NoAuthority)))");
}


#[test]
fn simple_get_normal_normalize()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	//There is no way to have percent normalization in host, as only part of host that would allow
	//percent-normalization is userinfo, and we do not allow that.
	s.push(&mut strsrc(b"GET /foo%3Abar%3a HTTP/1.1\r\nHost: eXample.org\r\n\r\n"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"Scheme https",
		"PathRaw /foo%3Abar%3a",
		"Path /foo:bar:",
		"AuthorityRaw eXample.org",
		"Authority example.org",
		"End of message",
	]);
}


#[test]
fn simple_get_normal_with_foo()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.set_use_std_flag();	//This should not modify custom headers.
	s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nfoo:\tbar\r\nHost: example.org\r\nqux: zot\r\n\r\n"),
		&mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"Scheme https",
		"PathRaw /foo",
		"Path /foo",
		"AuthorityRaw example.org",
		"Authority example.org",
		"Header: 'foo: bar'",
		"Header: 'qux: zot'",
		"End of message",
	]);
}

#[test]
fn simple_get_with_body()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nCONTENT-LENGTH:3\r\nHost: example.org\r\n\r\nabc"),
		&mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"Scheme https",
		"PathRaw /foo",
		"Path /foo",
		"AuthorityRaw example.org",
		"Authority example.org",
		"Content-Length 3",
		"Header: 'content-length: 3'",
		"End of headers",
		"Data: 'abc'",
		"End of message",
	]);
}

#[test]
fn simple_get_with_body_usestd()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.set_use_std_flag();
	s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nCONTENT-LENGTH:3\r\nHost: example.org\r\n\r\nabc"),
		&mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"Scheme https",
		"PathRaw /foo",
		"Path /foo",
		"AuthorityRaw example.org",
		"Authority example.org",
		"Content-Length 3",
		"HeaderStd: content-length: '3'",
		"End of headers",
		"Data: 'abc'",
		"End of message",
	]);
}

#[test]
fn simple_connection_usestd()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.set_use_std_flag();
	s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nCONTENT-LENGTH:3\r\nHost: example.org\r\n\
		CONNECTION:   CLOSE  , cC,  FOObar \r\n\r\nabc"),
		&mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"Scheme https",
		"PathRaw /foo",
		"Path /foo",
		"AuthorityRaw example.org",
		"Authority example.org",
		"ConnectionStd: close",
		"ConnectionStd: cc",
		"Connection: 'foobar'",
		"Content-Length 3",
		"HeaderStd: content-length: '3'",
		"End of headers",
		"Data: 'abc'",
		"End of message",
	]);
}

#[test]
fn simple_get_with_body_chunked()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nTRANSFER-ENCODING:chunked\r\n\
		Host: example.org\r\n\r\n3\r\nabc\r\n4;foo\r\ntest\r\n0\r\n\r\n"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"Scheme https",
		"PathRaw /foo",
		"Path /foo",
		"AuthorityRaw example.org",
		"Authority example.org",
		"Header: 'transfer-encoding: chunked'",
		"End of headers",
		"Data: 'abc'",
		"Data: 'test'",
		"End of message",
	]);
}

#[test]
fn simple_get_with_body_chunked_usestd()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.set_use_std_flag();
	s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nTRANSFER-ENCODING:chunked\r\n\
		Host: example.org\r\n\r\n3\r\nabc\r\n4;foo\r\ntest\r\n0\r\n\r\n"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"Scheme https",
		"PathRaw /foo",
		"Path /foo",
		"AuthorityRaw example.org",
		"Authority example.org",
		"HeaderStd: transfer-encoding: 'chunked'",
		"End of headers",
		"Data: 'abc'",
		"Data: 'test'",
		"End of message",
	]);
}

#[test]
fn simple_get_with_body_trailers()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nTRANSFER-ENCODING:chunked\r\nHost: example.org\r\n\r\n\
		3\r\nabc\r\n4;foo\r\ntest\r\n0\r\ntname: tvalue\r\n\r\n"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"Scheme https",
		"PathRaw /foo",
		"Path /foo",
		"AuthorityRaw example.org",
		"Authority example.org",
		"Header: 'transfer-encoding: chunked'",
		"End of headers",
		"Data: 'abc'",
		"Data: 'test'",
		"Trailer: 'tname: tvalue'",
		"End of message",
	]);
}

#[test]
fn connect_request()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.push(&mut strsrc(b"CONNECT example.org:443 HTTP/1.1\r\nHost: example.org:443\r\n\r\n"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method CONNECT",
		"Version HTTP/1.1",
		"AuthorityRaw example.org:443",
		"Authority example.org:443",
		"End of message",
	]);
}

#[test]
fn simple_response()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.set_reply(false);
	s.push(&mut strsrc(b"HTTP/1.1 200 OK\r\n\r\n"), &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Version HTTP/1.1",
		"Status 200",
		"Reason OK",
		"End of headers",
	]);
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Version HTTP/1.1",
		"Status 200",
		"Reason OK",
		"End of headers",
		"End of message",
	]);
}

#[test]
fn simple_response_length()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.set_reply(false);
	s.push(&mut strsrc(b"HTTP/1.1 200 OK\r\nContent-Length:3\r\n\r\nabc"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Version HTTP/1.1",
		"Status 200",
		"Reason OK",
		"Content-Length 3",
		"Header: 'content-length: 3'",
		"End of headers",
		"Data: 'abc'",
		"End of message",
	]);
}

#[test]
fn simple_response_length_100continue()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.set_reply(false);
	s.push(&mut strsrc(b"HTTP/1.1 100 Go on\r\n\r\nHTTP/1.1 200 OK\r\nContent-Length:3\r\n\r\nabc"), &mut a).
		unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Version HTTP/1.1",
		"Status 100",
		"Reason Go on",
		"End of interim headers",
		"Version HTTP/1.1",
		"Status 200",
		"Reason OK",
		"Content-Length 3",
		"Header: 'content-length: 3'",
		"End of headers",
		"Data: 'abc'",
		"End of message",
	]);
}


#[test]
fn simple_get_with_body_trailers_multipart()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nTRANSFER-"), &mut a).unwrap();
	s.push(&mut strsrc(b"ENCODING:chunked\r\nHost: example.org\r\n\r\n3\r\na"), &mut a).unwrap();
	s.push(&mut strsrc(b"bc\r\n4;foo\r\ntest\r\n0\r\ntname: tvalue\r\n\r\n"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"Scheme https",
		"PathRaw /foo",
		"Path /foo",
		"AuthorityRaw example.org",
		"Authority example.org",
		"Header: 'transfer-encoding: chunked'",
		"End of headers",
		"Data: 'a'",
		"Data: 'bc'",
		"Data: 'test'",
		"Trailer: 'tname: tvalue'",
		"End of message",
	]);
}

#[test]
fn upgrade_unsupported()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.set_reply(false);
	let result = s.push(&mut strsrc(
		b"HTTP/1.1 101 Upgrade\r\nUpgrade: foo\r\nContent-Length:3\r\n\r\nHEEEELLLLOOO"), &mut a);
	assert_eq!(&format!("{result:?}"), "Err(Stream(Error(FatalHeaderErrorF(UnsupportedProtocol))))");
}

#[test]
fn upgrade_websocket()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.set_reply(false);
	s.push(&mut strsrc(b"HTTP/1.1 101 Upgrade\r\nUpgrade: websocket\r\nContent-Length:3\r\n\r\nHEEEELLLLOOO"),
		&mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Version HTTP/1.1",
		"Status 101",
		"Reason Upgrade",
		"Protocol websocket",
		"Content-Length 3",
		"Header: 'content-length: 3'",
		"End of headers",
		"Data: 'HEEEELLLLOOO'",
	]);
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Version HTTP/1.1",
		"Status 101",
		"Reason Upgrade",
		"Protocol websocket",
		"Content-Length 3",
		"Header: 'content-length: 3'",
		"End of headers",
		"Data: 'HEEEELLLLOOO'",
		"End of message",
	]);
}

#[test]
fn simple_response_length_102hint()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.set_reply(false);
	s.push(&mut strsrc(b"HTTP/1.1 102 hint\r\n\r\nHTTP/1.1 200 OK\r\nContent-Length:3\r\n\r\nabc"), &mut a).
		unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Version HTTP/1.1",
		"Status 102",
		"Reason hint",
		"End of interim headers",
		"Version HTTP/1.1",
		"Status 200",
		"Reason OK",
		"Content-Length 3",
		"Header: 'content-length: 3'",
		"End of headers",
		"Data: 'abc'",
		"End of message",
	]);
}

#[test]
fn simple_response_length_move()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.set_reply(false);
	s.push(&mut strsrc(b"HTTP/1.1 200 OK\r\nContent-Length:3\r\nX:Y\r\n\r\nabc"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Version HTTP/1.1",
		"Status 200",
		"Reason OK",
		"Header: 'x: Y'",
		"Content-Length 3",
		"Header: 'content-length: 3'",
		"End of headers",
		"Data: 'abc'",
		"End of message",
	]);
}

#[test]
fn two_requests_recover_gracefully()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nContent-Length:5\r\n\r\nabcde\
GET /bar HTTP/1.1\r\nHost:example.org\r\n\r\n"), &mut a).unwrap_err();
	s.push(&mut EOF, &mut a).unwrap_err();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"Scheme https",
		"PathRaw /foo",
		"Path /foo",
		"Content-Length 5",
		"Header: 'content-length: 5'",
		"Abort(No authority)",
	]);
}

#[test]
fn two_requests_recover_gracefully_bodylen()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nContent-Length:5\r\nTransfer-Encoding:chunked\r\n\
host:zot\r\n\r\n5\r\nabcde\r\n0\r\n\r\nGET /bar HTTP/1.1\r\nHost:example.org\r\n\r\n"), &mut a).unwrap_err();
	s.push(&mut EOF, &mut a).unwrap_err();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"Scheme https",
		"PathRaw /foo",
		"Path /foo",
		"AuthorityRaw zot",
		"Authority zot",
		"Abort(Illegal Content-Length)",
	]);
}

#[test]
fn two_requests_recover_gracefully_bodylen2()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nTransfer-Encoding:chunked\r\nContent-Length:5\r\n\
host:zot\r\n\r\n5\r\nabcde\r\n0\r\n\r\nGET /bar HTTP/1.1\r\nHost:example.org\r\n\r\n"), &mut a).unwrap_err();
	s.push(&mut EOF, &mut a).unwrap_err();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"Scheme https",
		"PathRaw /foo",
		"Path /foo",
		"AuthorityRaw zot",
		"Authority zot",
		"Header: 'transfer-encoding: chunked'",
		"Abort(Illegal Content-Length)",
	]);
}

#[test]
fn simple_response_length_move_big()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.set_reply(false);
	s.push(&mut strsrc(b"HTTP/1.1 200 OK\r\nContent-Length:53\r\nX:Y\r\n\r\n\
ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Version HTTP/1.1",
		"Status 200",
		"Reason OK",
		"Header: 'x: Y'",
		"Content-Length 53",
		"Header: 'content-length: 53'",
		"End of headers",
		"Data: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz'",
		"End of message",
	]);
}

#[test]
fn simple_response_length_move_big_head()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.set_reply(true);
	s.push(&mut strsrc(b"HTTP/1.1 200 OK\r\nContent-Length:53\r\nX:Y\r\n\r\n"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Version HTTP/1.1",
		"Status 200",
		"Reason OK",
		"Header: 'x: Y'",
		"Content-Length 53",
		"Header: 'content-length: 53'",
		"End of message",
	]);
}

#[test]
fn transfer_encoding_and_length()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.set_reply(true);
	s.push(&mut strsrc(b"HTTP/1.1 200 OK\r\nContent-Length:53\r\nTransfer-Encoding:chunked\r\n\r\n"), &mut a).
		unwrap_err();
	string_list(&a.0, &[
		"Start of message",
		"Version HTTP/1.1",
		"Status 200",
		"Reason OK",
		"Abort(Illegal Content-Length)",
	]);
}

#[test]
fn transfer_encoding_and_length2()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.set_reply(true);
	s.push(&mut strsrc(b"HTTP/1.1 200 OK\r\nTransfer-Encoding:chunked\r\nContent-Length:53\r\n\r\n"), &mut a).
		unwrap_err();
	string_list(&a.0, &[
		"Start of message",
		"Version HTTP/1.1",
		"Status 200",
		"Reason OK",
		"Header: 'transfer-encoding: chunked'",
		"Abort(Illegal Content-Length)",
	]);
}

#[test]
fn how_long_is_this()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.set_reply(false);
	s.push(&mut strsrc(b"HTTP/1.1 200 OK\r\nContent-Length:1\r\nContent-Length:12\r\n\r\n"), &mut a).
		unwrap_err();
	string_list(&a.0, &[
		"Start of message",
		"Version HTTP/1.1",
		"Status 200",
		"Reason OK",
		"Abort(Inconsistent Content-Length)",
	]);
}

#[test]
fn upgrade_to_websocket()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nHost:foo\r\nUpgrade:websocket\r\nconnection:upgrade\r\n\r\n"),
		&mut a).unwrap();
	s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nHost:foo\r\n\r\n"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"Scheme https",
		"PathRaw /foo",
		"Path /foo",
		"AuthorityRaw foo",
		"Authority foo",
		"Protocol websocket",
		"Header: 'connection: upgrade'",
		"End of headers",
		"Data: 'GET /foo HTTP/1.1\\r\\nHost:foo\\r\\n\\r\\n'",
		"End of message",
	]);
}

#[test]
fn upgrade_to_websocket_2nd_chance()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nHost:foo\r\nUpgrade:foo, websocket \r\n\
		connection:upgrade\r\n\r\n"), &mut a).unwrap();
	s.push(&mut strsrc(b"GET /foo HTTP/1.1\r\nHost:foo\r\n\r\n"), &mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Method GET",
		"Version HTTP/1.1",
		"Scheme https",
		"PathRaw /foo",
		"Path /foo",
		"AuthorityRaw foo",
		"Authority foo",
		"Protocol websocket",
		"Header: 'connection: upgrade'",
		"End of headers",
		"Data: 'GET /foo HTTP/1.1\\r\\nHost:foo\\r\\n\\r\\n'",
		"End of message",
	]);
}

#[test]
fn require_websocket()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.set_reply(false);
	s.push(&mut strsrc(b"HTTP/1.1 426 Upgrade required\r\nContent-Length:3\r\nUpgrade: websocket\r\n\r\nabc"),
		&mut a).unwrap();
	s.push(&mut EOF, &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Version HTTP/1.1",
		"Status 426",
		"Reason Upgrade required",
		"Header: 'upgrade: websocket'",
		"Content-Length 3",
		"Header: 'content-length: 3'",
		"End of headers",
		"Data: 'abc'",
		"End of message",
	]);
}

#[test]
fn encode_request()
{
	let mut s = [0;512];
	let slen = {
		let mut s = SliceSink::new(&mut s);
		HeaderEncoder::request_line(&mut s, b"GET", b"https", b"foo.example", b"/bar", b"?zot").unwrap();
		HeaderEncoder::header(&mut s, b"abc", b"xyzw").unwrap();
		HeaderEncoder::end(&mut s).unwrap();
		s.written()
	};
	let s = &s[..slen];
	assert_eq!(s, &b"GET /bar?zot HTTP/1.1\r\nhost:foo.example\r\nabc:xyzw\r\n\r\n"[..]);
}

#[test]
fn encode_reply()
{
	let mut s = [0;512];
	let slen = {
		let mut s = SliceSink::new(&mut s);
		HeaderEncoder::status_line(&mut s, 245, b"xyzzy").unwrap();
		HeaderEncoder::header(&mut s, b"abc", b"xyzw").unwrap();
		HeaderEncoder::end(&mut s).unwrap();
		s.written()
	};
	let s = &s[..slen];
	assert_eq!(s, &b"HTTP/1.1 245 xyzzy\r\nabc:xyzw\r\n\r\n"[..]);
}

#[test]
fn response_headerline_hang()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.set_reply(false);
	s.push(&mut strsrc(b"HTTP/1.1 200 OK\r\nContent-Length:1\r\n\n"), &mut a).unwrap_err();
	s.push(&mut strsrc(b"\n"), &mut a).unwrap_err();
}

#[test]
fn response_headerline_split3()
{
	let mut a = ActionList(Vec::new());
	let mut s = Stream::new(b"https");
	s.set_reply(false);
	s.push(&mut strsrc(b"HTTP/1.1 200 OK\r\nfoo:"), &mut a).unwrap();
	s.push(&mut strsrc(b"bar"), &mut a).unwrap();
	s.push(&mut strsrc(b"\r\n\r\n"), &mut a).unwrap();
	string_list(&a.0, &[
		"Start of message",
		"Version HTTP/1.1",
		"Status 200",
		"Reason OK",
		"Header: 'foo: bar'",
		"End of headers",
	]);
}
