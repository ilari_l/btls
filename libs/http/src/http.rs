//!Generic HTTP definitions.
#![allow(unsafe_code)]
#![forbid(unused_must_use)]
#![deny(missing_docs)]

pub use crate::http2_hpack::StandardHttpHeader;
use btls_aux_fail::f_break;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_memory::Attachment;
use btls_aux_memory::copy_if_fits;
use btls_aux_memory::split_attach_first;
use btls_aux_memory::trim_ascii;
use btls_aux_serialization::Sink as DSink;
use core::fmt::Debug;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::str::from_utf8;
use core::str::from_utf8_unchecked;
use core::str::FromStr;


macro_rules! writeb
{
	($arr:ident $witr:ident $e:expr) => {{
		let ptr = $arr.get_mut($witr).ok_or(PartialDecodeError::IntBufferError)?;
		*ptr = $e;
		$witr = $witr.checked_add(1).ok_or(PartialDecodeError::IntBufferError)?;
	}}
}

///Error performing partial decode.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
#[non_exhaustive]
pub enum PartialDecodeError
{
	///Illegal byte in state.
	Illegal(u8, u8, usize),
	///Illegal byte
	Illegal2(u8, usize),
	///Illegal hex byte
	IllegalE(u8, usize),
	///Unclosed bracket.
	UnclosedBracket,
	///Truncated hex escape.
	TruncatedHexEscape,
	///Inconsistent.
	Inconsistent,
	///Too long.
	TooLong,
	///Empty.
	Empty,
	///Internal error: Illegal hex substate.
	IntIllegalHE(u8),
	///Internal error: Buffer error
	IntBufferError,
}

impl Display for PartialDecodeError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::PartialDecodeError::*;
		match *self {
			Illegal(b, s, p) => write!(f, "Illegal byte {b} (position {p} state 0-{s})"),
			Illegal2(b, p) => write!(f, "Illegal byte {b} (position {p})"),
			IllegalE(b, p) => write!(f, "Illegal hex byte {b} (position {p})"),
			TruncatedHexEscape => write!(f, "Truncated hex escape"),
			Inconsistent => write!(f, "Inconsistent with previous value"),
			TooLong => write!(f, "Too long"),
			Empty => write!(f, "Empty value"),
			UnclosedBracket => write!(f, "Unclosed square bracket"),
			IntIllegalHE(s) => write!(f, "Internal error: Hex escape in substate {s}"),
			IntBufferError => write!(f, "Internal error: Buffer overrun"),
		}
	}
}

fn partial_decode<'a>(s: &'a mut [u8], allowed: impl Fn(u8) -> bool, special: bool) ->
	Result<&'a mut [u8], PartialDecodeError>
{
	let allows_q = allowed(b'?');
	//Special states:
	//0 => Ordinary (allows usual set of escapes).
	//1 => Special Initial (allows usual set of escapes).
	//2 => Special Ordinary (allows usual set of escapes).
	//3 => Quoted address (allows special set of escapes)
	//4 => One after Closing brace of quoted address. (no escape allowed)
	//5 => Port number. (no escape allowed)
	let mut xsstate = if special { 1 } else { 0 };
	let mut wptr = 0;
	let mut state = 0;
	let slen = s.len();
	for rptr in 0..slen {
		let c = s[rptr];
		match state {
			0 => match (xsstate, c) {
				//Special state 0:
				//% => Start escape.
				//(Any allowed character) => Emit.
				(0,b'%') => state = 1,
				(0, c) if allowed(c) => writeb!(s wptr c),
				//Special state 1:
				//% => Start escape.
				//: => Move to special state 5, emit.
				//[ => Move to special state 3, emit.
				//(Any allowed character) => Move to state 2, Emit lowercased.
				(1,b'%') => state = 1,
				(1,b':') => { writeb!(s wptr c); xsstate = 5; },
				(1,b'[') => { writeb!(s wptr c); xsstate = 3; },
				(1,c) if allowed(c) => { writeb!(s wptr c.to_ascii_lowercase()); xsstate = 2; },
				//Special state 2:
				//% => Start escape.
				//: => Move to special state 5, emit.
				//(Any allowed character) => Emit lowercased.
				(2,b'%') => state = 1,
				(2,b':') => { writeb!(s wptr c); xsstate = 5; },
				(2,c) if allowed(c) => writeb!(s wptr c.to_ascii_lowercase()),
				//Special state 3:
				//% => Start escape.
				//] => Move to special state 4, emit.
				//(Any alternate allowed character) => Emit lowercased.
				(3,b'%') => state = 1,
				(3,b']') => { writeb!(s wptr c); xsstate = 4; },
				(3,c) if is_fchar(c) => writeb!(s wptr c.to_ascii_lowercase()),
				//Special state 4:
				//: => Move to special state 5, emit.
				(4,b':') => { writeb!(s wptr c); xsstate = 5; },
				//Special state 5:
				//(digit) => Emit.
				(5,c) if is_digit(c) => writeb!(s wptr c),
				//Nothing else is allowed.
				(s, c) => fail!(PartialDecodeError::Illegal(c, s, rptr))
			},
			1 if is_xdigit(c) => {
				let c = (c & 0xDF).wrapping_sub(48);
				let c = c.wrapping_sub(c / 16 % 2 * 7);
				state = 16 | c % 16;
			},
			16..=31 if is_xdigit(c) => {
				let c = (c & 0xDF).wrapping_sub(48);
				let c = c.wrapping_sub(c / 16 % 2 * 7);
				let c = (state << 4) | c % 16;
				let (lcase, allow) = match xsstate {
					0 => (false, allowed(c)),		//Normal rules.
					1 => (true, allowed(c)),		//Normal rules, lowercasing.
					2 => (true, allowed(c)),		//Normal rules, lowercasing.
					3 => (true, is_fchar(c)),		//Alternate rules, lowercasing.
					s => fail!(PartialDecodeError::IntIllegalHE(s))
				};
				//HACK: If ? is allowed, this is presumably query. Do not partially unescape '&',
				//as it is escaped if it appears in query field values.
				if allow && (!allows_q || c != b'&') {
					if lcase {
						writeb!(s wptr c.to_ascii_lowercase());
					} else {
						writeb!(s wptr c);
					}
				} else {
					let h = c >> 4;
					let l = c % 16;
					writeb!(s wptr b'%');
					writeb!(s wptr h + if h > 9 { 87 } else { 48 });
					writeb!(s wptr l + if l > 9 { 87 } else { 48 });
				}
				//If special state is 1, move to special state 2, since this character was not
				//[ (or was literial [).
				if xsstate == 1 { xsstate = 2; }
				state = 0;
			},
			_ => fail!(PartialDecodeError::IllegalE(c, rptr))
		}
	}
	//The special state 3 is not allowed at the end, since that means the ']' is missing. Also, the escape
	//state can not be nonzero, because that means there is truncated escape sequence.
	fail_if!(xsstate == 3, PartialDecodeError::UnclosedBracket);
	fail_if!(state != 0, PartialDecodeError::TruncatedHexEscape);
	s.get_mut(..wptr).ok_or(PartialDecodeError::IntBufferError)
}

#[doc(hidden)]
pub fn partial_decode_path<'a>(p: &'a mut [u8]) -> Result<&'a mut [u8], PartialDecodeError>
{
	partial_decode(p, is_pchar, false)
}

#[doc(hidden)]
pub fn partial_decode_query<'a>(q: &'a mut [u8]) -> Result<&'a mut [u8], PartialDecodeError>
{
	partial_decode(q, is_qchar, false)
}

#[doc(hidden)]
pub fn partial_decode_authority<'a>(q: &'a mut [u8]) -> Result<&'a mut [u8], PartialDecodeError>
{
	partial_decode(q, is_achar, true)
}



///Octet string.
pub trait OctetString
{
	///Get length.
	fn length(&self) -> usize;
	///Read byte by index.
	fn index(&self, idx: usize) -> u8;
	///Slice.
	fn slice(self, start: usize, end: usize) -> Self;
}

impl<'a> OctetString for &'a [u8]
{
	fn length(&self) -> usize { self.len() }
	fn index(&self, idx: usize) -> u8 { self[idx] }
	fn slice(self, start: usize, end: usize) -> Self { &self[start..end] }
}

impl<'a> OctetString for &'a mut [u8]
{
	fn length(&self) -> usize { self.len() }
	fn index(&self, idx: usize) -> u8 { self[idx] }
	fn slice(self, start: usize, end: usize) -> Self { &mut self[start..end] }
}

impl<'a> OctetString for &'a str
{
	fn length(&self) -> usize { self.len() }
	fn index(&self, idx: usize) -> u8 { self.as_bytes()[idx] }
	fn slice(self, start: usize, end: usize) -> Self { &self[start..end] }
}


///Trim whitespace on both ends of string.
pub fn http_trim<O:OctetString>(value: O) -> O
{
	let mut ss = 0;
	let mut se = value.length();
	//This loop can execute maximum of value.length() times. Each iteration increments ss by 1, and eventually it
	//will hit value.length(), which causes the loop to end by itself.
	while ss < value.length() && is_wchar(value.index(ss)) { ss += 1; }
	//This loop can execute maximum of value.length() times. Each iteration decrements ss by 1, and eventually it
	//will hit ss, which is at least 0, which causes the loop to end by itself.
	while se > ss && is_wchar(value.index(se-1)) { se -= 1; }
	value.slice(ss, se)
}

#[doc(hidden)]
pub use btls_aux_memory::equals_case_insensitive;


fn is_valid_scheme(s: &[u8]) -> Result<(), PartialDecodeError>
{
	fail_if!(s.len() < 1, PartialDecodeError::Empty);
	let (h,t) = s.split_at(1);
	let h = h[0];
	fail_if!(!is_alpha(h), PartialDecodeError::Illegal2(h, 0));
	if let Some((pos, illegal)) =  t.iter().cloned().enumerate().find(|&(_, c)|!is_schar(c)) {
		fail!(PartialDecodeError::Illegal2(illegal, pos + 1));
	}
	Ok(())
}

fn is_mask(x: u8, m2: u64, m1: u64) -> bool
{
	if x < 64 { (m1 >> x%64) & 1 != 0 } else if x < 128 { (m2 >> x%64) & 1 != 0 } else { false }
}

const M2_ALPHA: u64 = 0x07FFFFFE07FFFFFE;
const M2_ALPHA_LC: u64 = 0x07FFFFFE00000000;
const M1_DIGIT: u64 = 0x03FF000000000000;
const M2_UNRES: u64 = 0x4000000080000000|M2_ALPHA;
const M2_UNRES_LC: u64 = 0x4000000080000000|M2_ALPHA_LC;
const M1_UNRES: u64 = 0x0000600000000000|M1_DIGIT;
const M1_SDELIM: u64 = 0x28001FD200000000;
const M2_AT: u64 = 0x0000000000000001;
const M1_COLON: u64 = 0x0400000000000000;
const M1_QEXTRA: u64 = 0x8000800000000000;
const M1_PLUS: u64 = 0x0000080000000000;
const M2_TCHAR: u64 = 0x1000000140000000;
const M1_TCHAR: u64 = 0x00000CFA00000000;
const M1_SLASH: u64 = 0x0000800000000000;
const M1_WSPACE: u64 = 0x0000000100000200;

///Is alpha?
fn is_alpha(x: u8) -> bool { is_mask(x, M2_ALPHA, 0) }
#[doc(hidden)]
pub fn is_digit(x: u8) -> bool { is_mask(x, 0, M1_DIGIT) }
//fn is_unreserved(x: u8) -> bool { is_mask(x, M2_UNRES, M1_UNRES) }
//fn is_sub_delim(x: u8) -> bool { is_mask(x, 0, M1_SDELIM) }
//Altough '/' is not an actual pchar, allow it to not run any problems with hidden slashes.
///Is path character?
pub fn is_pchar(x: u8) -> bool { is_mask(x, M2_UNRES|M2_AT, M1_UNRES|M1_SDELIM|M1_COLON|M1_SLASH) }
fn is_qchar(x: u8) -> bool { is_mask(x, M2_UNRES|M2_AT, M1_UNRES|M1_SDELIM|M1_COLON|M1_QEXTRA) }
fn is_achar(x: u8) -> bool { is_mask(x, M2_UNRES, M1_UNRES|M1_SDELIM) }
fn is_fchar(x: u8) -> bool { is_mask(x, M2_UNRES, M1_UNRES|M1_SDELIM|M1_COLON) }
fn is_wchar(x: u8) -> bool { is_mask(x, 0, M1_WSPACE) }
///Is token character?
pub fn is_tchar(x: u8) -> bool { is_mask(x, M2_UNRES|M2_TCHAR, M1_UNRES|M1_TCHAR) }
///Is scheme character?
fn is_schar(x: u8) -> bool { is_mask(x, M2_ALPHA, M1_UNRES|M1_PLUS) }
#[doc(hidden)]
pub fn is_xdigit(x: u8) -> bool { is_mask(x, 0x0000007E0000007E, M1_DIGIT) }
pub(crate) fn is_tchar_hdr(x: u8) -> bool { is_mask(x, M2_UNRES_LC|M2_TCHAR, M1_UNRES|M1_TCHAR) }


///Receive buffer.
pub trait ReceiveBuffer
{
	///Create a new receive buffer.
	fn new() -> Self;
	///Borrow the receive buffer.
	fn borrow<'a>(&'a mut self) -> &'a mut [u8];
}

///Read data from source.
pub trait Source
{
	///Type of associated receive buffer.
	type Buffer: ReceiveBuffer;
	///Type of associated error.
	type Error: Sized+Debug;
	///Read the buffer. Returns true if there is more data, false if this is the end or error if permanent
	///error happens.
	fn read(&mut self, buf: &mut <Self as Source>::Buffer) -> Result<bool, <Self as Source>::Error>;
	///Reclaim the buffer.
	///
	///This is intended for optimizations where the buffer is reused. This is called after data from successful
	///read has been processed and the buffer is about to be dropped.
	fn reclaim_buffer(&mut self, _buf: <Self as Source>::Buffer) {}
}

///Write data to sink
pub trait Sink
{
	///Type of associated error.
	type Error: Sized+Debug+Display;
	///Write the buffer. Needs to accept the entiere concatenation of strings, or return an fatal error.
	fn write(&mut self, buf: &[&[u8]]) -> Result<(), <Self as Sink>::Error>;
}

///Stream error.
///
///If one gets this event, one should generate `RST_STREAM` with error code returned by `get_errorcode()`, if
///any. In case of `HTTP/1.x`, the connection should just be closed.
#[derive(Copy, Clone,PartialEq, Eq,Debug)]
#[non_exhaustive]
pub enum StreamAbort
{
	///Underlying connection lost.
	ConnectionLost,
	///HTTP/2 GOAWAY.
	Goaway(u32),
	///Header error.
	HeaderError(HeaderFault),
	///Error in underlying protocol.
	ProtocolError(&'static str),
	///Stream reset.
	StreamReset(u32, bool),
	///Refused
	Refused,
	///Unsupported protocol.
	UnsupportedProtocol,
	///Internal error.
	InternalError(&'static str),
}

impl Display for StreamAbort
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::StreamAbort::*;
		match *self {
			ConnectionLost => write!(f, "Underlying connection lost"),
			Goaway(code) => write!(f, "Received HTTP/2 Goaway (code={code})"),
			HeaderError(ref err) => Display::fmt(err,  f),
			StreamReset(code, remote) => write!(f, "Stream reset (code={code}, remote={remote})"),
			ProtocolError(ref err) => Display::fmt(err, f),
			Refused => write!(f, "Stream refused"),
			UnsupportedProtocol => write!(f, "Unsupported protocol"),
			InternalError(err) => write!(f, "Internal error: {err}"),
		}
	}
}

impl StreamAbort
{
	///Get error code associated with abort, if any.
	pub fn get_errorcode(&self) -> Option<u32>
	{
		use self::StreamAbort::*;
		match *self {
			ConnectionLost => None,
			Goaway(_) => None,
			HeaderError(_) => None,
			StreamReset(_, _) => None,	//Always received/sent reset already.
			ProtocolError(_) => Some(1),
			Refused => Some(7),
			UnsupportedProtocol => Some(10),
			InternalError(_) => Some(2),
		}
	}
	///Is connection lost error?
	pub fn is_connection_lost(&self) -> bool
	{
		matches!(*self, StreamAbort::ConnectionLost)
	}
}

///Error in header block.
#[derive(Copy, Clone,PartialEq, Eq,Debug)]
#[non_exhaustive]
pub enum HeaderFault
{
	///Illegal authority.
	IllegalAuthority(PartialDecodeError),
	///Illegal chunked.
	IllegalChunked,
	///Illegal content-length
	IllegalContentLength,
	///Illegal data.
	IllegalData,
	///Illegal end of message.
	IllegalEndOfMessage,
	///Illegal header.
	IllegalHeader,
	///Illegal header name.
	IllegalHeaderName(u8),
	///Illegal empty header name.
	IllegalHeaderNameEmpty,
	///Illegal header value.
	IllegalHeaderValue(u8),
	///Illegal method.
	IllegalMethod(PartialDecodeError),
	///Illegal path.
	IllegalPath(PartialDecodeError),
	///Illegal query.
	IllegalQuery(PartialDecodeError),
	///Illegal scheme.
	IllegalScheme(PartialDecodeError),
	///Illegal status.
	IllegalStatus,
	///Illegal trailers.
	IllegalTrailers,
	///Illegal userinfo.
	IllegalUserinfo,
	///Inconsistent content length.
	InconsistentContentLength,
	///No authority.
	NoAuthority,
	///Multiple host headers.
	///
	///This error occurs if message has multiple Host headers.
	MultipleHost,
	///Too late authority.
	TooLateAuthority,
	///Too late method.
	TooLateMethod,
	///Too late path.
	TooLatePath,
	///Too late protocol.
	TooLateProtocol,
	///Too late reason.
	TooLateReason,
	///Too late scheme.
	TooLateScheme,
	///Too late status.
	TooLateStatus,
	///Too late version.
	TooLateVersion,
	///Unpresentable header.
	UnrepresentableHeader,
	///Unsupported protocol.
	UnsupportedProtocol,
	///Header banned in HTTP/2.
	HeaderBannedInH2(StandardHttpHeader),
	///Header name too long.
	HeaderNameTooLong([u8;32], u8, usize, usize, usize),
	///Header value too long.
	HeaderValueTooLong([u8;32], u8, usize, usize, usize),
	///Header value too long for standard header.
	HeaderValueTooLongStd(StandardHttpHeader, usize, usize, usize),
	///Internal error.
	InternalError(&'static str),
	///Error in underlying protocol.
	ProtocolError(&'static str),
}

impl Display for HeaderFault
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::HeaderFault::*;
		match *self {
			IllegalAuthority(err) => write!(f, "Illegal authority: {err}"),
			IllegalChunked => write!(f, "Illegal chunked transfer-encoding"),
			IllegalContentLength => write!(f, "Illegal Content-Length"),
			IllegalData => write!(f, "Illegal data before headers or after trailers"),
			IllegalEndOfMessage => write!(f, "Illegal end of message before headers"),
			IllegalHeader => write!(f, "Illegal header after final headers"),
			IllegalHeaderName(byte) => write!(f, "Illegal header name byte {byte}"),
			IllegalHeaderNameEmpty => write!(f, "Illegal empty header name"),
			IllegalHeaderValue(byte) => write!(f, "Illegal header value byte {byte}"),
			IllegalMethod(err) => write!(f, "Illegal method: {err}"),
			IllegalPath(err) => write!(f, "Illegal path: {err}"),
			IllegalQuery(err) => write!(f, "Illegal query: {err}"),
			IllegalScheme(err) => write!(f, "Illegal scheme: {err}"),
			IllegalStatus => write!(f, "Illegal status"),
			IllegalTrailers => write!(f, "Illegal trailers"),
			IllegalUserinfo => write!(f, "Illegal userinfo"),
			InconsistentContentLength => write!(f, "Inconsistent Content-Length"),
			NoAuthority => write!(f, "No authority"),
			MultipleHost => write!(f, "Multiple Host headers"),
			TooLateAuthority => write!(f, "Authority appears too late"),
			TooLateMethod => write!(f, "Method appears too late"),
			TooLatePath => write!(f, "Path appears too late"),
			TooLateProtocol => write!(f, "Protocol appears too late"),
			TooLateReason => write!(f, "Reason appears too late"),
			TooLateScheme => write!(f, "Scheme appears too late"),
			TooLateStatus => write!(f, "Status appears too late"),
			TooLateVersion => write!(f, "Version appears too late"),
			UnrepresentableHeader => write!(f, "Unpresentable header"),
			UnsupportedProtocol => write!(f, "Unsupported protocol"),
			HeaderBannedInH2(name) => write!(f, "Header {name} not allowed in HTTP/2"),
			HeaderNameTooLong(name, namelen, len, unreliable, bound) =>
				length_err(f, &name, namelen, "name", len, unreliable, bound),
			HeaderValueTooLong(value, valuelen, len, unreliable, bound) =>
				length_err(f, &value, valuelen, "value", len, unreliable, bound),
			HeaderValueTooLongStd(name, len, unreliable, bound) =>
				length_err_std(f, name, len, unreliable, bound),
			InternalError(err) => write!(f, "Internal error: {err}"),

			ProtocolError(err) => write!(f, "{err}"),
		}
	}
}

struct PrintUnreliable(usize,usize);

impl Display for PrintUnreliable
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		let amt = self.0;
		let max = self.1;
		if amt >= max { write!(f, "at least {max}") } else { write!(f, "{amt}") }
	}
}

struct PrintName<'a>(&'a [u8], bool);

impl<'a> Display for PrintName<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		for &b in self.0.iter() {
			let c = b as char;
			match b {
				91 => f.write_str("\\["),
				92 => f.write_str("\\\\"),
				93 => f.write_str("\\]"),
				32..=126 => write!(f, "{c}"),
				_ => write!(f, "\\x{b:02x}"),
			}?;
		}
		if self.1 { f.write_str("[...]")?; }
		Ok(())
	}
}

fn length_err_std(f: &mut Formatter, name: StandardHttpHeader, len: usize, unreliable: usize, limit: usize) ->
	Result<(), FmtError>
{
	write!(f, "Header value for [{name}] too long ({len}, limit is {limit})",
		name=name.to_str(), len=PrintUnreliable(len, unreliable))
}


fn length_err(f: &mut Formatter, name: &[u8], namelen: u8, kind: &str, len: usize, unreliable: usize,
	limit: usize) -> Result<(), FmtError>
{
	let (overflow, name) = if namelen as usize > name.len() {
		(true, name)
	} else {
		//namelen <= name.len()
		(false, &name[..namelen as usize])
	};
	write!(f, "Header {kind} for [{name}] too long ({len}, limit is {limit})",
		name=PrintName(name, overflow), len=PrintUnreliable(len, unreliable))
}

fn get_content_length_name(use_std: bool) -> HeaderName<'static>
{
	match use_std {
		true => HeaderName::Standard(StandardHttpHeader::ContentLength),
		false => HeaderName::Custom(b"content-length"),
	}
}

impl HeaderFault
{
	fn _assert_header_valid(val: &[u8]) -> Result<(), HeaderFault>
	{
		if let Some(c) = val.iter().cloned().find(|x|*x==127||(*x<32&&*x!=9)) {
			fail!(HeaderFault::IllegalHeaderValue(c));
		}
		Ok(())
	}
}

#[derive(PartialEq,Debug)]	//Assume this is always properly constructed.
pub(crate) enum UnsafeHeaderName<'l>
{
	Empty,
	Standard(StandardHttpHeader),
	Custom(&'l [u8], bool),
}

impl<'l> UnsafeHeaderName<'l>
{
	pub const fn standard(x: StandardHttpHeader) -> UnsafeHeaderName<'static> { UnsafeHeaderName::Standard(x) }
	pub fn new<'m>(x: &'m [u8]) -> UnsafeHeaderName<'m>
	{
		if x.len() == 0 { return UnsafeHeaderName::Empty; }
		if let Some(x) = StandardHttpHeader::from(x) { return UnsafeHeaderName::Standard(x); }
		UnsafeHeaderName::Custom(x, false)
	}
	pub fn new_unsafe<'m>(x: &'m [u8]) -> UnsafeHeaderName<'m>
	{
		//This has already been checked to be valid name, which impiles it is valid UTF-8.
		let xu = unsafe{from_utf8_unchecked(x)};
		if let Some(x) = StandardHttpHeader::from_str(xu) { return UnsafeHeaderName::Standard(x); }
		UnsafeHeaderName::Custom(x, true)
	}
	pub fn as_standard(&self) -> Option<StandardHttpHeader>
	{
		if let &UnsafeHeaderName::Standard(x) = self { Some(x) } else { None }
	}
	pub(crate) fn as_raw(&self) -> &'l [u8]
	{
		match self {
			&UnsafeHeaderName::Empty => b"",
			&UnsafeHeaderName::Standard(s) => s.to_str().as_bytes(),
			&UnsafeHeaderName::Custom(name,_) => name,
		}
	}
	fn safe_header_name(self, use_std: bool) -> Result<HeaderName<'l>, HeaderFault>
	{
		match self {
			//Empty is always bad.
			UnsafeHeaderName::Empty => fail!(HeaderFault::IllegalHeaderNameEmpty),
			UnsafeHeaderName::Standard(s) => {
				//HTTP/2 magics are not safe, no matter the unsafe flag. Anything else is always
				//safe tho.
				fail_if!(s.is_http2_magic(), HeaderFault::IllegalHeaderName(b':'));
				//If !use_std, downgrade standard to custom.
				let s = match use_std {
					true => HeaderName::Standard(s),
					false => HeaderName::Custom(s.to_str().as_bytes()),
				};
				Ok(s)
			},
			UnsafeHeaderName::Custom(name, unsafe_flag) => {
				if !unsafe_flag {
					if let Some(c) = name.iter().cloned().find(|c|!is_tchar_hdr(*c)) {
						fail!(HeaderFault::IllegalHeaderName(c));
					}
					fail_if!(name.len() == 0, HeaderFault::IllegalHeaderNameEmpty);
				}
				Ok(HeaderName::Custom(name))
			}
		}
	}
}

///A potentially unchecked header value.
pub(crate) struct UnsafeHeaderValue<'l>(&'l mut [u8], bool);

impl<'l> UnsafeHeaderValue<'l>
{
	///Create a new unchecked header value.
	pub fn new<'m>(x: &'m mut [u8]) -> UnsafeHeaderValue<'m> { UnsafeHeaderValue(x, false) }
	///Unsafely create a new unchecked header value. This warrants that the string does not contain any bytes
	///0-8, 10-31 nor 127.
	pub fn new_unsafe<'m>(x: &'m mut [u8]) -> UnsafeHeaderValue<'m> { UnsafeHeaderValue(x, true) }
	///Transform to UnsafeHeaderValueC.
	pub fn to_const(self) -> UnsafeHeaderValueC<'l> { UnsafeHeaderValueC(self.0, self.1) }
	fn safe_generic(self) -> Result<&'l mut [u8], HeaderFault>
	{
		//If unsafe is set, assume value is generic-safe (avoid double-checking).
		if !self.1 { HeaderFault::_assert_header_valid(self.0)? }
		Ok(self.0)
	}
	fn safe_scheme(self) -> Result<&'l mut [u8], HeaderFault>
	{
		//Always check, because the flag is generic-safe, not specific-safe.
		is_valid_scheme(self.0).map_err(|e|HeaderFault::IllegalScheme(e))?;
		self.0.make_ascii_lowercase();
		Ok(self.0)
	}
}

///A potentially unchecked header value.
pub(crate) struct UnsafeHeaderValueC<'l>(&'l [u8], bool);

impl<'l> UnsafeHeaderValueC<'l>
{
	///Unsafely create a new unchecked header value. This warrants that the string does not contain any bytes
	///0-8, 10-31 nor 127.
	pub fn new_unsafe<'m>(x: &'m [u8]) -> UnsafeHeaderValueC<'m> { UnsafeHeaderValueC(x, true) }
	///Create a new unchecked header value from static string. The string must not contain any control
	///characters.
	pub fn fixed<'m>(s: &'static str) -> UnsafeHeaderValueC<'m> { UnsafeHeaderValueC(s.as_bytes(), true) }
	fn safe_generic(self) -> Result<&'l [u8], HeaderFault>
	{
		//If unsafe is set, assume value is generic-safe (avoid double-checking).
		if !self.1 { HeaderFault::_assert_header_valid(self.0)? }
		Ok(self.0)
	}
	fn safe_method(self) -> Result<&'l [u8], HeaderFault>
	{
		//Always check, because the flag is generic-safe, not specific-safe.
		if let Some((pos, illegal)) = self.0.iter().cloned().enumerate().find(|&(_, c)|!is_tchar(c)) {
			fail!(HeaderFault::IllegalMethod(PartialDecodeError::Illegal2(illegal, pos)));
		}
		Ok(self.0)
	}
	fn safe_status(self) -> Result<&'l [u8], HeaderFault>
	{
		//Always check, because the flag is generic-safe, not specific-safe.
		if self.0.len() != 3 || !self.0.iter().all(u8::is_ascii_digit) || self.0[0].wrapping_sub(49) > 5 {
			fail!(HeaderFault::IllegalStatus);
		}
		Ok(self.0)
	}
}


const MAX_HOST: usize = 261;	//dnsname(255) + ':' + port(5)

//const FLAG_ERROR: u16 = 1;		//Stream is in error state, should generate no more events.
const FLAG_REQUEST: u16 = 2;		//This is server side.
const FLAG_NO_CHUNKING: u16 = 4;	//Chunking not allowed.
const FLAG_NO_BODY: u16 = 8;		//Has no body.
const FLAG_CHUNKED: u16 = 16;		//Is chunked.
const FLAG_NONCHUNK_TE: u16 = 32;	//Has non-chunk T-E.
const FLAG_HAS_TE: u16 = 64;		//Has any T-E.
const FLAG_LENGTH_ERR: u16 = 128;	//Length error.
const FLAG_HEADERS_END: u16 = 256;	//Headers have ended.
const FLAG_SEEN_TRAILER: u16 = 512;	//Trailer has been seen.
const FLAG_END_STREAM: u16 = 1024;	//End stream has been seen.
const FLAG_UPGRADED: u16 = 2048;	//Upgrade performed.
const FLAG_HAS_UPGRADE: u16 = 4096;	//Upgrade header present.

///State of stream.
///
///This structure records the state of the stream between many invocations of pushing data. It is only
///intended to be passed to functions that manipulate streams.
#[derive(Copy)]
pub(crate) struct StreamState
{
	flags: u16,
	herror: Option<HeaderFault>,
}

impl Clone for StreamState
{
	fn clone(&self) -> Self { *self }
}

///Stream type.
#[derive(Copy,Clone,PartialEq,Eq,Debug)]
pub enum StreamType
{
	///HTTP/1.x request.
	Http1Request,
	///HTTP/1.x reply (not HEAD).
	Http1Reply,
	///HTTP/1.x HEAD reply.
	Http1Head,
	///HTTP/2 request.
	Http2Request,
	///HTTP/2 reply.
	Http2Reply,
}

#[doc(hidden)]
pub enum StreamMode
{
	Http1,
	Http1Head,
	Http2
}

impl StreamState
{
	///Create a new stream state with specified type.
	pub fn new2(stype: StreamType) -> StreamState
	{
		let flags = match stype {
			StreamType::Http1Request => FLAG_REQUEST,
			StreamType::Http1Reply => 0,
			StreamType::Http1Head => FLAG_NO_BODY,
			StreamType::Http2Request => FLAG_REQUEST | FLAG_NO_CHUNKING,
			StreamType::Http2Reply => FLAG_NO_CHUNKING,
		};
		StreamState {
			flags: flags,
			herror: None,
		}
	}
	#[doc(hidden)] pub fn set_eos(&mut self) { self.flags |= FLAG_END_STREAM; }
	///Check if main headers have already been received.
	pub fn main_headers_received(&self) -> bool { self.flags & FLAG_HEADERS_END != 0 }
}

///Kind of authority.
#[derive(Copy,Clone,PartialEq,Eq,Debug)]
pub(crate) enum AuthorityKind
{
	///Host header.
	HostHeader,
	///HTTP/1.x absolute form.
	Http1Absolute,
	///HTTP/2 :authority.
	Http2Authority,
}

///State of header block.
///
///This structure records the state of the header block between many invocations of pushing data. It is only
///intended to be passed to functions that manipulate header blocks.
#[derive(Copy)]
pub(crate) struct HeaderBlockState
{
	length: Option<u64>,
	host: [u8;MAX_HOST],
	hostlen: u16,
	has_host_header: bool,			//The message has host header.
	authority_ok: bool,			//The message has authority specified.
	authority_sent: bool,
	have_emitted_normal_headers: bool,
	statuscode: u16,
}

impl Clone for HeaderBlockState
{
	fn clone(&self) -> Self { *self }
}


impl HeaderBlockState
{
	///Create a new header block state.
	pub fn new() -> HeaderBlockState
	{
		HeaderBlockState {
			length: None,
			host: [0;MAX_HOST],
			hostlen: 0,
			has_host_header: false,
			authority_ok: false,
			authority_sent: false,
			have_emitted_normal_headers: false,
			statuscode: 0,
		}
	}
}

///The transition stream makes after header block is complete.
pub(crate) enum StreamTransition
{
	///Upgrade, transition to raw read.
	Upgrade,
	///Interrim header complete. Transition back to read response headers.
	InterimComplete,
	///Body continues until close. Transition to read body until close is received.
	BodyToClose,
	///Next message starts now. Transition to wait for next message.
	NextMessage,
	///Fixed-length message. Read that many bytes, then transition to wait for the next message.
	FixedLength(u64),
	///Chunked. Read chunked blocks, trailers and then wait for the next request.
	AsChunked,
	///Hard error. Send bad request if possible and close the connection.
	HardError(&'static str),
	///Hard error. Send bad request if possible and close the connection.
	HardErrorF(HeaderFault),
	///Hard error. Send bad request if possible and close the connection. Used for missing authority.
	HardErrorA,
	///End of middlers block.
	Middlers,
}

///Header name.
pub(crate) enum HeaderName<'a>
{
	Standard(StandardHttpHeader),
	Custom(&'a [u8]),
}

///Element Dispatcher
pub(crate) trait ElementDispatch
{
	///Generate abort
	fn abort(&mut self, cause: StreamAbort);
	///Generate authority.
	fn authority(&mut self, x: &[u8]);
	///Generate content-length
	fn content_length(&mut self, x: u64);
	///Generate path.
	fn path(&mut self, x: &[u8]);
	///Generate query.
	fn query(&mut self, x: &[u8]);
	///Generate scheme.
	fn scheme(&mut self, x: &[u8]);
	///Generate protocol.
	fn protocol(&mut self, x: &[u8]);
	///Generate method.
	fn method(&mut self, x: &[u8]);
	///Generate version.
	fn version(&mut self, x: &[u8]);
	///Generate reason.
	fn reason(&mut self, x: &[u8]);
	///Generate status.
	fn status(&mut self, x: u16);
	///Generate trailer.
	fn trailer(&mut self, n: HeaderName, v: &[u8]);
	///Generate middler.
	fn middler(&mut self, _n: HeaderName, _v: &[u8]) {}
	///Generate end of middlers.
	fn end_of_middlers(&mut self) {}
	///Generate header.
	fn header(&mut self, n: HeaderName, v: &mut [u8]);
	///Protocol supported query.
	fn protocol_supported(&self, x: &[u8]) -> bool;
	///Generate data.
	fn data(&mut self, x: &[u8]);
	///Generate end of message.
	fn end_of_message(&mut self);
	///Generate end of headers.
	fn end_of_headers(&mut self);
	///Generate end of interim headers.
	fn end_of_headers_interim(&mut self);
	///Generate message start
	fn start_of_message(&mut self);
	///Generate stream reap.
	fn reap_stream(&mut self, id: u32);
	///Generate raw authority.
	fn authority_raw(&mut self, raw_auth: &[u8]);
	///Generate raw path.
	fn path_raw(&mut self, raw_path: &[u8]);
	///Trace header fragment.
	fn trace_header_fragment(&mut self, _fragment: &[u8], _soh: bool, _eoh: bool) {}
	///Trace headers.
	fn trace_header(&mut self, _name: &[u8], _value: &[u8]) {}
}

fn call_if_not_error(ss: &mut StreamState, func: impl FnOnce() -> ()) { if ss.herror.is_none() { func() } }

fn with_header_fault<D:ElementDispatch>(ss: &mut StreamState, d: &mut D,
	func: impl FnOnce(&mut StreamState, &mut D) -> Result<(), HeaderFault>)
{
	if ss.herror.is_some() { return; }
	if let Err(err) = func(ss, d) {
		call_if_not_error(ss, ||d.abort(StreamAbort::HeaderError(err)));
		ss.herror = Some(err);
	}
}

#[doc(hidden)]
pub(crate) struct HttpUtils;

impl HttpUtils
{
	fn _authority(d: &mut impl ElementDispatch, state: &mut HeaderBlockState,
		sstate: &mut StreamState, a: UnsafeHeaderValue, kind: AuthorityKind) -> Result<(), HeaderFault>
	{
		use HeaderFault as HF;
		use PartialDecodeError as PDE;
		use AuthorityKind as AK;
		let a = a.safe_generic()?;
		//These headers are not allowed in middlers/trailers.
		fail_if!(sstate.flags & FLAG_HEADERS_END != 0, HF::IllegalHeader);
		//Do not enforce too late on authority if it has already been sent.
		fail_if!(state.have_emitted_normal_headers && !state.authority_sent, HF::TooLateAuthority);
		//Check that there is no userinfo.
		fail_if!(a.contains(&b'@'), HF::IllegalUserinfo);
		//There can be at most one Host.
		fail_if!(state.has_host_header && kind == AK::HostHeader, HF::MultipleHost);
		//Authority requirements can be met by either Http2Authority or HostHeader, but NOT Http1Absolute.
		state.authority_ok |= matches!(kind, AK::Http2Authority|AK::HostHeader);
		state.has_host_header |= kind == AK::HostHeader;
		//If there is no authority sent yet, send it. This can be always done because in HTTP/1.x, this is
		//either Http1Absolute, in which case we will check host for consistency, or HostHeader, in which
		//case there is nothing further. In HTTP/2, Http2Authority may have HostHeader following, which is
		//checked for consistency, or HostHeader , in which case there is nothing further.
		if !state.authority_sent { call_if_not_error(sstate, ||d.authority_raw(a)); }
		//Translate the name already, as we want to store translated name.
		let a = partial_decode_authority(a).map_err(HF::IllegalAuthority)?;
		//Emit the translated headers, if not done already. See the above comment.
		if !state.authority_sent {
			fail_if!(state.have_emitted_normal_headers, HF::TooLateAuthority);
			copy_if_fits(&mut state.host, a).ok_or(HF::IllegalAuthority(PDE::TooLong))?;
			state.hostlen = a.len() as u16;
			state.authority_sent = true;
			//:authority is always a header.
			call_if_not_error(sstate, ||d.authority(a));
		} else {
			//If authority has been sent, verify that new authority is either consistent or empty.
			let old_host = state.host.get(..state.hostlen as usize).
				ok_or(HF::IllegalAuthority(PDE::TooLong))?;
			fail_if!(a.len() > 0 && a != old_host, HF::IllegalAuthority(PDE::Inconsistent));
		}
		Ok(())
	}
	#[doc(hidden)]
	pub fn authority<D:ElementDispatch>(d: &mut D, state: &mut HeaderBlockState,
		sstate: &mut StreamState, a: UnsafeHeaderValue, kind: AuthorityKind)
	{
		with_header_fault(sstate, d, |sstate,d|Self::_authority(d, state, sstate, a, kind))
	}
	#[doc(hidden)]
	pub fn path<D:ElementDispatch>(d: &mut D, state: &mut HeaderBlockState,
		sstate: &mut StreamState, p: UnsafeHeaderValue)
	{
		with_header_fault(sstate, d, |sstate,d|{
			let p = p.safe_generic()?;
			fail_if!(sstate.flags & FLAG_HEADERS_END != 0, HeaderFault::IllegalHeader);
			fail_if!(state.have_emitted_normal_headers, HeaderFault::TooLatePath);
			//Do not split path and query in raw version.
			call_if_not_error(sstate, ||d.path_raw(p));
			let (path, query) = match split_attach_first(p, b"?", Attachment::Right) {
				Ok((p,q)) => (p, Some(q)),
				Err(p) => (p, None)
			};
			//Path must either be * or start with /. This holds before partial unescaping.
			match path.get(0) {
				Some(&b'*') => if path.len() > 1 {
					fail!(HeaderFault::IllegalPath(PartialDecodeError::Illegal2(path[1],1)));
				} else if query.is_some() {
					fail!(HeaderFault::IllegalPath(PartialDecodeError::Illegal2(b'?',1)));
				},
				Some(b'/') => (),
				Some(x) => fail!(HeaderFault::IllegalPath(PartialDecodeError::Illegal2(*x,0))),
				None => fail!(HeaderFault::IllegalPath(PartialDecodeError::Empty))
			};
			//Partially decode path and query before passing those. These two are always headers.
			let path = partial_decode_path(path).map_err(HeaderFault::IllegalPath)?;
			call_if_not_error(sstate, ||d.path(path));
			if let Some(query) = query {
				let query = partial_decode_query(query).map_err(HeaderFault::IllegalQuery)?;
				call_if_not_error(sstate, ||d.query(query));
			}
			Ok(())
		})
	}
	#[doc(hidden)]
	pub fn path_asterisk<D:ElementDispatch>(d: &mut D, state: &mut HeaderBlockState,
		sstate: &mut StreamState)
	{
		with_header_fault(sstate, d, |sstate,d|{
			fail_if!(sstate.flags & FLAG_HEADERS_END != 0, HeaderFault::IllegalHeader);
			fail_if!(state.have_emitted_normal_headers, HeaderFault::TooLatePath);
			//Inject fake raw path.
			call_if_not_error(sstate, ||d.path_raw(b"*"));
			call_if_not_error(sstate, ||d.path(b"*"));
			Ok(())
		})
	}
	#[doc(hidden)]
	pub fn path_root<D:ElementDispatch>(d: &mut D, state: &mut HeaderBlockState,
		sstate: &mut StreamState)
	{
		with_header_fault(sstate, d, |sstate,d|{
			fail_if!(sstate.flags & FLAG_HEADERS_END != 0, HeaderFault::IllegalHeader);
			fail_if!(state.have_emitted_normal_headers, HeaderFault::TooLatePath);
			//Inject fake raw path.
			call_if_not_error(sstate, ||d.path_raw(b"/"));
			call_if_not_error(sstate, ||d.path(b"/"));
			Ok(())
		})
	}
	#[doc(hidden)]
	pub fn scheme<D:ElementDispatch>(d: &mut D, state: &mut HeaderBlockState,
		sstate: &mut StreamState, s: UnsafeHeaderValue)
	{
		with_header_fault(sstate, d, |sstate,d|{
			fail_if!(sstate.flags & FLAG_HEADERS_END != 0, HeaderFault::IllegalHeader);
			let s = s.safe_scheme()?;
			fail_if!(state.have_emitted_normal_headers, HeaderFault::TooLateScheme);
			call_if_not_error(sstate, ||d.scheme(s));
			Ok(())
		})
	}
	#[doc(hidden)]
	pub fn scheme_default<D:ElementDispatch>(d: &mut D, state: &mut HeaderBlockState,
		sstate: &mut StreamState, s: &[u8])
	{
		with_header_fault(sstate, d, |sstate,d|{
			fail_if!(sstate.flags & FLAG_HEADERS_END != 0, HeaderFault::IllegalHeader);
			fail_if!(state.have_emitted_normal_headers, HeaderFault::TooLateScheme);
			call_if_not_error(sstate, ||d.scheme(s));
			Ok(())
		})
	}
	#[doc(hidden)]
	pub fn method<D:ElementDispatch>(d: &mut D, state: &mut HeaderBlockState,
		sstate: &mut StreamState, m: UnsafeHeaderValueC)
	{
		with_header_fault(sstate, d, |sstate,d|{
			fail_if!(sstate.flags & FLAG_HEADERS_END != 0, HeaderFault::IllegalHeader);
			let m = m.safe_method()?;
			fail_if!(state.have_emitted_normal_headers, HeaderFault::TooLateMethod);
			call_if_not_error(sstate, ||d.method(m));
			Ok(())
		})
	}
	#[doc(hidden)]
	pub fn version<D:ElementDispatch>(d: &mut D, state: &mut HeaderBlockState,
		sstate: &mut StreamState, v: UnsafeHeaderValueC)
	{
		with_header_fault(sstate, d, |sstate,d|{
			let v = v.safe_generic()?;
			fail_if!(sstate.flags & FLAG_HEADERS_END != 0, HeaderFault::IllegalHeader);
			fail_if!(state.have_emitted_normal_headers, HeaderFault::TooLateVersion);
			call_if_not_error(sstate, ||d.version(v));
			Ok(())
		})
	}
	#[doc(hidden)]
	pub fn reason<D:ElementDispatch>(d: &mut D, state: &mut HeaderBlockState,
		sstate: &mut StreamState, r: UnsafeHeaderValueC)
	{
		with_header_fault(sstate, d, |sstate,d|{
			let r = r.safe_generic()?;
			fail_if!(sstate.flags & FLAG_HEADERS_END != 0, HeaderFault::IllegalHeader);
			fail_if!(state.have_emitted_normal_headers, HeaderFault::TooLateReason);
			call_if_not_error(sstate, ||d.reason(r));
			Ok(())
		})
	}
	#[doc(hidden)]
	pub fn status<D:ElementDispatch>(d: &mut D, state: &mut HeaderBlockState,
		sstate: &mut StreamState, s: UnsafeHeaderValueC)
	{
		with_header_fault(sstate, d, |sstate,d|{
			fail_if!(sstate.flags & FLAG_HEADERS_END != 0, HeaderFault::IllegalHeader);
			fail_if!(state.have_emitted_normal_headers, HeaderFault::TooLateStatus);
			let s = s.safe_status()?;
			state.statuscode = from_utf8(s).ok().and_then(|s|u16::from_str(s).ok()).
				ok_or(HeaderFault::IllegalStatus)?;
			call_if_not_error(sstate, ||d.status(state.statuscode));
			Ok(())
		})
	}
	#[doc(hidden)]
	pub fn header<D:ElementDispatch>(d: &mut D, state: &mut HeaderBlockState,
		sstate: &mut StreamState, n: UnsafeHeaderName, v: UnsafeHeaderValue, noupgrade: bool, use_std: bool)
	{
		with_header_fault(sstate, d, |sstate,d|{
			let ns = n.as_standard();
			let nd = n.safe_header_name(use_std)?;
			let v = v.safe_generic()?;
			if sstate.flags & FLAG_HEADERS_END != 0 {
				//If FLAG_NO_CHUNKING is set, but FLAG_END_STREAM is not, these are actually
				//middlers. Forward those.
				if sstate.flags & (FLAG_NO_CHUNKING|FLAG_END_STREAM) == FLAG_NO_CHUNKING {
					call_if_not_error(sstate, ||d.middler(nd, v));
				} else {
					//These are trailers.
					sstate.flags |= FLAG_SEEN_TRAILER;
					call_if_not_error(sstate, ||d.trailer(nd, v));
				}
			} else {
				//Handle Content-Length specially.
				if ns == Some(StandardHttpHeader::ContentLength) {
					//Content-length is illegal with TE. Overrule CL with TE for error handling.
					fail_if!(sstate.flags & FLAG_HAS_TE != 0, HeaderFault::IllegalContentLength);
					let clen = from_utf8(v).ok().and_then(|line|u64::from_str(line).
						ok()).ok_or(HeaderFault::IllegalContentLength)?;
					if let Some(clen2) = state.length {
						if clen != clen2 { sstate.flags |= FLAG_LENGTH_ERR; }
					}
					state.length = Some(clen);
					return Ok(());
				}
				//Handle Transfer-Encoding specially.
				if ns == Some(StandardHttpHeader::TransferEncoding) {
					sstate.flags |= FLAG_HAS_TE;
					//We can not raise these errors midway.
					let mut illchunk = false;
					for coding in v.split(|x|*x==b',') {
						let is_chunked = trim_ascii(coding) == b"chunked";
						illchunk |= sstate.flags & FLAG_CHUNKED != 0;
						//Chunked if present must be the last.
						if is_chunked {
							fail_if!(sstate.flags & FLAG_NO_CHUNKING != 0,
								HeaderFault::IllegalChunked);
							sstate.flags |= FLAG_CHUNKED;
						} else {
							sstate.flags |= FLAG_NONCHUNK_TE;
						}
					}
					fail_if!(illchunk, HeaderFault::IllegalChunked);
					//Content-length is illegal with TE. However, still process the TE, because
					//we need that to not crash.
					fail_if!(state.length.is_some(), HeaderFault::IllegalContentLength);
					//This is transferred through.
				}
				//Handle upgrade specially.
				if ns == Some(StandardHttpHeader::Upgrade) && !noupgrade {
					sstate.flags |= FLAG_HAS_UPGRADE;
					for protocol in v.split(|x|*x==b',') {
						let protocol = trim_ascii(protocol);
						if d.protocol_supported(protocol) {
							call_if_not_error(sstate, ||d.protocol(protocol));
							sstate.flags |= FLAG_UPGRADED;
						}
					}
					return Ok(());
				}
				//Host is magic.
				if sstate.flags & FLAG_REQUEST != 0 && ns == Some(StandardHttpHeader::Host) {
					//This is the host header, so not special. v has already been
					//UnsafeHeaderValue, so can use new_unsafe.
					let v = UnsafeHeaderValue::new_unsafe(v);
					return Self::_authority(d, state, sstate, v, AuthorityKind::HostHeader);
				}
				state.have_emitted_normal_headers = true;
				call_if_not_error(sstate, ||d.header(nd, v));
			}
			Ok(())
		})
	}
	#[doc(hidden)]
	pub fn header_fault<D:ElementDispatch>(d: &mut D, sstate: &mut StreamState, fault: HeaderFault)
	{
		with_header_fault(sstate, d, |_,_|fail!(fault));
	}
	#[doc(hidden)]
	pub fn protocol<D:ElementDispatch>(d: &mut D, state: &mut HeaderBlockState,
		sstate: &mut StreamState, p: UnsafeHeaderValue) -> Result<(), StreamAbort>
	{
		let mut abort = Ok(());
		with_header_fault(sstate, d, |sstate,d|{
			let p = p.safe_generic()?;
			fail_if!(sstate.flags & FLAG_HEADERS_END != 0, HeaderFault::IllegalHeader);
			fail_if!(state.have_emitted_normal_headers, HeaderFault::TooLateProtocol);
			if d.protocol_supported(p) {
				call_if_not_error(sstate, ||d.protocol(p));
				sstate.flags |= FLAG_UPGRADED;
			} else {
				let acause = StreamAbort::UnsupportedProtocol;
				call_if_not_error(sstate, ||d.abort(acause));
				sstate.herror = Some(HeaderFault::UnsupportedProtocol);
				abort = Err(acause);
			}
			Ok(())
		});
		abort
	}
	#[doc(hidden)]
	pub fn data<D:ElementDispatch>(d: &mut D, sstate: &mut StreamState, payload: &[u8])
	{
		with_header_fault(sstate, d, |sstate,d|{
			fail_if!(sstate.flags & (FLAG_HEADERS_END|FLAG_SEEN_TRAILER|FLAG_END_STREAM) !=
				FLAG_HEADERS_END,HeaderFault::IllegalData);
			call_if_not_error(sstate, ||d.data(payload));
			Ok(())
		});
	}
	#[doc(hidden)]
	pub fn abort<D:ElementDispatch>(d: &mut D, sstate: &mut StreamState, acause: StreamAbort) ->
		Result<(), StreamAbort>
	{
		call_if_not_error(sstate, ||d.abort(acause));
		Err(acause)
	}
	#[doc(hidden)]
	pub fn end_of_message<D:ElementDispatch>(d: &mut D, sstate: &mut StreamState)
	{
		with_header_fault(sstate, d, |sstate,d|{
			fail_if!(sstate.flags & FLAG_HEADERS_END == 0, HeaderFault::IllegalEndOfMessage);
			call_if_not_error(sstate, ||d.end_of_message());
			Ok(())
		});
	}
	#[doc(hidden)]
	pub fn end_headers<D:ElementDispatch>(d: &mut D, state: &mut HeaderBlockState,
		sstate: &mut StreamState, use_std: bool) -> StreamTransition
	{
		if let Some(err) = sstate.herror { return StreamTransition::HardErrorF(err); }
		//If decoding trailers, this is always end of message.
		if sstate.flags & FLAG_HEADERS_END == FLAG_HEADERS_END {
			//If FLAG_NONCHUNK_TE & !FLAG_END_STREAM, then these are middlers. Notify of the end of
			//middler block.
			if sstate.flags & (FLAG_NO_CHUNKING|FLAG_END_STREAM) == FLAG_NO_CHUNKING {
				call_if_not_error(sstate, ||d.end_of_middlers());
				return StreamTransition::Middlers;
			} else {
				call_if_not_error(sstate, ||d.end_of_message());
				return StreamTransition::NextMessage;
			}
		}
		//This processing happens even in error.
		//If length is fixed, include content-length.
		if let (true, Some(mut len)) = (sstate.flags & FLAG_LENGTH_ERR == 0, state.length) {
			let olen = len;
			let mut v = [0;32];
			let mut zero = [48];
			let mut voff = v.len();
			//This loop can execute maximum of 20 times.
			while len > 0 {
				voff = f_break!(voff.checked_sub(1));
				f_break!(v.get_mut(voff).map(|t|*t = 48 + (len % 10) as u8));
				len /= 10;
			}
			let v = v.get_mut(voff..).unwrap_or(&mut zero[..]);
			call_if_not_error(sstate, ||d.content_length(olen));
			call_if_not_error(sstate, ||d.header(get_content_length_name(use_std), v));
		}
		//Only set headers end flag if this is not non-101 1xx response. Interim headers do not set this.
		//This does not matter if end of message gets sent.
		if state.statuscode / 100 != 1 || state.statuscode == 101 {
			sstate.flags |= FLAG_HEADERS_END;
			if sstate.flags & FLAG_REQUEST != 0 && !state.authority_ok {
				call_if_not_error(sstate,
					||d.abort(StreamAbort::HeaderError(HeaderFault::NoAuthority)));
				sstate.herror = Some(HeaderFault::NoAuthority);
				return StreamTransition::HardErrorA;
			}
		}
		if sstate.flags & FLAG_END_STREAM != 0 {
			//If end of stream is set, this is always end of message. This is used for HTTP/2, in case
			//EOS is set on header block.
			call_if_not_error(sstate, ||d.end_of_message());
			StreamTransition::NextMessage
		} else if sstate.flags & (FLAG_HAS_UPGRADE|FLAG_UPGRADED) == FLAG_HAS_UPGRADE {
			//Upgrade being present but no upgraded protocol. This is VERY bad, as the frame lock
			//has been lost.
			let err = HeaderFault::UnsupportedProtocol;
			call_if_not_error(sstate, ||d.abort(StreamAbort::HeaderError(err)));
			sstate.herror = Some(err);
			StreamTransition::HardErrorF(err)
		} else if sstate.flags & FLAG_UPGRADED != 0 {
			//If FLAG_UPGRADED is set, this is an request upgrade.
			call_if_not_error(sstate, ||d.end_of_headers());
			StreamTransition::Upgrade
		} else if state.statuscode == 101 {
			//If status code is 101, emit end of headers, for transition to reading
			//raw body.
			call_if_not_error(sstate, ||d.end_of_headers());
			StreamTransition::Upgrade
		} else if state.statuscode / 100 == 1 {
			//If status code is 1xx, emit end of interim headers.
			call_if_not_error(sstate, ||d.end_of_headers_interim());
			StreamTransition::InterimComplete
		} else if sstate.flags & FLAG_NO_CHUNKING != 0 {
			//If no chunking is set, emit end of headers and move to body to close. This is used for
			//normal HTTP/2 streams.
			call_if_not_error(sstate, ||d.end_of_headers());
			StreamTransition::BodyToClose
		} else if sstate.flags & FLAG_NO_BODY != 0 || state.statuscode == 204 || state.statuscode == 304 {
			//If this is marked as no-body or status is 204 or 304, emit end of message.
			call_if_not_error(sstate, ||d.end_of_message());
			StreamTransition::NextMessage
		} else if sstate.flags & FLAG_CHUNKED != 0 {
			//If this is marked as chunked, emit end of headers and move to chunked.
			call_if_not_error(sstate, ||d.end_of_headers());
			StreamTransition::AsChunked
		} else if sstate.flags & FLAG_HAS_TE != 0 {
			//If this has transfer-encoding but not chunked, emit end of headers and move to
			//body to close.
			call_if_not_error(sstate, ||d.end_of_headers());
			StreamTransition::BodyToClose
		} else if sstate.flags & FLAG_LENGTH_ERR != 0 {
			//If bad_length is set, that is an error.
			call_if_not_error(sstate, ||d.abort(
				StreamAbort::ProtocolError("Inconsistent Content-Length")));
			sstate.herror = Some(HeaderFault::InconsistentContentLength);
			StreamTransition::HardError("Inconsistent Content-Length")
		} else if let Some(len) = state.length {
			//If fixed length, that is the length.
			//0 length is special, transition to next message immediately.
			if len == 0 {
				call_if_not_error(sstate, ||d.end_of_message());
				StreamTransition::NextMessage
			} else {
				call_if_not_error(sstate, ||d.end_of_headers());
				StreamTransition::FixedLength(len)
			}
		} else if sstate.flags & FLAG_REQUEST != 0 {
			//If request, this has no body.
			call_if_not_error(sstate, ||d.end_of_message());
			StreamTransition::NextMessage
		} else {
			//To end of connection.
			call_if_not_error(sstate, ||d.end_of_headers());
			StreamTransition::BodyToClose
		}
	}
}

///Encode HTTP headers.
pub trait EncodeHeaders
{
	///Encode request line.
	fn request_line<S:DSink>(output: &mut S, method: &[u8], scheme: &[u8], authority: &[u8], path: &[u8],
		query: &[u8]) -> Result<(), ()>;
	///Encode status-line.
	fn status_line<S:DSink>(output: &mut S, status: u16, reason: &[u8]) -> Result<(), ()>;
	///Encode a header.
	fn header<S:DSink>(output: &mut S, name: &[u8], value: &[u8]) -> Result<(), ()>;
	///Encode a standard header.
	fn header_std<S:DSink>(output: &mut S, name: StandardHttpHeader, value: &[u8]) -> Result<(), ()>
	{
		Self::header(output, name.to_str().as_bytes(), value)
	}
	///Encode end of block.
	fn end<S:DSink>(output: &mut S) -> Result<(), ()>;
	///Is H2?
	fn is_h2() -> bool;
}

///Check if given header is dangerous, and should be filtered out from requests.
pub fn is_dangerous_header_request(name: &[u8]) -> bool
{
	name.starts_with(b"x-forwarded-") ||
		StandardHttpHeader::from(name).map(|x|x.is_dangerous_request()).unwrap_or(false)
}

///Check if given header is dangerous, and should be filtered out from responses.
pub fn is_dangerous_header_response(name: &[u8]) -> bool
{
	StandardHttpHeader::from(name).map(|x|x.is_dangerous_response()).unwrap_or(false)
}

#[test]
fn test_is_valid_scheme()
{
	assert!(is_valid_scheme(b"http").is_ok());
	assert!(is_valid_scheme(b"https").is_ok());
	assert!(is_valid_scheme(b"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-.").is_ok());
	assert!(is_valid_scheme(b"1A").is_err());
	assert!(is_valid_scheme(b"+A").is_err());
	assert!(is_valid_scheme(b"-A").is_err());
	assert!(is_valid_scheme(b".A").is_err());
	assert!(is_valid_scheme(b"A_").is_err());
	assert!(is_valid_scheme(b"A@").is_err());
	assert!(is_valid_scheme(b"A[").is_err());
	assert!(is_valid_scheme(b"A`").is_err());
	assert!(is_valid_scheme(b"A{").is_err());
	assert!(is_valid_scheme(b"A/").is_err());
	assert!(is_valid_scheme(b"A:").is_err());
}

#[cfg(test)]
fn test_charset<F>(func: F, set: &[u8]) where F: Fn(u8) -> bool
{
	let mut itr = 0;
	for i in 0..255 {
		let ic = unsafe{char::from_u32_unchecked(i as u32)};
		if itr < set.len() && i == set[itr] {
			if i > 31 { println!("'{ic}' should be in set..."); }
			assert!(func(i));
			itr += 1;
		} else {
			if i > 31 { println!("'{ic}' should not be in set...") }
			assert!(!func(i));
		}
	}
}

#[test]
fn test_alpha()
{
	test_charset(is_alpha, b"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
}

#[test]
fn test_schar()
{
	test_charset(is_schar, b"+-.0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
}

#[test]
fn test_pchar()
{
	test_charset(is_pchar, b"!$&'()*+,-./0123456789:;=@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz~");
}

#[test]
fn test_qchar()
{
	test_charset(is_qchar, b"!$&'()*+,-./0123456789:;=?@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz~");
}

#[test]
fn test_tchar()
{
	test_charset(is_tchar, b"!#$%&'*+-.0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ^_`abcdefghijklmnopqrstuvwxyz|~");
}

#[test]
fn test_wchar()
{
	test_charset(is_wchar, b"\t ");
}

#[test]
fn test_xdigit()
{
	test_charset(is_xdigit, b"0123456789ABCDEFabcdef");
}

#[test]
fn test_digit()
{
	test_charset(is_digit, b"0123456789");
}

#[test]
fn test_partial_decode_path_ampersand()
{
	let mut x = *b"foo&%26%ff%62ar";
	let x = partial_decode_path(&mut x).expect("Should succeed");
	assert_eq!(x, b"foo&&%ffbar");
}

#[test]
fn test_partial_decode_query_ampersand()
{
	let mut x = *b"foo&%26%ff%62ar";
	let x = partial_decode_query(&mut x).expect("Should succeed");
	assert_eq!(x, b"foo&%26%ffbar");
}
#[test]
fn test_partial_decode_authority_ampersand()
{
	let mut x = *b"foo&%26%ff%62ar";
	let x = partial_decode_authority(&mut x).expect("Should succeed");
	assert_eq!(x, b"foo&&%ffbar");
}
