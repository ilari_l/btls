use btls_aux_codegen::Bitmask;
use btls_aux_codegen::mkid;
use btls_aux_codegen::mkidf;
use btls_aux_codegen::quote;
use btls_aux_codegen::qwrite;
use std::env;
use std::fs::File;
use std::io::Read;
use std::str::FromStr;
use std::borrow::ToOwned;
use std::path::Path;
use std::collections::BTreeMap;

type _CsSetElem = u8;

struct CiphersuiteData
{
	name: String,
	cname: String,
	hname: String,
	id: u16,
	kex: String,
	protector: String,
	prf: String,
	algo_cbit: u32,
	algo_cbit_nokex: u32,
	is_chacha: bool,
	sw_priority: u32,
	hw_priority: u32,
}

fn read_data_file() -> Vec<CiphersuiteData>
{
	let mut x = Vec::new();
	let mut data = File::open("ciphersuites.dat").expect("missing ciphersuites.dat");
	let mut content = String::new();
	data.read_to_string(&mut content).expect("failed to read ciphersuites.dat");
	let mut chacha_map: BTreeMap<String, bool> = BTreeMap::new();
	let mut priority_map: BTreeMap<String, (u32, u32)> = BTreeMap::new();
	let mut kexprefix_name: BTreeMap<String, String> = BTreeMap::new();
	let mut kexprefix_cname: BTreeMap<String, String> = BTreeMap::new();
	let mut kexprefix_hname: BTreeMap<String, String> = BTreeMap::new();
	let mut csname_cname: BTreeMap<String, String> = BTreeMap::new();
	let mut hashname_hname: BTreeMap<String, String> = BTreeMap::new();
	let mut protname: BTreeMap<String, String> = BTreeMap::new();
	let mut nokex_num: BTreeMap<String, u32> = BTreeMap::new();
	let mut counter = 0;
	let mut counter_nokex = 0;
	for i in content.lines() {
		let i = i.trim();
		let mut i = i.replace('\t', " ");	//Convert tabs into spaces.
		while i.find("  ").is_some() {
			i = i.replace("  ", " ");	//Squash consequtive spaces.
		}
		if i.starts_with("#") || i.len() == 0 { continue; }
		if let Some(i) = i.strip_prefix("cipher ") {
			let parts = i.split(" ").collect::<Vec<_>>();
			let parts: [&str; 3] = parts.as_slice().try_into().
				expect("Expected 4 components for cipher line");
			let [symbol, readable, is_chacha] = parts;
			protname.insert(symbol.to_owned(), readable.to_owned());
			let value = match is_chacha {
				"true" => true,
				"false" => false,
				flag => panic!("Unexpected chacha value {flag}"),
			};
			chacha_map.insert(symbol.to_owned(), value);
		} else if let Some(i) = i.strip_prefix("hash ") {
			let parts = i.split(" ").collect::<Vec<_>>();
			let parts: [&str; 2] = parts.as_slice().try_into().
				expect("Expected 3 components for hash line");
			let [symbol, readable_name] = parts;
			hashname_hname.insert(symbol.to_owned(), readable_name.to_owned());
		} else if let Some(i) = i.strip_prefix("kex ") {
			let parts = i.split(" ").collect::<Vec<_>>();
			let parts: [&str; 4] = parts.as_slice().try_into().
				expect("Expected 5 components for kex line");
			let [symbol, enumprefix, cnamepart, readable] = parts;
			kexprefix_name.insert(symbol.to_owned(), enumprefix.to_owned());
			kexprefix_cname.insert(symbol.to_owned(), cnamepart.to_owned());
			kexprefix_hname.insert(symbol.to_owned(), readable.to_owned());
		} else if let Some(i) = i.strip_prefix("cipherhash ") {
			let parts = i.split(" ").collect::<Vec<_>>();
			let hwpri = match parts.len() {
				4 => parts[3],
				5 => parts[4],
				_ => panic!("Expected 5 or 6 components for cipherhash line"),
			};
			let parts: [&str; 4] = parts[..4].try_into().unwrap();
			let [protection, hash, namepat, swpri] = parts;
			let key = format!("{protection} {hash}");
			let swpri = u32::from_str(swpri).expect("Failed to parse SW priority");
			let hwpri = u32::from_str(hwpri).expect("Failed to parse HW priority");
			priority_map.insert(key.clone(), (swpri, hwpri));
			csname_cname.insert(key.clone(), namepat.to_owned());
		} else if let Some(i) = i.strip_prefix("ciphersuite ") {
			let parts = i.split(" ").collect::<Vec<_>>();
			let parts: [&str; 4] = parts.as_slice().try_into().
				expect("Expected 5 components for ciphersuite line");
			let [id, protection, hash, kex] = parts;
			let id = u16::from_str_radix(id, 16).expect("failed to parse cs id");
			let key = format!("{protection} {hash}");
			let nameprefix = kexprefix_name.get(kex).expect("No kexprefix entry for kex");
			let cnameprefix = kexprefix_cname.get(kex).expect("No kexprefix entry for kex");
			let hnameprefix = kexprefix_hname.get(kex).expect("No kexprefix entry for kex");
			let chacha = chacha_map.get(protection).expect("No chacha entry for protection");
			let priority = priority_map.get(&key).expect("No priority entry for protection/hash");
			let cname = csname_cname.get(&key).expect("No csname entry for protection/hash");
			let hname_h = hashname_hname.get(hash).expect("No hashname entry for hash");
			let hname_p = protname.get(protection).expect("No protname entry for protection");
			let count_nokex = *nokex_num.entry(key.clone()).or_insert_with(||{
				//Assign next free bit.
				let old = counter_nokex;
				counter_nokex += 1;
				old
			});
			x.push(CiphersuiteData {
				name: format!("{nameprefix}{protection}{hash}"),
				cname: format!("{cnameprefix}_{cname}"),
				hname: format!("{hnameprefix}_{hname_p}_{hname_h}"),
				id: id,
				protector: protection.to_owned(),
				prf: hash.to_owned(),
				kex: kex.to_owned(),
				algo_cbit: counter,
				algo_cbit_nokex: count_nokex,
				is_chacha: *chacha,
				sw_priority: priority.0,
				hw_priority: priority.1,
			});
			counter += 1;
		} else {
			panic!("Unknown ciphersuites.dat line '{i}'");
		}
	}
	x
}

fn read_banned_file() -> Vec<u16>
{
	let mut x = Vec::new();
	let mut data = File::open("banned.dat").expect("missing banned.dat");
	let mut content = String::new();
	data.read_to_string(&mut content).expect("failed to read banned.dat");
	for i in content.lines() {
		if i.len() == 0 { continue; }
		let i = if i.len() > 4 { &i[..4] } else { i };
		let ban = u16::from_str_radix(i, 16).expect("failed to parse banned line");
		x.push(ban);
	}
	x
}

fn write_autogenerated_file(fp: &mut File, data: &[CiphersuiteData], banned_cs_list: &[u16])
{
	let random_ciphersuite = mkid(&data[0].name);	//Just some random ciphersuite symbol.
	let num_bad_ciphers = banned_cs_list.len();	//Number of banned ciphers.
	let num_ciphersuites = data.len();		//Number of ciphersuites.
	let mut cs_consts_cs = Vec::new();		//The symbol names of CS_* constants.
	let mut cs_consts_id = Vec::new();		//The values for CS_* constants.
	let mut cs_name = Vec::new();			//The ciphersuite symbols.
	let mut cs_help = Vec::new();			//Help for ciphersuite symbols.
	let mut cs_hname = Vec::new();			//Human-readable names of ciphersuite symbols.
	let mut cs_is_chacha = Vec::new();		//Ciphersuite symbol is chacha flag.
	let mut cs_sw_priority = Vec::new();		//Ciphersuite symbol software priority.
	let mut cs_hw_priority = Vec::new();		//Ciphersuite symbol hardware priority.
	let mut cs_algo_cbit_nokex = Vec::new();	//Signal bit of ciphersuite symbol ignoring kex.
	let mut cs_algo_cbit = Vec::new();		//Signal bit of ciphersuite symbol including kex.
	let mut cs_prf = Vec::new();			//PRF symbol used by ciphersuite symbol.
	let mut cs_protector = Vec::new();		//Protector symbol used by ciphersuite symbol.
	let mut cs_kex = Vec::new();			//Kex symbol used by ciphersuite symbol.

	for i in data.iter() {
		cs_consts_cs.push(mkidf!("CS_{i_cname}", i_cname=&i.cname));
		cs_consts_id.push(i.id);
		cs_help.push(format!("TLS Ciphersuite {i_hname}", i_hname=&i.hname));
		cs_name.push(mkid(&i.name));
		cs_hname.push(i.hname.clone());
		cs_is_chacha.push(i.is_chacha);
		cs_hw_priority.push(i.hw_priority);
		cs_sw_priority.push(i.sw_priority);
		cs_algo_cbit_nokex.push(i.algo_cbit_nokex);
		cs_algo_cbit.push(i.algo_cbit);
		cs_prf.push(mkid(&i.prf));
		cs_protector.push(mkid(&i.protector));
		cs_kex.push(mkid(&i.kex));
	}

	let algo_cbit = |c:&CiphersuiteData|c.algo_cbit as usize;
	let css_bs = Bitmask::<_CsSetElem>::new(data.iter().map(algo_cbit));

	qwrite(fp, quote!{
///TLS ciphersuite key exchange type.
///
///This enumeration is returned by the
///[`Ciphersuite::get_key_exchange()`](enum.Ciphersuite.html#method.get_key_exchange) method.
///
///The supported methods are:
///
/// * Traits:
///   * `Copy`(`Clone`)
///   * `Debug`
///   * `Eq`(`PartialEq`)
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
#[non_exhaustive]
pub enum KeyExchangeType
{
	///TLS 1.2 ECDHE_RSA key exchange.
	Tls12EcdheRsa,
	///TLS 1.2 ECDHE_ECDSA key exchange.
	Tls12EcdheEcdsa,
	///TLS 1.3 key exchange.
	Tls13,
}

///The priority of a ciphersuite
///
///This structure contains the priority (lower is more preferred) for a ciphersuite. Both software-only and
///hardware-accelerated values are given.
///
///This enumeration is returned by the
///[`Ciphersuite::get_priority()`](enum.Ciphersuite.html#method.get_priority) method.
pub struct CiphersuitePriority
{
	///The hardware-accelerated priority. Larger is higher priority.
	pub hw: u32,
	///The software-only priority. Larger is higher priority.
	pub sw: u32
}

#(
#[allow(missing_docs)] #[deprecated(since="1.2.0", note="Use Ciphersuite::to_tls_id() instead")]
pub const #cs_consts_cs: u16 = #cs_consts_id;
)*

///A TLS ciphersuite
///
///This can be created by the [`by_tls_id2()`](#method.by_tls_id2), [`random_suite()`](#method.random_suite) or
///[`all_suites()`](#method.all_suites) methods.
///
///The supported methods are:
///
/// * Create a ciphersuite by ID: [`by_tls_id2()`](#method.by_tls_id2)
/// * Create some ciphersuite: [`random_suite()`](#method.random_suite)
/// * Return set of all ciphersuites: [`all_suites()`](#method.all_suites)
/// * Return TLS ID for ciphersuite: [`to_tls_id()`](#method.to_tls_id)
/// * Return key exchange type for ciphersuite: [`get_key_exchange()`](#method.get_key_exchange)
/// * Return protector for ciphersuite: [`get_protector()`](#method.get_protector)
/// * Return PRF hash for ciphersuite: [`get_prf()`](#method.get_prf)
/// * Get bitmask bit for ciphersuite: [`algo_const()`](#method.algo_const)
/// * Get bitmask bit for ciphersuite, ignoring key exchange: [`algo_const_nokex()`](#method.algo_const_nokex)
/// * Is ciphersuite using Chacha: [`is_chacha()`](#method.is_chacha)
/// * Get priority for ciphersuite: [`get_priority()`](#method.get_priority)
/// * Traits:
///   * `Copy`(`Clone`)
///   * `Debug`.
///   * `Eq`(`PartialEq`).
///
#[derive(Copy,Clone,PartialEq,Eq)] pub enum Ciphersuite
{
	#(#[doc=#cs_help] #cs_name),*
}

#[allow(missing_docs)]
#[deprecated(since="1.2.0", note="Use Ciphersuite::cipher_strings() instead")]
pub static CIPHERSUITE_STRINGS: [&'static str; #num_ciphersuites] = [ #(#cs_hname),* ];

const RANDOM_CIPHERSUITE: Ciphersuite = Ciphersuite::#random_ciphersuite;
static ALL_CIPHERSUITES: &'static [Ciphersuite] = &[ #(Ciphersuite::#cs_name),* ];
const CSS_UNIVERSE_BITS: _CiphersuiteSet2 = #css_bs;
type _CiphersuiteSet2 = #!MASKTYPE css_bs;



impl Ciphersuite
{
	#[allow(deprecated)]
	fn _by_tls_id(id: u16) -> Option<Ciphersuite>
	{
		Some(match id {
			#(#cs_consts_cs => Ciphersuite::#cs_name),*,
			_ => return None
		})
	}
	#[allow(deprecated)]
	fn _to_tls_id(&self) -> u16
	{
		match self {
			#(&Ciphersuite::#cs_name => #cs_consts_cs),*
		}
	}
	fn _get_key_exchange(&self) -> KeyExchangeType
	{
		match self {
			#(&Ciphersuite::#cs_name => KeyExchangeType::#cs_kex),*
		}
	}
	fn _get_protector(&self) -> ProtectorType
	{
		match self {
			#(&Ciphersuite::#cs_name => ProtectorType::#cs_protector),*
		}
	}
	fn _get_prf(&self) -> HashFunction2
	{
		match self {
			#(&Ciphersuite::#cs_name => btls_aux_hash::#cs_prf::function()),*
		}
	}
	fn _set_bit(&self) -> u32
	{
		match self { #(&Ciphersuite::#cs_name => #cs_algo_cbit),* }
	}
	fn _is_chacha(&self) -> bool
	{
		match self {
			#(&Ciphersuite::#cs_name => #cs_is_chacha),*
		}
	}
	fn _get_priority(&self) -> CiphersuitePriority
	{
		match self {
			#(&Ciphersuite::#cs_name =>
				CiphersuitePriority{ sw: #cs_sw_priority, hw: #cs_hw_priority }),*
		}
	}
	fn _as_string(&self) -> &'static str
	{
		match self { #(&Ciphersuite::#cs_name => #cs_hname),* }
	}
}

static BAD_CIPHERSUITES: [u16; #num_bad_ciphers] = [ #(#banned_cs_list),* ];

});
}

fn main()
{
	let out_dir = env::var("OUT_DIR").unwrap();
	let dest_path = Path::new(&out_dir).join("autogenerated.inc.rs");
	let mut f = File::create(&dest_path).unwrap();
	write_autogenerated_file(&mut f, &read_data_file(), &read_banned_file());

	println!("cargo:rerun-if-changed=ciphersuites.dat");	//This has no external deps to rebuild for.
	println!("cargo:rerun-if-changed=banned.dat");		//This has no external deps to rebuild for.
}
