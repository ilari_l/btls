#[test]
fn test_allows()
{
	use crate::Ciphersuite;
	use crate::CiphersuiteEnabled2;
	use btls_aux_tls_iana::CipherSuite;
	let css = CiphersuiteEnabled2::unsafe_any_policy();
	let cs = Ciphersuite::by_tls_id2_unchecked(CipherSuite::ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256).unwrap();
	assert!(css.allows2(cs));
	assert!(css.allows2(0xCCA9u16));
	assert!(css.allows2(CipherSuite::ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256));
}
