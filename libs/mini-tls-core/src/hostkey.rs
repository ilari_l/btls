use crate::HostKey;
use btls_aux_hash::Hash;
use btls_aux_sha3::XofStream;
use core::ops::Deref;


//Hack around Rust element limitations.
fn equals(a: &[u8], b: &[u8]) -> bool { a == b }

fn check_xof(mut s: XofStream, h: &[u8], buf: &mut [u8]) -> bool
{
	if h.len() < 32 || buf.len() == 0 { return false; }
	let mut h = h.chunks_exact(buf.len());
	while let Some(h) = h.next() {
		s.read(buf);
		if h != buf { return false; }
	}
	let h = h.remainder();
	s.read(buf);
	buf.get(..h.len()) == Some(h)
}

impl HostKey
{
	pub(crate) fn is_match(&self, data: &[u8]) -> bool
	{
		match self {
			&HostKey::Raw(ref d) => d.deref() == data,
			&HostKey::Sha256(h) => equals(&h, &btls_aux_hash::Sha256::hash(data)),
			&HostKey::Sha384(h) => equals(&h, &btls_aux_hash::Sha384::hash(data)),
			&HostKey::Sha512(h) => equals(&h, &btls_aux_hash::Sha512::hash(data)),
			&HostKey::Sha3256(h) => equals(&h, &btls_aux_hash::Sha3256::hash(data)),
			&HostKey::Sha3384(h) => equals(&h, &btls_aux_hash::Sha3384::hash(data)),
			&HostKey::Sha3512(h) => equals(&h, &btls_aux_hash::Sha3512::hash(data)),
			&HostKey::Shake128(ref h) => {
				let mut buf = [0;168];	//Block at time.
				let mut s = btls_aux_sha3::Shake128::new();
				s.input(data);
				check_xof(s.as_stream(), h, &mut buf)
			},
			&HostKey::Shake256(ref h) => {
				let mut buf = [0;136];	//Block at time.
				let mut s = btls_aux_sha3::Shake256::new();
				s.input(data);
				check_xof(s.as_stream(), h, &mut buf)
			},
			&HostKey::Unsatisfiable => false,
		}
	}
}
