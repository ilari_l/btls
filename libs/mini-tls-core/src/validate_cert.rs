use super::HostKey;
use btls_aux_collections::BTreeMap;
use btls_aux_collections::String;
use btls_aux_collections::Vec;
use btls_aux_fail::fail_if;
use btls_aux_ip::IpAddress;
use btls_aux_publiccalist::get_build_date;
use btls_aux_publiccalist::is_expired_ca;
use btls_aux_publiccalist::is_public_ca;
use btls_aux_time::Timestamp;
use btls_aux_tls_iana::Alert;
use btls_aux_signatures::SignatureAlgorithmEnabled2;
use btls_aux_signatures::SignatureFlags2;
use btls_aux_signatures::SignatureX5092;
use btls_aux_x509certparse::extract_spki;
use btls_aux_x509certparse::ParsedCertificate2;
use core::ops::Range;
use core::str::FromStr;


type TrustAnchorMap = BTreeMap<Vec<u8>, Vec<u8>>;
type Whitelist = Vec<HostKey>;

///Kind of error from certificate validation.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub(crate) enum ErrorKind
{
	///A Certificate failed to parse, is malformed or has invalid signature.
	Bad,
	///Wrong name.
	WrongName,
	///Certificate contains unknown signature algorithm.
	AlgUnknown,
	///Certificates do not chain properly.
	BadChain,
	///Certificate constraints (path length, name, etc...) are violated.
	Constraint,
	///A Certificate has expired.
	Expired,
	///The certificate chain does not chain to trust anchor.
	Untrusted,
	///Things went south due to a bug.
	Internal,
}

#[derive(Clone,Debug)]
pub(crate) struct CertificateError
{
	kind: ErrorKind,
	message: String,
}

impl CertificateError
{
	pub(crate) fn alert(&self) -> Alert
	{
		use self::ErrorKind::*;
		match self.kind {
			Bad => Alert::BAD_CERTIFICATE,
			WrongName => Alert::CERTIFICATE_UNKNOWN,
			AlgUnknown => Alert::BAD_CERTIFICATE,
			BadChain => Alert::BAD_CERTIFICATE,
			Constraint => Alert::CERTIFICATE_UNKNOWN,
			Expired => Alert::CERTIFICATE_EXPIRED,
			Untrusted => Alert::CERTIFICATE_UNKNOWN,
			Internal => Alert::INTERNAL_ERROR,
		}
	}
	pub(crate) fn into_message(self) -> String { self.message }
}

macro_rules! error
{
	($kind:ident $($msg:tt)*) => {
		CertificateError {
			kind: ErrorKind::$kind,
			message: format!($($msg)*),
		}
	}
}

impl core::fmt::Display for CertificateError
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		f.write_str(&self.message)
	}
}

pub(crate) fn rpk_validate<'a>(whitelist: &Whitelist, entity: &'a [u8]) -> Result<&'a [u8], CertificateError>
{
	if whitelist.iter().any(|v|v.is_match(entity)) { return Ok(entity); }
	Err(error!(Untrusted "Raw public key is not on whitelist"))
}

static CURSED_ISRG_CROSS_SIGN: &'static [u8] = include_bytes!("irsg-x1-dst-hack.crt");

pub(crate) fn x509_validate<'a>(anchors: &TrustAnchorMap, whitelist: &Whitelist, hostname: &str, entity: &'a [u8],
	mut chain: &[&'a [u8]], spolicy: SignatureAlgorithmEnabled2) -> Result<&'a [u8], CertificateError>
{
	//Get current time, and sanity-check it to catch time being wildly off.
	let now = Timestamp::now();
	fail_if!(now.get_day() < get_build_date(), error!(Internal "Clock is in the past"));
	//Fucking LE Android hacks that cause problems for everyone else. Find-Fix-Finish this cursed cert.
	if chain.len() == 2 && chain[1] == CURSED_ISRG_CROSS_SIGN {
		chain = &chain[..1];
	}
	__x509_validate(anchors, whitelist, hostname, entity, chain, now, spolicy)
}

fn __x509_validate<'a>(anchors: &TrustAnchorMap, whitelist: &Whitelist, hostname: &str, entity: &'a [u8],
	chain: &[&'a [u8]], now: Timestamp, spolicy: SignatureAlgorithmEnabled2) ->
	Result<&'a [u8], CertificateError>
{
		//Signature tuples collects tuples of (key, data, signature) that should all be valid for the
		//chain to be valid.
		let mut signature_tuples: Vec<(&[u8], &[u8], SignatureX5092)> = Vec::new();
		//A known CA has been seen.
		let mut known_ca = false;
		//Use cruder parser to grab the spki.
		let spki = extract_spki(entity).map_err(|err|{
			error!(Bad "Unparseable certificate: {err}")
		})?;
		//If hostkey is on whitelist, accept.
		if whitelist.iter().any(|v|v.is_match(spki)) { return Ok(spki); }
		//Use TLS 1.2 chain semantics, because TLS 1.3 ones massively increase attack surface.
		//Parse the certificate. This is needed even in the whitelist case as it needs host key, which
		//is parsed out of certificate.
		let entity = ParsedCertificate2::from(entity).map_err(|err|{
			error!(Bad "Unparseable certificate: {err}")
		})?;
		//The EE certificate issuer expiry check is done at EE certificate expiry time.
		fail_if!(is_expired_ca(entity.issuer.as_raw_issuer(), entity.not_after.unix_time()),
			error!(Expired "End-entity certificate issuer expired"));
		//The hostkey whitelist ooes not match. Need to check the certificate chain.
		//Save issuer, TBS and signature, so issuer can be compared with next one in chain, and the
		//TBS/siganture can be combined with key from next to create signature triple.
		let mut last_issuer = entity.issuer;
		let mut last_tbs = entity.tbs;
		let mut last_signature = entity.signature.map_err(|_|{
			error!(AlgUnknown "Certificate signature algorithm unknown")
		})?;
		//Track nesting of certificate lifetimes in chain. The lifetime of issuer must be superset of
		//the subject.
		let mut lifetime = entity.not_before..entity.not_after;

		//Run checks on EE certificate.
		check_subject(&entity, hostname, last_signature.oid(), now)?;
		//Loop on the chain.
		for (ord, chain_cert) in chain.iter().enumerate() {
			//If the issuer of last cert is trust anchor, stop chasing.
			known_ca |= anchors.contains_key(last_issuer.as_raw_issuer());
			//Parse the certificate in order to acces info on it.
			let chain_cert = ParsedCertificate2::from(chain_cert).map_err(|err|{
				error!(Bad "Unparseable issuer: {err}")
			})?;
			//The CA certificate issuer expiry check is done at CA certificate expiry time.
			fail_if!(is_expired_ca(chain_cert.issuer.as_raw_issuer(), chain_cert.not_after.unix_time()),
				error!(Expired "CA certificate issuer expired"));
			//Check that subject name of issuer matches with issuer name of subject. If the names
			//do not match, the issuer/subject certificates are mismatched. The actual comparision
			//rules are way too complicated, so use bitwise equality (which is commonly agreed to
			//be the right thing).
			fail_if!(!last_issuer.same_as(chain_cert.subject),
				error!(BadChain "Subject of issuer is not issuer of subject"));
			//signature_tuples contains key of this certificate, together with TBS and signature of
			//the PREVIOUS certificate. This way it is a triple that should verify. Do not do this
			//if root has been found in order to skip verifying signatures that do not need to be
			//verified.
			if !known_ca { signature_tuples.push((chain_cert.pubkey, last_tbs, last_signature)); }
			//Now save issuer, TBS and signature of this cert , so they can be combiend with next
			//certificate, in the same way as EE certificate.
			last_issuer = chain_cert.issuer;
			last_tbs = chain_cert.tbs;
			last_signature = chain_cert.signature.
				map_err(|_|error!(AlgUnknown "Issuer signature algorithm unknown"))?;
			//Run checks on issuer certificate. Only allow self-signeds for last entry if CA has
			//been found.
			check_issuer(&chain_cert, &entity.dnsnames, &lifetime, last_signature.oid(), ord, now,
				ord + 1 == chain.len() && known_ca)?;
			//There is an exception to lifetime nesting: The not-before of issuer to issuer chaining
			//does not have to be superset. Implement this by overwriting the not_before by now.
			lifetime = now..chain_cert.not_after;
		}
		//The certificate chain might be of correct length, in which case the above loop will not find
		//a trust anchor. So check once more.
		if let (false, Some(ta)) = (known_ca, anchors.get(last_issuer.as_raw_issuer())) {
			//Push the last signature tuple. the trusted key of TA together with TBS and
			//signature from the last certificate in chain. So this should form tuple that
			//does verify.
			signature_tuples.push((ta, last_tbs, last_signature));
			//Mark that the issuing CA is known, so the certificate is not rejected for being
			//issued by an unknown CA.
			known_ca = true;
		}
		
		//Check that the loop actually found the TA, as opposed to just looping to end of chain. If
		//end of chain was reached, that means there is no known issuing CA, and the certificate should
		//be rejected.
		fail_if!(!known_ca, error!(Untrusted "Certificate not issued by trusted CA"));
		for (key, data, signature) in signature_tuples.iter() {
			signature.verify(key, data, spolicy, SignatureFlags2::CERTIFICATE).map_err(|err|{
				error!(Bad "Chain signature failed: {err}")
			})?;
		}
		Ok(entity.pubkey)
}

pub(crate) enum ValidateName<'a>
{
	Ip(IpAddress),
	Dns(&'a [u8]),
}

impl<'a> ValidateName<'a>
{
	pub(crate) fn new(hostname: &'a str) -> Option<ValidateName<'a>>
	{
		Some(if let Some(addr) = hostname.strip_prefix("!") {
			let addr = IpAddress::from_str(addr).ok()?;
			ValidateName::Ip(addr)
		} else {
			ValidateName::Dns(hostname.as_bytes())
		})
	}
}

fn check_subject(cert: &ParsedCertificate2, hostname: &str, sig_oid: &[u8], now: Timestamp) ->
	Result<(), CertificateError>
{
	//1) Check the certificate is actually a TLS server certificate.
	fail_if!(!cert.use_tls_serv, error!(Bad "Certificate is not valid for TLS servers"));
	//2) Check that the certificate is actually valid for the name.
	let rname = ValidateName::new(hostname).ok_or(error!(Untrusted "Malformed IP address in server name"))?;
	fail_if!(match rname {
		ValidateName::Ip(addr) => cert.dnsnames.iter_addrs().all(|name|!name.matches_ip(addr)),
		ValidateName::Dns(hname) => cert.dnsnames.iter().all(|name|!hostnames_match(name, hname)),
	}, error!(WrongName "Certificate is for wrong name"));
	//3) Check that the certificate is in validity time.
	fail_if!(now < cert.not_before || now >= cert.not_after,
		error!(Expired "Certificate is not yet valid or expired"));
	//4) Check that certificate is not ACME validation certificate.
	fail_if!(cert.acme_tls_alpn.is_some(), error!(Bad "Certificate is for ACME validation"));
	//5) Certificates have two copies of signature algorithm. Verify that those two are consistent.
	fail_if!(cert.sig_algo1 != sig_oid, error!(Bad "Certificate has contradictionary signature algorithms"));
	//6) Do not allow self-signed certificates. Trust anchors on EE certificates are not supported. If one
	//wants to accept them anyway, one can use hostkey whitelist.
	fail_if!(cert.issuer.same_as(cert.subject), error!(Bad "Certificate is self-signed"));
	//7) Check that if certificate is issued by public CA that certificate lifetime is not too long.
	//This is mix of 2 and 1 year certificates, as all 3 year ones have expired by now.
	if is_public_ca(cert.issuer.as_raw_issuer()) {
		let validity = cert.not_before.delta(cert.not_after);
		fail_if!(validity >= 34_387_200, error!(Bad "Public certificate is valid for too long"));
	}
	Ok(())
}

fn check_issuer(cert: &ParsedCertificate2, names: &btls_aux_x509certparse::NameList, lifetime: &Range<Timestamp>,
	sig_oid: &[u8], n: usize, now: Timestamp, last: bool) -> Result<(), CertificateError>
{
	//1) Check that the issuer is in fact a CA.
	fail_if!(!cert.is_ca, error!(Constraint "Issuer is not a CA"));
	//2) Check that the issuer can in fact issue TLS server certificates. Thiis is not RFC-compliant, but is
	//commonly used meaning of the issuer EKU extension.
	fail_if!(!cert.use_tls_serv && !cert.use_any, error!(Constraint "Issuer can not issue for TLS servers"));
	//3) Check chain length constraint.
	if let Some(limit) = cert.max_path_len {
		fail_if!(n as u64 > limit, error!(Constraint "Issuer violates path length constraint"));
	}
	//4) Check that issuer is within its validity time.
	fail_if!(now < cert.not_before || now >= cert.not_after,
		error!(Expired "Issuer is not yet valid or has expired"));
	//5) Check that certificate is not ACME validation certificate.
	fail_if!(cert.acme_tls_alpn.is_some(), error!(Bad "Issuer is for ACME validation"));
	//6) Certificates have two copies of signature algorithm. Verify that those two are consistent.
	fail_if!(cert.sig_algo1 != sig_oid, error!(Bad
		"Issuer has contradictionary signature algorithms"));
	//7) Do not allow self-signed certificates. These certificates are probably actually self-issued,
	//which is a security issue as it circumvents things like chain length constraints. In case chain includes
	//self-signed root, the trust anchor check should cut the chain, preventing code from ever reahing
	//here. Last entry may be self-signed, in case someone sends extra root.
	fail_if!(!last && cert.issuer.same_as(cert.subject), error!(Untrusted "Issuer is self-signed"));
	//8) Check any IP constraints are well-formed address ranges.
	for ent in cert.names_allow.iter_addrs() {
		fail_if!(!ent.is_continuous(), error!(Bad "Issuer has invalid IP address constraint"));
	}
	for ent in cert.names_disallow.iter_addrs() {
		fail_if!(!ent.is_continuous(), error!(Bad "Issuer has invalid IP address constraint"));
	}
	//9) Check names in certificate against issuer name constraints. Check if whitelistes are empty or not,
	//as if whitelist is missing, all names except blacklisted ones are allowed, wheereas if whitelist is
	//present, only whitelisted (and not blacklisted) names are allowed.
	let is_name_whitelist = cert.names_allow.iter().next().is_some();
	let is_addr_whitelist = cert.names_allow.iter_addrs().next().is_some();
	for name in names.iter() {
		let mut ok = true;
		if is_name_whitelist {
			//Whitelist present -> If name is not on it, deny.
			if cert.names_allow.iter().all(|ct|!hostname_in_constraint(name, ct)) {
				ok = false;
			}
		}
		//If name is on blacklist, deny.
		if cert.names_disallow.iter().any(|ct|hostname_in_constraint(name, ct)) {
			ok = false;
		}
		fail_if!(!ok, error!(Constraint "Issuer is not allowed to issue for name"));
	}
	for name in names.iter_addrs() {
		let mut ok = true;
		if is_addr_whitelist {
			//Whitelist present -> If name is not on it, deny.
			if cert.names_allow.iter_addrs().all(|ct|!name.is_subset_of(&ct)) {
				ok = false;
			}
		}
		//If name is on blacklist, deny.
		if cert.names_disallow.iter_addrs().any(|ct|name.is_subset_of(&ct)) {
			ok = false;
		}
		fail_if!(!ok, error!(Constraint "Issuer is not allowed to issue for address"));
	}
	//10) Check lifetimes are properly nested.
	fail_if!(lifetime.start < cert.not_before || lifetime.end > cert.not_after,
		error!(Constraint "Subject lifetime exceeds issuer lifetime"));
	Ok(())
}

fn hostnames_match(x: &[u8], y: &[u8]) -> bool
{
	if hostnames_equal(x, y) { return true; }
	if x.len() >= 2 && &x[..2] == b"*." {
		//x is wildcard. Truncate the first label out of both x and y and compare.
		let p = y.iter().position(|x|*x==b'.');
		if let Some(p) = p { if p <= y.len() {
			return hostnames_equal(&x[1..], &y[p..])
		}}
	}
	false
}

pub(crate) fn hostnames_equal(x: &[u8], y: &[u8]) -> bool
{
	//This is close enough... The only aliasing bytes that are both valid are letters, and those are
	//both synonmys.
	x.iter().zip(y.iter()).all(|(x,y)|x&0xdf==y&0xdf)
}

fn hostname_in_constraint(name: &[u8], constraint: &[u8]) -> bool
{
	//If constraint begins with a '.', strip that (. is always 1 byte).
	if constraint.len() == 0 { return false; }
	let constraint = if constraint[0] == b'.' { &constraint[1..] } else { constraint };
	//Split the name into prefex and postfix.
	//Now the prefix must be empty or end with '.' and postfixes have to match.
	if name.len() >= constraint.len() {
		let (a, b) = name.split_at(constraint.len());
		//The prefix has to be empty or end with ., and postfix has to be equal to constraint.
		(a.len() == 0 || a.ends_with(b".")) && hostnames_equal(constraint, b)
	} else {
		false
	}
}

pub(crate) static CLIENT_CERTIFICATE_VERIFY: &'static str = "client CertificateVerify";
//pub(crate) static SERVER_CERTIFICATE_VERIFY: &'static str = "server CertificateVerify";

pub(crate) fn tls13_tbs_with_ctx(ctx: &str, data: &[u8]) -> Vec<u8>
{
	let mut tbs = Vec::with_capacity(74 + ctx.len() + data.len());
	tbs.extend_from_slice(&[32;64]);
	tbs.extend_from_slice(b"TLS 1.3, ");
	tbs.extend_from_slice(ctx.as_bytes());
	tbs.push(0);
	tbs.extend_from_slice(data);
	tbs
}

pub(crate) fn tls13_client_tbs(hash: btls_aux_hash::HashOutput2) -> Vec<u8>
{
	tls13_tbs_with_ctx(CLIENT_CERTIFICATE_VERIFY, &hash)
}

#[cfg(test)]
fn as_root(root: &[u8]) -> BTreeMap<Vec<u8>, Vec<u8>>
{
	let root = ParsedCertificate2::from(root).unwrap();
	let mut roots = BTreeMap::new();
	roots.insert(root.subject.as_raw_subject().to_vec(), root.pubkey.to_vec());
	roots
}

#[cfg(test)] use crate::get_signature_policy;
#[cfg(test)] static TEST_CERT: &'static [u8] = include_bytes!("lsnes.tas.bot.crt.der");
#[cfg(test)] static TEST_INTERMEDIATE: &'static [u8] = include_bytes!("lets-encrypt-r3.crt.der");
#[cfg(test)] static TEST_ROOT: &'static [u8] = include_bytes!("isrg-root-x1.crt.der");
#[cfg(test)] static TEST_ROOT_CURSED: &'static [u8] = include_bytes!("irsg-x1-dst-hack.crt");
#[cfg(test)] static TEST_TIME: i64 = 1630854166;
#[cfg(test)] static TEST_VALIDATOR: HostKey = HostKey::Sha256([
	0xa5,0x80,0x09,0x30,0x00,0x7a,0x4d,0x78,0x20,0xad,0xf6,0xbf,0xee,0x67,0x5b,0xad,
	0x39,0xfa,0xe0,0x58,0x5c,0xa9,0x0a,0xbe,0x70,0x78,0x7a,0x34,0x5c,0x8e,0xc9,0xe5
]);

#[test]
fn test_x509_validation()
{
	__x509_validate(&as_root(TEST_ROOT), &Vec::new(), "lsnes.tas.bot", TEST_CERT, &[TEST_INTERMEDIATE],
		Timestamp::new(TEST_TIME), get_signature_policy(false)).unwrap();
}

#[test]
fn test_x509_validation_redudant_root()
{
	__x509_validate(&as_root(TEST_ROOT), &Vec::new(), "lsnes.tas.bot", TEST_CERT,
		&[TEST_INTERMEDIATE, TEST_ROOT], Timestamp::new(TEST_TIME), get_signature_policy(false)).unwrap();
}

#[test]
fn test_x509_validation_cursed_chain()
{
	let e = __x509_validate(&as_root(TEST_ROOT), &Vec::new(), "lsnes.tas.bot", TEST_CERT,
		&[TEST_INTERMEDIATE,TEST_ROOT_CURSED], Timestamp::new(TEST_TIME), get_signature_policy(false)).
		unwrap_err();
	assert_eq!(e.kind, ErrorKind::Expired);
}

#[test]
fn test_x509_validation_badname()
{
	let e = __x509_validate(&as_root(TEST_ROOT), &Vec::new(), "x.tas.bot", TEST_CERT, &[TEST_INTERMEDIATE],
		Timestamp::new(TEST_TIME), get_signature_policy(false)).unwrap_err();
	assert_eq!(e.kind, ErrorKind::WrongName);
}

#[test]
fn test_x509_validation_badroot()
{
	let e = __x509_validate(&BTreeMap::new(), &Vec::new(), "lsnes.tas.bot", TEST_CERT, &[TEST_INTERMEDIATE],
		Timestamp::new(TEST_TIME), get_signature_policy(false)).unwrap_err();
	assert_eq!(e.kind, ErrorKind::Untrusted);
}

#[test]
fn test_x509_validation_interm_ta()
{
	__x509_validate(&as_root(TEST_INTERMEDIATE), &Vec::new(), "lsnes.tas.bot", TEST_CERT, &[TEST_INTERMEDIATE],
		Timestamp::new(TEST_TIME), get_signature_policy(false)).unwrap();
}

#[test]
fn test_x509_validation_ta_cert()
{
	let e = __x509_validate(&as_root(TEST_CERT), &Vec::new(), "lsnes.tas.bot", TEST_CERT, &[TEST_INTERMEDIATE],
		Timestamp::new(TEST_TIME), get_signature_policy(false)).unwrap_err();
	assert_eq!(e.kind, ErrorKind::Untrusted);
}

#[test]
fn test_whitelist_1()
{
	__x509_validate(&BTreeMap::new(), &vec![TEST_VALIDATOR.clone()], "x.tas.bot", TEST_CERT,
		&[TEST_INTERMEDIATE], Timestamp::new(TEST_TIME), get_signature_policy(false)).unwrap();
}


#[test]
fn test_whitelist_2()
{
	__x509_validate(&BTreeMap::new(), &vec![TEST_VALIDATOR.clone()], "x.tas.bot", TEST_CERT, &[],
		Timestamp::new(TEST_TIME), get_signature_policy(false)).unwrap();
}
