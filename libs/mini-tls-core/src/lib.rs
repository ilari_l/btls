#![deny(unsafe_code)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]
//!Mini TLS client interals.
use btls_aux_collections::Vec;
use btls_aux_signatures::SignatureAlgorithmEnabled2;
#[cfg(test)] #[macro_use] extern crate std;

#[cfg(not(test))]
macro_rules! format
{
	($($args:tt)*) => {{
		let mut s = btls_aux_collections::String::new();
		//Ignore any errors, because there should not be any errors.
		core::fmt::Write::write_fmt(&mut s, format_args!($($args)*)).ok();
		s
	}}
}


macro_rules! tlserror
{
	([$alert:ident] $($args:tt)*) => {
		TlsError {
			alert: btls_aux_tls_iana::Alert::$alert,
			msg: format!($($args)*),
		}
	}
}

fn get_signature_policy(nsa: bool) -> SignatureAlgorithmEnabled2
{
	if nsa {
		SignatureAlgorithmEnabled2::nsa_mode()
	} else {
		SignatureAlgorithmEnabled2::unsafe_any_policy()
	}
}

mod error;
mod handshake;
mod hostkey;
mod record;
mod validate_cert;
mod writer;

///Unstable stuff.
pub mod unstable
{
	use btls_aux_collections::Vec;
	use btls_aux_tls_iana::Alert;
	use btls_aux_tls_iana::HandshakeType;
	use btls_aux_tlskeyschedule::BoxedProtector;
	pub use crate::handshake::Blocked as ClientBlocked;
	pub use crate::handshake::CertificateRequest as ClientCertificateRequest;
	pub use crate::handshake::ConnectionInfo as ClientConnectionInfo;
	pub use crate::handshake::Handshake as ClientHandshake;
	pub use crate::handshake::Parameters as ClientParameters;
	pub use crate::handshake::SignatureRequest as ClientSignatureRequest;
	pub use crate::record::RecordLayer;
	pub use crate::record::RecordReceiveAB;
	pub use crate::record::RecordSendAB;

	///A TLS client.
	pub type Client = RecordLayer<ClientHandshake>;

	use core::fmt::Display;
	///Require the server to negotiate ALPN.
	///
	///specifiying this restriction on connection cause s th handshake to fail unless server explciity
	///negotiates Applicaition Layer Protocol.
	///
	///It is meant as ALPACA attack countermeasure.
	pub const MANDATORY_ALPN: u32 = 1<<0;
	///Require the server to negotiate EMS or TLS 1.3.
	///
	///specifiying this restriction on connection cause s th handshake to fail unless server uses extended
	///master secret or TLS 1.3. This also guarantees that `tls_extractor()` method is usable.
	///
	///It is meant as Triple Handshake Attack countermeasure.
	pub const MANDATORY_EMS: u32 = 1<<1;
	///Require the server to negotiate TLS 1.3.
	///
	///specifiying this restriction on connection cause s th handshake to fail unless server uses TLS 1.3.
	///
	///Some newer greenfield protocols requiere TLS 1.3.
	pub const MANDATORY_TLS13: u32 = 1<<2;
	///Require the server to negotiate high (at least 192 bit) encryption.
	///
	///Specifying this causes the client to only propose to server ciphesuites or key exchanges that offer
	///at least 192 bits of securtity (instead of orddinary 128).
	///
	///It is mdeled after the encryption strength requirements from CNSA suite. Note that this does not
	///make the library compliant with CNSA: E.g., Chacha20 (nominally 256-bit cipher) remains enabled, and
	///signaure algorithms in certifcates remain as usual.
	pub const MANDATORY_HIGH_ENC: u32 = 1<<3;
	///Require the server to perform ACME TLS-ALPN-01 validation.
	///
	///To use this, also:
	///
	/// * Specify ALPN `acme-tls/1` as the only ALPN.
	/// * Specify exactly one host key, of type `Sha256`, containing the SHA-256 hash of the key authorization.
	pub const MANDATORY_ACME_TLS: u32 = 1<<4;
	///NSA mode.
	pub const MANDATORY_NSA: u32 = 1<<5;
	///Phase of receive or transmit.
	#[derive(Copy,Clone,PartialEq,Eq,Debug)]
	pub enum RxTxPhase
	{
		///There is pending handshake data to be received or sent.
		Handshake,
		///Application data may be received or sent.
		Application,
		///Application data was possible to receive or send, but an EOF was either received or sent,
		///and that caused that phase to end.
		Shutdown,
		///Handshake is in progress, and it is not possible to useuflly receive or send data.
		Wait,
	}
	///Phase of handshake.
	#[derive(Copy,Clone,PartialEq,Eq,Debug)]
	pub enum HandshakePhase
	{
		///Running. The handshake is underway, not blocked, completed nor failed. For handshake to
		///make progress, oen either needs to receive more data (`rexeive_record()`), or send a flight
		///(`transmit_flight()`). 
		Running,
		///Blocked. The handshake has been temporarily suspended until some condition has been
		///met. 
		///
		///For obtaing more information about the condition that blocked the handshkae, call
		///`transmit_flight()`.
		Blocked,
		///Complete. The handshake has successfully completed.
		Complete,
		///ACME complete. The ACME-TLS validation handshake is done.
		AcmeComplete,
		///Failed. The handshake has failed. The connection is unsuable.
		Failed,
	}
	///Record layer status.
	///
	///This structure contains information about the status of the record layer.
	#[derive(Copy,Clone,PartialEq,Eq,Debug)]
	pub struct RecordLayerStatus
	{
		///Extractor ready flag.
		///
		///If this is set, `TLS-EXTRACTOR` is usable.
		pub extractor_ready: bool,
		///Fatal error flag.
		///
		///If this is set, the connection has encountered a fatal error and is unusable.
		pub fatal_error: bool,
		///The current handshake phase.
		pub handshake: HandshakePhase,
		///The current phase of receive half-connection.
		pub rx: RxTxPhase,
		///The current phase of transmit half-connection.
		pub tx: RxTxPhase,
	}
	///Handshake status.
	#[derive(Copy,Clone,PartialEq,Eq,Debug)]
	pub enum HandshakeStatus
	{
		///Wants to transmit.
		Send,
		///Blocked.
		Blocked,
		///Wants to receive.
		Receive,
		///Handshake complete.
		Complete,
		///ACME validation complete.
		AcmeComplete,
		///Handshake failed.
		Error,
	}
	///Return from transmit_flight.
	///
	///This object is retrun value of `transmit_flight()`. It either contains a fight to transmit, or an
	///indiation of why the handshake was blcoked.
	pub enum TransmitReturn<B>
	{
		///The handshake enountered an fatal error.
		Error,
		///Data to transmit in order to containue the handshake.
		Transmit(Vec<u8>),
		///Handshake is blocked.
		Blocked(B),
	}
	///Something containing a TLS alert.
	pub trait GetAlert
	{
		fn get_alert(&self) -> Alert;
	}
	///TLS Handshake object.
	pub trait TlsHandshake
	{
		///The type of errors.
		type Error: Display+GetAlert;
		///The type of parameters.
		type Parameters;
		///The type of block info.
		type BlockInfo;
		///The type of connection info.
		type ConnectionInfo: Default;
		///Create a new handshake object, with specified parameters.
		fn new(p: <Self as TlsHandshake>::Parameters) -> Self;
		///Receive Change Cipher Spec.
		fn recv_change_cipher_spec(&mut self, ractions: &mut RecordReceiveAB) ->
			Result<(), <Self as TlsHandshake>::Error>;
		///Receive handshake message.
		///
		///The last flag is set iff message ends at end of record.
		fn recv_message(&mut self, ractions: &mut RecordReceiveAB,
			cinfo: &mut <Self as TlsHandshake>::ConnectionInfo, mtype: HandshakeType, msg: &[u8],
			last: bool) -> Result<(), <Self as TlsHandshake>::Error>;
		///Fail the handshake.
		fn set_status_to_error(&mut self);
		///Transmit a flight, or return block info.
		fn transmit(&mut self, ractions: &mut RecordSendAB, old_protector: &mut BoxedProtector) ->
			Result<TransmitReturn<<Self as TlsHandshake>::BlockInfo>, <Self as TlsHandshake>::Error>;
		///Get status flags.
		fn get_status(&self) -> HandshakeStatus;
	}
}



///Host key validator.
///
///This structure presents a validator to match a host key against. The validator can be data itself, or hash of
///data, computed using cryptographically secure hash function.
///
///In all cases, the data to match against is X.509 SubjectPublicKeyInfo structure in its DER encoding (including
///the leading SEQUENCE).
#[derive(Clone)]
#[non_exhaustive]
pub enum HostKey
{
	///The raw key.
	Raw(Vec<u8>),
	///SHA-256 hash of the key.
	Sha256([u8;32]),
	///SHA-384 hash of the key.
	Sha384([u8;48]),
	///SHA-512 hash of the key.
	Sha512([u8;64]),
	///SHA3-256 hash of the key.
	Sha3256([u8;32]),
	///SHA3-384 hash of the key.
	Sha3384([u8;48]),
	///SHA3-512 hash of the key.
	Sha3512([u8;64]),
	///First bits of SHAKE128 XOF of the key. The length must be at least 32 bytes.
	Shake128(Vec<u8>),
	///First bits of SHAKE256 XOF of the key. The length must be at least 32 bytes.
	Shake256(Vec<u8>),
	///Never matches anything.
	Unsatisfiable,
}

#[cfg(test)] mod test;
