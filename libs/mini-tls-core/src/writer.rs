use btls_aux_collections::Vec;
use btls_aux_tls_iana::ExtensionType;

pub(crate) struct VectorWriter<'a>(&'a mut Vec<u8>);

impl<'a> VectorWriter<'a>
{
	pub(crate) fn new(x: &'a mut Vec<u8>) -> VectorWriter<'a> { VectorWriter(x) }
	pub(crate) fn len(&self) -> usize { self.0.len() }
	pub(crate) fn fixup_as_record(&mut self)
	{
		let l = self.len() as u16;
		self.__backwrite16(3, l.wrapping_sub(5));
	}
	fn __backwrite8(&mut self, p: usize, v: u8)
	{
		let e = v.to_be_bytes();
		self.0.get_mut(p..p+1).map(|dst|dst.copy_from_slice(&e));
	}
	fn __backwrite16(&mut self, p: usize, v: u16)
	{
		let e = v.to_be_bytes();
		self.0.get_mut(p..p+2).map(|dst|dst.copy_from_slice(&e));
	}
	fn __backwrite24(&mut self, p: usize, v: u32)
	{
		let e = v.to_be_bytes();
		self.0.get_mut(p..p+3).map(|dst|dst.copy_from_slice(&e[1..4]));
	}
	pub(crate) fn list8(&mut self, f: impl FnOnce(&mut Self))
	{
		let l = self.0.len();
		self.0.extend_from_slice(&[0]);
		f(self);
		let len = self.0.len()-l-1;
		self.__backwrite8(l, len as u8)
	}
	pub(crate) fn list16(&mut self, f: impl FnOnce(&mut Self))
	{
		let l = self.0.len();
		self.0.extend_from_slice(&[0,0]);
		f(self);
		let len = self.0.len()-l-2;
		self.__backwrite16(l, len as u16)
	}
	pub(crate) fn list24(&mut self, f: impl FnOnce(&mut Self))
	{
		let l = self.0.len();
		self.0.extend_from_slice(&[0,0,0]);
		f(self);
		let len = self.0.len()-l-3;
		self.__backwrite24(l, len as u32)
	}
	pub(crate) fn extension(&mut self, t: ExtensionType, f: impl FnOnce(&mut Self))
	{
		self.val16(t.get());
		self.list16(f);
	}
	pub(crate) fn val8(&mut self, v: u8) { self.0.extend_from_slice(&v.to_be_bytes()); }
	pub(crate) fn val16(&mut self, v: u16) { self.0.extend_from_slice(&v.to_be_bytes()); }
	pub(crate) fn val24(&mut self, v: u32) { self.0.extend_from_slice(&v.to_be_bytes()[1..4]); }
	pub(crate) fn bytes(&mut self, v: &[u8]) { self.0.extend_from_slice(v); }
	pub(crate) fn item8(&mut self, v: &[u8])
	{
		self.val8(v.len() as u8);
		self.0.extend_from_slice(v);
	}
	pub(crate) fn item16(&mut self, v: &[u8])
	{
		self.val16(v.len() as u16);
		self.0.extend_from_slice(v);
	}
	pub(crate) fn item24(&mut self, v: &[u8])
	{
		self.val24(v.len() as u32);
		self.0.extend_from_slice(v);
	}
}

#[test]
fn test_vectors()
{
	let mut orig = Vec::new();
	let mut v = VectorWriter::new(&mut orig);
	v.val8(42);
	v.list8(|v|{
		v.val16(258);
		v.val16(259);
	});
	v.list16(|v|{
		v.val16(258);
		v.val16(259);
	});
	v.list24(|v|{
		v.val16(258);
		v.val16(259);
	});
	v.val8(43);
	assert_eq!(&orig[..], &[42,4,1,2,1,3,0,4,1,2,1,3,0,0,4,1,2,1,3,43]);
}
