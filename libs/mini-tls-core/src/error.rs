use btls_aux_assert::AssertFailed;
use btls_aux_collections::String;
use btls_aux_hash::HashContext2;
use btls_aux_tls_iana::Alert;
use btls_aux_tls_iana::HandshakeType;
use btls_aux_tlskeyschedule::Tls12KeyScheduleError;
use btls_aux_tlskeyschedule::Tls13KeyScheduleError;

impl From<AssertFailed> for TlsError
{
	fn from(err: AssertFailed) -> TlsError
	{
		tlserror!([INTERNAL_ERROR] "BUG: {err}")
	}
}

impl From<crate::validate_cert::CertificateError> for TlsError
{
	fn from(x: crate::validate_cert::CertificateError) -> TlsError
	{
		TlsError{alert: x.alert(), msg: x.into_message()}
	}
}

pub(crate) struct W13KSE(pub(crate) Tls13KeyScheduleError, pub(crate) &'static str);

impl From<W13KSE> for TlsError
{
	fn from(x: W13KSE) -> TlsError
	{
		let pfx = x.1;
		match x.0 {
			Tls13KeyScheduleError::InternalError(err) => tlserror!([INTERNAL_ERROR] "BUG: {pfx}: {err}"),
			Tls13KeyScheduleError::SignatureFailed(err) =>
				tlserror!([DECRYPT_ERROR] "{pfx}: Signature does not verify: {err}"),
			Tls13KeyScheduleError::BadFinishedMac|Tls13KeyScheduleError::BadBinder =>
				tlserror!([DECRYPT_ERROR] "{pfx}: Wrong MAC"),
			Tls13KeyScheduleError::UnexpectedAlgorithm(a,e) => tlserror!([DECRYPT_ERROR]
				"{pfx}: Unexpected algorithm: Expected {exp}, got {act}",
				exp=e.tls_id2(), act=a.tls_id2()),
			_ => tlserror!([INTERNAL_ERROR] "{pfx}: ???"),
			
		}
	}
}

pub(crate) struct W12KSE(pub(crate) Tls12KeyScheduleError, pub(crate) &'static str);

impl From<W12KSE> for TlsError
{
	fn from(x: W12KSE) -> TlsError
	{
		let pfx = x.1;
		match x.0 {
			Tls12KeyScheduleError::InternalError(err) => tlserror!([INTERNAL_ERROR] "BUG: {pfx}: {err}"),
			Tls12KeyScheduleError::SignatureFailed(err) =>
				tlserror!([DECRYPT_ERROR] "{pfx}: Signature does not verify: {err}"),
			Tls12KeyScheduleError::BadFinishedMac => tlserror!([DECRYPT_ERROR] "{pfx}: Wrong MAC"),
			_ => tlserror!([INTERNAL_ERROR] "{pfx}: ???"),
		}
	}
}

///TLS error.
#[derive(Clone)]
pub struct TlsError
{
	pub(crate) alert: Alert,
	pub(crate) msg: String,
}

impl crate::unstable::GetAlert for TlsError
{
	fn get_alert(&self) -> Alert { self.alert }
}

impl core::fmt::Display for TlsError
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		f.write_str(&self.msg)
	}
}

macro_rules! conv_to_tls_error
{
	($clazz:ident $text:expr) => {
		impl From<btls_aux_tls_struct::$clazz> for TlsError
		{
			fn from(err: btls_aux_tls_struct::$clazz) -> TlsError
			{
				TlsError {
					alert: err.alert(),
					msg: format!("Error parsing {text}: {err}", text=$text),
				}
			}
		}
	}
}

conv_to_tls_error!(ClientHelloParseError "client hello");
conv_to_tls_error!(ServerHelloParseError "server hello");
conv_to_tls_error!(EncryptedExtensionsParseError "encrypted extensions");
conv_to_tls_error!(CertificateParseError "certificate");
conv_to_tls_error!(CertificateRequestParseError "certificate request");
conv_to_tls_error!(CertificateVerifyParseError "certificate verify");
conv_to_tls_error!(ClientKeyExchangeParseError "client key exchange");
conv_to_tls_error!(ServerKeyExchangeParseError "server key exchange");

pub(crate) struct TranscriptMessage<'a>
{
	pub(crate) mtype: HandshakeType,
	pub(crate) payload: &'a [u8],
}

impl<'a> TranscriptMessage<'a>
{
	pub(crate) fn add(self, t: &mut HashContext2)
	{
		let plen = self.payload.len();
		let header = [self.mtype.get(), (plen>>16) as u8, (plen>>8) as u8, plen as u8];
		t.append_v(&[&header[..], &self.payload[..]]);
	}
}

