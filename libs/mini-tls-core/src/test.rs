use super::HostKey;
use btls_aux_fail::f_break;
use btls_aux_hash::Hash;
use btls_aux_memory::EscapeByteString;
use std::string::ToString;
use std::vec::Vec;

macro_rules! test
{
	(NORMAL $name:ident $hfn:ident $shake:ident) => {
		#[test]
		fn $name()
		{
			let msg = b"Hello, World!";
			let hash = btls_aux_hash::$hfn::hash(msg);
			assert!(HostKey::$shake(hash.to_vec()).is_match(msg));
		}
	};
	(LONG $name:ident $shake:ident) => {
		#[test]
		fn $name()
		{
			let mut hash = [0;512];
			let msg = b"Hello, World!";
			let mut s = btls_aux_sha3::$shake::new();
			s.input(msg);
			s.output(&mut hash);
			assert!(HostKey::$shake(hash.to_vec()).is_match(msg));
		}
	};
	(SHORT $name:ident $shake:ident) => {
		#[test]
		fn $name()
		{
			let mut hash = [0;31];
			let msg = b"Hello, World!";
			let mut s = btls_aux_sha3::$shake::new();
			s.input(msg);
			s.output(&mut hash);
			assert!(!HostKey::$shake(hash.to_vec()).is_match(msg));
		}
	};
}

test!(NORMAL shake128_matcher Shake128256 Shake128);
test!(NORMAL shake256_matcher Shake256512 Shake256);
test!(LONG shake128_matcher_long Shake128);
test!(LONG shake256_matcher_long Shake256);
test!(SHORT shake128_matcher_short Shake128);
test!(SHORT shake256_matcher_short Shake256);

#[test]
fn shake128_badhash()
{
	let mut hash = [0;256];
	let msg = b"Hello, World!";
	let mut s = btls_aux_sha3::Shake128::new();
	s.input(msg);
	s.output(&mut hash);
	for i in 0..2048 {
		let mut hash = hash.to_vec();
		hash[i/8] ^= 1<<i%8;
		assert!(!HostKey::Shake128(hash).is_match(msg));
	}
}

#[test]
fn shake256_badhash()
{
	let mut hash = [0;256];
	let msg = b"Hello, World!";
	let mut s = btls_aux_sha3::Shake256::new();
	s.input(msg);
	s.output(&mut hash);
	for i in 0..2048 {
		let mut hash = hash.to_vec();
		hash[i/8] ^= 1<<i%8;
		assert!(!HostKey::Shake256(hash).is_match(msg));
	}
}

static ROOT: &'static [u8] = include_bytes!("isrg-root-x1.crt.der");

#[test]
fn lsnes_tas_bot()
{
	use crate::unstable::*;
	//use btls_aux_tls_struct::ClientHello;
	use std::io::Read;
	use std::io::Write;
	use std::collections::BTreeMap;
	use btls_aux_x509certparse::ParsedCertificate2;
	let root = ParsedCertificate2::from(ROOT).unwrap();
	let mut roots = BTreeMap::new();
	roots.insert(root.subject.as_raw_subject().to_vec(), root.pubkey.to_vec());
	let mut tcp = std::net::TcpStream::connect(("lsnes.tas.bot",453)).unwrap();
	let mut c = Client::new(crate::unstable::ClientParameters {
		alpns: vec![b"http/1.1".to_vec()],
		hostkey_whitelist: Vec::new(),
		trust_anchors: roots.clone(),
		mandatory: crate::unstable::MANDATORY_ALPN|crate::unstable::MANDATORY_EMS,
		server_name: "lsnes.tas.bot".to_string(),
	});
	assert_eq!(c.get_status_flags(), RecordLayerStatus{
		extractor_ready: false,
		rx: RxTxPhase::Wait,
		tx: RxTxPhase::Handshake,
		handshake: HandshakePhase::Running,
		fatal_error: false,
	});
	let mut rbuf = Vec::new();
	let mut tmp = [0;16650];
	let mut xdata = [0;8192];
	loop {
		let status = c.get_status_flags();
		assert!(status.rx == RxTxPhase::Handshake || status.tx == RxTxPhase::Handshake ||
			status.handshake == HandshakePhase::Blocked ||
			status.handshake == HandshakePhase::Complete ||
			status.handshake == HandshakePhase::Failed);
		if status.tx == RxTxPhase::Handshake {
			match c.transmit_flight().unwrap() {
				TransmitReturn::Transmit(data) => tcp.write_all(&data).unwrap(),
				_ => panic!("Unknown transmit_flight return value")
			};
		}
		if status.rx == RxTxPhase::Handshake {
			let amt = tcp.read(&mut xdata).unwrap();
			if amt == 0 { panic!("Got EOF during handshake!"); }
			rbuf.extend_from_slice(&xdata[..amt]);
			let need = Client::first_record_size(&rbuf);
			if need <= rbuf.len() {
				let (head, tail) = rbuf.split_at(need);
				//This can never give any data: We only feed one record at time, and handshake
				//is always finished by a handshake record, whereas application data is in
				//eponymous records. These can not be combined.
				c.receive_record(head, &mut tmp).unwrap();
				rbuf = tail.to_vec();
			}
		}
		match status.handshake {
			HandshakePhase::Running => (),
			HandshakePhase::Blocked => panic!("Handshake got blocked"),
			HandshakePhase::Failed => panic!("Handshake failed"),
			HandshakePhase::AcmeComplete => panic!("ACME complete???"),
			HandshakePhase::Complete => break,
		}
		if status.fatal_error  { panic!("Record layer died during handshake"); }
	}
	//Connection has been handshaked. Try some data.
	let req = "GET / HTTP/1.1\r\nhost:lsnes.tas.bot\r\n\r\n";
	let (iamt, oamt) = c.transmit_record(req.as_bytes(), &mut tmp).unwrap();
	assert_eq!(iamt, req.len());
	tcp.write_all(&tmp[..oamt]).unwrap();
	assert_eq!(c.get_status_flags(), RecordLayerStatus{
		extractor_ready: true,
		rx: RxTxPhase::Application,
		tx: RxTxPhase::Application,
		handshake: HandshakePhase::Complete,
		fatal_error: false,
	});
	tcp.write_all(&c.transmit_eof()).unwrap();
	assert_eq!(c.get_status_flags(), RecordLayerStatus{
		extractor_ready: true,
		rx: RxTxPhase::Application,
		tx: RxTxPhase::Shutdown,
		handshake: HandshakePhase::Complete,
		fatal_error: false,
	});

	'outer: loop {
		let amt = tcp.read(&mut xdata).unwrap();
		if amt == 0 { panic!("Insecure truncation"); }
		rbuf.extend_from_slice(&xdata[..amt]);
		let mut need = Client::first_record_size(&rbuf);
		while need <= rbuf.len() {
			let (head, tail) = rbuf.split_at(need);
			let amt = c.receive_record(head, &mut tmp).unwrap();
			let amt = f_break!(amt, 'outer);	//Break on EOF.
			print!("{tmp}", tmp=EscapeByteString(&tmp[..amt]));
			rbuf = tail.to_vec();
			need = Client::first_record_size(&rbuf);
		}
	}
}

/*
*/
