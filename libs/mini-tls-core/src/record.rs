use crate::unstable::RxTxPhase;
use crate::unstable::HandshakePhase;
use crate::unstable::RecordLayerStatus;
use crate::unstable::TransmitReturn;
use crate::unstable::GetAlert;
use crate::unstable::HandshakeStatus;
use crate::unstable::TlsHandshake;
use btls_aux_collections::String;
use btls_aux_collections::ToString;
use btls_aux_collections::Vec;
use btls_aux_fail::dtry;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_memory::split_at;
use btls_aux_tls_iana::Alert;
use btls_aux_tls_iana::ContentType;
use btls_aux_tls_iana::HandshakeType;
use btls_aux_tlskeyschedule::BoxedDeprotector;
use btls_aux_tlskeyschedule::BoxedExtractor;
use btls_aux_tlskeyschedule::BoxedProtector;
use core::mem::replace;
use core::fmt::Display;


///Receive action buffer.
///
///This structure buffers the actions to take at the end of processing current record.
pub struct RecordReceiveAB
{
	set_decryptor: Option<BoxedDeprotector>,
	set_encryptor: Option<BoxedProtector>,
	set_extractor: Option<BoxedExtractor>,
	record_size_limit: Option<u16>,
	req_key_update: bool,
	appdata_ok: bool,
	ignore_ccs: bool,
	is_ems: bool,
	no_data: bool,
}

impl RecordReceiveAB
{
	///Set the decryptor used for next received record.
	pub fn set_decryptor(&mut self, x: BoxedDeprotector) { self.set_decryptor = Some(x); }
	///Set the encryptor used for next sent record.
	pub fn set_encryptor(&mut self, x: BoxedProtector) { self.set_encryptor = Some(x); }
	///Set the extractor used.
	pub fn set_extractor(&mut self, x: BoxedExtractor) { self.set_extractor = Some(x); }
	///Set record size limit.
	pub fn set_record_size_limit(&mut self, rsl: u16) { self.record_size_limit = Some(rsl); }
	///Request key update.
	pub fn notify_send_key_updated(&mut self) -> bool { replace(&mut self.req_key_update, true) }
	///Allow application data.
	pub fn allow_application_data(&mut self) { self.appdata_ok = true; }
	///Ignore Change Cipher Spec messages.
	pub fn ignore_change_cipher_spec(&mut self) { self.ignore_ccs = true; }
	///Set EMS flag.
	pub fn set_ems(&mut self, ems: bool) { self.is_ems |= ems; }
	//Ban application data from this connection.
	pub fn ban_application_data(&mut self) { self.no_data = true; }
}

///Send action buffer.
///
///This structure buffers the actions to take at the end of processing current flight.
pub struct RecordSendAB
{
	set_decryptor: Option<BoxedDeprotector>,
	///If Some, change the encryptor used.
	set_encryptor: Option<BoxedProtector>,
	///If Some, change the extractor used.
	set_extractor: Option<BoxedExtractor>,
	///Maximum allowed record payload (read only).
	max_payload: usize,
}

impl RecordSendAB
{
	///Set the decryptor used for next received flight.
	pub fn set_decryptor(&mut self, x: BoxedDeprotector) { self.set_decryptor = Some(x); }
	///Set the encryptor used for next sent flight.
	pub fn set_encryptor(&mut self, x: BoxedProtector) { self.set_encryptor = Some(x); }
	///Set the extractor used.
	pub fn set_extractor(&mut self, x: BoxedExtractor) { self.set_extractor = Some(x); }
	///Get the maximum payload size.
	pub fn get_max_payload(&self) -> usize { self.max_payload }
}

pub(crate) fn handle_handshake_fragment<H:TlsHandshake>(buf: &mut Vec<u8>, handler: &mut H, mut payload: &[u8],
	ractions: &mut RecordReceiveAB, cinfo: &mut <H as TlsHandshake>::ConnectionInfo) ->
	Result<(), <H as TlsHandshake>::Error>
{
	//If there is pending message to fill, do that.
	if buf.len() > 0 {
		//The header is 4 bytes, fill it byte at a time.
		while buf.len() < 4 && payload.len() > 0 {
			let (head, tail) = payload.split_at(1);
			buf.push(head[0]);
			payload = tail;
		}
		//Now, check the length the message should have.
		let len = 4 + buf[1] as usize * 65536 + buf[2] as usize * 256 + buf[3] as usize;
		let missing = len - buf.len();
		let (head, payload) = split_at(payload, missing).unwrap_or_else(|p|(p,&[]));
		buf.extend_from_slice(head);
		if buf.len() >= len && buf.len() >= 4 {
			//Completed.
			let t = HandshakeType::new(buf[0]);
			handler.recv_message(ractions, cinfo, t, &buf[4..], payload.len() == 0)?;
			buf.clear();
		}
	}
	//Process whole records.
	while payload.len() >= 4 {
		let len = 4 + payload[1] as usize * 65536 + payload[2] as usize * 256 + payload[3] as usize;
		if let Ok((head, tail)) = split_at(payload, len) {
			//head.len() == len >= 4.
			if head.len() < 4 { break; }
			payload = tail;
			let t = HandshakeType::new(head[0]);
			handler.recv_message(ractions, cinfo, t, &head[4..], payload.len() == 0)?;
		} else { break; }
	}
	//Push the remainder.
	buf.extend_from_slice(payload);
	Ok(())
}

fn decompose_error<A:Display+GetAlert>(x:A) -> (Alert,String)
{
	(x.get_alert(), x.to_string())
}

///TLS Connection.
///
///This object represents an end of TLS connection.
///
///Do not use this type diretly, instead use the alias `Client`, which has:
/// * `get_handshake()` returns mutable reference to [`ClientHandshake`](struct.ClientHandshake.html).
/// * `get_connection_info()` returns reference to[`ClientConnectionInfo`](struct.ClientConnectionInfo.html)
pub struct RecordLayer<H:TlsHandshake>
{
	handshake: H,
	connection_info: <H as TlsHandshake>::ConnectionInfo,
	p_hsmsg: Vec<u8>,
	krypt: BoxedProtector,
	dekrypt: BoxedDeprotector,
	extract: BoxedExtractor,
	record_size_limit: Option<u16>,
	fatal_error: bool,
	req_key_update: bool,
	downstream_encrypted: bool,
	downstream_mac_check_ok: bool,
	downstream_secure_eof: bool,
	downstream_ignore_ccs: bool,
	upstream_eof: bool,
	appdata_ok: bool,
	is_ems: bool,
	extractor_ready: bool,
	no_data: bool,
	enc_overhead: usize,
}

impl<H:TlsHandshake> RecordLayer<H>
{
	///Create a new connection, with specified parameters.
	pub fn new(p: <H as TlsHandshake>::Parameters) -> RecordLayer<H>
	{
		RecordLayer {
			handshake: <H as TlsHandshake>::new(p),
			connection_info: Default::default(),
			p_hsmsg: Vec::new(),
			krypt: BoxedProtector::new_null(),
			dekrypt: BoxedDeprotector::new_null(),
			extract: BoxedExtractor::new_null(),
			record_size_limit: None,		//Record size limit not set.
			fatal_error: false,			//No fatal alert yet.
			req_key_update: false,			//Not requested yet.
			downstream_encrypted: false,		//Downstream not encrypted yet.
			downstream_mac_check_ok: false,		//Downstream MAC check not done.
			downstream_secure_eof: false,		//No EOF yet.
			downstream_ignore_ccs: false,		//Assume TLS 1.2.
			upstream_eof: false,			//No EOF yet.
			appdata_ok: false,			//Not yet.
			is_ems: false,				//Unknown.
			extractor_ready: false,			//Not yet.
			no_data: false,				//Assume data can be used.
			enc_overhead: 5,			//Unencrypted overhead is 5 bytes.
		}
	}
	///Get the underlying handshake object.
	pub fn get_handshake(&mut self) -> &mut H { &mut self.handshake }
	///Get the underlying handshake information.
	pub fn get_connection_info(&self) -> &<H as TlsHandshake>::ConnectionInfo { &self.connection_info }
	///Get maximum transmission unit.
	///
	///This is the largest amount of data that can be transmitted at once. If an attempt to transmit more
	///data than this is made, the result will be short write.
	///
	///Note: The value given is only reliable once handshake has completed (handshake == Complete or
	///tx == Application in the status).
	pub fn get_mtu(&self) -> usize { self.record_size_limit.unwrap_or(16384) as usize }
	///Get maximal amount of overhead per application data transmission.
	///
	///This is the worst-case overhead of single application data write. For example, if maximum overhead is
	///49, then writing 100 bytes of cleartext can return up to 149 bytes of ciphertext.
	///
	///Note: The value given is only reliable once handshake has completed (handshake == Complete or
	///tx == Application in the status).
	pub fn get_max_overhead(&self) -> usize { 2*self.enc_overhead + 5 }
	///Encrypt data for transfer.
	///
	///Note that this results in an error unless handshake has completed (handshake == Complete or
	///tx == Application in the status). It also results in an error after EOF or fatal alert has been sent.
	///Any errors are assumed to be instantly fatal, without so much as even sending out an alert.
	///
	///The size of output buffer should be at least input plus maximum overhead (`get_max_overhead()`) or
	///MTU (`get_mtu()`) plus maximum overhead, whichever is smaller.
	///
	///On success, the return value is amount of data consumed and amount of data produced, in that order.
	///The amount of data consumed can be at most the MTU. Attempts to transmit more will be truncated.
	///
	///There is no padding in the encrypted records, so determining the size of data is easy.
	pub fn transmit_record(&mut self, input: &[u8], output: &mut [u8]) -> Result<(usize, usize), String>
	{
		fail_if!(self.fatal_error, "Record layer is in fatal error state");
		fail_if!(self.upstream_eof, "Upstream EOF has already been sent");
		fail_if!(self.no_data, "No application data may be sent on this connection");
		fail_if!(self.handshake.get_status() != HandshakeStatus::Complete, "Handshake not complete");
		let mut i = 0;
		//Whee key updates...
		if replace(&mut self.req_key_update, false) {
			let msg = [HandshakeType::KEY_UPDATE.get(),0,0,1,0];
			match self.krypt.protect_coutput(output, &msg, ContentType::HANDSHAKE.get(), 5, false) {
				Ok((outc, 5)) => i = outc,
				//The connection is just in too screwed up state.
				_ => fail!("Failed to encrypt KEY_UPDATE")
			}
		}
		let max_payload = self.record_size_limit.unwrap_or(16384) as usize;
		match self.krypt.protect_coutput(&mut output[i..], input,
			ContentType::APPLICATION_DATA.get(), max_payload, false) {
			Ok((outc, inc)) => Ok((inc, outc+i)),
			//It is unlikely encrypting a alert about this would work, so abort.
			_ => fail!("Failed to encrypt application data")
		}
	}
	fn __transmit_alert(&mut self, alert: Alert) -> Vec<u8>
	{
		let mut v = Vec::new();
		//Any errors here are double faults. And clear req_key_update when testing, because
		//__transmit_alert() is also used for closes, which are not fatal.
		if replace(&mut self.req_key_update, false) {
			//Whee, if req_key_update is set, the stream has been rekeyed, but no KEY_UPDATE has
			//been sent yet. We need to send KEY_UPDATE before sending fatal alert. 
			let size = 5 + self.enc_overhead;
			v.resize(size, 0);
			let msg = [HandshakeType::KEY_UPDATE.get(),0,0,1,0];
			match self.krypt.protect_coutput(&mut v, &msg, ContentType::HANDSHAKE.get(), 5, false) {
				Ok((outc,5)) if outc == v.len() => (),
				//Double fault!
				_ => return Vec::new()
			}
		}
		if self.downstream_mac_check_ok {
			let osize = v.len();
			v.resize(osize + 2 + self.enc_overhead, 0);
			let w = &mut v[osize..];
			let level = match alert {
				Alert::CLOSE_NOTIFY|Alert::USER_CANCELED => 1,
				_ => 2,
			};
			let msg = [level,alert.get()];
			match self.krypt.protect_coutput(w, &msg, ContentType::ALERT.get(), 2, false) {
				Ok((outc,2)) if outc == w.len() => (),
				//Double fault!
				_ => return Vec::new()
			}
		} else {
			//Another fun special case: To avoid leaking information, send alerts unencrypted
			//until at least one MAC check passes. These are always fatal since they occur during
			//handshake.
			v.extend_from_slice(&[ContentType::ALERT.get(),3,3,0,2,2,alert.get()]);
		}
		v
	}
	///Handle a fault.
	fn __handle_errc(&mut self, alert: Alert, msg: &str) -> (Vec<u8>, String)
	{
		self.__handle_fault((alert,msg.to_string()))
	}
	fn __handle_fault(&mut self, err: (Alert,String)) -> (Vec<u8>, String)
	{
		self.fatal_error = true;
		let v = self.__transmit_alert(err.0);
		(v,err.1)
	}
	///Transmit a flight.
	///
	///This transmits a handshake flight, or tells why handshake is blocked.
	///
	///Note that this returns an error unless handshake status is either Send (tx == Handshake) or Blocked
	///(handshake == Blocked).
	///
	///In case of error, also returns an alert to send. This alert should be sent to remote peer if possible.
	pub fn transmit_flight(&mut self) ->
		Result<TransmitReturn<<H as TlsHandshake>::BlockInfo>, (Vec<u8>, String)>
	{
		fail_if!(self.fatal_error, (Vec::new(), "Record layer is in fatal error state".to_string()));
		//There better not be any pending data in reassembly buffer, because such data would indicate
		//that the server has sent data for next flight, or after handshake completion. The first is
		//just plain illegal. The second is not possible, as in TLS 1.2 finished is the last handshake
		//message, and in TLS 1.3 it is rquired to end its record.
		fail_if!(self.p_hsmsg.len() > 0, self.__handle_errc(Alert::UNEXPECTED_MESSAGE,
			"Unexpected data in handshake buffer when transmitting"));
		let mut ractions = RecordSendAB {
			set_encryptor: None,
			set_extractor: None,
			set_decryptor: None,
			max_payload: self.record_size_limit.unwrap_or(16384) as usize,
		};
		let r = match self.handshake.transmit(&mut ractions, &mut self.krypt) {
			Ok(r) => r,
			//If handshake failed, instead immediately return the alert to send.
			Err(e) => fail!(self.__handle_fault(decompose_error(e)))
		};
		//In case server sends back HRR, send the second client hello wih TLS 1.2 record version.
		self.krypt.set_modern_version();
		if let Some(p) = ractions.set_decryptor.take() {
			self.downstream_encrypted = true;
			self.dekrypt = p;
		}
		if let Some(p) = ractions.set_encryptor.take() {
			self.enc_overhead = p.get_min_pregap() + p.get_min_postgap();
			self.krypt = p;
		}
		if let Some(p) = ractions.set_extractor.take() {
			self.extract = p;
			self.extractor_ready |= self.is_ems;
		}
		Ok(r)
	}
	///Get size of leading record in buffer in bytes.
	///
	///The return value might be larger than the input buffer. This signals that there are no complete
	///records in the buffer.
	pub fn first_record_size(record: &[u8]) -> usize
	{
		if record.len() < 5 { return 5; }
		let len = 5 + record[3] as usize * 256 + record[4] as usize;
		core::cmp::min(len, 16645)
	}
	///Notify that TCP EOF has been received.
	///
	///If close_notify has been received, this does nothing. Otherwise it raises an error.
	pub fn receive_eof(&mut self) -> Result<(), String>
	{
		if !self.downstream_secure_eof {
			//There is no alert to return, since peer has presumebly gone away.
			self.fatal_error = true;
			fail!("Incoming stream insecurely truncated");
		}
		Ok(())
	}
	///Send a fatal alert message.
	///
	///After calling this, data can be no longer sent or received. Any attempt produces an error.
	///
	///The record containing alert is returned. In case connection is in severly screwed up state, this return
	///value is empty.
	///
	///The data returned should be flushed to the wire and the connection then closed.
	pub fn transmit_fatal_alert(&mut self, alert: Alert) -> Vec<u8>
	{
		if self.fatal_error { return Vec::new(); }
		self.fatal_error = true;
		self.__transmit_alert(alert)
	}
	///Send close notify.
	///
	///Unlike `transmit_fatal_alert()`, this only disallows further writing, not further reading. 
	///
	///The semantics of the return value are the same, except that connection should only be shut down after
	///seeing close_notify in both directions.
	pub fn transmit_eof(&mut self) -> Vec<u8>
	{
		if self.fatal_error || self.upstream_eof { return Vec::new(); }
		self.upstream_eof = true;
		self.__transmit_alert(Alert::CLOSE_NOTIFY)
	}
	///Receive a record.
	///
	///This handles an received record. The `record` parameter should point to exacly one record. See
	///`first_record_size()` on helper routine for that. The maximum possible size of record is 16,645 bytes.
	///
	///Note that the scratch buffer the record is decrypted to must be big enough to contain the decrypted
	///record, including TLS 1.3 encrypted type byte and possible padding. For this, the size of the scratch
	///buffer should be at least the size of the record on wire.
	///
	///This function causes an error after `transmit_fatal_alert()` has been called.
	///
	///The return value of this function is number of application data bytes receive, or `None` if EOF was
	///received. If code only calls `receive_record()` after checking that staus has rx == Handshake, the is
	///no risk of getting nonzero data count, as the same record can not enable appplication data and contain
	///it at the same time. However, this does not hold if code passes multiple records in bathch without
	///rechecking in between.
	///
	///In case of error, also returns an alert to send. This alert should be sent to remote peer if possible.
	pub fn receive_record(&mut self, record: &[u8], scratch: &mut [u8]) ->
		Result<Option<usize>, (Vec<u8>,String)>
	{
		let mut ractions = RecordReceiveAB {
			set_decryptor: None,
			set_encryptor: None,
			set_extractor: None,
			record_size_limit: None,
			//The req_key_update needs to be sticky, since the code can not handle two key updates
			//in one interval.
			req_key_update: self.req_key_update,
			appdata_ok: false,
			ignore_ccs: false,
			is_ems: false,
			no_data: false,
		};
		fail_if!(self.fatal_error, (Vec::new(), "Record layer is in fatal error state".to_string()));
		//Ignore fake TLS 1.3 change cipher spec message. However, do not ignore them anymore after
		//first encrypted packet, as they should not appear then.
		if self.downstream_ignore_ccs && !self.downstream_mac_check_ok &&
			record == &[ContentType::CHANGE_CIPHER_SPEC.get(),3,3,0,1,1] {
			//Just ignore this message, do not even pass it on.
			return Ok(Some(0));
		}
		//Decrypt record. If decryptor has been switched at least once, and decrypt is succesful, mark
		//downstream MAC check as successful.
		let (rtype, plen) = self.dekrypt.deprotect_btb(record, scratch).
			map_err(|_|self.__handle_errc(Alert::BAD_RECORD_MAC, "Record decryption failed"))?;
		self.downstream_mac_check_ok |= self.downstream_encrypted;
		fail_if!(plen > 16384, self.__handle_errc(Alert::RECORD_OVERFLOW, "Decrypted record >16kB"));
		let rtype = ContentType::new(rtype);
		let record = scratch.get(..plen). ok_or_else(||self.__handle_errc(Alert::INTERNAL_ERROR,
			"deprotect_btb return length too large"))?;

		let appdata = match rtype {
			ContentType::HANDSHAKE => {
				handle_handshake_fragment(&mut self.p_hsmsg, &mut self.handshake,
					record, &mut ractions, &mut self.connection_info).
					map_err(|e|self.__handle_fault(decompose_error(e)))?;
				Some(0)
			},
			ContentType::CHANGE_CIPHER_SPEC => {
				self.handshake.recv_change_cipher_spec(&mut ractions).
					map_err(|e|self.__handle_fault(decompose_error(e)))?;
				Some(0)
			},
			ContentType::ALERT => {
				fail_if!(record.len() < 2,
					self.__handle_errc(Alert::DECODE_ERROR, "Alert message too short"));
				//Just ignore level.
				let alert = Alert::new(record[1]);
				let is_close = alert==Alert::CLOSE_NOTIFY||alert==Alert::USER_CANCELED;
				if is_close && self.downstream_encrypted {
					self.downstream_secure_eof = true;
					self.appdata_ok = false;	//No longer OK.
					None
				} else {
					self.fatal_error = true;
					self.handshake.set_status_to_error();
					let msg = format!("Received fatal alert {alert}");
					fail!((Vec::new(), msg));
				}
			},
			ContentType::APPLICATION_DATA if self.appdata_ok && !self.no_data => Some(record.len()),
			ContentType::APPLICATION_DATA =>
				fail!(self.__handle_errc(Alert::UNEXPECTED_MESSAGE, "Unexpected application data")),
			t => {
				let msg = format!("Unknown record type {t}");
				fail!(self.__handle_fault((Alert::UNEXPECTED_MESSAGE, msg)))
			}
		};
		self.req_key_update |= ractions.req_key_update;
		self.appdata_ok |= ractions.appdata_ok;
		self.downstream_ignore_ccs |= ractions.ignore_ccs;
		self.is_ems |= ractions.is_ems;
		self.no_data |= ractions.no_data;
		if let Some(p) = ractions.set_encryptor.take() {
			self.enc_overhead = p.get_min_pregap() + p.get_min_postgap();
			self.krypt = p;
		}
		if let Some(p) = ractions.set_decryptor.take() {
			self.downstream_encrypted = true;
			self.dekrypt = p;
		}
		if let Some(p) = ractions.set_extractor.take() {
			self.extract = p;
			self.extractor_ready |= self.is_ems;
		}
		if let Some(p) = ractions.record_size_limit.take() { self.record_size_limit = Some(p); }
		Ok(appdata)
	}
	///Invoke TLS-EXTRACTOR.
	///
	///Note that this is only available if Extended Master Secret or TLS version 1.3 has been negotiated.
	///For this, it is useful to include `MANDATORY_EMS` in connection flags if this API is to be used.
	///Calling it otherwise results an error.
	///
	///Note that while in TLS 1.2, absent context (`None`) and empty context (`Some(b""))` are not the same,
	///in TLS 1.3 they are. For this reason, TLS 1.3 requires each label to specify if there is context or
	///not.
	///
	///In TLS 1.3, the size of output is limited to 255 hash output blocks (usually 8,160 bytes), and results
	///of different sizes are independent. In TLS 1.2, there is no limit on output length, but shorter outputs
	///are prefixes of longer outputs.
	pub fn tls_extractor(&self, label: &str, context: Option<&[u8]>, buffer: &mut [u8]) -> Result<(), ()>
	{
		dtry!(NORET self.extract.extract(label, context, buffer, &mut None))
	}
	///Get status flags.
	///
	///This returns the status of following things:
	///
	/// * Overall connection failed flag.
	/// * Status of receive and transmit half-connections.
	/// * Status of handshake.
	pub fn get_status_flags(&self) -> RecordLayerStatus
	{
		let hstatus = self.handshake.get_status();
		let tx = if hstatus == HandshakeStatus::Send {
			RxTxPhase::Handshake
		} else if hstatus == HandshakeStatus::Complete && !self.upstream_eof {
			RxTxPhase::Application
		} else if hstatus == HandshakeStatus::Complete {
			RxTxPhase::Shutdown
		} else {
			RxTxPhase::Wait
		};
		let rx = if hstatus == HandshakeStatus::Receive {
			RxTxPhase::Handshake
		} else if self.downstream_secure_eof {
			RxTxPhase::Shutdown
		} else if self.appdata_ok {
			RxTxPhase::Application
		} else {
			RxTxPhase::Wait
		};
		let handshake = match hstatus {
			HandshakeStatus::Send => HandshakePhase::Running,
			HandshakeStatus::Receive => HandshakePhase::Running,
			HandshakeStatus::Blocked => HandshakePhase::Blocked,
			HandshakeStatus::Complete => HandshakePhase::Complete,
			HandshakeStatus::AcmeComplete => HandshakePhase::AcmeComplete,
			HandshakeStatus::Error => HandshakePhase::Failed,
		};
		RecordLayerStatus {
			extractor_ready: self.extractor_ready,
			fatal_error: self.fatal_error,
			tx, rx, handshake
		}
	}
}

