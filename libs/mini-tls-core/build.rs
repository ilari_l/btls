use btls_aux_codegen::BareLiteral;
use btls_aux_codegen::mkid;
use btls_aux_codegen::mkidf;
use btls_aux_codegen::quote;
use btls_aux_memory::Attachment;
use btls_aux_memory::split_attach_first;
use std::collections::BTreeMap;
use std::collections::BTreeSet;
use std::path::Path;
use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::mem::replace;

fn camelcase_to_lowercase(x: &str) -> String
{
	let mut out = String::new();
	for c in x.chars() {
		if c.is_ascii_uppercase() {
			out.push('_');
			out.push(c.to_ascii_lowercase());
		} else {
			out.push(c);
		}
	}
	out
}

fn uppercase_to_lowercase(x: &str) -> String
{
	let mut out = String::new();
	for c in x.chars() {
		out.push(c.to_ascii_lowercase());
	}
	out
}

fn emit(def: &str) -> String
{
	let mut state_name = Vec::new();
	let mut state_fnames = Vec::new();
	let mut fnames_tmp = Vec::new();
	let mut fieldtypes: BTreeMap<&str, &str> = BTreeMap::new();
	let mut action: BTreeMap<&str, &str> = BTreeMap::new();
	let mut states: BTreeSet<&str> = BTreeSet::new();
	let mut state_fields: BTreeMap<&str,BTreeSet<&str>> = BTreeMap::new();
	let mut cstate_fields: BTreeSet<&str> = BTreeSet::new();
	let mut states_transcript: BTreeSet<&str> = BTreeSet::new();
	let mut transmit: Vec<(&str, &str)> = Vec::new();
	let mut receive: Vec<(&str, &str, &str,&str)> = Vec::new();
	let mut ccs: Vec<(&str, &str,&str)> = Vec::new();
	let mut transitions: Vec<(&str, &str, &str)> = Vec::new();
	let mut cstate: Option<&str> = None;
	for line in def.lines() {
		if line.starts_with("#") || line == "" {
			//Skip.
		} else if let Some(line) = line.strip_prefix("FIELDTYPE ") {
			let (k, v) = split_attach_first(line, " ", Attachment::Center).
				expect("No space on FIELDTYPE line");
			fieldtypes.insert(k, v);
		} else if let Some(n) = line.strip_prefix("STATE ") {
			if cstate.is_some() { panic!("STATE already open"); }
			states.insert(n);
			cstate = Some(n);
			cstate_fields = BTreeSet::new();
			state_name.push(mkidf!("St{n}"));
		} else if let Some(n) = line.strip_prefix("FIELD ") {
			let cstate = cstate.expect("STATE not open");
			let t = fieldtypes.get(n).expect(&format!("Not found '{n}' in fieldtypes"));
			if n == "transcript" { states_transcript.insert(cstate); }
			cstate_fields.insert(n);
			let n_sym = mkid(n);
			let t_sym = BareLiteral(t);	//Abuse BareLiteral to print this raw.
			fnames_tmp.push(quote!(#n_sym: #t_sym,));
		} else if let Some(n) = line.strip_prefix("ACTION ") {
			let cstate = cstate.expect("STATE not open");
			action.insert(cstate, n);
		} else if let Some(k) = line.strip_prefix("TRANSMIT ") {
			let cstate = cstate.expect("STATE not open");
			if k != "*" {
				if !states.contains(k) { panic!("No state '{k}' defined"); }
			}
			transmit.push((cstate,k));
		} else if let Some(line) = line.strip_prefix("RECEIVE ") {
			let cstate = cstate.expect("STATE not open");
			let (k, v) = split_attach_first(line, " ", Attachment::Center).
				expect("No space on RECEIVE line");
			let (v, d) = split_attach_first(v, " ", Attachment::Center).
				unwrap_or_else(|v|(v, ""));
			if v != "*" {
				if !states.contains(v) { panic!("No state '{v}' defined"); }
			}
			receive.push((cstate,k,v,d));
		} else if let Some(line) = line.strip_prefix("TRANSITION ") {
			let (tn, line) = split_attach_first(line, " ", Attachment::Center).
				expect("No space on TRANSITION line");
			let (tf, tt) = split_attach_first(line, " ", Attachment::Center).
				expect("No space2 on TRANSITION line");
			transitions.push((tn,tf,tt));
		} else if let Some(k) = line.strip_prefix("CCS ") {
			let cstate = cstate.expect("STATE not open");
			let (k, d) = split_attach_first(k, " ", Attachment::Center).unwrap_or_else(|k|(k, ""));
			if k != "*" {
				if !states.contains(k) { panic!("No state '{k}' defined"); }
			}
			ccs.push((cstate,k,d));
		} else if line == "ENDSTATE" {
			let cstate2 = cstate.expect("STATE not open");
			state_fields.insert(cstate2, replace(&mut cstate_fields, BTreeSet::new()));
			cstate = None;
			state_fnames.push(replace(&mut fnames_tmp, Vec::new()));
		} else {
			panic!("Unrecognized line: {line}");
		}
	}

	let mut macro_name = Vec::new();
	let mut macro_args = Vec::new();
	let mut macro_def = Vec::new();
	let mut state_names = Vec::new();
	let mut state_args = Vec::new();
	let mut ccs_lhs = Vec::new();
	let mut ccs_rhs = Vec::new();
	let mut message_lhs = Vec::new();
	let mut message_rhs = Vec::new();
	let mut transmit_lhs = Vec::new();
	let mut transmit_rhs = Vec::new();
	for (tname,tfrom,tto) in transitions.iter() {
		let mut args = Vec::new();
		let mut fields = Vec::new();
		let xtype = mkidf!("St{tto}");
		for fname in state_fields.get(tto).expect("Transto").iter() {
			let fname_sym = mkid(fname);
			//Abuse BareLiteral to print fname without anything extra.
			let fname_bare = BareLiteral(fname);
			if !state_fields.get(tfrom).expect("Transfrom").contains(fname) {
				args.push(quote!(, #fname_bare: $ #fname_sym:expr));
			}
		}
		for fname in state_fields.get(tto).expect("Transto").iter() {
			let fname_sym = mkid(fname);
			if !state_fields.get(tfrom).expect("Transfrom").contains(fname) {
				fields.push(quote!(#fname_sym: $ #fname_sym));
			} else {
				fields.push(quote!(#fname_sym: $base.#fname_sym));
			}
		}
		macro_name.push(mkid(tname));
		macro_args.push(args);
		macro_def.push(quote!(#xtype { #(#fields,)* }));
	}
	for state in states.iter() {
		state_names.push(mkid(state));
		state_args.push(mkidf!("St{state}"));
	}
	for (src,dst,disc) in ccs.iter() {
		let disc_sym = mkidf!("ccs{disc}");
		let next = if *dst != "*" {
			let dst = mkid(dst);
			quote!(_Handshake::#dst(ret))
		} else {
			quote!(ret)
		};
		ccs_lhs.push(mkid(src));
		ccs_rhs.push(quote!{
			let ret = #disc_sym(x, ractions)?;
			#next
		});
	}
	for (src,msg,dst,disc) in receive.iter() {
		let ts = states_transcript.contains(src);
		let maybe_mut = if ts { quote!(mut) } else { quote!() };
		let src_sym = mkid(src);
		let msg_sym = mkid(msg);
		let update = if ts {
			quote!{
				let u = x.transcript.output();
				t.add(&mut x.transcript);
				let t = u;
			}
		} else {
			quote!()
		};
		let next = if *dst != "*" {
			let dst = mkid(dst);
			quote!(_Handshake::#dst(ret))
		} else {
			quote!(ret)
		};
		let pfn = mkidf!("msg_{msglw}{disc}", msglw=uppercase_to_lowercase(msg));
		message_lhs.push(quote!((_Handshake::#src_sym(#maybe_mut x), HandshakeType::#msg_sym)));
		message_rhs.push(quote!{
			#update
			let ret = #pfn(x, ractions, cinfo, msg, last, t)?;
			#next
		}); 
	}
	for (src, dst) in transmit.iter() {
		let trans_sym = mkidf!("transmit{srclw}", srclw=camelcase_to_lowercase(src));
		let part1 = quote!(let (nstate, msg) = #trans_sym(x, ractions, old_protector)?;);
		let part2 = if *dst != "*" {
			let dst = mkid(dst);
			quote!{
				*h = _Handshake::#dst(nstate);
				TransmitReturn::Transmit(msg)
			}
		} else {
			quote!{
				*h = nstate;
				msg
			}
		};
		transmit_lhs.push(mkid(src));
		transmit_rhs.push(quote!(#part1 #part2));
	}
	let action_src: Vec<_> = action.iter().map(|x|mkid(x.0)).collect();
	let action_dst: Vec<_> = action.iter().map(|x|mkid(x.1)).collect();

	(quote!{
#(struct #state_name { #state_fnames })*
#(macro_rules! #macro_name { ($base:ident #macro_args) => { #macro_def }})*

enum _Handshake
{
	#(#state_names(#state_args),)*
	Error
}

fn __do_change_cipher_spec(h: &mut _Handshake, ractions: &mut RecordReceiveAB) -> Result<(), TlsError>
{
	*h = match replace(h, _Handshake::Error) {
		#(_Handshake::#ccs_lhs(x) => { #ccs_rhs },)*
		_ => fail!(tlserror!([UNEXPECTED_MESSAGE] "Unexpected change cipher spec"))
	};
	Ok(())
}

fn __do_message(h: &mut _Handshake, ractions: &mut RecordReceiveAB, cinfo: &mut ConnectionInfo, mtype: HandshakeType,
	msg: &[u8], last: bool) -> Result<(), TlsError>
{
	let t = TranscriptMessage{mtype: mtype, payload: msg};
	*h = match (replace(h, _Handshake::Error), mtype) {
		#(#message_lhs => { #message_rhs },)*
		(_,t) => fail!(tlserror!([UNEXPECTED_MESSAGE] "Unexpected handshake message {t}"))
	};
	Ok(())
}

fn __do_transmit(h: &mut _Handshake, ractions: &mut RecordSendAB, old_protector: &mut BoxedProtector) ->
	Result<TransmitReturn<Blocked>, TlsError>
{
	let msg = match replace(h, _Handshake::Error) {
		#(_Handshake::#transmit_lhs(x) => { #transmit_rhs },)*
		_ => TransmitReturn::Error
	};
	Ok(msg)
}

fn __do_status(h: &_Handshake) -> HandshakeStatus
{
	if is_handshake_blocked(h) { return HandshakeStatus::Blocked; }
	match h {
		#(_Handshake::#action_src(_) => HandshakeStatus::#action_dst,)*
		_Handshake::Error => HandshakeStatus::Error
	}
}

	}).to_string()
}

fn do_def(src: &Path, dst: &Path)
{
	let mut src = File::open(src).expect("Failed to open source file");
	let mut srcd = String::new();
	src.read_to_string(&mut srcd).expect("Read failed");
	let out = emit(&srcd);
	let mut dst = File::create(dst).expect("Failed to open destination file");
	dst.write_all(out.as_bytes()).expect("Write failed");
}

fn main()
{
	let out_dir = std::env::var("OUT_DIR").unwrap();
	do_def(Path::new("client.def"), &Path::new(&out_dir).join("autogenerated-client.inc.rs"));
}
