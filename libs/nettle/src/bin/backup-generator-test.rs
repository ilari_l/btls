extern crate btls_aux_nettle;
extern crate btls_aux_random;
use btls_aux_nettle::mix_secondary_stream;
use btls_aux_random::RandomStream;
use std::io::Write;
use std::io::stdout;

fn main()
{
	let mut tmp = [0u8; 16383];
	let key = [0;64];
	let mut s = RandomStream::new_with_seed(&key);
	loop {
		for i in tmp.iter_mut() { *i = 0; }
		mix_secondary_stream(&mut tmp, &mut s);
		stdout().write_all(&tmp).unwrap();
	}
}
