use super::AES128GCM_NAME;
use super::AES256GCM_NAME;
use super::EccCurve;
use super::EccScalar;
use super::GcmContext;
use super::NsaEcdhKey;
use super::Sha3256Ctx2;
use btls_aux_collections::Vec;
use btls_aux_random::TemporaryRandomLifetimeTag;
use btls_aux_random::TemporaryRandomStream;

#[test]
fn sha3_clienthello()
{
	const DATA: [u8;226] = [
		0x01, 0x00, 0x00, 0xDE, 0x03, 0x03, 0x7E, 0x89, 0x86, 0xB3, 0x3C, 0x04, 0x52, 0xE7, 0x9F, 0xB7,
		0x16, 0x48, 0x90, 0x49, 0xDA, 0x38, 0x0C, 0x6C, 0x94, 0xB8, 0x9F, 0xC0, 0xA5, 0x02, 0xD2, 0x8C,
		0x2F, 0xDA, 0xA6, 0xD8, 0x45, 0x9F, 0x00, 0x00, 0x18, 0xCC, 0xA9, 0xC0, 0x2B, 0xC0, 0x2C, 0xCC,
		0xA8, 0xC0, 0x2F, 0xC0, 0x30, 0xFF, 0x02, 0x13, 0x03, 0xFF, 0x00, 0x13, 0x01, 0xFF, 0x01, 0x13,
		0x02, 0x01, 0x00, 0x00, 0x9D, 0x00, 0x2B, 0x00, 0x0F, 0x0E, 0x7F, 0x16, 0x7F, 0x15, 0x7F, 0x14,
		0x7F, 0x13, 0x7F, 0x12, 0x7F, 0x11, 0x03, 0x03, 0x00, 0x0A, 0x00, 0x0C, 0x00, 0x0A, 0x00, 0x1D,
		0x00, 0x17, 0x00, 0x1E, 0x00, 0x18, 0x00, 0x19, 0x00, 0x28, 0x00, 0x26, 0x00, 0x24, 0x00, 0x1D,
		0x00, 0x20, 0x21, 0x81, 0x82, 0x9E, 0x18, 0xDC, 0x19, 0x45, 0x61, 0xFF, 0x38, 0xF9, 0x56, 0xE6,
		0x67, 0x56, 0x9A, 0x55, 0x23, 0x5F, 0x27, 0xD4, 0xDA, 0x00, 0x9B, 0xC1, 0x7C, 0x2A, 0x61, 0x37,
		0xD9, 0x23, 0x00, 0x00, 0x00, 0x0D, 0x00, 0x0B, 0x00, 0x00, 0x08, 0x66, 0x6F, 0x6F, 0x2E, 0x74,
		0x65, 0x73, 0x74, 0x00, 0x14, 0x00, 0x03, 0x02, 0x02, 0x00, 0x00, 0x05, 0x00, 0x05, 0x01, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x12, 0x00, 0x00, 0x00, 0x0D, 0x00, 0x18, 0x00, 0x16, 0x08, 0x07, 0x08,
		0x08, 0x04, 0x03, 0x05, 0x03, 0x06, 0x03, 0x08, 0x06, 0x08, 0x05, 0x08, 0x04, 0x06, 0x01, 0x05,
		0x01, 0x04, 0x01, 0x00, 0x17, 0x00, 0x00, 0xFF, 0x01, 0x00, 0x01, 0x00, 0x10, 0x53, 0x00, 0x02,
		0x40, 0x00
	];
	const HASH: [u8;32] = [
		0x9A, 0xD0, 0x07, 0x26, 0x9D, 0x64, 0x9A, 0xCC, 0xAE, 0x89, 0xE2, 0xE5, 0x2A, 0xDA, 0x2B, 0xF4,
		0x2A, 0xCA, 0x10, 0xB8, 0x16, 0x58, 0xDA, 0x9D, 0x05, 0x35, 0xC4, 0x39, 0x01, 0x46, 0xF0, 0x53
	];
	let mut h = Sha3256Ctx2::new();
	h.input(&DATA[..]);
	let h = h.output();
	assert_eq!(&h[..], &HASH[..]);
}

#[test]
fn noisy_ecdsa()
{
	//The conditions are:
	//1) Changing nothing should give identical signature.
	//2) Changing the key changes the first half.
	//3) Changing the message changes the first half.
	//4) Changing random seed changes the first half.
	let mut key1 = EccScalar::new(EccCurve::new(0).unwrap()); key1.load(&[1;32]).unwrap();
	let mut key2 = EccScalar::new(EccCurve::new(0).unwrap()); key2.load(&[2;32]).unwrap();

	let mut sig1 = [0;64];
	let mut sig2 = [0;64];
	let mut sig3 = [0;64];
	let mut sig4 = [0;64];
	let mut sig5 = [0;64];
	key1.ecdsa_sign_hash(&[3;32], &mut sig1, &mut TemporaryRandomStream::new(&[125;48])).unwrap();
	key1.ecdsa_sign_hash(&[3;32], &mut sig2, &mut TemporaryRandomStream::new(&[125;48])).unwrap();
	key2.ecdsa_sign_hash(&[3;32], &mut sig3, &mut TemporaryRandomStream::new(&[125;48])).unwrap();
	key1.ecdsa_sign_hash(&[4;32], &mut sig4, &mut TemporaryRandomStream::new(&[125;48])).unwrap();
	key1.ecdsa_sign_hash(&[3;32], &mut sig5, &mut TemporaryRandomStream::new(&[126;48])).unwrap();
	assert_eq!(&sig1[..], &sig2[..]);
	let r = [&sig2[..32], &sig3[..32], &sig4[..32], &sig5[..32]];
	for i in 0..4 {
		for j in 0..4 {
			assert_eq!(r[i] == r[j], i == j);
		}
	}
}

#[cfg(test)]
fn hex_to_vector(x: &str) -> Vec<u8>
{
	let mut out = Vec::new();
	let mut parity = false;
	let mut tmp = 0;
	for i in x.chars() {
		let val = match i {
			'0' => 0, '1' => 1, '2' => 2, '3' => 3, '4' => 4,
			'5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9,
			'a' => 10, 'b' => 11, 'c' => 12, 'd' => 13, 'e' => 14, 'f' => 15,
			'A' => 10, 'B' => 11, 'C' => 12, 'D' => 13, 'E' => 14, 'F' => 15,
			_ => { continue; }
		} as u8;
		if !parity {
			tmp = val << 4;
		} else {
			tmp |= val;
			out.push(tmp);
		}
		parity = !parity;
	}
	out
}

#[test]
fn aes_gcm_test_vector1()
{
	let key = hex_to_vector("AD7A2BD03EAC835A6F620FDCB506B345");
	let ad = hex_to_vector("D609B1F056637A0D46DF998D88E5222A
		B2C2846512153524C0895E8108000F10
		1112131415161718191A1B1C1D1E1F20
		2122232425262728292A2B2C2D2E2F30
		313233340001");
	let iv = hex_to_vector("12153524C0895E81B2C28465");
	let mut ciphertext = hex_to_vector("F09478A9B09007D06F46E9B6A1DA25DD");
	let ctx = GcmContext::new_context(&key, AES128GCM_NAME).unwrap();
	assert_eq!(unsafe{ctx.decrypt3(&iv, &ad, ciphertext.as_mut_ptr(), ciphertext.as_ptr(), 16)}, Ok(0));
}

#[test]
fn aes_gcm_test_vector2()
{
	let key = hex_to_vector("E3C08A8F06C6E3AD95A70557B23F7548
		3CE33021A9C72B7025666204C69C0B72");
	let ad = hex_to_vector("D609B1F056637A0D46DF998D88E5222A
		B2C2846512153524C0895E8108000F10
		1112131415161718191A1B1C1D1E1F20
		2122232425262728292A2B2C2D2E2F30
		313233340001");
	let iv = hex_to_vector("12153524C0895E81B2C28465");
	let mut ciphertext = hex_to_vector("2F0BC5AF409E06D609EA8B7D0FA5EA50");
	let ctx = GcmContext::new_context(&key, AES256GCM_NAME).unwrap();
	assert_eq!(unsafe{ctx.decrypt3(&iv, &ad, ciphertext.as_mut_ptr(), ciphertext.as_ptr(), 16)}, Ok(0));
}

#[test]
fn nsa_ecdh_load_save()
{
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let crv = EccCurve::new(0).unwrap();
	let mut p = [0;65];
	let (a, _) = NsaEcdhKey::new(crv, &mut p, &mut rng).unwrap();
	let mut _a = [0;32];
	a.save(&mut _a).unwrap();
	let b = NsaEcdhKey::load(crv, &_a).unwrap();
	let (_, plen) = NsaEcdhKey::new(crv, &mut p, &mut rng).unwrap();
	let p = &mut p[..plen];
	let mut r1 = [0;65];
	let mut r2 = [0;65];
	a.agree(&mut r1, p).unwrap();
	b.agree(&mut r2, p).unwrap();
	assert_eq!(&r1[..], &r2[..]);
}

#[test]
fn nsa_ecdh_load_save2()
{
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let crv = EccCurve::new(2).unwrap();
	let mut p = [0;133];
	let (a, _) = NsaEcdhKey::new(crv, &mut p, &mut rng).unwrap();
	let mut _a = [0;66];
	a.save(&mut _a).unwrap();
	let b = NsaEcdhKey::load(crv, &_a).unwrap();
	let (_, plen) = NsaEcdhKey::new(crv, &mut p, &mut rng).unwrap();
	let p = &mut p[..plen];
	let mut r1 = [0;133];
	let mut r2 = [0;133];
	a.agree(&mut r1, p).unwrap();
	b.agree(&mut r2, p).unwrap();
	assert_eq!(&r1[..], &r2[..]);
}

fn nsa_ecdh_test<const SIZE: usize>(crvid: u32)
{
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let crv = EccCurve::new(crvid).unwrap();
	let mut p1 = [0;SIZE];
	let mut p2 = [0;SIZE];
	let (a1, s1) = NsaEcdhKey::new(crv, &mut p1, &mut rng).unwrap();
	let (a2, s2) = NsaEcdhKey::new(crv, &mut p2, &mut rng).unwrap();
	assert!(p1 != p2 &&  s1 == SIZE && s2 == SIZE);
	let mut r1 = [0;SIZE];
	let mut r2 = [0;SIZE];
	let s1 = a1.agree(&mut r1, &p2).unwrap();
	let s2 = a2.agree(&mut r2, &p1).unwrap();
	assert!(r1 == r2 && s1 == SIZE && s2 == SIZE);
}

#[test]
fn nsa_p256_ecdh()
{
	nsa_ecdh_test::<65>(0);
}

#[test]
fn nsa_p384_ecdh()
{
	nsa_ecdh_test::<97>(1);
}

#[test]
fn nsa_p521_ecdh()
{
	nsa_ecdh_test::<133>(2);
}
