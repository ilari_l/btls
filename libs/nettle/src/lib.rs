//!Interface to Nettle cryptographic library.
//!
//!This crate provodes an interface into the Nettle cryptographic library. Especially covers hash functions,
//!AES-GCM, RSA and ECDSA.
//!
//!The most important items are:
//!
//! * Low-level encryption/decryption contexts:
//!   * For AES-128-GCM: [`Aes128GcmContext`](struct.Aes128GcmContext.html)
//!   * For AES-192-GCM: [`Aes192GcmContext`](struct.Aes192GcmContext.html)
//!   * For AES-256-GCM: [`Aes256GcmContext`](struct.Aes256GcmContext.html)
//! * Elliptic-curve cryptography:
//!   * A curve: [`EccCurve`](struct.EccCurve.html)
//!   * A point on elliptic curve (frequently used as EC public key): [`EccPoint`](struct.EccPoint.html)
//!   * A scalar for elliptic curve point multiplication (frequently used as EC private key): [`EccScalar`]
//!(struct.EccScalar.html)
//!   * NSA-curve ECDH private key (implements NSA-curve ECDH): [`NsaEcdhKey`](struct.NsaEcdhKey.html)
//! * RSA
//!   * A private key: [`RsaPrivate`](struct.RsaPrivate.html)
//!   * A public key: [`RsaPublic`](struct.RsaPublic.html)
//!   * Generate RSA key: [`generate_rsa_key()`](fn.generate_rsa_key.html)
//!   * Compute RSA public key operation: [`rsa_compute_power()`](fn.rsa_compute_power.html)
//!   * Compute RSA private key operation: [`rsa_compute_root()`](fn.rsa_compute_root.html)
//! * Hashes:
//!   * A context for SHA-1: [`Sha1Ctx`](struct.Sha1Ctx.html)
//!   * A context for SHA-256: [`Sha256Ctx`](struct.Sha256Ctx.html)
//!   * A context for SHA-384: [`Sha384Ctx`](struct.Sha384Ctx.html)
//!   * A context for SHA-512: [`Sha512Ctx`](struct.Sha512Ctx.html)
//!   * A context for SHA3-256: [`Sha3256Ctx`](struct.Sha3256Ctx.html)
//!   * A context for SHA3-384: [`Sha3384Ctx`](struct.Sha3384Ctx.html)
//!   * A context for SHA3-512: [`Sha3512Ctx`](struct.Sha3512Ctx.html)
#![allow(improper_ctypes)]
#![forbid(missing_docs)]
#![allow(unsafe_code)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]

#[cfg(test)] #[macro_use] extern crate std;
use btls_aux_aead_base::AeadError;
pub use btls_aux_aead_base::LowLevelAeadContext;
use btls_aux_collections::Vec;
use btls_aux_fail::dtry;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_memory::calculate_syndrome;
use btls_aux_memory::rsplit_at;
use btls_aux_memory::slice_fill;
use btls_aux_memory::slice_splitn_at;
use btls_aux_memory::slice_splitn_at_mut;
use btls_aux_nettle_cimpl::EccCurve as EccCurveLow;
use btls_aux_nettle_cimpl::_rsa_compute_root2;
use btls_aux_nettle_cimpl::Mpz as MpzLow;
use btls_aux_nettle_cimpl::RsaPrivate as RsaPrivateLow;
use btls_aux_nettle_cimpl::RsaPublic as RsaPublicLow;
use btls_aux_nettle_cimpl::GcmKey as GcmKeyLow;
use btls_aux_nettle_cimpl::GcmCtx as GcmCtxLow;
use btls_aux_nettle_cimpl::AesCtx as AesCtxLow;
use btls_aux_nettle_cimpl::EccPoint as EccPointLow;
use btls_aux_nettle_cimpl::EccScalar as EccScalarLow;
use btls_aux_nettle_cimpl::DsaSignature as DsaSignatureLow;
use btls_aux_nettle_cimpl::Sha1Ctx as Sha1CtxLow;
use btls_aux_nettle_cimpl::Sha256Ctx as Sha256CtxLow;
use btls_aux_nettle_cimpl::Sha3256Ctx as Sha3256CtxLow;
use btls_aux_nettle_cimpl::Sha3384Ctx as Sha3384CtxLow;
use btls_aux_nettle_cimpl::Sha3512Ctx as Sha3512CtxLow;
use btls_aux_nettle_cimpl::Sha384Ctx as Sha384CtxLow;
use btls_aux_nettle_cimpl::Sha512Ctx as Sha512CtxLow;
use btls_aux_random::RandomStream;
use btls_aux_random::TemporaryRandomStream;
use btls_aux_rope3::FragmentWriteOps;
use btls_aux_rope3::rope3;
use core::cmp::min;
use core::slice::from_raw_parts;
use core::slice::from_raw_parts_mut;


macro_rules! RANDOM
{
	($s:expr) => { |buf|$s.fill_bytes(buf) };
}

fn mpz_bytes(n: &MpzLow) -> usize
{
	(n.sizeinbase(2) + 7) / 8
}

fn mpz_bytes_asn1(n: &MpzLow) -> usize
{
	//Add +1 so that numbers that would otherwise be negative get extra leading zero to denote the numbers
	//are positive.
	(n.sizeinbase(2) + 8) / 8
}

//This is public for testing.
//Stores the SHA-512 hash of `data` into `seedbuf`. Used to compress down larger chunks for seeding.
#[doc(hidden)]
pub fn mix_secondary_init(seedbuf: &mut [u8;64], data: &[u8])
{
	let mut sstate = Sha512CtxLow::init();
	sstate.update(data);
	sstate.digest(seedbuf);
}

//This is public for testing.
//Stores the SHA-512 hash of concatnation of `data1` and `data2` into output.
#[doc(hidden)]
pub fn mix_secondary_seed(output: &mut [u8;64], data1: &[u8], data2: &[u8])
{
	let mut sstate = Sha512CtxLow::init();
	sstate.update(data1);
	sstate.update(data2);
	sstate.digest(output);
}

//This is public for testing.
//Extract enough data from the specified random buffer `RandomStream` to be equal in size to `output` and then XOR
//it bitwise into `output`.
#[doc(hidden)]
pub fn mix_secondary_stream(output: &mut [u8], stream: &mut RandomStream)
{
	let mut ptr = 0;
	let mut tmp = [0u8;256];
	while ptr < output.len() {
		let left = min(256, output.len() - ptr);
		stream.fill_bytes(&mut tmp[..left]);
		for i in 0..left { output[ptr+i] ^= tmp[i]; }
		ptr += left;
	}
}

//Extension trait so we can implement method on types of other crates.
trait MpzExportImport
{
	fn import_bigendian(&mut self, data: &[u8]);
	//Pads from left with zeroes.
	fn export_bigendian(&self, data: &mut [u8]) -> Result<(), ()>;
}

impl MpzExportImport for MpzLow
{
	fn import_bigendian(&mut self, data: &[u8])
	{
		//Load data.len() 1 byte words, with no padding and big-endian byte order.
		self.import(data, 1, 0);
	}
	fn export_bigendian(&self, data: &mut [u8]) -> Result<(), ()>
	{
		//Pad left with zeroes, export to right. How many bytes we need?
		let need_bytes = mpz_bytes(self);
		let (head, tail) = dtry!(rsplit_at(data, need_bytes));
		slice_fill(head, 0);
		self.export(tail, 1, 0);
		Ok(())
	}
}

///The MPZ type. This wraps the mpz_t type of Nettle.
#[repr(C)]
struct Mpz(MpzLow);

unsafe impl Sync for Mpz {}

impl Mpz
{
	//Initialize the element.
	pub fn new() -> Mpz { Mpz(MpzLow::new()) }
	//Import a big-endian octet string as unsigned integer.
	pub fn import_bigendian(&mut self, data: &[u8]) { self.0.import_bigendian(data) }
	//Export a big-endian number. Pads from left with zeroes, errors out if not enough space.
	pub fn export_bigendian(&self, data: &mut [u8]) -> Result<(), ()> { self.0.export_bigendian(data) }
}

///An RSA public key.
///
///This structure is created by the [`new()`](#method.new) method.
#[repr(C)]
pub struct RsaPublic(RsaPublicLow);
//Inside:
//size: 0 means invalid key.
//n: The RSA modulus n.
//e: The RSA public exponent e.

unsafe impl Sync for RsaPublic {}

impl RsaPublic
{
	///Create a new RSA public key.
	///
	///Initially, the value of the public key is undefined. As consequence, until the RSA parameters are loaded,
	///all operations on the public key fail.
	pub fn new() -> RsaPublic
	{
		RsaPublic(RsaPublicLow::new())
	}
	///Load specified RSA parameters as public key.
	///
	///After the parameters have been loaded, operations on the public key are possible.
	///
	///The parameters are:
	///
	/// * `n`: The RSA modulus, represented as big-endian base-256 integer.
	/// * `e`: The RSA public exponent, represented as big-endian base-256 integer.
	///
	///The `n` must be at least 256 and at most 1025 octets, The `n` must be at least 2^2040 and `e` must be
	///non-empty and at most length of `n`. Breaking any of these causes this function to fail.
	///
	///On success, returns `Ok(())`. If operation fails, returns `Err(())`.
	pub fn load(&mut self, n: &[u8], e: &[u8]) -> Result<(), ()>
	{
		fail_if!(n.len() < 256 || n.len() > 1025, ());
		fail_if!(e.len() < 1 || e.len() > n.len(), ());

		//Check that n is big enough. For easiness, check that n is at least 2^2040, by checking that OR of
		//all bytes except last 255 is nonzero.
		let magcheck = n.len() - 255;	//n.len() >= 256.
		let mag = n[0..magcheck].iter().fold(0, |x,y|x|*y);
		fail_if!(mag == 0, ());

		self.0.n.import_bigendian(n);
		self.0.e.import_bigendian(e);
		self.0.size = 0;
		self.0.key_prepare()?;
		Ok(())
	}
	///Export the public key as X.509 SubjectPublicKeyInfo DER representation.
	///
	///On success, returns `Ok(spki)`, where `spki` is the DER serialization of SubjectPublicKeyInfo structure.
	///If the key is uninitialized or public exponent is too big, returns `Err(())`.
	pub fn as_rsa_spki(&self) -> Result<Vec<u8>, ()>
	{
		fail_if!(self.0.size == 0, ());	//Don't do this for undefined keys.
		//1032B should be big enough. It is enough for full 8192-bit public key.
		let mut tmpa = [0;1032];
		let mut tmpb = [0;32];
		let need_bytes_n = mpz_bytes_asn1(&self.0.n);
		let need_bytes_e = mpz_bytes_asn1(&self.0.e);
		fail_if!(need_bytes_n > tmpa.len(), ());
		fail_if!(need_bytes_e > tmpb.len(), ());
		let r = rope3!(SEQUENCE(
			SEQUENCE(OBJECT_IDENTIFIER RSA_KEY_OID, NULL),
			BIT_STRING SEQUENCE(
				INTEGER{
					//In-range by check above. We bump the size one bit early,
					//which causes the leading zero be added instead of making
					//the value negative.
					let tmp2 = &mut tmpa[..need_bytes_n];
					self.0.n.export_bigendian(tmp2)?;
					&tmp2[..]
				},
				INTEGER{
					//In-range by check above. We bump the size one bit early,
					//which causes the leading zero be added instead of making
					//the value negative.
					let tmp2 = &mut tmpb[..need_bytes_e];
					self.0.e.export_bigendian(tmp2)?;
					&tmp2[..]
				}
			)
		));
		Ok(FragmentWriteOps::to_vector(&r))
	}
	///Obtain the size of the RSA signature for this key, in bytes.
	///
	///If key is undefined, returns 0.
	pub fn get_size(&self) -> usize { self.0.size }
}

///An RSA private key.
///
///This structure is created by the [`new()`](#method.new) method.
#[repr(C)]
pub struct RsaPrivate(RsaPrivateLow);

unsafe impl Sync for RsaPrivate {}

impl RsaPrivate
{
	///Create a new RSA private key.
	///
	///Initially, the value of the private key is undefined. As consequence, until the RSA parameters are loaded,
	///all operations on the private key fail.
	pub fn new() -> RsaPrivate { RsaPrivate(RsaPrivateLow::new()) }
	///Loads specified RSA parameters as private key.
	///
	///After the parameters have been loaded, operations on the private key are possible.
	///
	///The parameters are:
	///
	/// * `d`: The RSA private exponent, represented as big-endian base-256 integer.
	/// * `p`: The first prime factor of the RSA modulus, represented as big-endian base-256 integer.
	/// * `q`: The second prime factor of the RSA modulus, represented as big-endian base-256 integer.
	/// * `dp`: The `d % (p - 1)`, represented as big-endian base-256 integer.
	/// * `dq`: The `d % (q - 1)`, represented as big-endian base-256 integer.
	/// * `qi`: The `q^-1 (mod p)`, represented as big-endian base-256 integer.
	///
	///On success, returns `Ok(())`. If the parameters do not match up, returns 'Err(())`.
	pub fn load(&mut self, d: &[u8], p: &[u8], q: &[u8], dp: &[u8], dq: &[u8], qi: &[u8]) -> Result<(), ()>
	{
		self.0.d.import_bigendian(d);
		self.0.p.import_bigendian(p);
		self.0.q.import_bigendian(q);
		self.0.dp.import_bigendian(dp);
		self.0.dq.import_bigendian(dq);
		self.0.qi.import_bigendian(qi);
		self.0.size = 0;
		self.0.key_prepare()?;
		Ok(())
	}
	///Obtain the size of the RSA signature for this key, in bytes.
	///
	///If the key is undefined, returns 0.
	pub fn get_size(&self) -> usize { self.0.size }
}

///Generate an RSA keypair.
///
///The generated RSA keypair will have the specified number of bits `bits` and will be stored using internal
///representation to the given output buffer `output`.
///
///Only lengths of 2048 to 4096 in multiples of 8 are supported. Other sizes cause this function to fail. The
///output buffer also has to be big enough. The worst-case value is: 8 + 9 * `bits` / 16
///
///On success, returns `Ok(size)`, where `size` is the size of the internal representation in bytes. On error,
///returns `Err(())`.
pub fn generate_rsa_key(bits: usize, output: &mut [u8], rng: &mut TemporaryRandomStream) -> Result<usize, ()>
{
	fail_if!(bits < 2048 || bits > 4096 || bits % 8 != 0, ());
	let mut pubkey = RsaPublic::new();
	let mut privkey = RsaPrivate::new();
	//We need to load the pubkey.e. Load 65537, as that is the usual RSA prime, altough 3 would be better.
	pubkey.0.e.import_bigendian(&[1,0,1]);
	privkey.0.generate_keypair(&mut pubkey.0, bits as u32, 0, RANDOM!(rng), |_|())?;
	//The number of bytes to reserve for exports.
	let lbytes = pubkey.0.size;
	let ebytes = mpz_bytes(&pubkey.0.e);
	let sbytes = (lbytes + 1) / 2;
	//RSA key serialization is formed of 2 byte length of large component, 2 large components, 5 small
	//components and exponent.
	let lengths = (2usize, lbytes, lbytes, sbytes, sbytes, sbytes, sbytes, sbytes, ebytes);
	let ((out_hdr, out_n, out_d, out_p, out_q, out_dp, out_dq, out_qi, out_e), ser_size) =
		dtry!(slice_splitn_at_mut(output, lengths));
	out_hdr[0] = (lbytes >> 8) as u8;
	out_hdr[1] = lbytes as u8;
	pubkey.0.n.export_bigendian(out_n)?;
	pubkey.0.e.export_bigendian(out_e)?;
	privkey.0.d.export_bigendian(out_d)?;
	privkey.0.p.export_bigendian(out_p)?;
	privkey.0.q.export_bigendian(out_q)?;
	privkey.0.dp.export_bigendian(out_dp)?;
	privkey.0.dq.export_bigendian(out_dq)?;
	privkey.0.qi.export_bigendian(out_qi)?;
	Ok(ser_size)
}

//Why was this exposed?
#[doc(hidden)]
#[repr(C)]
pub struct DsaSignature(DsaSignatureLow);

///A ECC curve.
///
///This structure is created by the [`new()`](#method.new) method.
#[repr(C)]
#[derive(Copy,Clone)]
pub struct EccCurve(&'static EccCurveLow, u32, usize);

unsafe impl Send for EccCurve {}	//These are actually immutable, so safe for inter-thread access.
unsafe impl Sync for EccCurve {}

impl EccCurve
{
	///Create a new ECC curve object by curve identifier `id`.
	///
	///If the curve is known, returns `Ok(curve)`, where `curve` is the curve. Otherwise returns `Err(())`.
	///
	///# Known curve identifiers:
	///
	/// Identifier|Curve
	/// ----------|----------------------------------
	/// 0         |NSA P-256 / NIST P-256 / secp256r1
	/// 1         |NSA P-384 / NIST P-384 / secp384r1
	/// 2         |NSA P-521 / NIST P-512 / secp521r1
	pub fn new(id: u32) -> Result<EccCurve, ()>
	{
		Ok(match id {
			0 => EccCurve(EccCurveLow::secp_256r1(), id, 32),
			1 => EccCurve(EccCurveLow::secp_384r1(), id, 48),
			2 => EccCurve(EccCurveLow::secp_521r1(), id, 66),
			_ => fail!(())
		})
	}
	///Generate an ECC keypair.
	///
	///The resulting keypair is stored in internal format into the given buffer `output`.
	///
	///Currently, the largest possible output is 199 bytes. The exact output size can be calculated by
	///taking the point size given by [`point_size()`](#method.point_size) and multiplying it by `1.5`, rounding
	///down.
	///
	///On success, returns `Ok(size)`, where `size` is the size of the key in bytes. If the buffer is not big
	///enough, returns `Err(())`.
	///
	///# Current required sizes:
	///
	/// Curve     |Required size
	/// ----------|-------------
	/// NSA P-256 |97
	/// NSA P-384 |145
	/// NSA P-521 |199
	pub fn generate(&self, output: &mut [u8], rng: &mut TemporaryRandomStream) -> Result<usize, ()>
	{
		let mut pubkey = EccPoint::new(*self);
		let mut privkey = EccScalar::new(*self);
		privkey.inner.ecdsa_generate_keypair(&mut pubkey.inner, RANDOM!(rng));
		//We assume scalars are the same size as base field. That's certainly true for NIST curves.
		let cbytes = self.2;
		let lengths = (1usize, cbytes, cbytes, cbytes);
		let ((out_hdr, out_d, out_x, out_y), ser_size) = dtry!(slice_splitn_at_mut(output, lengths));
		//Serialize pubkey and privkey.
		out_hdr[0] = 0;		//All current curves have variant 0.
		let mut tmp1 = Mpz::new();
		let mut tmp2 = Mpz::new();
		privkey.inner.get(&mut tmp1.0);
		tmp1.export_bigendian(out_d)?;
		pubkey.inner.get(&mut tmp1.0, &mut tmp2.0);
		tmp1.export_bigendian(out_x)?;
		tmp2.export_bigendian(out_y)?;
		Ok(ser_size)
	}
	///Obtain the size of uncompressed points on this curve, in bytes.
	///
	///Currently, the largest possible return value is `133` bytes.
	///
	///# Current values:
	///
	/// Curve     |Point size
	/// ----------|-------------
	/// NSA P-256 |65
	/// NSA P-384 |97
	/// NSA P-521 |133
	pub fn point_size(&self) -> usize { 1 + 2 * self.2 }
}

///A point on ECC curve.
///
///Points are often used to represent public keys.
///
///This structure is created by the [`new()`](#method.new) method.
#[repr(C)]
pub struct EccPoint
{
	inner: EccPointLow,
	//The EccPoint mirrors a Nettle internal structure, so the first two fields have to be as above.
	//But grab a space for extra flag.
	undef: u8,
}

unsafe impl Send for EccPoint {}
unsafe impl Sync for EccPoint {}

impl EccPoint
{
	///Create a new uninitialized point object on curve `crv`.
	///
	///Because the point is uninitialized, all opreations on it fail until the parameters are loaded.
	pub fn new(crv: EccCurve) -> EccPoint
	{
		EccPoint{
			inner: EccPointLow::new(crv.0),
			undef: 1
		}
	}
	///Load ECC point coordinates `_x` and `_y` (as big-endian base-256 integers) as point paramters.
	///
	///After this has been successfully called on point, operations can be performed on the point.
	///
	///The coordinates are checked for being on curve.
	///
	///On success, returns `Ok(())`. If the specified coordinates are not on the curve, returns `Err(())`.
	pub fn load(&mut self, _x: &[u8], _y: &[u8]) -> Result<(), ()>
	{
		let mut x = Mpz::new();
		let mut y = Mpz::new();
		x.import_bigendian(_x);
		y.import_bigendian(_y);
		self.undef = 1;
		self.inner.set(&x.0, &y.0)?;
		self.undef = 0;
		Ok(())
	}
	///Verify ECDSA signature `r` and `s` for specified hash `hash` using this point as a public key.
	///
	///If validation is successful, returns: `Ok(())`. If validation fails, returns `Err(())`.
	pub fn ecdsa_verify_hash(&self, hash: &[u8], r: &[u8], s: &[u8]) -> Result<(), ()>
	{
		fail_if!(self.undef != 0, ());
		let mut sig = DsaSignature(DsaSignatureLow::new());
		sig.0.r.import_bigendian(r);
		sig.0.s.import_bigendian(s);
		self.inner.ecdsa_verify(hash, &sig.0)?;
		Ok(())
	}
	///Export ECC point coordinates to `x` and `y`.
	///
	///The size required for `x` and `y` is the size of point serialization (as returned by
	///[`EccCurve::point_size`](struct.EccCurve.html#method.point_size)) divided by 2, rounded down. The values
	///are written in big-endian base-256 nottation.
	///
	///On success, returns `Ok(())`. If the point is undefined or buffers are too small, returns `Err(())`.
	///
	///# Required sizes:
	///
	/// Curve     | Required size (bytes per component)
	/// ----------|------------------------------------
	/// NSA P-256 | 32
	/// NSA P-384 | 48
	/// NSA P-521 | 66
	pub fn export(&self, x: &mut [u8], y: &mut [u8]) -> Result<(), ()>
	{
		fail_if!(self.undef != 0, ());
		let mut rx = Mpz::new();
		let mut ry = Mpz::new();
		self.inner.get(&mut rx.0, &mut ry.0);
		rx.export_bigendian(x)?;
		ry.export_bigendian(y)?;
		Ok(())
	}
}

///A scalar multiplier on ECC curve.
///
///Scalar multipliers are often used to represent private keys.
///
///This structure is created by the [`new()`](#method.new) method.
#[repr(C)]
pub struct EccScalar
{
	inner: EccScalarLow,
	//Keep other members below the above two, as those are used by Nettle itself.
	csize: usize,
	undef: u8,
	seed: [u8;64],
}

unsafe impl Send for EccScalar {}
unsafe impl Sync for EccScalar {}

impl EccScalar
{
	///Create a new uninitialized scalar object.
	///
	///Because the scalar is uninitialized, all opreations on it fail until the parameters are loaded.
	pub fn new(crv: EccCurve) -> EccScalar
	{
		EccScalar{
			inner: EccScalarLow::new(crv.0),
			csize: crv.2,
			undef: 1,
			seed: [0u8; 64]
		}
	}
	///Load an integer `_d` (big-endian base-256 value) as scalar multiplier.
	///
	///After this has been successfully called on point, operations can be performed on the scalar.
	///
	///If the integer is valid, returns `Ok(())`. If the scalar is larger or equal to the order of the curve,
	///returns `Err(())`.
	pub fn load(&mut self, _d: &[u8]) -> Result<(), ()>
	{
		let mut d = Mpz::new();
		d.import_bigendian(_d);
		self.undef = 1;
		self.inner.set(&d.0)?;
		mix_secondary_init(&mut self.seed, _d);
		self.undef = 0;
		Ok(())
	}
	///Signs an ECDSA signature for specified hash `hash`, using the specified scalar as private key.
	///
	///The signature is stored as raw concatnation of `R` and `S`, both of equal size into `output`. Currently,
	///the maximum output size is 132 bytes.
	///
	///On success, returns `Ok(size)`, where `size` is the size of the signature in bytes. If the output size
	///is insufficient or if the scalar is uninitialized, returns `Err(())`.
	///
	///# Required sizes:
	///
	/// Curve     | Required size for signature (bytes)
	/// ----------|------------------------------------
	/// NSA P-256 | 64
	/// NSA P-384 | 96
	/// NSA P-521 | 132
	pub fn ecdsa_sign_hash(&self, hash: &[u8], output: &mut [u8], rng: &mut TemporaryRandomStream) ->
		Result<usize, ()>
	{
		fail_if!(self.undef != 0, ());
		let mut sig = DsaSignature(DsaSignatureLow::new());
		//Merge seed and hash.
		let mut seed = [0u8; 64];
		mix_secondary_seed(&mut seed, &self.seed, hash);
		//Mix in seed to randomness to guard against primary RNG failing.
		rng.reseed(&seed);
		self.inner.ecdsa_sign(hash, &mut sig.0, |buf|rng.fill_bytes(buf));
		//We assume the order fits in the same number of bytes as base field. This is certainly true
		//for the NIST curves.
		let pbytes = self.csize;
		let ((r, s), ser_size) = dtry!(slice_splitn_at_mut(output, (pbytes, pbytes)));
		sig.0.r.export_bigendian(r)?;
		sig.0.s.export_bigendian(s)?;
		Ok(ser_size)
	}
}

fn encode_nsa_public_key(point: &EccPoint, to: &mut [u8], pbytes: usize) -> Result<usize, ()>
{
	let mut tmp1 = Mpz::new();
	let mut tmp2 = Mpz::new();
	let ((header, x, y), ser_size) = dtry!(slice_splitn_at_mut(to, (1usize, pbytes, pbytes)));
	point.inner.get(&mut tmp1.0, &mut tmp2.0);
	header[0] = 4;
	tmp1.export_bigendian(x)?;
	tmp2.export_bigendian(y)?;
	Ok(ser_size)
}

fn decode_nsa_public_key(curve: EccCurve, from: &[u8]) -> Result<EccPoint, ()>
{
	let pbytes = curve.2;
	fail_if!(1 + 2 * pbytes > from.len(), ());
	let (header, x, y) = dtry!(slice_splitn_at(from, (1usize, pbytes, pbytes)));
	fail_if!(header[0] != 4, ());	//Unknown format.
	let mut tmp1 = Mpz::new();
	let mut tmp2 = Mpz::new();
	tmp1.import_bigendian(x);
	tmp2.import_bigendian(y);
	let mut point = EccPoint::new(curve);
	point.inner.set(&tmp1.0, &tmp2.0)?;
	Ok(point)
}

///NSA-curve ECDH private key.
///
///This structure represents a ECDH private key using the NSA curves (P-256, P-384 and P-521).
///
///This structure is created by the [`new()`](#method.new) method.
pub struct NsaEcdhKey(EccScalar, EccCurve, usize);

impl NsaEcdhKey
{
	///Create a new keypair for NSA-curve `crv` ECDH.
	///
	///The public part is stored into given buffer `pubkey`, in TLS uncompressed format. The size of public keys
	///is as returned by [`EccCurve::point_size`](struct.EccCurve.html#method.point_size). Currently the largest
	///possible size is 133 bytes.
	///
	///On success, returns `Ok((privkey, size))`, where `privkey` is the private key and `size` is the size
	///of the public key in bytes. If there is insufficient space for public key, returns `Err(())`.
	///
	///# Required sizes:
	///
	/// Curve     | Required size for public key (bytes)
	/// ----------|----------------------------------------
	/// NSA P-256 | 65
	/// NSA P-384 | 97
	/// NSA P-521 | 133
	pub fn new(crv: EccCurve, pubkey: &mut [u8], rng: &mut TemporaryRandomStream) ->
		Result<(NsaEcdhKey, usize), ()>
	{
		let mut _pubkey = EccPoint::new(crv);
		let mut privkey = EccScalar::new(crv);
		privkey.inner.random(RANDOM!(rng));
		_pubkey.inner.mul_g(&privkey.inner);
		let size = encode_nsa_public_key(&_pubkey, pubkey, crv.2)?;
		Ok((NsaEcdhKey(privkey, crv, crv.2), size))
	}
	///Perform key agreement between ECDH private key and another ECDH public key `peerpub`.
	///
	///The shared secret is written into the output buffer `shared`. The same pair of keys (even the other way
	///around with public and private keys) gives the same shared secret. Furthermore, this shared secret is
	///hard to compute from just the public keys. The shared secret is in in TLS uncompressed point format. The
	///size of shared secret is as returned by [`EccCurve::point_size`](struct.EccCurve.html#method.point_size).
	///Currently the largest possible size is 133 bytes.
	///
	///Note, however, that TLS does not use the uncompressed format for DH shared secrets, instead it uses just
	///x coordinate.
	///
	///On success, returns `Ok(size)`, where `size` is the size of the shared secret key in bytes. If the
	///shared secret buffer has insufficient space or the peer point is not on curve, returns `Err(())`.
	///
	///# Required sizes:
	///
	/// Curve     | Required size for shared secret (bytes)
	/// ----------|----------------------------------------
	/// NSA P-256 | 65
	/// NSA P-384 | 97
	/// NSA P-521 | 133
	pub fn agree(&self, shared: &mut [u8], peerpub: &[u8]) -> Result<usize, ()>
	{
		let pbytes = self.2;
		let mut _shared = EccPoint::new(self.1);
		let _peerpub = decode_nsa_public_key(self.1, peerpub)?;
		_shared.inner.mul(&self.0.inner, &_peerpub.inner).ok();	//Can not fail.
		encode_nsa_public_key(&_shared, shared, pbytes)
	}
	///Load an integer `_d` (big-endian base-256) as new private ECDH key.
	///
	///The integer is given in big-endian form, and must be less than the order of the curve.
	///
	///On success, returns `Ok(ecdhkey)`, where `ecdhkey` is the ECDH key. If the scalar is bigger than the
	///order of curve, returns `Err(())`.
	pub fn load(crv: EccCurve, _d: &[u8]) -> Result<NsaEcdhKey, ()>
	{
		let mut privkey = EccScalar::new(crv);
		privkey.load(_d)?;
		Ok(NsaEcdhKey(privkey, crv, crv.2))
	}
	///Same as `load`, but also save the corresponding public key to `pubkey` and return its size.
	pub fn load_with_public_key(crv: EccCurve, _d: &[u8], pubkey: &mut [u8]) ->
		Result<(NsaEcdhKey, usize), ()>
	{
		let key = Self::load(crv, _d)?;
		let mut _pubkey = EccPoint::new(crv);
		_pubkey.inner.mul_g(&key.0.inner);
		let size = encode_nsa_public_key(&_pubkey, pubkey, key.2)?;
		Ok((key, size))
	}
	///Save a private `ECDH` key as an integer `buffer`.
	///
	///The saved key is written in big-endian base-256 form. Currently the largest possible size is 66 bytes.
	///
	///On success, returns `Ok(size)`, where `size` is the size of the integer in bytes. If the buffer has
	///insufficient space, returns `Err(())`.
	///
	///# Required sizes:
	///
	/// Curve     | Required size for scalar (bytes)
	/// ----------|---------------------------------
	/// NSA P-256 | 32
	/// NSA P-384 | 48
	/// NSA P-521 | 66
	pub fn save(&self, buffer: &mut [u8]) -> Result<usize, ()>
	{
		let pbytes = self.2;
		let buffer = dtry!(buffer.get_mut(..pbytes));
		let mut d = Mpz::new();
		self.0.inner.get(&mut d.0);
		d.export_bigendian(buffer)?;
		Ok(pbytes)
	}
}

macro_rules! returntype
{
	($symbol:ident, true) => { $symbol };
	($symbol:ident, false) => { Result<$symbol, ()> };
}

macro_rules! return_val
{
	($symbol:expr, true) => { $symbol };
	($symbol:expr, false) => { Ok($symbol) };
}

macro_rules! make_hash
{
	($(#[$comments:meta])*; $symbol:ident, $outlen:expr, $statetype:ident, $uncond:ident) => {
		$(#[$comments])*
		///The hash contexts are created using the [`new()`](#method.new) method.
		#[repr(C)]
		pub struct $symbol($statetype);
		impl Clone for $symbol { fn clone(&self) -> Self { $symbol(self.0) } }
		impl $symbol
		{
			///Create a new hash context for the hash function.
			///
			///On success returns either `ctx` or `Ok(ctx)`, where `ctx` is the hash context. If the
			///hash is unsupported, returns `Err(())`.
			pub fn new() -> returntype!($symbol, $uncond)
			{
				let ret = $symbol($statetype::init());
				return_val!(ret, $uncond)
			}
			///Reset the hash context into its initial state (no data to be hashed).
			pub fn reset(&mut self)
			{
				self.0 = $statetype::init()
			}
			///Append more data `data` to be hashed into the context.
			pub fn input(&mut self, data: &[u8])
			{
				self.0.update(data)
			}
			///Obtains the hash of the data added to the context, consuming the context in process.
			pub fn output(self) -> [u8; $outlen]
			{
				let mut output: [u8; $outlen] = [0; $outlen];
				self.0.digest(&mut output);
				output
			}
		}
	}
}

macro_rules! AES
{
	($key:expr) => { |dst,src,len|$key.encrypt(dst,src,len) }
}

struct GcmContext
{
	gkey: GcmKeyLow,
	aeskey: AesCtxLow,
	name: &'static str,
}

///Human-readable name of AES-128-GCM.
pub static AES128GCM_NAME: &'static str = "AES-128-GCM";
///Human-readable name of AES-192-GCM.
pub static AES192GCM_NAME: &'static str = "AES-192-GCM";
///Human-readable name of AES-256-GCM.
pub static AES256GCM_NAME: &'static str = "AES-256-GCM";
const P_MAX: u64 = 68719476704;
const C_MAX: u64 = 68719476720;

impl GcmContext
{
	fn new_context(key: &[u8], name: &'static str) -> Result<GcmContext, AeadError>
	{
		AeadError::assert_key(name, key, |n|n==16||n==24||n==32)?;
		let mut nkey = GcmContext{
			gkey: GcmKeyLow::new(),
			aeskey: AesCtxLow::new(),
			name:name
		};
		//Setup the AES/GCM keys.
		nkey.aeskey.set_encrypt_key(key);
		unsafe {
			let gkey = &mut nkey.gkey;
			let aeskey = &nkey.aeskey;
			gkey.set_key(AES!(aeskey));
		}

		Ok(nkey)
	}
	unsafe fn encrypt3(&self, nonce: &[u8], ad: &[u8], optr: *mut u8, iptr: *const u8,
		ilen: usize, trailer: usize) -> Result<usize, AeadError>
	{
		let reallen = ilen.saturating_add(trailer);
		AeadError::assert_nonce(self.name, nonce, |n|n==12)?;
		AeadError::assert_ad(self.name, ad, 0x1FFFFFFFFFFFFFFFu64)?;
		let tagend = AeadError::assert_plaintext(self.name, reallen, 16, P_MAX)?;

		let mut gctx = GcmCtxLow::new();
		//If nonce is 12 bytes, we don't need the GCM key.
		gctx.set_iv(None, nonce);
		gctx.update(&self.gkey, ad);
		if trailer > 0 {
			//nettle_gcm_encrypt does not like misaligned blocks. If the ilen is not multiple
			//of 16, we got to have a temporary block.
			let overflow = ilen % 16;
			let ilenb = ilen - overflow;	//ilen%16 <= ilen.
			let ilenj = ilenb + if overflow > 0 { 16 } else { 0 };	//Offset after joiner.
			//The whole blocks of first input.
			if ilenb > 0 {
				unsafe{gctx.encrypt(&self.gkey, AES!(self.aeskey), optr, iptr, ilenb);}
			}
			//The block joiner.
			if overflow > 0 {
				//The joiner block.
				let mut tmp = [0;16];
				//The part from the first part.
				let of_slice = unsafe{from_raw_parts(iptr.add(ilenb), overflow)};
				(&mut tmp[..overflow]).copy_from_slice(of_slice);
				//The part from the second part.
				let joinlen = min(overflow + trailer, 16);
				let splen = joinlen - overflow;
				let uf_slice = unsafe{from_raw_parts(optr.add(ilen), splen)};
				(&mut tmp[overflow..joinlen]).copy_from_slice(uf_slice);
				//Process the joiner block
				unsafe {
					let key = AES!(self.aeskey);
					gctx.encrypt(&self.gkey, key, optr.add(ilenb), tmp.as_ptr(), joinlen);
				}
			}
			//The whole blocks of second input. This would crash if joinlen < 16, but in that
			//case, ovverflow+trailer < 16, which impiles ilenb+overflow+trailer < ilenb+16=
			//ilenj, and the condition below can't be true.
			if ilenj < reallen {
				//optr goes at least to reallen, and reallen > ilenj, so size is positive.
				unsafe {
					let key = AES!(self.aeskey);
					gctx.encrypt(&self.gkey, key, optr.add(ilenj), optr.add(ilenj),
						reallen - ilenj);
				}
			}
		} else {
			unsafe{gctx.encrypt(&self.gkey, AES!(self.aeskey), optr, iptr, ilen);}
		}
		unsafe{gctx.digest(&self.gkey, AES!(self.aeskey), from_raw_parts_mut(optr.add(reallen), 16));}
		Ok(tagend)
	}
	unsafe fn decrypt3(&self, nonce: &[u8], ad: &[u8], optr: *mut u8, iptr: *const u8, ilen: usize) ->
		Result<usize, AeadError>
	{
		AeadError::assert_nonce(self.name, nonce, |n|n==12)?;
		AeadError::assert_ad(self.name, ad, 0x1FFFFFFFFFFFFFFFu64)?;
		//Tag is 16 bytes.
		let rawlen = AeadError::assert_ciphertext(self.name, ilen, C_MAX, |s|s.checked_sub(16))?;
		let olen = rawlen;

		let mut gctx = GcmCtxLow::new();
		let mut tag = [0u8; 16];
		//If nonce is 12 bytes, we don't need the GCM key.
		gctx.set_iv(None, nonce);
		gctx.update(&self.gkey, ad);
		unsafe{gctx.decrypt(&self.gkey, AES!(self.aeskey), optr, iptr, rawlen);}
		unsafe{gctx.digest(&self.gkey, AES!(self.aeskey), &mut tag);}
		//Great API...
		let syndrome = calculate_syndrome(&tag, unsafe{from_raw_parts(iptr.add(rawlen), 16)});
		AeadError::assert_tag_ok(syndrome == 0, unsafe{from_raw_parts_mut(optr, olen)})?;
		Ok(rawlen)
	}
}

fn __encrypted_size(size: usize) -> Option<usize>
{
	AeadError::assert_plaintext("", size, 16, P_MAX).ok()
}

fn __max_plaintext_size(size: usize) -> Option<usize>
{
	AeadError::assert_ciphertext("", size, C_MAX, |s|s.checked_sub(16)).ok()
}

///Key for AES-128-GCM encryption/decryption.
///
///This structure contains the key used for AES-128-GCM encryption and decryption. It also forms a
///low-level context for AES-128-GCM, and thus implements [`LowLevelAeadContext`].
///
///[`LowLevelAeadContext`]: trait.LowLevelAeadContext.html
pub struct Aes128GcmContext(GcmContext);

impl LowLevelAeadContext for Aes128GcmContext
{
	fn new_context(key: &[u8]) -> Result<Aes128GcmContext, AeadError>
	{
		AeadError::assert_key(AES128GCM_NAME, key, |n|n==16)?;
		Ok(Aes128GcmContext(GcmContext::new_context(key, AES128GCM_NAME)?))
	}
	fn encrypted_size(&self, size: usize) -> Option<usize> { __encrypted_size(size) }
	fn max_plaintext_size(&self, size: usize) -> Option<usize> { __max_plaintext_size(size) }
	unsafe fn encrypt3(&self, nonce: &[u8], ad: &[u8], optr: *mut u8, iptr: *const u8,
		ilen: usize, trailerlen: usize) -> Result<usize, AeadError>
	{
		unsafe{self.0.encrypt3(nonce, ad, optr, iptr, ilen, trailerlen)}
	}
	unsafe fn decrypt3(&self, nonce: &[u8], ad: &[u8], optr: *mut u8, iptr: *const u8, ilen: usize) ->
		Result<usize, AeadError>
	{
		unsafe{self.0.decrypt3(nonce, ad, optr, iptr, ilen)}
	}
}

///Key for AES-192-GCM encryption/decryption.
///
///This structure contains the key used for AES-192-GCM encryption and decryption. It also forms a
///low-level context for AES-192-GCM, and thus implements [`LowLevelAeadContext`].
///
///[`LowLevelAeadContext`]: trait.LowLevelAeadContext.html
pub struct Aes192GcmContext(GcmContext);

impl LowLevelAeadContext for Aes192GcmContext
{
	fn new_context(key: &[u8]) -> Result<Aes192GcmContext, AeadError>
	{
		AeadError::assert_key(AES192GCM_NAME, key, |n|n==24)?;
		Ok(Aes192GcmContext(GcmContext::new_context(key, AES192GCM_NAME)?))
	}
	fn encrypted_size(&self, size: usize) -> Option<usize> { __encrypted_size(size) }
	fn max_plaintext_size(&self, size: usize) -> Option<usize> { __max_plaintext_size(size) }
	unsafe fn encrypt3(&self, nonce: &[u8], ad: &[u8], optr: *mut u8, iptr: *const u8,
		ilen: usize, trailerlen: usize) -> Result<usize, AeadError>
	{
		unsafe{self.0.encrypt3(nonce, ad, optr, iptr, ilen, trailerlen)}
	}
	unsafe fn decrypt3(&self, nonce: &[u8], ad: &[u8], optr: *mut u8, iptr: *const u8, ilen: usize) ->
		Result<usize, AeadError>
	{
		unsafe{self.0.decrypt3(nonce, ad, optr, iptr, ilen)}
	}
}

///Key for AES-256-GCM encryption/decryption.
///
///This structure contains the key used for AES-256-GCM encryption and decryption. It also forms a
///low-level context for AES-256-GCM, and thus implements [`LowLevelAeadContext`].
///
///[`LowLevelAeadContext`]: trait.LowLevelAeadContext.html
pub struct Aes256GcmContext(GcmContext);

impl LowLevelAeadContext for Aes256GcmContext
{
	fn new_context(key: &[u8]) -> Result<Aes256GcmContext, AeadError>
	{
		AeadError::assert_key(AES256GCM_NAME, key, |n|n==32)?;
		Ok(Aes256GcmContext(GcmContext::new_context(key, AES256GCM_NAME)?))
	}
	fn encrypted_size(&self, size: usize) -> Option<usize> { __encrypted_size(size) }
	fn max_plaintext_size(&self, size: usize) -> Option<usize> { __max_plaintext_size(size) }
	unsafe fn encrypt3(&self, nonce: &[u8], ad: &[u8], optr: *mut u8, iptr: *const u8,
		ilen: usize, trailerlen: usize) -> Result<usize, AeadError>
	{
		unsafe{self.0.encrypt3(nonce, ad, optr, iptr, ilen, trailerlen)}
	}
	unsafe fn decrypt3(&self, nonce: &[u8], ad: &[u8], optr: *mut u8, iptr: *const u8, ilen: usize) ->
		Result<usize, AeadError>
	{
		unsafe{self.0.decrypt3(nonce, ad, optr, iptr, ilen)}
	}
}


make_hash!(#[doc="
Context for SHA3-256.
	"]; Sha3256Ctx2, 32, Sha3256CtxLow, true);
make_hash!(#[doc="
Context for SHA3-384.
	"]; Sha3384Ctx2, 48, Sha3384CtxLow, true);
make_hash!(#[doc="
Context for SHA3-512.
	"]; Sha3512Ctx2, 64, Sha3512CtxLow, true);

make_hash!(#[doc="
Context for SHA-1.

# DANGER:

SHA-1 is cryptographically **BROKEN**. Do not rely on it. In practicular, using it with signatures opens one up
for signature forgeries.
	"]; Sha1Ctx, 20, Sha1CtxLow, true);
make_hash!(#[doc="
Context for SHA-256.
	"]; Sha256Ctx, 32, Sha256CtxLow, true);
make_hash!(#[doc="
Context for SHA-384.
	"]; Sha384Ctx, 48, Sha384CtxLow, true);
make_hash!(#[doc="
Context for SHA-512.
	"]; Sha512Ctx, 64, Sha512CtxLow, true);


///Compute the RSA private key operation, i.e. e:th root of specified number modulo RSA modulus.
///
///The input message `m` is read as big-endian base-256 integer, and the output integer `x` is stored in the same
///way. The output integer is always smaller than the RSA modulus, so fits into whatever buffer fits the RSA modulus
///(or signature, see [`RsaPrivate::get_size()`](struct.RsaPrivate.html#method.get_size)).
///
///The private key to use is `private` and the public key to use to check the answer is `public`.
///
///Note: This function checks the result is correct. This check is because RSA is is extremely vulernable to fault
///attacks. Basically one calculation error and your key **walks** with the result.
///
/// On success, returns `Ok(())`. If `x` is not big enough, or the calculations do not match up, returns `Err(())`.
pub fn rsa_compute_root(public: &RsaPublic, private: &RsaPrivate, x: &mut [u8], m: &[u8],
	rng: &mut TemporaryRandomStream) -> Result<(), ()>
{
	fail_if!(public.0.size == 0, ());
	fail_if!(private.0.size == 0, ());
	let mut _m = Mpz::new();
	let mut _x = Mpz::new();
	_m.import_bigendian(m);
	_rsa_compute_root2(&public.0, &private.0, &mut _x.0, &_m.0, RANDOM!(rng))?;
	_x.export_bigendian(x)?;
	Ok(())
}

///Compute the RSA public key operation, i.e. e:th power of specified number modulo RSA modulus.
///
///The input message is `m` read as big-endian base-256 integer, and the output integer `x` is stored in the same
///way. The output integer is always smaller than the RSA modulus, so fits into whatever buffer fits the RSA modulus
///(or signature, see [`RsaPublic::get_size()`](struct.RsaPublic.html#method.get_size)).
///
///The public key to use is `public`.
///
/// On success, returns `Ok(())`. If `x` is not big enough, or the public key is undefined, returns `Err(())`.
pub fn rsa_compute_power(public: &RsaPublic, x: &mut [u8], m: &[u8]) -> Result<(), ()>
{
	fail_if!(public.0.size == 0, ());
	let mut _m = Mpz::new();
	let mut _x = Mpz::new();
	_m.import_bigendian(m);
	_x.0.powm(&_m.0, &public.0.e, &public.0.n);
	_x.export_bigendian(x)?;
	Ok(())
}


#[cfg(test)]
mod test;
