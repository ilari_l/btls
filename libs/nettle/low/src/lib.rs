//!Low level Rust bindings to Nettle cryptographic library.
//!
//!This library provodes RSA and NIST/NSA-curve ECC implementations, as well as AES-GCM and SHA-2 and optionally
//!SHA-3.
//!
//!This is its own crate because there can be only one crate in entiere program like that. Thus it must remain
//!semver-compatible, which is much easier if the crate does as little as possible.
//!
//!The nettle-sys crate is not sufficient, as still supported versions of Nettle may lack some functions or
//!worse, have some function but broken implementation of it (need to detect if it works from preprocessor symbol
//!in include files).
//!
//!Nonwithstanding the previous, if Nettle is dynamically linked, as it seems by the default, there can be multiple
//!Nettle binding crates in one program.
#![no_std]
#![allow(improper_ctypes)]
#![allow(unsafe_code)]

use core::mem::transmute;
use core::mem::zeroed;
use core::ptr::null;
use core::ptr::null_mut;
use core::ptr::write_volatile;
use core::slice::from_raw_parts_mut;

struct ProgressTrampoline<F:FnMut(i32)>(F);

impl<F:FnMut(i32)> ProgressTrampoline<F>
{
	extern fn trampoline(func: *mut (), arg: i32)
	{ unsafe {
		let func: &mut Self = transmute(func);
		(func.0)(arg);
	}}
}

struct RandomTrampoline<F:for<'x> FnMut(&'x mut [u8])>(F);

impl<F:for<'x> FnMut(&'x mut [u8])> RandomTrampoline<F>
{
	extern fn trampoline(func: *mut (), len: usize, buf: *mut u8)
	{ unsafe {
		let func: &mut Self = transmute(func);
		(func.0)(from_raw_parts_mut(buf, len));
	}}
}

struct GcmTrampoline<F:FnMut(*mut u8, *const u8, usize)>(F);

impl<F:FnMut(*mut u8, *const u8, usize)> GcmTrampoline<F>
{
	extern fn trampoline(func: *mut (), len: u32, dst: *mut u8, src: *const u8)
	{ unsafe {
		let func: &mut Self = transmute(func);
		(func.0)(dst, src, len as usize);
	}}
}


///The ecc_curve type from Nettle. Opaque.
#[repr(C)]
pub struct EccCurve(());

impl EccCurve
{
	///Get P-256.
	pub fn secp_256r1() -> &'static EccCurve { unsafe { transmute(nettle_get_secp_256r1()) } }
	///Get P-384.
	pub fn secp_384r1() -> &'static EccCurve { unsafe { transmute(nettle_get_secp_384r1()) } }
	///Get P-521.
	pub fn secp_521r1() -> &'static EccCurve { unsafe { transmute(nettle_get_secp_521r1()) } }
}

///The mpz_t type from Nettle.
#[repr(C)]
pub struct Mpz
{
	alloc: i32,
	size: i32,
	data: *mut (),
}

unsafe impl Send for Mpz {}

impl Mpz
{
	///Create new.
	pub fn new() -> Mpz
	{ unsafe {
		let mut r: Mpz = zeroed();
		__gmpz_init(&mut r as *mut Mpz);
		r
	}}
	///As constant pointer.
	pub fn as_ptr(&self) -> *const Mpz { self as *const Mpz }
	///As mutable pointer.
	pub fn as_mut_ptr(&mut self) -> *mut Mpz { self as *mut Mpz }
	///Returns true if self.data is not NULL.
	pub fn need_clear(&self) -> bool { !self.data.is_null() }
	///Import.
	pub fn import<T:GmpImpExpType>(&mut self, op: &[T], order: i32, nails: usize)
	{ unsafe {
		let sz = core::mem::size_of::<T>();
		//Always native-endian.
		__gmpz_import(self.as_mut_ptr(), op.len(), order, sz, 0, nails, op.as_ptr() as _);
	}}
	///Export.
	pub fn export<'a,T:GmpImpExpType+'a>(&self, op: &'a mut [T], order: i32, nails: usize) -> usize
	{ unsafe {
		let sz = core::mem::size_of::<T>();
		let mut len = op.len();
		//Always native-endian.
		__gmpz_export(op.as_mut_ptr() as _, &mut len as _, order, sz, 0, nails, self.as_ptr());
		len
	}}
	///Size in base.
	pub fn sizeinbase(&self, base: i32) -> usize
	{ unsafe {
		__gmpz_sizeinbase(self.as_ptr(), base)
	}}
	///powmod
	pub fn powm(&mut self, base: &Mpz, exp: &Mpz, modulus: &Mpz)
	{ unsafe {
		__gmpz_powm(self.as_mut_ptr(), base.as_ptr(), exp.as_ptr(), modulus.as_ptr());
	}}
	///cmpabs
	pub fn cmpabs(a: &Mpz, b: &Mpz) -> i32
	{ unsafe {
		__gmpz_cmpabs(a.as_ptr(), b.as_ptr())
	}}
}

///Types allowed in gmp import/export.
pub unsafe trait GmpImpExpType {}

unsafe impl GmpImpExpType for u8 {}
unsafe impl GmpImpExpType for u16 {}
unsafe impl GmpImpExpType for u32 {}
unsafe impl GmpImpExpType for u64 {}


impl Drop for Mpz
{
	fn drop(&mut self)
	{ unsafe {
		if self.data.is_null() { return; }
		__gmpz_clear(self as *mut Mpz);
		self.data = null_mut();		//Avoid double-drop.
	}}
}


///The rsa_public_key type from Nettle.
#[repr(C)]
pub struct RsaPublic
{
	pub size: usize,	//0 means invalid key.
	pub n: Mpz,
	pub e: Mpz,
}

impl RsaPublic
{
	///Make new.
	pub fn new() -> RsaPublic
	{
		RsaPublic {
			size: 0,
			n: Mpz::new(),
			e: Mpz::new(),
		}
	}
	///As constant pointer.
	pub fn as_ptr(&self) -> *const RsaPublic { self as *const RsaPublic }
	///As mutable pointer.
	pub fn as_mut_ptr(&mut self) -> *mut RsaPublic { self as *mut RsaPublic }
	///Prepare public key.
	pub fn key_prepare(&mut self) -> Result<(), ()>
	{ unsafe {
		let r = nettle_rsa_public_key_prepare(self.as_mut_ptr());
		if r <= 0 { Err(()) } else { Ok(()) }
	}}
}

///The rsa_private_key type from Nettle.
#[repr(C)]
pub struct RsaPrivate
{
	pub size: usize,
	pub d: Mpz,
	pub p: Mpz,
	pub q: Mpz,
	pub dp: Mpz,
	pub dq: Mpz,
	pub qi: Mpz,
}

unsafe impl Send for RsaPrivate {}

impl RsaPrivate
{
	///Make new.
	pub fn new() -> RsaPrivate
	{
		RsaPrivate {
			size: 0,
			d: Mpz::new(),
			p: Mpz::new(),
			q: Mpz::new(),
			dp: Mpz::new(),
			dq: Mpz::new(),
			qi: Mpz::new(),
		}
	}
	///As constant pointer.
	pub fn as_ptr(&self) -> *const RsaPrivate { self as *const RsaPrivate }
	///As mutable pointer.
	pub fn as_mut_ptr(&mut self) -> *mut RsaPrivate { self as *mut RsaPrivate }
	///Compute root.
	pub fn compute_root(&self, x: &mut Mpz, m: &Mpz)
	{ unsafe {
		nettle_rsa_compute_root(self.as_ptr(), x.as_mut_ptr(), m.as_ptr());
	}}
	///Prepare private key.
	pub fn key_prepare(&mut self) -> Result<(), ()>
	{ unsafe {
		let r = nettle_rsa_private_key_prepare(self.as_mut_ptr());
		if r <= 0 { Err(()) } else { Ok(()) }
	}}
	///Generate keypair.
	pub fn generate_keypair<Random,Progress>(&mut self, public: &mut RsaPublic, n_size: u32, e_size: u32,
		random: Random, progress: Progress) -> Result<(), ()> where Random: for<'x> FnMut(&'x mut [u8]),
		Progress: FnMut(i32)
	{ unsafe {
		let mut random = RandomTrampoline(random);
		let randomfn = RandomTrampoline::<Random>::trampoline;
		let mut progress = ProgressTrampoline(progress);
		let progressfn = ProgressTrampoline::<Progress>::trampoline;
		let r = nettle_rsa_generate_keypair(public.as_mut_ptr(), self.as_mut_ptr(), voidptr(&mut random),
			randomfn, voidptr(&mut progress), progressfn, n_size, e_size);
		if r <= 0 { Err(()) } else { Ok(()) }
	}}
}


///The ecc_point type from Nettle.
#[repr(C)]
pub struct EccPoint
{
	curve: *const EccCurve,
	data: *mut (),
}

unsafe impl Send for EccPoint {}

impl EccPoint
{
	///Create a new point.
	pub fn new(curve: &'static EccCurve) -> EccPoint
	{ unsafe {
		let mut r: EccPoint = zeroed();
		nettle_ecc_point_init(r.as_mut_ptr(), curve as *const EccCurve);
		r
	}}
	///As constant pointer.
	pub fn as_ptr(&self) -> *const EccPoint { self as *const EccPoint }
	///As mutable pointer.
	pub fn as_mut_ptr(&mut self) -> *mut EccPoint { self as *mut EccPoint }
	///Set.
	pub fn set(&mut self, x: &Mpz, y: &Mpz) -> Result<(), ()>
	{ unsafe {
		let r = nettle_ecc_point_set(self.as_mut_ptr(), x.as_ptr(), y.as_ptr());
		if r <= 0 { Err(()) } else { Ok(()) }
	}}
	///Get.
	pub fn get(&self, x: &mut Mpz, y: &mut Mpz)
	{ unsafe {
		nettle_ecc_point_get(self.as_ptr(), x.as_mut_ptr(), y.as_mut_ptr());
	}}
	///Multiply basepoint.
	pub fn mul_g(&mut self, multiplier: &EccScalar)
	{ unsafe {
		nettle_ecc_point_mul_g(self.as_mut_ptr(), multiplier.as_ptr());
	}}
	///Multiply point.
	pub fn mul(&mut self, multiplier: &EccScalar, base: &EccPoint) -> Result<(), ()>
	{ unsafe {
		if self.curve != base.curve { return Err(()); }
		nettle_ecc_point_mul(self.as_mut_ptr(), multiplier.as_ptr(), base.as_ptr());
		Ok(())
	}}
	///Validate ecdsa signature.
	pub fn ecdsa_verify(&self, digest: &[u8], signature: &DsaSignature) -> Result<(), ()>
	{ unsafe {
		let r = nettle_ecdsa_verify(self.as_ptr(), digest.len(), digest.as_ptr(), signature.as_ptr());
		if r <= 0 { Err(()) } else { Ok(()) }
	}}
}

impl Drop for EccPoint
{
	fn drop(&mut self)
	{ unsafe {
		if self.data.is_null() { return; }
		nettle_ecc_point_clear(self as *mut EccPoint);
		self.data = null_mut();		//Avoid double-drop.
	}}
}

///The ecc_scalar type from Nettle.
#[repr(C)]
pub struct EccScalar
{
	curve: *const EccCurve,
	data: *mut (),
}

unsafe impl Send for EccScalar {}

impl EccScalar
{
	///Create a new scalar.
	pub fn new(curve: &'static EccCurve) -> EccScalar
	{ unsafe {
		let mut r: EccScalar = zeroed();
		nettle_ecc_scalar_init(r.as_mut_ptr(), curve as *const EccCurve);
		r
	}}
	///As constant pointer.
	pub fn as_ptr(&self) -> *const EccScalar { self as *const EccScalar }
	///As mutable pointer.
	pub fn as_mut_ptr(&mut self) -> *mut EccScalar { self as *mut EccScalar }
	///Set.
	pub fn set(&mut self, d: &Mpz) -> Result<(), ()>
	{ unsafe {
		let r = nettle_ecc_scalar_set(self.as_mut_ptr(), d.as_ptr());
		if r == 0 { Err(()) } else { Ok(()) }
	}}
	///Get.
	pub fn get(&self, d: &mut Mpz)
	{ unsafe {
		nettle_ecc_scalar_get(self.as_ptr(), d.as_mut_ptr());
	}}
	///Random.
	pub fn random<Random>(&mut self, random: Random) where Random: for<'x> FnMut(&'x mut [u8])
	{ unsafe {
		let mut random = RandomTrampoline(random);
		let randomfn = RandomTrampoline::<Random>::trampoline;
		nettle_ecc_scalar_random(self.as_mut_ptr(), voidptr(&mut random), randomfn);
	}}
	///Sign with ECDSA.
	pub fn ecdsa_sign<Random>(&self, digest: &[u8], signature: &mut DsaSignature, random: Random)
		where Random: for<'x> FnMut(&'x mut [u8])
	{ unsafe {
		let mut random = RandomTrampoline(random);
		let randomfn = RandomTrampoline::<Random>::trampoline;
		nettle_ecdsa_sign(self.as_ptr(), voidptr(&mut random), randomfn, digest.len(), digest.as_ptr(),
			signature.as_mut_ptr());
	}}
	///Generate ECDSA key.
	pub fn ecdsa_generate_keypair<Random>(&mut self, pubkey: &mut EccPoint, random: Random)
		where Random: for<'x> FnMut(&'x mut [u8])
	{ unsafe {
		let mut random = RandomTrampoline(random);
		let randomfn = RandomTrampoline::<Random>::trampoline;
		nettle_ecdsa_generate_keypair(pubkey.as_mut_ptr(), self.as_mut_ptr(), voidptr(&mut random),
			randomfn);
	}}
}


impl Drop for EccScalar
{
	fn drop(&mut self)
	{ unsafe {
		if self.data.is_null() { return; }
		nettle_ecc_scalar_clear(self as *mut EccScalar);
		self.data = null_mut();		//Avoid double-drop.
	}}
}

///The dsa_signature type from Nettle.
#[repr(C)]
pub struct DsaSignature
{
	pub r: Mpz,
	pub s: Mpz,
}

impl DsaSignature
{
	///Make new.
	pub fn new() -> DsaSignature
	{
		DsaSignature {
			r: Mpz::new(),
			s: Mpz::new(),
		}
	}
	///As constant pointer.
	pub fn as_ptr(&self) -> *const DsaSignature { self as *const DsaSignature }
	///As mutable pointer.
	pub fn as_mut_ptr(&mut self) -> *mut DsaSignature { self as *mut DsaSignature }
}

///The gcm_ctx type from Nettle.
#[repr(C)]
pub struct GcmCtx([u64;8]);		//64 bytes.
impl GcmCtx
{
	///Wipe the value from memory.
	pub fn wipe(&mut self)
	{
		for i in self.0.iter_mut() {
			//This is safe since u8 is not Drop and the cast happens from reference (so the
			//address is guaranteed to be valid).
			unsafe{write_volatile(i as *mut u64, 0)};
		}
	}
	///New.
	pub fn new() -> GcmCtx
	{ unsafe {
		zeroed()	//All zeroes is valid.
	}}
	///Set IV.
	pub fn set_iv(&mut self, key: Option<&GcmKey>, iv: &[u8])
	{ unsafe {
		let key: *const GcmKey = match key {
			Some(key) => key as _,
			//Missing key is only allowed for 12-byte IV.
			None if iv.len() == 12 => null(),
			None => return
		};
		nettle_gcm_set_iv(self as _, key, iv.len(), iv.as_ptr());
	}}
	///Update.
	pub fn update(&mut self, key: &GcmKey, data: &[u8])
	{ unsafe {
		nettle_gcm_update(self as _, key as _, data.len(), data.as_ptr());
	}}
	///Encrypt.
	pub unsafe fn encrypt<Encrypt>(&mut self, key: &GcmKey, encrypt: Encrypt, dst: *mut u8, src: *const u8,
		len: usize) where Encrypt: FnMut(*mut u8, *const u8, usize)
	{
		let mut encrypt = GcmTrampoline(encrypt);
		let encryptfn = GcmTrampoline::<Encrypt>::trampoline;
		nettle_gcm_encrypt(self as _, key as _, voidptr(&mut encrypt), encryptfn, len, dst, src);
	}
	///Decrypt.
	pub unsafe fn decrypt<Encrypt>(&mut self, key: &GcmKey, encrypt: Encrypt, dst: *mut u8, src: *const u8,
		len: usize) where Encrypt: FnMut(*mut u8, *const u8, usize)
	{
		let mut encrypt = GcmTrampoline(encrypt);
		let encryptfn = GcmTrampoline::<Encrypt>::trampoline;
		nettle_gcm_decrypt(self as _, key as _, voidptr(&mut encrypt), encryptfn,  len, dst, src);
	}
	///Digest.
	pub unsafe fn digest<Encrypt>(&mut self, key: &GcmKey, encrypt: Encrypt, digest: &mut [u8])
		where Encrypt: FnMut(*mut u8, *const u8, usize)
	{
		let len = digest.len();
		let mut encrypt = GcmTrampoline(encrypt);
		let encryptfn = GcmTrampoline::<Encrypt>::trampoline;
		nettle_gcm_digest(self as _, key as _, voidptr(&mut encrypt), encryptfn, len, digest.as_mut_ptr());
	}
}

fn voidptr<T:Sized>(x: &mut T) -> *mut () { x as *mut T as _ }


///The gcm_key type from Nettle.
#[repr(C)]
pub struct GcmKey([u64;512]);	//4096(!) bytes.
impl GcmKey
{
	///Wipe the value from memory.
	pub fn wipe(&mut self)
	{
		for i in self.0.iter_mut() {
			//This is safe since u8 is not Drop and the cast happens from reference (so the
			//address is guaranteed to be valid).
			unsafe{write_volatile(i as *mut u64, 0)};
		}
	}
	///New.
	pub fn new() -> GcmKey
	{ unsafe {
		zeroed()	//All zeroes is valid.
	}}
	///Set key.
	pub unsafe fn set_key<Encrypt>(&mut self, encrypt: Encrypt) where Encrypt: FnMut(*mut u8, *const u8, usize)
	{
		let mut encrypt = GcmTrampoline(encrypt);
		let encryptfn = GcmTrampoline::<Encrypt>::trampoline;
		nettle_gcm_set_key(self as _, voidptr(&mut encrypt), encryptfn);
	}
}

///The aes_ctx type from Nettle.
#[repr(C)]
pub struct AesCtx([u64;31]);	//248 bytes.
impl AesCtx
{
	///Wipe the value from memory.
	pub fn wipe(&mut self)
	{
		for i in self.0.iter_mut() {
			//This is safe since u8 is not Drop and the cast happens from reference (so the
			//address is guaranteed to be valid).
			unsafe{write_volatile(i as *mut u64, 0)};
		}
	}
	///New.
	pub fn new() -> AesCtx
	{ unsafe {
		zeroed()	//All zeroes is valid.
	}}
	///Encrypt.
	pub unsafe fn encrypt(&self, dst: *mut u8, src: *const u8, len: usize)
	{
		nettle_aes_encrypt(self as _, len, dst, src);
	}
	pub fn set_encrypt_key(&mut self, key: &[u8])
	{ unsafe {
		nettle_aes_set_encrypt_key(self as _, key.len(), key.as_ptr());
	}}
}

macro_rules! clone_from_copy
{
	($clazz:ident) => {
		impl Clone for $clazz { fn clone(&self) -> $clazz { *self }}
	}
}

macro_rules! impl_hash_context
{
	($clazz:ident, $init:ident, $update:ident, $digest:ident) => {
		impl $clazz
		{
			///Initialize.
			pub fn init() -> $clazz
			{ unsafe {
				let mut r = zeroed();
				$init(&mut r as _);
				r
			}}
			///Update
			pub fn update(&mut self, buffer: &[u8])
			{ unsafe {
				$update(self as _, buffer.len(), buffer.as_ptr());
			}}
			///Digest
			pub fn digest(mut self, buffer: &mut [u8])
			{ unsafe {
				let blen = buffer.len();
				$digest(&mut self as _, blen, buffer.as_mut_ptr());
			}}
		}
	}
}

///The sha1_ctx type from Nettle.
#[repr(C)]
#[derive(Copy)]
pub struct Sha1Ctx([u64;14]);
clone_from_copy!(Sha1Ctx);
impl_hash_context!(Sha1Ctx, nettle_sha1_init, nettle_sha1_update, nettle_sha1_digest);
///The sha256_ctx type from Nettle.
#[repr(C)]
#[derive(Copy)]
pub struct Sha256Ctx([u64;14]);
impl_hash_context!(Sha256Ctx, nettle_sha256_init, nettle_sha256_update, nettle_sha256_digest);
clone_from_copy!(Sha256Ctx);
///The sha384_ctx type from Nettle.
#[repr(C)]
#[derive(Copy)]
pub struct Sha384Ctx([u64;27]);
impl_hash_context!(Sha384Ctx, nettle_sha384_init, nettle_sha384_update, nettle_sha384_digest);
clone_from_copy!(Sha384Ctx);
///The sha512_ctx type from Nettle.
#[repr(C)]
#[derive(Copy)]
pub struct Sha512Ctx([u64;27]);
clone_from_copy!(Sha512Ctx);
impl_hash_context!(Sha512Ctx, nettle_sha512_init, nettle_sha512_update, nettle_sha512_digest);
///The sha3_256_ctx type from Nettle.
#[repr(C)]
#[derive(Copy)]
pub struct Sha3256Ctx([u64;43]);
clone_from_copy!(Sha3256Ctx);
impl_hash_context!(Sha3256Ctx, nettle_sha3_256_init, nettle_sha3_256_update, nettle_sha3_256_digest);
///The sha3_384_ctx type from Nettle.
#[repr(C)]
#[derive(Copy)]
pub struct Sha3384Ctx([u64;39]);
clone_from_copy!(Sha3384Ctx);
impl_hash_context!(Sha3384Ctx, nettle_sha3_384_init, nettle_sha3_384_update, nettle_sha3_384_digest);
///The sha3_512_ctx type from Nettle.
#[repr(C)]
#[derive(Copy)]
pub struct Sha3512Ctx([u64;35]);
clone_from_copy!(Sha3512Ctx);
impl_hash_context!(Sha3512Ctx, nettle_sha3_512_init, nettle_sha3_512_update, nettle_sha3_512_digest);

//Nettle sha384 update is identical to sha512 update.
unsafe fn nettle_sha384_update(ctx: *mut Sha384Ctx, length: usize, data: *const u8)
{
	nettle_sha512_update(ctx as _, length, data)
}

mod sha3
{
	use super::Sha3256Ctx;
	use super::Sha3384Ctx;
	use super::Sha3512Ctx;


	///Returns True if Nettle supports SHA-3, False otherwise.
	#[deprecated(since="1.1.0",note="Always returns true")]
	pub fn supports_sha3() -> bool { true }
	#[cfg_attr(static_nettle, link(name = "nettle", kind="static"))]
	#[cfg_attr(not(static_nettle), link(name = "nettle"))]
	extern {
		pub fn nettle_sha3_256_init(ctx: *mut Sha3256Ctx);
		pub fn nettle_sha3_384_init(ctx: *mut Sha3384Ctx);
		pub fn nettle_sha3_512_init(ctx: *mut Sha3512Ctx);
		pub fn nettle_sha3_256_update(ctx: *mut Sha3256Ctx, length: usize, data: *const u8);
		pub fn nettle_sha3_384_update(ctx: *mut Sha3384Ctx, length: usize, data: *const u8);
		pub fn nettle_sha3_512_update(ctx: *mut Sha3512Ctx, length: usize, data: *const u8);
		pub fn nettle_sha3_256_digest(ctx: *mut Sha3256Ctx, length: usize, data: *mut u8);
		pub fn nettle_sha3_384_digest(ctx: *mut Sha3384Ctx, length: usize, data: *mut u8);
		pub fn nettle_sha3_512_digest(ctx: *mut Sha3512Ctx, length: usize, data: *mut u8);
	}
}

pub use sha3::*;

///Compute RSA private key operation. The result is checked for correctness.
pub fn _rsa_compute_root(public: &RsaPublic, private: &RsaPrivate, x: &mut Mpz, m: &Mpz, random_ctx: *mut (),
	random: extern fn(*mut (), usize, *mut u8)) -> Result<(), ()>
{ unsafe {
	let r = nettle_rsa_compute_root_tr(public.as_ptr(), private.as_ptr(), random_ctx, random, x.as_mut_ptr(),
		m.as_ptr());
	if r <= 0 { return Err(()); }
	Ok(())
}}

///Compute RSA private key operation. The result is checked for correctness.
pub fn _rsa_compute_root2<Random>(public: &RsaPublic, private: &RsaPrivate, x: &mut Mpz, m: &Mpz,
	random: Random) -> Result<(), ()> where Random: for<'x> FnMut(&'x mut [u8])
{ unsafe {
	let mut random = RandomTrampoline(random);
	let randomfn = RandomTrampoline::<Random>::trampoline;
	let r = nettle_rsa_compute_root_tr(public.as_ptr(), private.as_ptr(), voidptr(&mut random), randomfn,
		x.as_mut_ptr(), m.as_ptr());
	if r <= 0 { return Err(()); }
	Ok(())
}}

#[cfg_attr(static_nettle, link(name = "gmp", kind="static"))]
#[cfg_attr(not(static_nettle), link(name = "gmp"))]
extern {
	pub fn __gmpz_init(num: *mut Mpz);
	pub fn __gmpz_clear(num: *mut Mpz);
	pub fn __gmpz_import(rop: *mut Mpz, count: usize, order: i32, size: usize, endian: i32, nails: usize,
		op: *const ());
	pub fn __gmpz_export(rop: *mut (), countp: *mut usize, order: i32, size: usize, endian: i32, nails: usize,
		op: *const Mpz);
	pub fn __gmpz_sizeinbase(op: *const Mpz, base: i32) -> usize;
	pub fn __gmpz_powm(rop: *mut Mpz, base: *const Mpz, exp: *const Mpz, modulus: *const Mpz);
	pub fn __gmpz_cmpabs(a: *const Mpz, b: *const Mpz) -> i32;
}

#[cfg_attr(static_nettle, link(name = "nettle", kind="static"))]
#[cfg_attr(not(static_nettle), link(name = "nettle"))]
extern {
	pub fn nettle_sha1_init(ctx: *mut Sha1Ctx);
	pub fn nettle_sha256_init(ctx: *mut Sha256Ctx);
	pub fn nettle_sha384_init(ctx: *mut Sha384Ctx);
	pub fn nettle_sha512_init(ctx: *mut Sha512Ctx);
	pub fn nettle_sha1_update(ctx: *mut Sha1Ctx, length: usize, data: *const u8);
	pub fn nettle_sha256_update(ctx: *mut Sha256Ctx, length: usize, data: *const u8);
	//fn nettle_sha384_update(ctx: *mut Sha384Ctx, length: usize, data: *const u8);
	pub fn nettle_sha512_update(ctx: *mut Sha512Ctx, length: usize, data: *const u8);
	pub fn nettle_sha1_digest(ctx: *mut Sha1Ctx, length: usize, data: *mut u8);
	pub fn nettle_sha256_digest(ctx: *mut Sha256Ctx, length: usize, data: *mut u8);
	pub fn nettle_sha384_digest(ctx: *mut Sha384Ctx, length: usize, data: *mut u8);
	pub fn nettle_sha512_digest(ctx: *mut Sha512Ctx, length: usize, data: *mut u8);
	pub fn nettle_aes_encrypt(ctx: *const AesCtx, length: usize, dst: *mut u8, src: *const u8);
	pub fn nettle_gcm_set_key(key: *mut GcmKey, cipher: *mut (), f: extern fn(*mut (), u32, *mut u8, *const u8));
	pub fn nettle_gcm_set_iv(ctx: *mut GcmCtx, key: *const GcmKey, length: usize, iv: *const u8);
	pub fn nettle_gcm_update(ctx: *mut GcmCtx, key: *const GcmKey, length: usize, data: *const u8);
	pub fn nettle_gcm_encrypt(ctx: *mut GcmCtx, key: *const GcmKey, cipher: *mut (),
		f: extern fn(*mut (), u32, *mut u8, *const u8), length: usize, dst: *mut u8, src: *const u8);
	pub fn nettle_gcm_decrypt(ctx: *mut GcmCtx, key: *const GcmKey, cipher: *mut (),
		f: extern fn(*mut (), u32, *mut u8, *const u8), length: usize, dst: *mut u8, src: *const u8);
	pub fn nettle_gcm_digest(ctx: *mut GcmCtx, key: *const GcmKey, cipher: *mut (),
		f: extern fn(*mut (), u32, *mut u8, *const u8), length: usize, digest: *mut u8);
	pub fn nettle_aes_set_encrypt_key(ctx: *mut AesCtx, length: usize, key: *const u8);
}


#[cfg_attr(static_nettle, link(name = "hogweed", kind="static"))]
#[cfg_attr(not(static_nettle), link(name = "hogweed"))]
extern {
	pub fn nettle_rsa_compute_root(private: *const RsaPrivate, x: *mut Mpz, m: *const Mpz);
	fn nettle_rsa_compute_root_tr(public: *const RsaPublic, private: *const RsaPrivate, random_ctx: *mut (),
		random: extern fn(*mut (), usize, *mut u8), x: *mut Mpz, m: *const Mpz) -> i32;
	pub fn nettle_rsa_public_key_prepare(public: *mut RsaPublic) -> i32;
	pub fn nettle_rsa_private_key_prepare(private: *mut RsaPrivate) -> i32;
	pub fn nettle_ecc_point_clear(p: *mut EccPoint);
	pub fn nettle_ecc_scalar_clear(s: *mut EccScalar);
	pub fn nettle_ecc_point_init(p: *mut EccPoint, crv: *const EccCurve);
	pub fn nettle_ecc_scalar_init(s: *mut EccScalar, crv: *const EccCurve);
	pub fn nettle_ecc_point_set(p: *mut EccPoint, x: *const Mpz, y: *const Mpz) -> i32;
	pub fn nettle_ecc_scalar_set(s: *mut EccScalar, d: *const Mpz) -> i32;
	pub fn nettle_ecc_point_get(p: *const EccPoint, x: *mut Mpz, y: *mut Mpz);
	pub fn nettle_ecc_scalar_get(s: *const EccScalar, d: *mut Mpz);
	pub fn nettle_ecdsa_sign(key: *const EccScalar, random_ctx: *mut (), random: extern fn(*mut (), usize,
		*mut u8), len: usize, digest: *const u8, signature: *mut DsaSignature);
	pub fn nettle_ecdsa_verify(point: *const EccPoint, len: usize, digest: *const u8,
		signature: *const DsaSignature) -> i32;
	pub fn nettle_ecdsa_generate_keypair(pubkey: *mut EccPoint, privkey: *mut EccScalar, random_ctx: *mut (),
		random: extern fn(*mut (), usize, *mut u8));
	pub fn nettle_rsa_generate_keypair(public: *mut RsaPublic, private: *mut RsaPrivate, random_ctx: *mut (),
		random: extern fn(*mut (), usize, *mut u8), progress_ctx: *mut (),
		progress_fn: extern fn(*mut (), i32), n_size: u32, e_size: u32) -> i32;
	pub fn nettle_ecc_scalar_random(result: *mut EccScalar, random_ctx: *mut (),
		random: extern fn(*mut (), usize, *mut u8));
	pub fn nettle_ecc_point_mul_g(result: *mut EccPoint, multiplier: *const EccScalar);
	pub fn nettle_ecc_point_mul(result: *mut EccPoint, multiplier: *const EccScalar, base: *const EccPoint);
	#[cfg_attr(prefixed_secp_data, link_name = "_nettle_secp_256r1")]
	pub static nettle_secp_256r1: EccCurve;
	#[cfg_attr(prefixed_secp_data, link_name = "_nettle_secp_384r1")]
	pub static nettle_secp_384r1: EccCurve;
	#[cfg_attr(prefixed_secp_data, link_name = "_nettle_secp_521r1")]
	pub static nettle_secp_521r1: EccCurve;
	#[cfg(get_curve_data)] pub fn nettle_get_secp_256r1() -> *const EccCurve;
	#[cfg(get_curve_data)] pub fn nettle_get_secp_384r1() -> *const EccCurve;
	#[cfg(get_curve_data)] pub fn nettle_get_secp_521r1() -> *const EccCurve;
}

#[cfg(not(get_curve_data))]
mod curvedata {
	use ::EccCurve;
	use ::nettle_secp_256r1;
	use ::nettle_secp_384r1;
	use ::nettle_secp_521r1;


	pub unsafe fn nettle_get_secp_256r1() -> *const EccCurve { &nettle_secp_256r1 as *const _ }
	pub unsafe fn nettle_get_secp_384r1() -> *const EccCurve { &nettle_secp_384r1 as *const _ }
	pub unsafe fn nettle_get_secp_521r1() -> *const EccCurve { &nettle_secp_521r1 as *const _ }
}

#[cfg(not(get_curve_data))]
pub use curvedata::*;
