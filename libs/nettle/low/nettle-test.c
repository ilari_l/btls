//Just include these files to check presence.
#include "nettle/sha2.h"
#include "nettle/rsa.h"
#include "gmp.h"

int test_main()
{
	char assert_int_is_4bytes[(sizeof(int)==4)*2-1];
	assert_int_is_4bytes[0] = 2;
	return assert_int_is_4bytes[0];
}
