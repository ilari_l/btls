#include "nettle/sha2.h"
#include "nettle/sha3.h"
#include "nettle/rsa.h"
#include "nettle/ecc.h"
#include "nettle/dsa.h"
#include "nettle/gcm.h"
#include "gmp.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
	size_t mpz = sizeof(mpz_t);
	size_t ptr = sizeof(void*);
	size_t xint = sizeof(int);
	assert(mpz == 2*xint+1*ptr);
	assert(sizeof(struct rsa_public_key) == 2*mpz+1*ptr);
	assert(sizeof(struct rsa_private_key) == 6*mpz+1*ptr);
	assert(sizeof(struct ecc_point) == 2*ptr);
	assert(sizeof(struct ecc_scalar) == 2*ptr);
	assert(sizeof(struct dsa_signature) == 2*mpz);
	assert(sizeof(struct gcm_ctx) == 64);
	assert(sizeof(struct gcm_key) == 4096);
	assert(sizeof(struct aes_ctx) == 244);		//248 in source.
	assert(sizeof(struct sha1_ctx) == 104);		//112 in source.
	assert(sizeof(struct sha256_ctx) == 112);
	assert(sizeof(struct sha384_ctx) == 216);
	assert(sizeof(struct sha512_ctx) == 216);
	assert(sizeof(struct sha3_256_ctx) == 344);
	assert(sizeof(struct sha3_384_ctx) == 312);
	assert(sizeof(struct sha3_512_ctx) == 280);
	printf("OK\n");
}
