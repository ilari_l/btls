#include "nettle/version.h"

#if NETTLE_VERSION_MAJOR > 3 || (NETTLE_VERSION_MAJOR == 3 && NETTLE_VERSION_MINOR >= 2)
int OK_NETTLE_IS_AT_LEAST_3_2 = 1;
#endif
