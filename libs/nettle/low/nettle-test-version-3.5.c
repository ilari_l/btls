#include "nettle/version.h"

#if NETTLE_VERSION_MAJOR > 3 || (NETTLE_VERSION_MAJOR == 3 && NETTLE_VERSION_MINOR >= 5)
int OK_NETTLE_IS_AT_LEAST_3_5 = 1;
#endif
