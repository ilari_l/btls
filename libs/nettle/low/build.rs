extern crate cc;
use cc::Build;
use std::env::var_os;
use std::path::Path;
use std::str::FromStr;

fn contains(x: &[u8], y: &[u8]) -> bool
{
	for i in 0..x.len()  { if x[i..].starts_with(y) { return true; }}
	false
}

fn main()
{
	if let Some(version) = var_os("ASSUME_NETTLE_VERSION") {
		let version = version.to_str().expect("Not a valid assumed nettle version");
		if !version.starts_with("3.") { panic!("Only libnettle 3.x is supported"); }
		let version = &version[2..];
		let mut s = 0;
		//Find something that is not a digit, and cut there.
		while s < version.len() && version.as_bytes()[s].wrapping_sub(48) < 10 { s += 1; }
		let version = &version[..s];
		if version.len() == 0 { panic!("Not a valid assumed nettle version"); }
		let minor = u64::from_str(version).expect("Not a valid assumed nettle version");
		if minor >= 5 {
			println!("cargo:rustc-cfg=prefixed_secp_data");
			println!("cargo:rustc-cfg=get_curve_data");
		} else if minor >= 2 {
			//Defaults.
		} else {
			panic!("Nettle >=3.2 is required");
		}
	} else {
		//Basic sanity check for libnettle and gmp.
		let mut config = Build::new();
		config.file("nettle-test.c");
		if config.try_expand().is_err() {
			panic!("Nettle and GMP do not appear to be installed");
		}
		//Detect Nettle 3.2.
		let mut config = Build::new();
		config.file("nettle-test-version-3.2.c");
		if !config.try_expand().map(|x|contains(&x, b"OK_NETTLE_IS_AT_LEAST_3_2")).unwrap_or(false) {
			panic!("Nettle >=3.2 is required");
		}
		//Detect Nettle 3.5.
		let mut config = Build::new();
		config.file("nettle-test-version-3.5.c");
		if config.try_expand().map(|x|contains(&x, b"OK_NETTLE_IS_AT_LEAST_3_5")).unwrap_or(false) {
			println!("cargo:rustc-cfg=prefixed_secp_data");
			println!("cargo:rustc-cfg=get_curve_data");
		}
	}
	if let Some(nettle_path) = var_os("STATIC_NETTLE_PATH") {
		println!("cargo:rustc-link-search=native={}", Path::new(&nettle_path).display());
		println!("cargo:rustc-cfg=static_nettle");
	}
	//Don't rebuild things unnecressarily.
	println!("cargo:rerun-if-changed=build.rs");
}
