use super::*;
use btls_aux_hash::Sha256;
use btls_aux_rope3::rope3;
use btls_aux_rope3::FragmentWrite;
use btls_aux_rope3::FragmentWriteOps;
use btls_aux_xed25519::ed25519_pubkey;
use btls_aux_xed25519::ed25519_sign;


#[cfg(test)]
#[derive(Copy,Clone)]
struct StringAdapter<'a>(&'a [&'a str], usize);

#[cfg(test)]
impl<'a> Iterator for StringAdapter<'a>
{
	type Item = &'a [u8];
	fn next(&mut self) -> Option<&'a [u8]>
	{
		let idx = self.1;
		self.1 += 1;
		if idx >= self.0.len() { return None; }
		Some(self.0[idx].as_bytes())
	}
}

#[test]
fn test_name_constraints_none()
{
	//If no name constraints, anything goes.
	let null: [& str; 0] = [];
	let name: [&str; 3] = ["foo.test", "bar.test", "qux.example"];
	assert!(check_name_constraints2(StringAdapter(&name,0), StringAdapter(&null,0), StringAdapter(&null,0)));
}

#[test]
fn test_name_constraints_disallow()
{
	//If no allow but some disallow, everything not disallowed is allowed.
	let null: [& str; 0] = [];
	let disallows: [& str; 1] = ["mil"];
	let disallowsd: [& str; 1] = [".mil"];
	let name: [&str; 3] = ["foo.test", "bar.test", "qux.example"];
	let name2: [&str; 4] = ["foo.test", "bar.test", "qux.example", "banned.mil"];
	let name3: [&str; 4] = ["foo.test", "bar.test", "qux.example", "notbanned.xmil"];
	let name4: [&str; 1] = ["mil"];
	let name5: [&str; 1] = ["xmil"];
	assert!(check_name_constraints2(StringAdapter(&name,0), StringAdapter(&null,0),
		StringAdapter(&disallows,0)));
	assert!(!check_name_constraints2(StringAdapter(&name2,0), StringAdapter(&null,0),
		StringAdapter(&disallows,0)));
	assert!(check_name_constraints2(StringAdapter(&name3,0), StringAdapter(&null,0),
		StringAdapter(&disallows,0)));
	assert!(!check_name_constraints2(StringAdapter(&name4,0), StringAdapter(&null,0),
		StringAdapter(&disallows,0)));
	assert!(check_name_constraints2(StringAdapter(&name5,0), StringAdapter(&null,0),
		StringAdapter(&disallows,0)));
	assert!(check_name_constraints2(StringAdapter(&name,0), StringAdapter(&null,0),
		StringAdapter(&disallowsd,0)));
	assert!(!check_name_constraints2(StringAdapter(&name2,0), StringAdapter(&null,0),
		StringAdapter(&disallowsd,0)));
	assert!(check_name_constraints2(StringAdapter(&name3,0), StringAdapter(&null,0),
		StringAdapter(&disallowsd,0)));
	assert!(!check_name_constraints2(StringAdapter(&name4,0), StringAdapter(&null,0),
		StringAdapter(&disallowsd,0)));
	assert!(check_name_constraints2(StringAdapter(&name5,0), StringAdapter(&null,0),
		StringAdapter(&disallowsd,0)));
}

#[test]
fn test_name_constraints_allow()
{
	//If just allow, the names listed are allowed
	let null: [& str; 0] = [];
	let allows: [& str; 1] = ["gov"];
	let allowsd: [& str; 1] = [".gov"];
	let name: [&str; 3] = ["a.gov", "b.gov", "c.gov"];
	let name2: [&str; 4] = ["a.gov", "b.gov", "c.gov", "banned.xgov"];
	let name3: [&str; 1] = ["gov"];
	let name4: [&str; 1] = ["xgov"];
	assert!(check_name_constraints2(StringAdapter(&name,0), StringAdapter(&allows,0),
		StringAdapter(&null,0)));
	assert!(!check_name_constraints2(StringAdapter(&name2,0), StringAdapter(&allows,0),
		StringAdapter(&null,0)));
	assert!(check_name_constraints2(StringAdapter(&name3,0), StringAdapter(&allows,0),
		StringAdapter(&null,0)));
	assert!(!check_name_constraints2(StringAdapter(&name4,0), StringAdapter(&allows,0),
		StringAdapter(&null,0)));
	assert!(check_name_constraints2(StringAdapter(&name,0), StringAdapter(&allowsd,0),
		StringAdapter(&null,0)));
	assert!(!check_name_constraints2(StringAdapter(&name2,0), StringAdapter(&allowsd,0),
		StringAdapter(&null,0)));
	assert!(check_name_constraints2(StringAdapter(&name3,0), StringAdapter(&allowsd,0),
		StringAdapter(&null,0)));
	assert!(!check_name_constraints2(StringAdapter(&name4,0), StringAdapter(&allowsd,0),
		StringAdapter(&null,0)));
}

#[test]
fn test_name_constraints_allow_disallow()
{
	let allows: [& str; 1] = ["gov"];
	let allowsd: [& str; 1] = [".gov"];
	let disallows: [& str; 1] = ["banned.gov"];
	let disallowsd: [& str; 1] = [".banned.gov"];
	let name: [&str; 3] = ["a.gov", "b.gov", "c.gov"];
	let name2: [&str; 4] = ["a.gov", "b.gov", "c.gov", "x.banned.gov"];
	let name3: [&str; 1] = ["gov"];
	let name4: [&str; 1] = ["notbanned.gov"];
	let name5: [&str; 1] = ["banned.gov"];
	assert!(check_name_constraints2(StringAdapter(&name,0), StringAdapter(&allows,0),
		StringAdapter(&disallows,0)));
	assert!(!check_name_constraints2(StringAdapter(&name2,0), StringAdapter(&allows,0),
		StringAdapter(&disallows,0)));
	assert!(check_name_constraints2(StringAdapter(&name3,0), StringAdapter(&allows,0),
		StringAdapter(&disallows,0)));
	assert!(check_name_constraints2(StringAdapter(&name4,0), StringAdapter(&allows,0),
		StringAdapter(&disallows,0)));
	assert!(!check_name_constraints2(StringAdapter(&name5,0), StringAdapter(&allows,0),
		StringAdapter(&disallows,0)));
	assert!(check_name_constraints2(StringAdapter(&name,0), StringAdapter(&allowsd,0),
		StringAdapter(&disallowsd,0)));
	assert!(!check_name_constraints2(StringAdapter(&name2,0), StringAdapter(&allowsd,0),
		StringAdapter(&disallowsd,0)));
	assert!(check_name_constraints2(StringAdapter(&name3,0), StringAdapter(&allowsd,0),
		StringAdapter(&disallowsd,0)));
	assert!(check_name_constraints2(StringAdapter(&name4,0), StringAdapter(&allowsd,0),
		StringAdapter(&disallowsd,0)));
	assert!(!check_name_constraints2(StringAdapter(&name5,0), StringAdapter(&allowsd,0),
		StringAdapter(&disallowsd,0)));
}


#[cfg(test)]
fn names_match_u(i: &str, refname: &str) -> bool { names_match(i.as_bytes(), refname.as_bytes()) }

#[test]
fn basic_matching()
{
	assert!(names_match_u("foo.example.org", "foo.example.org"));		//Basic.
	assert!(names_match_u("*.example.org", "foo.example.org"));		//Wildcard.
	assert!(!names_match_u("foo.example.org", "bar.example.org"));		//Basic mismatch first label.
	assert!(!names_match_u("foo.example.org", "foo.example.com"));		//Basic mismatch last lablel.
	assert!(!names_match_u("foo.example.org", "*.example.org"));		//Wildcard wrong way around
	assert!(!names_match_u("*.example.org", "bar.foo.example.com"));	//Multiple labels from wildcard.
	assert!(!names_match_u("*.example.org", "example.com"));		//No labels from wildcard.
	assert!(!names_match_u("foo.example.org", "foo.example.orgx"));		//Extra characters.
	assert!(!names_match_u("*.example.org", "foo.example.orgx"));		//Extra characters (wildcard).
	assert!(!names_match_u("foo.example.orgx", "foo.example.org"));		//Extra characters.
	assert!(!names_match_u("*.example.orgx", "foo.example.org"));		//Extra characters (wildcard).
}

#[test]
fn case_insensitive_matching()
{
	assert!(names_match_u("foo.ExAmPlE.oRg", "foo.example.org"));	//Basic case-insenstive.
	assert!(names_match_u("foo.ExAmPlE.oRg", "foo.eXaMpLe.OrG"));	//Basic case-insenstive.
	assert!(names_match_u("*.ExAmPlE.oRg", "foo.example.org"));	//Wildcard case-insenstive
	assert!(names_match_u("*.ExAmPlE.oRg", "foo.eXaMpLe.OrG"));	//Wildcard case-insenstive
	assert!(!names_match_u("foo.ExAmPlE.oRg", "foo.Fxample.org"));	//Middle labels don't match.
	assert!(!names_match_u("föö.example.org", "fÖÖ.example.org"));	//Non-ASCII is not case-mapped.
	assert!(!names_match_u("x-o.example.org", "xMo.example.org"));	//2D and 4D is not mapped.
	assert!(!names_match_u("x-o.example.org", "x\ro.example.org"));	//2D and 0D is not mapped.
	assert!(!names_match_u("x\ro.example.org", "x-o.example.org"));	//2D and 0D is not mapped.
	assert!(!names_match_u("xMo.example.org", "x-o.example.org"));	//2D and 4D is not mapped.
	assert!(!names_match_u("fo\u{e1}.example.org", "foA!.example.org"));	//High bits aren't lost.
	assert!(!names_match_u("fo\u{c1}.example.org", "fo\u{e1}.example.org"));	//81 and A1 aren't mapped.
	assert!(!names_match(b"fo\xc1.example.org", b"fo\xe1.example.org"));	//C1 and E1 aren't mapped.
}

static ED25519_OID: &'static [u8] = &[43,101,112];

fn key_for_entity(entity: &[u8]) -> ([u8;32], [u8;32])
{
	static PREFIX: &'static [u8] = b"TEST-KEY-DERIVATION-PREFIX";
	let sk = Sha256::hash_v(&[PREFIX, entity]);
	let mut pk = [0;32];
	ed25519_pubkey(&mut pk, &sk);
	(sk, pk)
}

fn sign_certificate(key: ([u8;32],[u8;32]), tbs: &[u8]) -> Vec<u8>
{
	let mut sig = [0;64];
	ed25519_sign(&key.0, &key.1, tbs, &mut sig);
	let x = rope3!(SEQUENCE(
		tbs,
		SEQUENCE(OBJECT_IDENTIFIER ED25519_OID),
		BIT_STRING sig
	));
	FragmentWriteOps::to_vector(&x)
}

fn subject_by_common_name(cn: &[u8]) -> Vec<u8>
{
	let x = rope3!(SEQUENCE(SET(SEQUENCE(
		OBJECT_IDENTIFIER X509_SUBJECT_COMMON_NAME,
		UTF8_STRING cn
	))));
	FragmentWriteOps::to_vector(&x)
}

fn spki<'a>(raw_key: &'a [u8]) -> impl FragmentWrite+'a
{
	rope3!(SEQUENCE(
		SEQUENCE(OBJECT_IDENTIFIER ED25519_OID),
		BIT_STRING raw_key
	))
}

fn make_certificate(issuer_key: ([u8;32], [u8;32]), issuer_cn: &[u8], subject_key: [u8;32], subject_cn: &[u8],
	serial: &[u8], is_ca: bool, maxpath: Option<u8>, sans: &[&[u8]]) -> Vec<u8>
{
	let mut san_list = Vec::new();
	for &san in sans {
		let x = rope3!([2] san);
		FragmentWriteOps::extend_vector(&x, &mut san_list);
	}
	//Key usage.
	let maxpath = if let (true, Some(maxpath)) = (is_ca, maxpath) { maxpath as u8 } else { 255 };
	
	let tbs = rope3!(SEQUENCE(
		[0](INTEGER 2u8),
		INTEGER serial,
		SEQUENCE(OBJECT_IDENTIFIER ED25519_OID),
		issuer_cn,
		SEQUENCE(
			UTC_TIME "200101000000Z",
			UTC_TIME "201231235959Z"
		),
		subject_cn,
		{spki(&subject_key)},
		[3](SEQUENCE(
			SEQUENCE(
				OBJECT_IDENTIFIER X509_EXT_BASIC_CONSTRAINTS,
				BOOLEAN_TRUE,
				OCTET_STRING SEQUENCE(
					if is_ca BOOLEAN_TRUE,
					if {maxpath < 255} INTEGER maxpath
				)
			),
			SEQUENCE(
				OBJECT_IDENTIFIER X509_EXT_KEY_USAGE,
				BOOLEAN_TRUE,
				OCTET_STRING BIT_STRING if is_ca 4u8 else 128u8
			),
			SEQUENCE(
				OBJECT_IDENTIFIER X509_EXT_EKU,
				OCTET_STRING SEQUENCE(
					OBJECT_IDENTIFIER X509_EKU_TLS_SERVER
				)
			),
			if {!is_ca} SEQUENCE(
				OBJECT_IDENTIFIER X509_EXT_SAN,
				OCTET_STRING SEQUENCE(
					san_list
				)
			)
		))
	));
	let tbs = FragmentWriteOps::to_vector(&tbs);
	sign_certificate(issuer_key, &tbs)
}

#[allow(deprecated)]
fn __chain_valid_raw(ee: &[u8], ca: &[&[u8]], mut tas: Vec<TrustAnchor>) -> Result<(), CertificateValidationError>
{
	let mut taset = BTreeSet::new();
	for ta in tas.drain(..) { taset.insert(TrustAnchorOrdered::new(ta)); }
	let pins: &[HostSpecificPin] = &[];
	let trust = (&taset, pins, SignatureAlgorithmEnabled2::unsafe_any_policy());
	let mut p = SanityCheckCertificateChain2 {
		strict_algo: false,
		feature_flags: 0,
		killist: &BTreeSet::new(),
		need_ocsp_flag: &mut false,
		need_sct_flag: &mut false,
		is_client: false,
		policy: SignatureAlgorithmEnabled2::unsafe_any_policy(),
		require_disclosed_ca: false,
		extended: Default::default(),
	};
	p.extended.test_ignore_build = true;
	sanity_check_certificate_chain2(ee, ca, Some(ReferenceName::DnsName("leaf.test")),
		&Timestamp::new(1598093645i64), p, validate_certificate_after_sanity_check2, trust)
}

fn assert_chain_valid_anchor(ee: &[u8], ca: &[&[u8]], tas: Vec<TrustAnchor>)
{
	__chain_valid_raw(ee, ca, tas).expect("Validating certificate failed");
}

fn assert_chain_valid_failed(ee: &[u8], ca: &[&[u8]], tas: Vec<TrustAnchor>, err: &str)
{
	match __chain_valid_raw(ee, ca, tas) {
		Ok(_) => panic!("Chain validation succeeded when it should not!"),
		Err(e) => assert_eq!(&format!("CertificateValidationError({err})"), &format!("{e:?}")),
	}
}

fn make_entity(x: &[u8]) -> (Vec<u8>, ([u8;32], [u8;32]))
{
	let cn = subject_by_common_name(x);
	let key = key_for_entity(&cn);
	(cn, key)
}

fn make_ta(cn: &[u8], pkey: &[u8]) -> TrustAnchor
{
	let pkey = FragmentWriteOps::to_vector(&spki(pkey));
	TrustAnchor::from_name_spki(&cn, &pkey, &[], &[]).expect("Bad TA")
}

fn make_chain_3(ca0: bool, pl0: Option<u8>) -> (Vec<u8>, Vec<u8>, Vec<u8>, TrustAnchor)
{
	let (cn_a, key_a) = make_entity(b"Leaf");
	let (cn_b, key_b) = make_entity(b"Intermediate");
	let (cn_c, key_c) = make_entity(b"Root");
	let cert_a = make_certificate(key_b, &cn_b, key_a.1, &cn_a, &[5], false, None, &[b"leaf.test"]);
	let cert_b = make_certificate(key_c, &cn_c, key_b.1, &cn_b, &[2], ca0, pl0, &[]);
	let cert_c = make_certificate(key_c, &cn_c, key_c.1, &cn_c, &[1], true, None, &[]);
	let ta_c = make_ta(&cn_c, &key_c.1);
	(cert_a, cert_b, cert_c, ta_c)
}

fn make_chain_4(ca0: bool, pl0: Option<u8>, ca1: bool, pl1: Option<u8>) ->
	(Vec<u8>, Vec<u8>, Vec<u8>, Vec<u8>, TrustAnchor, TrustAnchor)
{
	let (cn_a, key_a) = make_entity(b"Leaf");
	let (cn_b, key_b) = make_entity(b"Intermediate");
	let (cn_c, key_c) = make_entity(b"Root");
	let (cn_d, key_d) = make_entity(b"OldRoot");
	let cert_a = make_certificate(key_b, &cn_b, key_a.1, &cn_a, &[5], false, None, &[b"leaf.test"]);
	let cert_b = make_certificate(key_c, &cn_c, key_b.1, &cn_b, &[2], ca0, pl0, &[]);
	let cert_c = make_certificate(key_d, &cn_d, key_c.1, &cn_c, &[1], ca1, pl1, &[]);
	let cert_d = make_certificate(key_d, &cn_d, key_d.1, &cn_d, &[1], true, None, &[]);
	let ta_c = make_ta(&cn_c, &key_c.1);
	let ta_d = make_ta(&cn_d, &key_d.1);
	(cert_a, cert_b, cert_c, cert_d, ta_c, ta_d)
}

#[test]
fn basic_certificate_chain()
{
	let (cert_a, cert_b, _, ta_c) = make_chain_3(true, None);
	assert_chain_valid_anchor(&cert_a, &[&cert_b], vec![ta_c]);
}

#[test]
fn certificate_missing()
{
	let (cert_a,  _, _, ta_c) = make_chain_3(true, None);
	assert_chain_valid_failed(&cert_a, &[], vec![ta_c], "CaUnknown");
}

#[test]
fn certificate_from_bad_ca()
{
	let (cert_a, cert_b, _, _) = make_chain_3(true, None);
	let (cn_d, key_d) = make_entity(b"OldRoot");
	let ta_d = make_ta(&cn_d, &key_d.1);
	assert_chain_valid_failed(&cert_a, &[&cert_b], vec![ta_d], "CaUnknown");
}

#[test]
fn wrong_root_key()
{
	let (cert_a, cert_b, cert_c, _) = make_chain_3(true, None);
	let (_, key_d) = make_entity(b"Root2");
	let cn_d = subject_by_common_name(b"Root");
	let ta_d = make_ta(&cn_d, &key_d.1);
	assert_chain_valid_failed(&cert_a, &[&cert_b, &cert_c], vec![ta_d], "RootPublicKeyMismatch");
}

#[test]
fn basic_certificate_chain_corrupt_sig0()
{
	let (mut cert_a, cert_b, _, ta_c) = make_chain_3(true, None);
	cert_a.last_mut().map(|x|*x ^= 1);
	assert_chain_valid_failed(&cert_a, &[&cert_b], vec![ta_c],
		"SignatureFailure(0, SignatureError2(VerifyError))");
}

#[test]
fn basic_certificate_chain_corrupt_sig1()
{
	let (cert_a, mut cert_b, _, ta_c) = make_chain_3(true, None);
	cert_b.last_mut().map(|x|*x ^= 1);
	assert_chain_valid_failed(&cert_a, &[&cert_b], vec![ta_c],
		"SignatureFailure(1, SignatureError2(VerifyError))");
}

#[test]
fn basic_certificate_chain_notca()
{
	let (cert_a, cert_b, _, ta_c) = make_chain_3(false, None);
	assert_chain_valid_failed(&cert_a, &[&cert_b], vec![ta_c], "CaIsNotCa(0)");
}

#[test]
fn pathlen_certificate_chain()
{
	let (cert_a, cert_b, _, ta_c) = make_chain_3(true, Some(0));
	assert_chain_valid_anchor(&cert_a, &[&cert_b], vec![ta_c]);
}

#[test]
fn pathlen_certificate_chain_1()
{
	let (cert_a, cert_b, _, ta_c) = make_chain_3(true, Some(1));
	assert_chain_valid_anchor(&cert_a, &[&cert_b], vec![ta_c]);
}

#[test]
fn basic_certificate_chain_redundant_root()
{
	let (cert_a, cert_b, cert_c, ta_c) = make_chain_3(true, None);
	assert_chain_valid_anchor(&cert_a, &[&cert_b, &cert_c], vec![ta_c]);
}

#[test]
fn basic_certificate_chain_redundant_root_junk()
{
	let (cert_a, cert_b, cert_c, ta_c) = make_chain_3(true, None);
	assert_chain_valid_failed(&cert_a, &[&cert_b, &cert_c, &cert_c], vec![ta_c],
		"SelfsignedCertificateNotLast");
}

#[test]
fn basic_certificate_chain3()
{
	let (cert_a, cert_b, cert_c, _, _, ta_d) = make_chain_4(true, None, true, None);
	assert_chain_valid_anchor(&cert_a, &[&cert_b, &cert_c], vec![ta_d]);
}

#[test]
fn basic_certificate_chain3_md5()
{
	let (cert_a, cert_b, mut cert_c, _, ta_c, _) = make_chain_4(true, None, true, None);
	//Trash the algorithm IDs to simulate bad algorithm.
	for i in 19..22 { cert_c[i] = 0; }
	for i in cert_c.len()-70..cert_c.len()-67 { cert_c[i] = 0; }
	assert_chain_valid_anchor(&cert_a, &[&cert_b, &cert_c], vec![ta_c]);
}

#[test]
fn basic_certificate_chain3_md5_bad()
{
	let (cert_a, cert_b, mut cert_c, _, _, ta_d) = make_chain_4(true, None, true, None);
	//Trash the algorithm IDs to simulate bad algorithm.
	for i in 19..22 { cert_c[i] = 0; }
	for i in cert_c.len()-70..cert_c.len()-67 { cert_c[i] = 0; }
	assert_chain_valid_failed(&cert_a, &[&cert_b, &cert_c], vec![ta_d], "ChainBadSignatureAlgorithm");
}

#[test]
fn bad_certificate_chain3()
{
	let (cn_a, key_a) = make_entity(b"Leaf");
	let (cn_b, key_b) = make_entity(b"Intermediate");
	let (cn_c, key_c) = make_entity(b"Root");
	let (cn_d, key_d) = make_entity(b"OldRoot");
	let cn_c2 = subject_by_common_name(b"Root2");
	let cert_a = make_certificate(key_b, &cn_b, key_a.1, &cn_a, &[5], false, None, &[b"leaf.test"]);
	let cert_b = make_certificate(key_c, &cn_c, key_b.1, &cn_b, &[2], true, None, &[]);
	let cert_c = make_certificate(key_d, &cn_d, key_c.1, &cn_c2, &[1], true, None, &[]);
	let ta_d = make_ta(&cn_d, &key_d.1);
	assert_chain_valid_failed(&cert_a, &[&cert_b, &cert_c], vec![ta_d], "BrokenChain(1)");
}

#[test]
fn certificate_missing_3()
{
	let (cert_a, cert_b, _, _, _, ta_d) = make_chain_4(true, None, true, None);
	assert_chain_valid_failed(&cert_a, &[&cert_b], vec![ta_d], "CaUnknown");
}

#[test]
fn pathlen_certificate_chain3()
{
	let (cert_a, cert_b, cert_c, _, _, ta_d) = make_chain_4(true, Some(0), true, None);
	assert_chain_valid_anchor(&cert_a, &[&cert_b, &cert_c], vec![ta_d]);
}

#[test]
fn pathlen_certificate_chain3_1()
{
	let (cert_a, cert_b, cert_c, _, _, ta_d) = make_chain_4(true, Some(0), true, Some(1));
	assert_chain_valid_anchor(&cert_a, &[&cert_b, &cert_c], vec![ta_d]);
}

#[test]
fn pathlen_certificate_chain3_1_bad()
{
	let (cert_a, cert_b, cert_c, _, _, ta_d) = make_chain_4(true, Some(0), true, Some(0));
	assert_chain_valid_failed(&cert_a, &[&cert_b, &cert_c], vec![ta_d], "PathLenViolation");
}

#[test]
fn pathlen_certificate_chain3_1_bad2()
{
	let (cert_a, cert_b, cert_c, _, _, ta_d) = make_chain_4(true, Some(1), true, Some(0));
	assert_chain_valid_failed(&cert_a, &[&cert_b, &cert_c], vec![ta_d], "PathLenViolation");
}

#[test]
fn pathlen_certificate_chain3_1_bad3()
{
	let (cert_a, cert_b, cert_c, _, _, ta_d) = make_chain_4(true, None, true, Some(1));
	assert_chain_valid_failed(&cert_a, &[&cert_b, &cert_c], vec![ta_d], "PathLenViolation");
}

#[test]
fn basic_certificate_chain_redundant_root3()
{
	let (cert_a, cert_b, cert_c, cert_d, _, ta_d) = make_chain_4(true, None, true, None);
	assert_chain_valid_anchor(&cert_a, &[&cert_b, &cert_c, &cert_d], vec![ta_d]);
}

#[test]
fn basic_certificate_chain_new()
{
	let (cert_a, cert_b, cert_c, _, ta_c, _) = make_chain_4(true, None, true, None);
	assert_chain_valid_anchor(&cert_a, &[&cert_b, &cert_c], vec![ta_c]);
}

#[test]
fn basic_certificate_chain_redundant_root_new()
{
	let (cert_a, cert_b, cert_c, cert_d, ta_c, _) = make_chain_4(true, None, true, None);
	assert_chain_valid_anchor(&cert_a, &[&cert_b, &cert_c, &cert_d], vec![ta_c]);
}

//FIXME: Test HostSpecificPin
//FIXME: Test TrustAnchor
//FIXME: Test TrustAnchorOrdered
//FIXME: Test DataRepresentation
//FIXME: Test NameConstraint
//FIXME: Test ip_in_set
//FIXME: Test name_in_set
//FIXME: Test sanity_check_certificate_chain_load
//FIXME: Test validate_ee_spki
