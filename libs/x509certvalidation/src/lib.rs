//!X.509 certificate chain validator.
//!
//!This crate contains code to validate X.509 certificate chains.
//!
//!The most important items are:
//!
//! * Check if specified name belongs to specified set (with wildcard matching): [`name_in_set2`]
//! * Check if specified IP belongs to specified set: [`ip_in_set`]
//! * Perform a basic use-time sanity check on certificate chain: [`sanity_check_certificate_chain`]
//! * Perform a basic load-time sanity check on certificate chain: [`sanity_check_certificate_chain_load`]
//! * After sanity check is done, validate a certificate chain: [`validate_certificate_after_sanity_check`]. Note
//!that this function uses information extracted by [`sanity_check_certificate_chain`]
//! * Validate an End-Entity SubjectPublicKeyInfo against pins: [`validate_ee_spki`].
//!
//![`name_in_set2`]: fn.name_in_set2.html
//![`ip_in_set`]: fn.ip_in_set.html
//![`sanity_check_certificate_chain`]: fn.sanity_check_certificate_chain.html
//![`sanity_check_certificate_chain_load`]: fn.sanity_check_certificate_chain_load.html
//![`validate_certificate_after_sanity_check`]: fn.validate_certificate_after_sanity_check.html
//![`validate_chain`]: fn.validate_chain.html
//![`validate_chain_client2`]: fn.validate_chain_client2.html
//![`validate_ee_spki`]: fn.validate_ee_spki.html
#![deny(missing_docs)]
#![allow(unsafe_code)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]
#[cfg(test)] #[macro_use] extern crate std;

use btls_aux_assert::AssertFailed;
use btls_aux_collections::Arc;
use btls_aux_collections::BTreeMap;
use btls_aux_collections::BTreeSet;
use btls_aux_collections::String;
use btls_aux_collections::ToOwned;
use btls_aux_collections::ToString;
use btls_aux_collections::Vec;
use btls_aux_fail::dtry;
use btls_aux_fail::f_break;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::fail_if_none;
use btls_aux_fail::ResultExt;
use btls_aux_fail::ttry;
use btls_aux_hash::Hash;
use btls_aux_hash::HashFunctionEnabled;
use btls_aux_hash::sha256;
pub use btls_aux_ip::IpAddress;
use btls_aux_memory::Attachment;
use btls_aux_memory::EscapeByteString;
use btls_aux_memory::Hexdump;
use btls_aux_memory::split_array_head;
use btls_aux_memory::split_attach_first;
use btls_aux_memory::split_head_tail;
use btls_aux_publiccalist::get_build_date;
use btls_aux_publiccalist::is_expired_ca as _is_expired_ca;
use btls_aux_publiccalist::is_public_intermediate;
use btls_aux_publiccalist::is_public_ca;
use btls_aux_publiccalist::is_revoked;
use btls_aux_publiccalist::is_ta_blacklisted;
use btls_aux_publiccalist::known_signature;
use btls_aux_serialization::Source;
use btls_aux_serialization::Sink;
use btls_aux_serialization::VectorLength;
pub use btls_aux_signatures::SignatureAlgorithmEnabled2;
use btls_aux_signatures::SignatureError2;
use btls_aux_signatures::SignatureFlags2;
use btls_aux_signatures::SignatureX5092;
use btls_aux_time::TimeInterface;
pub use btls_aux_time::Timestamp;
use btls_aux_x509certparse::CertificateError;
pub use btls_aux_x509certparse::CertificateIssuer;
pub use btls_aux_x509certparse::CertificateSubject;
use btls_aux_x509certparse::CFLAG_MUST_STAPLE;
use btls_aux_x509certparse::CFLAG_UNKNOWN;
use btls_aux_x509certparse::DecodeTsForUser;
use btls_aux_x509certparse::IpAddressBlock;
pub use btls_aux_x509certparse::ParsedCertificate2;
use core::borrow::Borrow;
use core::cmp::Ordering;
use core::convert::TryInto;
use core::ops::Deref;
use core::ops::Range;
use core::fmt::Debug;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::fmt::Write;
use core::iter::once;
use core::marker::PhantomData;


///A representation of a piece of data.
///
///The data is represented either as-is, or as hash of the data. The assumption that a piece of data can be
///represented by its hash relies on collision resistance of the hash. It is not justified with weak hashes
///(e.g., SHA-1).
///
///Currently, only SHA-2 and SHA-3 hashes of 256, 384 and 512 bits are supported.
///
///This enumeration is intended to be constructed manually.
///
///There is method [`matches()`](#method.matches) that checks if given piece of data matches this data
///representation.
#[derive(Clone)]
#[non_exhaustive]
pub enum DataRepresentation
{
	///The actual value of data.
	///
	///Unlike the other variants, this is not a hash, but explicit value to match against. A piece of data
	///matches this if and only if it is bitwise equal to the one given.
	Value(Vec<u8>),
	///The SHA-256 hash of data.
	///
	///A piece of data matches this if and only if it is SHA-256 preimage of the given hash.
	Sha256([u8;32]),
	///The SHA-384 hash of data.
	///
	///A piece of data matches this if and only if it is SHA-384 preimage of the given hash.
	Sha384([u8;48]),
	///The SHA-512 hash of data.
	///
	///A piece of data matches this if and only if it is SHA-512 preimage of the given hash.
	Sha512([u8;64]),
	///The SHA3-256 hash of data.
	///
	///A piece of data matches this if and only if it is SHA3-256 preimage of the given hash.
	Sha3256([u8;32]),
	///The SHA3-384 hash of data.
	///
	///A piece of data matches this if and only if it is SHA3-384 preimage of the given hash.
	Sha3384([u8;48]),
	///The SHA3-512 hash of data.
	///
	///A piece of data matches this if and only if it is SHA3-512 preimage of the given hash.
	Sha3512([u8;64]),
	///Unsatisfiable entry.
	///
	///A piece of data never matches this.
	Unsatisfiable,
}

macro_rules! check_hash
{
	($($x:ident)::*, $data:expr, $refx:expr, $policy:expr) => {{
		$policy.allows($($x)::*::function()) && &$($x)::*::hash($data)[..] == &$refx[..]
	}};
}

impl DataRepresentation
{
	#[allow(missing_docs)]
	#[deprecated(since="2.1.0", note="Fundamentially unsafe, use matches2() instead")]
	pub fn matches(&self, data: &[u8]) -> bool
	{
		self.matches2(data, HashFunctionEnabled::unsafe_any_policy())
	}
	///Check if given data `data` matches this data representation.
	///
	///Note however, that this function is only reliable if strong hash function is used: It relies on the
	///collision resistance, which is the first thing to go when hash function breaks.
	pub fn matches2(&self, data: &[u8], pol: HashFunctionEnabled) -> bool
	{
		match self {
			&DataRepresentation::Value(ref x) => x.deref() == data,
			&DataRepresentation::Sha256(ref x) => check_hash!(btls_aux_hash::Sha256, data, x, pol),
			&DataRepresentation::Sha384(ref x) => check_hash!(btls_aux_hash::Sha384, data, x, pol),
			&DataRepresentation::Sha512(ref x) => check_hash!(btls_aux_hash::Sha512, data, x, pol),
			&DataRepresentation::Sha3256(ref x) => check_hash!(btls_aux_hash::Sha3256, data, x, pol),
			&DataRepresentation::Sha3384(ref x) => check_hash!(btls_aux_hash::Sha3384, data, x, pol),
			&DataRepresentation::Sha3512(ref x) => check_hash!(btls_aux_hash::Sha3512, data, x, pol),
			&DataRepresentation::Unsatisfiable => false,
		}
	}
}

///A Host-specific certificate pin / trust anchor.
///
///This structure contains a host-specific certificate pin or trust anchor. It can be used e.g. for implemeting
///DANE TLSA (RFC6698) or HPKP (RFC7469) support.
///
///If the End-Entity SubjectPublicKeyInfo is both pinned (including implicit pinning) and a trust anchor, then
///Raw Public Key support will be enabled.
///
///However, there are limitations to this mechanism:
///
/// * Matching of an X.509 Trust Anchor is only possible via SubjectPublicKeyInfo.
/// * Certificate pins can only be on certificates sent in the chain or X.509 Trust Anchors
/// * The entries, even if given as full unhashed certificates, are never used to complete a chain.
///
///A consequence of these three limitations is that if one sets the root certificate server uses as a trust anchor
///via this mechanism, the server **must** send the root certificate explicitly (which is not normally done).
///
///This method can be constructed either manually, or by the following constructors:
///
/// * [`trust_server_key_by_sha256()`]: Construct a entry that trusts a certificate or key with a given SHA-256 hash.
/// * [`from_tlsa_raw()`]: Construct an entry corresponding to given TLSA record data.
///
///[`trust_server_key_by_sha256()`]: #method.trust_server_key_by_sha256
///[`from_tlsa_raw()`]: #method.from_tlsa_raw
#[derive(Clone)]
pub struct HostSpecificPin
{
	///CA/EE flag.
	///
	///This flag sets if this entry matches Certificate Authority or End-Entity certificates or both:
	///
	/// * `Some(true)` only matches Certificate Authority certifiates.
	/// * `Some(false)` only matches End-Entity certificates.
	/// * `None` matches both Certificate Authority and End-Entity certificates.
	pub ca_flag: Option<bool>,
	///Pin flag.
	///
	///If all entries used in validation have this flag `false`, then nothing special happens. However, if any
	///entry used in validation has this flag set to `true`, then the specified end-entity certificate must appear
	///in the constructed certificate chain. This may be combined with `ta` flag to have a certificate that is
	///both pinned and a trust anchor.
	pub pin: bool,
	///Trust Anchor flag.
	///
	///If this flag is set to `true`, the this entry acts as a trust anchor (however, it is not used to complete
	///a chain, unlike X.509 Trust Anchors). If this is set to `false`, then this entry is not an trust anchor.
	///This may be combined with `pin` flag to have a certificate that is both pinned and a trust anchor.
	///
	///Each constructed certificate chain must contain at least one trust anchor, and the chain ends at such.
	pub ta: bool,
	///SPKI flag.
	///
	///If this flag set to true, the entry matches the SubjectPublicKeyInfo part of the certificates. If it is
	///set to `false`, the entiere certificate is matched.
	pub spki: bool,
	///The data to match against.
	///
	///This field gives either the SubjectPublicKeyInfo or the full certificate (depending on the value of the
	///`spki` field) to match against. If the relevant part of the certificate matches this, and the certificate
	///is of the correct type (considering `ca_flag` field), then it is appiled, and the matchin certificate is
	///granted pin or trust anchor status (as dictated by the `pin` and `ta` fields).
	pub data: DataRepresentation,
}

impl HostSpecificPin
{
	///Construct a `HostSpecificPin` that matches the key or certificate with the specified SHA-256 hash
	///`sha256` and is marked as trust anchor. The returned pin is not marked as pinned, so do not mix this
	///with pinning.
	///
	///This is practicularly handy for testing servers with self-signed certificates.
	///
	///The flag parameters are interpretted as follows:
	///
	/// * If `whole_cert` is `false`, then the hash is hash of the SubjectPublicKeyInfo.
	/// * If `whole_cert` is `true`, then the hash is hash of the entiere certificate.
	/// * If `issuer` is `false`, then only the end-entity certificate is considered.
	/// * If `issuer` is `true`, then only certificate authority certificates are considered. Note that the
	///server has to send the certificate (including when the certificate is root certificate) for this to work.
	pub fn trust_server_key_by_sha256(sha256: &[u8; 32], whole_cert: bool, issuer: bool) ->
		HostSpecificPin
	{
		HostSpecificPin {
			ca_flag: Some(issuer),
			pin: false,
			ta: true,
			spki: !whole_cert,
			data: DataRepresentation::Sha256(*sha256)
		}
	}
	///Create a HostSpecificPin from raw RRDATA of DNS TLSA record `raw_tlsa`.
	///
	///The created entry always has the `pin` flag set. If usage field in the record is `2` or `3`, then the
	///created entry will have the `ta` flag set, otherwise the `ta` flag is clear.
	///
	///This function comes handy when implementing DANE support, as it can be used to directly convert the
	///received RRDATA into `HostSpecificPin` records.
	///
	///On success, returns `Ok(pin)`, where `pin` is the pin object. If the RRDATA does not correspond to any
	///valid TLSA record, returns  `Err(())`.
	pub fn from_tlsa_raw(raw_tlsa: &[u8]) -> Result<HostSpecificPin, ()>
	{
		//The three-byte header has to be present.
		let (header, raw_tlsa) = split_array_head::<_,3>(raw_tlsa).ok_or(())?;
		let rtype = header[0];
		let target = header[1];
		let hash = header[2];

		let (ca_flag, pin, ta) = match rtype {
			1 => (Some(false), true, false),
			2 => (Some(true), true, true),
			3 => (Some(false), true, true),
			_ => fail!(())
		};
		let spki = match target {
			0 => false,
			1 => true,
			_ => fail!(())
		};
		let data = match hash {
			0 => DataRepresentation::Value(raw_tlsa.to_owned()),
			1 => DataRepresentation::Sha256(dtry!(raw_tlsa.try_into())),
			2 => DataRepresentation::Sha512(dtry!(raw_tlsa.try_into())),
			_ => fail!(())
		};
		Ok(HostSpecificPin{ca_flag:ca_flag,pin:pin,ta:ta,spki:spki,data:data})
	}
	fn is_pinned_ta(&self, any_pin: bool) -> bool
	{
		//TA must be set, and pin too, unless !any_pin.
		self.ta && (self.pin || !any_pin)
	}
	fn matches_ee(&self) -> bool
	{
		//ca_flag must be none or false.
		!self.ca_flag.unwrap_or(false)
	}
	fn ee_matches(&self, ee_spki: &[u8], any_pin: bool, policy: HashFunctionEnabled) -> bool
	{
		//The following must be true:
		//
		//1) It must match end entity.
		//2) It must be pinned TA.
		//3) spki must be set
		//4) data must match.
		self.matches_ee() && self.is_pinned_ta(any_pin) && self.spki && self.data.matches2(ee_spki, policy)
	}
}

///Check that specified reference name is in specified set (also considering wildcards).
///
///The name to check is `refname` and the set is `set`.
///
///Note that wildcards are only allowed in leftmost label, and wildcards match exactly one label. So for instance,
///`*.foo` matches `bar.foo` but not `foo`. And `foo.*.bar` is not a wildcard at all.
///
///The matching is case-insensitive for the `ASCII` alphabetic characters (A-Z), but case-sensitive for other
///characters. So for example `example.org` matches `EXAMPLE.ORG`, but `exämple.org` does not match `EXÄMPLE.ORG`.
pub fn name_in_set2<'a,T:Iterator<Item=&'a [u8]>>(mut set: T, refname: &[u8]) -> bool
{
	set.any(|i|names_match(i, refname))
}

///Check that specified reference IP is in specified set.
///
///The IP to check is `refname` and the set is `set`.
///
///For IP addresses, there are no wildcards.
pub fn ip_in_set<'a,T:Iterator<Item=IpAddressBlock>>(mut set: T, refname: &IpAddressBlock) -> bool
{
	set.any(|i|refname.is_subset_of(&i))
}

//Check if (possibly wildcard) pattern matches reference name.
fn names_match(i: &[u8], refname: &[u8]) -> bool
{
	//Non-wildcard case: The names have to match case insenstively (ASCII case insensitive only, the other
	//(locale dependent!) characters are not case-mapped.
	if matches_ascii_case_insensitive(refname, i) { return true; }
	if let Some((b"*.", suffix)) = split_array_head::<_,2>(i) {
		//Wildcard. In this case, remove the '*.' from pattern, and leftmost label from reference name,
		//and compare.
		if let Ok((_, refsuffix)) = split_attach_first(refname, b".", Attachment::Center) {
			if suffix.len() > 0 && matches_ascii_case_insensitive(suffix, refsuffix) { return true; }
		}
	}
	false
}

//Check if two names match ASCII case insensitively.
fn matches_ascii_case_insensitive(a: &[u8], b: &[u8]) -> bool
{
	//Map both strings to lowercase (ASCII alphabets only) and then compare.
	let itera = a.iter().map(u8::to_ascii_lowercase);
	let iterb = b.iter().map(u8::to_ascii_lowercase);
	itera.eq(iterb)
}

///Variant for the TLS certificate error classification.
///
///This enumeration holds a major category of certificate validation error. This classification is according to the
///certificate-related fatal TLS alerts.
///
///This enumeration is usually constructed by the [`CertificateValidationError::problem_class()`] method.
///
///[`CertificateValidationError::problem_class()`]: struct.CertificateValidationError.html#method.problem_class
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum MajorCertProblem
{
	///TLS alert `bad_certificate` should be raised.
	Bad,
	///TLS alert `unsupported_certificate` shold be raised.
	Unsupported,
	///TLS alert `certificate_expired` should be raised.
	Expired,
	///TLS alert `bad_certificate_status_response` should be raised.
	Ocsp,
	///TLS alert `certificate_unknown` should be raised.
	Unspecified,
	///TLS alert `unknown_ca` should be raised.
	UnknownCa,
	///TLS alert `internal_error` should be raised.
	InternalError,
}

///Error in validating certificate.
///
///This structure holds reason why validating a certificate failed.
///
///This structure is constructed by various certificate validation methods. It can not be constructed manually, nor
///it does have any constructors. It can be formatted in human-readable form by using the `{}` formatting operator.
///
///There is the [`problem_class()`](#method.problem_class) method, which returns the TLS alert that should be sent
///for this error.
#[derive(Clone,Debug,PartialEq,Eq)]
pub struct CertificateValidationError(_CertificateValidationError);
#[derive(Clone,Debug,PartialEq,Eq)]
enum _CertificateValidationError
{
	//Assertion failed!
	AssertionFailed(AssertFailed),
	//Path length constraint violated.
	PathLenViolation,
	//OCSP stapling is required.
	OcspStapleRequred,
	//Certificate requires unknown features.
	RequireUnknownFeatures,
	//Certificate has unmet feature requirements.
	UnmetFeatures(u32),
	//Error parsing CA certificate.
	CaCertParseError(usize, CertificateError),
	//CA did not issue EE certificate.
	DidNotIssueEeCert(usize),
	//CA key is kill-listed.
	CaKillisted(usize),
	//CA keys collide with EE keys.
	CaCantHaveEeKeys(usize),
	//CA signature algorithm mismatch.
	CaSignatureMismatch(usize),
	//CA not yet valid.
	CaNotYetValid(usize, Timestamp),
	//CA expired.
	CaExpired(usize, Timestamp),
	//CA is not marked as a CA.
	CaIsNotCa(usize),
	//CA certificate not valid for TLS.
	CaNotValidForTls(usize),
	//Name constraints violated.
	NameConstraintViolation(usize, String),
	//Name constraints violated for IP address.
	NameConstraintViolationIp(usize, IpAddressBlock),
	//CA uses bad signature algorithm.
	CaBadSignatureAlgorithm(usize),
	//Error parsing EE certificate.
	EeCertParseError(CertificateError),
	//EE key is kill-listed.
	EeKillisted,
	//EE signature algorithm mismatch.
	EeSignatureMismatch,
	//EE certificate not valid for TLS.
	EeNotValidForTls,
	//EE certificate is a CA.
	EeCertificateCa,
	//EE certificate not valid for name.
	EeNotValidForName(String, String),
	//EE certificate not valid for IP adddress.
	EeNotValidForIp(IpAddressBlock, String),
	//EE certificate not yet valid.
	EeNotYetValid(Timestamp),
	//EE certificate expired.
	EeExpired(Timestamp),
	//EE validity too long.
	EeValidityTooLong(u64, u64),
	//EE certificate is self-signed.
	SelfSigned,
	//EE certificate has bad signature algorithm.
	EeBadSignatureAlgorithm,
	//No EE issuer certificate found.
	NoIssuerCert,
	//No certificates match pins.
	NoCertifcatesMatchPins,
	//Certificate not signed by any known CA.
	CaUnknown,
	//Signature verification failed.
	SignatureFailure(usize, SignatureError2),
	//Signature unknown algorithm.
	SignatureUnknown(usize),
	//Public key mismatch against provoded one.
	PublicKeyMismatch,
	//Certificate chain can not be validated due to CA using bad signature algorithm.
	ChainBadSignatureAlgorithm,
	//Certificate chain can not be validated due to CA certificate being revoked.
	ChainRevoked,
	//Undisclosed unconstrained subCA of public CA in chain.
	UndisclosedCa(usize),
	//Is ACME validation certificate.
	AcmeValidationCertificate,
	//Bad IP in CA name constraint.
	BadIpRange(usize),
	//Root Public key mismatch.
	RootPublicKeyMismatch,
	//Self-signed certificate not last.
	SelfsignedCertificateNotLast,
	//Broken certificate chain.
	BrokenChain(usize),
	//Validity ranges do not nest.
	TimeNotNested(usize),
	//Ee certificate issuer expired.
	EeCertIssuerExpired,
	//Ca certificate issuer expired.
	CaCertIssuerExpired(usize),
}

impl CertificateValidationError
{
	///Is CaUnknown?
	pub fn is_ca_unknown(&self) -> bool { matches!(&self.0, _CertificateValidationError::CaUnknown) }
	///This method returns the class of the problem, for sending the correct TLS alert for the certificate
	///problem. These problem classes map one-to-one with TLS alert codes.
	pub fn problem_class(&self) -> MajorCertProblem
	{
		use self::_CertificateValidationError::*;
		use self::MajorCertProblem::*;
		match &self.0 {
			&AssertionFailed(_) => InternalError,
			&PathLenViolation => Bad,
			&OcspStapleRequred => Ocsp,
			&RequireUnknownFeatures => Unsupported,
			&UnmetFeatures(_) => Unsupported,
			&CaCertParseError(_, _) => Unsupported,
			&DidNotIssueEeCert(_) => Bad,
			&CaKillisted(_) => UnknownCa,
			&CaCantHaveEeKeys(_) => Bad,
			&CaSignatureMismatch(_) => Bad,
			&CaNotYetValid(_, _) => Expired,
			&CaExpired(_, _) => Expired,
			&CaIsNotCa(_) => Bad,
			&CaNotValidForTls(_) => Bad,
			&NameConstraintViolation(_, _) => Bad,
			&NameConstraintViolationIp(_, _) => Bad,
			&CaBadSignatureAlgorithm(_) => Unsupported,
			&EeCertParseError(_) => Unsupported,
			&EeKillisted => UnknownCa,
			&EeSignatureMismatch => Bad,
			&EeNotValidForTls => Bad,
			&EeCertificateCa => Bad,
			&EeNotValidForName(_, _) => Bad,
			&EeNotValidForIp(_, _) => Bad,
			&EeNotYetValid(_) => Expired,
			&EeExpired(_) => Expired,
			&EeValidityTooLong(_,_) => Expired,
			&SelfSigned => Unsupported,
			&EeBadSignatureAlgorithm => Unsupported,
			&NoIssuerCert => Unsupported,
			&NoCertifcatesMatchPins => UnknownCa,
			&CaUnknown => UnknownCa,
			&SignatureFailure(_, _) => Bad,
			&SignatureUnknown(_) => Bad,
			&PublicKeyMismatch => Bad,	//Should never happen.
			&ChainBadSignatureAlgorithm => Unsupported,
			&ChainRevoked => Ocsp,		//This is really a revocation error.
			&UndisclosedCa(_) => Ocsp,	//This is really a revocation error.
			&AcmeValidationCertificate => Bad,
			&BadIpRange(_) => Bad,
			&RootPublicKeyMismatch => UnknownCa,
			&SelfsignedCertificateNotLast => Bad,
			&BrokenChain(_) => Bad,
			&TimeNotNested(_) => Bad,
			&EeCertIssuerExpired => Expired,
			&CaCertIssuerExpired(_) => Expired,
		}
	}
}

impl From<_CertificateValidationError> for CertificateValidationError
{
	fn from(x: _CertificateValidationError) -> CertificateValidationError { CertificateValidationError(x) }
}

impl From<AssertFailed> for CertificateValidationError
{
	fn from(x: AssertFailed) -> CertificateValidationError
	{
		CertificateValidationError(_CertificateValidationError::AssertionFailed(x))
	}
}

impl Display for CertificateValidationError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::_CertificateValidationError::*;
		match &self.0 {
			&AssertionFailed(ref err) => write!(fmt, "Assertion failed: {err}"),
			&PathLenViolation => fmt.write_str("Path length constraints violated"),
			&OcspStapleRequred => fmt.write_str("Certificate requires OCSP stapling"),
			&RequireUnknownFeatures => fmt.write_str("Certificate requires unknown features"),
			&UnmetFeatures(flags) => write!(fmt, "Certificate has unknown unmet feature flags {flags:x}"),
			&CaCertParseError(idx, ref err) => write!(fmt, "Error parsing CA#{idx}: {err}"),
			&DidNotIssueEeCert(idx) => write!(fmt, "CA#{idx} did not issue the EE cert"),
			&CaKillisted(idx) => write!(fmt, "CA#{idx} is explicitly killisted"),
			&CaCantHaveEeKeys(idx) => write!(fmt, "CA#{idx} shares public key with EE cert"),
			&CaSignatureMismatch(idx) => write!(fmt, "CA#{idx} signature algorithms mismatch"),
			&CaNotYetValid(idx, ts) => write!(fmt, "CA#{idx} not yet valid (begins at {ts})",
				ts=DecodeTsForUser(ts, true)),
			&CaExpired(idx, ts) => write!(fmt, "CA#{idx} expired (expired at {ts})",
				ts=DecodeTsForUser(ts, true)),
			&CaIsNotCa(idx) => write!(fmt, "CA#{idx} is not a CA"),
			&CaNotValidForTls(idx) => write!(fmt, "CA#{idx} can not issue TLS server certs"),
			&NameConstraintViolation(idx, ref name) =>
				write!(fmt, "CA#{idx} name constraints don't allow '{name}'"),
			&NameConstraintViolationIp(idx, ref addr) =>
				write!(fmt, "CA#{idx} name constraints don't allow {addr}"),
			&CaBadSignatureAlgorithm(idx) => write!(fmt, "CA#{idx} uses unacceptable signature algorithm"),
			&EeCertParseError(ref err) => write!(fmt, "Error parsing EE certificate: {err}"),
			&EeKillisted => fmt.write_str("EE certificate is explicitly killisted"),
			&EeSignatureMismatch => fmt.write_str("EE Certificate signature algorithms mismatch"),
			&EeNotValidForTls => fmt.write_str("EE Certificate is not valid for TLS server certificate"),
			&EeCertificateCa => fmt.write_str("EE certificate can not be CA certificate"),
			&EeNotValidForName(ref name, ref names) =>
				write!(fmt,  "EE Certificate not valid for name '{name}' (got: {names})"),
			&EeNotValidForIp(ref addr, ref addrs) =>
				write!(fmt, "EE Certificate not valid for IP {addr} (got: {addrs})"),
			&EeNotYetValid(ts) => write!(fmt, "EE certificate not yet valid (begins at {ts})",
				ts=DecodeTsForUser(ts, true)),
			&EeExpired(ts) => write!(fmt, "EE Certificate expired (expired at {ts})",
				ts=DecodeTsForUser(ts, true)),
			&EeValidityTooLong(valid, limit) =>
				write!(fmt, "EE Certificate validity too long ({valid} > {limit})"),
			&SelfSigned => fmt.write_str("Self-signed EE certificates are not allowed"),
			&EeBadSignatureAlgorithm =>
				fmt.write_str("EE Certificate uses unacceptable signature algorithm"),
			&NoIssuerCert => fmt.write_str("Issuer certificate missing"),
			&NoCertifcatesMatchPins => fmt.write_str("No certificates matching given pins found"),
			&CaUnknown => fmt.write_str("Certificate issued by unknown CA"),
			&SignatureFailure(idx, ref err) =>
				write!(fmt, "Signature on certifcate #{idx} does not verify: {err}"),
			&SignatureUnknown(idx) => write!(fmt, "Signature on certifcate #{idx} missing"),
			&PublicKeyMismatch => fmt.write_str("Certificate public key does not match keypair"),
			&ChainBadSignatureAlgorithm =>
				fmt.write_str("Certificate chain unvalidatable due to bad signature algorithm"),
			&ChainRevoked => fmt.write_str("Certificate chain unvalidatable due to revoked certificate"),
			&UndisclosedCa(idx) => write!(fmt, "CA#{idx} is unconstrained but not disclosed"),
			&AcmeValidationCertificate => fmt.write_str("Certificate is ACME validation token"),
			&BadIpRange(idx) => write!(fmt, "Bad IP range in certifcate #{idx}"),
			&RootPublicKeyMismatch => fmt.write_str("Root public key mismatch"),
			&SelfsignedCertificateNotLast => fmt.write_str("Self-signed certificate not last"),
			&BrokenChain(idx) => write!(fmt, "CA#{idx} did not issue its subordinate"),
			&TimeNotNested(idx) =>
				write!(fmt, "CA#{idx} not valid for entiere duration of its subordinate"),
			&EeCertIssuerExpired => fmt.write_str("EE certificate issuer expired"),
			&CaCertIssuerExpired(idx) => write!(fmt, "CA#{idx} certificate issuer expired"),
		}
	}
}

//Is signature algorithm allowed?
fn allowed_signature_algorithm<'a>(algo: Result<SignatureX5092<'a>, (&[u8], &[u8])>,
	policy: SignatureAlgorithmEnabled2) -> bool
{
	match algo { Ok(sig) => policy.allows_x5092(sig), Err(_) => false }
}

fn order_names(b1: &[u8], r1: &Range<usize>, b2: &[u8], r2: &Range<usize>) -> Ordering
{
	match (take_range(b1, r1), take_range(b2, r2)) {
		(Some(x1), Some(x2)) => x1.cmp(x2),
		(None, Some(_)) => Ordering::Less,
		(Some(_), None) => Ordering::Greater,
		(None, None) => Ordering::Equal,
	}
}


///A Trust Anchor with order mapping.
///
///This structure wraps a [`TrustAnchor`](struct.TrustAnchor.html), provoding it with ordering according to the
///subject name. This allows Trust Anchors to be placed into `BTreeMap` for fast lookup by the subject name.
///
///The structure is created by the [`new()`](#method.new) method.
///
///The original trust anchor can be borrowed by using the [`borrow_inner()`](#method.borrow_inner) method.
///
///The implementation `Borrow<[u8]>` borrows the raw X.509 Name of the trust anchor, and it implements equality
///and ordering like the underlying octet slice type. This is what allows this structure to be used as a key
///in `BTreeMap`.
#[derive(Clone,Debug)]
pub struct TrustAnchorOrdered(TrustAnchor);

impl Borrow<[u8]> for TrustAnchorOrdered
{
	fn borrow(&self) -> &[u8]
	{
		static BLANK: [u8;0] = [];
		take_range(&self.0.backing, &self.0.subject).unwrap_or(&BLANK[..])
	}
}

impl Eq for TrustAnchorOrdered
{
}

impl PartialEq for TrustAnchorOrdered
{
	fn eq(&self, other: &TrustAnchorOrdered) -> bool { self.cmp(other) == Ordering::Equal }
}

impl Ord for TrustAnchorOrdered
{
	fn cmp(&self, other: &TrustAnchorOrdered) -> Ordering
	{
		order_names(&self.0.backing, &self.0.subject, &other.0.backing, &other.0.subject)
	}
}

impl PartialOrd for TrustAnchorOrdered
{
	fn partial_cmp(&self, other: &TrustAnchorOrdered) -> Option<Ordering> { Some(self.cmp(other)) }
}

impl TrustAnchorOrdered
{
	///This method wraps a trust anchor `x` into ordering wrapper.
	///
	///Note that the original anchor is consumed in the process.
	pub fn new(x: TrustAnchor) -> TrustAnchorOrdered { TrustAnchorOrdered(x) }
	///This method borrows the inner trust anchor from this ordering wrapper.
	///
	///This is useful for accessing the key of the trust anchor, or its constraints.
	pub fn borrow_inner(&self) -> &TrustAnchor { &self.0 }
}


///An X.509 trust anchor
///
///This structure is constructed by:
///
/// * [`from_name_spki()`](#method.from_name_spki): Construct a trust anchor from X.509 Name and
///X.509 SubjectPublicKeyInfo.
/// * [`from_name_spki2()`](#method.from_name_spki2): As above, but supports generic constraints.
/// * [`from_certificate2()`](#method.from_certificate2): Construct a trust anchor from X.509 certificate. Any
///name constraints are copied into the trust achor.
///
///The public key of the trust anchor can be obtained by the [`get_spki()`](#method.get_spki) method. There is
///currently no way to obtain the constraints the trust anchor has.
#[derive(Clone)]
pub struct TrustAnchor
{
	backing: Arc<Vec<u8>>,
	subject: Range<usize>,
	spki: Range<usize>,
	allowed_names: Range<usize>,
	disallowed_names: Range<usize>,
	allowed_ips: Range<usize>,
	disallowed_ips: Range<usize>,
}

struct PrintHexString2([u8;32]);
impl Display for PrintHexString2
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		Display::fmt(&Hexdump(&self.0), f)
	}
}

struct PrintEscapeString<'a>(&'a [u8]);
impl<'a> Display for PrintEscapeString<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		f.write_str("\"")?;
		for i in self.0.iter().cloned() {
			if i < 32 || i > 126 || i == 92 {
				write!(f, "\\{i:02x}")?;
			} else {
				write!(f, "{c}", c=i as char)?;
			}
		}
		f.write_str("\"")
	}
}

fn print_or_null<D:Display>(d: Option<D>, f: &mut Formatter) -> Result<(), FmtError>
{
	if let Some(y) = d { Display::fmt(&y, f) } else { Display::fmt("(null)", f) }
}

impl Debug for TrustAnchor
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		f.write_str("TrustAnchor { subject: ")?;
		print_or_null(take_range(&self.backing, &self.subject).map(PrintEscapeString), f)?;
		f.write_str(", spki: ")?;
		print_or_null(take_range(&self.backing, &self.spki).map(|x|PrintHexString2(sha256(x))), f)?;
		f.write_str(", allowed_names: ")?;
		print_or_null(take_range(&self.backing, &self.allowed_names).map(PrintEscapeString), f)?;
		f.write_str(", disallowed_names: ")?;
		print_or_null(take_range(&self.backing, &self.disallowed_names).map(PrintEscapeString), f)?;
		f.write_str(", allowed_ips: ")?;
		print_or_null(take_range(&self.backing, &self.allowed_ips).map(Hexdump), f)?;
		f.write_str(", disallowed_ips: ")?;
		print_or_null(take_range(&self.backing, &self.disallowed_ips).map(Hexdump), f)?;
		f.write_str(" }")
	}
}

#[derive(Clone)]
struct ListAdapter<'a,'b:'a>(&'a [&'b str], usize);

impl<'a,'b:'a> Iterator for ListAdapter<'a, 'b>
{
	type Item=&'b [u8];
	fn next(&mut self) -> Option<&'b [u8]> {
		let entry = self.0.get(self.1);
		//This never overflows because self.1 < self.0.len() and thus self.1 + 1 <= self.0.len(), which
		//is in-range.
		if entry.is_some() { self.1 += 1 }
		entry.map(|e|e.as_bytes())
	}
}

///A name constraint
///
///The name constraint might be either positive or negative depending on the context. This structure is intended to
///be manually created.
#[non_exhaustive]
pub enum NameConstraint<'a>
{
	///Constraint on DNS name (postfix of the constrained tree).
	Dns(&'a [u8]),
	///Constraint on IP address.
	Ip(IpAddressBlock),
}

#[derive(Clone)]
struct ConstraintNames<'a,'b:'a>(&'a [NameConstraint<'b>], usize);

impl<'a,'b:'a> Iterator for ConstraintNames<'a, 'b>
{
	type Item=&'b [u8];
	fn next(&mut self) -> Option<&'b [u8]> {
		while let Some(entry) = self.0.get(self.1) {
			//This never overflows because self.1 < self.0.len() and thus self.1 + 1 <= self.0.len(),
			//which is in-range.
			self.1 += 1;
			if let &NameConstraint::Dns(y) = entry { return Some(y); }
		}
		None
	}
}

#[derive(Clone)]
struct ConstraintIps<'a,'b:'a>(&'a [NameConstraint<'b>], usize);

impl<'a,'b:'a> Iterator for ConstraintIps<'a, 'b>
{
	type Item=IpAddressBlock;
	fn next(&mut self) -> Option<IpAddressBlock> {
		while let Some(entry) = self.0.get(self.1) {
			//This never overflows because self.1 < self.0.len() and thus self.1 + 1 <= self.0.len(),
			//which is in-range.
			self.1 += 1;
			if let &NameConstraint::Ip(y) = entry { return Some(y); }
		}
		None
	}
}


#[derive(Clone)]
struct EmptyIterator<T:Sized>(PhantomData<T>);

impl<T:Sized> Iterator for EmptyIterator<T>
{
	type Item=T;
	fn next(&mut self) -> Option<T> { None }
}

impl TrustAnchor
{
	///Create a new turst anchor from the specified subject `subject`, SPKI `spki` and name constriaints
	///`allowed_names` and `disallow_names`.
	///
	///The subject name includes the leading SEQUENCE header and the SPKI is in X.509 SubjectPublicKeyInfo
	///format.
	///
	///If `allowed_names` is empty, then all names not in `disallow_names` are allowed. Otherwise only names
	///that are in `allow_names` but not `disallow_names` are allowed. That is, `disallow_names` takes
	///percedence over `allow_names`.
	///
	///If one needs to include constraints on IP addresses, use the [`from_name_spki2()`] method.
	///
	///On success, returns `Ok(ta)`, where `ta` is the trust anchor. If the data is too mangled, returns
	///`Err(msg)`, where `msg` is a error message. However, this should never happen.
	///
	///[`from_name_spki2()`]: #method.from_name_spki2
	pub fn from_name_spki(subject: &[u8], spki: &[u8], allow_names: &[&str], disallow_names: &[&str]) ->
		Result<TrustAnchor, String>
	{
		ttry!(TrustAnchor::from_data(subject, spki, ListAdapter(allow_names, 0),
			ListAdapter(disallow_names, 0), EmptyIterator::<IpAddressBlock>(PhantomData),
			EmptyIterator::<IpAddressBlock>(PhantomData)).set_err("Internal error"))
	}
	///Same as [`from_name_spki()`], bust instead of taking allowed and disallowed names, takes generic
	///positive and negative constraints `allow` and `disallow`.
	///
	///The matching logic versus constraints is the same as in [`from_name_spki()`], except with names replaced
	///by more generic constraints.
	///
	///The constraints as instances of [`NameConstraint`](enum.NameConstraint.html) instances instead of
	///DNS hostname strings.
	///
	///[`from_name_spki()`]: #method.from_name_spki
	pub fn from_name_spki2(subject: &[u8], spki: &[u8], allow: &[NameConstraint],
		disallow: &[NameConstraint]) -> Result<TrustAnchor, String>
	{
		ttry!(TrustAnchor::from_data(subject, spki, ConstraintNames(allow, 0), ConstraintNames(disallow, 0),
			ConstraintIps(allow, 0), ConstraintIps(disallow, 0)).set_err("Internal error"))
	}
	///Create a new turst anchor from the specified parsed certificate `ta`.
	///
	///On success, returns `Ok(ta2)`, where `ta2` is the trust anchor. If the data passed in is too malformed to
	///even construct a trust anchor, returns `Err(msg)`, where `msg` is a error message. However, this should
	///never happen.
	pub fn from_certificate2<'a>(ta: &ParsedCertificate2<'a>) -> Result<TrustAnchor, String>
	{
		ttry!(Self::from_data(ta.subject.as_raw_subject(), ta.pubkey, ta.names_allow.iter(),
			ta.names_disallow.iter(), ta.names_allow.iter_addrs(), ta.names_disallow.iter_addrs()).
			set_err("Internal error"))
	}
	fn from_data<'a>(subject: &[u8], spki: &[u8], allow_names: impl Iterator<Item=&'a [u8]>+Clone,
		disallow_names: impl Iterator<Item=&'a [u8]>+Clone,
		allow_ips: impl Iterator<Item=IpAddressBlock>+Clone,
		disallow_ips: impl Iterator<Item=IpAddressBlock>+Clone) -> Result<TrustAnchor, ()>
	{
		let mut need_size = 0;
		need_size += subject.len();
		need_size += spki.len();
		for i in allow_names.clone() {
			need_size = need_size.saturating_add(reserve_name_constraint_entry(i));
		}
		for i in disallow_names.clone() {
			need_size = need_size.saturating_add(reserve_name_constraint_entry(i));
		}
		for i in allow_ips.clone() {
			need_size = need_size.saturating_add(reserve_ip_constraint_entry(i));
		}
		for i in disallow_ips.clone() {
			need_size = need_size.saturating_add(reserve_ip_constraint_entry(i));
		}
		let mut backing = Vec::with_capacity(need_size);

		let subject_range = append_slice(&mut backing, |backing|backing.write_slice(subject))?;
		let spki_range = append_slice(&mut backing, |backing|backing.write_slice(spki))?;
		let allow_range = append_slice(&mut backing, |backing|{
			for i in allow_names.clone() { write_name_constraint_entry(backing, i)?; }
			Ok(())
		})?;
		let disallow_range = append_slice(&mut backing, |backing|{
			for i in disallow_names.clone() { write_name_constraint_entry(backing, i)?; }
			Ok(())
		})?;
		let allow_ip_range = append_slice(&mut backing, |backing|{
			for i in allow_ips.clone() { write_ip_constraint_entry(backing, i)?; }
			Ok(())
		})?;
		let disallow_ip_range = append_slice(&mut backing, |backing|{
			for i in disallow_ips.clone() { write_ip_constraint_entry(backing, i)?; }
			Ok(())
		})?;

		Ok(TrustAnchor {
			backing: Arc::new(backing),
			subject: subject_range,
			spki: spki_range,
			allowed_names: allow_range,
			disallowed_names: disallow_range,
			allowed_ips: allow_ip_range,
			disallowed_ips: disallow_ip_range,
		})
	}
	///Return the SubjectPublicKeyInfo for the Trust Anchor.
	///
	///On success, returns `Some(spki)`, where `spki` is the SubjectPublicKeyInfo. If the trust anchor is too
	///malformed to have a public key, returns `None`.
	pub fn get_spki<'a>(&'a self) -> Option<&'a [u8]>
	{
		take_range(&self.backing, &self.spki)
	}
	fn get_allowed_names<'a>(&'a self) -> Option<&'a [u8]>
	{
		take_range(&self.backing, &self.allowed_names)
	}
	fn get_disallowed_names<'a>(&'a self) -> Option<&'a [u8]>
	{
		take_range(&self.backing, &self.disallowed_names)
	}
	fn get_allowed_ips<'a>(&'a self) -> Option<&'a [u8]>
	{
		take_range(&self.backing, &self.allowed_ips)
	}
	fn get_disallowed_ips<'a>(&'a self) -> Option<&'a [u8]>
	{
		take_range(&self.backing, &self.disallowed_ips)
	}
}

fn append_slice(backing: &mut Vec<u8>, func: impl FnOnce(&mut Vec<u8>) -> Result<(),()>) -> Result<Range<usize>, ()>
{
	let osize = backing.len();
	func(backing)?;
	let nsize = backing.len();
	Ok(osize..nsize)
}

fn reserve_name_constraint_entry(i: &[u8]) -> usize { i.len().saturating_add(2) }

fn reserve_ip_constraint_entry(i: IpAddressBlock) -> usize
{
	if let Some(_) = i.as_ipv6() { 33 } else if let Some(_) = i.as_ipv4() { 9 } else { 0 }
}

fn write_name_constraint_entry<S:Sink>(backing: &mut S, i: &[u8]) -> Result<(), ()>
{
	fail_if!(i.len() > 65535, ());
	backing.write_u16(i.len() as u16)?;
	backing.write_slice(i)?;
	Ok(())
}

fn write_ip_constraint_entry<S:Sink>(backing: &mut S, i: IpAddressBlock) -> Result<(), ()>
{
	if let Some((a, b)) = i.as_ipv6() {
		backing.write_u8(1)?;
		for j in a.iter().cloned() { backing.write_u16(j)?; }
		for j in b.iter().cloned() { backing.write_u16(j)?; }
	} else if let Some((a, b)) = i.as_ipv4() {
		backing.write_u8(0)?;
		for j in a.iter().cloned() { backing.write_u8(j)?; }
		for j in b.iter().cloned() { backing.write_u8(j)?; }
	}
	//Otherwise skip this.
	Ok(())
}

fn take_range<'a,T>(backing: &'a [T], range: &Range<usize>) -> Option<&'a [T]>
{
	backing.get(range.clone())
}

//Check if name matches given postfix.
fn name_matches_postfix(name: &[u8], postfix: &[u8]) -> bool
{
	//If postfix begins with a '.', strip that (. is always 1 byte).
	let postfix = if let Some((b'.', postfix)) = split_head_tail(postfix) { postfix } else { postfix };
	//Split the name into prefex and postfix.
	//Now the prefix must be empty or end with '.' and postfixes have to match.
	let prefixlen = name.len().wrapping_sub(postfix.len());
	if prefixlen > name.len() { return false; }
	let (a, b) = name.split_at(prefixlen);
	(a.len() == 0 || a.ends_with(b".")) && matches_ascii_case_insensitive(postfix, b)
}

//Check name constraints. If allow exists, the name must be on that. And the name must not be on disallow list.
fn check_name_constraints<'a,'b>(name: &[u8], mut allow: impl Iterator<Item=&'a [u8]>+Clone,
	mut disallow: impl Iterator<Item=&'b [u8]>+Clone) -> bool
{
	if allow.clone().next().is_some() { if !allow.any(|x|name_matches_postfix(name, x)) { return false; }}
	if disallow.any(|x|name_matches_postfix(name, x)) { return false; }
	true
}

//Check IP name constraints. If allow exists, the address must be on that. And the address must not be on disallow
//list.
fn check_name_constraints_ip(address: IpAddressBlock, mut allow: impl Iterator<Item=IpAddressBlock>+Clone,
	mut disallow: impl Iterator<Item=IpAddressBlock>+Clone) -> bool
{
	if allow.clone().next().is_some() { if !allow.any(|x|address.is_subset_of(&x)) { return false; }}
	if disallow.any(|x|address.is_subset_of(&x)) { return false; }
	true
}

fn is_expired_ca(ca: CertificateIssuer, ts: Timestamp) -> bool
{
	_is_expired_ca(ca.as_raw_issuer(), ts.unix_time())
}

//Do checks on EE certificate.
//
//The following checks are performed:
//
// - The two signature algorithms in certificate match up.
// - The certificate is valid for TLS client/server authentication (not AnyEku!)
// - The certificate is not CA certificate.
// - The certificate is in its validity period (however, not yet valid certs are allowed if allow_not_yet_valid is
//set).
// - The certificate is not self-signed.
// - The certificate signature algorithm is trusted (if strict_algo=true).
// - icert_count > 0 (if strict_algo=true).
//
//It also does the follows:
//
// - Sets bits corresponding to TLS features required in feature_flags_present.
// - Adds the issuer to issuers_seen.
//
//It most notably does NOT check:
// - That the EE key is not kill-listed.
// - That the SAN in certificate matches the reference name.
//
fn ee_certificate_checks<'b>(eecert: &ParsedCertificate2<'b>, time_now: Timestamp, strict_algo: bool,
	icert_count: usize, feature_flags_present: &mut u32, allow_not_yet_valid: bool, is_client: bool,
	policy: SignatureAlgorithmEnabled2, test_ignore_build: bool) -> Result<(), CertificateValidationError>
{
	use self::_CertificateValidationError::*;

	let now = time_now;
	//The signature algorithms of EE cert must match up.
	let oid = match eecert.signature { Ok(sig) => sig.oid(), Err((oid, _)) => oid };
	fail_if!(eecert.sig_algo1 != oid, EeSignatureMismatch);
	//The certificate needs to have appropriate EKU.
	fail_if!(!if is_client { eecert.use_tls_client } else { eecert.use_tls_serv }, EeNotValidForTls);
	//The certificate must not have expired issuer.
	fail_if!(is_expired_ca(eecert.issuer, eecert.not_after), EeCertIssuerExpired);
	//The certificate must not be a CA.
	fail_if!(eecert.is_ca, EeCertificateCa);
	//The certificate must not be ACME certificate.
	fail_if!(eecert.acme_tls_alpn.is_some(), AcmeValidationCertificate);
	//Sanity check on expiry.
	fail_if!(eecert.not_after.get_day() < get_build_date() && !test_ignore_build,
		EeExpired(eecert.not_after));
	//Public certificate validity limits.
	if is_public_ca(eecert.issuer.as_raw_issuer()) {
		const LIMIT: u64 = 398;
		//Round up the division result. Note that the not_after second is included.
		let validity = eecert.not_before.delta(eecert.not_after) / 86400 + 1;
		fail_if!(validity > LIMIT, EeValidityTooLong(validity, LIMIT));
	}
	//The EE cert must be in validity window.
	fail_if!(!allow_not_yet_valid && now < eecert.not_before, EeNotYetValid(eecert.not_before));
	fail_if!(now > eecert.not_after, EeExpired(eecert.not_after));
	//Self-signeds are NOT allowed. Note that if EE SPKI is trusted, the entiere certificate check will be
	//skipped. Also, if RPK is used on server, this sanity check will be skipped.
	//This holds even for client certificates. Note that if we fail validation of client certificate, that
	//just withholds the name, it does not distrupt the connection.
	fail_if!(eecert.issuer.same_as(eecert.subject), SelfSigned);
	//If the EE cert is not self-signed, it must have acceptable signature algorithm (if checked) and its
	//issuer certificate must be in chain.
	fail_if!(strict_algo && !allowed_signature_algorithm(eecert.signature, policy), EeBadSignatureAlgorithm);
	fail_if!(icert_count == 0 && strict_algo, NoIssuerCert);
	*feature_flags_present |= eecert.req_flags;
	Ok(())
}

//Do checks on CA certificate.
//
//The following checks are done:
// - The two signature algorithms in certificate match up.
// - The certificate is valid for TLS client/server authentication or AnyEku.
// - The certificate is a CA certificate.
// - The certificate is in its validity period (however, not yet valid certs are allowed if allow_not_yet_valid is
//set).
// - The certificate signature algorithm is trusted if not self-signed (if strict_algo=true).
// - The first CA entry (index=0) is the issuer of the EE certificate.
// - All EE certificate SANs satisfy the name constraints.
// - Some earlier certificate has been signed by this certificate.
// - The issuer has not been seen as subject already (prevent looping "chains").
//
//It also does the follows:
//
// - Sets bits corresponding to TLS features required in feature_flags_present.
// - Adds the issuer to issuers_seen.
//
//It most notably does NOT check:
// - That the CA key is not kill-listed.
//
fn ca_certificate_checks<'b>(icert: ParsedCertificate2<'b>, eecert: Option<&ParsedCertificate2<'b>>, index: usize,
	time_now: Timestamp, strict_algo: bool, feature_flags_present: &mut u32,
	require_disclosed_ca: bool, icerts: &mut Vec<ParsedCertificate2<'b>>, allow_not_yet_valid: bool,
	is_client: bool, policy: SignatureAlgorithmEnabled2, test_ignore_build: bool) ->
	Result<(), CertificateValidationError>
{
	use self::_CertificateValidationError::*;

	let now = time_now;
	//The feature flags accumulate through the chain.
	*feature_flags_present |= icert.req_flags;
	//If requiring disclosed CA, the certificate must be public or constrained.
	fail_if!(require_disclosed_ca && !(icert.constrained ||
		is_public_intermediate(icert.entiere)), UndisclosedCa(index));
	//The signature algorithms must be consistent.
	let oid = match icert.signature { Ok(sig) => sig.oid(), Err((oid, _)) => oid };
	fail_if!(icert.sig_algo1 != oid, CaSignatureMismatch(index));
	//Sanity check on expiry.
	fail_if!(icert.not_after.get_day() < get_build_date() && !test_ignore_build,
		CaExpired(index, icert.not_after));
	//The certificate must be in validity window.
	fail_if!(!allow_not_yet_valid && now < icert.not_before, CaNotYetValid(index, icert.not_before));
	fail_if!(now > icert.not_after, CaExpired(index, icert.not_after));
	//The certificate must be a CA.
	fail_if!(!icert.is_ca, CaIsNotCa(index));
	//The certificate must not be ACME certificate.
	fail_if!(icert.acme_tls_alpn.is_some(), AcmeValidationCertificate);
	//The certificate needs to have appropriate EKUs.
	fail_if!(!if is_client { icert.use_tls_client } else { icert.use_tls_serv } && !icert.use_any,
		CaNotValidForTls(index));
	//The address masks must be continuous.
	for ent in icert.names_allow.iter_addrs() { fail_if!(!ent.is_continuous(), BadIpRange(index)); }
	for ent in icert.names_disallow.iter_addrs() { fail_if!(!ent.is_continuous(), BadIpRange(index)); }
	if let Some(eecert) = eecert {
		//CA#0 must issue EE.
		fail_if!(index == 0 && !eecert.issuer.same_as(icert.subject), DidNotIssueEeCert(index));
		//CA must not share keys with EE cert.
		fail_if!(icert.pubkey == eecert.pubkey, CaCantHaveEeKeys(index));
		//Name constraints must allow all names in EE cert.
		for j in eecert.dnsnames.iter() {
			fail_if!(!check_name_constraints(j, icert.names_allow.iter(), icert.names_disallow.iter()),
				NameConstraintViolation(index, EscapeByteString(j).to_string()));
		}
		for j in eecert.dnsnames.iter_addrs() {
			fail_if!(!check_name_constraints_ip(j, icert.names_allow.iter_addrs(),
				icert.names_disallow.iter_addrs()), NameConstraintViolationIp(index, j));
		}
	}
	//If requested, check that signature algorithm is allowed. Note that self-signed certificates are always
	//ignored for this check.
	fail_if!(!icert.issuer.same_as(icert.subject) && strict_algo &&
		!allowed_signature_algorithm(icert.signature, policy), CaBadSignatureAlgorithm(index));
	//Insert certificate to list of certificates.
	icerts.push(icert);
	//OK.
	Ok(())
}

//Hashes a key and checks if it is on specified killist.
fn key_on_killist(key: &[u8], killist: &BTreeSet<[u8; 32]>) -> bool
{
	killist.contains(&sha256(key))
}

//Check a set of names against name constraints. Return if all are allowed.
fn check_name_constraints2<'a,'b,'c>(mut names: impl Iterator<Item=&'a [u8]>+Clone,
	allow: impl Iterator<Item=&'b [u8]>+Clone, disallow: impl Iterator<Item=&'c [u8]>+Clone) -> bool
{
	names.all(|i|check_name_constraints(i, allow.clone(), disallow.clone()))
}

//Check a set of addresses against name constraints. Return if all are allowed.
fn check_name_constraints_ip2(mut addresses: impl Iterator<Item=IpAddressBlock>+Clone,
	allow: impl Iterator<Item=IpAddressBlock>+Clone, disallow: impl Iterator<Item=IpAddressBlock>+Clone) -> bool
{
	addresses.all(|i|check_name_constraints_ip(i, allow.clone(), disallow.clone()))
}

const STORE_SLICE: VectorLength = VectorLength::variable(0, VectorLength::MAX_16BIT);

#[derive(Clone)]
struct DecayVectorToSlice<'a>(Source<'a>);

impl<'a> Iterator for DecayVectorToSlice<'a>
{
	type Item=&'a [u8];
	fn next(&mut self) -> Option<&'a [u8]> { self.0.slice(STORE_SLICE).ok() }
}

#[derive(Clone)]
struct DecayVectorToSlice2<'a>(Source<'a>);

impl<'a> Iterator for DecayVectorToSlice2<'a>
{
	type Item=IpAddressBlock;
	fn next(&mut self) -> Option<IpAddressBlock> {
		let x = self.0.u8().ok()?;
		Some(if x & 1 == 0 {
			let mut buf1 = [0;4];
			let mut buf2 = [0;4];
			for i in buf1.iter_mut() { *i = self.0.u8().unwrap_or(0); }
			for i in buf2.iter_mut() { *i = self.0.u8().unwrap_or(0); }
			IpAddressBlock::from_ipv4_mask(buf1, buf2)
		} else {
			let mut buf1 = [0;8];
			let mut buf2 = [0;8];
			for i in buf1.iter_mut() { *i = self.0.u16().unwrap_or(0); }
			for i in buf2.iter_mut() { *i = self.0.u16().unwrap_or(0); }
			IpAddressBlock::from_ipv6_mask(buf1, buf2)
		})
	}
}


//Check if specified name is a valid trust anchor.
fn check_trust_anchor<'a,'b>(dnsnames: impl Iterator<Item=&'a [u8]>+Clone,
	addresses: impl Iterator<Item=IpAddressBlock>+Clone, trust_anchor: &'b TrustAnchor,
	killist: &BTreeSet<[u8; 32]>) -> Option<&'b [u8]>
{
	let spki = trust_anchor.get_spki()?;
	let allowed_names = DecayVectorToSlice(Source::new(trust_anchor.get_allowed_names()?));
	let disallowed_names = DecayVectorToSlice(Source::new(trust_anchor.get_disallowed_names()?));
	let allowed_ips = DecayVectorToSlice2(Source::new(trust_anchor.get_allowed_ips()?));
	let disallowed_ips = DecayVectorToSlice2(Source::new(trust_anchor.get_disallowed_ips()?));
	//Key must not be on TA blacklist.
	fail_if_none!(is_ta_blacklisted(spki));
	//Key must not be killlisted, name constriaints must allow dnsnames and ip addresses.
	if !key_on_killist(spki, killist) &&
		check_name_constraints2(dnsnames, allowed_names, disallowed_names) &&
		check_name_constraints_ip2(addresses, allowed_ips, disallowed_ips) {
		Some(spki)
	} else {
		None
	}
}

///Parameters for `full_certificate_validation`.
pub struct FullCertificateValidation<'a,'b>
{
	///If `true`, enable strict algorithm checking. When strict algorithm checking is enabled, every
	///non-self-signed certificate in the chain, **including** the otherwise unused ones must be signed using a
	///known algorithm. If `false`, non-self-signed certificates signed using unknown algorithms are just
	///ignored.
	pub strict_algo: bool,
	///The feature flags (`CFLAG_*`) the connection satisfies. This must be superset of the feature flags the
	///certificate requires for validation to be successful.
	pub feature_flags: u32,
	///The public key kill list. If the chain contains public keys that match the SHA-256 hashes in this list,
	///the validation fails. In case of trust anchors that are not sent by the server, trust anchors with
	///matching keys are just ignored.
	pub killist: &'b BTreeSet<[u8; 32]>,
	///If the certificate needs an OCSP staple, this is set to `true`. Otherwise this value is left alone,
	///so any prior value is OR'ed with the certificate itself needing OCSP stapling.
	pub need_ocsp_flag: &'b mut bool,
	///If the certificate needs an Signed Certificate Timestamp staple, this is set to `true`. Otherwise this
	///value is left alone, so any prior value is OR'ed with the certificate itself needing
	///Signed Certificate Timestamp stapling.
	pub need_sct_flag: &'b mut bool,
	///If `true` the certificate has to have tls_client Extended Key Usage. If `false`, the certificate has
	///to have tls_server Extended Key Usage.
	pub is_client: bool,
	///The signature algorithms allowed for the certficate chain.
	pub policy: SignatureAlgorithmEnabled2,
	///Require that all CAs are publically disclosed or technically constrained.
	pub require_disclosed_ca: bool,
	///The issuer chain.
	pub issuers: &'b [&'a [u8]],
	///The reference name.
	pub reference: Option<ReferenceName<'b>>,
	///Current time.
	pub time_now: &'b dyn TimeInterface,
	///Trust anchors.
	pub trust_anchors: &'b BTreeSet<TrustAnchorOrdered>,
	///Host-specific pins.
	pub pins: &'b [HostSpecificPin],
	///Extended parameters for sanity check.
	pub extended: SanityCheckCertificateChainExtended<'b>
}

///Validate certificate.
#[allow(deprecated)]
pub fn full_certificate_validation<'a,'b>(cert: &ParsedCertificate2<'a>, p: FullCertificateValidation<'a,'b>) ->
	Result<(), CertificateValidationError>
{
	let time_now = p.time_now.get_time();		//Freeze time.
	let psanity = SanityCheckCertificateChain2 {
		strict_algo: p.strict_algo,
		feature_flags: p.feature_flags,
		killist: p.killist,
		need_ocsp_flag: p.need_ocsp_flag,
		need_sct_flag: p.need_sct_flag,
		is_client: p.is_client,
		policy: p.policy,
		require_disclosed_ca: p.require_disclosed_ca,
		extended: p.extended,
	};
	__sanity_check_certificate_chain(cert, p.issuers, p.reference, time_now, psanity,
		validate_certificate_after_sanity_check2, (p.trust_anchors, p.pins, p.policy))
}

///Parameters for `validate_certificate_after_sanity_check`.
#[deprecated(since="2.4.0", note="Use FullCertificateValidation instead")]
pub struct ValidateCertificateAfterSanityCheck<'a,'b,'c>
{
	#[doc(hidden)]	//Deprecated.
	pub keys_map: BTreeMap<CertificateSubject<'b>, &'a [u8]>,
	#[doc(hidden)]	//Deprecated.
	pub seen: BTreeSet<CertificateIssuer<'b>>,
	///The public key kill list.
	pub killist: &'c BTreeSet<[u8; 32]>,
	///The time certificate is valid until.
	pub valid_until: Timestamp,
	///The features the certificate needs.
	pub need_features: u32,
	#[doc(hidden)]	//Deprecated.
	pub constraint_flags: u64,
	///Require disclosed CAs?
	pub require_disclosed_ca: bool,
	_dummy: (),
}

#[allow(deprecated)]
#[allow(missing_docs)] #[deprecated(since="2.4.0", note="Use full_certificate_validation() instead")]
pub fn validate_certificate_after_sanity_check2<'a,'b,'c>(eecert: &ParsedCertificate2<'b>,
	mut intermediates: &[ParsedCertificate2<'b>], p: ValidateCertificateAfterSanityCheck<'a,'b,'c>,
	trust: (&'a BTreeSet<TrustAnchorOrdered>, &[HostSpecificPin], SignatureAlgorithmEnabled2)) ->
	Result<(), CertificateValidationError>
{
	use crate::_CertificateValidationError::*;
	let (anchors, pins, policy) = trust;

	//One can assume by sanity check that each certificate is signed by the next. Find if the last
	//certificate is self-signed, and what its issuer is.
	let lcert = intermediates.last().unwrap_or(eecert);
	let ta_name = if !lcert.issuer.same_as(lcert.subject) { Some(lcert.issuer) } else { None };
	//If there is any pin, end-entity certificate has to match pin. CA pining is not supported.
	if pins.iter().any(|p|p.pin) {
		let mut pin_satisfied = false;
		for pin in pins.iter() {
			if pin.ca_flag == Some(true) { continue; }	//Not EE pin.
			let matched_data = if pin.spki { eecert.pubkey } else { eecert.entiere };
			if pin.data.matches2(matched_data, policy.hash_subpolicy()) {
				//This matches. If pin is set, pinned entry has been found.
				pin_satisfied |= pin.pin;
			}
		}
		fail_if!(!pin_satisfied, NoCertifcatesMatchPins);
	}

	//Scan for trust anchor. If no entry is pinned, set pinned to true, so any trust anchor is accepted.
	let mut trust_anchor = None;
	let mut unsafe_dane_ta = false;
	'outer: for (index, cert) in once(eecert).chain(intermediates.iter()).enumerate() {
		//Consider trust anchors from pins.
		for pin in pins.iter() {
			if index == 0 && pin.ca_flag == Some(true) { continue; }	//EE, need CA.
			if index != 0 && pin.ca_flag == Some(false) { continue; }	//CA, need EE.
			if is_ta_blacklisted(cert.pubkey) { continue; }		//Blacklisted trust anchor.
			let matched_data = if pin.spki { cert.pubkey } else { cert.entiere };
			if pin.data.matches2(matched_data, policy.hash_subpolicy()) && pin.ta {
				//If an SPKI pin is trust anchor, the pointed to certificate MUST be
				//self-signed and have validatable signature. Otherwise there is an attack.
				//This does not affect 3 1 X thingies, because those should just skip the entiere
				//chain verification.
				unsafe_dane_ta |= pin.spki;
				trust_anchor = Some(index);
				break 'outer;
			}
		}
		//Then try trust anchors. There is no need to check for constraints or killist because that was
		//already checked by chain sanity check.
		if let Some(ta) = anchors.get(cert.subject.as_raw_subject()) {
			//Key has to match. If it matches, this is trust anchor. However, blacklisted TAs do
			//not match. The rootstore is assumed to be correct, so no need to check any self-signature.
			fail_if!(ta.0.get_spki() != Some(cert.pubkey), RootPublicKeyMismatch);
			if is_ta_blacklisted(cert.pubkey) { continue; }
			trust_anchor = Some(index);
			break;
		}
	}
	//Extract the trust anchor key.
	let ta_key = match trust_anchor {
		Some(0) => {
			//The EE is a trust anchor! The only need for validation is if unsafe_dane_ta is set.
			//In that case, the self-signature of EE cert needs to be checked.
			intermediates = &[];
			if unsafe_dane_ta { eecert.pubkey } else { &[] }
		},
		Some(n) if unsafe_dane_ta => {
			//Take the key of index n-1 entry, but truncate issuer chain to n entries. This way the
			//self-signature on the last entry gets verified, like it needs to be.
			intermediates = &intermediates[..n];
			intermediates[n-1].pubkey
		},
		Some(n) => {
			//There is explicit trust anchor. Take key of index n-1 entry, and truncate the issuer
			//chain to n-1 entries. That way the trust anchor is just out of chain. By construction,
			//trust_anchor can be at most intermediates.len().
			let (head, tail) = intermediates.split_at(n-1);
			intermediates = head;
			tail[0].pubkey
		},
		None => ta_name.and_then(|taname|anchors.get(taname.as_raw_issuer())).and_then(|maybe_ta|{
			//If no TA was set and last certificate was not selfsigned, try to look up the TA.
			//If not found, there is no trusted CA. Here the killist checks and constraint checks
			//are needed.
			let dns_names = eecert.dnsnames.iter();
			let ip_addresses = eecert.dnsnames.iter_addrs();
			check_trust_anchor(dns_names, ip_addresses, &maybe_ta.0, &p.killist)
		}).ok_or(CaUnknown)?,
	};
	//Check that there are no revoked certificates in remaining chain, and all signature algorithms are
	//allowed. Note that due to truncation of chain above, the allowed_signature_algorithm is never appiled
	//to self-signed root certificate.
	for cert in once(eecert).chain(intermediates.iter()) {
		fail_if!(is_revoked(cert.issuer.as_raw_issuer(), cert.serial_number, cert.pubkey), ChainRevoked);
		fail_if!(!allowed_signature_algorithm(cert.signature, policy), ChainBadSignatureAlgorithm);
	}
	//Ok, eecert should be signed by intermediates[0] (or ta_key if there is no such entry). Do not bother
	//with known_signature stuff, because it is unlikely to be found. The certificate is assumed to be
	//constrained.
	eecert.signature.set_err(SignatureUnknown(0))?.verify(
		intermediates.get(0).map(|c|c.pubkey).unwrap_or(ta_key), eecert.tbs, policy,
		SignatureFlags2::CERTIFICATE).map_err(|x|SignatureFailure(0, x))?;
	//Now, intermediates[n] should be signed by intermediates[n+1] (or ta_key if there is no such entry).
	for (tindex, tcert) in intermediates.iter().enumerate() {
		let nindex = tindex + 1;
		let nkey = intermediates.get(nindex).map(|c|c.pubkey).unwrap_or(ta_key);
		//constraint_flags bit 0 is for EE, which is ignored here. Do not do known_signature check on
		//constrained certificates, because it is unlikely to succeed.
		let constrained = tcert.constrained;
		if !constrained && known_signature(tcert.issuer.as_raw_issuer(), nkey, tcert.entiere) {
			continue;
		}
		//If !constrained and require_disclosed_ca, that is failure, as above check should have caught
		//it.
		fail_if!(p.require_disclosed_ca && !constrained, UndisclosedCa(tindex));
		//Check signature.
		tcert.signature.set_err(SignatureUnknown(nindex))?.verify(nkey, tcert.tbs, policy,
			SignatureFlags2::CERTIFICATE).map_err(|x|SignatureFailure(nindex, x))?;
	}
	//All checks passed.
	Ok(())
}


//Check path length constraints.
fn check_pathlen_constraints<'b>(icerts: &[ParsedCertificate2<'b>]) -> Result<(), CertificateValidationError>
{
	use self::_CertificateValidationError::*;
	for certs in icerts.windows(2) {
		match (certs.get(1).map(|c|c.max_path_len), certs.get(0).map(|c|c.max_path_len)) {
			(Some(Some(n)), Some(Some(t))) if n > t => (),	//Increase OK.
			(Some(None), _) => (),				//OK if next is None.
			_ => fail!(PathLenViolation)			//All other cases NOT OK.
		}
	}
	Ok(())
}

///Type of reference name.
#[non_exhaustive]
pub enum ReferenceName<'a>
{
	///DNS name, given as argument.
	DnsName(&'a str),
	///IP address, given as argument.
	Ip(IpAddress),
}

fn extract_reference_names(eecert: &ParsedCertificate2) -> String
{
	let mut names = String::new();
	for i in eecert.dnsnames.iter() {
		if names.len() > 0 { names.push_str(", "); }
		//This can not fail.
		write!(names, "{i}", i=EscapeByteString(i)).ok();
	}
	for i in eecert.dnsnames.iter_addrs() {
		if names.len() > 0 { names.push_str(", "); }
		//This can not fail.
		write!(names, "{i}").ok();
	}
	names
}

///Parameters for certificate sanity check using [`sanity_check_certificate_chain`]
///
///[`sanity_check_certificate_chain`]: fn.sanity_check_certificate_chain.html
pub struct SanityCheckCertificateChain2<'c>
{
	///If `true`, enable strict algorithm checking. When strict algorithm checking is enabled, every
	///non-self-signed certificate in the chain, **including** the otherwise unused ones must be signed using a
	///known algorithm. If `false`, non-self-signed certificates signed using unknown algorithms are just
	///ignored.
	pub strict_algo: bool,
	///The feature flags (`CFLAG_*`) the connection satisfies. This must be superset of the feature flags the
	///certificate requires for validation to be successful.
	pub feature_flags: u32,
	///The public key kill list. If the chain contains public keys that match the SHA-256 hashes in this list,
	///the validation fails. In case of trust anchors that are not sent by the server, trust anchors with
	///matching keys are just ignored.
	pub killist: &'c BTreeSet<[u8; 32]>,
	///If the certificate needs an OCSP staple, this is set to `true`. Otherwise this value is left alone,
	///so any prior value is OR'ed with the certificate itself needing OCSP stapling.
	pub need_ocsp_flag: &'c mut bool,
	///If the certificate needs an Signed Certificate Timestamp staple, this is set to `true`. Otherwise this
	///value is left alone, so any prior value is OR'ed with the certificate itself needing
	///Signed Certificate Timestamp stapling.
	pub need_sct_flag: &'c mut bool,
	///If `true` the certificate has to have tls_client Extended Key Usage. If `false`, the certificate has
	///to have tls_server Extended Key Usage.
	pub is_client: bool,
	///The signature algorithms allowed for the certficate chain.
	pub policy: SignatureAlgorithmEnabled2,
	///Require that all CAs are publically disclosed or technically constrained.
	pub require_disclosed_ca: bool,
	///Extended parameters for sanity check.
	pub extended: SanityCheckCertificateChainExtended<'c>
}

///Extended parameters for certificate sanity check using  [`sanity_check_certificate_chain`]
///
///Currently there are no extended parameters, so there is nothing here.
///
///This structure can be constructed using the [`Default`](#impl-Default) trait.
///
///The supported methods are:
///
/// * Traits:
///   * `Default`
///
///[`sanity_check_certificate_chain`]: fn.sanity_check_certificate_chain.html
pub struct SanityCheckCertificateChainExtended<'c>
{
	pub(crate) test_ignore_build: bool,
	dummy: PhantomData<&'c u8>,
}

impl<'c> Default for SanityCheckCertificateChainExtended<'c>
{
	fn default() -> SanityCheckCertificateChainExtended<'c>
	{
		SanityCheckCertificateChainExtended {
			test_ignore_build: false,
			dummy: PhantomData,
		}
	}
}

fn check_reference_name_in_certificate(eecert: &ParsedCertificate2, reference: Option<ReferenceName>) ->
	Result<(), CertificateValidationError>
{
	use self::_CertificateValidationError::*;
	if let Some(reference) = reference {
		match &reference {
			&ReferenceName::DnsName(ref reference) => if !name_in_set2(eecert.dnsnames.iter(),
				reference.as_bytes()) {
				let names = extract_reference_names(&eecert);
				fail!(EeNotValidForName((*reference).to_owned(), names));
			},
			&ReferenceName::Ip(ref addr) => {
				let aref = IpAddressBlock::from(*addr);
				if !ip_in_set(eecert.dnsnames.iter_addrs(), &aref) {
					let names = extract_reference_names(&eecert);
					fail!(EeNotValidForIp(aref, names));
				}
			},
		}
	}
	Ok(())
}

///Parameters for certificate sanity check using `CaInformation::new()`
pub struct SanityCheckCaChain<'c>
{
	///The public key kill list. If the chain contains public keys that match the SHA-256 hashes in this list,
	///the validation fails. In case of trust anchors that are not sent by the server, trust anchors with
	///matching keys are just ignored.
	pub killist: &'c BTreeSet<[u8; 32]>,
	///If `true` the certificate has to have tls_client Extended Key Usage. If `false`, the certificate has
	///to have tls_server Extended Key Usage.
	pub is_client: bool,
	///The signature algorithms allowed for the certficate chain.
	pub policy: SignatureAlgorithmEnabled2,
	///Extended parameters for sanity check.
	pub extended: SanityCheckCaChainExtended<'c>
}

///Extended parameters for certificate sanity check using  [`sanity_check_ca_chain`]
///
///[`sanity_check_ca_chain`]: fn.sanity_check_ca_chain.html
pub struct SanityCheckCaChainExtended<'c>
{
	dummy: PhantomData<&'c u8>,
}

impl<'c> Default for SanityCheckCaChainExtended<'c>
{
	fn default() -> SanityCheckCaChainExtended<'c>
	{
		SanityCheckCaChainExtended {
			dummy: PhantomData,
		}
	}
}

///Results of `sanity_check_ca_chain()`
#[derive(Clone,Debug)]
pub struct CaInformation
{
	subject: Vec<u8>,
	pubkey: Vec<u8>,
	valid_until: Timestamp,
	valid_from: Timestamp,
	name_whitelist: Vec<Vec<u8>>,
	ip_whitelist: Vec<IpAddressBlock>,
	name_blacklist: Vec<Vec<u8>>,
	ip_blacklist: Vec<IpAddressBlock>,
	is_client: bool,
	policy: SignatureAlgorithmEnabled2,
	__dummy: ()
}

impl CaInformation
{
	///Get CA subject and public key.
	pub fn ca_subject<'a>(&'a self) -> (CertificateSubject<'a>, &'a [u8])
	{
		(CertificateSubject(&self.subject), &self.pubkey)
	}
	#[allow(missing_docs)] #[deprecated(since="2.4.0", note="Use CaInformation::new2() instead")]
	pub fn new(_icerts: &[&[u8]], time_now: &dyn TimeInterface, p: SanityCheckCaChain) ->
		Result<CaInformation, CertificateValidationError>
	{
		let time_now = time_now.get_time();			//Freeze time.
		Self::new2(_icerts, time_now, p)
	}
	///Construct CA information.
	pub fn new2(_icerts: &[&[u8]], time_now: Timestamp, p: SanityCheckCaChain) ->
		Result<CaInformation, CertificateValidationError>
	{
		use self::_CertificateValidationError::*;
		let mut valid_until = Timestamp::positive_infinity();	//Maximum i64.

		let mut icerts = Vec::new();
		//Do icert0 specially.
		let icert0 = _icerts.get(0).ok_or(NoIssuerCert)?;
		let icert = ParsedCertificate2::from(icert0).map_err(|x|CaCertParseError(0, x))?;
		let orig_subject = icert.subject;
		let orig_pubkey = icert.pubkey;
		let orig_valid_until = icert.not_after;
		let orig_valid_from = icert.not_before;
		let mut issuer_of_subject = icert.issuer;
		let mut selfsigned = icert.issuer.same_as(icert.subject);
		let mut validity_range = icert.not_before..icert.not_after;
		let mut name_whitelist: Vec<_> = icert.names_allow.iter().collect();
		let mut ip_whitelist: Vec<_> = icert.names_allow.iter_addrs().collect();
		let mut name_blacklist: Vec<_> = icert.names_disallow.iter().collect();
		let mut ip_blacklist: Vec<_> = icert.names_disallow.iter_addrs().collect();
		valid_until = valid_until.first(icert.not_after);
		fail_if!(key_on_killist(icert.pubkey, p.killist), CaKillisted(0));
		//Not done in ca_certificate_checks because nest checks need to be pre-empted.
		fail_if!(is_expired_ca(icert.issuer, icert.not_after), CaCertIssuerExpired(0));
		ca_certificate_checks(icert, None, 0, time_now, true, &mut 0, false, &mut icerts,
			false, p.is_client, p.policy, false)?;

		for (index, cert) in _icerts.iter().enumerate() {
			if index == 0 { continue; }	//Skip first one, as it was done above.
			let icert = ParsedCertificate2::from(cert).map_err(|x|CaCertParseError(index, x))?;
			//Pre-empt nest check.
			fail_if!(is_expired_ca(icert.issuer, icert.not_after), CaCertIssuerExpired(index));
			//Check that last certificate is not selfsigned, and that issuer of last matches this
			//subject.
			fail_if!(selfsigned, SelfsignedCertificateNotLast);
			fail_if!(!issuer_of_subject.same_as(icert.subject), BrokenChain(index));
			issuer_of_subject = icert.issuer;
			selfsigned = icert.issuer.same_as(icert.subject);
			//Consider effect on validity. Validity start is handled specially: If the slave is not
			//the end-entity certificate, and both master and slave have not_before in past, interpret
			//that as proper nesting. This case can happen with root cross-signs.
			valid_until = valid_until.first(icert.not_after);
			let start_nests = validity_range.start >= icert.not_before ||
				validity_range.start < time_now && icert.not_before < time_now;
			let end_nests = validity_range.end <= icert.not_after;
			fail_if!(!start_nests || !end_nests, TimeNotNested(index));
			validity_range = icert.not_before..icert.not_after;	//Always wider.
			//Check CA on killist (we check this first, because ca_certificate_checks consumes the
			//parsed certificate.
			fail_if!(key_on_killist(icert.pubkey, p.killist), CaKillisted(index));
			//Consider effect on whitelists/blacklists.
			constraint_intersection(&mut name_whitelist, icert.names_allow.iter(),
				compare_name_constraints);
			constraint_intersection(&mut ip_whitelist, icert.names_allow.iter_addrs(),
				compare_ip_constraints);
			constraint_union(&mut name_blacklist, icert.names_disallow.iter(),
				compare_name_constraints);
			constraint_union(&mut ip_blacklist, icert.names_disallow.iter_addrs(),
				compare_ip_constraints);
			//Basic checks.
			ca_certificate_checks(icert, None, index, time_now, true, &mut 0, false, &mut icerts,
				false, p.is_client, p.policy, false)?;
		}
		//Check path length constraints.
		check_pathlen_constraints(&icerts)?;

		//Check that there are no revoked certificates in remaining chain, and all signature algorithms are
		//allowed. Note that due to truncation of chain above, the allowed_signature_algorithm is never
		//appiled to self-signed root certificate.
		for cert in icerts.iter() {
			fail_if!(is_revoked(cert.issuer.as_raw_issuer(), cert.serial_number, cert.pubkey),
				ChainRevoked);
			fail_if!(!allowed_signature_algorithm(cert.signature, p.policy),
				ChainBadSignatureAlgorithm);
		}
		//Now, intermediates[n] should be signed by intermediates[n+1].
		for (tindex, tcert) in icerts.iter().enumerate() {
			let nindex = tindex + 1;
			let nkey = f_break!(icerts.get(nindex)).pubkey;
			//Do not do known_signature check on constrained certificates, because it is unlikely to
			//succeed.
			if !tcert.constrained && known_signature(tcert.issuer.as_raw_issuer(), nkey,
				tcert.entiere) {
				continue;
			}
			//Check signature.
			tcert.signature.set_err(SignatureUnknown(nindex))?.
				verify(nkey, tcert.tbs, p.policy, SignatureFlags2::CERTIFICATE).
				map_err(|x|SignatureFailure(nindex, x))?;
		}
		//All checks passed.
		Ok(CaInformation {
			subject: orig_subject.as_raw_subject().to_owned(),
			pubkey: orig_pubkey.to_owned(),
			valid_from: orig_valid_from,
			valid_until: orig_valid_until,
			name_whitelist: name_whitelist.iter().map(|&x|x.to_owned()).collect(),
			name_blacklist: name_blacklist.iter().map(|&x|x.to_owned()).collect(),
			ip_whitelist, ip_blacklist,
			is_client: p.is_client,
			policy: p.policy,
			__dummy: ()
		})
	}
	#[allow(missing_docs)] #[deprecated(since="2.4.0", note="Use CaInformation::check_ee2() instead")]
	pub fn check_ee(&self, eecert: &ParsedCertificate2, time_now: &dyn TimeInterface) ->
		Result<(), CertificateValidationError>
	{
		let time_now = time_now.get_time();
		self.check_ee2(eecert, time_now)
	}
	///Check EE certificate against CA.
	pub fn check_ee2(&self, eecert: &ParsedCertificate2, time_now: Timestamp) ->
		Result<(), CertificateValidationError>
	{
		use self::_CertificateValidationError::*;
		//EE certificate must be issued by correct issuer.
		fail_if!(eecert.issuer.as_raw_issuer() != self.subject.deref(), DidNotIssueEeCert(0));
		//The issuer must be valid long enough, and certificate must not be expired.
		fail_if!(eecert.not_before < self.valid_from, TimeNotNested(0));
		fail_if!(eecert.not_after > self.valid_until, TimeNotNested(0));
		fail_if!(eecert.not_after < time_now, EeExpired(eecert.not_after));
		//Check signature on EE certificate.
		eecert.signature.set_err(SignatureUnknown(0))?.
			verify(&self.pubkey, eecert.tbs, self.policy, SignatureFlags2::CERTIFICATE).
			map_err(|x|SignatureFailure(0, x))?;
		//Do EE certificate sanity checks. No need to assert strict_algo, as signature check is done.
		ee_certificate_checks(&eecert, time_now, false, 1, &mut 0, false, self.is_client, self.policy,
			false)?;
		//Check name constraints.
		for j in eecert.dnsnames.iter() {
			fail_if!(!check_name_constraints(j, self.name_whitelist.iter().map(|x|x.deref()),
				self.name_blacklist.iter().map(|x|x.deref())),
				NameConstraintViolation(1, EscapeByteString(j).to_string()));
		}
		for j in eecert.dnsnames.iter_addrs() {
			fail_if!(!check_name_constraints_ip(j, self.ip_whitelist.iter().cloned(),
				self.ip_blacklist.iter().cloned()), NameConstraintViolationIp(1, j));
		}
		Ok(())
	}
}

fn constraint_intersection<T:Copy>(output: &mut Vec<T>, input: impl Iterator<Item=T>+Clone,
	cmp: fn(T, T) -> Option<Ordering>)
{
	if output.len() == 0 {
		//If output is empty, take maximal entries
		//There should not be lots of constraints, so this quadric algorithm ought to do.
		'x: for (index, joining) in input.clone().enumerate() {
			//In case some clown tries to add entries that are not incomparable, reject any entry that
			//is strict subset of any other entry, or equal to any previous entry.
			for (index2, &joining2) in output.iter().enumerate() {
				if index == index2 { continue; }
				match cmp(joining, joining2) {
					Some(Ordering::Greater) => break 'x,
					Some(Ordering::Equal) if index2 < index => break 'x,
					_ => ()
				}
			}
			output.push(joining);
		}
		return;
	} else if input.clone().next().is_none() {
		//If input is empty, return output as-is.
		return;
	}
	//Now, neither input nor output is empty.
	//There should not be lots of constraints, so this quadric algorithm ought to do.
	let mut idx = 0;
	'y: while idx < output.len() {
		//Scan for incoming entry that is superset of existing. If found, keep existing entry. If no
		//related entries are found, delete existing entry.
		let mut found_related = false;
		for joining in input.clone() {
			match cmp(output[idx], joining) {
				Some(Ordering::Greater|Ordering::Equal) => {
					idx += 1;
					continue 'y;
				},
				Some(_) => found_related = true,
				None => ()
			};
		}
		let to_replace: T = output.swap_remove(idx);
		if !found_related { continue; }
		//Push all maximal entries of incoming related to to_replace to output. None of them is a superset.
		//Because finding maximals is annyoing, push all and then remove overlaps. This can not lead to
		//recursion because superset check keeps the element and does not readd it.
		for joining in input.clone() {
			if cmp(to_replace, joining).is_some() { output.push(joining); }
		}
	}
	//Remove overlaps.
	let mut idx = 0;
	while idx < output.len() {
		let mut delete = false;
		for (idx2, &entry) in output.iter().enumerate() {
			if idx == idx2 { continue; }	//Same entry!
			if let Some(Ordering::Greater|Ordering::Equal) = cmp(output[idx], entry) {
				//Subset of another, delete this.
				delete = true;
			}
		}
		if delete {
			output.swap_remove(idx);
		} else {
			idx += 1;
		}
	}
}

#[cfg(test)]
#[derive(Clone)]
struct TIterate<'a>(&'a [&'static [u8]], usize);

#[cfg(test)]
impl<'a> Iterator for TIterate<'a>
{
	type Item = &'static [u8];
	fn next(&mut self) -> Option<&'static [u8]>
	{
		let ent = self.0.get(self.1)?;
		self.1 += 1;
		Some(ent)
	}
}

#[test]
fn test_constraint_intersection()
{
	let mut v = Vec::new();
	let na: &[u8] = b".a.com";
	let nb: &[u8] = b".b.com";
	let nbx: &[u8] = b".x.b.com";
	let nby: &[u8] = b".y.b.com";
	let nbyz: &[u8] = b".z.y.b.com";
	let ncom: &[u8] = b".com";
	constraint_intersection(&mut v, TIterate(&[na, nb, nbx, na], 0), compare_name_constraints);
	assert_eq!(&v[..], &[na, nb]);
	constraint_intersection(&mut v, TIterate(&[ncom,nbx],0), compare_name_constraints);
	assert_eq!(&v[..], &[na, nb]);
	constraint_intersection(&mut v, TIterate(&[na,nbx,nbyz,nbyz,nby],0), compare_name_constraints);
	assert_eq!(&v[..], &[na, nbx, nby]);
}

fn constraint_union<T:Copy>(output: &mut Vec<T>, input: impl Iterator<Item=T>+Clone,
	cmp: fn(T, T) -> Option<Ordering>)
{
	//There should not be lots of constraints, so this quadric algorithm ought to do.
	'x: for (index, joining) in input.clone().enumerate() {
		//In case some clown tries to add entries that are not incomparable, reject any entry that is
		//strict subset of any other entry, or equal to any previous entry.
		for (index2, &joining2) in output.iter().enumerate() {
			if index == index2 { continue; }
			match cmp(joining, joining2) {
				Some(Ordering::Greater) => break 'x,
				Some(Ordering::Equal) if index2 < index => break 'x,
				_ => ()
			}
		}
		//Reject any entry that is (non-strict) subset of existing entry. Record if this new entry is
		//superset of any entry.
		let mut is_superset = false;
		for &present in output.iter() { match cmp(joining, present) {
			Some(Ordering::Less) => is_superset = true,
			Some(Ordering::Equal|Ordering::Greater) => break 'x,
			None => ()
		}}
		if is_superset {
			//Remove any output entry that is strict subset of the joining entry.
			let mut idx = 0;
			while idx < output.len() {
				if cmp(joining, output[idx]) == Some(Ordering::Less) {
					output.swap_remove(idx);
				} else {
					idx += 1;
				}
			}
		}
		//Add the new entry.
		output.push(joining);
	}
}

#[test]
fn test_constraint_union()
{
	let mut v = Vec::new();
	let na: &[u8] = b".a.com";
	let nb: &[u8] = b".b.com";
	let nbx: &[u8] = b".x.b.com";
	let nby: &[u8] = b".y.b.com";
	let nc: &[u8] = b".c.com";
	let nd: &[u8] = b".d.org";
	let ncom: &[u8] = b".com";
	constraint_union(&mut v, TIterate(&[na, nb, nbx, na], 0), compare_name_constraints);
	assert_eq!(&v[..], &[na, nb]);
	constraint_union(&mut v, TIterate(&[nby], 0), compare_name_constraints);
	assert_eq!(&v[..], &[na, nb]);
	constraint_union(&mut v, TIterate(&[nc], 0), compare_name_constraints);
	assert_eq!(&v[..], &[na, nb, nc]);
	constraint_union(&mut v, TIterate(&[nd], 0), compare_name_constraints);
	assert_eq!(&v[..], &[na, nb, nc, nd]);
	constraint_union(&mut v, TIterate(&[ncom], 0), compare_name_constraints);
	assert_eq!(&v[..], &[nd, ncom]);
}

fn compare_name_constraints_prefix(ha: &[u8], ta: &[u8], b: &[u8]) -> Option<Ordering>
{
	if !matches_ascii_case_insensitive(ta, b) {
		//1) If tail is not the same, there definitely is no relationship.
		None
	} else if !b.starts_with(b".") {
		//2) If common tail does not start with '.',...
		//2a) If prefix is '.', then shorter name is actually more specific.
		//2b) Otherwise Names are unrelated.
		if ha == b"." { Some(Ordering::Less) } else { None }
	} else {
		//3) Otherwise longer name is subset of shorter.
		Some(Ordering::Greater)
	}
}

//Some(Less) if b is strict subset of a.
//Some(Equal) if a and b are the same set.
//Some(Greater) if a is strict subset of b.
//None if neither a nor are subsets of each other.
fn compare_name_constraints(a: &[u8], b: &[u8]) -> Option<Ordering>
{
	//Split prefixes and common tail.
	if a.len() > b.len() {
		let (ha, ta) = a.split_at(a.len() - b.len());
		compare_name_constraints_prefix(ha, ta, b)
	} else if b.len() > a.len() {
		let (hb, tb) = b.split_at(b.len() - a.len());
		//The comparision is still anticommutative.
		compare_name_constraints_prefix(hb, tb, a).map(|o|o.reverse())
	} else {
		//a and b are the same length. They must be either equal or incomparable.
		if !matches_ascii_case_insensitive(a, b) { None } else { Some(Ordering::Equal) }
	}
}

#[test]
fn name_constraint_comparision()
{
	assert_eq!(compare_name_constraints(b"foo.bar", b"foo.bar"), Some(Ordering::Equal));
	assert_eq!(compare_name_constraints(b".foo.bar", b".foo.bar"), Some(Ordering::Equal));
	assert_eq!(compare_name_constraints(b"zot.foo.bar", b".foo.bar"), Some(Ordering::Greater));
	assert_eq!(compare_name_constraints(b".foo.bar", b"zot.foo.bar"), Some(Ordering::Less));
	assert_eq!(compare_name_constraints(b"foo.bar", b".foo.bar"), Some(Ordering::Greater));
	assert_eq!(compare_name_constraints(b".foo.bar", b"foo.bar"), Some(Ordering::Less));
	assert_eq!(compare_name_constraints(b"zot.foo.bar", b"foo.bar"), None);
	assert_eq!(compare_name_constraints(b"foo.bar", b"zot.foo.bar"), None);
	assert_eq!(compare_name_constraints(b"foo.bar", b"xfoo.bar"), None);
	assert_eq!(compare_name_constraints(b"xfoo.bar", b"foo.bar"), None);
	assert_eq!(compare_name_constraints(b".foo.bar", b"xfoo.bar"), None);
	assert_eq!(compare_name_constraints(b"xfoo.bar", b".foo.bar"), None);
}

//Some(Less) if b is strict subset of a.
//Some(Equal) if a and b are the same set.
//Some(Greater) if a is strict subset of b.
//None if neither a nor are subsets of each other.
fn compare_ip_constraints(a: IpAddressBlock, b: IpAddressBlock) -> Option<Ordering>
{
	match (a.is_subset_of(&b), b.is_subset_of(&a)) {
		(true, true) => Some(Ordering::Equal),
		(true, false) => Some(Ordering::Greater),
		(false, true) => Some(Ordering::Less),
		(false, false) => None,
	}
}

#[test]
fn ip_constraint_comparision()
{
	let a = [192,168,0,0];
	let b = [192,168,128,0];
	assert_eq!(compare_ip_constraints(IpAddressBlock::from_ipv4_masklen(a, 16),
		IpAddressBlock::from_ipv4_masklen(a, 16)), Some(Ordering::Equal));
	assert_eq!(compare_ip_constraints(IpAddressBlock::from_ipv4_masklen(a, 16),
		IpAddressBlock::from_ipv4_masklen(b, 17)), Some(Ordering::Less));
	assert_eq!(compare_ip_constraints(IpAddressBlock::from_ipv4_masklen(b, 17),
		IpAddressBlock::from_ipv4_masklen(a, 16)), Some(Ordering::Greater));
	assert_eq!(compare_ip_constraints(IpAddressBlock::from_ipv4_masklen(b, 17),
		IpAddressBlock::from_ipv4_masklen(a, 17)), None);
	assert_eq!(compare_ip_constraints(IpAddressBlock::from_ipv4_masklen(a, 17),
		IpAddressBlock::from_ipv4_masklen(b, 17)), None);
}

#[allow(missing_docs)] #[deprecated(since="2.4.0", note="Use certificate_server_interlock_check2() instead")]
pub fn certificate_server_interlock_check<'b>(eecert: &ParsedCertificate2<'b>, icerts: &[&'b [u8]],
	reference: Option<ReferenceName>, features: u32, killist: &BTreeSet<[u8; 32]>,
	time_now: &dyn TimeInterface) -> Result<u32, CertificateValidationError>
{
	let time_now = time_now.get_time();
	certificate_server_interlock_check2(eecert, icerts, reference, features, killist, time_now)
}

///Perform server interlock cheks on certificate.
pub fn certificate_server_interlock_check2<'b>(eecert: &ParsedCertificate2<'b>, icerts: &[&'b [u8]],
	reference: Option<ReferenceName>, features: u32, killist: &BTreeSet<[u8; 32]>,
	time_now: Timestamp) -> Result<u32, CertificateValidationError>
{
	let mut dummy = false;
	let mut dummy2 = false;
	let p = SanityCheckCertificateChain2 {
		strict_algo: true,
		feature_flags: features,
		need_ocsp_flag: &mut dummy,
		need_sct_flag: &mut dummy2,
		is_client: false,
		policy: SignatureAlgorithmEnabled2::unsafe_any_policy(),
		killist: killist,
		require_disclosed_ca: false,	//Not supported on server side.
		extended: SanityCheckCertificateChainExtended::default(),
	};
	let mut flags = 0;
	#[allow(deprecated)]
	__sanity_check_certificate_chain(eecert, icerts, reference, time_now, p,
		|_,_,props,_|Ok(flags = props.need_features), ())?;
	Ok(flags)
}


#[allow(deprecated)]
#[allow(missing_docs)] #[deprecated(since="2.4.0", note="use full_certificate_validation() or certificate_server_interlock_check()")]
pub fn sanity_check_certificate_chain2<'c,'b, T, F, P>(_eecert: &'b [u8], _icerts: &[&'b [u8]],
	reference: Option<ReferenceName>, time_now: &dyn TimeInterface, p: SanityCheckCertificateChain2<'c>,
	callback: F, cbparam: P) -> Result<T, CertificateValidationError> where F:
	FnMut(&ParsedCertificate2<'b>, &[ParsedCertificate2<'b>],
	ValidateCertificateAfterSanityCheck<'b,'b,'c>, P) -> Result<T, CertificateValidationError>
{
	let time_now = time_now.get_time();		//Freeze time.
	use self::_CertificateValidationError::*;
	let eecert: ParsedCertificate2<'b> = ParsedCertificate2::from(_eecert).map_err(EeCertParseError)?;
	__sanity_check_certificate_chain(&eecert, _icerts, reference, time_now, p, callback, cbparam)
}

#[allow(deprecated)]
fn __sanity_check_certificate_chain<'c,'b, T, F, P>(eecert: &ParsedCertificate2<'b>, _icerts: &[&'b [u8]],
	reference: Option<ReferenceName>, time_now: Timestamp, p: SanityCheckCertificateChain2<'c>,
	mut callback: F, cbparam: P) -> Result<T, CertificateValidationError> where F:
	FnMut(&ParsedCertificate2<'b>, &[ParsedCertificate2<'b>],
	ValidateCertificateAfterSanityCheck<'b,'b,'c>, P) -> Result<T, CertificateValidationError>
{
	use self::_CertificateValidationError::*;
	let mut feature_flags_present: u32 = 0;
	let mut valid_until = Timestamp::positive_infinity();	//Maximum i64.

	//The basic checks on EE.
	let mut issuer_of_subject = eecert.issuer;
	let mut selfsigned = eecert.issuer.same_as(eecert.subject);
	ee_certificate_checks(&eecert, time_now, p.strict_algo, _icerts.len(), &mut feature_flags_present,
		false, p.is_client, p.policy, p.extended.test_ignore_build)?;
	//Check SAN in EE.
	check_reference_name_in_certificate(&eecert, reference)?;
	//Check EE on killist.
	fail_if!(key_on_killist(eecert.pubkey, p.killist), EeKillisted);
	//Consider effect on validity.
	valid_until = valid_until.first(eecert.not_after);
	let mut validity_range = eecert.not_before..eecert.not_after;

	let mut icerts = Vec::new();
	for (index, cert) in _icerts.iter().enumerate() {
		let icert = ParsedCertificate2::from(cert).map_err(|x|CaCertParseError(index, x))?;
		//Pre-empt nest check.
		fail_if!(is_expired_ca(icert.issuer, icert.not_after), CaCertIssuerExpired(index));
		//Check that last certificate is not selfsigned, and that issuer of last matches this subject.
		fail_if!(selfsigned, SelfsignedCertificateNotLast);
		fail_if!(!issuer_of_subject.same_as(icert.subject), BrokenChain(index));
		issuer_of_subject = icert.issuer;
		selfsigned = icert.issuer.same_as(icert.subject);
		//Consider effect on validity. Validity start is handled specially: If the slave is not the end-
		//entity certificate, and both master and slave have not_before in past, interpret that as proper
		//nesting. This case can happen with root cross-signs.
		valid_until = valid_until.first(icert.not_after);
		let start_nests = validity_range.start >= icert.not_before || validity_range.start < time_now &&
			icert.not_before < time_now && index > 0;
		let end_nests = validity_range.end <= icert.not_after;
		fail_if!(!start_nests || !end_nests, TimeNotNested(index));
		validity_range = icert.not_before..icert.not_after;	//Always wider.
		//Check CA on killist (we check this first, because ca_certificate_checks consumes the parsed
		//certificate.
		fail_if!(key_on_killist(icert.pubkey, p.killist), CaKillisted(index));
		//Basic checks.
		ca_certificate_checks(icert, Some(&eecert), index, time_now, p.strict_algo,
			&mut feature_flags_present, p.require_disclosed_ca, &mut icerts, false, p.is_client,
			p.policy, p.extended.test_ignore_build)?;
	}
	//Check path length constraints.
	check_pathlen_constraints(&icerts)?;

	//Check features. We set a flag if certificate is must-staple, because if OCSP is available isn't known yet.
	//The same with SCT stapling.
	*p.need_ocsp_flag |= feature_flags_present & CFLAG_MUST_STAPLE != 0;
	let features_unmet = feature_flags_present & !p.feature_flags;
	if features_unmet != 0 {
		fail_if!(features_unmet & CFLAG_MUST_STAPLE != 0, OcspStapleRequred);
		fail_if!(features_unmet & CFLAG_UNKNOWN != 0, RequireUnknownFeatures);
		fail!(UnmetFeatures(features_unmet));
	}

	let params = ValidateCertificateAfterSanityCheck {
		killist: p.killist,
		valid_until: valid_until,
		need_features: feature_flags_present,
		require_disclosed_ca: p.require_disclosed_ca,
		//Use dummy values for deprecated entries.
		keys_map: BTreeMap::new(),
		seen: BTreeSet::new(),
		constraint_flags: 0,
		_dummy: (),
	};
	callback(&eecert, &icerts, params, cbparam)
}

#[allow(missing_docs)] #[deprecated(since="2.4.0", note="Use sanity_check_certificate_chain_load2() instead")]
pub fn sanity_check_certificate_chain_load<'b>(_eecert: &'b [u8], _icerts: &[&'b [u8]], pubkey: &[u8],
	time_now: &dyn TimeInterface, need_ocsp_flag: &mut bool, _unused: &mut bool, selfsigned_ok: bool) ->
	Result<(), CertificateValidationError>
{
	let time_now = time_now.get_time();
	sanity_check_certificate_chain_load2(_eecert, _icerts, pubkey, time_now, need_ocsp_flag, _unused,
		selfsigned_ok)
}

///Perform load-time sanity-check a certificate chain.
///
///The end-entity certificate is `_eecert`, the set of intermediates is `_icerts`, the public key of the purported
///private key is `pubkey`, the current time is `time_now`.
///
///If the certificate requires OSCP stapling, `need_ocsp_flag` is set to `true`, otherwise it is left alone.
///
///This function does the same checks as [`sanity_check_certificate_chain()`], but with the following differences:
///
/// - The callback is not called.
/// - The signature algorithm must always be known.
/// - Only unknown feature flags trigger a failure, no known one does.
/// - No key kill list check is performed.
/// - One can allow self-signed end-entity certificates via setting the `selfsigned_ok` parameter to `true`.
/// - The public key is checked against the given one.
/// - The algorithms are not checked.
///
///This method is intended for load-time checking of certificate chains, as oppose to use-time checking that
///[`sanity_check_certificate_chain()`] is for.
///
///On success, returns `Ok(())`. If the sanity check failed, returns `Err(err)`, where `err` describes the error.
///
///[`sanity_check_certificate_chain()`]: fn.sanity_check_certificate_chain.html
pub fn sanity_check_certificate_chain_load2<'b>(_eecert: &'b [u8], _icerts: &[&'b [u8]], pubkey: &[u8],
	time_now: Timestamp, need_ocsp_flag: &mut bool, _unused: &mut bool, selfsigned_ok: bool) ->
	Result<(), CertificateValidationError>
{
	use self::_CertificateValidationError::*;
	let mut feature_flags_present: u32 = 0;

	//Check the public key given.
	let eecert: ParsedCertificate2<'b> = ParsedCertificate2::from(_eecert).map_err(|x|EeCertParseError(x))?;
	fail_if!(pubkey != eecert.pubkey, PublicKeyMismatch);

	if eecert.issuer.same_as(eecert.subject) && selfsigned_ok {
		//Self-signed, and self-signed OK, return Ok.
		return Ok(());
	}

	//Sanity-checking, so allow all algorithms.
	//The basic checks on EE (strict_algo is always true).
	let mut issuer_of_subject = eecert.issuer;
	let mut selfsigned = eecert.issuer.same_as(eecert.subject);
	ee_certificate_checks(&eecert, time_now, true, _icerts.len(), &mut feature_flags_present, true, false,
		SignatureAlgorithmEnabled2::unsafe_any_policy(), false)?;

	let mut icerts = Vec::new();
	for (index, cert) in _icerts.iter().enumerate() {
		let icert = ParsedCertificate2::from(cert).map_err(|x|CaCertParseError(index, x))?;
		//Check that last certificate is not selfsigned, and that issuer of last matches this subject.
		fail_if!(selfsigned, SelfsignedCertificateNotLast);
		fail_if!(!issuer_of_subject.same_as(icert.subject), BrokenChain(index));
		issuer_of_subject = icert.issuer;
		selfsigned = icert.issuer.same_as(icert.subject);
		//Sanity-checking, so allow all algorithms.
		//Basic checks (strict_algo is always true, require_disclosed_ca is always false).
		ca_certificate_checks(icert, Some(&eecert), index, time_now, true, &mut feature_flags_present,
			false, &mut icerts, true, false, SignatureAlgorithmEnabled2::unsafe_any_policy(), false)?;
	}

	//Check features. We set a flag if certificate is must-staple.
	*need_ocsp_flag |= feature_flags_present & CFLAG_MUST_STAPLE != 0;
	fail_if!(feature_flags_present & CFLAG_UNKNOWN != 0, RequireUnknownFeatures);

	check_pathlen_constraints(&icerts)?;
	Ok(())
}


#[allow(missing_docs)]
#[deprecated(since="2.1.0", note="Fundamentially unsafe, use validate_ee_spki2() instead")]
pub fn validate_ee_spki(ee_spki: &[u8], host_pins: &[HostSpecificPin]) -> Result<(), ()>
{
	let any_pin = host_pins.iter().any(|i|i.pin);
	if host_pins.iter().any(|i|i.ee_matches(ee_spki, any_pin, HashFunctionEnabled::unsafe_any_policy())) {
		Ok(())
	} else {
		Err(())
	}
}

///Validate EE SPKI against host pins.
///
///The SPKI to validate is `ee_spki` and the set of host pins is `host_pins`. The `ee_spki` is given in the X.509
///SubjectPublicKeyInfo format.
///
///This check succeeds if any host-specific pin entry matches end-entity certificate keys, represents the public
///key server sent and has (implicit) pin and trust anchor flags set.
///
///Note that this function is the only way to handle things like self-signed certificates, as the other validation
///functions do not like those.
///
///On success returns `Ok(())`. If the given key does not match any pin given, returns `Err(())`.
pub fn validate_ee_spki2(ee_spki: &[u8], host_pins: &[HostSpecificPin], hpolicy: HashFunctionEnabled) ->
	Result<(), ()>
{
	let any_pin = host_pins.iter().any(|i|i.pin);
	if host_pins.iter().any(|i|i.ee_matches(ee_spki, any_pin, hpolicy)) { Ok(()) } else { Err(()) }
}


#[cfg(test)]
mod test;
