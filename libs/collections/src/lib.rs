//!Re-exports of various collections.
//!
//!This crate contains re-exports of various container types from the standard library. These re-exports may either
//!come from libstd, or from various lower-level crates backing libstd.
//!
//!The following standard types are re-exported:
//!
//! * `Arc`
//! * `Box`
//! * `BTreeMap`
//! * `BTreeSet`
//! * `Cow`
//! * `Rc`
//! * `RcWeak` (`Weak` of `Rc`)
//! * `RefCell`
//! * `String`
//! * `ToOwned`
//! * `Vec`
//! * `Weak` (`Weak` of `Arc`)
//#![forbid(unsafe_code)]
#![forbid(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]

use crate::locking::*;
pub use core::cell::RefCell;
use core::default::Default;
use core::ops::Deref;
use core::ops::DerefMut;
use core::ptr::NonNull;
use core::ptr::null_mut;
use core::sync::atomic::AtomicPtr;
use core::sync::atomic::Ordering as MOrd;

extern crate alloc;
pub use alloc::borrow::Cow;
pub use alloc::borrow::ToOwned;
pub use alloc::boxed::Box;
pub use alloc::collections::BTreeMap;
pub use alloc::collections::BTreeSet;
pub use alloc::rc::Rc;
pub use alloc::rc::Weak as RcWeak;
pub use alloc::string::String;
pub use alloc::string::ToString;
pub use alloc::sync::Arc;
pub use alloc::sync::Weak;
pub use alloc::vec::Vec;

/*******************************************************************************************************************/
macro_rules! def_get_gm
{
	($clazz:ident, $inner:ident) => {
		impl<T:Default+Send+Sync> $clazz<T>
		{
			fn __get_noinit(&self) -> Option<NonNull<$inner<T>>>
			{
				NonNull::new(self.0.load(MOrd::Acquire))
			}
			fn __get(&self) -> NonNull<$inner<T>>
			{
				//Fastpath: If Mutex has already been created, return it.
				if let Some(ptr) = NonNull::new(self.0.load(MOrd::Acquire)) { return ptr; }
				self.__get_slow()
			}
			#[cold]
			fn __get_slow(&self) -> NonNull<$inner<T>>
			{
				let mutex = Box::new($inner::new(Default::default()));
				let mutex = Box::into_raw(mutex);
				//Try updating the pointer. If the pointer is not NULL, somebody raced with us and
				//won. Drop the object. Because current is NULL and this is strong exchange, ptr can
				//not be NULL.
				if let Err(ptr) = cmpxchg_ptr(&self.0, null_mut(), mutex) {
					unsafe{drop(Box::from_raw(mutex));}
					return unsafe{NonNull::new_unchecked(ptr)};
				}
				//We won the race. Return mutex. It can not be NULL.
				unsafe{NonNull::new_unchecked(mutex)}
			}
		}
	}
}

fn cmpxchg_ptr<T:Sized>(x: &AtomicPtr<T>, current: *mut T, new: *mut T) -> Result<*mut T, *mut T>
{
	x.compare_exchange(current, new, MOrd::AcqRel, MOrd::Acquire)
}

///Global mutex.
///
///Note that to be useful, T needs to be `Default`, `Send` and `Sync`. The `Sized` bound on type and `new()` is
///to work around things being unstable as usual.
pub struct GlobalMutex<T:Sized>(AtomicPtr<Mutex<T>>);

impl<T:Sized> GlobalMutex<T>
{
	///Create new.
	pub const fn new() -> GlobalMutex<T>
	{
		GlobalMutex(AtomicPtr::new(null_mut()))
	}
}

impl<T:Default+Send+Sync> GlobalMutex<T>
{
	///Lock for read.
	pub fn with_noinit(&self, func: impl FnOnce(&mut T))
	{
		unsafe{self.__get_noinit().map(|x|x.as_ref().with(func))};
	}
	///Lock.
	pub fn with<R:Sized>(&self, func: impl FnOnce(&mut T) -> R) -> R
	{
		unsafe{self.__get().as_ref().with(func)}
	}
}

def_get_gm!(GlobalMutex, Mutex);

///Global rwlock.
///
///Note that to be useful, T needs to be `Default`, `Send` and `Sync`. The `Sized` bound on type and `new()` is
///to work around things being unstable as usual.
pub struct GlobalRwLock<T:Sized>(AtomicPtr<RwLock<T>>);

impl<T:Sized> GlobalRwLock<T>
{
	///Create new.
	pub const fn new() -> GlobalRwLock<T>
	{
		GlobalRwLock(AtomicPtr::new(null_mut()))
	}
}

impl<T:Default+Send+Sync> GlobalRwLock<T>
{
	///Lock for read.
	pub fn with_read_noinit(&self, func: impl FnOnce(&T))
	{
		unsafe{self.__get_noinit().map(|x|x.as_ref().with_read(func))};
	}
	///Lock for read.
	pub fn with_read<R:Sized>(&self, func: impl FnOnce(&T) -> R) -> R
	{
		unsafe{self.__get().as_ref().with_read(func)}
	}
	///Lock for write.
	pub fn with_write<R:Sized>(&self, func: impl FnOnce(&mut T) -> R) -> R
	{
		unsafe{self.__get().as_ref().with_write(func)}
	}
}

def_get_gm!(GlobalRwLock, RwLock);

/*******************************************************************************************************************/
///Mutex
///
///A mutex can be held by at most one thread at a time. When held (locked), it grants the thread access to the
///inner object (of type `T`).
///
///Recursive locking is not supported, and likely leads to a deadlock.
pub struct Mutex<T:Sized>(__Mutex<T>);
unsafe impl<T:Sized+Send> Send for Mutex<T> {}
unsafe impl<T:Sized+Send> Sync for Mutex<T> {}
///R/W Lock
///
///R/W locks are closely related to mutexes. The difference is that they can be locked for read or write. When
///locked for read, there can be multiple simultaneout outstanding holds, but each hold only grants read-only
///access to the inner object (of type `T`).
///
///It is unspecified if more read locks are granted while there is a pending write lock or not.
pub struct RwLock<T:Sized>(__RwLock<T>);
unsafe impl<T:Sized+Send> Send for RwLock<T> {}
unsafe impl<T:Sized+Send> Sync for RwLock<T> {}
///Mutex guard
///
///This represents locked mutex. Dropping the guard unlocks the mutex, and the inner object can be accessed via the
///`Deref` and `DerefMut` traits.
pub struct MutexGuard<'a,T:Sized>(__MutexGuard<'a,T>);
///RwLock read guard
///
///This represents rwlock locked for read. Dropping the guard unlocks the lock, and the inner object can be
///accessed via the `Deref` trait.
///
///Poisoning is not supported. If thread panics while holding the lock, the lock is just unlocked.
pub struct RwLockReadGuard<'a,T:Sized>(__RwLockReadGuard<'a,T>);
///RwLock write guard
///
///This represents rwlock locked for write. Dropping the guard unlocks the lock, and the inner object can be
///accessed via the `Deref` and `DerefMut` traits.
///
///Poisoning is not supported. If thread panics while holding the lock, the lock is just unlocked.
pub struct RwLockWriteGuard<'a,T:Sized>(__RwLockWriteGuard<'a,T>);
///Once
///
///A function that is only called once.
pub struct Once(__Once);

impl<'a,T:Sized> Deref for MutexGuard<'a,T>
{
	type Target = T;
	fn deref(&self) -> &T { self.0.deref() }
}

impl<'a,T:Sized> DerefMut for MutexGuard<'a,T>
{
	fn deref_mut(&mut self) -> &mut T { self.0.deref_mut() }
}

impl<'a,T:Sized> Deref for RwLockReadGuard<'a,T>
{
	type Target = T;
	fn deref(&self) -> &T { self.0.deref() }
}

impl<'a,T:Sized> Deref for RwLockWriteGuard<'a,T>
{
	type Target = T;
	fn deref(&self) -> &T { self.0.deref() }
}

impl<'a,T:Sized> DerefMut for RwLockWriteGuard<'a,T>
{
	fn deref_mut(&mut self) -> &mut T { self.0.deref_mut() }
}

impl<T:Sized> RwLock<T>
{
	///Create new RwLock.
	pub fn new(val: T) -> RwLock<T> { RwLock(__RwLock::new(val)) }
	///Lock RwLock for read.
	///
	///If the lock is not held for write nor there is pending writer, the lock will be immediately granted.
	///If there is pending writer, it is unspecified if lock is immediately granted or if it blocks until
	///writer unlocks the lock. If the lock is held for write, this method blocks until writer releases the
	///lock.
	pub fn read(&self) -> RwLockReadGuard<T> { RwLockReadGuard(__handle_poison(self.0.read())) }
	///Try read lock mutex.
	///
	///This is similar as `read()`, but never blocks the thread. Instead, if `read()` would have blocked the
	///the thread, this returns `None`.
	pub fn try_read(&self) -> Option<RwLockReadGuard<T>>
	{
		__handle_trylock_poison(self.0.try_read()).map(RwLockReadGuard)
	}
	///Lock mutex for write.
	///
	///There can be only one writer at time, if the lock is not unlocked, this will block until the lock is
	///released. It is unspecified if new readers can grab the lock in the meantime.
	pub fn write(&self) -> RwLockWriteGuard<T> { RwLockWriteGuard(__handle_poison(self.0.write())) }
	///Try write lock mutex.
	///
	///This is similar as `write()`, but never blocks the thread. Instead, if `write()` would have blocked the
	///the thread, this returns `None`.
	pub fn try_write(&self) -> Option<RwLockWriteGuard<T>>
	{
		__handle_trylock_poison(self.0.try_write()).map(RwLockWriteGuard)
	}
	///Get without locking.
	///
	///This method returns mutable reference to innards of the lock for mutable reference to lock itself. The
	///mutable reference to lock proves that there can not be any other outstanding reference to the lock, so
	///this is safe.
	pub fn get_mut(&mut self) -> &mut T { __handle_poison(self.0.get_mut()) }
	///Destroy the lock and return the inner object.
	///
	///The owned reference proves this is the only reference to the lock, so it is safe to destry it and return
	///back the inner object.
	pub fn into_inner(self) -> T { __handle_poison(self.0.into_inner()) }
	///With read.
	///
	///This corresponds to locking the RwLock for read and then calling closure with reference to inner object
	///as parameter.
	pub fn with_read<F,R>(&self, f: F) -> R where F: FnOnce(&T) -> R
	{
		f(__handle_poison(self.0.read()).deref())
	}
	///With write.
	///
	///This corresponds to locking the RwLock for write and then calling closure with mutable reference to inner
	///object as parameter.
	pub fn with_write<F,R>(&self, f: F) -> R where F: FnOnce(&mut T) -> R
	{
		f(__handle_poison(self.0.write()).deref_mut())
	}
}

impl<T:Sized+core::fmt::Debug> core::fmt::Debug for Mutex<T>
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		match self.try_lock() {
			Some(x) => core::fmt::Debug::fmt(x.deref(), f),
			None => f.write_str("<locked>"),
		}
	}
}

impl<T:Sized> Mutex<T>
{
	///Create new mutex.
	pub fn new(val: T) -> Mutex<T> { Mutex(__Mutex::new(val)) }
	///Lock mutex.
	///
	///There can be only one holder at time, if the lock is not unlocked, this will block until the lock is
	///released.
	pub fn lock(&self) -> MutexGuard<T> { MutexGuard(__handle_poison(self.0.lock())) }
	///Try lock mutex.
	///
	///This is similar as `lock()`, but never blocks the thread. Instead, if `lock()` would have blocked the
	///the thread, this returns `None`.
	pub fn try_lock(&self) -> Option<MutexGuard<T>>
	{
		__handle_trylock_poison(self.0.try_lock()).map(MutexGuard)
	}
	///Get without locking.
	///
	///This method returns mutable reference to innards of the lock for mutable reference to lock itself. The
	///mutable reference to lock proves that there can not be any other outstanding reference to the lock, so
	///this is safe.
	pub fn get_mut(&mut self) -> &mut T { __handle_poison(self.0.get_mut()) }
	///Destroy the lock and return the inner object.
	///
	///The owned reference proves this is the only reference to the lock, so it is safe to destry it and return
	///back the inner object.
	pub fn into_inner(self) -> T { __handle_poison(self.0.into_inner()) }
	///With lock.
	///
	///This corresponds to locking the Mutex for and then calling closure with mutable reference to inner
	///object as parameter.
	pub fn with<F,R>(&self, f: F) -> R where F: FnOnce(&mut T) -> R
	{
		f(__handle_poison(self.0.lock()).deref_mut())
	}
}

///A condition variable.
pub struct Condvar(__Condvar);

impl Condvar
{
	///Create new condition variable.
	pub fn new() -> Condvar
	{
		Condvar(__Condvar::new())
	}
	///Wait on condition variable.
	pub fn wait<'a,T>(&self, guard: MutexGuard<'a,T>) -> MutexGuard<'a,T>
	{
		MutexGuard(__handle_poison(self.0.wait(guard.0)))
	}
	///Notify blocked threads on this condition variable.
	pub fn notify_all(&self)
	{
		self.0.notify_all()
	}
}

impl<T:Sized+core::fmt::Debug> core::fmt::Debug for RwLock<T>
{
	fn fmt(&self, f: &mut core::fmt::Formatter) -> Result<(), core::fmt::Error>
	{
		match self.try_read() {
			Some(x) => core::fmt::Debug::fmt(x.deref(), f),
			None => f.write_str("<locked>"),
		}
	}
}

impl Once
{
	///Create new.
	pub const fn new() -> Once { Once(__Once::new()) }
	///Call once.
	pub fn call_once<F>(&self, f: F) where F: FnOnce() { self.0.call_once(f) }
}

#[cfg(feature="std")]
mod locking
{
	extern crate std;
	pub(crate) type __Mutex<T> = std::sync::Mutex<T>;
	pub(crate) type __RwLock<T> = std::sync::RwLock<T>;
	pub(crate) type __MutexGuard<'a,T> = std::sync::MutexGuard<'a,T>;
	pub(crate) type __RwLockReadGuard<'a,T> = std::sync::RwLockReadGuard<'a,T>;
	pub(crate) type __RwLockWriteGuard<'a,T> = std::sync::RwLockWriteGuard<'a,T>;
	pub(crate) type __Condvar = std::sync::Condvar;
	pub(crate) type __Once = std::sync::Once;
	pub(crate) fn __handle_poison<T>(x: Result<T,std::sync::PoisonError<T>>) -> T
	{
		x.unwrap_or_else(std::sync::PoisonError::into_inner)
	}
	pub(crate) fn __handle_trylock_poison<T>(x: Result<T,std::sync::TryLockError<T>>) -> Option<T>
	{
		match x {
			Ok(x) => Some(x),
			Err(std::sync::TryLockError::Poisoned(x)) => Some(x.into_inner()),
			Err(std::sync::TryLockError::WouldBlock) => None,
		}
	}
}
#[cfg(not(feature="std"))]
mod locking
{
	use core::cell::UnsafeCell;
	use core::convert::identity;
	use core::hint::spin_loop;
	use core::ops::Deref;
	use core::ops::DerefMut;
	use core::sync::atomic::AtomicBool;
	use core::sync::atomic::AtomicUsize;
	use core::sync::atomic::Ordering as MOrd;
	pub(crate) fn __handle_poison<T>(x: T) -> T { x }
	pub(crate) fn __handle_trylock_poison<T>(x: Option<T>) -> Option<T> { x }
	pub(crate) struct __Mutex<T:Sized>(AtomicBool, UnsafeCell<T>);
	pub(crate) struct __RwLock<T:Sized>(AtomicUsize, UnsafeCell<T>);
	pub(crate) struct __MutexGuard<'a,T:Sized>(&'a AtomicBool, &'a mut T);
	pub(crate) struct __RwLockReadGuard<'a,T:Sized>(&'a AtomicUsize, &'a T);
	pub(crate) struct __RwLockWriteGuard<'a,T:Sized>(&'a AtomicUsize, &'a mut T);
	pub(crate) struct __Once(AtomicUsize);
	pub(crate) struct __Condvar(AtomicUsize);

	fn cmpxchg(x: &AtomicUsize, current: usize, new: usize) -> Result<usize, usize>
	{
		x.compare_exchange(current, new, MOrd::AcqRel, MOrd::Acquire)
	}

	fn cmpxchgb(x: &AtomicBool, current: bool, new: bool) -> Result<bool, bool>
	{
		x.compare_exchange(current, new, MOrd::AcqRel, MOrd::Acquire)
	}

	fn cmpxchg_weak(x: &AtomicUsize, current: usize, new: usize) -> Result<usize, usize>
	{
		x.compare_exchange_weak(current, new, MOrd::AcqRel, MOrd::Acquire)
	}

	fn cmpxchgb_weak(x: &AtomicBool, current: bool, new: bool) -> Result<bool, bool>
	{
		x.compare_exchange_weak(current, new, MOrd::AcqRel, MOrd::Acquire)
	}

	impl<'a,T:Sized> Drop for __MutexGuard<'a,T>
	{
		fn drop(&mut self) { self.0.store(false, MOrd::Release); }
	}
	impl<'a,T:Sized> Drop for __RwLockReadGuard<'a,T>
	{
		fn drop(&mut self) { self.0.fetch_sub(1, MOrd::AcqRel); }
	}
	impl<'a,T:Sized> Drop for __RwLockWriteGuard<'a,T>
	{
		//This always goes straight to unlocked.
		fn drop(&mut self) { self.0.store(0, MOrd::Release); }
	}

	impl<'a,T:Sized> Deref for __MutexGuard<'a,T>
	{
		type Target = T;
		fn deref(&self) -> &T { self.1 }
	}
	impl<'a,T:Sized> DerefMut for __MutexGuard<'a,T>
	{
		fn deref_mut(&mut self) -> &mut T { self.1 }
	}
	impl<'a,T:Sized> Deref for __RwLockReadGuard<'a,T>
	{
		type Target = T;
		fn deref(&self) -> &T { self.1 }
	}
	impl<'a,T:Sized> Deref for __RwLockWriteGuard<'a,T>
	{
		type Target = T;
		fn deref(&self) -> &T { self.1 }
	}
	impl<'a,T:Sized> DerefMut for __RwLockWriteGuard<'a,T>
	{
		fn deref_mut(&mut self) -> &mut T { self.1 }
	}

	impl<T:Sized> __Mutex<T>
	{
		pub(crate) fn new(val: T) -> __Mutex<T>
		{
			__Mutex(AtomicBool::new(false), UnsafeCell::new(val))
		}
		pub(crate) fn lock(&self) -> __MutexGuard<T>
		{
			loop {
				if cmpxchgb_weak(&self.0, false, true).is_ok() { break; }
				spin_loop();
			}
			__MutexGuard(&self.0, unsafe{core::mem::transmute(self.1.get())})
		}
		pub(crate) fn get_mut(&mut self) -> &mut T
		{
			unsafe{core::mem::transmute(self.1.get())}
		}
		pub(crate) fn into_inner(self) -> T
		{
			self.1.into_inner()
		}
		pub(crate) fn try_lock(&self) -> Option<__MutexGuard<T>>
		{
			cmpxchgb(&self.0, false, true).ok()?;
			Some(__MutexGuard(&self.0, unsafe{core::mem::transmute(self.1.get())}))
		}
	}

	impl<T:Sized> __RwLock<T>
	{
		pub(crate) fn new(val: T) -> __RwLock<T>
		{
			__RwLock(AtomicUsize::new(0), UnsafeCell::new(val))
		}
		pub(crate) fn read(&self) -> __RwLockReadGuard<T>
		{
			loop {
				let old = self.0.load(MOrd::Acquire);
				if old == !0 { continue; }	//Locked for write.
				if cmpxchg_weak(&self.0, old, old + 1).is_ok() { break; }
				spin_loop();
			}
			__RwLockReadGuard(&self.0, unsafe{core::mem::transmute(self.1.get())})
		}
		pub(crate) fn try_read(&self) -> Option<__RwLockReadGuard<T>>
		{
			loop {
				let old = self.0.load(MOrd::Acquire);
				if old == !0 { return None; }	//Locked for write.
				cmpxchg_weak(&self.0, old, old + 1).ok()?;
				break;
			}
			Some(__RwLockReadGuard(&self.0, unsafe{core::mem::transmute(self.1.get())}))
		}
		pub(crate) fn write(&self) -> __RwLockWriteGuard<T>
		{
			loop {
				if cmpxchg_weak(&self.0, 0, !0).is_ok()  { break; }
				spin_loop();
			}
			__RwLockWriteGuard(&self.0, unsafe{core::mem::transmute(self.1.get())})
		}
		pub(crate) fn try_write(&self) -> Option<__RwLockWriteGuard<T>>
		{
			cmpxchg(&self.0, 0, !0).ok()?;
			Some(__RwLockWriteGuard(&self.0, unsafe{core::mem::transmute(self.1.get())}))
		}
		pub(crate) fn get_mut(&mut self) -> &mut T
		{
			unsafe{core::mem::transmute(self.1.get())}
		}
		pub(crate) fn into_inner(self) -> T
		{
			self.1.into_inner()
		}
	}

	impl __Condvar
	{
		pub(crate) fn new() -> __Condvar
		{
			__Condvar(AtomicUsize::new(0))
		}
		pub(crate) fn wait<'a,T>(&self, guard: __MutexGuard<'a,T>) -> __MutexGuard<'a,T>
		{
			//Load the counter before dropping the lock to avoid races with notify.
			let counter = self.0.load(MOrd::Acquire);
			//Hack to temporarily drop the lock.
			guard.0.store(false, MOrd::Release);
			//Wait for counter to change.
			while counter == self.0.load(MOrd::Acquire) { spin_loop(); }
			//Wait to re-acquire the lock.
			loop {
				if cmpxchgb_weak(&guard.0, false, true).is_ok()  { break; }
				spin_loop();
			}
			guard
		}
		pub(crate) fn notify_all(&self) { self.0.fetch_add(1, MOrd::AcqRel); }
	}

	impl __Once
	{
		pub(crate) const fn new() -> __Once
		{
			__Once(AtomicUsize::new(0))
		}
		pub(crate) fn call_once<F>(&self, f: F) where F: FnOnce()
		{
			//Fastpath completed case.
			if self.0.load(MOrd::Acquire) > 1 { return; }
			let state = cmpxchg(&self.0, 0, 1).unwrap_or_else(identity);
			match state {
				0 => {
					//We were the first. Run the function and mark it done.
					f();
					self.0.store(2, MOrd::Release);
				},
				1 => {
					//Wait for existing execution to complete.
					while self.0.load(MOrd::Acquire) < 2 { spin_loop(); }
				},
				//Any other value means done.
				_ => ()
			}
		}
	}
}

#[cfg(test)]
mod test;
