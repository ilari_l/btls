use super::FieldT;

const BYTES: usize = 66;
const WORDS: usize = 17;
const DWORDS: usize = 33;

//b+3p, in order to absorb worst-case magnitude of -3x.
const B: [u32;17] = [
	0x6b503efd, 0xef451fd4, 0x3d2c34f1, 0x3573df88, 0x3bb1bf07, 0x1652c0bd, 0xec7e937b, 0x56193951,
	0x8ef109e1, 0xb8b48991, 0x99b315f3, 0xa2da725b, 0xb68540ee, 0x929a21a0, 0x8e1c9a1f, 0x953eb961,
	0x651
];

struct Field;

impl FieldT for Field
{
	type Element = [u32;WORDS];
	type DElement = [u32;DWORDS];
	type Store = [u8;BYTES];
	fn __read(x: <Self as FieldT>::Store) -> <Self as FieldT>::Element { impl_read!(B521 x) }
	fn __write(x: <Self as FieldT>::Element) -> <Self as FieldT>::Store { impl_write!(B521 x) }
	fn mul(a: <Self as FieldT>::Element, b: <Self as FieldT>::Element) -> <Self as FieldT>::Element
	{
		reduce_after_mul(__raw_multiply(a,b))
	}
	fn sqr(a: <Self as FieldT>::Element) -> <Self as FieldT>::Element
	{
		reduce_after_mul(__raw_square(a))
	}
	fn __sqrt(mut a: <Self as FieldT>::Element) -> <Self as FieldT>::Element
	{
		//Raise to power of 2^519
		for _ in 0..519 { a = Self::sqr(a); }
		a
	}
	fn __sign(a: <Self as FieldT>::Element) -> u8 { a[0] as u8 }
	fn __negate(a: &mut <Self as FieldT>::Element)
	{
		//Subtraction from 0xFFFFFFFF is the same as bitwise complement.
		for i in 0..16 { a[i] = !a[i]; }
		a[16] = 0x1FF - a[16];
	}
	fn __inrange(a: <Self as FieldT>::Element) -> bool
	{
		let mut overflowing = true;
		for i in 0..16 { overflowing &= a[i] == !0; }
		overflowing = if overflowing { a[16] >= 0x1FF } else { a[16] > 0x1FF };
		!overflowing
	}
	fn __recover_y2(x: <Self as FieldT>::Element) -> <Self as FieldT>::Element
	{
		//This can never produce carry.
		let (mut y2, _) = impl_recover_y2_part!(x B);
		//Add carry to partially reduce mod p.
		let mut carry = (y2[16] >> 9) as u64;
		y2[16] &= 0x1FF;
		for i in 0..17 {
			carry += y2[i] as u64;
			y2[i] = carry as u32;
			carry >>= 32;	//Here carry is always positive.
		}
		//>=p? If so, reduce once more.
		if y2[16] > 0x1FF || (y2[16] == 0x1FF && y2[..16].iter().fold(!0u32,|x,y|x&y) >= 0xFFFFFFFF) {
			//Carry ripple will always leave higher numbers at zero, only the lowest one can be
			//nonzero.
			y2[0] = y2[0].wrapping_add(y2[16] >> 9).wrapping_add(1);
			for i in 1..17 { y2[i] = 0; }
		}
		y2
	}
}

///Recover the y coordinate of NIST P-521 point, given x coordinate and the LSB of y coordinate.
///
///The x coordinate is given in big-endian form as the `x` parameter, and LSB of y coordinate is the LSB of the
///`ysign` parameter (all non-LSB bits of `ysign` are ignored).
///
///The returned y coordinate is given in big-endian form.
pub fn recover_y(x: [u8;BYTES], ysign: u8) -> Option<[u8;BYTES]> { Field::recover_y(x, ysign) }
///Check that given point is on the NIST P-521 curve.
///
///Both coordinates are given in big-endian form.
pub fn check_y(x: [u8;BYTES], y: [u8;BYTES]) -> Result<(),()> { Field::check_y(x, y) }

fn __raw_multiply(a: [u32;WORDS], b: [u32;WORDS]) -> [u64;DWORDS]
{
	let mut tmp = [0u64;DWORDS];
	for i in 0..WORDS { for j in 0..WORDS {
		let k = i+j;
		let r = a[i] as u64 * b[j] as u64;
		tmp[k+0] += r & 0xFFFFFFFF;
		if k >= 32 { return tmp; }	//Do not exceed bounds. Note multiply is 17x17 -> 33.
		tmp[k+1] += r >> 32;
	}}
	tmp
}

fn __raw_square(a: [u32;WORDS]) -> [u64;DWORDS]
{
	let mut tmp = [0u64;DWORDS];
	//Only loop to WORDS-1 as the last is special.
	for i in 0..WORDS-1 {
		let k = 2*i;
		let r = a[i] as u64 * a[i] as u64;
		tmp[k+0] += r & 0xFFFFFFFF;
		tmp[k+1] += r >> 32;
	}
	//The last can not have carry.
	tmp[DWORDS-1] += a[WORDS-1] as u64 * a[WORDS-1] as u64;
	for i in 0..WORDS { for j in 0..i {
		let k = i+j;
		let r = a[i] as u64 * a[j] as u64;
		//Double the number added. This can not overflow indices.
		tmp[k+0] += r << 1 & 0xFFFFFFFF;
		tmp[k+1] += r >> 31;
	}}
	tmp
}


fn reduce_after_add(mut t: [u64;17]) -> [u32;17]
{
	//First, push all high parts to next.
	let mut carry = 0;
	for i in 0..17 {
		let r = t[i] + carry;
		t[i] = r & 0xFFFFFFFF;
		carry = r >> 32;
	}
	//Split the excess. Note that there might be carry, which needs to be taken at weight 2^23.
	carry = carry << 23 | t[16] >> 9;
	t[16] &= 0x1FF;
	//Sum again.
	for i in 0..17 {
		let r = t[i] + carry;
		t[i] = r & 0xFFFFFFFF;
		carry = r >> 32;
	}
	//Overflow?
	if t[16] > 0x1FF || (t[16] == 0x1FF && t[..16].iter().fold(!0u64,|x,y|x&y) >= 0xFFFFFFFF) {
		carry = 1;
		for i in 0..17 {
			let r = t[i] + carry;
			t[i] = r & 0xFFFFFFFF;
			carry = r >> 32;
		}
	}
	t[16] &= 0x1FF;
	let mut r = [0;17];
	for i in 0..17 { r[i] = t[i] as u32 }
	r
}

fn reduce_after_mul(t: [u64;33]) -> [u32;17]
{
	//Disable overflow checks.
	let mut s = [0;17];
	for i in 0..16 { s[i] = t[i] + 8388608 * t[i+17]; }
	s[16] = t[16];
	reduce_after_add(s)
}

#[test]
fn test_key_decode()
{
	let x = [
		0x00,0xc6,
		0x85,0x8e,0x06,0xb7,0x04,0x04,0xe9,0xcd,0x9e,0x3e,0xcb,0x66,0x23,0x95,0xb4,0x42,
		0x9c,0x64,0x81,0x39,0x05,0x3f,0xb5,0x21,0xf8,0x28,0xaf,0x60,0x6b,0x4d,0x3d,0xba,
		0xa1,0x4b,0x5e,0x77,0xef,0xe7,0x59,0x28,0xfe,0x1d,0xc1,0x27,0xa2,0xff,0xa8,0xde,
		0x33,0x48,0xb3,0xc1,0x85,0x6a,0x42,0x9b,0xf9,0x7e,0x7e,0x31,0xc2,0xe5,0xbd,0x66,
	];
	let y = [
		0x01,0x18,
		0x39,0x29,0x6a,0x78,0x9a,0x3b,0xc0,0x04,0x5c,0x8a,0x5f,0xb4,0x2c,0x7d,0x1b,0xd9,
		0x98,0xf5,0x44,0x49,0x57,0x9b,0x44,0x68,0x17,0xaf,0xbd,0x17,0x27,0x3e,0x66,0x2c,
		0x97,0xee,0x72,0x99,0x5e,0xf4,0x26,0x40,0xc5,0x50,0xb9,0x01,0x3f,0xad,0x07,0x61,
		0x35,0x3c,0x70,0x86,0xa2,0x72,0xc2,0x40,0x88,0xbe,0x94,0x76,0x9f,0xd1,0x66,0x50,
	];
	let y2 = Field::recover_y(x, 0).expect("Square root failed");
	assert_eq!(&y[..], &y2[..]);
}

#[test]
fn p_not_in_range()
{
	assert!(!Field::__inrange([!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,0x1FF]));
}

#[test]
fn p_plus_1_not_in_range()
{
	assert!(!Field::__inrange([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0x200]));
}

#[test]
fn p_minus_delta_in_range()
{
	assert!(Field::__inrange([!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,0x1FE]));
}

#[test]
fn p_minus_1_in_range()
{
	assert!(Field::__inrange([!1,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,0x1FF]));
}

#[test]
fn sqrt_simple()
{
	let a = [4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	let b = [2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	let b2 = Field::__sqrt(a);
	assert_eq!(&b[..], &b2[..]);
}

#[test]
fn flipsign()
{
	let mut a = [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	let b = [!1,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,!0,0x1ff];
	Field::__negate(&mut a);
	assert_eq!(&a[..], &b[..]);
}
