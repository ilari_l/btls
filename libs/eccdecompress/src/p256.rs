use super::FieldT;
use core::num::Wrapping as W;

const BYTES: usize = 32;
const WORDS: usize = 8;
const DWORDS: usize = 16;

//b+3p, in order to absorb worst-case magnitude of -3x. The leading 3 is omitted.
const B: [u32;8] = [
	0x27D26048, 0x3BCE3C3E, 0xCC53B0F6, 0x651D06B3, 0x769886BC, 0xB3EBBD55, 0xAA3A93EA, 0x5AC635D5
];

const P: [u32;8] = [!0, !0, !0, 0, 0, 0, 1, !0];

macro_rules! sar32
{
	//Arithmetic shift.
	($x:ident) => { $x = (($x as i64) >> 32) as u64; }
}

macro_rules! chained_reduce_step
{
	($y2:ident $carry2:ident $carry:expr) => {{
		$carry2 = ($y2[0] as u64).wrapping_add($carry2).wrapping_add($carry);
		$y2[0] = $carry2 as u32; sar32!($carry2);
		$carry2 = ($y2[1] as u64).wrapping_add($carry2);
		$y2[1] = $carry2 as u32; sar32!($carry2);
		$carry2 = ($y2[2] as u64).wrapping_add($carry2);
		$y2[2] = $carry2 as u32; sar32!($carry2);
		$carry2 = ($y2[3] as u64).wrapping_add($carry2).wrapping_sub($carry);
		$y2[3] = $carry2 as u32; sar32!($carry2);
		$carry2 = ($y2[4] as u64).wrapping_add($carry2);
		$y2[4] = $carry2 as u32; sar32!($carry2);
		$carry2 = ($y2[5] as u64).wrapping_add($carry2);
		$y2[5] = $carry2 as u32; sar32!($carry2);
		$carry2 = ($y2[6] as u64).wrapping_add($carry2).wrapping_sub($carry);
		$y2[6] = $carry2 as u32; sar32!($carry2);
		$carry2 = ($y2[7] as u64).wrapping_add($carry2).wrapping_add($carry);
		$y2[7] = $carry2 as u32; sar32!($carry2);
	}}
}

struct Field;

impl FieldT for Field
{
	type Element = [u32;WORDS];
	type DElement = [u32;DWORDS];
	type Store = [u8;BYTES];
	fn __read(x: <Self as FieldT>::Store) -> <Self as FieldT>::Element { impl_read!(B256 x) }
	fn __write(x: <Self as FieldT>::Element) -> <Self as FieldT>::Store { impl_write!(B256 x) }
	fn mul(a: <Self as FieldT>::Element, b: <Self as FieldT>::Element) -> <Self as FieldT>::Element
	{
		reduce_after_mul(__raw_multiply(a,b))
	}
	fn sqr(a: <Self as FieldT>::Element) -> <Self as FieldT>::Element
	{
		reduce_after_mul(__raw_square(a))
	}
	fn __sqrt(mut a: <Self as FieldT>::Element) -> <Self as FieldT>::Element
	{
		//Raise to power of 2^254 - 2^222 + 2^190 + 2^94
		//a -> 2^94
		for _ in 0..94 { a = Self::sqr(a); }
		//b -> 2^190
		let mut b = a; for _ in 0..96 { b = Self::sqr(b); }
		//c -> 2^222
		let mut c = b; for _ in 0..32 { c = Self::sqr(c); }
		//d -> 2^254-2^222.
		let mut d = c;
		let mut e = d; for _ in 0..1 { e = Self::sqr(e); } d = Self::mul(d, e);
		let mut e = d; for _ in 0..2 { e = Self::sqr(e); } d = Self::mul(d, e);
		let mut e = d; for _ in 0..4 { e = Self::sqr(e); } d = Self::mul(d, e);
		let mut e = d; for _ in 0..8 { e = Self::sqr(e); } d = Self::mul(d, e);
		let mut e = d; for _ in 0..16 { e = Self::sqr(e); } d = Self::mul(d, e);
		//(2^254-2^222)+2^19+2^94 = 2^254 - 2^222 + 2^190 + 2^94
		Self::mul(d, Self::mul(b,a))
	}
	fn __sign(a: <Self as FieldT>::Element) -> u8 { a[0] as u8 }
	fn __negate(a: &mut <Self as FieldT>::Element)
	{
		//This can actually generate borrows.
		let mut carry = 0u64;
		for i in 0..8 {
			carry = carry.wrapping_add(P[i] as u64).wrapping_sub(a[i] as u64);
			a[i] = carry as u32;
			sar32!(carry);
		}
		let _ = carry;		//Shut up warning.
	}
	fn __inrange(a: <Self as FieldT>::Element) -> bool
	{
		let mut overflowing = true;
		for i in 0..8 { overflowing = if overflowing { a[i] >= P[i] } else { a[i] > P[i] }; }
		!overflowing
	}
	fn __recover_y2(x: <Self as FieldT>::Element) -> <Self as FieldT>::Element
	{
		let (mut y2, mut carry) = impl_recover_y2_part!(x B);
		//careful: carry can be negative here. It is guaranteed that carry is at least -3.
		carry = carry.wrapping_add(3);
		//each carry needs to be translated to X^7-X^6-X^3+1, where X=2^32.
		let mut carry2 = 0u64;
		chained_reduce_step!(y2 carry2 carry);
		//This can still leave carry2 or be out-of-range. 
		let c8 = carry2 > 0;
		let c7i = y2[7] < 0xFFFFFFFF;
		let c6 = y2[6] > 1;
		let c6i = y2[6] < 1;
		let c5 = (y2[5]|y2[4]|y2[3]) > 0;
		let c2i = (y2[2]&y2[1]&y2[0]) < 0xFFFFFFFF;
		if c8 || (!c7i && c6) || (!c7i && !c6i && c5) || (!c7i && !c6i && !c2i) {
			//Need another reduction. Carry is always 1.
			chained_reduce_step!(y2 carry2 1);
		}
		let _ = carry2;		//Shut up a warning.
		y2
	}
}

///Recover the y coordinate of NIST P-256 point, given x coordinate and the LSB of y coordinate.
///
///The x coordinate is given in big-endian form as the `x` parameter, and LSB of y coordinate is the LSB of the
///`ysign` parameter (all non-LSB bits of `ysign` are ignored).
///
///The returned y coordinate is given in big-endian form.
pub fn recover_y(x: [u8;BYTES], ysign: u8) -> Option<[u8;BYTES]> { Field::recover_y(x, ysign) }
///Check that given point is on the NIST P-256 curve.
///
///Both coordinates are given in big-endian form.
pub fn check_y(x: [u8;BYTES], y: [u8;BYTES]) -> Result<(),()> { Field::check_y(x, y) }
impl_normal_mul_sqr!(DWORDS, WORDS);


macro_rules! split_s
{
	($to:expr, $u:expr) => {{
		let u = $u;
		$to = u & W(0xFFFFFFFF);
		W(((u.0 as i64) >> 32) as u64)
	}}
}

fn reduce_after_add(mut t: [W<u64>;8], mut carry: W<u64>) -> [u32;8]
{
	let mut carry2;
	carry += t[7] >> 32;	//Shift to carry in order to avoid carry2 > 1.
	t[7] &= W(0xFFFFFFFF);
	carry2 = split_s!(t[0], t[0]+carry);
	carry2 = split_s!(t[1], t[1]+carry2);
	carry2 = split_s!(t[2], t[2]+carry2);
	carry2 = split_s!(t[3], t[3]+carry2-carry);
	carry2 = split_s!(t[4], t[4]+carry2);
	carry2 = split_s!(t[5], t[5]+carry2);
	carry2 = split_s!(t[6], t[6]+carry2-carry);
	carry2 = split_s!(t[7], t[7]+carry2+carry);
	let c8 = carry2 > W(0);
	let c7i = t[7] < W(0xFFFFFFFF);
	let c6 = t[6] > W(1);
	let c6i = t[6] < W(1);
	let c5 = (t[5]|t[4]|t[3]) > W(0);
	let c2i = (t[2]&t[1]&t[0]) < W(0xFFFFFFFF);
	if c8 || (!c7i && c6) || (!c7i && !c6i && c5) || (!c7i && !c6i && !c2i) {
		//Need another reduction.
		carry2 = split_s!(t[0], t[0]+W(1));
		carry2 = split_s!(t[1], t[1]+carry2);
		carry2 = split_s!(t[2], t[2]+carry2);
		carry2 = split_s!(t[3], t[3]+carry2-W(1));
		carry2 = split_s!(t[4], t[4]+carry2);
		carry2 = split_s!(t[5], t[5]+carry2);
		carry2 = split_s!(t[6], t[6]+carry2-W(1));
		split_s!(t[7], t[7]+carry2+W(1));
	}
	let mut r = [0;8];
	for i in 0..8 { r[i] = t[i].0 as u32 }
	r
}

fn reduce_after_mul(t: [u64;16]) -> [u32;8]
{
	//Disable overflow checks.
	let t = btls_aux_memory::make_array_wrapping(t);
	const X: W<u64> = W(0x500000000);
	let mut carry;
	let mut u = [W(0);8];
	carry = split_s!(u[0], X-W(5)+t[0]+t[8]+t[9]-t[11]-t[12]-t[13]-t[14]);
	carry = split_s!(u[1], X-W(5)+t[1]+t[9]+t[10]-t[12]-t[13]-t[14]-t[15]+carry);
	carry = split_s!(u[2], X-W(5)+t[2]+t[10]+t[11]-t[13]-t[14]-t[15]+carry);
	carry = split_s!(u[3], X+t[3]-t[8]-t[9]+W(2)*t[11]+W(2)*t[12]+t[13]-t[15]+carry);
	carry = split_s!(u[4], X-W(5)+t[4]-t[9]-t[10]+W(2)*t[12]+W(2)*t[13]+t[14]+carry);
	carry = split_s!(u[5], X-W(5)+t[5]-t[10]-t[11]+W(2)*t[13]+W(2)*t[14]+t[15]+carry);
	carry = split_s!(u[6], X+t[6]-t[8]-t[9]+t[13]+W(3)*t[14]+W(2)*t[15]+carry);
	carry = split_s!(u[7], X-W(10)+t[7]+t[8]-t[10]-t[11]-t[12]-t[13]+W(3)*t[15]+carry);
	reduce_after_add(u, carry)
}

#[test]
fn test_key_decode()
{
	let x = [
		0x6b,0x17,0xd1,0xf2,0xe1,0x2c,0x42,0x47,0xf8,0xbc,0xe6,0xe5,0x63,0xa4,0x40,0xf2,
		0x77,0x03,0x7d,0x81,0x2d,0xeb,0x33,0xa0,0xf4,0xa1,0x39,0x45,0xd8,0x98,0xc2,0x96,
	];
	let y = [
		0x4f,0xe3,0x42,0xe2,0xfe,0x1a,0x7f,0x9b,0x8e,0xe7,0xeb,0x4a,0x7c,0x0f,0x9e,0x16,
		0x2b,0xce,0x33,0x57,0x6b,0x31,0x5e,0xce,0xcb,0xb6,0x40,0x68,0x37,0xbf,0x51,0xf5,
	];
	let y2 = Field::recover_y(x, 1).expect("Square root failed");
	assert_eq!(&y[..], &y2[..]);
}

#[test]
fn p_not_in_range()
{
	assert!(!Field::__inrange(P));
}

#[test]
fn p_plus_1_not_in_range()
{
	assert!(!Field::__inrange([0, 0, 0, 1, 0, 0, 1, !0]));
}

#[test]
fn p_minus_1_in_range()
{
	assert!(Field::__inrange([!1, !0, !0, 0, 0, 0, 1, !0]));
}


#[test]
fn sqrt_simple()
{
	let a = [4,0,0,0,0,0,0,0];
	let b = [2,0,0,0,0,0,0,0];
	let b2 = Field::__sqrt(a);
	assert_eq!(&b[..], &b2[..]);
}

#[test]
fn flipsign()
{
	let mut a = [1,0,0,0,0,0,0,0];
	let b = [!1,!0,!0,0,0,0,1,!0];
	Field::__negate(&mut a);
	assert_eq!(&a[..], &b[..]);
}
