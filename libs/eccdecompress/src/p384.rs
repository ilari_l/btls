use super::FieldT;
use core::num::Wrapping as W;


const BYTES: usize = 48;
const WORDS: usize = 12;
const DWORDS: usize = 24;

//b+3p, in order to absorb worst-case magnitude of -3x. The leading 3 is omitted.
const B: [u32;12] = [
	0xD3EC2AEC, 0x2A85C8F0, 0x8A2ED19D, 0xC656398A, 0x50138757, 0x0314088F, 0xFE814112, 0x181D9C6E,
	0xE3F82D19, 0x988E056B, 0xE23EE7E4, 0xB3312FA7
];

const P: [u32;12] = [!0, 0, 0, !0, !1, !0, !0, !0, !0, !0, !0, !0];

macro_rules! sar32
{
	//Arithmetic shift.
	($x:ident) => { $x = (($x as i64) >> 32) as u64; }
}

macro_rules! chained_reduce_step
{
	($y2:ident $carry2:ident $carry:expr) => {{
		$carry2 = ($y2[0] as u64).wrapping_add($carry2).wrapping_add($carry);
		$y2[0] = $carry2 as u32; sar32!($carry2);
		$carry2 = ($y2[1] as u64).wrapping_add($carry2).wrapping_sub($carry);
		$y2[1] = $carry2 as u32; sar32!($carry2);
		$carry2 = ($y2[2] as u64).wrapping_add($carry2);
		$y2[2] = $carry2 as u32; sar32!($carry2);
		$carry2 = ($y2[3] as u64).wrapping_add($carry2).wrapping_add($carry);
		$y2[3] = $carry2 as u32; sar32!($carry2);
		$carry2 = ($y2[4] as u64).wrapping_add($carry2).wrapping_add($carry);
		$y2[4] = $carry2 as u32; sar32!($carry2);
		$carry2 = ($y2[5] as u64).wrapping_add($carry2);
		$y2[5] = $carry2 as u32; sar32!($carry2);
		$carry2 = ($y2[6] as u64).wrapping_add($carry2);
		$y2[6] = $carry2 as u32; sar32!($carry2);
		$carry2 = ($y2[7] as u64).wrapping_add($carry2);
		$y2[7] = $carry2 as u32; sar32!($carry2);
		$carry2 = ($y2[8] as u64).wrapping_add($carry2);
		$y2[8] = $carry2 as u32; sar32!($carry2);
		$carry2 = ($y2[9] as u64).wrapping_add($carry2);
		$y2[9] = $carry2 as u32; sar32!($carry2);
		$carry2 = ($y2[10] as u64).wrapping_add($carry2);
		$y2[10] = $carry2 as u32; sar32!($carry2);
		$carry2 = ($y2[11] as u64).wrapping_add($carry2);
		$y2[11] = $carry2 as u32; sar32!($carry2);
	}}
}

struct Field;

impl FieldT for Field
{
	type Element = [u32;WORDS];
	type DElement = [u32;DWORDS];
	type Store = [u8;BYTES];
	fn __read(x: <Self as FieldT>::Store) -> <Self as FieldT>::Element { impl_read!(B384 x) }
	fn __write(x: <Self as FieldT>::Element) -> <Self as FieldT>::Store { impl_write!(B384 x) }
	fn mul(a: <Self as FieldT>::Element, b: <Self as FieldT>::Element) -> <Self as FieldT>::Element
	{
		reduce_after_mul(__raw_multiply(a,b))
	}
	fn sqr(a: <Self as FieldT>::Element) -> <Self as FieldT>::Element
	{
		reduce_after_mul(__raw_square(a))
	}
/*
a=1;
for(i=0; i < 30; i++) { a = 2*a; }
b = a; for(i=0; i < 64; i++) { b = 2*b; }
d = b;
e = d; for(i=0; i < 1; i++) { e = 2*e; }; d = d+e;
e = d; for(i=0; i < 2; i++) { e = 2*e; }; d = d+e;
e = d; for(i=0; i < 4; i++) { e = 2*e; }; d = d+e;
e = d; for(i=0; i < 8; i++) { e = 2*e; }; d = d+e;
e = d; for(i=0; i < 16; i++) { e = 2*e; }; d = d+e;
c = 2*(d+b);
f = c;
e = c; for(i=0; i < 1; i++) { e = 2*e; }; c = c+e;
e = c; for(i=0; i < 2; i++) { e = 2*e; }; c = c+e;
e = c; for(i=0; i < 4; i++) { e = 2*e; }; c = c+e;
e = c; for(i=0; i < 8; i++) { e = 2*e; }; c = c+e;
c = 2*c+f;
f = c;
e = c; for(i=0; i < 17; i++) { e = 2*e; }; c = e+f;
e = c; for(i=0; i < 17; i++) { e = 2*e; }; c = e+f;
e = c; for(i=0; i < 17; i++) { e = 2*e; }; c = e+f;
e = c; for(i=0; i < 17; i++) { e = 2*e; }; c = e+f;
f = c;
e = c; for(i=0; i < 85; i++) { e = 2*e; }; c = e+f;
e = c; for(i=0; i < 85; i++) { e = 2*e; }; c = e+f;
a+c+d
*/
	fn __sqrt(mut a: <Self as FieldT>::Element) -> <Self as FieldT>::Element
	{
		//Raise to power of 2^382 - 2^126 - 2^94 + 2^30
		// = (2^382-2^127) + (2^126-2^94) + 2^30.
		//a -> 2^30
		for _ in 0..30 { a = Self::sqr(a); }
		//b -> 2^94
		let mut b = a; for _ in 0..64 { b = Self::sqr(b); }
		//d -> 2^126-2^94
		let mut d = b;
		let mut e = d; for _ in 0..1 { e = Self::sqr(e); } d = Self::mul(d, e);
		let mut e = d; for _ in 0..2 { e = Self::sqr(e); } d = Self::mul(d, e);
		let mut e = d; for _ in 0..4 { e = Self::sqr(e); } d = Self::mul(d, e);
		let mut e = d; for _ in 0..8 { e = Self::sqr(e); } d = Self::mul(d, e);
		let mut e = d; for _ in 0..16 { e = Self::sqr(e); } d = Self::mul(d, e);
		//c -> 2^127.
		let mut c = Self::sqr(Self::mul(d,b));
		let f = c;
		//2^127 -> 2^143-2^127.
		let mut e = c; for _ in 0..1 { e = Self::sqr(e); } c = Self::mul(c, e);
		let mut e = c; for _ in 0..2 { e = Self::sqr(e); } c = Self::mul(c, e);
		let mut e = c; for _ in 0..4 { e = Self::sqr(e); } c = Self::mul(c, e);
		let mut e = c; for _ in 0..8 { e = Self::sqr(e); } c = Self::mul(c, e);
		//2^143-2^127 -> 2^212-2^127.
		let mut c = Self::mul(Self::sqr(c), f);
		let f = c;
		let mut e = c; for _ in 0..17 { e = Self::sqr(e); } c = Self::mul(e, f);
		let mut e = c; for _ in 0..17 { e = Self::sqr(e); } c = Self::mul(e, f);
		let mut e = c; for _ in 0..17 { e = Self::sqr(e); } c = Self::mul(e, f);
		let mut e = c; for _ in 0..17 { e = Self::sqr(e); } c = Self::mul(e, f);
		//2^212-2^127 -> 2^382-2^127.
		let f = c;
		let mut e = c; for _ in 0..85 { e = Self::sqr(e); } c = Self::mul(e, f);
		let mut e = c; for _ in 0..85 { e = Self::sqr(e); } c = Self::mul(e, f);
		Self::mul(a,Self::mul(c,d))
	}
	fn __sign(a: <Self as FieldT>::Element) -> u8 { a[0] as u8 }
	fn __negate(a: &mut <Self as FieldT>::Element)
	{
		//This can actually generate borrows.
		let mut carry = 0u64;
		for i in 0..12 {
			carry = carry.wrapping_add(P[i] as u64).wrapping_sub(a[i] as u64);
			a[i] = carry as u32;
			sar32!(carry);
		}
		let _ = carry;		//Shut up warning.
	}
	fn __inrange(a: <Self as FieldT>::Element) -> bool
	{
		let mut overflowing = true;
		for i in 0..12 { overflowing = if overflowing { a[i] >= P[i] } else { a[i] > P[i] }; }
		!overflowing
	}
	fn __recover_y2(x: <Self as FieldT>::Element) -> <Self as FieldT>::Element
	{
		let (mut y2, mut carry) = impl_recover_y2_part!(x B);
		//careful: carry can be negative here. It is guaranteed that carry is at least -3.
		carry = carry.wrapping_add(3);
		//each carry needs to be translated to X^4+X^3-X^1+1, where X=2^32.
		let mut carry2 = 0u64;
		chained_reduce_step!(y2 carry2 carry);
		let c12 = carry2 > 0;
		let c11i = (y2[11]&y2[10]&y2[9]&y2[8]&y2[7]&y2[6]&y2[5]) < 0xFFFFFFFF;
		let c4i = y2[4] < 0xFFFFFFFE;
		let c4 = y2[4] > 0xFFFFFFFE;
		let c3i = y2[3] < 0xFFFFFFFF;
		let c2 = (y2[2]|y2[1]) > 0;
		let c0i = y2[0] < 0xFFFFFFFF;
		if c12 || (!c11i && c4) || (!c11i && !c4i && !c3i && c2) || (!c11i && !c4i && !c3i && !c0i) {
			//Need another reduction. Carry is always 1.
			chained_reduce_step!(y2 carry2 1);
		}
		let _ = carry2;		//Shut up a warning.
		y2
	}
}

///Recover the y coordinate of NIST P-384 point, given x coordinate and the LSB of y coordinate.
///
///The x coordinate is given in big-endian form as the `x` parameter, and LSB of y coordinate is the LSB of the
///`ysign` parameter (all non-LSB bits of `ysign` are ignored).
///
///The returned y coordinate is given in big-endian form.
pub fn recover_y(x: [u8;BYTES], ysign: u8) -> Option<[u8;BYTES]> { Field::recover_y(x, ysign) }
///Check that given point is on the NIST P-384 curve.
///
///Both coordinates are given in big-endian form.
pub fn check_y(x: [u8;BYTES], y: [u8;BYTES]) -> Result<(),()> { Field::check_y(x, y) }
impl_normal_mul_sqr!(DWORDS, WORDS);

macro_rules! split_s
{
	($to:expr, $u:expr) => {{
		let u = $u;
		$to = u & W(0xFFFFFFFF);
		W(((u.0 as i64) >> 32) as u64)
	}}
}

fn reduce_after_add(mut t: [W<u64>;12], mut carry: W<u64>) -> [u32;12]
{
	let mut carry2;
	carry += t[11] >> 32;	//Shift to carry in order to avoid carry2 > 1.
	t[11] &= W(0xFFFFFFFF);
	carry2 = split_s!(t[0], t[0]+carry);
	carry2 = split_s!(t[1], t[1]+carry2-carry);
	carry2 = split_s!(t[2], t[2]+carry2);
	carry2 = split_s!(t[3], t[3]+carry2+carry);
	carry2 = split_s!(t[4], t[4]+carry2+carry);
	carry2 = split_s!(t[5], t[5]+carry2);
	carry2 = split_s!(t[6], t[6]+carry2);
	carry2 = split_s!(t[7], t[7]+carry2);
	carry2 = split_s!(t[8], t[8]+carry2);
	carry2 = split_s!(t[9], t[9]+carry2);
	carry2 = split_s!(t[10], t[10]+carry2);
	carry2 = split_s!(t[11], t[11]+carry2);
	let c12 = carry2 > W(0);
	let c11i = (t[11]&t[10]&t[9]&t[8]&t[7]&t[6]&t[5]) < W(0xFFFFFFFF);
	let c4i = t[4] < W(0xFFFFFFFE);
	let c4 = t[4] > W(0xFFFFFFFE);
	let c3i = t[3] < W(0xFFFFFFFF);
	let c2 = (t[2]|t[1]) > W(0);
	let c0i = t[0] < W(0xFFFFFFFF);
	if c12 || (!c11i && c4) || (!c11i && !c4i && !c3i && c2) || (!c11i && !c4i && !c3i && !c0i) {
		//Need another reduction.
		carry2 = split_s!(t[0], t[0]+W(1));
		carry2 = split_s!(t[1], t[1]+carry2-W(1));
		carry2 = split_s!(t[2], t[2]+carry2);
		carry2 = split_s!(t[3], t[3]+carry2+W(1));
		carry2 = split_s!(t[4], t[4]+carry2+W(1));
		carry2 = split_s!(t[5], t[5]+carry2);
		carry2 = split_s!(t[6], t[6]+carry2);
		carry2 = split_s!(t[7], t[7]+carry2);
		carry2 = split_s!(t[8], t[8]+carry2);
		carry2 = split_s!(t[9], t[9]+carry2);
		carry2 = split_s!(t[10], t[10]+carry2);
		split_s!(t[11], t[11]+carry2);
	}
	let mut r = [0;12];
	for i in 0..12 { r[i] = t[i].0 as u32 }
	r
}

fn reduce_after_mul(t: [u64;24]) -> [u32;12]
{
	//Disable overflow checks.
	let t = btls_aux_memory::make_array_wrapping(t);
	const X: W<u64> = W(0x400000000);
	let mut carry;
	let mut u = [W(0);12];
	carry = split_s!(u[0], X-W(4)+t[0]+t[12]+t[20]+t[21]-t[23]);
	carry = split_s!(u[1], X+t[1]-t[12]+t[13]-t[20]+t[22]+t[23]+carry);
	carry = split_s!(u[2], X-W(4)+t[2]-t[13]+t[14]-t[21]+t[23]+carry);
	carry = split_s!(u[3], X-W(8)+t[3]+t[12]-t[14]+t[15]+t[20]+t[21]-t[22]-t[23]+carry);
	carry = split_s!(u[4], X-W(8)+t[4]+t[12]+t[13]-t[15]+t[16]+t[20]+W(2)*t[21]+t[22]-W(2)*t[23]+carry);
	carry = split_s!(u[5], X-W(4)+t[5]+t[13]+t[14]-t[16]+t[17]+t[21]+W(2)*t[22]+t[23]+carry);
	carry = split_s!(u[6], X-W(4)+t[6]+t[14]+t[15]-t[17]+t[18]+t[22]+W(2)*t[23]+carry);
	carry = split_s!(u[7], X-W(4)+t[7]+t[15]+t[16]-t[18]+t[19]+t[23]+carry);
	carry = split_s!(u[8], X-W(4)+t[8]+t[16]+t[17]-t[19]+t[20]+carry);
	carry = split_s!(u[9], X-W(4)+t[9]+t[17]+t[18]-t[20]+t[21]+carry);
	carry = split_s!(u[10], X-W(4)+t[10]+t[18]+t[19]-t[21]+t[22]+carry);
	carry = split_s!(u[11], X-W(4)+t[11]+t[19]+t[20]-t[22]+t[23]+carry);
	reduce_after_add(u, carry)
}

#[test]
fn test_key_decode()
{
	let x = [
		0xaa,0x87,0xca,0x22,0xbe,0x8b,0x05,0x37,0x8e,0xb1,0xc7,0x1e,0xf3,0x20,0xad,0x74,
		0x6e,0x1d,0x3b,0x62,0x8b,0xa7,0x9b,0x98,0x59,0xf7,0x41,0xe0,0x82,0x54,0x2a,0x38,
		0x55,0x02,0xf2,0x5d,0xbf,0x55,0x29,0x6c,0x3a,0x54,0x5e,0x38,0x72,0x76,0x0a,0xb7,
	];
	let y = [
		0x36,0x17,0xde,0x4a,0x96,0x26,0x2c,0x6f,0x5d,0x9e,0x98,0xbf,0x92,0x92,0xdc,0x29,
		0xf8,0xf4,0x1d,0xbd,0x28,0x9a,0x14,0x7c,0xe9,0xda,0x31,0x13,0xb5,0xf0,0xb8,0xc0,
		0x0a,0x60,0xb1,0xce,0x1d,0x7e,0x81,0x9d,0x7a,0x43,0x1d,0x7c,0x90,0xea,0x0e,0x5f,
	];
	let y2 = Field::recover_y(x, 1).expect("Square root failed");
	assert_eq!(&y[..], &y2[..]);
}

#[test]
fn p_not_in_range()
{
	assert!(!Field::__inrange(P));
}

#[test]
fn p_plus_1_not_in_range()
{
	assert!(!Field::__inrange([0, 1, 0, !0, !1, !0, !0, !0, !0, !0, !0, !0]));
}

#[test]
fn p_minus_1_in_range()
{
	assert!(Field::__inrange([!1, 0, 0, !0, !1, !0, !0, !0, !0, !0, !0, !0]));
}


#[test]
fn sqrt_simple()
{
	let a = [4,0,0,0,0,0,0,0,0,0,0,0];
	let b = [2,0,0,0,0,0,0,0,0,0,0,0];
	let b2 = Field::__sqrt(a);
	assert_eq!(&b[..], &b2[..]);
}

#[test]
fn flipsign()
{
	let mut a = [1,0,0,0,0,0,0,0,0,0,0,0];
	let b = [!1,0,0,!0,!1,!0,!0,!0,!0,!0,!0,!0];
	Field::__negate(&mut a);
	assert_eq!(&a[..], &b[..]);
}
