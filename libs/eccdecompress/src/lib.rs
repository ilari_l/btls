//!Routines to recover y coordinates for elliptic curves given x coordinate and sign (least bit) of y coordinate.
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]
macro_rules! __words_store
{
	(B256) => { 8 };
	(B384) => { 12 };
	(B521) => { 17 };
}

macro_rules! __words_ordinary
{
	(B256) => { 8 };
	(B384) => { 12 };
	(B521) => { 16 };
}

macro_rules! __shift
{
	(B256) => { 0 };
	(B384) => { 0 };
	(B521) => { 2 };
}

macro_rules! __eread
{
	(B256 $out:ident $w:ident $x:ident) => {  };
	(B384 $out:ident $w:ident $x:ident) => {  };
	(B521 $out:ident $w:ident $x:ident) => { $out[$w] = $x[0] as u32 * 256 + $x[1] as u32 };
}

macro_rules! __ewrite
{
	(B256 $out:ident $w:ident $y:ident) => {  };
	(B384 $out:ident $w:ident $y:ident) => {  };
	(B521 $out:ident $w:ident $y:ident) => {{
		$out[0] = ($y[$w] >> 8) as u8;
		$out[1] = $y[$w] as u8;
	}};
}

macro_rules! impl_read
{
	($b:ident $x:ident) => {{
		const W: usize = __words_ordinary!($b);
		const S: usize = __shift!($b);
		let mut out = [0;__words_store!($b)];
		for i in 0..W {
			let mut t = [0;4];
			t.copy_from_slice(&$x[4*W-4*i-4+S..4*W-4*i+S]);
			out[i] = u32::from_be_bytes(t);
		}
		__eread!($b out W $x);
		out
	}}
}

macro_rules! impl_write
{
	($b:ident $y:ident) => {{
		const W: usize = __words_ordinary!($b);
		const S: usize = __shift!($b);
		let mut out = [0;4*W+S];
		for i in 0..W { (&mut out[4*W-4*i-4+S..4*W-4*i+S]).copy_from_slice(&u32::to_be_bytes($y[i])); }
		__ewrite!($b out W $y);
		out
	}}
}

macro_rules! impl_normal_mul_sqr
{
	($w:expr, $ws:expr) => {
		fn __raw_multiply(a: [u32;$ws], b: [u32;$ws]) -> [u64;$w]
		{
			let mut tmp = [0u64;$w];
			for i in 0..$ws { for j in 0..$ws {
				let k = i+j;
				let r = a[i] as u64 * b[j] as u64;
				tmp[k+0] += r & 0xFFFFFFFF;
				tmp[k+1] += r >> 32;
			}}
			tmp
		}
		fn __raw_square(a: [u32;$ws]) -> [u64;$w]
		{
			let mut tmp = [0u64;$w];
			for i in 0..$ws {
				let k = 2*i;
				let r = a[i] as u64 * a[i] as u64;
				tmp[k+0] += r & 0xFFFFFFFF;
				tmp[k+1] += r >> 32;
			}
			for i in 0..$ws { for j in 0..i {
				let k = i+j;
				let r = a[i] as u64 * a[j] as u64;
				//Double the number added.
				tmp[k+0] += r << 1 & 0xFFFFFFFF;
				tmp[k+1] += r >> 31;
			}}
			tmp
		}
	}
}

macro_rules! impl_recover_y2_part
{
	($x:ident $b:ident) => {{
		let mut y2 = Self::mul($x, Self::sqr($x));
		let mut carry = 0u64;
		for i in 0..$b.len() {
			//Do not perform overflow checks, the last term can actually overflow subtraction, but 
			//wraparound is the correct thing to do in that case.
			carry = carry.wrapping_add(y2[i] as u64).wrapping_add($b[i] as u64).
				wrapping_sub(3 * $x[i] as u64);
			y2[i] = carry as u32;
			//Perform arithmetic shift, as carry can be negative to indicate borrow.
			carry = (carry as i64 >> 32) as u64;
		}
		(y2, carry)
	}}
}

trait FieldT
{
	type Element: Copy+Sized+Eq;
	type DElement: Copy+Sized;
	type Store: Copy+Sized;
	//Read field element.
	fn __read(x: <Self as FieldT>::Store) -> <Self as FieldT>::Element;
	//Write field element.
	fn __write(x: <Self as FieldT>::Element) -> <Self as FieldT>::Store;
	//Candidate square root.
	fn __sqrt(a: <Self as FieldT>::Element) -> <Self as FieldT>::Element;
	//Sign.
	fn __sign(a: <Self as FieldT>::Element) -> u8;
	//Negate.
	fn __negate(a: &mut <Self as FieldT>::Element);
	//Recover square of y.
	fn __recover_y2(x: <Self as FieldT>::Element) -> <Self as FieldT>::Element;
	//Check field element is in range.
	fn __inrange(x: <Self as FieldT>::Element) -> bool;
	//Multiply.
	fn mul(a: <Self as FieldT>::Element, b: <Self as FieldT>::Element) -> <Self as FieldT>::Element;
	//Square.
	fn sqr(a: <Self as FieldT>::Element) -> <Self as FieldT>::Element;
	fn recover_y(x: <Self as FieldT>::Store, ysign: u8) -> Option<<Self as FieldT>::Store>
	{
		let x = Self::__read(x);
		if !Self::__inrange(x) { return None; }
		let y2 = Self::__recover_y2(x);
		let mut y = Self::__sqrt(y2);
		let yc = Self::sqr(y);
		if yc != y2 { return None; }	//No square root.
		//Fix sign if needed.
		if Self::__sign(y) & 1 != ysign & 1 { Self::__negate(&mut y); }
		Some(Self::__write(y))
	}
	fn check_y(x: <Self as FieldT>::Store, y: <Self as FieldT>::Store) -> Result<(), ()>
	{
		let x = Self::__read(x);
		let y = Self::__read(y);
		if !Self::__inrange(x) || !Self::__inrange(y) { return Err(()); }
		let y2 = Self::__recover_y2(x);
		let yc = Self::sqr(y);
		if yc != y2 { return Err(()); }		//Does not satisfy the curve equation.
		Ok(())
	}
}

mod p256;
mod p384;
mod p521;
pub use crate::p256::recover_y as recover_y_nist_p256;
pub use crate::p256::check_y as check_point_nist_p256;
pub use crate::p384::recover_y as recover_y_nist_p384;
pub use crate::p384::check_y as check_point_nist_p384;
pub use crate::p521::recover_y as recover_y_nist_p521;
pub use crate::p521::check_y as check_point_nist_p521;
