use btls_aux_codegen::CodeBlock as TokenStream;
use btls_aux_codegen::mkid;
use btls_aux_codegen::quote;
use btls_aux_codegen::qwrite;
use btls_aux_rope3::FragmentWriteOps;
use btls_aux_rope3::rope3;
use btls_aux_oid::encode_oid_to_bytes as _encode_oid_to_bytes;
use std::env;
use std::fs::File;
use std::path::Path;

//If set, make the item const instead of static.
const F_CONST: u32 = 1;
//If set, include the 06 <length> prefix.
const F_HDR: u32 = 2;
//If set, include 05 00 trailer.
const F_TNULL: u32 = 4;
//If set, also define foo_I constant with no header no trailer.
const F_INNER: u32 = 8;
//If set, define foo_FULL with these values, and foo with no header nor trailer.
const F_FULL: u32 = 16;

fn encode_oid_to_bytes(textual: &str, flags: u32) -> Vec<u8>
{
	let mut result = _encode_oid_to_bytes(textual).expect("Encoding OID failed");
	//F_HDR adds OID header.
	if flags & F_HDR != 0 {
		//Assume that OIDs are at most 127 bytes, so the OID length fits into one byte.
		if result.len() > 127 { panic!(">127 byte OIDs are not implemented!"); }
		let olen = result.len() as u8;
		result.insert(0, 6);
		result.insert(1, olen);
	}
	//F_TNULL adds trailing NULL.
	if flags & F_TNULL != 0 {
		result.push(5);
		result.push(0);
	}
	result
}

fn write_as_array(name: &str, data: &[u8], constant: bool, help: &str) -> TokenStream
{
	let ikind = if constant { quote!(const) } else { quote!(static) };
	let name = mkid(name);
	let datalen = data.len();
	quote!(#[doc=#help] pub #ikind #name: [u8; #datalen] = [#(#data),*];)
}

fn write_oid_fp(fp: &mut File, name: &str, oid: &str, flags: u32)
{
	//If F_INNER is set, version of OID with no header nor NULL is also written as foo_I.
	if flags & F_INNER != 0 {
		//Write the inner version.
		write_oid_fp(fp, &format!("{name}_I"), oid, 0);
	}
	//If F_FULL is set, two versions are written: Inner version as foo, and whavever was specified (minus the
	//F_FULL flag to avoid recursion) as foo_FULL. This will obviously do strange stuff if you specify both
	//F_FULL and F_INNER.
	if flags & F_FULL != 0 {
		//Write the inner and full version.
		write_oid_fp(fp, name, oid, 0);
		write_oid_fp(fp, &format!("{name}_FULL"), oid, flags & !F_FULL);
		return;
	}
	let e = encode_oid_to_bytes(oid, flags);

	//Hodgepodge together the doc comment for the constant.
	let mut extra = String::new();
	extra = extra + (if flags & F_HDR != 0 { "including header" } else { "no header" });
	if flags & F_TNULL != 0 { extra = extra + ", including NULL"; }
	let help = format!("OID {oid} ({extra})\n");
	//If F_CONST is specified, the item is a constant, otherwise it is a static.
	qwrite(fp, write_as_array(name, &e, flags & F_CONST != 0, &help));
}

fn write_rsapss_block(fp: &mut File, symbol: &str, help: &str, hashoid: &str, bits: usize, pointless_null: bool)
{
	let tmp_rsapss_oid = encode_oid_to_bytes(RSA_PSS_OID, 0);
	let tmp_hash_oid = encode_oid_to_bytes(hashoid, 0);
	let tmp_mgf1_oid = encode_oid_to_bytes(MGF1_OID, 0);

	let block = rope3!(
		OBJECT_IDENTIFIER tmp_rsapss_oid,
		SEQUENCE(
			[0](SEQUENCE(
				OBJECT_IDENTIFIER {&tmp_hash_oid},
				if pointless_null NULL
			)),
			[1](SEQUENCE(
				OBJECT_IDENTIFIER tmp_mgf1_oid,
				SEQUENCE(
					OBJECT_IDENTIFIER {&tmp_hash_oid},
					if pointless_null NULL
				)
			)),
			//Assume bits is less than 1024, as all interesting hash functions are in that range.
			[2](INTEGER {(bits >> 3) as u8})
		)
	);

	//Ok, dump it.
	let output = FragmentWriteOps::to_vector(&block);
	let help = format!("The canonical parameters block for RSA-PSS with {help}.\n");
	let symbol = mkid(symbol);
	let dlen = output.len();
	qwrite(fp, quote!(#[doc=#help] pub static #symbol: &'static [u8;#dlen] = &[#(#output),*];));
}

static RSA_PSS_OID: &'static str = "1.2.840.113549.1.1.10";
static SHA256_OID: &'static str = "2.16.840.1.101.3.4.2.1";
static SHA384_OID: &'static str = "2.16.840.1.101.3.4.2.2";
static SHA512_OID: &'static str = "2.16.840.1.101.3.4.2.3";
static SHA3_256_OID: &'static str = "2.16.840.1.101.3.4.2.8";
static SHA3_384_OID: &'static str = "2.16.840.1.101.3.4.2.9";
static SHA3_512_OID: &'static str = "2.16.840.1.101.3.4.2.10";
static MGF1_OID: &'static str = "1.2.840.113549.1.1.8";

fn write_ekus(fp: &mut File)
{
	write_oid_fp(fp, "X509_EKU_TLS_SERVER", "1.3.6.1.5.5.7.3.1", 0);
	write_oid_fp(fp, "X509_EKU_TLS_CLIENT", "1.3.6.1.5.5.7.3.2", 0);
	write_oid_fp(fp, "X509_EKU_ANY", "2.5.29.37.0", 0);
	write_oid_fp(fp, "X509_EKU_OCSP", "1.3.6.1.5.5.7.3.9", 0);
}

fn write_exts(fp: &mut File)
{
	write_oid_fp(fp, "X509_EXT_TLS_FEATURE", "1.3.6.1.5.5.7.1.24", 0);
	write_oid_fp(fp, "X509_EXT_SCT", "1.3.6.1.4.1.11129.2.4.2", 0);
	write_oid_fp(fp, "X509_EXT_EKU", "2.5.29.37", 0);
	write_oid_fp(fp, "X509_EXT_BASIC_CONSTRAINTS", "2.5.29.19", 0);
	write_oid_fp(fp, "X509_EXT_KEY_USAGE", "2.5.29.15", 0);
	write_oid_fp(fp, "X509_EXT_SAN", "2.5.29.17", 0);
	write_oid_fp(fp, "X509_EXT_NAME_CONSTRAINTS", "2.5.29.30", 0);
	write_oid_fp(fp, "X509_EXT_AIA", "1.3.6.1.5.5.7.1.1", 0);
	write_oid_fp(fp, "X509_EXT_H_CT_V1", "1.3.6.1.4.1.11129.2.4.2", F_HDR|F_INNER);
	write_oid_fp(fp, "X509_EXT_H_CT_V2", "1.3.101.75", F_HDR|F_INNER);
	write_oid_fp(fp, "X509_EXT_QC_STATEMENTS", "1.3.6.1.5.5.7.1.3", 0);
	write_oid_fp(fp, "X509_EXT_POLICY_MAPPINGS", "2.5.29.33", 0);
	write_oid_fp(fp, "X509_EXT_POLICY_CONSTRAINTS", "2.5.29.36", 0);
	write_oid_fp(fp, "X509_EXT_INHIBIT_ANY_POLICY", "2.5.29.54", 0);
	write_oid_fp(fp, "X509_EXT_ACME_ALPN", "1.3.6.1.5.5.7.1.31", 0);
}

fn write_subjects(fp: &mut File)
{
	write_oid_fp(fp, "X509_SUBJECT_COMMON_NAME", "2.5.4.3", 0);
	write_oid_fp(fp, "X509_SUBJECT_SURNAME", "2.5.4.4", 0);
	write_oid_fp(fp, "X509_SUBJECT_SERIAL_NUMBER", "2.5.4.5", 0);
	write_oid_fp(fp, "X509_SUBJECT_COUNTRY_NAME", "2.5.4.6", 0);
	write_oid_fp(fp, "X509_SUBJECT_LOCALITY_NAME", "2.5.4.7", 0);
	write_oid_fp(fp, "X509_SUBJECT_STATE_OR_PROVINCE_NAME", "2.5.4.8", 0);
	write_oid_fp(fp, "X509_SUBJECT_STREET_ADDRESS", "2.5.4.9", 0);
	write_oid_fp(fp, "X509_SUBJECT_ORGANIZATION_NAME", "2.5.4.10", 0);
	write_oid_fp(fp, "X509_SUBJECT_ORGANIZATIONAL_UNIT_NAME", "2.5.4.11", 0);
	write_oid_fp(fp, "X509_SUBJECT_TITLE", "2.5.4.12", 0);
	write_oid_fp(fp, "X509_SUBJECT_NAME", "2.5.4.41", 0);
	write_oid_fp(fp, "X509_SUBJECT_GIVEN_NAME", "2.5.4.42", 0);
	write_oid_fp(fp, "X509_SUBJECT_INITIALS", "2.5.4.43", 0);
	write_oid_fp(fp, "X509_SUBJECT_GENERATION_QUALIFIER", "2.5.4.44", 0);
	write_oid_fp(fp, "X509_SUBJECT_DN_QUALIFIER", "2.5.4.46", 0);
	write_oid_fp(fp, "X509_SUBJECT_PSEUDONYM", "2.5.4.65", 0);
	write_oid_fp(fp, "X509_SUBJECT_DOMAIN_COMPONENT", "0.9.2342.19200300.100.1.25", 0);
	write_oid_fp(fp, "X509_SUBJECT_UID", "0.9.2342.19200300.100.1.1", 0);
}

fn write_misc_exts(fp: &mut File)
{
	write_oid_fp(fp, "X509_CSR_EXT_REQ", "1.2.840.113549.1.9.14", 0);
	write_oid_fp(fp, "X509_OCSP_SCT", "1.3.6.1.4.1.11129.2.4.5", 0);
	write_oid_fp(fp, "X509_OCSP_BASIC_RESPONSE", "1.3.6.1.5.5.7.48.1.1", 0);
	write_oid_fp(fp, "X509_AIA_OCSP", "1.3.6.1.5.5.7.48.1", 0);
	write_oid_fp(fp, "X509_AIA_ISSUER", "1.3.6.1.5.5.7.48.2", 0);
}

fn write_signatures(fp: &mut File)
{
	write_oid_fp(fp, "ECDSA_SHA256_OID", "1.2.840.10045.4.3.2", F_HDR|F_INNER);
	write_oid_fp(fp, "ECDSA_SHA384_OID", "1.2.840.10045.4.3.3", F_HDR|F_INNER);
	write_oid_fp(fp, "ECDSA_SHA512_OID", "1.2.840.10045.4.3.4", F_HDR|F_INNER);
	write_oid_fp(fp, "RSA_PKCS1_SHA256_OID", "1.2.840.113549.1.1.11", F_HDR|F_TNULL|F_INNER);
	write_oid_fp(fp, "RSA_PKCS1_SHA384_OID", "1.2.840.113549.1.1.12", F_HDR|F_TNULL|F_INNER);
	write_oid_fp(fp, "RSA_PKCS1_SHA512_OID", "1.2.840.113549.1.1.13", F_HDR|F_TNULL|F_INNER);
	write_oid_fp(fp, "RSA_PKCS1_SHA3_256_OID", "2.16.840.1.101.3.4.3.14", F_HDR|F_TNULL|F_INNER);
	write_oid_fp(fp, "RSA_PKCS1_SHA3_384_OID", "2.16.840.1.101.3.4.3.15", F_HDR|F_TNULL|F_INNER);
	write_oid_fp(fp, "RSA_PKCS1_SHA3_512_OID", "2.16.840.1.101.3.4.3.16", F_HDR|F_TNULL|F_INNER);
	write_oid_fp(fp, "ED25519_OID", "1.3.101.112", F_HDR|F_INNER);
	write_oid_fp(fp, "ED448_OID", "1.3.101.113", F_HDR|F_INNER);
	write_oid_fp(fp, "RSA_PKCS1_SHA1_OID_INSECURE_I", "1.2.840.113549.1.1.5", 0);
}

fn write_keys(fp: &mut File)
{
	write_oid_fp(fp, "MGF1_OID_I", MGF1_OID, 0);
	write_oid_fp(fp, "RSA_PSS_OID_I", RSA_PSS_OID, 0);
	write_oid_fp(fp, "ED25519_KEY_OID", "1.3.101.112", 0);
	write_oid_fp(fp, "ED448_KEY_OID", "1.3.101.113", 0);
	write_oid_fp(fp, "RSA_KEY_OID", "1.2.840.113549.1.1.1", 0);
	write_oid_fp(fp, "RSA_PSS_KEY_OID", RSA_PSS_OID, 0);
	write_oid_fp(fp, "ECDSA_KEY_OID", "1.2.840.10045.2.1", 0);
	write_oid_fp(fp, "CURVE_NSA_P256", "1.2.840.10045.3.1.7", 0);
	write_oid_fp(fp, "CURVE_NSA_P384", "1.3.132.0.34", 0);
	write_oid_fp(fp, "CURVE_NSA_P521", "1.3.132.0.35", 0);
}

fn write_hashes(fp: &mut File)
{
	write_oid_fp(fp, "SHA1_INSECURE_OID", "1.3.14.3.2.26", 0);
	write_oid_fp(fp, "SHA256_OID", SHA256_OID, F_HDR|F_TNULL|F_FULL);
	write_oid_fp(fp, "SHA384_OID", SHA384_OID, F_HDR|F_TNULL|F_FULL);
	write_oid_fp(fp, "SHA512_OID", SHA512_OID, F_HDR|F_TNULL|F_FULL);
	write_oid_fp(fp, "SHA3_256_OID", SHA3_256_OID, F_HDR|F_FULL);
	write_oid_fp(fp, "SHA3_384_OID", SHA3_384_OID, F_HDR|F_FULL);
	write_oid_fp(fp, "SHA3_512_OID", SHA3_512_OID, F_HDR|F_FULL);
}

fn write_rsapss(fp: &mut File)
{
	write_rsapss_block(fp, "RSA_PSS_SHA256_OID", "SHA-256", SHA256_OID, 256, true);
	write_rsapss_block(fp, "RSA_PSS_SHA384_OID", "SHA-384", SHA384_OID, 384, true);
	write_rsapss_block(fp, "RSA_PSS_SHA512_OID", "SHA-512", SHA512_OID, 512, true);
	write_rsapss_block(fp, "RSA_PSS_SHA3_256_OID", "SHA3-256", SHA3_256_OID, 256, false);
	write_rsapss_block(fp, "RSA_PSS_SHA3_384_OID", "SHA3-384", SHA3_384_OID, 384, false);
	write_rsapss_block(fp, "RSA_PSS_SHA3_512_OID", "SHA3-512", SHA3_512_OID, 512, false);
}

fn main()
{
	let out_dir = env::var("OUT_DIR").unwrap();
	let dest_path = Path::new(&out_dir).join("autogenerated.inc.rs");
	let mut f = File::create(&dest_path).unwrap();
	write_ekus(&mut f);
	write_exts(&mut f);
	write_hashes(&mut f);
	write_keys(&mut f);
	write_misc_exts(&mut f);
	write_signatures(&mut f);
	write_subjects(&mut f);
	write_rsapss(&mut f);

	println!("cargo:rerun-if-changed=build.rs");	//This has no external deps to rebuild for.
}
