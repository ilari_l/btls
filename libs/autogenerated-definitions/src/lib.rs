//!Definitions of binary representations of various OIDs.
//!
//!This crate contains binary representations of various OIDs (Object IDentifiers) that come accross in PKIX
//!certificates, compiled from the textual representations of said OIDs.
//!
//!Some of the constants lack headers, only containing the value part of OBJECT IDENTIFIER tag-length-value. Others
//!have the tag and length parts as well. And yet others in addition to tag and length fields also have a trailing
//!NULL tag-length-value.
#![allow(dead_code)]
#![forbid(unsafe_code)]
#![forbid(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]
include!(concat!(env!("OUT_DIR"), "/autogenerated.inc.rs"));

#[cfg(test)]
mod test;
