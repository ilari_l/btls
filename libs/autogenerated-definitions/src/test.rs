use super::RSA_PSS_SHA256_OID;
use super::RSA_PSS_SHA384_OID;
use super::RSA_PSS_SHA3_256_OID;
use super::RSA_PSS_SHA3_384_OID;
use super::RSA_PSS_SHA3_512_OID;
use super::RSA_PSS_SHA512_OID;
use super::CURVE_NSA_P521;
use super::ECDSA_SHA256_OID;
use super::RSA_PKCS1_SHA384_OID;

//Golden master tests
#[test]
fn rsa_pss_sha256_oid_ok()
{
	static REF: &'static [u8; 65] = include_bytes!("rsa-pss-sha2-256.hdr");
	assert_eq!(&RSA_PSS_SHA256_OID[..], &REF[..]);
}

#[test]
fn rsa_pss_sha384_oid_ok()
{
	static REF: &'static [u8; 65] = include_bytes!("rsa-pss-sha2-384.hdr");
	assert_eq!(&RSA_PSS_SHA384_OID[..], &REF[..]);
}

#[test]
fn rsa_pss_sha512_oid_ok()
{
	static REF: &'static [u8; 65] = include_bytes!("rsa-pss-sha2-512.hdr");
	assert_eq!(&RSA_PSS_SHA512_OID[..], &REF[..]);
}

#[test]
fn rsa_pss_sha3_256_oid_ok()
{
	static REF: &'static [u8; 61] = include_bytes!("rsa-pss-sha3-256.hdr");
	assert_eq!(&RSA_PSS_SHA3_256_OID[..], &REF[..]);
}

#[test]
fn rsa_pss_sha3_384_oid_ok()
{
	static REF: &'static [u8; 61] = include_bytes!("rsa-pss-sha3-384.hdr");
	assert_eq!(&RSA_PSS_SHA3_384_OID[..], &REF[..]);
}

#[test]
fn rsa_pss_sha3_512_oid_ok()
{
	static REF: &'static [u8; 61] = include_bytes!("rsa-pss-sha3-512.hdr");
	assert_eq!(&RSA_PSS_SHA3_512_OID[..], &REF[..]);
}

#[test]
fn test_some_oids()
{
	assert_eq!(CURVE_NSA_P521, &[43, 129, 4, 0, 35][..]);
	assert_eq!(ECDSA_SHA256_OID, &[6, 8, 42, 134, 72, 206, 61, 4, 3, 2][..]);
	assert_eq!(RSA_PKCS1_SHA384_OID, &[6, 9, 42, 134, 72, 134, 247, 13, 1, 1, 12, 5, 0][..]);
}
