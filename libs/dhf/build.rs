use btls_aux_codegen::Bitmask;
use btls_aux_codegen::CodeBlock as TokenStream;
use btls_aux_codegen::mkid;
use btls_aux_codegen::mkidf;
use btls_aux_codegen::quote;
use btls_aux_codegen::qwrite;
use btls_aux_codegen::Symbol;
use btls_aux_tls_iana::NamedGroup;
use std::io::Write as IoWrite;
use std::cmp::max;
use std::env::var;
use std::path::PathBuf;
use std::borrow::ToOwned;
use std::fs::File;

struct KemSize(usize, usize, usize);	//pk, ct, ss.
struct KemSecurity(u32, u32, u32);	//sec, qsec, rank.
//enumsym, constsym, hname, iname, groupstring
struct KemName(&'static str, &'static str, &'static str, &'static str, &'static str);
const FLAG_TLS12: u32 = 1;
const FLAG_DEFAULT: u32 = 2;
const FLAG_KEM: u32 = 4;
const FLAG_NSA: u32 = 8;

struct GroupEntry
{
	tls_id: Result<&'static str, u16>,
	name: KemName,
	size: KemSize,
	security: KemSecurity,
	priority: u32,
	flags: u32,
}

impl GroupEntry
{
	fn to_groupdata(&self, ordinal: usize) -> GroupInfo
	{
		let tls_id = match self.tls_id {
			Ok(symbolic) => match NamedGroup::new_name(symbolic) {
				Some(numeric) => numeric.get(),
				None => panic!("No group named {symbolic} known")
			},
			Err(numeric) => numeric,
		};
		GroupInfo {
			ordinal: ordinal as u32,
			tls_nid: tls_id,
			private_use: self.tls_id.is_err(),	//Assume these are private use.
			pksize1: self.size.0,
			pksize2: self.size.1,
			sssize: self.size.2,
			secbits: self.security.0,
			qsecbits: self.security.1,
			security_rank: self.security.2,
			tls12_flag: self.flags & FLAG_TLS12 != 0,
			is_default: self.flags & FLAG_DEFAULT != 0 && self.tls_id.is_ok(),
			is_nsa: self.flags & FLAG_NSA != 0,
			kem: self.flags & FLAG_KEM != 0,
			priority: self.priority,
			group: self.name.0.to_string(),
			tls_id: self.name.1.to_string(),
			name: self.name.2.to_string(),
			int_name: self.name.3.to_string(),
			dhgrp_str: self.name.4.to_string(),
		}
	}
}

static GROUP_DATA: &'static [GroupEntry] = &[
	GroupEntry {
		tls_id: Ok("secp256r1"),
		name: KemName("NsaP256","P256","NSA P-256","P-256","NIST_P256"),
		size: KemSize(65,65,32),
		security: KemSecurity(128,0,20),
		priority: 101,
		flags: FLAG_TLS12|FLAG_DEFAULT,
	},
	GroupEntry {
		tls_id: Ok("secp384r1"),
		name: KemName("NsaP384","P384","NSA P-384","P-384","NIST_P384"),
		size: KemSize(97,97,48),
		security: KemSecurity(192,0,30),
		priority: 201,
		flags: FLAG_TLS12|FLAG_DEFAULT|FLAG_NSA,
	},
	GroupEntry {
		tls_id: Ok("x25519"),
		name: KemName("X25519","X25519","X25519","X25519","X25519"),
		size: KemSize(32,32,32),
		security: KemSecurity(128,0,10),
		priority: 100,
		flags: FLAG_TLS12|FLAG_DEFAULT,
	},
	GroupEntry {
		tls_id: Ok("x448"),
		name: KemName("X448","X448","X448","X448","X448"),
		size: KemSize(56,56,56),
		security: KemSecurity(224,0,40),
		priority: 200,
		flags: FLAG_TLS12|FLAG_DEFAULT,
	},
	GroupEntry {
		tls_id: Ok("secp521r1"),
		name: KemName("NsaP521","P521","NSA P-521","P-521","NIST_P521"),
		size: KemSize(133,133,66),
		security: KemSecurity(256,0,50),
		priority: 300,
		flags: FLAG_TLS12|FLAG_DEFAULT,
	},
	GroupEntry {
		tls_id: Ok("X25519Kyber768Draft00"),
		name: KemName("X25519Kyber768","X25519_KYBER768","X25519Kyber768","X25519Kyber768","X25519_KYBER768"),
		size: KemSize(1216,1120,64),
		security: KemSecurity(192,128,11),
		priority: 400,
		flags: FLAG_KEM|FLAG_DEFAULT,
	},
];

#[derive(Clone)]
struct GroupInfo
{
	//Ordinal.
	ordinal: u32,
	//The name of group (SnakeCase identifier)
	group: String,
	//The TLS group symbolic identifier (CAPS_WITH_UNDERSCORES identifier)
	tls_id: String,
	//The TLS group numeric identifier.
	tls_nid: u16,
	///Private use?
	private_use: bool,
	//Is compatible with TLS 1.2?
	tls12_flag: bool,
	//Is enabled by default?
	is_default: bool,
	//Is NSA?
	is_nsa: bool,
	//Human-readable name of group
	name: String,
	//Group identifier in GROUP-*.
	int_name: String,
	//Identifier in dh group strings.
	dhgrp_str: String,
	//Public key size.
	pksize1: usize,
	//Ciphertext size.
	pksize2: usize,
	//Shared secret size.
	sssize: usize,
	//Classical security.
	secbits: u32,
	//Quantum security (0 if pre-quantum).
	qsecbits: u32,
	//Priority.
	priority: u32,
	//Security rank.
	security_rank: u32,
	//Generic KEM.
	kem: bool,
}

impl GroupInfo
{
	fn symbol(&self) -> Symbol { mkid(&self.group) }
	fn name(&self) -> String { self.name.clone() }
	fn int_name(&self) -> String { self.int_name.clone() }
	fn string_name(&self) -> String { self.dhgrp_str.clone() }
	fn signal_bit(&self) -> usize { self.ordinal as usize }
	fn tls_id(&self) -> u16 { self.tls_nid }
	fn tls12(&self) -> bool { self.tls12_flag }
	fn security_rank(&self) -> u32 { self.security_rank }
	fn sec_bits(&self) -> u32 { self.secbits }
	fn sec_qbits(&self) -> u32 { self.qsecbits }
	fn is_group(g: &&Self) -> bool { !g.kem }
	fn is_kem(g: &&Self) -> bool { g.kem }
	fn is_normal(g: &&Self) -> bool { g.is_default && !g.private_use }
	fn is_nsa(g: &&Self) -> bool { g.is_nsa }
	fn is_post_quantum(g: &&Self) -> bool { g.qsecbits >= 64 }
}

enum ValueType
{
	Short(u16),
	Size(usize),
}

fn make_const(pfx: &str, name: &str, value: ValueType) -> TokenStream
{
	let sym = mkidf!("{pfx}{name}");
	match value {
		ValueType::Short(value) => quote!(const #sym: u16 = #value;),
		ValueType::Size(value) => quote!(const #sym: usize = #value;),
	}
}


type _KemSetElem = u8;

fn emit_dhf_defs(fp: &mut impl IoWrite, groups: &[GroupInfo])
{

	let mut max_dhf_output = 0;
	let mut max_dhf_output_small = 0;
	let mut max_dhf_pubkey = 0;
	let mut max_dhf_pubkey_small = 0;
	let mut default_kem_bitmask = 0u64;
	let mut max_ordinal = 0usize;
	let mut consts: Vec<_> = Vec::new();
	let grouplist: Vec<_> = groups.iter().filter(GroupInfo::is_group).map(GroupInfo::symbol).collect();
	let gkemlist: Vec<_> = groups.iter().filter(GroupInfo::is_kem).map(GroupInfo::symbol).collect();
	let group_count = groups.len();
	for g in groups.iter() {
		max_ordinal = max(max_ordinal, g.ordinal as usize);
		if g.is_default && g.ordinal < 64 { default_kem_bitmask |= 1u64 << g.ordinal; }
		max_dhf_pubkey = max(max_dhf_pubkey, g.pksize1);
		max_dhf_pubkey = max(max_dhf_pubkey, g.pksize2);
		max_dhf_output = max(max_dhf_output, g.sssize);
		if g.private_use {
			consts.push(make_const("__TLS_NID_", &g.tls_id, ValueType::Short(g.tls_nid)));
		}
		consts.push(make_const("__SS_SIZE_", &g.tls_id, ValueType::Size(g.sssize)));
		consts.push(make_const("__PK_SIZE_", &g.tls_id, ValueType::Size(g.pksize1)));
		consts.push(make_const("__CT_SIZE_", &g.tls_id, ValueType::Size(g.pksize2)));
		if g.tls12_flag {
			max_dhf_pubkey_small = max(max_dhf_pubkey_small, g.pksize1);
			max_dhf_pubkey_small = max(max_dhf_pubkey_small, g.pksize2);
			max_dhf_output_small = max(max_dhf_output_small, g.sssize);
		}
	}

	//The grouplist needs to be sorted in order of priority.
	let mut groups_priority: Vec<GroupInfo> = groups.to_owned();
	groups_priority.sort_unstable_by_key(|&GroupInfo{priority,..}|priority);
	//Universe mask.
	let kem_normal = groups.iter().filter(GroupInfo::is_normal).map(GroupInfo::signal_bit);
	let kem_nsa = groups.iter().filter(GroupInfo::is_nsa).map(GroupInfo::signal_bit);
	let kem_post_quantum = groups.iter().filter(GroupInfo::is_post_quantum).map(GroupInfo::signal_bit);
	let kem_bs = Bitmask::<_KemSetElem>::new(groups.iter().map(GroupInfo::signal_bit));
	let kem_normal = kem_bs.make(kem_normal);
	let kem_nsa = kem_bs.make(kem_nsa);
	let kem_post_quantum = kem_bs.make(kem_post_quantum);

	qwrite(fp, quote!{

static DHGROUP_STRINGS: [&'static str; #group_count] = [
	#([groups] #string_name,)*
];
type _KemSet = #!MASKTYPE kem_bs;

#(#consts)*
const _KEM_UNIVERSE_MASK: _KemSet = #kem_bs; //[ #(#kem_universe),* ];
const _KEM_NORMAL_MASK: _KemSet = [ #(#kem_normal),* ];
const _KEM_POST_QUANTUM: _KemSet = [ #(#kem_post_quantum),* ];
const NSA_KEY: _KemSet = [ #(#kem_nsa),* ];
const _MAX_DHF_OUTPUT: usize = #max_dhf_output;
const _MAX_DHF_OUTPUT_SMALL: usize = #max_dhf_output_small;
const _MAX_DHF_PUBKEY: usize = #max_dhf_pubkey;
const _MAX_DHF_PUBKEY_SMALL: usize = #max_dhf_pubkey_small;
static _KEM_PRIORITY_ORDER: &'static [KEM] = &[
	#([groups_priority] KEM(_KEM::#symbol),)*
];
///The maximum possible size of `TLS 1.2` KEM public key in bytes.
///
///Note that KEM public keys in `TLS 1.3` can be larger than this.
pub const MAX_TLS12_KEM_PUBKEY: usize = #max_dhf_pubkey_small;
///The default-enabled KEM mask constant.
///
///This mask is suitable for passing to the [`KemEnabled::from_mask()`] function.
///
///[`KemEnabled::from_mask()`]:struct.KemEnabled.html#method.from_mask
pub const DEFAULT_KEM_BITMASK: u64 = #default_kem_bitmask;

#[allow(deprecated)]
impl KemSetIterator
{
	fn __next(&mut self) -> Option<_KEM>
	{
		let var = &mut self.0;
		//Iteration needs to be from high to low priority.
		#([groups_priority]
			if var.raw_test_and_remove(#signal_bit) { return Some(_KEM::#symbol); }
		)*
		None	//No more.
	}
}

#[allow(deprecated)]
impl KemSet
{
	fn __get_tls_id(&self, id: u16) -> Option<_KEM>
	{
		match id {
			#([groups]
				#tls_id if raw_bitset_isset(&self.0, #signal_bit) => Some(_KEM::#symbol),
			)*
			_ => None,
		}
	}
}

#[derive(Clone)]
#[allow(dead_code)]
enum _KemPublicKey
{
	#(#grouplist(<#grouplist as DiffieHellmanKem>::PK),)*
	Generic(_KEM, Vec<u8>)
}

impl _KemPublicKey
{
	fn kem(&self) -> _KEM
	{ 
		match self {
			#(_KemPublicKey::#grouplist(_) => _KEM::#grouplist,)*
			_KemPublicKey::Generic(kem, _) => *kem
		}
	}
	fn as_raw(&self) -> &[u8]
	{
		match self {
			#(_KemPublicKey::#grouplist(ref h) => h,)*
			_KemPublicKey::Generic(_, ref h) => h
		}
	}
}

#[derive(Clone)]
#[allow(dead_code)]
enum _KemCiphertext
{
	#(#grouplist(<#grouplist as DiffieHellmanKem>::PK),)*
	Generic(_KEM, Vec<u8>)
}

impl _KemCiphertext
{
	fn kem(&self) -> _KEM
	{ 
		match self {
			#(_KemCiphertext::#grouplist(_) => _KEM::#grouplist,)*
			_KemCiphertext::Generic(kem, _) => *kem
		}
	}
	fn as_raw(&self) -> &[u8]
	{
		match self {
			#(_KemCiphertext::#grouplist(ref h) => h,)*
			_KemCiphertext::Generic(_, ref h) => h
		}
	}
}

enum _KemPlaintext
{
	#(#grouplist(<#grouplist as DiffieHellmanKem>::SS),)*
	#(#gkemlist(<#gkemlist as GenericKem>::SS),)*
}

impl _KemPlaintext
{
	fn as_raw(&self) -> &[u8]
	{
		match self {
			#(_KemPlaintext::#grouplist(ref h) => h,)*
			#(_KemPlaintext::#gkemlist(ref h) => h,)*
		}
	}
}

#[derive(Copy,Clone,PartialEq,Eq,Debug)]
enum _KEM
{
	#(#grouplist,)*
	#(#gkemlist,)*
}
impl _KEM
{
	fn suitable_for_tls12(&self) -> bool
	{
		match self {
			#([groups] _KEM::#symbol => #tls12,)*
		}
	}
	fn get_classical_security(&self) -> u32
	{
		match self {
			#([groups] _KEM::#symbol => #sec_bits,)*
		}
	}
	fn get_quantum_security(&self) -> u32
	{
		match self {
			#([groups] _KEM::#symbol => #sec_qbits,)*
		}
	}
	fn get_security_rank(&self) -> u32
	{
		match self {
			#([groups] _KEM::#symbol => #security_rank,)*
		}
	}
	fn tls_id(&self) -> u16
	{
		match self {
			#([groups] _KEM::#symbol => #symbol::tls_id(),)*
		}
	}
	fn sigbit(&self) -> u32
	{
		let bit = match self {
			#([groups] &_KEM::#symbol => #signal_bit,)*
		};
		bit as u32
	}
	fn as_string(&self) -> &'static str
	{
		match self {
			#([groups] &_KEM::#symbol => #name,)*
		}
	}
	fn generate_key(&self, rng: &mut TemporaryRandomStream) -> Result<(_KemInitiator, _KemPublicKey), ()>
	{
		match self {
			#(
				_KEM::#grouplist => dtry!(NORET #grouplist::keygen(rng).map(|(sk,pk)|{
					(_KemInitiator::#grouplist(sk, pk), _KemPublicKey::#grouplist(pk))
				})),
			)*
			#(
				_KEM::#gkemlist => dtry!(NORET #gkemlist::keygen(rng).map(|(sk,pk)|{
					(_KemInitiator::#gkemlist(sk),_KemPublicKey::Generic(_KEM::#gkemlist, pk))
				})),
			)*
		}
	}
	fn responder(&self, rng: &mut TemporaryRandomStream) -> Result<_KemResponder, ()>
	{
		match self {
			#(
				_KEM::#grouplist => dtry!(NORET #grouplist::keygen(rng).map(|(sk,pk)|{
					_KemResponder::#grouplist(sk, pk)
				})),
			)*
			#(
				_KEM::#gkemlist => Ok(_KemResponder::#gkemlist),
			)*
		}
	}
	fn by_tls_id(id: u16) -> Option<_KEM>
	{
		match id {
			#([groups] #tls_id => Some(_KEM::#symbol),)*
			_ => None
		}
	}
	fn by_internal_name(name: &str) -> Option<_KEM>
	{
		#([groups]
			if name == #int_name { return Some(_KEM::#symbol); }
		)*
		None
	}
	fn public_key_check(&self, data: &[u8]) -> Result<(), ()>
	{
		match self {
			#(_KEM::#grouplist => #grouplist::key_check(data),)*
			#(_KEM::#gkemlist => #gkemlist::public_key_check(data),)*
		}
	}
	fn ciphertext_check(&self, data: &[u8]) -> Result<(), ()>
	{
		match self {
			#(_KEM::#grouplist => #grouplist::key_check(data),)*
			#(_KEM::#gkemlist => #gkemlist::ciphertext_check(data),)*
		}
	}
}
enum _KemInitiator
{
	//Privkey, Pubkey.
	#(#grouplist(<#grouplist as DiffieHellmanKem>::SK,<#grouplist as DiffieHellmanKem>::PK),)*
	#(#gkemlist(<#gkemlist as GenericKem>::SK),)*
}

impl _KemInitiator
{
	fn kem(&self) -> _KEM
	{ 
		match self {
			#(_KemInitiator::#grouplist(_,_) => _KEM::#grouplist,)*
			#(_KemInitiator::#gkemlist(_) => _KEM::#gkemlist,)*
		}
	}
	fn into_responder(self) -> Option<_KemResponder>
	{
		match self {
			#(_KemInitiator::#grouplist(sk, pk) => Some(_KemResponder::#grouplist(sk, pk)),)*
			#(_KemInitiator::#gkemlist(_) => None,)*
		}
	}
	fn decapsulate(self, ciphertext: &[u8]) -> Result<_KemPlaintext, ()>
	{
		match self {
			#(
				_KemInitiator::#grouplist(sk, _) => {
					let ss = dtry!(#grouplist::agree(sk, ciphertext));
					let ret_ss = _KemPlaintext::#grouplist(ss);
					Ok(ret_ss)
				},
			)*
			#(
				_KemInitiator::#gkemlist(sk) => {
					let ss = dtry!(#gkemlist::decapsulate(sk, ciphertext));
					let ret_ss = _KemPlaintext::#gkemlist(ss);
					Ok(ret_ss)
				},
			)*
		}
	}
}

enum _KemResponder
{
	//Privkey, Pubkey.
	#(#grouplist(<#grouplist as DiffieHellmanKem>::SK,<#grouplist as DiffieHellmanKem>::PK),)*
	#(#gkemlist,)*		//No state for generic KEM responders.
}

impl _KemResponder
{
	fn kem(&self) -> _KEM
	{ 
		match self {
			#(_KemResponder::#grouplist(_,_) => _KEM::#grouplist,)*
			#(_KemResponder::#gkemlist => _KEM::#gkemlist,)*
		}
	}
	fn encapsulate(self, key: &[u8], _rng: &mut TemporaryRandomStream) ->
		Result<(_KemPlaintext, _KemCiphertext), ()>
	{
		match self {
			#(
				_KemResponder::#grouplist(sk, pk) => {
					let ss = dtry!(#grouplist::agree(sk, key));
					let ret_ss = _KemPlaintext::#grouplist(ss);
					Ok((ret_ss, _KemCiphertext::#grouplist(pk)))
				},
			)*
			#(
				_KemResponder::#gkemlist => {
					let (ss, ct) = dtry!(#gkemlist::encapsulate(key, _rng));
					let ret_ss = _KemPlaintext::#gkemlist(ss);
					Ok((ret_ss, _KemCiphertext::Generic(_KEM::#gkemlist, ct)))
				},
			)*
		}
	}
}
	});
}

fn emit_dhf_tests<W:IoWrite>(fp: &mut W, groups: &[GroupInfo])
{
	let mut name = Vec::new();
	let mut group = Vec::new();
	let mut pubkeylen = Vec::new();
	let mut ctlen = Vec::new();
	let mut sslen = Vec::new();
	let mut tls12_status = Vec::new();
	let mut tlsnid = Vec::new();
	let mut idx = Vec::new();
	let mut hname = Vec::new();
	let mut iname = Vec::new();
	let mut qhard = Vec::new();
	let mut csecl = Vec::new();
	let mut qsecl = Vec::new();
	let mut testf_basic = Vec::new();
	let mut testf_keys_ok = Vec::new();
	let mut testf_delta = Vec::new();
	let mut testf_oneoff = Vec::new();
	let mut testf_veryshort = Vec::new();
	let mut testf_info = Vec::new();
	for (_idx, g) in groups.iter().enumerate() {
		let mut _name = String::new();
		for j in g.tls_id.chars() { _name.extend(j.to_lowercase()); }
		name.push(_name.clone());
		group.push(mkid(&g.group));
		pubkeylen.push(g.pksize1);
		ctlen.push(g.pksize2);
		sslen.push(g.sssize);
		tls12_status.push(g.tls12_flag);
		tlsnid.push(g.tls_nid);
		idx.push(_idx);
		hname.push(g.name.clone());
		iname.push(g.int_name.clone());
		qhard.push(g.qsecbits > 0);
		csecl.push(g.secbits);
		qsecl.push(g.qsecbits);
		testf_basic.push(mkidf!("test_basic_kex_{_name}"));
		testf_keys_ok.push(mkidf!("test_keys_ok_kex_{_name}"));
		testf_delta.push(mkidf!("test_delta_kex_{_name}"));
		testf_oneoff.push(mkidf!("test_oneoff_kex_{_name}"));
		testf_veryshort.push(mkidf!("test_veryshort_kex_{_name}"));
		testf_info.push(mkidf!("test_info_{_name}"));
	}

	qwrite(fp, quote!{
use crate::_KEM;
use crate::KEM;
use crate::KemEnabled;


use btls_aux_random::TemporaryRandomStream;
#(
#[test]
fn #testf_basic()
{
	let seed = [0;48];
	let mut rng = TemporaryRandomStream::new(&seed);
	let kem = KEM(_KEM::#group);
	let (alice, alice_pub) = kem.generate_key(&mut rng).expect("KEM keygen failed");
	assert!(alice.kem() == kem);
	assert!(alice_pub.kem() == kem);
	assert_eq!(alice_pub.as_raw().len(), #pubkeylen);
	assert!(alice_pub.as_raw().len() <= crate::_MAX_DHF_PUBKEY);
	assert!(!#tls12_status || alice_pub.as_raw().len() <= crate::_MAX_DHF_PUBKEY_SMALL);
	let bob = kem.responder(&mut rng).expect("KEM responder create failed");
	assert!(bob.kem() == kem);
	let (ba, bob_pub) = bob.encapsulate(alice_pub.as_raw(), &mut rng).expect("KEM encapsulate failed");
	assert!(bob_pub.kem() == kem);
	assert_eq!(bob_pub.as_raw().len(), #ctlen);
	assert!(bob_pub.as_raw().len() <= crate::_MAX_DHF_PUBKEY);
	assert!(!#tls12_status || bob_pub.as_raw().len() <= crate::_MAX_DHF_PUBKEY_SMALL);
	let ab = alice.decapsulate(bob_pub.as_raw()).expect("KEM decapsulate failed");
	assert_eq!(ab.as_raw().len(), #sslen);
	assert_eq!(ba.as_raw().len(), #sslen);
	assert!(ab.as_raw().len() <= crate::_MAX_DHF_OUTPUT);
	assert!(!#tls12_status || ab.as_raw().len() <= crate::_MAX_DHF_OUTPUT_SMALL);
	assert_eq!(ab.as_raw(), ba.as_raw());
}

#[test]
fn #testf_keys_ok()
{
	let kem = KEM(_KEM::#group);
	let mut seed = [3;48];
	let dt = std::time::SystemTime::now().duration_since(std::time::UNIX_EPOCH).unwrap().as_secs() as u64;
	(&mut seed[..8]).copy_from_slice(&dt.to_le_bytes());
	use core::str::FromStr;
	let roundcount = std::env::var("DHF_TEST_ROUNDS").ok().and_then(|r|{
		usize::from_str(&r).ok()
	}).unwrap_or(5000);
	let mut rng = TemporaryRandomStream::new(&seed);
	for _ in 0..roundcount{
		let (alice, alice_pub) = kem.generate_key(&mut rng).expect("KEM keygen failed");
		crate::kem_public_key_check(#tlsnid, alice_pub.as_raw()).expect("PK check failed");
		let bob = kem.responder(&mut rng).expect("KEM responder create failed");
		let (ba, bob_pub) = bob.encapsulate(alice_pub.as_raw(), &mut rng).expect("KEM encapsulate failed");
		crate::kem_ciphertext_check(#tlsnid, bob_pub.as_raw()).expect("CT check failed");
		let ab = alice.decapsulate(bob_pub.as_raw()).expect("KEM decapsulate failed");
		assert_eq!(ab.as_raw(), ba.as_raw());
	}
}

#[test]
fn #testf_delta()
{
	let kem = KEM(_KEM::#group);
	let seed = [0;48];
	let mut rng = TemporaryRandomStream::new(&seed);
	let (_, alice1_pub) = kem.generate_key(&mut rng).expect("KEM keygen failed");
	let (_, alice2_pub) = kem.generate_key(&mut rng).expect("KEM keygen failed");
	//Check that public keys are different.
	assert!(&alice1_pub.as_raw()[..16] != &alice2_pub.as_raw()[..16]);
	assert!(&alice1_pub.as_raw()[#pubkeylen-16..] != &alice2_pub.as_raw()[#pubkeylen-16..]);
	assert!(&alice1_pub.as_raw()[..] != &alice2_pub.as_raw()[..]);
	let bob1 = kem.responder(&mut rng).expect("KEM responder create failed");
	let bob2 = kem.responder(&mut rng).expect("KEM responder create failed");
	let (ba1, bob1_pub) = bob1.encapsulate(alice1_pub.as_raw(), &mut rng).expect("KEM encapsulate failed");
	let (ba2, bob2_pub) = bob2.encapsulate(alice2_pub.as_raw(), &mut rng).expect("KEM encapsulate failed");
	//Check that ciphertexts are different.
	assert!(&bob1_pub.as_raw()[..16] != &bob2_pub.as_raw()[..16]);
	assert!(&bob1_pub.as_raw()[..] != &bob2_pub.as_raw()[..]);
	assert!(&bob1_pub.as_raw()[#ctlen-16..] != &bob2_pub.as_raw()[#ctlen-16..]);
	//Check that shared secrets are different.
	assert!(&ba1.as_raw()[..16] != &ba2.as_raw()[..16]);
	assert!(&ba1.as_raw()[..] != &ba2.as_raw()[..]);
	assert!(&ba1.as_raw()[#sslen-16..] != &ba2.as_raw()[#sslen-16..]);
}

#[test]
fn #testf_oneoff()
{
	let kem = KEM(_KEM::#group);
	let seed = [0;48];
	let mut rng = TemporaryRandomStream::new(&seed);
	let (alice, alice_pub) = kem.generate_key(&mut rng).expect("KEM keygen failed");
	let bob1 = kem.responder(&mut rng).expect("KEM responder create failed");
	let bob2 = kem.responder(&mut rng).expect("KEM responder create failed");
	//Crashing with one short ids is not acceptable.
	assert!(bob1.encapsulate(&alice_pub.as_raw()[..#pubkeylen-1], &mut rng).is_err());
	let (_, bob_pub) = bob2.encapsulate(alice_pub.as_raw(), &mut rng).expect("KEM encapsulate failed");
	assert!(alice.decapsulate(&bob_pub.as_raw()[..#ctlen-1]).is_err());
}

#[test]
fn #testf_veryshort()
{
	let kem = KEM(_KEM::#group);
	let seed = [0;48];
	let mut rng = TemporaryRandomStream::new(&seed);
	let (alice, alice_pub) = kem.generate_key(&mut rng).expect("KEM keygen failed");
	let bob1 = kem.responder(&mut rng).expect("KEM responder create failed");
	let bob2 = kem.responder(&mut rng).expect("KEM responder create failed");
	//Crashing with too short ids is not acceptable.
	assert!(bob1.encapsulate(&alice_pub.as_raw()[..16], &mut rng).is_err());
	let (_, bob_pub) = bob2.encapsulate(alice_pub.as_raw(), &mut rng).expect("KEM encapsulate failed");
	assert!(alice.decapsulate(&bob_pub.as_raw()[..16]).is_err());
}

#[test]
fn #testf_info()
{
	let kem = KEM(_KEM::#group);
	assert_eq!(kem.suitable_for_tls12(), #tls12_status);
	assert_eq!(KEM::by_tls_id(#tlsnid, KemEnabled::unsafe_any_policy()), Some(kem));
	assert_eq!(kem.tls_id(), #tlsnid);
	assert_eq!(kem.to_bitmask(), 1u64 << #idx);
	assert_eq!(kem.as_string(), #hname);
	assert_eq!(KEM::by_internal_name_unchecked(#iname), Some(kem));
	assert_eq!(kem.quantum_hard(), #qhard);
	assert_eq!(kem.get_classical_security(), #csecl);
	assert_eq!(kem.get_quantum_security(), #qsecl);
}
	)*
	});
}

fn read_groups() -> Vec<GroupInfo>
{
	let mut groups = Vec::new();
	for (ordinal, data) in GROUP_DATA.iter().enumerate() {
		groups.push(data.to_groupdata(ordinal));
	}
	groups
}

fn main()
{
	let out_dir = var("OUT_DIR").unwrap();
	
	let mut f = File::create(&PathBuf::from(&out_dir).join("autogenerated.inc.rs")).unwrap();
	emit_dhf_defs(&mut f, &read_groups());

	let mut f = File::create(&PathBuf::from(out_dir).join("autogenerated-test.inc.rs")).unwrap();
	emit_dhf_tests(&mut f, &read_groups());

	println!("cargo:rerun-if-changed=build.rs");	//This has no external deps to rebuild for.
}
