use btls_aux_dhf::KEM;
use btls_aux_dhf::kem_ciphertext_check;
use btls_aux_dhf::kem_public_key_check;
use btls_aux_dhf::KemEnabled;
use btls_aux_random::TemporaryRandomStream;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::sync::mpsc::channel;
use std::sync::mpsc::Sender;

#[cfg(target_arch = "x86_64")]
fn rdtsc() -> u64 { unsafe { std::arch::x86_64::_rdtsc() as u64 } }
#[cfg(not(target_arch = "x86_64"))]
fn rdtsc() -> u64 { 0 }

fn test_kem(kem: KEM, s: Sender<(usize,u64,u64)>, tag: usize)
{
	let mut seed = [3;48];
	let dt = std::time::SystemTime::now().duration_since(std::time::UNIX_EPOCH).unwrap().as_secs() as u64;
	(&mut seed[..8]).copy_from_slice(&dt.to_le_bytes());
	(&mut seed[8..16]).copy_from_slice(&(tag as u64).to_le_bytes());
	let nid = kem.tls_id();
	let mut rng = TemporaryRandomStream::new(&seed);
	let mut i = 0;
	let mut pkctime = 0;
	loop {
		let (alice, alicepub) = kem.generate_key(&mut rng).unwrap();
		let alicepub = alicepub.as_raw();
		let t = rdtsc();
		crate::kem_public_key_check(nid, &alicepub).unwrap();
		pkctime += rdtsc() - t;
		let bob = kem.responder(&mut rng).unwrap();
		let (bob_ss, bobpub) = bob.encapsulate(&alicepub, &mut rng).unwrap();
		let bobpub = bobpub.as_raw();
		let t = rdtsc();
		crate::kem_ciphertext_check(nid, &bobpub).unwrap();
		pkctime += rdtsc() - t;
		let alice_ss = alice.decapsulate(&bobpub).unwrap();
		assert_eq!(alice_ss.as_raw(), bob_ss.as_raw());
		i += 1;
		if i % 1000 == 0 { s.send((tag,i,pkctime/(2*i))).ok(); }
	}
	//P256: 100M, P384: 45M, P521: 20M, X25519: 370M, X448: 75M
}

#[derive(Copy,Clone,Default)]
struct PrintResult(u64, u64);

impl Display for PrintResult
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "{amt}({cyc})", amt=self.0, cyc=self.1)
	}
}

fn main()
{
	let all_kems = KemEnabled::from_mask(!0);
	let p256 = KEM::by_tls_id(23, all_kems).unwrap();
	let p384 = KEM::by_tls_id(24, all_kems).unwrap();
	let p521 = KEM::by_tls_id(25, all_kems).unwrap();
	let x25519 = KEM::by_tls_id(29, all_kems).unwrap();
	let x448 = KEM::by_tls_id(30, all_kems).unwrap();
	let (s, r) = channel();
	let s1 = s.clone();
	let s2 = s.clone();
	let s3 = s.clone();
	let s4 = s.clone();
	let s5 = s.clone();
	std::thread::spawn(move ||test_kem(p256,s1,0));
	std::thread::spawn(move ||test_kem(p384,s2,1));
	std::thread::spawn(move ||test_kem(p521,s3,2));
	std::thread::spawn(move ||test_kem(x25519,s4,3));
	std::thread::spawn(move ||test_kem(x448,s5,4));
	drop(s);
	let mut arr: [PrintResult;5] = Default::default();
	println!("");
	loop {
		let (tag, amt, cyc) = r.recv().unwrap();
		arr[tag] = PrintResult(amt, cyc);
		let [p256, p384, p521, x25519, x448] = arr;
		println!("\x1b[1AP256: {p256}, P384: {p384}, P521: {p521}, X25519: {x25519}, X448: {x448}");
	}
}
