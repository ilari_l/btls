#![allow(unsafe_code)]
use super::GenericKem;
use btls_aux_collections::Box;
use btls_aux_collections::Vec;
use btls_aux_kyber::FromByteSlice;
use btls_aux_kyber::KEM as KyberKEM;
use btls_aux_kyber::Kyber768;
use btls_aux_memory::split_at;
use btls_aux_random::TemporaryRandomStream;
use btls_aux_tls_iana::NamedGroup;
use btls_aux_xed25519::x25519_agree;
use btls_aux_xed25519::x25519_generate;
use core::convert::TryInto;
use core::mem::MaybeUninit;

const SS_SIZE: usize = 64;

//impl ByteArray for [u8;SS_SIZE] { fn borrow_bytes<'a>(&'a self) -> &'a [u8] { self } }

pub struct X25519Kyber768Secret
{
	x25519: [u8;32],
	kyber: <Kyber768 as KyberKEM>::SK,
}

pub struct X25519Kyber768;

impl GenericKem for X25519Kyber768
{
	type SK = Box<X25519Kyber768Secret>;
	type SS = [u8;SS_SIZE];
	fn tls_id() -> u16 { NamedGroup::X25519KYBER768DRAFT00.get() }
	fn keygen(rng: &mut TemporaryRandomStream) -> Option<(Box<X25519Kyber768Secret>, Vec<u8>)>
	{
		let mut sk1 = [0;32];
		let mut pk1 = [0;32];
		let mut sk2 = MaybeUninit::<<Kyber768 as KyberKEM>::SK>::uninit();
		let mut pk2 = MaybeUninit::<<Kyber768 as KyberKEM>::PK>::uninit();
		rng.fill_bytes(&mut sk1);
		x25519_generate(&mut pk1, &sk1);
		let (_, pk2) = Kyber768::keypair(rng, &mut sk2, &mut pk2);
		let mut pk = Vec::with_capacity(32 + pk2.as_ref().len());
		pk.extend_from_slice(&pk1);
		pk.extend_from_slice(pk2.as_ref());
		let sk = X25519Kyber768Secret {
			x25519: sk1,
			kyber: unsafe{sk2.assume_init()},
		};
		Some((Box::new(sk), pk))
	}
	fn encapsulate(pk: &[u8], rng: &mut TemporaryRandomStream) -> Option<([u8;SS_SIZE], Vec<u8>)>
	{
		let mut ct1 = [0;32];
		let mut ss1 = [0;32];
		let mut sk1 = [0;32];
		let mut ct2 = MaybeUninit::<<Kyber768 as KyberKEM>::CT>::uninit();
		let mut ss2 = MaybeUninit::<<Kyber768 as KyberKEM>::SS>::uninit();
		let (pk1, pk2) = split_at(pk, 32).ok()?;
		let pk1: &[u8;32] = pk1.try_into().ok()?;
		let pk2 = <Kyber768 as KyberKEM>::PK::from_slice(pk2)?;
		rng.fill_bytes(&mut sk1);
		x25519_generate(&mut ct1, &sk1);
		x25519_agree(&mut ss1, &sk1, pk1);
		let (ct2, ss2) = Kyber768::encapsulate(rng, &mut ct2, &mut ss2, pk2);
		let mut ss = [0;SS_SIZE];
		(&mut ss[..32]).copy_from_slice(&ss1);
		(&mut ss[32..]).copy_from_slice(ss2.as_ref());
		let mut ct = Vec::with_capacity(32 + ct2.as_ref().len());
		ct.extend_from_slice(&ct1);
		ct.extend_from_slice(ct2.as_ref());
		Some((ss, ct))
	}
	fn decapsulate(sk: Box<X25519Kyber768Secret>, ct: &[u8]) -> Option<[u8;SS_SIZE]>
	{
		let mut ss1 = [0;32];
		let mut ss2 = MaybeUninit::<<Kyber768 as KyberKEM>::SS>::uninit();
		let (ct1, ct2) = split_at(ct, 32).ok()?;
		let ct1: &[u8;32] = ct1.try_into().ok()?;
		let ct2 = <Kyber768 as KyberKEM>::CT::from_slice(ct2)?;
		x25519_agree(&mut ss1, &sk.x25519, ct1);
		let ss2 = Kyber768::decapsulate_unchecked(&mut ss2, ct2, &sk.kyber);
		let mut ss = [0;SS_SIZE];
		(&mut ss[..32]).copy_from_slice(&ss1);
		(&mut ss[32..]).copy_from_slice(ss2.as_ref());
		Some(ss)
	}
	fn public_key_check(data: &[u8]) -> Result<(), ()>
	{
		if data.len() == 32 + <Kyber768 as KyberKEM>::PK::length() { Ok(()) } else { Err(()) }
	}
	fn ciphertext_check(data: &[u8]) -> Result<(), ()>
	{
		if data.len() == 32 + <Kyber768 as KyberKEM>::CT::length() { Ok(()) } else { Err(()) }
	}
}
