use crate::all_zero;
use crate::DiffieHellmanKem;
use btls_aux_fail::fail_if_none;
use btls_aux_random::TemporaryRandomStream;
use btls_aux_xed25519::x25519_agree;
use btls_aux_xed25519::x25519_generate;
use btls_aux_xed448::x448_agree;
use btls_aux_xed448::x448_generate;
use core::convert::TryInto;


macro_rules! impl_dh_xfunc
{
	($name:ident, $($generate:ident)::*, $($agree:ident)::*, $size:ident) => {
		pub struct $name;
		use crate::$size;
		impl DiffieHellmanKem for $name
		{
			type PK = [u8;$size];
			type SK = [u8;$size];
			type SS = [u8;$size];
			fn tls_id() -> u16 { btls_aux_tls_iana::NamedGroup::$name.get() }
			fn keygen(rng: &mut TemporaryRandomStream) -> Option<([u8;$size], [u8;$size])>
			{
				let mut sk = [0;$size];
				rng.fill_bytes(&mut sk);
				let mut pk = [0;$size];
				$($generate)::*(&mut pk, &sk);
				Some((sk, pk))
			}
			fn agree(sk: [u8;$size], peerpk: &[u8]) -> Option<[u8;$size]>
			{
				let key2: &[u8; $size] = peerpk.try_into().ok()?;
				let mut ss = [0; $size];
				$($agree)::*(&mut ss, &sk, key2);
				fail_if_none!(all_zero(&ss));
				Some(ss)
			}
			//Anything of correct length is good.
			fn key_check(data: &[u8]) -> Result<(), ()>
			{
				if data.len() == $size { Ok(()) } else { Err(()) }
			}
		}
	}
}

impl_dh_xfunc!(X25519, x25519_generate, x25519_agree, __SS_SIZE_X25519);
impl_dh_xfunc!(X448, x448_generate, x448_agree, __SS_SIZE_X448);
