use crate::DiffieHellmanKem;
use btls_aux_eccdecompress::check_point_nist_p256 as check_p256;
use btls_aux_eccdecompress::check_point_nist_p384 as check_p384;
use btls_aux_eccdecompress::check_point_nist_p521 as check_p521;
use btls_aux_fail::dtry;
use btls_aux_fail::fail_if;
use btls_aux_fail::fail_if_none;
use btls_aux_memory::split_array_head;
use btls_aux_nettle::EccCurve;
use btls_aux_nettle::NsaEcdhKey;
use btls_aux_random::TemporaryRandomStream;
use btls_aux_tls_iana::NamedGroup as G;


macro_rules! impl_nsa_p_x
{
	($name:ident $bsize:ident $crvid:ident $oncurve:ident) => {
		pub struct $name;
		use crate::$bsize;
		impl DiffieHellmanKem for $name
		{
			type PK = [u8;2*$bsize+1];
			type SK = NsaEcdhKey;
			type SS = [u8;$bsize];
			fn tls_id() -> u16 { $crvid::tlsid() }
			fn keygen(rng: &mut TemporaryRandomStream) -> Option<(NsaEcdhKey, [u8;2*$bsize+1])>
			{
				let crv = EccCurve::new($crvid::id()).ok()?;
				let mut pubkey = [0;2*$bsize+1];
				let (key, plen) = NsaEcdhKey::new(crv, &mut pubkey, rng).ok()?;
				fail_if_none!(plen != 2*$bsize+1);
				Some((key, pubkey))
			}
			fn agree(sk: NsaEcdhKey, peerpk: &[u8]) -> Option<[u8;$bsize]>
			{
				let mut shared = [0; 2*$bsize+1];
				fail_if_none!(peerpk.len() != 2*$bsize+1);
				sk.agree(&mut shared, peerpk).ok()?;
				//Only take the x component. $bsize+1 <= 2*$bsize+1.
				let out = unsafe{shared.as_ptr().add(1).cast::<[u8;$bsize]>().read()};
				Some(out)
			}
			fn key_check(data: &[u8]) -> Result<(), ()>
			{
				//Must be of correct length, and format must be 4.
				let (format, data) = dtry!(split_array_head::<u8,1>(data));
				fail_if!(format != &[4], ());
				//Extract (x,y). There should be nothing left over.
				let (x, data) = dtry!(split_array_head::<u8,$bsize>(data));
				let (y, data) = dtry!(split_array_head::<u8,$bsize>(data));
				fail_if!(data.len() > 0, ());
				//Check (x,y) is on curve.
				$oncurve(*x, *y)
			}
		}
	}
}

impl_nsa_p_x!(NsaP256 __SS_SIZE_P256 P256 check_p256);
impl_nsa_p_x!(NsaP384 __SS_SIZE_P384 P384 check_p384);
impl_nsa_p_x!(NsaP521 __SS_SIZE_P521 P521 check_p521);


trait NettleCurve { fn id() -> u32; fn tlsid() -> u16; }
struct P256; impl NettleCurve for P256 { fn id() -> u32 { 0 } fn tlsid() -> u16 { G::SECP256R1.get() } }
struct P384; impl NettleCurve for P384 { fn id() -> u32 { 1 } fn tlsid() -> u16 { G::SECP384R1.get() } }
struct P521; impl NettleCurve for P521 { fn id() -> u32 { 2 } fn tlsid() -> u16 { G::SECP521R1.get() } }

macro_rules! test_point_compression
{
	($name:ident, $len:expr, $clazz:ident, $check:ident, $recover:ident) => {
		#[test]
		fn $name()
		{
			use btls_aux_random::TemporaryRandomLifetimeTag;
			let rng = TemporaryRandomLifetimeTag;
			let mut rng = TemporaryRandomStream::new_system(&rng);
			for _ in 0..1000 {
				let pk = $clazz::keygen(&mut rng).map(|(_,pk)|pk).unwrap();
				let (_, pk) = split_array_head::<u8,1>(&pk).unwrap();
				let (x, pk) = split_array_head::<u8,$len>(pk).unwrap();
				let (y, pk) = split_array_head::<u8,$len>(pk).unwrap();
				assert!(pk.len() == 0);
				let ysign = y[$len-1] & 1;
				assert!($check(*x, *y).is_ok());
				let y2 = btls_aux_eccdecompress::$recover(*x, ysign).unwrap();
				assert_eq!(y, &y2);
			}
		}
	}
}

test_point_compression!(point_compression_p256, 32, NsaP256, check_p256, recover_y_nist_p256);
test_point_compression!(point_compression_p384, 48, NsaP384, check_p384, recover_y_nist_p384);
test_point_compression!(point_compression_p521, 66, NsaP521, check_p521, recover_y_nist_p521);
