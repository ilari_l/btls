use super::*;
use btls_aux_collections::ToString;

#[test]
fn test_oid_encode()
{
	assert_eq!(&encode_oid_to_bytes("0.0").unwrap(), &[0]);
	assert_eq!(&encode_oid_to_bytes("0.39").unwrap(), &[39]);
	assert_eq!(&encode_oid_to_bytes("1.0").unwrap(), &[40]);
	assert_eq!(&encode_oid_to_bytes("1.39").unwrap(), &[79]);
	assert_eq!(&encode_oid_to_bytes("2.0").unwrap(), &[80]);
	assert_eq!(&encode_oid_to_bytes("2.18446744073709551615").unwrap(), &[130, 128, 128, 128, 128, 128, 128,
		128, 128, 79]);
	assert_eq!(&encode_oid_to_bytes("2.18446744073709551536").unwrap(), &[130, 128, 128, 128, 128, 128, 128,
		128, 128, 0]);
	assert_eq!(&encode_oid_to_bytes("2.18446744073709551535").unwrap(), &[129, 255, 255, 255, 255, 255, 255,
		255, 255, 127]);
	assert_eq!(&encode_oid_to_bytes("2.9223372036854775728").unwrap(), &[129, 128, 128, 128, 128, 128, 128, 128,
		128, 0]);
	assert_eq!(&encode_oid_to_bytes("2.9223372036854775727").unwrap(), &[255, 255, 255, 255, 255, 255, 255, 255,
		127]);
	assert_eq!(&encode_oid_to_bytes("2.72057594037927856").unwrap(), &[129, 128, 128, 128, 128, 128, 128, 128,
		0]);
	assert_eq!(&encode_oid_to_bytes("2.72057594037927855").unwrap(), &[255, 255, 255, 255, 255, 255, 255, 127]);
	assert_eq!(&encode_oid_to_bytes("2.562949953421232").unwrap(), &[129, 128, 128, 128, 128, 128, 128, 0]);
	assert_eq!(&encode_oid_to_bytes("2.562949953421231").unwrap(), &[255, 255, 255, 255, 255, 255, 127]);
	assert_eq!(&encode_oid_to_bytes("2.4398046511024").unwrap(), &[129, 128, 128, 128, 128, 128, 0]);
	assert_eq!(&encode_oid_to_bytes("2.4398046511023").unwrap(), &[255, 255, 255, 255, 255, 127]);
	assert_eq!(&encode_oid_to_bytes("2.34359738288").unwrap(), &[129, 128, 128, 128, 128, 0]);
	assert_eq!(&encode_oid_to_bytes("2.34359738287").unwrap(), &[255, 255, 255, 255, 127]);
	assert_eq!(&encode_oid_to_bytes("2.268435376").unwrap(), &[129, 128, 128, 128, 0]);
	assert_eq!(&encode_oid_to_bytes("2.268435375").unwrap(), &[255, 255, 255, 127]);
	assert_eq!(&encode_oid_to_bytes("2.2097072").unwrap(), &[129, 128, 128, 0]);
	assert_eq!(&encode_oid_to_bytes("2.2097071").unwrap(), &[255, 255, 127]);
	assert_eq!(&encode_oid_to_bytes("2.16304").unwrap(), &[129, 128, 0]);
	assert_eq!(&encode_oid_to_bytes("2.16303").unwrap(), &[255, 127]);
	assert_eq!(&encode_oid_to_bytes("2.48").unwrap(), &[129,0]);
	assert_eq!(&encode_oid_to_bytes("2.47").unwrap(), &[127]);
	assert_eq!(&encode_oid_to_bytes("2.1.18446744073709551615").unwrap(), &[81, 129, 255, 255, 255, 255, 255,
		255, 255, 255, 127]);
	assert_eq!(&encode_oid_to_bytes("2.1.9223372036854775808").unwrap(), &[81, 129, 128, 128, 128, 128, 128, 128,
		128, 128, 0]);
	assert_eq!(&encode_oid_to_bytes("2.1.9223372036854775807").unwrap(), &[81, 255, 255, 255, 255, 255, 255, 255,
		255, 127]);
	assert_eq!(&encode_oid_to_bytes("2.1.72057594037927936").unwrap(), &[81, 129, 128, 128, 128, 128, 128, 128,
		128, 0]);
	assert_eq!(&encode_oid_to_bytes("2.1.72057594037927935").unwrap(), &[81, 255, 255, 255, 255, 255, 255, 255,
		127]);
	assert_eq!(&encode_oid_to_bytes("2.1.562949953421312").unwrap(), &[81, 129, 128, 128, 128, 128, 128, 128,
		0]);
	assert_eq!(&encode_oid_to_bytes("2.1.562949953421311").unwrap(), &[81, 255, 255, 255, 255, 255, 255, 127]);
	assert_eq!(&encode_oid_to_bytes("2.1.4398046511104").unwrap(), &[81, 129, 128, 128, 128, 128, 128, 0]);
	assert_eq!(&encode_oid_to_bytes("2.1.4398046511103").unwrap(), &[81, 255, 255, 255, 255, 255, 127]);
	assert_eq!(&encode_oid_to_bytes("2.1.34359738368").unwrap(), &[81, 129, 128, 128, 128, 128, 0]);
	assert_eq!(&encode_oid_to_bytes("2.1.34359738367").unwrap(), &[81, 255, 255, 255, 255, 127]);
	assert_eq!(&encode_oid_to_bytes("2.1.268435456").unwrap(), &[81, 129, 128, 128, 128, 0]);
	assert_eq!(&encode_oid_to_bytes("2.1.268435455").unwrap(), &[81, 255, 255, 255, 127]);
	assert_eq!(&encode_oid_to_bytes("2.1.2097152").unwrap(), &[81, 129, 128, 128, 0]);
	assert_eq!(&encode_oid_to_bytes("2.1.2097151").unwrap(), &[81, 255, 255, 127]);
	assert_eq!(&encode_oid_to_bytes("2.1.16384").unwrap(), &[81, 129, 128, 0]);
	assert_eq!(&encode_oid_to_bytes("2.1.16383").unwrap(), &[81, 255, 127]);
	assert_eq!(&encode_oid_to_bytes("2.1.128").unwrap(), &[81, 129, 0]);
	assert_eq!(&encode_oid_to_bytes("2.1.127").unwrap(), &[81, 127]);
	assert_eq!(&encode_oid_to_bytes("uuid:db8b94df-5c5c-4005-a66b-928a782e0c05"),
		&encode_oid_to_bytes("uuid:DB8B94DF-5C5C-4005-A66B-928A782E0C05"));
	assert_eq!(encode_oid_to_bytes(""), Err(OidEncodeError::BadOidSyntax));
	assert_eq!(encode_oid_to_bytes("."), Err(OidEncodeError::BadOidSyntax));
	assert_eq!(encode_oid_to_bytes(".1"), Err(OidEncodeError::BadOidSyntax));
	assert_eq!(encode_oid_to_bytes("1."), Err(OidEncodeError::BadOidSyntax));
	assert_eq!(encode_oid_to_bytes("1..2"), Err(OidEncodeError::BadOidSyntax));
	assert_eq!(encode_oid_to_bytes("1.2."), Err(OidEncodeError::BadOidSyntax));
	assert_eq!(encode_oid_to_bytes("1.2.3."), Err(OidEncodeError::BadOidSyntax));
	assert_eq!(encode_oid_to_bytes("/.2.3.4.5"), Err(OidEncodeError::UnparseableNumber(0)));
	assert_eq!(encode_oid_to_bytes(":.2.3.4.5"), Err(OidEncodeError::UnparseableNumber(0)));
	assert_eq!(encode_oid_to_bytes("1./.3.4.5"), Err(OidEncodeError::UnparseableNumber(1)));
	assert_eq!(encode_oid_to_bytes("1.:.3.4.5"), Err(OidEncodeError::UnparseableNumber(1)));
	assert_eq!(encode_oid_to_bytes("1.2./.4.5"), Err(OidEncodeError::UnparseableNumber(2)));
	assert_eq!(encode_oid_to_bytes("1.2.:.4.5"), Err(OidEncodeError::UnparseableNumber(2)));
	assert_eq!(encode_oid_to_bytes("1.2.3.:.5"), Err(OidEncodeError::UnparseableNumber(3)));
	assert_eq!(encode_oid_to_bytes("1.2.3./.5"), Err(OidEncodeError::UnparseableNumber(3)));
	assert_eq!(encode_oid_to_bytes("1.2.3.4.:"), Err(OidEncodeError::UnparseableNumber(4)));
	assert_eq!(encode_oid_to_bytes("1.2.3.4./"), Err(OidEncodeError::UnparseableNumber(4)));
	assert_eq!(encode_oid_to_bytes("2.18446744073709551616.3.5").unwrap(),
		&[130,128,128,128,128,128,128,128,128,80,3,5]);
	assert_eq!(encode_oid_to_bytes("2.5.18446744073709551616.5").unwrap(),
		&[85,130,128,128,128,128,128,128,128,128,0,5]);
	assert_eq!(encode_oid_to_bytes("2.5.4.18446744073709551616").unwrap(),
		&[85,4,130,128,128,128,128,128,128,128,128,0]);
	assert_eq!(encode_oid_to_bytes("0"), Err(OidEncodeError::LessThanTwoComponents));
	assert_eq!(encode_oid_to_bytes("1"), Err(OidEncodeError::LessThanTwoComponents));
	assert_eq!(encode_oid_to_bytes("2"), Err(OidEncodeError::LessThanTwoComponents));
	assert_eq!(encode_oid_to_bytes("3"), Err(OidEncodeError::FirstComponentOutOfRange(3)));
	assert_eq!(encode_oid_to_bytes("3.4"), Err(OidEncodeError::FirstComponentOutOfRange(3)));
	assert_eq!(encode_oid_to_bytes("5.4"), Err(OidEncodeError::FirstComponentOutOfRange(5)));
	assert_eq!(encode_oid_to_bytes("0.40"), Err(OidEncodeError::SecondComponentOutOfRange(40)));
	assert_eq!(encode_oid_to_bytes("1.40"), Err(OidEncodeError::SecondComponentOutOfRange(40)));
	assert_eq!(encode_oid_to_bytes("uuid:db8b94df-5c5c-4005-a66b-928a782e0c0"),
		Err(OidEncodeError::BadUuidSyntax));
	assert_eq!(encode_oid_to_bytes("uuid:db8b94df-5c5c-4005-a66b-928a782e0c052"),
		Err(OidEncodeError::BadUuidSyntax));
	assert_eq!(encode_oid_to_bytes("uuid:db8b94df-5c5c-4005-a66b-928a782e0c05-"),
		Err(OidEncodeError::BadUuidSyntax));
	assert_eq!(encode_oid_to_bytes("uuid:-db8b94df-5c5c-4005-a66b-928a782e0c05-"),
		Err(OidEncodeError::BadUuidSyntax));
	assert_eq!(encode_oid_to_bytes("uuid:db8b94df-5c5c-4005-a66b--928a782e0c05"),
		Err(OidEncodeError::BadUuidSyntax));
	assert_eq!(encode_oid_to_bytes("uuid:db8b94df-5c5c-4005-a66b-928a782e0c0g"),
		Err(OidEncodeError::BadUuidSyntax));
	assert_eq!(encode_oid_to_bytes("uuid:db8b94df-5c5c-4005-a66b-928a782e0c0G"),
		Err(OidEncodeError::BadUuidSyntax));
	assert_eq!(encode_oid_to_bytes("uuid:db8b94df-5c5c-4005-a66b-928a782e0c0`"),
		Err(OidEncodeError::BadUuidSyntax));
	assert_eq!(encode_oid_to_bytes("uuid:db8b94df-5c5c-4005-a66b-928a782e0c0@"),
		Err(OidEncodeError::BadUuidSyntax));
	assert_eq!(encode_oid_to_bytes("uuid:db8b94df-5c5c-4005-a66b-928a782e0c0/"),
		Err(OidEncodeError::BadUuidSyntax));
	assert_eq!(encode_oid_to_bytes("uuid:db8b94df-5c5c-4005-a66b-928a782e0c0:"),
		Err(OidEncodeError::BadUuidSyntax));
}

#[cfg(test)]
fn backforth(sample: &str)
{
	assert_eq!(&TranslateAsn1Oid(&encode_oid_to_bytes(sample).unwrap()).to_string(), sample);
}

#[cfg(test)]
fn backforth2(sample: &[u8])
{
	let txt = TranslateAsn1Oid(sample).to_string();
	assert_eq!(sample, &encode_oid_to_bytes(&txt).unwrap()[..]);
}

#[test]
fn test_gigantic_second()
{
	let x = "2.314159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211";
	backforth(x);
}

#[test]
fn test_gigantic_third()
{
	let x = "2.9.3141592653589793238462643383279502884197169399375105820974944592307816406286208998628034825342";
	backforth(x);
}

#[test]
fn test_gigantic_not_uuid()
{
	let x = "2.25.314159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534";
	backforth(x);
}

#[test]
fn test_uuid_out_of_range()
{
	let x = "2.25.340282366920938463463374607431768211456";
	backforth(x);
}

#[test]
fn test_zeroes()
{
	backforth("0.0");
	backforth("0.1");
	backforth("0.39");
	backforth("1.0");
	backforth("1.39");
	backforth("2.0");
	backforth("2.0.1");
	backforth("2.5.0");
	backforth("2.5.0.2");
	backforth("2.5.34.0");
}

#[test]
fn test_edges_2nd()
{
	for i in 0..127 {
		let mut x = Vec::new();
		for _ in 0..i { x.push(0xFF); }
		x.push(0x7F);
		x.push(0);
		backforth2(&x);
		let mut x = Vec::new();
		x.push(0x81);
		for _ in 0..i { x.push(0x80); }
		x.push(0);
		x.push(0);
		backforth2(&x);
	}
}

#[test]
fn test_edges_3rd()
{
	for i in 0..127 {
		let mut x = Vec::new();
		x.push(42);
		for _ in 0..i { x.push(0xFF); }
		x.push(0x7F);
		x.push(0);
		backforth2(&x);
		let mut x = Vec::new();
		x.push(42);
		x.push(0x81);
		for _ in 0..i { x.push(0x80); }
		x.push(0);
		x.push(0);
		backforth2(&x);
	}
}


#[test]
fn test_backforth()
{
	let tests = ["2.18446744073709551535", "2.9223372036854775728", "2.9223372036854775727",
		"2.72057594037927856", "2.72057594037927855", "2.562949953421232", "2.562949953421231",
		"2.4398046511024", "2.4398046511023", "2.34359738288", "2.34359738287", "2.268435376",
		"2.268435375", "2.2097072", "2.2097071", "2.16304", "2.16303", "2.48", "2.47", "2.0", "1.39",
		"1.0", "0.39", "0.0", "2.12345.643643", "2.0.643643", "1.39.643643", "1.0.643643", "0.39.643643",
		"0.0.643643", "1.2.3.4.5"];
	for i in tests.iter() {
		backforth(i);
	}
	backforth("uuid:db8b94df-5c5c-4005-a66b-928a782e0c05");
	backforth("uuid:aa008681-931d-4bd2-99f7-50f96567664a");
	backforth("uuid:c404be0b-f801-4a93-a06c-7fee64a9d030");
	backforth("uuid:02c0f58b-f7b9-449d-8a24-615be1b58fa4");
	backforth("uuid:ffffffff-ffff-ffff-ffff-ffffffffffff");
	backforth("uuid:3fffffff-ffff-ffff-ffff-ffffffffffff");
	backforth("uuid:007fffff-ffff-ffff-ffff-ffffffffffff");
	backforth("uuid:0000ffff-ffff-ffff-ffff-ffffffffffff");
	backforth("uuid:000001ff-ffff-ffff-ffff-ffffffffffff");
	backforth("uuid:00000003-ffff-ffff-ffff-ffffffffffff");
	backforth("uuid:00000000-07ff-ffff-ffff-ffffffffffff");
	backforth("uuid:00000000-000f-ffff-ffff-ffffffffffff");
	backforth("uuid:00000000-0000-1fff-ffff-ffffffffffff");
	backforth("uuid:00000000-0000-003f-ffff-ffffffffffff");
	backforth("uuid:00000000-0000-0000-7fff-ffffffffffff");
	backforth("uuid:00000000-0000-0000-00ff-ffffffffffff");
	backforth("uuid:00000000-0000-0000-0001-ffffffffffff");
	backforth("uuid:00000000-0000-0000-0000-03ffffffffff");
	backforth("uuid:00000000-0000-0000-0000-0007ffffffff");
	backforth("uuid:00000000-0000-0000-0000-00000fffffff");
	backforth("uuid:00000000-0000-0000-0000-0000001fffff");
	backforth("uuid:00000000-0000-0000-0000-000000003fff");
	backforth("uuid:00000000-0000-0000-0000-00000000007f");
	backforth("uuid:00000000-0000-0000-0000-000000000000");
}


#[test]
fn test_trans_asn1_oid()
{
	assert_eq!(&TranslateAsn1Oid(&[]).to_string(), "<Blank>");
	assert_eq!(&TranslateAsn1Oid(&[42]).to_string(), "1.2");
	assert_eq!(&TranslateAsn1Oid(&[120]).to_string(), "2.40");
	assert_eq!(&TranslateAsn1Oid(&[129,0]).to_string(), "2.48");
	assert_eq!(&TranslateAsn1Oid(&[128,129,0,0]).to_string(), "<Bad>.0");
	assert_eq!(&TranslateAsn1Oid(&[255,5,7]).to_string(), "2.16181.7");
	assert_eq!(&TranslateAsn1Oid(&[129, 255, 255, 255, 255, 255, 255, 255, 255, 127, 3]).to_string(),
		"2.18446744073709551535.3");
	assert_eq!(&TranslateAsn1Oid(&[130, 128, 128, 128, 128, 128, 128, 128, 128, 0, 3]).to_string(),
		"2.18446744073709551536.3");
	assert_eq!(&TranslateAsn1Oid(&[42, 3, 4, 5]).to_string(), "1.2.3.4.5");
	assert_eq!(&TranslateAsn1Oid(&[42, 129, 3, 4, 5]).to_string(), "1.2.131.4.5");
	assert_eq!(&TranslateAsn1Oid(&[42, 129, 130, 5, 4, 5]).to_string(), "1.2.16645.4.5");
	assert_eq!(&TranslateAsn1Oid(&[42, 128, 129, 130, 5, 4, 5]).to_string(), "1.2.<Bad>.4.5");
	assert_eq!(&TranslateAsn1Oid(&[42, 129, 255, 255, 255, 255, 255, 255, 255, 255, 127, 3]).to_string(),
		"1.2.18446744073709551615.3");
	assert_eq!(&TranslateAsn1Oid(&[42, 130, 128, 128, 128, 128, 128, 128, 128, 128, 0, 3]).to_string(),
		"1.2.18446744073709551616.3");
	assert_eq!(&TranslateAsn1Oid(&[42, 129, 130]).to_string(), "1.2.<Truncated>");
	assert_eq!(&TranslateAsn1Oid(&[105, 127, 127]).to_string(), "2.25.127.127");
	assert_eq!(&TranslateAsn1Oid(&[105, 127]).to_string(), "uuid:00000000-0000-0000-0000-00000000007f");
	assert_eq!(&TranslateAsn1Oid(&[105, 129, 127]).to_string(), "uuid:00000000-0000-0000-0000-0000000000ff");
	assert_eq!(&TranslateAsn1Oid(&[105, 255, 127]).to_string(), "uuid:00000000-0000-0000-0000-000000003fff");
	assert_eq!(&TranslateAsn1Oid(&[105, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
		255, 255, 255, 255, 255, 255, 127]).to_string(), "uuid:3fffffff-ffff-ffff-ffff-ffffffffffff");
	assert_eq!(&TranslateAsn1Oid(&[105, 131, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
		255, 255, 255, 255, 255, 255, 255, 127]).to_string(), "uuid:ffffffff-ffff-ffff-ffff-ffffffffffff");
	assert_eq!(&TranslateAsn1Oid(&[105, 130, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128, 128, 128, 128, 128, 0]).to_string(), "uuid:80000000-0000-0000-0000-000000000000");
	assert_eq!(&TranslateAsn1Oid(&[105, 129, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128, 128, 128, 128, 128, 0]).to_string(), "uuid:40000000-0000-0000-0000-000000000000");
	assert_eq!(&TranslateAsn1Oid(&[105, 192, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128, 128, 128, 128, 0]).to_string(), "uuid:20000000-0000-0000-0000-000000000000");
	assert_eq!(&TranslateAsn1Oid(&[105, 160, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128, 128, 128, 128, 0]).to_string(), "uuid:10000000-0000-0000-0000-000000000000");
	assert_eq!(&TranslateAsn1Oid(&[105, 144, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128, 128, 128, 128, 0]).to_string(), "uuid:08000000-0000-0000-0000-000000000000");
	assert_eq!(&TranslateAsn1Oid(&[105, 136, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128, 128, 128, 128, 0]).to_string(), "uuid:04000000-0000-0000-0000-000000000000");
	assert_eq!(&TranslateAsn1Oid(&[105, 132, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128, 128, 128, 128, 0]).to_string(), "uuid:02000000-0000-0000-0000-000000000000");
	assert_eq!(&TranslateAsn1Oid(&[105, 130, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128, 128, 128, 128, 0]).to_string(), "uuid:01000000-0000-0000-0000-000000000000");
	assert_eq!(&TranslateAsn1Oid(&[105, 129, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128, 128, 128, 128, 0]).to_string(), "uuid:00800000-0000-0000-0000-000000000000");
	assert_eq!(&TranslateAsn1Oid(&[105, 192, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128, 128, 128, 0]).to_string(), "uuid:00400000-0000-0000-0000-000000000000");
	assert_eq!(&TranslateAsn1Oid(&[105, 160, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128, 128, 128, 0]).to_string(), "uuid:00200000-0000-0000-0000-000000000000");
	assert_eq!(&TranslateAsn1Oid(&[105, 144, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128, 128, 128, 0]).to_string(), "uuid:00100000-0000-0000-0000-000000000000");
	assert_eq!(&TranslateAsn1Oid(&[105, 136, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128, 128, 128, 0]).to_string(), "uuid:00080000-0000-0000-0000-000000000000");
	assert_eq!(&TranslateAsn1Oid(&[105, 132, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128, 128, 128, 0]).to_string(), "uuid:00040000-0000-0000-0000-000000000000");
	assert_eq!(&TranslateAsn1Oid(&[105, 130, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128, 128, 128, 0]).to_string(), "uuid:00020000-0000-0000-0000-000000000000");
	assert_eq!(&TranslateAsn1Oid(&[105, 129, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128, 128, 128, 0]).to_string(), "uuid:00010000-0000-0000-0000-000000000000");
	assert_eq!(&TranslateAsn1Oid(&[105, 192, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128, 128, 0]).to_string(), "uuid:00008000-0000-0000-0000-000000000000");
	assert_eq!(&TranslateAsn1Oid(&[105, 130, 0]).to_string(), "uuid:00000000-0000-0000-0000-000000000100");
	assert_eq!(&TranslateAsn1Oid(&[105, 129, 0]).to_string(), "uuid:00000000-0000-0000-0000-000000000080");
	assert_eq!(&TranslateAsn1Oid(&[105, 64]).to_string(), "uuid:00000000-0000-0000-0000-000000000040");
	assert_eq!(&TranslateAsn1Oid(&[105, 32]).to_string(), "uuid:00000000-0000-0000-0000-000000000020");
	assert_eq!(&TranslateAsn1Oid(&[105, 16]).to_string(), "uuid:00000000-0000-0000-0000-000000000010");
	assert_eq!(&TranslateAsn1Oid(&[105, 8]).to_string(), "uuid:00000000-0000-0000-0000-000000000008");
	assert_eq!(&TranslateAsn1Oid(&[105, 4]).to_string(), "uuid:00000000-0000-0000-0000-000000000004");
	assert_eq!(&TranslateAsn1Oid(&[105, 2]).to_string(), "uuid:00000000-0000-0000-0000-000000000002");
	assert_eq!(&TranslateAsn1Oid(&[105, 1]).to_string(), "uuid:00000000-0000-0000-0000-000000000001");
}
