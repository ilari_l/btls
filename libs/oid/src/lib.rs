//!ASN.1 OID encoding/decoding routines
//!
//!This crate contains code to encode and decode ASN.1 OBJECT IDENTIFIERs.
//!
//!The important items include:
//!
//! * Encode OID to bytes: [`encode_oid_to_bytes()`](fn.encode_oid_to_bytes.html)
//! * Print an OID in textual form: [`TranslateAsn1Oid`](struct.TranslateAsn1Oid.html)
#![forbid(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]

#[cfg(test)] #[macro_use] extern crate std;
use btls_aux_collections::Vec;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::fail_if_none;
use btls_aux_memory::split_at;
use btls_aux_memory::split_head_tail;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::str::from_utf8_unchecked;


///Error from encoding OID.
///
///This is constructed as error type by [`encode_oid_to_bytes()`](fn.encode_oid_to_bytes.html). It can be printed as
///an error message using the `{}` format operator.
#[derive(Debug,PartialEq,Eq)]
#[non_exhaustive]
pub enum OidEncodeError
{
	///Bad OID syntax.
	///
	///One gets this for things like leading and trailing dot, two consequtive dots.
	BadOidSyntax,
	///Bad UUID syntax.
	BadUuidSyntax,
	///Invalid component number. The argument is number of component.
	UnparseableNumber(usize),
	#[doc(hidden)]
	ComponentTooBig(usize),		//Not used.
	///Less than two components.
	LessThanTwoComponents,
	///First component must be 0, 1 or 2. The argument is component value
	FirstComponentOutOfRange(u64),
	///The second component is out of range. The argument is component value.
	///
	///Note: This is only for first components 0 and 1.
	SecondComponentOutOfRange(u64),
}

impl Display for OidEncodeError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&OidEncodeError::BadOidSyntax => fmt.write_str("Bad OID syntax"),
			&OidEncodeError::BadUuidSyntax => fmt.write_str("Bad UUID syntax"),
			&OidEncodeError::UnparseableNumber(num) =>
				write!(fmt, "OID component #{num} unparseable", num=num.saturating_add(1)),
			&OidEncodeError::ComponentTooBig(num) =>
				write!(fmt, "OID component #{num} too big", num=num.saturating_add(1)),
			&OidEncodeError::LessThanTwoComponents =>
				fmt.write_str("OID must have at least two components"),
			&OidEncodeError::FirstComponentOutOfRange(val) =>
				write!(fmt, "The first OID component is {val}, must be at most 2"),
			&OidEncodeError::SecondComponentOutOfRange(val) =>
				write!(fmt, "The second OID component is {val}, must be at most 39"),
		}
	}
}

//Is valid number?
fn is_valid_number(x: &str) -> bool
{
	if x == "" { return false; }		//Empty is not valid.
	if x == "0" { return true; }		//'0' is the only valid number starting with 0.
	if x.starts_with("0") { return false; }	//Can not otherwise start with 0.
	//Must consist of digits.
	x.as_bytes().iter().all(|&b|b>=48&&b<=57)
}

fn __encode_uuid(uuid: &str) -> Result<Vec<u8>, OidEncodeError>
{
	let mut result = Vec::new();
	let mut bin = [0u8;16];
	let mut hexes = 0;
	let mut last_dash = true;
	for i in uuid.as_bytes().iter() {
		fail_if!(hexes >= 32, OidEncodeError::BadUuidSyntax);
		let hex = match *i {
			x@48..=57 => x - 48,
			x@65..=70 => x - 55,
			x@97..=102 => x - 87,
			45 if !last_dash => { last_dash = true; continue },
			_ => fail!(OidEncodeError::BadUuidSyntax)
		};
		last_dash = false;
		//Hexes is at most 31.
		bin[hexes >> 1] |= hex << 4 - 4 * (hexes % 2);
		hexes += 1;
	}
	fail_if!(last_dash || hexes < 32, OidEncodeError::BadUuidSyntax);
	//Copy bits backwards.
	result.push(bin[15] & 127);
	for i in 7..128 {
		let j = 127 - i;
		//j < 128, so j >> 3 < 16.
		if (bin[j >> 3] >> 7 - (j & 7)) & 1 == 1 {
			//The loop below extends result if it is too short to be in-bounds.
			while i/7 >= result.len() { result.push(128); }
			result[i/7] |= 1 << (i as u32 % 7);
		}
	}
	result.push(105);
	result.reverse();
	Ok(result)
}

//Outright assumes that the decimal input is valid.
fn __decode_u64_besteffort(decimal: &str) -> u64
{
	let mut x: u64 = 0;
	for &b in decimal.as_bytes().iter() { x = x.saturating_mul(10).saturating_add(b as u64 - 48); }
	x
}

//The number added can be at most 128.
fn __add_small_to_component(output: &mut Vec<u8>, mut carry: u8)
{
	for i in 0..output.len() {
		output[i] += carry;
		carry = output[i] >> 7;
		output[i] &= 127;
	}
	if carry > 0 { output.push(carry); }
}

//The number subtracted can be at most 128.
fn __subtract_small_from_component(output: &mut Vec<u8>, mut borrow: u8)
{
	for i in 0..output.len() {
		output[i] = output[i].wrapping_sub(borrow);
		borrow = output[i] >> 7;
		output[i] &= 127;
	}
	if output.ends_with(b"\0") { output.pop(); }
}

fn __multiply_component_by_10(output: &mut Vec<u8>)
{
	let mut carry = 0u8;
	for i in 0..output.len() {
		let r = output[i] as u16 * 10 + carry as u16;
		output[i] = r as u8 & 127;
		carry = (r >> 7) as u8;
	}
	if carry > 0 { output.push(carry); }
}

fn __divide_component_by_10(output: &mut Vec<u8>) -> u8
{
	//It is easier to perform division in bigendian.
	output.reverse();
	let mut carry = 0u16;
	for b in output.iter_mut() {
		carry = 128 * carry + *b as u16;
		*b = (carry / 10) as u8;
		carry %= 10;
	}
	output.reverse();
	if output.ends_with(b"\0") { output.pop(); }
	carry as u8
}

fn __debug_base128(inp: &[u8]) -> u64
{
	let mut x = 0u64;
	let mut s = 1u64;
	for &b in inp.iter() {
		x = x.saturating_add(s.saturating_mul(b as u64));
		s = s.saturating_mul(128);
	}
	x
}

//Outright assumes that the decimal input is valid.
fn __encode_component(decimal: &str, shift80: bool) -> Vec<u8>
{
	//Start encoding into little-endian base-128, then set all the continuation bits and reverse output.
	//Add initial zero to handle decimal=="0" correctly.
	let mut output = Vec::new();
	output.push(0);
	for &b in decimal.as_bytes().iter() {
		__multiply_component_by_10(&mut output);
		__add_small_to_component(&mut output, b - 48);
	}
	//If requesting shift by 80, add it. Then set continuations and reverse to get bigendian.
	if shift80 { __add_small_to_component(&mut output, 80); }
	for i in 1..output.len() { output[i] |= 128; }
	output.reverse();
	output
}

///Encode textual OID into bytes.
///
///The oid to encode is `textual` and it is in textual form (e.g. `1.2.3.4` or
///`uuid:eed79780-e1d5-4c23-9b96-ffa2645dc5cc`).
///
///On success, returns `Ok(oid)`, where `oid` is the binary DER representation of the oid (without header). If the
///encoding is invalid, returns `Err(err)`, where `err` describes the error that happened.
pub fn encode_oid_to_bytes(textual: &str) -> Result<Vec<u8>, OidEncodeError>
{
	if let Some(uuid) = textual.strip_prefix("uuid:") {
		//This is UUID.
		return __encode_uuid(uuid);
	}
	//Not UUID. Split into integers. Every component must be valid integer.
	//For some reason, empty components give BadOidSyntax, not UnparseableNumber.
	fail_if!(textual==""||textual.starts_with(".")||textual.ends_with(".")||textual.find("..").is_some(),
		OidEncodeError::BadOidSyntax);
	let mut components = textual.split('.');
	if let Some(bad) = components.clone().position(|c|!is_valid_number(c)) {
		fail!(OidEncodeError::UnparseableNumber(bad));
	}
	let mut result = Vec::new();
	//The first component must be 0,1 or 2.
	match components.next().ok_or(OidEncodeError::BadOidSyntax)? {
		//For the 0 and 1 cases, doing best-effort decode on second component is enough, because
		//the second component can be at most 39, and __decode_u64_besteffort() can read such
		//integers without fail.
		"0" => {
			let n = components.next().ok_or(OidEncodeError::LessThanTwoComponents)?;
			let second = __decode_u64_besteffort(n);
			fail_if!(second > 39, OidEncodeError::SecondComponentOutOfRange(second));
			result.push(second as u8);
		},
		"1" => {
			let n = components.next().ok_or(OidEncodeError::LessThanTwoComponents)?;
			let second = __decode_u64_besteffort(n);
			fail_if!(second > 39, OidEncodeError::SecondComponentOutOfRange(second));
			result.push(40 + second as u8);
		},
		"2" => {
			let n = components.next().ok_or(OidEncodeError::LessThanTwoComponents)?;
			result.extend_from_slice(&__encode_component(n, true));
		},
		x => fail!(OidEncodeError::FirstComponentOutOfRange(__decode_u64_besteffort(x)))
	}
	//The rest of the components.
	while let Some(n) = components.next() {
		result.extend_from_slice(&__encode_component(n, false));
	}
	Ok(result)
}

fn __is_uuid<'a>(x: &'a [u8]) -> Option<&'a [u8]>
{
	let (head, tail) = split_head_tail(x)?;
	//The thing must be in 2.25. arc.
	fail_if_none!(head != 105);
	//No padding zeroes.
	let first = *tail.first()?;
	fail_if_none!(tail.len() > 1 && first == 128);
	//Check that the thing is just one component.
	let end = tail.iter().position(|&x|x<128)?;
	fail_if_none!(end + 1 != tail.len());
	//Short enough to fit into 128 bits.
	fail_if_none!(tail.len() >= 20 || tail.len() == 19 && first > 131);
	Some(tail)
}

///Structure that implements Display on binary ASN.1 OID.
///
///The standard way to use this is to print `TranslateAsn1Oid(oid)` using the `{}` format modifier.
///
///The OID is printed in textual form. Branch `2.25` (UUIDs) is printed as `uuid:<uuid>`.
#[derive(Copy,Clone,Debug)]
pub struct TranslateAsn1Oid<'a>(pub &'a [u8]);

impl<'a> Display for TranslateAsn1Oid<'a>
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		let x = self.0;
		if x.len() == 0 {
			//Special.
			return fmt.write_str("<Blank>");
		}
		//Handle embedded UUIDs
		if let Some(y) = __is_uuid(x) {
			let mut zval = 0u128;
			for &b in y.iter() { zval = zval << 7 | (b & 0x7F) as u128; }
			let p1 = (zval >> 96) as u32;
			let p2 = (zval >> 80) as u16;
			let p3 = (zval >> 64) as u16;
			let p4 = (zval >> 48) as u16;
			let p5 = (zval >> 32) as u16;
			let p6 = zval as u32;
			return write!(fmt, "uuid:{p1:08x}-{p2:04x}-{p3:04x}-{p4:04x}-{p5:04x}{p6:08x}");
		}
		__print_normal_oid(fmt, x)
	}
}

fn __print_oid_component(fmt: &mut Formatter, x: &[u8], shift80: bool) -> Result<(), FmtError>
{
	//Component can not start with 128.
	if x.starts_with(b"\x80") { return fmt.write_str(".<Bad>"); }
	//More efficient handling for short parts.
	if x.len() <= 9 {
		let mut y = 0;
		for &b in x.iter() { y = y * 128 + b as u64 % 128; }
		if shift80 { y -= 80; }
		return write!(fmt, ".{y}");
	}
	//Don't print huge numbers, that is too slow.
	if x.len() > 130 { return write!(fmt, ".<huge>"); }
	//Brr... The integer is at least 2^63. Perform slowpath conversion suitable for large integers.
	//Start by converting x into base-128 little-endian. Also assemble y as little-endian.
	let mut y = Vec::new();
	let mut x = x.to_vec();
	for b in x.iter_mut() { *b &= 127; }
	x.reverse();
	if shift80 { __subtract_small_from_component(&mut x, 80); }
	while x.len() > 0 { y.push(48 + __divide_component_by_10(&mut x)); }
	y.reverse();
	//All the characters are always valid UTF-8.
	let y = unsafe{from_utf8_unchecked(&y)};
	write!(fmt, ".{y}")
}

fn __split_oid_part<'a>(x: &'a [u8]) -> Option<(&'a [u8], &'a [u8])>
{
	let end = x.iter().position(|&b|b<128)? + 1;
	split_at(x, end).ok()
}

fn __print_normal_oid(fmt: &mut Formatter, x: &[u8]) -> Result<(), FmtError>
{
	if x.len() == 0 { return Ok(()); }
	let mut x = match __split_oid_part(x) {
		Some((y, x)) => {
			match y {
				&[y] if y < 120 => write!(fmt, "{first}.{second}", first=y/40, second=y%40)?,
				&[y] => write!(fmt, "2.{second}", second=y-80)?,
				//Pfft... The first part is bad.	
				[128,..] => fmt.write_str("<Bad>")?,
				y => {
					fmt.write_str("2")?;
					__print_oid_component(fmt, y, true)?;
				}
			};
			x
		},
		None => return fmt.write_str("<Truncated>")
	};
	while let Some((head, tail)) = __split_oid_part(x) {
		__print_oid_component(fmt, head, false)?;
		x = tail;
	}
	if x.len() > 0 { return fmt.write_str(".<Truncated>"); }
	Ok(())
}

#[cfg(test)]
mod test;
