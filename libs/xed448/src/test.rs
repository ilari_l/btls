use super::*;
use std::borrow::ToOwned;
use std::fs::File;
use std::io::Read;
use std::vec::Vec;

#[test]
fn reference_ed448_signature()
{
	let privkey = [
		0xcd, 0x23, 0xd2, 0x4f, 0x71, 0x42, 0x74, 0xe7,
		0x44, 0x34, 0x32, 0x37, 0xb9, 0x32, 0x90, 0xf5,
		0x11, 0xf6, 0x42, 0x5f, 0x98, 0xe6, 0x44, 0x59,
		0xff, 0x20, 0x3e, 0x89, 0x85, 0x08, 0x3f, 0xfd,
		0xf6, 0x05, 0x00, 0x55, 0x3a, 0xbc, 0x0e, 0x05,
		0xcd, 0x02, 0x18, 0x4b, 0xdb, 0x89, 0xc4, 0xcc,
		0xd6, 0x7e, 0x18, 0x79, 0x51, 0x26, 0x7e, 0xb3,
		0x28
	];
	let pubkeyref = [
		0xdc, 0xea, 0x9e, 0x78, 0xf3, 0x5a, 0x1b, 0xf3,
		0x49, 0x9a, 0x83, 0x1b, 0x10, 0xb8, 0x6c, 0x90,
		0xaa, 0xc0, 0x1c, 0xd8, 0x4b, 0x67, 0xa0, 0x10,
		0x9b, 0x55, 0xa3, 0x6e, 0x93, 0x28, 0xb1, 0xe3,
		0x65, 0xfc, 0xe1, 0x61, 0xd7, 0x1c, 0xe7, 0x13,
		0x1a, 0x54, 0x3e, 0xa4, 0xcb, 0x5f, 0x7e, 0x9f,
		0x1d, 0x8b, 0x00, 0x69, 0x64, 0x47, 0x00, 0x14,
		0x00
	];
	let mut pubkey = [0;57];
	let mut sig = [0; 114];
	let msg = [
		0x0c, 0x3e, 0x54, 0x40, 0x74, 0xec, 0x63, 0xb0, 0x26, 0x5e, 0x0c
	];
	let sigref = [
		0x1f, 0x0a, 0x88, 0x88, 0xce, 0x25, 0xe8, 0xd4,
		0x58, 0xa2, 0x11, 0x30, 0x87, 0x9b, 0x84, 0x0a,
		0x90, 0x89, 0xd9, 0x99, 0xaa, 0xba, 0x03, 0x9e,
		0xaf, 0x3e, 0x3a, 0xfa, 0x09, 0x0a, 0x09, 0xd3,
		0x89, 0xdb, 0xa8, 0x2c, 0x4f, 0xf2, 0xae, 0x8a,
		0xc5, 0xcd, 0xfb, 0x7c, 0x55, 0xe9, 0x4d, 0x5d,
		0x96, 0x1a, 0x29, 0xfe, 0x01, 0x09, 0x94, 0x1e,
		0x00, 0xb8, 0xdb, 0xde, 0xea, 0x6d, 0x3b, 0x05,
		0x10, 0x68, 0xdf, 0x72, 0x54, 0xc0, 0xcd, 0xc1,
		0x29, 0xcb, 0xe6, 0x2d, 0xb2, 0xdc, 0x95, 0x7d,
		0xbb, 0x47, 0xb5, 0x1f, 0xd3, 0xf2, 0x13, 0xfb,
		0x86, 0x98, 0xf0, 0x64, 0x77, 0x42, 0x50, 0xa5,
		0x02, 0x89, 0x61, 0xc9, 0xbf, 0x8f, 0xfd, 0x97,
		0x3f, 0xe5, 0xd5, 0xc2, 0x06, 0x49, 0x2b, 0x14,
		0x0e, 0x00
	];
	ed448_pubkey(&mut pubkey, &privkey);
	assert_eq!(&pubkey[..], &pubkeyref[..]);
	ed448_sign(&privkey, &pubkey, &msg, &mut sig);
	assert_eq!(&sig[..], &sigref[..]);
	test_ed448_sig(pubkey, &msg, sig);
}

fn test_ed448_sig(pubkey: [u8;57], msg: &[u8], sig: [u8;114])
{
	assert!(ed448_verify(&pubkey, msg, &sig).is_ok());
	let mut nsg = msg[..].to_owned();
	for i in 0..8*nsg.len() {
		nsg[i/8] ^= 1 << i % 8;
		assert!(ed448_verify(&pubkey, &nsg, &sig).is_err());
		nsg[i/8] ^= 1 << i % 8;
	}
	let mut bubkey = pubkey;
	for i in 0..8*bubkey.len() {
		bubkey[i/8] ^= 1 << i % 8;
		assert!(ed448_verify(&bubkey, msg, &sig).is_err());
		bubkey[i/8] ^= 1 << i % 8;
	}
	let mut bsig = sig;
	for i in 0..8*bsig.len() {
		bsig[i/8] ^= 1 << i % 8;
		assert!(ed448_verify(&pubkey, msg, &bsig).is_err());
		bsig[i/8] ^= 1 << i % 8;
	}
}

#[test]
fn test_callback_one_rngdep()
{
	let mut sig1 = [0;114];
	let mut sig2 = [0;114];
	let mut sig3 = [0;114];
	let mut rng = [0;114];
	let mut privkey = [0;57];
	let mut pubkey = [0;57];
	let mut msg = [0;700];
	//Random keys.
	File::open("/dev/urandom").and_then(|mut fp|{
		fp.read_exact(&mut msg)?;
		fp.read_exact(&mut privkey)?;
		fp.read_exact(&mut rng)?;
		Ok(())
	}).unwrap();
	ed448_pubkey(&mut pubkey, &privkey);
	ed448_sign_callback(&privkey, &pubkey, &mut sig1, &rng, &mut |h|h.append(&msg));
	msg[0] += 1;
	ed448_sign_callback(&privkey, &pubkey, &mut sig2, &rng, &mut |h|h.append(&msg));
	rng[0] += 1;
	ed448_sign_callback(&privkey, &pubkey, &mut sig3, &rng, &mut |h|h.append(&msg));
	//Sig1 should have the same first 57 bytes as sig2, but different from sig3. The last 57 bytes should
	//differ.
	assert!(&sig1[..57] == &sig2[..57]);
	assert!(&sig1[..57] != &sig3[..57]);
	assert!(&sig1[57..] != &sig2[57..]);
	assert!(&sig1[57..] != &sig3[57..]);
	assert!(&sig2[57..] != &sig3[57..]);
}

#[test]
fn test_callback_one_a()
{
	let mut sig = [0;114];
	let mut rng = [0;114];
	let mut privkey = [0;57];
	let mut pubkey = [0;57];
	let mut msg = [0;700];
	//Random keys.
	File::open("/dev/urandom").and_then(|mut fp|{
		fp.read_exact(&mut msg)?;
		fp.read_exact(&mut privkey)?;
		fp.read_exact(&mut rng)?;
		Ok(())
	}).unwrap();
	ed448_pubkey(&mut pubkey, &privkey);
	ed448_sign_callback(&privkey, &pubkey, &mut sig, &rng, &mut |h|h.append(&msg));
	test_ed448_sig(pubkey, &msg, sig);
}

#[test]
fn test_callback_one_b()
{
	let mut sig = [0;114];
	let mut privkey = [0;57];
	let mut pubkey = [0;57];
	let mut msg = [0;700];
	//Random keys.
	File::open("/dev/urandom").and_then(|mut fp|{
		fp.read_exact(&mut msg)?;
		fp.read_exact(&mut privkey)?;
		Ok(())
	}).unwrap();
	ed448_pubkey(&mut pubkey, &privkey);
	ed448_sign(&privkey, &pubkey, &msg, &mut sig);
	assert!(ed448_verify_callback(&pubkey, &mut sig, &mut |h|h.append(&msg)).is_ok());
	assert!(ed448_verify_callback(&pubkey, &mut sig, &mut |h|h.append(&msg[..699])).is_err());
}

#[test]
fn test_splitting()
{
	let mut sig = [0;114];
	let mut privkey = [0;57];
	let mut pubkey = [0;57];
	let mut msg = [0;7000];
	//Random keys.
	File::open("/dev/urandom").and_then(|mut fp|{
		fp.read_exact(&mut msg)?;
		fp.read_exact(&mut privkey)?;
		Ok(())
	}).unwrap();
	ed448_pubkey(&mut pubkey, &privkey);
	ed448_sign(&privkey, &pubkey, &msg, &mut sig);
	assert!(ed448_verify_callback(&pubkey, &mut sig, &mut |h|{
		h.append(&msg[..127]);
		h.append(&msg[127..383]);
		h.append(&msg[383..512]);
		h.append(&msg[512..1024]);
		h.append(&msg[1024..]);
	}).is_ok());
}

fn test_ed448_once()
{
	let mut privkey = [0;57];
	let mut pubkey = [0;57];
	let mut sig = [0;114];
	let mut msg = Vec::new();

	//Random keys.
	File::open("/dev/urandom").and_then(|mut fp|{
		let mut msglen = [0];
		fp.read_exact(&mut msglen)?;
		msg.resize(msglen[0] as usize, 0);
		fp.read_exact(&mut msg)?;
		fp.read_exact(&mut privkey)
	}).unwrap();
	ed448_pubkey(&mut pubkey, &privkey);
	ed448_sign(&privkey, &pubkey, &msg, &mut sig);
	test_ed448_sig(pubkey, &msg, sig);
}


fn test_key_agreement_once()
{
	let mut base = [0;56];
	base[0] = 5;
	let mut apriv = [0;56]; let mut bpriv = [0;56]; let mut cpriv = [0;56];
	let mut apub = [0;56]; let mut bpub = [0;56]; let mut cpub = [0;56];
	let mut apub2 = [0;56]; let mut bpub2 = [0;56]; let mut cpub2 = [0;56];
	let mut abshr = [0;56]; let mut bashr = [0;56];
	let mut acshr = [0;56]; let mut cashr = [0;56];
	let mut bcshr = [0;56]; let mut cbshr = [0;56];
	//Random keys.
	File::open("/dev/urandom").and_then(|mut fp|{
		fp.read_exact(&mut apriv)?;
		fp.read_exact(&mut bpriv)?;
		fp.read_exact(&mut cpriv)
	}).unwrap();
	//Public keys. Calculate both using generate and agree. The two better match up!
	x448_generate(&mut apub2, &apriv);
	x448_generate(&mut bpub2, &bpriv);
	x448_generate(&mut cpub2, &cpriv);
	x448_agree(&mut apub, &apriv, &base);
	x448_agree(&mut bpub, &bpriv, &base);
	x448_agree(&mut cpub, &cpriv, &base);
	assert_eq!(&apub[..], &apub2[..]);
	assert_eq!(&bpub[..], &bpub2[..]);
	assert_eq!(&cpub[..], &cpub2[..]);
	//Calculate all pairwise products. The sides better commute, but the keys better not!
	x448_agree(&mut abshr, &apriv, &bpub);
	x448_agree(&mut bashr, &bpriv, &apub);
	x448_agree(&mut acshr, &apriv, &cpub);
	x448_agree(&mut cashr, &cpriv, &apub);
	x448_agree(&mut bcshr, &bpriv, &cpub);
	x448_agree(&mut cbshr, &cpriv, &bpub);
	assert_eq!(&abshr[..], &bashr[..]); assert_eq!(&acshr[..], &cashr[..]); assert_eq!(&bcshr[..], &cbshr[..]);
	assert_ne!(&abshr[..], &acshr[..]); assert_ne!(&abshr[..], &bcshr[..]); assert_ne!(&acshr[..], &bcshr[..]);
}

#[test]
fn x448_3wise_1k()
{
	for _ in 0..1000 { test_key_agreement_once(); }
}

#[test]
fn ed448_25()
{
	for _ in 0..25 { test_ed448_once(); }
}
