//!Low level implementation of `Ed448` and `X448`.
//!
//!This crate provodes a low-level implementations of the `Ed448` signature algorithm, and the `X448` key
//!agreement.
//!
//!The most important items are:
//!
//! * Generate a X448 public key from private key: [`x448_generate`](fn.x448_generate.html)
//! * Perform a X448 key agreement: [`x448_agree`](fn.x448_agree.html)
//! * Generate a Ed448 public key from private key: [`ed448_pubkey`](fn.ed448_pubkey.html)
//! * Sign a message using Ed448: [`ed448_sign`](fn.ed448_sign.html)
//! * Verify a message using Ed448: [`ed448_verify`](fn.ed448_verify.html)
#![forbid(missing_docs)]
#![allow(unsafe_code)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]
#[cfg(test)] #[macro_use] extern crate std;

use btls_aux_xed448_cimpl::curved448_scalarmult_basepoint_xed448;
use btls_aux_xed448_cimpl::curved448_scalarmult_xed448;
use btls_aux_xed448_cimpl::ed448_publickey_xed448;
use btls_aux_xed448_cimpl::ed448_sign_open_xed448;
use btls_aux_xed448_cimpl::ed448_sign_xed448;
use btls_aux_xed448_cimpl::ed448_sign_callback_xed448;
use btls_aux_xed448_cimpl::ed448_verify_callback_xed448;
pub use btls_aux_xed448_cimpl::Hasher;

///Generates the X448 public key corresponding to a private key.
///
///The private key is `privkey` and the result is written into `pubkey`. The input private key should be randomly
///and uniformly chosen over all octet strings of appropriate length.
pub fn x448_generate(pubkey: &mut [u8;56], privkey: &[u8;56])
{
	unsafe{curved448_scalarmult_basepoint_xed448(pubkey.as_mut_ptr(), privkey.as_ptr())};
}

///Perform X448 key agreement.
///
///The private key is `privkey`, the peer public key is `peerpub` and the result is written in `shared`.
///
///The key agreement has the the following properties:
///
/// * It is comutative with respect to exchange of participants. That is, the result of key agreement between
///private key A and public key B, and private key B and public key A, is identical.
/// * It is computationally very hard to compute the key exchange result from just the public keys (without a
///quantum computer).
///
///Use [`x448_generate`](fn.x448_generate.html) to compute public key from private key.
///
///Note that after performing the key agreement, one should check that the result is not all zeroes. The all-zeroes
///output can be triggered by certain invalid public keys. While secure key exchange protocols can withstand such
///failures, more poorly done ones (e.g., the original `TLS 1.2` key exchange) are broken in such circumstances.
pub fn x448_agree(shared: &mut [u8;56], privkey: &[u8;56], peerpub: &[u8;56])
{
	unsafe{curved448_scalarmult_xed448(shared.as_mut_ptr(), privkey.as_ptr(), peerpub.as_ptr())};
}

///Generates the `Ed448` public key corresponding to a private key.
///
///The private key is provoded as array `privkey` and the resulting public key is written into array `pubkey`.
///Both public and private keys are always 57 octets. The input private key should be randomly and uniformly
///chosen over all octet strings of length 57.
///
///The private key is `privkey` and the result is written into `pubkey`. The input private key should be randomly
///and uniformly chosen over all octet strings of appropriate length.
pub fn ed448_pubkey(pubkey: &mut [u8; 57], privkey: &[u8; 57])
{
	unsafe{ed448_publickey_xed448(privkey.as_ptr(), pubkey.as_mut_ptr())};
}

///Sign a piece of data using Ed448 private key.
///
///The private key is `privkey`, the corresponding public key is `pubkey`, the message is `message` and the
///signature is written to `signature`.
///
///The message can be of any length.
///
///Use [`ed448_pubkey`](fn.ed448_pubkey.html) to compute public keys from private keys.
///
///Note that if the private key and public key passed do not correspond to one another, this function will silently
///compute incorrect signature.
///
///Also note that these signature may be forged using a quantum computer.
pub fn ed448_sign(privkey: &[u8; 57], pubkey: &[u8; 57], message: &[u8], signature: &mut [u8;114])
{
	unsafe{ed448_sign_xed448(message.as_ptr(), message.len(), privkey.as_ptr(), pubkey.as_ptr(),
		signature.as_mut_ptr())};
}

///Like `ed448_sign()`, but takes the message via callback.
///
///Also, instead of deterministically generating the urnonce, this takes 114 random bytes that *MUST BE FULLY
///RANDOMLY SET FOR EACH SIGNATURE OR ALL SECURITY IS LOST*.
pub fn ed448_sign_callback(privkey: &[u8; 57], pubkey: &[u8; 57], signature: &mut [u8;114], random: &[u8; 114],
	message: &mut dyn FnMut(&mut Hasher))
{
	unsafe{ed448_sign_callback_xed448(privkey.as_ptr(), pubkey.as_ptr(), signature.as_mut_ptr(),
		random.as_ptr(), message)};
}

///Verifies an Ed448 signature on piece of data using public key.
///
///The public key is `pubkey`, the message is `message` and the purported signature is `signature`.
///
///On success, returns `Ok(())`. If the signature does not verify, returns `Err(())`.
///
///Note that the signature does not bind the public key due to existence of 4 weak keys. That is, there can be
///multiple messages and public keys that produce the same signature. However, any such signature must purport to
///be using one of the weak keys, which are listed below:
///
/// * 000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
/// * 000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000080
/// * 010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
/// * FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
///
///Note that flipping the most significant bit of the last octet and/or the least significant bit of the first octet
///in any weak key either gives another weak key, or an invalid key where there are no accepted signatures. This
///can be used to reduce the above set to 2 masked keys.
pub fn ed448_verify(pubkey: &[u8; 57], message: &[u8], signature: &[u8;114]) -> Result<(), ()>
{
	let r = unsafe{ed448_sign_open_xed448(message.as_ptr(), message.len(), pubkey.as_ptr(), signature.as_ptr())};
	if r < 0 { Err(()) } else { Ok(()) }
}

///Like `ed448_verify()`, but takes the message via callback.
pub fn ed448_verify_callback(pubkey: &[u8; 57], signature: &[u8;114], message: &mut dyn FnMut(&mut Hasher)) ->
	Result<(), ()>
{
	let r = unsafe{ed448_verify_callback_xed448(pubkey.as_ptr(), signature.as_ptr(), message)};
	if r < 0 { Err(()) } else { Ok(()) }
}

#[cfg(test)]
mod test;
