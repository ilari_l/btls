//!Low-level implementation of X448 and Ed448 cryptographic primitives.
//!
//!This is its own crate because this crate is a "highlander"; there can be only one crate in the entiere program.
//!Thus it must remain semver-compatible, which is much easier if the crate does as little as possible.
//!
//!It is only meant to be used by the [`btls-aux-xed448`](../btls_aux_xed448/index.html) crate.
#![allow(unsafe_code)]
#![allow(improper_ctypes)]
#![allow(unknown_lints)]
#![allow(bare_trait_objects)]
#![no_std]

use core::mem::transmute;
use core::ptr::write_volatile;
use core::slice::from_raw_parts_mut;

#[cfg(feature="use_collections")]
mod x {
	extern crate btls_aux_collections;

	pub fn _call_only_once_xed448(func: extern fn() -> ())
	{
		use self::btls_aux_collections::Once;
		static X: Once = Once::new();
		X.call_once(||{func();});
	}
}

#[cfg(all(feature="threadsafe", not(feature="use_collections")))]
mod x {
	extern crate std;

	#[allow(deprecated)]
	pub fn _call_only_once_xed448(func: extern fn() -> ())
	{
		static X: self::std::sync::Once = std::sync::ONCE_INIT;
		X.call_once(||{func();});
	}
}
#[cfg(all(not(feature="threadsafe"), not(feature="use_collections")))]
mod x {
	#[allow(deprecated)] const ATOMIC_USIZE_INIT: AtomicUsize = ::core::sync::atomic::ATOMIC_USIZE_INIT;
	use core::sync::atomic::AtomicUsize;
	use core::sync::atomic::Ordering;

	struct Once(AtomicUsize);
	const ONCE_INIT: Once = Once(ATOMIC_USIZE_INIT);
	impl Once
	{
		pub fn call_once<F>(&'static self, cb: F) where F: FnOnce()
		{
			//Completed?
			if self.0.load(Ordering::Acquire) > 1 { return; }
			//Should we run the callback?
			if self.0.compare_exchange(0, 1, Ordering::AcqRel, Ordering::Acquire) == Ok(0) {
				cb();
				self.0.store(2, Ordering::Release);
				return;
			}
			//Wait for call to finish. The value can not be 0, because the compare_and_swap()
			//above would have branched. And if value is >1, it means we raced with complete
			//initialization, so we can complete immediately.
			while self.0.load(Ordering::Acquire) == 1 { }
		}
	}

	pub fn _call_only_once_xed448(func: extern fn() -> ())
	{
		static X: Once = ONCE_INIT;
		X.call_once(||{func();});
	}
}

#[doc(hidden)]
#[no_mangle]
pub unsafe extern fn zeroize_xed448(ptr: *mut u8, len: usize)
{
	let slice = from_raw_parts_mut(ptr, len);
	for i in slice.iter_mut() {
		//This is safe since u8 is not Drop and the cast happens from reference (so the address is guaranteed
		//to be valid).
		write_volatile(i as *mut u8, 0);
	}
}

#[doc(hidden)]
#[no_mangle]
pub unsafe extern fn call_only_once_xed448(func: extern fn() -> ()) { x::_call_only_once_xed448(func) }


//Dirty trick: This is the context structure for the hash, so it can be directly manipulated via Rust code.
#[repr(C)]
struct HashContext
{
	state: [u64;25],
	__unused1: u64,
	blockfill: u64,
	__unused2: u64,
	__unused3: u64,
}

type HCb = extern fn(*mut (), *const u8, usize);
type Cb = extern fn(HCb, *mut (), *mut ());

struct WrapCb<'a>(&'a mut FnMut(&mut Hasher));

///Message hasher.
pub struct Hasher(*mut ());

impl Hasher
{
	///Append data.
	#[inline(always)]
	pub fn append(&mut self, mfrag: &[u8])
	{ unsafe {
		let ctx: &mut HashContext = transmute(self.0);
		ed448_sha3_input_xed448(ctx, mfrag.as_ptr(), mfrag.len())
	}}
}

extern fn hasher_trampoline(_: HCb, hctx: *mut (), ctx: *mut ())
{
	let ctx: &mut WrapCb = unsafe{transmute(ctx)};
	let mut h = Hasher(hctx);
	(*ctx.0)(&mut h);
}

///Do Ed448 signing. Both sk and pk are 57 bytes, sig and rng are 114 bytes.
pub unsafe fn ed448_sign_callback_xed448(sk: *const u8, pk: *const u8, sig: *mut u8,
	rng: *const u8, msg: &mut FnMut(&mut Hasher))
{
	let mut msg = WrapCb(msg);
	let msg = &mut msg;
	sign_callback(sk, pk, sig, rng, hasher_trampoline, transmute(msg))
}

///Do Ed448 verification. Pk is 57 bytes, sig is 114 bytes. Returns 0 if signature
//verifies, -1 if it does not.
pub unsafe fn ed448_verify_callback_xed448(pk: *const u8, sig: *const u8, msg: &mut FnMut(&mut Hasher)) -> i32
{
	let mut msg = WrapCb(msg);
	let msg = &mut msg;
	verify_callback(pk, sig, hasher_trampoline, transmute(msg))
}

extern
{
	///Do X448 basepoint scalarmult. p and s are 56 bytes.
	pub fn curved448_scalarmult_basepoint_xed448(p: *mut u8, s: *const u8);
	///Do X448 variable-base scalarmult. p, s and b are 56 bytes.
	pub fn curved448_scalarmult_xed448(o: *mut u8, s: *const u8, b: *const u8) -> i32;
	///Do Ed448 public from private key genration. Both sk and pk are 57 bytes.
	pub fn ed448_publickey_xed448(sk: *const u8, pk: *mut u8);
	///Do Ed448 signing. Both sk and pk are 57 bytes, sig is 114 bytes. Msg is msglen bytes.
	pub fn ed448_sign_xed448(msg: *const u8, msglen: usize, sk: *const u8, pk: *const u8, sig: *mut u8);
	///Do Ed448 verification. Pk is 57 bytes, sig is 114 bytes. Msg is msglen bytes. Returns 0 if signature
	//verifies, -1 if it does not.
	pub fn ed448_sign_open_xed448(msg: *const u8, msglen: usize, pk: *const u8, sig: *const u8) -> i32;
	#[link_name="ed448_sign_with_callback_xed448"]
	fn sign_callback(sk: *const u8, pk: *const u8, sig: *mut u8, rng: *const u8, hc: Cb, hcctx: *mut ());
	#[link_name="ed448_verify_with_callback_xed448"]
	fn verify_callback(pk: *const u8, sig: *const u8, hc: Cb, hcctx: *mut ()) -> i32;
	fn ed448_sha3_input_xed448(ctx: &mut HashContext, msg: *const u8, msglen: usize);
}
