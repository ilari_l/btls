extern crate cc;
use cc::Build;
use std::panic::catch_unwind;
use std::env::var;

fn main()
{
	let support_128bit_type = {
		let mut config = Build::new();
		config.file("src/test64.c");
		let mut toolcmd = config.get_compiler().to_command();
		toolcmd.arg("-o");
		toolcmd.arg(&format!("{}/test.o", var("OUT_DIR").unwrap()));
		toolcmd.arg("-c");
		toolcmd.arg("src/test64.c");
		toolcmd.output().map(|x|x.status.success()).unwrap_or(false)
	};
	let x = catch_unwind(||{
		if !support_128bit_type { panic!("64-bit version needs 128-bit type"); }
		let mut config = Build::new();
		config.file("src/xed448.c");
		config.define("USE_64BIT_MATH", None);
		config.compile("libxed448.a");
	});
	if x.is_err() {
		//Fall back to 32-bit.
		let mut config = Build::new();
		config.file("src/xed448.c");
		config.compile("libxed448.a");
	}
}
