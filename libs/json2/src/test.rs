use super::*;
use btls_aux_collections::BTreeMap;
use btls_aux_collections::String;
use btls_aux_collections::ToOwned;
use btls_aux_collections::ToString;
use core::iter::FromIterator;
use core::str::from_utf8;

fn simulate_token_to_string(x: &str) -> Result<String,()>
{
	match JSON::parse(x) {
		Ok(JSON::String(s)) => Ok(s),
		Ok(_) => panic!("What is this thing?"),
		Err(_) => Err(())
	}
}

#[test]
fn test_json_string_unescape()
{
	simulate_token_to_string("").unwrap_err();
	simulate_token_to_string("foo").unwrap_err();
	simulate_token_to_string("\"foo").unwrap_err();
	simulate_token_to_string("foo\"").unwrap_err();
	simulate_token_to_string("\"f\\x006go\"").unwrap_err();
	assert_eq!(simulate_token_to_string("\"\"").unwrap(), format!(""));
	assert_eq!(simulate_token_to_string("\"foo\"").unwrap(), format!("foo"));
	assert_eq!(simulate_token_to_string("\"f\\\\o\"").unwrap(), format!("f\\o"));
	assert_eq!(simulate_token_to_string("\"f\\\"o\"").unwrap(), format!("f\"o"));
	assert_eq!(simulate_token_to_string("\"f\\bo\"").unwrap(), format!("f\x08o"));
	assert_eq!(simulate_token_to_string("\"f\\fo\"").unwrap(), format!("f\x0Co"));
	assert_eq!(simulate_token_to_string("\"f\\no\"").unwrap(), format!("f\no"));
	assert_eq!(simulate_token_to_string("\"f\\ro\"").unwrap(), format!("f\ro"));
	assert_eq!(simulate_token_to_string("\"f\\to\"").unwrap(), format!("f\to"));
	assert_eq!(simulate_token_to_string("\"f\\u006fo\"").unwrap(), format!("foo"));
	assert_eq!(simulate_token_to_string("\"f\\ud800\\udc00o\"").unwrap(),
		format!("f{c}o", c=char::from_u32(0x10000).unwrap()));
	assert_eq!(simulate_token_to_string("\"f\\udbff\\udffdo\"").unwrap(),
		format!("f{c}o", c=char::from_u32(0x10FFFD).unwrap()));
	simulate_token_to_string("\"\\s\"").unwrap_err();
	simulate_token_to_string("\"\\x6666\"").unwrap_err();
	simulate_token_to_string("\"\\ud800\"").unwrap_err();
	simulate_token_to_string("\"\\udc00\"").unwrap_err();
	simulate_token_to_string("\"\\ud800x\\udc00\"").unwrap_err();
}

#[test]
fn test_json_equals()
{
	assert!(JSON::parse("1").unwrap().as_number().unwrap() == 1f64);
	assert!(JSON::parse("1").unwrap().as_number().unwrap() != 2f64);
	assert!(JSON::parse("false").unwrap() == false);
	assert!(JSON::parse("false").unwrap() != true);
	assert!(JSON::parse("\"foo bar\"").unwrap() == "foo bar");
	assert!(JSON::parse("\"foo bar\"").unwrap() != "foo baz");
}

#[test]
fn test_json_parse()
{
	JSON::parse("").unwrap_err();
	JSON::parse("0,").unwrap_err();
	JSON::parse("True").unwrap_err();
	JSON::parse("true2").unwrap_err();
	JSON::parse("0f5").unwrap_err();
	JSON::parse("[,]").unwrap_err();
	JSON::parse("[1,3,7,]").unwrap_err();
	JSON::parse("{,}").unwrap_err();
	JSON::parse("{\"x\":0,}").unwrap_err();
	JSON::parse("{\"x\":0,\"x\":0}").unwrap_err();
	JSON::parse("{x:0}").unwrap_err();
	//Numbers.
	assert_eq!(JSON::parse("0").unwrap(), JSON::from(0f64));
	assert_eq!(JSON::parse(" 0").unwrap(), JSON::from(0f64));
	assert_eq!(JSON::parse("0 ").unwrap(), JSON::from(0f64));
	assert_eq!(JSON::parse("160 ").unwrap(), JSON::from(160f64));
	assert_eq!(JSON::parse("1.6e2").unwrap(), JSON::from(160f64));
	assert_eq!(JSON::parse("-1.6e2").unwrap(), JSON::from(-160f64));
	assert_eq!(JSON::parse("-1.635e2").unwrap(), JSON::from(-163.5f64));
	assert_eq!(JSON::parse("7.5e-1").unwrap(), JSON::from(0.75f64));
	//Null, False, True
	assert_eq!(JSON::parse("null").unwrap(), JSON::Null);
	assert_eq!(JSON::parse("false").unwrap(), JSON::Boolean(false));
	assert_eq!(JSON::parse("true").unwrap(), JSON::Boolean(true));
	assert_eq!(JSON::parse("true ").unwrap(), JSON::Boolean(true));
	//Strings.
	assert_eq!(JSON::parse("\"foobar\"").unwrap(), JSON::String(format!("foobar")));
	assert_eq!(JSON::parse("\"foo bar\"").unwrap(), JSON::String(format!("foo bar")));
	assert_eq!(JSON::parse("\"foo\\bbar\"").unwrap(), JSON::String(format!("foo\x08bar")));
	assert_eq!(JSON::parse("\"foo\\u00f6bar\"").unwrap(), JSON::String(format!("fooöbar")));
	assert_eq!(JSON::parse("\t  \"foobar\" \t ").unwrap(), JSON::String(format!("foobar")));
	//Arrays.
	assert_eq!(JSON::parse("[]").unwrap(), JSON::Array(vec![]));
	assert_eq!(JSON::parse("[null]").unwrap(), JSON::Array(vec![JSON::Null]));
	assert_eq!(JSON::parse("[1,3,7]").unwrap(), JSON::Array(vec![JSON::from(1f64),
		JSON::from(3f64), JSON::from(7f64)]));
	assert_eq!(JSON::parse("[1  ,   3,  \t 7] ").unwrap(), JSON::Array(vec![JSON::from(1f64),
		JSON::from(3f64), JSON::from(7f64)]));
	assert_eq!(JSON::parse("[1,3,7,\"qux\"]").unwrap(), JSON::Array(vec![JSON::from(1f64),
		JSON::from(3f64), JSON::from(7f64), JSON::String(format!("qux"))]));
	//Dictionaries.
	assert_eq!(JSON::parse("{}").unwrap(), JSON::Object(BTreeMap::new()));
	assert_eq!(JSON::parse("{\"foo\":false}").unwrap(), JSON::Object(BTreeMap::from_iter([
		(format!("foo"), JSON::Boolean(false))].iter().cloned())));
	assert_eq!(JSON::parse("{\"foo\":false,\"bar\":null}").unwrap(), JSON::Object(
		BTreeMap::from_iter([(format!("bar"), JSON::Null), (format!("foo"), JSON::Boolean(false))].
		iter().cloned())));
	assert_eq!(JSON::parse("  {\"foo\":false  ,  \t \"bar\":null}  ").unwrap(), JSON::Object(
		BTreeMap::from_iter([(format!("bar"), JSON::Null), (format!("foo"), JSON::Boolean(false))].
		iter().cloned())));
}

#[test]
fn json_testcases()
{
	use std::fs::File;
	use std::fs::read_dir;
	use std::io::Read;


	let iter = read_dir("src/json-tests").unwrap();
	let mut failed = 0;
	for i in iter {
		let j = i.unwrap();
		let p = j.path();
		let f = j.file_name().into_string().unwrap();
		let mut should_succeed = if f.starts_with("y_") {
			Some(true)
		} else if f.starts_with("n_") {
			Some(false)
		} else if f.starts_with("i_") {
			None
		} else {
			continue
		};
		let mut file = File::open(p).unwrap();
		let mut content = String::new();
		let mut successful = true;
		let mut msg = String::new();
		if successful {
			match file.read_to_string(&mut content) {
				Ok(_) => (),
				Err(_) => {
					successful = false;
					should_succeed = None;	//Don't care about tests that aren't valid UTF-8.
				}
			}
		}
		if successful {
			match super::parse_json(&content) {
				Ok(_) => (),
				Err(e) => {
					msg = e.to_string();
					successful = false
				},
			}
		}
		let (status, delta) = match (successful, should_succeed) {
			(false, Some(false)) => ("PASS (failed as it should)", 0),
			(false, None) => ("IGNORE (got failed)", 0),
			(false, Some(true)) => ("FAILED (expect pass, but failed)", 1),
			(true, Some(false)) => ("FAILED (expect fail, but passed)", 1),
			(true, None) => ("IGNORE (got passed)", 0),
			(true, Some(true)) => ("PASS (passed as it should)", 0),
		};
		if delta > 0 {
			use std::io::stdout;
			use std::io::Write;
			stdout().write_all(format!("{f}: {status} [{msg}]\n").as_bytes()).unwrap();
		}
		failed += delta;
	}
	assert!(failed == 0);
}

#[test]
fn overlong_bom()
{
	let input = [0xF0, 0x8F, 0xBB, 0xBF];
	assert!(from_utf8(&input).is_err());
}

#[test]
fn parse_json_bom()
{
	let input = [0xEF, 0xBB, 0xBF, 0x22, 0x66, 0x6F, 0x6F, 0x22];
	assert_eq!(JSON::parse(from_utf8(&input).unwrap()).unwrap(), JSON::String("foo".to_owned()));
}

#[test]
fn json_casts()
{
	assert_eq!(JSON::parse("false").unwrap().as_boolean(), Some(false));
	assert_eq!(JSON::parse("true").unwrap().as_boolean(), Some(true));
	assert_eq!(JSON::parse("null").unwrap().as_boolean(), None);

	assert_eq!(JSON::parse("null").unwrap().as_null(), Some(()));
	assert_eq!(JSON::parse("false").unwrap().as_null(), None);
	assert_eq!(JSON::parse("true").unwrap().as_null(), None);

	assert_eq!(JSON::parse("false").unwrap().as_number(), None);
	assert_eq!(JSON::parse("true").unwrap().as_number(), None);
	assert_eq!(JSON::parse("5.5").unwrap().as_number().unwrap().float(), 5.5);
	assert_eq!(JSON::parse("12.75").unwrap().as_number().unwrap().float(), 12.75);
	assert_eq!(JSON::parse("123.652e3").unwrap().as_number().unwrap().float(), 123652.0);
	assert_eq!(JSON::parse("-123.652e3").unwrap().as_number().unwrap().float(), -123652.0);
	assert_eq!(JSON::parse("3.0e20").unwrap().as_number().unwrap().float(), 3.0e20);

	assert_eq!(JSON::parse("false").unwrap().as_number(), None);
	assert_eq!(JSON::parse("true").unwrap().as_number(), None);
	assert_eq!(JSON::parse("5.5").unwrap().as_number().unwrap().signed(), None);
	assert_eq!(JSON::parse("12.75").unwrap().as_number().unwrap().signed(), None);
	assert_eq!(JSON::parse("123.652e3").unwrap().as_number().unwrap().signed(), Some(123652));
	assert_eq!(JSON::parse("-123.652e3").unwrap().as_number().unwrap().signed(), Some(-123652));
	assert_eq!(JSON::parse("3.0e20").unwrap().as_number().unwrap().signed(), None);

	assert_eq!(JSON::parse("0").unwrap().as_string(), None);
	assert_eq!(JSON::parse("\"0\"").unwrap().as_string(), Some("0"));
	assert_eq!(JSON::parse("\"foo\"").unwrap().as_string(), Some("foo"));
	assert_eq!(JSON::parse("[\"foo\"]").unwrap().as_string(), None);

	assert_eq!(JSON::parse("\"foo\"").unwrap().as_array().is_none(), true);
	assert_eq!(JSON::parse("[\"foo\"]").unwrap().as_array().unwrap().len(), 1);
	assert_eq!(JSON::parse("[\"foo\",null]").unwrap().as_array().unwrap().len(), 2);

	assert_eq!(JSON::parse("\"foo\"").unwrap().as_object().is_none(), true);
	assert_eq!(JSON::parse("[\"foo\",null]").unwrap().as_object().is_none(), true);
	assert_eq!(JSON::parse("{\"foo\":null}").unwrap().as_object().unwrap().len(), 1);
	assert_eq!(JSON::parse("{\"foo\":false,\"bar\":true}").unwrap().as_object().unwrap().len(), 2);
}


#[test]
fn test_json_parse_num()
{
	assert_eq!(JSON::parse("1234567890123456789").expect("Failed to parse").as_number().
		expect("Not number"), 1234567890123456789u64);
	assert_eq!(JSON::parse("-912345678912345678").expect("Failed to parse").as_number().
		expect("Not number"), -912345678912345678i64);
	assert_eq!(JSON::parse("-1.5").expect("Failed to parse").as_number().
		expect("Not number").float(), -1.5f64);
	assert_eq!(JSON::parse("-1.5e10").expect("Failed to parse").as_number().
		expect("Not number").float(), -1.5e10f64);
}

macro_rules! int_testcase
{
	($i:expr) => { ($i, stringify!($i)) }
}

#[test]
fn test_write_integer()
{
	static TESTCASES: &'static [(i64, &'static str)] = &[
		int_testcase!(-9223372036854775808), int_testcase!(-9223372036854775807),
		int_testcase!(-1000000000000000000), int_testcase!(-999999999999999999),
		int_testcase!(-100000000000000000), int_testcase!(-99999999999999999),
		int_testcase!(-10000000000000000), int_testcase!(-9999999999999999),
		int_testcase!(-1000000000000000), int_testcase!(-999999999999999), int_testcase!(-100000000000000),
		int_testcase!(-99999999999999), int_testcase!(-10000000000000), int_testcase!(-9999999999999),
		int_testcase!(-1000000000000), int_testcase!(-999999999999), int_testcase!(-100000000000),
		int_testcase!(-99999999999), int_testcase!(-10000000000), int_testcase!(-9999999999),
		int_testcase!(-1000000000), int_testcase!(-999999999), int_testcase!(-100000000),
		int_testcase!(-99999999), int_testcase!(-10000000), int_testcase!(-9999999), int_testcase!(-1000000),
		int_testcase!(-999999), int_testcase!(-100000), int_testcase!(-99999),
		int_testcase!(-10000), int_testcase!(-9999), int_testcase!(-1000), int_testcase!(-999),
		int_testcase!(-100), int_testcase!(-99), int_testcase!(-10), int_testcase!(-9), int_testcase!(-1),
		int_testcase!(0), int_testcase!(1), int_testcase!(9), int_testcase!(10), int_testcase!(99),
		int_testcase!(100), int_testcase!(999), int_testcase!(1000), int_testcase!(9999),
		int_testcase!(10000), int_testcase!(99999), int_testcase!(100000), int_testcase!(999999),
		int_testcase!(1000000), int_testcase!(9999999), int_testcase!(10000000), int_testcase!(99999999),
		int_testcase!(100000000), int_testcase!(999999999), int_testcase!(1000000000),
		int_testcase!(9999999999), int_testcase!(10000000000), int_testcase!(99999999999),
		int_testcase!(100000000000), int_testcase!(999999999999), int_testcase!(1000000000000),
		int_testcase!(9999999999999), int_testcase!(10000000000000), int_testcase!(99999999999999),
		int_testcase!(100000000000000), int_testcase!(999999999999999), int_testcase!(1000000000000000),
		int_testcase!(9999999999999999), int_testcase!(10000000000000000), int_testcase!(99999999999999999),
		int_testcase!(100000000000000000), int_testcase!(999999999999999999),
		int_testcase!(1000000000000000000), int_testcase!(9223372036854775807),
	];
	for (k, v) in TESTCASES.iter() { assert_eq!(JSON::from(*k).serialize_string().deref(), *v); }
}

#[test]
fn test_json_write_type()
{
	assert_eq!(JSON::from(()).serialize_string().deref(), "null");
	assert_eq!(JSON::from(false).serialize_string().deref(), "false");
	assert_eq!(JSON::from(true).serialize_string().deref(), "true");
	assert_eq!(JSON::from(42i64).serialize_string().deref(), "42");
	assert_eq!(JSON::from("hello").serialize_string().deref(), "\"hello\"");
	assert_eq!(JSON::from("hello".to_owned()).serialize_string().deref(), "\"hello\"");
	assert_eq!(JSON::from(&"hello".to_owned()).serialize_string().deref(), "\"hello\"");
	assert_eq!(JSON::from(&[JSON::from(1i64), JSON::from(2i64), JSON::from(3i64)][..]).
		serialize_string().deref(), "[1,2,3]");
	assert_eq!(JSON::from([JSON::from(1i64), JSON::from(2i64), JSON::from(3i64)].to_vec()).
		serialize_string().deref(), "[1,2,3]");
	assert_eq!(JSON::from(&[("a", JSON::from(1i64)), ("b", JSON::from(2i64)),
		("c", JSON::from(3i64))][..]).serialize_string().deref(), "{\"a\":1,\"b\":2,\"c\":3}");
	assert_eq!(JSON::from(&[("a".to_owned(), JSON::from(1i64)), ("b".to_owned(), JSON::from(2i64)),
		("c".to_owned(), JSON::from(3i64))][..]).serialize_string().deref(), "{\"a\":1,\"b\":2,\"c\":3}");
}

#[test]
fn test_json_write_string_special()
{
	let teststring = "\
		0:\u{0}1:\u{1}2:\u{2}3:\u{3}4:\u{4}5:\u{5}6:\u{6}7:\u{7}\
		8:\u{8}9:\u{9}a:\u{a}b:\u{b}c:\u{c}d:\u{d}e:\u{e}f:\u{f}\
		10:\u{10}11:\u{11}12:\u{12}13:\u{13}14:\u{14}15:\u{15}16:\u{16}17:\u{17}\
		18:\u{18}19:\u{19}1a:\u{1a}1b:\u{1b}1c:\u{1c}1d:\u{1d}1e:\u{1e}1f:\u{1f}\
		\u{20}!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`\
		abcdefghijklmnopqstuvwxyz{|}~ö\u{5}ö\u{8}ö\u{9}ö\u{a}ö\u{c}ö\u{d}ö\"ö\\ö\u{10000}X";
	let answer = "\"\
		0:\\u00001:\\u00012:\\u00023:\\u00034:\\u00045:\\u00056:\\u00067:\\u0007\
		8:\\b9:\\ta:\\nb:\\u000bc:\\fd:\\re:\\u000ef:\\u000f\
		10:\\u001011:\\u001112:\\u001213:\\u001314:\\u001415:\\u001516:\\u001617:\\u0017\
		18:\\u001819:\\u00191a:\\u001a1b:\\u001b1c:\\u001c1d:\\u001d1e:\\u001e1f:\\u001f\
		\u{20}!\\\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\\\]^_`\
		abcdefghijklmnopqstuvwxyz{|}~ö\\u0005ö\\bö\\tö\\nö\\fö\\rö\\\"ö\\\\ö\u{10000}X\"";
	assert_eq!(JSON::from(teststring).serialize_string().deref(), answer);
	assert_eq!(&JSON::from(&[(teststring,JSON::from(()))][..]).serialize_string()[1..394], answer);
}

#[test]
fn serialize_torture()
{
	let test_input = r####"
{"a":{},"b":{"x":"y"},"c":[],"d":["y"],"e":["x","y"]}
"####;
	JSON::parse(test_input).expect("Should parse").serialize_string();
}

#[test]
fn test_json_object()
{
	assert_eq!(json_object!(type => "dns", value => "foo.example.org", wildcard => true).serialize_string().
		deref(), "{\"type\":\"dns\",\"value\":\"foo.example.org\",\"wildcard\":true}");
	assert_eq!(json_array!(1i64, 2i64, 3i64).serialize_string().deref(), "[1,2,3]");
	assert_eq!(json_value!(false).serialize_string().deref(), "false");
	assert_eq!(json_value!(()).serialize_string().deref(), "null");
	assert_eq!(json_value!(42u64).serialize_string().deref(), "42");
}

#[test]
fn test_parse_integer()
{
	//Some basic integer parsing (including negatives).
	assert_eq!(parse_integer("0"), (Some(0), Some(0)));
	assert_eq!(parse_integer("-0"), (Some(0), Some(0)));
	assert_eq!(parse_integer("-0.000"), (Some(0), Some(0)));
	assert_eq!(parse_integer("1"), (Some(1), Some(1)));
	assert_eq!(parse_integer("-1"), (Some(-1), None));
	assert_eq!(parse_integer("9223372036854775807"), (Some(9223372036854775807), Some(9223372036854775807)));
	assert_eq!(parse_integer("9223372036854775808"), (None, Some(9223372036854775808)));
	assert_eq!(parse_integer("-9223372036854775808"), (Some(-9223372036854775808), None));
	assert_eq!(parse_integer("18446744073709551615"), (None, Some(18446744073709551615)));
	assert_eq!(parse_integer("18446744073709551616"), (None, None));
	//The maximum values in odd way.
	assert_eq!(parse_integer("9.223372036854775807e18"), (Some(9223372036854775807), Some(9223372036854775807)));
	assert_eq!(parse_integer("92.23372036854775807e17"), (Some(9223372036854775807), Some(9223372036854775807)));
	assert_eq!(parse_integer("9.223372036854775808E18"), (None, Some(9223372036854775808)));
	assert_eq!(parse_integer("-9.223372036854775808E18"), (Some(-9223372036854775808), None));
	assert_eq!(parse_integer("1.8446744073709551615e19"), (None, Some(18446744073709551615)));
	assert_eq!(parse_integer("1.8446744073709551616E19"), (None, None));
	//Out of range stuff.
	assert_eq!(parse_integer("1e19"), (None, Some(10000000000000000000)));
	assert_eq!(parse_integer("1e20"), (None, None));
	//Partial shifts.
	assert_eq!(parse_integer("1.2345e19"), (None, Some(12345000000000000000)));
	assert_eq!(parse_integer("1.2345e+19"), (None, Some(12345000000000000000)));
	//Negative exponents.
	assert_eq!(parse_integer("12000e-3"), (Some(12), Some(12)));
	assert_eq!(parse_integer("12000.0e-3"), (Some(12), Some(12)));
	//Non-integer stuff.
	assert_eq!(parse_integer("10100e-3"), (None, None));
	assert_eq!(parse_integer("10000.1e-1"), (None, None));
	assert_eq!(parse_integer("10000.1"), (None, None));
	assert_eq!(parse_integer("10000.123e+2"), (None, None));
	//Zeroes.
	assert_eq!(parse_integer("-0.0000E99999999999999999999999999999999999999999999999"), (Some(0), Some(0)));
	assert_eq!(parse_integer("-0.0000E-9999999999999999999999999999999999999999999999"), (Some(0), Some(0)));
}

#[test]
fn test_json_parse_is_sound()
{
	static TESTCASES: &'static [(i64, &'static str)] = &[
		int_testcase!(-9007199254740993), int_testcase!(9007199254740993),
	];
	for (k, v) in TESTCASES.iter() { assert_eq!(JSON::from(*k).serialize_string().deref(), *v); }
}

#[test]
fn test_json_float_uint()
{
	assert_eq!(Number::new_float(0.0).unsigned(), Some(0));
	assert_eq!(Number::new_float(1.0).unsigned(), Some(1));
	for i in 0..53 {
		let mut x = 0x0000000000000u64;
		let mut y = 0xFFFFFFFFFFFFFu64 >> i << i;
		let x2 = 1 << 52 - i;
		let y2 = (1 << 53 - i) - 1;
		x += (1075 - i) << 52;
		y += (1075 - i) << 52;
		let x = f64::from_bits(x);
		let y = f64::from_bits(y);
		assert_eq!(Number::new_float(x).unsigned(), Some(x2));
		assert_eq!(Number::new_float(y).unsigned(), Some(y2));
	}
	assert_eq!(Number::new_float(9007199254740991.0).unsigned(), Some(9007199254740991));
	assert_eq!(Number::new_float(9007199254740992.0).unsigned(), Some(9007199254740992));
	for i in 0..12 {
		let mut x = 0x0000000000000u64;
		let mut y = 0xFFFFFFFFFFFFFu64;
		let x2 = (1 << 52 | x) << i;
		let y2 = (1 << 52 | y) << i;
		x += (1075 + i) << 52;
		y += (1075 + i) << 52;
		let x = f64::from_bits(x);
		let y = f64::from_bits(y);
		assert_eq!(Number::new_float(x).unsigned(), Some(x2));
		assert_eq!(Number::new_float(y).unsigned(), Some(y2));
	}
	assert_eq!(Number::new_float(18446744073709549568.0).unsigned(), Some(18446744073709549568));
	assert_eq!(Number::new_float(18446744073709551616.0).unsigned(), None);
}

#[test]
fn test_json_float_nint()
{
	assert_eq!(Number::new_float(-0.0).signed(), Some(0));
	assert_eq!(Number::new_float(-1.0).signed(), Some(-1));
	for i in 0..53 {
		let mut x = 0x0000000000000u64;
		let mut y = 0xFFFFFFFFFFFFFu64 >> i << i;
		let x2 = -(1 << 52 - i);
		let y2 = -((1 << 53 - i) - 1);
		x += (3123 - i) << 52;
		y += (3123 - i) << 52;
		let x = f64::from_bits(x);
		let y = f64::from_bits(y);
		assert_eq!(Number::new_float(x).signed(), Some(x2));
		assert_eq!(Number::new_float(y).signed(), Some(y2));
	}
	assert_eq!(Number::new_float(-9007199254740991.0).signed(), Some(-9007199254740991));
	assert_eq!(Number::new_float(-9007199254740992.0).signed(), Some(-9007199254740992));
	for i in 0..11 {
		let mut x = 0x0000000000000u64;
		let mut y = 0xFFFFFFFFFFFFFu64;
		let x2 = -((1 << 52 | x as i64) << i);
		let y2 = -((1 << 52 | y as i64) << i);
		x += (3123 + i) << 52;
		y += (3123 + i) << 52;
		let x = f64::from_bits(x);
		let y = f64::from_bits(y);
		assert_eq!(Number::new_float(x).signed(), Some(x2));
		assert_eq!(Number::new_float(y).signed(), Some(y2));
	}
	assert_eq!(Number::new_float(-9223372036854774784.0).signed(), Some(-9223372036854774784));
	assert_eq!(Number::new_float(-9223372036854775808.0).signed(), Some(-9223372036854775808));
	assert_eq!(Number::new_float(-9223372036854777856.0).signed(), None);
}

#[test]
fn print_json_floats()
{
	assert_eq!(Number::new_float(2251799813685247.25).to_string(), "22517998136852472e-1");
	assert_eq!(Number::new_float(2251799813685247.5).to_string(), "22517998136852475e-1");
	assert_eq!(Number::new_float(2251799813685247.75).to_string(), "22517998136852478e-1");
	assert_eq!(Number::new_float(4503599627370495.5).to_string(), "45035996273704955e-1");
	assert_eq!(Number::new_float(18446744073709551616.0).to_string(), "18446744073709552e3");
	assert_eq!(Number::new_float(18446744073709555712.0).to_string(), "18446744073709556e3");
	assert_eq!(Number::new_float(1.0e21).to_string(), "1e21");
	assert_eq!(Number::new_float(100000000000000008388608.0).to_string(), "100000000000000008e6");
	assert_eq!(Number::new_float(1.0e22).to_string(), "1e22");
}

#[test]
fn print_json_float_nmin_e0()
{
	assert_eq!(Number::new_float(1.0000000000000002).to_string(), "10000000000000002e-16");
}

struct PrintNumberBits(u64);

impl Display for PrintNumberBits
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		let (sign,m,mag) = Number::encode_value(self.0);
		if m == !0 { return f.write_str(if sign { "-inf" } else { "inf" }); }
		if m == !1 { return f.write_str("NaN"); }
		if m == 0 { return f.write_str("0"); }
		if sign { f.write_str("-")?; }
		write!(f, "{m}e{mag}")
	}
}

#[test]
fn print_json_specials()
{
	assert_eq!(PrintNumberBits(0).to_string(), "0");
	assert_eq!(PrintNumberBits(1).to_string(), "5e-324");
	assert_eq!(PrintNumberBits(4503599627370495).to_string(), "22250738585072009e-324");
	assert_eq!(PrintNumberBits(4503599627370496).to_string(), "22250738585072014e-324");
	assert_eq!(PrintNumberBits(9214364837600034815).to_string(), "89884656743115785e291");
	assert_eq!(PrintNumberBits(9214364837600034816).to_string(), "89884656743115795e291");
	assert_eq!(PrintNumberBits(9214364837600034817).to_string(), "89884656743115815e291");
	assert_eq!(PrintNumberBits(9218868437227405310).to_string(), "179769313486231551e291");
	assert_eq!(PrintNumberBits(9218868437227405311).to_string(), "179769313486231571e291");
	assert_eq!(PrintNumberBits(9218868437227405312).to_string(), "inf");
	assert_eq!(PrintNumberBits(9218868437227405313).to_string(), "NaN");

	assert_eq!(PrintNumberBits(0|1<<63).to_string(), "0");
	assert_eq!(PrintNumberBits(1|1<<63).to_string(), "-5e-324");
	assert_eq!(PrintNumberBits(4503599627370495|1<<63).to_string(), "-22250738585072009e-324");
	assert_eq!(PrintNumberBits(4503599627370496|1<<63).to_string(), "-22250738585072014e-324");
	assert_eq!(PrintNumberBits(9214364837600034816|1<<63).to_string(), "-89884656743115795e291");
	assert_eq!(PrintNumberBits(9218868437227405311|1<<63).to_string(), "-179769313486231571e291");
	assert_eq!(PrintNumberBits(9218868437227405312|1<<63).to_string(), "-inf");
	assert_eq!(PrintNumberBits(9218868437227405313|1<<63).to_string(), "NaN");
}

#[test]
fn weird_number()
{
	assert_eq!(Number::parse("3.602879701896397e16").unsigned(), Some(36028797018963970));
}

#[test]
fn json_parse_bad()
{
	let json = "{\"foo\":\u{15}";
	assert_eq!(JSON::parse(json).unwrap_err().to_string(),
		"JSON error at position 7: Expected Digit, '-', 'f', 'n', 't', '\"', '[' or '{', got NAK");
}

#[test]
fn json_parse_bad2()
{
	let json = "{\"isEnabled\":treu}";
	assert_eq!(JSON::parse(json).unwrap_err().to_string(),
		"JSON error at position 15: Expected 'u', got 'e'");
}

#[test]
fn json_parse_bad3()
{
	let json = "{\"foo\":7\u{90}";
	assert_eq!(JSON::parse(json).unwrap_err().to_string(),
		"JSON error at position 8: Expected Whitespace, Digit, '.', 'e', 'E', ',', ']', '}' or End of Input, got DCS");
}

#[test]
fn json_parse_some_acme_stuff()
{
	let authz = include_str!("34856458480.json");
	println!("A: {authz:?}");
	let authz = JSON::parse(authz).expect("No JSON parse");
	println!("B: {authz:?}");
	assert!(authz.field("identifier").is_some());
	assert!(authz.field("status").is_some());
	assert!(authz.field("challenges").is_some());
}

#[test]
fn json_two_nested_objects()
{
	let json = r#"{"foo":{"bar":"baz"}}"#;
	let json = JSON::parse(json).expect("No JSON parse");
	assert_eq!(format!("{json:?}"), r#"Object({"foo": Object({"bar": String("baz")})})"#);
}

#[test]
fn json_parse_serialize()
{
	let json = r#"  {
		"foo"
	:
			[1,
		2    ,   3
]
	, "bar": false, "baz":
null,  "zot": true  }  "#;
	let json = JSON::parse(json).expect("Should be valid");
	let mut out_json = String::new();
	json.serialize(&mut out_json).expect("Should be valid");
	assert_eq!(&out_json, r#"{"bar":false,"baz":null,"foo":[1,2,3],"zot":true}"#);
}


#[test]
fn test_insane_nest2()
{
	let mut json = String::new();
	for _ in 0..MAX_DEPTH { json.push('['); }
	json.push('0');
	for _ in 0..MAX_DEPTH { json.push(']'); }
	let _x = JSON::parse(&json).expect("Should have parsed");
}

#[test]
fn test_insane_nest3()
{
	let mut json = String::new();
	for _ in 0..MAX_DEPTH { json.push_str(r#"{"x":"#); }
	json.push('0');
	for _ in 0..MAX_DEPTH { json.push('}'); }
	let _x = JSON::parse(&json).expect("Should have parsed");
}
