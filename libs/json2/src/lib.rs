//!JSON encoder/decoder
//!
//!This crate implements a JSON encoder and decoder.
#![forbid(unsafe_code)]
#![deny(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]

#[cfg(test)] #[macro_use] extern crate std;
use btls_aux_collections::BTreeMap;
use btls_aux_collections::String;
use btls_aux_collections::ToOwned;
use btls_aux_collections::Vec;
use btls_aux_fail::f_break;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::fail_if_none;
use btls_aux_fail::ttry;
use btls_aux_memory::split_at;
use btls_aux_memory::split_head_tail;
use core::fmt::Debug;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::mem::replace;
use core::ops::Deref;
use core::str::from_utf8;
use core::str::FromStr;

#[derive(Copy,Clone,Debug)]
enum ParseState
{
	///Initial state, before the entiere JSON.
	Initial,
	///Final state, after the entiere JSON.
	Final,
	///Immediately after [
	ArrayStart,
	///Immediately after value in array.
	ArrayValue,
	///Immediately after comma in array.
	ArrayComma,
	///Immediately after {
	ObjectStart,
	///Immediately after key in object.
	ObjectKey,
	///Immediately after colon in array.
	ObjectColon,
	///Immediately after value in object.
	ObjectValue,
	///Immediately after comma in object.
	ObjectComma,
}

#[derive(Copy,Clone,Debug)]
enum NumberState
{
	///Initial.
	Init,
	///Minus.
	Minus,
	///Zero.
	Zero,
	///Integer.
	Integer,
	///Immediately after dot.
	Dot,
	///Decimal.
	Decimal,
	///Immediately after e.
	Exp,
	///Immediately after exp sign.
	ExpSign,
	///Exponent.
	Exponent,
}

fn unwrap_hex(p: usize, x: Option<char>) -> Result<u32, ParseError>
{
	Ok(x.and_then(|y|y.to_digit(16)).ok_or((p,_ParseError::EscapeHex(x)))?)
}

#[derive(Clone)]
struct StringCursor<'a>(&'a str, usize);

impl<'a> StringCursor<'a>
{
	fn peek(&self) -> Option<char>
	{
		self.0.chars().next()
	}
	fn next(&mut self) -> Option<char>
	{
		let c = self.0.chars().next()?;
		let l = c.len_utf8();
		//This must be true.
		if self.0.is_char_boundary(l) { self.0 = &self.0[l..]; self.1 += l; }
		Some(c)
	}
	fn p(&self) -> usize { self.1 }
	fn decode_json_string_escape(&mut self) -> Result<Result<char,u32>,ParseError>
	{
		self.next();	//Pop the backslash, this can be assumed infallible.
		let p = self.1;
		match self.next() {
			Some('b') => return Ok(Ok('\u{8}')),
			Some('t') => return Ok(Ok('\u{9}')),
			Some('n') => return Ok(Ok('\u{a}')),
			Some('f') => return Ok(Ok('\u{c}')),
			Some('r') => return Ok(Ok('\u{d}')),
			Some('"') => return Ok(Ok('"')),
			Some('/') => return Ok(Ok('/')),
			Some('\\') => return Ok(Ok('\\')),
			Some('u') => (),	//More processing.
			c => fail!((p,_ParseError::BadEscape(c)))
		};
		let c4 = unwrap_hex(self.1, self.next())?;
		let c3 = unwrap_hex(self.1, self.next())?;
		let c2 = unwrap_hex(self.1, self.next())?;
		let c1 = unwrap_hex(self.1, self.next())?;
		let c = c4 * 4096 + c3 * 256 + c2 * 16 + c1;
		if let Some(c) = char::from_u32(c) { return Ok(Ok(c)); }
		Ok(Err(c))
	}
	fn decode_json_string(&mut self) -> Result<String,ParseError>
	{
		let mut y = String::new();
		let mut b = self.0.as_bytes();
		while b.len() > 0 && b[0] != 34 {
			//Characters <32 are not allowed in strings.
			fail_if!(b[0] < 32, (self.1,_ParseError::IllegalStringChar(b[0])));
			if b[0] == 92 {
				//Escape.
				let ep = self.1;
				let h = match self.decode_json_string_escape()? {
					Ok(c) => {
						y.push(c);
						b = self.0.as_bytes();	//Sync b.
						continue;
					},
					Err(h) => h,
				};
				//This is surrogate.
				fail_if!(h >> 10 != 54, (ep,_ParseError::UnpairedSurrogate(h)));
				let ep2 = self.1;
				let l = match self.decode_json_string_escape()? {
					//Success means this is not a surrogate, so fail.
					Ok(_) => fail!((ep,_ParseError::UnpairedSurrogate(h))),
					Err(l) => l,
				};
				fail_if!(l >> 10 != 55, (ep2,_ParseError::UnpairedSurrogate(l)));
				let c = h * 1024 + l - 56613888;
				//This should succeed... If not, blame the later surrogate.
				let c = char::from_u32(c).ok_or((ep2,_ParseError::UnpairedSurrogate(l)))?;
				y.push(c);
				b = self.0.as_bytes();	//Sync b.
				continue;
			}
			//Not escape, find length that can be fast-copied, and fast-copy it.
			let mut l = 0;
			fn string_not_special(b: u8) -> bool { b >= 32 && b != b'\"' && b != b'\\' }
			while l < b.len() && string_not_special(b[l]) { l += 1; }
			if let Ok((head,tail)) = split_at(self.0, l) {		//Better be true!
				y.push_str(head);
				self.0 = tail;
				self.1 += head.len();
				b = tail.as_bytes();
			}
		}
		Ok(y)
	}
	fn decode_json_number(self) -> Result<(usize,Number),ParseError>
	{
		//Number is terminated by comma, righ bracket, whitespace or end of input.
		fn continue_number(b:u8) -> bool { !matches!(b,b'\t'|b'\n'|b'\r'|b' '|b','|b']'|b'}') }
		let b = self.0.as_bytes();
		let mut l = 0;
		while l<b.len()&&continue_number(b[l]) { l += 1; }
		//l is always a character boundary.
		let (n,tail) = split_at(self.0, l).unwrap_or(("",""));
		let mut s = NumberState::Init;
		for (i,c) in n.char_indices() { match s {
			NumberState::Init => match c {
				'-' => s = NumberState::Minus,
				'0' => s = NumberState::Zero,
				'1'..='9' => s = NumberState::Integer,
				_ => fail!((self.1+i,_ParseError::BadNum(s,Some(c))))
			},
			NumberState::Minus => match c {
				'-' => s = NumberState::Minus,
				'0' => s = NumberState::Zero,
				'1'..='9' => s = NumberState::Integer,
				_ => fail!((self.1+i,_ParseError::BadNum(s,Some(c))))
			},
			NumberState::Zero => match c {
				'.' => s = NumberState::Dot,
				'e'|'E' => s = NumberState::Exp,
				_ => fail!((self.1+i,_ParseError::BadNum(s,Some(c))))
			},
			NumberState::Integer => match c {
				'0'..='9' => (),
				'.' => s = NumberState::Dot,
				'e'|'E' => s = NumberState::Exp,
				_ => fail!((self.1+i,_ParseError::BadNum(s,Some(c))))
			},
			NumberState::Dot => match c {
				'0'..='9' => s = NumberState::Decimal,
				_ => fail!((self.1+i,_ParseError::BadNum(s,Some(c))))
			},
			NumberState::Decimal => match c {
				'0'..='9' => (),
				'e'|'E' => s = NumberState::Exp,
				_ => fail!((self.1+i,_ParseError::BadNum(s,Some(c))))
			},
			NumberState::Exp => match c {
				'0'..='9' => s = NumberState::Exponent,
				'+'|'-' => s = NumberState::ExpSign,
				_ => fail!((self.1+i,_ParseError::BadNum(s,Some(c))))
			},
			NumberState::ExpSign => match c {
				'0'..='9' => s = NumberState::Exponent,
				_ => fail!((self.1+i,_ParseError::BadNum(s,Some(c))))
			},
			NumberState::Exponent => match c {
				'0'..='9' => (),
				_ => fail!((self.1+i,_ParseError::BadNum(s,Some(c))))
			}
		}}
		match s {
			NumberState::Zero|NumberState::Integer|NumberState::Decimal|NumberState::Exponent =>
				Ok((l-1,Number::parse(n))),
			_ => fail!((self.1+l,_ParseError::BadNum(s,tail.chars().next()))),
		}
	}
	fn check(&mut self, reference: &str) -> Result<(),ParseError>
	{
		for r in reference.chars() {
			let p = self.1;
			let c = self.next();
			fail_if!(c != Some(r), (p,_ParseError::Mismatch(r,c)));
		}
		Ok(())
	}
	fn discard_bytes(&mut self, l: usize)
	{
		//Assume this is true.
		if let Some(rest) = self.0.get(l..) { self.0 = rest; self.1 += l; }
	}
	fn discard_json_whitespace(&mut self)
	{
		let mut l = 0;
		let mut itr = self.0.chars();
		while let Some(c) = itr.next() { match c {
			//These all are 1 byte.
			'\t'|'\n'|'\r'|' ' => l += 1,
			_ => break
		}}
		//This must be true.
		if let Some(rest) = self.0.get(l..) { self.0 = rest; self.1 += l; }
	}
}

enum ParseContextItem
{
	Array(Vec<JSON>),
	Object(BTreeMap<String,JSON>, Option<String>),
}

struct PrintChar(Option<char>);

impl Display for PrintChar
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self.0 {
			Some('\u{0}') => write!(f, "NUL"), Some('\u{1}') => write!(f, "SOH"),
			Some('\u{2}') => write!(f, "STX"), Some('\u{3}') => write!(f, "ETX"),
			Some('\u{4}') => write!(f, "EOT"), Some('\u{5}') => write!(f, "ENQ"),
			Some('\u{6}') => write!(f, "ACK"), Some('\u{7}') => write!(f, "BEL"),
			Some('\u{8}') => write!(f, "BS"),  Some('\u{9}') => write!(f, "HT"),
			Some('\u{a}') => write!(f, "LF"),  Some('\u{b}') => write!(f, "VT"),
			Some('\u{c}') => write!(f, "FF"),  Some('\u{d}') => write!(f, "CR"),
			Some('\u{e}') => write!(f, "SO"),  Some('\u{f}') => write!(f, "SI"),
			Some('\u{10}') => write!(f, "DLE"), Some('\u{11}') => write!(f, "DC1"),
			Some('\u{12}') => write!(f, "DC2"), Some('\u{13}') => write!(f, "DC3"),
			Some('\u{14}') => write!(f, "DC4"), Some('\u{15}') => write!(f, "NAK"),
			Some('\u{16}') => write!(f, "SYN"), Some('\u{17}') => write!(f, "ETB"),
			Some('\u{18}') => write!(f, "CAN"), Some('\u{19}') => write!(f, "EM"),
			Some('\u{1a}') => write!(f, "SUB"), Some('\u{1b}') => write!(f, "ESC"),
			Some('\u{1c}') => write!(f, "FS"),  Some('\u{1d}') => write!(f, "GS"),
			Some('\u{1e}') => write!(f, "RS"),  Some('\u{1f}') => write!(f, "US"),
			Some('\u{7f}') => write!(f, "DEL"),
			Some('\u{80}') => write!(f, "U80"), Some('\u{81}') => write!(f, "U81"),
			Some('\u{82}') => write!(f, "BPH"), Some('\u{83}') => write!(f, "NBH"), 
			Some('\u{84}') => write!(f, "U84"), Some('\u{85}') => write!(f, "NEL"),
			Some('\u{86}') => write!(f, "SSA"), Some('\u{87}') => write!(f, "ESA"),
			Some('\u{88}') => write!(f, "CTS"), Some('\u{89}') => write!(f, "CTJ"),
			Some('\u{8a}') => write!(f, "LTS"), Some('\u{8b}') => write!(f, "PLF"),
			Some('\u{8c}') => write!(f, "PLB"), Some('\u{8d}') => write!(f, "RLF"),
			Some('\u{8e}') => write!(f, "SS2"), Some('\u{8f}') => write!(f, "SS3"),
			Some('\u{90}') => write!(f, "DCS"), Some('\u{91}') => write!(f, "PU1"),
			Some('\u{92}') => write!(f, "PU2"), Some('\u{93}') => write!(f, "STS"),
			Some('\u{94}') => write!(f, "CC"),  Some('\u{95}') => write!(f, "MW"),
			Some('\u{96}') => write!(f, "SGA"), Some('\u{97}') => write!(f, "EGA"),
			Some('\u{98}') => write!(f, "SOS"), Some('\u{99}') => write!(f, "U99"),
			Some('\u{9a}') => write!(f, "SCI"), Some('\u{9b}') => write!(f, "CSI"),
			Some('\u{9c}') => write!(f, "ST"),  Some('\u{9d}') => write!(f, "OSC"),
			Some('\u{9e}') => write!(f, "PM"),  Some('\u{9f}') => write!(f, "APC"),
			Some(c) => write!(f, "'{c}'"),
			None => write!(f, "End of Input"),
		}
	}
}

///Error class.
#[derive(Copy,Clone,PartialEq,Eq,Debug)]
pub enum ErrorClass
{
	///Truncated.
	///
	///Part of data is missing. Adding more data can fix the issue.
	Truncated,
	///Bad Format
	///
	///The data is bad. Extending will not help.
	BadFormat,
	///Duplicate key.
	///
	///Object contains a duplicate key.
	DuplicateKey,
	///Garbage after end.
	///
	///Prefix of data is valid, but the data contains junk afterwards.
	GarbageAfterEnd,
	///Internal limitation.
	///
	///The data might be valid, but can not be parsed because internal limitation.
	InternalLimitation,
	///Internal error.
	///
	///The data might be valid, but can not be parsed because internal error. THIS IS A BUG.
	InternalError,
}

///Print Error parsing JSON.
///
///This differs from `ParseError` in that the offset of the error is not printed when using the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct ParseErrorPrint(_ParseError);

///Error parsing JSON.
///
///The full error message can be printed using the `Display` impl.
#[derive(Copy,Clone,Debug)]
pub struct ParseError(usize,_ParseError);
#[derive(Copy,Clone,Debug)]
enum _ParseError
{
	Mismatch(char,Option<char>),
	IllegalStringChar(u8),
	BadEscape(Option<char>),
	BadIn(ParseState,Option<char>),
	EscapeHex(Option<char>),
	BadNum(NumberState,Option<char>),
	UnpairedSurrogate(u32),
	DuplicateKey,
	ExpectedArray,
	ExpectedObject,
	TooDeep,
}

impl ParseError
{
	///Get the class of the error.
	pub fn classify(&self) -> ErrorClass
	{
		use self::_ParseError::*;
		match self.1 {
			BadIn(ParseState::Final,_) => ErrorClass::GarbageAfterEnd,
			Mismatch(_,None)|BadEscape(None)|BadIn(_,None) => ErrorClass::Truncated,
			EscapeHex(None)|BadNum(_,None) => ErrorClass::Truncated,
			Mismatch(_,_)|BadEscape(_)|BadIn(_,_) => ErrorClass::BadFormat,
			EscapeHex(_)|BadNum(_,_)|IllegalStringChar(_) => ErrorClass::BadFormat,
			DuplicateKey => ErrorClass::DuplicateKey,
			UnpairedSurrogate(_) => ErrorClass::InternalLimitation,
			ExpectedArray|ExpectedObject => ErrorClass::InternalError,
			TooDeep => ErrorClass::InternalLimitation,
		}
	}
	///Get offset of error.
	///
	///The offset is measured in bytes, and points to the offending codepoint. If the offending codepoint is
	///end of string, the offset is length of the entiere string.
	pub fn offset(&self) -> usize { self.0 }
	///Get printer for error that only prints the message part.
	///
	///The error can be formatted using the `Display` impl. Unlike `Display` of `ParseError`, the offset of
	///the offending codepoint is not printed.
	pub fn message(&self) -> ParseErrorPrint { ParseErrorPrint(self.1) }
}

impl Display for _ParseError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::_ParseError::*;
		let escs = "'b', 'f', 'n', 'r', 't', 'u' '\"', '/' or '\\'";
		let expn_state = |s:ParseState|match s {
			ParseState::Initial => "Digit, '-', 'f', 'n', 't', '\"', '[' or '{'",
			ParseState::Final => "End of Input",
			ParseState::ArrayStart => "Digit, '-', 'f', 'n', 't', '\"', '[', '{' or ']'",
			ParseState::ArrayValue => "',' or ']'",
			ParseState::ArrayComma => "Digit, '-', 'f', 'n', 't', '\"', '[' or '{'",
			ParseState::ObjectStart => "'\"' or '}'",
			ParseState::ObjectKey => "':'",
			ParseState::ObjectColon => "Digit, '-', 'f', 'n', 't', '\"', '[' or '{'",
			ParseState::ObjectValue => "',' or '}'",
			ParseState::ObjectComma => "'\"'",
		};
		let expn_num = |s:NumberState|match s {
			NumberState::Init => "Digit or '-'",
			NumberState::Minus => "Digit",
			NumberState::Zero => "Whitespace, '.', 'e', 'E', ',', ']', '}' or End of Input",
			NumberState::Integer => "Whitespace, Digit, '.', 'e', 'E', ',', ']', '}' or End of Input",
			NumberState::Dot => "Digit",
			NumberState::Decimal => "Whitespace, Digit, 'e' or 'E', ',', ']', '}' or End of Input",
			NumberState::Exp => "Digit, '+' or '-'",
			NumberState::ExpSign => "Digit",
			NumberState::Exponent => "Whitespace, Digit, ',', ']', '}' or End of Input",
		};
		match *self {
			Mismatch(r,Some(c)) => write!(f, "Expected '{r}', got '{c}'"),
			Mismatch(r,None) => write!(f, "Expected '{r}', got End of Input"),
			IllegalStringChar(b) => {
				let b = match b {
					0=>"NUL", 1=>"SOH", 2=>"STX", 3=>"ETX", 4=>"EOT", 5=>"ENQ", 6=>"ACK",
					7=>"BEL", 8=>"BS", 9=>"HT", 10=>"LF", 11=>"VT", 12=>"FF", 13=>"CR", 14=>"SO",
					15=>"SI", 16=>"DLE", 17=>"DC1", 18=>"DC2", 19=>"DC3", 20=>"DC4", 21=>"NAK",
					22=>"SYN", 23=>"ETB", 24=>"CAN", 25=>"EM", 26=>"SUB", 27=>"ESC", 28=>"FS",
					29=>"GS", 30=>"RS", 31=>"US", _ => "End of Input"
				};
				write!(f, "Expected any non-control, got {b}")
			},
			BadEscape(c) => write!(f, "Expected {escs}, got {c}", c=PrintChar(c)),
			BadIn(s,c) => write!(f, "Expected {s}, got {c}", s=expn_state(s), c=PrintChar(c)),
			BadNum(s,c) => write!(f, "Expected {s}, got {c}", s=expn_num(s), c=PrintChar(c)),
			ExpectedArray => write!(f, "Internal error: Expected array on stack top"),
			ExpectedObject => write!(f, "Internal error: Expected object on stack top"),
			EscapeHex(c) => write!(f, "Expected hexdigit, got {c}", c=PrintChar(c)),
			UnpairedSurrogate(s) => write!(f, "Unpaired surrogate \\u{s:04x}"),
			DuplicateKey => write!(f, "Duplicate object key"),
			TooDeep => write!(f, "Too deep nested structure"),
		}
	}
}

impl Display for ParseError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "JSON error at position {pos}: {err}", pos=self.0, err=self.1)
	}
}

impl Display for ParseErrorPrint
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		Display::fmt(&self.0, f)
	}
}

impl From<(usize,_ParseError)> for ParseError
{
	fn from(x: (usize, _ParseError)) -> ParseError { ParseError(x.0,x.1) }
}

fn __write_value(state: &mut ParseState, stack: &mut Vec<ParseContextItem>, v: JSON) -> Option<JSON>
{
	match stack.last_mut() {
		Some(ParseContextItem::Array(a)) => {
			a.push(v);
			*state = ParseState::ArrayValue;
			None
		},
		Some(ParseContextItem::Object(o, ref mut key)) => {
			//This is never supposed to be called with key=None, so anything but illegal operations
			//is acceptable.
			if let Some(key) = key.take() { o.insert(key, v); }
			*state = ParseState::ObjectValue;
			None
		},
		None => {
			*state = ParseState::Final;
			Some(v)
		}
	}
}

const MAX_DEPTH: usize = 2048;

fn parse_json(x: &str) -> Result<JSON, ParseError>
{
	use self::_ParseError::*;
	let mut cursor = StringCursor(x, 0);
	let mut state = ParseState::Initial;
	let mut stack: Vec<ParseContextItem> = Vec::new();
	let mut json: Option<JSON> = None;
	//Why??? Skip BOM if any.
	if cursor.peek() == Some('\u{feff}') { cursor.next(); }
	loop {
		cursor.discard_json_whitespace();
		//Hack: Save old iterator for number parsing.
		let old_cursor = cursor.clone();
		let p = cursor.1;
		let c = f_break!(cursor.next());
		match c {
			']' => match state {
				ParseState::ArrayStart|ParseState::ArrayValue => {
					//When in Array* states, the topmost entry in context stack is always
					//Array storing the current array. Pop it and write the value.
					let v = match stack.pop() {
						Some(ParseContextItem::Array(a)) => JSON::Array(a),
						_ => fail!((p,ExpectedArray))
					};
					json = __write_value(&mut state, &mut stack, v);
				},
				state => fail!((p,BadIn(state,Some(c))))
			},
			'}' => match state {
				ParseState::ObjectStart|ParseState::ObjectValue => {
					//When in Object* states, the topmost entry in context stack is always
					//Object storing the current object. Pop it and write the value.
					//Furthermore, the key field is always None.
					let v = match stack.pop() {
						Some(ParseContextItem::Object(o,None)) => JSON::Object(o),
						_ => fail!((p,ExpectedObject))
					};
					json = __write_value(&mut state, &mut stack, v);
				},
				state => fail!((p,BadIn(state,Some(c))))
			},
			'[' => match state {
				ParseState::Initial|ParseState::ArrayStart|ParseState::ArrayComma|
					ParseState::ObjectColon => {
					//Push Array to context stack and go to start of array state.
					stack.push(ParseContextItem::Array(Vec::new()));
					state = ParseState::ArrayStart;
				},
				state => fail!((p,BadIn(state,Some(c))))
			},
			'{' => match state {
				ParseState::Initial|ParseState::ArrayStart|ParseState::ArrayComma|
					ParseState::ObjectColon => {
					//Push Object to context stack and go to start of object state.
					//The key field is always None.
					stack.push(ParseContextItem::Object(BTreeMap::new(), None));
					state = ParseState::ObjectStart;
				},
				state => fail!((p,BadIn(state,Some(c))))
			},
			',' => match state {
				ParseState::ArrayValue => state = ParseState::ArrayComma,
				ParseState::ObjectValue => state = ParseState::ObjectComma,
				state => fail!((p,BadIn(state,Some(c))))
			},
			':' => match state {
				ParseState::ObjectKey => state = ParseState::ObjectColon,
				state => fail!((p,BadIn(state,Some(c))))
			},
			'n' => match state {
				ParseState::Initial|ParseState::ArrayStart|ParseState::ArrayComma|
					ParseState::ObjectColon => {
					cursor.check("ull")?;
					json = __write_value(&mut state, &mut stack, JSON::Null);
				},
				state => fail!((p,BadIn(state,Some(c))))
			},
			'f' => match state {
				ParseState::Initial|ParseState::ArrayStart|ParseState::ArrayComma|
					ParseState::ObjectColon => {
					cursor.check("alse")?;
					json = __write_value(&mut state, &mut stack, JSON::Boolean(false));
				},
				state => fail!((p,BadIn(state,Some(c))))
			},
			't' => match state {
				ParseState::Initial|ParseState::ArrayStart|ParseState::ArrayComma|
					ParseState::ObjectColon => {
					cursor.check("rue")?;
					json = __write_value(&mut state, &mut stack, JSON::Boolean(true));
				},
				state => fail!((p,BadIn(state,Some(c))))
			},
			'-'|'0'..='9' => match state {
				ParseState::Initial|ParseState::ArrayStart|ParseState::ArrayComma|
					ParseState::ObjectColon => {
					let (l,n) = old_cursor.decode_json_number()?;
					//Discard the decoded bytes from cursor.
					cursor.discard_bytes(l);
					json = __write_value(&mut state, &mut stack, JSON::Number(n));
				},
				state => fail!((p,BadIn(state,Some(c))))
			},
			'"' => match state {
				ParseState::Initial|ParseState::ArrayStart|ParseState::ArrayComma|
					ParseState::ObjectColon => {
					let s = cursor.decode_json_string()?;
					//Pop the double quote. Any failure can be assumed to be truncation.
					cursor.next().ok_or((cursor.p(),IllegalStringChar(255)))?;
					json = __write_value(&mut state, &mut stack, JSON::String(s));
				},
				ParseState::ObjectStart|ParseState::ObjectComma => {
					let nkey = cursor.decode_json_string()?;
					//This is always supposed to be true.
					if let Some(&mut ParseContextItem::Object(ref o, ref mut key)) =
						stack.last_mut() {
						fail_if!(o.contains_key(&nkey), (p,DuplicateKey));
						*key = Some(nkey);
					}
					state = ParseState::ObjectKey;
					//Pop the double quote. Any failure can be assumed to be truncation.
					cursor.next().ok_or((cursor.p(),IllegalStringChar(255)))?;
				},
				state => fail!((p,BadIn(state,Some(c))))
			},
			c => fail!((p,BadIn(state,Some(c))))
		}
		//This limit is there because dropping resulting JSON will overflow stack if the JSON has too
		//deep nesting.
		fail_if!(stack.len() > MAX_DEPTH, (p,_ParseError::TooDeep));
	}
	//If json=None, the input is incomplete.
	ttry!(json.ok_or((cursor.1,BadIn(state,None))))
}


//Note: This returns unspecified results if number is not valid JSON number.
fn parse_integer_v(num: &str) -> Option<(bool, u64)>
{
	//The first character may be - for negative numbers.
	let (mut negative, num) = if num.starts_with("-") {
		//Starting with - guarantees that index 1 is valid.
		(true, &num[1..])
	} else {
		(false, num)
	};
	//The integer part ends in ., e or E.
	let (mut integer, num) = if let Some(pos) = num.find(|c|c=='.'||c=='e'||c=='E') {
		(&num[..pos], &num[pos..])
	} else {
		(num, "")
	};
	//The decimal part starts with . and ends in e or E.
	let (mut decimal, num) = if num.starts_with(".") {
		let num = &num[1..];	//num[0] must be ., so 1 is valid to split on.
		if let Some(pos) = num.find(|c|c=='e'||c=='E') {
			(&num[..pos], &num[pos..])
		} else {
			(num, "")
		}
	} else {
		("", num)
	};
	//The exponent.
	let (mut exp_neg, mut exponent) = if num.starts_with("e") || num.starts_with("E") {
		let num = &num[1..];	//num[0] must be e or E, so 1 is valid to split on.
		if num.starts_with("+") {
			(false, &num[1..])	//num[0]=='+'.
		} else if num.starts_with("-") {
			(true, &num[1..])	//num[0]=='-'.
		} else {
			(false, num)
		}
	} else {
		(false, "")
	};
	//Truncate trailing zeroes off the decimal part, and leading zeroes off the exponent part, as these do not
	//affect value.
	while let Some(t) = decimal.strip_suffix("0") { decimal = t; }
	while let Some(t) = exponent.strip_prefix("0") { exponent = t; }
	if exponent == "" { exponent = "0"; }
	//If exponent is zero (empty), flip exponent to positive.
	if exponent == "0" { exp_neg = false; }
	//Zero is always positive. Due to trailing zero stripping, zero decimal part is "". JSON requires integer
	//part to not have a leading zero. Also zero the exponent, as it does not matter.
	if integer == "0" && decimal == "" {
		negative = false;
		exp_neg = false;
		exponent = "0";
	}
	//Due to above zeroing of exponent if base is 0, any large positive or negative exponent must give either
	//huge value or non-intger.
	let mut exponent = usize::from_str(exponent).ok()?;
	//If exponent is negative, then decimal must be "" and the integer must have that many trailing zeroes,
	//because otherwise number is not an integer.
	if exp_neg {
		fail_if_none!(decimal != "");
		for _ in 0..exponent { integer = integer.strip_suffix("0")?; }
		exponent = 0;	//The exponent is like 0 now.
	}
	//Parse the integer part as u64. Then adjust decimal part.
	let mut integer = u64::from_str(integer).ok()?;
	while exponent > 0 {
		integer = integer.checked_mul(10)?;
		exponent -= 1;
		//Missing reads like 0.
		if let Some((head, tail)) = split_head_tail(decimal) {
			let f = head.to_digit(10)? as u64;
			integer = integer.checked_add(f)?;
			decimal = tail;
		}
	}
	//Now, the exponent must be big enough to shift entiere decimal part.
	fail_if_none!(decimal != "");
	//Okay, got parse as unsigned.
	Some((negative, integer))
}

//Note: This returns unspecified results if number is not valid JSON number.
fn parse_integer(num: &str) -> (Option<i64>, Option<u64>)
{
	match parse_integer_v(num) {
		Some((true, magnitude)) => {
			//Negative numbers never fit unsigned. Due to behavior of parse_integer_v(), zero is always
			//positive. If top bit of magnitude - 1 is set, then it will not fit.
			let m = magnitude - 1;
			if m << 1 >> 1 != m { return (None, None); }
			(Some((!m) as i64), None)
		},
		//If the top bit of magnitude is set, this will not fit signed.
		Some((false, magnitude)) => if magnitude << 1 >> 1 != magnitude {
			(None, Some(magnitude))
		} else {
			(Some(magnitude as i64), Some(magnitude))
		},
		None => return (None, None)
	}
}

///A string that can be appended to.
pub trait StringWriterTarget
{
	///The error type of write operation.
	///
	///The write operation might be infallable. In that case, the error type should be uninhabited type in order
	///to signal that an error can not happen.
	type Error: Sized;
	///Append the specified string to end of this string.
	///
	///If the operation fails, returns an error.
	fn write(&mut self, frag: &str) -> Result<(), <Self as StringWriterTarget>::Error>;
}

impl StringWriterTarget for String
{
	type Error = Void;
	fn write(&mut self, frag: &str) -> Result<(), Void> { Ok(self.push_str(frag)) }
}

impl<'a> StringWriterTarget for Formatter<'a>
{
	type Error = FmtError;
	fn write(&mut self, frag: &str) -> Result<(), FmtError> { self.write_str(frag) }
}

macro_rules! scale
{
	(P1) => { 10 };
	(P2) => { 100 };
	(P3) => { 1_000 };
	(P4) => { 10_000 };
	(P5) => { 100_000 };
	(P6) => { 1_000_000 };
	(P7) => { 10_000_000 };
	(P8) => { 100_000_000 };
	(P9) => { 1_000_000_000 };
	(P10) => { 10_000_000_000 };
	(P11) => { 100_000_000_000 };
	(P12) => { 1_000_000_000_000 };
	(P13) => { 10_000_000_000_000 };
	(P14) => { 100_000_000_000_000 };
	(P15) => { 1_000_000_000_000_000 };
	(P16) => { 10_000_000_000_000_000 };
	(P17) => { 100_000_000_000_000_000 };
	(P18) => { 1_000_000_000_000_000_000 };
	(P19) => { 10_000_000_000_000_000_000 };
}

macro_rules! write_adv
{
	($arr:ident $idx:ident $val:expr) => {{
		$arr[$idx] = $val;
		$idx += 1;
	}}
}

macro_rules! write_digit
{
	($arr:ident $idx:ident [P0] $val:expr) => {{
		write_adv!($arr $idx ($val%10+48) as u8);
	}};
	($arr:ident $idx:ident [ $dval:ident $($vals:ident)* ] $val:expr) => {{
		let d = scale!($dval);
		if $val >= d { write_adv!($arr $idx ($val/d%10+48) as u8); }
		write_digit!($arr $idx [$($vals)*] $val);
	}};
}


///A JSON number.
///
///The supported operations are intentionally limited. The reason for this class is that floating-point operations
///are inherently inaccurate, and to actually have guarantees on accuracy, one must keep numbers as integers.
///
///The 'Display` impl prints the value. For integers in range of `i64` or `u64`, the printed value always uses
///integer notation. In addition, printing values using `LowerExp` and `UpperExp` impls is supported.
///
///For values in range of `i64` and `u64', the numbers can additionally be printed as binary, octal or hexadecimal
///(uppercase or lowercase). However, these formats are not supported for floating-point values.
#[derive(Copy,Clone)]
pub struct Number
{
	neg_int: Option<i64>,
	pos_int: Option<u64>,
	float: f64,
}

impl Number
{
	///Make JSON number that is an unsigned integer.
	pub fn new_unsigned(x: u64) -> Number
	{
		let y = x as i64;
		Number {
			neg_int: if y >= 0 { Some(y) } else { None },
			pos_int: Some(x),
			float: x as f64,
		}
	}
	///Make JSON number that is an signed integer.
	pub fn new_signed(x: i64) -> Number
	{
		Number {
			neg_int: Some(x),
			pos_int: if x >= 0 { Some(x as u64) } else { None },
			float: x as f64,
		}
	}
	///Make JSON number.
	///
	///If the floating-point number exactly describes an integer that is in range for either `i64` or `u64`
	///type, the number is stored as an integer, not as floating-point number.
	pub fn new_float(x: f64) -> Number
	{
		let y = x.to_bits();
		let mantissa = y & 0xFFFFFFFFFFFFF;
		let exponent = y >> 52 & 2047;
		let sign = y >> 63 != 0;
		Number {
			neg_int: Self::decode_float_signed(mantissa, exponent, sign),
			pos_int: Self::decode_float_unsigned(mantissa, exponent, sign),
			float: x,
		}
	}
	///Get unsigned integer value of JSON number.
	///
	///If the number is integer in range of `u64`, returns `Some(v)`, where `v` is the value, with guaranteed
	///zero error. Otherwise return `none`.
	pub fn unsigned(&self) -> Option<u64> { self.pos_int }
	///Get signed integer value of JSON number.
	///
	///If the number is integer in range of `i64`, returns `Some(v)`, where `v` is the value, with guaranteed
	///zero error. Otherwise return `none`.
	pub fn signed(&self) -> Option<i64> { self.neg_int }
	///Get approximate float value of JSON number.
	///
	///There are no guarantees on accuracy of the approximation.
	pub fn float(&self) -> f64
	{
		if let Some(n) = self.pos_int {
			n as f64
		} else if let Some(n) = self.neg_int {
			n as f64
		} else {
			self.float
		}
	}
	fn decode_float_signed(mantissa: u64, exponent: u64, sign: bool) -> Option<i64>
	{
		let mut v = Self::decode_float_nosign(mantissa, exponent)?;
		//If negative, do one's complement. In any case, check limits.
		if sign {
			if v > 1 << 63 { return None; }
			v = (!v).wrapping_add(1);
		} else {
			if v >= 1 << 63 { return None; }
		}
		Some(v as i64)
	}
	fn decode_float_unsigned(mantissa: u64, exponent: u64, sign: bool) -> Option<u64>
	{
		//Reject negative numbers if mantissa or exponent is nonzero.
		if sign && (mantissa != 0 || exponent != 0) { return None; }
		Self::decode_float_nosign(mantissa, exponent)
	}
	fn decode_float_nosign(mantissa: u64, exponent: u64) -> Option<u64>
	{
		match exponent {
			//Zero.
			0 if mantissa == 0 => Some(0),
			//Exponents 0 to 1022 are not integer (zero is handled specially).
			0..=1022 => None,
			//Exponents 1023 to 1074 might be integer or not.
			1023..=1074 => {
				let zbits = 1075 - exponent;
				let zmask = (1 << zbits) - 1;
				if mantissa & zmask == 0 {
					Some((mantissa | 1 << 52) >> zbits)
				} else {
					None
				}
			},
			//Exponents 1075 to 1086 are always integers due to resolution.
			1075..=1086 => Some((mantissa | 1 << 52) << (exponent - 1075)),
			//Exponents 1087 to 2047 are always too big for u64.
			_ => None
		}
	}
	pub(crate) fn parse(s: &str) -> Number
	{
		//The floating-point approximation has absolutely no guarantees, so it is okay to just give zero
		//if it fails.
		let fval = f64::from_str(s).unwrap_or(0.0f64);
		let (sint, uint) = parse_integer(s);
		Number{
			neg_int: sint,
			pos_int: uint,
			float: fval,
		}
	}
	pub(crate) fn encode_value(bits: u64) -> (bool, u64, i32)
	{
		const Q100: u64 = 100_000_000_000_000_000;	//100 quadrillion.
		const Q500: u64 = 500_000_000_000_000_000;	//500 quadrillion.
		const Q1000: u64 = 1000_000_000_000_000_000;	//1000 quadrillion.
		let mut mantissa = bits & 0xFFFFFFFFFFFFF;
		let mut unit = (bits >> 52) as i32 % 2048 - 1075;
		let sign = bits >> 63 != 0;
		let mantissa_z = mantissa == 0;
		//Unit=972 is infinites and Nans.
		if unit>=972 {
			return match mantissa {
				0 => (sign, !0, 9999),
				_ => (false, !1, 9999),
			};
		}
		//Unit=-1075 is subnormals. Otherwise it is not subnormal, and implicit bit52 is asserted.
		//subnormals are the same size a lowest normal, so unit bottoms at -1074. Scale the bottommost
		//possible bit to 0.
		if unit > -1075 { mantissa |= 1<<52; } else { unit = -1074; }
		let base = (unit + 1074) as usize;
		//If mantissa is 0, the number is just zero. 
		if mantissa == 0 { return (false, 0,0); }
		//If mantissa is smallest possible for the number, reduce effective exponent by 1. Except the
		//unit equivalent to subnormals can not be reduced.
		let halfscale = if mantissa_z && base > 0 { FLOAT_HALF_MAGS[base-1] } else { FLOAT_HALF_MAGS[base] };
		//Scaled add.
		let mut mag = FLOAT_CONV_TABLE3[base].2;
		let mut m = (0,0);
		for i in 0..53 {
			if base+i >= FLOAT_CONV_TABLE3.len() { break; }
			//If magnitude grows, adapt to it. The magnitude growth is at most 1 in any step.
			if FLOAT_CONV_TABLE3[base+i].2 > mag {
				m.1 = m.0 % 10 * Q100 + m.1 / 10;
				m.0 /= 10;
				mag += 1;
			}
			if mantissa >> i & 1 != 0 {
				m.0 += FLOAT_CONV_TABLE3[base+i].0;
				m.1 += FLOAT_CONV_TABLE3[base+i].1;
				let carry = m.1 / Q1000;
				m.1 %= Q1000;
				m.0 += carry;
			}
		}
		mag += 18;	//Discard effect of low word.
		//Continue scaling up to halfscale.
		while halfscale > mag {
			m.1 = m.0 % 10 * Q100 + m.1 / 10;
			m.0 /= 10;
			mag += 1;
		}
		//Round (banker's rounding).
		if m.1 > Q500 || (m.1 == Q500 && m.0 % 2 == 1) { m.0 += 1; }
		let mut m = m.0;
		//Strip trailing zeros.
		while m % 10 == 0 { m /= 10; mag += 1; }
		(sign, m,mag)
	}
	fn serialize_len(&self) -> usize
	{
		let (sign,m) = if let Some(n) = self.pos_int {
			(false, n)
		} else if let Some(n) = self.neg_int {
			//This is negative, do 2s complement.
			(true, (!(n as u64)).wrapping_add(1))
		} else {
			//Do the float path.
			return self.dump_float_len();
		};
		let mut l = 0;
		if sign { l+=1; }
		l + Self::__numeric_magnitude(m)
	}
	fn dump<B:StringWriterTarget>(&self, to: &mut B) -> Result<(), <B as StringWriterTarget>::Error>
	{
		let (sign,m) = if let Some(n) = self.pos_int {
			(false, n)
		} else if let Some(n) = self.neg_int {
			//This is negative, do 2s complement.
			(true, (!(n as u64)).wrapping_add(1))
		} else {
			//Do the float path.
			return self.dump_float(to);
		};
		let mut out = [0u8;32];
		let mut ptr = 0;
		if sign { write_adv!(out ptr b'-'); }
		write_digit!(out ptr [P19 P18 P17 P16 P15 P14 P13 P12 P11 P10 P9 P8 P7 P6 P5 P4 P3 P2 P1 P0] m);
		//This can not fail.
		let out = from_utf8(&out[..ptr]).unwrap_or("");
		to.write(out)
	}
	fn dump_float<B:StringWriterTarget>(&self, to: &mut B) -> Result<(), <B as StringWriterTarget>::Error>
	{
		let (sign,m,mag) = Self::encode_value(self.float.to_bits());
		//Couple special values.
		if m == !0 { return to.write(if sign { "-inf" } else { "inf" }); }
		if m == !1 { return to.write("NaN"); }
		if m == 0 { return to.write("0"); }
		//Write out the thing.
		let mut out = [0u8;32];
		let mut ptr = 0;
		if sign { write_adv!(out ptr b'-'); }
		write_digit!(out ptr [P19 P18 P17 P16 P15 P14 P13 P12 P11 P10 P9 P8 P7 P6 P5 P4 P3 P2 P1 P0] m);
		write_adv!(out ptr b'e');
		if mag < 0 { write_adv!(out ptr b'-'); }
		let amag = if mag < 0 { -mag } else { mag };
		write_digit!(out ptr [P2 P1 P0] amag);
		//This can not fail.
		let out = from_utf8(&out[..ptr]).unwrap_or("");
		to.write(out)
	}
	fn dump_float_len(&self) -> usize
	{
		let (sign,m,mag) = Self::encode_value(self.float.to_bits());
		//Couple special values.
		if m == !0 { return 3 + sign as usize; }
		if m == !1 { return 3; }
		if m == 0 { return 1; }
		//Write out the thing.
		let mut ptr = 1;
		if sign { ptr += 1; }
		if mag < 0 { ptr += 1; }
		ptr += Self::__numeric_magnitude(m);
		let amag = if mag < 0 { -mag } else { mag };
		ptr += match amag {
			0..=9 => 1,
			10..=99 => 2,
			_ => 3,		//Assume no more than 3 digits.
		};
		ptr
	}
	fn __numeric_magnitude(m: u64) -> usize
	{
		match m {
			0..=9 => 1,
			10..=99 => 2,
			100..=999 => 3,
			1000..=9999 => 4,
			10000..=99999 => 5,
			100000..=999999 => 6,
			1000000..=9999999 => 7,
			10000000..=99999999 => 8,
			100000000..=999999999 => 9,
			1000000000..=9999999999 => 10,
			10000000000..=99999999999 => 11,
			100000000000..=999999999999 => 12,
			1000000000000..=9999999999999 => 13,
			10000000000000..=99999999999999 => 14,
			100000000000000..=999999999999999 => 15,
			1000000000000000..=9999999999999999 => 16,
			10000000000000000..=99999999999999999 => 17,
			100000000000000000..=999999999999999999 => 18,
			1000000000000000000..=9999999999999999999 => 19,
			10000000000000000000..=18446744073709551615 => 20,
		}
	}
}

include!("floattbl.inc");

impl Display for Number
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		if let Some(n) = self.pos_int {
			write!(f, "{n}")
		} else if let Some(n) = self.neg_int {
			write!(f, "{n}")
		} else {
			Self::dump_float(self, f)
		}
	}
}

impl Debug for Number
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		if let Some(n) = self.pos_int {
			write!(f, "<unsigned>{n}")
		} else if let Some(n) = self.neg_int {
			write!(f, "<signed>{n}")
		} else {
			write!(f, "<float>{float}", float=self.float)
		}
	}
}

macro_rules! impl_fmt
{
	(INT $fmt:ident) => {
		impl core::fmt::$fmt for Number
		{
			fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
			{
				if let Some(n) = self.pos_int {
					core::fmt::$fmt::fmt(&n, f)
				} else if let Some(n) = self.neg_int {
					core::fmt::$fmt::fmt(&n, f)
				} else {
					f.write_str("<Not an integer>")
				}
			}
		}
	};
	(FP $fmt:ident) => {
		impl core::fmt::$fmt for Number
		{
			fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
			{
				if let Some(n) = self.pos_int {
					Display::fmt(&n, f)
				} else if let Some(n) = self.neg_int {
					Display::fmt(&n, f)
				} else {
					core::fmt::$fmt::fmt(&self.float, f)
				}
			}
		}
	};
}

impl_fmt!(INT Binary);
impl_fmt!(INT LowerHex);
impl_fmt!(INT Octal);
impl_fmt!(INT UpperHex);
impl_fmt!(FP LowerExp);
impl_fmt!(FP UpperExp);


impl PartialEq<Number> for Number
{
	fn eq(&self, n: &Number) -> bool
	{
		if let (Some(x), Some(y)) = (self.pos_int, n.pos_int) { return x == y };
		if let (Some(x), Some(y)) = (self.neg_int, n.neg_int) { return x == y };
		self.float == n.float
	}
}

impl PartialEq<i64> for Number
{
	fn eq(&self, n: &i64) -> bool { self.neg_int == Some(*n) }
}

impl PartialEq<u64> for Number
{
	fn eq(&self, n: &u64) -> bool { self.pos_int == Some(*n) }
}

impl PartialEq<f64> for Number
{
	fn eq(&self, n: &f64) -> bool { self.float == *n }
}

///A JSON value node.
///
///This is a single JSON value, that is, one of:
///
/// * `Null`
/// * Boolean
/// * Number (integer or double-precision floating point)
/// * Text string
/// * Array
/// * Dictionary/Object/Map
#[derive(Clone,Debug)]
pub enum JSON
{
	///The null value (the only value of null type).
	Null,
	///Number (integer or float).
	Number(Number),
	///UTF-8 Text string
	///
	///Note that the string must be valid `UTF-8`.
	String(String),
	///Boolean
	Boolean(bool),
	///Array.
	///
	///The argument vector stores all the values in JSON array, in order, without gaps.
	Array(Vec<JSON>),
	///Dictionary (map or object)
	///
	///The argument map stores mapping from all the keys in object to all the values in the object, with no
	///extra or missing entries. Note that JSON requires keys to be strings, and strings can only constist of
	///`UTF-8` codepoints, so the keys must be valid `UTF-8` strings.
	Object(BTreeMap<String, JSON>),
}

struct OptionEmptyIterator<T,U:Iterator<Item=T>>(Option<U>);

impl<T,U:Iterator<Item=T>> Iterator for OptionEmptyIterator<T,U>
{
	type Item = T;
	fn next(&mut self) -> Option<T> { self.0.as_mut().and_then(|i|i.next()) }
}

impl JSON
{
	///The JSON `FALSE` simple value.
	pub const FALSE: JSON = JSON::Boolean(false);
	///The JSON `TRUE` simple value.
	pub const TRUE: JSON = JSON::Boolean(true);
	///The JSON `NULL` simple value.
	pub const NULL: JSON = JSON::Null;
	///Parse the given text string as JSON.
	///
	///If the text has `"UTF-8 BOM"` (U+0xFEFF) as its first codepoint, that codepoint is skipped.
	///
	///This routine can not parse strings that are:
	///
	/// * Not valid JSON.
	/// * Have too deep nesting (the maximum depth limit is currently 500).
	/// * Have surrogate escape present and not properly paired with another surrogate escape.
	pub fn parse(data: &str) -> Result<JSON, ParseError>
	{
		parse_json(data)
	}
	///Is NULL?
	pub fn is_null(&self) -> bool { if let JSON::Null = self { true } else { false } }
	///Is Boolean?
	pub fn is_boolean(&self) -> bool { if let JSON::Boolean(_) = self { true } else { false } }
	///Is Number?
	pub fn is_number(&self) -> bool { if let JSON::Number(_) = self { true } else { false } }
	///Is String?
	pub fn is_string(&self) -> bool { if let JSON::String(_) = self { true } else { false } }
	///Is Array?
	pub fn is_array(&self) -> bool { if let JSON::Array(_) = self { true } else { false } }
	///Is Object?
	pub fn is_object(&self) -> bool { if let JSON::Object(_) = self { true } else { false } }
	///Cast JSON node into null.
	///
	///If the node is not null, returns `None`.
	pub fn as_null<'a>(&'a self) -> Option<()>
	{
		if let &JSON::Null = self { Some(()) } else { None }
	}
	///Cast JSON node into string.
	///
	///If the node is not a string, returns `None`.
	pub fn as_string<'a>(&'a self) -> Option<&'a str>
	{
		if let &JSON::String(ref x) = self { Some(x.deref()) } else { None }
	}
	///Cast JSON node into boolean.
	///
	///If the node is not a boolean, returns `None`.
	pub fn as_boolean<'a>(&'a self) -> Option<bool>
	{
		if let &JSON::Boolean(ref x) = self { Some(*x) } else { None }
	}
	///Cast JSON node into array.
	///
	///If the node is not an array, returns `None`.
	pub fn as_array<'a>(&'a self) -> Option<&'a [JSON]>
	{
		if let &JSON::Array(ref x) = self { Some(&x[..]) } else { None }
	}
	///Cast JSON node into object (dictionary, map).
	///
	///If the node is not an object, returns `None`.
	pub fn as_object<'a>(&'a self) -> Option<&'a BTreeMap<String, JSON>>
	{
		if let &JSON::Object(ref x) = self { Some(x) } else { None }
	}
	///Cast JSON node into number.
	///
	///If the node is not a number, returns `None`.
	pub fn as_number<'a>(&'a self) -> Option<Number>
	{
		if let &JSON::Number(x) = self { Some(x) } else { None }
	}
	///Get nth index of array.
	///
	///If the node is not an array, or if the given index points outside the array, returns `None`.
	pub fn nth<'a>(&'a self, n: usize) -> Option<&'a JSON>
	{
		self.as_array().and_then(|a|a.get(n))
	}
	///Get length of array.
	///
	///If called on non-array, returns 0.
	pub fn array_len(&self) -> usize { self.as_array().map(|a|a.len()).unwrap_or(0) }
	///Iterate over array.
	///
	///If called on non-array, returns empty iterator.
	pub fn iter_array<'a>(&'a self) -> impl Iterator<Item=&'a JSON>
	{
		OptionEmptyIterator(self.as_array().map(|a|a.iter()))
	}
	///Get object field with specified name.
	///
	///If the node is not an object, of if the given field does not exist in the object, returns `None`.
	pub fn field<'a>(&'a self, name: &str) -> Option<&'a JSON>
	{
		self.as_object().and_then(|a|a.get(name))
	}
	///Return iterator over all field names.
	///
	///If called on non-dictionary, returns empty iterator.
	pub fn field_names<'a>(&'a self) -> impl Iterator<Item=&'a String>
	{
		OptionEmptyIterator(self.as_object().map(|d|d.keys()))
	}
	///Iterate over object, both keys and values.
	///
	///If called on non-object, returns empty iterator.
	pub fn iter_object<'a>(&'a self) -> impl Iterator<Item=(&'a String, &'a JSON)>
	{
		OptionEmptyIterator(self.as_object().map(|a|a.iter()))
	}
	///Insert or modify given field in object to contain the given string.
	///
	///This does nothing if the node is not an object.
	pub fn write_string_field(&mut self, name: &str, value: &str)
	{
		if let &mut JSON::Object(ref mut x) = self {
			x.insert(name.to_owned(), JSON::String(value.to_owned()));
		}
	}
	///Return size of object or array.
	///
	///Anything that is not an object nor an array gives 0.
	pub fn size(&self) -> usize
	{
		match self {
			JSON::Array(ref x) => x.len(),
			JSON::Object(ref x) => x.len(),
			_ => 0
		}
	}
	///Filter object by key.
	///
	///The new object contains all the keys where the given function `f(key)` returned true.
	///
	///If passed non-object, returns the object unchanged.
	pub fn filter_object<F:Fn(&str)->bool>(&self, f: F) -> JSON
	{
		match self {
			JSON::Object(ref x) => {
				let mut out = BTreeMap::new();
				for (key,value) in x.iter() {
					if f(key) { out.insert(key.clone(),value.clone()); }
				}
				JSON::Object(out)
			},
			x => x.clone()
		}
	}
	///Make null.
	pub fn new_null() -> JSON { JSON::Null }
	///Make boolean.
	pub fn new_boolean(b: bool) -> JSON { JSON::Boolean(b) }
	///Make number.
	pub fn new_number(n: Number) -> JSON { JSON::Number(n) }
	///Make string.
	pub fn new_string(s: &str) -> JSON { JSON::String(s.to_owned()) }
	///Make array.
	pub fn new_array<'a,I:Iterator<Item=&'a JSON>>(itr: I) -> JSON
	{
		let v: Vec<_> = itr.cloned().collect();
		JSON::Array(v)
	}
	///Make object.
	pub fn new_object<'a,I:Iterator<Item=&'a (String, JSON)>>(itr: I) -> JSON
	{
		let m: BTreeMap<_,_> = itr.cloned().collect();
		JSON::Object(m)
	}
	///Serialize the node as JSON string.
	///
	///Unlike `serialize()`, this returns the serialized representation as a `String` instead of appending to
	//given string.
	pub fn serialize_string(&self) -> String
	{
		let need = self.serialize_len();
		let mut x = String::with_capacity(need);
		self.serialize(&mut x).ok();	//This just plain can not fail.
#[cfg(test)] { assert_eq!(x.len(), need); }
		x
	}
	///Serialize the node as JSON string.
	///
	///In case string append operation fails, the error is returned. This is the only possible failure mode:
	///this function can never fail with any other error.
	pub fn serialize<B:StringWriterTarget>(&self, to: &mut B) -> Result<(), <B as StringWriterTarget>::Error>
	{
		match self {
			&JSON::Null => to.write("null"),
			&JSON::Boolean(false) => to.write("false"),
			&JSON::Boolean(true) => to.write("true"),
			&JSON::Number(n) => n.dump(to),
			&JSON::String(ref s) => Self::__dump_string(s, to),
			&JSON::Array(ref a) => {
				to.write("[")?;
				let mut first = true;
				for item in a.iter() {
					if !replace(&mut first, false) { to.write(",")?; }
					item.serialize(to)?;
				}
				to.write("]")
			},
			&JSON::Object(ref o) => {
				to.write("{")?;
				let mut first = true;
				for (key,value) in o.iter() {
					if !replace(&mut first, false) { to.write(",")?; }
					Self::__dump_string(key, to)?;
					to.write(":")?;
					value.serialize(to)?;
				}
				to.write("}")
			},
		}
	}
	///Get the length of the node as a JSON string.
	pub fn serialize_len(&self) -> usize
	{
		match self {
			&JSON::Null => 4,
			&JSON::Boolean(false) => 5,
			&JSON::Boolean(true) => 4,
			&JSON::Number(n) => n.serialize_len(),
			&JSON::String(ref s) => Self::__dump_string_len(s),
			&JSON::Array(ref a) => {
				//Subtract 1 if length is greater than 0 for the missing comma.
				let mut l = 2 + a.len().saturating_sub(1);
				for item in a.iter() { l += item.serialize_len(); }
				l
			},
			&JSON::Object(ref o) => {
				//Subtract 1 if length is greater than 0 for the missing comma.
				let mut l = 2 + o.len() + o.len().saturating_sub(1);
				for (key,value) in o.iter() {
					l += Self::__dump_string_len(key) + value.serialize_len();
				}
				l
			},
		}
	}
	fn __dump_string<B:StringWriterTarget>(x: &str, to: &mut B) -> Result<(), <B as StringWriterTarget>::Error>
	{
		to.write("\"")?;
		let mut rstart = 0;
		for (i,c) in x.char_indices() {
			let mut buf = [b'\\',b'u',b'0',b'0',0,0];
			let buf = match c as u32 {
				8 => "\\b",
				9 => "\\t",
				10 => "\\n",
				12 => "\\f",
				13 => "\\r",
				34 => "\\\"",
				92 => "\\\\",
				d@0..=31 => {
					buf[4] = 48 + d as u8 / 16;
					buf[5] = 48 + d as u8 % 16;
					if buf[5] > 57 { buf[5] += 39; }	//hexalpha.
					//Can not fail.
					from_utf8(&buf).unwrap_or("")
				},
				_ => ""
			};
			//If there is escape, the past input has to be flushed.
			if buf.len() > 0 {
				if rstart < i { if let Some(s) = x.get(rstart..i) { to.write(s)?; } }
				rstart = i + c.len_utf8();
				to.write(buf)?;
			}
		}
		//Flush the remainder.
		if rstart < x.len() { if let Some(s) = x.get(rstart..) { to.write(s)?; } }
		to.write("\"")
	}
	fn __dump_string_len(x: &str) -> usize
	{
		//Fastpath for non-special strings.
		if !x.as_bytes().iter().any(|&b|matches!(b, 0..=31|34|92)) { return 2 + x.len(); }
		let mut l = 2;
		for &b in x.as_bytes().iter() {
			l += match b { 8|9|10|12|13|34|92 => 2, 0..=31 => 6, _ => 1 };
		}
		l
	}
	///Create a node from given value.
	///
	///The source type to node type mappings are:
	///
	/// * `()` -> `Null`.
	/// * `bool` -> `Boolean`.
	/// * `i64` -> `Number`.
	/// * `String` -> `String`.
	/// * `&str` -> `String`.
	/// * `&String` -> `String`.
	/// * `&[u8]` -> `String` (base64url encoded).
	/// * `&[JSON]` -> `Array`.
	/// * `Vec<JSON>` -> `Array`.
	/// * `&[(&str, JSON)]` -> `Object`.
	/// * `&[(String, JSON)]` -> `Object`.
	pub fn from(v: impl ToJSON) -> JSON { v.to_json() }
}

impl<'a> PartialEq<&'a str> for JSON
{
	fn eq(&self, other: &&'a str) -> bool
	{
		if let &JSON::String(ref x) = self { x == *other } else { false }
	}
}

impl PartialEq<str> for JSON
{
	fn eq(&self, other: &str) -> bool
	{
		if let &JSON::String(ref x) = self { x == other } else { false }
	}
}

impl PartialEq<bool> for JSON
{
	fn eq(&self, other: &bool) -> bool
	{
		if let &JSON::Boolean(x) = self { x == *other } else { false }
	}
}

impl PartialEq for JSON
{
	fn eq(&self, other: &Self) -> bool
	{
		match (self, other) {
			(&JSON::Null, &JSON::Null) => true,
			(&JSON::Boolean(x), &JSON::Boolean(y)) => x == y,
			(&JSON::Number(x), &JSON::Number(y)) => x == y,
			(&JSON::String(ref x), &JSON::String(ref y)) => x == y,
			(&JSON::Array(ref x), &JSON::Array(ref y)) => {
				if x.len() != y.len() { return false; }
				for (i, j) in x.iter().zip(y.iter()) { if i != j { return false; } }
				true
			},
			(&JSON::Object(ref x), &JSON::Object(ref y)) => {
				if x.len() != y.len() { return false; }
				for ((k1, v1), (k2, v2)) in x.iter().zip(y.iter()) {
					if k1 != k2 { return false; }
					if v1 != v2 { return false; }
				}
				true
			},
			(_, _) => false
		}
	}
}

impl Eq for JSON {}


impl PartialEq<i64> for JSON
{
	fn eq(&self, other: &i64) -> bool
	{
		if let &JSON::Number(x) = self { x == *other } else { false }
	}
}

impl PartialEq<u64> for JSON
{
	fn eq(&self, other: &u64) -> bool
	{
		if let &JSON::Number(x) = self { x == *other } else { false }
	}
}

///Turn something into JSON node.
pub trait ToJSON
{
	///Construct JSON from this value.
	fn to_json(self) -> JSON;
}

impl ToJSON for JSON
{
	fn to_json(self) -> JSON { self }
}

impl ToJSON for ()
{
	fn to_json(self) -> JSON { JSON::Null }
}

impl ToJSON for bool
{
	fn to_json(self) -> JSON { JSON::Boolean(self) }
}

impl ToJSON for i64
{
	fn to_json(self) -> JSON { JSON::Number(Number::new_signed(self)) }
}

impl ToJSON for u64
{
	fn to_json(self) -> JSON { JSON::Number(Number::new_unsigned(self)) }
}

impl ToJSON for f64
{
	fn to_json(self) -> JSON { JSON::Number(Number::new_float(self)) }
}

impl ToJSON for String
{
	fn to_json(self) -> JSON { JSON::String(self) }
}

impl<'a> ToJSON for &'a String
{
	fn to_json(self) -> JSON { JSON::String(self.clone()) }
}

impl<'a> ToJSON for &'a str
{
	fn to_json(self) -> JSON { JSON::String(self.to_owned()) }
}

impl<'a> ToJSON for &'a [u8]
{
	fn to_json(self) -> JSON { JSON::String(base64url(self)) }
}

impl<'a> ToJSON for &'a [JSON]
{
	fn to_json(self) -> JSON { JSON::Array(self.to_owned()) }
}

impl<'a> ToJSON for Vec<JSON>
{
	fn to_json(self) -> JSON { JSON::Array(self) }
}

impl<'a> ToJSON for &'a [(String, JSON)]
{
	fn to_json(self) -> JSON
	{
		JSON::Object(self.iter().cloned().collect())
	}
}

impl<'a> ToJSON for &'a [(&'a str, JSON)]
{
	fn to_json(self) -> JSON
	{
		JSON::Object(self.iter().map(|(k,v)|((*k).to_owned(), v.clone())).collect())
	}
}

///Generate JSON object.
///
///The keys and values are presented as comma-separated list of `$key => $value` pairs. The `$key` must be something
///that is valid as input for `stringify!()`, and the `$value` must be something recognized by `JSON::value()`.
#[macro_export]
macro_rules! json_object
{
	($($key:ident => $value:expr),*) => {
		$crate::JSON::from(&[$((stringify!($key), $crate::JSON::from($value))),*][..])
	}
}

///Generate JSON array.
///
///The values are comma-separated, and must be something recognized by `JSON::value()`.
#[macro_export]
macro_rules! json_array
{
	($($value:expr),*) => {
		$crate::JSON::from(&[$($crate::JSON::from($value)),*][..])
	}
}

///Generate JSON primitive value.
///
///The value must be something recognized by `JSON::value()`.
#[macro_export]
macro_rules! json_value
{
	($value:expr) => {
		$crate::JSON::from($value)
	}
}


fn base64url_ch(x: u8) -> char
{
	let m1 = (x.wrapping_add(230) >> 7).wrapping_add(255) & 6;
	let m2 = (x.wrapping_add(204) >> 7).wrapping_add(255) & 181;
	let m3 = (x.wrapping_add(194) >> 7).wrapping_add(255) & 243;
	let m4 = (x.wrapping_add(193) >> 7).wrapping_add(255) & 49;
	x.wrapping_add(65).wrapping_add(m1).wrapping_add(m2).wrapping_add(m3).wrapping_add(m4) as char
}

///Encode the input using base64url encoding.
///
///Note that there will be no extraneous output, including padding and whitespace.
pub fn base64url(input: &[u8]) -> String
{
	let mut out = String::with_capacity((4 * input.len() + 2) / 3);
	__base64url(&mut out, input);
	out
}

///Like `base64url()`, but appends to given string.
pub fn base64url_append(out: &mut String, input: &[u8])
{
	out.reserve((4 * input.len() + 2) / 3);
	__base64url(out, input);
}

fn __base64url(out: &mut String, input: &[u8])
{
	let mut value = 0u16;
	let mut bits = 0u16;
	for i in input.iter() {
		value = value << 8 | (*i as u16);
		bits = bits + 8;
		while bits >= 6 {
			out.push(base64url_ch(((value >> (bits - 6)) & 0x3F) as u8));
			bits = bits.wrapping_sub(6);
		}
	}
	if bits > 0 {
		out.push(base64url_ch(((value << (6 - bits)) & 0x3F) as u8));
	}
}

#[doc(hidden)]
#[derive(Copy,Clone,Debug)]
pub enum Void {}

#[cfg(test)]
mod test;
