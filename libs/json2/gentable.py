'''
print("static FLOAT_CONV_TABLE: &'static [(u64,i32,u8,i32)] = [");
for expn in range(-400,401):
	for lead in range(0,10):
		#Number is lead*10^expn.
		a = 0;
		b = 0;
		print("\t({},{},{},{}),".format(a,b,lead,expn));
print("];");
'''
print("static FLOAT_HALF_MAGS: &'static [i32] = &[");
for expn in range(-1075,972):
	n = 0;
	e = 0;
	if expn >= 0:
		n = str(2**expn);
	else:
		n = 1;
		for i in range(0,-expn):
			n *= 5;
			e -= 1;
			pass;
		n = str(n);
	e += len(n) - 1;
	print("\t{}, /* expn={} */".format(e,expn+53));
	pass;
print("];");
print("static FLOAT_CONV_TABLE3: &'static [(u64,u64,i32)] = &[");
for expn in range(-1074,1024):
	n = 0;
	e = 0;
	if expn >= 0:
		n = str(2**expn);
	else:
		n = 1;
		for i in range(0,-expn):
			n *= 5;
			e -= 1;
			pass;
		n = str(n);
	while len(n) < 36:
		n += "0";
		e -= 1;
	e += len(n) - 36;
	n = n[:36];
	s = "\t({},{},{}),".format(n[:18],n[18:],e);
	print(s);
print("];");
