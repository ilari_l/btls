use super::HpkeError;
use btls_aux_kyber::Kyber768;
use btls_aux_kyber::KEM;
use btls_aux_random::TemporaryRandomStream;
use core::mem::MaybeUninit;
use core::mem::transmute;
use core::ptr::copy_nonoverlapping;

#[repr(C)]
struct CompositePk
{
	x25519: [u8;32],
	kyber: [u8;1184],
}

#[repr(C)]
struct CompositePkOut
{
	x25519: [u8;32],
	kyber: MaybeUninit<[u8;1184]>,
}

#[repr(C)]
struct CompositeSk
{
	x25519: [u8;32],
	kyber: [u8;2400],
}

#[repr(C)]
struct CompositeSkOut
{
	x25519: [u8;32],
	kyber: MaybeUninit<[u8;2400]>,
}

#[repr(C)]
struct CompositeEk
{
	x25519: [u8;32],
	kyber: [u8;1088],
}

#[repr(C)]
struct CompositeEkOut
{
	x25519: [u8;32],
	kyber: MaybeUninit<[u8;1088]>,
}

#[repr(C)]
struct CompositeSS
{
	x25519: [u8;32],
	kyber: MaybeUninit<[u8;32]>,
}

pub(super) fn __encapsulate(pkr: &[u8;1216], rng: &mut TemporaryRandomStream) ->
	Result<([u8;64], [u8;1120]), HpkeError>
{
	let pkr: &CompositePk = unsafe{transmute(pkr)};
	let (ss1, ek1) = crate::x25519::__encapsulate(&pkr.x25519, rng)?;
	let mut ss = CompositeSS { x25519: ss1, kyber: MaybeUninit::uninit() };
	let mut ek = CompositeEkOut { x25519: ek1, kyber: MaybeUninit::uninit() };
	Kyber768::encapsulate(rng, &mut ek.kyber, &mut ss.kyber, &pkr.kyber);
	Ok(unsafe{(transmute(ss), transmute(ek))})
}

pub(super) fn __decapsulate(sk: &[u8;2432], pk: &[u8;1216], ek: &[u8;1120]) -> Result<[u8;64], HpkeError>
{
	let sk: &CompositeSk = unsafe{transmute(sk)};
	let pk: &CompositePk = unsafe{transmute(pk)};
	let ek: &CompositeEk = unsafe{transmute(ek)};
	let mut skx = sk.x25519;
	crate::x25519::clampkey(&mut skx);
	let ss1 = crate::x25519::__decapsulate(&skx, &pk.x25519, &ek.x25519)?;
	let mut ss = CompositeSS { x25519: ss1, kyber: MaybeUninit::uninit() };
	Kyber768::decapsulate(&mut ss.kyber, &ek.kyber, &sk.kyber).map_err(|_|{
		HpkeError::ExplicitReject
	})?;
	Ok(unsafe{transmute(ss)})
}

pub(super) fn __keygen(rng: &mut TemporaryRandomStream) -> Result<([u8;2432], [u8;1216]), HpkeError>
{
	let (sk1, pk1) = crate::x25519::__keygen(rng)?;
	let mut sk = CompositeSkOut { x25519: sk1, kyber: MaybeUninit::uninit() };
	let mut pk = CompositePkOut { x25519: pk1, kyber: MaybeUninit::uninit() };
	Kyber768::keypair(rng, &mut sk.kyber, &mut pk.kyber);
	Ok(unsafe{(transmute(sk), transmute(pk))})
}

pub(super) fn __keycheck(sk: &[u8;2432]) -> Result<[u8;1216], HpkeError>
{
	let sk: &CompositeSk = unsafe{transmute(sk)};
	let pk1 = crate::x25519::__keycheck(&sk.x25519)?;
	let mut pk = CompositePkOut { x25519: pk1, kyber: MaybeUninit::uninit() };
	//Public key is contained in private key at offset 1152.
	unsafe{copy_nonoverlapping::<u8>(sk.kyber.as_ptr().add(1152), pk.kyber.as_mut_ptr().cast(), 1184);}
	Ok(unsafe{transmute(pk)})
}

pub(super) fn __checkpk(_: &[u8;1216]) -> Result<(), HpkeError>
{
	Ok(())
}
