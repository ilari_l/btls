use btls_aux_eccdecompress::check_point_nist_p256 as check_point;
use btls_aux_fail::ResultExt;
include!(concat!(env!("OUT_DIR"), "/autogenerated-hpke-p256.inc.rs"));

pub(super) fn __checkpk(pk: &[u8;65]) -> Result<(), HpkeError>
{
	let x = pk[1..33].try_into().set_err(HpkeError::Impossible(901))?;
	let y = pk[33..65].try_into().set_err(HpkeError::Impossible(902))?;
	fail_if!(pk[0] != 4, HpkeError::UnknownPointEncoding);
	check_point(x, y).set_err(HpkeError::PointNotOnCurve)
}
