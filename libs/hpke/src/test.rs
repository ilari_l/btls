use super::*;
use btls_aux_nettle::EccCurve;
use btls_aux_nettle::NsaEcdhKey;
use core::mem::transmute;


fn h(b: &str) -> Vec<u8>
{
	let mut carry = None;
	let mut y = Vec::new();
	for c in b.chars() {
		if c.is_whitespace() { continue; }
		let c = c.to_digit(16).expect("Not hexdigit") as u8;
		if let Some(carry2) = carry {
			y.push(carry2 * 16 + c);
			carry = None;
		} else {
			carry = Some(c);
		}
	}
	if carry.is_some() { panic!("Odd number of hexes"); }
	y
}

fn b12(b: &[u8]) -> &[u8;12] { b.try_into().unwrap() }
fn b32(b: &[u8]) -> &[u8;32] { b.try_into().unwrap() }

fn clamp32(x: &mut Vec<u8>)
{
	x[0] &= 0xF8;
	x[31] &= 0x3F;
	x[31] |= 0x40;
}

#[test]
fn rfc9180_section_a_1_1()
{
	let mode = 0;
	let kem_id = KEM::X25519;
	let aead_id = AEAD::Aes128Gcm;
	let info = h("4f6465206f6e2061204772656369616e2055726e");
	let pkem = h("37fda3567bdbd628e88668c3c8d7e97d1d1253b6d4ea6d44c150f741f1bf4431");
	let skem = h("52c4a758a802cd8b936eceea314432798d5baf2d7e9235dc084ab1b9cfa2f736");
	let pkrm = h("3948cfe0ad1ddb695d780e59077195da6c56506b027329794ab02bca80815c4d");
	let skrm = h("4612c550263fc8ad58375df3f557aac531d26850903e55a9f23f21d8534e8ac8");
	let ss =   h("fe0e18c9f024ce43799ae393c7e8fe8fce9d218875e8227b0187c04e7d2ea1fc");
	let key =  h("4531685d41d65f03dc48f6b8302c05b0");
	let bnce = h("56d890e5accaaf011cff4b7d");
	let exps = h("45ff1c2e220db587171952c0592d5f5ebe103f1561a2614e38f2ffd47e99e3f8");
	let ss2 = kem_hlpr::<HKDF<btls_aux_hash::Sha256>>(&pkem, &skem, &pkrm, &skrm, kem_id);
	kdf_tail::<HKDF<btls_aux_hash::Sha256>,_>(ss2, mode, &ss, &info, kem_id, aead_id, 16, 12, None, &key, &bnce,
		&exps);
}

#[test]
fn rfc9180_section_a_1_1_1()
{
	let     kem_id = KEM::X25519;
	let     kdf_id = KDF::Sha256;
	let     aead_id = AEAD::Aes128Gcm;
	let     alg =  btls_aux_aead::ProtectorType::Aes128Gcm;
	let     key =  h("4531685d41d65f03dc48f6b8302c05b0");
	let     bnce = h("56d890e5accaaf011cff4b7d");
	let     exps = h("45ff1c2e220db587171952c0592d5f5ebe103f1561a2614e38f2ffd47e99e3f8");
	let mut ctx = EncryptContext {
		key: alg.new_encryptor(&key).expect("new_encryptor"),
		base_nonce: Nonce::X12(*b12(&bnce)),
		exporter: ExportContext(SuiteFull(kem_id, kdf_id, AEAD2::Normal(aead_id)), exps.clone()),
		seq: Some(0),
	};
	assert_eq!(ctx.seq, Some(0));
	let ct = ctx.encrypt(
		&h("436f756e742d30"),
		&h("4265617574792069732074727574682c20747275746820626561757479")).expect("encrypt");
	assert_eq!(&ct, &h("f938558b5d72f1a23810b4be2ab4f84331acc02fc97babc53a52ae8218a355a9\
		6d8770ac83d07bea87e13c512a"));
	assert_eq!(ctx.seq, Some(1));
	let ct = ctx.encrypt(
		&h("436f756e742d31"),
		&h("4265617574792069732074727574682c20747275746820626561757479")).expect("encrypt");
	assert_eq!(&ct, &h("af2d7e9ac9ae7e270f46ba1f975be53c09f8d875bdc8535458c2494e8a6eab25\
		1c03d0c22a56b8ca42c2063b84"));
	assert_eq!(ctx.seq, Some(2));
	let ct = ctx.encrypt(
		&h("436f756e742d32"),
		&h("4265617574792069732074727574682c20747275746820626561757479")).expect("encrypt");
	assert_eq!(&ct, &h("498dfcabd92e8acedc281e85af1cb4e3e31c7dc394a1ca20e173cb7251649158\
		8d96a19ad4a683518973dcc180"));
	assert_eq!(ctx.seq, Some(3));
	ctx.seq = Some(4);
	assert_eq!(ctx.seq, Some(4));
	let ct = ctx.encrypt(
		&h("436f756e742d34"),
		&h("4265617574792069732074727574682c20747275746820626561757479")).expect("encrypt");
	assert_eq!(&ct, &h("583bd32bc67a5994bb8ceaca813d369bca7b2a42408cddef5e22f880b631215a\
		09fc0012bc69fccaa251c0246d"));
	assert_eq!(ctx.seq, Some(5));
	ctx.seq = Some(255);
	assert_eq!(ctx.seq, Some(255));
	let ct = ctx.encrypt(
		&h("436f756e742d323535"),
		&h("4265617574792069732074727574682c20747275746820626561757479")).expect("encrypt");
	assert_eq!(&ct, &h("7175db9717964058640a3a11fb9007941a5d1757fda1a6935c805c21af32505b\
		f106deefec4a49ac38d71c9e0a"));
	assert_eq!(ctx.seq, Some(256));
	let ct = ctx.encrypt(
		&h("436f756e742d323536"),
		&h("4265617574792069732074727574682c20747275746820626561757479")).expect("encrypt");
	assert_eq!(&ct, &h("957f9800542b0b8891badb026d79cc54597cb2d225b54c00c5238c25d05c30e3\
		fbeda97d2e0e1aba483a2df9f2"));
}

#[test]
fn rfc9180_section_a_1_1_2()
{
	let     kem_id = KEM::X25519;
	let     kdf_id = KDF::Sha256;
	let     aead_id = AEAD::Aes128Gcm;
	let     exps = h("45ff1c2e220db587171952c0592d5f5ebe103f1561a2614e38f2ffd47e99e3f8");
	let exporter = ExportContext(SuiteFull(kem_id, kdf_id, AEAD2::Normal(aead_id)), exps.clone());
	let mut tmp = [0;32];
	exporter.export(&h(""), &mut tmp);
	assert_eq!(&tmp, h("3853fe2b4035195a573ffc53856e77058e15d9ea064de3e59f4961d0095250ee").deref());
	exporter.export(&h("00"), &mut tmp);
	assert_eq!(&tmp, h("2e8f0b54673c7029649d4eb9d5e33bf1872cf76d623ff164ac185da9e88c21a5").deref());
	exporter.export(&h("54657374436f6e74657874"), &mut tmp);
	assert_eq!(&tmp, h("e9e43065102c3836401bed8c3c3c75ae46be1639869391d62c61f1ec7af54931").deref());	
}

#[test]
fn rfc9180_section_a_1_2()
{
	let mode = 0;	//__generic_kdf internally turns this to 1.
	let kem_id = KEM::X25519;
	let aead_id = AEAD::Aes128Gcm;
	let info = h("4f6465206f6e2061204772656369616e2055726e");
	let pkem = h("0ad0950d9fb9588e59690b74f1237ecdf1d775cd60be2eca57af5a4b0471c91b");
	let skem = h("463426a9ffb42bb17dbe6044b9abd1d4e4d95f9041cef0e99d7824eef2b6f588");
	let pkrm = h("9fed7e8c17387560e92cc6462a68049657246a09bfa8ade7aefe589672016366");
	let skrm = h("c5eb01eb457fe6c6f57577c5413b931550a162c71a03ac8d196babbd4e5ce0fd");
	let ss =   h("727699f009ffe3c076315019c69648366b69171439bd7dd0807743bde76986cd");
	let key =  h("15026dba546e3ae05836fc7de5a7bb26");
	let bnce = h("9518635eba129d5ce0914555");
	let exps = h("3d76025dbbedc49448ec3f9080a1abab6b06e91c0b11ad23c912f043a0ee7655");
	let pski = h("456e6e796e20447572696e206172616e204d6f726961");
	let psk =  h("0247fd33b913760fa1fa51e1892d9f307fbe65eb171e8132c2af18555a738b82");
	let ss2 = kem_hlpr::<HKDF<btls_aux_hash::Sha256>>(&pkem, &skem, &pkrm, &skrm, kem_id);
	kdf_tail::<HKDF<btls_aux_hash::Sha256>,_>(ss2, mode, &ss, &info, kem_id, aead_id, 16, 12,
		Some((&pski, &psk)), &key, &bnce, &exps);
}

#[test]
fn rfc9180_section_a_1_3()
{
	let mode = 2;
	let kem_id = KEM::X25519;
	let aead_id = AEAD::Aes128Gcm;
	let info = h("4f6465206f6e2061204772656369616e2055726e");
	let pkem = h("23fb952571a14a25e3d678140cd0e5eb47a0961bb18afcf85896e5453c312e76");
	let skem = h("ff4442ef24fbc3c1ff86375b0be1e77e88a0de1e79b30896d73411c5ff4c3518");
	let pkrm = h("1632d5c2f71c2b38d0a8fcc359355200caa8b1ffdf28618080466c909cb69b2e");
	let skrm = h("fdea67cf831f1ca98d8e27b1f6abeb5b7745e9d35348b80fa407ff6958f9137e");
	let pksm = h("8b0c70873dc5aecb7f9ee4e62406a397b350e57012be45cf53b7105ae731790b");
	let sksm = h("dc4a146313cce60a278a5323d321f051c5707e9c45ba21a3479fecdf76fc69dd");
	let ss =   h("2d6db4cf719dc7293fcbf3fa64690708e44e2bebc81f84608677958c0d4448a7");
	let key =  h("b062cb2c4dd4bca0ad7c7a12bbc341e6");
	let bnce = h("a1bc314c1942ade7051ffed0");
	let exps = h("ee1a093e6e1c393c162ea98fdf20560c75909653550540a2700511b65c88c6f1");
	let ss2 = kem_hlpr_auth::<HKDF<btls_aux_hash::Sha256>>(&pkem, &skem, &pkrm, &skrm, &pksm, &sksm, kem_id);
	kdf_tail::<HKDF<btls_aux_hash::Sha256>,_>(ss2, mode, &ss, &info, kem_id, aead_id, 16, 12, None, &key,
		&bnce, &exps);
}

#[test]
fn rfc9180_section_a_1_4()
{
	let mode = 2;	//__generic_kdf internally turns this to 3.
	let kem_id = KEM::X25519;
	let aead_id = AEAD::Aes128Gcm;
	let info = h("4f6465206f6e2061204772656369616e2055726e");
	let pkem = h("820818d3c23993492cc5623ab437a48a0a7ca3e9639c140fe1e33811eb844b7c");
	let skem = h("14de82a5897b613616a00c39b87429df35bc2b426bcfd73febcb45e903490768");
	let pkrm = h("1d11a3cd247ae48e901939659bd4d79b6b959e1f3e7d66663fbc9412dd4e0976");
	let skrm = h("cb29a95649dc5656c2d054c1aa0d3df0493155e9d5da6d7e344ed8b6a64a9423");
	let pksm = h("2bfb2eb18fcad1af0e4f99142a1c474ae74e21b9425fc5c589382c69b50cc57e");
	let sksm = h("fc1c87d2f3832adb178b431fce2ac77c7ca2fd680f3406c77b5ecdf818b119f4");
	let ss =   h("f9d0e870aba28d04709b2680cb8185466c6a6ff1d6e9d1091d5bf5e10ce3a577");
	let key =  h("1364ead92c47aa7becfa95203037b19a");
	let bnce = h("99d8b5c54669807e9fc70df1");
	let exps = h("f048d55eacbf60f9c6154bd4021774d1075ebf963c6adc71fa846f183ab2dde6");
	let pski = h("456e6e796e20447572696e206172616e204d6f726961");
	let psk =  h("0247fd33b913760fa1fa51e1892d9f307fbe65eb171e8132c2af18555a738b82");
	let ss2 = kem_hlpr_auth::<HKDF<btls_aux_hash::Sha256>>(&pkem, &skem, &pkrm, &skrm, &pksm, &sksm, kem_id);
	kdf_tail::<HKDF<btls_aux_hash::Sha256>,_>(ss2, mode, &ss, &info, kem_id, aead_id, 16, 12,
		Some((&pski, &psk)), &key, &bnce, &exps);
}

#[test]
fn rfc9180_section_a_2_1_1()
{
	let     kem_id = KEM::X25519;
	let     kdf_id = KDF::Sha256;
	let     aead_id = AEAD::Chacha20Poly1305;
	let     alg =  btls_aux_aead::ProtectorType::Chacha20Poly1305;
	let     key =  h("ad2744de8e17f4ebba575b3f5f5a8fa1f69c2a07f6e7500bc60ca6e3e3ec1c91");
	let     bnce = h("5c4d98150661b848853b547f");
	let     exps = h("a3b010d4994890e2c6968a36f64470d3c824c8f5029942feb11e7a74b2921922");
	let mut ctx = EncryptContext {
		key: alg.new_encryptor(&key).expect("new_encryptor"),
		base_nonce: Nonce::X12(*b12(&bnce)),
		exporter: ExportContext(SuiteFull(kem_id, kdf_id, AEAD2::Normal(aead_id)), exps.clone()),
		seq: Some(0),
	};
	assert_eq!(ctx.seq, Some(0));
	let ct = ctx.encrypt(
		&h("436f756e742d30"),
		&h("4265617574792069732074727574682c20747275746820626561757479")).expect("encrypt");
	assert_eq!(&ct, &h("1c5250d8034ec2b784ba2cfd69dbdb8af406cfe3ff938e131f0def8c8b60b4db\
		21993c62ce81883d2dd1b51a28"));
	assert_eq!(ctx.seq, Some(1));
	let ct = ctx.encrypt(
		&h("436f756e742d31"),
		&h("4265617574792069732074727574682c20747275746820626561757479")).expect("encrypt");
	assert_eq!(&ct, &h("6b53c051e4199c518de79594e1c4ab18b96f081549d45ce015be002090bb119e\
		85285337cc95ba5f59992dc98c"));
	assert_eq!(ctx.seq, Some(2));
	let ct = ctx.encrypt(
		&h("436f756e742d32"),
		&h("4265617574792069732074727574682c20747275746820626561757479")).expect("encrypt");
	assert_eq!(&ct, &h("71146bd6795ccc9c49ce25dda112a48f202ad220559502cef1f34271e0cb4b02\
		b4f10ecac6f48c32f878fae86b"));
	assert_eq!(ctx.seq, Some(3));
	ctx.seq = Some(4);
	assert_eq!(ctx.seq, Some(4));
	let ct = ctx.encrypt(
		&h("436f756e742d34"),
		&h("4265617574792069732074727574682c20747275746820626561757479")).expect("encrypt");
	assert_eq!(&ct, &h("63357a2aa291f5a4e5f27db6baa2af8cf77427c7c1a909e0b37214dd47db122b\
		b153495ff0b02e9e54a50dbe16"));
	assert_eq!(ctx.seq, Some(5));
	ctx.seq = Some(255);
	assert_eq!(ctx.seq, Some(255));
	let ct = ctx.encrypt(
		&h("436f756e742d323535"),
		&h("4265617574792069732074727574682c20747275746820626561757479")).expect("encrypt");
	assert_eq!(&ct, &h("18ab939d63ddec9f6ac2b60d61d36a7375d2070c9b683861110757062c52b888\
		0a5f6b3936da9cd6c23ef2a95c"));
	assert_eq!(ctx.seq, Some(256));
	let ct = ctx.encrypt(
		&h("436f756e742d323536"),
		&h("4265617574792069732074727574682c20747275746820626561757479")).expect("encrypt");
	assert_eq!(&ct, &h("7a4a13e9ef23978e2c520fd4d2e757514ae160cd0cd05e556ef692370ca53076\
		214c0c40d4c728d6ed9e727a5b"));
}

#[test]
fn rfc9180_section_a_3_1()
{
	let mode = 0;
	let kem_id = KEM::P256;
	let aead_id = AEAD::Aes128Gcm;
	let info = h("4f6465206f6e2061204772656369616e2055726e");
	let pkem = h("04a92719c6195d5085104f469a8b9814d5838ff72b60501e2c4466e5e67b32\
		5ac98536d7b61a1af4b78e5b7f951c0900be863c403ce65c9bfcb9382657222d18c4");
	let skem = h("4995788ef4b9d6132b249ce59a77281493eb39af373d236a1fe415cb0c2d7beb");
	let pkrm = h("04fe8c19ce0905191ebc298a9245792531f26f0cece2460639e8bc39cb7f70\
		6a826a779b4cf969b8a0e539c7f62fb3d30ad6aa8f80e30f1d128aafd68a2ce72ea0");
	let skrm = h("f3ce7fdae57e1a310d87f1ebbde6f328be0a99cdbcadf4d6589cf29de4b8ffd2");
	let ss =   h("c0d26aeab536609a572b07695d933b589dcf363ff9d93c93adea537aeabb8cb8");
	let key =  h("868c066ef58aae6dc589b6cfdd18f97e");
	let bnce = h("4e0bc5018beba4bf004cca59");
	let exps = h("14ad94af484a7ad3ef40e9f3be99ecc6fa9036df9d4920548424df127ee0d99f");
	let ss2 = kem_hlpr_nsa::<HKDF<btls_aux_hash::Sha256>>(&pkem, &skem, &pkrm, &skrm, kem_id, 0, 32);
	kdf_tail::<HKDF<btls_aux_hash::Sha256>,_>(ss2, mode, &ss, &info, kem_id, aead_id, 16, 12, None, &key, &bnce,
		&exps);
}

#[test]
fn rfc9180_section_a_4_1()
{
	let mode = 0;
	let kem_id = KEM::P256;
	let aead_id = AEAD::Aes128Gcm;
	let info = h("4f6465206f6e2061204772656369616e2055726e");
	let pkem = h("0493ed86735bdfb978cc055c98b45695ad7ce61ce748f4dd63c525a3b8d53a\
		15565c6897888070070c1579db1f86aaa56deb8297e64db7e8924e72866f9a472580");
	let skem = h("2292bf14bb6e15b8c81a0f45b7a6e93e32d830e48cca702e0affcfb4d07e1b5c");
	let pkrm = h("04085aa5b665dc3826f9650ccbcc471be268c8ada866422f739e2d531d4a88\
		18a9466bc6b449357096232919ec4fe9070ccbac4aac30f4a1a53efcf7af90610edd");
	let skrm = h("3ac8530ad1b01885960fab38cf3cdc4f7aef121eaa239f222623614b4079fb38");
	let ss =   h("02f584736390fc93f5b4ad039826a3fa08e9911bd1215a3db8e8791ba533cafd");
	let key =  h("090ca96e5f8aa02b69fac360da50ddf9");
	let bnce = h("9c995e621bf9a20c5ca45546");
	let exps = h("4a7abb2ac43e6553f129b2c5750a7e82d149a76ed56dc342d7bca61e26d494\
		f4855dff0d0165f27ce57756f7f16baca006539bb8e4518987ba610480ac03efa8");
	let ss2 = kem_hlpr_nsa::<HKDF<btls_aux_hash::Sha256>>(&pkem, &skem, &pkrm, &skrm, kem_id, 0, 32);
	kdf_tail::<HKDF<btls_aux_hash::Sha512>,_>(ss2, mode, &ss, &info, kem_id, aead_id, 16, 12, None, &key, &bnce,
		&exps);
}

#[test]
fn rfc9180_section_a_4_1_2()
{
	let     kem_id = KEM::P256;
	let     kdf_id = KDF::Sha512;
	let     aead_id = AEAD::Aes128Gcm;
	let exps = h("4a7abb2ac43e6553f129b2c5750a7e82d149a76ed56dc342d7bca61e26d494\
		f4855dff0d0165f27ce57756f7f16baca006539bb8e4518987ba610480ac03efa8");
	let exporter = ExportContext(SuiteFull(kem_id, kdf_id, AEAD2::Normal(aead_id)), exps.clone());
	let mut tmp = [0;32];
	exporter.export(&h(""), &mut tmp);
	assert_eq!(&tmp, h("a32186b8946f61aeead1c093fe614945f85833b165b28c46bf271abf16b57208").deref());
	exporter.export(&h("00"), &mut tmp);
	assert_eq!(&tmp, h("84998b304a0ea2f11809398755f0abd5f9d2c141d1822def79dd15c194803c2a").deref());
	exporter.export(&h("54657374436f6e74657874"), &mut tmp);
	assert_eq!(&tmp, h("93fb9411430b2cfa2cf0bed448c46922a5be9beff20e2e621df7e4655852edbc").deref());	
}

#[test]
fn rfc9180_section_a_6_1()
{
	let mode = 0;
	let kem_id = KEM::P521;
	let aead_id = AEAD::Aes256Gcm;
	let info = h("4f6465206f6e2061204772656369616e2055726e");
	let pkem = h("040138b385ca16bb0d5fa0c0665fbbd7e69e3ee29f63991d3e9b5fa740aab8\
		900aaeed46ed73a49055758425a0ce36507c54b29cc5b85a5cee6bae0cf1c21f2731\
		ece2013dc3fb7c8d21654bb161b463962ca19e8c654ff24c94dd2898de12051f1ed0\
		692237fb02b2f8d1dc1c73e9b366b529eb436e98a996ee522aef863dd5739d2f29b0");
	let skem = h("014784c692da35df6ecde98ee43ac425dbdd0969c0c72b42f2e708ab9d5354\
		15a8569bdacfcc0a114c85b8e3f26acf4d68115f8c91a66178cdbd03b7bcc5291e374b");
	let pkrm = h("0401b45498c1714e2dce167d3caf162e45e0642afc7ed435df7902ccae0e84\
		ba0f7d373f646b7738bbbdca11ed91bdeae3cdcba3301f2457be452f271fa6837580\
		e661012af49583a62e48d44bed350c7118c0d8dc861c238c72a2bda17f64704f464b\
		57338e7f40b60959480c0e58e6559b190d81663ed816e523b6b6a418f66d2451ec64");
	let skrm = h("01462680369ae375e4b3791070a7458ed527842f6a98a79ff5e0d4cbde83c2\
		7196a3916956655523a6a2556a7af62c5cadabe2ef9da3760bb21e005202f7b2462847");
	let ss =   h("776ab421302f6eff7d7cb5cb1adaea0cd50872c71c2d63c30c4f1d5e436533\
		36fef33b103c67e7a98add2d3b66e2fda95b5b2a667aa9dac7e59cc1d46d30e818");
	let key =  h("751e346ce8f0ddb2305c8a2a85c70d5cf559c53093656be636b9406d4d7d1b70");
	let bnce = h("55ff7a7d739c69f44b25447b");
	let exps = h("e4ff9dfbc732a2b9c75823763c5ccc954a2c0648fc6de80a58581252d0ee321\
		5388a4455e69086b50b87eb28c169a52f42e71de4ca61c920e7bd24c95cc3f992");
	let ss2 = kem_hlpr_nsa::<HKDF<btls_aux_hash::Sha512>>(&pkem, &skem, &pkrm, &skrm, kem_id, 2, 66);
	kdf_tail::<HKDF<btls_aux_hash::Sha512>,_>(ss2, mode, &ss, &info, kem_id, aead_id, 32, 12, None, &key, &bnce,
		&exps);
}

#[test]
fn rfc9180_section_a_7_1()
{
	let mode = 0;
	let kem_id = KEM::X25519;
	let info = h("4f6465206f6e2061204772656369616e2055726e");
	let pkem = h("e5e8f9bfff6c2f29791fc351d2c25ce1299aa5eaca78a757c0b4fb4bcd830918");
	let skem = h("095182b502f1f91f63ba584c7c3ec473d617b8b4c2cec3fad5af7fa6748165ed");
	let pkrm = h("194141ca6c3c3beb4792cd97ba0ea1faff09d98435012345766ee33aae2d7664");
	let skrm = h("33d196c830a12f9ac65d6e565a590d80f04ee9b19c83c87f2c170d972a812848");
	let ss =   h("e81716ce8f73141d4f25ee9098efc968c91e5b8ce52ffff59d64039e82918b66");
	let exps = h("79dc8e0509cf4a3364ca027e5a0138235281611ca910e435e8ed58167c72f79b");
	let ss2 = kem_hlpr::<HKDF<btls_aux_hash::Sha256>>(&pkem, &skem, &pkrm, &skrm, kem_id);
	assert_eq!(ss2.as_bytes(), &ss);
	let (_, exp) = __generic_kdf::<HKDF<btls_aux_hash::Sha256>,(),_>(&ss, mode, &info, kem_id,
		AEAD2::ExportOnly, 0, 0, None, |_, _|()).expect("__generic_kdf failed");
	assert_eq!(&exp.1, &exps);
}

#[test]
fn kyber_crystals()
{
	let pk = h("a3aa882fee0de0059cec0569c8e1b4872fb6cb4d82361b72ee1148dc7ddc0c
2b210747403222b16597f4881d694c12366c53fde2b3d346b7ee87b16dd42f44ec59
4cea6ba78b256092cbbc16baaf6ccc46f2386da22de9d142f593739eb9c245018e0c
61975514ac42639d3c5b0299b772acd59d55520a5d660f135075e33a673fd5b9e2d5
6803889fc62b0362f8cbe9990cb36b4cdef17586c8cc58d72d84fb9398f1c1efb0a6
282508083c23965a9851acb89afc723e7a6c60bc4007a41ad1950c4590a2f8d2bb3b
832f5db1707ad8bad1c4c426aaa7da97b34a921283415851f19b0f01ca3924754dba
6596f9329454b1e3d9b5f357a66c59bf5fc4a045908b5eb107d3302f0cb9be0af958
4846c1475b92d3c16051935dc7411acaa64c80c836b0643fd72b38cb0a33feb11f48
13b66f705268b3838b8974e28c12b4f9bbc8623c936b32a015262d4a33172b7f3a69
b6c2fab5a3c18ffdab2927e77598d1556d51a8559550c251796290b617ac9804167b
d9a76e9d8bba64059d165acfe2483e9ed0cbc11cb71dd148776aa1cb862ce2b1026e
773600d101a300671a70710a877a5c1732275c362085b2b8cc66206b3ec37c82ac87
3d1ec1862a8aa457fc9776960b396c23768c931cdc77731792c569c2088c52ddb5cc
0c90ab9187c1e0ca2c98818859aa86fe44801be483cc1469d636cd3e019267c1cc68
4640359ca67c5abd1dc100c4d3c5924acf1b988d3b5019e7b06ef238412b7608dd23
115c6047a59b4b1d7a731126925728c645c140aa4704c1b808b6c401be736bf18bb7
d654342c6576236565c6c5b0727b25ae773c5fb76be794304dc1b672aa5909659b6b
b8a1f430a141882b0f9753662794e625885782154dc148e632b6b2079087958d83c6
c82cf55a47eb4ed819a409d94ceb0c74e8d497b95975a0a5c659f5bf0a033d2adca9
8a693304413fff95342319a09fd62f263b91a2c6540d2196dd2ba90dd113042428ae
eb15156c03949660776b80bc1501b0d80a946a623906291ed3668f3c99c1889d3ae3
c59819c38f6b0c46558c2ca520c2107c166452b917cea53bb50c4cb839a99f60e54e
9236c6a419a8de5508f4e3545409499b97939ee940a9d48ed5547003350e391b4c96
d657cb395b5c035370e9c8ece32c83b3cff347ca16bb1e2943669f370f48e70462d4
369a07804bc09fcf399bc2d11b47b0370660916944a179423519a310cc0737407c55
ef09255530c7ec817999c95e20aa23f8f6782aa820d34c89c2299ff0ec9a9021b6f7
dbbd19503fa6f170d8770e12875d558bbb2ca66fd1136e0e5729ef30346109cd289a
1ce0c531a493581ed64533e1749fc818b85ab664255bbfe4a641f6bdf43ac1695c28
ab2b58b3bab5bed5893439455b669b63d65ceff75b8c5857f4ba5cf767cf57aa8e28
691cc6dc67fca434e3b1560c6c53ce37c2a2f14764c1cf1e5697cd8757a544b05b76
6f4400cef7ecc46ec29a1d679d7fe385c4366579db06d1d840c9911fab8b6b5df203
5cb95410f79b861411b4eb5a4119208f8872674639617452f6b6394c94c6d6f5b833
690dd98406b5e7c0827b1a3617a03ba90c3d185a954252f1ba5b157a3f61749548e2
81fc543dec205e757932bcc717b99b7df7123500f3bcc660c080093b3fbac56ff51b
9c3b037f76e3f43c0e46b5588cf617f4de85044390a9947daacba87cd5");
	let sk = h("cf61f1a7b05c83f9c2a4b27dc0e9bdbf4e52ba1bbd906cb3776ac12268a9f4d0
c348342d192f0458ab53d19c1dc135d11b48978c878bca6d7d1bc91428259e43aa
dc9700b76aa9aa66a65db91a77d72513e40697226557b53400bb6752fb4e11a5ba2f
e12644698a48c9948ec121cc9c9ce7384c65f798012c9df8f5ac0cd371d7d19d9a24
c30cb0909c665e43c89328735fe95a62653352fad3cfe6330b436a4f72c9ac9323ba
bd912cf5970222eb0dd178c810bcc79beba0813039ec4333c33b13d4cc5183b6b14d
c09cb9604d46242353a1a1df82999e4a4929f28f498c330d552ac64156cf123cceeb
bccf81b2fd86218f2a9112040943a359d7a858cd641467e54b25f03d66b9a150fbcf
3f19bbd1791abec47269b2a72f4083a79c2559fbb6d0500208a78baa7392874443c3
9c38577b4c40db6220ca5c84b148ffb344164c723df1c0fb37ae52c0854bea023e2a
45efaa8869c924ecf360008607e7079187978fdac30e8b76a3110349de272b25f549
0dc87e3d8caf59a5a51a57230ea702dfda0f5d2c0fe94442254834b0f6aa71852601
a8c5b7b211f108c1de2b1092e9b89df4a9feea882aa00ab97235940924e8a81d8f83
597f72383f7a1b99c3c8481953be917fef0b44e32b0aa1f862eedc8d0d94030ae92e
73097cde1b34de9b8279293322e0b5f9564395cb4998810818544ad2025018c40deb
f0b97bca2f1d861f8d5b51a2a84f35503cb37112b280ad0a4c99a2eb9c43300ce7c6
6eb89cb4443a44edc40869b8c2d90c5d484554557c408da7b46752bec14876815334
b783207f60943d1738b5183d64394e27bb8f1dbb6ed9c58aa338171967bf5a613e91
94c13395573615cee02012438a68aa104afd56a943d05caefc7f20a0104e2cced2a5
191a1a68fe431920e1844a8154fc42a73d70c82f26846ec332fda50c340c1c503796
5daaccd3cacfcab3c85a7516d712890fd6a2b1f5cb7c745cf1798dc0a49ed7571763
0c78d56bb8db272a85a009b2685ca9f4840c948226d224de1a0385565e569b8901c4
508ae7b9214b88b6c2ac63807710d85e593a01ba20541cd03fa8364b4cb79f110745
b30818521a7d0b6015a20483dde33188e94fc4aa224558bd53d384a9f6916964bbef
0b0770b11e7b4117f41639bbe0c9ce119c8f8aab451608c2f06a8cd85f37519b7e3c
1f9f07a6449059a972260cf80c23e52fe1b559e11c723b2618752672bfab67305358
e7f048960475f1c720d8ba5fe4883981065c462c5062757bddd2666de67265990d00
53229693a8bfd8811c84494853095c875639dcbcfcc02785910e35643f5bb4b0aa59
af7a86ae94dcc01f952eb1d151c4ba1aa4da02c100b461904229f7b11aac35d307ab
187255baa32eed32b3b262aaf2db6019089ad4250079280a0efb109ab27a364135ac
3067ace5c82dea1fafb04dfedba9fabc196832878eb7b4314556e8aa8210e2c72959
723e23176b703d4db42aabba62229790f6a743a2ec3c43dc8dbe0b4c36dc2323ec0e
f21c116941b43bb12763460eed032a7a039185e36dcbf69d88f645e6728d3ba79dae
0a25ddd4c3a8bba8334aa8fb6658a9dca99a8cc6362745d6080b0fd8af6af71e9f75
2d7b763035ec40c0fc98326081ea4c36cdf992e73a16719b9fb7c06e6c1bb7210747
403222b16597f4881d694c12366c53fde2b3d346b7ee87b16dd42f44ec594cea6ba7
8b256092cbbc16baaf6ccc46f2386da22de9d142f593739eb9c245018e0c61975514
ac42639d3c5b0299b772acd59d55520a5d660f135075e33a673fd5b9e2d56803889f
c62b0362f8cbe9990cb36b4cdef17586c8cc58d72d84fb9398f1c1efb0a628250808
3c23965a9851acb89afc723e7a6c60bc4007a41ad1950c4590a2f8d2bb3b832f5db1
707ad8bad1c4c426aaa7da97b34a921283415851f19b0f01ca3924754dba6596f932
9454b1e3d9b5f357a66c59bf5fc4a045908b5eb107d3302f0cb9be0af9584846c147
5b92d3c16051935dc7411acaa64c80c836b0643fd72b38cb0a33feb11f4813b66f70
5268b3838b8974e28c12b4f9bbc8623c936b32a015262d4a33172b7f3a69b6c2fab5
a3c18ffdab2927e77598d1556d51a8559550c251796290b617ac9804167bd9a76e9d
8bba64059d165acfe2483e9ed0cbc11cb71dd148776aa1cb862ce2b1026e773600d1
01a300671a70710a877a5c1732275c362085b2b8cc66206b3ec37c82ac873d1ec186
2a8aa457fc9776960b396c23768c931cdc77731792c569c2088c52ddb5cc0c90ab91
87c1e0ca2c98818859aa86fe44801be483cc1469d636cd3e019267c1cc684640359c
a67c5abd1dc100c4d3c5924acf1b988d3b5019e7b06ef238412b7608dd23115c6047
a59b4b1d7a731126925728c645c140aa4704c1b808b6c401be736bf18bb7d654342c
6576236565c6c5b0727b25ae773c5fb76be794304dc1b672aa5909659b6bb8a1f430
a141882b0f9753662794e625885782154dc148e632b6b2079087958d83c6c82cf55a
47eb4ed819a409d94ceb0c74e8d497b95975a0a5c659f5bf0a033d2adca98a693304
413fff95342319a09fd62f263b91a2c6540d2196dd2ba90dd113042428aeeb15156c
03949660776b80bc1501b0d80a946a623906291ed3668f3c99c1889d3ae3c59819c3
8f6b0c46558c2ca520c2107c166452b917cea53bb50c4cb839a99f60e54e9236c6a4
19a8de5508f4e3545409499b97939ee940a9d48ed5547003350e391b4c96d657cb39
5b5c035370e9c8ece32c83b3cff347ca16bb1e2943669f370f48e70462d4369a0780
4bc09fcf399bc2d11b47b0370660916944a179423519a310cc0737407c55ef092555
30c7ec817999c95e20aa23f8f6782aa820d34c89c2299ff0ec9a9021b6f7dbbd1950
3fa6f170d8770e12875d558bbb2ca66fd1136e0e5729ef30346109cd289a1ce0c531
a493581ed64533e1749fc818b85ab664255bbfe4a641f6bdf43ac1695c28ab2b58b3
bab5bed5893439455b669b63d65ceff75b8c5857f4ba5cf767cf57aa8e28691cc6dc
67fca434e3b1560c6c53ce37c2a2f14764c1cf1e5697cd8757a544b05b766f4400ce
f7ecc46ec29a1d679d7fe385c4366579db06d1d840c9911fab8b6b5df2035cb95410
f79b861411b4eb5a4119208f8872674639617452f6b6394c94c6d6f5b833690dd984
06b5e7c0827b1a3617a03ba90c3d185a954252f1ba5b157a3f61749548e281fc543d
ec205e757932bcc717b99b7df7123500f3bcc660c080093b3fbac56ff51b9c3b037f
76e3f43c0e46b5588cf617f4de85044390a9947daacba87cd5137b60651b30bf805d
a1597faef1bc8b2645cda273144c4af1d13eaa2ad9101c7b58b14601aff81754afc7
76f8b7f7b9324d420b66706b96ea7f99f8fa11bed3");
	let ek = h("1d06980e46fd3842db6b87226231eedd2cc9684ee98a1d9d902bd9300e2c4d4
1b64fba47a50fe32dd0df3b0a75801c11022cd98a6ff5a83a8472ade82bdd6f1e8a6
5a94a88523ada0d8275165f707f1067a6a576e54525d9141e95223f5713456bda7ec
5eb558adfc6b7f0d80de46222579a3274e45ab43fad14f7e9855a872d2716e8dc78d
4c12027bef3184904476c8961552fd031361358f2d9deae8ad98194047a142229476
12972574c57514266e9e67a3b6dd89972cc8a0882be7474f4923549dfcd944dcbe58
b088079aa8b70c8f291cb4e45066bad4a832ccd8f40e51861f7a25b6a2358842f1bb
e8108a6a6f0ae93153a2e7f9f53e180a90532531a632367b81bf08ed97effcf0140d
d0e92cc438f6be7e6f3d97a9f7787f7e3981f971617f0bfb618caa7db1e453f33a38
6c3863b16d462229c41f4946b49e4e49c27e0f35d77e21304b6ad238a55a51e9e370
dd39e713d626044fb970bb7c2af7d7b9cb9004394741a0ea2de592816359006f24ab
dfc2aa890720b00b2f7b8bb240120f22bdb84f9fc5c8fdc7ca7047ae633868c184c4
d75e9e107eb9c6d8fe879415926457d818bc31e88b87a5881584a5650859e88b06fa
a2cfe1bde95dfa344af14f214cedecde4d89c87334c33e2d7ee3ab40d5df396cf0ff
5a99588e0dcd205f1d876b380b963f5baccc0baeae569892a8d252f5eeeb7c751f66
3eb906ac99a165656224281add3ab271ff4f406b6932cbf1afff62109794f52ff3e7
23f5cdd706e3715d1d2d421bdac73fa047b5d9761569534fb2dd57b86a608f79db7d
4ab99847490e76eaf0c683bdc54d12f2f2664a79de6a2f25bec3f43584f98ec41ad3
fb19ba5ba936c3c893e9c0994b412ba3d07329086c20b04e1cd1d9b4f24a82f8c1f7
b5db58b4056a4b4e27b60c957f5af8081bffab98d8455cab97e35042ed636c995931
fd304b3d02fcf545df360cc421be64adc3d7a121ea75ab3440a9eba74fba1c5b40bd
b66b54583ff2f76304ccaeae99ed94fb332d30d771fe0e45acb9e966f497b1629f5a
5df15cea507d2fd1aa045a171e84bec932e4049639477f16fb9afdd107668f9b3531
c3c7eb1d67753ac652c575b526e6f2965f1e4500e99f38ae1d34bce151a68e278f14
405ad76f580b549d025b03be98b6a737f10238b9f84f1694173544ba2c97f811a174
85129a146084bc5382e2086aaf51b11a4918bdb5485bf28a9be2d2c9d69468268fa0
4fa071c39942b43a0caf561278cfc1b47781fa9ef559f86b2dad703141b78b7ddb35
c9c9ff4c1134580da26367dbf3db7eaf039dfbae238959c4cf55d40d78a2c5597ba0
38f2be5f994d60c79e8a92121fb0488eef9690d550ef9fa40b1774221aac8c8c1dcf
97faa07c28e840feb9daf0bf3bed277a6e10a33490c0bee7e5fa318638f5b80a2272
700e591ffc14985d0ed19876725c2bec9356b45ca96d295e30bce86effc626a2bd78
39af05ae373801af510cfb378ce42088607909c91ceb4a90e4d7b2b6288b9cdfa262
570ffda8692b58f0b05a7c7899a717a3a97b6e64489f56323b000793f807ca75ca99
1");
	let ssref = h("1368d71518fadbe42fb75fbd356e016b0aaad6b4d3d91ce7f2070
73e4fb08c537217aba238aea92a7f855820518a8342b3a31f82ebbcdb479f33ad82b
dcdc953");
	let sk = unsafe{transmute(sk.as_ptr())};
	let pk = unsafe{transmute(pk.as_ptr())};
	let ek = unsafe{transmute(ek.as_ptr())};
	let ss = crate::x25519kyber768draft00::__decapsulate(sk, pk, ek).expect("Should decrypt");
	assert_eq!(ss, &ssref[..]);
}

#[test]
fn x25519_again()
{
	let sk = h("4012c550263fc8ad58375df3f557aac531d26850903e55a9f23f21d8534e8a48");
	let pk = h("3948cfe0ad1ddb695d780e59077195da6c56506b027329794ab02bca80815c4d");
	let ek = h("37fda3567bdbd628e88668c3c8d7e97d1d1253b6d4ea6d44c150f741f1bf4431");
	let ssref = h("fe0e18c9f024ce43799ae393c7e8fe8fce9d218875e8227b0187c04e7d2ea1fc");
	let sk = unsafe{transmute(sk.as_ptr())};
	let pk = unsafe{transmute(pk.as_ptr())};
	let ek = unsafe{transmute(ek.as_ptr())};
	let ss = crate::x25519::__decapsulate(sk, pk, ek).expect("Should decrypt");
	assert_eq!(ss, &ssref[..]);
}

fn kem_hlpr_auth<KDF:KeyDerivationFunction>(pkem: &[u8], skem: &[u8], pkrm: &[u8], skrm: &[u8], pksm: &[u8],
	sksm: &[u8], kem_id: KEM) -> impl ArrayOfBytes
{
	let mut skem = skem.to_vec();
	let mut skrm = skrm.to_vec();
	let mut sksm = sksm.to_vec();
	clamp32(&mut skem);
	clamp32(&mut skrm);
	clamp32(&mut sksm);
	//The pk/sk pairs should complete load.
	let _e = PrivateKey::load(kem_id, &skem, Some(&pkem)).expect("Key load e failed");
	let _r = PrivateKey::load(kem_id, &skrm, Some(&pkrm)).expect("Key load r failed");
	let _s = PrivateKey::load(kem_id, &sksm, Some(&pksm)).expect("Key load s failed");
	//Compute DH result, as it is not given.
	let mut dhe = [0;32];
	let mut dhr = [0;32];
	let mut dhs = [0;32];
	let mut dht = [0;32];
	btls_aux_xed25519::x25519_agree(&mut dhe, b32(&skem), b32(&pkrm));
	btls_aux_xed25519::x25519_agree(&mut dhr, b32(&skrm), b32(&pkem));
	assert_eq!(&dhe, &dhr);
	btls_aux_xed25519::x25519_agree(&mut dhs, b32(&sksm), b32(&pkrm));
	btls_aux_xed25519::x25519_agree(&mut dht, b32(&skrm), b32(&pksm));
	assert_eq!(&dhs, &dht);
	__dhkem_auth_compute_key::<KDF>(kem_id, &dhe, &dhs, &pkem, &pkrm, &pksm)
}

fn kem_hlpr_nsa<KDF:KeyDerivationFunction>(pkem: &[u8], skem: &[u8], pkrm: &[u8], skrm: &[u8], kem_id: KEM,
	crvid: u32, len: usize) -> impl ArrayOfBytes
{
	let crv = btls_aux_nettle::EccCurve::new(crvid).expect("Bad curve");
	//The pk/sk pairs should complete load.
	PrivateKey::load(kem_id, skem, Some(pkem)).expect("Key load e failed");
	PrivateKey::load(kem_id, skrm, Some(pkrm)).expect("Key load r failed");
	//Compute DH result, as it is not given.
	let mut dhe = [0;133];
	let mut dhr = [0;133];
	let skem = btls_aux_nettle::NsaEcdhKey::load(crv, &skem).expect("Bad key e");
	let skrm = btls_aux_nettle::NsaEcdhKey::load(crv, &skrm).expect("Bad key r");
	skem.agree(&mut dhe, pkrm).expect("Agree error 1");
	skrm.agree(&mut dhr, pkem).expect("Agree error 2");
	assert_eq!(&dhe, &dhr);
	__dhkem_compute_key::<KDF>(kem_id, &dhe[1..len+1], pkem, pkrm)
}

fn kem_hlpr<KDF:KeyDerivationFunction>(pkem: &[u8], skem: &[u8], pkrm: &[u8], skrm: &[u8], kem_id: KEM) ->
	impl ArrayOfBytes
{
	let mut skem = skem.to_vec();
	let mut skrm = skrm.to_vec();
	clamp32(&mut skem);
	clamp32(&mut skrm);
	//The pk/sk pairs should complete load.
	PrivateKey::load(kem_id, &skem, Some(&pkem)).expect("Key load e failed");
	PrivateKey::load(kem_id, &skrm, Some(&pkrm)).expect("Key load r failed");
	//Compute DH result, as it is not given.
	let mut dhe = [0;32];
	let mut dhr = [0;32];
	btls_aux_xed25519::x25519_agree(&mut dhe, b32(&skem), b32(&pkrm));
	btls_aux_xed25519::x25519_agree(&mut dhr, b32(&skrm), b32(&pkem));
	assert_eq!(&dhe, &dhr);
	__dhkem_compute_key::<KDF>(kem_id, &dhe, pkem, pkrm)
}

fn kdf_tail<KDF:KeyDerivationFunction,A:ArrayOfBytes>(ss2: A, mode: u8, ss: &[u8], info: &[u8], kem_id: KEM,
	aead_id: AEAD, nk: usize, nn: usize, psk: Option<(&[u8], &[u8])>, key: &[u8], bnce: &[u8], exps: &[u8])
{
	assert_eq!(ss2.as_bytes(), ss);
	let (_, exp) = __generic_kdf::<KDF,(),_>(ss, mode, info, kem_id,
		AEAD2::Normal(aead_id), nk, nn, psk, |key2, nonce2|{
		assert_eq!(key2, key);
		assert_eq!(nonce2, bnce);
		()
	}).expect("__generic_kdf failed");
	assert_eq!(&exp.1, exps);
}

#[test]
fn rfc9180_a_5_1_test_vector_kem()
{
	let skr = h("a4d1c55836aa30f9b3fbb6ac98d338c877c2867dd3a77396d13f68d3ab150d3b");
	let pke = b"\x04\xc0\x78\x36\xa0\x20\x6e\x04\xe3\x1d\x8a\xe9\x9b\xfd\x54\x93\x80\xb0\x72\xa1\xb1\xb8\x2e\x56\
		\x3c\x93\x5c\x09\x58\x27\x82\x4f\xc1\x55\x9e\xac\x6f\xb9\xe3\xc7\x0c\xd3\x19\x39\x68\x99\x4e\x7f\xe9\
		\x78\x1a\xa1\x03\xf5\xb5\x0e\x93\x4b\x5b\x2f\x38\x7e\x38\x12\x91";
	let pkr = b"\x04\xa6\x97\xbf\xfd\xe9\x40\x5c\x99\x28\x83\xc5\xc4\x39\xd6\xcc\x35\x81\x70\xb5\x1a\xf7\x28\x12\
		\x33\x3b\x01\x56\x21\xdc\x0f\x40\xba\xd9\xbb\x72\x6f\x68\xa5\xc0\x13\x80\x6a\x79\x0e\xc7\x16\xab\x86\
		\x69\xf8\x4f\x6b\x69\x45\x96\xc2\x98\x7c\xf3\x5b\xab\xa2\xa0\x06";
	let rss = b"\x80\x65\x20\xf8\x2e\xf0\xb0\x3c\x82\x3b\x7f\xc5\x24\xb6\xb5\x5a\x08\x8f\x56\x6b\x97\x51\xb8\x95\
		\x51\xc1\x70\xf4\x11\x3b\xd8\x50";
	//Bah, no DH result is given in test vectors.
	let mut dh = [0;65];
	let crv = EccCurve::new(0).expect("Bad curve");
	let sk = NsaEcdhKey::load(crv, &skr).expect("Key load failed");
	let sssize = sk.agree(&mut dh, pke).expect("kex failed");
	assert_eq!(sssize, 65);
	let dh = &dh[1..33];
	let ss = __dhkem_compute_key::<HKDF<btls_aux_hash::Sha256>>(KEM::P256, dh, pke, pkr);
	assert_eq!(ss.as_bytes(), rss);
}

#[test]
fn rfc9180_a_5_1_test_vector_kdf()
{
	let ss = b"\x80\x65\x20\xf8\x2e\xf0\xb0\x3c\x82\x3b\x7f\xc5\x24\xb6\xb5\x5a\x08\x8f\x56\x6b\x97\x51\xb8\x95\
		\x51\xc1\x70\xf4\x11\x3b\xd8\x50";
	let info = b"\x4f\x64\x65\x20\x6f\x6e\x20\x61\x20\x47\x72\x65\x63\x69\x61\x6e\x20\x55\x72\x6e";
	__generic_kdf::<HKDF<btls_aux_hash::Sha256>,_,_>(ss, 0, info, KEM::P256,
		AEAD2::Normal(AEAD::Chacha20Poly1305), 32, 12, None, |key, nonce|{
		let rkey = b"\xa8\xf4\x54\x90\xa9\x2a\x3b\x04\xd1\xdb\xf6\xcf\x2c\x39\x39\xad\x8b\xfc\x9b\xfc\xb9\
			\x7c\x04\xbf\xfe\x11\x67\x30\xc9\xdf\xe3\xfc";
		let rnonce = b"\x72\x6b\x43\x90\xed\x22\x09\x80\x9f\x58\xc6\x93";
		assert_eq!(key, rkey);
		assert_eq!(nonce, rnonce);
	}).expect("__generic_kdf failed");
}

fn do_basic_test(do_auth: bool, psk: Option<(&[u8], &[u8])>, info: &[u8], ad: &[u8], msg: &[u8])
{
	use btls_aux_random::TemporaryRandomLifetimeTag;
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let sk = PrivateKey::new(KEM::X25519, &mut rng).expect("Keygen failed");
	let ask = PrivateKey::new(KEM::X25519, &mut rng).expect("Keygen failed");
	let pk = sk.to_public();
	let apk = ask.to_public();
	let ask = if do_auth { Some(&ask) } else { None };
	let apk = if do_auth { Some(&apk) } else { None };
	let (mut ectx, ek) = pk.encapsulate(KDF::Sha256, AEAD::Chacha20Poly1305, info, ask, psk, &mut rng).
		expect("Encaps failed");
	let emsg = ectx.encrypt(ad, msg).expect("Encrypt failed");
	let mut dctx = sk.decapsulate(KDF::Sha256, AEAD::Chacha20Poly1305, &ek, info, apk, psk).
		expect("Decaps failed");
	let dmsg = dctx.decrypt(ad, &emsg).expect("Decrypt failed");
	assert_eq!(&dmsg, msg);
}

#[test]
fn singleshot()
{
	do_basic_test(false, None, b"bar", b"foo", b"Hello, World!\n");
	
}

#[test]
fn singleshot_psk()
{
	do_basic_test(false, Some((b"zot", b"quux")), b"bar", b"foo", b"Hello, World!\n");
}

#[test]
fn singleshot_auth()
{
	do_basic_test(true, None, b"bar", b"foo", b"Hello, World!\n");
}

#[test]
fn singleshot_auth_psk()
{
	do_basic_test(true, Some((b"zot", b"quux")), b"bar", b"foo", b"Hello, World!\n");
}

#[test]
fn kem_ek_size()
{
	assert_eq!(KEM::P256.get_ek_size(), 65);
	assert_eq!(KEM::P384.get_ek_size(), 97);
	assert_eq!(KEM::P521.get_ek_size(), 133);
	assert_eq!(KEM::X25519.get_ek_size(), 32);
	assert_eq!(KEM::X448.get_ek_size(), 56);
}

#[test]
fn test_recommmended_kdf()
{
	let map: &[(u16,u16)] = &[(16,1),(17,2),(18,3),(32,1),(33,3)];
	for &(kem, kdf) in map.iter() {
		let kem = KEM::by_id(kem).unwrap();
		let kdf = KDF::by_id(kdf).unwrap();
		assert!(kem.__recommended_kdf() == kdf);
	}
}

#[test]
fn test_recommmended_kdf2()
{
	if true { return; }	//Do not actually run this, only compile.
	use btls_aux_random::TemporaryRandomLifetimeTag;
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let sk = PrivateKey::new(KEM::X25519, &mut rng).expect("Keygen failed");
	let pk = sk.to_public();
	let _ = pk.recommended_kdf();
}

#[test]
fn kem_ids()
{
	assert_eq!(KEM::P256 as u16, 16);
	assert_eq!(KEM::P384 as u16, 17);
	assert_eq!(KEM::P521 as u16, 18);
	assert_eq!(KEM::X25519 as u16, 32);
	assert_eq!(KEM::X448 as u16, 33);
	assert_eq!(KEM::X25519Kyber768Draft00 as u16, 48);
	for i in 0..65536 { assert!(KEM::ok(i as u16) == matches!(i, 16|17|18|32|33|48)); }
}

#[test]
fn kdf_ids()
{
	assert_eq!(KDF::Sha256 as u16, 1);
	assert_eq!(KDF::Sha384 as u16, 2);
	assert_eq!(KDF::Sha512 as u16, 3);
	for i in 0..65536 { assert!(KDF::ok(i as u16) == matches!(i, 1|2|3)); }
}

#[test]
fn aead_ids()
{
	assert_eq!(AEAD::Aes128Gcm as u16, 1);
	assert_eq!(AEAD::Aes256Gcm as u16, 2);
	assert_eq!(AEAD::Chacha20Poly1305 as u16, 3);
	for i in 0..65536 { assert!(AEAD::ok(i as u16) == matches!(i, 1|2|3)); }
}
