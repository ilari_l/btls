use btls_aux_codegen::BareLiteral;
use btls_aux_codegen::CodeBlock;
use btls_aux_codegen::mkid;
use btls_aux_codegen::mkidf;
use btls_aux_codegen::quote;
use std::env;
use std::fs::File;
use std::path::PathBuf;
use std::io::Write as IoWrite;

#[derive(Copy,Clone)]
struct KemInfo<'a>
{
	sk_size: usize,
	pk_size: usize,
	ss_size: usize,
	ss2_size: usize,
	ek_size: usize,
	kdf: &'a str,
	kind: &'a str,
}

struct Keygen<'a,'b>
{
	kem: KemInfo<'b>,
	inner: &'a [&'a CodeBlock],
}

impl<'a,'b> Keygen<'a,'b>
{
	fn emit(&self) -> String
	{
		let inner = self.inner.to_vec();
		let pksize = self.kem.pk_size;
		let sksize = self.kem.sk_size;
		(quote!{
pub(super) fn __keygen(rng: &mut TemporaryRandomStream) -> Result<([u8;#sksize], [u8;#pksize]), HpkeError>
{
	let mut pk = [0;#pksize];
	let mut sk = [0;#sksize];
	#(#inner)*
	Ok((sk, pk))
}

		}).to_string()
	}
}

struct Encapsulate<'a,'b>
{
	kem: KemInfo<'b>,
	inner: &'a [&'a CodeBlock],
}

impl<'a,'b> Encapsulate<'a,'b>
{
	fn emit(&self) -> String
	{
		let inner = self.inner.to_vec();
		let pksize = self.kem.pk_size;
		let eksize = self.kem.ek_size;
		let ss_size = self.kem.ss_size;
		let ss2_size = self.kem.ss2_size;
		let kdf = BareLiteral(self.kem.kdf.to_string());
		let kind = mkid(self.kem.kind);
		quote!(
pub(super) fn __encapsulate(pkr: &[u8;#pksize], rng: &mut TemporaryRandomStream) ->
	Result<([u8;#ss2_size], [u8;#eksize]), HpkeError>
{
	let mut pk = [0;#eksize];
	let mut ss = [0;#ss_size];
	#(#inner)*
	let ss2 = __dhkem_compute_key::<#kdf>(KEM::#kind, ssx, &pk, pkr);
	Ok((ss2.0, pk))
}

		).as_ref().to_string()
	}
}

struct AuthEncapsulate<'a,'b>
{
	kem: KemInfo<'b>,
	inner: &'a [&'a CodeBlock],
}

impl<'a,'b> AuthEncapsulate<'a,'b>
{
	fn emit(&self) -> String
	{
		let inner = self.inner.to_vec();
		let sksize = self.kem.sk_size;
		let pksize = self.kem.pk_size;
		let eksize = self.kem.ek_size;
		let ss2_size = self.kem.ss2_size;
		let ss_size = self.kem.ss_size;
		let kdf = BareLiteral(self.kem.kdf.to_string());
		let kind = mkid(self.kem.kind);
		(quote!{
pub(super) fn __auth_encapsulate(pkr: &[u8;#pksize], auth_sk: &[u8;#sksize], auth_pk: &[u8;#pksize],
	rng: &mut TemporaryRandomStream) -> Result<([u8;#ss2_size], [u8;#eksize]), HpkeError>
{
	let mut pk = [0;#eksize];
	let mut ss1 = [0;#ss_size];
	let mut ss2 = [0;#ss_size];
	#(#inner)*
	let ss3 = __dhkem_auth_compute_key::<#kdf>(KEM::#kind, ss1x, ss2x, &pk, pkr, auth_pk);
	Ok((ss3.0, pk))
}

		}).to_string()
	}
}

struct Decapsulate<'a,'b>
{
	kem: KemInfo<'b>,
	inner: &'a [&'a CodeBlock],
}

impl<'a,'b> Decapsulate<'a,'b>
{
	fn emit(&self) -> String
	{
		let inner = self.inner.to_vec();
		let sksize = self.kem.sk_size;
		let pksize = self.kem.pk_size;
		let eksize = self.kem.ek_size;
		let ss2_size = self.kem.ss2_size;
		let ss_size = self.kem.ss_size;
		let kdf = BareLiteral(self.kem.kdf.to_string());
		let kind = mkid(self.kem.kind);
		(quote!{
pub(super) fn __decapsulate(sk: &[u8;#sksize], pk: &[u8;#pksize], ek: &[u8;#eksize]) ->
	Result<[u8;#ss2_size], HpkeError>
{
	let mut ss = [0;#ss_size];
	#(#inner)*
	let ss2 = __dhkem_compute_key::<#kdf>(KEM::#kind, ssx, ek, pk);
	Ok(ss2.0)
}

		}).to_string()
	}
}

struct AuthDecapsulate<'a,'b>
{
	kem: KemInfo<'b>,
	inner: &'a [&'a CodeBlock],
}

impl<'a,'b> AuthDecapsulate<'a,'b>
{
	fn emit(&self) -> String
	{
		let inner = self.inner.to_vec();
		let sksize = self.kem.sk_size;
		let pksize = self.kem.pk_size;
		let eksize = self.kem.ek_size;
		let ss2_size = self.kem.ss2_size;
		let ss_size = self.kem.ss_size;
		let kdf = BareLiteral(self.kem.kdf.to_string());
		let kind = mkid(self.kem.kind);
		(quote!{
pub(super) fn __auth_decapsulate(sk: &[u8;#sksize], pk: &[u8;#pksize], ek: &[u8;#eksize],
	auth_pk: &[u8;#pksize]) -> Result<[u8;#ss2_size], HpkeError>
{
	let mut ss1 = [0;#ss_size];
	let mut ss2 = [0;#ss_size];
	#(#inner)*
	let ss3 = __dhkem_auth_compute_key::<#kdf>(KEM::#kind, ss1x, ss2x, ek, pk, auth_pk);
	Ok(ss3.0)
}

		}).to_string()
	}
}

struct Check<'a,'b>
{
	kem: KemInfo<'b>,
	inner: &'a [&'a CodeBlock],
}

impl<'a,'b> Check<'a,'b>
{
	fn emit(&self) -> String
	{
		let inner = self.inner.to_vec();
		let sksize = self.kem.sk_size;
		let pksize = self.kem.pk_size;
		(quote!{
pub(super) fn __keycheck(sk: &[u8;#sksize]) -> Result<[u8;#pksize], HpkeError>
{
	let mut pk2 = [0;#pksize];
	#(#inner)*
	Ok(pk2)
}

		}).to_string()
	}
}

fn get_hsize(hash: &str) -> usize
{
	match hash {
		"Sha256" => 32,
		"Sha384" => 48,
		"Sha512" => 64,
		hash => panic!("Unrecognized hash '{hash}'")
	}
}

fn write_x(size: usize, hash: &str, kind: &str) -> String
{
	let hsize = get_hsize(hash);
	let mkkp = quote!{
		rng.fill_bytes(&mut sk);
		clampkey(&mut sk);
		generate_key(&mut pk, &sk);
	};

	let ssx = quote!(let ssx = &ss;);
	let ssxx = quote!{
		let ss1x = &ss1;
		let ss2x = &ss2;
	};

	let kdf = format!("HKDF<btls_aux_hash::{hash}>");
	let kem = KemInfo {
		sk_size: size,
		pk_size: size,
		ss_size: size,
		ss2_size: hsize,
		ek_size: size,
		kdf: &kdf,
		kind: kind,
	};

	let mut s = String::new();
	s.push_str(quote!{
		use super::__dhkem_auth_compute_key;
		use super::__dhkem_compute_key;
		use super::HKDF;
		use super::HpkeError;
		use super::KEM;
		use btls_aux_random::TemporaryRandomStream;
	}.as_ref());
	s.push_str(&Keygen { kem, inner: &[
		&mkkp,
	]}.emit());

	s.push_str(&Encapsulate { kem, inner: &[
		&quote!(let mut sk = [0;#size];),
		&mkkp,
		&quote!(agree_key(&mut ss, &sk, pkr);),
		&ssx,
	]}.emit());

	s.push_str(&AuthEncapsulate { kem, inner: &[
		&quote!(let mut sk = [0;#size];),
		&mkkp,
		&quote!{
			agree_key(&mut ss1, &sk, pkr);
			agree_key(&mut ss2, auth_sk, pkr);
		},
		&ssxx,
	]}.emit());

	s.push_str(&Decapsulate { kem, inner: &[
		&quote!{
			agree_key(&mut ss, sk, ek);
			__reject_x_exchange(&ss)?;
		},
		&ssx,
	]}.emit());

	s.push_str(&AuthDecapsulate { kem, inner: &[
		&quote!{
			agree_key(&mut ss1, sk, ek);
			agree_key(&mut ss2, sk, auth_pk);
			__reject_x_exchange(&ss1)?;
			__reject_x_exchange(&ss2)?;
		},
		&ssxx,
	]}.emit());

	s.push_str(&Check { kem, inner: &[
		&quote!(generate_key(&mut pk2, sk);),
	]}.emit());

	s
}

fn write_nsa(size: usize, crvid: u32, hash: &str, kind: &str) -> String
{
	let hsize = get_hsize(hash);
	let assert_pk2 = quote!(assert_size(&pk2, pk2size, "PK2")?;);
	let assert_ss = quote!(assert_size(&ss, ss_size, "SS")?;);
	let assert_ss1 = quote!(assert_size(&ss1, ss1size, "SS1")?;);
	let assert_ss2 = quote!(assert_size(&ss2, ss2_size, "SS2")?;);
	let assert_sk = quote!(assert_size(&sk, sksize, "SK")?;);

	let mkcrv = quote!{
		let crv = btls_aux_nettle::EccCurve::new(#crvid).set_err(HpkeError::Impossible(803))?;
	};

	let mkkp = quote!{
		let (sk2, pksize) = btls_aux_nettle::NsaEcdhKey::new(crv, &mut pk, rng).
			set_err(HpkeError::Impossible(804))?;
		assert_size(&pk, pksize, "PK")?;
	};
	let load = quote!{
		let sk = btls_aux_nettle::NsaEcdhKey::load(crv, sk).set_err(HpkeError::Impossible(801))?;
	};

	let limit = size + 1;
	let ssx = quote!(let ssx = &ss[1..#limit];);
	let ssxx = quote!{
		let ss1x = &ss1[1..#limit];
		let ss2x = &ss2[1..#limit];
	};

	let kdf = format!("HKDF<btls_aux_hash::{hash}>");
	let kem = KemInfo {
		sk_size: size,
		pk_size: 2*size+1,
		ss_size: 2*size+1,	//Need space to store y, cut off in ssx.
		ss2_size: hsize,
		ek_size: 2*size+1,
		kdf: &kdf,
		kind: kind,
	};

	let mut s = String::new();

	s.push_str(quote!{
		use super::__dhkem_auth_compute_key;
		use super::__dhkem_compute_key;
		use super::assert_size;
		use super::HKDF;
		use super::HpkeError;
		use super::KEM;
		use btls_aux_random::TemporaryRandomStream;
		use core::convert::TryInto;
	}.as_ref());

	s.push_str(&Keygen { kem, inner: &[
		&mkcrv,
		&mkkp,
		&quote!(let sksize = sk2.save(&mut sk).set_err(HpkeError::Impossible(802))?;),
		&assert_sk,
	]}.emit());

	s.push_str(&Encapsulate { kem, inner: &[
		&mkcrv,
		&mkkp,
		&quote!(let ss_size = sk2.agree(&mut ss, pkr).set_err(HpkeError::Impossible(805))?;),
		&assert_ss,
		&ssx,
	]}.emit());

	s.push_str(&AuthEncapsulate { kem, inner: &[
		&mkcrv,
		&quote!{
			let auth_sk2 = btls_aux_nettle::NsaEcdhKey::load(crv, auth_sk).
				set_err(HpkeError::Impossible(801))?;
		},
		&mkkp,
		&quote!(let ss1size = sk2.agree(&mut ss1, pkr).set_err(HpkeError::Impossible(805))?;),
		&assert_ss1,
		&quote!(let ss2_size = auth_sk2.agree(&mut ss2, pkr).set_err(HpkeError::Impossible(805))?;),
		&assert_ss2,
		&ssxx,
	]}.emit());

	s.push_str(&Decapsulate { kem, inner: &[
		&mkcrv,
		&load,
		&quote!(let ss_size = sk.agree(&mut ss, ek).set_err(HpkeError::ExplicitReject)?;),
		&assert_ss,
		&ssx,
	]}.emit());

	s.push_str(&AuthDecapsulate { kem, inner: &[
		&mkcrv,
		&load,
		&quote!(let ss1size = sk.agree(&mut ss1, ek).set_err(HpkeError::ExplicitReject)?;),
		&assert_ss1,
		&quote!(let ss2_size = sk.agree(&mut ss2, auth_pk).set_err(HpkeError::ExplicitReject)?;),
		&assert_ss2,
		&ssxx,
	]}.emit());

	s.push_str(&Check { kem, inner: &[
		&mkcrv,
		&quote!{
			let (_,pk2size) = btls_aux_nettle::NsaEcdhKey::load_with_public_key(crv, sk, &mut pk2).
				set_err(HpkeError::Impossible(801))?;
		},
		&assert_pk2,
	]}.emit());

	s
}

#[derive(Copy,Clone)]
struct AeadInfo2
{
	symbol: &'static str,
	doc: &'static str,
	ptsym: &'static str,
	id: u16,
}

static AEAD_INFO: &'static [AeadInfo2] = &[
	AeadInfo2 {
		symbol: "Aes128Gcm",
		doc: "AES (Advanced Encryption Standard) with 128 bit key in GCM (Galois Counter Mode) mode",
		ptsym: "ProtectorType::Aes128Gcm",
		id: 1,
	},
	AeadInfo2 {
		symbol: "Aes256Gcm",
		doc: "AES (Advanced Encryption Standard) with 256 bit key in GCM (Galois Counter Mode) mode",
		ptsym: "ProtectorType::Aes256Gcm",
		id: 2,
	},
	AeadInfo2 {
		symbol: "Chacha20Poly1305",
		doc: "Chacha with 20 rounds with Poly1305 MAC",
		ptsym: "ProtectorType::Chacha20Poly1305",
		id: 3,
	},
];

#[derive(Copy,Clone)]
struct KdfInfo2
{
	symbol: &'static str,
	doc: &'static str,
	ty: &'static str,
	id: u16,
}

static KDF_INFO: &'static [KdfInfo2] = &[
	KdfInfo2 {
		symbol: "Sha256",
		doc: "HKDF-SHA256",
		ty: "HKDF<btls_aux_hash::Sha256>",
		id: 1,
	},
	KdfInfo2 {
		symbol: "Sha384",
		doc: "HKDF-SHA384",
		ty: "HKDF<btls_aux_hash::Sha384>",
		id: 2,
	},
	KdfInfo2 {
		symbol: "Sha512",
		doc: "HKDF-SHA512",
		ty: "HKDF<btls_aux_hash::Sha512>",
		id: 3,
	},
];


#[derive(Copy,Clone)]
struct KemInfo2
{
	symbol: &'static str,
	sfx: &'static str,
	doc: &'static str,
	id: u16,
	pksize: usize,
	sksize: usize,
	eksize: usize,
	ss_size: usize,
	auth: bool,
}

static KEM_INFO: &'static [KemInfo2] = &[
	KemInfo2 {
		symbol: "P256",
		sfx: "p256",
		doc: "NIST P-256 curve (a.k.a. secp256r1) with HKDF-SHA256",
		id: 0x0010,
		pksize: 65, sksize: 32, eksize: 65, ss_size: 32,
		auth: true,
	},
	KemInfo2 {
		symbol: "P384",
		sfx: "p384",
		doc: "NIST P-384 curve (a.k.a. secp384r1) with HKDF-SHA384",
		id: 0x0011,
		pksize: 97, sksize: 48,  eksize: 97, ss_size: 48,
		auth: true,
	},
	KemInfo2 {
		symbol: "P521",
		sfx: "p521",
		doc: "NIST P-521 curve (a.k.a. secp521r1) with HKDF-SHA512",
		id: 0x0012,
		pksize: 133, sksize: 66, eksize: 133, ss_size: 64,
		auth: true,
	},
	KemInfo2 {
		symbol: "X25519",
		sfx: "x25519",
		doc: "X25519 (a.k.a. curve25519) with HKDF-SHA256",
		id: 0x0020,
		pksize: 32, sksize: 32, eksize: 32, ss_size: 32,
		auth: true,
	},
	KemInfo2 {
		symbol: "X448",
		sfx: "x448",
		doc: "X448 (a.k.a. curve448) with HKDF-SHA512",
		id: 0x0021,
		pksize: 56, sksize: 56, eksize: 56, ss_size: 64,
		auth: true,
	},
	KemInfo2 {
		symbol: "X25519Kyber768Draft00",
		sfx: "x25519kyber768draft00",
		doc: "X25519 Kyber768-draft00 hybrid",
		id: 0x0030,
		pksize: 1216, sksize: 2432, eksize: 1120, ss_size: 64,
		auth: false,
	},
];

fn dump_kem_info() -> String
{
	let mut function_stubs = String::new();
	let mut kems = Vec::new();
	let mut kem_symbol = Vec::new();
	let mut kem_eksize = Vec::new();
	let mut kem_ss_size = Vec::new();
	let mut kem_id = Vec::new();
	let mut kem_auth_decaps_fn = Vec::new();
	let mut kem_auth_encaps_fn = Vec::new();
	let mut kem_checkpk_fn = Vec::new();
	let mut kem_decaps_fn = Vec::new();
	let mut kem_encaps_fn = Vec::new();
	let mut kem_keycheck_fn = Vec::new();
	let mut kem_keygen_fn = Vec::new();
	for kem in KEM_INFO.iter() {
		let doc = kem.doc;
		let symbol = mkid(kem.symbol);
		let sfx = mkid(kem.sfx);
		let pksize = kem.pksize;
		let sksize = kem.sksize;
		let eksize = kem.eksize;
		let rsfx = kem.sfx;
		let checkpk_fn = mkidf!("checkpk_{rsfx}");
		let decaps_fn = mkidf!("decapsulate_{rsfx}");
		let encaps_fn = mkidf!("encapsulate_{rsfx}");
		let keycheck_fn = mkidf!("keycheck_{rsfx}");
		let keygen_fn = mkidf!("keygen_{rsfx}");
		let id = kem.id;

		kems.push(quote!(#[doc=#doc] #symbol = #id));
		kem_symbol.push(mkid(kem.symbol));
		kem_eksize.push(kem.eksize);
		kem_ss_size.push(kem.ss_size);
		kem_id.push(kem.id);
		if kem.auth {
			let auth_decaps_fn = mkidf!("_auth_decapsulate_{rsfx}");
			let auth_encaps_fn = mkidf!("_auth_encapsulate_{rsfx}");
			kem_auth_decaps_fn.push(auth_decaps_fn.clone());
			kem_auth_encaps_fn.push(auth_encaps_fn.clone());
			function_stubs.push_str(quote!{
fn #auth_encaps_fn(pkr: &[u8], auth_sk: &[u8], auth_pk: &[u8], rng: &mut TemporaryRandomStream) ->
	Result<(SharedSecret, _EncapsulatedKey), HpkeError>
{
	let pkr = array_cast::<#pksize>(pkr, KIND_RPK)?;
	let auth_sk = array_cast::<#sksize>(auth_sk, KIND_ASK)?;
	let auth_pk = array_cast::<#pksize>(auth_pk, KIND_APK)?;
	let (ss, ek) = #sfx::__auth_encapsulate(pkr, auth_sk, auth_pk, rng)?;
	Ok((SharedSecret::#symbol(ss), _EncapsulatedKey::#symbol(ek)))
}

fn #auth_decaps_fn(sk: &[u8], pk: &[u8], ek: &[u8], auth_pk: &[u8]) -> Result<SharedSecret, HpkeError>
{
	let sk = array_cast::<#sksize>(sk, KIND_SK)?;
	let pk = array_cast::<#pksize>(pk, KIND_PK)?;
	let ek = array_cast::<#eksize>(ek, KIND_EK)?;
	let auth_pk = array_cast::<#pksize>(auth_pk, KIND_APK)?;
	let ss = #sfx::__auth_decapsulate(sk, pk, ek, auth_pk)?;
	Ok(SharedSecret::#symbol(ss))
}

			}.as_ref());
		} else {
			kem_auth_decaps_fn.push(mkid("__auth_decapsulate_unsupported"));
			kem_auth_encaps_fn.push(mkid("__auth_encapsulate_unsupported"));
		}

		kem_checkpk_fn.push(checkpk_fn.clone());
		kem_decaps_fn.push(decaps_fn.clone());
		kem_encaps_fn.push(encaps_fn.clone());
		kem_keycheck_fn.push(keycheck_fn.clone());
		kem_keygen_fn.push(keygen_fn.clone());

		function_stubs.push_str(quote!{
fn #encaps_fn(pkr: &[u8], rng: &mut TemporaryRandomStream) -> Result<(SharedSecret, _EncapsulatedKey), HpkeError>
{
	let pkr = array_cast::<#pksize>(pkr, KIND_RPK)?;
	let (ss, ek) = #sfx::__encapsulate(pkr, rng)?;
	Ok((SharedSecret::#symbol(ss), _EncapsulatedKey::#symbol(ek)))
}


fn #decaps_fn(sk: &[u8], pk: &[u8], ek: &[u8]) -> Result<SharedSecret, HpkeError>
{
	let sk = array_cast::<#sksize>(sk, KIND_SK)?;
	let pk = array_cast::<#pksize>(pk, KIND_PK)?;
	let ek = array_cast::<#eksize>(ek, KIND_EK)?;
	let ss = #sfx::__decapsulate(sk, pk, ek)?;
	Ok(SharedSecret::#symbol(ss))
}

fn #keygen_fn(rng: &mut TemporaryRandomStream) -> Result<(Vec<u8>, Vec<u8>), HpkeError>
{
	let (sk, pk) = #sfx::__keygen(rng)?;
	Ok((sk.to_vec(), pk.to_vec()))
}

fn #keycheck_fn(sk: &[u8], pk: Option<&[u8]>) -> Result<Vec<u8>, HpkeError>
{
	let sk = array_cast::<#sksize>(sk, KIND_SK)?;
	let pk = pk.map(|pk|array_cast::<#pksize>(pk, KIND_PK)).transpose()?;
	let pk2 = #sfx::__keycheck(sk)?;
	if let Some(pk) = pk {
		fail_if!(&pk2 != pk, HpkeError::MismatchedPkSk);
		Ok(pk.to_vec())
	} else {
		Ok(pk2.to_vec())
	}
}

fn #checkpk_fn(pk: &[u8]) -> Result<(), HpkeError>
{
	let pk = array_cast::<#pksize>(pk, KIND_PK)?;
	#sfx::__checkpk(pk)
}

		}.as_ref());
	}

	function_stubs.push_str(quote!{
fn __auth_encapsulate_unsupported(_: &[u8], _: &[u8], _: &[u8], _: &mut TemporaryRandomStream) ->
	Result<(SharedSecret, _EncapsulatedKey), HpkeError>
{
	fail!(HpkeError::AuthNotSupported)
}

fn __auth_decapsulate_unsupported(_: &[u8], _: &[u8], _: &[u8], _pk: &[u8]) -> Result<SharedSecret, HpkeError>
{
	fail!(HpkeError::AuthNotSupported)
}

#[derive(Copy,Clone)]
enum _EncapsulatedKey
{
	#(#kem_symbol([u8;#kem_eksize])),*
}

impl Deref for EncapsulatedKey
{
	type Target = [u8];
	fn deref(&self) -> &[u8]
	{
		match &self.0 {
			#(&_EncapsulatedKey::#kem_symbol(ref x) => x),*
		}
	}
}

enum SharedSecret
{
	#(#kem_symbol([u8;#kem_ss_size])),*
}

///HPKE KEM (Key Encapsulation Mechanism).
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
#[repr(u16)]
#[non_exhaustive]
pub enum KEM
{
	#(#kems),*
}

impl KEM
{
	fn __by_id(id: u16) -> Option<KEM>
	{
		match id {
			#(#kem_id => Some(KEM::#kem_symbol),)*
			_ => None
		}
	}
	fn __ok(id: u16) -> bool { matches!(id, #(#kem_id)|*) }
	fn __eklen(self) -> usize
	{
		match self {
			#(KEM::#kem_symbol => #kem_eksize),*
		}
	}
}

static __KEM_LIST: &'static [KEM] = &[#(KEM::#kem_symbol),*];

fn kem_encapsulate(kemid: KEM, pkr: &[u8], rng: &mut TemporaryRandomStream, auth: Option<(&[u8], &[u8])>) ->
	Result<(SharedSecret, _EncapsulatedKey), HpkeError>
{
	if let Some((auth_sk, auth_pk)) = auth {
		let func: AuthEncapsFn = match kemid {
			#(KEM::#kem_symbol => #kem_auth_encaps_fn),*
		};
		func(pkr, auth_sk, auth_pk, rng)
	} else {
		let func: EncapsFn = match kemid {
			#(KEM::#kem_symbol => #kem_encaps_fn),*
		};
		func(pkr, rng)
	}
}

fn kem_decapsulate(kemid: KEM, sk: &[u8], pk: &[u8],  ek: &[u8], auth: Option<&[u8]>) ->
	Result<SharedSecret,HpkeError>
{
	if let Some(auth_pk) = auth {
		let func: AuthDecapsFn = match kemid {
			#(KEM::#kem_symbol => #kem_auth_decaps_fn),*
		};
		func(sk, pk, ek, auth_pk)
	} else {
		let func: DecapsFn = match kemid {
			#(KEM::#kem_symbol => #kem_decaps_fn),*
		};
		func(sk, pk, ek)
	}
}

fn __split_kem_ss<'a>(ss: &'a SharedSecret) -> (KEM, &'a [u8])
{
	match ss {
		#(&SharedSecret::#kem_symbol(ref x) => (KEM::#kem_symbol, x)),*
	}
}

fn __keygen(kem: KEM, rng: &mut TemporaryRandomStream) -> Result<(Vec<u8>, Vec<u8>), HpkeError>
{
	let func: KeygenFn = match kem {
		#(KEM::#kem_symbol => #kem_keygen_fn),*
	};
	func(rng)
}

fn __keycheck(kem: KEM, sk: &[u8], pk: Option<&[u8]>) -> Result<Vec<u8>, HpkeError>
{
	let func: KeycheckFn = match kem {
		#(KEM::#kem_symbol => #kem_keycheck_fn),*
	};
	func(sk, pk)
}

fn __check_pk_ok(kem: KEM, pk: &[u8]) -> Result<(), HpkeError>
{
	let func: CheckpkFn = match kem {
		#(KEM::#kem_symbol => #kem_checkpk_fn),*
	};
	func(pk)
}

	}.as_ref());
	function_stubs
}

fn dump_kdf_info() -> String
{
	let mut kdfs = Vec::new();
	let mut kdf_symbol = Vec::new();
	let mut kdf_ty = Vec::new();
	let mut kdf_id = Vec::new();
	for kdf in KDF_INFO.iter() {
		let doc = kdf.doc;
		let symbol = mkid(kdf.symbol);
		let id = kdf.id;
		kdfs.push(quote!(#[doc=#doc] #symbol = #id));
		kdf_symbol.push(mkid(kdf.symbol));
		kdf_id.push(kdf.id);
		//This can be composite, so abuse BareLiteral.
		kdf_ty.push(BareLiteral(kdf.ty.to_string()));
	}
	(quote!{
///HPKE KDF (Key Deriviation Function)
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
#[repr(u16)]
#[non_exhaustive]
pub enum KDF
{
	#(#kdfs),*
}

impl KDF
{
	fn __by_id(id: u16) -> Option<KDF>
	{
		match id {
			#(#kdf_id => Some(KDF::#kdf_symbol),)*
			_ => None
		}
	}
	fn __ok(id: u16) -> bool { matches!(id, #(#kdf_id)|*) }
}

static __KDF_LIST: &'static [KDF] = &[#(KDF::#kdf_symbol),*];

fn __kdf_encapsulate_fn(kdf: KDF) -> fn(&SharedSecret, u8, &[u8], AEAD, Option<(&[u8], &[u8])>,
	fn(ProtectorType, &[u8]) -> Option<EncryptionKey>) ->
	Result<(EncryptionKey, Nonce, ExportContext), HpkeError>
{
	match kdf {
		#(KDF::#kdf_symbol => __kdf_xcrypt::<#kdf_ty,EncryptionKey>),*
	}
}

fn __kdf_decapsulate_fn(kdf: KDF) -> fn(&SharedSecret, u8, &[u8], AEAD, Option<(&[u8], &[u8])>,
	fn(ProtectorType, &[u8]) -> Option<DecryptionKey>) ->
	Result<(DecryptionKey, Nonce, ExportContext), HpkeError>
{
	match kdf {
		#(KDF::#kdf_symbol => __kdf_xcrypt::<#kdf_ty,DecryptionKey>),*
	}
}

fn __kdf_exporter_fn(kdf: KDF) -> fn(&SharedSecret, u8, &[u8], Option<(&[u8], &[u8])>) ->
	Result<ExportContext, HpkeError>
{
	match kdf {
		#(KDF::#kdf_symbol => __kdf_xcrypt_export_only::<#kdf_ty>),*
	}
}

fn __kdf_exporter2_fn(kdf: KDF) -> fn(SuiteFull, &[u8], &[u8], &mut [u8])
{
	match kdf {
		#(KDF::#kdf_symbol => __generic_exporter::<#kdf_ty>),*
	}
}

	}).to_string()
}

fn dump_aead_info() -> String
{
	let mut aeads = Vec::new();
	let mut aead_symbol = Vec::new();
	let mut aead_ptsym = Vec::new();
	let mut aead_id = Vec::new();
	for aead in AEAD_INFO.iter() {
		let doc = aead.doc;
		let symbol = mkid(aead.symbol);
		let id = aead.id;
		aeads.push(quote!(#[doc=#doc] #symbol = #id));
		aead_symbol.push(mkid(aead.symbol));
		aead_id.push(aead.id);
		//This can be composite, so abuse BareLiteral.
		aead_ptsym.push(BareLiteral(aead.ptsym.to_string()));
	}
	(quote!{
///HPKE AEAD (Authenticated Encryption with Associated Data).
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
#[repr(u16)]
#[non_exhaustive]
pub enum AEAD
{
	#(#aeads),*
}

impl AEAD
{
	fn __by_id(id: u16) -> Option<AEAD>
	{
		match id {
			#(#aead_id => Some(AEAD::#aead_symbol),)*
			_ => None
		}
	}
	fn __ok(id: u16) -> bool { matches!(id, #(#aead_id)|*) }
}

static __AEAD_LIST: &'static [AEAD] = &[#(AEAD::#aead_symbol),*];

fn __pt_for_aead(aeadid: AEAD) -> ProtectorType
{
	match aeadid {
		#(AEAD::#aead_symbol => #aead_ptsym),*
	}
}

	}).to_string()
}

fn hpke_funcs() -> String
{
	let mut s = String::new();
	s.push_str(&dump_kem_info());
	s.push_str(&dump_kdf_info());
	s.push_str(&dump_aead_info());
	s
}


fn write(f: &mut File, func: impl FnOnce() -> String)
{
	let s = func();
	f.write_all(s.as_bytes()).unwrap();
}

fn file(p: &str) -> File
{
	let out_dir = PathBuf::from(env::var_os("OUT_DIR").unwrap());
	File::create(&out_dir.join(p)).unwrap()
}

fn main()
{
	write(&mut file("autogenerated-hpke-int.inc.rs"), hpke_funcs);
	write(&mut file("autogenerated-hpke-x25519.inc.rs"), ||write_x(32, "Sha256", "X25519"));
	write(&mut file("autogenerated-hpke-x448.inc.rs"), ||write_x(56, "Sha512", "X448"));
	write(&mut file("autogenerated-hpke-p256.inc.rs"), ||write_nsa(32, 0, "Sha256", "P256"));
	write(&mut file("autogenerated-hpke-p384.inc.rs"), ||write_nsa(48, 1, "Sha384", "P384"));
	write(&mut file("autogenerated-hpke-p521.inc.rs"), ||write_nsa(66, 2, "Sha512", "P521"));

	println!("cargo:rerun-if-changed=build.rs");	//This has no external deps to rebuild for.
}
