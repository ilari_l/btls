#![allow(deprecated)]
use super::*;
use btls_aux_collections::Box;
use btls_aux_collections::BTreeMap;
use btls_aux_collections::String;
use btls_aux_collections::ToOwned;
use btls_aux_collections::ToString;
use btls_aux_collections::Vec;
use btls_aux_json2::Number as N;
use core::cmp::Ordering;
use core::iter::FromIterator;
use core::str::from_utf8;

fn assert_cbor_rel(x: &CborNode, y: &CborNode, ordering: Ordering)
{
	assert_eq!(x.partial_cmp(y), Some(ordering));
	assert_eq!(y.partial_cmp(x), Some(ordering.reverse()));
	assert_eq!(x.cmp(y), ordering);
	assert_eq!(y.cmp(x), ordering.reverse());
	assert_eq!(x.eq(y), ordering.is_eq());
	assert_eq!(x.ne(y), ordering.is_ne());
}

#[test]
fn cbor_compares()
{
	//Various CBOR values in order, for testing.
	let list = [
		CborNode::Integer(0), CborNode::Integer(1), CborNode::Integer(6), CborNode::Integer(22),
		CborNode::Integer(0xFFFFFFFFFFFFFFFF), CborNode::NegInteger(0), CborNode::NegInteger(1),
		CborNode::NegInteger(15), CborNode::NegInteger(0xFFFFFFFFFFFFFFFF), CborNode::Octets(Vec::new()),
		CborNode::Octets(vec![0]), CborNode::Octets(vec![1]), CborNode::Octets(vec![2]),
		CborNode::Octets(vec![255]), CborNode::Octets(vec![0,0]), CborNode::Octets(vec![0,1]),
		CborNode::Octets(vec![1,0]), CborNode::Octets(vec![255,0]), CborNode::Octets(vec![255,255]),
		CborNode::Octets(vec![255,255,0]), CborNode::Octets(vec![255,255,255]),
		CborNode::String(format!("")), CborNode::String(format!(" ")), CborNode::String(format!("!")),
		CborNode::String(format!("0")), CborNode::String(format!("1")), CborNode::String(format!("A")),
		CborNode::String(format!("Z")), CborNode::String(format!("0x")), CborNode::String(format!("Z ")),
		CborNode::String(format!("ZZ")), CborNode::String(format!("ZZZZZZZ")), CborNode::Array(Vec::new()),
		CborNode::Array(vec![CborNode::Integer(0)]),
		CborNode::Array(vec![CborNode::Integer(1)]),
		CborNode::Array(vec![CborNode::Integer(2)]),
		CborNode::Array(vec![CborNode::Double(0xFFFFFFFFFFFFFFFF)]),
		CborNode::Array(vec![CborNode::Integer(1), CborNode::Integer(0)]),
		CborNode::Array(vec![CborNode::Integer(1), CborNode::Integer(1)]),
		CborNode::Array(vec![CborNode::Integer(1), CborNode::Double(0xFFFFFFFFFFFFFFFF)]),
		CborNode::Array(vec![CborNode::Integer(2), CborNode::Integer(0)]),
		CborNode::Array(vec![CborNode::Integer(2), CborNode::Integer(1), CborNode::Integer(6)]),
		CborNode::Array(vec![CborNode::Integer(2), CborNode::Integer(1), CborNode::Integer(7)]),
		CborNode::Dictionary(BTreeMap::new()),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(0), CborNode::Integer(0))].iter().
			cloned())),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(0), CborNode::Integer(1))].iter().
			cloned())),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(1), CborNode::Integer(0))].iter().
			cloned())),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(2), CborNode::Integer(0))].iter().
			cloned())),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Double(0), CborNode::Integer(0))].iter().
			cloned())),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(2), CborNode::Integer(0)),
			(CborNode::Integer(0), CborNode::Integer(0))].iter().cloned())),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(2), CborNode::Integer(0)),
			(CborNode::Integer(0), CborNode::Integer(1))].iter().cloned())),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(2), CborNode::Integer(0)),
			(CborNode::Integer(0), CborNode::Integer(1)),
			(CborNode::Integer(7), CborNode::Integer(5))].iter().cloned())),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(2), CborNode::Integer(0)),
			(CborNode::Integer(1), CborNode::Integer(0)),
			(CborNode::Integer(6), CborNode::Integer(4))].iter().cloned())),
		CborNode::Tag(0, Box::new(CborNode::Integer(0))),
		CborNode::Tag(0, Box::new(CborNode::Double(0xFFFFFFFFFFFFFFFF))),
		CborNode::Tag(1, Box::new(CborNode::Integer(0))),
		CborNode::Tag(1, Box::new(CborNode::Double(0xFFFFFFFFFFFFFFFF))),
		CborNode::Tag(2, Box::new(CborNode::Integer(0))),
		CborNode::Simple(0), CborNode::Simple(1), CborNode::Simple(2), CborNode::Simple(20),
		CborNode::Simple(21), CborNode::Simple(22), CborNode::Simple(23), CborNode::Simple(32),
		CborNode::Simple(254), CborNode::Simple(255), CborNode::HalfFloat(0), CborNode::HalfFloat(1),
		CborNode::HalfFloat(2), CborNode::HalfFloat(0xFFFF), CborNode::Float(0), CborNode::Float(1),
		CborNode::Float(2), CborNode::Float(0xFFFFFFFF), CborNode::Double(0), CborNode::Double(1),
		CborNode::Double(5), CborNode::Double(0xFFFFFFFFFFFFFFFF),
	];
	for i in 0..list.len() {
		for j in 0..i { assert_cbor_rel(&list[i], &list[j], Ordering::Greater); }
		assert_cbor_rel(&list[i], &list[i], Ordering::Equal);
		for j in i+1..list.len() { assert_cbor_rel(&list[i], &list[j], Ordering::Less); }
	}
}

#[test]
fn parse_cbor_abc_ind()
{
	assert_eq!(CborNode::parse(&[0x7F, 0x63, 65, 66, 67, 0xFF][..]).unwrap(), CborNode::String(format!("ABC")));
}

#[test]
fn parse_cbor()
{
	//Empty.
	CborNode::parse(&[]).unwrap_err();
	CborNode::parse(&[0x00, 0x01][..]).unwrap_err();
	//Positive integers.
	assert_eq!(CborNode::parse(&[0x00][..]).unwrap(), CborNode::Integer(0));
	assert_eq!(CborNode::parse(&[0x01][..]).unwrap(), CborNode::Integer(1));
	assert_eq!(CborNode::parse(&[0x17][..]).unwrap(), CborNode::Integer(23));
	CborNode::parse(&[0x18][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x18, 0x00][..]).unwrap(), CborNode::Integer(0));
	assert_eq!(CborNode::parse(&[0x18, 0x17][..]).unwrap(), CborNode::Integer(23));
	assert_eq!(CborNode::parse(&[0x18, 0x18][..]).unwrap(), CborNode::Integer(24));
	assert_eq!(CborNode::parse(&[0x18, 0xFF][..]).unwrap(), CborNode::Integer(255));
	CborNode::parse(&[0x19, 0x01][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x19, 0x00, 0x00][..]).unwrap(), CborNode::Integer(0));
	assert_eq!(CborNode::parse(&[0x19, 0x00, 0xFF][..]).unwrap(), CborNode::Integer(255));
	assert_eq!(CborNode::parse(&[0x19, 0x01, 0x00][..]).unwrap(), CborNode::Integer(256));
	assert_eq!(CborNode::parse(&[0x19, 0x01, 0x03][..]).unwrap(), CborNode::Integer(259));
	assert_eq!(CborNode::parse(&[0x19, 0xFF, 0xFE][..]).unwrap(), CborNode::Integer(0xFFFE));
	assert_eq!(CborNode::parse(&[0x19, 0xFF, 0xFF][..]).unwrap(), CborNode::Integer(0xFFFF));
	CborNode::parse(&[0x1A, 0x01, 0x00, 0x00][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x1A, 0x00, 0x00, 0x00, 0x00][..]).unwrap(), CborNode::Integer(0));
	assert_eq!(CborNode::parse(&[0x1A, 0x00, 0x00, 0x00, 0x01][..]).unwrap(), CborNode::Integer(1));
	assert_eq!(CborNode::parse(&[0x1A, 0x00, 0x00, 0x00, 0xFF][..]).unwrap(), CborNode::Integer(255));
	assert_eq!(CborNode::parse(&[0x1A, 0x00, 0x00, 0x01, 0x00][..]).unwrap(), CborNode::Integer(256));
	assert_eq!(CborNode::parse(&[0x1A, 0x01, 0x05, 0x08, 0x14][..]).unwrap(), CborNode::Integer(0x01050814));
	assert_eq!(CborNode::parse(&[0x1A, 0xFF, 0xFE, 0xFD, 0xFC][..]).unwrap(), CborNode::Integer(0xFFFEFDFC));
	assert_eq!(CborNode::parse(&[0x1A, 0xFF, 0xFF, 0xFF, 0xFF][..]).unwrap(), CborNode::Integer(0xFFFFFFFF));
	CborNode::parse(&[0x1B, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x1B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00][..]).unwrap(),
		CborNode::Integer(0));
	assert_eq!(CborNode::parse(&[0x1B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01][..]).unwrap(),
		CborNode::Integer(1));
	assert_eq!(CborNode::parse(&[0x1B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF][..]).unwrap(),
		CborNode::Integer(255));
	assert_eq!(CborNode::parse(&[0x1B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00][..]).unwrap(),
		CborNode::Integer(256));
	assert_eq!(CborNode::parse(&[0x1B, 0x16, 0x22, 0x10, 0x02, 0x01, 0x05, 0x08, 0x14][..]).unwrap(),
		CborNode::Integer(0x1622100201050814));
	assert_eq!(CborNode::parse(&[0x1B, 0xFF, 0xFE, 0xFD, 0xFC, 0xFB, 0xFA, 0xF9, 0xF8][..]).unwrap(),
		CborNode::Integer(0xFFFEFDFCFBFAF9F8));
	assert_eq!(CborNode::parse(&[0x1B, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF][..]).unwrap(),
		CborNode::Integer(0xFFFFFFFFFFFFFFFF));
	CborNode::parse(&[0x1C][..]).unwrap_err();
	CborNode::parse(&[0x1D][..]).unwrap_err();
	CborNode::parse(&[0x1E][..]).unwrap_err();
	CborNode::parse(&[0x1F][..]).unwrap_err();
	CborNode::parse(&[0x1F, 0x00, 0xFF][..]).unwrap_err();
	//Negative integers.
	assert_eq!(CborNode::parse(&[0x20][..]).unwrap(), CborNode::NegInteger(0));
	assert_eq!(CborNode::parse(&[0x21][..]).unwrap(), CborNode::NegInteger(1));
	assert_eq!(CborNode::parse(&[0x37][..]).unwrap(), CborNode::NegInteger(23));
	assert_eq!(CborNode::parse(&[0x38, 0x01][..]).unwrap(), CborNode::NegInteger(0x01));
	assert_eq!(CborNode::parse(&[0x38, 0x18][..]).unwrap(), CborNode::NegInteger(0x18));
	assert_eq!(CborNode::parse(&[0x38, 0xFF][..]).unwrap(), CborNode::NegInteger(0xFF));
	assert_eq!(CborNode::parse(&[0x39, 0x01, 0x03][..]).unwrap(), CborNode::NegInteger(0x0103));
	assert_eq!(CborNode::parse(&[0x3A, 0x01, 0x03, 0x05, 0x07][..]).unwrap(), CborNode::NegInteger(0x01030507));
	assert_eq!(CborNode::parse(&[0x3B, 0x01, 0x03, 0x05, 0x07, 0x09, 0x0B, 0x0D, 0x0F][..]).unwrap(),
		CborNode::NegInteger(0x01030507090B0D0F));
	CborNode::parse(&[0x3C][..]).unwrap_err();
	CborNode::parse(&[0x3D][..]).unwrap_err();
	CborNode::parse(&[0x3E][..]).unwrap_err();
	CborNode::parse(&[0x3F][..]).unwrap_err();
	CborNode::parse(&[0x3F, 0x00, 0xFF][..]).unwrap_err();
	//Octet strings.
	assert_eq!(CborNode::parse(&[0x40][..]).unwrap(), CborNode::Octets(vec![]));
	assert_eq!(CborNode::parse(&[0x41, 0x05][..]).unwrap(), CborNode::Octets(vec![5]));
	CborNode::parse(&[0x42, 0x05][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x42, 0x05, 0x09][..]).unwrap(), CborNode::Octets(vec![5, 9]));
	assert_eq!(CborNode::parse(&[0x44, 0x05, 0x09, 6, 2][..]).unwrap(), CborNode::Octets(vec![5, 9, 6, 2]));
	assert_eq!(CborNode::parse(&[0x58, 3, 0x09, 6, 2][..]).unwrap(), CborNode::Octets(vec![9, 6, 2]));
	assert_eq!(CborNode::parse(&[0x59, 0, 3, 0x09, 6, 2][..]).unwrap(), CborNode::Octets(vec![9, 6, 2]));
	assert_eq!(CborNode::parse(&[0x5A, 0, 0, 0, 3, 0x09, 6, 2][..]).unwrap(), CborNode::Octets(vec![9, 6, 2]));
	assert_eq!(CborNode::parse(&[0x5B, 0, 0, 0, 0, 0, 0, 0, 3, 0x09, 6, 2][..]).unwrap(),
		CborNode::Octets(vec![9, 6, 2]));
	CborNode::parse(&[0x5C][..]).unwrap_err();
	CborNode::parse(&[0x5D][..]).unwrap_err();
	CborNode::parse(&[0x5E][..]).unwrap_err();
	CborNode::parse(&[0x5F][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x5F, 0xFF][..]).unwrap(), CborNode::Octets(vec![]));
	assert_eq!(CborNode::parse(&[0x5F, 0x40, 0xFF][..]).unwrap(), CborNode::Octets(vec![]));
	assert_eq!(CborNode::parse(&[0x5F, 0x41, 2, 0xFF][..]).unwrap(), CborNode::Octets(vec![2]));
	assert_eq!(CborNode::parse(&[0x5F, 0x42, 2, 6, 0xFF][..]).unwrap(), CborNode::Octets(vec![2, 6]));
	assert_eq!(CborNode::parse(&[0x5F, 0x42, 2, 6, 0x43, 1, 2, 3, 0xFF][..]).unwrap(),
		CborNode::Octets(vec![2, 6, 1, 2, 3]));
	assert_eq!(CborNode::parse(&[0x5F, 0x42, 2, 6, 0x40, 0x43, 1, 2, 3, 0xFF][..]).unwrap(),
		CborNode::Octets(vec![2, 6, 1, 2, 3]));
	CborNode::parse(&[0x5F, 0x42, 2, 6, 0x60, 0x43, 1, 2, 3, 0xFF][..]).unwrap_err();
	//Strings.
	assert_eq!(CborNode::parse(&[0x60][..]).unwrap(), CborNode::String(format!("")));
	assert_eq!(CborNode::parse(&[0x61, 0x41][..]).unwrap(), CborNode::String(format!("A")));
	CborNode::parse(&[0x62, 0x41][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x62, 0x41, 0x44][..]).unwrap(), CborNode::String(format!("AD")));
	assert_eq!(CborNode::parse(&[0x78, 3, 65, 66, 67][..]).unwrap(), CborNode::String(format!("ABC")));
	assert_eq!(CborNode::parse(&[0x79, 0, 3, 65, 66, 67][..]).unwrap(), CborNode::String(format!("ABC")));
	assert_eq!(CborNode::parse(&[0x7A, 0, 0, 0, 3, 65, 66, 67][..]).unwrap(), CborNode::String(format!("ABC")));
	assert_eq!(CborNode::parse(&[0x7B, 0, 0, 0, 0, 0, 0, 0, 3, 65, 66, 67][..]).unwrap(),
		CborNode::String(format!("ABC")));
	assert_eq!(CborNode::parse(&[0x78, 3, 0xE2, 0xA2, 0x83][..]).unwrap(),
		CborNode::String(char::from_u32(0x2883).unwrap().to_string()));
	assert_eq!(CborNode::parse(&[0x78, 5, 33, 0xE2, 0xA2, 0x83, 36][..]).unwrap(),
		CborNode::String(format!("!{c}$", c=char::from_u32(0x2883).unwrap())));
	CborNode::parse(&[0x7C][..]).unwrap_err();
	CborNode::parse(&[0x7D][..]).unwrap_err();
	CborNode::parse(&[0x7E][..]).unwrap_err();
	CborNode::parse(&[0x7F][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x7F, 0xFF][..]).unwrap(), CborNode::String(format!("")));
	CborNode::parse(&[0x7F, 0x63, 65, 66, 67][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x7F, 0x63, 65, 66, 67, 0xFF][..]).unwrap(), CborNode::String(format!("ABC")));
	assert_eq!(CborNode::parse(&[0x7F, 0x62, 65, 66, 0x61, 67, 0xFF][..]).unwrap(),
		CborNode::String(format!("ABC")));
	assert_eq!(CborNode::parse(&[0x7F, 0x62, 65, 66, 0x60, 0x61, 67, 0xFF][..]).unwrap(),
		CborNode::String(format!("ABC")));
	CborNode::parse(&[0x7F, 0x62, 65, 66, 0x40, 0x61, 67, 0xFF][..]).unwrap_err();
	CborNode::parse(&[0x7F, 0x62, 0xE2, 0xA2, 0x61, 0x83, 0xFF][..]).unwrap_err();
	//Arrays
	assert_eq!(CborNode::parse(&[0x80][..]).unwrap(), CborNode::Array(vec![]));
	assert_eq!(CborNode::parse(&[0x81, 0x04][..]).unwrap(), CborNode::Array(vec![CborNode::Integer(4)]));
	assert_eq!(CborNode::parse(&[0x81, 0xC2, 0x24][..]).unwrap(), CborNode::Array(vec![CborNode::Tag(2,
		Box::new(CborNode::NegInteger(4)))]));
	CborNode::parse(&[0x83, 0x04, 0x07, 0x18][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x83, 0x04, 0x07, 0x02][..]).unwrap(),
		CborNode::Array(vec![CborNode::Integer(4), CborNode::Integer(7), CborNode::Integer(2)]));
	assert_eq!(CborNode::parse(&[0x83, 0x04, 0x07, 0x18, 0x19][..]).unwrap(),
		CborNode::Array(vec![CborNode::Integer(4), CborNode::Integer(7), CborNode::Integer(0x19)]));
	assert_eq!(CborNode::parse(&[0x98, 3, 0x04, 0x07, 0x18, 0x19][..]).unwrap(),
		CborNode::Array(vec![CborNode::Integer(4), CborNode::Integer(7), CborNode::Integer(0x19)]));
	assert_eq!(CborNode::parse(&[0x99, 0, 3, 0x04, 0x07, 0x18, 0x19][..]).unwrap(),
		CborNode::Array(vec![CborNode::Integer(4), CborNode::Integer(7), CborNode::Integer(0x19)]));
	assert_eq!(CborNode::parse(&[0x9A, 0, 0, 0, 3, 0x04, 0x07, 0x18, 0x19][..]).unwrap(),
		CborNode::Array(vec![CborNode::Integer(4), CborNode::Integer(7), CborNode::Integer(0x19)]));
	assert_eq!(CborNode::parse(&[0x9B, 0, 0, 0, 0, 0, 0, 0, 3, 0x04, 0x07, 0x18, 0x19][..]).unwrap(),
		CborNode::Array(vec![CborNode::Integer(4), CborNode::Integer(7), CborNode::Integer(0x19)]));
	CborNode::parse(&[0x9B, 1, 0, 0, 0, 0, 0, 0, 0][..]).unwrap_err();
	CborNode::parse(&[0x81, 1, 0][..]).unwrap_err();
	CborNode::parse(&[0x9C][..]).unwrap_err();
	CborNode::parse(&[0x9D][..]).unwrap_err();
	CborNode::parse(&[0x9E][..]).unwrap_err();
	CborNode::parse(&[0x9F][..]).unwrap_err();
	CborNode::parse(&[0x9F, 0x04, 0x07, 0x18, 0x19][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0x9F, 0x04, 0x07, 0x18, 0x19, 0xFF][..]).unwrap(),
		CborNode::Array(vec![CborNode::Integer(4), CborNode::Integer(7), CborNode::Integer(0x19)]));
	//Dictionaries
	assert_eq!(CborNode::parse(&[0xA0][..]).unwrap(),
		CborNode::Dictionary(BTreeMap::from_iter([].iter().cloned())));
	assert_eq!(CborNode::parse(&[0xA1, 0x03, 0x05][..]).unwrap(),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(3), CborNode::Integer(5))].iter().
		cloned())));
	CborNode::parse(&[0xA2, 0x03, 0x05, 0x03, 0x05][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0xA2, 0x03, 0x05, 0x04, 0x05][..]).unwrap(),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(3), CborNode::Integer(5)),
		(CborNode::Integer(4), CborNode::Integer(5))].iter().cloned())));
	assert_eq!(CborNode::parse(&[0xA2, 0xC2, 0x03, 0x05, 0xC1, 0x03, 0x06][..]).unwrap(),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Tag(1, Box::new(CborNode::Integer(3))),
		CborNode::Integer(6)), (CborNode::Tag(2, Box::new(CborNode::Integer(3))), CborNode::Integer(5))].
		iter().cloned())));
	assert_eq!(CborNode::parse(&[0xB8, 2, 0x03, 0x05, 0x04, 0x07][..]).unwrap(),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(3), CborNode::Integer(5)),
		(CborNode::Integer(4), CborNode::Integer(7))].iter().cloned())));
	assert_eq!(CborNode::parse(&[0xB9, 0, 2, 0x03, 0x05, 0x04, 0x07][..]).unwrap(),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(3), CborNode::Integer(5)),
		(CborNode::Integer(4), CborNode::Integer(7))].iter().cloned())));
	assert_eq!(CborNode::parse(&[0xBA, 0, 0, 0, 2, 0x03, 0x05, 0x04, 0x07][..]).unwrap(),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(3), CborNode::Integer(5)),
		(CborNode::Integer(4), CborNode::Integer(7))].iter().cloned())));
	assert_eq!(CborNode::parse(&[0xBB, 0, 0, 0, 0, 0, 0, 0, 2, 0x03, 0x05, 0x04, 0x07][..]).unwrap(),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Integer(3), CborNode::Integer(5)),
		(CborNode::Integer(4), CborNode::Integer(7))].iter().cloned())));
	CborNode::parse(&[0xA1, 1][..]).unwrap_err();
	CborNode::parse(&[0xA1, 1, 0, 4][..]).unwrap_err();
	CborNode::parse(&[0xBC][..]).unwrap_err();
	CborNode::parse(&[0xBD][..]).unwrap_err();
	CborNode::parse(&[0xBE][..]).unwrap_err();
	CborNode::parse(&[0xBF][..]).unwrap_err();
	CborNode::parse(&[0xBF, 0xC2, 0x03, 0x05, 0xC1, 0x03, 0x06][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0xBF, 0xC2, 0x03, 0x05, 0xC1, 0x03, 0x06, 0xFF][..]).unwrap(),
		CborNode::Dictionary(BTreeMap::from_iter([(CborNode::Tag(1, Box::new(CborNode::Integer(3))),
		CborNode::Integer(6)), (CborNode::Tag(2, Box::new(CborNode::Integer(3))), CborNode::Integer(5))].
		iter().cloned())));
	//Tags
	assert_eq!(CborNode::parse(&[0xC0, 0x01][..]).unwrap(), CborNode::Tag(0, Box::new(CborNode::Integer(1))));
	assert_eq!(CborNode::parse(&[0xC1, 0x03][..]).unwrap(), CborNode::Tag(1, Box::new(CborNode::Integer(3))));
	assert_eq!(CborNode::parse(&[0xD7, 0x03][..]).unwrap(), CborNode::Tag(23, Box::new(CborNode::Integer(3))));
	assert_eq!(CborNode::parse(&[0xD8, 0x01, 0x03][..]).unwrap(), CborNode::Tag(1, Box::new(
		CborNode::Integer(3))));
	assert_eq!(CborNode::parse(&[0xD9, 0x01, 0x03, 0x04][..]).unwrap(), CborNode::Tag(0x0103, Box::new(
		CborNode::Integer(4))));
	assert_eq!(CborNode::parse(&[0xDA, 0x01, 0x03, 0x05, 0x07, 0x04][..]).unwrap(), CborNode::Tag(0x01030507,
		Box::new(CborNode::Integer(4))));
	assert_eq!(CborNode::parse(&[0xDB, 0x01, 0x03, 0x05, 0x07, 0x09, 0x0B, 0x0D, 0x0F, 0x04][..]).unwrap(),
		CborNode::Tag(0x01030507090B0D0F, Box::new(CborNode::Integer(4))));
	CborNode::parse(&[0xDC][..]).unwrap_err();
	CborNode::parse(&[0xDD][..]).unwrap_err();
	CborNode::parse(&[0xDE][..]).unwrap_err();
	CborNode::parse(&[0xDF][..]).unwrap_err();
	CborNode::parse(&[0xDF, 0x00, 0xFF][..]).unwrap_err();
	//Simples
	assert_eq!(CborNode::parse(&[0xE0][..]).unwrap(), CborNode::Simple(0));
	assert_eq!(CborNode::parse(&[0xE1][..]).unwrap(), CborNode::Simple(1));
	assert_eq!(CborNode::parse(&[0xF4][..]).unwrap(), CborNode::Simple(20));
	assert_eq!(CborNode::parse(&[0xF5][..]).unwrap(), CborNode::Simple(21));
	assert_eq!(CborNode::parse(&[0xF6][..]).unwrap(), CborNode::Simple(22));
	assert_eq!(CborNode::parse(&[0xF7][..]).unwrap(), CborNode::Simple(23));
	CborNode::parse(&[0xF8, 0x1F][..]).unwrap_err();
	assert_eq!(CborNode::parse(&[0xF8, 0x20][..]).unwrap(), CborNode::Simple(32));
	assert_eq!(CborNode::parse(&[0xF8, 0x21][..]).unwrap(), CborNode::Simple(33));
	assert_eq!(CborNode::parse(&[0xF8, 0xFF][..]).unwrap(), CborNode::Simple(255));
	//HalfFloats
	assert_eq!(CborNode::parse(&[0xF9, 0x00, 0x00][..]).unwrap(),
		CborNode::HalfFloat(0));
	assert_eq!(CborNode::parse(&[0xF9, 0x00, 0xFF][..]).unwrap(),
		CborNode::HalfFloat(0xFF));
	assert_eq!(CborNode::parse(&[0xF9, 0x01, 0x03][..]).unwrap(),
		CborNode::HalfFloat(0x0103));
	assert_eq!(CborNode::parse(&[0xF9, 0xFF, 0xFF][..]).unwrap(),
		CborNode::HalfFloat(0xFFFF));
	//Floats
	assert_eq!(CborNode::parse(&[0xFA, 0x00, 0x00, 0x00, 0x00][..]).unwrap(),
		CborNode::Float(0));
	assert_eq!(CborNode::parse(&[0xFA, 0x00, 0x00, 0xFF, 0xFF][..]).unwrap(),
		CborNode::Float(0xFFFF));
	assert_eq!(CborNode::parse(&[0xFA, 0x01, 0x03, 0x05, 0x07][..]).unwrap(),
		CborNode::Float(0x01030507));
	assert_eq!(CborNode::parse(&[0xFA, 0xFF, 0xFF, 0xFF, 0xFF][..]).unwrap(),
		CborNode::Float(0xFFFFFFFF));
	//Doubles
	assert_eq!(CborNode::parse(&[0xFB, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00][..]).unwrap(),
		CborNode::Double(0));
	assert_eq!(CborNode::parse(&[0xFB, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF][..]).unwrap(),
		CborNode::Double(0xFFFFFFFF));
	assert_eq!(CborNode::parse(&[0xFB, 0x01, 0x03, 0x05, 0x07, 0x09, 0x0B, 0x0D, 0x0F][..]).unwrap(),
		CborNode::Double(0x01030507090B0D0F));
	assert_eq!(CborNode::parse(&[0xFB, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF][..]).unwrap(),
		CborNode::Double(0xFFFFFFFFFFFFFFFF));
	//Invalid.
	CborNode::parse(&[0xFC][..]).unwrap_err();
	CborNode::parse(&[0xFD][..]).unwrap_err();
	CborNode::parse(&[0xFE][..]).unwrap_err();
	CborNode::parse(&[0xFF][..]).unwrap_err();
}


#[test]
fn overlong_bom()
{
	let input = [0xF0, 0x8F, 0xBB, 0xBF];
	assert!(from_utf8(&input).is_err());
}


#[test]
fn cbor_casts()
{
	assert_eq!(CBOR_NULL.as_null(), Some(()));
	assert_eq!(CBOR_UNDEFINED.as_null(), None);
	assert_eq!(CBOR_FALSE.as_null(), None);
	assert_eq!(CBOR_TRUE.as_null(), None);

	assert_eq!(CBOR_NULL.as_undefined(), None);
	assert_eq!(CBOR_UNDEFINED.as_undefined(), Some(()));
	assert_eq!(CBOR_FALSE.as_undefined(), None);
	assert_eq!(CBOR_TRUE.as_undefined(), None);

	assert_eq!(CBOR_NULL.as_boolean(), None);
	assert_eq!(CBOR_UNDEFINED.as_boolean(), None);
	assert_eq!(CBOR_FALSE.as_boolean(), Some(false));
	assert_eq!(CBOR_TRUE.as_boolean(), Some(true));

	assert_eq!(CBOR_NULL.as_unsigned(), None);
	assert_eq!(CBOR_FALSE.as_unsigned(), None);
	assert_eq!(CborNode::Integer(0).as_unsigned(), Some(0));
	assert_eq!(CborNode::Integer(1).as_unsigned(), Some(1));
	assert_eq!(CborNode::Integer(1234).as_unsigned(), Some(1234));
	assert_eq!(CborNode::Integer(9223372036854775808).as_unsigned(), Some(9223372036854775808));
	assert_eq!(CborNode::NegInteger(0).as_unsigned(), None);

	assert_eq!(CBOR_NULL.as_integer(), None);
	assert_eq!(CBOR_FALSE.as_integer(), None);
	assert_eq!(CborNode::Integer(0).as_integer(), Some(0));
	assert_eq!(CborNode::Integer(1).as_integer(), Some(1));
	assert_eq!(CborNode::Integer(1234).as_integer(), Some(1234));
	assert_eq!(CborNode::Integer(9223372036854775808).as_integer(), None);
	assert_eq!(CborNode::NegInteger(0).as_integer(), Some(-1));
	assert_eq!(CborNode::NegInteger(9223372036854775807).as_integer(), Some(-9223372036854775808i64));

	assert_eq!(CborNode::Integer(0).as_unsigned_negative(), None);
	assert_eq!(CborNode::NegInteger(0).as_unsigned_negative(), Some(0));
	assert_eq!(CborNode::NegInteger(9223372036854775807).as_unsigned_negative(), Some(9223372036854775807));

	assert_eq!(CborNode::Integer(0).as_octets(), None);
	assert_eq!(CborNode::parse(&[0x40]).unwrap().as_octets().unwrap().len(), 0);
	assert_eq!(CborNode::parse(&[0x43, 102, 111, 111]).unwrap().as_octets().unwrap().len(), 3);
	assert_eq!(CborNode::parse(&[0x60]).unwrap().as_octets().is_none(), true);
	assert_eq!(CborNode::parse(&[0x63, 102, 111, 111]).unwrap().as_octets().is_none(), true);

	assert_eq!(CborNode::Integer(0).as_string(), None);
	assert_eq!(CborNode::parse(&[0x40]).unwrap().as_string().is_none(), true);
	assert_eq!(CborNode::parse(&[0x43, 102, 111, 111]).unwrap().as_string().is_none(), true);
	assert_eq!(CborNode::parse(&[0x60]).unwrap().as_string().unwrap().len(), 0);
	assert_eq!(CborNode::parse(&[0x63, 102, 111, 111]).unwrap().as_string().unwrap().len(), 3);

	assert_eq!(CborNode::Integer(0).as_array().is_none(), true);
	assert_eq!(CborNode::parse(&[0x40]).unwrap().as_array().is_none(), true);
	assert_eq!(CborNode::parse(&[0x43, 102, 111, 111]).unwrap().as_array().is_none(), true);
	assert_eq!(CborNode::parse(&[0x80]).unwrap().as_array().unwrap().len(), 0);
	assert_eq!(CborNode::parse(&[0x83, 0x13, 0x24, 0x01]).unwrap().as_array().unwrap().len(), 3);

	assert_eq!(CborNode::parse(&[0x80]).unwrap().as_dictionary().is_none(), true);
	assert_eq!(CborNode::parse(&[0x83, 0x13, 0x24, 0x01]).unwrap().as_dictionary().is_none(), true);
	assert_eq!(CborNode::parse(&[0xA0]).unwrap().as_dictionary().unwrap().len(), 0);
	assert_eq!(CborNode::parse(&[0xA2, 0x13, 0x24, 0x01, 0x17]).unwrap().as_dictionary().unwrap().len(), 2);

	assert_eq!(CborNode::parse(&[0xA0]).unwrap().as_tag().is_none(), true);
	assert_eq!(CborNode::parse(&[0xC3,0xF4]).unwrap().as_tag(), Some((3u64, &CBOR_FALSE)));

	assert_eq!(CborNode::parse(&[0xF9, 0x12, 0x34]).unwrap().as_half_float(), Some(0x1234));
	assert_eq!(CborNode::parse(&[0xFA, 0x12, 0x34, 0x56, 0x78]).unwrap().as_half_float(), None);
	assert_eq!(CborNode::parse(&[0xFB, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0]).unwrap().as_half_float(),
		None);

	assert_eq!(CborNode::parse(&[0xF9, 0x12, 0x34]).unwrap().as_single_float(), None);
	assert_eq!(CborNode::parse(&[0xFA, 0x12, 0x34, 0x56, 0x78]).unwrap().as_single_float(), Some(0x12345678));
	assert_eq!(CborNode::parse(&[0xFB, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0]).unwrap().
		as_single_float(), None);

	assert_eq!(CborNode::parse(&[0xF9, 0x12, 0x34]).unwrap().as_double_float(), None);
	assert_eq!(CborNode::parse(&[0xFA, 0x12, 0x34, 0x56, 0x78]).unwrap().as_double_float(), None);
	assert_eq!(CborNode::parse(&[0xFB, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0]).unwrap().
		as_double_float(), Some(0x123456789ABCDEF0));
}

#[test]
fn cbor_from_integer()
{
	assert_eq!(CborNode::from_integer(-2), CborNode::NegInteger(1));
	assert_eq!(CborNode::from_integer(-1), CborNode::NegInteger(0));
	assert_eq!(CborNode::from_integer(0), CborNode::Integer(0));
	assert_eq!(CborNode::from_integer(1), CborNode::Integer(1));
}

#[test]
fn test_json_parse_num()
{
	assert_eq!(JsonNode2::parse("1234567890123456789").expect("Failed to parse").as_number().
		expect("Not number"), 1234567890123456789u64);
	assert_eq!(JsonNode2::parse("-912345678912345678").expect("Failed to parse").as_number().
		expect("Not number"), -912345678912345678i64);
	assert_eq!(JsonNode2::parse("-1.5").expect("Failed to parse").as_number().
		expect("Not number").float(), -1.5f64);
	assert_eq!(JsonNode2::parse("-1.5e10").expect("Failed to parse").as_number().
		expect("Not number").float(), -1.5e10f64);
}

macro_rules! int_testcase
{
	($i:expr) => { ($i, stringify!($i)) }
}

#[test]
fn test_write_integer()
{
	static TESTCASES: &'static [(i64, &'static str)] = &[
		int_testcase!(-9223372036854775808), int_testcase!(-9223372036854775807),
		int_testcase!(-1000000000000000000), int_testcase!(-999999999999999999),
		int_testcase!(-100000000000000000), int_testcase!(-99999999999999999),
		int_testcase!(-10000000000000000), int_testcase!(-9999999999999999),
		int_testcase!(-1000000000000000), int_testcase!(-999999999999999), int_testcase!(-100000000000000),
		int_testcase!(-99999999999999), int_testcase!(-10000000000000), int_testcase!(-9999999999999),
		int_testcase!(-1000000000000), int_testcase!(-999999999999), int_testcase!(-100000000000),
		int_testcase!(-99999999999), int_testcase!(-10000000000), int_testcase!(-9999999999),
		int_testcase!(-1000000000), int_testcase!(-999999999), int_testcase!(-100000000),
		int_testcase!(-99999999), int_testcase!(-10000000), int_testcase!(-9999999), int_testcase!(-1000000),
		int_testcase!(-999999), int_testcase!(-100000), int_testcase!(-99999),
		int_testcase!(-10000), int_testcase!(-9999), int_testcase!(-1000), int_testcase!(-999),
		int_testcase!(-100), int_testcase!(-99), int_testcase!(-10), int_testcase!(-9), int_testcase!(-1),
		int_testcase!(0), int_testcase!(1), int_testcase!(9), int_testcase!(10), int_testcase!(99),
		int_testcase!(100), int_testcase!(999), int_testcase!(1000), int_testcase!(9999),
		int_testcase!(10000), int_testcase!(99999), int_testcase!(100000), int_testcase!(999999),
		int_testcase!(1000000), int_testcase!(9999999), int_testcase!(10000000), int_testcase!(99999999),
		int_testcase!(100000000), int_testcase!(999999999), int_testcase!(1000000000),
		int_testcase!(9999999999), int_testcase!(10000000000), int_testcase!(99999999999),
		int_testcase!(100000000000), int_testcase!(999999999999), int_testcase!(1000000000000),
		int_testcase!(9999999999999), int_testcase!(10000000000000), int_testcase!(99999999999999),
		int_testcase!(100000000000000), int_testcase!(999999999999999), int_testcase!(1000000000000000),
		int_testcase!(9999999999999999), int_testcase!(10000000000000000), int_testcase!(99999999999999999),
		int_testcase!(100000000000000000), int_testcase!(999999999999999999),
		int_testcase!(1000000000000000000), int_testcase!(9223372036854775807),
	];
	for (k, v) in TESTCASES.iter() { assert_eq!(JsonWriter::from(*k).serialize().deref(), *v); }
}

#[test]
fn test_json_write_type()
{
	assert_eq!(JsonWriter::from(()).serialize().deref(), "null");
	assert_eq!(JsonWriter::from(false).serialize().deref(), "false");
	assert_eq!(JsonWriter::from(true).serialize().deref(), "true");
	assert_eq!(JsonWriter::from(42).serialize().deref(), "42");
	assert_eq!(JsonWriter::from("hello").serialize().deref(), "\"hello\"");
	assert_eq!(JsonWriter::from("hello".to_owned()).serialize().deref(), "\"hello\"");
	assert_eq!(JsonWriter::from(&"hello".to_owned()).serialize().deref(), "\"hello\"");
	assert_eq!(JsonWriter::from(&[JsonWriter::from(1), JsonWriter::from(2), JsonWriter::from(3)][..]).
		serialize().deref(), "[1,2,3]");
	assert_eq!(JsonWriter::from([JsonWriter::from(1), JsonWriter::from(2), JsonWriter::from(3)].to_vec()).
		serialize().deref(), "[1,2,3]");
	assert_eq!(JsonWriter::from(&[("a", JsonWriter::from(1)), ("b", JsonWriter::from(2)),
		("c", JsonWriter::from(3))][..]).serialize().deref(), "{\"a\":1,\"b\":2,\"c\":3}");
	assert_eq!(JsonWriter::from(&[("a".to_owned(), JsonWriter::from(1)), ("b".to_owned(), JsonWriter::from(2)),
		("c".to_owned(), JsonWriter::from(3))][..]).serialize().deref(), "{\"a\":1,\"b\":2,\"c\":3}");
}

#[test]
fn test_json_write_string_special()
{
	let teststring = "\
		0:\u{0}1:\u{1}2:\u{2}3:\u{3}4:\u{4}5:\u{5}6:\u{6}7:\u{7}\
		8:\u{8}9:\u{9}a:\u{a}b:\u{b}c:\u{c}d:\u{d}e:\u{e}f:\u{f}\
		10:\u{10}11:\u{11}12:\u{12}13:\u{13}14:\u{14}15:\u{15}16:\u{16}17:\u{17}\
		18:\u{18}19:\u{19}1a:\u{1a}1b:\u{1b}1c:\u{1c}1d:\u{1d}1e:\u{1e}1f:\u{1f}\
		\u{20}!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`\
		abcdefghijklmnopqstuvwxyz{|}~ö\u{5}ö\u{8}ö\u{9}ö\u{a}ö\u{c}ö\u{d}ö\"ö\\ö\u{10000}X";
	let answer = "\"\
		0:\\u00001:\\u00012:\\u00023:\\u00034:\\u00045:\\u00056:\\u00067:\\u0007\
		8:\\b9:\\ta:\\nb:\\u000bc:\\fd:\\re:\\u000ef:\\u000f\
		10:\\u001011:\\u001112:\\u001213:\\u001314:\\u001415:\\u001516:\\u001617:\\u0017\
		18:\\u001819:\\u00191a:\\u001a1b:\\u001b1c:\\u001c1d:\\u001d1e:\\u001e1f:\\u001f\
		\u{20}!\\\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\\\]^_`\
		abcdefghijklmnopqstuvwxyz{|}~ö\\u0005ö\\bö\\tö\\nö\\fö\\rö\\\"ö\\\\ö\u{10000}X\"";
	assert_eq!(JsonWriter::from(teststring).serialize().deref(), answer);
	assert_eq!(&JsonWriter::from(&[(teststring,JsonWriter::from(()))][..]).serialize()[1..394], answer);
}

#[test]
fn test_cbor_serialize()
{
	//Integers.
	assert_eq!(CborNode::from(0u64).serialize_to_vec().deref(), &[0x00]);
	assert_eq!(CborNode::from(0i64).serialize_to_vec().deref(), &[0x00]);
	assert_eq!(CborNode::from(1u64).serialize_to_vec().deref(), &[0x01]);
	assert_eq!(CborNode::from(1i64).serialize_to_vec().deref(), &[0x01]);
	assert_eq!(CborNode::from(23u64).serialize_to_vec().deref(), &[0x17]);
	assert_eq!(CborNode::from(24u64).serialize_to_vec().deref(), &[0x18, 0x18]);
	assert_eq!(CborNode::from(0x34u64).serialize_to_vec().deref(), &[0x18, 0x34]);
	assert_eq!(CborNode::from(255u64).serialize_to_vec().deref(), &[0x18, 0xFF]);
	assert_eq!(CborNode::from(256u64).serialize_to_vec().deref(), &[0x19, 1, 0]);
	assert_eq!(CborNode::from(0x1234u64).serialize_to_vec().deref(), &[0x19, 0x12, 0x34]);
	assert_eq!(CborNode::from(65535u64).serialize_to_vec().deref(), &[0x19, 0xFF, 0xFF]);
	assert_eq!(CborNode::from(65536u64).serialize_to_vec().deref(), &[0x1A, 0, 1, 0, 0]);
	assert_eq!(CborNode::from(0x12345678u64).serialize_to_vec().deref(), &[0x1A, 0x12, 0x34, 0x56, 0x78]);
	assert_eq!(CborNode::from(4294967295u64).serialize_to_vec().deref(), &[0x1A, 0xFF, 0xFF, 0xFF, 0xFF]);
	assert_eq!(CborNode::from(4294967296u64).serialize_to_vec().deref(), &[0x1B, 0, 0, 0, 1, 0, 0, 0, 0]);
	assert_eq!(CborNode::from(0x123456789ABCDEF0u64).serialize_to_vec().deref(),
		&[0x1B, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0]);
	assert_eq!(CborNode::from(0x123456789ABCDEF0i64).serialize_to_vec().deref(),
		&[0x1B, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0]);
	assert_eq!(CborNode::from(9223372036854775807i64).serialize_to_vec().deref(),
		&[0x1B, 0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]);
	assert_eq!(CborNode::from(18446744073709551615u64).serialize_to_vec().deref(),
		&[0x1B, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]);
	//Negative Integers.
	assert_eq!(CborNode::from(-1i64).serialize_to_vec().deref(), &[0x20]);
	assert_eq!(CborNode::from(-2i64).serialize_to_vec().deref(), &[0x21]);
	assert_eq!(CborNode::from(-24i64).serialize_to_vec().deref(), &[0x37]);
	assert_eq!(CborNode::from(-25i64).serialize_to_vec().deref(), &[0x38, 0x18]);
	assert_eq!(CborNode::from(-0x35i64).serialize_to_vec().deref(), &[0x38, 0x34]);
	assert_eq!(CborNode::from(-256i64).serialize_to_vec().deref(), &[0x38, 0xFF]);
	assert_eq!(CborNode::from(-257i64).serialize_to_vec().deref(), &[0x39, 1, 0]);
	assert_eq!(CborNode::from(-0x1235i64).serialize_to_vec().deref(), &[0x39, 0x12, 0x34]);
	assert_eq!(CborNode::from(-65536i64).serialize_to_vec().deref(), &[0x39, 0xFF, 0xFF]);
	assert_eq!(CborNode::from(-65537i64).serialize_to_vec().deref(), &[0x3A, 0, 1, 0, 0]);
	assert_eq!(CborNode::from(-0x12345679i64).serialize_to_vec().deref(), &[0x3A, 0x12, 0x34, 0x56, 0x78]);
	assert_eq!(CborNode::from(-4294967296i64).serialize_to_vec().deref(), &[0x3A, 0xFF, 0xFF, 0xFF, 0xFF]);
	assert_eq!(CborNode::from(-4294967297i64).serialize_to_vec().deref(), &[0x3B, 0, 0, 0, 1, 0, 0, 0, 0]);
	assert_eq!(CborNode::from(-0x123456789ABCDEF1i64).serialize_to_vec().deref(),
		&[0x3B, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0]);
	assert_eq!(CborNode::from(-9223372036854775807i64).serialize_to_vec().deref(),
		&[0x3B, 0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFE]);
	assert_eq!(CborNode::from(-9223372036854775808i64).serialize_to_vec().deref(),
		&[0x3B, 0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]);
	assert_eq!(CborNode::NegInteger(18446744073709551615).serialize_to_vec().deref(),
		&[0x3B, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]);
	//Octet strings.
	assert_eq!(CborNode::from(&b""[..]).serialize_to_vec().deref(), &[0x40]);
	assert_eq!(CborNode::from(&b"X"[..]).serialize_to_vec().deref(), b"AX");
	assert_eq!(CborNode::from(&b"ABCDEFGHIJKLMNOPQRSTUVZ"[..]).serialize_to_vec().deref(),
		b"WABCDEFGHIJKLMNOPQRSTUVZ");
	assert_eq!(CborNode::from(&b"ABCDEFGHIJKLMNOPQRSTUVWZ"[..]).serialize_to_vec().deref(),
		&b"X\x18ABCDEFGHIJKLMNOPQRSTUVWZ"[..]);
	assert_eq!(CborNode::from(&b"ABCDEFGHIJKLMNOPQRSTUVWXYZ012345"[..]).serialize_to_vec().deref(),
		&b"X ABCDEFGHIJKLMNOPQRSTUVWXYZ012345"[..]);
	assert_eq!(CborNode::from(&b"ABCDEFGHIJKLMNOPQRSTUVWXYZ012345".to_vec()).serialize_to_vec().deref(),
		&b"X ABCDEFGHIJKLMNOPQRSTUVWXYZ012345"[..]);
	assert_eq!(CborNode::from(b"ABCDEFGHIJKLMNOPQRSTUVWXYZ012345".to_vec()).serialize_to_vec().deref(),
		&b"X ABCDEFGHIJKLMNOPQRSTUVWXYZ012345"[..]);
	//Text strings.
	assert_eq!(CborNode::from("").serialize_to_vec().deref(), &[0x60]);
	assert_eq!(CborNode::from("X").serialize_to_vec().deref(), b"aX");
	assert_eq!(CborNode::from("ABCDEFGHIJKLMNOPQRSTUVZ").serialize_to_vec().deref(),
		b"wABCDEFGHIJKLMNOPQRSTUVZ");
	assert_eq!(CborNode::from("ABCDEFGHIJKLMNOPQRSTUVWZ").serialize_to_vec().deref(),
		&b"x\x18ABCDEFGHIJKLMNOPQRSTUVWZ"[..]);
	assert_eq!(CborNode::from("ABCDEFGHIJKLMNOPQRSTUVWXYZ012345").serialize_to_vec().deref(),
		&b"x ABCDEFGHIJKLMNOPQRSTUVWXYZ012345"[..]);
	assert_eq!(CborNode::from(&"ABCDEFGHIJKLMNOPQRSTUVWXYZ012345".to_owned()).serialize_to_vec().deref(),
		&b"x ABCDEFGHIJKLMNOPQRSTUVWXYZ012345"[..]);
	assert_eq!(CborNode::from("ABCDEFGHIJKLMNOPQRSTUVWXYZ012345".to_owned()).serialize_to_vec().deref(),
		&b"x ABCDEFGHIJKLMNOPQRSTUVWXYZ012345"[..]);
	//Arrays.
	assert_eq!(CborNode::Array(Vec::new()).serialize_to_vec().deref(), &[0x80]);
	assert_eq!(CborNode::from(&[
		CborNode::from(23u64),
	][..]).serialize_to_vec().deref(), &[0x81, 0x17]);
	assert_eq!(CborNode::from(&[
		CborNode::from(23u64), CborNode::from(255u64),
	][..]).serialize_to_vec().deref(), &[0x82, 0x17, 0x18, 0xFF]);
	assert_eq!(CborNode::from(&[
		CborNode::from("A"), CborNode::from("B"), CborNode::from("C"), CborNode::from("D"),
		CborNode::from("E"), CborNode::from("F"), CborNode::from("G"), CborNode::from("H"),
		CborNode::from("I"), CborNode::from("J"), CborNode::from("K"), CborNode::from("L"),
		CborNode::from("M"), CborNode::from("N"), CborNode::from("O"), CborNode::from("P"),
		CborNode::from("Q"), CborNode::from("R"), CborNode::from("S"), CborNode::from("T"),
		CborNode::from("U"), CborNode::from("V"), CborNode::from("W"), CborNode::from("X"),
		CborNode::from("Y"), CborNode::from("Z"),
	][..]).serialize_to_vec().deref(), &b"\x98\x1AaAaBaCaDaEaFaGaHaIaJaKaLaMaNaOaPaQaRaSaTaUaVaWaXaYaZ"[..]);
	assert_eq!(CborNode::from(&[
		CborNode::from(&[CborNode::from("ZOT"), CborNode::from("QUX")][..]),
	][..]).serialize_to_vec().deref(), b"\x81\x82cZOTcQUX");
	assert_eq!(CborNode::from(&[
		CborNode::from(&[CborNode::from("ZOT"), CborNode::from("QUX")][..]),
	].to_vec()).serialize_to_vec().deref(), b"\x81\x82cZOTcQUX");
	assert_eq!(CborNode::from([
		CborNode::from(&[CborNode::from("ZOT"), CborNode::from("QUX")][..]),
	].to_vec()).serialize_to_vec().deref(), b"\x81\x82cZOTcQUX");
	//Dictionaries.
	assert_eq!(CborNode::Dictionary(BTreeMap::new()).serialize_to_vec().deref(), &[0xA0]);
	assert_eq!(CborNode::from(&[
		(CborNode::from(23u64), CborNode::from(true)),
	][..]).serialize_to_vec().deref(), &[0xA1, 0x17, 0xF5]);
	assert_eq!(CborNode::from(&[
		(CborNode::from(23u64), CborNode::from(true)),
		(CborNode::from(22u64), CborNode::from(false)),
	][..]).serialize_to_vec().deref(), &[0xA2, 0x16, 0xF4, 0x17, 0xF5]);
	assert_eq!(CborNode::from(&[
		(CborNode::from(23u64), CborNode::Simple(23)), (CborNode::from(22u64), CborNode::Simple(22)),
		(CborNode::from(21u64), CborNode::Simple(21)), (CborNode::from(20u64), CborNode::Simple(20)),
		(CborNode::from(19u64), CborNode::Simple(19)), (CborNode::from(18u64), CborNode::Simple(18)),
		(CborNode::from(17u64), CborNode::Simple(17)), (CborNode::from(16u64), CborNode::Simple(16)),
		(CborNode::from(15u64), CborNode::Simple(15)), (CborNode::from(14u64), CborNode::Simple(14)),
		(CborNode::from(13u64), CborNode::Simple(13)), (CborNode::from(12u64), CborNode::Simple(12)),
		(CborNode::from(11u64), CborNode::Simple(11)), (CborNode::from(10u64), CborNode::Simple(10)),
		(CborNode::from(9u64), CborNode::Simple(9)), (CborNode::from(8u64), CborNode::Simple(8)),
		(CborNode::from(7u64), CborNode::Simple(7)), (CborNode::from(6u64), CborNode::Simple(6)),
		(CborNode::from(5u64), CborNode::Simple(5)), (CborNode::from(4u64), CborNode::Simple(4)),
		(CborNode::from(3u64), CborNode::Simple(3)), (CborNode::from(2u64), CborNode::Simple(2)),
		(CborNode::from(1u64), CborNode::Simple(1)), (CborNode::from(0u64), CborNode::Simple(0)),
	][..]).serialize_to_vec().deref(), &[0xB8, 0x18, 0x00, 0xE0, 0x01, 0xE1, 0x02, 0xE2, 0x03, 0xE3,
		0x04, 0xE4, 0x05, 0xE5, 0x06, 0xE6, 0x07, 0xE7, 0x08, 0xE8, 0x09, 0xE9, 0x0A, 0xEA, 0x0B, 0xEB,
		0x0C, 0xEC, 0x0D, 0xED, 0x0E, 0xEE, 0x0F, 0xEF, 0x10, 0xF0, 0x11, 0xF1, 0x12, 0xF2, 0x13, 0xF3,
		0x14, 0xF4, 0x15, 0xF5, 0x16, 0xF6, 0x17, 0xF7][..]);
	//Tags.
	assert_eq!(CborNode::from((0u64, CborNode::from(false))).serialize_to_vec().deref(), &[0xC0, 0xF4]);
	assert_eq!(CborNode::from((1u64, CborNode::from(false))).serialize_to_vec().deref(), &[0xC1, 0xF4]);
	assert_eq!(CborNode::from((23u64, CborNode::from(false))).serialize_to_vec().deref(), &[0xD7, 0xF4]);
	assert_eq!(CborNode::from((24u64, CborNode::from(false))).serialize_to_vec().deref(), &[0xD8, 0x18, 0xF4]);
	assert_eq!(CborNode::from((0x34u64, CborNode::from(false))).serialize_to_vec().deref(), &[0xD8, 0x34, 0xF4]);
	assert_eq!(CborNode::from((255u64, CborNode::from(false))).serialize_to_vec().deref(), &[0xD8, 0xFF, 0xF4]);
	assert_eq!(CborNode::from((256u64, CborNode::from(false))).serialize_to_vec().deref(), &[0xD9, 1, 0, 0xF4]);
	assert_eq!(CborNode::from((0x1234u64, CborNode::from(false))).serialize_to_vec().deref(),
		&[0xD9, 0x12, 0x34, 0xF4]);
	assert_eq!(CborNode::from((65535u64, CborNode::from(false))).serialize_to_vec().deref(),
		&[0xD9, 0xFF, 0xFF, 0xF4]);
	assert_eq!(CborNode::from((65536u64, CborNode::from(false))).serialize_to_vec().deref(),
		&[0xDA, 0, 1, 0, 0, 0xF4]);
	assert_eq!(CborNode::from((0x12345678u64, CborNode::from(false))).serialize_to_vec().deref(),
		&[0xDA, 0x12, 0x34, 0x56, 0x78, 0xF4]);
	assert_eq!(CborNode::from((4294967295u64, CborNode::from(false))).serialize_to_vec().deref(),
		&[0xDA, 0xFF, 0xFF, 0xFF, 0xFF, 0xF4]);
	assert_eq!(CborNode::from((4294967296u64, CborNode::from(false))).serialize_to_vec().deref(),
		&[0xDB, 0, 0, 0, 1, 0, 0, 0, 0, 0xF4]);
	assert_eq!(CborNode::from((0x123456789ABCDEF0u64, CborNode::from(false))).serialize_to_vec().deref(),
		&[0xDB, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0, 0xF4]);
	assert_eq!(CborNode::from((18446744073709551615u64, CborNode::from(false))).serialize_to_vec().deref(),
		&[0xDB, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xF4]);
	assert_eq!(CborNode::from((0u64, CborNode::from((2u64, CborNode::from(false))))).serialize_to_vec().deref(),
		&[0xC0, 0xC2, 0xF4]);
	//Simples.
	assert_eq!(CborNode::Simple(0).serialize_to_vec().deref(), &[0xE0]);
	assert_eq!(CborNode::Simple(23).serialize_to_vec().deref(), &[0xF7]);
	assert_eq!(CborNode::Simple(32).serialize_to_vec().deref(), &[0xF8, 0x20]);
	assert_eq!(CborNode::Simple(255).serialize_to_vec().deref(), &[0xF8, 0xFF]);
	//FP.
	assert_eq!(CborNode::HalfFloat(0x7C00).serialize_to_vec().deref(), &[0xF9, 0x7C, 0x00]);
	assert_eq!(CborNode::HalfFloat(0xFC00).serialize_to_vec().deref(), &[0xF9, 0xFC, 0x00]);
	assert_eq!(CborNode::HalfFloat(0x7FFF).serialize_to_vec().deref(), &[0xF9, 0x7E, 0x00]);
	assert_eq!(CborNode::HalfFloat(0xFFFF).serialize_to_vec().deref(), &[0xF9, 0x7E, 0x00]);
	assert_eq!(CborNode::Float(0x7F800000).serialize_to_vec().deref(), &[0xF9, 0x7C, 0x00]);
	assert_eq!(CborNode::Float(0xFF800000).serialize_to_vec().deref(), &[0xF9, 0xFC, 0x00]);
	assert_eq!(CborNode::Float(0x7FFFFFFF).serialize_to_vec().deref(), &[0xF9, 0x7E, 0x00]);
	assert_eq!(CborNode::Float(0xFFFFFFFF).serialize_to_vec().deref(), &[0xF9, 0x7E, 0x00]);
	assert_eq!(CborNode::Double(0x7FF0000000000000).serialize_to_vec().deref(), &[0xF9, 0x7C, 0x00]);
	assert_eq!(CborNode::Double(0xFFF0000000000000).serialize_to_vec().deref(), &[0xF9, 0xFC, 0x00]);
	assert_eq!(CborNode::Double(0x7FFFFFFFFFFFFFFF).serialize_to_vec().deref(), &[0xF9, 0x7E, 0x00]);
	assert_eq!(CborNode::Double(0xFFFFFFFFFFFFFFFF).serialize_to_vec().deref(), &[0xF9, 0x7E, 0x00]);
	assert_eq!(CborNode::from(0.0f32).serialize_to_vec().deref(), &[0xF9, 0x00, 0x00]);
	assert_eq!(CborNode::from(0.0f64).serialize_to_vec().deref(), &[0xF9, 0x00, 0x00]);
	assert_eq!(CborNode::from(1.0f32).serialize_to_vec().deref(), &[0xF9, 0x3C, 0x00]);
	assert_eq!(CborNode::from(1.0f64).serialize_to_vec().deref(), &[0xF9, 0x3C, 0x00]);
	assert_eq!(CborNode::from(-1.0f32).serialize_to_vec().deref(), &[0xF9, 0xBC, 0x00]);
	assert_eq!(CborNode::from(-1.0f64).serialize_to_vec().deref(), &[0xF9, 0xBC, 0x00]);
	assert_eq!(CborNode::from(2049.0f32).serialize_to_vec().deref(), &[0xFA, 0x45, 0x00, 0x10, 0x00]);
	assert_eq!(CborNode::from(2049.0f64).serialize_to_vec().deref(), &[0xFA, 0x45, 0x00, 0x10, 0x00]);
	assert_eq!(CborNode::from(-2049.0f32).serialize_to_vec().deref(), &[0xFA, 0xC5, 0x00, 0x10, 0x00]);
	assert_eq!(CborNode::from(-2049.0f64).serialize_to_vec().deref(), &[0xFA, 0xC5, 0x00, 0x10, 0x00]);
	assert_eq!(CborNode::from(16777217.0f64).serialize_to_vec().deref(),
		&[0xFB, 0x41, 0x70, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00]);
	assert_eq!(CborNode::from(-16777217.0f64).serialize_to_vec().deref(),
		&[0xFB, 0xC1, 0x70, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00]);
}

#[test]
fn test_json_object()
{
	assert_eq!(json_object!(type => "dns", value => "foo.example.org", wildcard => true).serialize().deref(),
		"{\"type\":\"dns\",\"value\":\"foo.example.org\",\"wildcard\":true}");
	assert_eq!(json_array!(1, 2, 3).serialize().deref(), "[1,2,3]");
	assert_eq!(json_value!(false).serialize().deref(), "false");
	assert_eq!(json_value!(()).serialize().deref(), "null");
	assert_eq!(json_value!(42).serialize().deref(), "42");
}

#[test]
fn test_json_parse_is_sound()
{
	static TESTCASES: &'static [(i64, &'static str)] = &[
		int_testcase!(-9007199254740993), int_testcase!(9007199254740993),
	];
	for (k, v) in TESTCASES.iter() { assert_eq!(JsonWriter::from(*k).serialize().deref(), *v); }
}

fn nv(x: u64) -> JsonNumber
{
	JsonNumber::parse(N::new_unsigned(x))
}

fn not_null(j: &JsonNode2)
{
	assert!(!j.is_null());
	assert!(j.as_null().is_none());
	assert!(j != &JsonNode2::null());
}

fn not_boolean(j: &JsonNode2)
{
	assert!(!j.is_boolean());
	assert!(j.as_boolean().is_none());
	assert!(j != &false);
	assert!(j != &true);
}

fn not_number(j: &JsonNode2)
{
	assert!(!j.is_number());
	assert!(j.as_number().is_none());
	assert!(j != &42i64);
	assert!(j != &42u64);
	assert!(j != &-42i64);
}

fn not_string(j: &JsonNode2)
{
	assert!(!j.is_string());
	assert!(j.as_string().is_none());
	assert!(j != "foo");
	assert!(j != "bar");
	assert!(j != "x");
	assert!(j != "y");
}

fn not_array(j: &JsonNode2)
{
	assert!(!j.is_array());
	assert!(j.as_array().is_none());
	assert!(j.nth(0).is_none());
	assert!(j.array_len() == 0);
	assert!(j.iter_array().next().is_none());
}

fn not_object(j: &JsonNode2)
{
	assert!(!j.is_object());
	assert!(j.as_dictionary().is_none());
	assert!(j.field("x").is_none());
	assert!(j.field("y").is_none());
	assert!(j.field("foo").is_none());
	assert!(j.field("bar").is_none());
	assert!(j.field_names().next().is_none());
	assert!(j.iter_object().next().is_none());
}

#[test]
fn json_node_2_null()
{
	let j = JsonNode2::parse("null").unwrap();
	assert!(j.is_null());
	assert!(j.as_null() == Some(()));
	assert!(j == JsonNode2::null());
	not_boolean(&j);
	not_number(&j);
	not_string(&j);
	not_array(&j);
	not_object(&j);
}

#[test]
fn json_node_2_boolean()
{
	let j = JsonNode2::parse("false").unwrap();
	assert!(j.is_boolean());
	assert!(j.as_boolean() == Some(false));
	assert!(j == false);
	assert!(j != true);
	not_null(&j);
	not_number(&j);
	not_string(&j);
	not_array(&j);
	not_object(&j);
}

#[test]
fn json_node_2_number()
{
	let j = JsonNode2::parse("42").unwrap();
	assert!(j.is_number());
	assert!(j.as_number() == Some(nv(42)));
	assert!(j == 42i64);
	assert!(j != -42i64);
	assert!(j == 42u64);
	not_null(&j);
	not_boolean(&j);
	not_string(&j);
	not_array(&j);
	not_object(&j);
}

#[test]
fn json_node_2_string()
{
	let j = JsonNode2::parse("\"foo\"").unwrap();
	assert!(j.is_string());
	assert!(j.as_string() == Some("foo"));
	assert!(j == "foo");
	not_null(&j);
	not_boolean(&j);
	not_number(&j);
	not_array(&j);
	not_object(&j);
}

#[test]
fn json_node_2_array()
{
	let j = JsonNode2::parse("[42,56]").unwrap();
	assert!(j.is_array());
	assert!(j.as_array().map(|x|x.len()) == Some(2));
	assert!(j.nth(0).and_then(|x|x.as_number()) == Some(nv(42)));
	assert!(j.nth(1).and_then(|x|x.as_number()) == Some(nv(56)));
	assert!(j.nth(2).is_none());
	assert!(j.array_len() == 2);
	let mut a = j.iter_array();
	assert!(a.next().and_then(|x|x.as_number()) == Some(nv(42)));
	assert!(a.next().and_then(|x|x.as_number()) == Some(nv(56)));
	assert!(a.next().is_none());
	not_null(&j);
	not_boolean(&j);
	not_number(&j);
	not_string(&j);
	not_object(&j);
}


fn map_field<'a>(x: (&'a String, &'a JsonNode2)) -> Option<(&'a str, JsonNumber)>
{
	x.1.as_number().map(|n|(x.0.deref(), n))
}

#[test]
fn json_node_2_object()
{
	let j = JsonNode2::parse("{\"x\":42,\"y\":56}").unwrap();
	assert!(j.is_object());
	assert!(j.as_dictionary().map(|x|x.len()) == Some(2));
	assert!(j.field("x").and_then(|x|x.as_number()) == Some(nv(42)));
	assert!(j.field("y").and_then(|x|x.as_number()) == Some(nv(56)));
	assert!(j.field("z").is_none());
	let mut a = j.field_names();
	assert!(a.next().map(|x|x.deref()) == Some("x"));
	assert!(a.next().map(|x|x.deref()) == Some("y"));
	assert!(a.next().is_none());
	let mut a = j.iter_object();
	assert!(a.next().and_then(map_field) == Some(("x", nv(42))));
	assert!(a.next().and_then(map_field) == Some(("y", nv(56))));
	assert!(a.next().is_none());
	not_null(&j);
	not_boolean(&j);
	not_number(&j);
	not_string(&j);
	not_array(&j);
}

#[test]
fn json_node_2_mkultra()
{
	let j = JsonNode2::parse("{\"x\":42,\"y\":56}").unwrap();
	let mut k = BTreeMap::new();
	k.insert("x".to_string(), JsonNode2::parse("42").unwrap());
	k.insert("y".to_string(), JsonNode2::parse("56").unwrap());
	let k = JsonNode2::make_object(k);
	assert!(j == k);
}

#[test]
fn json_node_2_equalities()
{
	let a = [
		JsonNode2::parse("null").unwrap(),
		JsonNode2::parse("false").unwrap(),
		JsonNode2::parse("true").unwrap(),
		JsonNode2::parse("42").unwrap(),
		JsonNode2::parse("-42").unwrap(),
		JsonNode2::parse("\"foo\"").unwrap(),
		JsonNode2::parse("\"bar\"").unwrap(),
		JsonNode2::parse("[42,56]").unwrap(),
		JsonNode2::parse("[42,57]").unwrap(),
		JsonNode2::parse("[43,57]").unwrap(),
		JsonNode2::parse("[42,56,1]").unwrap(),
		JsonNode2::parse("{\"x\":42,\"y\":56}").unwrap(),
		JsonNode2::parse("{\"x\":42,\"z\":56}").unwrap(),
		JsonNode2::parse("{\"x\":42,\"y\":57}").unwrap(),
		JsonNode2::parse("{\"x\":43,\"y\":56}").unwrap(),
		JsonNode2::parse("{\"u\":42,\"y\":56}").unwrap(),
		JsonNode2::parse("{\"x\":42,\"y\":56,\"z\":1}").unwrap(),
	];
	//All are different, with reflexive equality.
	for i in 0..a.len() { for j in 0..a.len() {
		assert!((a[i] == a[j]) == (i == j));
	}}
}

#[test]
fn cbor_from_integer_edge_cases()
{
	assert_eq!(CborNode::from_integer(-9223372036854775808), CborNode::NegInteger(9223372036854775807));
	assert_eq!(CborNode::from_integer(-9223372036854775807), CborNode::NegInteger(9223372036854775806));
	assert_eq!(CborNode::from_integer(9223372036854775806), CborNode::Integer(9223372036854775806));
	assert_eq!(CborNode::from_integer(9223372036854775807), CborNode::Integer(9223372036854775807));
}

#[test]
fn cbor_as_integer_edge_cases()
{
	assert_eq!(None, CborNode::NegInteger(9223372036854775808).as_integer());
	assert_eq!(Some(-9223372036854775808), CborNode::NegInteger(9223372036854775807).as_integer());
	assert_eq!(Some(-9223372036854775807), CborNode::NegInteger(9223372036854775806).as_integer());
	assert_eq!(Some(9223372036854775806), CborNode::Integer(9223372036854775806).as_integer());
	assert_eq!(Some(9223372036854775807), CborNode::Integer(9223372036854775807).as_integer());
	assert_eq!(None, CborNode::Integer(9223372036854775808).as_integer());
}
