//!JSON and CBOR encoder/decoder
//!
//!This crate implements a JSON and CBOR encoders and decoders.
#![deprecated(since="1.3.4", note="Use btls-aux-json2 or btls-aux-cbor instead")]
#![forbid(unsafe_code)]
#![deny(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]

#[cfg(test)] #[macro_use] extern crate std;
use btls_aux_collections::Box;
use btls_aux_collections::BTreeMap;
use btls_aux_collections::String;
use btls_aux_collections::ToOwned;
use btls_aux_collections::Vec;
use btls_aux_fail::dtry;
use core::cmp::Ord;
use core::cmp::Ordering;
use core::cmp::PartialEq;
use core::cmp::PartialOrd;
use core::convert::TryInto;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::ops::Deref;


///A JSON number.
///
///The supported operations are intentionally limited. The reason for this class is that floating-point operations
///are inherently inaccurate, and to actually have guarantees on accuracy, one must keep numbers as integers.
#[derive(Copy,Clone)]
pub struct JsonNumber(btls_aux_json2::Number);

impl JsonNumber
{
	///Get unsigned integer value of JSON number.
	///
	///If the number is integer in range of `u64`, returns `Some(v)`, where `v` is the value, with guaranteed
	///zero error. Otherwise return `none`.
	pub fn unsigned(&self) -> Option<u64> { self.0.unsigned() }
	///Get signed integer value of JSON number.
	///
	///If the number is integer in range of `i64`, returns `Some(v)`, where `v` is the value, with guaranteed
	///zero error. Otherwise return `none`.
	pub fn signed(&self) -> Option<i64> { self.0.signed() }
	///Get approximate float value of JSON number.
	///
	///There are no guarantees on accuracy of the approximation.
	pub fn float(&self) -> f64 { self.0.float() }
	fn parse(s: btls_aux_json2::Number) -> JsonNumber
	{
		JsonNumber(s)
	}
}

macro_rules! impl_fmt
{
	($fmt:ident) => {
		impl core::fmt::$fmt for JsonNumber
		{
			fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
			{
				core::fmt::$fmt::fmt(&self.0, f)
			}
		}
	};
}

impl_fmt!(Binary);
impl_fmt!(Debug);
impl_fmt!(Display);
impl_fmt!(LowerHex);
impl_fmt!(Octal);
impl_fmt!(UpperHex);
impl_fmt!(LowerExp);
impl_fmt!(UpperExp);


impl PartialEq<JsonNumber> for JsonNumber
{
	fn eq(&self, n: &JsonNumber) -> bool { self.0 == n.0 }
}

impl PartialEq<i64> for JsonNumber
{
	fn eq(&self, n: &i64) -> bool { self.0 == *n }
}

impl PartialEq<u64> for JsonNumber
{
	fn eq(&self, n: &u64) -> bool { self.0 == *n }
}

///A JSON value node.
///
///This is a single JSON value, that is, one of:
///
/// * `Null`
/// * Boolean
/// * Number (integer or double-precision floating point)
/// * Text string
/// * Array
/// * Dictionary/Object/Map
///
///This structure can be created by the [`parse()`](#method.parse) method.
///
///There are number of `as_*()` methods for casting the value node into value of given type.
#[derive(Clone,Debug)]
pub struct JsonNode2(_JsonNode2);
#[derive(Clone,Debug)] enum _JsonNode2
{
	///The null value (the only value of null type).
	Null,
	///Number (integer or float).
	Number(JsonNumber),
	///UTF-8 Text string
	///
	///Note that the string must be valid `UTF-8`.
	String(String),
	///Boolean
	Boolean(bool),
	///Array.
	///
	///The argument vector stores all the values in JSON array, in order, without gaps.
	Array(Vec<JsonNode2>),
	///Dictionary (map or object)
	///
	///The argument map stores mapping from all the keys in object to all the values in the object, with no
	///extra or missing entries. Note that JSON requires keys to be strings, and strings can only constist of
	///`UTF-8` codepoints, so the keys must be valid `UTF-8` strings.
	Dictionary(BTreeMap<String, JsonNode2>),
}


struct OptionEmptyIterator<T,U:Iterator<Item=T>>(Option<U>);

impl<T,U:Iterator<Item=T>> Iterator for OptionEmptyIterator<T,U>
{
	type Item = T;
	fn next(&mut self) -> Option<T> { self.0.as_mut().and_then(|i|i.next()) }
}

impl JsonNode2
{
	///Parse a text string `data` as JSON.
	///
	///If the text has `"UTF-8 BOM"` (U+0xFEFF) as its first codepoint, that codepoint is skipped.
	///
	///This routine can not parse strings that are:
	///
	/// * Not valid JSON.
	/// * Have too deep nesting (the maximum depth limit is currently 500).
	/// * Have surrogate escape present and not properly paired with another surrogate escape.
	///
	///On success, returns `Ok(value)`, where `value` is the decoded JSON value at root level. Otherwise
	///returns `Err(())`.
	pub fn parse(data: &str) -> Result<JsonNode2, ()>
	{
		let data = dtry!(btls_aux_json2::JSON::parse(data));
		Ok(Self::_parse(data))
	}
	///Is NULL?
	pub fn is_null(&self) -> bool { matches!(&self.0, &_JsonNode2::Null) }
	///Is Boolean?
	pub fn is_boolean(&self) -> bool { matches!(&self.0, &_JsonNode2::Boolean(_)) }
	///Is Number?
	pub fn is_number(&self) -> bool { matches!(&self.0, &_JsonNode2::Number(_)) }
	///Is String?
	pub fn is_string(&self) -> bool { matches!(&self.0, &_JsonNode2::String(_)) }
	///Is Array?
	pub fn is_array(&self) -> bool { matches!(&self.0, &_JsonNode2::Array(_)) }
	///Is Object?
	pub fn is_object(&self) -> bool { matches!(&self.0, &_JsonNode2::Dictionary(_)) }
	///Cast JSON node into null.
	///
	///If the node is null, returns `Some(())`. Otherwise returns `None`.
	pub fn as_null<'a>(&'a self) -> Option<()>
	{
		if let &_JsonNode2::Null = &self.0 { Some(()) } else { None }
	}
	///Cast JSON node into string.
	///
	///If the node is a string, returns `Some(s)` where `s` is the string. Otherwise returns
	///`None`.
	pub fn as_string<'a>(&'a self) -> Option<&'a str>
	{
		if let &_JsonNode2::String(ref x) = &self.0 { Some(x.deref()) } else { None }
	}
	///Cast JSON node into boolean.
	///
	///If the node is a boolean, returns `Some(b)` where `b` is the boolean. Otherwise returns
	///`None`.
	pub fn as_boolean<'a>(&'a self) -> Option<bool>
	{
		if let &_JsonNode2::Boolean(ref x) = &self.0 { Some(*x) } else { None }
	}
	///Cast JSON node into array.
	///
	///If the node is an array, returns `Some(a)` where `a` is the array. Otherwise returns
	///`None`.
	pub fn as_array<'a>(&'a self) -> Option<&'a [JsonNode2]>
	{
		if let &_JsonNode2::Array(ref x) = &self.0 { Some(&x[..]) } else { None }
	}
	///Cast JSON node into dictionary (object, map).
	///
	///If the node is a dictionary, returns `Some(d)` where `d` is the dictionary. Otherwise
	///returns `None`.
	pub fn as_dictionary<'a>(&'a self) -> Option<&'a BTreeMap<String, JsonNode2>>
	{
		if let &_JsonNode2::Dictionary(ref x) = &self.0 { Some(x) } else { None }
	}
	///Cast JSON node into number.
	///
	///If the node is a number, returns `Some(n)` where `n` is the number. Otherwise returns `None`.
	pub fn as_number<'a>(&'a self) -> Option<JsonNumber>
	{
		if let &_JsonNode2::Number(ref x) = &self.0 { Some(*x) } else { None }
	}
	///Get nth index of array.
	///
	///If index is valid, returns `Some(v)`, where `v` is the value. Otherwise returns `None`.
	pub fn nth<'a>(&'a self, n: usize) -> Option<&'a JsonNode2>
	{
		self.as_array().and_then(|a|a.get(n))
	}
	///Get length of array.
	///
	///If called on non-array, returns 0.
	pub fn array_len(&self) -> usize { self.as_array().map(|a|a.len()).unwrap_or(0) }
	///Iterate over array.
	///
	///If called on non-array, returns empty iterator.
	pub fn iter_array<'a>(&'a self) -> impl Iterator<Item=&'a JsonNode2>
	{
		OptionEmptyIterator(self.as_array().map(|a|a.iter()))
	}
	///Get object field with specified name.
	///
	///If field name is valid, returns `Some(v)`, where `v` is the value. Otherwise returns `None`.
	pub fn field<'a>(&'a self, name: &str) -> Option<&'a JsonNode2>
	{
		self.as_dictionary().and_then(|a|a.get(name))
	}
	///Return iterator over all field names.
	///
	///If called on non-dictionary, returns empty iterator.
	pub fn field_names<'a>(&'a self) -> impl Iterator<Item=&'a String>
	{
		OptionEmptyIterator(self.as_dictionary().map(|d|d.keys()))
	}
	///Iterate over object.
	///
	///If called on non-object, returns empty iterator.
	pub fn iter_object<'a>(&'a self) -> impl Iterator<Item=(&'a String, &'a JsonNode2)>
	{
		OptionEmptyIterator(self.as_dictionary().map(|a|a.iter()))
	}
	///Return NULL JSON node.
	pub fn null() -> JsonNode2 { JsonNode2(_JsonNode2::Null) }
	///Wrap a dictionary into JSON object.
	pub fn make_object(fields: BTreeMap<String, JsonNode2>) -> JsonNode2
	{
		JsonNode2(_JsonNode2::Dictionary(fields))
	}
	fn _parse<'a>(x: btls_aux_json2::JSON) -> JsonNode2
	{
		match x {
			btls_aux_json2::JSON::Null => JsonNode2(_JsonNode2::Null),
			btls_aux_json2::JSON::Boolean(b) => JsonNode2(_JsonNode2::Boolean(b)),
			btls_aux_json2::JSON::Number(n) => JsonNode2(_JsonNode2::Number(JsonNumber::parse(n))),
			btls_aux_json2::JSON::String(s) => JsonNode2(_JsonNode2::String(s)),
			btls_aux_json2::JSON::Array(mut x) => {
				let mut y = Vec::new();
				for z in x.drain(..) { y.push(Self::_parse(z)); }
				JsonNode2(_JsonNode2::Array(y))
			},
			btls_aux_json2::JSON::Object(mut x) => {
				let mut y = BTreeMap::new();
				//Why the fuck there is no method to do this without excess copies???
				while let Some((k,_)) = x.iter().next() {
					let k: String = k.clone();
					if let Some(v) = x.remove(&k) { y.insert(k, Self::_parse(v)); }
				}
				JsonNode2(_JsonNode2::Dictionary(y))
			},
		}
	}
}

impl<'a> PartialEq<&'a str> for JsonNode2
{
	fn eq(&self, other: &&'a str) -> bool
	{
		if let &_JsonNode2::String(ref x) = &self.0 { x == *other } else { false }
	}
}

impl PartialEq<str> for JsonNode2
{
	fn eq(&self, other: &str) -> bool
	{
		if let &_JsonNode2::String(ref x) = &self.0 { x == other } else { false }
	}
}

impl PartialEq<bool> for JsonNode2
{
	fn eq(&self, other: &bool) -> bool
	{
		if let &_JsonNode2::Boolean(x) = &self.0 { x == *other } else { false }
	}
}

impl PartialEq for JsonNode2
{
	fn eq(&self, other: &Self) -> bool
	{
		match (&self.0, &other.0) {
			(&_JsonNode2::Null, &_JsonNode2::Null) => true,
			(&_JsonNode2::Boolean(x), &_JsonNode2::Boolean(y)) => x == y,
			(&_JsonNode2::Number(x), &_JsonNode2::Number(y)) => x == y,
			(&_JsonNode2::String(ref x), &_JsonNode2::String(ref y)) => x == y,
			(&_JsonNode2::Array(ref x), &_JsonNode2::Array(ref y)) => {
				if x.len() != y.len() { return false; }
				for (i, j) in x.iter().zip(y.iter()) { if i != j { return false; } }
				true
			},
			(&_JsonNode2::Dictionary(ref x), &_JsonNode2::Dictionary(ref y)) => {
				if x.len() != y.len() { return false; }
				for ((k1, v1), (k2, v2)) in x.iter().zip(y.iter()) {
					if k1 != k2 { return false; }
					if v1 != v2 { return false; }
				}
				true
			},
			(_, _) => false
		}
	}
}

impl Eq for JsonNode2 {}


impl PartialEq<i64> for JsonNode2
{
	fn eq(&self, other: &i64) -> bool
	{
		if let &_JsonNode2::Number(x) = &self.0 { x == *other } else { false }
	}
}

impl PartialEq<u64> for JsonNode2
{
	fn eq(&self, other: &u64) -> bool
	{
		if let &_JsonNode2::Number(x) = &self.0 { x == *other } else { false }
	}
}

///Encode input using base64url encoding.
///
///The input to encode is `input`.
///
///Note that there will be no extraneous output, including padding and whitespace.
pub fn base64url(input: &[u8]) -> String { btls_aux_json2::base64url(input) }

///A JSON writer
///
///This enumeration is JSON node writer. It can write a subtree of JSON values into a string. Alternatively, it
///implements `Display` for streaming encoding. The reason this is separate from `JsonNode2` is that floating-point
///numbers are inherently inaccurate, so to have guarantees, one needs to ensure floating-point numbers are never
///encoded.
#[derive(Clone,Debug)]
#[non_exhaustive]
pub enum JsonWriter
{
	///NULL value.
	Null,
	///Number (integer).
	///
	///Note that only integers are supported, because floating-point numbers can't be properly canonicalized.
	Number(i64),
	///UTF-8 Text string
	///
	///Note that only valid UTF-8 strings are supported as strings.
	String(String),
	///Boolean
	Boolean(bool),
	///Array.
	///
	///The argument vector stores all the values in JSON array, in order, without gaps.
	Array(Vec<JsonWriter>),
	///Dictionary (map, object)
	///
	///The argument map stores mapping from all the keys in object to all the values in the object, with no
	///extra or missing entries. Note that only valid UTF-8 strings are supported as keys.
	Dictionary(BTreeMap<String, JsonWriter>),
}

trait JsonWriterTarget
{
	type Error: Sized;
	fn write(&mut self, frag: &str) -> Result<(), <Self as JsonWriterTarget>::Error>;
}

#[doc(hidden)]
pub enum Void {}

impl JsonWriterTarget for String
{
	type Error = Void;	//No error can ever happen.
	fn write(&mut self, frag: &str) -> Result<(), Void> { Ok(self.push_str(frag)) }
}

impl<'a> JsonWriterTarget for Formatter<'a>
{
	type Error = FmtError;
	fn write(&mut self, frag: &str) -> Result<(), FmtError> { self.write_str(frag) }
}

impl JsonWriter
{
	///Serialize a value into a UTF-8 text string.
	pub fn serialize(&self) -> String
	{
		let mut s = String::new();
		let json = self.__convert();
		json.serialize(&mut s).ok();	//No, this can not fail.
		s
	}
	fn __convert(&self) -> btls_aux_json2::JSON
	{
		match self {
			&JsonWriter::Null => btls_aux_json2::JSON::Null,
			&JsonWriter::Boolean(b) => btls_aux_json2::JSON::Boolean(b),
			&JsonWriter::Number(n) =>
				btls_aux_json2::JSON::Number(btls_aux_json2::Number::new_signed(n)),
			&JsonWriter::String(ref s) => btls_aux_json2::JSON::String(s.clone()),
			&JsonWriter::Array(ref x) => {
				let mut y = Vec::new();
				for z in x.iter() { y.push(Self::__convert(z)); }
				btls_aux_json2::JSON::Array(y)
			},
			&JsonWriter::Dictionary(ref x) => {
				let mut y = BTreeMap::new();
				for (k,v) in x.iter() { y.insert(k.clone(), Self::__convert(v)); }
				btls_aux_json2::JSON::Object(y)
			}
		}
	}
	///Serialize a value into a octet string.
	///
	///Note that this is the same as calling `.into_bytes()` on the `.serialize()` return value.
	pub fn serialize_bytes(&self) -> Vec<u8> { self.serialize().into_bytes() }
	///Create value from.
	///
	///The source type to node type mappings are:
	///
	/// * `()` -> `Null`.
	/// * `bool` -> `Boolean`.
	/// * `i64` -> `Number`.
	/// * `String` -> `String`.
	/// * `&str` -> `String`.
	/// * `&String` -> `String`.
	/// * `&[u8]` -> `String` (base64url encoded).
	/// * `&[JsonWriter]` -> `Array`.
	/// * `Vec<JsonWriter>` -> `Array`.
	/// * `&[(&str, JsonWriter)]` -> `Object`.
	/// * `&[(String, JsonWriter)]` -> `Object`.
	pub fn from(v: impl ToJsonWriter) -> JsonWriter { v.to_json() }
}

impl Display for JsonWriter
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		let json = self.__convert();
		json.serialize(f)
	}
}

///Turn something into JSON node.
pub trait ToJsonWriter
{
	///Construct JSON from this value.
	fn to_json(self) -> JsonWriter;
}

impl ToJsonWriter for JsonWriter
{
	fn to_json(self) -> JsonWriter { self }
}

impl ToJsonWriter for ()
{
	fn to_json(self) -> JsonWriter { JsonWriter::Null }
}

impl ToJsonWriter for bool
{
	fn to_json(self) -> JsonWriter { JsonWriter::Boolean(self) }
}

impl ToJsonWriter for i64
{
	fn to_json(self) -> JsonWriter { JsonWriter::Number(self) }
}

impl ToJsonWriter for String
{
	fn to_json(self) -> JsonWriter { JsonWriter::String(self) }
}

impl<'a> ToJsonWriter for &'a String
{
	fn to_json(self) -> JsonWriter { JsonWriter::String(self.clone()) }
}

impl<'a> ToJsonWriter for &'a str
{
	fn to_json(self) -> JsonWriter { JsonWriter::String(self.to_owned()) }
}

impl<'a> ToJsonWriter for &'a [u8]
{
	fn to_json(self) -> JsonWriter { JsonWriter::String(base64url(self)) }
}

impl<'a> ToJsonWriter for &'a [JsonWriter]
{
	fn to_json(self) -> JsonWriter { JsonWriter::Array(self.to_owned()) }
}

impl<'a> ToJsonWriter for Vec<JsonWriter>
{
	fn to_json(self) -> JsonWriter { JsonWriter::Array(self) }
}

impl<'a> ToJsonWriter for &'a [(String, JsonWriter)]
{
	fn to_json(self) -> JsonWriter
	{
		JsonWriter::Dictionary(self.iter().cloned().collect())
	}
}

impl<'a> ToJsonWriter for &'a [(&'a str, JsonWriter)]
{
	fn to_json(self) -> JsonWriter
	{
		JsonWriter::Dictionary(self.iter().map(|(k,v)|((*k).to_owned(), v.clone())).collect())
	}
}

///Generate JSON object.
#[macro_export]
macro_rules! json_object
{
	($($key:ident => $value:expr),*) => {
		$crate::JsonWriter::from(&[$((stringify!($key), $crate::JsonWriter::from($value))),*][..])
	}
}

///Generate JSON array.
#[macro_export]
macro_rules! json_array
{
	($($value:expr),*) => {
		$crate::JsonWriter::from(&[$($crate::JsonWriter::from($value)),*][..])
	}
}

///Generate JSON primitive value.
#[macro_export]
macro_rules! json_value
{
	($value:expr) => {
		$crate::JsonWriter::from($value)
	}
}

macro_rules! b
{
	($b:tt $sym:expr) => { ($sym >> 8*$b) as u8 }
}

macro_rules! wcborn
{
	(0 $tag:expr, $n:expr) => { [32 * $tag + $n as u8] };
	(1 $tag:expr, $n:expr) => { [32 * $tag + 24, b!(0 $n)] };
	(2 $tag:expr, $n:expr) => { [32 * $tag + 25, b!(1 $n), b!(0 $n)] };
	(4 $tag:expr, $n:expr) => { [32 * $tag + 26, b!(3 $n), b!(2 $n), b!(1 $n), b!(0 $n)] };
	(8 $tag:expr, $n:expr) => {
		[32 * $tag + 27, b!(7 $n), b!(6 $n), b!(5 $n), b!(4 $n), b!(3 $n), b!(2 $n), b!(1 $n), b!(0 $n)]
	};
}

///Trait: Target for writing bytes.
pub trait BytesWriterTarget
{
	///Type of error from write.
	type Error: Sized;
	///Write bytes to target.
	fn write(&mut self, frag: &[u8]) -> Result<(), <Self as BytesWriterTarget>::Error>;
	#[doc(hidden)]
	fn write_number(&mut self, major: u8, num: u64) -> Result<(), <Self as BytesWriterTarget>::Error>
	{
		if num < 24 {
			self.write(&wcborn!(0 major, num))
		} else if num < 256 {
			self.write(&wcborn!(1 major, num))
		} else if num < 65536 {
			self.write(&wcborn!(2 major, num))
		} else if num < 4294967296 {
			self.write(&wcborn!(4 major, num))
		} else {
			self.write(&wcborn!(8 major, num))
		}
	}
}

impl BytesWriterTarget for Vec<u8>
{
	type Error = Void;
	fn write(&mut self, frag: &[u8]) -> Result<(), Void> { Ok(self.extend_from_slice(frag)) }
}

///A CBOR value node.
///
///This is a single CBOR value, that is, one of:
///
/// * Integer
/// * Negative integer
/// * Octet string (not in JSON)
/// * Text string
/// * Array
/// * Dictionary
/// * Tag (not in JSON)
/// * `Null`
/// * `Undefined` (not in JSON)
/// * Boolean
/// * Simple item (not in JSON)
/// * Half-precision floating point (not in JSON)
/// * Single-precision floating point (not in JSON)
/// * Double-precision floating point
///
///Through CBOR is mostly compatible with JSON, the CBOR has data types not present in JSON.
///
///Note that the `Ordering` implemented by this enumeration is undefined and does not equal the canonical CBOR
///ordering. However, the ordering is total and self-consistent, so `BTreeMap` is happy to have CBOR values as
///keys.
///
///Also unlike the case with JSON, CborNode can be directly serialized. This is because CBOR uses binary
///representation for floating-point numbers, separate from integers.
///
///This structure can be created by the [`parse()`](#method.parse) method or by manual construction.
///
///There are number of `as_*()` methods for casting the value node into value of given type.
#[derive(Clone,Debug)]
#[non_exhaustive]
pub enum CborNode
{
	///Nonnegative integer.
	Integer(u64),
	///Negative integer.
	///
	///Note that there is bias of `1`. That is, 0 represents value of -1, and 2^64-1 represents vale of -2^64.
	NegInteger(u64),
	///Octet string.
	///
	///Note that this type is 8-bit clean, it can represent arbitrary octet string. JSON has no native octet
	///string type, in JSON, octet strings are commonly represented as base64url encoding of the octet string,
	///encoded as a text string.
	Octets(Vec<u8>),
	///`UTF-8` string.
	///
	///Note that CBOR requires text strings to be valid UTF-8, so arbitrary valid text string can be represented.
	String(String),
	///Array
	///
	///The argument vector stores all the values in CBOR array, in order, without gaps.
	Array(Vec<CborNode>),
	///Dictionary (map).
	///
	///The argument map stores mapping from all the keys in object to all the values in the object, with no
	///extra or missing entries. Note that arbitrary CBOR values are supported as keys, including arrays or
	///other dictionaries. Note that JSON does not support keys that are not text strings.
	Dictionary(BTreeMap<CborNode, CborNode>),
	///A tagged value.
	///
	///The CBOR tag type. The arguments are `(tagnumber, taggee)`, where `tagnumber` is the tag number and
	///`taggee` is the object being tagged.
	///
	///JSON does not have equivalent data type.
	Tag(u64, Box<CborNode>),
	///Simple value
	///
	///The simple value number must either be between 0 and 23, inclusive or between 32 and 255 inclusive.
	///Values 24-31 are **not** allowed.
	///
	///The values, `false` and `true`, `null` and `undefined` (this is not in JSON) are represeted as simple
	///values, numbers 20, 21, 22 and 23 (constatnts [`CBOR_FALSE`], [`CBOR_TRUE`], [`CBOR_NULL`] and
	///[`CBOR_UNDEFINED`]), respectively.
	///
	///[`CBOR_FALSE`]: constant.CBOR_FALSE.html
	///[`CBOR_TRUE`]: constant.CBOR_TRUE.html
	///[`CBOR_NULL`]: constant.CBOR_NULL.html
	///[`CBOR_UNDEFINED`]: constant.CBOR_UNDEFINED.html
	Simple(u8),
	///Half-precision (16 bit) floating-point value.
	///
	///The argument is the raw encoding of the floating-point number, without considering endianess.
	///
	///In JSON, this corresponds to subset of the number type.
	HalfFloat(u16),
	///Single-precision (32 bit) floating-point value.
	///
	///The argument is the raw encoding of the floating-point number, without considering endianess.
	///
	///In JSON, this corresponds to subset of the number type.
	Float(u32),
	///Double-precision (64 bit) floating-point value.
	///
	///The argument is the raw encoding of the floating-point number, without considering endianess.
	///
	///In JSON, this corresponds to the de-facto number type. In JSON, number type can be assumed to have
	///the same range and accuracy as double-precision floating point type.
	Double(u64),
}

///The CBOR `FALSE` simple value.
pub const CBOR_FALSE: CborNode = CborNode::Simple(20);
///The CBOR `TRUE` simple value.
pub const CBOR_TRUE: CborNode = CborNode::Simple(21);
///The CBOR `NULL` simple value.
pub const CBOR_NULL: CborNode = CborNode::Simple(22);
///The CBOR `UNDEFINED` simple value.
///
///There is no corresponding value in JSON.
pub const CBOR_UNDEFINED: CborNode = CborNode::Simple(23);

impl CborNode
{
	///Is NULL?
	pub fn is_null(&self) -> bool { matches!(self, &CborNode::Simple(22)) }
	///Is Undefined?
	pub fn is_undefined(&self) -> bool { matches!(self, &CborNode::Simple(23)) }
	///Is Boolean?
	pub fn is_boolean(&self) -> bool { matches!(self, &CborNode::Simple(20)|&CborNode::Simple(21)) }
	///Is Integer?
	pub fn is_integer(&self) -> bool { matches!(self, &CborNode::Integer(_)|&CborNode::NegInteger(_)) }
	///Is Octets?
	pub fn is_octets(&self) -> bool { matches!(self, &CborNode::Octets(_)) }
	///Is String?
	pub fn is_string(&self) -> bool { matches!(self, &CborNode::String(_)) }
	///Is Array?
	pub fn is_array(&self) -> bool { matches!(self, &CborNode::Array(_)) }
	///Is Dictionary?
	pub fn is_dictionary(&self) -> bool { matches!(self, &CborNode::Dictionary(_)) }
	///Is Tag?
	pub fn is_tag(&self) -> bool { matches!(self, &CborNode::Tag(_, _)) }
	///Is Simple?
	pub fn is_simple(&self) -> bool { matches!(self, &CborNode::Simple(_)) }
	///Is float?
	pub fn is_float(&self) -> bool
	{
		matches!(self, &CborNode::HalfFloat(_)|&CborNode::Float(_)|&CborNode::Double(_))
	}
	///Construct CBOR node corresponding to an integer `x`.
	pub fn from_integer(x: i64) -> CborNode
	{
		if x < 0 { CborNode::NegInteger((-(x + 1)) as u64) } else { CborNode::Integer(x as u64) }
	}
	///Cast CBOR node into non-negative integer.
	///
	///Note that floating-point values are **not** converted into integers, even if value is an exact integer.
	///
	///If the node is an non-negative integer, returns `Some(i)` where `i` is the integer. Otherwise returns
	///`None`.
	pub fn as_unsigned<'a>(&'a self) -> Option<u64>
	{
		if let &CborNode::Integer(x) = self { Some(x) } else { None }
	}
	///Cast CBOR node into integer.
	///
	///Note that floating-point values are **not** converted into integers, even if value is an exact integer.
	///
	///If the node is an integer in range representable by `i64`, returns `Some(i)` where `i` is the integer.
	///Otherwise returns
	pub fn as_integer<'a>(&'a self) -> Option<i64>
	{
		match self {
			&CborNode::Integer(x) => x.try_into().ok(),
			&CborNode::NegInteger(x) => x.try_into().ok().map(|n:i64|-n-1),
			_ => None
		}
	}
	///Cast CBOR node into negative integer.
	///
	///Note that floating-point values are **not** converted into integers, even if value is an exact integer.
	///
	///If the node is an negative integer, returns `Some(i)` where `i` is the negation of the integer plus 1
	///(e.g., `-1` is returned as `0`, `-2` is returned as `1` and so on). Otherwise returns `None`.
	pub fn as_unsigned_negative<'a>(&'a self) -> Option<u64>
	{
		if let &CborNode::NegInteger(x) = self { Some(x) } else { None }
	}
	///Cast CBOR node into octet string.
	///
	///If the node is an octet string, returns `Some(o)` where `o` is the octet string. Otherwise returns
	///`None`.
	pub fn as_octets<'a>(&'a self) -> Option<&'a [u8]>
	{
		if let &CborNode::Octets(ref x) = self { Some(&x[..]) } else { None }
	}
	///Cast CBOR node into text string.
	///
	///If the node is a text string, returns `Some(s)` where `s` is the text string. Otherwise returns
	///`None`.
	pub fn as_string<'a>(&'a self) -> Option<&'a str>
	{
		if let &CborNode::String(ref x) = self { Some(x.deref()) } else { None }
	}
	///Cast CBOR node into array.
	///
	///If the node is an array, returns `Some(a)` where `a` is the array. Otherwise returns
	///`None`.
	pub fn as_array<'a>(&'a self) -> Option<&'a [CborNode]>
	{
		if let &CborNode::Array(ref x) = self { Some(&x[..]) } else { None }
	}
	///Cast CBOR node into dictionary.
	///
	///If the node is a dictionary, returns `Some(d)` where `d` is the dictionary. Otherwise returns
	///`None`.
	pub fn as_dictionary<'a>(&'a self) -> Option<&'a BTreeMap<CborNode, CborNode>>
	{
		if let &CborNode::Dictionary(ref x) = self { Some(x) } else { None }
	}
	///Cast CBOR node into tag.
	///
	///If the node is a tag, returns `Some((tag, taggee))` where `tag` is the tag and `taggee` is the tagged
	///value. Otherwise returns `None`.
	pub fn as_tag<'a>(&'a self) -> Option<(u64, &'a CborNode)>
	{
		if let &CborNode::Tag(t, ref x) = self { Some((t, x)) } else { None }
	}
	///Cast CBOR node into simple.
	///
	///If the node is a simple value, returns `Some(v)` where `v` is the simple value. Otherwise returns
	///`None`.
	pub fn as_simple<'a>(&'a self) -> Option<u8>
	{
		if let &CborNode::Simple(ref x) = self { Some(*x) } else { None }
	}
	///Cast CBOR node into null.
	///
	///If the node is a `null`, returns `Some(())`. Otherwise returns `None`.
	pub fn as_null<'a>(&'a self) -> Option<()>
	{
		if let &CborNode::Simple(22) = self { Some(()) } else { None }
	}
	///Cast CBOR node into undefined.
	///
	///If the node is a `undefined`, returns `Some(())`. Otherwise returns `None`.
	pub fn as_undefined<'a>(&'a self) -> Option<()>
	{
		if let &CborNode::Simple(23) = self { Some(()) } else { None }
	}
	///Cast CBOR node into boolean.
	///
	///If the node is a boolean, returns `Some(b)` where `b` is the boolean. Otherwise returns
	///`None`.
	pub fn as_boolean<'a>(&'a self) -> Option<bool>
	{
		match self {
			&CborNode::Simple(20) => Some(false),
			&CborNode::Simple(21) => Some(true),
			_ => None
		}
	}
	///Cast CBOR node into half-float.
	///
	///Note that single-float and double-float values are **not** demoted, even if such demotion would be exact.
	///Also integers are **not** promoted.
	///
	///If the node is a half-float, returns `Some(f)` where `f` is the float raw value. Otherwise returns
	///`None`.
	pub fn as_half_float<'a>(&'a self) -> Option<u16>
	{
		if let &CborNode::HalfFloat(ref x) = self { Some(*x) } else { None }
	}
	///Cast CBOR node into single-float.
	///
	///Note that double-float values are **not** demoted, even if such demotion would be exact.
	///Also integers and half-floats are **not** promoted.
	///
	///If the node is a single-float, returns `Some(f)` where `f` is the float raw value. Otherwise returns
	///`None`.
	pub fn as_single_float<'a>(&'a self) -> Option<u32>
	{
		if let &CborNode::Float(ref x) = self { Some(*x) } else { None }
	}
	///Cast CBOR node into double-float.
	///
	///Note that integers, half-floats and single-floats are **not** promoted.
	///
	///If the node is a double-float, returns `Some(f)` where `f` is the float raw value. Otherwise returns
	///`None`.
	pub fn as_double_float<'a>(&'a self) -> Option<u64>
	{
		if let &CborNode::Double(ref x) = self { Some(*x) } else { None }
	}
	///Cast CBOR node into double-float, promoting half-floats and single-floats.
	///
	///Note that integers are **not** promoted.
	///
	///If the node is any float, returns `Some(f)` where `f` is the float raw value. Otherwise returns
	///`None`.
	pub fn as_double_float_p<'a>(&'a self) -> Option<u64>
	{
		match self {
			&CborNode::HalfFloat(x) => btls_aux_cbor::CBOR::HalfFloat(x).as_double_float_p(),
			&CborNode::Float(x) => btls_aux_cbor::CBOR::Float(x).as_double_float_p(),
			&CborNode::Double(x) => Some(x),
			_ => None
		}
	}
	///Like `as_double_float_p()`, but bitcasts the value, so return value is actually `f64`.
	///
	///Note: The returned value may be zero, infinity (both plus or minus) or NaN.
	pub fn as_double_float_v<'a>(&'a self) -> Option<f64> { self.as_double_float_p().map(|x|f64::from_bits(x)) }
	///Get nth index of array.
	///
	///If index is valid, returns `Some(v)`, where `v` is the value. Otherwise returns `None`.
	pub fn nth<'a>(&'a self, n: usize) -> Option<&'a CborNode>
	{
		self.as_array().and_then(|a|a.get(n))
	}
	///Get length of array.
	///
	///If called on non-array, returns 0.
	pub fn array_len(&self) -> usize { self.as_array().map(|a|a.len()).unwrap_or(0) }
	///Iterate over array.
	///
	///If called on non-array, returns empty iterator.
	pub fn iter_array<'a>(&'a self) -> impl Iterator<Item=&'a CborNode>
	{
		OptionEmptyIterator(self.as_array().map(|a|a.iter()))
	}
	///Get object field with specified name.
	///
	///If field name is valid, returns `Some(v)`, where `v` is the value. Otherwise returns `None`.
	pub fn field<'a,T:ToCbor>(&'a self, name: T) -> Option<&'a CborNode>
	{
		let name = name.to_cbor();
		self.as_dictionary().and_then(|a|a.get(&name))
	}
	///Return iterator over all field names.
	///
	///If called on non-dictionary, returns empty iterator.
	pub fn field_names<'a>(&'a self) -> impl Iterator<Item=&'a CborNode>
	{
		OptionEmptyIterator(self.as_dictionary().map(|d|d.keys()))
	}
	///Iterate over object.
	///
	///If called on non-object, returns empty iterator.
	pub fn iter_object<'a>(&'a self) -> impl Iterator<Item=(&'a CborNode, &'a CborNode)>
	{
		OptionEmptyIterator(self.as_dictionary().map(|a|a.iter()))
	}
	///Parse slice `data` as CBOR structure.
	///
	///This routine will fail to parse non-CBOR data and structures with more than 500 levels of nesting.
	///
	///On success, returns `Ok(value)`, where `value` is the decoded CBOR value at root level. Otherwise
	///returns `Err(())`.
	pub fn parse(data: &[u8]) -> Result<CborNode, ()>
	{
		let c = dtry!(btls_aux_cbor::CBOR::parse(data));
		Ok(Self::__convert(c))
	}
	///Serialize the CBOR node as CBOR.
	pub fn serialize_to_vec(&self) -> Vec<u8>
	{
		let mut out = Vec::new();
		let cbor = self.__backconvert();
		cbor.serialize(&mut out).ok();		//Can not fail.
		out
	}
	///Serialize the CBOR node as CBOR.
	pub fn serialize<B:BytesWriterTarget>(&self, to: &mut B) -> Result<(), <B as BytesWriterTarget>::Error>
	{
		let cbor = self.__backconvert();
		struct Wrapper<'a,X:BytesWriterTarget>(&'a mut X);
		impl<'a,X:BytesWriterTarget> btls_aux_cbor::BytesWriterTarget for Wrapper<'a,X>
		{
			type Error = <X as BytesWriterTarget>::Error;
			fn write(&mut self, frag: &[u8]) -> Result<(), <X as BytesWriterTarget>::Error>
			{
				self.0.write(frag)
			}
		}
		cbor.serialize(&mut Wrapper(to))
	}
	///Create value from.
	///
	///The source type to node type mappings are:
	///
	/// * `bool` -> `Boolean`
	/// * `i64` -> `Integer`
	/// * `u64` -> `Integer`
	/// * `String` -> `String`
	/// * `&String` -> `String`
	/// * `&str` -> `String`
	/// * `Vec<u8>` -> `Octets`
	/// * `&Vec<u8>` -> `Octets`
	/// * `&[u8]` -> `Octets`
	/// * `&[CborNode]` -> `Array`
	/// * `Vec<CborNode>` -> `Array`
	/// * `&[(CborNode, CborNode)]` -> `Dictionary`
	/// * `&[(u64, CborNode)]` -> `Tag`
	/// * `f32` -> `SingleFloat`
	/// * `f64` -> `DoubleFloat`
	pub fn from(v: impl ToCbor) -> CborNode { v.to_cbor() }
	fn __backconvert(&self) -> btls_aux_cbor::CBOR
	{
		match self {
			&CborNode::Integer(x) => btls_aux_cbor::CBOR::Integer(x),
			&CborNode::NegInteger(x) => btls_aux_cbor::CBOR::NegInteger(x),
			&CborNode::Octets(ref x) => btls_aux_cbor::CBOR::Octets(x.clone()),
			&CborNode::String(ref x) => btls_aux_cbor::CBOR::String(x.clone()),
			&CborNode::Array(ref x) => {
				let mut y = Vec::new();
				for z in x.iter() { y.push(z.__backconvert()); }
				btls_aux_cbor::CBOR::Array(y)
			},
			&CborNode::Dictionary(ref x) => {
				let mut y = BTreeMap::new();
				for (k,v) in x.iter() {
					y.insert(k.__backconvert(), v.__backconvert());
				}
				btls_aux_cbor::CBOR::Dictionary(y)
			},
			&CborNode::Tag(x, ref y) => btls_aux_cbor::CBOR::Tag(x, Box::new(y.__backconvert())),
			&CborNode::Simple(x) => btls_aux_cbor::CBOR::Simple(x),
			&CborNode::HalfFloat(x) => btls_aux_cbor::CBOR::HalfFloat(x),
			&CborNode::Float(x) => btls_aux_cbor::CBOR::Float(x),
			&CborNode::Double(x) => btls_aux_cbor::CBOR::Double(x),
		}
	}
	fn __convert(x: btls_aux_cbor::CBOR) -> CborNode
	{
		match x {
			btls_aux_cbor::CBOR::Integer(x) => CborNode::Integer(x),
			btls_aux_cbor::CBOR::NegInteger(x) => CborNode::NegInteger(x),
			btls_aux_cbor::CBOR::Octets(x) => CborNode::Octets(x),
			btls_aux_cbor::CBOR::String(x) => CborNode::String(x),
			btls_aux_cbor::CBOR::Array(mut x) => {
				let mut y = Vec::new();
				for z in x.drain(..) { y.push(Self::__convert(z)); }
				CborNode::Array(y)
			},
			btls_aux_cbor::CBOR::Dictionary(mut x) => {
				let mut y = BTreeMap::new();
				//Why the fuck there is no method to do this without excess copies???
				while let Some((k,_)) = x.iter().next() {
					let k = k.clone();
					if let Some(v) = x.remove(&k) {
						y.insert(Self::__convert(k), Self::__convert(v));
					}
				}
				CborNode::Dictionary(y)
			},
			btls_aux_cbor::CBOR::Tag(x, y) => CborNode::Tag(x, Box::new(Self::__convert(*y))),
			btls_aux_cbor::CBOR::Simple(x) => CborNode::Simple(x),
			btls_aux_cbor::CBOR::HalfFloat(x) => CborNode::HalfFloat(x),
			btls_aux_cbor::CBOR::Float(x) => CborNode::Float(x),
			btls_aux_cbor::CBOR::Double(x) => CborNode::Double(x),
			//I hope this is never hit.
			_ => CBOR_UNDEFINED
		}
	}
	fn priority(&self) -> u8
	{
		//Each discriminant must give a different value.
		match self {
			&CborNode::Integer(_) => 0x00,
			&CborNode::NegInteger(_) => 0x20,
			&CborNode::Octets(_) => 0x40,
			&CborNode::String(_) => 0x60,
			&CborNode::Array(_) => 0x80,
			&CborNode::Dictionary(_) => 0xA0,
			&CborNode::Tag(_, _) => 0xC0,
			&CborNode::Simple(_) => 0xE0,
			&CborNode::HalfFloat(_) => 0xF9,
			&CborNode::Float(_) => 0xFA,
			&CborNode::Double(_) => 0xFB,
		}
	}
}

macro_rules! eq_chain
{
	($val:expr) => {{ let tmp = $val; if tmp.is_ne() { return tmp; } }}
}

impl Ord for CborNode
{
	fn cmp(&self, other: &Self) -> Ordering
	{
		//Implement the canonical CBOR order.
		match (self, other) {
			(&CborNode::Integer(ref x), &CborNode::Integer(ref y)) => x.cmp(y),
			(&CborNode::NegInteger(ref x), &CborNode::NegInteger(ref y)) => x.cmp(y),
			(&CborNode::Octets(ref x), &CborNode::Octets(ref y)) =>
				x.len().cmp(&y.len()).then_with(||x.cmp(y)),
			(&CborNode::String(ref x), &CborNode::String(ref y)) =>
				x.len().cmp(&y.len()).then_with(||x.cmp(y)),
			(&CborNode::Array(ref x), &CborNode::Array(ref y)) => {
				eq_chain!(x.len().cmp(&y.len()));
				for (i, j) in x.iter().zip(y.iter()) { eq_chain!(i.cmp(j)); }
				Ordering::Equal
			},
			(&CborNode::Dictionary(ref x), &CborNode::Dictionary(ref y)) => {
				eq_chain!(x.len().cmp(&y.len()));
				for ((k1, v1), (k2, v2)) in x.iter().zip(y.iter()) {
					eq_chain!(k1.cmp(k2));
					eq_chain!(v1.cmp(v2));
				}
				Ordering::Equal
			},
			(&CborNode::Tag(t1, ref x), &CborNode::Tag(t2, ref y)) => t1.cmp(&t2).then_with(||x.cmp(y)),
			(&CborNode::Simple(ref x), &CborNode::Simple(ref y)) => x.cmp(y),
			(&CborNode::HalfFloat(ref x), &CborNode::HalfFloat(ref y)) => x.cmp(y),
			(&CborNode::Float(ref x), &CborNode::Float(ref y)) => x.cmp(y),
			(&CborNode::Double(ref x), &CborNode::Double(ref y)) => x.cmp(y),
			(ref x, ref y) => x.priority().cmp(&y.priority())
		}
	}
}

impl PartialEq for CborNode
{
	fn eq(&self, other: &Self) -> bool { self.cmp(other).is_eq() }
}

impl PartialOrd for CborNode
{
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> { Some(self.cmp(other)) }
}

impl PartialEq<i64> for CborNode
{
	fn eq(&self, x: &i64) -> bool { self.as_integer() == Some(*x) }
}

impl PartialEq<u64> for CborNode
{
	fn eq(&self, x: &u64) -> bool { self.as_unsigned() == Some(*x) }
}

impl PartialEq<str> for CborNode
{
	fn eq(&self, x: &str) -> bool { self.as_string() == Some(x) }
}

impl PartialEq<[u8]> for CborNode
{
	fn eq(&self, x: &[u8]) -> bool { self.as_octets() == Some(x) }
}

impl Eq for CborNode {}

///Turn something into CBOR node.
pub trait ToCbor
{
	///Construct CBOR from this value.
	fn to_cbor(self) -> CborNode;
}

impl ToCbor for bool
{
	fn to_cbor(self) -> CborNode { if self { CBOR_TRUE } else { CBOR_FALSE } }
}

impl ToCbor for i64
{
	fn to_cbor(self) -> CborNode
	{
		if self >= 0 { CborNode::Integer(self as u64) } else { CborNode::NegInteger((!self) as u64) }
	}
}

impl ToCbor for u64
{
	fn to_cbor(self) -> CborNode { CborNode::Integer(self) }
}

impl ToCbor for String
{
	fn to_cbor(self) -> CborNode { CborNode::String(self) }
}

impl<'a> ToCbor for &'a String
{
	fn to_cbor(self) -> CborNode { CborNode::String(self.clone()) }
}

impl<'a> ToCbor for &'a str
{
	fn to_cbor(self) -> CborNode { CborNode::String(self.to_owned()) }
}

impl ToCbor for Vec<u8>
{
	fn to_cbor(self) -> CborNode { CborNode::Octets(self) }
}

impl<'a> ToCbor for &'a Vec<u8>
{
	fn to_cbor(self) -> CborNode { CborNode::Octets(self.clone()) }
}

impl<'a> ToCbor for &'a [u8]
{
	fn to_cbor(self) -> CborNode { CborNode::Octets(self.to_owned()) }
}

impl<'a> ToCbor for &'a [CborNode]
{
	fn to_cbor(self) -> CborNode { CborNode::Array(self.to_owned()) }
}

impl ToCbor for Vec<CborNode>
{
	fn to_cbor(self) -> CborNode { CborNode::Array(self) }
}

impl<'a> ToCbor for &'a Vec<CborNode>
{
	fn to_cbor(self) -> CborNode { CborNode::Array(self.clone()) }
}

impl<'a> ToCbor for &'a [(CborNode, CborNode)]
{
	fn to_cbor(self) -> CborNode { CborNode::Dictionary(self.iter().cloned().collect()) }
}

impl<'a> ToCbor for (u64, CborNode)
{
	fn to_cbor(self) -> CborNode { CborNode::Tag(self.0, Box::new(self.1)) }
}

impl<'a> ToCbor for f32
{
	fn to_cbor(self) -> CborNode { CborNode::Float(self.to_bits()) }
}

impl<'a> ToCbor for f64
{
	fn to_cbor(self) -> CborNode { CborNode::Double(self.to_bits()) }
}


#[cfg(test)]
mod test;
