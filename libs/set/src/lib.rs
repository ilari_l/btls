//!Set types
#![no_std]
//Would produce too many warnings which are too hard to silence without just disabling all deprecation warnings.
//This crate is going to be removed soon anyway.
//#![deprecated(since="1.2.1", note="Use btls-aux-set2 instead")]
#![deny(missing_docs)]
#![deny(unsafe_code)]
#![warn(unsafe_op_in_unsafe_fn)]
pub use btls_aux_set2::Downcast;
pub use btls_aux_set2::Keycast;
pub use btls_aux_set2::Upcast;
pub use core::fmt::Debug as __RustBuiltinDebug;
pub use core::fmt::Error as __RustBuiltinFmtError;
pub use core::fmt::Formatter as __RustBuiltinFormatter;
use core::marker::PhantomData;
use core::mem::replace;
pub use core::ops::BitAnd as __RustBuiltinBitAnd;
pub use core::ops::BitOr as __RustBuiltinBitOr;


///Type of set storage element.
pub trait SetStorageElementTrait
{
	///Number of bits in value.
	fn bits() -> usize;
	///Clear bit b.
	fn clear(&mut self, b: usize);
	///Set bit b.
	fn set(&mut self, b: usize);
	///Is bit b set?
	fn isset(&self, b: usize) -> bool;
	///Compute bitwise and.
	fn __bitand(&self, o: &Self) -> Self;
	///Compute bitwise or.
	fn __bitor(&self, o: &Self) -> Self;
	///Test-and-Clear bit b.
	fn test_and_clear(&mut self, b: usize) -> bool
	{
		let x = self.isset(b);
		self.clear(b);
		x
	}
	///Test-and-Set bit b.
	fn test_and_set(&mut self, b: usize) -> bool
	{
		let x = self.isset(b);
		self.set(b);
		x
	}
	///Cast to u64.
	fn __to_u64(&self) -> u64;
	///Cast from u64.
	fn __from_u64(m: u64) -> Self;
	///Compute hamming weight.
	fn __hamming_weight(&self) -> u32;
	///Is nonzero?
	fn __is_nonzero(&self) -> bool;
}

macro_rules! impl_set_storage_element_trait
{
	($xtype:ident $bits:tt) => {
		impl SetStorageElementTrait for $xtype
		{
			fn bits() -> usize { $bits }
			fn clear(&mut self, b: usize) { *self &= !(1 << b % $bits) }
			fn set(&mut self, b: usize) { *self |= 1 << b % $bits }
			fn isset(&self, b: usize) -> bool { *self & 1 << b % $bits != 0 }
			fn __bitand(&self, o: &Self) -> Self { *self & *o }
			fn __bitor(&self, o: &Self) -> Self { *self | *o }
			fn __to_u64(&self) -> u64 { *self as u64 }
			fn __from_u64(m: u64) -> Self { m as _ }
			fn __hamming_weight(&self) -> u32 { self.count_ones() }
			fn __is_nonzero(&self) -> bool { *self != 0 }
		}
	}
}

impl_set_storage_element_trait!(u8 8);
impl_set_storage_element_trait!(u16 16);
impl_set_storage_element_trait!(u32 32);
impl_set_storage_element_trait!(u64 64);
impl_set_storage_element_trait!(u128 128);

fn unwrap_ty<D:Default>(x: Option<D>) -> D { x.unwrap_or_else(Default::default) }

macro_rules! impl_sc
{
	(TRAIT_X [$(#[$comments:meta])*] $name:ident $ret:ty) => {
		$(#[$comments])* fn $name(&mut self, b: usize) -> $ret
		{
			unwrap_ty(self.__word_bits_mut(b).map(|(w,b)|w.$name(b)))
		}
	};
	(TRAIT_ISSET [$(#[$comments:meta])*] $name:ident) => {
		$(#[$comments])* fn $name(&self, b: usize) -> bool
		{
			unwrap_ty(self.__word_bits(b).map(|(w,b)|w.$name(b)))
		}
	};
	(TYPE_X $name:ident $ret:ty) => {
		fn $name(&mut self, b: usize) -> $ret { SetStorageElementTrait::$name(self, b) }
	};
	(TYPE_ISSET $name:ident) => {
		fn $name(&self, b: usize) -> bool { SetStorageElementTrait::$name(self, b) }
	};
}

///Storage of bit set.
pub trait SetStorageTrait: Sized
{
	///Type of element.
	type Element: SetStorageElementTrait;
	///Get all zeroes.
	fn __new() -> Self;
	///Get w'th element.
	fn __word<'a>(&'a self, w: usize) -> Option<&'a <Self as SetStorageTrait>::Element>;
	///Get w'th element, mutably.
	fn __word_mut<'a>(&'a mut self, w: usize) -> Option<&'a mut <Self as SetStorageTrait>::Element>;
	///Construct new value with elementwise function.
	fn __elementwise(&self, b: &Self, func: impl Fn(&<Self as SetStorageTrait>::Element,
		&<Self as SetStorageTrait>::Element) -> <Self as SetStorageTrait>::Element) -> Self;
	impl_sc!(TRAIT_X [#[doc="Clear b'th bit."]] clear ());
	impl_sc!(TRAIT_X [#[doc="Set b'th bit."]] set ());
	impl_sc!(TRAIT_X [#[doc="Test-and-Clear b'th bit."]] test_and_clear bool);
	impl_sc!(TRAIT_X [#[doc="Test-and-Set b'th bit."]] test_and_set bool);
	impl_sc!(TRAIT_ISSET [#[doc="Is the b'th bit set?"]] isset);
	///Compute bitwise and.
	fn _bitand(&self, o: &Self) -> Self { self.__elementwise(o, |a,b|a.__bitand(b)) }
	///Compute bitwise or.
	fn _bitor(&self, o: &Self) -> Self { self.__elementwise(o, |a,b|a.__bitor(b)) }
	#[doc(hidden)]
	fn __word_bits_mut(&mut self, b: usize) -> Option<(&mut <Self as SetStorageTrait>::Element, usize)>
	{
		let idx = b.checked_div(<Self as SetStorageTrait>::Element::bits())?;
		self.__word_mut(idx).map(|w|(w,b))
	}
	#[doc(hidden)]
	fn __word_bits(&self, b: usize) -> Option<(&<Self as SetStorageTrait>::Element, usize)>
	{
		let idx = b.checked_div(<Self as SetStorageTrait>::Element::bits())?;
		self.__word(idx).map(|w|(w,b))
	}
	#[doc(hidden)]
	fn __from_u64(m: u64) -> Self
	{
		let mut out = Self::__new();
		let mut s = 0;
		let mut idx = 0;
		while let Some(w) = out.__word_mut(idx) {
			if s >= 64 { break; }
			*w = <Self as SetStorageTrait>::Element::__from_u64(m >> s);
			s += <Self as SetStorageTrait>::Element::bits();
			idx += 1;
		}
		out
	}
	#[doc(hidden)]
	fn __to_u64(&self) -> u64
	{
		let mut out = 0;
		let mut s = 0;
		let mut idx = 0;
		while let Some(w) = self.__word(idx) {
			if s >= 64 { break; }
			out |= w.__to_u64() << s;
			s += <Self as SetStorageTrait>::Element::bits();
			idx += 1;
		}
		out
	}
	#[doc(hidden)]
	fn __hamming_weight(&self) -> u32
	{
		let mut out = 0;
		let mut idx = 0;
		while let Some(w) = self.__word(idx) {
			out += w.__hamming_weight();
			idx += 1;
		}
		out
	}
	#[doc(hidden)]
	fn __is_nonzero(&self) -> bool
	{
		let mut idx = 0;
		while let Some(w) = self.__word(idx) {
			if w.__is_nonzero() { return true; }
			idx += 1;
		}
		false
	}
}

macro_rules! impl_set_storage_trait_primitive
{
	($xtype:ident) => {
		impl SetStorageTrait for $xtype
		{
			type Element = $xtype;
			fn __new() -> Self { 0 }
			fn __word<'a>(&'a self, w: usize) -> Option<&'a $xtype>
			{
				if w == 0 { Some(self) } else { None }
			}
			fn __word_mut<'a>(&'a mut self, w: usize) -> Option<&'a mut $xtype>
			{
				if w == 0 { Some(self) } else { None }
			}
			fn __elementwise(&self, b: &Self, func: impl Fn(&$xtype, &$xtype) -> $xtype) -> Self
			{
				func(self, b)
			}
			//A bit more optimized implementations.
			impl_sc!(TYPE_X clear ());
			impl_sc!(TYPE_X set ());
			impl_sc!(TYPE_X test_and_clear bool);
			impl_sc!(TYPE_X test_and_set bool);
			impl_sc!(TYPE_ISSET isset);
			fn __from_u64(m: u64) -> Self { <$xtype as SetStorageElementTrait>::__from_u64(m) }
			fn __to_u64(&self) -> u64 { SetStorageElementTrait::__to_u64(self)  }
			fn __hamming_weight(&self) -> u32 { SetStorageElementTrait::__hamming_weight(self) }
			fn __is_nonzero(&self) -> bool { SetStorageElementTrait::__is_nonzero(self) }
		}
	}
}

macro_rules! impl_set_storage_trait_array
{
	($xtype:ident) => {
		impl<const N: usize> SetStorageTrait for [$xtype;N]
		{
			type Element = $xtype;
			fn __new() -> Self { [0;N] }
			fn __word<'a>(&'a self, w: usize) -> Option<&'a $xtype> { self.get(w) }
			fn __word_mut<'a>(&'a mut self, w: usize) -> Option<&'a mut $xtype> { self.get_mut(w) }
			fn __elementwise(&self, b: &Self, func: impl Fn(&$xtype, &$xtype) -> $xtype) -> Self
			{
				let mut r = [0;N];
				for (r, (a, b)) in r.iter_mut().zip(self.iter().zip(b.iter())) { *r = func(a, b); }
				r
			}
			fn __hamming_weight(&self) -> u32
			{
				self.iter().fold(0,|acc,element|{
					acc + SetStorageElementTrait::__hamming_weight(element)
				})
			}
			fn __is_nonzero(&self) -> bool
			{
				for element in self.iter() {
					if SetStorageElementTrait::__is_nonzero(element) { return true; }
				}
				false
			}
		}
	};
}

///Clear bit b in raw set.
pub fn raw_bitset_clear(set: &mut impl SetStorageTrait, b: usize) { set.clear(b) }
///Set bit b in raw set.
pub fn raw_bitset_set(set: &mut impl SetStorageTrait, b: usize) { set.set(b) }
///Is bit b set in raw set?
pub fn raw_bitset_isset(set: &impl SetStorageTrait, b: usize) -> bool { set.isset(b) }
///Test-and-Clear bit b in raw set.
pub fn raw_bitset_test_and_clear(set: &mut impl SetStorageTrait, b: usize) -> bool { set.test_and_clear(b) }
///Test-and-Set bit b in raw set.
pub fn raw_bitset_test_and_set(set: &mut impl SetStorageTrait, b: usize) -> bool { set.test_and_set(b) }
///Raw bitset to u64.
///
///If there are more than 64 bits in the raw bitset, the extra bits are just lost.
pub fn raw_bitset_to_u64(set: &impl SetStorageTrait) -> u64 { set.__to_u64() }
///Construct raw bitset from u64.
///
///If there are more than 64 bits in the raw bitset, there is no way to control the extra bits.
pub fn raw_bitset_from_u64<S:SetStorageTrait>(m: u64) -> S { S::__from_u64(m) }
///Compute hamming weight of raw bitset.
pub fn raw_bitset_hamming_weight(set: &impl SetStorageTrait) -> u32 { set.__hamming_weight() }
///Is raw bitset non-empty?
pub fn raw_bitset_not_empty(set: &impl SetStorageTrait) -> bool { set.__is_nonzero() }


impl_set_storage_trait_primitive!(u8);
impl_set_storage_trait_primitive!(u16);
impl_set_storage_trait_primitive!(u32);
impl_set_storage_trait_primitive!(u64);
impl_set_storage_trait_primitive!(u128);
impl_set_storage_trait_array!(u8);
impl_set_storage_trait_array!(u16);
impl_set_storage_trait_array!(u32);
impl_set_storage_trait_array!(u64);
impl_set_storage_trait_array!(u128);


///Item in set.
pub trait SetItem: __RustBuiltinDebug+Sized+Clone+'static
{
	///Get number of sign bit for item.
	fn __sign_bit(&self) -> usize;
}


///Set of items.
///
///It is assumed that set items are `Sized`, `Clone` and `Debug`, and that the universe is finite.
///
///It is assumed that the set itself can be unioned using the `|` operator and intersected using the `&` operator.
///Additionally, the set can be turned into iterator by using the `iter()` method.
pub trait Set: Sized+__RustBuiltinBitOr<Self>+__RustBuiltinBitAnd<Self>
{
	///Type of item in set.
	type Item: SetItem;
	///Storage.
	type Storage: SetStorageTrait;
	///Create a new empty set.
	fn new() -> Self;
	///Create from iterator.
	fn from_iterator<S,I:Iterator<Item=S>>(iter: I) -> Self where <Self as Set>::Item: Upcast<S>
	{
		let mut tmp = <Self as Set>::new();
		for item in iter {
			let casted_item = <<Self as Set>::Item as Upcast<S>>::__upcast(&item);
			tmp.__storage_mut().set(casted_item.__sign_bit());
		}
		tmp
	}
	///Borrow the storage.
	fn __storage<'a>(&'a self) -> &'a <Self as Set>::Storage;
	///Borrow the storage mutably.
	fn __storage_mut<'a>(&'a mut self) -> &'a mut <Self as Set>::Storage;
	///Get the universe of set.
	fn __universe() -> &'static [<Self as Set>::Item];
	#[doc(hidden)]
	fn _is_in(&self, item: &<Self as Set>::Item) -> bool { self.__storage().isset(item.__sign_bit()) }
	#[doc(hidden)]
	fn _add(&mut self, item: <Self as Set>::Item) { self.__storage_mut().set(item.__sign_bit()) }
	///Add a member to set.
	fn add<S>(&mut self, item: &S) where <Self as Set>::Item: Upcast<S>
	{
		self._add(<<Self as Set>::Item as Upcast<S>>::__upcast(item));
	}
	///Is member of set?
	///
	///If keycast fails, item is reported to not be in the set.
	fn is_in<S>(&self, item: &S) -> bool where <Self as Set>::Item: Keycast<S>
	{
		<<Self as Set>::Item as Keycast<S>>::__keycast(item).map(|x|self._is_in(&x)).unwrap_or(false)
	}
	///Like `is_in()`, but takes value instead of reference.
	fn is_in2<S:Sized>(&self, item: S) -> bool where <Self as Set>::Item: Keycast<S>
	{
		self.is_in(&item)
	}
	///Lookup member.
	///
	///This differs from `is_in()` in that on sucess the matching item is returned, and on failure, `None`
	///is returned.
	fn lookup<S>(&self, item: &S) -> Option<<Self as Set>::Item> where <Self as Set>::Item: Keycast<S>
	{
		if let Some(item) = <<Self as Set>::Item as Keycast<S>>::__keycast(item) {
			if self._is_in(&item) { return Some(item); }
		}
		None
	}
	///Get iterator over items.
	///
	///Unlike `__iter_all()` method, this performs automatic downcast and filtering on items before returning.
	///If this downcast fails, the item is omitted.
	///
	///Due to Rust limitations, the return type is exposed in full instead of being just
	///impl Iterator<Item=V>+'a. One should not assume anything about the exact return type, other than that
	///it implements Iterator<Item=V>, and lives for 'a.
	fn iter<'a,V>(&'a self) -> __DowncastIterator<'a, V, <Self as Set>::Item, Self>
		where <Self as Set>::Item: Downcast<V>
	{
		__DowncastIterator(Self::__universe().iter(), self, PhantomData)
	}
	///Count number of elements.
	///
	///This may be overridden with more efficient implementation.
	fn count(&self) -> u32 { Self::__universe().iter().filter(|item|self._is_in(item)).count() as u32 }
	///Has elements?
	///
	///This may be overridden with more efficient implementation.
	fn has_elements(&self) -> bool { Self::__universe().iter().any(|item|self._is_in(item)) }
	///Print debugging version.
	///
	///The reason this is not used as `Debug` implementation itself is that coherence does not allow that.
	///Nevertheless, the `Debug::fmt()` method of any set type should just directly call this.
	fn fmt_debug(&self, f: &mut __RustBuiltinFormatter) -> Result<(), __RustBuiltinFmtError>
	{
		let mut first = true;
		f.write_str("{")?;
		for alg in Self::__universe().iter() { if self._is_in(alg) {
			if !replace(&mut first, false) { f.write_str(", ")?; }
			__RustBuiltinDebug::fmt(alg, f)?;
		}}
		f.write_str("}")
	}
	///Raw test and remove of element described by bit b.
	fn __raw_test_and_remove(&mut self, b: usize) -> bool { self.__storage_mut().test_and_clear(b) }
	///Raw test and add of element described by bit b.
	fn __raw_test_and_add(&mut self, b: usize) -> bool { self.__storage_mut().test_and_set(b) }
}

///Implment Debug, BitOr and BitAnd for set.
#[macro_export]
macro_rules! impl_set_traits
{
	($xtype:ident) => {
		impl $crate::__RustBuiltinDebug for $xtype
		{
			fn fmt(&self, f: &mut $crate::__RustBuiltinFormatter) ->
				Result<(), $crate::__RustBuiltinFmtError>
			{
				self.fmt_debug(f)
			}
		}
		impl $crate::__RustBuiltinBitAnd<$xtype> for $xtype
		{
			type Output = Self;
			fn bitand(self, other: Self) -> Self
			{
				use $crate::SetStorageTrait;
				let mut r = $xtype::new();
				*r.__storage_mut() = self.__storage()._bitand(other.__storage());
				r
			}
		}
		impl $crate::__RustBuiltinBitOr<$xtype> for $xtype
		{
			type Output = Self;
			fn bitor(self, other: Self) -> Self
			{
				use $crate::SetStorageTrait;
				let mut r = $xtype::new();
				*r.__storage_mut() = self.__storage()._bitor(other.__storage());
				r
			}
		}
		impl $crate::__RustBuiltinBitAnd<Option<$xtype>> for $xtype
		{
			type Output = $xtype;
			fn bitand(self, o: Option<$xtype>) -> Self
			{
				if let Some(o) = o { self & o } else { self }
			}
		}
	};
}

#[doc(hidden)]
pub struct __DowncastIterator<'a,V,T:Downcast<V>+SetItem,S:Set<Item=T>>(core::slice::Iter<'a,T>,
	&'a S, PhantomData<V>);

impl<'a,V,T:Downcast<V>+SetItem,S:Set<Item=T>> Iterator for __DowncastIterator<'a,V,T,S>
{
	type Item=V;
	fn next(&mut self) -> Option<V>
	{
		loop {
			let item = self.0.next()?;
			if self.1._is_in(&item) { if let Some(item) = <T as Downcast<V>>::__downcast(&item) {
				return Some(item);
			}}
		}
	}
}
