use btls_aux_proc_macro_tools::Element;
use btls_aux_proc_macro_tools::ElementIdent;
use btls_aux_proc_macro_tools::ElementList;
use btls_aux_proc_macro_tools::ElementLiteral;
use btls_aux_proc_macro_tools::ElementStream;
use btls_aux_proc_macro_tools::output;
use btls_aux_proc_macro_tools::OutputIdentifier as Id;
use btls_aux_proc_macro_tools::OwnedOutput;
use btls_aux_proc_macro_tools::RootElementList;
use proc_macro::TokenStream;
use std::collections::BTreeMap;
use std::fmt::Arguments;
use std::fmt::Display;
use std::ops::Deref;
use std::str::FromStr;

/*
*/

static OID_TABLE: &'static [(&'static str, &'static str)] = &[
	("CURVE_NSA_P256", "1.2.840.10045.3.1.7"),
	("CURVE_NSA_P384", "1.3.132.0.34"),
	("CURVE_NSA_P521", "1.3.132.0.35"),
	("ECDSA_KEY_OID", "1.2.840.10045.2.1"),
	("ECDSA_SHA256_OID_I", "1.2.840.10045.4.3.2"),
	("ECDSA_SHA384_OID_I", "1.2.840.10045.4.3.3"),
	("ECDSA_SHA512_OID_I", "1.2.840.10045.4.3.4"),
	("ED25519_KEY_OID", "1.3.101.112"),
	("ED25519_OID_I", "1.3.101.112"),
	("ED448_KEY_OID", "1.3.101.113"),
	("ED448_OID_I", "1.3.101.113"),
	("MGF1_OID_I", "1.2.840.113549.1.1.8"),
	("RSA_KEY_OID", "1.2.840.113549.1.1.1"),
	("RSA_PKCS1_SHA1_OID_INSECURE_I", "1.2.840.113549.1.1.5"),
	("RSA_PKCS1_SHA256_OID_I", "1.2.840.113549.1.1.11"),
	("RSA_PKCS1_SHA384_OID_I", "1.2.840.113549.1.1.12"),
	("RSA_PKCS1_SHA3_256_OID_I", "2.16.840.1.101.3.4.3.14"),
	("RSA_PKCS1_SHA3_384_OID_I", "2.16.840.1.101.3.4.3.15"),
	("RSA_PKCS1_SHA3_512_OID_I", "2.16.840.1.101.3.4.3.16"),
	("RSA_PKCS1_SHA512_OID_I", "1.2.840.113549.1.1.13"),
	("RSA_PSS_KEY_OID", "1.2.840.113549.1.1.10"),
	("RSA_PSS_OID_I", "1.2.840.113549.1.1.10"),
	("SHA1_INSECURE_OID", "1.3.14.3.2.26"),
	("SHA256_OID", "2.16.840.1.101.3.4.2.1"),
	("SHA384_OID", "2.16.840.1.101.3.4.2.2"),
	("SHA3_256_OID", "2.16.840.1.101.3.4.2.8"),
	("SHA3_384_OID", "2.16.840.1.101.3.4.2.9"),
	("SHA3_512_OID", "2.16.840.1.101.3.4.2.10"),
	("SHA512_OID", "2.16.840.1.101.3.4.2.3"),
	("X509_AIA_ISSUER", "1.3.6.1.5.5.7.48.2"),
	("X509_AIA_OCSP", "1.3.6.1.5.5.7.48.1"),
	("X509_CSR_EXT_REQ", "1.2.840.113549.1.9.14"),
	("X509_EKU_ANY", "2.5.29.37.0"),
	("X509_EKU_OCSP", "1.3.6.1.5.5.7.3.9"),
	("X509_EKU_TLS_CLIENT", "1.3.6.1.5.5.7.3.2"),
	("X509_EKU_TLS_SERVER", "1.3.6.1.5.5.7.3.1"),
	("X509_EXT_ACME_ALPN", "1.3.6.1.5.5.7.1.31"),
	("X509_EXT_AIA", "1.3.6.1.5.5.7.1.1"),
	("X509_EXT_BASIC_CONSTRAINTS", "2.5.29.19"),
	("X509_EXT_DELEGATION_USAGE", "1.3.6.1.4.1.44363.44"),
	("X509_EXT_EKU", "2.5.29.37"),
	("X509_EXT_H_CT_V1_I", "1.3.6.1.4.1.11129.2.4.2"),
	("X509_EXT_H_CT_V2_I", "1.3.101.75"),
	("X509_EXT_INHIBIT_ANY_POLICY", "2.5.29.54"),
	("X509_EXT_KEY_USAGE", "2.5.29.15"),
	("X509_EXT_NAME_CONSTRAINTS", "2.5.29.30"),
	("X509_EXT_POLICY_CONSTRAINTS", "2.5.29.36"),
	("X509_EXT_POLICY_MAPPINGS", "2.5.29.33"),
	("X509_EXT_QC_STATEMENTS", "1.3.6.1.5.5.7.1.3"),
	("X509_EXT_SAN", "2.5.29.17"),
	("X509_EXT_SCT", "1.3.6.1.4.1.11129.2.4.2"),
	("X509_EXT_TLS_FEATURE", "1.3.6.1.5.5.7.1.24"),
	("X509_OCSP_BASIC_RESPONSE", "1.3.6.1.5.5.7.48.1.1"),
	("X509_OCSP_SCT", "1.3.6.1.4.1.11129.2.4.5"),
	("X509_SUBJECT_COMMON_NAME", "2.5.4.3"),
	("X509_SUBJECT_COUNTRY_NAME", "2.5.4.6"),
	("X509_SUBJECT_DN_QUALIFIER", "2.5.4.46"),
	("X509_SUBJECT_DOMAIN_COMPONENT", "0.9.2342.19200300.100.1.25"),
	("X509_SUBJECT_GENERATION_QUALIFIER", "2.5.4.44"),
	("X509_SUBJECT_GIVEN_NAME", "2.5.4.42"),
	("X509_SUBJECT_INITIALS", "2.5.4.43"),
	("X509_SUBJECT_LOCALITY_NAME", "2.5.4.7"),
	("X509_SUBJECT_NAME", "2.5.4.41"),
	("X509_SUBJECT_ORGANIZATIONAL_UNIT_NAME", "2.5.4.11"),
	("X509_SUBJECT_ORGANIZATION_NAME", "2.5.4.10"),
	("X509_SUBJECT_PSEUDONYM", "2.5.4.65"),
	("X509_SUBJECT_SERIAL_NUMBER", "2.5.4.5"),
	("X509_SUBJECT_STATE_OR_PROVINCE_NAME", "2.5.4.8"),
	("X509_SUBJECT_STREET_ADDRESS", "2.5.4.9"),
	("X509_SUBJECT_SURNAME", "2.5.4.4"),
	("X509_SUBJECT_TITLE", "2.5.4.12"),
	("X509_SUBJECT_UID", "0.9.2342.19200300.100.1.1"),
];

fn oid_table_post()
{
	for x in OID_TABLE.windows(2) {
		let first = x[0].0;
		let second = x[1].0;
		if first >= second { panic!("BUG: OID_TABLE {first} >= {second}"); }
	}
}

fn oid_table_find(t: &ElementIdent) -> Option<&'static str>
{
	let t = t.as_identifier()?;
	let index = OID_TABLE.binary_search_by(|&(candidate,_)|candidate.cmp(&t)).ok()?;
	Some(OID_TABLE[index].1)
}

static ASN1T_TABLE: &'static [(&'static str, u8)] = &[
	("BIT_STRING_RAW", 3),
	("BMP_STRING", 30),
	("BOOLEAN", 1),
	("CHARACTER_STRING", 29),
	("EMBEDDED_PDV", 11),
	("ENUMERATED", 10),
	("GENERALIZED_TIME", 24),
	("GENERAL_STRING", 27),
	("GRAPHICS_STRING", 25),
	("IA5_STRING", 22),
	("INSTANCE_OF", 8),
	("INTEGER", 2),
	("ISO646_STRING", 26),
	("NUMERIC_STRING", 18),
	("OBJECT_DESCRIPTOR", 7),
	("OBJECT_IDENTIFIER", 6),
	("OCTET_STRING", 4),
	("PRINTABLE_STRING", 19),
	("REAL", 9),
	("RELATIVE_OID", 13),
	("TELEX_STRING", 20),
	("UNIVERSAL_STRING", 28),
	("UTC_TIME", 23),
	("UTF8_STRING", 12),
	("VIDEO_STRING", 21),
];

fn asn1t_table_post()
{
	for x in ASN1T_TABLE.windows(2) {
		let first = x[0].0;
		let second = x[1].0;
		if first >= second { panic!("BUG: ASN1T_TABLE {first} >= {second}"); }
	}
}

fn asn1t_table_find(t: &ElementIdent) -> Option<u8>
{
	let t = t.as_identifier()?;
	let index = ASN1T_TABLE.binary_search_by(|&(candidate,_)|candidate.cmp(&t)).ok()?;
	Some(ASN1T_TABLE[index].1)
}

static BINDING_TABLE: &'static [(&'static str, &'static str)] = &[
	("Alert", "u8"),
	("AuthorizationDataFormat", "u8"),
	("CachedInformationType", "u8"),
	("CertChainType", "u8"),
	("CertificateCompressionAlgorithmId", "u16"),
	("CertificateStatusType", "u8"),
	("CertificateType", "u8"),
	("CipherSuite", "u16"),
	("ClientCertificateType", "u8"),
	("CompressionMethod", "u8"),
	("ConnectionIdUsage", "u8"),
	("ContentType", "u8"),
	("DtlsSrtpProtectionProfile", "u16"),
	("EcCurveType", "u8"),
	("EcPointFormat", "u8"),
	("EktCipher", "u8"),
	("ExtensionType", "u16"),
	("HandshakeType", "u8"),
	("HashAlgorithm", "u8"),
	("HeartbeatMessageType", "u8"),
	("HeartbeatMode", "u8"),
	("KdfIdentifier", "u16"),
	("NamedGroup", "u16"),
	("PskKeyExchangeMode", "u8"),
	("ServerNameType", "u8"),
	("SignatureAlgorithm", "u8"),
	("SignatureScheme", "u16"),
	("SupplementalDataFormat", "u16"),
	("TlsVersion", "u16"),
	("TokenBindingKeyParameter", "u8"),
	("UserMappingType", "u8"),
];

fn binding_table_post()
{
	for x in BINDING_TABLE.windows(2) {
		let first = x[0].0;
		let second = x[1].0;
		if first >= second { panic!("BUG: BINDING_TABLE {first} >= {second}"); }
	}
}

fn binding_table_find(t: &ElementIdent) -> Option<&'static str>
{
	let t = t.as_identifier()?;
	let index = BINDING_TABLE.binary_search_by(|&(candidate,_)|candidate.cmp(&t)).ok()?;
	Some(BINDING_TABLE[index].1)
}

static TYPED_TABLE: &'static [(&'static str, &'static str)] = &[
	("i128", "FragmentCi128"),
	("i16", "FragmentCi16"),
	("i32", "FragmentCi32"),
	("i64", "FragmentCi64"),
	("i8", "FragmentCi8"),
	("u128", "FragmentCu128"),
	("u16", "FragmentCu16"),
	("u32", "FragmentCu32"),
	("u64", "FragmentCu64"),
	("u8", "FragmentCu8"),
];

fn typed_table_post()
{
	for x in TYPED_TABLE.windows(2) {
		let first = x[0].0;
		let second = x[1].0;
		if first >= second { panic!("BUG: TYPED_TABLE {first} >= {second}"); }
	}
}

fn typed_table_find(t: &ElementIdent) -> Option<&'static str>
{
	let t = t.as_identifier()?;
	let index = TYPED_TABLE.binary_search_by(|&(candidate,_)|candidate.cmp(&t)).ok()?;
	Some(TYPED_TABLE[index].1)
}


#[derive(Copy,Clone,Debug)]
enum Asn1Tag
{
	Basic(u8),
	Extended(u8,u64),
}

fn __additional_asn1_len(tag: Asn1Tag, len: u64) -> u64
{
	let l1 = match tag {
		Asn1Tag::Basic(_) => 1,
		Asn1Tag::Extended(_, tag) => match tag.leading_zeros() {
			0 => 11,
			1..=7 => 10,
			8..=14 => 9,
			15..=21 => 8,
			22..=28 => 7,
			29..=35 => 6,
			36..=42 => 5,
			43..=49 => 4,
			50..=56 => 3,
			57|58 => 2,
			59 if tag == 31 => 2,
			_ => 1,
		}
	};
	let l2 = match len.leading_zeros() {
		0..=7 => 9,
		8..=15 => 8,
		16..=23 => 7,
		24..=31 => 6,
		32..=39 => 5,
		40..=47 => 4,
		48..=55 => 3,
		56 => 2,
		_ => 1,
	};
	l1 + l2
}

macro_rules! common_ast
{
	($clazz:ident) => {
		impl $clazz {
		}
	}
}

#[derive(Clone,Debug)]
enum AstNode
{
	Asn1Boolean(Vec<Element>),
	Asn1BitString2(Vec<AstNode>),
	RustExpr(Vec<Element>),
	Asn1Tagged2(Asn1Tag, Vec<AstNode>),
	Vector(Element, Vec<AstNode>),
	Extension(String, Vec<AstNode>),
	Constant(&'static str, Element),
	Binding(usize),
	ConstByte(u8),
	If(Element, Vec<AstNode>, Vec<AstNode>),
	IfLet(ElementList, Vec<AstNode>),
}

common_ast!(AstNode);

impl AstNode
{
	fn __fixed_length_slice(x: &[Self]) -> Option<u64>
	{
		let mut sum = 0;
		for child in x.iter() { sum += child.__fixed_length()?; }
		Some(sum)
	}
	fn __fixed_length(&self) -> Option<u64>
	{
		match self {
			&AstNode::ConstByte(_) => Some(1),
			&AstNode::Asn1Boolean(_) => Some(3),
			&AstNode::Asn1BitString2(ref children) => {
				//The padding byte.
				let mut len =  Self::__fixed_length_slice(children)? + 1;
				len += __additional_asn1_len(Asn1Tag::Basic(3), len);
				Some(len)
			},
			&AstNode::Asn1Tagged2(t, ref children) => {
				let mut len =  Self::__fixed_length_slice(children)?;
				len += __additional_asn1_len(t, len);
				Some(len)
			},
			//No hope with remaining.
			_ => None
		}
	}
	fn from_constant_bytes(parent: &mut Vec<AstNode>, b: &[u8])
	{
		for &g in b.iter() { parent.push(AstNode::ConstByte(g)); }
	}
	fn oid_compoent_to_vec(p: u64, v: &mut Vec<AstNode>)
	{
		let z = p.leading_zeros();
		for j in 0..9 { if z <= 7*j { v.push(AstNode::ConstByte(128|(p>>63-7*j) as u8)); } }
		v.push(AstNode::ConstByte((p&127) as u8));
	}
	fn from_oid(oid: &[u64], parent: &mut Vec<AstNode>, oidx: &str)
	{
		if oid.len() < 2 || oid[0] > 2 || oid[0] < 2 && oid[1] > 39 || oid[1] > 0xFFFF_FFFF_FFFF_FFB0 {
			panic!("Bad OID '{oidx}': Invalid prefix");
		}
		let head = oid[0] * 40 + oid[1];
		let tail = &oid[2..];
		Self::oid_compoent_to_vec(head, parent);
		for &g in tail.iter() { Self::oid_compoent_to_vec(g, parent); }
	}
	fn from_uuid(uuid: &[u8;16], parent: &mut Vec<AstNode>)
	{
		//The prefix byte is always 96 (2.16).
		parent.push(AstNode::ConstByte(96));
		for &g in uuid.iter() { parent.push(AstNode::ConstByte(g)); }
	}
	fn from_oid_string(oid: &str, parent: &mut Vec<AstNode>)
	{
		if oid.find(".").is_some() {
			//Contains a '.'. Presume this is normal OID.
			let mut tmp: Vec<u64> = Vec::new();
			for part in oid.split(".") {
				match u64::from_str(part) {
					Ok(x) => tmp.push(x),
					Err(_) => panic!("Bad OID '{oid}': Bad part")
				}
			}
			Self::from_oid(&tmp, parent, oid)
		} else {
			//No '.'. Presume this is UUID.
			let mut tmp = [0;16];
			let mut hexes = 0;
			for c in oid.chars() {
				if c == '-' {
					continue;
				} else if let Some(d) = c.to_digit(16) {
					if hexes < 32 { tmp[hexes/2] |= (d as u8) << 4-hexes%2*4; }
					hexes += 1;
				} else {
					panic!("Bad OID '{oid}': Illegal character {c}");
				}
			}
			if hexes != 32 { panic!("Bad OID '{oid}': Expected 32 hexes in UUID"); }
			Self::from_uuid(&tmp, parent)
		}
	}
	fn from_oid_tokens(strm: &mut ElementStream, parent: &mut Vec<AstNode>, bindings: &BindMap)
	{
		let t = strm.next();
		if let Element::Bracket(t) = t {
			let mut s = String::new();
			for i in t { match i {
				//The only allowable punct are alone '.', and '-'.
				Element::Punct(ref x) if x.is_punct(".") => s.push('.'),
				Element::Punct(ref x) if x.is_punct("-") => s.push('-'),
				Element::Punct(ref y) => bindings.root.panic(y, "Unsupported punct in OBJECT"),
				Element::Literal(x) => s.push_str(&x.to_string()),
				Element::Ident(x) => s.push_str(&x.to_string()),
				y => bindings.root.panic(&y, "Unsupported element type in OBJECT"),
			}}
			bindings.debug_oneshot(format_args!("OBJECT {s}"));
			Self::from_oid_string(&s, parent)
		} else {
			bindings.root.panic(&t, "Expected bracket-expr");
		}
	}
	fn paren_expr_forbidden(strm: &mut ElementStream, bindings: &BindMap)
	{
		if let Element::Paren(t) = strm.peek() {
			bindings.root.panic(&t, "Unexpected paren-expr");
		}
	}
	fn do_if(strm: &mut ElementStream, parent: &mut Vec<Self>, bindings: &mut BindMap)
	{
		let mut btrue = Vec::new();
		let mut bfalse = Vec::new();
		//First element is condition, it must be identifier or brace-block.
		let condition = match strm.next() {
			e@Element::Ident(_) => e,
			e@Element::Brace(_) => e,
			//Bracket expressions are used with if let, and those are handled specially.
			Element::Bracket(inner) => {
				//Then come the true branch.
				Self::parse2(strm, &mut btrue, bindings);
				parent.push(Self::IfLet(inner, btrue));
				return;
			},
			e => bindings.root.panic(&e, "Invalid if condition")
		};
		bindings.debug_oneshot(format_args!("if condition {condition:?}"));
		//Then come the true branch.
		bindings.debug_parse(&mut btrue, strm, "if true branch", Self::parse2);
		//Next, there must be else, ',' or end of stream.
		let fb = match strm.peek() {
			Element::Ident(id) => if id.is_keyword("else") {
				strm.next();	//Discard the else.
				Some(true)
			} else {
				None		//Bad.
			},
			Element::Punct(x) => if x.is_punct(",") {
				Some(false)	//Leave the , be.
			} else {
				None		//Bad.
			},
			Element::End(_) => Some(false),
			_ => None
		};
		let fb = fb.unwrap_or_else(||{
			bindings.root.panic(strm, "Expected else, ',' or end");
		});
		if fb {
			//False branch.
			bindings.debug_parse(&mut bfalse, strm, "if false branch", Self::parse2);
		}
		parent.push(AstNode::If(condition, btrue, bfalse));
	}
	fn do_brace(parent: &mut Vec<AstNode>, bindings: &mut BindMap, inner: ElementList)
	{
		//Brace is used for Rust expressions. The braces are preserved in the code,
		//as some expressions are not valid if unwrapped.
		bindings.debug_oneshot(format_args!("Rust expression: {inner:?}"));
		parent.push(AstNode::RustExpr(vec![Element::Brace(inner)]));
	}
	fn do_bracket(strm: &mut ElementStream, parent: &mut Vec<AstNode>, bindings: &mut BindMap, inner: ElementList)
	{
		//Bracket is used for custom ASN.1 tags.
		let mut istrm = inner.to_stream();
		let mut kind = if istrm.peek().is_identifier("UNIVERSAL") {
			istrm.next();	//Discard UNIVERSAL.
			0x00
		} else if istrm.peek().is_identifier("APPLICATION") {
			istrm.next();	//Discard APPLICATION.
			0x40
		} else if istrm.peek().is_identifier("PRIVATE") {
			istrm.next();	//Discard PRIVATE.
			0xC0
		} else {
			0x80
		};
		let tagid = istrm.expect_integer::<u64>(&bindings.root);
		istrm.expect_end(&bindings.root);
		//Is this constructed?
		let constructed = matches!(strm.peek(), Element::Paren(_));
		if constructed { kind |= 0x20; }
		bindings.debug_oneshot(format_args!("Explicit ASN.1 tag: {tkind}/{tagid}", tkind=kind>>5));
		let tag = if tagid < 31 {
			Asn1Tag::Basic(kind | tagid as u8)
		} else {
			Asn1Tag::Extended(kind, tagid)
		};
		//There can be either paren-expr or not.
		let mut children = Vec::new();
		if constructed {
			let mut istrm = strm.expect_paren_expr(&bindings.root);
			bindings.debug_parse(&mut children, &mut istrm, "Explicit ASN.1 paren-expr", Self::parse_list);
			istrm.expect_end(&bindings.root);
		} else {
			bindings.debug_parse(&mut children, strm, "Explicit ASN.1 value item", Self::parse2);
		};
		parent.push(AstNode::Asn1Tagged2(tag, children));
	}
	fn do_literial(parent: &mut Vec<AstNode>, lit: ElementLiteral)
	{
		let lit2 = lit.to_string();
		for &(suffix, ty) in TYPED_TABLE.iter() {
			if lit2.ends_with(suffix) {
				parent.push(AstNode::Constant(ty, Element::Literal(lit)));
				return;
			}
		}
		//Otherwise push it raw.
		parent.push(Self::RustExpr(vec![Element::Literal(lit)]));
	}
	fn do_ident(strm: &mut ElementStream, parent: &mut Vec<AstNode>, bindings: &mut BindMap, id: ElementIdent)
	{
		let idx = id.as_identifier();
		let idx = idx.as_deref().unwrap_or("???");
		bindings.lastid = Some(idx.to_string());
		if id.is_identifier("DEBUG") {
			bindings.debug = true;
		} else if id.is_identifier("SHOWOUT") {
			bindings.showout = true;
		} else if id.is_keyword("if") {
			Self::do_if(strm, parent, bindings);
		} else if let Some(oid) = oid_table_find(&id) {
			Self::from_oid_string(oid, parent);
		} else if let Some(tag) = asn1t_table_find(&id) {
			Self::paren_expr_forbidden(strm, bindings);
			let mut children = Vec::new();
			bindings.debug_parse(&mut children, strm, "ASN.1 tag value item", Self::parse2);
			parent.push(AstNode::Asn1Tagged2(Asn1Tag::Basic(tag), children));
		} else if let Some(ty) = binding_table_find(&id) {
			strm.expect_punct(&bindings.root, "::");
			let name = strm.expect_identifier(&bindings.root);
			bindings.debug_oneshot(format_args!("Codepoint: {idx}::{name}"));
			let x = format!("{idx}!{name}");
			parent.push(Self::Binding(bindings.bind(&x, ty)));
		} else if let Some(ty) = typed_table_find(&id) {
			bindings.debug_oneshot(format_args!("Typed({ty}): {tok:?}", tok=strm.peek()));
			match strm.next() {
				e@Element::Ident(_) => parent.push(Self::Constant(ty, e)),
				e@Element::Literal(_) => parent.push(Self::Constant(ty, e)),
				id => bindings.root.panic(&id, "Expected ident or literal")
			}
		} else if id.is_identifier("ERROR_MAP") {
			let id = strm.expect_ident(&bindings.root);
			bindings.debug_oneshot(format_args!("Using error map {id}", id=id.to_string()));
			if bindings.error_map.is_some() {
				bindings.root.panic(&id, "Only one ERROR_MAP is allowed");
			}
			bindings.error_map = Some(id);
		} else if id.is_identifier("EPSILON") {
			//This adds nothing to the parent.
		} else if id.is_identifier("CONCATENATE") {
			let mut strm = strm.expect_paren_expr(&bindings.root);
			//This is parsed to the same parent as this one.
			bindings.debug_parse(parent, &mut strm, "CONCATENATE paren-expr", Self::parse_list);
			strm.expect_end(&bindings.root);
		} else if id.is_identifier("BOOLEAN_VAL") {
			bindings.debug_parse(parent, strm, "BOOLEAN_VAL eat rust expression", |strm, parent, _|{
				let expr = Self::eat_rust_expression(strm);
				parent.push(AstNode::Asn1Boolean(expr));
			});
		} else if id.is_identifier("BOOLEAN_TRUE") {
			Self::from_constant_bytes(parent, &[1,1,255])
		} else if id.is_identifier("BIT_STRING") {
			Self::paren_expr_forbidden(strm, bindings);
			let mut children = Vec::new();
			bindings.debug_parse(&mut children, strm, "BIT STRING value item", Self::parse2);
			parent.push(AstNode::Asn1BitString2(children));
		} else if id.is_identifier("NULL") {
			Self::from_constant_bytes(parent, &[5,0])
		} else if id.is_identifier("OBJECT") {
			Self::from_oid_tokens(strm, parent, bindings)
		} else if id.is_identifier("SEQUENCE") {
			let mut strm = strm.expect_paren_expr(&bindings.root);
			//This parses into separate list. And then pushes to parent.
			let mut children = Vec::new();
			bindings.debug_parse(&mut children, &mut strm, "SEQUENCE paren-expr", Self::parse_list);
			strm.expect_end(&bindings.root);
			parent.push(AstNode::Asn1Tagged2(Asn1Tag::Basic(0x30), children));
		} else if id.is_identifier("SET") {
			let mut strm = strm.expect_paren_expr(&bindings.root);
			//This parses into separate list. And then pushes to parent.
			let mut children = Vec::new();
			bindings.debug_parse(&mut children, &mut strm, "SET paren-expr", Self::parse_list);
			strm.expect_end(&bindings.root);
			parent.push(AstNode::Asn1Tagged2(Asn1Tag::Basic(0x31), children));
		} else if id.is_identifier("VECTOR") {
			let info = Element::Ident(strm.expect_ident(&bindings.root));
			let mut strm = strm.expect_paren_expr(&bindings.root);
			let mut children = Vec::new();
			bindings.debug_parse(&mut children, &mut strm, "VECTOR paren-expr", Self::parse_list);
			strm.expect_end(&bindings.root);
			parent.push(AstNode::Vector(info, children));
		} else if id.is_identifier("EXTENSION") {
			let ename = strm.expect_identifier(&bindings.root);
			let mut strm = strm.expect_paren_expr(&bindings.root);
			bindings.debug_oneshot(format_args!("Extension {ename}"));
			let mut children = Vec::new();
			bindings.debug_parse(&mut children, &mut strm, "EXTENSION paren-expr", Self::parse_list);
			strm.expect_end(&bindings.root);
			//Stick the extension number into stream the same as binding for ExtensionType.
			let x = format!("ExtensionType!{ename}");
			parent.push(Self::Binding(bindings.bind(&x, "u16")));
			parent.push(AstNode::Extension(ename.to_string(), children));
		} else if id.as_identifier().is_some() {
			//This is ident. Push it raw.
			parent.push(AstNode::RustExpr(vec![Element::Ident(id)]));
		} else {
			bindings.root.panic(&id, "Unsupported non-identifier ident");
		}
	}
	fn eat_rust_expression(strm: &mut ElementStream) -> Vec<Element>
	{
		let mut expr: Vec<Element> = Vec::new();
		loop {
			let t = strm.peek();
			//Comma or EOF ends the expression. The comma is not eaten.
			if t.is_punct(",") || t.is_end() { break; }
			strm.next();
			expr.push(t);
		}
		expr
	}
/*
*/
	fn parse2(strm: &mut ElementStream, parent: &mut Vec<AstNode>, bindings: &mut BindMap)
	{
		match strm.next() {
			//Brace is used for Rust expressions. The braces are preserved in the code,
			//as some expressions are not valid if unwrapped.
			Element::Brace(inner) => AstNode::do_brace(parent, bindings, inner),
			Element::Bracket(inner) => AstNode::do_bracket(strm, parent, bindings, inner),
			Element::Ident(id) => AstNode::do_ident(strm, parent, bindings, id),
			Element::Literal(lit) => AstNode::do_literial(parent, lit),
			Element::Paren(ref x) => bindings.root.panic(x, "Unexpected paren-expr"),
			Element::GroupNone(ref x) => bindings.root.panic(x, "Unexpected gnone-expr"),
			Element::Lifetime(ref x) => bindings.root.panic(x, "Unexpected lifetime"),
			Element::Punct(ref x) => bindings.root.panic(x, "Unexpected punct"),
			Element::End(ref x) => bindings.root.panic(x, "Unexpected end"),
		}
	}
	fn parse_list(strm: &mut ElementStream, parent: &mut Vec<Self>, bindings: &mut BindMap)
	{
		while strm.more() {
			bindings.debug_parse(parent, strm, "List item", Self::parse2);
			strm.expect_list_comma(&bindings.root);
			bindings.debug_oneshot(format_args!("[NEXT {tok:?}] Eat comma", tok=strm.peek()));
		}
	}
}

struct BindMap<'a>
{
	forward: Vec<(String, String)>,
	backward: BTreeMap<String, usize>,
	error_map: Option<ElementIdent>,
	root: &'a RootElementList,
	lastid: Option<String>,
	debug: bool,
	showout: bool,
	depth: usize,
}

impl<'a> BindMap<'a>
{
	fn new(root: &'a RootElementList) -> BindMap<'a>
	{
		BindMap {
			forward: Vec::new(),
			backward: BTreeMap::new(),
			error_map: None,
			root: root,
			lastid: None,
			debug: false,
			showout: false,
			depth: 0
		}
	}
	fn make(id: usize) -> String { format!("__ROPE3_TEMP_CONSTANT_{id}") }
	fn has_bindings(&self) -> bool { self.forward.len() > 0 }
	fn emit_binding(&self, num: usize) -> OwnedOutput
	{
		let frag_f = &self.forward[num].1;
		let frag_ty = format!("FragmentC{frag_f}");
		let cname = Self::make(num);
		output!(asn1_func(&frag_ty), "::<", Id(&cname), ">")
	}
	fn emit_bindings(&self) -> OwnedOutput
	{
		let mut v = Vec::new();
		for (idx, &(ref sym, ref ty)) in self.forward.iter().enumerate() {
			let ty = ty.deref();
			let sname = Self::make(idx);
			let mut value: Vec<OwnedOutput> = Vec::new();
			for part in sym.split("!") { output!(TO[value] "::", Id(part)); }
			output!(TO[v] "const", Id(&sname), ":", ty, "= ::btls_aux_tls_iana", value, ".get();");
		}
		output!(v)
	}
	fn bind(&mut self, sym: &str, ty: &str) -> usize
	{
		if let Some(&num) = self.backward.get(sym) { return num; }
		let num = self.forward.len();
		self.forward.push((sym.to_string(), ty.to_string()));
		self.backward.insert(sym.to_string(), num);
		num
	}
	fn debug_oneshot(&self, a: Arguments)
	{
		if self.debug { println!("Trace[{depth}]: {a}", depth=self.depth); }
	}
	fn debug_parse(&mut self, parent: &mut Vec<AstNode>, strm: &mut ElementStream, a: impl Display,
		f: impl FnOnce(&mut ElementStream, &mut Vec<AstNode>, &mut BindMap<'a>))
	{
		if self.debug { println!("Enter[{depth}]: [NEXT {tok:?}] {a}", depth=self.depth, tok=strm.peek()); }
		self.depth += 1;
		f(strm, parent, self);
		self.depth -= 1;
		if self.debug { println!("Leave[{depth}]: [NEXT {tok:?}] {a}", depth=self.depth, tok=strm.peek()); }
	}
}


fn emit_rust_expr(expr: &[Element]) -> OwnedOutput
{
	let mut v = Vec::new();
	for elem in expr.iter() { output!(TO[v] elem); }
	output!(v)
}

fn emit_asn1_tlv(tag: Asn1Tag, arg: OwnedOutput) -> OwnedOutput
{
	use proc_macro::Literal;
	match tag {
		Asn1Tag::Basic(t) => {
			let t = Literal::u8_unsuffixed(t);
			output!(asn1_func("__private_asn1_tlv_small"), output!(() t, ",", arg))
		},
		Asn1Tag::Extended(k, t) => {
			let t = Literal::u64_unsuffixed(t);
			let ctr = if k & 0x20 != 0 { "true" } else { "false" };
			let mem = match k {
				0x00..=0x3F => "Universal",
				0x40..=0x7F => "Application",
				0x80..=0xBF => "ContextSpecific",
				0xC0..=0xFF => "Private",
			};
			output!(asn1_func("__private_asn1_tlv"), output!(() asn1_func("__PRIVATE_ASN1_TAG"),
				"::", mem, output!(() t, ",", ctr), ",", arg
			))
		}
	}
}

fn asn1_func(name: &str) -> OwnedOutput
{
	output!("::btls_aux_rope3::", Id(name))
}

fn throw_suffix(x: &Option<ElementIdent>) -> OwnedOutput
{
	if let Some(id) = x.as_ref() {
		output!(".map_err", output!(() id), "?")
	} else {
		output!("?")
	}
}

fn translate_ast(node: &AstNode, bindings: &BindMap) -> OwnedOutput
{
	use proc_macro::Literal;
	match node {
		&AstNode::Asn1Boolean(ref x) => {
			output!(asn1_func("__private_asn1_boolean"), output!(() emit_rust_expr(x)))
		},
		&AstNode::Asn1BitString2(ref x) => {
			output!(asn1_func("__private_asn1_bit_string"), output!(()
				concat_asts(x, bindings)
			))
		},
		&AstNode::RustExpr(ref x) => emit_rust_expr(x),
		&AstNode::Asn1Tagged2(tag, ref x) => {
			emit_asn1_tlv(tag, concat_asts(x, bindings))
		},
		&AstNode::ConstByte(b) => {
			let b = Literal::u8_unsuffixed(b);
			output!(asn1_func("FragmentCu8"), "::<", b, ">")
		},
		&AstNode::If(ref condition, ref btrue, ref bfalse) => {
			let (btrue2, bfalse2) = if bfalse.len() > 0 {
				let btrue2 = output!("Ok", output!(() concat_asts(btrue, bindings)));
				let bfalse2 = output!("Err", output!(() concat_asts(bfalse, bindings)));
				(btrue2, bfalse2)
			} else {
				let btrue2 = output!("Some", output!(() concat_asts(btrue, bindings)));
				(btrue2, output!("None"))
			};
			output!("if", condition, output!({} btrue2), "else", output!({} bfalse2))
		},
		&AstNode::IfLet(ref condition, ref btrue) => {
			let btrue2 = output!("Some", output!(() concat_asts(btrue, bindings)));
			output!("if let", condition, output!({} btrue2), "else { None }")
		},
		&AstNode::Vector(ref elem, ref children) => {
			output!(elem, ".", "__private_tls_vector", output!(() concat_asts(children, bindings)),
				throw_suffix(&bindings.error_map))
		},
		&AstNode::Extension(ref name, ref children) => {
			let name = name.deref();
			let name2 = Literal::string(name);
			output!(asn1_func("__private_tls_extension_body"), output!(()
				name2, ",", concat_asts(children, bindings)
			), throw_suffix(&bindings.error_map))
		},
		&AstNode::Constant(ty, ref child) => {
			output!(asn1_func(ty), "::<", child, ">")
		}
		&AstNode::Binding(num) => {
			bindings.emit_binding(num)
		}
	}
}

fn concat_asts(nodes: &[AstNode], bindings: &BindMap) -> OwnedOutput
{
	//use btls_aux_proc_macro_tools::OutputKeyword::*;
	if nodes.len() == 0 {
		//This returns unit.
		output!(())
	} else if nodes.len() == 1 {
		//This returns the translation of the ast.
		translate_ast(&nodes[0], bindings)
	} else {
		let split = nodes.len() / 2;
		assert!(split > 0 && split < nodes.len());
		let (left, right) = nodes.split_at(split);
		let left = concat_asts(left, bindings);
		let right = concat_asts(right, bindings);
		output!("::btls_aux_rope3::FragmentSequence::new", output!(() left, ",", right))
	}
}

#[proc_macro]
pub fn rope3(s: TokenStream) -> TokenStream
{
	oid_table_post();
	asn1t_table_post();
	binding_table_post();
	typed_table_post();
	let ret = {
		let s = RootElementList::new(s);
		let mut bindings = BindMap::new(&s);
		let mut s = s.to_stream();
		let mut ast = Vec::new();
		bindings.debug_parse(&mut ast, &mut s, "Toplevel", AstNode::parse_list);
		let output = concat_asts(&ast, &bindings);
		let output = if bindings.has_bindings() {
			output!({} bindings.emit_bindings(), output)
		} else {
			output
		};
		if bindings.showout { println!("output: {output:?}"); }
		output.to_token_stream()
	};
	ret
}
