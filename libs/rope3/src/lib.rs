#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]
use btls_aux_assert::AssertFailed;
use btls_aux_collections::Arc;
use btls_aux_collections::Rc;
use btls_aux_collections::String;
use btls_aux_collections::ToString;
use btls_aux_collections::Vec;
use btls_aux_fail::f_return;
//use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::fail_if_none;
use btls_aux_memory::BackedVector;
use btls_aux_memory::NonNullMut as NonNull;
use btls_aux_memory::SliceArrayExt;
use btls_aux_memory::TrustedInvariantLength;
pub use btls_aux_rope3_derive::rope3;
pub use btls_aux_serialization::Asn1Tag as __PRIVATE_ASN1_TAG;
use btls_aux_serialization::VectorLength;
use core::cmp::min;
use core::convert::TryInto;
use core::fmt::Display;
use core::fmt::Formatter;
use core::fmt::Error as FmtError;
use core::marker::PhantomData;
use core::mem::MaybeUninit;
use core::mem::size_of;
use core::ops::Deref;


macro_rules! write_byte_seq
{
	($arr:expr,$idx:ident,$val:expr) => {{
		$arr.get_mut($idx).map(|x|*x=$val as u8);
		$idx += 1;
	}};
}


///Vector field descriptor.
///
///This is used to serialize vector fields.
#[derive(Copy,Clone)]
pub struct VectorField(pub &'static str, pub VectorLength);

impl VectorField
{
	fn check(&self, len: usize) -> Result<(), VectorFieldError>
	{
		if !self.1.acceptable_length(len) {
			Err(VectorFieldError(_VectorFieldError::Size(self.0.to_string(), len, self.1)))
		} else {
			Ok(())
		}
	}
	#[doc(hidden)]
	pub fn __private_tls_vector<T:FragmentWrite>(self, value: T) -> Result<FragmentVector<T>, VectorFieldError>
	{
		let length = value.length();
		self.check(length)?;
		let xlen = self.1.length_bytes();
		let mut prefix = [0;23];
		let mut pfxlen = 0;
		//Assume all lengths are at most 4GB.
		if xlen > 3 { write_byte_seq!(prefix, pfxlen, length>> 24); }
		if xlen > 2 { write_byte_seq!(prefix, pfxlen, length>> 16); }
		if xlen > 1 { write_byte_seq!(prefix, pfxlen, length >> 8); }
		if xlen > 0 { write_byte_seq!(prefix, pfxlen, length); }
		let pfxlen = pfxlen as u8;
		Ok(FragmentVector{pfxlen, prefix, value})
	}
}

#[doc(hidden)]
pub fn __private_tls_extension_body<T:FragmentWrite>(name: &'static str, value: T) ->
	Result<FragmentSequence<u16,T>, VectorFieldError>
{
	let length = value.length();
	fail_if!(length > 0xFFFF, VectorFieldError(_VectorFieldError::Extension(name, length)));
	Ok(FragmentSequence::new(length as u16, value))
}

///Vector field error.
#[derive(Clone,Debug)]
enum _VectorFieldError
{
	Assert(AssertFailed),
	Size(String, usize, VectorLength),
	Extension(&'static str, usize),
}
#[derive(Clone,Debug)]
pub struct VectorFieldError(_VectorFieldError);

impl VectorFieldError
{
	///Prefix the name.
	pub fn prefix(&self, pfx: &str) -> VectorFieldError
	{
		if let &_VectorFieldError::Size(ref name, size, acceptable) = &self.0 {
			let mut name2 = String::with_capacity(pfx.len() + 2 + name.len());
			name2.push_str(pfx);
			name2.push_str("->");
			name2.push_str(name);
			VectorFieldError(_VectorFieldError::Size(name2, size, acceptable))
		} else {
			self.clone()
		}
	}
}

impl From<AssertFailed> for VectorFieldError
{
	fn from(x: AssertFailed) -> VectorFieldError
	{
		VectorFieldError(_VectorFieldError::Assert(x))
	}
}

impl Display for VectorFieldError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match &self.0 {
			&_VectorFieldError::Assert(ref x) => Display::fmt(x, f),
			&_VectorFieldError::Size(ref name, size, acceptable) =>
				write!(f, "Length {size} of {name} not {acceptable}"),
			&_VectorFieldError::Extension(name, size) => write!(f, "Extension {name} size {size} too big"),
		}
	}
}


///Fragment buffer trait.
///
///This trait marks the type as fragment buffer. The operations fragment buffers support are:
///
/// * Copy the data to slice or raw array.
/// * Get length of data in buffer.
///
/// # Unsafety:
///
///This trait is unsafe to implement. All implementations are required to obey the following invariants:
///
/// * If callers uphold preconditions of `write_all()`, no illegal operations are executed.
/// * The methods themselves never panic nor abort.
/// * Return value of `length()` can never change.
/// * `write_all()` always writes exactly `length()` bytes.
/// * `__write()` always writes at least `length()-start` or `buf.len()`  bytes, whichever is less.
/// * If `length_const()` returns `Some(n)`, then `length()` returns `n`.
/// * The return value of `length()` is saturated to the maximum.
pub unsafe trait FragmentWrite
{
	///Write the fragment into memory.
	///
	///The parameter `ptr` gives pointer to copy the data to.
	///
	///Returns `ptr+length()`.
	///
	/// # Unsafety:
	///
	///The caller of this function must ensure that `ptr` points to at least `length()` bytes of writable
	///memory.
	unsafe fn write_all(&self, ptr: NonNull<u8>) -> NonNull<u8>;
	///Write part of the fragment into memory.
	///
	///The parameter `start` gives the starting offset of part of fragment to copy.
	///
	///The parameter `buffer` gives buffer to write the data to.
	///
	///If the part of fragment fits into buffer, returns `Some(())`. Otherwise returns `None` (still filling the
	///data).
	fn write(&self, start: usize, buffer: &mut BackedVector<u8>) -> Option<()>;
	///Get length of fragment buffer in bytes.
	fn length(&self) -> usize;
	///Get length of fragment buffer in bytes.
	///
	///This function is an optimization: Returning `None` is always valid, but for types that always
	///encode fixed-length fragment buffers, returning the length improves performance of some operations.
	///For instance, `FragmentArray` calculates indices using division instead iterating through array.
	fn length_const() -> Option<usize> { None }
}

///Marker trait for constant-size fragment buffers.
///
/// # Unsafety:
///
///This trait is unsafe to implement. All implementations are required to obey the following invariants:
///
/// * `length_const2()` returns the same value as `FragmentWrite::length_const()`.
///
pub unsafe trait FragmentWriteConstantSize : FragmentWrite
{
	///Get length of fragment buffer in bytes.
	fn length_const2() -> usize;
}

///Object-safe version of `FragmentWrite`.
pub unsafe trait FragmentWriteDyn
{
	///See `FragmentWrite::write_all()`.
	unsafe fn write_all_dyn(&self, ptr: NonNull<u8>) -> NonNull<u8>;
	///See `FragmentWrite::write()`.
	fn write_dyn(&self, start: usize, buffer: &mut BackedVector<u8>) -> Option<()>;
	///See `FragmentWrite::length()`.
	fn length_dyn(&self) -> usize;
}

unsafe impl<T:FragmentWrite+?Sized> FragmentWriteDyn for T
{
	///See `FragmentWrite::write_all()`.
	unsafe fn write_all_dyn(&self, ptr: NonNull<u8>) -> NonNull<u8>
	{
		unsafe{self.write_all(ptr)}
	}
	///See `FragmentWrite::write()`.
	fn write_dyn(&self, start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		self.write(start, buffer)
	}
	///See `FragmentWrite::length()`.
	fn length_dyn(&self) -> usize { self.length() }
}

unsafe fn __rope_to_vec(length: usize, fill: impl FnOnce(NonNull<u8>) -> NonNull<u8>) -> Vec<u8>
{
	//Do something if fragment buffer has insane length.
	f_return!(TryInto::<isize>::try_into(length), Vec::new());
	let mut v = Vec::with_capacity(length);
	//This has length writable bytes.
	let r = fill(v.as_nonnull_ptr());
	debug_assert!(r == unsafe{v.as_nonnull_ptr().add(length)});
	//write_all() wrote length bytes.
	unsafe{v.set_len(length);}
	v
}

unsafe fn __rope_extend_vec(v: &mut Vec<u8>, length: usize, fill: impl FnOnce(NonNull<u8>) -> NonNull<u8>)
{
	let oldlen = v.len();
	let newlen = oldlen.saturating_add(length);
	//Do something if fragment buffer has insane length.
	f_return!(TryInto::<isize>::try_into(newlen), ());
	v.reserve(length);
	let r = unsafe{fill(v.as_nonnull_ptr().add(oldlen))};
	debug_assert!(r == unsafe{v.as_nonnull_ptr().add(newlen)});
	unsafe{v.set_len(newlen);}
}


///Some common operations on fragment buffers.
///
///This struct implements some common helper operations for fragment buffers.
pub struct FragmentWriteOps;

impl FragmentWriteOps
{
	///Turn rope into vector.
	pub fn to_vector<T:FragmentWrite>(obj: &T) -> Vec<u8>
	{
		unsafe{__rope_to_vec(obj.length(), |ptr|obj.write_all(ptr))}
	}
	///Extend vector with rope.
	pub fn extend_vector<T:FragmentWrite>(obj: &T, v: &mut Vec<u8>)
	{
		unsafe{__rope_extend_vec(v, obj.length(), |ptr|obj.write_all(ptr))}
	}
	///Write to slice.
	pub fn to_slice_len<T:FragmentWrite>(obj: &T, buf: &mut [u8]) -> Option<usize>
	{
		let len = obj.length();
		//SAFETY: buf.len() can not be saturated, so this will always fail on fragment too large.
		fail_if_none!(len > buf.len());
		unsafe {
			//Have space to write this.
			obj.write_all(buf.as_nonnull_ptr());
		}
		Some(len)
	}
	///Like `to_slice()`, but takes uninitialized input buffer instead of initialized input buffer.
	pub fn to_slice_u<'b,T:FragmentWrite>(obj: &T, sink: &'b mut [MaybeUninit<u8>]) ->
		Result<&'b [u8], (usize, usize)>
	{
		let mut sink = BackedVector::new(sink);
		let len = obj.length();
		let slen = sink.remaining_capacity();
		fail_if!(len > slen, (len, slen));
		unsafe {
			//Have space to write this. And sink has correct number of elements.
			obj.write_all(sink.unfilled_nonnull());
			sink.assume_init(len);
			Ok(sink.into_inner())
		}
	}
	///Turn rope into vector.
	pub fn to_vector_dyn(obj: &dyn FragmentWriteDyn) -> Vec<u8>
	{
		unsafe{__rope_to_vec(obj.length_dyn(), |ptr|obj.write_all_dyn(ptr))}
	}
	///Extend vector with rope.
	pub fn extend_vector_dyn(obj: &dyn FragmentWriteDyn, v: &mut Vec<u8>)
	{
		unsafe{__rope_extend_vec(v, obj.length_dyn(), |ptr|obj.write_all_dyn(ptr))}
	}
	///Write to slice.
	pub fn to_slice_len_dyn(obj: &dyn FragmentWriteDyn, buf: &mut [u8]) -> Option<usize>
	{
		let len = obj.length_dyn();
		//SAFETY: buf.len() can not be saturated, so this will always fail on fragment too large.
		fail_if_none!(len > buf.len());
		unsafe {
			//Have space to write this.
			obj.write_all_dyn(buf.as_nonnull_ptr());
		}
		Some(len)
	}
	///Like `to_slice_dyn()`, but takes uninitialized input buffer instead of initialized input buffer.
	pub fn to_slice_u_dyn<'b>(obj: &dyn FragmentWriteDyn, sink: &'b mut [MaybeUninit<u8>]) ->
		Result<&'b [u8], (usize, usize)>
	{
		let mut sink = BackedVector::new(sink);
		let len = obj.length_dyn();
		let slen = sink.remaining_capacity();
		fail_if!(len > slen, (len, slen));
		unsafe {
			obj.write_all_dyn(sink.unfilled_nonnull());
			sink.assume_init(len);
			Ok(sink.into_inner())
		}
	}
}

///Fragment reader.
pub struct FragmentReader<'a,T:FragmentWriteDyn+?Sized+'a>
{
	object: &'a T,
	position: usize,
}

impl<'a,T:FragmentWriteDyn+?Sized+'a> FragmentReader<'a,T>
{
	///Create new.
	pub fn new(obj: &'a T) -> FragmentReader<'a,T>
	{
		FragmentReader {
			object: obj,
			position: 0,
		}
	}
	///Read block.
	///
	///The return flag is more flag.
	pub fn read<'b>(&mut self, buf: &'b mut [MaybeUninit<u8>]) -> (bool, &'b [u8])
	{
		let mut buf = BackedVector::<u8>::new(buf);
		let more = self.object.write_dyn(self.position, &mut buf).is_none();
		let buf = buf.into_inner();
		self.position += buf.len();
		(more, buf)
	}
}

/*********************************************************************************************************************/
macro_rules! define_fragmentwrite_for_integer
{
	($clazz:ident) => {
		unsafe impl FragmentWrite for $clazz
		{
			unsafe fn write_all(&self, ptr: NonNull<u8>) -> NonNull<u8>
			{
				unsafe{self.to_be_bytes().write_all(ptr)}
			}
			fn write(&self, start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
			{
				self.to_be_bytes().write(start, buffer)
			}
			fn length(&self) -> usize { size_of::<$clazz>() }
			fn length_const() -> Option<usize> { Some(size_of::<$clazz>()) }
		}
		unsafe impl FragmentWriteConstantSize for $clazz
		{
			fn length_const2() -> usize { size_of::<$clazz>() }
		}
	};
	($clazz:ident $ty:ident) => {
		///Fragment that is type-level constant.
		///
		///This writes the number `X` using big-endian representation.
		#[derive(Copy,Clone,Debug)]
		pub struct $clazz<const X: $ty>;
		unsafe impl<const X:$ty> FragmentWrite for $clazz<X>
		{
			unsafe fn write_all(&self, ptr: NonNull<u8>) -> NonNull<u8>
			{
				unsafe{X.to_be_bytes().write_all(ptr)}
			}
			fn write(&self, start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
			{
				X.to_be_bytes().write(start, buffer)
			}
			fn length(&self) -> usize { size_of::<$ty>() }
			fn length_const() -> Option<usize> { Some(size_of::<$ty>()) }
		}
		unsafe impl<const X:$ty> FragmentWriteConstantSize for $clazz<X>
		{
			fn length_const2() -> usize { size_of::<$ty>() }
		}
	};
}

unsafe impl FragmentWrite for ()
{
	unsafe fn write_all(&self, ptr: NonNull<u8>) -> NonNull<u8> { ptr }
	fn write(&self, _: usize, _: &mut BackedVector<u8>) -> Option<()> { Some(()) }
	fn length(&self) -> usize { 0 }
	fn length_const() -> Option<usize> { Some(0) }
}

unsafe impl FragmentWriteConstantSize for ()
{
	fn length_const2() -> usize { 0 }
}

define_fragmentwrite_for_integer!(FragmentCi8 i8);
define_fragmentwrite_for_integer!(FragmentCu8 u8);
define_fragmentwrite_for_integer!(FragmentCi16 i16);
define_fragmentwrite_for_integer!(FragmentCu16 u16);
define_fragmentwrite_for_integer!(FragmentCi32 i32);
define_fragmentwrite_for_integer!(FragmentCu32 u32);
define_fragmentwrite_for_integer!(FragmentCi64 i64);
define_fragmentwrite_for_integer!(FragmentCu64 u64);
define_fragmentwrite_for_integer!(FragmentCi128 i128);
define_fragmentwrite_for_integer!(FragmentCu128 u128);

define_fragmentwrite_for_integer!(i8);
define_fragmentwrite_for_integer!(u8);
define_fragmentwrite_for_integer!(i16);
define_fragmentwrite_for_integer!(u16);
define_fragmentwrite_for_integer!(i32);
define_fragmentwrite_for_integer!(u32);
define_fragmentwrite_for_integer!(i64);
define_fragmentwrite_for_integer!(u64);
define_fragmentwrite_for_integer!(i128);
define_fragmentwrite_for_integer!(u128);

unsafe fn __slice_write_all(obj: &[u8], ptr: NonNull<u8>) -> NonNull<u8>
{
	let n = obj.len();
	unsafe{ptr.write_slice(obj);}
	unsafe{ptr.add(n)}
}

fn __slice_write(obj: &[u8], start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
{
	let obj = obj.get(start..).unwrap_or(&[]);
	buffer.write_slice(obj)
}

unsafe impl<const N: usize> FragmentWrite for [u8;N]
{
	unsafe fn write_all(&self, ptr: NonNull<u8>) -> NonNull<u8>
	{
		unsafe{__slice_write_all(self, ptr)}
	}
	fn write(&self, start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		__slice_write(self, start, buffer)
	}
	fn length(&self) -> usize { N }
	fn length_const() -> Option<usize> { Some(N) }
}

unsafe impl<const N: usize> FragmentWriteConstantSize for [u8;N]
{
	fn length_const2() -> usize { N }
}

unsafe impl<'a, const N: usize> FragmentWrite for &'a [u8;N]
{
	unsafe fn write_all(&self, ptr: NonNull<u8>) -> NonNull<u8>
	{
		unsafe{__slice_write_all(*self, ptr)}
	}
	fn write(&self, start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		__slice_write(*self, start, buffer)
	}
	fn length(&self) -> usize { N }
	fn length_const() -> Option<usize> { Some(N) }
}

unsafe impl<'a,const N: usize> FragmentWriteConstantSize for &'a [u8;N]
{
	fn length_const2() -> usize { N }
}

macro_rules! adapt_slicelike
{
	(raw $e:expr) => { $e };
	(as_bytes $e:expr) => { $e.as_bytes() };
	(deref $e:expr) => { $e.deref() };
}

macro_rules! fw_slicelike
{
	($kind:ident $xtype:ty [$($impl_args:tt)*] [$($pfx_args:tt)*]) => {
		unsafe impl $($impl_args)* FragmentWrite for $($pfx_args)* $xtype
		{
			unsafe fn write_all(&self, ptr: NonNull<u8>) -> NonNull<u8>
			{
				unsafe{__slice_write_all(adapt_slicelike!($kind self), ptr)}
			}
			fn write(&self, start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
			{
				__slice_write(adapt_slicelike!($kind self), start, buffer)
			}
			fn length(&self) -> usize { self.len() }
		}
	}
}

fw_slicelike!(raw [u8] [] []);
fw_slicelike!(as_bytes str [] []);
fw_slicelike!(deref Vec<u8> [] []);
fw_slicelike!(as_bytes String [] []);
fw_slicelike!(raw [u8] [<'a>] [&'a]);
fw_slicelike!(as_bytes str [<'a>] [&'a]);
fw_slicelike!(deref Vec<u8> [<'a>] [&'a]);
fw_slicelike!(as_bytes String [<'a>] [&'a]);


/*********************************************************************************************************************/
//*ptr must point to *size writable bytes.
fn chained_write<X:FragmentWrite+?Sized>(obj: &X, start: &mut usize, buffer: &mut BackedVector<u8>) -> Option<()>
{
	let len = obj.length();
	if *start == 0 && buffer.remaining_capacity() >= len { unsafe {
		//Write the entiere object. Assume this initializes len.
		let ptr = buffer.unfilled_nonnull();
		let ptr2 = obj.write_all(buffer.unfilled_nonnull());
		debug_assert!(ptr2.segment_offset() - ptr.segment_offset() == len);
		buffer.assume_init(len);
	}} else if *start < len {
		//The object will be written to the end if ? does not break, so start is set to 0.
		obj.write(*start, buffer)?;
		*start = 0;
	} else {
		//Just advance start position for next subobject.
		*start -= len;
	};
	Some(())
}

#[doc(hidden)]
#[derive(Copy,Clone)]
pub struct FragmentSequence<A:FragmentWrite,B:FragmentWrite>(A, B, usize);

impl<A:FragmentWrite,B:FragmentWrite> FragmentSequence<A,B>
{
	///Create new.
	pub fn new(a: A, b: B) -> FragmentSequence<A,B>
	{
		let len = a.length().saturating_add(b.length());
		FragmentSequence(a, b, len)
	}
}

unsafe impl<A:FragmentWrite,B:FragmentWrite> FragmentWrite for FragmentSequence<A,B>
{
	unsafe fn write_all(&self, mut ptr: NonNull<u8>) -> NonNull<u8>
	{
		ptr = unsafe{self.0.write_all(ptr)};
		ptr = unsafe{self.1.write_all(ptr)};
		ptr
	}
	fn write(&self, mut start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		chained_write(&self.0, &mut start, buffer)?;
		chained_write(&self.1, &mut start, buffer)?;
		Some(())
	}
	fn length(&self) -> usize { self.2 }
	fn length_const() -> Option<usize>
	{
		let a = A::length_const()?;
		let b = B::length_const()?;
		Some(a.saturating_add(b))
	}
}

unsafe impl<A:FragmentWriteConstantSize,B:FragmentWriteConstantSize> FragmentWriteConstantSize for
	FragmentSequence<A,B>
{
	fn length_const2() -> usize { A::length_const2().saturating_add(B::length_const2()) }
}

unsafe impl<A:FragmentWrite,B:FragmentWrite> FragmentWrite for Result<A,B>
{
	unsafe fn write_all(&self, ptr: NonNull<u8>) -> NonNull<u8>
	{
		match self { &Ok(ref x) => unsafe{x.write_all(ptr)}, &Err(ref x) => unsafe{x.write_all(ptr)} }
	}
	fn write(&self, start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		match self {
			&Ok(ref x) => x.write(start, buffer),
			&Err(ref x) => x.write(start, buffer),
		}
	}
	fn length(&self) -> usize
	{
		match self { &Ok(ref x) => x.length(), &Err(ref x) => x.length() }
	}
}

unsafe impl<T:FragmentWrite> FragmentWrite for Option<T>
{
	unsafe fn write_all(&self, ptr: NonNull<u8>) -> NonNull<u8>
	{
		match self { &Some(ref x) => unsafe{x.write_all(ptr)}, &None => ptr }
	}
	fn write(&self, start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		match self { &Some(ref x) => x.write(start, buffer), &None => Some(()) }
	}
	fn length(&self) -> usize
	{
		match self { &Some(ref x) => x.length(), &None => 0 }
	}
}

#[doc(hidden)]
#[derive(Copy,Clone)]
pub struct FragmentTrustedLen<X:TrustedInvariantLength>(X, usize);

///Create fragment out of something that is `TrustedInvariantLength`.
pub fn fragment_trusted_invariant_length<X:TrustedInvariantLength>(x: X) -> FragmentTrustedLen<X>
{
	//Store length, in case getting slice is slow.
	let len = x.as_byte_slice().len();
	FragmentTrustedLen(x, len)
}

unsafe impl<X:TrustedInvariantLength> FragmentWrite for FragmentTrustedLen<X>
{
	unsafe fn write_all(&self, ptr: NonNull<u8>) -> NonNull<u8>
	{
		//By assumption, the slice is of courrect length.
		unsafe{__slice_write_all(self.0.as_byte_slice(), ptr)}
	}
	fn write(&self, start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		//The same as in write_all() appiles here too.
		__slice_write(self.0.as_byte_slice(), start, buffer)
	}
	fn length(&self) -> usize { self.1 }
}

#[doc(hidden)]
#[derive(Copy,Clone)]
pub struct FragmentPadded<'a>(&'a [u8], usize);

impl<'a> FragmentPadded<'a>
{
	fn get(self) -> (usize, &'a [u8])
	{
		if self.0.len() < self.1 {
			(self.1 - self.0.len(), self.0)
		} else {
			//Write whole. self.0.len() >= self.1.
			(0, &self.0[self.0.len()-self.1..])
		}
		
	}
}

///Create padded fragment.
pub fn fragment_padded<'a>(data: &'a [u8], len: usize) -> FragmentPadded<'a> { FragmentPadded(data, len) }

unsafe impl<'a> FragmentWrite for FragmentPadded<'a>
{
	unsafe fn write_all(&self, mut ptr: NonNull<u8>) -> NonNull<u8>
	{
		let (pad, data) = self.get();
		unsafe{ptr.write_bytes_adv(0, pad);}
		unsafe{ptr.write_slice_adv(data);}
		ptr
	}
	fn write(&self, mut start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		let (pad, data) = self.get();
		chained_write(&FragmentZeros(pad), &mut start, buffer)?;
		chained_write(&data, &mut start, buffer)?;
		Some(())
	}
	fn length(&self) -> usize { self.1 }
}


#[doc(hidden)]
pub struct FragmentAsRef<Y:FragmentWriteConstantSize,X:AsRef<[Y]>>(X, usize, PhantomData<Y>);

//Manually impl Copy and Clone, because Y does not need to be copy/clone, but X does.
impl<Y:FragmentWriteConstantSize,X:AsRef<[Y]>+Copy> Copy for FragmentAsRef<Y,X> {}
impl<Y:FragmentWriteConstantSize,X:AsRef<[Y]>+Clone> Clone for FragmentAsRef<Y,X>
{
	fn clone(&self) -> Self { FragmentAsRef(self.0.clone(), self.1, PhantomData) }
}

///Create fragment out of something that is `AsRef<[Y]>`, where `Y` is constant-size fragment.
///
///If `Y==u8`, `fragment_asref()` is more efficient.
pub fn fragment_asref2<Y:FragmentWriteConstantSize,X:AsRef<[Y]>>(x: X) -> FragmentAsRef<Y,X>
{
	//Store length, since it can vary.
	let len = x.as_ref().len();
	FragmentAsRef(x, len, PhantomData)
}

unsafe impl<Y:FragmentWriteConstantSize,X:AsRef<[Y]>> FragmentWrite for FragmentAsRef<Y,X>
{
	unsafe fn write_all(&self, mut ptr: NonNull<u8>) -> NonNull<u8>
	{
		//Be careful: Bad AsRef implementation can return slices of varying sizes. Do not cause memory
		//corruption in that case. Truncate slice if it is too long, zero-pad if slice is too short.
		let tmp = self.0.as_ref();
		let tlen = tmp.len();
		if tlen >= self.1 {
			for i in 0..self.1 { ptr = unsafe{tmp[i].write_all(ptr)}; }
		} else {
			for i in 0..tlen { ptr = unsafe{tmp[i].write_all(ptr)}; }
			let zlen = (self.1-tlen)*Y::length_const2();
			unsafe{ptr.write_bytes(0, zlen);}
			ptr = unsafe{ptr.add(zlen)};
		}
		ptr
	}
	fn write(&self, mut start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		//The same as in write_all() appiles here too.
		let slice = self.0.as_ref();
		let ylen = Y::length_const2();
		//Handle the degnerate case where length of element is 0. This is so that dividing by length
		//is safe.
		if ylen == 0 { return Some(()); }
		let offset = start / ylen;
		start %= ylen;
		if self.1 <= slice.len() {
			//There are sufficient elements.
			for i in offset..self.1 {
				chained_write(&slice[i], &mut start, buffer)?;
			}
		} else if offset >= slice.len() {
			//Entiere range is padding.
			let pelems = self.1.saturating_sub(offset);
			buffer.write_zeros(pelems.saturating_mul(ylen).saturating_sub(start))?;
		} else {
			//Part is elements, part is padding.
			for i in offset..slice.len() {
				chained_write(&slice[i], &mut start, buffer)?;
			}
			//Padding.
			let pelems = self.1-slice.len();
			buffer.write_zeros(pelems.saturating_mul(ylen).saturating_sub(start))?;
		}
		Some(())
	}
	fn length(&self) -> usize { self.1.saturating_mul(Y::length_const2()) }
}

#[doc(hidden)]
#[derive(Copy,Clone)]
pub struct FragmentZeros(usize);

///Create fragment of specified number of zeros.
pub fn fragment_zeros(size: usize) -> FragmentZeros
{
	FragmentZeros(size)
}

unsafe impl FragmentWrite for FragmentZeros
{
	unsafe fn write_all(&self, ptr: NonNull<u8>) -> NonNull<u8>
	{
		unsafe{ptr.write_bytes(0, self.0);}
		unsafe{ptr.add(self.0)}
	}
	fn write(&self, start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		buffer.write_zeros(self.0.saturating_sub(start))
	}
	fn length(&self) -> usize { self.0 }
}

unsafe impl<T:FragmentWrite> FragmentWrite for Arc<T>
{
	unsafe fn write_all(&self, ptr: NonNull<u8>) -> NonNull<u8>
	{
		unsafe{self.deref().write_all(ptr)}
	}
	fn write(&self, start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		self.deref().write(start, buffer)
	}
	fn length(&self) -> usize { self.deref().length() }
	fn length_const() -> Option<usize> { T::length_const() }
}

unsafe impl<T:FragmentWriteConstantSize> FragmentWriteConstantSize for Arc<T>
{
	fn length_const2() -> usize { T::length_const2() }
}

unsafe impl<T:FragmentWrite> FragmentWrite for Rc<T>
{
	unsafe fn write_all(&self, ptr: NonNull<u8>) -> NonNull<u8>
	{
		unsafe{self.deref().write_all(ptr)}
	}
	fn write(&self, start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		self.deref().write(start, buffer)
	}
	fn length(&self) -> usize { self.deref().length() }
	fn length_const() -> Option<usize> { T::length_const() }
}

unsafe impl<T:FragmentWriteConstantSize> FragmentWriteConstantSize for Rc<T>
{
	fn length_const2() -> usize { T::length_const2() }
}

fn __skip_fragment_array<'a,T:FragmentWrite+'a>(mut start: usize,  mut x: &'a [T]) -> (usize, &'a [T])
{
	if let Some(esize) = T::length_const() {
		//If elements are empty, nothing ever gets written.
		if esize == 0 { return (0,&[]); }
		let offset = start / esize;
		start %= esize;
		x = x.get(offset..).unwrap_or(&[])
	} else {
		//Read size of items.
		while x.len() > 0 {
			let len = x[0].length();
			if start < len { break; }
			start -= len;
			x = &x[1..];
		}
	}
	(start, x)
}

//The reason why this is sound is subtle: FragmentWrite is not allowed to ever alter its length.
#[doc(hidden)]
#[derive(Copy,Clone)]
pub struct FragmentArray<T:FragmentWrite, const N: usize>([T;N], usize);

impl<T:FragmentWrite, const N: usize> FragmentArray<T,N>
{
	#[doc(hidden)]
	pub fn new(x: [T;N]) -> FragmentArray<T,N>
	{
		let len = if let Some(flen) = T::length_const() {
			flen.saturating_mul(N)
		} else {
			x.iter().fold(0usize,|x,y|x.saturating_add(y.length()))
		};
		FragmentArray(x, len)
	}
}

unsafe impl<T:FragmentWrite, const N: usize> FragmentWrite for FragmentArray<T,N>
{
	unsafe fn write_all(&self, mut ptr: NonNull<u8>) -> NonNull<u8>
	{
		for element in self.0.iter() { ptr = unsafe{element.write_all(ptr)}; }
		ptr
	}
	fn write(&self, start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		let (mut start, x) = __skip_fragment_array(start, &self.0);
		for element in x.iter() { chained_write(element, &mut start, buffer)?; }
		Some(())
	}
	fn length(&self) -> usize { self.1 }
}

unsafe impl<T:FragmentWriteConstantSize, const N: usize> FragmentWriteConstantSize for FragmentArray<T,N>
{
	fn length_const2() -> usize { T::length_const2().saturating_mul(N) }
}

//The reason why this is sound is subtle: FragmentWrite is not allowed to ever alter its length.
#[doc(hidden)]
#[derive(Copy,Clone)]
pub struct FragmentSlice<'a,T:FragmentWrite+'a>(&'a [T], usize);

impl<'a,T:FragmentWrite+'a> FragmentSlice<'a,T>
{
	#[doc(hidden)]
	pub fn new(x: &'a [T]) -> FragmentSlice<'a,T>
	{
		let len = if let Some(flen) = T::length_const() {
			flen.saturating_mul(x.len())
		} else {
			x.iter().fold(0usize,|x,y|x.saturating_add(y.length()))
		};
		FragmentSlice(x, len)
	}
}

unsafe impl<'a,T:FragmentWrite+'a> FragmentWrite for FragmentSlice<'a,T>
{
	unsafe fn write_all(&self, mut ptr: NonNull<u8>) -> NonNull<u8>
	{
		for element in self.0.iter() { ptr = unsafe{element.write_all(ptr)}; }
		ptr
	}
	fn write(&self, start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		let (mut start, x) = __skip_fragment_array(start, self.0);
		for element in x.iter() { chained_write(element, &mut start, buffer)?; }
		Some(())
	}
	fn length(&self) -> usize { self.1 }
}

//For soundness, the output type has to have constant length.
#[doc(hidden)]
pub struct FragmentArrayMap<'a,T,U:FragmentWriteConstantSize>(&'a [T], fn(&T) -> U);

//Manually impl Copy and Clone because this can always be Copy+Clone.
impl<'a,T,U:FragmentWriteConstantSize> Copy for FragmentArrayMap<'a,T,U> {}
impl<'a,T,U:FragmentWriteConstantSize> Clone for FragmentArrayMap<'a,T,U>
{
	fn clone(&self) -> Self { *self }
}

///Create fragment out of slice and exponential type.
pub fn fragment_array_map<'a,T,U:FragmentWriteConstantSize>(x: &'a [T], map: fn(&T) -> U) -> FragmentArrayMap<'a,T,U>
{
	FragmentArrayMap(x, map)
}

unsafe impl<'a,T,U:FragmentWriteConstantSize> FragmentWrite for FragmentArrayMap<'a,T,U>
{
	unsafe fn write_all(&self, mut ptr: NonNull<u8>) -> NonNull<u8>
	{
		for element in self.0.iter() { ptr = unsafe{self.1(element).write_all(ptr)}; }
		ptr
	}
	fn write(&self, mut start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		//Skip already written elements.
		let flen = U::length_const2();
		if flen == 0 { return Some(()); }	//Trivial case of zero-length element.
		let offset = start / flen;
		start %= flen;
		if offset < self.0.len() {
			let items = &self.0[offset..];
			for element in items.iter() {
				chained_write(&self.1(element), &mut start, buffer)?;
			}
		}
		Some(())
	}
	fn length(&self) -> usize { self.0.len().saturating_mul(U::length_const2()) }
}

//For soundness, the output type has to have constant length.
#[doc(hidden)]
pub struct FragmentIterator<T:FragmentWriteConstantSize,I:Iterator<Item=T>+Clone>(I, usize);

//Manually impl Clone, because this can always be clone.
impl<T:FragmentWriteConstantSize,I:Iterator<Item=T>+Clone> Clone for FragmentIterator<T, I>
{
	fn clone(&self) -> Self { FragmentIterator(self.0.clone(), self.1) }
}

///Create fragment out of iterator.
pub fn fragment_iterator<T:FragmentWriteConstantSize,I:Iterator<Item=T>+Clone>(x: I) -> FragmentIterator<T,I>
{
	let len = x.clone().count();
	FragmentIterator(x, len)
}

unsafe impl<T:FragmentWriteConstantSize,I:Iterator<Item=T>+Clone> FragmentWrite for FragmentIterator<T,I>
{
	unsafe fn write_all(&self, mut ptr: NonNull<u8>) -> NonNull<u8>
	{
		//For soundness in case the iterator is bad, write at most self.1 elements, and if the iterator
		//has less elements than that, pad with zeroes.
		let mut written = 0;
		for element in self.0.clone().take(self.1) {
			ptr = unsafe{element.write_all(ptr)};
			written += 1;
		}
		let missing = self.1.saturating_sub(written).saturating_mul(T::length_const2());
		unsafe{ptr.write_bytes(0, missing);}
		unsafe{ptr.add(missing)}
	}
	fn write(&self, mut start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		//For soundness in case the iterator is bad, write at most self.1 elements, and if the iterator
		//has less elements than that, pad with zeroes.
		let eltsz = T::length_const2();
		if eltsz == 0 { return Some(()); }	//Nothing is ever written.
		let skip = start / eltsz;
		start %= eltsz;
		let mut ewritten = 0;
		for element in self.0.clone().skip(skip).take(self.1) {
			chained_write(&element, &mut start, buffer)?;
			ewritten += 1;
		}
		let missing = self.1.saturating_sub(ewritten).saturating_mul(eltsz).saturating_sub(start);
		buffer.write_zeros(missing)
	}
	fn length(&self) -> usize { self.1.saturating_mul(T::length_const2()) }
}

#[doc(hidden)]
#[derive(Copy,Clone)]
pub struct FragmentRepeat<T:FragmentWrite>(usize, T);

///Repat fragment.
pub fn fragment_repeat<T:FragmentWrite>(repeats: usize, f: T) -> FragmentRepeat<T>
{
	FragmentRepeat(repeats, f)
}

unsafe impl<T:FragmentWrite> FragmentWrite for FragmentRepeat<T>
{
	unsafe fn write_all(&self, ptr: NonNull<u8>) -> NonNull<u8>
	{
		let len = self.1.length();
		if self.0 > 0 {	//Do not write anything if repeat count is 0.
			unsafe{self.1.write_all(ptr);}
			//All the repeats are assumed identical, copy them.
			for i in 1..self.0 {
				unsafe{ptr.add(i * len).copy_nonoverlapping(ptr.to_const(), len);}
			}
		}
		unsafe{ptr.add(self.0 * len)}
	}
	fn write(&self, mut start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		let eltsz = self.1.length();
		if eltsz == 0 { return Some(()); }	//Eltsz == 0, nothing to write.
		let offset = start / eltsz;
		start %= eltsz;
		//The elements pre-start should be skipped.
		for _ in offset..self.0 { chained_write(&self.1, &mut start, buffer)?; }
		Some(())
	}
	fn length(&self) -> usize { self.0.saturating_mul(self.1.length()) }
}

#[doc(hidden)]
#[derive(Copy,Clone)]
pub struct FragmentVector<T:FragmentWrite>
{
	pfxlen: u8,
	prefix: [u8;23],	//Worst case 20=(11+9) bytes.
	value: T,
}

impl<T:FragmentWrite> FragmentVector<T>
{
	fn __get_prefix(&self) -> &[u8]
	{
		let l = self.pfxlen as usize;
		self.prefix.get(..l).unwrap_or(&self.prefix)
	}
}

fn __encode_asn1_tag(prefix: &mut [u8], pfxlen: &mut usize, base: u64, tval: u64)
{
	//Save position to temporary, as write_byte_seq! does not like references.
	let mut pfxlen2 = *pfxlen;
	let tvalz = tval.leading_zeros();
	let f = min(tval, 31);
	write_byte_seq!(prefix, pfxlen2, base|f);
	for j in 0..9 { if tvalz <= 7*j { write_byte_seq!(prefix, pfxlen2, 128|tval>>63-7*j); } }
	if tval > 30 { write_byte_seq!(prefix, pfxlen2, tval&127); }
	*pfxlen = pfxlen2;
}

fn __encode_asn1_length(prefix: &mut [u8], pfxlen: &mut usize, vlen: u64)
{
	//Save position to temporary, as write_byte_seq! does not like references.
	let mut pfxlen2 = *pfxlen;
	let vlenz = vlen.leading_zeros();
	if vlenz < 57 {
		write_byte_seq!(prefix, pfxlen2, 136-vlenz/8);
		for j in 1..8 { if vlenz < 8*j { write_byte_seq!(prefix, pfxlen2, vlen>>64-8*j); } }
	}
	write_byte_seq!(prefix, pfxlen2, vlen);
	*pfxlen = pfxlen2;
}

unsafe impl<T:FragmentWrite> FragmentWrite for FragmentVector<T>
{
	unsafe fn write_all(&self, mut ptr: NonNull<u8>) -> NonNull<u8>
	{
		ptr = unsafe{self.__get_prefix().write_all(ptr)};
		ptr = unsafe{self.value.write_all(ptr)};
		ptr
	}
	fn write(&self, mut start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		chained_write(self.__get_prefix(), &mut start, buffer)?;
		chained_write(&self.value, &mut start, buffer)?;
		Some(())
	}
	fn length(&self) -> usize
	{
		(self.pfxlen as usize).saturating_add(self.value.length())
	}
}

#[doc(hidden)] pub trait __WriteLength
{
	type Out: AsRef<[u8]>;
	fn length(x: usize) -> <Self as __WriteLength>::Out;
	fn llength() -> usize;
	fn limit() -> usize;
	fn has_ext() -> bool;
}

#[doc(hidden)] pub struct __Length8;
#[doc(hidden)] pub struct __Length16;
#[doc(hidden)] pub struct __Length24;
#[doc(hidden)] pub struct __LengthT13;

impl __WriteLength for __Length8
{
	type Out = [u8;1];
	fn length(x: usize) -> [u8;1] { (x as u8).to_be_bytes() }
	fn llength() -> usize { 1 }
	fn limit() -> usize { (1 << 8) - 1 }
	fn has_ext() -> bool { false }
}

impl __WriteLength for __Length16
{
	type Out = [u8;2];
	fn length(x: usize) -> [u8;2] { (x as u16).to_be_bytes() }
	fn llength() -> usize { 2 }
	fn limit() -> usize { (1 << 16) - 1 }
	fn has_ext() -> bool { false }
}

impl __WriteLength for __Length24
{
	type Out = [u8;3];
	fn length(x: usize) -> [u8;3] { [(x >> 16) as u8, (x >> 8) as u8, x as u8] }
	fn llength() -> usize { 3 }
	fn limit() -> usize { (1 << 24) - 1 }
	fn has_ext() -> bool { false }
}

impl __WriteLength for __LengthT13
{
	type Out = [u8;3];
	fn length(x: usize) -> [u8;3] { [(x >> 16) as u8, (x >> 8) as u8, x as u8] }
	fn llength() -> usize { 3 }
	fn limit() -> usize { (1 << 24) - 1 }
	fn has_ext() -> bool { true }
}


#[doc(hidden)]
pub struct FragmentListOctets<'a, T:TrustedInvariantLength+'a, L:__WriteLength>(&'a [T], usize, PhantomData<L>);

//Manually impl Copy/Clone because indpendence of L.
impl<'a, T:TrustedInvariantLength+'a, L:__WriteLength> Copy for FragmentListOctets<'a,T,L> {}
impl<'a, T:TrustedInvariantLength+'a, L:__WriteLength> Clone for FragmentListOctets<'a,T,L>
{
	fn clone(&self) -> Self { *self }
}

fn __calculate_length_of_list<T:TrustedInvariantLength>(x: &[T], limit: usize, overhead: usize) ->
	Result<usize, (usize, usize)>
{
	let mut len = 0usize;
	for (idx, ent) in x.iter().enumerate() {
		let elen = ent.as_byte_slice().len();
		fail_if!(elen < 1 || elen > limit, (idx + 1, elen));
		len = len.saturating_add(elen + overhead);
	}
	Ok(len)
}

///Create new list of ALPNs.
pub fn fragment_alpn_list<'a,T:TrustedInvariantLength+'a>(x: &'a [T]) ->
	Result<FragmentListOctets<'a,T,__Length8>, (usize, usize)>
{
	let len = __calculate_length_of_list(x, 0xFF, 1)?;
	Ok(FragmentListOctets(x, len, PhantomData))
}

///Create new list of SCTs.
pub fn fragment_sct_list<'a, T:TrustedInvariantLength+'a>(x: &'a [T]) ->
	Result<FragmentListOctets<'a,T,__Length16>, (usize, usize)>
{
	let len = __calculate_length_of_list(x, 0xFFFF, 2)?;
	Ok(FragmentListOctets(x, len, PhantomData))
}

///Create new list of TLS 1.2 certificates.
pub fn fragment_tls12_cert_list<'a, T:TrustedInvariantLength+'a>(x: &'a [T]) ->
	Result<FragmentListOctets<'a,T,__Length24>, (usize, usize)>
{
	let len = __calculate_length_of_list(x, 0xFFFFFF, 3)?;
	Ok(FragmentListOctets(x, len, PhantomData))
}

///Create new list of TLS 1.3 certificates.
pub fn fragment_tls13_cert_list<'a, T:TrustedInvariantLength+'a>(x: &'a [T]) ->
	Result<FragmentListOctets<'a,T,__LengthT13>, (usize, usize)>
{
	let len = __calculate_length_of_list(x, 0xFFFFFF, 5)?;	//3+2 bytes of overhead.
	Ok(FragmentListOctets(x, len, PhantomData))
}


static DUMMY_EXT: &'static [u8] = &[0;2];

unsafe impl<'a, T:TrustedInvariantLength+'a, L:__WriteLength> FragmentWrite for FragmentListOctets<'a,T,L>
{
	unsafe fn write_all(&self, mut ptr: NonNull<u8>) -> NonNull<u8>
	{
		for item in self.0.iter() {
			let item = item.as_byte_slice();
			let hdr = L::length(item.len());
			ptr = unsafe{hdr.as_ref().write_all(ptr)};
			ptr = unsafe{item.write_all(ptr)};
			//Dummy extensions block if needed.
			if L::has_ext() { ptr = unsafe{DUMMY_EXT.write_all(ptr)}; }
		}
		ptr
	}
	fn write(&self, mut start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		let overhead = L::llength() + (if L::has_ext() { 2 } else { 0 });
		let mut x = self.0;
		//Fast skip elements that are before start.
		while x.len() > 0 {
			let length = overhead + x[0].as_byte_slice().len();
			if start < length { break; }
			start -= length;
			x = &x[1..];
		}
		for item in x.iter() {
			let item = item.as_byte_slice();
			let hdr = L::length(item.len());
			chained_write(hdr.as_ref(), &mut start, buffer)?;
			chained_write(item, &mut start, buffer)?;
			if L::has_ext() { chained_write(DUMMY_EXT, &mut start, buffer)?; }
		}
		Some(())
	}
	fn length(&self) -> usize { self.1 }
}

///KEM public key.
///
///The `get_group()` method returns the group ID for the KEM public key. The actual KEM public key is returned
///by the `as_byte_slice()` method from the [`TrustedInvariantLength`] supertrait.
///
///[`TrustedInvariantLength`]: trait.TrustedInvariantLength.html
pub trait KemPublicKey: TrustedInvariantLength
{
	///Get group.
	fn get_group(&self) -> u16;
}

#[doc(hidden)]
pub struct FragmentPkList<'a, T:KemPublicKey+'a>(&'a [T], usize);

//Manually impl Copy/Clone because indpendence of T.
impl<'a, T:KemPublicKey+'a> Copy for FragmentPkList<'a, T> {}
impl<'a, T:KemPublicKey+'a> Clone for FragmentPkList<'a, T>
{
	fn clone(&self) -> Self { *self }
}

///Create a new fragment of key shares.
pub fn fragment_key_shares<'a,T:KemPublicKey+'a>(keys: &'a [T]) -> Result<FragmentPkList<'a, T>, (usize, usize)>
{
	let mut len = 0usize;
	for (idx, ent) in keys.iter().enumerate() {
		let elen = ent.as_byte_slice().len();
		fail_if!(elen < 1 || elen > 65535, (idx + 1, elen));
		len = len.saturating_add(elen + 4);
	}
	Ok(FragmentPkList(keys, len))
}

impl<'a, T:KemPublicKey+'a> FragmentPkList<'a, T>
{
	fn __get_item(item: &T) -> ([u8;4], &[u8])
	{
		let grpid = item.get_group();
		let item = item.as_byte_slice();
		let ilen = item.len();
		let hdr: [u8;4] = [(grpid >> 8) as u8, grpid as u8, (ilen >> 8) as u8, ilen as u8];
		(hdr, item)
	}
}

unsafe impl<'a, T:KemPublicKey+'a> FragmentWrite for FragmentPkList<'a, T>
{
	unsafe fn write_all(&self, mut ptr: NonNull<u8>) -> NonNull<u8>
	{
		for item in self.0.iter() {
			let (hdr, item) = Self::__get_item(item);
			ptr = unsafe{hdr.write_all(ptr)};
			ptr = unsafe{item.write_all(ptr)};
		}
		ptr
	}
	fn write(&self, mut start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		let mut x = self.0;
		//Fast skip elements that are before start. The overhead is always 4 bytes.
		while x.len() > 0 {
			let length = x[0].as_byte_slice().len() + 4;
			if start < length { break; }
			start -= length;
			x = &x[1..];
		}
		for item in x.iter() {
			let (hdr, item) = Self::__get_item(item);
			chained_write(&hdr, &mut start, buffer)?;
			chained_write(item, &mut start, buffer)?;
		}
		Some(())
	}
	fn length(&self) -> usize { self.1 }
}

macro_rules! msb128s
{
	($val:expr) => {{
		let mut t = [0;12];	//10 bytes in worst case.
		let mut i = 0;
		let z = $val.leading_zeros();
		for j in 0..9 { if z <= 7*j { write_byte_seq!(t, i, 128|$val>>63-7*j); } }
		write_byte_seq!(t, i, $val&127);
		(t,i)
	}}
}

#[derive(Copy,Clone)]
struct FragmentMsb128(u64);

unsafe impl FragmentWrite for FragmentMsb128
{
	unsafe fn write_all(&self, mut ptr: NonNull<u8>) -> NonNull<u8>
	{
		let (t,i) = msb128s!(self.0);
		ptr = unsafe{t[..i].write_all(ptr)};
		ptr
	}
	fn write(&self, start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		let (t,i) = msb128s!(self.0);
		t[..i].write(start, buffer)
	}
	fn length(&self) -> usize
	{
		if self.0 == 0 { return 1; }
		(70-self.0.leading_zeros() as usize)/7
	}
}

#[doc(hidden)]
#[derive(Copy,Clone)]
pub struct FragmentAsn1Oid<'a>(u64,&'a [u64],usize);

///Fragment ASN.1 OID
pub fn fragment_asn1_oid<'a>(x: &'a [u64]) -> FragmentAsn1Oid<'a>
{
	let tail = x.get(2..).unwrap_or(&[]);
	let head = if x.len() >= 2 { x[0] * 40 + x[1] } else { x.get(0).map(|x|*x).unwrap_or(0) };
	let len = FragmentMsb128(head).length() + tail.iter().
		fold(0usize,|x,&y|x.saturating_add(FragmentMsb128(y).length()));
	FragmentAsn1Oid(head,tail,len)
}

unsafe impl<'a> FragmentWrite for FragmentAsn1Oid<'a>
{
	unsafe fn write_all(&self, mut ptr: NonNull<u8>) -> NonNull<u8>
	{
		ptr = unsafe{FragmentMsb128(self.0).write_all(ptr)};
		for &e in self.1.iter() { ptr = unsafe{FragmentMsb128(e).write_all(ptr)}; }
		ptr
	}
	fn write(&self, mut start: usize, buffer: &mut BackedVector<u8>) -> Option<()>
	{
		chained_write(&FragmentMsb128(self.0), &mut start, buffer)?;
		for &e in self.1.iter() { chained_write(&FragmentMsb128(e), &mut start, buffer)?; }
		Some(())
	}
	fn length(&self) -> usize { self.2 }
}




#[doc(hidden)]
pub fn __private_asn1_tlv<T:FragmentWrite>(tag: __PRIVATE_ASN1_TAG, value: T) -> FragmentVector<T>
{
	let (mut base, constructed, tval) = match tag {
		__PRIVATE_ASN1_TAG::Universal(v, c) => (0x00, c, v),
		__PRIVATE_ASN1_TAG::Application(v, c) => (0x40, c, v),
		__PRIVATE_ASN1_TAG::ContextSpecific(v, c) => (0x80, c, v),
		__PRIVATE_ASN1_TAG::Private(v, c) => (0xC0, c, v),
	};
	if constructed { base |= 0x20; }
	let mut prefix = [0;23];
	let mut pfxlen = 0;
	__encode_asn1_tag(&mut prefix, &mut pfxlen, base, tval);
	__encode_asn1_length(&mut prefix, &mut pfxlen, value.length() as u64);
	FragmentVector{pfxlen: pfxlen as u8, prefix, value}
}

#[doc(hidden)]
pub fn __private_asn1_tlv_small<T:FragmentWrite>(tagb: u8, value: T) -> FragmentVector<T>
{
	let mut prefix = [0;23];
	prefix[0] = tagb;
	let mut pfxlen = 1;
	__encode_asn1_length(&mut prefix, &mut pfxlen, value.length() as u64);
	FragmentVector{pfxlen: pfxlen as u8, prefix, value}
}

#[doc(hidden)]
pub fn __private_asn1_boolean(value: bool) -> [u8;3]
{
	let value = if value { 255 } else { 0 };
	[1,1,value]	//BOOLEAN.
}

#[doc(hidden)]
pub fn __private_asn1_bstrip_bit_string<T:FragmentWrite>(bstrip: u8, value: T) -> FragmentVector<T>
{
	let mut prefix = [0;23];
	prefix[0] = 3;		//BIT STRING.
	let mut pfxlen = 1;
	//Add +1 to length for the bit strip.
	__encode_asn1_length(&mut prefix, &mut pfxlen, value.length() as u64 + 1);
	write_byte_seq!(prefix, pfxlen, bstrip);
	FragmentVector{pfxlen: pfxlen as u8, prefix, value}
}

#[doc(hidden)]
pub fn __private_asn1_bit_string<T:FragmentWrite>(value: T) -> FragmentVector<T>
{
	__private_asn1_bstrip_bit_string(0, value)
}


///Construct a fragment buffer that is sequence of given fragment buffers.
///
///I.e., concatenates 1 or more fragment buffers.
///
///All parts must implement `FragmentWrite`. The result implements `FragmentWrite`.
///
///The result implements `Copy` iff all the parts implement `Copy`. Similarly for `Clone`.
#[macro_export]
macro_rules! fragment_sequence
{
	($($x:expr),*,) => { fragment_sequence!($($x)*) };
	() => { () };
	($a:expr) => { $a };
	($head:expr,$($tail:expr),*) => { $crate::FragmentSequence::new($head, fragment_sequence!($($tail),*)) };
}


/*

/*******************************************************************************************************************/

#[cfg(test)] mod test;
*/
