use std::env::var;
use std::fs::File;
use std::io::Write;
use std::path::Path;

fn main()
{
	use ::std::io::Read as IR;
	let mut override_version = String::new();
	::std::fs::File::open("version.override").and_then(|mut fp|fp.read_to_string(&mut override_version)).ok();

	let out_dir = var("OUT_DIR").unwrap();
	let product = var("CARGO_PKG_NAME").unwrap();
	let version = var("CARGO_PKG_VERSION").unwrap();
	let version = if override_version.len() > 0 { override_version } else { version };
	let dest_path = Path::new(&out_dir).join("product.inc");
	let mut f = File::create(&dest_path).unwrap();
	writeln!(f, r#"static PRODUCT_NAME: &'static str = "{product}";"#).unwrap();
	writeln!(f, r#"static PRODUCT_VERSION: &'static str = "{version}";"#, version=version.trim()).unwrap();

	println!("cargo:rerun-if-changed=build.rs");
}
