extern crate btls_aux_hash;
use self::btls_aux_hash::Checksum;
use self::session::handle_peer;
pub use self::session::TryClone;
use super::transport::TransportListener;
use btls::ServerConfiguration;
use btls::callbacks::ClientCertificateInfo;
use btls::callbacks::CryptographicParametersInfo;
use btls::callbacks::PointInTime;
use btls::callbacks::RequestCertificateParameters;
use btls::callbacks::ServerNamesParameters;
use btls::callbacks::TlsCallbacks;
use btls::certificates::CertificateLookup;
use btls::logging::StderrLog;
use btls::test::yesno;
use btls::utility::Mutex;
use btls_aux_memory::Hexdump;
use std::env::var;
use std::io::Write as IoWrite;
use std::io::stderr;
use std::str::FromStr;
use std::sync::Arc;

#[path="./session.rs"]
mod session;

struct Controller
{
	irq: session::Controller,
}

impl Controller
{
	fn new() -> Controller { Controller{irq: session::Controller::new()} }
}

impl session::GetController for Controller
{
	fn get<'a>(&'a mut self) -> &'a mut session::Controller { &mut self.irq }
}

impl TlsCallbacks for Controller
{
	fn client_authentication(&mut self, info: &ClientCertificateInfo) -> bool
	{
		self.on_certificate_auth(&info);
		true
	}
	fn request_certificate(&mut self, _params: RequestCertificateParameters) -> bool
	{
		//Always request client certificate if possible.
		true
	}
	fn server_names(&mut self, params: ServerNamesParameters) { self.irq.server_names(params); }
	fn cryptographic_parameters(&mut self, info: CryptographicParametersInfo)
	{
		self.irq.cryptographic_parameters(info)
	}
	fn set_deadline(&mut self, dl: Option<PointInTime>) { self.irq.set_deadline(dl); }
}

impl Controller
{
	fn on_certificate_auth(&mut self, info: &ClientCertificateInfo)
	{
		let spki_hash = info.get_public_key_hash2(Checksum::Sha256);
		let ee_hash = info.get_ee_certificate_hash2(Checksum::Sha256);
		let name = info.get_subject();
		if spki_hash.is_none() {
			writeln!(stderr(), "Client did not send a certificate.").unwrap();
			return;
		}
		writeln!(stderr(), "Client sent a certificate:").unwrap();
		writeln!(stderr(), "OCSP:             {status}", status=yesno(info.is_ocsp_stapled())).unwrap();
		writeln!(stderr(), "CT:               {status}", status=yesno(info.is_sct_stapled())).unwrap();
		if let Some(spki_hash) = spki_hash {
			writeln!(stderr(), "Key hash:         {hash}", hash=Hexdump(&spki_hash)).unwrap();
		} else {
			writeln!(stderr(), "Key hash:         N/A").unwrap();
		}
		if let Some(ee_hash) = ee_hash {
			writeln!(stderr(), "Certificate hash: {hash}", hash=Hexdump(&ee_hash)).unwrap();
		} else {
			writeln!(stderr(), "Certificate hash: N/A").unwrap();
		}
		if let Some(name) = name {
			writeln!(stderr(), "Subject:          {subj}", subj=Hexdump(&name)).unwrap();
		} else {
			writeln!(stderr(), "Subject:          N/A").unwrap();
		}
	}
}

pub fn do_it(acceptsock: TransportListener, store: CertificateLookup)
{
	let mut dbgflags = 0;
	let mut server_config = ServerConfiguration::from(store);
	if let Ok(flags) = var("BTLS_DEBUG") { dbgflags = u64::from_str(&flags).unwrap(); }
	server_config.set_debug(dbgflags, Box::new(StderrLog));
	if let Ok(settings) = var("BTLS_SETTINGS") {
		server_config.config_flags(&settings, |error|{writeln!(stderr(), "{error}").unwrap();});
	}
	writeln!(stderr(), "Waiting for connection...").unwrap();
	let mut sock = None;
	while sock.is_none() {
		let (sock2, _) = match acceptsock.accept() {
			Ok(x) => x,
			Err(err) => {
				writeln!(stderr(), "Can't accept connection: {err}").unwrap();
				continue;
			}
		};
		sock = Some(sock2);
	}
	let sock = sock.unwrap();
	writeln!(stderr(), "Received connection...").unwrap();
	let controller = Arc::new(Mutex::new(Controller::new()));
	let mut server_session = server_config.make_connection(Box::new(controller.clone()));
	server_session.set_source_address(sock.source_ip()).unwrap();
	server_session.set_destination_address(sock.destination_ip()).unwrap();
	handle_peer(server_session, sock, "Server", controller);
}
