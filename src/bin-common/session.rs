use btls::Connection;
use btls::callbacks::CryptographicParametersInfo;
use btls::callbacks::PointInTime;
use btls::callbacks::ServerNamesParameters;
use btls::callbacks::TlsCallbacks;
use btls::test::yesno;
use btls::transport::PullTcpBuffer;
use btls::transport::PushTcpBuffer;
use btls::utility::Mutex;
use std::io::ErrorKind;
use std::io::Read as IoRead;
use std::io::stdin;
use std::io::stdout;
use std::io::stderr;
use std::io::Result as IoResult;
use std::io::Write as IoWrite;
use std::mem::replace;
use std::thread::spawn;
use std::panic::catch_unwind;
use std::panic::UnwindSafe;
use std::panic::AssertUnwindSafe;
use std::sync::mpsc::channel as mpsc_channel;
use std::sync::mpsc::Receiver as MpscReceiver;
use std::sync::mpsc::RecvTimeoutError;
use std::sync::mpsc::Sender as MpscSender;
use std::sync::Arc;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering as MemOrder;
use std::time::Duration;

const EV_STDIN_EOF: u32 = 0;
const EV_STDIN_ENDTASK: u32 = 1;
const EV_STDOUT_EOF: u32 = 2;
const EV_STDOUT_ENDTASK: u32 = 3;
const EV_HS_FINISHED: u32 = 4;
const EV_BROKEN_PIPE: u32 = 5;
const EV_HS_TIMEOUT: u32 = 6;

#[path="./circbuf.rs"]
mod circbuf;

use self::circbuf::CircularBuffer;

pub trait TryClone: Sized
{
	fn try_clone_trait(&self) -> IoResult<Self>;
}

pub trait GetController: TlsCallbacks+Send+'static
{
	fn get<'a>(&'a mut self) -> &'a mut Controller;
}

pub struct Controller
{
	crypto_parameters: Option<CryptographicParametersInfo>,
	deadline: Option<PointInTime>,
}

impl Controller
{
	pub fn new() -> Controller
	{
		Controller{
			crypto_parameters: None,
			deadline: None,
		}
	}
	pub fn server_names(&mut self, params: ServerNamesParameters)
	{
		for i in params.dns_names.iter() {
			writeln!(stderr(), "Server certificate valid for: {i}").unwrap();
		}
		for i in params.ip_addresses.iter() {
			writeln!(stderr(), "Server certificate valid for IP {i}").unwrap();
		}
	}
	pub fn cryptographic_parameters(&mut self, info: CryptographicParametersInfo)
	{
		self.crypto_parameters = Some(info);
	}
	pub fn set_deadline(&mut self, deadline: Option<PointInTime>) { self.deadline = deadline; }
	//Err() means timeout, Ok(None) means no timeout, otherwise Ok(Some(x)) for deadline x in future.
	fn time_left(&self) -> Result<Option<Duration>, ()>
	{
		match self.deadline {
			Some(dl) => match dl.time_to() {
				Some(to) => Ok(Some(to)),	//Timeout.
				None => Err(()),		//Already timed out!
			},
			None => Ok(None),			//No timeout.
		}
	}
}

macro_rules! handle_write_error
{
	($err:expr, $lost:expr) => {
		if $err.kind() == ErrorKind::BrokenPipe {
			writeln!(stderr(), "<Peer has gone away>").unwrap();
			$lost.store(true, MemOrder::SeqCst);
			break;
		} else if $err.kind() == ErrorKind::WouldBlock || $err.kind() == ErrorKind::Interrupted {
			0usize
		} else {
			$lost.store(true, MemOrder::SeqCst);
			let err = $err;
			writeln!(stderr(), "<Error writing to peer: {err}>").unwrap();
			break;
		}
	}
}

struct IrqSend(MpscSender<()>);

impl btls::transport::WaitBlockCallback for IrqSend
{
	fn unblocked(&mut self) { self.0.send(()).ok(); }
}

fn stdin_to_tls_task_v2<Socktype:IoRead+IoWrite+TryClone+Send+UnwindSafe+'static,
	Sess:Connection+Clone+Send+UnwindSafe+'static>(mut wsession: Sess, mut wsock: Socktype,
	irq_sender: MpscSender<()>, irq_receiver: MpscReceiver<()>, exit_chan: MpscSender<u32>,
	connection_lost: Arc<AtomicBool>)
{
	let mut stdin_eof = false;
	let mut stdin_eof_ack2 = false;
	let mut stdin_buf = [0; 32768];
	let mut fin_ack = false;
	loop {
		let flags = wsession.get_status();
		let aborted = flags.is_dead();
		let rts = flags.is_may_tx();
		let stdin_eof_ack = flags.is_tx_end();
		//Finished handling.
		if !flags.is_handshaking() && !replace(&mut fin_ack, true) {
			exit_chan.send(EV_HS_FINISHED).unwrap();
		}
		//If all data that will be sent is sent, signal we are ready for exit.
		if stdin_eof_ack && !replace(&mut stdin_eof_ack2, true) { exit_chan.send(EV_STDIN_EOF).unwrap(); }
		//If connection has been torn down, exit the loop.
		if aborted { break; }
		//Past this point, we know that the output buffer is empty.

		//If stdin buffer is empty, EOF has not been received from stdin and data is ready to send,
		//read from stdin. On transient errors, restart the loop to try again.
		if !stdin_eof && rts {
			let tosend = match stdin().read(&mut stdin_buf) {
				Ok(0) => {
					stdin_eof = true;
					PullTcpBuffer::no_data().eof()
				},
				Ok(x) => PullTcpBuffer::write(&stdin_buf[..x]),
				Err(err) => if err.kind() == ErrorKind::WouldBlock ||
					err.kind() == ErrorKind::Interrupted {
					continue
				} else {
					writeln!(stderr(), "<Error reading from stdin: {err}>").unwrap();
					break;
				}
			};
			match wsession.pull_tcp_data(tosend) {
				Ok(buf) => if let Err(e) = wsock.write_all(&buf) {
					handle_write_error!(e, connection_lost);
				},
				Err(err) => {
					writeln!(stderr(), "<Error in pull_tcp_data: {err}>").unwrap();
					break;
				},
			}
		}
		//Otherwise wait for next IRQ to avoid burning CPU and then fil the record buffer with no output.
		let flags = wsession.get_status();
		if !flags.is_want_tx() {
			let mut blocking = true;
			if flags.is_blocked() {
				blocking &= wsession.register_on_unblock(Box::new(IrqSend(irq_sender.clone())));
			}
			//Block until next IRQ.
			if blocking { irq_receiver.recv().ok(); }
		}
		match wsession.pull_tcp_data(PullTcpBuffer::no_data()) {
			Ok(buf) => if let Err(e) = wsock.write_all(&buf) {
				handle_write_error!(e, connection_lost);
			},
			Err(err) => {
				writeln!(stderr(), "<Error in pull_tcp_data: {err}>").unwrap();
				break;
			},
		}
	}
}

fn tls_to_stdout_task_v2<Socktype:IoRead+IoWrite+TryClone+Send+UnwindSafe+'static,
	Sess:Connection+Clone+Send+UnwindSafe+'static>(mut rsession: Sess, mut rsock: Socktype,
	irq_send: MpscSender<()>, exit_chan: MpscSender<u32>, connection_lost: Arc<AtomicBool>)
{
	let mut buffer = CircularBuffer::new(65536);
	let mut stdout_eof = false;
	let mut stdout_eof_ack = false;
	let mut fin_ack = false;
	loop {
		let flags = rsession.get_status();
		//If connection is aborted, end loop.
		if flags.is_dead() { break; }
		//Finished handling.
		if !flags.is_handshaking() && !replace(&mut fin_ack, true) {
			exit_chan.send(EV_HS_FINISHED).unwrap();
		}
		//Read from socket into circular bufer.
		let amount = {
			let wslice = buffer.get_write(16384 + 2048 + 5).unwrap();
			match rsock.read(wslice) {
				Ok(0) => {
					writeln!(stderr(), "<Peer has gone away>").unwrap();
					connection_lost.store(true, MemOrder::SeqCst);
					break;
				},
				Ok(x) => x,
				Err(err) => {
					if err.kind() == ErrorKind::BrokenPipe {
						writeln!(stderr(), "<Peer has gone away>").unwrap();
						connection_lost.store(true, MemOrder::SeqCst);
						break;
					} else if err.kind() == ErrorKind::WouldBlock ||
						err.kind() == ErrorKind::Interrupted {
						0usize
					} else {
						writeln!(stderr(), "<Error reading from peer: {err}>").unwrap();
						connection_lost.store(true, MemOrder::SeqCst);
						break;
					}
				}
			}
		};
		if amount == 0 { continue; }
		buffer.put_write(amount).unwrap();
		//Pass the data to TLS.
		let len = buffer.data_remaining();
		let tcpslice = buffer.get_read(len).unwrap();
		let (tmp, eofd) = rsession.push_tcp_data(PushTcpBuffer::u8(tcpslice)).expect("push_tcp_data failed");
		buffer.put_read(len).unwrap();
		if tmp.len() > 0 {
			let mut itr2 = 0;
			//Dump this into stdout already.
			while itr2 < tmp.len() {
				let amt = stdout().write(&tmp[itr2..]).unwrap();
				itr2 += amt;
			}
		}
		stdout_eof |= eofd;
		let flags = rsession.get_status();
		//Send TX interrupt. It is not disaster if this races.
		if flags.is_want_tx() { irq_send.send(()).ok(); }
		if stdout_eof && !stdout_eof_ack {
			writeln!(stderr(), "<End of data from peer>").unwrap();
			exit_chan.send(EV_STDOUT_EOF).unwrap();	//The input direction saw EOF.
			stdout_eof_ack = true;
		}
	}
}


fn show_conninfo(conninfo: &CryptographicParametersInfo)
{
	writeln!(stderr(), "Handshake complete, crypto parameters:").unwrap();
	let &CryptographicParametersInfo{version_str, protection_str, hash_str, kex_str, record_overhead,
		signature_str, ths_fixed, validated_ocsp, validated_ocsp_shortlived, validated_ct, version, ..} =
		conninfo;
	if version < 4 {	//1.2 has two variants with THS.
		let ths_status = if ths_fixed { "THS attack fixed" } else { "VULERNABLE to THS attack" };
		writeln!(stderr(), " - TLS version:    {version_str} ({ths_status})").unwrap();
	} else {
		writeln!(stderr(), " - TLS version:    {version_str}").unwrap();
	}
	writeln!(stderr(), " - Encryption:     {protection_str} with {hash_str} using {kex_str} \
		(+{record_overhead} bytes/record)").unwrap();
	writeln!(stderr(), " - Authentication: {signature_str} (OCSP: {ocsp}, CT: {ct})",
		ocsp=if validated_ocsp_shortlived { "Shortlived" } else { yesno(validated_ocsp) },
		ct=yesno(validated_ct)).unwrap();
}

pub fn handle_peer<Socktype:IoRead+IoWrite+TryClone+Send+UnwindSafe+'static,
	Sess:Connection+Clone+Send+UnwindSafe+'static, Ctrl:GetController>(
	session: Sess, sock: Socktype, _side: &str, controller: Arc<Mutex<Ctrl>>)
{
	let (send, recv) = mpsc_channel();
	let (chan_tx1, chan_rx) = mpsc_channel();
	let chan_tx2 = chan_tx1.clone();
	let chan_tx1b = chan_tx1.clone();
	let chan_tx2b = chan_tx2.clone();
	let _chan_tx_backup = chan_tx1.clone();

	let send1 = send.clone();

	let connection_lost = Arc::new(AtomicBool::new(false));
	let connection_lost1 = connection_lost.clone();
	let connection_lost2 = connection_lost.clone();

	let rsession = session.clone();
	let wsession = session.clone();
	let rsock = sock.try_clone_trait().unwrap();
	let wsock = sock.try_clone_trait().unwrap();

	//The threads can immediately drive the handshake, except for the timeout and showing crypto parameters,
	//which needs separate handling. Immediately drop the handles to detech them.
	spawn(move ||{
		match catch_unwind(AssertUnwindSafe(move ||{
			tls_to_stdout_task_v2(rsession, rsock, send1, chan_tx1, connection_lost1);
		})) {
			Ok(_) => (),
			Err(_) => writeln!(stderr(), "<Read thread paniced>").unwrap()
		};
		chan_tx1b.send(EV_STDOUT_ENDTASK).unwrap();	//Crashed or exited
	});
	spawn(move ||{
		match catch_unwind(AssertUnwindSafe(move ||{
			stdin_to_tls_task_v2(wsession, wsock, send, recv, chan_tx2, connection_lost2);
		})) {
			Ok(_) => (),
			Err(_) => writeln!(stderr(), "<Write thread paniced>").unwrap()
		};
		chan_tx2b.send(EV_STDIN_ENDTASK).unwrap();	//Crashed or exited.
	});
	//Loop handling events from threads.
	let mut crypto_shown = false;
	let mut stdin_eof = false;
	let mut stdout_eof = false;
	let mut timeout = false;
	loop {
		let left = controller.with(|x|x.get().time_left());
		let event = match left {
			//Wait for event, but only up to specified bound.
			Ok(Some(x)) => match chan_rx.recv_timeout(x) {
				Ok(x) => x,
				Err(RecvTimeoutError::Timeout) => continue,	//Try again.
				Err(RecvTimeoutError::Disconnected) => EV_BROKEN_PIPE,
			},
			//Wait for event indefinitely.
			Ok(None) => match chan_rx.recv() {
				Ok(x) => x,
				Err(_) => EV_BROKEN_PIPE,
			},
			//Timeout exceeded.
			Err(_) => EV_HS_TIMEOUT
		};
		timeout |= event == EV_HS_TIMEOUT;
		if event == EV_HS_FINISHED && !crypto_shown {
			controller.with(|x|x.get().crypto_parameters.as_ref().map(|x|show_conninfo(x)));
			crypto_shown = true;
		}
		//If any tasks exit or if timeout occurs, exit ourselves.
		if event == EV_BROKEN_PIPE || event == EV_STDIN_ENDTASK || event == EV_STDOUT_ENDTASK ||
			event == EV_HS_TIMEOUT {
			break;
		}
		stdin_eof |= event == EV_STDIN_EOF;
		stdout_eof |= event == EV_STDOUT_EOF;
		//If EOFs are seen in both directions, exit.
		if stdin_eof && stdout_eof { break; }
	}

	let flags = session.get_status();
	if flags.is_dead() {
		if let Some(err) = session.get_error() {
			if !flags.is_handshaking() {
				writeln!(stderr(), "<Connection error: {err}>")
			} else {
				writeln!(stderr(), "<Handshake error: {err}>")
			}
		} else {
			writeln!(stderr(), "<Normal close>")
		}
	} else if connection_lost.load(MemOrder::SeqCst) {
		writeln!(stderr(), "<Connection to peer lost>")
	} else if timeout {
		writeln!(stderr(), "<Handshake timed out>")
	} else {
		writeln!(stderr(), "<Connection ended with no cause???>")
	}.ok();
}
