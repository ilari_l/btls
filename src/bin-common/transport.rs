extern crate btls_aux_ip;
extern crate btls_aux_memory;
use self::btls_aux_ip::IpAddress;
use self::btls_aux_memory::ByteStringLines;
use self::btls_aux_memory::EscapeByteString;
use self::btls_aux_memory::filter_map_utf8;
use self::btls_aux_memory::truncate_line_hash;
pub use self::unixwrapper::UnixListenerWrapper;
pub use self::unixwrapper::UnixSocketAddressWrapper;
pub use self::unixwrapper::UnixStreamWrapper;
pub use super::common::TryClone;
use std::env::var;
use std::fmt::Write as FmtWrite;
use std::fs::File;
use std::io::Error as IoError;
use std::io::ErrorKind as IoErrorKind;
use std::io::Read as IoRead;
use std::io::Result as IoResult;
use std::io::stderr;
use std::io::Write as IoWrite;
use std::str::FromStr;
use std::net::SocketAddr;
use std::net::TcpListener;
use std::net::TcpStream;
use std::net::IpAddr;
use std::net::Ipv6Addr;


pub enum TransportListener
{
	Tcp(TcpListener),
	Unix(UnixListenerWrapper),
}

pub enum TransportStream
{
	Tcp(TcpStream),
	Unix(UnixStreamWrapper),
}

#[derive(Clone)]
pub enum TransportAddress
{
	Tcp(SocketAddr),
	Unix(UnixSocketAddressWrapper),
}

#[cfg(not(unix))]
mod unixwrapper
{
	use super::TryClone;

	use std::io::Error as IoError;
	use std::io::ErrorKind as IoErrorKind;
	use std::io::Read as IoRead;
	use std::io::Result as IoResult;
	use std::io::Write as IoWrite;
	use std::path::Path;

	//Dummy versions of UnixStream and UnixListener.
	#[derive(Copy,Clone)]
	enum Void {}
	pub struct UnixStreamWrapper(Void);
	pub struct UnixListenerWrapper(Void);
	#[derive(Clone)]
	pub struct UnixSocketAddressWrapper(Void);

	impl TryClone for UnixStreamWrapper
	{
		fn try_clone_trait(&self) -> IoResult<Self> { Ok(UnixStreamWrapper(self.0)) }
	}

	impl IoRead for UnixStreamWrapper
	{
		fn read(&mut self, buf: &mut [u8]) -> IoResult<usize> { Ok(buf.len()) }
	}

	impl IoWrite for UnixStreamWrapper
	{
		fn write(&mut self, buf: &[u8]) -> IoResult<usize> { Ok(buf.len()) }
		fn flush(&mut self) -> IoResult<()> { Ok(()) }
	}

	impl UnixStreamWrapper
	{
		pub fn connect<P: AsRef<Path>>(_path: P) -> IoResult<UnixStreamWrapper>
		{
			Err(IoError::new(IoErrorKind::Other, "Unix domain sockets not supported on Windows"))
		}
	}

	impl UnixListenerWrapper
	{
		pub fn bind<P: AsRef<Path>>(_path: P) -> IoResult<UnixListenerWrapper>
		{
			Err(IoError::new(IoErrorKind::Other, "Unix domain sockets not supported on Windows"))
		}
		pub fn accept(&self) -> IoResult<(UnixStreamWrapper, UnixSocketAddressWrapper)>
		{
			Ok((UnixStreamWrapper(self.0), UnixSocketAddressWrapper(self.0)))
		}
	}
}

#[cfg(unix)]
mod unixwrapper
{
	use super::TryClone;
	use std::io::Read as IoRead;
	use std::io::Result as IoResult;
	use std::io::Write as IoWrite;
	use std::os::unix::net::SocketAddr;
	use std::os::unix::net::UnixListener;
	use std::os::unix::net::UnixStream;
	use std::path::Path;

	pub struct UnixStreamWrapper(UnixStream);
	pub struct UnixListenerWrapper(UnixListener);
	#[derive(Clone)]
	pub struct UnixSocketAddressWrapper(SocketAddr);

	impl TryClone for UnixStreamWrapper
	{
		fn try_clone_trait(&self) -> IoResult<Self> { self.0.try_clone().map(|x|UnixStreamWrapper(x)) }
	}

	impl IoRead for UnixStreamWrapper
	{
		fn read(&mut self, buf: &mut [u8]) -> IoResult<usize> { self.0.read(buf) }
	}

	impl IoWrite for UnixStreamWrapper
	{
		fn write(&mut self, buf: &[u8]) -> IoResult<usize> { self.0.write(buf) }
		fn flush(&mut self) -> IoResult<()> { self.0.flush() }
	}

	impl UnixStreamWrapper
	{
		pub fn connect<P: AsRef<Path>>(path: P) -> IoResult<UnixStreamWrapper>
		{
			UnixStream::connect(path).map(|x|UnixStreamWrapper(x))
		}
	}

	impl UnixListenerWrapper
	{
		pub fn bind<P: AsRef<Path>>(path: P) -> IoResult<UnixListenerWrapper>
		{
			UnixListener::bind(path).map(|x|UnixListenerWrapper(x))
		}
		pub fn accept(&self) -> IoResult<(UnixStreamWrapper, UnixSocketAddressWrapper)>
		{
			self.0.accept().map(|x|(UnixStreamWrapper(x.0), UnixSocketAddressWrapper(x.1)))
		}
	}
}

impl TryClone for TransportStream
{
	fn try_clone_trait(&self) -> IoResult<Self>
	{
		match self {
			&TransportStream::Tcp(ref x) => x.try_clone().map(|x|TransportStream::Tcp(x)),
			&TransportStream::Unix(ref x) => x.try_clone_trait().map(|x|TransportStream::Unix(x)),
		}
	}
}

impl IoRead for TransportStream
{
	fn read(&mut self, buf: &mut [u8]) -> IoResult<usize>
	{
		match self {
			&mut TransportStream::Tcp(ref mut x) => x.read(buf),
			&mut TransportStream::Unix(ref mut x) => x.read(buf),
		}
	}
}

impl IoWrite for TransportStream
{
	fn write(&mut self, buf: &[u8]) -> IoResult<usize>
	{
		match self {
			&mut TransportStream::Tcp(ref mut x) => x.write(buf),
			&mut TransportStream::Unix(ref mut x) => x.write(buf),
		}
	}
	fn flush(&mut self) -> IoResult<()>
	{
		match self {
			&mut TransportStream::Tcp(ref mut x) => x.flush(),
			&mut TransportStream::Unix(ref mut x) => x.flush(),
		}
	}
}

fn extract_refname(name: &str) -> String { name.rsplitn(2, ":").nth(1).unwrap().to_owned() }

fn convert_etc_services_to_utf8(bytes: Vec<u8>) -> String
{
	//First, try to take as UTF-8.
	let bytes = match String::from_utf8(bytes) {
		Ok(s) => return s,
		Err(e) => e.into_bytes(),	//Back to bytes.
	};
	let mut out = String::new();
	let lines = ByteStringLines::new(&bytes).map(truncate_line_hash).filter_map(filter_map_utf8).
		filter(|line|line.len() > 0);
	for line in lines { writeln!(out, "{line}").ok(); }
	out
}

fn lookup_symbolic_port(name: &str) -> Result<u16, ()>
{
	let name = name.to_lowercase();
	let etc_services = {
		let mut buf = Vec::new();
		let mut fp = dtry!(File::open("/etc/services"));
		dtry!(fp.read_to_end(&mut buf));
		convert_etc_services_to_utf8(buf)
	};
	for line in etc_services.lines() {
		//If there is '#' in the line, truncate at that point.
		let line = match line.find("#") { Some(x) => &line[..x], None => line };
		let mut pnum = None;
		let mut matches = false;
		let mut count = 0;
		for part in line.split(|c|{c==' '||c=='\t'}) {
			if part.len() == 0 { continue; } //Skip empty parts.
			if count == 1 {
				//The part has to end with '/tcp' to be stream port.
				let part = f_break!(part.strip_suffix("/tcp"));
				pnum = u16::from_str(part).ok();
			} else {
				matches |= part.to_lowercase() == name;
			}
			count += 1;
		}
		if let (Some(pnum), true) = (pnum, matches) { return Ok(pnum); }
	}
	//Not found.
	Err(())
}

fn get_hostport<'a>(name: &'a str) -> Result<(&'a str, u16), IoError>
{
	let host = name.rsplitn(2, ":").nth(1).unwrap();
	let port = name.rsplitn(2, ":").next().unwrap();
	//Do port number parse here, since connect() can not handle symbolic ports.
	let port = match u16::from_str(port).ok().or_else(||lookup_symbolic_port(port).ok()) {
		Some(x) => x,
		None => return Err(IoError::new(IoErrorKind::Other, "Invalid port number"))
	};
	Ok((host, port))
}

fn tcpstream_connect_to_low(host: &str, port: u16) -> Result<TcpStream, IoError>
{
	if let Some(host) = host.strip_prefix("[").and_then(|host|host.strip_suffix("]")) {
		//IP address.
		let address = match IpAddress::from_str(host) {
			Ok(x) => x,
			Err(_) => return Err(IoError::new(IoErrorKind::Other, "Invalid IP address"))
		};
		let address: IpAddr = address.into();
		TcpStream::connect((address, port))
	} else {
		//Use clean port number. Use temporary variable because String conversion is too new.
		let tmp = format!("{host}:{port}");
		TcpStream::connect(&tmp[..])
	}
}

fn tcpstream_connect_to(name: &str) -> Result<TcpStream, IoError>
{
	let (host, port) = get_hostport(name)?;
	if let (443, Ok(proxy)) = (port, var("https_proxy")) {
		let proxy = proxy.strip_prefix("http://").
			ok_or_else(||IoError::new(IoErrorKind::Other, "Invalid proxy address"))?;
		let proxy = proxy.strip_suffix("/").unwrap_or(proxy);
		let (phost, pport) = get_hostport(proxy)?;
		write!(stderr(), "Using proxy {phost} port {pport} instead of direct TCP connect...").ok();
		stderr().flush().ok();
		let mut stream = tcpstream_connect_to_low(phost, pport)?;
		//Okay, connected. Now send CONNECT header.
		let name = format!("{host}:{port}");
		stream.write(format!("CONNECT {name} HTTP/1.1\r\nHost: {name}\r\n\r\n", name=name).as_bytes())?;
		//Wait for response from proxy.
		let mut buffer = Vec::new();
		while !buffer.ends_with(b"\r\n\r\n") {
			let osize = buffer.len();
			buffer.resize(osize+1, 0);
			let r = stream.read(&mut buffer[osize..])?;
			if r == 0 {
				let msg = format!("Unexpected EOF reading proxy response (after {blen} bytes)",
					blen=buffer.len());
				fail!(IoError::new(IoErrorKind::UnexpectedEof, msg));
			}
			buffer.resize(osize + r, 0);
		}
		//response must start with "HTTP/1.x 200 ". Rewrite so it is HTTP/1.1.
		buffer.get_mut(7).map(|x|*x=if x.wrapping_sub(48) < 10 { 49 } else { *x });
		if !buffer.starts_with(b"HTTP/1.1 200 ") {
			return Err(IoError::new(IoErrorKind::Other, format!("Bad response from proxy: {buffer}",
				buffer=EscapeByteString(&buffer))));
		}
		writeln!(stderr(), "OK").ok();
		Ok(stream)
	} else {
		tcpstream_connect_to_low(host, port)
	}
}

impl TransportStream
{
	pub fn connect(name: &str) -> IoResult<(TransportStream, String)>
	{
		let mut refname: Option<String> = None;
		let name = if name.contains("#") {
			let idx = name.rfind("#").unwrap();
			refname = Some((&name[idx+1..]).to_owned());
			&name[..idx]
		} else {
			name
		};
		if name.contains(":") && !name.contains("/") {
			//Assme TCP.
			if refname.is_none() {
				//Just take the part before ':'.
				refname = Some(extract_refname(name));
			}
			return tcpstream_connect_to(name).map(|x|(TransportStream::Tcp(x), refname.unwrap()));
		} else {
			fail_if!(refname.is_none(), IoError::new(IoErrorKind::Other,
				"Unix domain sockets need reference name"));
			return UnixStreamWrapper::connect(name).map(|x|(TransportStream::Unix(x), refname.unwrap()));
		}
	}
	pub fn source_ip(&self) -> IpAddr
	{
		match self {
			&TransportStream::Tcp(ref x) => x.peer_addr().ok().map(|x|x.ip()),
			_ => None,
		}.unwrap_or(IpAddr::V6(Ipv6Addr::from([0;16])))
	}
	pub fn destination_ip(&self) -> IpAddr
	{
		match self {
			&TransportStream::Tcp(ref x) => x.local_addr().ok().map(|x|x.ip()),
			_ => None,
		}.unwrap_or(IpAddr::V6(Ipv6Addr::from([0;16])))
	}
}

impl TransportListener
{
	pub fn bind(name: &str) -> IoResult<TransportListener>
	{
		if name.contains(":") && !name.contains("/") {
			//Assme TCP.
			return TcpListener::bind(name).map(|x|TransportListener::Tcp(x));
		} else {
			//Assme Unix.
			return UnixListenerWrapper::bind(name).map(|x|TransportListener::Unix(x));
		}
	}
	pub fn accept(&self) -> IoResult<(TransportStream, TransportAddress)>
	{
		match self {
			&TransportListener::Tcp(ref x) => x.accept().
				map(|x|(TransportStream::Tcp(x.0), TransportAddress::Tcp(x.1))),
			&TransportListener::Unix(ref x) => x.accept().
				map(|x|(TransportStream::Unix(x.0), TransportAddress::Unix(x.1))),
		}
	}
}
