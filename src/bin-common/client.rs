extern crate btls_aux_serialization;
extern crate btls_aux_signatures;
extern crate btls_aux_x509certparse;
use crate::btls_aux_futures::FutureMapperFn;
use crate::btls_aux_futures::FutureReceiver;
use self::btls_aux_serialization::Source;
use self::btls_aux_signatures::TemporaryRandomStream;
use self::session::handle_peer;
pub use self::session::TryClone;
use btls::ClientConfiguration;
use btls::callbacks::CryptographicParametersInfo;
use btls::callbacks::PointInTime;
use btls::callbacks::SelectClientCertificateParameters;
use btls::callbacks::ServerNamesParameters;
use btls::callbacks::TlsCallbacks;
use btls::certificates::ClientCertificate;
use btls::certificates::Hasher;
use btls::certificates::HostSpecificPin;
use btls::certificates::KeyPair2;
use btls::certificates::LocalKeyPair;
use btls::certificates::SignatureAlgorithmTls2;
use btls::certificates::SignatureFormat;
use btls::certificates::TrustAnchor;
use btls::logging::StderrLog;
use btls::utility::Mutex;
use std::env::var;
use std::io::Read as IoRead;
use std::io::Write as IoWrite;
use std::io::stderr;
use std::fs::File;
use std::ops::Deref;
use std::panic::UnwindSafe;
use std::sync::Arc;
use std::str::FromStr;


//A certificate that has been selected for signing.
#[derive(Clone)]
struct SelectedCertificate
{
	//The keypair that does the signing.
	keypair: Arc<Box<dyn KeyPair2+Send+Sync>>,
	//The TLS algorithm number to use.
	tls_algorithm: SignatureAlgorithmTls2,
	//The certificate chain to send.
	certificate: Arc<Vec<u8>>,
	chain: Arc<Vec<Vec<u8>>>,
}

impl ClientCertificate for SelectedCertificate
{
	fn sign(&self, data: &mut dyn FnMut(&mut Hasher), rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<(u16, Vec<u8>), ()>>
	{
		//The KeyPair sign method returns future for Vec<u8>, but this function needs to return future for
		//(u16, Vec<u8>). Therefore we need an adapter to add the algorithm.
		let algo = self.tls_algorithm.to_generic();
		let tlsid = self.tls_algorithm.tls_id();
		//Start the signing process.
		self.keypair.sign_callback(algo, rng, data).
			map(FutureMapperFn::new(move |val: Result<(SignatureFormat, Vec<u8>), ()>|{
			Ok((tlsid, val?.1))
		}))
	}
	fn certificate<'a>(&'a self) -> &'a [u8] { self.certificate.deref().deref() }
	fn chain<'a>(&'a self) -> &'a [Vec<u8>] { self.chain.deref().deref() }
	//We don't implement get_ocsp nor get_scts. These get replaced by defaults.
}

struct Controller
{
	irq: session::Controller,
}

impl Controller
{
	fn new() -> Controller { Controller{irq: session::Controller::new()} }
}

impl session::GetController for Controller
{
	fn get<'a>(&'a mut self) -> &'a mut session::Controller { &mut self.irq }
}

impl TlsCallbacks for Controller
{
	fn select_client_certificate(&mut self, criteria: SelectClientCertificateParameters) ->
		Option<Box<dyn ClientCertificate+Send>>
	{
		if let Ok(certname) = var("BTLS_CLIENTCERT") {
			let keyfile = format!("{certname}.key");
			let certfile = format!("{certname}.crt");
			let kp = match LocalKeyPair::new_file(keyfile) {
				Ok(x) => x,
				Err(err) => {
					writeln!(stderr(), "Can't load private key: {err}").unwrap();
					return None;
				}
			};
			let can_sign = kp.get_signature_algorithms().iter().
				filter_map(|x|x.downcast_tls().map(|y|y.tls_id()));
			let algo = match criteria.choose_sigalg(can_sign) {
				Some(x) => x,
				None => {
					writeln!(stderr(), "Can't find acceptable signature algorithm").unwrap();
					return None;
				}
			};
			let mut certificates = Vec::new();
			let mut fp = match File::open(certfile) {
				Ok(x) => x,
				Err(err) => {
					writeln!(stderr(), "Can't open certificate file: {err}").unwrap();
					return None;
				}
			};
			let mut certfile = Vec::new();
			match fp.read_to_end(&mut certfile) {
				Ok(_) => (),
				Err(err) => {
					writeln!(stderr(), "Can't read certificate file: {err}").unwrap();
					return None;
				}
			};
			let mut certfile = Source::new(&certfile);
			while certfile.more() { match certfile.asn1_sequence_out() {
				Ok(x) => certificates.push(x.to_owned()),
				Err(err) => {
					writeln!(stderr(), "Can't read a certificate: {err}").unwrap();
					return None;
				}
			}}
			if certificates.len() == 0 {
				writeln!(stderr(), "No certificates in chain").unwrap();
				return None;
			}
			let cert = certificates.remove(0);
			
			Some(Box::new(SelectedCertificate{
				keypair: Arc::new(Box::new(kp)),
				tls_algorithm: algo,
				certificate: Arc::new(cert),
				chain: Arc::new(certificates),
			}))
		} else {
			None
		}
	}
	fn server_names(&mut self, params: ServerNamesParameters) { self.irq.server_names(params); }
	fn cryptographic_parameters(&mut self, info: CryptographicParametersInfo)
	{
		self.irq.cryptographic_parameters(info)
	}
	fn set_deadline(&mut self, dl: Option<PointInTime>) { self.irq.set_deadline(dl); }
}

#[path="./session.rs"]
mod session;

pub fn do_it<Socktype:IoRead+IoWrite+TryClone+Send+UnwindSafe+'static>(sock: Socktype, reference: Option<String>,
	trust_anchors: Vec<TrustAnchor>, pins: Vec<HostSpecificPin>)
{
	let mut alpn: Option<String> = None;
	let mut dbgflags = 0;
	if trust_anchors.len() == 0 && pins.len() == 0 {
		panic!("Need a target, reference and at least one trust anchor");
	}
	let mut client_config = ClientConfiguration::new();
	for i in trust_anchors.iter() { client_config.add_trust_anchor(i); }
	if let Ok(flags) = var("BTLS_DEBUG") { dbgflags = u64::from_str(&flags).unwrap(); }
	client_config.set_debug(dbgflags, Box::new(StderrLog));
	if let Ok(settings) = var("BTLS_SETTINGS") {
		client_config.config_flags(&settings, |error|{writeln!(stderr(), "{error}").unwrap();});
	}
	if let Ok(alpn2) = var("BTLS_ALPN") { alpn = Some(alpn2.to_owned()); }
	if let Some(alpn) = alpn { client_config.add_alpn(&alpn); }
	let controller = Arc::new(Mutex::new(Controller::new()));
	let client_session = client_config.make_connection_pinned(&reference.clone().unwrap(), &pins,
		Box::new(controller.clone()));
	handle_peer(client_session, sock, "Client", controller);
}
