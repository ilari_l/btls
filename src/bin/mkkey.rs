#![allow(improper_ctypes)]
#![forbid(unsafe_code)]
extern crate btls_aux_random;
extern crate btls_aux_serialization;
extern crate btls_aux_signatures;
use btls_aux_fail::dtry;
use btls_aux_fail::ResultExt;
use btls_aux_filename::Filename;
use btls_aux_random::TemporaryRandomLifetimeTag;
use btls_aux_random::TemporaryRandomStream;
use btls_aux_serialization::*;
use btls_aux_signatures::EcdsaCurve;
use btls_aux_signatures::EcdsaKeyDecode;
use btls_aux_signatures::EcdsaKeyPair;
use btls_aux_signatures::Ed25519KeyDecode;
use btls_aux_signatures::Ed25519KeyPair;
use btls_aux_signatures::Ed448KeyDecode;
use btls_aux_signatures::Ed448KeyPair;
use btls_aux_signatures::SignatureKeyPair;
use std::env::args_os;
use std::fs::File as StdFile;
use std::io::stderr;
use std::io::Write as IoWrite;
use std::path::Path as StdPath;
use std::process::exit;

macro_rules! stderr
{
	($x:expr) => { let _ = writeln!(stderr(), $x); };
	($x:expr, $($a:tt)*) => { let _ = writeln!(stderr(), $x, $($a)*); };
}

macro_rules! abort
{
	($x:expr) => {{ stderr!($x); exit(1); }};
	($x:expr, $($a:tt)*) => {{ stderr!($x, $($a)*); exit(1); }};
}

fn write_integer_sexp<S:Sink>(target: &mut S, val: &[u8]) -> Result<(), ()> { write_padded(target, val, val.len()) }

fn write_padded<S:Sink>(sink: &mut S, data: &[u8], datalen: usize) -> Result<(), ()>
{
	let prefix = format!("{datalen}:");
	for i in prefix.as_bytes().iter() { dtry!(sink.write_u8(*i)); }
	for _ in data.len()..datalen { dtry!(sink.write_u8(0)); }
	if data.len() > datalen {
		dtry!(sink.write_slice(&data[data.len()-datalen..]));
	} else {
		dtry!(sink.write_slice(data));
	}
	Ok(())
}

fn write_ecdsa_key<S:Sink>(sink: &mut S, crv: &str, d: &[u8], x: &[u8], y: &[u8], bytes: usize) -> Result<(), ()>
{
	dtry!(sink.write_u8(40));
	write_integer_sexp(sink, "ecdsa".as_bytes())?;	//Not actually integer but close enough...
	write_integer_sexp(sink, crv.as_bytes())?;	//Not actually integer but close enough...
	write_padded(sink, d, bytes)?;
	write_padded(sink, x, bytes)?;
	write_padded(sink, y, bytes)?;
	dtry!(sink.write_u8(41));
	Ok(())
}

fn write_eddsa_key<S:Sink>(sink: &mut S, sub: &str, privkey: &[u8], pubkey: &[u8]) -> Result<(), ()>
{
	dtry!(sink.write_u8(40));
	write_integer_sexp(sink, sub.as_bytes())?;	//Not actually integer but close enough...
	write_integer_sexp(sink, privkey)?;		//Not actually integer but close enough...
	write_integer_sexp(sink, pubkey)?;		//Not actually integer but close enough...
	dtry!(sink.write_u8(41));
	Ok(())
}

fn generate_ecdsa_key(crvid: EcdsaCurve) -> Result<Vec<u8>, String>
{
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let key = EcdsaKeyPair::keypair_generate(crvid, &mut rng).set_err("Error generating private key")?;
	let p = EcdsaKeyDecode::new(&key).set_err("Generated bad keypair representation")?;
	let crvname = p.crv.get_sexp_curve_name();
	let len = p.crv.get_component_bytes();
	let mut out = Vec::new();
	write_ecdsa_key(&mut out, crvname, p.d, p.x, p.y, len).set_err("Error serializing key")?;
	Ok(out)
}

fn generate_eddsa_key(subtype: &str) -> Result<Vec<u8>, String>
{
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let mut out = Vec::new();
	let error = if subtype == "ed25519" {
		let keypair = Ed25519KeyPair::keypair_generate((), &mut rng).set_err("Error generating key")?;
		let parse = Ed25519KeyDecode::new(&keypair).set_err("Error parsing generated key")?;
		write_eddsa_key(&mut out, subtype, &parse.private, &parse.public)
	} else if subtype == "ed448" {
		let keypair = Ed448KeyPair::keypair_generate((), &mut rng).set_err("Error generating key")?;
		let parse = Ed448KeyDecode::new(&keypair).set_err("Error parsing generated key")?;
		write_eddsa_key(&mut out, subtype, &parse.private, &parse.public)
	} else {
		return Err(format!("Unknown subtype for EdDSA"));
	};
	error.set_err("Error serializing key")?;
	Ok(out)
}

fn main()
{
	let mut outputf = None;
	let mut algorithm = None;
	for i in args_os().enumerate() {
		if i.0 == 0 { continue; }	//Skip program name.
		if outputf.is_none() {
			outputf = Some(i.1);
		} else if algorithm.is_none() {
			let j = match i.1.into_string() {
				Ok(x) => x,
				Err(x) => {
					println!("Bad argument `{x:?}`, skipping");
					continue;
				}
			};
			algorithm = Some(j);
		}
	}
	if algorithm.is_none() {
		stderr!("Syntax: mkkey <output> <algorithm>");
		stderr!("");
		stderr!("<algorithm> can be:");
		stderr!("\tecdsa:    256-bit ECDSA");
		stderr!("\tecdsa256: 256-bit ECDSA");
		stderr!("\tecdsa384: 384-bit ECDSA");
		stderr!("\tecdsa521: 521-bit ECDSA");
		stderr!("\ted25519:  Ed25519");
		stderr!("\ted448:    Ed448");
		abort!("Error: missing paramteters");
	}

	let algorithm = algorithm.clone().unwrap();

	let crv = if algorithm == "ecdsa" || algorithm == "ecdsa256" {
		Some(0x80000002u32)
	} else if algorithm == "ecdsa384" {
		Some(0x80000003u32)
	} else if algorithm == "ecdsa521" {
		Some(0x80000004u32)
	} else if algorithm == "ed25519" {
		Some(0xFFFFFFFEu32)		//Special.
	} else if algorithm == "ed448" {
		Some(0xFFFFFFFFu32)		//Special.
	} else {
		abort!("Error: Unknown algorithm '{algorithm}'");
	};

	let out = if let Some(0xFFFFFFFEu32) = crv {
		generate_eddsa_key("ed25519")
	} else if let Some(0xFFFFFFFFu32) = crv {
		generate_eddsa_key("ed448")
	} else if let Some(crvx) = crv {
		let crvobj = EcdsaCurve::by_id(crvx - 0x80000002u32).unwrap_or_else(||{
			abort!("Error: Unknown ECDSA curve")
		});
		generate_ecdsa_key(crvobj)
	} else {
		abort!("Error: Not ECDSA nor EdDSA key?");
	};
	let out = out.unwrap_or_else(|err|{
		abort!("Error: {err}")
	});
	let fname = outputf.clone().unwrap();
	let fname = StdPath::new(&fname);
	let fnamed = Filename::from_path(fname);
	let mut fp = StdFile::create(fname).unwrap_or_else(|err|{
		abort!("Error creating '{fnamed}': {err}")
	});
	fp.write_all(&out).unwrap_or_else(|err|{
		abort!("Error writing '{fnamed}': {err}")
	});
}
