extern crate btls_aux_xed25519;
extern crate btls_aux_xed448;
extern crate btls_aux_nettle;
extern crate btls_aux_random;
use btls_aux_xed25519::x25519_agree;
use btls_aux_xed25519::x25519_generate;
use btls_aux_xed25519::ed25519_pubkey;
use btls_aux_xed25519::ed25519_sign;
use btls_aux_xed448::x448_agree;
use btls_aux_xed448::x448_generate;
use btls_aux_xed448::ed448_pubkey;
use btls_aux_xed448::ed448_sign;
use btls_aux_nettle::EccCurve;
use btls_aux_nettle::EccScalar;
use btls_aux_nettle::Sha256Ctx;
use btls_aux_nettle::Sha384Ctx;
use btls_aux_nettle::Sha512Ctx;
use btls_aux_random::TemporaryRandomStream;
use btls_aux_random::TemporaryRandomLifetimeTag;
use std::time::Instant;


fn do_rounds(f: &mut impl FnMut()) -> (f64, usize)
{
	let ts = Instant::now();
	let mut now = ts;
	let mut done = 0;
	while now.duration_since(ts).as_secs() < 5 {
		for _ in 0..1000 { f(); }
		done += 1000;
		now = Instant::now();
	}
	let dt = now.duration_since(ts);
	(dt.as_nanos() as f64 / 1_000_000_000.0, done)
}

fn calculate_ops(dt: f64, rounds: usize) -> f64 { rounds as f64 / dt }

fn test_x25519()
{
	let mut out = [0;32];
	let scalar = [0;32];
	let peerpub = [0;32];
	let (dt, rounds) = do_rounds(&mut ||{
		x25519_agree(&mut out, &scalar, &peerpub);
	});
	println!("x25519:\t\t{ops:.0} ops/s", ops=calculate_ops(dt, rounds));
}

fn test_x25519g()
{
	let mut out = [0;32];
	let scalar = [0;32];
	let (dt, rounds) = do_rounds(&mut ||{
		x25519_generate(&mut out, &scalar);
	});
	println!("x25519(kgen):\t{ops:.0} ops/s", ops=calculate_ops(dt, rounds));
}

fn test_x448()
{
	let mut out = [0;56];
	let scalar = [0;56];
	let peerpub = [0;56];
	let (dt, rounds) = do_rounds(&mut ||{
		x448_agree(&mut out, &scalar, &peerpub);
	});
	println!("x448:\t\t{ops:.0} ops/s", ops=calculate_ops(dt, rounds));
}

fn test_x448g()
{
	let mut out = [0;56];
	let scalar = [0;56];
	let (dt, rounds) = do_rounds(&mut ||{
		x448_generate(&mut out, &scalar);
	});
	println!("x448(kgen):\t{ops:.0} ops/s", ops=calculate_ops(dt, rounds));
}

fn test_ecdsa_p256()
{
	let mut out = [0;128];
	let rkey = [1;32];
	let msg = [0;48];
	let crv = EccCurve::new(0).unwrap();
	let mut key = EccScalar::new(crv);
	key.load(&rkey).unwrap();
	let trng = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&trng);
	let (dt, rounds) = do_rounds(&mut ||{
		let mut h = Sha256Ctx::new();
		h.input(&msg);
		key.ecdsa_sign_hash(&h.output(), &mut out, &mut rng).unwrap();
	});
	println!("ECDSA P-256:\t{ops:.0} sigs/s", ops=calculate_ops(dt, rounds));
}

fn test_ecdsa_p384()
{
	let mut out = [0;192];
	let rkey = [1;48];
	let msg = [0;48];
	let crv = EccCurve::new(1).unwrap();
	let mut key = EccScalar::new(crv);
	key.load(&rkey).unwrap();
	let trng = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&trng);
	let (dt, rounds) = do_rounds(&mut ||{
		let mut h = Sha384Ctx::new();
		h.input(&msg);
		key.ecdsa_sign_hash(&h.output(), &mut out, &mut rng).unwrap();
	});
	println!("ECDSA P-384:\t{ops:.0} sigs/s", ops=calculate_ops(dt, rounds));
}

fn test_ecdsa_p521()
{
	let mut out = [0;256];
	let rkey = [1;66];
	let msg = [0;48];
	let crv = EccCurve::new(2).unwrap();
	let mut key = EccScalar::new(crv);
	key.load(&rkey).unwrap();
	let trng = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&trng);
	let (dt, rounds) = do_rounds(&mut ||{
		let mut h = Sha512Ctx::new();
		h.input(&msg);
		key.ecdsa_sign_hash(&h.output(), &mut out, &mut rng).unwrap();
	});
	println!("ECDSA P-521:\t{ops:.0} sigs/s", ops=calculate_ops(dt, rounds));
}

fn test_ed25519()
{
	let mut out = [0;64];
	let mut pubkey = [0;32];
	let privkey = [1;32];
	let msg = [0;48];
	ed25519_pubkey(&mut pubkey, &privkey);
	let (dt, rounds) = do_rounds(&mut ||{
		ed25519_sign(&privkey, &pubkey, &msg, &mut out);
	});
	println!("Ed25519:\t{ops:.0} sigs/s", ops=calculate_ops(dt, rounds));
}

fn test_ed448()
{
	let mut out = [0;114];
	let mut pubkey = [0;57];
	let privkey = [1;57];
	let msg = [0;48];
	ed448_pubkey(&mut pubkey, &privkey);
	let (dt, rounds) = do_rounds(&mut ||{
		ed448_sign(&privkey, &pubkey, &msg, &mut out);
	});
	println!("Ed448:\t\t{ops:.0} sigs/s", ops=calculate_ops(dt, rounds));
}

fn main()
{
	test_x25519g();
	test_x25519();
	test_x448g();
	test_x448();
	test_ecdsa_p256();
	test_ecdsa_p384();
	test_ecdsa_p521();
	test_ed25519();
	test_ed448();
}
