extern crate btls;
#[macro_use] extern crate btls_aux_fail;
use btls::certificates::CertificateDirectory;
use btls::logging::StderrLog;
use std::env::args_os;
use std::ops::Deref;
use transport::TransportListener;
use transport::TransportStream;

#[path="../bin-common/server.rs"]
mod common;
#[path="../bin-common/transport.rs"]
mod transport;

fn main()
{
	if false { dummy(); }		//Silence some warnings.
	let mut target = None;
	let mut cdir = None;
	let mut dummy = false;
	for i in args_os() {
		if !dummy {
			dummy = true;
		} else if target.is_none() {
			target = Some(i.clone().into_string().unwrap_or_else(|_|{
				panic!("Invalid target `{i:?}`")
			}));
		} else if cdir.is_none() {
			cdir = Some(i);
		} else {
			panic!("Expected only two arguments");
		}
	}
	if cdir.is_none() { panic!("Need target and certificate directory"); }
	let store = CertificateDirectory::new(&cdir.unwrap(), StderrLog).unwrap_or_else(|err|{
		panic!("Can't create ceritificate store: {err}")
	});
	let acceptsock = TransportListener::bind(target.clone().unwrap().deref()).unwrap_or_else(|err|{
		panic!("Can't bind to `{target:?}`: {err}", target=target.unwrap())
	});
	common::do_it(acceptsock, store.lookup());
}

fn dummy() { let _ = TransportStream::connect(""); }
