extern crate btls;
#[macro_use] extern crate btls_aux_fail;
use btls::certificates::LocalKeyPair;
use btls::certificates::LocalServerCertificateStore;
use std::env::args_os;
use std::io::Write as IoWrite;
use std::io::stderr;
use std::ops::Deref;
use std::process::exit;
use transport::TransportListener;
use transport::TransportStream;


#[path="../bin-common/server.rs"]
mod common;
#[path="../bin-common/transport.rs"]
mod transport;

fn main()
{
	if false { dummy(); }		//Silence some warnings.
	let mut target = None;
	let mut key = None;
	let mut dummy = false;
	let mut files = Vec::new();
	for i in args_os() {
		if !dummy {
			dummy = true;
		} else if target.is_none() {
			target = Some(i.clone().into_string().unwrap_or_else(|_|{
				panic!("Invalid target `{i:?}`")
			}));
		} else if key.is_none() {
			key = Some(i);
		} else {
			let file = i.clone();
			files.push(file);
		}
	}
	if files.len() == 0 { panic!("Need target, key and some certificate files"); }
	let mut store = LocalServerCertificateStore::new();
	let _key = key.clone().unwrap().to_string_lossy().into_owned();
	let keyfile = key.unwrap();
	let privkey = LocalKeyPair::new_file(&keyfile).unwrap_or_else(|err|{
		writeln!(stderr(), "{err}").unwrap();
		exit(1);
	});
	store.add_privkey(privkey, &_key);
	for i in files {
		match store.add_file(&i) {
			Ok(_) => (),
			Err(err) => writeln!(stderr(), "{err}").unwrap()
		}
	}

	let acceptsock = TransportListener::bind(target.clone().unwrap().deref()).unwrap_or_else(|err|{
		panic!("Can't bind to `{target:?}`: {err}", target=target.unwrap())
	});
	common::do_it(acceptsock, store.lookup());
}

fn dummy() { let _ = TransportStream::connect(""); }
