extern crate btls;
#[macro_use] extern crate btls_aux_fail;
extern crate btls_aux_futures;
use btls::certificates::HostSpecificPin;
use btls::certificates::TrustAnchor;
use std::env::args_os;
use std::ops::Deref;
use transport::TransportListener;
use transport::TransportStream;

#[path="../bin-common/client.rs"]
mod common;
#[path="../bin-common/transport.rs"]
mod transport;

fn main()
{
	if false { dummy(); }	//Silence some warnings.
	let mut target = None;
	let mut dummy = false;
	let mut trust_anchors = Vec::new();
	let mut pins = Vec::new();
	for i in args_os() {
		if !dummy {
			dummy = true;
		} else if target.is_none() {
			target = Some(i.clone().into_string().unwrap_or_else(|_|{
				panic!("Invalid target `{i:?}`")
			}));
		} else {
			let file = i.clone();
			if let Some(ref x) = i.to_str().and_then(|x|{
				x.strip_prefix("hash:")
			}) {
				let mut hash = [0; 32];
				let mut valid = false;
				for i in x.chars().enumerate() {
					let ch = i.1 as u32;
					let shift = 4 ^ ((i.0 & 1) << 2) as u32;
					if let Some(x) = hash.get_mut(i.0>>1) {
						if ch >= 0x30 && ch <= 0x39 {
							*x |= (ch.saturating_sub(0x30)).wrapping_shl(shift) as u8;
						} else if ch >= 0x41 && ch <= 0x46 {
							*x |= (ch.saturating_sub(0x41-10)).wrapping_shl(shift) as u8;
						} else if ch >= 0x61 && ch <= 0x66 {
							*x |= (ch.saturating_sub(0x61-10)).wrapping_shl(shift) as u8;
						} else {
							break;	//Not valid.
						}
					} else {
						valid = false;
						break;
					}
					valid |= i.0 == 63;	//Completed.
				}
				if !valid { panic!("Bad keyhash (expected 64 hex digits)."); }
				pins.push(HostSpecificPin::trust_server_key_by_sha256(&hash, false, false));
			} else {
				let ta = TrustAnchor::from_certificate_file(file).unwrap_or_else(|err|{
					panic!("Can't load TA `{i:?}`: {err}")
				});
				trust_anchors.push(ta);
			}
		}
	}
	let (clientsock, reference) = TransportStream::connect(target.clone().unwrap().deref()).unwrap_or_else(|err|{
		panic!("Can't connect to `{target:?}`: {err}", target=target.unwrap())
	});
	if false { clientsock.source_ip(); }		//Dummy to avoid warning.
	if false { clientsock.destination_ip(); }	//Dummy to avoid warning.
	common::do_it(clientsock, Some(reference), trust_anchors, pins);
}

fn dummy()
{
	let x = TransportListener::bind("").unwrap();
	let _ = x.accept();
}
