extern crate btls;
extern crate btls_aux_hash;
extern crate btls_aux_signatures;
use btls::certificates::LocalKeyPair;
use btls::certificates::PGP_WORDLIST;
use btls_aux_filename::Filename;
use btls_aux_hash::sha256;
use btls_aux_signatures::KeyPair2;
use btls_aux_tls_iana::SignatureScheme;
use std::env::args_os;
use std::fmt::Write as FmtWrite;
use std::process::exit;
use std::path::Path;
use std::io::stderr;
use std::io::Write;
//use std::ops::Deref;


fn hexchar(val: u8) -> char { (val + if val < 10 { 48 } else { 87 }) as char }

fn as_hex(data: &[u8]) -> String
{
	let mut ret = String::new();
	for i in data.iter() {
		ret.push(hexchar(*i >> 4));
		ret.push(hexchar(*i & 15));
	}
	ret
}

fn base64char(val: u8) -> char
{
	val.wrapping_add(match val {
		0..=25 => 65,
		26..=51 => 71,
		52..=61 => 252,
		62 => 237,
		63 => 240,
		_ => unreachable!(),
	}) as char
}

fn as_base64(data: &[u8]) -> String
{
	let mut ret = String::new();
	for i in 0..data.len() / 3 {
		let a = data[3*i+0];
		let b = data[3*i+1];
		let c = data[3*i+2];
		ret.push(base64char(a >> 2));
		ret.push(base64char(((a << 4) | (b >> 4)) & 63));
		ret.push(base64char(((b << 2) | (c >> 6)) & 63));
		ret.push(base64char(c & 63));
	}
	if data.len() % 3 == 1 {
		let a = data[data.len()-1];
		ret.push(base64char(a >> 2));
		ret.push(base64char((a << 4) & 63));
		ret.push('=');
		ret.push('=');
	}
	if data.len() % 3 == 2 {
		let a = data[data.len()-2];
		let b = data[data.len()-1];
		ret.push(base64char(a >> 2));
		ret.push(base64char(((a << 4) | (b >> 4)) & 63));
		ret.push(base64char((b << 2) & 63));
		ret.push('=');
	}
	ret
}

fn as_pgp_words(data: &[u8]) -> String
{
	let mut s = String::new();
	let mut linelen = 0;
	const MAX_LINE: usize = 69;
	let mut parity = false;
	for i in data.iter() {
		let idx = (*i as usize) * 2 + if parity { 1 } else { 0 };
		let word = PGP_WORDLIST[idx];
		if linelen + 1 + word.len() > MAX_LINE {
			s.push_str("\n          ");
			linelen = 0;
		} else if linelen > 0 {
			s.push(' ');
			linelen += 1;
		}
		s.push_str(word);
		linelen += word.len();
		parity = !parity;
	}
	s
}

pub fn main()
{
	let mut key = None;
	let mut dummy = false;
	for i in args_os() { if !dummy { dummy = true; } else if key.is_none() { key = Some(i); } }
	if key.is_none() {
		writeln!(stderr(), "Syntax: btls-keyinfo <privatekey>").unwrap();
		exit(1);
	}
	let key = key.unwrap();
	let key = Path::new(&key);
	let keyd = Filename::from_path(key);
	let keypair = match LocalKeyPair::new_file(key) {
		Ok(x) => x,
		Err(err) => {
			writeln!(stderr(), "Can't load private key '{keyd}': {err}").unwrap();
			exit(1);
		}
	};
	let rawkey = keypair.get_public_key2();
	let schemes = keypair.get_signature_algorithms();
	let mut supported_schemes = String::new();
	for &i in schemes.iter() {
		if let Some(i) = i.downcast_tls() {
			let i = SignatureScheme::new(i.tls_id());
			if supported_schemes.len() > 0 { supported_schemes.push(' '); }
			write!(supported_schemes, "{i}").ok();
		} else {
			if supported_schemes.len() > 0 { supported_schemes.push(' '); }
			write!(supported_schemes, "[{i:?}]").ok();
		}
	}
	let pkhash = sha256(&rawkey);
	println!("File:     {keyd}");
	println!("Type:     {type}", type=keypair.get_key_type2());
	println!("Supports: {supported_schemes}");
	println!("SHA-256:  {hash}", hash=as_hex(&pkhash));
	println!("pin:      pin-sha256=\"{hash}\"", hash=as_base64(&pkhash));
	println!("PGPwords: {hash}", hash=as_pgp_words(&pkhash));
}
