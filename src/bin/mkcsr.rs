extern crate btls_aux_keypair_local;
extern crate btls_aux_random;
extern crate btls_aux_signatures;
use btls_aux_filename::Filename;
use btls_aux_keypair_local::LocalKeyPair;
use btls_aux_memory::Attachment;
use btls_aux_memory::split_attach_first;
use btls_aux_random::TemporaryRandomLifetimeTag;
use btls_aux_random::TemporaryRandomStream;
use btls_aux_signatures::CsrParams;
use btls_aux_signatures::KeyPair2;
use btls_aux_signatures::encode_pem_csr;
use btls_aux_signatures::self_verify_csr;
use std::env::args_os;
use std::fs::File as StdFile;
use std::io::stderr;
use std::io::Write as IoWrite;
use std::path::Path as StdPath;
use std::process::exit;

macro_rules! stderr
{
	($x:expr) => { let _ = writeln!(stderr(), $x); };
	($x:expr, $($a:tt)*) => { let _ = writeln!(stderr(), $x, $($a)*); };
}

macro_rules! abort
{
	($x:expr) => {{ stderr!($x); exit(1); }};
	($x:expr, $($a:tt)*) => {{ stderr!($x, $($a)*); exit(1); }};
}

struct Parameters
{
	output: Option<String>,
	privkey: Option<String>,
	request: CsrParams,
	want_pem: bool,
}

fn decode_parameter(p: &mut CsrParams, arg: &str)
{
	if let Some(arg2) = arg.strip_prefix("--") {
		let (key, value) = split_attach_first(arg2, "=", Attachment::Center).unwrap_or_else(|_|{
			abort!("No = sign in --subject")
		});
		match p.apply_name_value(key, value) {
			Ok(true) => (),
			Ok(false) => {
				stderr!("Unrecognized parameter '{key}'");
				abort!("Unrecognized parameter");
			},
			Err(err) => abort!("Error processing '{arg}': {err}")
		};
	} else {
		p.apply_name(arg);
	}
}

fn main()
{
	let mut p = Parameters{
		output: None,
		privkey: None,
		request: CsrParams::new(),
		want_pem: false,
	};
	for i in args_os().enumerate() {
		if i.0 == 0 { continue; }	//Skip program name.
		let j = match i.1.into_string() {
			Ok(x) => x,
			Err(x) => {
				stderr!("Bad argument `{x:?}`, skipping");
				continue;
			}
		};
		if j == "--pem" {
			p.want_pem = true;
			continue;
		}
		if j.starts_with("--") {
			decode_parameter(&mut p.request, &j);
			continue;
		}
		if p.output.is_none() {
			p.output = Some(j);
		} else if p.privkey.is_none() {
			p.privkey = Some(j);
		} else {
			decode_parameter(&mut p.request, &j);
		}
	}
	if p.request.sans.len() == 0 && p.request.sans_other.len() == 0 && p.request.sans_ip.len() == 0 {
		stderr!("Syntax: mkcsr [--pem] <output> <keypair> <dnsname>...");
		stderr!("");
		stderr!("For multi-SAN certificate, specify multiple <dnsname>, separated by spaces");
		stderr!("--pem causes the CSR to be output as PEM instead of DER.");
		abort!("Missing parameters");
	}

	let privkeyname = if let &Some(ref x) = &p.privkey { x } else { abort!("No private key file specified!") };
	let signing_keypair = LocalKeyPair::new_file(privkeyname).unwrap_or_else(|err|{
		abort!("Can't load file key `{privkeyname}`: {err}")
	});

	//Make the CSR.
	let lifetime = TemporaryRandomLifetimeTag;
	let csr_future = signing_keypair.sign_csr2(&p.request, &mut TemporaryRandomStream::new_system(&lifetime));
	while !csr_future.settled() {};	//Wait until settled.
	let result = match csr_future.read() {
		Ok(Ok(x)) => x,
		_ => abort!("Error signing the CSR!")
	};
	//Do CSR self-verify.
	match self_verify_csr(&result) {
		Ok(_) => {stderr!("CSR self-verify OK");},
		Err(_) => abort!("Failed to self-verify the CSR!")
	};
	//If we want pem, encode as PEM.
	let result = if p.want_pem { encode_pem_csr(&result) } else { result };

	let fname = p.output.clone().unwrap();
	let fname = StdPath::new(&fname);
	let fnamed = Filename::from_path(fname);
	let mut fp = StdFile::create(fname).unwrap_or_else(|err|{
		abort!("Error creating '{fnamed}': {err}")
	});
	fp.write_all(&result).unwrap_or_else(|err|{
		let fnamed = Filename::from_path(fname);
		abort!("Error writing '{fnamed}': {err}")
	});
	stderr!("Wrote {fnamed}");
}

#[test]
fn test_encode_csr_long()
{
	use btls_aux_random::secure_random;
	use btls_aux_signatures::decode_pem;
	use std::str::from_utf8;
	let mut original = [0; 65538];
	original[0] = 0x30;
	original[1] = 0x82;
	original[2] = 0xFF;
	original[3] = 0xFE;
	secure_random(&mut original[4..]);
	let encoded = encode_pem_csr(&original[..]);
	let text = from_utf8(&encoded).unwrap();
	let decoded = decode_pem(&encoded, " CERTIFICATE REQUEST", false).unwrap();
	assert_eq!(&decoded[..], &original[..]);
	assert_eq!(encoded[encoded.len()-1], 10);
	let mut textlines = text.lines();
	assert_eq!(textlines.next().unwrap(), "-----BEGIN CERTIFICATE REQUEST-----");
	let mut old_line_len = 72;
	loop {
		let line = textlines.next().unwrap();
		if line == "-----END CERTIFICATE REQUEST-----" { break; }
		for i in line.chars() {
			let i = i as u32;
			assert!((i >= 65 && i <= 90) || (i >= 97 && i <= 122) || (i >= 47 && i <= 57) || i == 43);
		}
		assert_eq!(old_line_len, 72);
		old_line_len = line.len();
		assert!(old_line_len > 0 && old_line_len <= 72);
		assert!((old_line_len % 4) == 0);
	}
}

#[test]
fn test_encode_csr_long_m1()
{
	use btls_aux_random::secure_random;
	use btls_aux_signatures::decode_pem;
	use std::str::from_utf8;
	let mut original = [0; 65501];
	original[0] = 0x30;
	original[1] = 0x82;
	original[2] = 0xFF;
	original[3] = 0xD9;
	secure_random(&mut original[4..]);
	let encoded = encode_pem_csr(&original[..]);
	let text = from_utf8(&encoded).unwrap();
	let decoded = decode_pem(&encoded, " CERTIFICATE REQUEST", false).unwrap();
	assert_eq!(&decoded[..], &original[..]);
	assert_eq!(encoded[encoded.len()-1], 10);
	let mut textlines = text.lines();
	assert_eq!(textlines.next().unwrap(), "-----BEGIN CERTIFICATE REQUEST-----");
	let mut padding = 0;
	loop {
		let line = textlines.next().unwrap();
		if line == "-----END CERTIFICATE REQUEST-----" { break; }
		assert_eq!(padding, 0);
		assert_eq!(line.len(), 72);
		padding = 0;
		while line.as_bytes()[line.len()-padding-1] == 61 { padding += 1; }
	}
	assert_eq!(padding, 1);
}

#[test]
fn test_encode_csr_long_m2()
{
	use btls_aux_random::secure_random;
	use btls_aux_signatures::decode_pem;
	use std::str::from_utf8;
	let mut original = [0; 65500];
	original[0] = 0x30;
	original[1] = 0x82;
	original[2] = 0xFF;
	original[3] = 0xD8;
	secure_random(&mut original[4..]);
	let encoded = encode_pem_csr(&original[..]);
	let text = from_utf8(&encoded).unwrap();
	let decoded = decode_pem(&encoded, " CERTIFICATE REQUEST", false).unwrap();
	assert_eq!(&decoded[..], &original[..]);
	assert_eq!(encoded[encoded.len()-1], 10);
	let mut textlines = text.lines();
	assert_eq!(textlines.next().unwrap(), "-----BEGIN CERTIFICATE REQUEST-----");
	let mut padding = 0;
	loop {
		let line = textlines.next().unwrap();
		if line == "-----END CERTIFICATE REQUEST-----" { break; }
		assert_eq!(padding, 0);
		assert_eq!(line.len(), 72);
		padding = 0;
		while line.as_bytes()[line.len()-padding-1] == 61 { padding += 1; }
	}
	assert_eq!(padding, 2);
}

#[test]
fn test_encode_csr_4bytes()
{
	use btls_aux_signatures::decode_pem;
	use std::str::from_utf8;
	//decode_pem doesn't like inputs that don't have SEQUENCE header.
	let mut original = [0x30, 0x02, 0x00, 0x00];
	for i in 0..255 {
		original[3] = i as u8;
		let encoded = encode_pem_csr(&original[..]);
		let text = from_utf8(&encoded).unwrap();
		let decoded = decode_pem(&encoded, " CERTIFICATE REQUEST", false).unwrap();
		assert_eq!(&decoded[..], &original[..]);
		let mut textlines = text.lines();
		assert_eq!(textlines.next().unwrap(), "-----BEGIN CERTIFICATE REQUEST-----");
		let line = textlines.next().unwrap().as_bytes();
		assert_eq!(textlines.next().unwrap(), "-----END CERTIFICATE REQUEST-----");
		let i = line[4];
		let j = line[5];
		assert_eq!(line[6], 61);
		assert_eq!(line[7], 61);
		assert_eq!(line.len(), 8);
		assert!((i >= 65 && i <= 90) || (i >= 97 && i <= 122) || (i >= 47 && i <= 57) || i == 43);
		assert!(j == 65 || j == 81 || j == 103 || j == 119);
	}
}

#[test]
fn test_encode_csr_5bytes()
{
	use btls_aux_signatures::decode_pem;
	use std::str::from_utf8;
	//decode_pem doesn't like inputs that don't have SEQUENCE header.
	let mut original = [0x30, 0x03, 0x00, 0x00, 0x00];
	for i in 0..255 {
		original[4] = i as u8;
		let encoded = encode_pem_csr(&original[..]);
		let text = from_utf8(&encoded).unwrap();
		let decoded = decode_pem(&encoded, " CERTIFICATE REQUEST", false).unwrap();
		assert_eq!(&decoded[..], &original[..]);
		let mut textlines = text.lines();
		assert_eq!(textlines.next().unwrap(), "-----BEGIN CERTIFICATE REQUEST-----");
		let line = textlines.next().unwrap().as_bytes();
		assert_eq!(textlines.next().unwrap(), "-----END CERTIFICATE REQUEST-----");
		let i = line[4];
		let j = line[5];
		let k = line[6];
		assert_eq!(line[7], 61);
		assert_eq!(line.len(), 8);
		assert!((i >= 65 && i <= 90) || (i >= 97 && i <= 122) || (i >= 47 && i <= 57) || i == 43);
		assert!((j >= 65 && j <= 90) || (j >= 97 && j <= 122) || (j >= 47 && j <= 57) || j == 43);
		//AEIMQUYcgkosw048
		assert!((k >= 65 && k <= 90 && k % 4 == 1) || (k >= 97 && k <= 122 && k % 4 == 3) ||
			(k >= 47 && k <= 57 && k % 4 == 0));
	}
}
