use self::internal::DPO_ALERT;
use self::internal::DPO_CLOSE;
use self::internal::DPO_DATA;
use self::internal::DPO_DATA_IGNORE;
use self::internal::DPO_FATAL;
use self::internal::DPO_IGNORE;
use self::internal::DPO_INVAL_DATA_E;
use self::internal::DPO_INVAL_DATA_L;
use self::internal::SessionInternalContext;
use crate::callbacks::ClientCertificateInfo;
use crate::callbacks::TlsCallbacks;
use crate::common::FutureFinishedNotifier;
use crate::errors::Error;
use crate::logging::DebugSetting;
use crate::messages::DumpHexblock;
use crate::messages::DumpHexblock3;
use crate::stdlib::Box;
use crate::stdlib::min;
use crate::stdlib::replace;
use crate::stdlib::Vec;
use crate::transport::DataBuffer;
use crate::transport::PullTcpBuffer;
use crate::transport::StatusFlags;
use crate::utils::HsBuffer;
use crate::utils::to_mu8_mut;
use btls_aux_bitflags::Bitflags;
use btls_aux_fail::ResultExt;
use btls_aux_memory::BackedVector;
use btls_aux_memory::split_at;
use btls_aux_rope3::FragmentWriteDyn as FragmentWriteDyn3;
use btls_aux_rope3::FragmentWriteOps as FragmentWriteOps3;
use btls_aux_tls_iana::Alert;
use btls_aux_tls_iana::ContentType;
use btls_aux_tls_iana::HandshakeType;
use btls_aux_tlskeyschedule::BoxedDeprotector;
use btls_aux_tlskeyschedule::BoxedExtractor;
use btls_aux_tlskeyschedule::BoxedProtector;
pub use btls_aux_tlskeyschedule::DiffieHellmanSharedSecretInitiator;
pub use btls_aux_tlskeyschedule::DiffieHellmanSharedSecretResponder;
pub use btls_aux_tlskeyschedule::Exportable;
pub use btls_aux_tlskeyschedule::GenerateFinished;
pub use btls_aux_tlskeyschedule::HasAd;
pub use btls_aux_tlskeyschedule::IsEms;
pub use btls_aux_tlskeyschedule::Tls12MasterSecret;
pub use btls_aux_tlskeyschedule::Tls12PremasterSecret;
pub use btls_aux_tlskeyschedule::Tls13HandshakeSecret;
pub use btls_aux_tlskeyschedule::Tls13MasterSecret;
pub use btls_aux_tlskeyschedule::Tls13TrafficSecretIn;
pub use btls_aux_tlskeyschedule::Tls13TrafficSecretOut;
pub use btls_aux_tlskeyschedule::UnsignedTls13HandshakeSecret;
use core::mem::MaybeUninit;
use core::slice::from_raw_parts_mut;


mod internal;
mod utils;

fn arith_overflow() -> Error { Error::from(assert_failure!("Arithmetic overflow")) }

fn read_hsmsg_len(hdr: &[u8]) -> Option<usize>
{
	if hdr.len() < 4 { return None; }
	Some(hdr[1] as usize * 65536 + hdr[2] as usize * 256 + hdr[3] as usize)
}

fn split_buffer(mut data: &[&[u8]], mut mss: usize, mut f: impl FnMut(&[u8]) -> Result<(), Error>) ->
	Result<(), Error>
{
	//Cap mss at 16384.
	let mut tmp = [MaybeUninit::uninit();MAX_TLS_RECORD_PLAIN];
	if mss > tmp.len() { mss = tmp.len(); }
	sanity_check!(mss > 0, "Zero MSS");
	let mut tmp = BackedVector::new(&mut tmp[..mss]);
	let mut consumed = 0usize;
	while data.len() > 0 {
		//Discard empty buffers.
		if consumed >= data[0].len() {
			consumed -= data[0].len();
			data = &data[1..];
			continue;
		}
		let data0 = &data[0][consumed..];
		if tmp.len() > 0 {
			consumed += tmp.extend_fitting(data0);
			if tmp.is_full() || data.len() == 1 {
				f(tmp.as_inner())?;
				tmp.clear();
			}
			continue;
		}
		let mut chunks = data0.chunks_exact(mss);
		while let Some(chunk) = chunks.next() { f(chunk)?; }
		let chunk = chunks.remainder();
		if chunk.len() > 0 {
			if data.len() == 1 {
				//This is last chunk, submit it.
				f(chunk)?;
			} else {
				//This is not the last chunk, save it for further input next call.
				//chunk is always smaller, since chunk.len() < mss, and mss <= tmp.len().
				tmp.extend_fitting(chunk);
			}
		}
		//Processed data0.
		consumed += data0.len();
	}
	Ok(())
}

//Reply to internal data.
pub trait InternalReceiverReply
{
	fn reply(&mut self, data: &[u8]);
	fn send_end(&mut self);
	fn send_error(&mut self, error: Error);
}

struct InternalReceiverReplyData
{
	data: Vec<u8>,
	end: bool,
	alert: Option<Error>,
}

impl InternalReceiverReplyData
{
	fn new() -> InternalReceiverReplyData
	{
		InternalReceiverReplyData{data: Vec::new(), end: false, alert: None}
	}
}

impl InternalReceiverReply for InternalReceiverReplyData
{
	fn reply(&mut self, data: &[u8]) { self.data.extend_from_slice(data); }
	fn send_end(&mut self) { self.end = true; }
	fn send_error(&mut self, error: Error) { self.alert = Some(error); }
}

//Internal data receiver.
pub trait InternalReceiver
{
	fn receive(&mut self, x: &[u8], reply: &mut dyn InternalReceiverReply);
	fn receive_end(&mut self, reply: &mut dyn InternalReceiverReply);
	fn receive_init(&mut self, reply: &mut dyn InternalReceiverReply);
}

//Control message handler.
pub(crate) trait UpcallInterface
{
	//The type to send wakeups to.
	type Waker: FutureFinishedNotifier+Send+Clone+'static;

	fn recv_handshake(&mut self, mtype: HandshakeType, msg: &[u8],
		controller: &mut crate::record::UpcallReturn, last: bool,
		debug: crate::logging::DebugSetting) -> Result<(), Error>
	{
		let state = self._get_state_name();
		if !debug.prints_for(debug_class!(HANDSHAKE_DATA)) {
			//DEBUG_HANDSHAKE_DATA causes more detailed message, so
			//don't print this here.
			debug!(HANDSHAKE_MSGS debug, "[State {state}] Received {mtype}, length {mlen}",
				mlen=msg.len());
		}
		debug!(HANDSHAKE_DATA debug, "[State {state}] Received {mtype}:\n{payload}",
			payload=DumpHexblock::new(&msg, "  ", "Payload"));
		self._recv_handshake(mtype, msg, controller, last)?;
		if mtype == HandshakeType::FINISHED {
			//Receiving data is OK.
			controller.assert_hs_rx_finished();
		}
		Ok(())
	}
	fn recv_change_cipher_spec(&mut self, controller: &mut crate::record::UpcallReturn,
		debug: crate::logging::DebugSetting) -> Result<(), Error>
	{
		let state = self._get_state_name();
		debug!(HANDSHAKE_DATA debug, "[State {state}] Received change cipher spec");
		self._recv_change_cipher_spec(controller)
	}
	fn client_certificate(&mut self, info: ClientCertificateInfo)
	{
		self._get_callbacks().client_certificate(info)
	}
	//Set CID.
	fn _set_cid(&mut self, cid: u64);
	//Handle received handshake message.
	fn _recv_handshake(&mut self, mtype: HandshakeType, msg: &[u8], controller: &mut UpcallReturn,
		last: bool) -> Result<(), Error>;
	//Receive ChangeCipherSpec.
	fn _recv_change_cipher_spec(&mut self, controller: &mut UpcallReturn) -> Result<(), Error>;
	//Get name of state.
	fn _get_state_name(&self) -> &'static str;
	//Handle request for tx update
	fn handle_tx_update(&mut self, controller: &mut UpcallReturn) -> Result<(), Error>;
	//Get TlsCallbacks.
	fn _get_callbacks<'a>(&'a mut self) -> &'a mut dyn TlsCallbacks;
	//Wants RX?
	fn wants_rx(&self) -> bool;
	//Wants TX?
	fn wants_tx(&self) -> bool;
	//Is blocked?
	fn is_blocked(&self) -> bool;
	//Queue handshake transmit.
	fn queue_hs_tx(&mut self, base: &mut impl DowncallInterface, waker: <Self as UpcallInterface>::Waker) ->
		Result<bool, Error>;
}

struct UpcallInterfaceCallbacks<'a,U:UpcallInterface>(&'a mut U);

impl<'a,U:UpcallInterface> TlsCallbacks for UpcallInterfaceCallbacks<'a,U>
{
	fn client_certificate(&mut self, info: ClientCertificateInfo) { self.0.client_certificate(info) }
}

//Session control.
pub(crate) struct UpcallReturn
{
	queued_key_update: bool,
	tx_ku_flag: bool,
	hs_rx_finished: bool,
	queued_key_update_req: bool,
	use_tls12_records: bool,
	internal_flag: bool,
	abort: Option<Error>,
	next_extractor: Option<BoxedExtractor>,
	next_protector: Option<BoxedProtector>,
	next_deprotector: Option<BoxedDeprotector>,
	queued_key_update_state: &'static str,
	max_fragment_length: Option<usize>,
	new_internal_recv: Option<Box<dyn InternalReceiver+Send>>,
	trigger_internal_service_flag: bool,
	need_send_ccs: bool,
	handshake_data: Vec<u8>,
	force_unencrypted_alerts: Option<bool>,
	force_bidir_close: bool,
}

impl UpcallReturn
{
	//Set protector.
	//
	//The protector change will be activated once current record has been processed.
	pub(crate) fn set_protector(&mut self, obj: BoxedProtector) { self.next_protector = Some(obj); }
	//Set deprotector.
	//
	//The deprotector change will be activated once current record has been processed.
	pub(crate) fn set_deprotector(&mut self, obj: BoxedDeprotector) { self.next_deprotector = Some(obj); }
	//Set TLS extractor.
	//
	//The extractor change will be activated once current record has been processed.
	pub(crate) fn set_extractor(&mut self, obj: BoxedExtractor) { self.next_extractor = Some(obj); }
	//Abort handshake.
	//
	//Hanshake code should not call this, it is internally called after returning an error from handshake.
	pub(crate) fn abort_handshake(&mut self, fault: Error) { self.abort = Some(fault); }
	//Queue key update.
	//
	//The queued key update will be sent before the protector is changed, if one calls both send_key_update and
	//set_protector in the same upcall.
	pub(crate) fn send_key_update(&mut self, req_reply: bool, state: &'static str)
	{
		self.queued_key_update = true;
		self.queued_key_update_req |= req_reply;
		self.queued_key_update_state = state;
	}
	//Change to TLS 1.2 record version.
	pub(crate) fn set_use_tls12_records(&mut self) { self.use_tls12_records = true; }
	//Was last message sent a KeyUpdate?
	pub(crate) fn last_tx_key_update(&self) -> bool { self.tx_ku_flag }
	//Ok to send appdata.
	//
	//Hanshake code should not call this, it is internally called after processing Finished.
	pub(crate) fn assert_hs_rx_finished(&mut self) { self.hs_rx_finished = true; }
	//Set the maximum fragment length.
	pub(crate) fn set_max_fragment_length(&mut self, maxlen: usize) { self.max_fragment_length = Some(maxlen); }
	//Set the internal only receive.
	pub(crate) fn set_internal_receiver(&mut self, receiver: Box<dyn InternalReceiver+Send>)
	{
		self.new_internal_recv = Some(receiver);
	}
	//Get the internal only flag.
	pub(crate) fn is_internal_only(&self) -> bool { self.internal_flag || self.new_internal_recv.is_some() }
	//Trigger internal service.
	pub(crate) fn trigger_internal_service(&mut self) { self.trigger_internal_service_flag = true; }
	//Send a cleartext ChangeCipherSpec.
	pub(crate) fn send_clear_ccs(&mut self) { self.need_send_ccs = true; }
	//Send a small cleartext handshake message.
	pub(crate) fn send_hs_msg_clear(&mut self, data: &[u8]) { self.handshake_data.extend_from_slice(data); }
	//Set force unencrypted alerts flag (this prevents information leak on corrupt key).
	pub(crate) fn set_force_unencrypted_alerts(&mut self, flag: bool)
	{
		self.force_unencrypted_alerts = Some(flag)
	}
	//Force bidirectional close.
	pub(crate) fn force_bidir_close(&mut self) { self.force_bidir_close = true; }
}

#[derive(Copy,Clone,Debug,Bitflags)]
//If set, there is pending receive handshake request..
#[bitflag(RX_HS_REQUEST)]
//If set, there is pending transmit request.
//
//This is used to bootstrap sending handshake data:
// - Client starting to send client hello.
// - Restarting transmission after pausing for promise.
//
//The case where there is transmission already going on rolls on its own weight: TX/RX cycle transmit part lasts
//until handshake has no data to transmit. The direction switch from RX to TX is handled by performing TX/RX cycle
//after every read.
#[bitflag(TX_REQUEST)]
//If set, handshake was just queued.
#[bitflag(HANDSHAKE_QUEUED)]
//If set, last transmitted message was KeyUpdate
#[bitflag(LAST_TX_KEY_UPDATE)]
//If set, there is pending decrypted data.
#[bitflag(PENDING_DECRYPTED_DATA)]
//If set, force alerts to be unencrypted.
#[bitflag(FORCE_UNENCRYPTED_ALERTS)]
//If set, treat as internal.
#[bitflag(TREAT_AS_INTERNAL)]
pub struct Flags(u8);

//Downcall interface.
pub trait DowncallInterface
{
	fn set_use_tls12_records(&mut self);
	fn force_break(&mut self) -> Result<(), Error>;
	fn send_clear_ccs(&mut self);
	fn trigger_internal_service(&mut self);
	fn is_internal_only(&self) -> bool;
	fn change_deprotector(&mut self, deprotector: BoxedDeprotector);
	fn change_protector(&mut self, protector: BoxedProtector);
	fn change_extractor(&mut self, extractor: BoxedExtractor);
	fn force_bidir_close(&mut self);
	fn set_alerts_always_fatal(&mut self);
	fn treat_as_internal(&mut self);
	fn send_handshake_v3(&mut self, data: &dyn FragmentWriteDyn3, size_hint: usize, type_hint: HandshakeType,
		state: &str);
	fn next_record_may_be_unencrypted_alert(&mut self);
}

//Base session type.
struct SessionBase
{
	//Flags.
	flags: Flags,
	//The receive temporary buffer.
	receive_tmpbuf: Vec<u8>,
	//Handshake buffer.
	hs_buffer: HsBuffer,

	//Waiting buffer.
	waiting_buffer: Option<Vec<u8>>,
	//The received data queue.
	red_recv_q: Option<Vec<u8>>,
	//Send ciphertext queue. This queue contains encrypted data ready to be sent.
	send_q: Vec<u8>,
	send_q_ptr: usize,
	//Scratch buffer for operations.
	scratch: Vec<u8>,
	//Extractor.
	extractor: BoxedExtractor,
	//Debugging.
	debug: DebugSetting,
	//Pending handshake message size.
	pending_hs_size: usize,
	//Internal connection receiver.
	internal_receiver: Option<Box<dyn InternalReceiver+Send>>,
	//Internal context.
	internal: SessionInternalContext,
}

const MAX_RAW_RECV_BUF: usize = 24 * 1024;	//This needs to be larger than max TLS record size.
const MIN_TLS_RECORD_PLAIN: usize = 63;		//max_write_size. This is 63 for TLS 1.3.
const MAX_TLS_RECORD_PLAIN: usize = 16384;
//const MIN_TLS_OFFSET: usize = 13;		//5 byte header, 8 bytes of boobytrap.


impl DowncallInterface for SessionBase
{
	//Use TLS 1.2 record version.
	fn set_use_tls12_records(&mut self) { self.internal.set_use_tls12_records(); }
	//Force handshake message break.
	fn force_break(&mut self) -> Result<(), Error>
	{
		//If there is buffered handshake message in scratch buffer, send it.
		if self.pending_hs_size > 0 {
			//pending_hs_size should never be positive in raw mode, so this should be unreachable.
			sanity_check!(!self.internal.is_raw_handshake(),
				"force_break(): pending handshake message in raw mode");
			let hs_size = self.pending_hs_size;
			debug!(HANDSHAKE_DATA self.debug,
				"Handshake protocol: Flushing {hs_size} bytes of buffered handshake.");
			self.encrypt_record_noraw(ContentType::HANDSHAKE, hs_size);
			self.pending_hs_size = 0;
			//There probably is stuff to send now.
		}
		Ok(())
	}
	//Send a cleartext ChangeCipherSpec.
	fn send_clear_ccs(&mut self)
	{
		//This is just ignored in raw handshake mode.
		if self.internal.is_raw_handshake() { return; }
		//Do not send fake CCS twice.
		if !self.internal.send_fake_ccs() { return; }
		debug!(HANDSHAKE_EVENTS self.debug, "ChangeCipherSpec protocol: Sending record");
		debug!(HANDSHAKE_RAWDATA self.debug, "Sending fake CCS.");
		self.send_q.extend_from_slice(&[20, 3, 3, 0, 1, 1]);
	}
	fn trigger_internal_service(&mut self)
	{
		//internal_receiver can never be set in raw handshake mode. This causes the call to be just
		//ignored on raw handshake mode.
		let reply = if let Some(ref mut internal_receiver) = self.internal_receiver.as_mut() {
			let mut reply = InternalReceiverReplyData::new();
			internal_receiver.receive_init(&mut reply);
			Some(reply)
		} else {
			None
		};
		if let Some(reply) = reply { self.process_internal_reply_noraw(reply); }
	}
	//Get the internal only flag.
	fn is_internal_only(&self) -> bool { self.internal_receiver.is_some() }
	//Change deprotector
	fn change_deprotector(&mut self, deprotector: BoxedDeprotector)
	{
		//Message are synchronous (except for appdata), so we can change the deprotector here.
		self.internal.set_deprotector(deprotector);
	}
	//Change protector
	fn change_protector(&mut self, protector: BoxedProtector)
	{
		//Message are synchronous (except for appdata), so we can change the protector here.
		self.internal.set_protector(protector);
	}
	//Change extractor
	fn change_extractor(&mut self, extractor: BoxedExtractor)
	{
		//Message are synchronous (except for appdata), so we can change the extractor here.
		self.extractor = extractor;
	}
	fn force_bidir_close(&mut self)
	{
		self.internal.force_rx_eof();
		self.internal.set_eof_status();
		self.generate_abort_message();
	}
	//Set "alerts are always fatal" flag.
	fn set_alerts_always_fatal(&mut self) { self.internal.set_alerts_always_fatal(); }
	fn treat_as_internal(&mut self) { self.flags.set_TREAT_AS_INTERNAL(); }
	fn send_handshake_v3(&mut self, data: &dyn FragmentWriteDyn3, size_hint: usize, type_hint: HandshakeType,
		state: &str)
	{
		self.__send_handshake_v(DataRope3(data), size_hint, type_hint, state)
	}
	//Set one-shot "next record may be unencrypted alert" flag.
	fn next_record_may_be_unencrypted_alert(&mut self)
	{
		self.internal.set_maybe_next_unprotected_alert();
	}
}

fn datarope_send_big_message_loop(base: &mut SessionBase, fill: &mut usize, max_write_size: usize,
	mut f: impl FnMut(usize, &mut [u8]) -> usize) -> Result<(), Error>
{
	let mut offset = 0;
	let maxw = max_write_size;
	sanity_check!(maxw >= 32, "max_write_size too small (needs >=32)");
	loop {
		let tgt = match base.scratch.get_mut(*fill..maxw) {
			Some(tgt) => tgt,
			None => {
				//Out of range, trigger immediate flush.
				base.encrypt_record_noraw(ContentType::HANDSHAKE, maxw);
				*fill = 0;
				break;
			}
		};
		let amt = f(offset, tgt);
		if amt < tgt.len() {
			//Last partial record.
			*fill += amt;
			break;
		}
		//Entiere buffer got filled, flush
		offset += tgt.len();
		base.encrypt_record_noraw(ContentType::HANDSHAKE, maxw);
		*fill = 0;
		//It can happen that write fills the space exactly. In this case, next
		//call to write() will return 0 and trigger the above case to break
		//out of the loop.
	}
	Ok(())
}

fn datarope_send_small_message_loop(base: &mut SessionBase, bufoff: &mut usize, length: usize,
	f: impl FnOnce(&mut [u8])) -> Result<(), Error>
{
	let next = bufoff.checked_add(length).ok_or_else(arith_overflow)?;
	let tgt = base.scratch.get_mut(*bufoff..next).
		ok_or_else(||assert_failure!("Message fragment overflows scratch buffer"))?;
	*bufoff = next;
	f(tgt);
	Ok(())
}

struct DataRope3<'a>(&'a (dyn FragmentWriteDyn3+'a));

impl<'a> DataRope3<'a>
{
	fn send_big_message_loop(&self, base: &mut SessionBase, fill: &mut usize, max_write_size: usize) ->
		Result<(), Error>
	{
		datarope_send_big_message_loop(base, fill, max_write_size, |offset, tgt|{
			let mut tgt = BackedVector::new(unsafe{to_mu8_mut(tgt)});
			self.0.write_dyn(offset, &mut tgt);
			tgt.len()
		})
	}
	fn send_small_message_loop(&self, base: &mut SessionBase, bufoff: &mut usize) -> Result<(), Error>
	{
		let length = self.0.length_dyn();
		datarope_send_small_message_loop(base, bufoff, length, |tgt|{
			//This will always return length, so no need to check that.
			FragmentWriteOps3::to_slice_len_dyn(self.0, tgt);
		})
	}
	fn write_to_vector(&self, vector: &mut Vec<u8>)
	{
		FragmentWriteOps3::extend_vector_dyn(self.0, vector);
	}
	fn as_hexblock(&self, strip: usize, lpfx: &'a str, name: &'a str) -> DumpHexblock3<'a>
	{
		DumpHexblock3::new(self.0, strip, lpfx, name)
	}
}

impl SessionBase
{
	fn __send_handshake_v(&mut self, data: DataRope3, size_hint: usize,
		type_hint: HandshakeType, state: &str)
	{
		if !self.debug.prints_for(debug_class!(HANDSHAKE_DATA)) {
			//DEBUG_HANDSHAKE_DATA causes more detailed message, so don't print this here.
			debug!(HANDSHAKE_MSGS self.debug,
				"[State {state}] send handshake type {type_hint}, length {length}",
				length=size_hint.saturating_sub(4));
		}
		match self.send_handshake_message_inner(data, size_hint, type_hint, state) {
			Ok(_) => (),
			Err(ref fault) if self.internal.is_raw_handshake() => {
				self.internal.set_error(true, fault.clone());
				return;
			},
			//Not in raw handshake mode due t branch above.
			Err(x) => self.do_local_abort_noraw(x)
		}
	}
	//Send handshake message that is too big to fit into the buffer. After this call, pending_hs_size=0.
	//This must not be called in raw handshake mode.
	fn send_big_hs_message_noraw(&mut self, data: &DataRope3, size_hint: usize,
		type_hint: HandshakeType, max_write_size: usize) -> Result<(), Error>
	{
		//Large message. We can not handle 0 byte case here, so exclude it. Also, we need to limit the
		//max_write_size to buffer size available.
		sanity_check!(size_hint > 0, "Can't handle 0 byte message as large message");
		let max_write_size = min(max_write_size, self.scratch.len());
		sanity_check!(max_write_size > 0, "No data fits into the record scratch buffer");
		//The message is so large that is has to be fragmented. Flush the previous messages so we start
		//the fragmeted message from start of record.
		self.force_break()?;
		let mut fill = 0;
		data.send_big_message_loop(self, &mut fill, max_write_size)?;
		//If the last fragment was partial, flush it to line. Note that pending_hs_size=0, because the
		//force_break() call above. Also note that the handshake buffer is now empty afterwards.
		if fill > 0 {
			self.encrypt_record_noraw(ContentType::HANDSHAKE, fill);
		}
		debug!(HANDSHAKE_DATA self.debug,
			"Handshake protocol: Flushing immediately after record (htype {type_hint}, length {len})",
			len=size_hint.saturating_sub(4));
		Ok(())
	}
	//Send handshake message that fits into the buffer. After this call, pending_hs_size may be anything.
	//Returns true on success, false  if message is too big to send this way. This must not be called in raw
	//handshake mode.
	fn send_small_hs_message_noraw(&mut self, data: &DataRope3, size_hint: usize,
		max_write_size: usize) -> Result<bool, Error>
	{
		//We may have to fragment the message. Check if the message fits into handshake message buffer.
		//If not, force a break, so we may start sending the large message from empty buffer.
		if size_hint > max_write_size.saturating_sub(self.pending_hs_size) { self.force_break()?; }
		//If the message to be sent is small enough to fit into message buffer, write it there.
		//Note that the force_break() call may invalidate self.pending_hs_size, so we need to recheck the
		//condition.
		if size_hint <= max_write_size.saturating_sub(self.pending_hs_size) {
			//Just append the message into buffer.
			let mut bufoff = self.pending_hs_size;
			data.send_small_message_loop(self, &mut bufoff)?;
			//Calculate the size of pending handshake messages.
			self.pending_hs_size = bufoff;
			//If message type is HandshakeType::KEY_UPDATE, we need to force a break, since keys will
			//change after this.
			if self.flags.is_LAST_TX_KEY_UPDATE() { self.force_break()?; }
			return Ok(true);	//Sent.
		}
		Ok(false)	//Too big!
	}
	//Send an encrypted record. this can not be used in raw handshake mode.
	fn encrypt_record_noraw(&mut self, rtype: ContentType, plen: usize)
	{
		//plen can be at most MAX_TLS_RECORD_PLAIN.
		let max_write_size = if self.internal.is_encrypted() {
			self.internal.get_max_fragment_length()
		} else {
			MAX_TLS_RECORD_PLAIN
		};
		if plen > max_write_size {
			//This can never be called in raw handshake mode, because this whole routine can not be
			//used in raw handshake mode.
			self.do_local_abort_noraw(Error::from(assert_failure!(F
				"Packet to send too big ({plen}, maximum is {max_write_size}")));
			return;
		}
		//KeyUpdate must be HANDSHAKE, so anything else can not be KeyUpdate.
		if rtype != ContentType::HANDSHAKE { self.flags.clear_LAST_TX_KEY_UPDATE(); }
		//Using this when processing abort causes double abort if it fails. No need to concern about raw
		//handshake mode, because this routine can not be called in raw handshake mode.
		//plen + MIN_TLS_OFFSET is smaller than scratch buffer.
		let pkt = self.scratch.get(..plen).unwrap_or(&[]);
		if rtype == ContentType::HANDSHAKE {
			debug!(HANDSHAKE_RAWDATA self.debug, "Sending raw handshake record:\n{payload}",
				payload=DumpHexblock::new(pkt, " ", "RawdataOut"));
		}
		//If desired, pad the record.
		let extra = if self.internal.pad_to_maximum_active() { max_write_size - plen } else { 0 };
		if let Err(err) = self.internal.protect(&mut self.send_q, rtype, pkt, extra) {
			return self.do_local_abort_noraw(Error::from(err));
		};
	}
	//Send EOF. This can not be used in raw handshake mode.
	fn do_send_eof_noraw(&mut self)
	{
		debug!(ABORTS self.debug, "Alert protocol: Sending alert#0: End of Stream");
		self.internal.set_eof_status();
	}
	//Send a local abort. If there is already local abort in process, this causes double abort, which finishes
	//instantly, but retains the old abort number (unless it was closure alert). This must not be called in
	//raw handshake mode.
	fn do_local_abort_noraw(&mut self, fault: Error)
	{
		let code = fault.tls_alert_num2();
		debug!(ABORTS self.debug, "Alert protocol: Sending alert {code}: {fault}");
		let mut do_double_abort = false;
		if self.internal.set_abort_code(fault) {
			debug!(ABORTS self.debug, "Alert protocol: Fatal recursive alert");
			do_double_abort = true;
		}
		//Additionally, double aborts complete instantly. Otherwise, assert TX IRQ for new data.
		if do_double_abort {
			self.abort_completed_noraw("alert while trying to send alert");
		}
	}
	//Notify of completed abort. This must not be called in raw handshake mode.
	fn abort_completed_noraw(&mut self, subcat: &str)
	{
		debug!(ABORTS self.debug, "Alert protocol: Processing completed: ({subcat})");
	}
	//Process reply from internal service. Must not be called in raw handshake mode.
	fn process_internal_reply_noraw(&mut self, reply: InternalReceiverReplyData)
	{
		//Clear the record, it was processed (if it was there in the first place).
		self.flags.clear_PENDING_DECRYPTED_DATA();
		self.receive_tmpbuf.clear();
		//Process the stuff in reply.
		if reply.data.len() > 0 { self.write_internal(&reply.data[..]); }
		if let Some(error) = reply.alert {
			self.do_local_abort_noraw(error);
		} else if reply.end {
			self.write_eos_internal();
		}
	}
	//Send handshake message. size_hint includes headers. The contents are dumped for debugging if enabled.
	fn send_handshake_message_inner(&mut self, data: DataRope3, size_hint: usize,
		type_hint: HandshakeType, state: &str) -> Result<(), Error>
	{
		//Update LastTxKeyUpdate field.
		self.flags.force_LAST_TX_KEY_UPDATE(type_hint == HandshakeType::KEY_UPDATE);

		//The size without headers is size_hint - 4. max_write_size is valid fragment size due to internal
		//check in get_max_fragment_length().
		let rhsize = size_hint.saturating_sub(4);
		//get_max_fragment_length() can only be used if encrypted. Otherwise the maximum is always
		//MAX_TLS_RECORD_PLAIN.
		let max_write_size = if self.internal.is_encrypted() {
			self.internal.get_max_fragment_length()
		} else {
			MAX_TLS_RECORD_PLAIN
		};
		//Sanity checks:
		// - Largest possible plaintext must fit into scratch buffer.
		// - The message can be at most 16MB - 1.
		sanity_check!(self.scratch.len() >= MAX_TLS_RECORD_PLAIN,
			 "Max record size overflows scratch buffer ({len}, need {MAX_TLS_RECORD_PLAIN})",
			len=self.scratch.len());
		sanity_check!(rhsize <= 0xFFFFFF, F "Handshake message over 16MB ({rhsize})");

		//Print a debug message dumping the handshake message if enabled.
		debug!(HANDSHAKE_DATA self.debug, "[State {state}] Sending {type_hint}:\n{payload}",
			payload=data.as_hexblock(4, "  ", "Payload"));

		//In raw handshake mode, just stuff the message into output buffer.
		if self.internal.is_raw_handshake() {
			//Write the raw handshake message into send_q and assert the txirq.
			data.write_to_vector(&mut self.send_q);
			return Ok(());
		}
		//Not in raw handshake mode. We may have to fragment the message. First try sending a small
		//messge. This can not hapen in raw handshake mode, because of the check above.
		if self.send_small_hs_message_noraw(&data, size_hint, max_write_size)? {
			return Ok(());	//Ok, sent.
		}
		//The message is too big to send in one go, fragment it. This definitely writes something into
		//handshake buffer, so assert txirq afterwards. This can not hapen in raw handshake mode,
		//because of the check above.
		self.send_big_hs_message_noraw(&data, size_hint, type_hint, max_write_size)?;
		Ok(())
	}
	//Make a UpcallReturn.
	fn make_session_receiver(&self) -> UpcallReturn
	{
		UpcallReturn {
			abort: None,
			next_extractor: None,
			next_protector: None,
			next_deprotector: None,
			queued_key_update: false,
			queued_key_update_req: false,
			queued_key_update_state: "",
			use_tls12_records: false,
			tx_ku_flag: self.flags.is_LAST_TX_KEY_UPDATE(),
			hs_rx_finished: false,
			max_fragment_length: None,
			internal_flag: self.internal_receiver.is_some(),
			new_internal_recv: None,
			trigger_internal_service_flag: false,
			need_send_ccs: false,
			handshake_data: Vec::new(),
			force_unencrypted_alerts: None,
			force_bidir_close: false,
		}
	}
	//Process updates from UpcallReturn. If this fails, the failure should be immediately propagated
	//out from read.
	fn process_session_receiver(&mut self, receiver: UpcallReturn) -> Result<bool, Error>
	{
		//The highest priority event is change in maximum fragment length.
		if let Some(x) = receiver.max_fragment_length {
			self.internal.set_max_fragment_length(x);
		}
		//Next, comes queued key udates.
		if receiver.queued_key_update {
			let arg = if receiver.queued_key_update_req { 1 } else { 0 };
			let msg = [HandshakeType::KEY_UPDATE.get(), 0, 0, 1, arg];
			let msg = &msg[..];
			self.send_handshake_v3(&msg, msg.len(), HandshakeType::KEY_UPDATE,
				receiver.queued_key_update_state);
		}
		//Next, any queued aborts.
		if let Some(x) = receiver.abort {
			//Restart is just magic. It gets signaled as an immediate error besides aborting the
			//handshake. The same happens in raw handshake mode.
			if self.internal.is_raw_handshake() || x.is_restart().is_some() {
				self.internal.set_error(true, x.clone());
				fail!(x);
			}
			//Trigger normal abort. Note that this is not a failure, because the code will wait for the
			//abort to complete.
			self.abort_handshake(x);
			return Ok(false);
		}
		//Next, if TLS 1.2 record layer is requested, enable it.
		if receiver.use_tls12_records {
			self.set_use_tls12_records();
		}
		//Next, process changes to force_unencrypted_alerts.
		if let Some(force_unencrypted_alerts) = receiver.force_unencrypted_alerts {
			self.flags.force_FORCE_UNENCRYPTED_ALERTS(force_unencrypted_alerts);
		}
		//If there are special handshake messages, those are emitted before any CCS or key changes. This
		//is actually only used for HelloRetryRequest.
		if receiver.handshake_data.len() > 0 {
			let h = &receiver.handshake_data;
			sanity_check!(h.len() <= 16384, "Special handshake message too big ({hlen})", hlen=h.len());
			debug!(HANDSHAKE_DATA self.debug, "Handshake: Special outgoing data:\n{payload}",
				payload=DumpHexblock::new(h, "  ", "Payload"));
			//Assume these messages are small enough not to need fragmentation, and that writes can
			//not fail. If not in raw handshake mode, need to write record header.
			if !self.internal.is_raw_handshake() {
				//Reseve space also for the next push.
				self.send_q.reserve(receiver.handshake_data.len() + 5);
				let hdr: [u8;5] = [22, 3, 3, (h.len() >> 8) as u8, h.len() as u8];
				self.send_q.extend_from_slice(&hdr);
			}
			self.send_q.extend_from_slice(&receiver.handshake_data);
		}
		//Emit the CSS. This needs to happen before any key changes.
		if receiver.need_send_ccs { self.send_clear_ccs(); }
		//Change cryptographic context if requested.
		if let Some(x) = receiver.next_extractor { self.extractor = x; }
		if let Some(x) = receiver.next_protector { self.internal.set_protector(x); }
		if let Some(x) = receiver.next_deprotector { self.internal.set_deprotector(x); }
		//Set internal receiver if requested. Note this will not work in internal handshake mode.
		if let Some(x) = receiver.new_internal_recv {
			sanity_check!(!self.internal.is_raw_handshake(),
				"Trying to use internal service is raw handshake mode");
			self.internal_receiver = Some(x);
		}
		//If force_bidir_close is set, send bidirectional EOF by forcing close_notify alert.
		if receiver.force_bidir_close { self.force_bidir_close(); }
		//Set the handshake complete flag if needed.
		if receiver.hs_rx_finished { self.internal.notify_handshake_received(); }
		//If needed, trigger the internal service, passing the init event to it.
		if receiver.trigger_internal_service_flag {
			self.trigger_internal_service();
		}
		//tx_ku_flag stays constant.
		Ok(true)
	}
	//Trigger a protector update, queuing the KeyUpdate message. This can fail only in raw handshake mode.
	fn prod_protector_update(&mut self, controller: &mut impl UpcallInterface) -> Result<(), Error>
	{
		let mut receiver = self.make_session_receiver();
		if let Err(err) = controller.handle_tx_update(&mut receiver) {
			receiver.abort_handshake(err);
		}
		self.process_session_receiver(receiver)?;
		Ok(())
	}
	//Handle a decrypted raw handshake record.
	fn handle_decrypted_handshake(&mut self, output: &[u8], controller: &mut impl UpcallInterface,
		receiver: &mut UpcallReturn) -> Result<(), Error>
	{
		let rawhs = self.internal.is_raw_handshake();
		fail_if!(!rawhs && output.len() == 0, error!(empty_handshake_message));
		debug!(HANDSHAKE_RAWDATA self.debug, "Received raw handshake record:\n{payload}",
			payload=DumpHexblock::new(output, " ", "RawdataIn"));
		let mut _msg = output;
		if self.hs_buffer.is_idle() || rawhs {
			//Grab entiere messages at once.
			loop {
				//4 bytes at least.
				let hlen = f_break!(read_hsmsg_len(_msg));
				//Check if enough space (hlen is at most 16MB, well in bounds).
				//Take the entiere record.
				let (hmsg, trailer) = f_break!(split_at(_msg, hlen + 4));
				let mtype = HandshakeType::new(hmsg[0]);
				let mpayload = &hmsg[4..];	//At least 4 bytes.
				_msg = trailer;
				let last = _msg.len() == 0;
				controller.recv_handshake(mtype, mpayload, receiver, last, self.debug.clone())?;
			}
			//Buffer the remaining messages.
			if _msg.len() > 0 && receiver.abort.is_none() {
				match self.hs_buffer.extract(&mut _msg) {
					Ok(None) => (),
					Ok(Some(_)) => sanity_failed!(
						"No handshake messages but handshake buffer extracted one"),
					Err(err) => fail!(error!(hs_msg_buffer err))
				}
			}
			return Ok(());
		}
		loop { match self.hs_buffer.extract(&mut _msg) {
			Ok(None) => break,
			Ok(Some((htype, payload, last))) => {
				controller.recv_handshake(htype, &payload, receiver, last, self.debug.clone())?;
			},
			Err(err) => fail!(error!(hs_msg_buffer err))
		}}
		Ok(())
	}
	//Handle a decrypted raw handshake record.
	fn handle_decrypted_ccs(&mut self, output: &[u8], controller: &mut impl UpcallInterface,
		receiver: &mut UpcallReturn) -> Result<(), Error>
	{
		fail_if!(!self.hs_buffer.empty(), error!(ccs_in_middle_of_handshake_message));
		//The || short-circuits, so the array access has to have output.len()=1 => in bounds.
		fail_if!(output.len() != 1 || output[0] != 1, error!(invalid_ccs));
		debug!(HANDSHAKE_RAWDATA self.debug, "Received CCS");
		controller.recv_change_cipher_spec(receiver, self.debug.clone())?;
		Ok(())
	}
	//Handle a decrypted raw control (handshake or CSS) record.
	fn handle_decrypted_record_control(&mut self, code: ContentType, output: &[u8],
		controller: &mut impl UpcallInterface) -> Result<bool, Error>
	{
		let mut receiver = self.make_session_receiver();
		self.flags.clear_RX_HS_REQUEST();
		if let Err(err) = if code == ContentType::HANDSHAKE {
			self.handle_decrypted_handshake(output, controller, &mut receiver)
		} else if code == ContentType::CHANGE_CIPHER_SPEC {
			self.handle_decrypted_ccs(output, controller, &mut receiver)
		} else {
			//What the hell is this? Abort connection with unexpected message.
			Err(error!(unexpected_rtype code))
		} {
			receiver.abort_handshake(err);
		};
		//No data received, pause processing if required. In case of restart abort, we need to
		//trigger error from read, and this means failing.
		Ok(self.process_session_receiver(receiver)?)
	}
	fn __handle_post_flush(&mut self, len: usize) -> Result<(), Error>
	{
		//Discard the written data. If this completes the write, discard buffer.
		self.send_q_ptr += len;
		if self.send_q_ptr >= self.send_q.len() {
			self.send_q.clear();
			self.send_q_ptr = 0;
			//If handshake was transmitted, release the memory held by the send queue, as
			//post-handshake messages are much smaller. This should only trigger once.
			if self.flags.is_HANDSHAKE_QUEUED() { self.send_q = Vec::new(); }
			self.flags.clear_HANDSHAKE_QUEUED();
		}
		//Set the outgoing EOF flag or abort completed if we have gotten that far. Also raise the
		//TxEof or ConnectionReset event if we got that far. This can never happen in raw handshake
		//mode because all aborts in it complete immediately, and EOFs can be never sent.
		if self.internal.run_countdown(len) || self.internal.is_eof_sent() {
			sanity_check!(!self.internal.is_raw_handshake(),
				"flush_queue() running countdown in raw handshake mode");
			//Complete abort if any. We can not be in raw handshake mode due to the check
			//above.
			self.abort_completed_noraw("Alert sent");
		}
		Ok(())
	}
	//Flush queue to vector.
	fn flush_queue_vec(&mut self, output: &mut Vec<u8>) -> Result<(), Error>
	{
		self.flags.clear_TX_REQUEST();	//Disarm TX request.
		if self.send_q_ptr < self.send_q.len() {
			//We have some data to send first...
			let src = &self.send_q[self.send_q_ptr..];
			let len = src.len();
			output.extend_from_slice(src);
			self.__handle_post_flush(len)?;
		}
		Ok(())
	}
	//Generate an alert from present abort. This must not be used to generate alerts in raw handshake mode.
	fn generate_abort_message(&mut self)
	{
		//Schedule alerts to be sent.
		let acode = f_return!(self.internal.ack_pending_abort(), ());
		const ALERT: ContentType = ContentType::ALERT;
		let warning = acode.is_close() && !self.internal.get_alerts_always_fatal();
		let severity = if warning { 1 } else { 2 };
		let code = acode.get();
		if self.flags.is_FORCE_UNENCRYPTED_ALERTS() || !self.internal.is_encrypted() {
			//Handle unencrypted alerts specially.
			self.send_q.extend_from_slice(&[ALERT.get(), 3, 3, 0, 2, severity, code]);
		} else {
			//Normal encrypted alert.
			let maxmss = self.internal.get_max_fragment_length();
			let scratch = match self.scratch.get_mut(..maxmss) {
				Some(x) => x,
				None => &mut self.scratch
			};
			if scratch.len() < 2 { return; }	//Much too small.
			scratch[0] = severity;
			scratch[1] = code;
			self.encrypt_record_noraw(ContentType::ALERT, 2);
		}
		//Schedule outgoing_eof or abort_completed to be set when this alert message has been sent.
		let remain = self.send_q.len().saturating_sub(self.send_q_ptr);
		self.internal.set_countdown_timer(remain);
	}
}

impl SessionBase
{
	//Create a new session base.
	fn new(rawhs: bool, debug: DebugSetting) -> SessionBase
	{
		SessionBase {
			flags: Flags::none,
			receive_tmpbuf: Vec::new(),
			hs_buffer: HsBuffer::new(),

			waiting_buffer: None,
			red_recv_q: None,
			send_q: Vec::new(),
			send_q_ptr: 0,
			scratch: vec![0;MAX_RAW_RECV_BUF],
			internal_receiver: None,
			internal: SessionInternalContext::new(rawhs),
			extractor: BoxedExtractor::new_null(),
			debug: debug,
			pending_hs_size: 0,
		}
	}
	//Abort handshake and set error.
	fn abort_handshake(&mut self, fault: Error)
	{
		//In raw handshake mode, aborts complete instantly.
		if self.internal.is_raw_handshake() {
			self.internal.set_error(true, fault.clone());
			return;
		}
		//Do basic handling of the fault. This can not be called in raw handshake mode due to the check
		//above.
		self.do_local_abort_noraw(fault.clone());
		//Overwrite the error message with HandshakeError class error instead of generic class error.
		//Also, turn possible close notify into error (including clearing close_notify_done).
		self.internal.set_error(false, fault);
		self.internal.clear_close_notify_done();
	}
	//Set TX handshake finished flag.
	fn finish_tx_handshake(&mut self)
	{
		//Set HandshakeQueued, so transmit knows to drop queues.
		self.internal.notify_handshake_transmitted();
		self.flags.set_HANDSHAKE_QUEUED();
	}
	//Set Request RX handshake flag.
	fn request_rx_handshake(&mut self)
	{
		self.flags.set_RX_HS_REQUEST();
	}
	//Set tx_request flag to active. Also, if TX IRQ is not already active, activate if if needed.
	fn do_tx_request(&mut self)
	{
		self.flags.set_TX_REQUEST();
	}
	fn extractor(&self, label: &str, context: Option<&[u8]>, buffer: &mut [u8]) -> Result<(), Error>
	{
		Ok(self.extractor.extract(label, context, buffer, &mut ks_debug!(&self.debug))?)
	}
	fn raise_alert(&mut self, code: Alert)
	{
		//In raw handshake mode, trigger immediate abort.
		if self.internal.is_raw_handshake() {
			let fault = error!(raise_alert code);
			self.internal.set_error(true, fault.clone());
			return;
		}
		if code != Alert::CLOSE_NOTIFY {
			//We can not be in raw handshake mode due to the check above.
			self.do_local_abort_noraw(error!(raise_alert code));
		} else {
			//code = 0 is special. This forces input eof and sends output eof. Ignore any failures
			//from sending EOF out, those failures come apparent when application tries to send out
			//the close notify.
			self.internal.force_rx_eof();
			//Ignored if dead, internal receiver or raw handshake.
			if self.internal.aborted_by().is_some() || self.internal_receiver.is_some() ||
				self.internal.is_raw_handshake() || self.internal.is_pending_abort() ||
				self.internal.is_close_notify_done() { return; }
			debug!(ABORTS self.debug, "Alert protocol: Sending alert#0: End of Stream");
			self.internal.set_eof_status();
			self.generate_abort_message();
		}
	}
	fn _pull_tcp_data_housekeeping(&mut self, controller: &mut impl UpcallInterface) -> Result<Vec<u8>, Error>
	{
		//Reuse waiting buffer if any.
		let mut output = self.waiting_buffer.take().unwrap_or(Vec::new());
		output.clear();
		self.flush_queue_vec(&mut output)?;
		//In raw handshake mode, we can never have any alert to send, beause in raw handshake mode, all
		//alerts complete immediately, and the above errors out in that case.
		self.generate_abort_message();
		if self.internal.is_handshake_transmitted() && self.internal.is_pending_change() {
			self.internal.clear_pending_change();
			self.prod_protector_update(controller)?;
		}
		//Try flushing the queue again to get the data produced.
		self.flush_queue_vec(&mut output)?;
		Ok(output)
	}
	fn __tx_close(&mut self, output: &mut Vec<u8>) -> Result<(), Error>
	{
		//Schedule EOS to be sent. This can not happen in raw handshake mode due to the check above.
		if !self.internal.is_pending_abort() && !self.internal.is_close_notify_done() {
			//This sets queued_eof and generates the alert for it. Not using do_send_eof_noraw, because
			//that would unconditionally assert the TX IRQ, which is not desired.
			debug!(ABORTS self.debug, "Alert protocol: Sending alert#0: End of Stream");
			self.internal.set_eof_status();
			self.generate_abort_message();
		}
		self.flush_queue_vec(output)
	}
	fn __prepare_tx_data(&mut self, output: &mut Vec<u8>, total_len: usize) ->
		Result<(usize, bool), Error>
	{
		//What's the MSS? It is bounded by the TLS maximum, maximum fragment size and any application
		//wishes.
		let mss = self.internal.get_max_fragment_length();
		let mut whole_fragments = total_len / mss;
		let mut remainder = total_len % mss;
		//If padmax, round up to whole fragments.
		let padmax = self.internal.pad_to_maximum_active();
		if padmax && remainder > 0 {
			whole_fragments += 1;
			remainder = 0;
		}
		let wfsize = self.internal.encrypted_size(mss);
		let pfsize = if remainder > 0 { self.internal.encrypted_size(remainder) } else { 0 };
		let need = whole_fragments.saturating_mul(wfsize).saturating_add(pfsize);
		//Check the needed size against maximum possible, as it will overflow if raw data does anyway
		//(but also catches overflows from overhead).
		fail_if!(need > core::isize::MAX as usize, assert_failure!("Too much data to send"));
		output.reserve(need);
		Ok((mss, padmax))
	}
	fn __tx_data_single(&mut self, output: &mut Vec<u8>, data: &[u8]) ->
		Result<(), Error>
	{
		if data.len() == 0 { return Ok(()); }	//Nothing to send.
		let (mss, padmax) = self.__prepare_tx_data(output, data.len())?;
		sanity_check!(mss > 0, "Zero MSS");
		for frag in data.chunks(mss) {
			let mut pad = 0;
			if padmax && frag.len() < mss { pad = mss - frag.len(); }
			self.internal.protect(output, ContentType::APPLICATION_DATA, frag, pad)?;
		}
		Ok(())
	}
	fn __tx_data_multi(&mut self, output: &mut Vec<u8>, data: &[&[u8]]) ->
		Result<(), Error>
	{
		let total_len = data.iter().fold(0usize, |x,y|x.saturating_add(y.len()));
		if total_len == 0 { return Ok(()); }	//Nothing to send.
		let (mss, padmax) = self.__prepare_tx_data(output, total_len)?;
		split_buffer(data, mss, |frag|{
			let mut pad = 0;
			if padmax && frag.len() < mss { pad = mss - frag.len(); }
			self.internal.protect(output, ContentType::APPLICATION_DATA, frag, pad)?;
			Ok(())
		})?;
		Ok(())
	}
	fn pull_tcp_data(&mut self, controller: &mut impl UpcallInterface, data: PullTcpBuffer) ->
		Result<Vec<u8>, Error>
	{
		//If an abort has completed, return the error.
		match self.internal.aborted_by() {
			Some(Some(x)) => fail!(x),
			//Normal shutdown impiles that connection has been closed for sending before.
			Some(None) => if data.data.is_some() || data.eof {
				sanity_failed!("Connection already closed for sending");
			} else {
				Ok(Vec::new())
			},
			None => {
				//If data is to be sent, check that connection is ready for that.
				if data.data.is_some() || data.eof {
					fail_if!(self.internal.is_raw_handshake(),
						assert_failure!("Can not send data in raw handshake mode"));
					//Check tx_end before may_tx, as closed connections should give error
					//about those being closed.
					fail_if!(self.is_tx_end(),
						assert_failure!("Connection already closed for sending"));
					//Special case: In case of pending abort, allow application data, just
					//ignore it.
					fail_if!(!self.__is_may_tx(true),
						assert_failure!("Connection not yet ready to send data"));
				}
				let mut output = self._pull_tcp_data_housekeeping(controller)?;
				//Do not call tx data with pending abort, it does not work.
				if !self.__is_may_tx(false) { return Ok(output); }
				match data.data {
					Some(DataBuffer::Multi(data)) =>
						self.__tx_data_multi(&mut output, data)?,
					Some(DataBuffer::Single(data)) =>
						self.__tx_data_single(&mut output, data)?,
					None => ()
				}
				if data.eof { self.__tx_close(&mut output)?; }
				Ok(output)
			},
		}
	}
	fn write_internal(&mut self, buffer: &[u8])
	{
		//Ignore if is raw handshake is set.
		if self.internal.is_raw_handshake() { return; }
		//Assume this is all encrypted.
		let maxpacket = self.internal.get_max_fragment_length();
		let mut itr = 0;
		while itr < buffer.len() {
			let maxcopy = min(buffer.len().saturating_sub(itr), maxpacket);
			(&mut self.scratch[..maxcopy]).copy_from_slice(&buffer[itr..][..maxcopy]);
			//Can not be called in raw handshake mode due to the check above.
			self.encrypt_record_noraw(ContentType::APPLICATION_DATA, maxcopy);
			itr += maxcopy;
		}
	}
	fn write_eos_internal(&mut self)
	{
		//Ignore if is raw handshake is set.
		if self.internal.is_raw_handshake() { return; }
		//Schedule EOS to be sent.
		if !self.internal.is_pending_abort() && !self.internal.is_close_notify_done() {
			//This sets queued_eof and generates the alert for it. Can not be called in raw handshake
			//mode due to the check above.
			self.do_send_eof_noraw();
			self.generate_abort_message();
		}
	}
	fn rawhs_promote_error(&self) -> Result<(), Error>
	{
		//In raw handshake mode, if there is an error, return it.
		if self.internal.is_raw_handshake() {
			if let Some(Some(x)) = self.internal.aborted_by() { fail!(x); }
		}
		Ok(())
	}
	fn set_cid(&mut self, cid: u64, upcall: &mut impl UpcallInterface)
	{
		self.debug.set_cid(cid);
		upcall._set_cid(cid);
	}
	fn is_may_rx(&self) -> bool
	{
		//Internal receviers can not be read.
		if self.internal_receiver.is_some() || self.internal.is_raw_handshake() { return false; }
		//May rx if handshake has been received, and no EOF has been received.
		self.internal.is_handshake_received() && !self.internal.is_receive_eof()
	}
	fn __is_may_tx(&self, past: bool) -> bool
	{
		//Can not write if internal receiver or if close_notify has been generated.
		if self.internal_receiver.is_some() || self.internal.is_close_notify_done() ||
			self.internal.is_raw_handshake() {
			return false;
		}
		//can_send_appdata() returns Err in raw handshake mode, which may not send data.
		self.internal.can_send_appdata(past) == Ok(true)
	}
	fn is_may_tx(&self) -> bool { self.__is_may_tx(false) }
	fn is_rx_end(&self) -> bool
	{
		//Internal receviers can not be read.
		if self.internal_receiver.is_some() || self.internal.is_raw_handshake() { return false; }
		self.internal.is_receive_eof()
	}
	fn is_tx_end(&self) -> bool
	{
		//Internal receviers can not be written.
		if self.internal_receiver.is_some() || self.internal.is_raw_handshake() { return false; }
		self.internal.is_close_notify_done()
	}
	fn is_want_rx(&self) -> bool
	{
		//If abort has completed, do not want more data, the connection is dead.
		if self.internal.is_abort_complete() { return false; }
		//If connection has seen bidirectional closes, do not want more data, the connection is dead.
		//is_eof_sent() internally waits for send buffer flush, so no need to check for buffer status.
		if self.internal.is_receive_eof() && self.internal.is_eof_sent() { return false; }
		//If in handshake, return the status of RxHsRequest, which is flag telling if handshake handler
		//is requesting more handshake messages. But do not return readable if there is buffered data
		//(handshake is half-duplex).
		if !self.internal.is_handshake_received() {
			return self.flags.is_RX_HS_REQUEST() && self.send_q_ptr >= self.send_q.len();
		}
		//Otherwise, the connection is in showtime, want more data. This is even if in raw handshake mode,
		//because of post-handshake messages.
		true
	}
	fn is_want_tx(&self) -> bool
	{
		//If abort has completed, do not want send more data, the connection is dead.
		if self.internal.is_abort_complete() { return false; }
		//Otherwise the conditions we want to write in are:
		// - Pending change of protector.
		// - Data in send buffer.
		// - Pending abort.
		// - Tx request has been asserted.
		self.internal.is_pending_change() || self.send_q_ptr < self.send_q.len() ||
			self.internal.is_pending_abort() || self.flags.is_TX_REQUEST()
	}
	fn is_handshaking(&self) -> bool
	{
		//Internal connections always get stuck in handshake, unless dead.
		if self.internal_receiver.is_some() && !self.is_dead(){ return true; }
		!self.internal.is_handshake_received() || !self.internal.is_handshake_transmitted()
	}
	fn is_dead(&self) -> bool
	{
		self.internal.is_receive_eof() && self.internal.is_eof_sent() || self.internal.is_abort_complete()
	}
	fn is_aborted(&self) -> bool
	{
		self.internal.is_abort_complete()
	}
	fn get_error(&self) -> Option<Error>
	{
		self.internal.aborted_by().unwrap_or(None)
	}
	//SAFETY: First <maxsize> bytes of <buffer> (concatenated) SHALL be initialized (if maxamt is None, all of
	//it SHALL be initialized).
	unsafe fn push_tcp_data(&mut self, data: &[&[MaybeUninit<u8>]], maxamt: Option<usize>,
		controller: &mut impl UpcallInterface) -> Result<(Vec<u8>, bool), Error>
	{
		const MAX_HANDSHAKE: u32 = 131072;
		//If abort has completed, return error.
		match self.internal.aborted_by() {
			Some(Some(err)) => fail!(err),
			//This is bidirectional teardown, which impiles RX end.
			Some(None) => return Ok((Vec::new(), true)),
			None => (),
		}
		//Do not process data if in error. The status of EOF flag is frozen.
		if self.internal.is_aborted() {
			let eof = self.is_rx_end();
			return Ok((Vec::new(), eof));
		}
		let rawmax = if self.internal.is_raw_handshake() { Some(MAX_HANDSHAKE) } else { None };
		//The safety invariant of this function is exactly the safety invariant of the function below.
		let mut buf = unsafe{self::utils::ReadState::new(data, maxamt, rawmax)};
		//Take the receive queue. If there is nothing to be taken, create a new empty one.
		let mut red_recv_q = self.red_recv_q.take().unwrap_or(Vec::new());
		while !self.internal.is_aborted() { match buf.get(&mut self.receive_tmpbuf)? {
			self::utils::ReadResult::None => break,
			self::utils::ReadResult::Direct(buf) =>
				self.__push_tcp_record(buf, controller, &mut red_recv_q, rawmax)?,
			self::utils::ReadResult::Indirect => {
				//Submit the record. Temporarily swap receive_tmpbuf in order to disable
				//borrow checker. Any errors are terminal. Ignore any further input in
				//error state. Clear the buffer, as its contents are no longer needed.
				let tmp = replace(&mut self.receive_tmpbuf, Vec::new());
				self.__push_tcp_record(&tmp, controller, &mut red_recv_q, rawmax)?;
				self.receive_tmpbuf = tmp;
				self.receive_tmpbuf.clear();
			},
		}}
		//If red_recv_q is empty, return it, as application will not need it, but we do.
		if red_recv_q.is_empty() {
			let tmp = replace(&mut red_recv_q, Vec::new());
			self.red_recv_q = Some(tmp);
		}
		let eof = self.is_rx_end();
		Ok((red_recv_q, eof))
	}
	fn __push_tcp_record(&mut self, record: &[u8], controller: &mut impl UpcallInterface,
		red_recv_q: &mut Vec<u8>, rawmax: Option<u32>) -> Result<(), Error>
	{
		//Some errors abort immediately, rest do not.
		let ret = self.__push_tcp_record1(record, controller, red_recv_q, rawmax);
		ret.or_else(|err|if err.is_promoted_error() || rawmax.is_some() {
			//Abort immediately. This is used for remote alerts and in raw mode.
			self.internal.set_error(true, err.clone());
			Err(err)
		} else {
			//Can not be in raw mode, as raw mode errors are promoted immediately.
			self.do_local_abort_noraw(err);
			Ok(())
		})
	}
	fn __push_tcp_record1(&mut self, record: &[u8], controller: &mut impl UpcallInterface,
		red_recv_q: &mut Vec<u8>, rawmax: Option<u32>) -> Result<(), Error>
	{
		if let Some(maxhs) = rawmax {
			//Eh, how is this even possible?
			fail_if!(record.len() < 4, assert_failure!("__push_tcp_record1: Message too small"));
			let rlen = record[1] as u32 * 65536 + record[2] as u32 * 256 + record[3] as u32;
			//The only known case where rlen does not match record length is when record is too big.
			fail_if!(rlen as usize + 4 != record.len(), error!(hs_msg_buffer
				crate::utils::HsBufferError::HandshakeMsgTooLong(rlen, maxhs)));
			//Push record. Any errors are triggered immediately. The type is always HANDSHAKE, since
			//this is raw handshake mode.
			let mut receiver = self.make_session_receiver();
			self.flags.clear_RX_HS_REQUEST();
			self.handle_decrypted_handshake(record, controller, &mut receiver)?;
			self.process_session_receiver(receiver)?;
			Ok(())
		} else {
			//Eh, how is this even possible?
			fail_if!(record.len() < 5, assert_failure!("__push_tcp_record1: Record too small"));
			let rlen = record[3] as usize * 256 + record[4] as usize;
			//The only known case where rlen does not match record length is when record is too big.
			fail_if!(rlen + 5 != record.len(), error!(rx_record_too_big rlen));
			//Prepare buffer that is the size of encrypted record, so one can decrypt to it. Note that
			//reserve never decreases capacity. The buffer may not be initialized, so use MaybeUninit.
			red_recv_q.reserve(rlen);
			let ptr = red_recv_q.len();
			let excess = red_recv_q.capacity() - ptr;
			let red_buf = unsafe {
				let ptr = red_recv_q.as_mut_ptr().add(ptr).cast::<MaybeUninit<u8>>();
				from_raw_parts_mut(ptr, excess)
			};
			let r = self.__push_tcp_record_to_buffer(record, red_buf, controller);
			if let Some(r) = r? {
				//Consider the appdata just written into buffer to be part of it.
				let newsize = min(red_recv_q.len() + r, red_recv_q.capacity());
				unsafe { red_recv_q.set_len(newsize); }
			}
			Ok(())
		}
	}
	fn __push_tcp_record_to_buffer(&mut self, record: &[u8], red_buf: &mut [MaybeUninit<u8>],
		controller: &mut impl UpcallInterface) -> Result<Option<usize>, Error>
	{
		let out = self.internal.deprotect_btb(record, red_buf).set_err(error!(deprotection_failed))?;
		//The range shall be valid.
		let red_buf = out.range;
		//Print alerts for debugging.
		if out.flags & DPO_ALERT != 0 {
			debug!(HANDSHAKE_DATA self.debug, "Alert protocol: Received message:\n{msg}",
				msg=DumpHexblock::new(red_buf, "  ", "Payload"));
		}
		//Bad application data.
		fail_if!(out.flags & DPO_INVAL_DATA_L != 0, error!(appdata_after_eof));
		fail_if!(out.flags & DPO_INVAL_DATA_E != 0, error!(appdata_before_finished));
		if out.flags & DPO_IGNORE != 0 {
			//Ignored. Used for TLS 1.3 compatibility ChangeCipherSpec.
			debug!(HANDSHAKE_MSGS self.debug, "ChangeCipherSpec protocol: Record ignored");
		} else if out.flags & DPO_CLOSE != 0 {
			//Close alerts.
			debug!(ABORTS self.debug, "Alert protocol: Received end of stream from peer");
			if let Some(recv) = self.internal_receiver.as_mut() {
				let mut reply = InternalReceiverReplyData::new();
				recv.receive_end(&mut reply);
				self.process_internal_reply_noraw(reply);
			} else {
				return Ok(None);	//Special for EOF.
			}
		} else if out.flags & DPO_FATAL != 0 {
			//Fatal alerts.
			let code = Alert::new(out.code);
			debug!(ABORTS self.debug, "Alert protocol: Received alert#{num}: {code}", num=out.code);
			//The error should be available. This encompasses any extended alert, if one was sent.
			if let Some(Some(err)) = self.internal.aborted_by() { fail!(err); }
			//As fallback, use normal alert receive error.
			fail!(error!(received_alert code));
		} else if out.flags & DPO_DATA_IGNORE != 0 {
			//Ignore. Used for messages post forced EOF.
		} else if out.flags & DPO_DATA != 0 {
			if let Some(recv) = self.internal_receiver.as_mut() {
				if red_buf.len() > 0 {
					let mut reply = InternalReceiverReplyData::new();
					recv.receive(red_buf, &mut reply);
					self.process_internal_reply_noraw(reply);
				}
			} else {
				return Ok(Some(red_buf.len()));
			}
		} else {
			let content = ContentType::new(out.code);
			//Other: Forward as control message. The deprotect range is always valid.
			self.handle_decrypted_record_control(content, red_buf, controller)?;
		}
		Ok(Some(0))
	}
}

fn __record_length(data0: &[u8], raw: bool) -> usize
{
	if raw {
		let mut record_len = if data0.len() >= 4 {
			data0[1] as usize * 65536 + data0[2] as usize * 256 + data0[3] as usize + 4
		} else {
			4
		};
		//Do not hang on oversized messages. Pass those to __push_tcp_record() 
		if record_len > 131076 { record_len = 4; }
		record_len
	} else {
		let mut record_len = if data0.len() >= 5 {
			data0[3] as usize * 256 + data0[4] as usize + 5
		} else {
			5
		};
		//Do not hang on oversized records. Pass those to __push_tcp_record() 
		if record_len > 16645 { record_len = 5; }
		record_len
	}
}

pub(crate) type WaitDone = Box<dyn crate::transport::WaitBlockCallback+Send+'static>;

pub(crate) struct Session<H:UpcallInterface>
{
	base: SessionBase,
	hs: H,
	wait_done: Option<WaitDone>,
}

macro_rules! status_flags_fn
{
	($q:ident $s:ident) => {
		(
			SessionBase::$q as for<'r> fn(&'r SessionBase) -> bool,
			StatusFlags::$s as for<'r> fn(&'r mut StatusFlags)
		)
	}
}

impl<H:UpcallInterface> Session<H>
{
	pub(crate) fn new(rawhs: bool, hs: H, debug: DebugSetting) -> Session<H>
	{
		Session {
			base: SessionBase::new(rawhs, debug),
			hs: hs,
			wait_done: None,
		}
	}
	pub(crate) fn controller<'a>(&'a self) -> &'a H { &self.hs }
	pub(crate) fn controller_mut<'a>(&'a mut self) -> &'a mut H { &mut self.hs }
	pub(crate) fn do_tx_request(&mut self) { self.base.do_tx_request(); }
	pub(crate) fn request_rx_handshake(&mut self) { self.base.request_rx_handshake(); }
	pub(crate) fn take_wait_done(&mut self) -> Option<WaitDone> { self.wait_done.take() }
	pub(crate) fn set_cid(&mut self, cid: u64) { self.base.set_cid(cid, &mut self.hs); }
	pub(crate) fn raise_alert(&mut self, code: Alert) { self.base.raise_alert(code) }
	pub(crate) fn get_error(&self) -> Option<Error> { self.base.get_error() }
	pub(crate) fn extractor(&self, label: &str, context: Option<&[u8]>, buffer: &mut [u8]) -> Result<(), Error>
	{
		self.base.extractor(label, context, buffer)
	}
	pub(crate) fn set_mss(&mut self, mss: usize, padmax: bool)
	{
		self.base.internal.set_mss(mss, padmax);
	}
	pub(crate) fn register_on_unblock(&mut self, cb: WaitDone) -> bool
	{
		//If not in blocked state, just ignore the callback and return false to indicate
		//that the connection is already ready.
		if !self.hs.is_blocked() { return false; }
		//Register the callback, and signal it will be called. It is not possible for race
		//condition to have changed the waiting status, as we are holding connection lock,
		//and changing waiting status needs to take the connection lock. So another thread
		//would be blocked on taking the connection lock, and after we return, get it,
		//change the status and correctly call the newly set callback.
		self.wait_done = Some(cb);
		true
	}
	//SAFETY: First <maxsize> bytes of <buffer> (concatenated) SHALL be initialized (if maxsize is None, all of
	//it SHALL be initialized).
	pub(crate) unsafe fn push_tcp_data(&mut self, data: &[&[MaybeUninit<u8>]], maxamt: Option<usize>) ->
		Result<(Vec<u8>, bool), Error>
	{
		unsafe{self.base.push_tcp_data(data, maxamt, &mut self.hs)}
	}
	pub(crate) fn push_tcp_data_end(&mut self) -> Result<(), Error>
	{
		//If abort has completed, return error.
		if let Some(Some(err)) = self.base.internal.aborted_by() { fail!(err); }
		//If not secure EOF, abort.
		if !self.base.internal.is_receive_eof() {
			let err = if self.base.internal.is_handshake_received() {
				error!(insecure_truncation)
			} else {
				error!(handshake_truncation)
			};
			self.base.internal.set_error(true, err.clone());
			fail!(err)
		}
		//Ok to end here.
		Ok(())
	}
	pub(crate) fn return_buffer_push(&mut self, buf: Vec<u8>)
	{
		//Only overwrite if returned capacity is greater.
		let cap = self.base.red_recv_q.as_ref().map(|x|x.capacity()).unwrap_or(0);
		if buf.capacity() > cap { self.base.red_recv_q = Some(buf); }
	}
	pub(crate) fn pull_tcp_data(&mut self, data: PullTcpBuffer) -> Result<Vec<u8>, Error>
	{
		self.base.pull_tcp_data(&mut self.hs, data)
	}
	pub(crate) fn return_buffer_pull(&mut self, buf: Vec<u8>)
	{
		self.base.waiting_buffer = Some(buf);
	}
	pub(crate) fn get_status(&self) -> crate::transport::StatusFlags
	{
		let flag_funcs = [
			status_flags_fn!(is_want_rx set_want_rx),
			status_flags_fn!(is_want_tx set_want_tx),
			status_flags_fn!(is_may_rx set_may_rx),
			status_flags_fn!(is_may_tx set_may_tx),
			status_flags_fn!(is_rx_end set_rx_end),
			status_flags_fn!(is_tx_end set_tx_end),
			status_flags_fn!(is_handshaking set_handshaking),
			status_flags_fn!(is_dead set_dead),
			status_flags_fn!(is_aborted set_aborted),
		];
		let base = &self.base;
		let mut flags = crate::transport::StatusFlags::new();
		for (q, s) in flag_funcs.iter() { if q(base) { s(&mut flags); } }
		//Blocked is special: It is queried from handshake controller, not record controller, as
		//handshake controller executes the blocks.
		if self.hs.is_blocked() { flags.set_blocked(); }
		//These are not flags..
		flags.set_plaintext_mss(base.internal.get_max_fragment_length());
		flags
	}
	pub(crate) fn do_tls_tx_rx_cycle(&mut self, wakeup: &<H as UpcallInterface>::Waker) -> Result<(),Error>
	{
		//Do TX cycles if needed.
		while self.hs.wants_tx() {
			match self.hs.queue_hs_tx(&mut self.base, wakeup.clone()) {
				Ok(true) => self.base.finish_tx_handshake(),
				Ok(false) => (),
				Err(e) => self.base.abort_handshake(e),
			}
			//In raw handshake mode, queue_hs_tx may have failed. In this case, we need
			//to generate a prompt error.
			self.base.rawhs_promote_error()?;
		}
		//Re-arm the RX if needed. Note that previous TX cycle can have altered the
		//condition.
		if self.hs.wants_rx() { self.base.request_rx_handshake(); }
		Ok(())
	}
}
