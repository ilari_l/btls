use super::MAX_TLS_RECORD_PLAIN;
use super::MIN_TLS_RECORD_PLAIN;
use crate::errors::Error;
use crate::stdlib::max;
use crate::stdlib::min;
use crate::stdlib::replace;
use crate::stdlib::Vec;
use btls_aux_assert::AssertFailed;
use btls_aux_bitflags::Bitflags;
use btls_aux_memory::BackedVector;
use btls_aux_tls_iana::Alert;
use btls_aux_tls_iana::ContentType;
use btls_aux_tlskeyschedule::BoxedDeprotector;
use btls_aux_tlskeyschedule::BoxedProtector;
use core::mem::MaybeUninit;
use core::slice::from_raw_parts_mut;


//This limit is due to AES. It would be trillions of times bigger for Chacha20.
const MAX_PROTECTOR_USES: u64 = 2000000;
const MAX_PROTECTOR_USES_FIRST: u64 = 1000;



pub const DPO_CLOSE: u32 = 1;		//Message is close alert.
pub const DPO_FATAL: u32 = 2;		//Message is fatal alert.
pub const DPO_DATA: u32 = 4;		//Message is application data.
pub const DPO_INVAL_DATA_L: u32 = 8;	//Message would be application data, but received after EOF.
pub const DPO_INVAL_DATA_E: u32 = 16;	//Message would be application data, but received before finished.
pub const DPO_ALERT: u32 = 32;		//Message is alert.
pub const DPO_IGNORE: u32 = 64;		//Message is ignored.
pub const DPO_DATA_IGNORE: u32 = 128;	//Ignore contents of the data.

#[derive(Clone,Debug)]
pub struct DeprotectOutput<'a>
{
	pub flags: u32,			//DPO_*
	pub range: &'a [u8],		//Message range.
	pub code: u8,			//Alert code if DPO_FATAL is set.
					//Record type if flags=0.
}

#[derive(Copy,Clone,Debug,Bitflags)]
//Set when the record layer has set nontrivial protector.
#[bitflag(NON_TRIVIAL)]
//Protector has already been changed at least once.
#[bitflag(PROTECTOR_CHANGED_ONCE)]
//The protector is in progress of being changed.
#[bitflag(CHANGING_PROTECTOR)]
//Connection aborted.
#[bitflag(ABORTED)]
//Connection abort completed.
#[bitflag(ABORT_COMPLETED)]
//TX EOF has been sent.
#[bitflag(TX_EOF_SENT)]
//RX EOF has been received.
#[bitflag(RX_EOF_RECVD)]
//Close notify has been done.
#[bitflag(CLOSE_NOTIFY_DONE)]
//Next message might be unprotected alert.
#[bitflag(NEXT_UNPROTECTED_ALERT)]
//All of handshake has been transmitted.
#[bitflag(TX_HS_FINISHED)]
//All of handshake has been received.
#[bitflag(RX_HS_FINISHED)]
//Fake CCS has been sent.
#[bitflag(FAKE_CCS_SENT)]
//Alerts are always fatal.
#[bitflag(ALERTS_ALWAYS_FATAL)]
//RX EOF has been forced.
#[bitflag(RX_EOF_FORCED)]
//Raw handshake mode.
#[bitflag(RAW_HANDSHAKE_MODE)]
//Protector set.
#[bitflag(PROTECTOR_SET)]
//Pad to maximum.
#[bitflag(PAD_MAXIMUM)]
pub struct Flags(u8, u8, u8, u8);



//Session internal context.
pub struct SessionInternalContext
{
	//Flags.
	flags: Flags,
	//Maximum write size in one go.
	max_fragment_length: u16,
	//MSS setting.
	mss: u16,
	//The maximum plaintext and ciphertext sizes, considering both max_fragment_length and MSS.
	mss_pt: u16,
	mss_ct: u16,
	//TLS abort to be sent. This gets cleared after the alert has been queued.
	abort_code: Option<Alert>,
	//Times ChangeCipherSpec has been received.
	ccs_count: u8,
	//Protector use counter.
	prot_use_count: u64,
	//Error message. normally None. Note that error messages are gated behind AbortCompleted.
	error: Option<Error>,
	//Countdown to outgoing EOF. This is normally None. However, when alert is queued, it is updated to point to
	//the end of the alert. When this hits zero, either AbortCompleted or OutgoingEof gets set (depending on
	//if local_tls_abort is set).
	bytes_to_outgoing_eof: Option<usize>,
	//Protector.
	protector: BoxedProtector,
	//Deprotector.
	deprotector: BoxedDeprotector,
}

impl SessionInternalContext
{
	fn _use_protector(&mut self)
	{
		//This is about tracking uses, so can be saturating add.
		self.prot_use_count = self.prot_use_count.saturating_add(1);
		if self.prot_use_count >= MAX_PROTECTOR_USES_FIRST && !self.flags.is_PROTECTOR_CHANGED_ONCE() {
			self.flags.set_CHANGING_PROTECTOR();
			self.flags.set_PROTECTOR_CHANGED_ONCE();
		}
		if self.prot_use_count >= MAX_PROTECTOR_USES { self.flags.set_CHANGING_PROTECTOR(); }
	}
	fn _do_remote_fatal_alert<'a>(&mut self, code: Alert, range: &'a [u8]) -> DeprotectOutput<'a>
	{
		self.flags.set_ABORTED();
		self.flags.set_ABORT_COMPLETED();	//Remote aborts are instant.
		self.error = Some(error!(received_alert code));
		DeprotectOutput{
			flags: DPO_FATAL | DPO_ALERT,
			range: range,
			code: code.get(),
		}
	}
	fn update_mss(&mut self)
	{
		//Note that mss_ct is not directly the smaller of mss and amount of cipertext MFL would produce.
		//It is amount of ciphertext the fragment of maximum plaintext size produces. The plaintext size
		//is capped by MFL and amount of plaintext that fits into MSS. The MSS is truncated to amount of
		//space neded to hold MIN/MAX fragment lengths.
		let mut mss = self.mss as usize;
		mss = max(mss, self.protector.get_ciphertext_size(MIN_TLS_RECORD_PLAIN));
		mss = min(mss, self.protector.get_ciphertext_size(MAX_TLS_RECORD_PLAIN));
		let max_mss_pt = self.protector.get_max_plaintext_size(mss) as u16;
		self.mss_pt = min(max_mss_pt, self.max_fragment_length);
		self.mss_ct = self.protector.get_ciphertext_size(self.mss_pt as usize) as u16;
	}
	pub fn new(rawmode: bool) -> SessionInternalContext
	{
		SessionInternalContext{
			flags: Flags::RAW_HANDSHAKE_MODE.and(rawmode),
			prot_use_count: 0,
			ccs_count: 0,
			max_fragment_length: MAX_TLS_RECORD_PLAIN as u16,
			mss: 65535,
			mss_ct: 0,	//Will be initialized later.
			mss_pt: 0,	//Will be initialized later.
			abort_code: None,
			error: None,
			bytes_to_outgoing_eof: None,
			protector: BoxedProtector::new_null(),
			deprotector: BoxedDeprotector::new_null(),
		}
	}
	//Set the protector used.
	pub fn set_protector(&mut self, x: BoxedProtector)
	{
		self.flags.set_NON_TRIVIAL();
		self.flags.set_PROTECTOR_SET();
		self.prot_use_count = 0;
		self.flags.clear_CHANGING_PROTECTOR();
		self.protector = x;
		self.update_mss();
	}
	//Set the deprotector used.
	pub fn set_deprotector(&mut self, x: BoxedDeprotector)
	{
		self.flags.set_NON_TRIVIAL();
		self.deprotector = x;
	}
	//Clear pending protector change.
	pub fn clear_pending_change(&mut self) { self.flags.clear_CHANGING_PROTECTOR(); }
	//Finish handshake reception.
	pub fn notify_handshake_received(&mut self) { self.flags.set_RX_HS_FINISHED(); }
	//Finish handshake transmssion.
	pub fn notify_handshake_transmitted(&mut self) { self.flags.set_TX_HS_FINISHED(); }
	//Set the "next record might be unprotected alert" flag.
	pub fn set_maybe_next_unprotected_alert(&mut self) { self.flags.set_NEXT_UNPROTECTED_ALERT(); }
	//Use TLS 1.2 record version in unprotected packets.
	pub fn set_use_tls12_records(&mut self)
	{
		if self.flags.is_NON_TRIVIAL() { return; }
		self.flags.set_NON_TRIVIAL();
		self.protector.set_modern_version();
	}
	///Set MSS.
	pub fn set_mss(&mut self, mut mss: usize, padmax: bool)
	{
		//mss=0 means maximum.
		if mss == 0 { mss = 65535; }
		self.mss = min(mss, 65535) as u16;
		self.flags.force_PAD_MAXIMUM(padmax);
		self.update_mss();
	}
	//Set maximum fragment length.
	pub fn set_max_fragment_length(&mut self, maxlen: usize)
	{
		//Floor at 64 bytes, cap at 16384.
		self.max_fragment_length = min(max(maxlen, MIN_TLS_RECORD_PLAIN), MAX_TLS_RECORD_PLAIN) as u16;
		self.update_mss();
	}
	//Set the abort code. Returns True if double abort.
	pub fn set_abort_code(&mut self, fault: Error) -> bool
	{
		let code = fault.tls_alert_num2();
		let doublefault = if let Some(ref mut x) = self.abort_code {
			//Overwrite error close, but not other alerts.
			if x.is_close() {
				*x = code;
				false
			} else {
				true
			}
		} else {
			self.abort_code = Some(code);
			false
		};
		if !code.is_close() || doublefault {
			//Mark this as fault.
			self.flags.set_ABORTED();
			self.error = Some(fault);
		}
		if doublefault { self.flags.set_ABORT_COMPLETED(); }	//Complete double aborts intstantly.
		doublefault
	}
	//Set EOF status. Does nothing if there is already abort in progress.
	pub fn set_eof_status(&mut self)
	{
		if self.abort_code.is_none() {
			self.abort_code = Some(Alert::CLOSE_NOTIFY);
			self.flags.set_CLOSE_NOTIFY_DONE();
		}
	}
	//Clear close_notify_done.
	pub fn clear_close_notify_done(&mut self) { self.flags.clear_CLOSE_NOTIFY_DONE(); }
	//Check if there is pending abort and ACK it.
	pub fn ack_pending_abort(&mut self) -> Option<Alert> { replace(&mut self.abort_code, None) }
	//Set the connection error field, and abort connection (completing instantly if abort_now=true).
	pub fn set_error(&mut self, abort_now: bool, error: Error)
	{
		self.flags.set_ABORTED();
		if abort_now { self.flags.set_ABORT_COMPLETED(); }
		self.error = Some(error);
	}
	//Set value in Countdown timer. When this timer expires, either AbortCompleted or TxEofSent gets set,
	//depending on if there is pending abort or not.
	pub fn set_countdown_timer(&mut self, amount: usize) { self.bytes_to_outgoing_eof = Some(amount); }
	//Run the countdown timer by specified number of ticks. Returns true if alert was sent.
	pub fn run_countdown(&mut self, len: usize) -> bool
	{
		let mut ret = false;
		let mut clear = false;
		if let Some(ref mut remaining) = self.bytes_to_outgoing_eof {
			*remaining -= min(*remaining, len);
			if *remaining == 0 {
				clear = true;
				if self.flags.is_ABORTED() {
					ret = !self.flags.is_ABORT_COMPLETED();
					self.flags.set_ABORT_COMPLETED();
				} else {
					self.flags.set_TX_EOF_SENT();
				}
			}
		}
		if clear { self.bytes_to_outgoing_eof = None; }
		ret
	}

	//Has handshake been received?
	pub fn is_handshake_received(&self) -> bool { self.flags.is_RX_HS_FINISHED() }
	//Has handshake been transmitted?
	pub fn is_handshake_transmitted(&self) -> bool { self.flags.is_TX_HS_FINISHED() }
	//Has EOF alert been generated?
	pub fn is_eof_sent(&self) -> bool { self.flags.is_TX_EOF_SENT() }
	//Has close_notify been generated?
	pub fn is_close_notify_done(&self) -> bool { self.flags.is_CLOSE_NOTIFY_DONE() }
	//Has the connection been aborted?
	pub fn is_aborted(&self) -> bool { self.flags.is_ABORTED() }
	//Has the connection abort finished?
	pub fn is_abort_complete(&self) -> bool { self.flags.is_ABORT_COMPLETED() }
	//Is there pending protector change?
	pub fn is_pending_change(&self) -> bool { self.flags.is_CHANGING_PROTECTOR() }
	//Has EOF been received?
	pub fn is_receive_eof(&self) -> bool { self.flags.is_RX_EOF_RECVD() }
	//Is there abort without generated alert record?
	pub fn is_pending_abort(&self) -> bool { self.abort_code.is_some() }
	//Is raw handshake mode.
	pub fn is_raw_handshake(&self) -> bool { self.flags.is_RAW_HANDSHAKE_MODE() }
	//Is there been protector change?
	pub fn is_encrypted(&self) -> bool { self.flags.is_PROTECTOR_SET() }
	//Pad to maximum active?
	pub fn pad_to_maximum_active(&self) -> bool
	{
		//No point using this if padding is not possible.
		self.flags.is_PAD_MAXIMUM() && self.protector.supports_padding()
	}
	//Get the reason connection aborted. Some(None) means bidirectional EOF.
	pub fn aborted_by(&self) -> Option<Option<Error>>
	{
		if self.flags.is_RX_EOF_RECVD() && self.flags.is_TX_EOF_SENT() { return Some(None); }
		fail_if_none!(!self.flags.is_ABORT_COMPLETED());	//Delay reporting until abort is complete.
		//None maps to None, Some(x) maps to Some(Some(x)).
		self.error.clone().map(|x|Some(x))
	}
	//Force RX abort.
	pub fn force_rx_eof(&mut self) { self.flags.set_RX_EOF_FORCED(); }
	//Get maximum fragment length.
	//The return value is always between MIN_TLS_RECORD_PLAIN and MAX_TLS_RECORD_PLAIN, inclusive.
	pub fn get_max_fragment_length(&self) -> usize
	{
		//Use mss_pt, as that also takes into account the MSS setting. This gives whacked out values until
		//protector is initialized, but nobody should be using this before that.
		self.mss_pt as usize
	}
	fn deprotect_tail<'a>(&mut self, rtype: ContentType, range: &'a [u8]) -> DeprotectOutput<'a>
	{
		if rtype == ContentType::ALERT {
			//If length is invalid, interpret as fatal code 50 alert.
			let (badlen, level, code) = if range.len() >= 2 {
				(false, range[0], Alert::new(range[1]))
			} else {
				(true, 2, Alert::INTERNAL_ERROR)
			};
			//Level is meaningless in TLS 1.3, and unusable in TLS 1.2. Do meaningless check in order
			//to avoid warning.
			if badlen || !code.is_close() || level < 1 || level > 2 {
				//Fatal alert.
				self._do_remote_fatal_alert(code, range)
			} else {
				//Close notify.
				self.flags.set_RX_EOF_RECVD();
				DeprotectOutput{
					flags: DPO_CLOSE|DPO_ALERT,
					range: range,
					code: 0,
				}
			}
		} else if rtype == ContentType::APPLICATION_DATA {
			if !self.flags.is_RX_HS_FINISHED() {
				DeprotectOutput{
					flags: DPO_DATA|DPO_INVAL_DATA_E,
					range: &[],
					code: 0,
				}
			} else if self.flags.is_RX_EOF_RECVD() {
				DeprotectOutput{
					flags: DPO_DATA|DPO_INVAL_DATA_L,
					range: &[],
					code: 0,
				}
			} else if self.flags.is_RX_EOF_FORCED() {
				DeprotectOutput{
					flags: DPO_DATA | DPO_DATA_IGNORE,
					range: &[],
					code: 0,
				}
			} else {
				DeprotectOutput{
					flags: DPO_DATA,
					range: range,
					code: 0,
				}
			}
		} else if rtype == ContentType::new(0) {
			//Ignore this record.
			DeprotectOutput{
				flags: DPO_IGNORE,
				range: &[],
				code: 0,
			}
		} else {
			DeprotectOutput{
				flags: 0,
				range: range,
				code: rtype.get(),
			}
		}
	}
	//Check if data can be sent.
	pub(super) fn can_send_appdata(&self, past: bool) -> Result<bool, ()>
	{
		//Protection is not possible in raw handshake mode.
		fail_if!(self.flags.is_RAW_HANDSHAKE_MODE(), ());
		//HACK: If we are TLS 1.2 client, we need to check that both RxHsFinished and TxHsFinished is
		//set before sending data in order to avoid sending data to unauthenticated attacker. However, that
		//check is harmful for TLS 1.3 server, because 0.5-RTT (which is safe due do subtle reasons) runs
		//with TxHsFinished&&!RxHsFinished. The remaining two cases are do not care.
		//
		//Now, TLS 1.3 uses AlertsAlwaysFatal flag. On client side, it is set late, but on server side,
		//already in ServerHello processing. It is not set at all for TLS 1.2. Thus, it can be used for
		//detecting if omitting RxHsFinished check is ok.
		//
		//In addition to this, aborted flag must be clear for sending application data to be allowed.
		//In case asking for past state, use AbortCompleted instead of Abort.
		let abort_flag = if past { self.flags.is_ABORT_COMPLETED() } else { self.flags.is_ABORTED() };
		Ok(self.flags.is_TX_HS_FINISHED() && !abort_flag &&
			(self.flags.is_RX_HS_FINISHED() || self.flags.is_ALERTS_ALWAYS_FATAL()))
	}
	//Deprotect a record.
	pub fn deprotect_btb<'a>(&mut self, buf: &[u8], target: &'a mut [MaybeUninit<u8>]) ->
		Result<DeprotectOutput<'a>, ()>
	{
		//Deprotection is not possible in raw handshake mode.
		fail_if!(self.flags.is_RAW_HANDSHAKE_MODE(), ());
		//Don't read after abort has completed. This is especially due to remote aborts.
		fail_if!(self.flags.is_ABORT_COMPLETED(), ());
		//Limit number of ChangeCipherSpec messages.
		fail_if!(buf.get(0) == Some(&ContentType::CHANGE_CIPHER_SPEC.get()) &&
			!self.count_ccs_limited(), ());
		//This is the unprotected alert special case: Even if encryption is enabled, alerts may be
		//unencrypted.
		if self.flags.is_NEXT_UNPROTECTED_ALERT() && buf.len() >= 5 &&
			buf.get(0) == Some(&ContentType::ALERT.get()) {
			//Need to copy message to output buffer in order to return range for it.
			let mut target = BackedVector::new(target);
			target.extend_fitting(&buf[5..]);
			let range = target.into_inner();
			//Unprotected alert. The error code is at offset 6, if it exists. These are always
			//treated as fatal.
			let code = buf.get(6).map(|x|Alert::new(*x)).unwrap_or(Alert::INTERNAL_ERROR);
			return Ok(self._do_remote_fatal_alert(code, range));
		}
		self.flags.clear_NEXT_UNPROTECTED_ALERT();	//This is oneshot flag.
		//Check that record type is in valid range. Currently types 20-24 are defined. Type 24 can not happen
		//because we do not support Heartbeat.
		fail_if!(*buf.get(0).unwrap_or(&0)/4 != 5, ());
		//Try normal decrpytion. This also handles the case where encryption is not enabled yet.
		let (rtype, range) = self.deprotector.deprotect_btb2(buf, target)?;
		Ok(self.deprotect_tail(rtype, range))
	}
	pub fn encrypted_size(&self, x: usize) -> usize { self.protector.get_ciphertext_size(x) }
	//Protect a record.
	pub fn protect(&mut self, output: &mut Vec<u8>, rtype: ContentType, input: &[u8], mut extra: usize) ->
		Result<(), AssertFailed>
	{
		//Don't pad if not supported.
		if !self.protector.supports_padding() { extra = 0; }
		//Protection is not possible in raw handshake mode.
		fail_if!(self.flags.is_RAW_HANDSHAKE_MODE(), assert_failure!("Raw handshake mode"));
		//Don't write after abort has completed. This is especially due to remote aborts.
		fail_if!(self.flags.is_ABORT_COMPLETED(), assert_failure!("Abort already completed"));

		self._use_protector();
		//extra bytes for padding.
		let need_size = self.protector.get_ciphertext_size(input.len() + extra);
		let olen = output.len();
		output.reserve(need_size);
		let oslice: &mut [MaybeUninit<u8>] = unsafe{
			let reserved = output.capacity() - output.len();
			let optr = output.as_mut_ptr().add(olen).cast::<MaybeUninit<u8>>();
			from_raw_parts_mut(optr, reserved)
		};
		//If requesting padding, pad to full output size.
		let len = assert_some!(self.protector.protect_btb2(rtype, input, oslice, extra > 0),
			"Failed to protect record").len();
		//Consider the data written to be part of buffer.
		unsafe{output.set_len(olen + len)};
		Ok(())
	}
	//Flag fake CCS as sent.
	pub fn send_fake_ccs(&mut self) -> bool
	{
		let ret = self.flags.is_FAKE_CCS_SENT();
		self.flags.set_FAKE_CCS_SENT();
		!ret
	}
	//Set the "alerts always fatal" flag.
	pub fn set_alerts_always_fatal(&mut self) { self.flags.set_ALERTS_ALWAYS_FATAL(); }
	//Get the "alerts always fatal" flag.
	pub fn get_alerts_always_fatal(&self) -> bool { self.flags.is_ALERTS_ALWAYS_FATAL() }
	//Count a ChangeCipherSpec. Returns false if overflow.
	fn count_ccs_limited(&mut self) -> bool
	{
		match self.ccs_count.checked_add(1) {
			Some(x) => { self.ccs_count = x; true }
			None => false
		}
	}
}
