//#![allow(dead_code)]
use crate::errors::Error;
use crate::stdlib::Vec;
use core::mem::MaybeUninit;
use core::slice::from_raw_parts;

macro_rules! ack_data
{
	($selfx:expr, $buf:expr, $maxamt:expr) => {{
		$selfx.maxsize -= $buf.len();
		$selfx.sptr += $buf.len();
		if $selfx.sptr >= $maxamt {
			//Exactly Overflowed to next fragment.
			$selfx.sptr = 0;
			$selfx.ptr += 1;
		}
	}};
}

pub(super) enum ReadResult<'a>
{
	Direct(&'a [u8]),
	Indirect,
	None,
}

pub(super) struct ReadState<'a>
{
	buffer: &'a [&'a [MaybeUninit<u8>]],
	ptr: usize,
	sptr: usize,
	infinite: bool,
	maxsize: usize,
	raw: Option<u32>,
}

impl<'a> ReadState<'a>
{
	#[cfg(test)]
	fn new_test(buffer: &'a [&'a [u8]], maxsize: usize, raw: Option<u32>) -> ReadState<'a>
	{
		use crate::utils::to_mu8_slice;
		ReadState {
			buffer: to_mu8_slice(buffer),
			ptr: 0,
			sptr: 0,
			infinite: false,
			maxsize: maxsize,
			raw: raw,
		}
	}
	//SAFETY: First <maxsize> bytes of <buffer> (concatenated) SHALL be initialized.
	pub(super) unsafe fn new(buffer: &'a [&'a [MaybeUninit<u8>]], maxsize: Option<usize>, raw: Option<u32>) ->
		ReadState<'a>
	{
		ReadState {
			buffer: buffer,
			ptr: 0,
			sptr: 0,
			infinite: maxsize.is_none(),
			maxsize: maxsize.unwrap_or(!0),
			raw: raw,
		}
	}
	pub(super) fn get(&mut self, indirect: &mut Vec<u8>) -> Result<ReadResult<'a>, Error>
	{
		//Need to continue if maxsize is positive or None (which means infinity).
		while self.ptr < self.buffer.len() && (self.infinite || self.maxsize > 0) {
			let data = self.buffer[self.ptr];
			let datalen = data.len();
			//Remove the already processed sub-buffer and cap to maxsize.
			let mut data = data.get(self.sptr..).unwrap_or(&[]);
			if !self.infinite && self.maxsize < data.len() {
				data = &data[..self.maxsize];
			};
			//The data shall be initialized by the invariant.
			let data = unsafe{from_raw_parts(data.as_ptr().cast::<u8>(), data.len())};
			if indirect.len() > 0 {
				//Need to complete indirect.
				let size_limit = if self.raw.is_some() { 4 } else { 5 };
				let oldsize = indirect.len();
				let size = read_packet_size(&indirect, self.raw);
				let missing = size.saturating_sub(indirect.len());
				//missing has to be positive because otherwise that would mean that last iteration
				//should have completed the read, but it did not.
				sanity_check!(missing > 0, "ReadState::get(): missing = 0");
				if let Some(mdata) = data.get(..missing) {
					//The record completes. Push it to indirect buffer, and update pointers.
					//maxsize changes as data has been processed. Note that
					//maxsize >= data.len() >= missing.
					indirect.extend_from_slice(mdata);
					ack_data!(self, mdata, datalen);
					//If indirect buffer was too small to determine the size, do not return
					//complete buffer, as size can be unreliable (and most likely is wrong).
					if oldsize >= size_limit { return Ok(ReadResult::Indirect); }
				} else {
					//The record does not complete.
					indirect.extend_from_slice(data);
					ack_data!(self, data, datalen);
				}
			} else {
				//Need to grab next packet.
				let size = read_packet_size(&data, self.raw);
				//If buffer is not big enough, save the remainder. Otherwise process the thing.
				//Remember that maxsize >= data.len().
				if let Some(mdata) = data.get(..size) {
					ack_data!(self, mdata, datalen);
					return Ok(ReadResult::Direct(mdata));
				} else {
					indirect.extend_from_slice(data);
					ack_data!(self, data, datalen);
				}
			}
		}
		//Did not manage to complete any records.
		Ok(ReadResult::None)
	}
}

fn read_packet_size(buf: &[u8], raw: Option<u32>) -> usize
{
	if let Some(maxraw) = raw {
		if buf.len() >= 4 {
			let s = buf[1] as u32 * 65536 + buf[2] as u32 * 256 + buf[3] as u32;
			//If packet is too big, return minimum size to avoid buffering it and trigger failure
			//as soon as possible.
			if s > maxraw { return 4; }
			s as usize + 4
		} else {
			4
		}
	} else {
		if buf.len() >= 5 {
			let s = buf[3] as usize * 256 + buf[4] as usize + 5;
			//If packet is too big, return minimum size to avoid buffering it and trigger failure
			//as soon as possible.
			if s > 16645 { return 5; }
			s
		} else {
			5
		}
	}
}

#[cfg(test)]
fn assert_pkt(state: &mut ReadState, vector: &mut Vec<u8>, reference: &[u8], direct: bool)
{
	match state.get(vector).expect("Should have suceeded") {
		ReadResult::Direct(y2) if direct => assert_eq!(y2, reference),
		ReadResult::Indirect if !direct => assert_eq!(&vector[..], reference),
		_ => unreachable!(),
	}
	assert!(!direct || vector.len() == 0);
	vector.clear();
}

#[cfg(test)]
fn assert_end(state: &mut ReadState, vector: &mut Vec<u8>)
{
	match state.get(vector).expect("Should have suceeded") {
		ReadResult::Direct(_) => unreachable!(),
		ReadResult::Indirect => unreachable!(),
		ReadResult::None => (),
	}
	assert!(vector.len() == 0);
}

#[test]
fn threepart_allsplit_noraw()
{
	static Y1: &'static [u8] = b"\x16\x03\x03\0\x0eHello, World!\n";
	static Y2: &'static [u8] = b"\x16\x03\x03\0\x0fHello, World!\nx";
	static Y3: &'static [u8] = b"\x16\x03\x03\0\x10Hello, World!\nxx";
	let mut y = Vec::new();
	let mut z = Vec::new();
	y.extend_from_slice(Y1);
	y.extend_from_slice(Y2);
	y.extend_from_slice(Y3);
	y.extend_from_slice(Y3);	//Extra component to test readv_ret.
	for i in 0..y.len() {
		let (head, tail) = y.split_at(i);
		let yarr: [&[u8];2] = [head, tail];
		let y0c = 0;
		let y1c = y0c + Y1.len();
		let y2c = y1c + Y2.len();
		let y3c = y2c + Y3.len();
		let mut x = ReadState::new_test(&yarr, y3c, None);
		assert_pkt(&mut x, &mut z, Y1, i <= y0c || i >= y1c);
		assert_pkt(&mut x, &mut z, Y2, i <= y1c || i >= y2c);
		assert_pkt(&mut x, &mut z, Y3, i <= y2c || i >= y3c);
		assert_end(&mut x, &mut z);
	}
}

#[test]
fn threepart_allsplit_raw()
{
	static Y1: &'static [u8] = b"\x01\0\0\x0eHello, World!\n";
	static Y2: &'static [u8] = b"\x01\0\0\x0fHello, World!\nx";
	static Y3: &'static [u8] = b"\x01\0\0\x10Hello, World!\nxx";
	let mut y = Vec::new();
	let mut z = Vec::new();
	y.extend_from_slice(Y1);
	y.extend_from_slice(Y2);
	y.extend_from_slice(Y3);
	y.extend_from_slice(Y3);	//Extra component to test readv_ret.
	for i in 0..y.len() {
		let (head, tail) = y.split_at(i);
		let yarr: [&[u8];2] = [head, tail];
		let y0c = 0;
		let y1c = y0c + Y1.len();
		let y2c = y1c + Y2.len();
		let y3c = y2c + Y3.len();
		let mut x = ReadState::new_test(&yarr, y3c, Some(131072));
		assert_pkt(&mut x, &mut z, Y1, i <= y0c || i >= y1c);
		assert_pkt(&mut x, &mut z, Y2, i <= y1c || i >= y2c);
		assert_pkt(&mut x, &mut z, Y3, i <= y2c || i >= y3c);
		assert_end(&mut x, &mut z);
	}
}
