use btls_aux_fail::ResultExt;
use btls_aux_serialization::Source;
use btls_aux_signatures::SignatureTypesEnabled;
use errors::Error;
use messages::BaseRawMessage;
use messages::DigitallySigned;
use messages::DigitallySignedReadError as DSRE;
use messages::certificate::Tls13Certificate;
use messages::certificate::Tls13CertificateP;
use messages::tlsconsts::TLS_LEN_CERT_CONTEXT;
use messages::tlsconsts::TLS_LEN_CERT_ENTRY;
use messages::tlsconsts::TLS_LEN_SIGNATURE;


///Parsed exported authenticator.
pub struct ExportedAuthenticator<'a>
{
	///The parsed certificate.
	pub certificate: Tls13Certificate<'a>,
	///The parsed certificate verify.
	pub signature: DigitallySigned<'a>,
	///The MAC.
	pub mac: &'a [u8],
	///The raw certificate structure.
	pub raw_cert: &'a [u8],
	///The raw signature structure.
	pub raw_sig: &'a [u8],
}

impl<'a> ExportedAuthenticator<'a>
{
	///Parse a TLS Exported Authenticator.
	pub fn parse(b: BaseRawMessage<'a>, enabled_sig: SignatureTypesEnabled) ->
		Result<ExportedAuthenticator<'a>, Error>
	{
		let BaseRawMessage(msg, debugger) = b;
		let mut src = Source::new(msg);
		//The Tls13Certificate parser doesn't like extra data, so figure out how long the certificate part is.
		//Note that we need the headers!
		src.read_source(TLS_LEN_CERT_CONTEXT, error!(cant_parse_exported_authenticator))?;
		src.read_source(TLS_LEN_CERT_ENTRY, error!(cant_parse_exported_authenticator))?;
		let pos1 = src.position();
		src.u16().set_err(error!(cant_parse_exported_authenticator))?;
		src.read_source(TLS_LEN_SIGNATURE, error!(cant_parse_exported_authenticator))?;
		let pos2 = src.position();
		let raw_cert = &msg[..pos1];	//Pos1 must be in-range by being from source.
		let raw_sig = &msg[pos1..pos2];	//Pos2 must be in-range by being from source.
		let mac = &msg[pos2..];
		//Parse the certificate. Exported authenticators are never RPK.
		let basemsg = BaseRawMessage(raw_cert, debugger);
		let cert = Tls13Certificate::parse(basemsg, Tls13CertificateP {
			spki_only_in_cert: false,
			use_spki_only: false
		})?;
		let cv = match DigitallySigned::parse(&mut Source::new(raw_sig), enabled_sig) {
			Ok(x) => x,
			Err(DSRE::CantParse) => fail!(error!(cant_parse_cv)),
			Err(DSRE::BogusAlgorithm(x)) => fail!(error!(bogus_cv_signature_algo x)),
		};
		Ok(ExportedAuthenticator{
			certificate: cert,
			signature: cv,
			mac: mac,
			raw_cert: raw_cert,
			raw_sig: raw_sig,
		})
	}
}
