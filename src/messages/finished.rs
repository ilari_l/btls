use super::HandshakeHash;
use super::common::tx_handshake3;
use crate::errors::Error;
use crate::logging::DebugSetting;
use crate::record::DowncallInterface;
use crate::record::GenerateFinished;
use btls_aux_tls_iana::HandshakeType;
use core::ops::Deref;

//Send finished message.
//
//Parameters:
//
// * secret: The secret to use for computing finished MAC.
// * hs_hash: The running handshake hash.
// * base: The downcall interface to send via.
// * debug: The debug context.
// * state: The name of handshake state to use in debug messages.
//
//Notes:
//
// * Forces a break.
//
//Returns nothing on success, on error an error object.
pub(crate) fn tx_finished(secret: &impl GenerateFinished, hs_hash: &mut HandshakeHash,
	base: &mut impl DowncallInterface, debug: DebugSetting, state: &str) -> Result<(), Error>
{
	let mac = secret.generate_finished(&hs_hash.checkpoint(), ks_debug!(&debug))?;
	tx_handshake3(debug, HandshakeType::FINISHED, mac.deref(), base, hs_hash, state)?;
	base.force_break()?;	//Always break after Finished
	Ok(())
}
