use super::HashMessages;
use super::SessionId;
use super::certificate::sct_extension_3;
use super::certificate::trans_extension_3;
use super::common::do_renego;
use super::common::do_server_alpn;
use super::common::tx_handshake3;
use super::fields::FIELD_EXT_LIST;
use super::fields::FIELD_KEY_SHARE;
use super::fields::FIELD_SESSION_ID;
use crate::common::CryptoTracker;
use crate::errors::Error;
use crate::logging::DebugSetting;
use crate::record::DowncallInterface;
use crate::record::DiffieHellmanSharedSecretResponder;
use crate::stdlib::Deref;
use crate::stdlib::Vec;
use btls_aux_rope3::fragment_trusted_invariant_length;
use btls_aux_rope3::rope3;
use btls_aux_rope3::VectorFieldError;
use btls_aux_tls_iana::HandshakeType;
use btls_aux_tls_iana::TlsVersion as IanaTlsVersion;


pub static HRR_MAGIC: [u8; 32] = [
	0xCF, 0x21, 0xAD, 0x74, 0xE5, 0x9A, 0x61, 0x11, 0xBE, 0x1D, 0x8C, 0x02, 0x1E, 0x65, 0xB8, 0x91,
	0xC2, 0xA2, 0x11, 0x16, 0x7A, 0xBB, 0x8C, 0x5E, 0x07, 0x9E, 0x09, 0xE2, 0xC8, 0xA8, 0x33, 0x9C,
];

pub(crate) struct ServerHelloParameters<'a>
{
	//CT offered.
	pub ct_offered: bool,
	//Transparency Info offered.
	pub trans_offered: bool,
	//Send SPKI only.
	pub send_spki_only: bool,
	//EMS offered.
	pub ems_offered: bool,
	//Reduced record size.
	pub reduced_record_size: bool,
	//Maybe send OCSP.
	pub maybe_send_ocsp: bool,
	//ALPN.
	pub alpn: Option<&'a str>,
	//TLS 1.3.
	pub tls13: bool,
	//Ciphersuite.
	pub cs_id: u16,
	//Server random.
	pub server_random: &'a [u8;32],
	//Session ID.
	pub session_id: SessionId,
	//Version code (TLS 1.3 only).
	pub version_code: IanaTlsVersion,
	//Diffie-Hellman secret.
	pub dh_secret: Option<&'a DiffieHellmanSharedSecretResponder>,
	//Stapled SCTs.
	pub sct_list: &'a [Vec<u8>],
	//Stapled Transparency Items.
	pub trans_list: &'a [Vec<u8>],
}

pub(crate) fn tx_server_hello(p: ServerHelloParameters, debug: DebugSetting, crypto_algs: &mut CryptoTracker,
	base: &mut impl DowncallInterface, hs_hash: &mut impl HashMessages, state: &str) -> Result<(), Error>
{
	let handle_err = |e:VectorFieldError|error!(serialize_failed "encrypted_extensions", e);
	let sct_extension3 = if !p.tls13 && p.ct_offered && p.sct_list.len() > 0 {
		debug!(HANDSHAKE_EVENTS debug, "Server certificate: Sending {scnt} SCTs via extension",
			scnt=p.sct_list.len());
		crypto_algs.ct_validated();
		Some(sct_extension_3(p.sct_list).map_err(handle_err)?)
	} else {
		None	//SCTs are sent along certificate in TLS 1.3.
	};
	let trans_extension3 = if !p.tls13 && p.trans_offered && p.trans_list.len() > 0 {
		debug!(HANDSHAKE_EVENTS debug, "Server certificate: Sending {scnt} Transparency Items via extension",
			scnt=p.trans_list.len());
		crypto_algs.ct_validated();
		Some(trans_extension_3(p.trans_list).map_err(handle_err)?)
	} else {
		None	//Transparency Items are sent along certificate in TLS 1.3.
	};
	let content3 = rope3!(ERROR_MAP handle_err,
		TlsVersion::TLS_1_2,
		{p.server_random},
		VECTOR FIELD_SESSION_ID({p.session_id.deref()}),
		{p.cs_id},			//Ciphersuite
		CompressionMethod::NULL,
		VECTOR FIELD_EXT_LIST(if {p.tls13} CONCATENATE(
			//Real Version.
			EXTENSION SUPPORTED_VERSIONS({p.version_code.get()}),
			//Extension key_share. We always send this in TLS 1.3.
			if[Some(secret) = p.dh_secret] EXTENSION KEY_SHARE(
				{secret.get_kem().tls_id()},
				VECTOR FIELD_KEY_SHARE({fragment_trusted_invariant_length(secret.get_public_key())})
			)
		) else CONCATENATE (
			//The CT extensions are here only for TLS 1.2
			sct_extension3,
			trans_extension3,
			//ALPN, if any.
			{do_server_alpn(p.alpn).map_err(handle_err)?},
			//Server key type. If in TLS 1.2 and using RPK, send this.
			if {p.send_spki_only} EXTENSION SERVER_CERTIFICATE_TYPE(CertificateType::RAW_PUBLIC_KEY),
			//If offered, ACK OCSP on TLS 1.2. Don't do this in SPKI mode.
			if {p.maybe_send_ocsp} EXTENSION STATUS_REQUEST(),
			//Extension EMS. We always send this in TLS 1.2 if offered.
			if {p.ems_offered} EXTENSION EXTENDED_MASTER_SECRET(),
			//Extension renegotiation_info. We always send this in TLS 1.2.
			{do_renego(b"").map_err(handle_err)?},
			//Extension record_size_limit, send this if reduced. Our limit is always 16384.
			if {p.reduced_record_size} EXTENSION RECORD_SIZE_LIMIT(16384u16),
		))
	);
	tx_handshake3(debug.clone(), HandshakeType::SERVER_HELLO, content3, base, hs_hash, state)
}
