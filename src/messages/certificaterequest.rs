use super::HashMessages;
use super::common::check_any_signatures;
use super::common::get_sig_bounding_set;
use super::common::send_signature_algorithms3;
use super::common::SignatureUsage;
use super::common::tx_handshake3;
use super::fields::FIELD_CERT_CONTEXT;
use super::fields::FIELD_EXT_LIST;
use crate::callbacks::TlsCallbacks;
use crate::common::is_unknown_extension;
use crate::errors::Error;
use crate::features::ConfigPages;
use crate::logging::DebugSetting;
use crate::record::DowncallInterface;
use crate::stdlib::Vec;
use btls_aux_rope3::rope3;
use btls_aux_rope3::VectorFieldError;
use btls_aux_serialization::Sink;
use btls_aux_signatures::SignatureAlgorithmEnabled2;
use btls_aux_tls_iana::ExtensionType as E;
use btls_aux_tls_iana::HandshakeType;
use btls_aux_tls_struct::LEN_EXTENSION_DATA;
use btls_aux_tls_struct::RemoteFeature as RF;

//Send a certificate_request message.
pub(crate) fn tx_certificate_request(context: &[u8], cflags: &ConfigPages,
	enabled_sig: SignatureAlgorithmEnabled2, debug: DebugSetting, base: &mut impl DowncallInterface,
	hs_hash: &mut impl HashMessages, state: &str, callbacks: &mut dyn TlsCallbacks,
	server_caps: &mut ServerSupport) -> Result<(), Error>
{
	fail_if!(!check_any_signatures(cflags, enabled_sig, SignatureUsage::Tls13Client), error!(adv_no_signatures));
	//Extra extensions.
	let mut extra_exts = Vec::new();
	let mut special_extensions = Vec::new();
	let mut reqs = callbacks.offer_unknown_extension();
	for (id, payload) in reqs.drain(..) {
		sanity_check!(is_unknown_extension(id), F "Tried to send invalid unknown extension {id}");
		special_extensions.push(id);
		assert_some!(extra_exts.write_u16(id), "Failed to write extension id");
		assert_some!(extra_exts.vector2_fn(LEN_EXTENSION_DATA, |x|x.write_slice(&payload)),
			"Failed to write extension payload");
	}
	*server_caps = ServerSupport::new(enabled_sig, special_extensions);
	//EMIT CERTIFICATEREQUEST.
	let handle_err = |e:VectorFieldError|error!(serialize_failed "certificate_request", e);
	let content = rope3!(ERROR_MAP handle_err,
		VECTOR FIELD_CERT_CONTEXT(context),
		VECTOR FIELD_EXT_LIST(
			EXTENSION STATUS_REQUEST(),
			EXTENSION SIGNED_CERTIFICATE_TIMESTAMP(),
			{send_signature_algorithms3(cflags, enabled_sig, SignatureUsage::Tls13Client).
				map_err(handle_err)?},
			extra_exts
		)
	);
	tx_handshake3(debug.clone(), HandshakeType::CERTIFICATE_REQUEST, content, base, hs_hash, state)
}



pub(crate) struct ServerSupport
{
	//Only capture stuff not easily available in configuration.
	enabled_sig: SignatureAlgorithmEnabled2,
	//This is most of the time empty.
	special_extensions: Vec<u16>,
}

impl ServerSupport
{
	pub(crate) fn dummy() -> ServerSupport
	{
		ServerSupport {
			enabled_sig: SignatureAlgorithmEnabled2::none(),
			special_extensions: Vec::new(),
		}
	}
	pub(crate) fn new(enabled_sig: SignatureAlgorithmEnabled2, special_extensions: Vec<u16>) -> ServerSupport
	{
		ServerSupport {
			enabled_sig,
			special_extensions,
		}
	}
	pub(crate) fn get_enabled_sig(&self) -> SignatureAlgorithmEnabled2 { self.enabled_sig }
	pub(crate) fn enable(&self, config: &crate::server::ServerConfiguration, f: RF) -> bool
	{
		use btls_aux_tls_iana::CertificateStatusType as CSTY;
		let flags0 = config.flags0();
		//Only include what is needed to parse the certificate message.
		match f {
			//Extensions.
			RF::Extension(E::STATUS_REQUEST) => true,
			RF::Extension(E::SIGNED_CERTIFICATE_TIMESTAMP) => true,
			//These two are needed to unlock DelegatedCredentials and SignatureAlgorithm.
			RF::Extension(E::DELEGATED_CREDENTIALS) => true,
			RF::Extension(E::SIGNATURE_ALGORITHMS) => true,
			//Extra extensions.
			RF::Extension(e) => self.special_extensions.iter().cloned().any(|e2|e.get()==e2),
			RF::SignatureAlgorithm(s) => self.enabled_sig.allows_tls2(s) &&
				get_sig_bounding_set(flags0, SignatureUsage::Tls13Client).is_in2(s),
			//Certificate status type.
			RF::CertificateStatusType(t) => t == CSTY::OCSP,
			//Rest is just plain not supported.
			_ => false
		}
	}
}
