use super::HashMessages;
use super::is_grease_alpn;
use super::SignatureUsage;
use super::common::do_renego;
use super::common::check_any_signatures;
use super::common::get_sig_bounding_set;
use super::common::send_signature_algorithms3;
use super::common::tx_handshake3;
use super::fields::FIELD_ALPN_ENTRY;
use super::fields::FIELD_ALPN_LIST;
use super::fields::FIELD_CIPHERSUITE_LIST;
use super::fields::FIELD_COMPRESSION_LIST;
use super::fields::FIELD_COOKIE;
use super::fields::FIELD_EC_FORMATS;
use super::fields::FIELD_EXT_LIST;
use super::fields::FIELD_GROUP_LIST;
use super::fields::FIELD_HOST_NAME;
use super::fields::FIELD_OCSP_EXTENSIONS;
use super::fields::FIELD_OCSP_RESPONDER_LIST;
use super::fields::FIELD_SERVER_NAME_LIST;
use super::fields::FIELD_SHARE_LIST;
use super::fields::FIELD_S_CERT_TYPE_LIST;
use super::fields::FIELD_SESSION_ID;
use super::fields::FIELD_VERSION_LIST;
use crate::callbacks::TlsCallbacks;
use crate::client::AlpnProtocol;
use crate::client::ReferenceNameOwned;
use crate::common::is_unknown_extension;
use crate::common::TlsVersion;
use crate::common::unknown_allowed_in_ee;
use crate::errors::Error;
use crate::features::ConfigPages;
use crate::logging::DebugSetting;
use crate::record::DowncallInterface;
use crate::stdlib::from_utf8;
use crate::stdlib::min;
use crate::stdlib::Vec;
use btls_aux_tlsciphersuites::KeyExchangeType;
use btls_aux_dhf::KEM;
use btls_aux_dhf::KemEnabled;
use btls_aux_dhf::KemInitiator;
use btls_aux_dhf::KemPublicKey;
use btls_aux_rope3::fragment_alpn_list;
use btls_aux_rope3::fragment_array_map;
use btls_aux_rope3::fragment_asref2;
use btls_aux_rope3::fragment_iterator as fragment_iterator3;
use btls_aux_rope3::fragment_key_shares;
use btls_aux_rope3::fragment_zeros as fragment_zeros3;
use btls_aux_rope3::FragmentWrite as FragmentWrite3;
use btls_aux_rope3::KemPublicKey as RopeKemPublicKey3;
use btls_aux_rope3::rope3;
use btls_aux_rope3::VectorFieldError;
use btls_aux_serialization::Sink;
use btls_aux_serialization::Source;
use btls_aux_signatures::SignatureAlgorithmEnabled2;
use btls_aux_tls_iana::AlpnProtocolId;
use btls_aux_tls_iana::HandshakeType;
use btls_aux_tls_struct::LEN_EXTENSION_DATA;
use btls_aux_tls_struct::LEN_PROTOCOL_NAME;
use btls_aux_tls_struct::RemoteFeature as RF;
//use btls_aux_tls_struct::ClientHello;
use btls_aux_tlsciphersuites::Ciphersuite;
use btls_aux_tlsciphersuites::CiphersuiteEnabled2;

///A list of ALPN protocols.
///
///This structure is internally created for the [`select_alpn()`] callback.
///
///The only possible way to access the internal ALPN (Application Layer Protocols Negotiation) protocol name is
///via the [`iter()`](#method.iter) method.
///
///[`select_alpn()`]: trait.TlsCallbacks.html#method.select_alpn
#[derive(Copy,Clone,Debug)]
pub struct AlpnList<'a>(&'a [u8]);
///An iterator type for the ALPN list [`AlpnList`](struct.AlpnList.html).
///
///Most notably, implements the `Iterator<Item=&str>` trait. Each protocol in the list is yielded in turn.
///
///This structure can be created using the [`AlpnList::iter()`](struct.AlpnList.html#method.iter) method.
#[derive(Clone,Debug)]
pub struct AlpnListIterator<'a>(Source<'a>);

impl<'a> AlpnList<'a>
{
	#[doc(hidden)]
	pub fn blank() -> AlpnList<'a>
	{
		static BLANK: [u8;0] = [];
		AlpnList(&BLANK[..])
	}
	pub(crate) fn __new(x: &'a [u8]) -> AlpnList<'a> { AlpnList(x) }
	///Make an iterator (yielding `&'a str` for each element) out of ALPN list.
	pub fn iter(&self) -> AlpnListIterator<'a> { AlpnListIterator(Source::new(self.0)) }
}

impl<'a> Iterator for AlpnListIterator<'a>
{
	type Item = &'a str;
	fn next(&mut self) -> Option<&'a str>
	{
		while self.0.more() {
			let alp = self.0.slice(LEN_PROTOCOL_NAME).ok()?;
			if is_grease_alpn(alp) { continue; }
			match from_utf8(alp) { Ok(x) => return Some(x), Err(_) => continue };
		}
		None
	}
}

///Send server name indication (client)
fn send_client_sni3<'a>(sni: &'a ReferenceNameOwned) -> Result<impl FragmentWrite3+'a, VectorFieldError>
{
	Ok(if let &ReferenceNameOwned::DnsName(ref name) = sni {
		Some(rope3!(EXTENSION SERVER_NAME(
			VECTOR FIELD_SERVER_NAME_LIST(
				ServerNameType::HOST_NAME,
				VECTOR FIELD_HOST_NAME(name)
			)
		)))
	} else {
		None		//Blank.
	})
}

#[derive(Clone)]
struct KemListAdapter<I:Iterator<Item=KEM>+Clone>(I);

impl<I:Iterator<Item=KEM>+Clone> Iterator for KemListAdapter<I>
{
	type Item = u16;
	fn next(&mut self) -> Option<u16> { self.0.next().map(|x|x.tls_id()) }
}


const MAX_CIPHERS: usize = 64;
#[derive(Copy)]
struct RopeTlsCiphers([u16;MAX_CIPHERS],usize);

//Pfft...
impl Clone for RopeTlsCiphers
{
	fn clone(&self) -> RopeTlsCiphers { *self }
}

impl RopeTlsCiphers
{
	fn new_x<FlagFn>(aes_hw: bool, flag_fn: FlagFn) -> RopeTlsCiphers
		where FlagFn: Fn(Ciphersuite) -> Option<u32>
	{
		let mut ciphertable = [(0xFFFFFFFFu32, 0xFFFFFFFFu32, 0u16);MAX_CIPHERS];
		let mut j = 0;
		for i in Ciphersuite::all_suites() {
			let flag = match flag_fn(*i) { Some(x) => x, None => continue };
			let priority = i.get_priority();
			let priority = if aes_hw { priority.hw } else { priority.sw };
			ciphertable.get_mut(j).map(|x|*x = (flag, priority, i.tls_id()));
			j += 1;
		}
		RopeTlsCiphers::new_table(&mut ciphertable)
	}
	fn new_table(tab: &mut [(u32, u32, u16)]) -> RopeTlsCiphers
	{
		let mut ciphertable2 = [0u16;MAX_CIPHERS];
		let mut count = 0;
		tab.sort();
		for i in tab.iter() {
			if i.2 == 0 { break; }  //End of list.
			if ciphertable2.get_mut(count).map(|x|*x = i.2).is_none() { break; } //No more entries fit.
			count += 1;
		}
		RopeTlsCiphers(ciphertable2, count)
	}
	fn new_tls12(aes_hw: bool, enabled_cs: CiphersuiteEnabled2, enable_ecdsa: bool, enable_rsa: bool) ->
		RopeTlsCiphers
	{
		RopeTlsCiphers::new_x(aes_hw, |i|{
			fail_if_none!(!enabled_cs.allows2(i));	//Filter disabled ciphersuites.
			match i.get_key_exchange() {
				KeyExchangeType::Tls12EcdheEcdsa if enable_ecdsa => Some(0),
				KeyExchangeType::Tls12EcdheRsa if enable_rsa => Some(1),
				_ => None,
			}
		})
	}
	fn new_tls13(aes_hw: bool, enabled_cs: CiphersuiteEnabled2) -> RopeTlsCiphers
	{
		RopeTlsCiphers::new_x(aes_hw, |i|{
			fail_if_none!(!enabled_cs.allows2(i));	//Filter disabled ciphersuites.
			match i.get_key_exchange() { KeyExchangeType::Tls13 => Some(0), _ => None }
		})
	}
}

impl AsRef<[u16]> for RopeTlsCiphers
{
	fn as_ref(&self) -> &[u16] { &self.0[..min(self.0.len(), self.1)] }
}

pub(crate) struct KemKeyPair(pub(crate) KemInitiator, pub(crate) KemPublicKey);

impl KemKeyPair
{
	pub(crate) fn pair(x: (KemInitiator, KemPublicKey)) -> KemKeyPair
	{
		KemKeyPair(x.0, x.1)
	}
}

pub(crate) struct ClientHelloParameters<'a>
{
	//Offer TLS 1.2.
	pub offer_tls12: bool,
	//Minimum CH size.
	pub min_ch_size: usize,
	//Config flags.
	pub flags: ConfigPages,
	//Is ACME connection.
	pub is_acme: bool,
	//Cookie.
	pub cookie: Option<&'a [u8]>,
	//SNI to send.
	pub sni: Option<&'a ReferenceNameOwned>,
	//Might use SPKI only.
	pub might_use_spki_only: bool,
	//ALPNs supported.
	pub alpn: &'a [AlpnProtocol],
	//Enable ECDSA.
	pub enable_ecdsa: bool,
	//Enable RSA.
	pub enable_rsa: bool,
	//AES-GCM is hardware-accelerated.
	pub aes_gcm_is_hw: bool,
	//Client random.
	pub client_random: &'a [u8;32],
	//Middlebox compat mode.
	pub middlebox_compat: bool,
	//Special extension list.
	pub special_extensions: &'a mut Vec<u16>,
	//No data transfer flag (flags connection as internal).
	pub no_data_transfer: bool,
	//Key shares.
	pub key_shares: &'a [KemKeyPair],
}

fn extract_tls_version(x: &TlsVersion) -> u16 { x.to_code().get() }

#[allow(unsafe_code)] unsafe impl btls_aux_memory::TrustedInvariantLength for KemKeyPair
{
	//There is no interrior mutability for self.1.
	fn as_byte_slice(&self) -> &[u8] { self.1.as_byte_slice() }
}

impl RopeKemPublicKey3 for KemKeyPair
{
	fn get_group(&self) -> u16 { self.1.kem().tls_id() }
}


pub(crate) fn tx_client_hello(p: &mut ClientHelloParameters, debug: DebugSetting, base: &mut impl DowncallInterface,
	hs_hash: &mut impl HashMessages, state: &str, callbacks: &mut dyn TlsCallbacks,
	caps: &mut ClientSupport) -> Result<(), Error>
{
	//Compute the flags.
	let mut eflags = 0;
	if p.enable_ecdsa { eflags |= EFLAG_ENABLE_ECDSA; }
	if p.enable_rsa { eflags |= EFLAG_ENABLE_RSA; }
	if p.is_acme { eflags |= EFLAG_IS_ACME; }
	if p.might_use_spki_only { eflags |= EFLAG_ENABLE_SPKI; }
	if p.sni.is_none() { eflags |= EFLAG_NO_SNI; }
	if p.offer_tls12 { eflags |= EFLAG_OFFER_TLS12; }
	caps.eflags = eflags;
	let enabled_cs = caps.enabled_cs;
	let enabled_kem = caps.enabled_kem;
	let enabled_sig = caps.enabled_sig;
	//Extra extensions. Not done if checking ACME.
	let mut extra_exts = Vec::new();
	if !p.no_data_transfer {
		let mut reqs = callbacks.offer_unknown_extension();
		for (id, payload) in reqs.drain(..) {
			sanity_check!(is_unknown_extension(id), F "Tried to send invalid unknown extension {id}");
			assert_some!(extra_exts.write_u16(id), "Failed to write extension id");
			assert_some!(extra_exts.vector2_fn(LEN_EXTENSION_DATA, |x|x.write_slice(&payload)),
				"Failed to write extension payload");
			if unknown_allowed_in_ee(id) { p.special_extensions.push(id); }
		}
	} else {
		//Signal the DowncallInterface to treat this as internal.
		base.treat_as_internal();
	}
	//Save the special extensions.
	caps.special_extensions = p.special_extensions.clone();

	let ciphers_12 = RopeTlsCiphers::new_tls12(p.aes_gcm_is_hw, enabled_cs, p.enable_ecdsa, p.enable_rsa);
	let ciphers_13 = RopeTlsCiphers::new_tls13(p.aes_gcm_is_hw, enabled_cs);
	//Check that there is at least one supported algorithm, as serialization will fail otherwise!
	fail_if!(enabled_kem.iter().count() == 0, error!(adv_no_groups));
	fail_if!(ciphers_13.as_ref().len() == 0, error!(adv_no_cipher_suites));
	fail_if!(!check_any_signatures(&p.flags, enabled_sig, SignatureUsage::Tls13Server), error!(adv_no_signatures));

	let handle_err = |e:VectorFieldError|error!(serialize_failed "client_hello", e);
	let basecontent3 = rope3!(ERROR_MAP handle_err,
		TlsVersion::TLS_1_2,
		{p.client_random},
		VECTOR FIELD_SESSION_ID(if {p.middlebox_compat} {p.client_random}),
		VECTOR FIELD_CIPHERSUITE_LIST(
			if {p.offer_tls12} {fragment_asref2::<u16,_>(ciphers_12)},
			{fragment_asref2::<u16,_>(ciphers_13)}
		),
		VECTOR FIELD_COMPRESSION_LIST(CompressionMethod::NULL)
	);
	//If TLS 1.2 is offered, use Tls12Server to allow various TLS 1.2 stuff, else require Tls13Server,
	//which disables deprecated in TLS 1.3 stuff.
	let sig_usage = if p.offer_tls12 { SignatureUsage::Tls12Server } else { SignatureUsage::Tls13Server };
	let extensions3 = rope3!(ERROR_MAP handle_err,
		EXTENSION SUPPORTED_VERSIONS(VECTOR FIELD_VERSION_LIST(
			{fragment_array_map(TlsVersion::supported_tls13(), extract_tls_version)},
			if {p.offer_tls12} TlsVersion::TLS_1_2
		)),
		//Include ALPN extension if requested. If ACME is active, use special fixed value.
		if {p.alpn.len() > 0 && !p.is_acme} EXTENSION APPLICATION_LAYER_PROTOCOL_NEGOTIATION(
			VECTOR FIELD_ALPN_LIST({
				fragment_alpn_list(p.alpn).map_err(|(idx,sz)|{
					assert_failure!(F "Length of ALPN entry #{idx} ({sz} bytes) out of range")
				})?
			})
		),
		if {p.is_acme} EXTENSION APPLICATION_LAYER_PROTOCOL_NEGOTIATION(
			VECTOR FIELD_ALPN_LIST(
				VECTOR FIELD_ALPN_ENTRY("acme-tls/1"),
			)
		),
		EXTENSION SUPPORTED_GROUPS(VECTOR FIELD_GROUP_LIST(
			{fragment_iterator3(enabled_kem.iter().map(|x|x.tls_id()))}
		)),
		EXTENSION KEY_SHARE(VECTOR FIELD_SHARE_LIST({
			fragment_key_shares(p.key_shares).map_err(|(idx,sz)|{
				assert_failure!(F "Length of key share entry #{idx} ({sz} bytes) out of range")
			})?
		})),
		//If sni is set, include server_name extension.
		if[Some(sni) = p.sni] {send_client_sni3(sni).map_err(handle_err)?},
		if {p.might_use_spki_only} EXTENSION SERVER_CERTIFICATE_TYPE(
			VECTOR FIELD_S_CERT_TYPE_LIST(
				CertificateType::RAW_PUBLIC_KEY,
				CertificateType::X509
			)
		),
		//Extensions only if certificate validation is not bypassed.
		if {!p.is_acme} CONCATENATE(
			EXTENSION STATUS_REQUEST(
				CertificateStatusType::OCSP,
				VECTOR FIELD_OCSP_RESPONDER_LIST(),
				VECTOR FIELD_OCSP_EXTENSIONS()
			),
			EXTENSION SIGNED_CERTIFICATE_TIMESTAMP()
		),
		//Include signature_algorithms extension.
		{send_signature_algorithms3(&p.flags, enabled_sig, sig_usage).map_err(handle_err)?},
		//If TLS 1.2 is on the table, offer extenended_master_secret and renegotiation_info.
		//And supported_point_formats, with just uncompressed as format.
		if {p.offer_tls12} CONCATENATE(
			EXTENSION EXTENDED_MASTER_SECRET(),
			{do_renego(b"").map_err(handle_err)?},
			EXTENSION EC_POINT_FORMATS(VECTOR FIELD_EC_FORMATS(EcPointFormat::UNCOMPRESSED))
		),
		//If we got cookie from last time, offer cookie.
		if[Some(cookie) = p.cookie] EXTENSION COOKIE(VECTOR FIELD_COOKIE(cookie)),
		//We always advertise 16384 byte limit (+1 is for TLS 1.3 record type).
		EXTENSION RECORD_SIZE_LIMIT(16385u16),
		//Extra extensions.
		extra_exts
	);
	//Pad the ClientHello if needed. The +2 is for extensions length, and +4 is for Padding extension header.
	let ch_size = basecontent3.length() + extensions3.length() + 2;
	let content3 = rope3!(ERROR_MAP handle_err,
		basecontent3,
		VECTOR FIELD_EXT_LIST(
			extensions3,
			if {ch_size < p.min_ch_size} EXTENSION PADDING(
				{fragment_zeros3(p.min_ch_size.saturating_sub(ch_size + 4))}
			)
		)
	);
	tx_handshake3(debug.clone(), HandshakeType::CLIENT_HELLO, content3, base, hs_hash, state)
}

const EFLAG_ENABLE_ECDSA: u8 = 1<<0;
const EFLAG_ENABLE_RSA: u8 = 1<<1;
const EFLAG_IS_ACME: u8 = 1<<2;
const EFLAG_ENABLE_SPKI: u8 = 1<<3;
const EFLAG_NO_SNI: u8 = 1<<4;
const EFLAG_OFFER_TLS12: u8 = 1<<6;

pub(crate) struct ClientSupport
{
	//Only capture stuff not easily available in configuration.
	eflags: u8,
	enabled_kem: KemEnabled,
	enabled_sig: SignatureAlgorithmEnabled2,
	enabled_cs: CiphersuiteEnabled2,
	//This is most of the time empty.
	special_extensions: Vec<u16>,
}

impl ClientSupport
{
	pub(crate) fn new(config: &crate::client::ClientConfiguration) -> ClientSupport
	{
		let (enabled_kem, enabled_sig, enabled_cs) = crate::features::get_algs_enabled_f(config.flags());
		ClientSupport {
			eflags: 0,	//Not initialized yet.
			enabled_kem: enabled_kem,
			enabled_sig: enabled_sig,
			enabled_cs: enabled_cs,
			special_extensions: Vec::new(),		//Not initialized yet.
		}
	}
	pub(crate) fn get_enabled_cs(&self) -> CiphersuiteEnabled2 { self.enabled_cs }
	pub(crate) fn get_enabled_kem(&self) -> KemEnabled { self.enabled_kem }
	pub(crate) fn get_enabled_sig(&self) -> SignatureAlgorithmEnabled2 { self.enabled_sig }
	pub(crate) fn enable(&self, config: &crate::client::ClientConfiguration, f: RF) -> bool
	{
		let (alpn, alpn_mask) = config.borrow_alpn();
		let offer_alpn = alpn.len() > 0 || self.eflags & EFLAG_IS_ACME != 0;
		let check_cert = self.eflags & EFLAG_IS_ACME == 0;
		let acme = self.eflags & EFLAG_IS_ACME != 0;
		let offering_tls12 = self.eflags & EFLAG_OFFER_TLS12 != 0;
		let enable_ecdsa = self.eflags & EFLAG_ENABLE_ECDSA != 0;
		let enable_rsa = self.eflags & EFLAG_ENABLE_RSA != 0;
		let flags0 = config.flags0();

		use btls_aux_tls_iana::ExtensionType as E;
		use btls_aux_tls_iana::TlsVersion as VER;
		use btls_aux_tls_iana::CertificateType as CTY;
		use btls_aux_tls_iana::CertificateStatusType as CSTY;
		match f {
			//Extensions.
			RF::Extension(E::SUPPORTED_VERSIONS) => true,
			RF::Extension(E::APPLICATION_LAYER_PROTOCOL_NEGOTIATION) => offer_alpn,
			RF::Extension(E::SUPPORTED_GROUPS) => true,
			RF::Extension(E::KEY_SHARE) => true,
			RF::Extension(E::SIGNATURE_ALGORITHMS) => true,
			RF::Extension(E::SERVER_NAME) => self.eflags & EFLAG_NO_SNI == 0,
			RF::Extension(E::SERVER_CERTIFICATE_TYPE) => self.eflags & EFLAG_ENABLE_SPKI != 0,
			RF::Extension(E::STATUS_REQUEST) => check_cert,
			RF::Extension(E::SIGNED_CERTIFICATE_TIMESTAMP) => check_cert,
			RF::Extension(E::EXTENDED_MASTER_SECRET) => offering_tls12,
			RF::Extension(E::RENEGOTIATION_INFO) => offering_tls12,
			RF::Extension(E::EC_POINT_FORMATS) => offering_tls12,
			RF::Extension(E::RECORD_SIZE_LIMIT) => true,
			//Extra extensions.
			RF::Extension(e) => self.special_extensions.iter().cloned().any(|e2|e.get()==e2),
			//Groups.
			RF::Group(g) => self.enabled_kem.allows2(g),
			RF::SignatureAlgorithm(s) => {
				let bound = get_sig_bounding_set(flags0, if offering_tls12 {
					SignatureUsage::Tls12Server
				} else {
					SignatureUsage::Tls13Server
				});
				self.enabled_sig.allows_tls2(s) && bound.is_in2(s)
			},
			RF::CipherSuite(c) => match Ciphersuite::by_tls_id4(c, self.enabled_cs) {
				Some(c) => match Some(c.get_key_exchange()) {
					Some(KeyExchangeType::Tls13) => true,
					Some(KeyExchangeType::Tls12EcdheEcdsa) => offering_tls12 && enable_ecdsa,
					Some(KeyExchangeType::Tls12EcdheRsa) => offering_tls12 && enable_rsa,
					_ => false, 
				},
				None => false,
			},
			//ALPN, just search saved list.
			RF::AlpnProtocol(alpn) if acme => alpn == btls_aux_tls_iana::AlpnProtocolId::ACME_TLS,
			RF::AlpnProtocol(protocol) => if let Some((_,bit)) =
				AlpnProtocolId::get_standard(protocol.get()) {
				alpn_mask.is_set(bit)
			} else {
				//Skip standard ALPNs, because those would have been matched above.
				alpn.iter().any(|alpn|match alpn {
					AlpnProtocol::Custom(ref c) => c.as_bytes()==protocol.get(),
					AlpnProtocol::Standard(_) => false
				})
			},
			//TLS versions.
			RF::Version(VER::TLS_1_3) => true,
			RF::Version(VER::TLS_1_2) => offering_tls12,
			//Server certificate types.
			RF::ServerCertificateType(CTY::X509) => true,
			RF::ServerCertificateType(CTY::RAW_PUBLIC_KEY) => self.eflags & EFLAG_ENABLE_SPKI != 0,
			//Certificate status type.
			RF::CertificateStatusType(CSTY::OCSP) => check_cert,
			//Others flat out not supported.
			_ => false
		}
	}
}
