use super::HashMessages;
use super::fields::FIELD_ALPN_ENTRY;
use super::fields::FIELD_ALPN_LIST;
use super::fields::FIELD_RENEGO_CONN;
use super::fields::FIELD_SIGALG;
use super::fields::FIELD_SIGALG_CERT;
use crate::errors::Error;
use crate::features::ConfigPages;
use crate::features::FLAGS0_ALLOW_DYNAMIC_RSA;
use crate::features::FLAGS0_ALLOW_PKCS1_CLIENT;
use crate::features::FLAGS0_ALLOW_PKCS1_SIG;
use crate::features::FLAGS0_NO_ADVERTISE_ECDSA_SIG;
use crate::features::FLAGS0_NO_ADVERTISE_EDDSA_SIG;
use crate::features::FLAGS0_NO_ADVERTISE_RSA_SIG;
use crate::features::FLAGS0_NO_ADVERTISE_UNKNOWN_SIG;
use crate::features::FLAGS0_ONLY_FLOATY_KEYS;
use crate::logging::DebugSetting;
use crate::record::DowncallInterface;
use crate::stdlib::min;
use btls_aux_rope3::fragment_asref2;
use btls_aux_rope3::fragment_sequence;
use btls_aux_rope3::FragmentWrite as FragmentWrite3;
use btls_aux_rope3::rope3;
use btls_aux_rope3::VectorFieldError;
use btls_aux_set2::SetOf;
use btls_aux_signatures::SignatureAlgorithmEnabled2;
use btls_aux_signatures::SignatureAlgorithm2;
use btls_aux_signatures::SignatureAlgorithmTls2;
use btls_aux_signatures::SignatureTypeMajor2;
use btls_aux_tls_iana::HandshakeType;
use core::i32::MIN as I32_MIN;

//Get priority for TLS algo id
fn get_priority(algo: u16) -> i32
{
	//Assign minimum possible priority to all unknown entries.
	SignatureAlgorithmTls2::by_tls_id(algo).map(|x|x.to_generic().get_priority()).unwrap_or(I32_MIN)
}

///Signature usage.
#[derive(Copy,Clone,PartialEq,Eq)]
pub enum SignatureUsage
{
	///Certificate.
	Certificate,
	///TLS 1.2 server (or dual TLS 1.2/1.3 server).
	Tls12Server,
	///TLS 1.3 server
	Tls13Server,
	///TLS 1.3 client.
	Tls13Client,
}

pub struct SignatureIterator<T:Iterator<Item=SignatureAlgorithmTls2>>
{
	raw: T,
	usage: SignatureUsage,
	flags0: u64,
}

impl<T:Iterator<Item=SignatureAlgorithmTls2>> SignatureIterator<T>
{
	pub fn new(usage: SignatureUsage, flags: ConfigPages, iterator: T) -> SignatureIterator<T>
	{
		SignatureIterator {
			usage: usage,
			raw: iterator,
			flags0: flags.get_flags(),
		}
	}
}

pub(crate) fn do_server_alpn<'a>(alpn: Option<&'a str>) -> Result<impl FragmentWrite3+'a, VectorFieldError>
{
	Ok(if let Some(alpn) = alpn {
		Some(rope3!(EXTENSION APPLICATION_LAYER_PROTOCOL_NEGOTIATION(
			VECTOR FIELD_ALPN_LIST(VECTOR FIELD_ALPN_ENTRY(alpn))
		)))
	} else {
		None
	})
}

pub(crate) fn do_renego<'a>(conn: &'a [u8]) -> Result<impl FragmentWrite3+'a, VectorFieldError>
{
	Ok(rope3!(EXTENSION RENEGOTIATION_INFO(VECTOR FIELD_RENEGO_CONN(conn))))
}

pub(crate) fn get_sig_bounding_set(flags0: u64, usage: SignatureUsage) -> SetOf<SignatureAlgorithm2>
{
	use btls_aux_signatures::SignatureAlgorithm2 as SA;
	let only_float = flags0 & FLAGS0_ONLY_FLOATY_KEYS != 0;
	let allow_rsa = flags0 & FLAGS0_ALLOW_DYNAMIC_RSA != 0;
	let allow_pkcs1 = flags0 & FLAGS0_ALLOW_PKCS1_SIG != 0;
	let allow_ccert = flags0 & FLAGS0_ALLOW_PKCS1_CLIENT != 0;
	use self::SignatureUsage::*;
	//The more-specific requirements.
	let mut requirement = match usage {
		Tls12Server => SA::SET_ALL,
		Tls13Server => SA::SET_TLS13_OK,
		Tls13Client if allow_pkcs1 => SA::SET_ALL,
		Tls13Client if allow_ccert => SA::SET_CCONLY | SA::SET_TLS13_OK,
		Tls13Client => SA::SET_TLS13_OK,
		Certificate => SA::SET_HAS_OID,
	};
	//If !allow_rsa, the algorithm has to be in NOT_RSA. If only_float, then algorithm has to
	//be in NOT_RSA_ENCRYPTION. Certificates are extempt.
	if usage != Certificate {
		if !allow_rsa { requirement = requirement & SA::SET_NOT_RSA; }
		if only_float { requirement = requirement & SA::SET_NOT_RSA_ENCRYPTION; }
	}
	requirement
}

pub(crate) fn signature_is_ok(item: SignatureAlgorithmTls2, flags0: u64, usage: SignatureUsage) -> bool
{
	//It has to be of advertised type.
	let nflag = match item.to_generic().major() {
		SignatureTypeMajor2::Rsa => FLAGS0_NO_ADVERTISE_RSA_SIG,
		SignatureTypeMajor2::Ecdsa => FLAGS0_NO_ADVERTISE_ECDSA_SIG,
		SignatureTypeMajor2::Eddsa => FLAGS0_NO_ADVERTISE_EDDSA_SIG,
		_ => FLAGS0_NO_ADVERTISE_UNKNOWN_SIG
	};
	if flags0 & nflag != 0 { return false; }
	get_sig_bounding_set(flags0, usage).is_in2(item)
}

impl<T:Iterator<Item=SignatureAlgorithmTls2>> Iterator for SignatureIterator<T>
{
	type Item=SignatureAlgorithmTls2;
	fn next(&mut self) -> Option<SignatureAlgorithmTls2>
	{
		loop {
			let item = self.raw.next()?;
			if signature_is_ok(item, self.flags0, self.usage) { return Some(item); }
		}
	}
}

#[derive(Copy)]
struct RopeSignatureAlgorithms([u16; 64], usize);

impl Clone for RopeSignatureAlgorithms { fn clone(&self) -> Self { *self }}

impl RopeSignatureAlgorithms
{
	pub fn new(flags: &ConfigPages, enabled_sig: SignatureAlgorithmEnabled2, usage: SignatureUsage) ->
		RopeSignatureAlgorithms
	{
		//Read up to 64 signatures.
		let mut siglist = [0u16; 64];
		for (n, v) in SignatureIterator::new(usage, *flags, enabled_sig.iter_tls()).enumerate() {
			//Ok, write to list.
			siglist.get_mut(n).map(|t|*t=v.tls_id());	//Ignore overflowing entries.
		}
		//Sort entries in descending order of priority.
		siglist.sort_by(|x,y|get_priority(*y).cmp(&get_priority(*x)));
		//Count number of valid algorithms: Algorithm is valid if its ID is not 0.
		let count = siglist.iter().filter(|alg|**alg!=0).count();
		RopeSignatureAlgorithms(siglist, count)
	}
}

macro_rules! slicearr
{
	($x:expr,$y:expr) => { &$x[..min($x.len(), $y)] }
}

impl AsRef<[u16]> for RopeSignatureAlgorithms
{
	fn as_ref(&self) -> &[u16] { slicearr!(self.0, self.1) }
}

impl PartialEq for RopeSignatureAlgorithms
{
	fn eq(&self, rhs: &RopeSignatureAlgorithms) -> bool { slicearr!(self.0, self.1) == slicearr!(rhs.0, rhs.1) }
}

//Assume SignatureUsage::Certificate is superset of all others.
pub(crate) fn check_any_signatures(flags: &ConfigPages, enabled_sig: SignatureAlgorithmEnabled2,
	usage: SignatureUsage) -> bool
{
	SignatureIterator::new(usage, *flags, enabled_sig.iter_tls()).next().is_some()
}

///Send signature algorithms offer.
pub(crate) fn send_signature_algorithms3(flags: &ConfigPages, enabled_sig: SignatureAlgorithmEnabled2,
	usage: SignatureUsage) -> Result<impl FragmentWrite3, VectorFieldError>
{
	let inner_l = RopeSignatureAlgorithms::new(flags, enabled_sig, usage);
	let inner_r = RopeSignatureAlgorithms::new(flags, enabled_sig, SignatureUsage::Certificate);
	let ntc = inner_r.as_ref().len() > 0;
	Ok(rope3!(
		EXTENSION SIGNATURE_ALGORITHMS(VECTOR FIELD_SIGALG(
			{fragment_asref2::<u16, _>(inner_l)}
		)),
		//If signature_algorithms_cert does not match signature_algorithms, send it too.
		if {inner_l != inner_r && ntc} EXTENSION SIGNATURE_ALGORITHMS_CERT(VECTOR FIELD_SIGALG_CERT(
			{fragment_asref2::<u16, _>(inner_r)}
		))
	))
}

//Emit a scatter-gather handshake message, adding it to the handshake hash.
//
//Parameters:
//
// * debug: The debug context.
// * htype: The handshake message type.
// * data: The message payload.
// * base: The downcall interface to send via.
// * hs_hash: The running handshake hash.
// * state: The name of handshake state to use in debug messages.
// * callbacks: The TLS callbacks structure.
//
//Notes:
//
// * Does not actually flush the message onto wire. A break must be forced for that.
//
//Returns nothing on success, on error an error object.
pub(crate) fn tx_handshake3(debug: DebugSetting, htype: HandshakeType, data: impl FragmentWrite3,
	base: &mut impl DowncallInterface, hs_hash: &mut impl HashMessages, state: &str) -> Result<(), Error>
{
	let tsize = data.length();
	sanity_check!(tsize <= 0xFFFFFF, F "Handshake message too big to send ({tsize})");
	let hdr = [htype.get(), (tsize >> 16) as u8, (tsize >> 8) as u8, tsize as u8];
	let fdata = fragment_sequence!(&hdr[..], data);
	hs_hash.add_v3(&fdata, htype, debug)?;
	base.send_handshake_v3(&fdata, tsize.saturating_add(4), htype, state);
	Ok(())
}
