use super::HashMessages;
use super::common::tx_handshake3;
use super::fields::FIELD_SIGNATURE;
use super::fields::FIELD_TLS12_ECDH;
use crate::common::CryptoTracker;
use crate::errors::Error;
use crate::logging::DebugSetting;
use crate::record::DowncallInterface;
use crate::stdlib::Vec;
use btls_aux_dhf::KemPublicKey;
use btls_aux_fail::ResultExt;
use btls_aux_rope3::fragment_trusted_invariant_length;
use btls_aux_rope3::rope3;
use btls_aux_rope3::VectorFieldError;
use btls_aux_set2::SetOf;
use btls_aux_signatures::SignatureAlgorithm2;
use btls_aux_signatures::SignatureAlgorithmTls2;
use btls_aux_tls_iana::HandshakeType;


//Send a CertificateVerify or ServerKeyExchange+ServerHelloDone message.
//
//Parameters:
//
// * signature: The signature on message. Must be Ok((tls_wire_id, rawsigdata)). If Err(()), causes an error.
// * peer_sigalgo_mask: The signature algorithms allowed by the peer.
// * tracker: Crypto algorithm tracker to update with the algorithm (optional).
// * base: The downcall interface to send via.
// * hs_hash: The running handshake hash.
// * state: The name of handshake state to use in debug messages.
// * ske_base: The ServerKeyExchange without signature. If not provoded, a CertificateVerify is sent instead.
// * debug: The debug context.
// * callbacks: The TLS callbacks structure.
//
//Notes:
//
// * If sending SKE+SHD, forces break.
//
//Returns nothing on success, on error an error object.
pub fn tx_certificate_verify(signature: Result<(u16, Vec<u8>), ()>, peer_sigalgo_mask: SetOf<SignatureAlgorithm2>,
	mut tracker: Option<&mut CryptoTracker>, base: &mut impl DowncallInterface, hs_hash: &mut impl HashMessages,
	state: &str, ske_base: Option<&KemPublicKey>, debug: DebugSetting) -> Result<(), Error>
{
	let (htype, hname) = if ske_base.is_some() {
		(HandshakeType::SERVER_KEY_EXCHANGE, "server_key_exchange")
	} else {
		(HandshakeType::CERTIFICATE_VERIFY, "certificate_verify")
	};
	let (sigalgo, signature) = signature.set_err(error!(kex_signature_failed))?;
	//Signing, so allow all schemes.
	if let Some(x) = SignatureAlgorithmTls2::by_tls_id(sigalgo) {
		fail_if!(!peer_sigalgo_mask.is_in(&x), error!(interlock_signature_algorithm));
		if let &mut Some(ref mut crypto_algs) = &mut tracker { crypto_algs.set_signature(x); }
	} else {
		sanity_failed!(F "Trying to sign handshake with unknown signature scheme {sigalgo:x}");
	};

	let handle_err = |e:VectorFieldError|error!(serialize_failed hname, e);
	let content = rope3!(ERROR_MAP handle_err,
		if[Some(share) = ske_base] CONCATENATE(
			3u8,							//Fixed group.
			{share.kem().tls_id()},					//Group ID.
			VECTOR FIELD_TLS12_ECDH({fragment_trusted_invariant_length(share)}),	//Share.
		),
		sigalgo,
		VECTOR FIELD_SIGNATURE(signature)
	);
	debug!(HANDSHAKE_EVENTS debug, "Certificate: Sending exchange signature");
	tx_handshake3(debug.clone(), htype, content, base, hs_hash, state)?;
	if htype == HandshakeType::SERVER_KEY_EXCHANGE {
		//The next message after ServerKeyExchange is ServerHelloDone.
		//This kind of message fusion would cause problems if there is anything between ServerKeyExchange
		//and ServerHelloDone. Fortunately, that only includes CertificateRequest, which we do not and will
		//not support. Also, it would cause problems if ServerKeyExchange is missing, but that can only
		//happen in pure-PSK, which we do not support and will not support. This is the last message in
		//flight.
		tx_handshake3(debug, HandshakeType::SERVER_HELLO_DONE, (), base, hs_hash, state)?;
		base.force_break()?;
	}
	Ok(())
}
