use super::HashMessages;
use super::common::tx_handshake3;
use super::fields::FIELD_CERT;
use super::fields::FIELD_CERT_CONTEXT;
use super::fields::FIELD_CERT_LIST;
use super::fields::FIELD_EECERT;
use super::fields::FIELD_EXT_LIST;
use super::fields::FIELD_OCSP_RESPONSE;
use super::fields::FIELD_RPK;
use super::fields::FIELD_SCT_LIST;
use super::fields::FIELD_TRANS_LIST;
use crate::certificates::ServerCertificate;
use crate::certificates::ClientCertificate;
use crate::certificates::get_isrg_x1_dst_hack_certificate;
use crate::common::CryptoTracker;
use crate::errors::Error;
use crate::logging::DebugSetting;
use crate::record::DowncallInterface;
use crate::stdlib::Box;
use crate::stdlib::Deref;
use crate::stdlib::Vec;
use btls_aux_rope3::FragmentWrite as FragmentWrite3;
use btls_aux_rope3::fragment_sct_list;
use btls_aux_rope3::fragment_tls12_cert_list;
use btls_aux_rope3::fragment_tls13_cert_list;
use btls_aux_rope3::rope3;
use btls_aux_rope3::VectorFieldError;
use btls_aux_tls_iana::HandshakeType;
use btls_aux_x509certparse::extract_spki;

///A client certificate object. Which is a certificate waiting to be emitted.
pub(crate) struct ClientCertificateObject
{
	///The actual selected certificate.
	pub certificate: Box<dyn ClientCertificate+Send>,
	///True => Only send SPKI of the certificate, False => send the entiere certificate.
	pub send_spki_only: bool,
	///Allow inclusion of OCSP staples.
	pub ocsp_enabled: bool,
	///Allow inclusion of SCT staples.
	pub sct_enabled: bool,
	///Allow inclusion of transparency items.
	pub trans_enabled: bool,
}

///A certificate object. Which is a certificate waiting to be emitted.
pub(crate) struct CertificateObject
{
	///The actual selected certificate.
	pub certificate: ServerCertificate,
	///True => Only send SPKI of the certificate, False => send the entiere certificate.
	pub send_spki_only: bool,
	///Allow inclusion of OCSP staples.
	pub ocsp_enabled: bool,
	///Allow inclusion of SCT staples.
	pub sct_enabled: bool,
	///Allow inclusion of transparency items.
	pub trans_enabled: bool,
}

//Parameters for tx_certificate function.
pub(crate) struct TlsCertificateParameters<'a,'b>
{
	//The certificate object to emit.
	pub certificate: &'a CertificateObject,
	//The end-entity certificate. If None, this is fetched on demand.
	pub eecert: &'b [u8],
	//The certificate chain. If None, this is fetched on demand. Note that this must have at leat one entry,
	//unless eecert is Some _and_ RPK mode is enabled.
	pub chain: &'a [Vec<u8>],
	//The OCSP staple. If None, this is fetched on demand.
	pub ocsp_res: Option<&'a [u8]>,
	//The SCT staples. If None, this is fetched on demand.
	pub sct_res: &'a [Vec<u8>],
	//The Transparency Info staples. If None, this is fetched on demand.
	pub trans_res: &'a [Vec<u8>],
	//ISRG X1 DST hack.
	pub isrg_x1_dst_hack: bool,
	//True => emit TLS 1.3 layout, false => emit TLS 1.2 layout.
	pub tls13: bool,
}

fn certificate_status_msg<'a>(staple: &'a [u8]) -> Result<impl FragmentWrite3+'a, VectorFieldError>
{
	sanity_check!(staple.len() > 0, "Certificate has empty OCSP staple data");
	Ok(rope3!(
		CertificateStatusType::OCSP,
		VECTOR FIELD_OCSP_RESPONSE(staple)
	))
}

pub(crate) fn sct_extension_3<'a>(staple: &'a [Vec<u8>]) -> Result<impl FragmentWrite3+'a, VectorFieldError>
{
	sanity_check!(staple.len() > 0, "Certificate selection gave empty SCT list");
	Ok(rope3!(EXTENSION SIGNED_CERTIFICATE_TIMESTAMP(
		VECTOR FIELD_SCT_LIST({
			//TODO: Do this better
			fragment_sct_list(staple).map_err(|(idx,sz)|{
				assert_failure!(F "Signed Certificate timestamp #{idx} too big ({sz} bytes)")
			})?
		})
	)))
}

pub(crate) fn trans_extension_3<'a>(staple: &'a [Vec<u8>]) -> Result<impl FragmentWrite3+'a, VectorFieldError>
{
	//The CTv1 types "sct" and LEN_SERIALIZED_SCT_LIST are close enough to work for CTv2.
	sanity_check!(staple.len() > 0, "Certificate selection gave empty transparency item list");
	Ok(rope3!(EXTENSION TRANSPARENCY_INFO(
		VECTOR FIELD_TRANS_LIST({
			//TODO: Do this better
			fragment_sct_list(staple).map_err(|(idx,sz)|{
				assert_failure!(F "Transparency item #{idx} too big ({sz} bytes)")
			})?
		})
	)))
}

//No tracker state changes occur, because OCSP and CT are never sent with SPKI.
pub(crate) fn tx_certificate_spki(eecert: &[u8], tls13: bool, debug: DebugSetting,
	base: &mut impl DowncallInterface, hs_hash: &mut impl HashMessages, state: &str) -> Result<(), Error>
{
	let spki: &[u8] = extract_spki(eecert).map_err(|x|error!(cant_extract_spki x))?;

	let handle_err = |e:VectorFieldError|error!(serialize_failed "certificate(spki)", e);
	let content = rope3!(ERROR_MAP handle_err,
		if tls13 CONCATENATE(
			VECTOR FIELD_CERT_CONTEXT(),
			VECTOR FIELD_CERT_LIST(
				VECTOR FIELD_RPK(spki),
				VECTOR FIELD_EXT_LIST()
			)
		) else CONCATENATE(
			VECTOR FIELD_RPK(spki)
		)
	);
	tx_handshake3(debug.clone(), HandshakeType::CERTIFICATE, content, base, hs_hash, state)?;
	Ok(())
}

fn empty_list_to_none<'a>(t: &'a [Vec<u8>]) -> Option<&'a [Vec<u8>]>
{
	if t.len() > 0 { Some(t) } else { None }
}

//Send a certificate message.
pub(crate) fn tx_certificate(params: TlsCertificateParameters, debug: DebugSetting,
	tracker: Option<&mut CryptoTracker>, base: &mut impl DowncallInterface,
	hs_hash: &mut impl HashMessages, state: &str) -> Result<(), Error>
{
	let ocsp_res: Option<&[u8]> = if params.certificate.ocsp_enabled { params.ocsp_res } else { None };
	let sct_res = if params.certificate.sct_enabled {
		empty_list_to_none(params.sct_res)
	} else {
		None
	};
	let trans_res = if params.certificate.trans_enabled {
		empty_list_to_none(params.trans_res)
	} else {
		None
	};

	call(send_certificate_tail {
		eecert: params.eecert,
		chain: params.chain,
		tls13: params.tls13,
		send_spki_only: params.certificate.send_spki_only,
		isrg_x1_dst_hack: params.isrg_x1_dst_hack,
		ocsp_res: ocsp_res,
		sct_res: sct_res,
		trans_res: trans_res,
		debug: debug,
		tracker: tracker,
		base: base,
		hs_hash: hs_hash,
		state: state
	})
}

//Send a certificate message.
pub(crate) fn tx_certificate_client(xcertificate: &ClientCertificateObject, debug: DebugSetting,
	tracker: Option<&mut CryptoTracker>, base: &mut impl DowncallInterface,
	hs_hash: &mut impl HashMessages, state: &str) -> Result<(), Error>
{
	let certificate = xcertificate.certificate.deref();
	let ocsp_res = if xcertificate.ocsp_enabled { certificate.ocsp() } else { None };
	let sct_res = if xcertificate.sct_enabled {
		empty_list_to_none(certificate.scts())
	} else {
		None
	};
	let trans_res = if xcertificate.trans_enabled {
		empty_list_to_none(certificate.trans_items())
	} else {
		None
	};

	call(send_certificate_tail {
		eecert: certificate.certificate(),
		chain: certificate.chain(),
		tls13: true,
		send_spki_only: xcertificate.send_spki_only,
		isrg_x1_dst_hack: false,
		ocsp_res: ocsp_res,
		sct_res: sct_res,
		trans_res: trans_res,
		debug: debug,
		tracker: tracker,
		base: base,
		hs_hash: hs_hash,
		state: state,
	})
}

#[allow(non_camel_case_types)]
struct send_certificate_tail<'anon,DIF:DowncallInterface,HM:HashMessages>
{
	eecert: &'anon [u8],
	chain: &'anon [Vec<u8>],
	tls13: bool,
	send_spki_only: bool,
	isrg_x1_dst_hack: bool,
	ocsp_res: Option<&'anon [u8]>,
	sct_res: Option<&'anon [Vec<u8>]>,
	trans_res: Option<&'anon [Vec<u8>]>,
	debug: DebugSetting,
	tracker: Option<&'anon mut CryptoTracker>,
	base: &'anon mut DIF,
	hs_hash: &'anon mut HM,
	state: &'anon str,
}

impl<'anon,DIF:DowncallInterface,HM:HashMessages> Call for send_certificate_tail<'anon,DIF,HM>
{
	type Output = Result<(), Error>;
	fn call(self) -> Result<(), Error>
	{
		let send_certificate_tail{eecert, chain, tls13, send_spki_only, isrg_x1_dst_hack, ocsp_res, sct_res,
			trans_res, debug, mut tracker, base, hs_hash, state} = self;
		sanity_check!(!send_spki_only, "send_certificate_tail(): Can not handle send_spki_only");
		if let (true, &mut Some(ref mut x)) = (ocsp_res.is_some() && tls13, &mut tracker) {
			x.ocsp_validated();
		}
		if let (true, &mut Some(ref mut x)) = (sct_res.is_some() && tls13, &mut tracker) {
			x.ct_validated();
		}
		if tls13 {
			let content = __send_tls13_certificate(eecert, chain, ocsp_res, sct_res, trans_res).
				map_err(|e|error!(serialize_failed "certificate(tls1.3)", e))?;
			tx_handshake3(debug, HandshakeType::CERTIFICATE, content, base, hs_hash, state)
		} else {
			let content = __send_tls12_certificate(eecert, chain, isrg_x1_dst_hack).
				map_err(|e|error!(serialize_failed "certificate(tls1.2)", e))?;
			tx_handshake3(debug.clone(), HandshakeType::CERTIFICATE, content, base, hs_hash, state)?;
			//Send OCSP message if we have one.
			if let Some(ocspcontent) = ocsp_res {
				let content = certificate_status_msg(ocspcontent).
					map_err(|e|error!(serialize_failed "certificate_status", e))?;
				tx_handshake3(debug.clone(), HandshakeType::CERTIFICATE_STATUS,
					content, base, hs_hash, state)?;
				debug!(HANDSHAKE_EVENTS debug, "Sent OCSP staple");
				//Mark the OCSP as stapled.
				if let &mut Some(ref mut x) = &mut tracker { x.ocsp_validated(); }
			}
			Ok(())
		}
	}
}

fn __send_tls12_certificate<'a>(eecert: &'a [u8], chain: &'a [Vec<u8>], isrg_x1_dst_hack: bool) ->
	Result<impl FragmentWrite3+'a, VectorFieldError>
{
	Ok(rope3!(
		VECTOR FIELD_CERT_LIST(
			VECTOR FIELD_EECERT(eecert),
			{
				fragment_tls12_cert_list(chain).map_err(|(idx,sz)|{
					assert_failure!(F "CA Certificate entry #{idx} ({sz} bytes) too big")
				})?
			},
			if isrg_x1_dst_hack VECTOR FIELD_CERT({get_isrg_x1_dst_hack_certificate()})
		)
	))
}

fn __send_tls13_certificate<'a>(eecert: &'a [u8], chain: &'a [Vec<u8>], ocsp: Option<&'a [u8]>,
	sct: Option<&'a [Vec<u8>]>, trans: Option<&'a [Vec<u8>]>) -> Result<impl FragmentWrite3+'a, VectorFieldError>
{
	Ok(rope3!(
		VECTOR FIELD_CERT_CONTEXT(),
		VECTOR FIELD_CERT_LIST(
			VECTOR FIELD_EECERT(eecert),
			VECTOR FIELD_EXT_LIST(
				//TLS 1.3 sticks the certificate_status message inside extension.
				if[Some(x) = ocsp] EXTENSION STATUS_REQUEST({certificate_status_msg(x)?}),
				if[Some(x) = sct] {sct_extension_3(x)?},
				if[Some(x) = trans] {trans_extension_3(x)?}
			),
			{
				fragment_tls13_cert_list(chain).map_err(|(idx,sz)|{
					assert_failure!(F "CA Certificate entry #{idx} ({sz} bytes) too big")
				})?
			}
		)
	))
}

//Send a certificate message (client NAK).
pub(crate) fn tx_certificate_client_nak(context: Option<&[u8]>, debug: DebugSetting,
	base: &mut impl DowncallInterface, hs_hash: &mut impl HashMessages, state: &str) -> Result<(), Error>
{
	let content = __send_cert_nak(context).map_err(|e|error!(serialize_failed "certificate(nak)", e))?;
	tx_handshake3(debug.clone(), HandshakeType::CERTIFICATE, content, base, hs_hash, state)
}

fn __send_cert_nak<'a>(context: Option<&'a [u8]>) -> Result<impl FragmentWrite3+'a, VectorFieldError>
{
	Ok(rope3!(
		//Context is only present in TLS 1.3.
		if[Some(ctx) = context] VECTOR FIELD_CERT_CONTEXT(ctx),
		VECTOR FIELD_CERT_LIST()
	))
}


pub trait Call
{
	type Output: Sized;
	fn call(self) -> <Self as Call>::Output;
}

fn call<T:Call>(t: T) -> <T as Call>::Output { t.call() }
