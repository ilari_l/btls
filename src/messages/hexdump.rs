//This generates crazy amount (100+(!)) of warnings.
#![allow(unsafe_op_in_unsafe_fn)]
//use super::FragmentReader;
//use super::FragmentWriteDyn;
use crate::test::var_os;
use btls_aux_collections::String;
use btls_aux_collections::Vec;
use btls_aux_memory::BackedVector;
use btls_aux_memory::slice_assume_init;
use btls_aux_rope3::FragmentWriteDyn as FragmentWriteDyn3;
use core::cmp::min;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::mem::MaybeUninit;
use core::ptr::copy_nonoverlapping;
use core::ptr::read;

trait LineDraw
{
	fn size() -> usize;
	unsafe fn horizontal(out: *mut u8) -> *mut u8;
	unsafe fn vertical(out: *mut u8) -> *mut u8;
	unsafe fn upleft(out: *mut u8) -> *mut u8;
	unsafe fn upmid(out: *mut u8) -> *mut u8;
	unsafe fn upright(out: *mut u8) -> *mut u8;
	unsafe fn midleft(out: *mut u8) -> *mut u8;
	unsafe fn midmid(out: *mut u8) -> *mut u8;
	unsafe fn midright(out: *mut u8) -> *mut u8;
	unsafe fn downleft(out: *mut u8) -> *mut u8;
	unsafe fn downmid(out: *mut u8) -> *mut u8;
	unsafe fn downright(out: *mut u8) -> *mut u8;
	unsafe fn fleft(out: *mut u8) -> *mut u8;
	unsafe fn fright(out: *mut u8) -> *mut u8;
}

struct Ascii;
struct Unicode;

unsafe fn linedraw_ascii(out: *mut u8, x: u8) -> *mut u8
{
	out.write(x);
	out.add(1)
}

impl LineDraw for Ascii
{
	fn size() -> usize { 1 }
	unsafe fn horizontal(out: *mut u8) -> *mut u8 { linedraw_ascii(out, b'-') }
	unsafe fn vertical(out: *mut u8) -> *mut u8 { linedraw_ascii(out, b'|') }
	unsafe fn upleft(out: *mut u8) -> *mut u8 { linedraw_ascii(out, b'+') }
	unsafe fn upmid(out: *mut u8) -> *mut u8 { linedraw_ascii(out, b'+') }
	unsafe fn upright(out: *mut u8) -> *mut u8 { linedraw_ascii(out, b'+') }
	unsafe fn midleft(out: *mut u8) -> *mut u8 { linedraw_ascii(out, b'+') }
	unsafe fn midmid(out: *mut u8) -> *mut u8 { linedraw_ascii(out, b'+') }
	unsafe fn midright(out: *mut u8) -> *mut u8 { linedraw_ascii(out, b'+') }
	unsafe fn downleft(out: *mut u8) -> *mut u8 { linedraw_ascii(out, b'+') }
	unsafe fn downmid(out: *mut u8) -> *mut u8 { linedraw_ascii(out, b'+') }
	unsafe fn downright(out: *mut u8) -> *mut u8 { linedraw_ascii(out, b'+') }
	unsafe fn fleft(out: *mut u8) -> *mut u8 { linedraw_ascii(out, b'[') }
	unsafe fn fright(out: *mut u8) -> *mut u8 { linedraw_ascii(out, b']') }
}

unsafe fn linedraw_unicode(out: *mut u8, x: u8) -> *mut u8
{
	out.cast::<u16>().write_unaligned(u16::from_be(0xe294));
	out.add(2).write(x);
	out.add(3)
}

impl LineDraw for Unicode
{
	fn size() -> usize { 3 }
	unsafe fn horizontal(out: *mut u8) -> *mut u8 { linedraw_unicode(out, 0x80) }
	unsafe fn vertical(out: *mut u8) -> *mut u8 { linedraw_unicode(out, 0x82) }
	unsafe fn upleft(out: *mut u8) -> *mut u8 { linedraw_unicode(out, 0x8c) }
	unsafe fn upmid(out: *mut u8) -> *mut u8 { linedraw_unicode(out, 0xac) }
	unsafe fn upright(out: *mut u8) -> *mut u8 { linedraw_unicode(out, 0x90) }
	unsafe fn midleft(out: *mut u8) -> *mut u8 { linedraw_unicode(out, 0x9c) }
	unsafe fn midmid(out: *mut u8) -> *mut u8 { linedraw_unicode(out, 0xbc) }
	unsafe fn midright(out: *mut u8) -> *mut u8 { linedraw_unicode(out, 0xa4) }
	unsafe fn downleft(out: *mut u8) -> *mut u8 { linedraw_unicode(out, 0x94) }
	unsafe fn downmid(out: *mut u8) -> *mut u8 { linedraw_unicode(out, 0xb4) }
	unsafe fn downright(out: *mut u8) -> *mut u8 { linedraw_unicode(out, 0x98) }
	unsafe fn fleft(out: *mut u8) -> *mut u8 { linedraw_unicode(out, 0xa4) }
	unsafe fn fright(out: *mut u8) -> *mut u8 { linedraw_unicode(out, 0x9c) }
}


fn num_to_hex(x: u8) -> u8
{
	let y = x + 6;
	(y >> 4) * 7 + x + 48
}

fn num_to_hexl(x: u8) -> u8
{
	let y = x + 6;
	(y >> 4) * 39 + x + 48
}

fn byte_to_ascii(x: u8) -> u8
{
	if x >= 32 && x <= 126 { x } else { b'.' }
}

unsafe fn write4(output: *mut u8, x: u16) -> *mut u8
{
	output.add(0).write(num_to_hex((x >> 12) as u8 & 15));
	output.add(1).write(num_to_hex((x >>  8) as u8 & 15));
	output.add(2).write(num_to_hex((x >>  4) as u8 & 15));
	output.add(3).write(num_to_hex((x      ) as u8 & 15));
	output.add(4)
}

unsafe fn copy_string(output: *mut u8, s: &str) -> *mut u8
{
	copy_nonoverlapping(s.as_ptr(), output, s.len());
	output.add(s.len())
}

unsafe fn hexdump_left_field<D:LineDraw>(mut output: *mut u8, line: u64, llen: usize, lpfx: &str) -> *mut u8
{
	output = copy_string(output, lpfx);
	output = D::vertical(output);
	for i in 0..llen {
		let x = line >> 4 * (llen - i - 1);
		output = linedraw_ascii(output, num_to_hexl(x as u8 & 15));
	}
	D::vertical(output)
}

unsafe fn hexudmp_right_field<D:LineDraw>(mut output: *mut u8, input: *const u8, bytes: usize) -> *mut u8
{
	output = D::vertical(output);
	for i in 0..bytes { output = linedraw_ascii(output, byte_to_ascii(read(input.add(i)))); }
	for _ in bytes..16 { output = linedraw_ascii(output, 32); }
	output = D::vertical(output);
	linedraw_ascii(output, b'\n')
}

fn size_of_hex_line<D:LineDraw>(llen: usize, lpfx: &str) -> usize
{
	//The accounting of bytes on hex line is:
	// - lpfx.len() bytes of prefix.
	// - 1 line draw character.
	// - llen bytes for offset.
	// - 1 line draw character.
	// - 39 bytes for hexdump.
	// - 1 line draw character.
	// - 16 bytes for ASCII dump.
	// - 1 line draw character.
	// - 1 byte for LF.
	//
	// => 56 bytes, 4 characters, llen and lpfx.len().
	56 + 4 * D::size() + llen + lpfx.len()
}

//Writes 68+lpfx.len()+llen bytes in unicode mode, 60+lpfx.len()+llen otherwise.
unsafe fn write_complete_hexline<D:LineDraw>(mut output: *mut u8, input: *const u8, line: u64, llen: usize,
	lpfx: &str) -> *mut u8
{
	output = hexdump_left_field::<D>(output, line, llen, lpfx);
	for i in 0..7 {
		output = write4(output, input.add(2*i).cast::<u16>().read_unaligned());
		output = linedraw_ascii(output, b' ');
	}
	output = write4(output, input.add(14).cast::<u16>().read_unaligned());
	hexudmp_right_field::<D>(output, input, 16)
}

//Writes 68+lpfx.len()+llen bytes in unicode mode, 60+lpfx.len()+llen otherwise.
//Bytes MUST be less than 16.
unsafe fn write_partial_hexline<D:LineDraw>(mut output: *mut u8, input: *const u8, bytes: usize, line: u64,
	llen: usize, lpfx: &str) -> *mut u8
{
	output = hexdump_left_field::<D>(output, line, llen, lpfx);
	output.write_bytes(32, 39);
	for i in 0..bytes {
		let x = input.add(i).read();
		output.add(2*i+i/2+0).write(num_to_hex(x>>4));
		output.add(2*i+i/2+1).write(num_to_hex(x&15));
	}
	output = output.add(39);
	hexudmp_right_field::<D>(output, input, bytes)
}

pub(crate) unsafe trait HexdumpSource
{
	fn left(&self) -> usize;
	fn discard(&mut self, amt: usize);
	//This SHALL read partial buffer only if data runs out.
	fn read<'b>(&mut self, buffer: &'b mut [MaybeUninit<u8>]) -> &'b [u8];
}

unsafe fn write_hexlines<D:LineDraw,S:HexdumpSource>(mut output: *mut u8, input: &mut S, llen: usize,
	lpfx: &str, _: &D) -> *mut u8
{
	const BLOCKLINES: usize = 256;
	let mut buf = [MaybeUninit::uninit();BLOCKLINES<<4];
	let capacity = buf.len();
	//input.discard(strip);
	let mut line = 0;
	loop {
		let block = input.read(&mut buf);
		let mut bytes = block.len();
		let more = bytes == capacity;
		let mut block = block.as_ptr();
		//Complete lines.
		while bytes >= 16 {
			output = write_complete_hexline::<D>(output, block, line, llen, lpfx);
			block = block.add(16);
			bytes -= 16;
			line += 1;
		}
		//Last partial line. This can only happen on last block.
		if bytes > 0 { output = write_partial_hexline::<D>(output, block, bytes, line, llen, lpfx); }
		if !more { break; }
	}
	output
}

fn size_of_first_line<D:LineDraw>(label: &str, label_chars: usize, llen: usize, lpfx: &str) -> usize
{
	let mut ans = 1 + (llen + 59) * D::size() + lpfx.len();
	if label_chars > 0 {
		ans = ans - (label_chars + 2) * D::size() + 2 + label.len();
	}
	ans
}

unsafe fn first_line<D:LineDraw>(mut output: *mut u8, label: &str, label_chars: usize, llen: usize, lpfx: &str) ->
	*mut u8
{
	output = copy_string(output, lpfx);
	output = D::upleft(output);
	for _ in 0..llen { output = D::horizontal(output); }
	output = D::upmid(output);
	if label_chars > 0 {
		for _ in 0..(36-label_chars)/2 { output = D::horizontal(output); }
		output = D::fleft(output);
		output = linedraw_ascii(output, b' ');
		output = copy_string(output, label);
		output = linedraw_ascii(output, b' ');
		output = D::fright(output);
		for _ in 0..(35-label_chars)/2 { output = D::horizontal(output); }
	} else {
		for _ in 0..39 { output = D::horizontal(output); }
	}
	output = D::upmid(output);
	for _ in 0..16 { output = D::horizontal(output); }
	output = D::upright(output);
	linedraw_ascii(output, b'\n')
}

unsafe fn write_integer(mut output: *mut u8, length: usize, ldigits: usize) -> *mut u8
{
	for i in 0..ldigits {
		let mut d = 1;
		for _ in 0..ldigits-i-1 { d *= 10; }
		output = linedraw_ascii(output, 48 + (length / d % 10) as u8)
	}
	output
}

fn size_of_last_line<D:LineDraw>(ldigits: usize, llen: usize, lpfx: &str) -> usize
{
	9 + ldigits + (llen + 51 - ldigits) * D::size() + lpfx.len()
}

unsafe fn last_line<D:LineDraw>(mut output: *mut u8, length: usize, ldigits: usize, llen: usize, lpfx: &str) -> *mut u8
{
	output = copy_string(output, lpfx);
	output = D::downleft(output);
	for _ in 0..llen { output = D::horizontal(output); }
	output = D::downmid(output);
	for _ in 0..(30-ldigits)/2 { output = D::horizontal(output); }
	output = D::fleft(output);
	output = linedraw_ascii(output, b' ');
	output = write_integer(output, length, ldigits);
	output = copy_string(output, " bytes ");
	output = D::fright(output);
	for _ in 0..(29-ldigits)/2 { output = D::horizontal(output); }
	output = D::downmid(output);
	for _ in 0..16 { output = D::horizontal(output); }
	output = D::downright(output);
	linedraw_ascii(output, b'\n')
}

pub(crate) fn __calculate_xdigits(mut t: usize) -> usize
{
	let mut llen = 0;
	while t > 0 {
		t /= 16;
		llen += 1;
	}
	llen
}

pub(crate) fn __calculate_digits(mut t: usize) -> usize
{
	let mut ldigits = 0;
	while t > 0 {
		ldigits += 1;
		t /= 10;
	}
	if ldigits == 0 { ldigits = 1; }
	ldigits
}

fn __dump_hex_block<D:LineDraw,S:HexdumpSource>(source: &mut S, strip: usize, lpfx: &str, name: &str, ld: D) ->
	String
{ unsafe {
	source.discard(strip);
	let length = source.left();
	//Calculate length of line indices and digits in length.
	let hlines = (length + 15) / 16;
	let lchars = min(name.chars().count(), 35);
	let llen = __calculate_xdigits(if hlines > 0 { hlines - 1 } else { 0 });
	let ldigits = __calculate_digits(length);
	//Calculate size.
	let tsize = size_of_first_line::<D>(name, lchars, llen, lpfx) +
		hlines * size_of_hex_line::<D>(llen, lpfx) +
		size_of_last_line::<D>(ldigits, llen, lpfx);
	//Write stuff.
	let mut output = Vec::with_capacity(tsize);
	let out = output.as_mut_ptr();
	let out = first_line::<D>(out, name, lchars, llen, lpfx);
	let out = write_hexlines(out, source, llen, lpfx, &ld);
	let out = last_line::<D>(out, length, ldigits, llen, lpfx);
	let ssize = out as usize - output.as_ptr() as usize;
	debug_assert!(ssize == tsize);
	output.set_len(ssize);
	String::from_utf8_unchecked(output)
}}

fn _dump_hex_block<S:HexdumpSource>(source: &mut S, strip: usize, lpfx: &str, name: &str) -> String
{
	let unicode = var_os("BTLS_NO_UNICODE").is_none();
	if unicode {
		__dump_hex_block(source, strip, lpfx, name, Unicode)
	} else {
		__dump_hex_block(source, strip, lpfx, name, Ascii)
	}
}

unsafe impl<'a> HexdumpSource for &'a [u8]
{
	fn left(&self) -> usize { self.len() }
	fn discard(&mut self, amt: usize) { *self = self.get(amt..).unwrap_or(&[]); }
	fn read<'b>(&mut self, buffer: &'b mut [MaybeUninit<u8>]) -> &'b [u8]
	{
		unsafe {
			let amt = min(self.len(), buffer.len());
			copy_nonoverlapping::<u8>(self.as_ptr(), buffer.as_mut_ptr().cast(), amt);
			slice_assume_init(buffer, amt)
		}
	}
}


pub(crate) fn dump_hexblock(mut rope: &[u8], lpfx: &str, name: &str) -> String
{
	_dump_hex_block(&mut rope, 0, lpfx, name)
}

///Dump a block as hexadecimal dump.
pub struct DumpHexblock<'a>
{
	rope: &'a [u8],
	lpfx: &'a str,
	name: &'a str,
}

impl<'a> DumpHexblock<'a>
{
	pub fn new(rope: &'a [u8], lpfx: &'a str, name: &'a str) -> DumpHexblock<'a>
	{
		DumpHexblock{rope, lpfx, name}
	}
}

impl<'a> Display for DumpHexblock<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		//For now, first dump to intermediate string and then to output. This avoids having formatter
		//version of the hexdumper.
		let mut rope = self.rope;
		let tmp = _dump_hex_block(&mut rope, 0, self.lpfx, self.name);
		f.write_str(&tmp)
	}
}

struct Dumper3<'a>(&'a dyn FragmentWriteDyn3, usize);

unsafe impl<'a> HexdumpSource for Dumper3<'a>
{
	fn left(&self) -> usize { self.0.length_dyn().saturating_sub(self.1) }
	fn discard(&mut self, amt: usize) { self.1 += amt; }
	fn read<'b>(&mut self, buffer: &'b mut [MaybeUninit<u8>]) -> &'b [u8]
	{
		let mut buffer = BackedVector::new(buffer);
		self.0.write_dyn(self.1, &mut buffer);
		let buffer = buffer.into_inner();
		self.1 += buffer.len();
		buffer
	}
}

pub(crate) fn dump_hexblock3(rope: &dyn FragmentWriteDyn3, strip: usize, lpfx: &str, name: &str) ->
	String
{
	_dump_hex_block(&mut Dumper3(rope, 0), strip, lpfx, name)
}

///Dump a block as hexadecimal dump.
pub struct DumpHexblock3<'a>
{
	rope: &'a (dyn FragmentWriteDyn3+'a),
	strip: usize,
	lpfx: &'a str,
	name: &'a str,
}

impl<'a> DumpHexblock3<'a>
{
	pub fn new(rope: &'a (dyn FragmentWriteDyn3+'a), strip: usize, lpfx: &'a str, name: &'a str) ->
		DumpHexblock3<'a>
	{
		DumpHexblock3{rope, strip, lpfx, name}
	}
}

impl<'a> Display for DumpHexblock3<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		//For now, first dump to intermediate string and then to output. This avoids having formatter
		//version of the hexdumper.
		let tmp = _dump_hex_block(&mut Dumper3(self.rope, 0), self.strip, self.lpfx, self.name);
		f.write_str(&tmp)
	}
}


#[cfg(test)]
fn assert_first_line_size<D:LineDraw>()
{ unsafe {
	let mut buf = [0;1024];
	//Even.
	let sz = size_of_last_line::<D>(4, 5, "foobar" );
	let buf2 = last_line::<D>(buf.as_mut_ptr(), 1234, 4, 5, "foobar" );
	assert!(buf2 as usize - buf.as_ptr() as usize == sz);
	//Odd.
	let sz = size_of_last_line::<D>(5, 5, "foobar" );
	let buf2 = last_line::<D>(buf.as_mut_ptr(), 12345, 5, 5, "foobar" );
	assert!(buf2 as usize - buf.as_ptr() as usize == sz);
}}

#[cfg(test)]
fn assert_last_line_size<D:LineDraw>()
{ unsafe {
	let mut buf = [0;1024];
	//Empty label.
	let sz = size_of_first_line::<D>("", 0, 5, "foobar" );
	let buf2 = first_line::<D>(buf.as_mut_ptr(), "", 0, 5, "foobar" );
	assert!(buf2 as usize - buf.as_ptr() as usize == sz);
	//Even length label.
	let sz = size_of_first_line::<D>("hi", 2, 5, "foobar" );
	let buf2 = first_line::<D>(buf.as_mut_ptr(), "hi", 2, 5, "foobar" );
	assert!(buf2 as usize - buf.as_ptr() as usize == sz);
	//Odd length label.
	let sz = size_of_first_line::<D>("hi!", 3, 5, "foobar" );
	let buf2 = first_line::<D>(buf.as_mut_ptr(), "hi!", 3, 5, "foobar" );
	assert!(buf2 as usize - buf.as_ptr() as usize == sz);
}}

#[cfg(test)]
fn assert_hex_line_size<D:LineDraw>()
{ unsafe {
	let mut buf = [0;1024];
	let input = [42;16];
	let sz = size_of_hex_line::<D>(5, "foobar" );
	//Full line.
	let buf2 = write_complete_hexline::<D>(buf.as_mut_ptr(), input.as_ptr(), 10, 5, "foobar");
	assert!(buf2 as usize - buf.as_ptr() as usize == sz);
	//Partial line.
	let buf2 = write_partial_hexline::<D>(buf.as_mut_ptr(), input.as_ptr(), 11, 10, 5, "foobar");
	assert!(buf2 as usize - buf.as_ptr() as usize == sz);
}}



#[test] fn assert_first_line_size_ascii() { assert_first_line_size::<Ascii>(); }
#[test] fn assert_first_line_size_unicode() { assert_first_line_size::<Unicode>(); }
#[test] fn assert_last_line_size_ascii() { assert_last_line_size::<Ascii>(); }
#[test] fn assert_last_line_size_unicode() { assert_last_line_size::<Unicode>(); }
#[test] fn assert_hex_line_size_ascii() { assert_hex_line_size::<Ascii>(); }
#[test] fn assert_hex_line_size_unicode() { assert_hex_line_size::<Unicode>(); }

#[test]
fn hex_dump_0()
{
	let mut msg: &[u8] = &[];
	let result = __dump_hex_block(&mut msg, 0, "PREFIX>", "foo", Unicode);
	assert_eq!(result, "\
PREFIX>┌┬────────────────┤ foo ├────────────────┬────────────────┐
PREFIX>└┴──────────────┤ 0 bytes ├──────────────┴────────────────┘
");
}

#[test]
fn hex_dump_foo()
{
	let mut msg: &[u8] = b"foo";
	let result = __dump_hex_block(&mut msg, 0, "PREFIX>", "foo", Unicode);
	assert_eq!(result, "\
PREFIX>┌┬────────────────┤ foo ├────────────────┬────────────────┐
PREFIX>││666F 6F                                │foo             │
PREFIX>└┴──────────────┤ 3 bytes ├──────────────┴────────────────┘
");
}

#[test]
fn hex_dump_16()
{
	let mut msg: &[u8] = b"0123456789abcdef";
	let result = __dump_hex_block(&mut msg, 0, "PREFIX>", "foo", Unicode);
	assert_eq!(result, "\
PREFIX>┌┬────────────────┤ foo ├────────────────┬────────────────┐
PREFIX>││3130 3332 3534 3736 3938 6261 6463 6665│0123456789abcdef│
PREFIX>└┴──────────────┤ 16 bytes ├─────────────┴────────────────┘
");
}

#[test]
fn hex_dump_17()
{
	let mut msg: &[u8] = b"0123456789abcdefg";
	let result = __dump_hex_block(&mut msg, 0, "PREFIX>", "foo", Unicode);
	assert_eq!(result, "\
PREFIX>┌─┬────────────────┤ foo ├────────────────┬────────────────┐
PREFIX>│0│3130 3332 3534 3736 3938 6261 6463 6665│0123456789abcdef│
PREFIX>│1│67                                     │g               │
PREFIX>└─┴──────────────┤ 17 bytes ├─────────────┴────────────────┘
");
}

#[test]
fn hex_dump_17_ascii()
{
	let mut msg: &[u8] = b"0123456789abcdefg";
	let result = __dump_hex_block(&mut msg, 0, "PREFIX>", "foo", Ascii);
	assert_eq!(result, "\
PREFIX>+-+----------------[ foo ]----------------+----------------+
PREFIX>|0|3130 3332 3534 3736 3938 6261 6463 6665|0123456789abcdef|
PREFIX>|1|67                                     |g               |
PREFIX>+-+--------------[ 17 bytes ]-------------+----------------+
");
}

#[test]
fn hex_dump_16_strip()
{
	let mut msg: &[u8] = b"XYZZY____0123456789abcdef";
	let result = __dump_hex_block(&mut msg, 9, "PREFIX>", "foo", Unicode);
	assert_eq!(result, "\
PREFIX>┌┬────────────────┤ foo ├────────────────┬────────────────┐
PREFIX>││3130 3332 3534 3736 3938 6261 6463 6665│0123456789abcdef│
PREFIX>└┴──────────────┤ 16 bytes ├─────────────┴────────────────┘
");
}

