use super::HashMessages;
use super::fields::FIELD_TLS12_ECDH;
use super::common::tx_handshake3;
use crate::errors::Error;
use crate::logging::DebugSetting;
use crate::record::DowncallInterface;
use crate::stdlib::Deref;
use btls_aux_dhf::KemCiphertext;
use btls_aux_rope3::rope3;
use btls_aux_rope3::VectorFieldError;
use btls_aux_tls_iana::HandshakeType;


//Send a certificate_request message.
pub fn tx_client_key_exchange(pubkey: &KemCiphertext, debug: DebugSetting, base: &mut impl DowncallInterface,
	hs_hash: &mut impl HashMessages, state: &str) -> Result<(), Error>
{
	let pubkey = pubkey.deref();
	let handle_err = |e:VectorFieldError|error!(serialize_failed "client_key_exchange", e);
	let content = rope3!(ERROR_MAP handle_err,
		VECTOR FIELD_TLS12_ECDH({pubkey.as_raw()})
	);
	tx_handshake3(debug.clone(), HandshakeType::CLIENT_KEY_EXCHANGE, content, base, hs_hash, state)?;
	base.force_break()?;	//Next up is ChangeCipherSpec.
	Ok(())
}
