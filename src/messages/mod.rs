pub(crate) use self::certificate::CertificateObject;				//(!)
pub(crate) use self::certificate::ClientCertificateObject;			//(!)
pub(crate) use self::certificate::TlsCertificateParameters;			//(!)
pub(crate) use self::certificate::tx_certificate;				//(!)
pub(crate) use self::certificate::tx_certificate_client;			//(!)
pub(crate) use self::certificate::tx_certificate_client_nak;			//(!)
pub(crate) use self::certificate::tx_certificate_spki;				//(!)
pub(crate) use self::certificaterequest::ServerSupport;				//(!)
pub(crate) use self::certificaterequest::tx_certificate_request;		//(!)
pub(crate) use self::certificateverify::tx_certificate_verify;			//(!)
pub use self::clienthello::AlpnList;
pub use self::clienthello::AlpnListIterator;
pub(crate) use self::clienthello::ClientHelloParameters;			//(!)
pub(crate) use self::clienthello::ClientSupport;				//(!)
pub(crate) use self::clienthello::KemKeyPair;					//(!)
pub(crate) use self::clienthello::tx_client_hello;				//(!)
pub(crate) use self::clientkeyexchange::tx_client_key_exchange;			//(!)
pub(crate) use self::common::SignatureIterator;					//(!)
pub(crate) use self::common::SignatureUsage;					//(!)
pub(crate) use self::encryptedextensions::EncryptedExtensionsParameters;	//(!)
pub(crate) use self::encryptedextensions::tx_encrypted_extensions;		//(!)
pub(crate) use self::finished::tx_finished;					//(!)
pub(crate) use self::handshakehash::EarlyHandshakeRep;				//(!)
pub(crate) use self::handshakehash::HandshakeHash;				//(!)
pub(crate) use self::handshakehash::HandshakeMessage;				//(!)
pub(crate) use self::handshakehash::HashMessages;				//(!)
pub(crate) use self::hexdump::dump_hexblock;					//(!)
pub(crate) use self::hexdump::dump_hexblock3;					//(!)
pub(crate) use self::hexdump::DumpHexblock;					//(!)
pub(crate) use self::hexdump::DumpHexblock3;					//(!)
pub(crate) use self::serverhello::HRR_MAGIC;					//(!)
pub(crate) use self::serverhello::ServerHelloParameters;			//(!)
pub(crate) use self::serverhello::tx_server_hello;				//(!)
use crate::errors::Error;
use crate::stdlib::Arc;
use crate::stdlib::Deref;
use crate::stdlib::min;
use crate::stdlib::String;
use crate::stdlib::Vec;
use btls_aux_fail::ResultExt;
use btls_aux_serialization::Source;
use btls_aux_signatures::Hasher;
use btls_aux_signatures::SignatureAlgorithmEnabled2;
use btls_aux_signatures::SignatureAlgorithmTls2;
use btls_aux_signatures::SignatureError2;
use btls_aux_signatures::SignatureFlags2;
use btls_aux_signatures::SignatureTls2;
use btls_aux_tls_iana::ExtensionType;
use btls_aux_tls_iana::SignatureScheme;
use btls_aux_tls_struct::LEN_SIGNATURE;
pub use btls_aux_x509certparse::SctList;
pub use btls_aux_x509certparse::SctListIterator;


mod certificate;
mod certificaterequest;
mod certificateverify;
mod clienthello;
mod clientkeyexchange;
mod common;
mod encryptedextensions;
pub(crate) mod fields;
mod finished;
mod handshakehash;
mod hexdump;
mod serverhello;
pub(crate) mod tlsconsts;


type Debugger = crate::logging::DebugSetting;

///Base raw message.
pub struct BaseRawMessage<'a>(pub &'a [u8], pub Debugger);

///TLS DigitallySigned structure
#[derive(Copy,Clone,Debug)]
pub struct DigitallySigned<'a>
{
	///The algorithm.
	pub algorithm: SignatureAlgorithmTls2,
	///The signature value.
	pub signature: &'a [u8],
}

///Read error for DigitallySigned structure.
#[derive(Copy,Clone,Debug)]
pub enum DigitallySignedReadError
{
	///Can't parse.
	CantParse,
	///Bogus algorithm.
	BogusAlgorithm(SignatureScheme),
}

impl<'a> DigitallySigned<'a>
{
	///Parse a DigitallySigned structure.
	pub fn parse(msg: &mut Source<'a>, policy: SignatureAlgorithmEnabled2) ->
		Result<DigitallySigned<'a>, DigitallySignedReadError>
	{
		let (sigtype, sigdata) = {
			let sigalgo_id = SignatureScheme::new(
				msg.u16().set_err(DigitallySignedReadError::CantParse)?);
			//The flag in allows_tls is ignore private use, not allow private use, so always pass
			//false there (do not ignore private use).
			let sigalgo = SignatureAlgorithmTls2::by_tls_id2(sigalgo_id).
				and_then(|sigalgo|if policy.allows_tls2(sigalgo) {
				Some(sigalgo)
			} else {
				None
			}).ok_or(DigitallySignedReadError::BogusAlgorithm(sigalgo_id))?;
			let signature = msg.slice(LEN_SIGNATURE).
				set_err(DigitallySignedReadError::CantParse)?;
			(sigalgo, signature)
		};
		Ok(DigitallySigned {
			algorithm: sigtype,
			signature: sigdata,
		})
	}
	///Validate a signature.
	pub fn verify(self, key: &[u8], tbs: &[u8], policy: SignatureAlgorithmEnabled2,
		flags: SignatureFlags2) -> Result<(), SignatureError2>
	{
		SignatureTls2::new(self.algorithm, self.signature).verify(key, tbs, policy, flags)
	}
	///Validate a signature.
	pub fn verify_callback(self, key: &[u8], tbs: &mut dyn FnMut(&mut Hasher),
		policy: SignatureAlgorithmEnabled2, flags: SignatureFlags2) -> Result<(), SignatureError2>
	{
		SignatureTls2::new(self.algorithm, self.signature).verify_callback(key, policy, flags, tbs)
	}
}


///Trait of extension recognizer.
pub trait ExtensionAssignments: Sized
{
	///Get the extension entry corresponding to extension id, or None if none is known.
	fn get_entry(ext_id: ExtensionType, tls12: bool) -> Option<Self>;
	///Get the extension flag bit number corresponding to extension id.
	fn get_bit(&self) -> u32;
}

///Error from extension parsing.
#[derive(Clone)]
pub enum ExtensionParseError
{
	///The message can't be parsed,
	CantParse,
	///Duplicate extension.
	Duplicate(ExtensionType),
	///Junk after extension.
	JunkAfter(ExtensionType),
	///Wrapped error from extension parser.
	Wrapped(Error)
}


///Owned session ID type.
#[derive(Copy, Clone, Debug)]
pub struct SessionId([u8;32], usize);

impl SessionId
{
	///Create a blank session ID.
	pub fn blank() -> SessionId { SessionId([0;32],0) }
	pub(crate) fn new(data: &[u8]) -> Result<SessionId, ()>
	{
		let mut b = [0;32];
		fail_if!(data.len() > 32, ());
		(&mut b[..data.len()]).copy_from_slice(data);
		Ok(SessionId(b, data.len()))
	}
}

impl Deref for SessionId
{
	type Target = [u8];
	fn deref(&self) -> &[u8]
	{
		//We outright assume length is valid.
		&self.0[..min(self.1, self.0.len())]
	}
}

impl AsRef<[u8]> for SessionId
{
	fn as_ref(&self) -> &[u8] { self.deref() }
}

trait ToBytes
{
	fn to_bytes<'a>(&'a self) -> &'a [u8];
}

impl ToBytes for Arc<String>
{
	fn to_bytes<'a>(&'a self) -> &'a [u8] { self.as_bytes() }
}

impl ToBytes for Arc<Vec<u8>>
{
	fn to_bytes<'a>(&'a self) -> &'a [u8] { self.deref().deref() }
}

#[derive(Clone)]
struct ToBytesWrapper<T:ToBytes>(T);

impl<T:ToBytes> AsRef<[u8]> for ToBytesWrapper<T>
{
	fn as_ref(&self) -> &[u8] { self.0.to_bytes() }
}

fn is_grease_alpn(alpn: &[u8]) -> bool { alpn.len() == 2 && alpn[0] == alpn[1] && alpn[0] & 15 == 10 }
