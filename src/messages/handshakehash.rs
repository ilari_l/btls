use crate::errors::Error;
use crate::messages::DumpHexblock;
use crate::messages::DumpHexblock3;
use crate::logging::DebugSetting;
use crate::stdlib::Vec;
use btls_aux_hash::HashContext2;
use btls_aux_hash::HashFunction2;
use btls_aux_hash::HashOutput2;
use btls_aux_rope3::FragmentReader as FragmentReader3;
use btls_aux_rope3::FragmentWriteDyn as FragmentWriteDyn3;
use btls_aux_rope3::FragmentWriteOps as FragmentWriteOps3;
use btls_aux_tls_iana::HandshakeType;
use core::mem::MaybeUninit;


//Structure containing a handshake message.
#[derive(Clone)]
pub struct HandshakeMessage<'a>
{
	hs_type: HandshakeType,	//The type of message.
	msg: &'a [u8],		//The message payload.
}

impl<'a> HandshakeMessage<'a>
{
	pub fn new(hs_type: HandshakeType, msg: &'a [u8]) -> HandshakeMessage<'a>
	{
		HandshakeMessage {
			hs_type: hs_type,
			msg: msg,
		}
	}
}

pub trait HashMessages
{
	//Add a message to be hashed.
	fn add<'a>(&mut self, msg: HandshakeMessage<'a>, debug: DebugSetting) -> Result<(), Error>;
	//Add a message consisting of fragment to be hashed.
	fn add_v3(&mut self, msgv: &dyn FragmentWriteDyn3, hstype: HandshakeType, debug: DebugSetting) ->
		Result<(), Error>;
}

//Handshake hash.
pub struct HandshakeHash
{
	context: HashContext2,
}

impl HandshakeHash
{
	pub fn new(prf: HashFunction2, debug: DebugSetting) -> HandshakeHash
	{
		debug!(CRYPTO_CALCS debug, "Setting up handshake hash: {prf:?}");
		HandshakeHash {
			context: prf.context()
		}
	}
	//Input raw data (no headers are inserted) into handshake hash.
	pub fn write_raw(&mut self, raw: &[u8], debug: DebugSetting) -> Result<(), Error>
	{
		self.context.append(raw);
		debug!(CRYPTO_CALCS debug, "Including {rlen} bytes of raw data into handshake hash, updated:\n{data}",
			rlen=raw.len(), data=DumpHexblock::new(&self.checkpoint(), "  ", "Data"));
		Ok(())
	}
	//Reinitialize hash and link hashes, with given explicit linked hash.
	pub fn chain_handshake(&mut self, hash: &[u8], debug: DebugSetting)
	{
		self.context.reset();
		let dlen = hash.len();
		let header = [254, (dlen >> 16) as u8, (dlen >> 8) as u8, dlen as u8];
		self.context.append_v(&[&header, hash]);
		debug!(CRYPTO_CALCS debug, "Compressing handshake block, updated:\n{data}",
			data=DumpHexblock::new(&self.checkpoint(), "  ", "Result"));
	}
	pub fn compress(&mut self, debug: DebugSetting)
	{
		let hashout = self.checkpoint();
		self.chain_handshake(&hashout, debug)
	}
	//Get the output of handshake hash.
	pub fn checkpoint(&self) -> HashOutput2
	{
		self.context.output()
	}
}

impl HashMessages for HandshakeHash
{
	fn add<'a>(&mut self, msg: HandshakeMessage<'a>, debug: DebugSetting) -> Result<(), Error>
	{
		let dlen = msg.msg.len();
		let header = [msg.hs_type.get(), (dlen >> 16) as u8, (dlen >> 8) as u8, dlen as u8];
		self.context.append_v(&[&header, msg.msg]);
		debug!(CRYPTO_CALCS debug, "Including a message {htype} in handshake hash:\n{data}\n{hash}",
			htype=msg.hs_type, data=DumpHexblock::new(msg.msg, "  ", "Payload"),
			hash=DumpHexblock::new(&self.checkpoint(), "  ", "Hash now"));
		Ok(())
	}
	fn add_v3(&mut self, msgv: &dyn FragmentWriteDyn3, hstype: HandshakeType, debug: DebugSetting) ->
		Result<(), Error>
	{
		let mut buf = [MaybeUninit::uninit();16384];
		let mut reader = FragmentReader3::new(msgv);
		loop {
			let (more, tail) = reader.read(&mut buf);
			self.context.append(tail);
			if !more { break; }
		}
		debug!(CRYPTO_CALCS debug, "Including a message {hstype} in handshake hash:\n{data}\n{hash}",
			data=DumpHexblock3::new(msgv, 4, "  ", "Payload"),
			hash=DumpHexblock::new(&self.checkpoint(), "  ", "Hash Now"));
		Ok(())
	}
}

//Early handshake hash. This can possibly be in plaintext as the hash function is not necressarily known.
pub enum EarlyHandshakeRep
{
	Plaintext(Vec<u8>),
	Hash(HandshakeHash),
}

impl EarlyHandshakeRep
{
	///Create new empty handshake representation.
	pub fn new() -> EarlyHandshakeRep { EarlyHandshakeRep::Plaintext(Vec::new()) }
	///Convert the early handshake representation to hashed form. If compress is true, the hash is injected
	///indirectly (used for restart handshake in TLS 1.3-draft19+.). Can only be called once.
	pub fn convert_to_hash(&mut self, hash: HashFunction2, debug: DebugSetting, compress: bool) ->
		Result<(), Error>
	{
		let mut hs_hash = HandshakeHash::new(hash, debug.clone());
		match self {
			&mut EarlyHandshakeRep::Plaintext(ref x) => {
				hs_hash.write_raw(x, debug.clone())?;
				if compress { hs_hash.compress(debug.clone()); }
			},
			_ => sanity_failed!("Attempted to set PRF twice"),
		};
		*self = EarlyHandshakeRep::Hash(hs_hash);
		Ok(())
	}
	///Unwrap the early handshake representation as hash. convert_to_hash() must be called before this.
	pub fn unwrap_hash(self) -> Result<HandshakeHash, Error>
	{
		match self {
			EarlyHandshakeRep::Plaintext(_) => sanity_failed!("PRF not set before calling unwrap_hash"),
			EarlyHandshakeRep::Hash(x) => Ok(x),
		}
	}
}

impl HashMessages for EarlyHandshakeRep
{
	fn add<'a>(&mut self, msg: HandshakeMessage<'a>, debug: DebugSetting) -> Result<(), Error>
	{
		match self {
			&mut EarlyHandshakeRep::Plaintext(ref mut client_hello) => {
				let dlen = msg.msg.len();
				let header = [msg.hs_type.get(), (dlen >> 16) as u8, (dlen >> 8) as u8,
					dlen as u8];
				client_hello.extend_from_slice(&header);
				client_hello.extend_from_slice(msg.msg);
			},
			&mut EarlyHandshakeRep::Hash(ref mut hash) => { hash.add(msg, debug)?; }
		}
		Ok(())
	}
	fn add_v3(&mut self, msgv: &dyn FragmentWriteDyn3, hstype: HandshakeType, debug: DebugSetting) ->
		Result<(), Error>
	{
		match self {
			&mut EarlyHandshakeRep::Plaintext(ref mut client_hello) =>
				FragmentWriteOps3::extend_vector_dyn(msgv, client_hello),
			&mut EarlyHandshakeRep::Hash(ref mut hash) => { hash.add_v3(msgv, hstype, debug)?; }
		}
		Ok(())
	}
}
