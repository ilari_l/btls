use super::HashMessages;
use super::common::do_server_alpn;
use super::common::tx_handshake3;
use super::fields::FIELD_EXT_LIST;
use crate::callbacks::TlsCallbacks;
use crate::common::test_failure;
use crate::common::unknown_allowed_in_ee;
use crate::errors::Error;
use crate::features::ConfigPages;
use crate::features::FLAGS5_TEST_EE_SEND_FAIL;
use crate::logging::DebugSetting;
use crate::record::DowncallInterface;
use crate::stdlib::Vec;
use btls_aux_rope3::rope3;
use btls_aux_rope3::VectorFieldError;
use btls_aux_serialization::Sink;
use btls_aux_tls_iana::HandshakeType;
use btls_aux_tls_struct::LEN_EXTENSION_DATA;


pub(crate) struct EncryptedExtensionsParameters<'a>
{
	//Send spki only.
	pub send_spki_only: bool,
	//Client reduced record size.
	pub reduced_record_size: bool,
	//Config flags.
	pub flags: &'a ConfigPages,
	//Interesting extensions.
	pub interesting_exts: Vec<u16>,
	//ALPN.
	pub alpn: Option<&'a str>,
}

pub(crate) fn tx_encrypted_extensions(mut p: EncryptedExtensionsParameters, debug: DebugSetting,
	base: &mut impl DowncallInterface, hs_hash: &mut impl HashMessages, state: &str,
	callbacks: &mut dyn TlsCallbacks) -> Result<(), Error>
{
	//call respond_unknown_extension in order to collect responses to unknown extensions.
	let mut extra_exts = Vec::new();
	for i in p.interesting_exts.drain(..) {
		if !unknown_allowed_in_ee(i) { continue; }
		if let Some(response) = callbacks.respond_unknown_extension(i) {
			assert_some!(extra_exts.write_u16(i), "Failed to write extension id");
			assert_some!(extra_exts.vector2_fn(LEN_EXTENSION_DATA, |x|x.write_slice(&response)),
				"Failed to write extension payload");
		}
	}
	//EMIT ENCRYPTEDEXTENSIONS.
	test_failure(p.flags, FLAGS5_TEST_EE_SEND_FAIL)?;

	let handle_err = |e:VectorFieldError|error!(serialize_failed "encrypted_extensions", e);
	let content = rope3!(ERROR_MAP handle_err, VECTOR FIELD_EXT_LIST(
		//Extension record_size_limit, send this if reduced. Our limit is always 16384 (+1).
		if {p.reduced_record_size} EXTENSION RECORD_SIZE_LIMIT(16385u16),
		//Send ALPN if needed.
		{do_server_alpn(p.alpn).map_err(handle_err)?},
		//If needed, send ServerCertificateType here.
		if {p.send_spki_only} EXTENSION SERVER_CERTIFICATE_TYPE(CertificateType::RAW_PUBLIC_KEY),
		//Extra extensions.
		extra_exts
	));
	tx_handshake3(debug.clone(), HandshakeType::ENCRYPTED_EXTENSIONS, content, base, hs_hash, state)
}
