//!This module contains definitions of feature flags.

use crate::stdlib::Display;
use crate::stdlib::String;
use crate::stdlib::FmtError;
use crate::stdlib::Formatter;
use btls_aux_aead::ProtectorType;
use btls_aux_dhf::KEM;
use btls_aux_dhf::KemEnabled;
use btls_aux_hash::HashFunctionEnabled;
use btls_aux_hash::HashType;
use btls_aux_memory::Attachment;
use btls_aux_memory::split_attach_first;
use btls_aux_memory::split_head_tail;
use btls_aux_set2::SetItem;
use btls_aux_set2::SetOf;
use btls_aux_signatures::SignatureAlgorithmEnabled2;
use btls_aux_signatures::SignatureType;
use btls_aux_signatures::SignatureTypeSecondary;
use btls_aux_tlsciphersuites::CiphersuiteEnabled2;

trait SetOps
{
	fn x_set_bits(&mut self, mask: Self);
	fn x_clear_bits(&mut self, mask: Self);
}

impl<T:SetItem> SetOps for SetOf<T>
{
	fn x_set_bits(&mut self, mask: Self) { *self |= mask; }
	fn x_clear_bits(&mut self, mask: Self) { *self -= mask; }
}

impl SetOps for u64
{
	fn x_set_bits(&mut self, mask: Self) { *self |= mask; }
	fn x_clear_bits(&mut self, mask: Self) { *self &= !mask; }
}

macro_rules! flag_by_name
{
	($selfx:ident, $name:ident, $op:tt, $ffn:ident) => {{
		let name = $name;
		if let Some(name) = name.strip_prefix("GROUP-") {
			if let Some(f) = SetOf::<KEM>::singleton_by_name(name) {
				$selfx.dhf.$op(f);
				return true;
			}
		}
		if let Some(name) = name.strip_prefix("HASH-") {
			if let Some(f) = SetOf::<HashType>::singleton_by_name(name) {
				$selfx.hash.$op(f);
				return true;
			}
		}
		if let Some(name) = name.strip_prefix("CIPHER-") {
			if let Some(f) = SetOf::<ProtectorType>::singleton_by_name(name) {
				$selfx.cipher.$op(f);
				return true;
			}
		}
		if let Some(name) = name.strip_prefix("SIGNATURE-") {
			if let Some(f) = SetOf::<SignatureType>::singleton_by_name(name) {
				$selfx.signature.$op(f);
				return true;
			}
		}
		if let Some(name) = name.strip_prefix("SIGNATUREEXT-") {
			if let Some(f) = SetOf::<SignatureTypeSecondary>::singleton_by_name(name) {
				$selfx.signature_extra.$op(f);
				return true;
			}
		}
		//Not one of the categories. Use legacy path.
		if let Some((page,mask)) = __lookup_flag(name) {
			$selfx.$ffn(page, mask);
			return true;
		}
	}}
}

macro_rules! flag_ops
{
	($selfx:ident, $op:ident, $page:ident, $mask:ident) => {
		match $page {
			FLAGPAGE_MAIN => $selfx.main.$op($mask),
			FLAGPAGE_DHF => $selfx.dhf.$op(SetOf::from_raw_mask_offset($mask, 0)),
			FLAGPAGE_HASH => $selfx.hash.$op(SetOf::from_raw_mask_offset($mask, 0)),
			FLAGPAGE_CIPHER => $selfx.cipher.$op(SetOf::from_raw_mask_offset($mask, 0)),
			FLAGPAGE_SIGNATURE => $selfx.signature.$op(SetOf::from_raw_mask_offset($mask, 0)),
			FLAGPAGE_TEST => $selfx.test.$op($mask),
			FLAGPAGE_SIGNATURE_EXTRA => $selfx.signature_extra.$op(SetOf::from_raw_mask_offset($mask, 0)),
			_ => ()
		}
	}
}

#[doc(hidden)]		//This should not be exposed.
#[derive(Copy,Clone)]
pub struct ConfigPages
{
	main: u64,
	dhf: SetOf<KEM>,
	hash: SetOf<HashType>,
	cipher: SetOf<ProtectorType>,
	signature: SetOf<SignatureType>,
	test: u64,
	signature_extra: SetOf<SignatureTypeSecondary>,
}

impl ConfigPages
{
	pub(crate) fn get_test(&self) -> u64 { self.test }
	pub(crate) fn get_flags(&self) -> u64 { self.main }
	pub(crate) fn is_flag(&self, mask: u64) -> bool { self.main & mask == mask }
	pub(crate) fn set_flags(&mut self, page: usize, mask: u64)
	{
		flag_ops!(self, x_set_bits, page, mask)
	}
	pub(crate) fn clear_flags(&mut self, page: usize, mask: u64)
	{
		flag_ops!(self, x_clear_bits, page, mask)
	}
	pub(crate) fn set_flag_by_name(&mut self, name: &str) -> bool
	{
		flag_by_name!(self, name, x_set_bits, set_flags);
		false	//Not known.
	}
	pub(crate) fn clear_flag_by_name(&mut self, name: &str) -> bool
	{
		flag_by_name!(self, name, x_clear_bits, clear_flags);
		false	//Not known.
	}
}

///Enable TLS 1.2 support.
///
///If set, TLS 1.2 support is enabled, otherwise TLS 1.2 is disabled. Supported for both client and server.
///
///This flag resides in page 0.
pub const FLAGS0_ENABLE_TLS12: u64 = 0x1;
#[doc(hidden)] pub const FLAGS0_ENABLE_TLS13: u64 = 0x2;	//Removed.
///Require EMS.
///
///If set, handshake attempts where the other side does not support either Extended Master Secret nor TLS 1.3
///will fail, otherwise such handshake attempts will be accepted (but exporters do not work). This flag should be
///set if using exporters. Supported for both client and server.
///
///This flag resides in page 0.
pub const FLAGS0_REQUIRE_EMS: u64 = 0x4;
///Require Certificate Transparency.
///
///If set, handshake attempts where the server does not deliver a valid Signed Certificate Timestamp for its
///certificate will fail, otherwise such attempts will be accepted. Only supported for client side.
///
///This flag resides in page 0.
pub const FLAGS0_REQUIRE_CT: u64 = 0x8;
///Don't send any shares.
///
///If set, the client omits all key shares for initial client hello, otherwise key shares are included. This should
///only be used for debugging purposes, it does not do anything useful. Only supported for client side.
///
///However, this does not affect groups specified via [`ClientConnection::hint_group()`], those are still included.
///
///This flag resides in page 0.
///
///[`ClientConnection::hint_group()`]: ../struct.ClientConnection.html#method.hint_group
pub const FLAGS0_DEBUG_NO_SHARES: u64 = 0x10;
///Assume hardware-accelerated AES.
///
///If set, assume hardware-accelerated AES is available, otherwise assume such support is absent. This should only
///be used for debugging purposes, otherwise it just degrades performance. Supported for both client and server.
///
///This flag resides in page 0.
pub const FLAGS0_ASSUME_HW_AES: u64 = 0x20;
///Require Online Certificate Status Protocol stapling.
///
///If set, attempts to handshake with servers that don't send OCSP staple will fail, otherwise such handshakes will
///be accepted. Only supported for client side.
///
///This flag resides in page 0.
pub const FLAGS0_REQUIRE_OCSP: u64 = 0x40;
///Allow bad cryptographic algorithms
///
///If set, server will reject client hellos containing bad cryptographic algorithm, otherwise such messges will be
///accepted. Only supported for server side.
///
///This flag resides in page 0.
pub const FLAGS0_ALLOW_BAD_CRYPTO: u64 = 0x80;
///Don't advertise ECDSA signature algorithms.
///
///If set, client hello does not contain any ECDSA-type signature algorithms, otherwise such will be included if
///ECDSA is enabled. This should only be used for testing purposes, as it does NOT affect what is actually enabled.
///Only supported for client side.
///
///This flag resides in page 0.
pub const FLAGS0_NO_ADVERTISE_ECDSA_SIG: u64 = 0x100;
///Don't advertise EdDSA signature algorithms.
///
///If set, client hello does not contain any EdDSA-type signature algorithms, otherwise such will be included if
///EdDSA is enabled. This should only be used for testing purposes, as it does NOT affect what is actually enabled.
///Only supported for client side.
///
///This flag resides in page 0.
pub const FLAGS0_NO_ADVERTISE_EDDSA_SIG: u64 = 0x200;
///Don't advertise RSA signature algorithms.
///
///If set, client hello does not contain any RSA-type signature algorithms, otherwise such will be included if
///RSA is enabled. This should only be used for testing purposes, as it does NOT affect what is actually enabled.
///Only supported for client side.
//
///This flag resides in page 0.
pub const FLAGS0_NO_ADVERTISE_RSA_SIG: u64 = 0x400;
///Don't advertise unknown type signature algorithms.
///
///If set, client hello does not contain any unknown-type signature algorithms, otherwise such will be included if
///enabled. This should only be used for testing purposes, as it does NOT affect what is actually enabled.
///Only supported for client side.
///
///This flag resides in page 0.
pub const FLAGS0_NO_ADVERTISE_UNKNOWN_SIG: u64 = 0x800;
///Don't perform spontaneous rekeys with TLS 1.3.
///
///If set, rekey messages are not spontaneously sent in TLS 1.3, otherwise rekey messages are spontaneously sent.
///This does not affect receiving or responding to rekey messages. Useful if the peer does not correctly implement
///rekeys. Supported for both client and server.
///
///This flag resides in page 0.
pub const FLAGS0_NO_SPONTANEOUS_REKEY: u64 = 0x1000;
///Unconditionally allow PKCS1v1.5 signatures.
///
///If set, RSA-PKCS1v1.5 signatures allowed even for TLS 1.3 handshake signature, otherwise RSA-PKCS1v1.5 signatures
///are rejected for such. Only supported for server side.
///
///This flag resides in page 0.
pub const FLAGS0_ALLOW_PKCS1_SIG: u64 = 0x2000;
///TLS 1.3 Middlebox compatibility mode.
///
///If set, TLS 1.3 middlebox compatibility mode is activated, otherwise compatibility mode is off. Only supported
///for client side.
///
///This flag resides in page 0.
pub const FLAGS0_MIDDLEBOX_COMPAT: u64 = 0x4000;
///Allow only non-RsaEncryption end-entity keys.
///
///If set, if peer sends a RsaEncryption end-entity keys, the handshake will fail, otherwise the handshake will
///proceed even with such key.  The term "floaty" is reference to DROWN. Supported for both client and server.
///
///This flag resides in page 0.
pub const FLAGS0_ONLY_FLOATY_KEYS: u64 = 0x8000;
///Allow private-use signatures.
///
///If set, peer sends enabled private-use signature codepoints to peer, and does not ignore any received
///private-use codepoints. Note that this does not affect if X.509 certificate is accepted or not. Supported for
///both client and server.
///
///This flag resides in page 0.
pub const FLAGS0_PRIVATE_USE_SIG: u64 = 0x10000;
///Only accept disclosed public uncostrained CAs.
///
///If this is set, only disclosed public CAs or technically constrained CAs are accepted. Ignored on server side.
///
///This flag resides in page 0.
pub const FLAGS0_KNOWN_CA_ONLY: u64 = 0x20000;
#[doc(hidden)] pub const FLAGS0_TLS13_DRAFTS: u64 = 0x40000;			//Removed.
///Allow dynamic RSA.
///
///If not set, all handshake RSA keys trigger immediate connection failure.
///
///This flag resides in page 0.
pub const FLAGS0_ALLOW_DYNAMIC_RSA: u64 = 0x100000;
///Allow RSA PKCS#1 client-certificate signatures.
///
///If set, dedicated TLS 1.3 client certificate RSA-PKCS1v1.5 signatures (0420, 0520 and 0620) are allowed.
///Only supported for server side.
///
///This flag resides in page 0.
pub const FLAGS0_ALLOW_PKCS1_CLIENT: u64 = 0x200000;
///Don't assume hardware-accelerated AES.
///
///If set, assume hardware-accelerated AES is not available (but if hardware acceleration is available, it is
///still used). Supported for both client and server.
///
///This flag resides in page 0.
pub const FLAGS0_NO_ASSUME_HW_AES: u64 = 0x400000;
///Allow private-use ciphersuites.
///
///If set, peer sends enabled private-use ciphersuite codepoints to peer, and does not ignore any received
///private-use codepoints. Supported for both client and server.
///
///This flag resides in page 0.
pub const FLAGS0_PRIVATE_USE_CS: u64 = 0x800000;
///Activate NSA mode.
pub const FLAGS0_NSA: u64 = 0x1000000;
///Restrict to post-quantum KEMs.
///
///If set, only post-quantum KEMs are negotiated. This obviously does not work if there are no supported post-quantum
///KEMs. Supported for both client and server.
pub const FLAGS0_POST_QUANTUM_KEM: u64 = 0x2000000;
///Send share for post-quantum KEM.
///
///If set, send post-quantum share, instead of ordinary share.
pub const FLAGS0_POST_QUANTUM_SHARE: u64 = 0x4000000;


//Handshake test flags. All of these just plain cause handshake to fail.
#[doc(hidden)] pub const FLAGS5_TEST_CH_SEND_FAIL: u64 = 0x1;		//Fail send ClientHello.
#[doc(hidden)] pub const FLAGS5_TEST_CH_RECV_FAIL: u64 = 0x2;		//Fail recv ClientHello.
#[doc(hidden)] pub const FLAGS5_TEST_SH_SEND_FAIL: u64 = 0x4;		//Fail send ServerHello.
#[doc(hidden)] pub const FLAGS5_TEST_SH_RECV_FAIL: u64 = 0x8;		//Fail recv ServerHello.
#[doc(hidden)] pub const FLAGS5_TEST_EE_SEND_FAIL: u64 = 0x10;		//Fail send EncryptedExtensions.
#[doc(hidden)] pub const FLAGS5_TEST_EE_RECV_FAIL: u64 = 0x20;		//Fail recv EncryptedExtensions.
#[doc(hidden)] pub const FLAGS5_TEST_CFIN_SEND_FAIL: u64 = 0x40;	//Fail send client finished.
#[doc(hidden)] pub const FLAGS5_TEST_CFIN_RECV_FAIL: u64 = 0x80;	//Fail recv client finished.
#[doc(hidden)] pub const FLAGS5_TEST_SFIN_RECV_FAIL: u64 = 0x100;	//Fail recv server finished.
#[doc(hidden)] pub const FLAGS5_FORCE_TLS12: u64 = 0x200;		//Force TLS 1.2

const FLAGPAGE_MAIN: usize = 0;
const FLAGPAGE_TEST: usize = 5;
///Page containing Diffie-Hellman Function mask constants (Dhf).
pub const FLAGPAGE_DHF: usize = 1;
///Page containing Hash Function mask constants (HashFunction).
pub const FLAGPAGE_HASH: usize = 2;
///Page containing Cipher algorithm mask constants (ProtectorType).
pub const FLAGPAGE_CIPHER: usize = 3;
///Page containing Signature algorithm mask constants (MajorSignatureAlgorithm).
pub const FLAGPAGE_SIGNATURE: usize = 4;
///Page containing secondary Signature algorithm mask constants.
pub const FLAGPAGE_SIGNATURE_EXTRA: usize = 6;


///Default flags for pages.
///
///Page 5 is various test flags and should always be kept as 0 (handshakes will fail otherwise).
pub const DEFAULT_FLAGS: ConfigPages = ConfigPages {
	main: FLAGS0_ENABLE_TLS12|FLAGS0_ALLOW_DYNAMIC_RSA,
	dhf: KEM::default_set(),
	hash: HashType::default_set(),
	cipher: ProtectorType::default_set(),
	signature: SignatureType::default_set(),
	test: 0,
	signature_extra: SignatureTypeSecondary::default_set(),
};

///Get algorithms enabled for given connection flags.
pub fn get_algs_enabled_f(flags: &ConfigPages) -> (KemEnabled, SignatureAlgorithmEnabled2, CiphersuiteEnabled2)
{
	//NSA mode overrides other settings.
	if flags.main & FLAGS0_NSA != 0 {
		let kems = KemEnabled::nsa_mode();
		let sigs = SignatureAlgorithmEnabled2::nsa_mode();
		let ciphers = CiphersuiteEnabled2::nsa_mode();
		return (kems, sigs, ciphers);
	}
	let pu_sig = flags.main & FLAGS0_PRIVATE_USE_SIG != 0;
	let pu_cs = flags.main & FLAGS0_PRIVATE_USE_CS != 0;
	let mut dhf_flags = flags.dhf;
	//If requested, restrict to post-quantum.
	if flags.main & FLAGS0_POST_QUANTUM_KEM != 0 { dhf_flags &= KEM::SET_POST_QUANTUM; }
	let kems = KemEnabled::from_set(dhf_flags);
	let sigs = SignatureAlgorithmEnabled2::from_set(flags.signature, flags.hash, flags.signature_extra, pu_sig);
	let ciphers = CiphersuiteEnabled2::from_set(flags.cipher, flags.hash, pu_cs);
	(kems, sigs, ciphers)
}

fn __lookup_flag(flagname: &str) -> Option<(usize, u64)>
{
	if flagname == ENABLE_TLS12_NAME { return Some((0, FLAGS0_ENABLE_TLS12)); }
	if flagname == REQUIRE_EMS_NAME { return Some((0, FLAGS0_REQUIRE_EMS)); }
	if flagname == REQUIRE_OCSP_NAME { return Some((0, FLAGS0_REQUIRE_OCSP)); }
	if flagname == REQUIRE_CT_NAME { return Some((0, FLAGS0_REQUIRE_CT)); }
	if flagname == DEBUG_NO_SHARES_NAME { return Some((0, FLAGS0_DEBUG_NO_SHARES)); }
	if flagname == ASSUME_HW_AES { return Some((0, FLAGS0_ASSUME_HW_AES)); }
	if flagname == NO_ASSUME_HW_AES { return Some((0, FLAGS0_NO_ASSUME_HW_AES)); }
	if flagname == ALLOW_BAD_CRYPTO_NAME { return Some((0, FLAGS0_ALLOW_BAD_CRYPTO)); }
	if flagname == NO_ADVERTISE_ECDSA_SIG { return Some((0, FLAGS0_NO_ADVERTISE_ECDSA_SIG)); }
	if flagname == NO_ADVERTISE_EDDSA_SIG { return Some((0, FLAGS0_NO_ADVERTISE_EDDSA_SIG)); }
	if flagname == NO_ADVERTISE_RSA_SIG { return Some((0, FLAGS0_NO_ADVERTISE_RSA_SIG)); }
	if flagname == NO_ADVERTISE_UNKNOWN_SIG { return Some((0, FLAGS0_NO_ADVERTISE_UNKNOWN_SIG)); }
	if flagname == NO_SPONTANEOUS_REKEY { return Some((0, FLAGS0_NO_SPONTANEOUS_REKEY)); }
	if flagname == PRIVATE_USE_SIG { return Some((0, FLAGS0_PRIVATE_USE_SIG)); }
	if flagname == PRIVATE_USE_CS { return Some((0, FLAGS0_PRIVATE_USE_CS)); }
	if flagname == ALLOW_PKCS1_SIG { return Some((0, FLAGS0_ALLOW_PKCS1_SIG)); }
	if flagname == ALLOW_PKCS1_CLIENT { return Some((0, FLAGS0_ALLOW_PKCS1_CLIENT)); }
	if flagname == ALLOW_DYNAMIC_RSA { return Some((0, FLAGS0_ALLOW_DYNAMIC_RSA)); }
	if flagname == MIDDLEBOX_COMPAT { return Some((0, FLAGS0_MIDDLEBOX_COMPAT)); }
	if flagname == ONLY_FLOATY_KEYS { return Some((0, FLAGS0_ONLY_FLOATY_KEYS)); }
	if flagname == KNOWN_CA_ONLY { return Some((0, FLAGS0_KNOWN_CA_ONLY)); }
	if flagname == NSA {return Some((0, FLAGS0_NSA)); }
	if flagname == POST_QUANTUM_KEM {return Some((0, FLAGS0_POST_QUANTUM_KEM)); }
	if flagname == POST_QUANTUM_SHARE {return Some((0, FLAGS0_POST_QUANTUM_SHARE)); }
	None
}

///This function looks up a flag by name `flagname`.
///
///If the flag exists, returns `Some(page, mask)`, where `page` is the page number and `mask` is the flag (the
///bitmask constant) in the page. Otherwise returns `None`.
///
///The following flags are currently known:
///
/// * tls12 -> FLAGS0_ENABLE_TLS12 (page 0)
/// * require-ems -> FLAGS0_REQUIRE_EMS (page 0)
/// * require-ocsp -> FLAGS0_REQUIRE_OCSP (page 0)
/// * require-ct -> FLAGS0_REQUIRE_CT (page 0)
/// * debug-no-shares -> FLAGS0_DEBUG_NO_SHARES (page 0)
/// * assume-hw-aes -> FLAGS0_ASSUME_HW_AES (page 0)
/// * no-assume-hw-aes -> FLAGS0_NO_ASSUME_HW_AES (page 0)
/// * allow-bad-crypto -> FLAGS0_ALLOW_BAD_CRYPTO (page 0)
/// * no-advertise-ecdsa-sig -> FLAGS0_NO_ADVERTISE_ECDSA_SIG (page 0)
/// * no-advertise-eddsa-sig -> FLAGS0_NO_ADVERTISE_EDDSA_SIG (page 0)
/// * no-advertise-rsa-sig -> FLAGS0_NO_ADVERTISE_RSA_SIG (page 0)
/// * no-advertise-unknown-sig -> FLAGS0_NO_ADVERTISE_UNKNOWN_SIG (page 0)
/// * no-spontaneous-rekey -> FLAGS0_NO_SPONTANEOUS_REKEY (page 0)
/// * private-use-sig -> FLAGS0_PRIVATE_USE_SIG (page 0)
/// * private-use-cs -> FLAGS0_PRIVATE_USE_CS (page 0)
/// * allow-pkcs1-sig -> FLAGS0_ALLOW_PKCS1_SIG (page 0)
/// * allow-pkcs1-client -> FLAGS0_ALLOW_PKCS1_CLIENT (page 0)
/// * allow-dynamic-rsa -> FLAGS0_ALLOW_DYNAMIC_RSA (page 0)
/// * middlebox-compat -> FLAGS0_MIDDLEBOX_COMPAT (page 0)
/// * only-floaty-keys -> FLAGS0_ONLY_FLOATY_KEYS (page 0)
/// * known-ca-only -> FLAGS0_KNOWN_CA_ONLY (page 0)
/// * nsa -> FLAGS0_NSA (page 0)
/// * post-quantum-kem -> FLAGS0_POST_QUANTUM_KEM (page 0)
/// * post-quantum-share -> FLAGS0_POST_QUANTUM_SHARE (page 0)
/// * GROUP-* -> Group flags (page 1)
/// * HASH-* -> Hash flags (page 2)
/// * CIPHER-* -> Cipher flags (page 3)
/// * SIGNATURE-* -> Signature flags (page 4).
/// * SIGNATUREEXT-* -> Signature extended flags (page 6).
pub fn lookup_flag(flagname: &str) -> Option<(usize, u64)>
{
	if let Some(t) = __lookup_flag(flagname) { return Some(t); }
	if let Some(flagname) = flagname.strip_prefix("GROUP-") {
		return KEM::bit_by_name(flagname).and_then(|b|{
			cycle_mask_bits(b, &[FLAGPAGE_DHF])
		});
	}
	if let Some(flagname) = flagname.strip_prefix("HASH-") {
		return HashFunctionEnabled::bit_by_name(flagname).and_then(|b|{
			cycle_mask_bits(b, &[FLAGPAGE_HASH])
		});
	}
	if let Some(flagname) = flagname.strip_prefix("CIPHER-") {
		return ProtectorType::by_internal_name(flagname).map(|p|p.algo_bit()).and_then(|b|{
			cycle_mask_bits(b, &[FLAGPAGE_CIPHER])
		});
	}
	if let Some(flagname) = flagname.strip_prefix("SIGNATURE-") {
		return SignatureAlgorithmEnabled2::alg_bit_by_name(flagname).and_then(|b|{
			cycle_mask_bits(b, &[FLAGPAGE_SIGNATURE])
		});
	}
	if let Some(flagname) = flagname.strip_prefix("SIGNATUREEXT-") {
		return SignatureAlgorithmEnabled2::secondary_bit_by_name(flagname).and_then(|b|{
			cycle_mask_bits(b, &[FLAGPAGE_SIGNATURE_EXTRA])
		});
	}
	None
}

fn cycle_mask_bits(b: u32, pages: &[usize]) -> Option<(usize, u64)>
{
	pages.get(b as usize / 64).map(|&p|(p, 1<<b%64))
}

///Error from setting flags from admin config.
///
///A human-readable error message can be printed using the `{}` operator (trait `Display`).
#[derive(Copy,Clone,Debug)]
pub struct ConfigFlagsError<'a>
{
	///The errorneous entry.
	pub entry: ConfigFlagsEntry<'a>,
	///The more precise error.
	pub kind: ConfigFlagsErrorKind<'a>,
}

impl<'a> Display for ConfigFlagsError<'a>
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		write!(fmt, "Entry#{entry}({str}): {kind}",
			entry=self.entry.entrynum.saturating_add(1), str=self.entry.entry_str, kind=self.kind)
	}
}

///Kind of error from setting flags from admin config.
///
///A human-readable error message can be printed using the `{}` operator (trait `Display`).
#[derive(Copy,Clone,Debug)]
pub enum ConfigFlagsErrorKind<'a>
{
	///Unrecognized setting.
	///
	///The setting name is not recognized. The argument is name of the offending setting.
	UnrecognizedSetting(&'a str),
	///Setting has no effect.
	///
	///The specified setting is recognized. However, setting it has no effect, e.g., because it has been
	///deprecated or the setting does not make sense for the endpoint. The argument is name of the offending
	///setting.
	NoEffect(&'a str),
	///Setting requires argument.
	///
	///The specified setting is recognized. However, it requires an argument, which was not present.
	///The argument is name of the offending setting.
	ArgumentRequired(&'a str),
	///Setting has no argument.
	///
	///The specified setting is recognized. However, it does not take an argument, while argument was specified.
	///The argument is name of the offending setting.
	NoArgument(&'a str),
	///Error setting the setting.
	///
	///The specified setting is recognized. However, when processing the setting, some unspecified error
	///occured. The argument contains a description of he problem.
	Error(&'a str),
}

impl<'a> Display for ConfigFlagsErrorKind<'a>
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::ConfigFlagsErrorKind::*;
		match self {
			&UnrecognizedSetting(name) => write!(fmt, "Unrecognized setting '{name}'"),
			&NoEffect(name) => write!(fmt, "Setting '{name}' to this value has no effect"),
			&ArgumentRequired(name) => write!(fmt, "Setting '{name}' requires an argument"),
			&NoArgument(name) => write!(fmt, "Setting '{name}' has no argument"),
			&Error(err) => write!(fmt, "Error processing: {err}"),
		}
	}
}

#[doc(hidden)] pub static ENABLE_TLS12_NAME: &'static str = "tls12";
#[doc(hidden)] pub static ENABLE_TLS13_NAME: &'static str = "tls13";
#[doc(hidden)] pub static REQUIRE_EMS_NAME: &'static str = "require-ems";
#[doc(hidden)] pub static REQUIRE_CT_NAME: &'static str = "require-ct";
#[doc(hidden)] pub static REQUIRE_OCSP_NAME: &'static str = "require-ocsp";
#[doc(hidden)] pub static DEBUG_NO_SHARES_NAME: &'static str = "debug-no-shares";
#[doc(hidden)] pub static ASSUME_HW_AES: &'static str = "assume-hw-aes";
#[doc(hidden)] pub static NO_ASSUME_HW_AES: &'static str = "no-assume-hw-aes";
#[doc(hidden)] pub static ALLOW_BAD_CRYPTO_NAME: &'static str = "allow-bad-crypto";
#[doc(hidden)] pub static NO_ADVERTISE_RSA_SIG: &'static str = "no-advertise-rsa-sig";
#[doc(hidden)] pub static NO_ADVERTISE_ECDSA_SIG: &'static str = "no-advertise-ecdsa-sig";
#[doc(hidden)] pub static NO_ADVERTISE_EDDSA_SIG: &'static str = "no-advertise-eddsa-sig";
#[doc(hidden)] pub static NO_ADVERTISE_UNKNOWN_SIG: &'static str = "no-advertise-unknown-sig";
#[doc(hidden)] pub static PRIVATE_USE_SIG: &'static str = "private-use-sig";
#[doc(hidden)] pub static PRIVATE_USE_CS: &'static str = "private-use-cs";
#[doc(hidden)] pub static NO_SPONTANEOUS_REKEY: &'static str = "no-spontaneous-rekey";
#[doc(hidden)] pub static ALLOW_PKCS1_SIG: &'static str = "allow-pkcs1-sig";
#[doc(hidden)] pub static ALLOW_PKCS1_CLIENT: &'static str = "allow-pkcs1-client";
#[doc(hidden)] pub static ALLOW_DYNAMIC_RSA: &'static str = "allow-dynamic-rsa";
#[doc(hidden)] pub static MIDDLEBOX_COMPAT: &'static str = "middlebox-compat";
#[doc(hidden)] pub static ONLY_FLOATY_KEYS: &'static str = "only-floaty-keys";
#[doc(hidden)] pub static KNOWN_CA_ONLY: &'static str = "known-ca-only";
#[doc(hidden)] pub static TLS13_DRAFTS: &'static str = "tls13-drafts";
#[doc(hidden)] pub static NSA: &'static str = "nsa";
#[doc(hidden)] pub static POST_QUANTUM_KEM: &'static str = "post-quantum-kem";
#[doc(hidden)] pub static POST_QUANTUM_SHARE: &'static str = "post-quantum-share";

#[doc(hidden)]
pub fn set_combine_flags_set(_page: usize, newmask: u64, oldmask: u64) -> u64 { oldmask | newmask }

#[doc(hidden)]
pub fn set_combine_flags_clear(_page: usize, newmask: u64, oldmask: u64) -> u64 { oldmask & !newmask }

#[doc(hidden)]
#[derive(Copy,Clone,Debug)]
//Value for config flag.
pub enum ConfigFlagsValue<'a>
{
	//Disabled.
	Disabled,
	//Enabled.
	Enabled,
	//Explicit
	Explicit(&'a str),
}

#[derive(Copy,Clone,Debug)]
///Entry information for configuration flag.
///
///This is used when reporting errors setting configuration via [`ConfigFlagsError`](struct.ConfigFlagsError.html).
pub struct ConfigFlagsEntry<'a>
{
	///Ordinal of the entry (starting from 0).
	pub entrynum: usize,
	///The whole entry.
	pub entry_str: &'a str,
}


#[doc(hidden)]
pub fn split_config_str<T, F>(objself: &mut T, config: &str, mut cb: F) where F: FnMut(&mut T, &str,
	ConfigFlagsValue, ConfigFlagsEntry)
{
	let _config = config.as_bytes();
	let mut itemstart = 0;
	let mut escape = false;
	let mut ordinal = 0;
	for idx in 0.._config.len() {
		if !escape {
			if *_config.get(idx).unwrap_or(&0) == 92 {
				//Backslash escape.
				escape = true;
			} else if *_config.get(idx).unwrap_or(&0) == 44 {
				//Split here. The slice can not fail.
				let item = config.get(itemstart..idx).unwrap_or("");
				call_with_config_item(objself, item, &mut cb, ordinal);
				ordinal = ordinal.wrapping_add(1);
				itemstart = idx.wrapping_add(1);
			} else {
				//Not special.
			}
		} else {
			escape = false;
		}
	}
	let item = config.get(itemstart..).unwrap_or("");
	if item != "" { call_with_config_item(objself, item, &mut cb, ordinal); }
}

#[doc(hidden)]
pub fn call_with_config_item<T, F>(objself: &mut T, item: &str, cb: &mut F, ordinal: usize)
	where F: FnMut(&mut T, &str, ConfigFlagsValue, ConfigFlagsEntry)
{
	let mut itemback = String::new();
	let aitem = if item.find('\\') != None {
		let mut escape = false;
		for i in item.chars() {
			if i == '\\' && !escape {
				escape = true;
			} else {
				itemback.push(i);
				escape = false;
			}
		}
		&itemback
	} else {
		item
	};
	let entryinfo = ConfigFlagsEntry{entrynum:ordinal, entry_str:aitem};
	if let Ok((name, value)) = split_attach_first(aitem, "=", Attachment::Center) {
		cb(objself, name, ConfigFlagsValue::Explicit(value), entryinfo);
	} else {
		//Boolean flag.
		let (fchar, aremain) = split_head_tail(aitem).unwrap_or(('\0', aitem));
		if fchar == '+'{
			cb(objself, aremain, ConfigFlagsValue::Enabled, entryinfo);
		} else if fchar == '-' || fchar == '!' {
			cb(objself, aremain, ConfigFlagsValue::Disabled, entryinfo);
		} else {
			cb(objself, aitem, ConfigFlagsValue::Enabled, entryinfo);
		}
	}
}
