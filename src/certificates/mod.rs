//!Definitions related to certificates.
//!
//!This module contains the various types related to certificates store. This includes:
//!
//! * The base trait for certificate selection: [`CertificateLookup`] and The base trait for key/certificate
//!stores [`KeyStoreOperations`]
//!   * Implemented by Store of signable certificates loaded from files: [`LocalServerCertificateStore`].
//!   * Implemented by Store of signable certificates loaded from a directory (with automatic managment):
//![`CertificateDirectory`].
//! * The base trait for server keys: [`KeyPair`].
//!   * Implemented by Key pairs loaded from files, or file-like things: [`LocalKeyPair`].
//! * The base trait for signable certificate: [`ServerCertificate`].
//!   * Usually implemented internally by [`LocalServerCertificateStore`] or [`CertificateDirectory`]
//! * PGP wordlist: [`PGP_WORDLIST`].
//! * Representing data as either the data itself, or hash thereof: [`DataRepresentation`].
//! * A HSTS/DANE pin/TA (and similar things): [`HostSpecificPin`].
//! * A PKIX trust anchor: [`TrustAnchor`].
//! * Trusted Certificate Transparency log: [`TrustedLog`].
//!
//!Additionally, many types associated with these are provoded. E.g., the types related to futures.
//!
//![`CertificateLookup`]: struct.CertificateLookup.html
//![`KeyStoreOperations`]: trait.KeyStoreOperations.html
//![`LocalServerCertificateStore`]: struct.LocalServerCertificateStore.html
//![`CertificateDirectory`]: struct.CertificateDirectory.html
//![`KeyPair`]: trait.KeyPair.html
//![`LocalKeyPair`]: struct.LocalKeyPair.html
//![`ServerCertificate`]: trait.ServerCertificate.html
//![`PGP_WORDLIST`]: static.PGP_WORDLIST.html
//![`DataRepresentation`]: enum.DataRepresentation.html
//![`HostSpecificPin`]: struct.HostSpecificPin.html
//![`TrustAnchor`]: struct.TrustAnchor.html
//![`TrustedLog`]: struct.TrustedLog.html
pub use self::local::CertificateLookup;
pub use self::local::CertificateNames;
pub use self::local::LocalServerCertificateHandle;
pub use self::local::LocalServerCertificateStore;
pub use self::directory::CertificateDirectory;
pub use crate::pgpwords::PGP_WORDLIST;
use crate::stdlib::AsRef;
use crate::stdlib::Debug;
use crate::stdlib::FmtError;
use crate::stdlib::Formatter;
use crate::stdlib::IoRead;
use crate::stdlib::String;
use crate::stdlib::ToOwned;
use crate::stdlib::Vec;
use crate::system::File;
use crate::system::Path;
pub use btls_aux_certificate_transparency::LogHashFunction;
pub use btls_aux_certificate_transparency::TrustedLogInfo2;
use btls_aux_collections::ToString;
use btls_aux_filename::Filename;
pub use btls_aux_futures::FutureMapper;
pub use btls_aux_futures::FutureMapperFn;
pub use btls_aux_futures::FutureReceiver;
pub use btls_aux_futures::FutureSender;
pub use btls_aux_futures::FutureSink;
pub use btls_aux_futures::create_future;
pub use btls_aux_ip::IpAddress;
pub use btls_aux_ip::IpAddressBlock;
pub use btls_aux_keypair_local::KeyLoadingError;
pub use btls_aux_keypair_local::KeyReadError;
pub use btls_aux_keypair_local::LocalKeyPair;
pub use btls_aux_keypair_local::ModuleError;
pub use btls_aux_random::TemporaryRandomLifetimeTag;
pub use btls_aux_random::TemporaryRandomStream;
use btls_aux_set2::SetOf;
pub use btls_aux_signatures::CsrParams;
pub use btls_aux_signatures::Hasher;
pub use btls_aux_signatures::KeyPair2;
pub use btls_aux_signatures::SignatureAlgorithm2;
pub use btls_aux_signatures::SignatureAlgorithmTls2;
#[allow(deprecated)] pub use btls_aux_signatures::SignatureAlgorithmSet2;
pub use btls_aux_signatures::SignatureFormat;
pub use btls_aux_signatures::SubjectPublicKeyInfo;
pub use btls_aux_signatures::SupportedSchemes;
use btls_aux_serialization::Asn1Tag;
use btls_aux_serialization::Sink;
use btls_aux_x509certparse::ParsedCertificate2;
pub use btls_aux_x509certvalidation::DataRepresentation;
pub use btls_aux_x509certvalidation::HostSpecificPin;
pub use btls_aux_x509certvalidation::NameConstraint;
pub use btls_aux_x509certvalidation::TrustAnchor as TrustAnchorInner;
pub use btls_aux_x509certvalidation::TrustAnchorOrdered;


mod local;
mod directory;

pub(crate) mod restricted
{
	use crate::stdlib::Arc;
	use crate::stdlib::Vec;
	use btls_aux_ip::IpAddressBlock;
	use btls_aux_time::Timestamp;
	#[derive(Clone)]
	pub struct FastInterlock
	{
		///DNS names this certificate is valid for.
		pub(crate) names: Arc<Vec<Vec<u8>>>,
		///Addresses this certificate is valid for.
		pub(crate) addresses: Arc<Vec<IpAddressBlock>>,
		///Expiry time of certificate.
		pub(crate) c_expiry: Timestamp,
		///Expiry time of OCSP. Meaningless if there is no OCSP.
		pub(crate) o_expiry: Timestamp,
	}
}

#[doc(hidden)]		//Debugging function.
pub fn sign_certificate(tbs: &[u8], key: &dyn KeyPair2, alg_id: SignatureAlgorithm2) -> Result<Vec<u8>, ()>
{
	//Just grab the first acceptable signing algorithm.
	let oid = dtry!(alg_id.get_oid());
	//Grab the signature.
	let lifetime = TemporaryRandomLifetimeTag;
	let fsig = key.sign_data(tbs, alg_id, &mut TemporaryRandomStream::new_system(&lifetime));
	//Drop the signature format we do not need.
	let fsig = fsig.map(FutureMapperFn::new(|x: Result<(SignatureFormat, Vec<u8>), ()>|Ok(x?.1)));
	sign_certificate_tail(tbs, fsig, oid)
}

fn sign_certificate_tail(tbs: &[u8], fsig: FutureReceiver<Result<Vec<u8>,()>>, oid: &[u8]) -> Result<Vec<u8>, ()>
{
	while !fsig.settled() {};	//Wait until settled.
	let sig = dtry!(fsig.read())?;
	//Serialize the result. It is SEQUENCE of three components: The TBS (which is assumed to have ASN.1
	//header), the algorithm and then finally the signature as BIT STRING (0 bits stripped).
	let mut vec = Vec::new();
	vec.asn1_fn(Asn1Tag::SEQUENCE, |x|{
		x.write_slice(tbs)?;
		x.asn1_fn(Asn1Tag::SEQUENCE, |y|y.write_slice(oid))?;
		x.asn1_fn(Asn1Tag::BIT_STRING, |y|{
			y.write_u8(0)?;			//No bits to discard: Octet aligned.
			y.write_slice(&sig)?;
			Ok(())
		})?;
		Ok(())
	})?;
	Ok(vec)
}

///A client certificate.
pub trait ClientCertificate
{
	///Sign a message `data`.
	///
	///The transient random number stream to be used for signing is `rng`.
	///
	///Because signing can involve contacting an external signer, the return value is a future
	///and not a direct value. If the future has not resolved, the handshake will be paused
	///until it is.
	///
	///On success, the returned future shall resolve to `Ok((scheme, sig))`, where `scheme` is the TLS
	///SignatureScheme value and `sig` is the raw signature (in form it goes to TLS signature field. On error,
	///the future shall resolve to `Err(())`.
	fn sign(&self, data: &mut dyn FnMut(&mut Hasher), rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<(u16, Vec<u8>), ()>>;
	///Get the certificate.
	fn certificate<'a>(&'a self) -> &'a [u8];
	///Get the certificate issuer chain.
	fn chain<'a>(&'a self) -> &'a [Vec<u8>];
	///Get the OCSP staple, if any.
	fn ocsp<'a>(&'a self) -> Option<&'a [u8]> { None }
	///Get the SCT staples, if any.
	fn scts<'a>(&'a self) -> &'a [Vec<u8>] { &[] }
	///Get the transparency items, if any.
	fn trans_items<'a>(&'a self) -> &'a [Vec<u8>] { &[] }
}

pub(crate) enum ServerCertificate
{
	Acme(crate::acme::AcmeCertificate),
	Master(local::SelectedCertificate),
	Hostkey(local::ServerHostKeyAlg),
}

impl ServerCertificate
{
	pub(crate) fn sign(&self, data: &mut dyn FnMut(&mut Hasher), rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<(u16, Vec<u8>), ()>>
	{
		use self::ServerCertificate::*;
		match self {
			&Acme(ref x) => x.sign(data, rng),
			&Master(ref x) => x.sign(data, rng),
			&Hostkey(ref x) => x.sign(data, rng),
		}
	}
	pub(crate) fn is_ecdsa(&self) -> bool
	{
		match self {
			&ServerCertificate::Master(ref x) => x.is_ecdsa(),
			//Assume the remaining are either ECDSA keys or used with TLS 1.3, where it does not
			//matter.
			_ => true
		}
	}
	pub(crate) fn hack_root_isrg_x1(&self) -> bool
	{
		match self {
			&ServerCertificate::Master(ref x) => x.hack_root_isrg_x1(),
			_ => false	//The remaining are always self-signed.
		}
	}
	pub(crate) fn certificate<'a>(&'a self) -> &'a [u8]
	{
		use self::ServerCertificate::*;
		match self {
			&Acme(ref x) => x.certificate(),
			&Master(ref x) => x.certificate(),
			&Hostkey(ref x) => x.certificate(),
		}
	}
	pub(crate) fn chain<'a>(&'a self) -> &'a [Vec<u8>]
	{
		match self {
			&ServerCertificate::Master(ref x) => x.chain(),
			_ => &[]	//The remaining are always self-signed.
		}
	}
	pub(crate) fn ocsp<'a>(&'a self) -> Option<&'a [u8]>
	{
		match self {
			&ServerCertificate::Master(ref x) => x.ocsp(),
			_ => None	//The remaining do not have OCSP.
		}
	}
	pub(crate) fn scts<'a>(&'a self) -> &'a [Vec<u8>]
	{
		match self {
			&ServerCertificate::Master(ref x) => x.scts(),
			_ => &[]	//The remaining do not have CT.
		}
	}
	pub(crate) fn trans_items<'a>(&'a self) -> &'a [Vec<u8>]
	{
		match self {
			&ServerCertificate::Master(ref x) => x.trans_items(),
			_ => &[]	//The remaining do not have Transparency Items.
		}
	}
	pub(crate) fn fast_interlock(&self) -> Option<&restricted::FastInterlock>
	{
		match self {
			&ServerCertificate::Master(ref x) => Some(x.fast_interlock()),
			_ => None,	//The remaining always fail interlocks.
		}
	}
}

#[derive(Debug)]
pub(crate) struct CertificateLookupCriteria<'a>
{
	pub sni: Option<&'a str>,
	pub sig_flags2: SetOf<SignatureAlgorithm2>,
	pub ee_flags2: SetOf<SignatureAlgorithm2>,
	pub tls13: bool,
	pub selfsigned_ok: bool,
	pub nsa: bool,
}

///Key storage operations.
///
///In practicular, this trait provodes operations on the private keys in the key storage.
///
///This trait is implemented by the key/certificate stores [`LocalServerCertificateStore`] and
///[`CertificateDirectory`].
///
///[`LocalServerCertificateStore`]: struct.LocalServerCertificateStore.html
///[`CertificateDirectory`]: struct.CertificateDirectory.html
pub trait KeyStoreOperations
{
	///Enumerate the names of available private keys, together with their types
	///
	///In the returned pairs, the first element is name of the key, and the second element is the type of the
	///key.
	fn enumerate_private_keys(&self) -> Vec<(String, String)>;
	///Export the public key for the specified private key `privkey`.
	///
	///The private key is specified by the name.
	///
	///If specified private key is found in the container, returns `Some(spki)`, where `spki` is the SPKI
	///encoding of the public key. Otherwise returns `None`.
	fn get_public_key(&self, privkey: &str) -> Option<Vec<u8>>;
	///Sign a CSR with parameters `csr_params` and private key `privkey`.
	///
	///The private key is specified by the name. Since the signing may not complete immediately, a future is
	///returned.
	///
	///If the signing is successful, the future will resolve to `Ok(csr)`, where `csr` is the signed CSR.
	///Otherwise it will resolve to `Err(())`.
	fn sign_csr(&self, privkey: &str, csr_params: &CsrParams) -> FutureReceiver<Result<Vec<u8>, ()>>;
}

///Extended error information for certificate lookup.
pub enum CertificateLookupError
{
	///Host not found.
	NoSuchHost,
	///No match.
	NoMatch,
	///Internal error.
	InternalError(&'static str),
}


///A trusted Certificate Transparency log.
///
///This structure implements the trait [`TrustedLogInfo`](trait.TrustedLogInfo.html).
///
///It also has `From` conversion from [`&TrustedLogInfo`](trait.TrustedLogInfo.html).
#[derive(Clone)]
pub struct TrustedLog
{
	///Name of the log.
	pub name: String,
	///The public key of the Log (X.509 SPKI format).
	pub key: Vec<u8>,
	///Time log expired.
	///
	///If the log has not expired, `None`. Otherwise `Some(expiry_time)`, where `expiry_time` is the expiry time.
	///
	///Expired logs are still trusted trusted to issue precertificates up to given bound. However, these logs
	///are not trusted to issue OCSP staples or dedicated SCT staples, regardless of when those were purported
	///to have been issued.
	pub expiry: Option<i64>,
}

impl TrustedLogInfo2 for TrustedLog
{
	fn get_name<'a>(&'a self) -> &'a str { &self.name }
	fn get_expiry(&self) -> Option<i64> { self.expiry }
	fn get_key<'a>(&'a self) -> &'a [u8] { &self.key }
}

impl<'a> From<&'a dyn TrustedLogInfo2> for TrustedLog
{
	fn from(x: &'a dyn TrustedLogInfo2) -> TrustedLog
	{
		TrustedLog {
			name: x.get_name().to_owned(),
			key: x.get_key().to_owned(),
			expiry: x.get_expiry(),
		}
	}
}


///A trust anchor.
///
///This structure represents a trust anchor to evaluate trust against.
///
///There are four basic ways of creating this structure:
///
/// * Pass the subject (as X.509 Name) and public key (as X.509 SubjectPublicKeyInfo), along with optional
///name constraints to [`TrustAnchor::from_name_spki()`](#method.from_name_spki)
/// * Pass certificate data in DER form to [`TrustAnchor::from_certificate()`](#method.from_certificate)
/// * Pass filename of file containg the certificate data in DER form to
///[`TrustAnchor::from_certificate_file()`](#method.from_certificate_file).
/// * Pass `TrustAnchorOrdered` to [`TrustAnchor::from_ordered()`](#method.from_ordered).
#[derive(Clone)]
pub struct TrustAnchor(TrustAnchorInner);

impl Debug for TrustAnchor
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> { Debug::fmt(&self.0, f) }
}

#[derive(Clone)]
struct ListAdapter<'a,'b:'a>(&'a [&'b str], usize);

impl<'a,'b:'a> Iterator for ListAdapter<'a, 'b>
{
	type Item=&'b [u8];
	fn next(&mut self) -> Option<&'b [u8]> {
		let item = self.0.get(self.1).map(|x|x.as_bytes());
		if item.is_some() { self.1 += 1; }	//Advance if Ok.
		item
	}
}

impl TrustAnchor
{
	///Create a new turst anchor from the specified subject `subject`, SPKI `spki` and name constriaints
	///`allowed` and `disallowed`.
	///
	///The subject is a X.509 Name structure, including the leading ASN.1 SEQUENCE header. The SPKI is X.509
	///SubjectPublicKeyInfo structure, including the leading ASN.1 SEQUENCE header. The `allow` and
	///`disallow` are lists of name constraints. If names allowed is empty list, all names not disallowed are
	///allowed. If a name is on both allow and disallow lists disallow takes percedence.
	///
	///Note that name constraints are evaluated per type and DNS names, IPv4 addresses and IPv6 addresses are
	///all considered separate. So if, e.g., IP address certificates are not desired, one has to add a name
	///constraint on both IPv4 and IPv6 space separatedly.
	///
	///On success, returns `Ok(anchor)`, where `anchor` is the new trust anchor. Otherwise returns `Err(err)`,
	///where `err` describes the error.
	pub fn from_name_spki(subject: &[u8], spki: &[u8], allow: &[NameConstraint], disallow: &[NameConstraint]) ->
		Result<TrustAnchor, String>
	{
		Ok(TrustAnchor(TrustAnchorInner::from_name_spki2(subject, spki, allow, disallow)?))
	}
	///Creates a new turst anchor from the specified DER-formated certificate, given as object `certificate`
	///implementing the `Read` trait.
	///
	///The subject name, SPKI and constraints from the certificate are considered in constructing the root.
	///
	///On success, returns `Ok(anchor)`, where `anchor` is the new trust anchor. Otherwise returns `Err(err)`,
	///where `err` describes the error.
	///
	///`&[u8]` implements `Read`. However, note that the method takes in a mutable reference to object
	///implementing `Read`. Therefore you need to pass in a `&mut &[u8]` in order to read from a buffer.
	pub fn from_certificate<R:IoRead>(certificate: &mut R) -> Result<TrustAnchor, String>
	{
		let mut data = Vec::new();
		certificate.read_to_end(&mut data).map_err(|x|x.to_string())?;
		let ta = ParsedCertificate2::from(&data[..]).map_err(|x|x.to_string())?;
		Ok(TrustAnchor(TrustAnchorInner::from_certificate2(&ta)?))
	}
	///Creates a new turst anchor from the specified DER-formated certificate, given file named `certificate`.
	///
	///This is esssentially the same as [`from_certificate()`](#method.from_certificate), but also opens the
	///named file before passing it to [`from_certificate()`](#method.from_certificate).
	///
	///On success, returns `Ok(anchor)`, where `anchor` is the new trust anchor. Otherwise returns `Err(err)`,
	///where `err` describes the error.
	pub fn from_certificate_file<P:AsRef<Path>>(certificate: P) -> Result<TrustAnchor, String>
	{
		let certificate = certificate.as_ref();
		let mut _certificate = File::open(certificate).map_err(|err|{
			let certd = Filename::from_path(certificate);
			format!("Can't open trust anchor `{certd}`: {err}")
		})?;
		Self::from_certificate(&mut _certificate)
	}
	///Get ordered trust anchor for this trust anchor.
	///
	///The ordered trust anchors can be placed into map structure key, which is useful when needing to asscoiate
	///attributes with trust anchor.
	pub fn as_ordered(&self) -> TrustAnchorOrdered { TrustAnchorOrdered::new(self.0.clone()) }
	///Reverse of [`as_ordered()`](#method.as_ordered)
	pub fn from_ordered(ta: &TrustAnchorOrdered) -> TrustAnchor
	{
		TrustAnchor(ta.borrow_inner().clone())
	}
}

static ISRG_X1_DST_HACK_CERT: &'static [u8] = include_bytes!("irsg-x1-dst-hack.crt");

pub(crate) fn get_isrg_x1_dst_hack_certificate() -> &'static [u8] { ISRG_X1_DST_HACK_CERT }
pub(crate) fn get_irsg_x1_subject() -> &'static [u8] { &ISRG_X1_DST_HACK_CERT[143..224] }

///Helper for signing TLS exchange.
pub fn tls_sign_helper(key: &dyn KeyPair2, data: &mut dyn FnMut(&mut Hasher), algorithm: SignatureAlgorithmTls2,
	rng: &mut TemporaryRandomStream) -> FutureReceiver<Result<(u16, Vec<u8>), ()>>
{
	let algo = algorithm.tls_id();
	let sig = key.sign_callback(algorithm.to_generic(), rng, data);
	sig.map(FutureMapperFn::new(move |val: Result<(SignatureFormat, Vec<u8>), ()>|{
		Ok((algo, val?.1))
	}))
}
