//#![allow(unused_variables)]
//#![allow(dead_code)]
use super::CertificateLookupCriteria;
use super::KeyStoreOperations;
use crate::certificates::CertificateLookupError as CLErr;
use crate::certificates::ServerCertificate;
use crate::certificates::TrustedLog;
use crate::certificates::restricted::FastInterlock;
use crate::stdlib::Arc;
use crate::stdlib::AsRef;
use crate::stdlib::AtomicOrdering;
use crate::stdlib::Box;
use crate::stdlib::BTreeMap;
use crate::stdlib::Cow;
use crate::stdlib::Deref;
use crate::stdlib::Display;
use crate::stdlib::fence;
use crate::stdlib::FmtError;
use crate::stdlib::Formatter;
use crate::stdlib::Hash;
use crate::stdlib::Hasher;
use crate::stdlib::IoRead;
use crate::stdlib::max;
use crate::stdlib::min;
use crate::stdlib::Range;
use crate::stdlib::RwLock;
use crate::stdlib::String;
use crate::stdlib::ToOwned;
use crate::stdlib::ToString;
use crate::stdlib::Vec;
use crate::system::File;
use crate::system::Path;
use btls_aux_assert::AssertFailed;
use btls_aux_certificate_transparency::ParsedSctV1Raw2;
use btls_aux_fail::ResultExt;
use btls_aux_filename::Filename;
use btls_aux_futures::FutureMapperFn;
use btls_aux_futures::FutureReceiver;
use btls_aux_hash::sha256;
use btls_aux_ip::IpAddressBlock;
use btls_aux_memory::subslice_to_range;
use btls_aux_ocsp::extract_scts_from_ocsp;
use btls_aux_ocsp::OcspDumpedScts;
use btls_aux_ocsp::OcspValidationError;
use btls_aux_ocsp::sanity_check_ocsp3;
use btls_aux_publiccalist::is_public_ca;
use btls_aux_random::TemporaryRandomLifetimeTag;
use btls_aux_random::TemporaryRandomStream;
use btls_aux_serialization::Asn1Tag;
use btls_aux_serialization::Source;
use btls_aux_set2::SetOf;
use btls_aux_set2::SetItem;
use btls_aux_signatures::CsrParams;
use btls_aux_signatures::Hasher as SigHasher;
use btls_aux_signatures::iterate_pem_or_derseq_fragments;
use btls_aux_signatures::KeyPair2;
use btls_aux_signatures::PemDerseqFragment2 as PFragment;
use btls_aux_signatures::PemFragmentKind as PFKind;
use btls_aux_signatures::SignatureAlgorithm2;
use btls_aux_signatures::SignatureAlgorithmEnabled2;
use btls_aux_signatures::SignatureAlgorithmTls2;
use btls_aux_signatures::SignatureFormat;
use btls_aux_signatures::Tls12SignatureType2;
use btls_aux_time::Timestamp;
use btls_aux_x509certparse::CertificateError;
use btls_aux_x509certparse::CertificateIssuer;
use btls_aux_x509certparse::extract_spki;
use btls_aux_x509certparse::ParsedCertificate2;
use btls_aux_x509certparse::PublicKeyAlgorithm;
use btls_aux_x509certvalidation::CertificateValidationError;
use btls_aux_x509certvalidation::sanity_check_certificate_chain_load2;


///Error in loading a certificate.
///
///This can be formatted as human-readable error by using the `{}` format modifier (trait `Display`).
#[derive(Clone,Debug,PartialEq,Eq)]
#[non_exhaustive]
pub enum CertificateLoadError
{
	//Assertion failed.
	AssertionFailed(AssertFailed),

	///Certificate chain empty
	CertificateChainEmpty,
	///Certificate is missing valid header.
	ValidHeaderMissing(usize),
	///Certificate has junk after end.
	JunkAfterEndOfCert(usize),
	///Error parsing certificate.
	CertParseError(usize, CertificateError),
	///No corresponding private key available.
	NoPrivateKeyAvailable,
	///The private key does not support any known algorithm.
	NoKnownAlgorithms,
	///Certificate expired.
	CertificateExpired,
	///Error sanity-checking certificate chain.
	SanityCheckFailed(CertificateValidationError),
	///Error sanity-checking OCSP.
	OcspSanityCheckFailed(OcspValidationError),
	///Error reading certificate.
	ReadError(String),
	///Certificate handle overflow.
	HandleOverflowed,
	///Effective certificate lifetime is zero.
	NoEffectiveCertificateLifetime,
}

impl From<AssertFailed> for CertificateLoadError
{
	fn from(x: AssertFailed) -> CertificateLoadError { CertificateLoadError::AssertionFailed(x) }
}

impl Display for CertificateLoadError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::CertificateLoadError::*;
		match self {
			&AssertionFailed(ref err) => write!(fmt, "Assertion failed: {err}"),
			&CertificateChainEmpty => fmt.write_str("Certificate chain is empty!"),
			&ValidHeaderMissing(idx) if idx == 0 => fmt.write_str("EE Certificate missing valid header"),
			&ValidHeaderMissing(idx) => write!(fmt, "CA Certificate #{idx} missing valid header"),
			&JunkAfterEndOfCert(idx) if idx == 0 => fmt.write_str("EE Certificate has junk after end"),
			&JunkAfterEndOfCert(idx) => write!(fmt, "CA Certificate #{idx} has junk after end"),
			&CertParseError(idx, ref err) if idx == 0 =>
				write!(fmt, "EE Certificate does not parse: {err}"),
			&CertParseError(idx, ref err) => write!(fmt, "CA Certificate #{idx} does not parse: {err}"),
			&NoPrivateKeyAvailable => fmt.write_str("No corresponding private key available"),
			&NoKnownAlgorithms => fmt.write_str("Private key does not support any known algorithms"),
			&CertificateExpired => fmt.write_str("Certificate has expired"),
			&NoEffectiveCertificateLifetime => fmt.write_str("Effective certificate lifetime is zero"),
			&SanityCheckFailed(ref err) => write!(fmt, "Certificate chain sanity check failed: {err}"),
			&OcspSanityCheckFailed(ref err) => write!(fmt, "OCSP response sanity check failed: {err}"),
			&HandleOverflowed => fmt.write_str("Certificate handle overflowed"),
			&ReadError(ref err) => write!(fmt, "Read error: {err}"),
		}
	}
}

//An internal function.
//A certificate that has been selected for signing.
#[derive(Clone)]
pub(crate) struct SelectedCertificate
{
	//The keypair that does the signing.
	keypair: Arc<Box<dyn KeyPair2+Send+Sync>>,
	//The signature algorithm to use.
	sig_algorithm: SignatureAlgorithmTls2,
	//Is ECDSA-type algorithm (according to TLS 1.2 rules)?
	ecdsa_type: bool,
	//The certificate to send.
	certificate: Arc<Vec<u8>>,
	//The certificate chain to send.
	chain: Arc<Vec<Vec<u8>>>,
	//The OCSP staple to send.
	ocsp_staple: Option<Arc<Vec<u8>>>,
	//The SCT list to send.
	sct_list: Arc<Vec<Vec<u8>>>,
	//The Transparency Item list to send.
	trans_list: Arc<Vec<Vec<u8>>>,
	//Root is ISRG X1 hack flag.
	root_is_isrg_x1: bool,
	//Fast interlock info.
	fast_interlock: FastInterlock,
}

impl SelectedCertificate
{
	pub(super) fn sign(&self, data: &mut dyn FnMut(&mut SigHasher), rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<(u16, Vec<u8>), ()>>
	{
		//The KeyPair2 sign method returns future for Vec<u8>, but this function needs to return future for
		//(u16, Vec<u8>). Therefore we need an adapter to add the algorithm.
		let algo = self.sig_algorithm;
		//Start the signing process.
		let sig = self.keypair.sign_callback(algo.to_generic(), rng, data);
		sig.map(FutureMapperFn::new(move |val: Result<(SignatureFormat, Vec<u8>), ()>|{
			Ok((algo.tls_id(), val?.1))
		}))
	}
	pub(super) fn is_ecdsa(&self) -> bool { self.ecdsa_type }
	pub(super) fn hack_root_isrg_x1(&self) -> bool { self.root_is_isrg_x1 }
	pub(super) fn certificate<'a>(&'a self) -> &'a [u8] { self.certificate.deref().deref() }
	pub(super) fn chain<'a>(&'a self) -> &'a [Vec<u8>] { self.chain.deref().deref() }
	pub(super) fn ocsp<'a>(&'a self) -> Option<&'a [u8]> { self.ocsp_staple.as_ref().map(|x|x.deref().deref()) }
	pub(super) fn scts<'a>(&'a self) -> &'a [Vec<u8>] { self.sct_list.deref().deref() }
	pub(super) fn trans_items<'a>(&'a self) -> &'a [Vec<u8>] { self.trans_list.deref().deref() }
	pub(super) fn fast_interlock(&self) -> &FastInterlock { &self.fast_interlock }
}

//A keyring of keys.
#[derive(Clone)]
struct Keyring
{
	map: BTreeMap<[u8;32], Arc<Box<dyn KeyPair2+Send+Sync>>>,
	names: BTreeMap<String, [u8;32]>
}

impl Keyring
{
	//Create a new keyring.
	fn new() -> Keyring { Keyring{map:BTreeMap::new(), names:BTreeMap::new()} }
	//Add a new key to the keyring.
	fn add(&mut self, key: Arc<Box<dyn KeyPair2+Send+Sync>>, name: &str)
	{
		let chksum = sha256(key.get_public_key2());
		self.map.insert(chksum, key);
		self.names.insert(name.to_owned(), chksum);
	}
	//Look up a key from keyring, using the public key as lookup key.
	fn lookup(&self, spki: &[u8]) -> Option<Arc<Box<dyn KeyPair2+Send+Sync>>>
	{
		let chksum = sha256(spki);
		self.map.get(&chksum).map(Clone::clone)
	}
	fn enumerate_private_keys(&self) -> Vec<(String, String)>
	{
		let mut list: Vec<(String, String)> = Vec::new();
		//Join map to names. Just ignore the case where name entry points to nonexistent keyid, as it
		//should not happen.
		for (name, keyid) in self.names.iter() { if let Some(x) = self.map.get(keyid) {
			list.push((name.to_owned(), x.get_key_type2().to_owned()));
		}}
		list
	}
	fn get_public_key<'a>(&'a self, privkey: &str) -> Option<&'a [u8]>
	{
		let checksum = self.names.get(privkey)?;
		self.map.get(checksum).map(|x|x.get_public_key2())
	}
	fn sign_csr(&self, privkey: &str, csr_params: &CsrParams) -> FutureReceiver<Result<Vec<u8>, ()>>
	{
		let err = FutureReceiver::from(Err(()));
		//Look up the key.
		let checksum = f_return!(self.names.get(privkey), err);
		let key = f_return!(self.map.get(checksum), err);
		//Actual sign with key.
		let lifetime = TemporaryRandomLifetimeTag;
		key.sign_csr2(csr_params, &mut TemporaryRandomStream::new_system(&lifetime))
	}
}

fn handle_external_sct<'a>(i: &'a [u8], builtin: &mut bool)
{
	*builtin |= ParsedSctV1Raw2::parse(i).is_ok();
}

fn handle_staple_sct<'a>(i: &'a (Arc<TrustedLog>, Vec<u8>), out: &mut Vec<Vec<u8>>)
{
	if ParsedSctV1Raw2::parse(i.1.deref()).is_err() { return; }
	//Add all v1 stuff, but otherwise ignore.
	out.push(i.1.clone());
}

fn cname_from_parse<'a>(x: &'a [u8]) -> Option<&'a [u8]>
{
	let mut x = Source::new(x);
	let mut y = x.asn1(Asn1Tag::SEQUENCE).ok()?;
	fail_if_none!(x.more());
	let mut cname = None;
	while y.more() {
		let mut z = y.asn1(Asn1Tag::SET).ok()?;
		while z.more() {
			let mut w = z.asn1(Asn1Tag::SEQUENCE).ok()?;
			let oid = w.asn1_slice(Asn1Tag::OID).ok()?;
			let (_,payload) = w.asn1_any().ok()?;
			fail_if_none!(w.more());			//No more allowed.
			if oid == &[85, 4, 3] {				//CommonName.
				fail_if_none!(cname.is_some());		//Two CommnonNames?
				cname = Some(payload.as_slice());
			}
		}
	}
	cname
}

///Information about names in the certificate.
pub struct CertificateNames
{
	///The CommonName, if any.
	pub common_name: Option<Arc<Vec<u8>>>,
	///The DNS names.
	pub dns_names: Arc<Vec<Vec<u8>>>,
	///The IP addresses.
	pub ip_addresses: Arc<Vec<IpAddressBlock>>,
	///Validity range of the certificate.
	pub validity: Range<Timestamp>,
	__dummy: ()
}

//A certificate available for signing.
#[derive(Clone)]
struct AvailableCertificate
{
	//The keypair for certificate.
	keypair: Arc<Box<dyn KeyPair2+Send+Sync>>,
	//The certificate to send.
	certificate: Arc<Vec<u8>>,
	chain: Arc<Vec<Vec<u8>>>,
	//The OCSP staple to send.
	ocsp_staple: Option<Arc<Vec<u8>>>,
	//The SCT list to send.
	sct_list: Arc<Vec<Vec<u8>>>,
	//The Transparency Item list to send.
	trans_list: Arc<Vec<Vec<u8>>>,
	//Raw SCT list, of all the SCTs.
	raw_sct_list: Vec<(Arc<TrustedLog>, Vec<u8>)>,
	//The SCTs extracted from certificate.
	certificate_scts: Vec<Vec<u8>>,
	//The SCTs extracted from OCPS.
	ocsp_scts: Vec<Vec<u8>>,
	//Has certificate or OCSP SCTs?
	has_builtin_scts: bool,
	//Mask of available algorithms, using SIGALGO_* constants in bitmask.
	algorithms: SetOf<SignatureAlgorithm2>,
	//The algorithms needed by the chain.
	need_chain_algs: SetOf<SignatureAlgorithm2>,
	//Chain needs unknown algorithm.
	need_chain_unknown: bool,
	//Priority boost to apply.
	prio_boost: u64,
	//Is public certificate?.
	is_public_ca: bool,
	//The validity time of the certificate.
	cert_validity: Range<Timestamp>,
	//The validity time of the OCSP staple.
	ocsp_validity: Range<Timestamp>,
	//The certificate requires OCSP?
	needs_ocsp: bool,
	//The certificate requires SCT?
	needs_sct: bool,
	//The range for issuer in the EE certificate.
	ee_issuer: Range<usize>,
	//The range for serial in the EE certificate.
	ee_serial: Range<usize>,
	//Certificate Common Name.
	cname: Option<Arc<Vec<u8>>>,
	//Self-signed flag.
	selfsigned: bool,
	//Root is ISRG X1 flag.
	root_is_isrg_x1: bool,
	//Revoked flag.
	revoked: bool,
	//Is NSA compliant?
	nsa_chain: bool,
	//Fast interlock info.
	fast_interlock: FastInterlock,
}

thread_local!(static EMPTY_LIST: Arc<Vec<Vec<u8>>> = Arc::new(Vec::new()));

fn __parse_certificate<'a>(content: &'a [u8], ordinal: usize) -> Result<ParsedCertificate2<'a>, CertificateLoadError>
{
	use self::CertificateLoadError::*;
	//Content is supposed to be exactly one ASN.1 SEQUENCE.
	let mut src = Source::new(content);
	let cert = src.asn1_sequence_out().set_err(ValidHeaderMissing(ordinal))?;
	src.expect_end().set_err(JunkAfterEndOfCert(ordinal))?;
	//Parse the certificate.
	let parsed = ParsedCertificate2::from(cert).map_err(|x|CertParseError(ordinal, x))?;
	Ok(parsed)
}

fn __process_certificate(parsed: &ParsedCertificate2, need_chain_algs: &mut SetOf<SignatureAlgorithm2>,
	need_chain_unknown: &mut bool, nsa_chain: &mut bool, cert_validity: &mut Range<Timestamp>)
{
	//Grab the algorithm. If this is some whacko algorithm, treat as unknown.
	let alg = parsed.signature.ok().map(|s|s.generic_algorithm());
	*nsa_chain &= parsed.nsa;
	//Don't count algorithms of self-signed certificates for need-to-know.
	if !parsed.issuer.same_as(parsed.subject) {
		if let Some(alg) = alg {
			need_chain_algs.add(&alg);
		} else {
			*need_chain_unknown = true;
		}
	}
	cert_validity.start = max(cert_validity.start, parsed.not_before);
	cert_validity.end = min(cert_validity.end, parsed.not_after);
}

fn set_neq<T:SetItem>(a: SetOf<T>, b: SetOf<T>) -> bool { (a ^ b).has_elements() }

impl AvailableCertificate
{
	//Create a new available certificate. OCSP and SCT staples are initially empty.
	fn new(keyring: &Keyring, cert: Vec<u8>, chain: Arc<Vec<Vec<u8>>>,
		time_now: Timestamp) -> Result<AvailableCertificate, CertificateLoadError>
	{
		use self::CertificateLoadError::*;
		//Parse the certificate chain.
		let keypair;
		let mut need_chain_algs = SetOf::<SignatureAlgorithm2>::empty_set();
		let prio_boost;
		let public_ca;
		let issuer_range;
		let serial_range;
		let mut cert_validity = Timestamp::negative_infinity()..Timestamp::positive_infinity();
		let mut needs_ocsp = false;
		let mut needs_sct = false;
		let mut certificate_scts = Vec::new();
		let selfsigned;
		let mut need_chain_unknown = false;
		let mut cert_refs: Vec<&[u8]> = Vec::with_capacity(chain.len());
		let san_list;
		let san_list_ip;
		let cname;
		let mut root_is_isrg_x1 = false;
		let mut nsa_chain = true;
		{
			//Process the end-entity certificate.
			let parsed = __parse_certificate(&cert, 0)?;
			__process_certificate(&parsed, &mut need_chain_algs, &mut need_chain_unknown,
				&mut nsa_chain, &mut cert_validity);
			//Is selfsigned?
			selfsigned = parsed.issuer.same_as(parsed.subject);
			//The EE certificate is handled a bit specially.
			//Look up the private key to use.
			keypair = keyring.lookup(parsed.pubkey);
			san_list = parsed.dnsnames.iter().map(|x|x.to_owned()).collect();
			san_list_ip = parsed.dnsnames.iter_addrs().collect();
			cname = cname_from_parse(parsed.subject.as_raw_subject()).
				map(|x|Arc::new(x.to_owned()));
			//The issuer and serial are taken from here.
			issuer_range = assert_some!(subslice_to_range(&cert, parsed.issuer.0),
				"Certificate issuer not from certificate???");
			serial_range = assert_some!(subslice_to_range(&cert, parsed.serial_number),
				"Certificate serial not from certificate???");
			//Has stapled SCTs?
			certificate_scts.extend(parsed.scts.iter().map(|x|(*x).to_owned()));
			//Compute priority boost according to the algorithm.
			prio_boost = match parsed.pubkey_algo {
				PublicKeyAlgorithm::Ed25519 => 5u64,
				PublicKeyAlgorithm::Ed448 => 4u64,
				PublicKeyAlgorithm::EcdsaP256 => 3u64,
				PublicKeyAlgorithm::EcdsaP384 => 2u64,
				PublicKeyAlgorithm::EcdsaP521 => 1u64,
				_ => 0
			} << 40;
			//Give boost for public certificates. But RPKs do not get this boost.
			public_ca = is_public_ca(parsed.issuer.as_raw_issuer());
			//root_is_isrg_x1 will never be set, because isrg x1 does not sign certificates usable
			//for TLS.
		}
		for (ordinal,i) in chain.iter().enumerate() {
			cert_refs.push(i.deref());
			let parsed = __parse_certificate(i, ordinal+1)?;	//Reserve 0 => EE.
			__process_certificate(&parsed, &mut need_chain_algs, &mut need_chain_unknown,
				&mut nsa_chain, &mut cert_validity);
			//Only the last certificate matters here, as this hack only fires if ISRG X1 is not
			//explicitly included.
			root_is_isrg_x1 = parsed.issuer.as_raw_issuer() == super::get_irsg_x1_subject();
		}
		//Do this immediately after main parse to give this error if the chain is empty.
		fail_if!(cert_validity.start > cert_validity.end, NoEffectiveCertificateLifetime);
		let keypair = match keypair {
			Some(x) => x,
			None => fail!(NoPrivateKeyAvailable)
		};
		let mut algorithms = SetOf::<SignatureAlgorithm2>::empty_set();
		for alg in keypair.get_signature_algorithms().iter().filter_map(|x|x.downcast_tls()) {
			algorithms.add(&alg);
		}
		fail_if!(!algorithms.has_elements(), NoKnownAlgorithms);
		fail_if!(Timestamp::now() > cert_validity.end, CertificateExpired);
		let kp_pubk: &[u8] = keypair.get_public_key2();
		sanity_check_certificate_chain_load2(&cert, &cert_refs, kp_pubk, time_now,
			&mut needs_ocsp, &mut needs_sct, true).map_err(SanityCheckFailed)?;
		Ok(AvailableCertificate {
			keypair: keypair.clone(),
			certificate: Arc::new(cert),
			chain: chain,
			ocsp_staple: None,
			sct_list: EMPTY_LIST.with(|x|x.clone()),
			trans_list: EMPTY_LIST.with(|x|x.clone()),
			raw_sct_list: Vec::new(),
			certificate_scts: certificate_scts,
			ocsp_scts: Vec::new(),
			has_builtin_scts: false,
			algorithms: algorithms,
			need_chain_algs: need_chain_algs,
			need_chain_unknown: need_chain_unknown,
			prio_boost: prio_boost,
			is_public_ca: public_ca,
			cert_validity: cert_validity.clone(),
			//No OCSP. Note, the endpoints are in wrong order.
			ocsp_validity: Timestamp::positive_infinity()..Timestamp::negative_infinity(),
			needs_ocsp: needs_ocsp,
			needs_sct: needs_sct,
			ee_issuer: issuer_range,
			ee_serial: serial_range,
			cname: cname,
			selfsigned: selfsigned,
			nsa_chain: nsa_chain,
			root_is_isrg_x1: root_is_isrg_x1,
			revoked: false,
			fast_interlock: FastInterlock {
				names: Arc::new(san_list),
				addresses: Arc::new(san_list_ip),
				c_expiry: cert_validity.end,
				//OCSP is never available at first.
				o_expiry: Timestamp::negative_infinity(),
			},
		})
	}
	//Read certificate chain from file.
	fn read_certifcate_chain<R:IoRead>(file: &mut R) -> Result<(Vec<u8>, Vec<Vec<u8>>), CertificateLoadError>
	{
		use PFragment::*;
		let mut _chain = Vec::new();
		let mut cchain = Vec::new();
		file.read_to_end(&mut _chain).map_err(|x|CertificateLoadError::ReadError(x.to_string()))?;
		iterate_pem_or_derseq_fragments(&_chain, |frag| {match frag {
			Comment(_) => Ok(()),	//Skip comments.
			Error => Err(CertificateLoadError::ReadError(format!("Not in PEM nor DERseq format"))),
			Der(cert)|Pem(PFKind::Certificate, cert) => Ok(cchain.push(cert.to_owned())),
			Pem(_, _) => Ok(())	//Skip unknown resources.
		}})?;
		fail_if!(cchain.len() == 0, CertificateLoadError::CertificateChainEmpty);
		let cert = cchain.remove(0);
		Ok((cert, cchain))
	}
	//Rebuild the SCT staples.
	fn rebuild_sct_staple(&mut self) -> bool
	{
		let mut builtin = false;
		let mut out = Vec::new();
		//Certificate SCTs.
		for i in self.certificate_scts.iter() { handle_external_sct(i, &mut builtin); }
		//OCSP SCTs.
		for i in self.ocsp_scts.iter() { handle_external_sct(i, &mut builtin); }
		//Extension SCTs.
		for i in self.raw_sct_list.iter() { handle_staple_sct(i, &mut out); }
		self.sct_list = if out.is_empty() { EMPTY_LIST.with(|x|x.clone()) } else { Arc::new(out) };
		builtin
	}
	//Try adding a SCT response.
	fn try_add_sct(&mut self, sct: Vec<u8>, log: Arc<TrustedLog>)
	{
		let item = sct;
		self.raw_sct_list.push((log, item.clone()));
		self.has_builtin_scts = self.rebuild_sct_staple();
	}
	//Read a SCT response from file.
	fn read_sct<R:IoRead>(file: &mut R) -> Result<Vec<u8>, CertificateLoadError>
	{
		let mut _data = Vec::new();
		file.read_to_end(&mut _data).map_err(|x|CertificateLoadError::ReadError(x.to_string()))?;
		Ok(_data)
	}
	//Try setting OCSP response.
	fn try_set_ocsp(&mut self, ocsp: Option<Arc<Vec<u8>>>, current_time: Timestamp) ->
		Result<(), CertificateLoadError>
	{
		let staple_data = match &ocsp {
			&Some(ref x) => x,
			&None => {
				//We can clear the OCSP staple at any time.
				//We assert memory fences to ensure that update to structures can't be
				//interrupted by a panic.
				fence(AtomicOrdering::SeqCst);
				//We don't have OCSP anymore. Note, that endpoints are in the wrong order.
				//o_expiry becomes meaningless, so does not need update.
				self.ocsp_scts.clear();
				self.ocsp_validity = Timestamp::positive_infinity()..Timestamp::negative_infinity();
				self.ocsp_staple = None;
				fence(AtomicOrdering::SeqCst);
				//Rebuild this, with no OCSP anymore.
				self.has_builtin_scts = self.rebuild_sct_staple();
				return Ok(());
			}
		};
		//There has to be at least one certificate, the constructor checks this.
		//Also, slice the issuer and serial out of the EE cert.
		{
			let ee_cert = self.certificate.deref().deref();
			let issuer = CertificateIssuer(&ee_cert[self.ee_issuer.clone()]);
			let serial = &ee_cert[self.ee_serial.clone()];
			//Sanity-check the OCSP response. Outgoing, so allow all.
			let ocsp_validity = match sanity_check_ocsp3(staple_data.deref(), issuer,
				serial, current_time, SignatureAlgorithmEnabled2::unsafe_any_policy()) {
				Ok(x) => x.not_before..x.not_after,
				Err(x) => if x.is_revoked() {
					//Can not unrevoke, so this is never set to false.
					self.revoked = true;
					return Ok(());
				} else {
					fail!(CertificateLoadError::OcspSanityCheckFailed(x));
				}
			};
			let ocsp_scts = extract_scts_from_ocsp(staple_data.deref()).
				unwrap_or(OcspDumpedScts::blank()).list.iter().map(|&x|x.to_owned()).collect();
			let response = Some(staple_data.clone());
			//We assert memory fences to ensure that update to structures can't be interrupted by a
			//panic.
			fence(AtomicOrdering::SeqCst);
			self.fast_interlock.o_expiry = ocsp_validity.end;
			self.ocsp_validity = ocsp_validity;
			self.ocsp_scts = ocsp_scts;
			self.ocsp_staple = response;
			fence(AtomicOrdering::SeqCst);
		}
		self.has_builtin_scts = self.rebuild_sct_staple();		//OCSP might have changed.
		Ok(())
	}
	//Read a OCSP response from file.
	fn read_ocsp<R:IoRead>(file: &mut R) -> Result<Option<Arc<Vec<u8>>>, CertificateLoadError>
	{
		let mut _data = Vec::new();
		file.read_to_end(&mut _data).map_err(|x|CertificateLoadError::ReadError(x.to_string()))?;
		Ok(Some(Arc::new(_data)))
	}
	//Make a signer using specified algorithm.
	fn signer_with_algo(&self, algorithm: SignatureAlgorithmTls2,
		_criteria: &CertificateLookupCriteria, current_time: Timestamp) -> SelectedCertificate
	{
		let time = current_time;
		let valid_ocsp = time >= self.ocsp_validity.start && time < self.ocsp_validity.end;
		let ecdsa_type = algorithm.to_generic().tls12_type() == Tls12SignatureType2::Ecdsa;
		SelectedCertificate {
			keypair: self.keypair.clone(),
			sig_algorithm: algorithm,
			ecdsa_type: ecdsa_type,
			certificate: self.certificate.clone(),
			chain: self.chain.clone(),
			//We have to drop OCSP staple if it isn't valid.
			ocsp_staple: if valid_ocsp { self.ocsp_staple.clone() } else { None },
			sct_list: self.sct_list.clone(),
			trans_list: self.trans_list.clone(),
			root_is_isrg_x1: self.root_is_isrg_x1,
			fast_interlock: self.fast_interlock.clone(),
		}
	}
	//Is the certificate any good at all?
	fn is_useable(&self, criteria: &CertificateLookupCriteria, allow_unknown_algo: bool, timenow: Timestamp) ->
		bool
	{
		let mut ok = true;
		//Revoked is not ok.
		ok &= !self.revoked;
		//NSA mode checks. Accept if not in NSA mode, or NSA compliant chain.
		ok &= !criteria.nsa || self.nsa_chain;
		//No selfsigneds unless asked. Non-selfsigneds are OK even if asked.
		ok &= criteria.selfsigned_ok || !self.selfsigned;
		//Compute intersection of available signature algorithms and check there is at least one valid
		//algorithm. If TLS 1.3 is in use, mask algorithms that can't be used with TLS 1.3.
		let tls13mask = if criteria.tls13 { Some(SignatureAlgorithm2::SET_TLS13_OK) } else { None };
		ok &= (criteria.ee_flags2 & self.algorithms & tls13mask).has_elements();
		//Supported CA signature algorithms? We skip this check if arbitrary algorithms are allowed in the
		//chain. Also, self-signed roots are not counted, because need_chain_algs does not accumulate those.
		//Note that we check that complement of sig_flags does not contain any algorithms from
		//need_chain_algs, as this is equivalent to check that sig_flags contains all algorithms in
		//need_chain_algs.
		let needs_unknown = self.need_chain_unknown ||
			set_neq(self.need_chain_algs & criteria.sig_flags2, self.need_chain_algs);
		ok &= allow_unknown_algo || !needs_unknown;
		//If certificate needs OCSP, it must be within validity.
		ok &= !self.needs_ocsp || timenow.in_range(&self.ocsp_validity);
		//If certificate needs SCT, we have to have at least one stapled one, OCSP with one or cerificate
		//has to have one.
		ok &= !self.needs_sct || self.sct_list.len() > 0 || self.has_builtin_scts;
		//Check not-yet-valid or expired.
		ok &= timenow.in_range(&self.cert_validity);
		//All checks done.
		ok
	}
	//Score certificate, assuming it is usable.
	fn score_certificate(&self, rpk: bool) -> u64
	{
		const TIME_BIAS: i64 = 1i64 << 39;
		let ts_score = min(max(self.cert_validity.end.unix_time(), -TIME_BIAS), TIME_BIAS) + TIME_BIAS;
		let mut boost = self.prio_boost;
		//Give even more boost to public certificates, unless RPK is asserted.
		if !rpk && self.is_public_ca { boost += 1 << 43; }
		boost.saturating_add(ts_score as u64)
	}
	//If the certificate isn't suitable, returns None. Otherwise returns score (bigger is better) for this
	//certificate.
	fn is_good(&self, criteria: &CertificateLookupCriteria, allow_unknown_algo: bool, timenow: Timestamp) ->
		Option<u64>
	{
		if self.is_useable(criteria, allow_unknown_algo, timenow) {
			//Assume valid.
			Some(self.score_certificate(criteria.selfsigned_ok))
		} else {
			None
		}
	}
}

//A handle for AvailableCertificate
#[derive(Clone)]
struct SharedAvailableCertificate
{
	inner: Arc<RwLock<AvailableCertificate>>,
}

impl SharedAvailableCertificate
{
	fn new(cert: AvailableCertificate) -> SharedAvailableCertificate
	{
		SharedAvailableCertificate{inner: Arc::new(RwLock::new(cert))}
	}
	fn with_read_access<F,T>(&self, cb: F) -> T where F: FnOnce(&AvailableCertificate) -> T
	{
		self.inner.with_read(cb)
	}
	fn with_write_access<F,T>(&self, cb: F) -> T where F: FnOnce(&mut AvailableCertificate) -> T
	{
		self.inner.with_write(cb)
	}
}

fn remove_from_vec2(x: &mut Vec<u64>, item: u64)
{
	let mut idx = 0;
	while idx < x.len() {
		//idx is in range by loop condition.
		if x[idx] == item {
			x.swap_remove(idx);
		} else {
			idx += 1;	//Array index so in-range.
		}
	}
}

#[derive(Clone)]
struct CertificateJar
{
	contents: BTreeMap<u64, SharedAvailableCertificate>,
	name_index: BTreeMap<Vec<u8>, Vec<u64>>,
	wild_name_index: BTreeMap<Vec<u8>, Vec<u64>>,
	all_index: Vec<u64>,
	private_keys: Keyring,
	next_id: u64,
	ct_logs: Vec<([u8;32], Arc<TrustedLog>)>,
	id_by_name: BTreeMap<String, u64>,
}

impl CertificateJar
{
	fn lookup_certificate(&self, id: u64) -> Option<SharedAvailableCertificate>
	{
		self.contents.get(&id).map(|x|x.clone())
	}
	fn delete_certificate(&mut self, id: u64)
	{
		//TODO: Optimize this.
		for i in self.name_index.iter_mut() { remove_from_vec2(i.1, id); }
		for i in self.wild_name_index.iter_mut() { remove_from_vec2(i.1, id); }
		remove_from_vec2(&mut self.all_index, id);
		//TODO: Remove entry from id_by_name (but things still work without doing this).
		self.contents.remove(&id);
	}
	fn add_certificate(&mut self, acert: AvailableCertificate, name: &str) -> Result<u64, CertificateLoadError>
	{
		let san_list = acert.fast_interlock.names.clone();
		let san_list_ip = acert.fast_interlock.addresses.clone();
		let handle = SharedAvailableCertificate::new(acert);
		//We need to use the names to construct the maps.
		{
			//Allocate ID number. Hopefully these don't grow too large...
			fail_if!(self.next_id.checked_add(1).is_none(), CertificateLoadError::HandleOverflowed);
			let id = self.next_id;
			self.next_id = self.next_id + 1;
			self.contents.insert(id, handle);
			self.all_index.push(id);
			for i in san_list.iter() {
				let sni = i;
				//Do not accept entries starting with '[', as those have special meaning.
				if sni.get(0) == Some(&b'[') { continue; }
				//Insert wildcard entries for wildcard certs, and normal entries for non-wildcard
				//ones.
				let (idx, ent) = if sni.len() >= 2 && &sni[..2] == b"*." {
					(&mut self.wild_name_index, &sni[2..])
				} else {
					(&mut self.name_index, &sni[..])
				};
				idx.entry(ent.to_owned()).or_insert_with(||Vec::new()).push(id);
			}
			for i in san_list_ip.iter() {
				//This never has wildcard entry. By default ip address blocks have no slash.
				let sni = format!("[{i}]").into_bytes();
				self.name_index.entry(sni).or_insert_with(||Vec::new()).push(id);
			}
			self.id_by_name.insert(name.to_owned(), id);
			return Ok(id);
		}
	}
	fn select_best_among(&self, certs: &[u64], best_cert: &mut Option<SharedAvailableCertificate>,
		best_score: &mut u64, criteria: &CertificateLookupCriteria, allow_unknown_algo: bool)
	{
		let timenow = Timestamp::now();
		for cnum in certs.iter() {
			if let Some(y) = self.contents.get(cnum) {
				let score = y.with_read_access(|x|x.is_good(criteria, allow_unknown_algo, timenow));
				if let Some(score) = score {
					if score > *best_score {
						*best_cert = Some(y.clone());
						*best_score = score;
					}
				}
			}
		}
	}
	fn select_best_certificate(&self, criteria: &CertificateLookupCriteria, allow_unknown_algo: bool) ->
		Result<SharedAvailableCertificate, CLErr>
	{
		let mut best_cert = None;
		let mut best_score = 0;
		let mut found_host = false;
		if let Some(sni) = criteria.sni {
			let sni = sni.as_bytes();
			let sniwild = sni.splitn(2, |x|*x==b'.').nth(1).unwrap_or(&[]);
			if let Some(x) = self.name_index.get(sni) {
				found_host |= !x.is_empty();	//If index no empty, host found.
				self.select_best_among(x, &mut best_cert, &mut best_score, criteria,
					allow_unknown_algo);
				//If this finds anything suitable, shadow the possible wildcard.
				if let Some(best_cert) = best_cert { return Ok(best_cert); }
			}
			if let Some(x) = self.wild_name_index.get(sniwild) {
				found_host |= !x.is_empty();	//If index no empty, host found.
				self.select_best_among(x, &mut best_cert, &mut best_score, criteria,
					allow_unknown_algo);
			}
		} else {
			found_host |= !self.all_index.is_empty();	//If index no empty, host found.
			self.select_best_among(&self.all_index, &mut best_cert, &mut best_score, criteria,
				allow_unknown_algo);
		}
		//If none was found, and allow_unknown_algo is not set, search again with it set.
		if !allow_unknown_algo && best_cert.is_none() {
			self.select_best_certificate(criteria, true)
		} else {
			//If indices have matching entries, but select_best_among() refused to pick one, that
			//means no matching certificate. If even the indices are empty or non-existent, that
			//means there is no such host.
			best_cert.ok_or_else(||if found_host { CLErr::NoMatch } else { CLErr::NoSuchHost })
		}
	}
	fn lookup(&self, criteria: &CertificateLookupCriteria) -> Result<ServerCertificate, CLErr>
	{
		let best_cert = self.select_best_certificate(criteria, false)?;
		//If select_best_certificate() succeeded, but this fails, assume no matching certificate.
		best_cert.with_read_access(|cert| {
			let tls13mask = if criteria.tls13 { Some(SignatureAlgorithm2::SET_TLS13_OK) } else { None };
			let available_algos = cert.algorithms & criteria.ee_flags2 & tls13mask;
			let algo = available_algos.iter::<SignatureAlgorithmTls2>().next().ok_or(CLErr::NoMatch)?;
			Ok(ServerCertificate::Master(cert.signer_with_algo(algo, criteria, Timestamp::now())))
		})
	}
	pub fn add_trusted_log(&mut self, log: &TrustedLog)
	{
		//This should never fail.
		self.ct_logs.push((sha256(&log.key), Arc::new(log.clone())));
	}
	fn any_ok_certificate(&self, list: &[u64]) -> Option<Timestamp>
	{
		let timenow = Timestamp::now();
		let mut last_expiry = None;
		for cnum in list.iter() {
			if let Some(y) = self.contents.get(cnum) {
				y.with_read_access(|cert|{
					let mut ok = true;
					//Must not be revoked.
					ok &= !cert.revoked;
					//OCSP must be in validity.
					ok &= !cert.needs_ocsp || timenow.in_range(&cert.ocsp_validity);
					//If certificate needs SCT, we have to have at least one stapled one, OCSP
					//with one or cerificate has to have one.
					ok &= !cert.needs_sct || cert.sct_list.len() > 0 || cert.has_builtin_scts;
					//Check not-yet-valid or expired.
					ok &= timenow.in_range(&cert.cert_validity);
					//All checks done. If OK, take expiry to be the latest expiry.
					let expiry = if cert.needs_ocsp {
						min(cert.ocsp_validity.end, cert.cert_validity.end)
					} else {
						cert.cert_validity.end
					};
					if ok { last_expiry = Some(max(expiry, last_expiry.unwrap_or(expiry))); }
				})
			}
		}
		last_expiry
	}
	fn certificate_exists_for_name_until(&self, name: &str) -> Option<Timestamp>
	{
		let mut until1 = None;
		let mut until2 = None;
		let sni = name.as_bytes();
		let sniwild = sni.splitn(2, |x|*x==b'.').nth(1).unwrap_or(&[]);
		if let Some(certlist) = self.name_index.get(sni) {
			until1 = self.any_ok_certificate(certlist);
		}
		if let Some(certlist) = self.wild_name_index.get(sniwild) {
			until2 = self.any_ok_certificate(certlist);
		}
		match (until1, until2) {
			(Some(x), Some(y)) => Some(max(x, y)),
			(Some(x), None) => Some(x),
			(None, Some(y)) => Some(y),
			(None, None) => None,
		}
	}
	fn get_names_for_certificate(&self, cert_name: &str) -> Option<CertificateNames>
	{
		let id: u64 = self.id_by_name.get(cert_name)?.clone();
		let y = self.contents.get(&id)?;
		Some(y.with_read_access(|cert|CertificateNames {
			common_name: cert.cname.clone(),
			dns_names: cert.fast_interlock.names.clone(),
			ip_addresses: cert.fast_interlock.addresses.clone(),
			validity: if cert.needs_ocsp {
				max(cert.ocsp_validity.start, cert.cert_validity.start)..
					min(cert.ocsp_validity.end, cert.cert_validity.end)
			} else {
				cert.cert_validity.clone()
			},
			__dummy: ()
		}))
	}
	fn get_list_of_names(&self, id: u64) -> Option<Vec<Vec<u8>>>
	{
		let y = self.contents.get(&id)?;
		Some(y.with_read_access(|cert|{
			let mut output: Vec<Vec<u8>> = Vec::new();
			for entry in cert.fast_interlock.names.iter() { output.push(entry.to_owned()); }
			for entry in cert.fast_interlock.addresses.iter() {
				output.push(format!("[{entry}]").into_bytes());
			}
			output
		}))
	}
}

///Handle for local certificate.
///
///This handle is used for referencing local certificates, for adding or updating staple data.
///
///There is no explict constructor, instead, these handles are returned by the [`LocalServerCertificateStore::add()`]
///and [`LocalServerCertificateStore::add_file()`] methods.
///
///This implements the `Clone` method for cloning handles. Both handles will refer to the same certificate, and
///will share OCSP staple and SCT staples. Additionally deleting one handle will delete both. The handles can be
///compared with `==` operator, which is "handle for the same certificate" equality.
///
///[`LocalServerCertificateStore::add()`]: struct.LocalServerCertificateStore.html#method.add
///[`LocalServerCertificateStore::add_file()`]: struct.LocalServerCertificateStore.html#method.add_file
#[derive(Clone)]
pub struct LocalServerCertificateHandle
{
	jar: Arc<RwLock<CertificateJar>>,
	id: u64,
}


impl PartialEq for LocalServerCertificateHandle
{
	fn eq(&self, another: &Self) -> bool
	{
		if self.jar.deref() as *const _ != another.jar.deref() as *const _ { return false; }
		if self.id != another.id { return false; }
		true
	}
}

impl Eq for LocalServerCertificateHandle {}

impl Hash for LocalServerCertificateHandle
{
	fn hash<H:Hasher>(&self, state: &mut H)
	{
		state.write_usize(self.jar.deref() as *const _ as usize);
		state.write_u64(self.id);
	}
}

impl LocalServerCertificateHandle
{
	///Get ID of this handle.
	pub fn get_id(&self) -> u64 { self.id }
	///Clear the OCSP staple for the certificate pointed by this handle.
	///
	///Note that this method is inherently racy. If one wants to atomically replace the existing OCSP response,
	///use [`set_ocsp_staple()`](#method.set_ocsp_staple) or
	///[`set_ocsp_staple_file()`](#method.set_ocsp_staple_file) without first clearing the existing OCSP
	///response.
	///
	///On success returns `Ok(())`. Otherwise returns `Err(err)`, where `err` is the error message.
	pub fn clear_ocsp_staple(&mut self) -> Result<(), Cow<'static, str>>
	{
		let cert = self.jar.read().lookup_certificate(self.id).ok_or("Stale certificate handle")?;
		let now = Timestamp::now();
		cert.with_write_access(|crt|{
			crt.try_set_ocsp(None, now).set_err(Cow::Borrowed("Failed to clear OCSP staple"))
		})
	}
	///Update the OCSP staple for the certificate pointed by this handle. The new staple is read from stream
	///`staple` that implements the `Read` trait.
	///
	///On success returns `Ok(())`. Otherwise returns `Err(err)`, where `err` is the error message.
	///
	///For clearing the OCSP staple of given handle, use the [`clear_ocsp_staple()`] method. However, this SHOULD
	///NOT be done before loading a new OCSP staple, the old staple if any is atomically overwritten.
	///The OCSP staple is not reloaded automatically and has limited lifespan, so the application should
	///periodically refetch the OCSP staple and overwrite the existing one.
	///
	///[`clear_ocsp_staple()`]: #method.clear_ocsp_staple
	pub fn set_ocsp_staple<R:IoRead>(&mut self, staple: &mut R) -> Result<(), Cow<'static, str>>
	{
		let cert = self.jar.read().lookup_certificate(self.id).ok_or("Stale certificate handle")?;
		let response = AvailableCertificate::read_ocsp(staple).map_err(|err|{
			format!("Read: {err}")
		})?;
		let now = Timestamp::now();
		cert.with_write_access(|crt|{
			crt.try_set_ocsp(response, now).map_err(|err|{
				Cow::Owned(format!("Set: {err}"))
			})
		})
	}
	///Update the OCSP staple for the certificate pointed by this handle. The new staple is read from file
	///`staple`.
	///
	///On success returns `Ok(())`. Otherwise returns `Err(err)`, where `err` is the error message.
	///
	///For clearing the OCSP staple of given handle, use the [`clear_ocsp_staple()`] method. However, this
	///SHOULD NOT be done before loading a new OCSP staple, the old staple if any is atomically overwritten.
	///The OCSP staple is not reloaded automatically and has limited lifespan, so the application should
	///periodically refetch the OCSP staple and overwrite the existing one.
	///
	///This differs from [`set_ocsp_staple()`](#method.set_ocsp_staple) in that the OCSP staple is passed as
	///filename instead of readable stream object.
	///
	///[`clear_ocsp_staple()`]: #method.clear_ocsp_staple
	pub fn set_ocsp_staple_file<P:AsRef<Path>>(&mut self, staple: P) -> Result<(), Cow<'static, str>>
	{
		let staple = staple.as_ref();
		let mut _staple = File::open(staple).map_err(|err|{
			format!("Open: {err}")
		})?;
		self.set_ocsp_staple(&mut _staple)
	}
	///Add an SCT staple for the certificate pointed by this handle. The new staple is read from stream
	///`staple` that implements `Read` in raw SerializedSct format, with no leading length marker.
	///
	///On success returns `Ok(())`. Otherwise returns `Err(err)`, where `err` is the error message.
	///
	///This can be called multiple times in order to add multiple SCTs. There is no way to clear added SCT.
	pub fn add_sct_staple<R:IoRead>(&mut self, staple: &mut R) -> Result<(), Cow<'static, str>>
	{
		let cert = self.jar.read().lookup_certificate(self.id).ok_or("Stale certificate handle")?;
		let response = AvailableCertificate::read_sct(staple).map_err(|err|{
			format!("Read: {err}")
		})?;
		//Grab the EE certficate and issuer key.
		let (eecert, isskey): (Vec<u8>, Vec<u8>) = cert.with_read_access(|crt|{
			let eecert = crt.certificate.deref().clone();
			let isskey = crt.chain.get(0).and_then(|rcrt|extract_spki(&rcrt).ok()).
				map(|x|x.to_owned()).unwrap_or(Vec::new());
			(eecert, isskey)
		});
		fail_if!(isskey.len() == 0, "No issuer key available");
		let log = self.check_new_trans_item(self.jar.read().deref(), &eecert, response.deref().deref(),
			&isskey, Timestamp::now())?;
		cert.with_write_access(|crt|{
			crt.try_add_sct(response, log);
			Ok(())
		})
	}
	///Add an SCT staple for the certificate pointed by this handle. The new staple is read from file
	///`staple` in raw SerializedSct format, with no leading length marker.
	///
	///On success returns `Ok(())`. Otherwise returns `Err(err)`, where `err` is the error message.
	///
	///This can be called multiple times in order to add multiple SCTs. There is no way to clear added SCT.
	///
	///This differs from [`add_sct_staple()`](#method.add_sct_staple) in that the SCT staple is passed as
	///filename instead of readable stream object.
	pub fn add_sct_staple_file<P:AsRef<Path>>(&mut self, staple: P) -> Result<(), Cow<'static, str>>
	{
		let staple = staple.as_ref();
		let mut _staple = File::open(staple).map_err(|err|{
			format!("Open: {err}")
		})?;
		self.add_sct_staple(&mut _staple)
	}
	///Delete the certificate pointed by this handle from the store.
	pub fn delete(&self)
	{
		self.jar.write().delete_certificate(self.id);
	}
	///Get list of names in this certificate.
	pub fn get_name_list(&self) -> Option<Vec<Vec<u8>>>
	{
		self.jar.read().get_list_of_names(self.id)
	}
	fn check_new_trans_item(&self, x: &CertificateJar, eecert: &[u8], response: &[u8], isskey: &[u8],
		time_now: Timestamp) -> Result<Arc<TrustedLog>, Cow<'static, str>>
	{
		let item = ParsedSctV1Raw2::parse(response).map_err(|err|{
			format!("Insane SCT: {err}")
		})?;
		let id = item.log_id;
		let mut log: Option<Arc<TrustedLog>> = None;
		//TODO: Optimize this.
		for i in x.ct_logs.iter() {
			if id == &i.0 { log = Some(i.1.clone()); }
		}
		let log2: Arc<TrustedLog> = log.ok_or("SCT was issued by unknown log")?;
		let log = log2.deref();
		//Just checking, so use unsafe_any_policy, as one should not omit CTs even with weak algorithms.
		let any_policy = SignatureAlgorithmEnabled2::unsafe_any_policy();
		let vres = item.validate(eecert, isskey, log, false, time_now, any_policy).map(|_|true);
		let vres = vres.map_err(|err|{
			format!("SCT fails validation: {err}")
		})?;
		fail_if!(!vres, "Insane SCT: Disallowed algorithm");
		Ok(log2.clone())
	}
}

///A certificate lookup that uses local certificates and associated private keys.
///
///This implements the certificate lookup trait [`CertificateLookup`]. The certificates and keys to resolve
///requests are taken from added private keys and certificates. Since it is a certificate store, it also implements
///the [`KeyStoreOperations`](trait.KeyStoreOperations.html) trait.
///
///The private keys are added by [`add_privkey()`](#method.add_privkey) method. There is no way to remove a
///private key. After the key for certificate is added, the certificate itself can be added using the
///[`add()`](#method.add) or [`add_file()`](#method.add_file) methods.
///
///For OCSP and SCT stapling, the handle given by [`add()`](#method.add) or [`add_file()`](#method.add_file) methods
///can be used to modify the stapling for the certificate.
///
///Note that if `LocalServerCertificateStore` is cloned, both halves are just handles to the same store.
///
///[`CertificateLookup`]: trait.CertificateLookup.html
#[derive(Clone)]
pub struct LocalServerCertificateStore(Arc<RwLock<CertificateJar>>);

impl LocalServerCertificateStore
{
	///Create a new certificate store. Initially the store has no keys nor certificates.
	pub fn new() -> LocalServerCertificateStore
	{
		let _store = CertificateJar {
			contents: BTreeMap::new(),
			name_index: BTreeMap::new(),
			wild_name_index: BTreeMap::new(),
			all_index: Vec::new(),
			private_keys: Keyring::new(),
			next_id: 0,
			ct_logs: Vec::new(),
			id_by_name: BTreeMap::new(),
		};
		LocalServerCertificateStore(Arc::new(RwLock::new(_store)))
	}
	///Adds a private key into store. The private key can be any object `privkey` that implements the trait
	///[`KeyPair2`](trait.KeyPair2.html).
	///
	///The name key is referred to in error messages is `name`.
	///
	///On success returns `Ok(())`. Otherwise returns `Err(err)`, where `err` is the error message.
	///
	///Typically, the key object is instance of structure [`LocalKeyPair`](struct.LocalKeyPair.html).
	pub fn add_privkey<K: KeyPair2+Send+Sync+'static>(&mut self, privkey: K, name: &str)
	{
		self.0.write().private_keys.add(Arc::new(Box::new(privkey)), name);
	}
	///Add a new certificate chain to to the store. The chain is given by stream `chain` that implements the
	///`Read` trait. The certificate chain can be given in either DER or PEM format.
	///
	///The name used to refer to the certificate chain in error messages is `name`.
	///
	//The private key corresponding to the first certificate has to already exist in the store, or
	///adding a certiifcate will fail. In the added chain, if the first certificate is not self-signed, then the
	///second certificate must sign the first.
	///
	///On success returns `Ok(handle)`, where `handle` is a handle for the certificate. Otherwise returns
	///`Err(err)`, where `err` is the error message.
	///
	///`&[u8]` implements `Read`. However, note that the method takes in a mutable reference to object
	///implementing `Read`. Therefore you need to pass in a `&mut &[u8]` in order to read from a buffer.
	pub fn add<R:IoRead>(&mut self, chain: &mut R, name: &str) ->
		Result<LocalServerCertificateHandle, Cow<'static, str>>
	{
		let (cert, cchain) = AvailableCertificate::read_certifcate_chain(chain).map_err(|err|{
			format!("Read: {err}")
		})?;
		self.add_explicit_chain(cert, Arc::new(cchain), name)
	}
	///Add a new certificate chain to the store. The end-entity and issuer chains are explicitly given.
	pub fn add_explicit_chain(&mut self, cert: Vec<u8>, chain: Arc<Vec<Vec<u8>>>,
		name: &str) -> Result<LocalServerCertificateHandle, Cow<'static, str>>
	{
		let acert = {
			let now = Timestamp::now();
			let inner = self.0.read();
			AvailableCertificate::new(&inner.private_keys, cert, chain, now).map_err(|err|{
				format!("Load: {err}")
			})?
		};
		let id = {
			let mut inner = self.0.write();
			inner.add_certificate(acert, name).map_err(|err|{
				format!("Add`: {err}")
			})?
		};
		Ok(LocalServerCertificateHandle {jar:self.0.clone(), id:id})
	}
	///Add a new certificate chain to to the store. The chain is given by file named `chain`. The file can be
	//in either DER or PEM format.
	///
	//The private key corresponding to the first certificate has to already exist in the store, or
	///adding a certiifcate will fail. In the added chain, if the first certificate is not self-signed, then the
	///second certificate must sign the first.
	///
	///On success returns `Ok(handle)`, where `handle` is a handle for the certificate. Otherwise returns
	///`Err(err)`, where `err` is the error message.
	///
	///This differs from [`add()`](#method.add) in that the chain is passed as filename instead of readable
	///stream object.
	pub fn add_file<P: AsRef<Path>>(&mut self, chain: P) ->
		Result<LocalServerCertificateHandle, Cow<'static, str>>
	{
		let chain = chain.as_ref();
		let mut _chain = File::open(chain).map_err(|err|{
			format!("open: {err}")
		})?;
		//Add has its own logging.
		self.add(&mut _chain, &Filename::from_path(chain).to_string())
	}
	///Add a trusted Certificate Transparency log `log` to the store.
	///
	///Any dedicated SCT staples added to certificates must come from trusted logs, or those staples will
	///be rejected. This restriction does not apply to SCTs stapled to certificates or OCSP responses.
	pub fn add_trusted_log(&mut self, log: &TrustedLog)
	{
		self.0.write().add_trusted_log(log);
	}
	///Check how long there exists a certificate for the name `name`.
	///
	///To check for wildcard cert, start `name` with `*.`.
	///
	///Returns `Some(timestamp)` if such certificate exists until `timestamp`, returns `None` otherwise.
	pub fn certificate_exists_for_name_until(&self, name: &str) -> Option<Timestamp>
	{
		self.0.read().certificate_exists_for_name_until(name)
	}
	///Extract name information for certificate.
	///
	///The first element is canonical name if any, the second element is list of SANs.
	pub fn get_names_for_certificate(&self, cert_name: &str) -> Option<CertificateNames>
	{
		self.0.read().get_names_for_certificate(cert_name)
	}
	///Get certificate lookup corresponding to this store.
	pub fn lookup(&self) -> CertificateLookup
	{
		CertificateLookup(_CertificateLookup::Jar(self.0.clone()))
	}
}

#[derive(Clone)]
struct ServerHostKey(Arc<_ServerHostKey>);
pub(crate) struct ServerHostKeyAlg(Arc<_ServerHostKey>, SignatureAlgorithmTls2);
struct _ServerHostKey
{
	cert: Vec<u8>,
	key: Box<dyn KeyPair2+Send+Sync+'static>,
}

impl ServerHostKey
{
	fn check_request(&self, ee_alg: SetOf<SignatureAlgorithm2>) -> Result<SignatureAlgorithmTls2, CLErr>
	{
		//Take first overlap.
		for alg in self.0.key.get_signature_algorithms() {
			if let (true, Some(alg)) = (ee_alg.is_in(alg), alg.downcast_tls()) {
				return Ok(alg);
			}
		}
		//Algorithm does not match.
		Err(CLErr::NoMatch)
	}
}

impl ServerHostKeyAlg
{
	pub(super) fn sign(&self, data: &mut dyn FnMut(&mut SigHasher), rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<(u16, Vec<u8>), ()>>
	{
		super::tls_sign_helper(self.0.key.deref(), data, self.1, rng)
	}
	pub(super) fn certificate<'a>(&'a self) -> &'a [u8] { &self.0.cert }
}

///A certificate lookup.
#[derive(Clone)]
pub struct CertificateLookup(_CertificateLookup);
#[derive(Clone)]
enum _CertificateLookup
{
	Null,
	NoHostKey,
	Jar(Arc<RwLock<CertificateJar>>),
	HostKey(ServerHostKey),
}

fn make_fake_cert(spki: &[u8]) -> Vec<u8>
{
	use btls_aux_rope3::rope3;
	use btls_aux_rope3::FragmentWriteOps;
	//The certificate can be umm... Interesting.
	let x = rope3!(SEQUENCE(
		SEQUENCE(
			[0](INTEGER 2u8),
			INTEGER 1u8,
			SEQUENCE(OBJECT_IDENTIFIER 0u8),
			SEQUENCE(),
			SEQUENCE(
				GENERALIZED_TIME "00010101000000Z",
				GENERALIZED_TIME "99991231235959Z"
			),
			SEQUENCE(),
			spki,
			[3](SEQUENCE())
		),
		SEQUENCE(OBJECT_IDENTIFIER 0u8),
		BIT_STRING EPSILON
	));
	FragmentWriteOps::to_vector(&x)
}

impl CertificateLookup
{
	///Create a new NULL lookup that always fails to look up certificate.
	pub fn null() -> CertificateLookup { CertificateLookup(_CertificateLookup::Null) }
	///Create a new host key lookup.
	pub fn hostkey(hostkey: Option<Box<dyn KeyPair2+Send+Sync+'static>>) -> CertificateLookup
	{
		if let Some(hostkey) = hostkey {
			let spki = hostkey.get_public_key2();
			let fake_cert = make_fake_cert(spki);
			CertificateLookup(_CertificateLookup::HostKey(ServerHostKey(Arc::new(_ServerHostKey {
				cert: fake_cert,
				key: hostkey,
			}))))
		} else {
			CertificateLookup(_CertificateLookup::NoHostKey)
		}
	}
	pub(crate) fn lookup(&self, criteria: &CertificateLookupCriteria) ->
		Result<ServerCertificate, CLErr>
	{
		match &self.0 {
			&_CertificateLookup::Null => Err(CLErr::NoSuchHost),
			&_CertificateLookup::NoHostKey => Err(CLErr::InternalError("No host key configured")),
			&_CertificateLookup::Jar(ref x) => x.read().lookup(criteria),
			&_CertificateLookup::HostKey(ref x) => {
				//Check algorithm, and if OK, return the key.
				let alg = x.check_request(criteria.sig_flags2)?;
				Ok(ServerCertificate::Hostkey(ServerHostKeyAlg(x.0.clone(), alg)))
			}
		}
	}
}

impl KeyStoreOperations for LocalServerCertificateStore
{
	fn enumerate_private_keys(&self) -> Vec<(String, String)>
	{
		self.0.read().private_keys.enumerate_private_keys()
	}
	fn get_public_key(&self, privkey: &str) -> Option<Vec<u8>>
	{
		self.0.read().private_keys.get_public_key(privkey).map(|x|x.to_owned())
	}
	fn sign_csr(&self, privkey: &str, csr_params: &CsrParams) -> FutureReceiver<Result<Vec<u8>, ()>>
	{
		self.0.read().private_keys.sign_csr(privkey, csr_params)
	}
}

#[test]
fn test_fake_cert()
{
	let test_key: &'static [u8] = b"0\x07ABCDEFG";
	let cert = make_fake_cert(test_key);
	let key = extract_spki(&cert).unwrap();
	assert_eq!(key, test_key);
}

#[test]
fn test_cname_from_parse()
{
	let mat = b"\
		\x30\x4f\x31\x0b\x30\x09\x06\x03\x55\x04\x06\x13\x02\x55\x53\x31\
		\x29\x30\x27\x06\x03\x55\x04\x0a\x13\x20\x49\x6e\x74\x65\x72\x6e\
		\x65\x74\x20\x53\x65\x63\x75\x72\x69\x74\x79\x20\x52\x65\x73\x65\
		\x61\x72\x63\x68\x20\x47\x72\x6f\x75\x70\x31\x15\x30\x13\x06\x03\
		\x55\x04\x03\x13\x0c\x49\x53\x52\x47\x20\x52\x6f\x6f\x74\x20\x58\
		\x31\
	";
	assert_eq!(cname_from_parse(mat), Some(&b"ISRG Root X1"[..]));
}
