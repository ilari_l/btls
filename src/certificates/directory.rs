use super::CertificateLookup;
use super::KeyStoreOperations;
use super::LocalServerCertificateHandle;
use super::LocalServerCertificateStore;
use crate::certificates::LocalKeyPair;
use crate::certificates::TrustedLog;
use crate::logging::Logging;
use crate::logging::MessageSeverity;
use crate::stdlib::Arc;
use crate::stdlib::AtomicBool;
use crate::stdlib::AtomicOrdering;
use crate::stdlib::Box;
use crate::stdlib::BTreeMap;
use crate::stdlib::Cow;
use crate::stdlib::Deref;
use crate::stdlib::Duration;
use crate::stdlib::IoRead;
use crate::stdlib::Mutex;
use crate::stdlib::Ordering;
use crate::stdlib::OsString;
use crate::stdlib::String;
use crate::stdlib::ToOwned;
use crate::stdlib::ToString;
use crate::stdlib::UNIX_EPOCH;
use crate::stdlib::Vec;
use crate::system::Builder;
use crate::system::File;
use crate::system::Path;
use crate::system::PathBuf;
use crate::system::read_dir;
use crate::system::sleep;
use btls_aux_certificate_transparency::TrustedLogInfo2;
use btls_aux_certificate_transparency_builtin::BuiltinLogData;
use btls_aux_filename::Filename;
use btls_aux_futures::FutureReceiver;
use btls_aux_inotify::InotifyEventSink;
use btls_aux_inotify::InotifyTrait;
use btls_aux_inotify::InotifyWatcher as _InotifyWatcher;
use btls_aux_inotify::MOVED;
use btls_aux_inotify::DELETED;
use btls_aux_memory::ByteStringLines;
use btls_aux_memory::filter_map_utf8;
use btls_aux_publiccalist::is_public_ca;
use btls_aux_signatures::Base64Decoder;
use btls_aux_signatures::CsrParams;
use btls_aux_signatures::iterate_pem_or_derseq_fragments;
use btls_aux_signatures::PemDerseqFragment2 as PFragment;
use btls_aux_signatures::PemFragmentKind as PFKind;
use btls_aux_time::Timestamp;
use btls_aux_x509certparse::ParsedCertificate2;
use core::fmt::Write as FmtWrite;


macro_rules! LOG
{
	(CONTINUE $logger:expr, $sev:ident, $($args:tt)*) => {{
		$logger.message_ex(None, MessageSeverity::$sev, format_args!($($args)*));
		continue
	}};
	($logger:expr, $sev:ident, $($args:tt)*) => {
		$logger.message_ex(None, MessageSeverity::$sev, format_args!($($args)*))
	}
}

type LoggerI = Box<dyn Logging+Send+Sync+'static>;
type Logger = Arc<LoggerI>;

struct CertificateInfo
{
	handle: LocalServerCertificateHandle,
	ocsp_timestamp: Option<i64>,
	scts_seen: BTreeMap<OsString, ()>,
	ocsp_loaded: bool,
}

impl CertificateInfo
{
	fn read_file_and_decode_pem(path: &Path, logger: Logger, allow_multi: bool) -> Result<Vec<u8>, ()>
	{
		let mut content = Vec::new();
		let pathd = Filename::from_path(path);
		File::open(&path).and_then(|mut fp|fp.read_to_end(&mut content)).map_err(|err|{
			LOG!(logger, Error, "Failed to read `{pathd}`: {err}")
		})?;
		//PEM-decode if needed.
		use PFragment::*;
		use PFKind::*;
		let mut certificates = 0usize;
		let mut content2 = Vec::new();
		iterate_pem_or_derseq_fragments(&content, |frag| -> Result<(), ()> { match frag {
			Comment(_) => Ok(()),
			Error => Err(LOG!(logger, Error, "File `{pathd}` is not in PEM nor DERseq format")),
			Der(cert)|Pem(Certificate, cert) => {
				content2.extend_from_slice(cert);
				certificates += 1;
				Ok(())
			},
			Pem(kind, _) => Ok(LOG!(logger, Warning, "File `{pathd}` contains unknown fragment {kind}")),
		}})?;
		//This won't triger above if the EE cert is not PEM.
		if !allow_multi && certificates != 1 {
			fail!(LOG!(logger, Error, "Expected `{pathd}` to contain one certificate"));
		}
		Ok(content2)
	}
	fn new(path: &Path, logger: Logger, mut inner: LocalServerCertificateStore) -> Result<CertificateInfo, ()>
	{
		let ee_path = path.join("certificate");
		let issuer_path = path.join("issuer");
		let end_entity = Self::read_file_and_decode_pem(&ee_path, logger.clone(), false)?;
		let issuer = Self::read_file_and_decode_pem(&issuer_path, logger.clone(), true)?;
		let pathd = Filename::from_path(path);
		let ipathd = Filename::from_path(&issuer_path);

		//Parse the EE certificate check if it is selfsigned.
		let eecert = ParsedCertificate2::from(&end_entity).map_err(|err|{
			let epathd = Filename::from_path(&ee_path);
			LOG!(logger, Error, "Failed to parse `{epathd}` as certificate: {err}")
		})?;
		//For selfsigned certificates, the issuer must be empty.
		if eecert.issuer.same_as(eecert.subject) && issuer.len() != 0 {
			fail!(LOG!(logger, Error, "Issuer chain `{ipathd}` not empty for self-signed certificate"));
		}
		//If issuer is public, then the issuer chain must not be empty. We do not need selfsigned check,
		//because selfsigned EE certs should not have the same subject as a public CA!
		if is_public_ca(eecert.issuer.as_raw_issuer()) && issuer.len() == 0 {
			fail!(LOG!(logger, Error, "Issuer chain `{ipathd}` empty for public certificate"));
		}
		//Contcatenate the EE cert and issuer chain.
		let mut certdata = Vec::new();
		certdata.extend_from_slice(&end_entity);
		certdata.extend_from_slice(&issuer);
		let mut certdata: &[u8] = &certdata;
		//There &mut &[u8] is a Read, so &mut &mut &[u8] is a &mut Read.
		let handle = match inner.add(&mut &mut certdata, &Filename::from_path(path).to_string()) {
			Ok(x) => {
				LOG!(logger, Notice, "Loaded certificate `{pathd}` as #{id}", id=x.get_id());
				x
			},
			Err(err) => fail!(LOG!(logger, Error, "Failed to load certificate '{pathd}': {err}"))
		};
		let mut certinfo = CertificateInfo {
			handle: handle,
			ocsp_timestamp: None,
			scts_seen: BTreeMap::new(),
			ocsp_loaded: false,
		};
		//On the first round, do update in order to load OCSP and SCTs.
		certinfo.update(path, logger);
		Ok(certinfo)
	}
	fn process_sct(&mut self, sctpath: &Path, logger: Logger)
	{
		let fname = f_return!(sctpath.file_name(), ());
		if self.scts_seen.contains_key(fname) { return; }	//Seen this one.
		self.scts_seen.insert(fname.to_owned(), ());
		let spath = Filename::from_path(sctpath);
		match self.handle.add_sct_staple_file(sctpath) {
			Ok(_) => LOG!(logger, Informational, "Added SCT {spath}"),
			Err(err) => LOG!(logger, Error, "Failed to add SCT {spath}: {err}"),
		}
	}
	fn update(&mut self, path: &Path, logger: Logger)
	{
		let mut seen_ocsp = false;
		let mut ocsp_timestamp = self.ocsp_timestamp;
		let pathd = Filename::from_path(path);
		let mut itr = f_return!(read_dir(path), F[|err|{
			LOG!(logger, Error, "Failed to read directory `{pathd}`: {err}")
		}]);
		while let Some(dir_entry) = itr.next() {
			let fpath = f_return!(dir_entry, F[|err|{
				LOG!(logger, Error, "Failed to read directory `{pathd}`: {err}")
			}]).path();
			let fpathd = Filename::from_path(&fpath);
			//We are interested about file called 'ocsp'.
			if fpath.file_name().and_then(|x|x.to_str()) == Some("ocsp") {
				//This is OCSP staple.
				seen_ocsp = true;
				//Get metadata on OCSP.
				let ocsp_metadata = f_return!(fpath.metadata(), F[|err|{
					LOG!(logger, Error, "Failed to get metadata for `{fpathd}`: {err}")
				}]);
				let ocsp_modify = match ocsp_metadata.modified() {
					Ok(x) => Some(match x.duration_since(UNIX_EPOCH) {
						Ok(y) => y.as_secs() as i64,
						Err(y) => -(y.duration().as_secs() as i64)
					}),
					Err(_) => None
				};
				//ocsp_modify == None means unknown. We reload in that case.
				if self.ocsp_timestamp == ocsp_modify && ocsp_modify.is_some() {
					continue;
				}
				ocsp_timestamp = ocsp_modify;
				match self.handle.set_ocsp_staple_file(&fpath) {
					Ok(_) => {
						LOG!(logger, Informational, "Updated OCSP for {pathd}");
						self.ocsp_loaded = true;
					},
					Err(err) => LOG!(logger, Error, "Reloading OCSP {fpathd}: {err}")
				};
			}
			//We are interested in '.sct' files.
			if let Some(x) = fpath.extension() {
				if x.to_str() == Some("sct") { self.process_sct(&fpath, logger.clone()); }
			}
		}
		if !seen_ocsp && self.ocsp_loaded {
			//No OCSP response, clear it.
			match self.handle.clear_ocsp_staple() {
				Ok(_) => self.ocsp_loaded = false,
				Err(err) => LOG!(logger, Error, "Clearing OCSP for {pathd}: {err}")
			};
		}
		self.ocsp_timestamp = ocsp_timestamp;
	}
}

fn join_links(src: &Path, dest: &Path) -> PathBuf
{
	if dest.is_absolute() { return dest.to_path_buf(); }
	let src_m1 = match src.parent() {
		Some(x) => x,
		None => return dest.to_path_buf()	//Hope for the best...
	};
	src_m1.join(dest)
}

fn convert_blacklist_to_utf8(bytes: Vec<u8>) -> String
{
	//First, try to take as UTF-8.
	let bytes = match String::from_utf8(bytes) {
		Ok(s) => return s,
		Err(e) => e.into_bytes(),	//Back to bytes.
	};
	//Take the non-empty lines that are valid UTF-8.
	let mut out = String::new();
	let lines = ByteStringLines::new(&bytes).filter_map(filter_map_utf8).filter(|line|line.len() > 0);
	for line in lines { writeln!(out, "{line}").ok(); }
	out
}

#[test]
fn test_blacklist_convert()
{
	let input = b"foo\r\nbar\xff\r\nbaz\r\n";
	let output = "foo\nbaz\n";
	assert!(convert_blacklist_to_utf8(input.to_vec()) == output);
}

struct DirectoryManager
{
	old_seen: BTreeMap<PathBuf, PathBuf>,
	logs_seen: BTreeMap<PathBuf, ()>,
	cert_handles: BTreeMap<PathBuf, (CertificateInfo, PathBuf)>,
	directory: PathBuf,
	blacklist: Option<PathBuf>,
	store: LocalServerCertificateStore,
	logger: Logger,
	loaded_keys: BTreeMap<PathBuf, ()>,
}

impl DirectoryManager
{
	fn new(directory: &Path, blacklist: Option<&Path>, mut inner: LocalServerCertificateStore,
		logger: Logger) -> Result<DirectoryManager, Cow<'static, str>>
	{
		let mut loaded_keys = BTreeMap::new();
		let mut keys_loaded = 0usize;
		let ts = Timestamp::now().unix_time();
		//Load all builtin logs (since there is no disadvantage about knowing a log).
		for log in BuiltinLogData::get_all_logs() {
			//Ignore any expired logs. Log data is only used for stapled SCTs, and those can not be from
			//expired logs.
			if log.get_expiry().map(|exp|exp < ts).unwrap_or(false) { continue; }
			let info = TrustedLog::from(log as &BuiltinLogData as &dyn TrustedLogInfo2);
			inner.add_trusted_log(&info);
			LOG!(logger, Notice, "Added new trusted CT log: {name}", name=info.name);
		}
		//None is mapped to none, Some is canonicalized.
		let blacklist = match blacklist.map(|bl|bl.canonicalize()) {
			Some(Ok(x)) => Some(x),
			Some(Err(err)) => fail!(format!("Failed to canonicalize blacklist path: {err}")),
			None => None,
		};
		let directory = directory.canonicalize().map_err(|err|{
			format!("Failed to canonicalize directory path: {err}")
		})?;
		let mut itr = read_dir(&directory).map_err(|err|format!("Failed to read directory: {err}"))?;
		while let Some(dir_entry) = itr.next() {
			let fpath = dir_entry.map_err(|err|format!("Failed to read directory: {err}"))?.path();
			let fpathd = Filename::from_path(&fpath);
			//We are only interested in '.key' files (and ctlog files).
			let extension = f_continue!(fpath.extension());
			if extension.to_str() != Some("key") { continue; }
			//Okay, try to load this.
			let fstem = f_continue!(fpath.file_stem());
			let keyname = fstem.to_string_lossy();
			let key = match LocalKeyPair::new_file(&fpath) {
				Ok(x) => x,
				Err(err) => LOG!(CONTINUE logger, Error, "Failed to load private key {fpathd}: {err}")
			};
			inner.add_privkey(key, keyname.deref());
			LOG!(logger, Notice, "Loaded private key `{fpathd}`");
			//Okay, loaded a key. The keys take memory, so this is enough not to overflow.
			loaded_keys.insert(fpath.to_path_buf(), ());
			keys_loaded += 1;
		}
		if keys_loaded == 0 {
			fail!(Cow::Borrowed("No private keys found or could be loaded"));
		}
		let mut  mgr = DirectoryManager {
			old_seen: BTreeMap::new(),
			cert_handles: BTreeMap::new(),
			logs_seen: BTreeMap::new(),
			directory: directory,
			store: inner,
			blacklist: blacklist,
			logger: logger,
			loaded_keys: loaded_keys,
		};
		//The first pass on certificates is the same as update.
		mgr.update(true);
		Ok(mgr)
	}
	fn load_late_key(&mut self, fpath: &Path)
	{
		let fpathd = Filename::from_path(fpath);
		let keyname = f_return!(fpath.file_stem(), ()).to_string_lossy();
		if self.loaded_keys.contains_key(fpath) { return; }
		let key = f_return!(LocalKeyPair::new_file_synchronous(&fpath), F[|err|{
			LOG!(self.logger, Error, "Failed to load private key {fpathd}: {err}")
		}]);
		self.store.add_privkey(key, keyname.deref());
		LOG!(self.logger, Notice, "Loaded private key `{fpathd}`");
		self.loaded_keys.insert(fpath.to_path_buf(), ());
	}
	fn get_path(&self) -> PathBuf { self.directory.clone() }
	fn scan_files_into_array(&self) -> Result<(BTreeMap<PathBuf, PathBuf>, Vec<PathBuf>, Vec<PathBuf>), ()>
	{
		let logger = self.logger.clone();
		//Read the blacklist file.
		let blacklist = if let Some(blacklist) = self.blacklist.as_ref() {
			let mut content = Vec::new();
			if let Err(err) = File::open(blacklist).and_then(|mut fp|fp.read_to_end(&mut content)) {
				LOG!(logger, Warning, "Failed to read blacklist file: {err}");
			}
			let content = convert_blacklist_to_utf8(content);
			let mut blset = BTreeMap::new();
			for line in content.lines() { blset.insert(line.to_owned(), ()); }
			blset
		} else {
			BTreeMap::new()
		};
		let mut seen = BTreeMap::<PathBuf, PathBuf>::new();
		let mut newlogs = Vec::new();
		let mut newkeys = Vec::new();
		let mut itr = read_dir(&self.directory).map_err(|err|{
			LOG!(logger, Error, "Failed to read directory: {err}");
		})?;
		while let Some(dir_entry) = itr.next() {
			let fpath = dir_entry.map_err(|err|{
				LOG!(logger, Error, "Faile to read directory: {err}");
			})?.path();
			let fpathd = Filename::from_path(&fpath);
			if blacklist.contains_key(fpath.to_string_lossy().deref()) {
				continue;	//In blacklist, ignore.
			}
			//We are only interested in '.key', '.crt' and 'ctlog' files.
			match fpath.extension() {
				Some(x) => {
					if x.to_str() == Some("key") && !self.loaded_keys.contains_key(&fpath) {
						//Okay, this is a new key, load it.
						newkeys.push(fpath.to_path_buf());
					}
					if x.to_str() == Some("ctlog") && !self.logs_seen.contains_key(&fpath) {
						//Okay, this is a new log, load it.
						newlogs.push(fpath.to_path_buf());
					}
					if x.to_str() != Some("crt") { continue; }
				},
				None => continue	//Just ignore this.
			};
			//Okay, try to load the target.
			let slinktype = match fpath.symlink_metadata() {
				Ok(x) => x.file_type(),
				Err(err) => LOG!(CONTINUE logger, Error,
					"Failed to read (symlink) metadata for `{fpathd}`: {err}")
			};
			let target = if slinktype.is_symlink() {
				//This is a symbolic link.
				match fpath.read_link() {
					Ok(x) => join_links(&fpath, &x),
					Err(err) => LOG!(CONTINUE logger, Error,
						"Failed to read symlink target for `{fpathd}`: {err}")
				}
			} else if slinktype.is_file() {
				//This is a regular file.
				let mut content = String::new();
				match File::open(&fpath).and_then(|mut fp|fp.read_to_string(&mut content)) {
					Ok(x) => x,
					Err(err) => LOG!(CONTINUE logger, Error,
						"Failed to read file `{fpathd}`: {err}")
				};
				if content.len() == 0 {
					LOG!(CONTINUE logger, Error,
						"File `{fpathd}` is not valid reference: File is empty");
				}
				let mut dclone = self.directory.clone();
				dclone.push(&content);
				dclone
			} else {
				//Other, error.
				LOG!(CONTINUE logger, Error, "'{fpathd}' is not a symbolic link nor regular file");
			};
			//Ok, now the target is in variable target. Check it is a directory.
			let targetd = Filename::from_path(&target);
			match target.metadata() {
				Ok(x) => if x.file_type().is_dir() {
					//Record this.
					seen.insert(fpath, target);
				} else {
					LOG!(logger, Error, "`{targetd}` (target of `{fpathd}`) is not a directory");
				},
				Err(err) => LOG!(logger, Error,
					"Failed to read metadata for `{targetd}` (target of `{fpathd}`): {err}"),
			}
		}
		Ok((seen, newlogs, newkeys))
	}
	fn _del_handle(h: LocalServerCertificateHandle, logger: &mut Logger)
	{
		let id = h.get_id();
		h.delete();
		logger.message_ex(None, MessageSeverity::Notice, format_args!("Unloaded certificate #{id}"));
	}
	fn call_with_pair(name: &Path, old: Option<&Path>, new: Option<&Path>, mut logger: Logger,
		inner: LocalServerCertificateStore, cert_handles: &mut BTreeMap<PathBuf, (CertificateInfo, PathBuf)>)
		-> bool
	{
		match (old, new) {
			(Some(_old), Some(_new)) => {
				if _old == _new {
					//Reread entry.
					match cert_handles.get_mut(name) {
						Some(x) => x.0.update(_new, logger.clone()),
						None => return true	//The entry is still there.
					};
				} else {
					//Full reload of entry.
					let old_handle = cert_handles.get(name).map(|x|x.0.handle.clone());
					let new_handle = match CertificateInfo::new(_new, logger.clone(),
						inner.clone()) {
						Ok(x) => x,
						Err(_) => {
							//The old entry is still deleted.
							if let Some(h) = old_handle {
								Self::_del_handle(h, &mut logger);
							}
							cert_handles.remove(name);
							return false;
						}
					};
					//Swap the entry.
					match cert_handles.get_mut(name) {
						//The direct path is new (unwrapped as _new).
						Some(x) => *x = (new_handle, _new.to_path_buf()),
						None => {
							Self::_del_handle(new_handle.handle, &mut logger);
							return false;	//This should have existed!
						}
					};
					if let Some(h) = old_handle { Self::_del_handle(h, &mut logger); }
				}
			},
			(Some(_old), None) => {
				//Entry got deleted.
				match cert_handles.remove(name) {
					Some(x) => Self::_del_handle(x.0.handle, &mut logger),
					None => return true	//Should not happen. False is only for new cert
								//failures.
				};
			},
			(None, Some(_new)) => {
				//New entry got created. The direct path is new (unwrapped as _new).
				match CertificateInfo::new(_new, logger.clone(), inner.clone()) {
					Ok(x) => cert_handles.insert(name.to_path_buf(), (x, _new.to_path_buf())),
					Err(_) => return false
				};
			},
			(None, None) => {
				//Should not happen.
			}
		}
		return true;
	}
	fn process_pairs(&mut self, seen: &BTreeMap<PathBuf, PathBuf>, logger: Logger) -> BTreeMap<PathBuf, ()>
	{
		let mut to_delete = BTreeMap::<PathBuf, ()>::new();
		let mut itr_old = self.old_seen.iter();
		let mut itr_new = seen.iter();
		let mut entry_old = itr_old.next();
		let mut entry_new = itr_new.next();
		loop {
			//The step flags are:
			//Bit 0: Old entry should be stepped.
			//Bit 1: New entry should be stepped.
			//Bit 2: If set with bit 1, the new entry should be deleted.
			let step = if let &(Some((ref eold, ref trgold)), Some((ref enew, ref trgnew))) =
				&(entry_old, entry_new) {
				//Both old and new lists still have entries.
				match eold.cmp(enew) {
					//<. This means that new is ahead of old. We should process this as
					//deletion, since there is no replacement.
					Ordering::Less => {
						Self::call_with_pair(eold, Some(trgold), None, logger.clone(),
							self.store.clone(), &mut self.cert_handles);
						1u32	//advance old
					},
					//=. This means new and old are equal. We should process this as update.
					//If entry should not exist, then delete it.
					Ordering::Equal => if Self::call_with_pair(eold, Some(trgold), Some(trgnew),
						logger.clone(), self.store.clone(), &mut self.cert_handles) {
						3u32	//advance both
					} else {
						7u32	//advance both, delete new.
					},
					//>. This means new is behind old. We should process this as add.
					//If entry should not exist, then delete it.
					Ordering::Greater => if	Self::call_with_pair(enew, None, Some(trgnew),
						logger.clone(), self.store.clone(), &mut self.cert_handles) {
						2u32	//advance new
					} else {
						6u32	//advance new, delete new.
					},
				}
			} else if let &Some((ref eold, ref trgold)) = &entry_old {
				//Only the old list still has entries. We should process this as deletion, as
				//there can be no replacement.
				Self::call_with_pair(eold, Some(trgold), None, logger.clone(), self.store.clone(),
					&mut self.cert_handles);
				1u32			//advance old.
			} else if let &Some((ref enew, ref trgnew)) = &entry_new {
				//Only the new list still has entries. We should process this as addition, as
				//there can be no old entry. If entry should not exist, then delete it.
				if Self::call_with_pair(enew, None, Some(trgnew), logger.clone(), self.store.clone(),
					&mut self.cert_handles) {
					2u32		//advance new
				} else {
					6u32		//advance new, delete new.
				}
			} else {
				break;					//All entries processed.
			};
			//This is only valid if new entry is valid.
			if let (6, &Some((ref wname, _))) = (step & 6, &entry_new) {
				to_delete.insert(wname.to_path_buf(), ());
			}
			if (step & 1) != 0 { entry_old = itr_old.next(); }
			if (step & 2) != 0 { entry_new = itr_new.next(); }
		}
		to_delete
	}
	fn process_log(&mut self, name: &Path)
	{
		let named = Filename::from_path(name);
		let mut content = String::new();
		f_return!(File::open(name).and_then(|mut fp|fp.read_to_string(&mut content)), F[|err|{
			LOG!(self.logger, Error, "Failed to add CT log file `{named}`: Failed to read: {err}");
		}]); 
		let mut parts = content.split('\n').collect::<Vec<_>>();
		while parts.len() > 0 && parts[parts.len()-1] == "" { parts.pop(); }
		let (lname, b64key) = if parts.len() == 2 {
			//v1 log (2 parts)
			(parts[0].to_owned(), parts[1])
		} else {
			return LOG!(self.logger, Error, "Failed to add CT log file `{named}`: Unrecognized format");
		};
		//base64 decode the key.
		let mut key = Vec::new();
		let mut decoder = Base64Decoder::new();
		f_return!(decoder.data(b64key, &mut key).and_then(|_|decoder.end(&mut key)), F[|err|{
			LOG!(self.logger, Error,
				"Failed to add CT log file `{named}`: Failed to decode public key: {err}")
		}]);
		//Construct and add.
		let newlog = TrustedLog {
			name: lname,
			key: key,
			expiry: None,		//These logs always have no expiry
		};
		self.store.add_trusted_log(&newlog);
		LOG!(self.logger, Notice, "Added new trusted CT log: {name}", name=newlog.name);
		self.logs_seen.insert(name.to_path_buf(), ());
	}
	fn update(&mut self, major: bool)
	{
		//If not performing major scan, perform update on every certificate.
		if !major {
			for (_, &mut (ref mut cert, ref path)) in self.cert_handles.iter_mut() {
				cert.update(path.deref(), self.logger.clone());
			};
			return;
		}

		//Error is used to signal directory too unreliable to scan.
		let (mut seen, newlogs, newkeys) = f_return!(self.scan_files_into_array(), ());
		//Add all new logs found.
		for log in newlogs.iter() { self.process_log(&log); }
		//Add new keys. Note that this has to happen before process_pairs, since we need the keys to be
		//loaded.
		for key in newkeys.iter() { self.load_late_key(&key); }
		let logger2 = self.logger.clone();
		let to_delete = self.process_pairs(&seen, logger2);
		//Remove certs that failed to load, so we will retry next iteration.
		for i in to_delete.iter() { seen.remove(i.0); }
		//Finally, make this seen the base for next iteration.
		self.old_seen = seen;
	}
}

struct InotifySink
{
	logger: Logger,
	flag: Arc<AtomicBool>,
}

impl InotifyEventSink for InotifySink
{
	//These are almost always bad news, so use error.
	fn log_message(&self, msg: &str)
	{
		LOG!(self.logger, Error, "{msg}");
	}
	fn inotify_event(&self, name: &[u8], event: u32) -> bool
	{
		//We are interested of events that are for .ctlog or .crt files,
		//and are moves or deletes.
		let is_ctlog = name.ends_with(b".ctlog");
		let is_crt = name.ends_with(b".crt");
		let is_signal = name == "rescan".as_bytes();
		//Match?
		let is_match = (is_ctlog || is_crt || is_signal) && event & (MOVED | DELETED) != 0;
		if is_match { self.flag.store(true, AtomicOrdering::Relaxed); }
		is_match
	}
}

struct InotifyWatcher
{
	inner: Option<_InotifyWatcher>,		//Option because INOTIFY may not be supported.
	debug: Logger,
	flag: Arc<AtomicBool>,
}

impl InotifyWatcher
{
	fn new<P:AsRef<Path>>(p: P, log: Logger) -> InotifyWatcher
	{
		let flag = Arc::new(AtomicBool::new(false));
		let ctrl = Box::new(InotifySink{logger: log.clone(), flag: flag.clone()});
		let inner = match _InotifyWatcher::new(p, ctrl, MOVED | DELETED) {
			Ok(w) => {
				LOG!(log, Informational, "Certificate directory inotify poller armed");
				Some(w)
			},
			Err(err) => {
				LOG!(log, Error, "Error arming certificate directory inotify poller: {err}");
				None
			}
		};
		InotifyWatcher{ inner: inner, debug: log, flag: flag }
	}
	fn wait(&mut self) -> bool
	{
		match self.inner.as_mut() {
			Some(ref mut inner) => {
				//Wait for event for maximum of 10min.
				inner.wait(Some(600000));
				//On activity, exit this loop in order to rescan the directory.
				if self.flag.swap(false, AtomicOrdering::Relaxed) {
					LOG!(self.debug, Notice, "Rescanning certificate directory due to activity");
					return true;
				}
				false
			},
			None => {
				//Just sleep for 5min to conserve CPU.
				sleep(Duration::from_secs(300));
				true
			}
		}
	}
}

///A certificate store loaded from a directory
///
///This implements the certificate lookup trait [`CertificateLookup`]. The certificates and keys to resolve
///requests are taken from the contents of the specified directory. Since it is a certificate store, it also
///implements the [`KeyStoreOperations`](trait.KeyStoreOperations.html) trait.
///
///The files this store takes into account are:
///
/// * `*.key`: On startup, these files are passed to `LocalKeyPair` for loading. At least 1 key must load
///successfully.
/// * `*.crt`: These files are scanned on startup and periodically afterwards. These files must be symbolic
///links into a directory. The contents of these directories are loaded as certificates.
///    * If the symbolic link is added, a new certificate is loaded.
///    * If the symbolic link is modified, the new certificate is loaded and the old certificate is unloaded.
///    * If the symbolic link is deleted, the old certificate is deleted.
/// * `*.crt/certificate`: The end-entity certificate. Note that this must not have the issuer chain.
/// * `*.crt/issuer`: The issuer chain. Note that this must be always be present. If certificate is self-signed, this
///must be empty. Otherwise it must be non-empty.
/// * `*.crt/ocsp`: These files are on startup and periodically loaded to serve as OCSP responses.
///    * If the file is created, the next pass will add OCSP staple for the certificate.
///    * If the file is modified, the next pass will update OCSP staple for the certificate.
///    * If the file is deleted, the next pass will clear OCSP staple for the certificate.
/// * `*.crt/*.sct`: These files are on startup and periodically loaded as SCT staples. Note that modifying or
///deleting these files does nothing.
/// * `*.ctlog`: On startup and periodically, these files are loaded as trusted CT logs.
///
///# `*.ctlog` format:
///
/// The `*.ctlog` files consist of 2 or 3 lines:
///
/// * The first line is the log name.
/// * The second line is Base64 (not base64url) encoding of the SPKI-format log public key.
/// * The third line is only present for v2 logs. It is in format `<oid>/<hash>`, where:
///   * `<oid>` is the textual OID of the log.
///   * `<hash>` is he hash function log uses. Currently only `sha256` is supported.
///
///# Inotify support:
///
///If inotify is supported by the OS (Linux), then rescan is automatically triggered if a `*.ctlog` or `*.crt`
///file is renamed or deleted. Creation or modification does not count: You are expected to only create or
///modify such files by renaming them (the standard rename-as-atomic-overwrite trick).
///
///# Notes:
///
/// * All symbolic links to directories in the same directory can be replaced by regular files holding just
///the name of directory.
/// * Due to race conditions, only replace symbolic links with symbolic links and regular file references with
///regular file references while this code is running.
//
///This struct implements trait [`CertificateLookup`], so it can be used as certificate lookup.
///
///Yes, really: The `::new()` function is all there is to this class.
///
///[`CertificateLookup`]: trait.CertificateLookup.html
#[derive(Clone)]
pub struct CertificateDirectory
{
	inner: LocalServerCertificateStore,
	//This reference exists solely to keep DirectoryManager alive as the only strong reference.
	//There is also weak reference that is actually used after upgrading.
	_manager: Arc<Mutex<DirectoryManager>>,
}

impl CertificateDirectory
{
	///The same as `CertificateDirectory::new_blacklist(path, None, log)`.
	pub fn new<L:Logging+Send+Sync+'static, P:AsRef<Path>>(path: P, log: L) ->
		Result<CertificateDirectory, Cow<'static, str>>
	{
		//PathBuf is just some Sized type that can fill the AsRef<Path> bound.
		CertificateDirectory::new_blacklist::<L,P,PathBuf>(path, None, log)
	}
	///Create a new certificate store that loads certificates from directory `path` and logs the messages
	///into log `log`. The file to read blacklisted paths from is `blacklist` (if none, there is no blacklist).
	///
	///On success, returns `Ok(lookup)`, where `lookup` is the newly created certificate lookup. Otherwise
	///returns `Err(err)`, where `err` is the error message.
	pub fn new_blacklist<L:Logging+Send+Sync+'static, P:AsRef<Path>, P2:AsRef<Path>>(path: P,
		blacklist: Option<P2>, log: L) -> Result<CertificateDirectory, Cow<'static, str>>
	{
		let inner = LocalServerCertificateStore::new();
		let log = Arc::new(Box::new(log) as LoggerI);
		let blacklist = blacklist.as_ref().map(|bl|bl.as_ref());
		let directory = path.as_ref();
		let (ipath, state) = match DirectoryManager::new(directory, blacklist, inner.clone(), log.clone()) {
			Ok(x) => {
				let ipath = x.get_path();
				(ipath, Arc::new(Mutex::new(x)))
			},
			Err(err) => {
				let directory = Filename::from_path(directory);
				let errmsg = format!("Failed to load certificates from directory `{directory}`: \
					{err}");
				LOG!(log, Error, "{errmsg}");
				fail!(Cow::Owned(errmsg));
			}
		};
		//Because the above succeeded, the directory presumably exists. So this presumably only fails if
		//the OS does not support inotify, and we fall back in that case.
		let mut inotifyobj = InotifyWatcher::new(&ipath, log.clone());
		let state2 = state.clone();
		let b = Builder::new();
		let b = b.name(String::from("Cert updater"));
		match b.spawn(move || {		//Drop the handle to floor (and we can't report errors).
			let _state = Arc::downgrade(&state);
			drop(state);	//Dispose the strong reference.
			let mut major;	//First round is always major.
			loop {
				major = inotifyobj.wait();
				let _state = f_break!(_state.upgrade()); //The store is gone, exit loop.
				_state.with(|x|x.update(major));
			}
		}) {
			Ok(_) => (),
			Err(err) => {
				let errmsg = format!("Internal error: Failed to start directory updater thread: \
					{err}");
				LOG!(log, Error, "{errmsg}");
				fail!(Cow::Owned(errmsg));
			}
		}
		Ok(CertificateDirectory {
			inner: inner,
			_manager: state2,
		})
	}
	///Get certificate lookup corresponding to this store.
	pub fn lookup(&self) -> CertificateLookup
	{
		self.inner.lookup()
	}
}

impl KeyStoreOperations for CertificateDirectory
{
	fn enumerate_private_keys(&self) -> Vec<(String, String)>
	{
		KeyStoreOperations::enumerate_private_keys(&self.inner)
	}
	fn get_public_key(&self, privkey: &str) -> Option<Vec<u8>>
	{
		KeyStoreOperations::get_public_key(&self.inner, privkey)
	}
	fn sign_csr(&self, privkey: &str, csr_params: &CsrParams) -> FutureReceiver<Result<Vec<u8>, ()>>
	{
		KeyStoreOperations::sign_csr(&self.inner, privkey, csr_params)
	}
}
