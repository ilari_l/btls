use self::controller::HandshakeController;
#[doc(hidden)] pub use self::controller::ReferenceNameOwned;
use crate::Connection;
use crate::callbacks::TlsCallbacks;
use crate::certificates::HostSpecificPin;
use crate::certificates::TrustAnchor;
use crate::certificates::TrustedLog;
use crate::common::builtin_killist;
use crate::common::FutureFinishedNotifier;
use crate::common::handle_blacklist;
use crate::common::handle_ctlog;
use crate::common::handle_hs_timelimit;
use crate::common::handle_ocsp_maxvalid;
use crate::common::handle_trustanchor;
use crate::errors::Error;
use crate::features::ALLOW_BAD_CRYPTO_NAME;
use crate::features::ALLOW_PKCS1_CLIENT;
use crate::features::ALLOW_PKCS1_SIG;
use crate::features::ConfigPages;
use crate::features::ConfigFlagsError;
use crate::features::ConfigFlagsErrorKind;
use crate::features::ConfigFlagsValue;
use crate::features::DEFAULT_FLAGS;
use crate::features::ENABLE_TLS13_NAME;
use crate::features::split_config_str;
use crate::features::TLS13_DRAFTS;
use crate::logging::DebugSetting;
use crate::logging::Logging;
use crate::record::Session;
use crate::stdlib::Arc;
use crate::stdlib::Box;
use crate::stdlib::Duration;
use crate::stdlib::min;
use crate::stdlib::Mutex;
use crate::stdlib::String;
use crate::stdlib::ToOwned;
use crate::stdlib::Vec;
use btls_aux_collections::BTreeSet;
use btls_aux_memory::TrustedInvariantLength;
use btls_aux_tls_iana::AlpnMaskBits;
use btls_aux_tls_iana::AlpnProtocolId;
use btls_aux_x509certvalidation::TrustAnchorOrdered;


mod controller;

#[derive(Clone)]
pub(crate) enum AlpnProtocol
{
	Standard(AlpnProtocolId<'static>),
	Custom(String),
}

#[allow(unsafe_code)] unsafe impl TrustedInvariantLength for AlpnProtocol
{
	fn as_byte_slice(&self) -> &[u8]
	{
		//This structure has no interrior mutability, which is strictly stronger guarantee than needed
		//for TrustedInvariantLength.
		match self {
			AlpnProtocol::Standard(x) => x.as_byte_slice(),
			AlpnProtocol::Custom(ref x) => x.as_byte_slice(),
		}
	}
}

///Client configuration.
///
///This structure contains various parts of the client configuration.
///
///# Creating the client configuration object.
///
///The client configuration object is created using the `ClientConfiguration::new()` method. Note that by default
///there are no trust anchors, which causes all handshakes to fail (see "Trust anchors" below).
///
///# Trust anchors:
///
///Trust anchors are CA root certificates that are trusted to verify other intermediate and end-entity certificates.
///Name constraints in root certificates are respected. Note that trust anchors have to be CA certificates
///(`BasicConstraints.CA=true`), end-entity certificates can not be made into trust anchors.
///
///Trust anchors are added using the [`add_trust_anchor()`](#method.add_trust_anchor) method. By default there are
///no trust anchors.
///
///One can "burn" certificate into the executable image as follows:
///
///```no_run
///let mut talist = Vec::new();
///let cert = include_bytes!("../certificate/test3.der");
///let cert = TrustAnchor::from_certificate(&mut &cert[..])?;
///clientconfig.add_trust_anchor(&cert);
///```
///
///Or load certificates from external files like:
///
///```no_run
///let mut talist = Vec::new();
///clientconfig.add_trust_anchor(&TrustAnchor::from_certificate_file("ca1.crt")?);
///clientconfig.add_trust_anchor(&TrustAnchor::from_certificate_file("ca2.crt")?);
///```
///
///# Host specific pins:
///
///Another source of certificate trust besides trust anchors is host specific pins. These can be used to implement
///DANE (DNS Authentication of Named Entities; RFC6698) or HPKP (HTTP Public Key Pinning; RFC7469). In contrast to
///trust anchors, the end entity certificates or even keys can be declared trusted. Host-specific pins are specified
///by creating the connection using the [`make_connection_pinned()`](#method.make_connection_pinned) method.
///
///If host-specific pin that covers the end-entity key is specified, then raw public key support is enabled: The
///client hello specifies both X.509 and Raw Public Key certificates as acceptable).
///
///# OCSP maximum validity:
///
///The OCSP maximum validity determines the longest lifetime allowed for OCSP response. If the lifetime is longer
///than the limit, the OCSP response is not treated as valid and does not fullfill requirement to staple OCSP, if
///such requirement is present (via configuration or having OCSP must-staple in the certificate). The OCSP maximum
///validity can be set using the [`set_ocsp_maxvalidity()`](#method.set_ocsp_maxvalidity) method.
///
///Also, if the certificate lifetime is less than the OCSP maximum validity, it is treated like it had stapled OCSP,
///even if not actually present. This also fulfills requirement to staple OCSP (weither via configuration or via
///certificate extension.
///
///# Configuration flags:
///
///Configuration flags are various binary options that configure the behavior of the system. The flags can default
///to either set or unset. The flags can be then configured to be cleared or set. However, some flags only affect
///server and some exist just for backward compatibility, and actually do nothing.
///
///The flags can be confgured using the [`set_flags()`](#method.set_flags) and [`clear_flags()`](#method.clear_flags)
///methods. Additionally, the configuration strings configured using the [`config_flags()`](#method.config_flags)
///method can set or clear flags.
///
///The flags are divided into pages. Currently there are 5 pages:
///
/// * Page #0 contains various flags affecting TLS library behavior.
/// * Page #1 contains the enabled key exchanges.
/// * Page #2 contains the enabled hash algorithms.
/// * Page #3 contains the enabled encryption algorithms.
/// * Page #4 contains the enabled signature algorithms.
///
///See the documentation of the [`features`](features/index.html) module for more documentation about what various
///flags do.
///
///# ALPN (Application Layer Protocol Negotiation)
///
///To enable ALPN, specify one or more protocols using the [`add_alpn()`](#method.add_alpn) method. By default no
///ALPN extension is advertised. If one or more protocols has been specified, then the ALPN extension is sent
///containing those protocols.
///
///# Debugging mode
///
///The debugging mode is controlled by the [`set_debug()`](#method.set_debug) method. This method takes a
///debugging event mask of events to enable and a sink to send the debugging events to.
///
///The following types of debugging events are available (defined in module [`logging`]):
///
/// * `DEBUG_ABORTS`: Debugging message on connection abort event.
/// * `DEBUG_CRYPTO_CALCS`: Debugging message on cryptographic key calculations (requires `enable-key-debug` feature
///when compiling).
/// * `DEBUG_HANDSHAKE_DATA`: Debugging message on received or sent handshake messages, complete with message
///contents.
/// * `DEBUG_HANDSHAKE_EVENTS`: Debugging message on various events (like rekeyings or things being verified) when
///handshaking.
/// * `DEBUG_HANDSHAKE_MSGS`: Debugging message on received or sent handshake messages, but without message contents.
/// * `DEBUG_TLS_EXTENSIONS`: Debugging messages on TLS extensions being received.
///
///# Certificate Transparency
///
///One can add trusted certificate transparency logs using the [`add_ct_log()`](#method.add_ct_log) method. Note that
///since there are no auditing capabilities, the logs are operated in pure countersignature mode. Also, as
///consequence, all Certificate Transparency items except Signed Certificate Timestamps are ignored. Also,
///signature from any trusted log is deemed sufficient to fullfill requirement for Certificate Transparency
///(specified either via configuration or certificate).
///
///# Certificate blacklisting
///
///Certificates can be blacklisted by SHA-256 hash of their public key by using the
///[`blacklist()`](#method.blacklist) method. If a blacklisted key is encountered in handshake, the handshake will
///fail.
///
///# Making session
///
///When you have it all set up, you can get client end of connection using the
///[`make_connection()`](#method.make_connection) method.
///
///```no_run
///let mut session = config.make_connection(hostname, callbacks);
///```
///
///Or if you want to specify host-specific pins:
///
///```no_run
///const SHAHASH1: [u8; 32] = [66;32/*...*/];
///const SHAHASH2: [u8; 32] = [77;32/*...*/];
///let pin1 = HostSpecificPin::trust_server_key_by_sha256(&SHAHASH1, false, true);
///let pin2 = HostSpecificPin::trust_server_key_by_sha256(&SHAHASH2, false, true);
///let pins = [pin1, pin2];
///let mut session = config.make_connection_pinned(hostname, &pins[..], callbacks);
///```
///
///This is useful for implementing DANE (RFC6698) or HPKP (RFC7469). Or if you want to "just test" without "valid"
///CA certificate.
///
///# Configuration examples:
///
///```no_run
///config.set_flags(0, FLAGS0_REQUIRE_EMS);
///```
///
///This sets Extended Master Secret (or TLS 1.3, which is equivalent) to be required. This is useful if you are
///going to use exporters, since those do not work without Extended Master Secret. If the server does not support
///Extended Master Secret (nor TLS 1.3) then the handshake will fail.
///
///```no_run
///config.clear_flags(0, FLAGS0_ENABLE_TLS12);
///```
///
///This disables TLS 1.2 support, leaving just TLS 1.3 supported.
///
///
///```no_run
///let ct_log = TrustedLog{...};
///config.add_ct_log(&ct_log);
///```
///
///This adds specified Certificate Transparency log to list of trusted CT logs.
///
///```no_run
///config.set_flags(0, FLAGS0_REQUIRE_CT);
///```
///
///This marks certificate transparency as required. If the server does not present at least one Signed Certificate
///Timestamp from trusted log, the connection will fail.
///
///```no_run
///config.set_flags(0, FLAGS0_REQUIRE_OCSP);
///```
///
///This marks OCSP stapling as required. If the server does not present OCSP staple for the end-entity certificate
///(or the end-entity certificate itself lives shorter than the OCSP limit), the handshake will fail.
///
///
///[`logging`]: logging/index.html
pub struct ClientConfiguration
{
	//Trust anchor set.
	trust_anchors: BTreeSet<TrustAnchorOrdered>,
	//Supported ALPN's, in order of decreasing perference.
	supported_alpn: Vec<AlpnProtocol>,
	supported_alpn_mask: AlpnMaskBits,
	//Flags pages.
	flags: ConfigPages,
	//Logging config.
	log: DebugSetting,
	//Certificate Killist.
	killist: BTreeSet<[u8; 32]>,
	//Trusted logs.
	trusted_logs: Vec<TrustedLog>,
	//OCSP maximum validitiy.
	ocsp_maxvalid: u64,
	//Handshake time limit.
	handshake_time_limit: Option<Duration>,
	//Raw handshake mode.
	raw_handshake: bool,
	//Minimum client hello size.
	min_ch_size: usize,
	//Supress SNI.
	suppress_sni: bool,
}

impl Clone for ClientConfiguration
{
	fn clone(&self) -> ClientConfiguration
	{
		ClientConfiguration {
			trust_anchors:self.trust_anchors.clone(),
			supported_alpn:self.supported_alpn.clone(),
			supported_alpn_mask:self.supported_alpn_mask.clone(),
			flags: self.flags.clone(),
			log:self.log.clone(),
			killist: self.killist.clone(),
			trusted_logs: self.trusted_logs.clone(),
			ocsp_maxvalid: self.ocsp_maxvalid.clone(),
			handshake_time_limit: self.handshake_time_limit.clone(),
			raw_handshake: self.raw_handshake.clone(),
			min_ch_size: self.min_ch_size.clone(),
			suppress_sni: self.suppress_sni.clone(),
		}
	}
}


impl ClientConfiguration
{
	pub(crate) fn borrow_alpn(&self) -> (&[AlpnProtocol], AlpnMaskBits)
	{
		(&self.supported_alpn, self.supported_alpn_mask)
	}
	pub(crate) fn flags0(&self) -> u64 { self.flags.get_flags() }
	pub(crate) fn flags(&self) -> &ConfigPages { &self.flags }
	///Return a default client configuration with empty trust anchor list.
	///
	///Note that in order to successfully create a session out of this, you need to use
	///[`make_connection_pinned()`](#method.make_connection_pinned)
	///specifying at least one pin with TA flag set (`trust_server_key_by_sha256` and `from_tlsa_raw` (when
	///first byte is 2 or 3) constructors both set the TA flag) or add at least one trust anchor using the
	///[`add_trust_anchor()`](#method.add_trust_anchor) method.
	pub fn new() -> ClientConfiguration
	{
		ClientConfiguration {
			trust_anchors: BTreeSet::new(),
			supported_alpn: Vec::new(),
			supported_alpn_mask: AlpnMaskBits::new(),
			flags: DEFAULT_FLAGS,
			log: DebugSetting::new(),
			killist: builtin_killist(),
			trusted_logs: Vec::new(),
			ocsp_maxvalid: 7 * 60 * 60 * 24,	//7 days.
			handshake_time_limit: None,
			raw_handshake: false,
			min_ch_size: 0,
			suppress_sni: false,
		}
	}
	///Add a trust anchor to trust pile.
	///
	///This method adds trust anchor `anchor` to the trust anchor pile. This pile is used for verifying server
	///certificates.
	///
	///Trust anchors can also be added via the `trustanchor=` option in configuration string (see the method
	///[`config_flags`](#method.config_flags)).
	///
	///The default trust pile contains any trust anchors set when creating the configuration object.
	pub fn add_trust_anchor(&mut self, anchor: &TrustAnchor)
	{
		self.trust_anchors.insert(anchor.as_ordered());
	}
	////Set the OCSP maximum validity limit.
	///
	///This method sets the OCSP maximum validity limit `limit` (expressed in seconds).
	///
	///This limit is used in two ways:
	///
	/// - If OCSP response lives longer than this limit, the OCSP response is not considered valid.
	/// - If certificate lives at most this limit, no OCSP response is ever required. This is even if
	///OCSP required flag is set, or if certificate is marked must-staple.
	///
	///This limit can also be modified by `ocsp-maxvalid=` option in configuration string (see the method
	///[`config_flags`](#method.config_flags)).
	///
	///The default value is 604,800 seconds (i.e., 7 days).
	pub fn set_ocsp_maxvalidity(&mut self, limit: u64) { self.ocsp_maxvalid = limit; }
	///Add supported ALPN.
	///
	///Add a new least-preferred ALPN value `alpn` as supported.
	///
	///When ALPN value is marked as supported, it is sent to the server for possible choice. Then ALPN values
	///are sent in the order added.
	///
	///The default is no ALPN values recognized (do not send ALPN extension).
	pub fn add_alpn(&mut self, alpn: &str)
	{
		self.supported_alpn.push(if let Some((std,bit)) = AlpnProtocolId::get_standard(alpn.as_bytes()) {
			self.supported_alpn_mask.add(bit);
			AlpnProtocol::Standard(std)
		} else {
			AlpnProtocol::Custom(alpn.to_owned())
		})
	}
	///Set flags.
	///
	///This method sets flags specified by bitmask `mask` on configuration page `page`.
	///
	///Flags can affect various options in server behavior. See module [`features`] for the flag constants.
	///
	///If nonexistent page is specified, this method does nothing.
	///
	///[`features`]: features/index.html
	pub fn set_flags(&mut self, page: usize, mask: u64)
	{
		self.flags.set_flags(page, mask);
	}
	///Clear flags.
	///
	///This method clears flags specified by bitmask `mask` on configuration page `page`.
	///
	///Flags can affect various options in server behavior. See module [`features`] for the flag constants.
	///
	///If nonexistent page is specified, this method does nothing.
	///
	///[`features`]: features/index.html
	pub fn clear_flags(&mut self, page: usize, mask: u64)
	{
		self.flags.clear_flags(page, mask);
	}
	///Set/Clear flags according to config string.
	///
	///This method sets or clears configuration options according to configuration string `config` (presumably
	///read from configuration file). If any errors are encountered during processing, the error callback
	///`error_cb` is called to report those errors (possibly multiple times).
	///
	///The configuration string consists of comma-separated elements, with each element being one of:
	///
	/// * flagname -> Enable flag named `flagname`.
	/// * +flagname -> Enable flag named `flagname`.
	/// * -flagname -> Disable flag named `flagname`.
	/// * !flagname -> Disable flag named `flagname`.
	/// * setting=value -> Set setting `setting` to value `value`.
	///
	///Use backslashes to escape backslash or comma in the value.
	///
	///# The following settings are supported:
	///
	/// * `blacklist` -> Blacklist specified SPKI hash (specified as 64 hexadecimal digit representation of
	///SHA-256 hash of the SubjectPublicKeyInfo).
	/// * `trustanchor` -> Read the specified file in DER (possibly concatenated from multiple certificates) or
	///PEM format as series of trust anchors to add.
	/// * `ctlog` -> Read the specified file as series of CT logs (one per line) to add. If prefixed with ?,
	///read specified internal log (`*` means all, that is, one can add all internal logs with `ctlog=?*`).
	/// * `ocsp-maxvalid` -> Set the OCSP maximum validity limit in seconds.
	/// * `handshake-deadline` -> Set handshake deadline in seconds (or empty value disables the limit).
	///
	///# Format of ctlog files:
	///
	///ctlog files consists of one or more lines, with each line containing a trusted CT log. Each line has one
	///of two forms:
	///
	/// * `v1:<key>:<expiry>:<name>`
	/// * `v2:<id>/<hash>:<key>:<expiry>:<name>`
	///
	///Where:
	///
	/// * `<id>` is base64 encoding of DER encoding the log OID, without the DER tag or length.
	/// * `<hash>` is the log hash function. Currently only `sha256` is supported.
	/// * `<key>`is base64 encoding of log key formatted as X.509 SubjectPublicKeyInfo.
	/// * `<expiry>` is the time log expired in seconds since Unix epoch, or blank if log has not expired yet.
	///Certificate-stapled SCTs are accepted up to expiry date from expired logs, other kinds of SCTs are not
	///accepted.
	/// * `<name>` is the name of the log.
	pub fn config_flags<F>(&mut self, config: &str, mut error_cb: F) where F: FnMut(ConfigFlagsError)
	{
		split_config_str(self, config, |objself, name, value, entry| {
			let bad = if name == "blacklist" {
				config_strval!(handle_blacklist, value, &mut error_cb, entry, &mut objself.killist)
			} else if name == "trustanchor" {
				config_strval!(handle_trustanchor, value, &mut error_cb, entry, &mut objself.
					trust_anchors)
			} else if name == "ctlog" {
				config_strval!(handle_ctlog, value, &mut error_cb, entry, &mut objself.
					trusted_logs)
			} else if name == "ocsp-maxvalid" {
				config_strval!(handle_ocsp_maxvalid, value, &mut error_cb, entry, &mut objself.
					ocsp_maxvalid)
			} else if name == "handshake-deadline" {
				config_strval!(handle_hs_timelimit, value, &mut error_cb, entry, &mut objself.
					handshake_time_limit)
			} else {
				false
			};
			if bad {
				return error_cb(ConfigFlagsError{
					entry:entry,
					kind:ConfigFlagsErrorKind::ArgumentRequired(name)
				});
			}
			//All the remaning ones are flags.
			let bad = match value {
				//Some flags no longer have an effect or are not supported client side.
				ConfigFlagsValue::Disabled if name == ENABLE_TLS13_NAME =>
					return error_cb(ConfigFlagsError{
						entry:entry,
						kind:ConfigFlagsErrorKind::NoEffect(name)
					}),
				ConfigFlagsValue::Enabled if name == ALLOW_BAD_CRYPTO_NAME ||
					name == ALLOW_PKCS1_SIG || name == TLS13_DRAFTS ||
					name == ALLOW_PKCS1_CLIENT =>
					return error_cb(ConfigFlagsError{
						entry:entry,
						kind:ConfigFlagsErrorKind::NoEffect(name)
					}),
				ConfigFlagsValue::Explicit(_) => return error_cb(ConfigFlagsError{
					entry:entry,
					kind:ConfigFlagsErrorKind::NoArgument(name)
				}),
				ConfigFlagsValue::Disabled => !objself.flags.clear_flag_by_name(name),
				ConfigFlagsValue::Enabled => !objself.flags.set_flag_by_name(name),
			};
			if bad {
				return error_cb(ConfigFlagsError{
					entry:entry,
					kind:ConfigFlagsErrorKind::UnrecognizedSetting(name)
				});
			}
		});
	}
	///Set debugging mode.
	///
	///This method enables debugging mode, with debug event mask of `mask` (constructed from bitwise OR of
	///various `DEBUG_*` constatnts from module [`logging`]) and debug sink `log`.
	///
	///Note that enabling `DEBUG_CRYPTO_CALCS` class requires a build with `enable-key-debug` cargo feature,
	///otherwise that debugging event is just ignored.
	///
	///[`logging`]: logging/index.html
	pub fn set_debug(&mut self, mask: u64, log: Box<dyn Logging+Send+Sync>)
	{
		self.log.set_debug(mask, Arc::new(log));
	}
	///Add a new trusted certificate transparency log.
	///
	///This method adds certificate transprancy log `log` to the list of trusted certificate transparency logs.
	///This list is used for verifying certificate transparency proofs in server certificates.
	///
	///CT logs can also be added via the `ctlog=` option in configuration string (see the method
	///[`config_flags`](#method.config_flags)).
	///
	///The default is not to have any trusted CT logs.
	pub fn add_ct_log(&mut self, log: &TrustedLog) { self.trusted_logs.push(log.clone()); }
	///Add a new certificate key blacklist entry.
	///
	///This method adds a blacklist entry for certificate key hash `spkihash` (a 32-byte SHA-256 hash of
	///certificate SubjectPublicKeyInfo).
	///
	///If the key hash of any sent client certificate chain entry matches any hash on the blacklist, the
	///certificate validation is rejected and connection fails.
	///
	///Additionally, if trust anchor matches blacklist entry, that trust anchor is not used.
	///
	///Default is not to have any blacklist entries.
	pub fn blacklist(&mut self, spkihash: &[u8])
	{
		let mut x = [0; 32];
		if spkihash.len() != x.len() { return; }
		x.copy_from_slice(spkihash);
		self.killist.insert(x);
	}
	///Set the handshake deadline to `x`. If the handshake has not finished by that time, the application is
	///signaled to tear down the connection (see callback `set_deadline()`).
	///
	///Passing `Some(x)` sets deadline to `x` from creation of the connection context, and passing `None`
	///disables the deadline. By default, deadline is disabled.
	///
	///The deadline can also be modified via the `handshake-deadline=` option in configuration string (see the
	///method [`config_flags`](#method.config_flags)).
	pub fn set_deadline(&mut self, x: Option<Duration>) { self.handshake_time_limit = x; }
	///Set raw handshake mode status to `x`.
	///
	///If raw handshake mode is enabled, then TLS will only run the TLS handshake protocol. The I/O to/from
	///wire will be handshake messages, including the 4-byte message header. Any errors are promptly returned
	///as failures from read/write functions.
	///
	///If raw handshake mode is enabled, then no data is accepted and no data is ever produced. Also,
	///end-of-stream can not be sent or received. Any encryption/decryption keys are passed back to
	///appplication via callbacks.
	pub fn set_raw_handshake(&mut self, x: bool) { self.raw_handshake = x; }
	///Set minimum ClientHello size to `x-4`.
	///
	///If the ClientHello would be smaller than this limit, is is padded using Padding extension. Default is 0,
	///which always sends smallest ClientHello. This setting is capped at 16376 (`x=16380`).
	///
	///The `-4` means the `x` is actually the size including the message header.
	pub fn set_min_client_hello_size(&mut self, x: usize)
	{
		self.min_ch_size = min(x.saturating_sub(4), 16376);
	}
	///Set suppress SNI flag to `x`
	///
	///When suppress SNI flag is set, the server_name extension is not sent. However, the hostname passed is
	///still used in certificate checks.
	pub fn set_suppress_sni(&mut self, x: bool) { self.suppress_sni = x; }
	///Make a new connection.
	///
	///This methods creates a new client connection to host with name `host` (this name is sent as SNI), using
	///callback methods `callbacks`.
	pub fn make_connection(&self, host: &str, callbacks: Box<dyn TlsCallbacks+Send>) -> ClientConnection
	{
		let controller = HandshakeController::new(&self, host, self.log.clone(), callbacks);
		self.make_connection_tail(controller)
	}
	///Make a new connection with host-specific pins.
	///
	///This method creates a new client connection to host with name `host` (this name is sent as SNI) and
	///host-specific pins `pins`, using callback methods `callbacks`.
	///
	///This is useful for implementing DANE (RFC6698) and HPKP (RFC7469)
	pub fn make_connection_pinned(&self, host: &str, pins: &[HostSpecificPin],
		callbacks: Box<dyn TlsCallbacks+Send>) -> ClientConnection
	{
		let controller = HandshakeController::new_pinned(&self, host, pins, self.log.clone(), callbacks);
		self.make_connection_tail(controller)
	}
	///Create a connection for checking ACME TLS-ALPN challenge.
	///
	///Uses callback methods `callbacks`. However, the callbacks are restricted. Only I/O, abort and complete
	///callbacks are used. The ACME key authorization hash is `kahash`. The host to connect to is `host`.
	pub fn make_connection_acme_check(&self, host: &str, callbacks: Box<dyn TlsCallbacks+Send>,
		kahash: [u8;32]) -> ClientConnection
	{
		let controller = HandshakeController::new_acme_check(&self, self.log.clone(), callbacks, host,
			kahash);
		self.make_connection_tail(controller)
	}
	fn make_connection_tail(&self, controller: HandshakeController) -> ClientConnection
	{
		let mut sess = Session::new(self.raw_handshake, controller, self.log.clone());
		//Since client goes first, immediately do the first TX cycle.
		sess.do_tx_request();
		//Ok.
		ClientConnection(Arc::new(Mutex::new(sess)))
	}
}


///The client side of TLS connection.
///
///Use [`ClientConfiguration::make_connection()`] or [`ClientConfiguration::make_connection_pinned()`] to create it.
///
///For operations supported, see trait [`Connection`].
///
///If the connection is cloned, both connections refer to the same underlying TLS connection. This can be used to
///refer to the same connection from multiple threads at once.
///
///[`ClientConfiguration::make_connection()`]: struct.ClientConfiguration.html#method.make_connection
///[`ClientConfiguration::make_connection_pinned()`]: struct.ClientConfiguration.html#method.make_connection_pinned
///[`ClientConfiguration`]: struct.ClientConfiguration.html
///[`Connection`]: trait.Connection.html
#[derive(Clone)]
pub struct ClientConnection(Arc<Mutex<Session<HandshakeController>>>);

impl_connection_connection!(ClientConnection, false);
impl_session_tls_tx_rx_cycle!(ClientConnection);

impl ClientConnection
{
	///Hint: Add a share for groupid `grpid`.
	///
	///This is just a hint, and can not fail. However, for this to take effect, this must be called before
	///trying to write any data.
	pub fn hint_group(&self, grpid: u16)
	{
		self.0.with(|x|x.controller_mut().hint_group(grpid));
	}
}
