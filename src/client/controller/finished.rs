use super::BaseMessageParams;
use super::BaseMessageParamsS;
use super::Flags;
use super::HandshakeState2;
use super::Tls12ShowtimeFields;
use super::Tls13C2ndFSecrets;
use super::Tls13HsSecrets;
use super::Tls13ShowtimeFields;
use super::TlsClientCert;
use super::certificate::Tls13ClientCertificateFields;
use crate::callbacks::KeyExportType as KET;
use crate::callbacks::ServerNamesParameters;
use crate::callbacks::TlsCallbacks;
use crate::common::test_failure;
use crate::common::tls13_update_recv_key;
use crate::common::tls13_update_send_key_base;
use crate::common::ValidateExtraTag;
use crate::errors::Error;
use crate::features::FLAGS5_TEST_CFIN_SEND_FAIL;
use crate::features::FLAGS5_TEST_SFIN_RECV_FAIL;
use crate::logging::DebugSetting;
use crate::messages::HandshakeHash;
use crate::messages::HashMessages;
use crate::messages::tx_finished;
use crate::record::DowncallInterface;
use crate::record::UpcallReturn;
use crate::record::Tls12MasterSecret;
use crate::stdlib::Arc;
use crate::stdlib::Deref;
use crate::stdlib::String;
use crate::stdlib::Vec;


pub(crate) struct Tls12ClientFinishedFields
{
	pub master_secret: Tls12MasterSecret,		//The master secret.
	pub hs_hash: HandshakeHash,
	pub server_names: Option<Arc<Vec<String>>>,
	pub server_ips: Option<Arc<Vec<String>>>,
	pub flags: Flags,
}

pub(crate) struct Tls12ServerFinishedFields
{
	pub master_secret: Tls12MasterSecret,		//The master secret.
	pub hs_hash: HandshakeHash,
	pub server_names: Option<Arc<Vec<String>>>,
	pub server_ips: Option<Arc<Vec<String>>>,
	pub flags: Flags,
}

pub(crate) struct Tls13ServerFinishedFields
{
	pub hs_secrets: Tls13HsSecrets,
	pub hs_hash: HandshakeHash,
	pub cc: TlsClientCert,
	pub server_names: Option<Arc<Vec<String>>>,
	pub server_ips: Option<Arc<Vec<String>>>,
	pub cert_tag: ValidateExtraTag,
	pub client_random: [u8;32],
	pub flags: Flags,
}

pub(crate) struct Tls13ClientFinishedFields
{
	pub hs_secrets2: Tls13C2ndFSecrets,
	pub hs_hash: HandshakeHash,
	pub flags: Flags,
}

impl Tls13ServerFinishedFields
{
	//Handle received Finished message.
	pub fn rx_finished(self, controller: &mut UpcallReturn, msg: &[u8], bp: BaseMessageParams) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParams{rawmsg, config, debug, ssl_key_log, callbacks, ..} = bp;
		test_failure(&config.flags, FLAGS5_TEST_SFIN_RECV_FAIL)?;

		//Check that certificate has been duly validated.
		sanity_check!(self.cert_tag.is_done(), "Improper server certificate validation");

		//The finished message just consists of the MAC, so no need to parse it.
		self.hs_secrets.server_hs_write.check_finished(&self.hs_hash.checkpoint(), msg, ks_debug!(&debug))?;
		//Release server names. This will not do anything useful if there was no chain validation.
		if !self.flags.is_NO_DATA_TRANSFER() {
			call_server_names(self.server_names, self.server_ips, callbacks);
		}

		let mut hs_hash = self.hs_hash;
		hs_hash.add(rawmsg, debug.clone())?;

		let hs_hash = hs_hash;
		let app_hash = hs_hash.checkpoint();
		let (server_app_write, client_app_write, extractor) = self.hs_secrets.master_secret.
			to_traffic_secrets(&app_hash, ks_debug!(&debug))?;
		if ssl_key_log {
			callbacks.ssl_key_log(KET::ServerApplicationTrafficSecret0, self.client_random,
				server_app_write.debug_export());
			callbacks.ssl_key_log(KET::ClientApplicationTrafficSecret0, self.client_random,
				client_app_write.debug_export());
			callbacks.ssl_key_log(KET::ExporterMasterSecret, self.client_random,
				extractor.debug_export());
		}
		tls13_update_recv_key(controller, &server_app_write, config.raw_handshake, debug.clone(),
			callbacks)?;
		controller.set_extractor(extractor);
		debug!(HANDSHAKE_EVENTS debug, "Finished: received, rekeying downstream");
		let keys = Tls13C2ndFSecrets {
			client_app_write: client_app_write,
			client_hs_write: self.hs_secrets.client_hs_write,
			server_app_write: server_app_write,
		};

		let ret = if self.flags.is_REQUESTED_CERTIFICATE() {
			HandshakeState2::Tls13ClientCertificate(Tls13ClientCertificateFields {
				hs_secrets2: keys,
				hs_hash: hs_hash,
				cc: self.cc,
				flags: self.flags,
			})
		} else {
			HandshakeState2::Tls13ClientFinished(Tls13ClientFinishedFields {
				hs_secrets2: keys,
				hs_hash: hs_hash,
				flags: self.flags,
			})
		};
		Ok(ret)
	}
}

impl Tls13ClientFinishedFields
{
	//Transmit a Finished message.
	pub fn tx_finished(self, base: &mut impl DowncallInterface, bp: BaseMessageParamsS) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParamsS{debug, config, sname, callbacks, ..} = bp;
		test_failure(&config.flags, FLAGS5_TEST_CFIN_SEND_FAIL)?;
		if self.flags.is_COMPATIBILITY_MODE() {
			//Send fake CCS.
			base.send_clear_ccs();
		}
		let mut hs_hash = self.hs_hash;
		tx_finished(&self.hs_secrets2.client_hs_write, &mut hs_hash, base, debug.clone(), sname)?;
		tls13_update_send_key_base(base, &self.hs_secrets2.client_app_write, config.raw_handshake,
			debug.clone(), callbacks)?;
		debug!(HANDSHAKE_EVENTS debug, "Finished: Sent, rekeying upstream");
		//Set close to use severity=2.
		base.set_alerts_always_fatal();

		//Callbacks.
		if !self.flags.is_NO_DATA_TRANSFER() {
			callbacks.set_deadline(None);	//Deadline cleared on end of handshake.
		} else {
			//This connection can not be used to transport data, so force bidirectional close.
			base.force_bidir_close();
		}

		//hs_hash at this point is the final handshake hash, e.g. for RMS.
		Ok(HandshakeState2::Tls13Showtime(Tls13ShowtimeFields {
			client_app_write: self.hs_secrets2.client_app_write,
			server_app_write: self.hs_secrets2.server_app_write,
			flags: self.flags,
		}))
	}
}

impl Tls12ClientFinishedFields
{
	//Transmit a Finished message.
	pub fn tx_finished(self, base: &mut impl DowncallInterface, bp: BaseMessageParamsS) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParamsS{debug, sname, ..} = bp;
		base.send_clear_ccs();
		base.change_protector(self.master_secret.get_protector(ks_debug!(&debug))?);
		debug!(HANDSHAKE_EVENTS debug, "ChangeCipherSpec protocol: Sent, upstream encrypted");
		//The CCS message is not hashed.

		let mut hs_hash = self.hs_hash;
		tx_finished(&self.master_secret, &mut hs_hash, base, debug.clone(), sname)?;
		base.change_extractor(self.master_secret.get_exporter(ks_debug!(&debug))?);

		//We do not do can_send_data, because the server has not authenticated itself yet, and we do
		//not want to send to who knows who.
		Ok(HandshakeState2::Tls12ServerChangeCipherSpec(Tls12ServerChangeCipherSpecFields{
			master_secret: self.master_secret,
			hs_hash: hs_hash,
			server_names: self.server_names,
			server_ips: self.server_ips,
			flags: self.flags,
		}))
	}
}

fn call_server_names(list: Option<Arc<Vec<String>>>, list_ip: Option<Arc<Vec<String>>>,
	callbacks: &mut dyn TlsCallbacks)
{
	let list_dns: Option<&[String]> = list.as_deref().map(Deref::deref);
	let list_ip: Option<&[String]> = list_ip.as_deref().map(Deref::deref);
	if list_dns.is_none() && list_ip.is_none() { return; }
	callbacks.server_names(ServerNamesParameters {
		dns_names: list_dns.unwrap_or(&[]),
		ip_addresses: list_ip.unwrap_or(&[]),
	});
}

impl Tls12ServerFinishedFields
{
	//Handle received Finished message.
	pub fn rx_finished(self, controller: &mut UpcallReturn, msg: &[u8], bp: BaseMessageParams) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParams{rawmsg, debug, callbacks, ..} = bp;
		let mut hs_hash = self.hs_hash;
		//The finished message just consists of the MAC, so no need to parse it.
		self.master_secret.check_finished(&hs_hash.checkpoint(), ks_debug!(&debug), msg)?;

		//Ok, release server names.
		if !self.flags.is_NO_DATA_TRANSFER() {
			call_server_names(self.server_names, self.server_ips, callbacks);
		}

		//The final handshake hash.
		hs_hash.add(rawmsg, debug)?;

		//Callbacks.
		if !self.flags.is_NO_DATA_TRANSFER() {
			callbacks.set_deadline(None);	//Deadline cleared on end of handshake.
		} else {
			//This connection can not be used to transport data, so force bidirectional close.
			controller.force_bidir_close();
		}

		Ok(HandshakeState2::Tls12Showtime(Tls12ShowtimeFields{
		}))
	}
}

pub(crate) struct Tls12ServerChangeCipherSpecFields
{
	pub master_secret: Tls12MasterSecret,		//The master secret.
	pub hs_hash: HandshakeHash,
	pub server_names: Option<Arc<Vec<String>>>,
	pub server_ips: Option<Arc<Vec<String>>>,
	pub flags: Flags,
}

impl Tls12ServerChangeCipherSpecFields
{
	//Handle received ChangeCipherSpec message.
	pub fn rx_change_cipher_spec(self, controller: &mut UpcallReturn, debug: DebugSetting)
		-> Result<HandshakeState2, Error>
	{
		//Not real handshake message, thus not added to the hash.
		controller.set_deprotector(self.master_secret.get_deprotector(ks_debug!(&debug))?);
		debug!(HANDSHAKE_EVENTS debug, "ChangeCipherSpec protocol: Received, downstream encrypted");
		Ok(HandshakeState2::Tls12ServerFinished(Tls12ServerFinishedFields{
			master_secret: self.master_secret,
			hs_hash: self.hs_hash,
			server_names: self.server_names,
			server_ips: self.server_ips,
			flags: self.flags,
		}))
	}
}
