use super::BaseMessageParams;
use super::Flags;
use super::HandshakeState2;
use super::ReferenceNameOwned;
use super::Tls13HsSecretsPre;
use super::TlsCVInfo;
use crate::callbacks::KeyExportType as KET;
use crate::callbacks::TargetInfoParameters;
use crate::client::controller::certificate::Tls12CertificateFields;
use crate::client::controller::clienthello::ClientHelloFields;
use crate::client::controller::encryptedextensions::Tls13EncryptedExtensionsFields;
use crate::common::Ciphersuite;
use crate::common::CollectedScts;
use crate::common::CryptoTracker;
use crate::common::test_failure;
use crate::common::tls13_update_recv_key;
use crate::common::tls13_update_send_key;
use crate::common::TlsVersion;
use crate::errors::Error;
use crate::features::FLAGS0_ENABLE_TLS12;
use crate::features::FLAGS0_MIDDLEBOX_COMPAT;
use crate::features::FLAGS0_NO_SPONTANEOUS_REKEY;
use crate::features::FLAGS0_REQUIRE_EMS;
use crate::features::FLAGS5_TEST_SH_RECV_FAIL;
use crate::messages::DumpHexblock;
use crate::messages::EarlyHandshakeRep;
use crate::messages::HandshakeMessage;
use crate::messages::HashMessages;
use crate::messages::KemKeyPair;
use crate::record::DiffieHellmanSharedSecretInitiator;
use crate::record::Exportable;
use crate::record::HasAd;
use crate::record::Tls13HandshakeSecret;
use crate::record::UpcallReturn;
use crate::stdlib::Deref;
use crate::stdlib::ToOwned;
use crate::stdlib::Vec;
use crate::test::yesno;
use btls_aux_dhf::KEM;
use btls_aux_dhf::kem_ciphertext_check2;
use btls_aux_random::TemporaryRandomLifetimeTag;
use btls_aux_random::TemporaryRandomStream;
use btls_aux_tls_iana::CertificateType;
use btls_aux_tls_iana::HandshakeType;
use btls_aux_tls_iana::NamedGroup;
use btls_aux_tls_iana::TlsVersion as IanaTlsVersion;
use btls_aux_tls_struct::ServerHello as PServerHello;
use btls_aux_tlsciphersuites::KeyExchangeType;
use core::fmt::Arguments;


pub(crate) struct ServerHelloFields
{
	//Information to validate the server certificate.
	pub cvinfo: TlsCVInfo,
	//The TLS features supported.
	pub features: u32,
	//The key shares that were generated for TLS 1.3. These can be reused for TLS 1.2 key exchange if the
	//algorithm is symmetric (e.g., Diffie-Hellman).
	pub tls13_shares: Vec<KemKeyPair>,
	//Ciphersuite that has been already assigned. This happens if HelloRetryRequest (or its equivalent) is
	//received in TLS 1.3-draft19 and above.
	pub ciphersuite_assigned: Option<Ciphersuite>,
	//Version that has been already assigned. This happens with HelloRetryRequest (or its equivalen) is
	//received.
	pub version_assigned: Option<TlsVersion>,
	//The handshake hash so far. This is using special form, because the hash function is not necressarily
	//known.
	pub hs_hash: EarlyHandshakeRep,
	//The group identifier received in HelloRetryRequest, if any.
	pub group_id: Option<NamedGroup>,
	//The client random.
	pub client_random: [u8; 32],
	//The active cryptographic algorithms.
	pub crypto_algs: CryptoTracker,
	//ACME hash, if any.
	pub acme_hash: Option<[u8;32]>,
	//Client capabilities.
	pub(crate) client_caps: crate::messages::ClientSupport,
	//The flags for handshake.
	pub flags: Flags,
}

//Old compatibility code.
#[doc(hidden)]
pub struct TlsHelloRetryRequest<'a>
{
	///The version number
	pub version: TlsVersion,
	///Ciphersuite (-draft19 only).
	pub ciphersuite: Option<Ciphersuite>,
	///Key share group.
	pub kex_kem: Option<NamedGroup>,
	///Cookie.
	pub cookie: Option<&'a [u8]>,
	///Nontrivial session.
	pub nt_session: bool,
}

impl ServerHelloFields
{
	//Handle KeyShare extension.
	fn handle_ext_keyshare<'a>(keyshare: (KEM, &[u8]), tls13_shares: &mut Vec<KemKeyPair>, version: TlsVersion,
		xkem_id: Option<NamedGroup>, crypto_algs: &mut CryptoTracker) ->
		Result<DiffieHellmanSharedSecretInitiator, Error>
	{
		//This can only be present in TLS 1.3, so enable TLS 1.3 mode. the EMS and RENEGO features are
		//always considered to be present for TLS 1.3.
		crypto_algs.set_version(version);

		let kem = keyshare.0;
		let pubkey = keyshare.1;
		//The group MUST match the previously known one (received in HelloRetryRequest), if any.
		let bad_group = NamedGroup::new(0);
		fail_if!(xkem_id.is_some() && xkem_id != Some(kem.tls_id2()),
			error!(key_share_group_disagreement xkem_id.unwrap_or(bad_group), kem.tls_id2()));
		//Find the correct key and perform key exchange with it. If no such key is found, that is an error,
		//because it means the server selected a group we did not send a share for.
		{
			for KemKeyPair(sk, pk) in tls13_shares.drain(..) {
				if pk.kem() != kem { continue; }
				//This one is the correct key. Perform key exchange and add it to secret chain as
				//(EC)DH key. The PSK must be zero block. Then return the resulting shared key.
				let result = DiffieHellmanSharedSecretInitiator::new(sk, pubkey)?;
				return Ok(result);
			}
			//The key was not found, this only happens if server sends a public key with bogus group.
			sanity_failed!("Trying to perform key exchange with bogus KEM {kem}", kem=kem.tls_id2());
		}
	}
	//Process a received hello_retry_request (including wrapped one).
	pub fn handle_hello_retry_request(self, emsg: TlsHelloRetryRequest, bp: BaseMessageParams, msg: &[u8],
		hs_type: HandshakeType) -> Result<HandshakeState2, Error>
	{
		let BaseMessageParams{debug, ..} = bp;
		let mut ciphersuite_assigned = None;	//There can only be one HRR.
		let mut cookie = None;
		let mut hs_hash = self.hs_hash;
		let mut tls13_shares = self.tls13_shares;
		let mut crypto_algs = self.crypto_algs;
		let version = emsg.version;

		//HelloRetryRequest is only possible in TLS 1.3. We do not need to check for versions even older
		//because those are rejected at message parse time.
		match emsg.version {
			TlsVersion::Tls12 => fail!(error!(bogus_hrr_tls_version IanaTlsVersion::TLS_1_2)),
			_=> (),
		};

		debug!(HANDSHAKE_EVENTS debug, "Handshake restart: TLS version {ver}", ver=version.to_code());

		//If ciphersuite information is available, record the ciphersuite and initialize the handshake hash.
		//This happens for TLS 1.3-draft19 and newer.
		if let Some(csobj) = emsg.ciphersuite {
			let hash = csobj.get_prf2();
			hs_hash.convert_to_hash(hash, debug.clone(), true)?;
			ciphersuite_assigned = Some(csobj);
			crypto_algs.set_ciphersuite(csobj);
			debug!(HANDSHAKE_EVENTS debug, "Handshake restart: Ciphersuite is {csobj:?}");
		}

		//This flag signinfies that HelloRetryRequest caused some nontrivial change in ClientHello.
		let mut had_cookie_or_something = false;

		//If cookie is present, include it in next ClientHello.
		if let Some(cookiex) = emsg.cookie {
			cookie = Some(cookiex.to_owned());
			had_cookie_or_something = true;
			debug!(HANDSHAKE_EVENTS debug, "Handshake restart: Received cookie, ok");
		}
		//The server may also give the group id to use. Note, this must occur last of the change checks,
		//since if there was some other change, then group already present is allowed, but not otherwise.
		if let Some(grp_id) = emsg.kex_kem {
			//If we already have a key for the correct group, extract it, discarding the rest. The
			//new ClientHello should only have one group.
			let mut _dhkey = None;
			for KemKeyPair(sk, pk) in tls13_shares.drain(..) {
				if pk.kem().tls_id2() == grp_id {
					//Hit!
					_dhkey = Some((sk, pk));
					break;
				}
			}
			debug!(HANDSHAKE_EVENTS debug, "Handshake restart: Key exchange is {grp_id}");
			//If no key exchange key was found, generate a new one.
			let _dhkey = if let Some(x) = _dhkey {
				//Found a suitable key. If no other changes have occured as result of this message,
				//the server should have accepted the previous ClientHello, so sending new
				//essentially unchanged one will not work.
				fail_if!(!had_cookie_or_something, error!(hrr_already_had_share grp_id));
				x
			} else {
				//No suitable key found, generate a new one. This is TLS 1.3, so we are always the
				//inititator.
				let kem = KEM::by_tls_id2(grp_id, self.client_caps.get_enabled_kem()).ok_or_else(||{
					assert_failure!(F "Parsed hello_retry_request contains bad KEM {grp_id}")
				})?;
				let lifetime = TemporaryRandomLifetimeTag;
				assert_some!(kem.generate_key(&mut TemporaryRandomStream::new_system(&lifetime)), F
					"Can't generate Diffie-Hellman keypair for group {grp_id}")
			};
			//Clear all the keys already present and replace with just the correct key.
			tls13_shares.clear();
			tls13_shares.push(KemKeyPair::pair(_dhkey));
		}
		//.add() handles both the case where the hash has been initialized or not correctly. Note that the
		//message type of this message may be either server_hello or hello_retry_request, depending on the
		//TLS 1.3 draft.
		hs_hash.add(HandshakeMessage::new(hs_type, msg), debug.clone())?;

		//Next up, send client_hello again.
		Ok(HandshakeState2::RetryClientHello(ClientHelloFields{
			hs_hash: hs_hash,
			cookie: cookie,
			tls13_shares: tls13_shares,
			ciphersuite_assigned: ciphersuite_assigned,
			version_assigned: Some(emsg.version),
			features: self.features,
			group_id: self.group_id,
			client_random: self.client_random,
			cvinfo: self.cvinfo,
			crypto_algs: crypto_algs,
			acme_hash: self.acme_hash,
			client_caps: self.client_caps,
			flags: self.flags.fforce_COMPATIBILITY_MODE(emsg.nt_session),
		}))
	}
	fn __rx_hello_retry_request(self, pmsg: btls_aux_tls_struct::HelloRetryRequest, msg: &[u8],
		bp: BaseMessageParams) -> Result<HandshakeState2, Error>
	{
		let cs = pmsg.get_cipher_suite();
		let en_cs = self.client_caps.get_enabled_cs();
		let ciphersuite = Ciphersuite::by_tls_id4(cs, en_cs).ok_or_else(||{
			assert_failure!(F "Parsed hello_retry_request contains bad cipher {cs}")
		})?;
		let server_version = TlsVersion::by_tls_id(pmsg.get_tls_version(), false)?;
		let reconstructed = TlsHelloRetryRequest{
			//The cookie received.
			cookie: pmsg.get_cookie(),
			//The ciphersuite received. This may or may not be valid, depending on the TLS 1.3
			//draft used.
			ciphersuite: Some(ciphersuite),
			//The TLS version.
			version: server_version,
			//The key exchange group used, if any.
			kex_kem: pmsg.get_group(),
			//The message contained a nontrivial session ID, compatibility mode should be
			//activated.
			nt_session: pmsg.get_session_id().len() > 0
		};
		//This message is always SERVER_HELLO in type.
		let msg_type = HandshakeType::SERVER_HELLO;
		self.handle_hello_retry_request(reconstructed, bp, msg, msg_type)
	}
	fn __rx_server_hello_tls12(mut self, controller: &mut UpcallReturn,
		pmsg: btls_aux_tls_struct::ServerHelloTls12, bp: BaseMessageParams) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParams{callbacks, rawmsg, config, debug, ..} = bp;
		test_failure(&config.flags, FLAGS5_TEST_SH_RECV_FAIL)?;
		//Check that nobody is downgrading to TLS 1.2 after HRR.
		fail_if!(self.version_assigned.is_some(), error!(version_mismatch_hrr_sh));
		//Check that version has not been disabled. TLS 1.2 is hard disabled in raw handshake mode.
		fail_if!(!config.flags.is_flag(FLAGS0_ENABLE_TLS12) || config.raw_handshake,
			error!(sh_unsupported_tls_version IanaTlsVersion::TLS_1_2));
		//No need for downgrade check, it already has been done in message parse.
		let random = pmsg.get_random();
		debug!(CRYPTO_CALCS debug, "Server hello: Server random:\n{random}",
			random=DumpHexblock::new(&random, "  ", "Server random"));
		//Read the ciphersuite and initialize PRF.
		let cs = pmsg.get_cipher_suite();
		let en_cs = self.client_caps.get_enabled_cs();
		let ciphersuite = Ciphersuite::by_tls_id4(cs, en_cs).ok_or_else(||{
			assert_failure!(F "Parsed server_hello contains bad cipher {cs}")
		})?;
		fail_if!(ciphersuite.get_key_exchange() == KeyExchangeType::Tls13, error!(sh_bad_tls12_cipher cs));
		debug!(HANDSHAKE_EVENTS debug, "Server Hello: Ciphersuite is {cs}");
		self.crypto_algs.set_ciphersuite(ciphersuite);
		//Do not compress the ClientHello.
		self.hs_hash.convert_to_hash(ciphersuite.get_prf2(), debug.clone(), false)?;
		//Process the received ALPN extension. The received ALPN must be one of the proposed ALPN values.
		if let (None, Some(alpn)) = (self.acme_hash, pmsg.get_alpn_protocol_id()) {
			debug!(HANDSHAKE_EVENTS debug, "Server Hello: Application Layer Protocol is '{alpn}'");
		}
		let acme_proto = btls_aux_tls_iana::AlpnProtocolId::ACME_TLS;
		fail_if!(self.acme_hash.is_some() && pmsg.get_alpn_protocol_id() != Some(acme_proto),
			error!(bogus_acme_alpn));
		//If maybe_send_status is set, that means the server might send certificate_status message.
		if pmsg.acknowledged_status_request() { self.flags.set_MAYBE_SEND_STATUS(); }
		//If the signed_certificate_timestamp extension is present, add all the SCTs obtained from the
		//extension to the pile of SCTs.
		let mut staple_scts = CollectedScts::new();
		for sct in pmsg.get_signed_certificate_timestamps() {
			let n = staple_scts.add_raw_final_sct(sct.get_serialized_sct());
			debug!(HANDSHAKE_EVENTS debug, "Server certificate: Received SCT#{n} using extension.");
		}
		//Handle the server_certificate_type extension.
		match pmsg.get_server_certificate_type() {
			CertificateType::X509 => (),	//Do nothing.
			ctype@CertificateType::RAW_PUBLIC_KEY => {
				debug!(HANDSHAKE_EVENTS debug, "Server certificate type: Selected type {ctype}");
				self.flags.set_USE_SPKI_ONLY();
			},
			ctype => sanity_failed!(F "Parsed server_hello contains bad server certificate type {ctype}")
		}
		//Handle the Extended Master Secret extension. This is just noted, including asserting the feature
		//flag.
		if pmsg.use_extended_master_secret() {
			debug!(HANDSHAKE_EVENTS debug, "Server hello: Extended Master Secret enabled");
			self.crypto_algs.enable_ems();
		} else if config.flags.is_flag(FLAGS0_REQUIRE_EMS) {
			//EMS required but not present.
			fail!(error!(vulernable_ths));
		};
		//Renegotiation info extension. This is required, so the feature flag is always set.
		match pmsg.get_renegotiated_connection() {
			Some(x) if x.is_empty() => (),	//OK.
			Some(_) => fail!(error!(renegotiation_attack_detected)),
			None => fail!(error!(vulernable_renego)),
		}
		//Record size limit extension. Just note the maximum size.
		if let Some(maxsize) = pmsg.get_record_size_limit() {
			debug!(HANDSHAKE_EVENTS debug, "Server Hello: Maximum fragment size supported by peer is \
				{maxsize}");
			//This is TLS 1.2, so raw value.
			controller.set_max_fragment_length(maxsize);
		}
		//Print summary of parameters.
		debug!(HANDSHAKE_EVENTS debug, "Chosen configuration: Version: TLS 1.2, Cipher Suite: {cs}, \
			Extended Master Secret: {ems}",
			ems=yesno(pmsg.use_extended_master_secret()));
		//Unwrap the handshake hash and add the server hello message.
		let mut hs_hash = self.hs_hash.unwrap_hash()?;
		hs_hash.add(rawmsg, debug.clone())?;
		//Emit the callback about target information, now that SNI and ALPN are known.
		//If accessing by IP, there is no SNI. If no data transfer is allowed, suppress this
		//callback.
		if !self.flags.is_NO_DATA_TRANSFER() {
			let sni = if self.flags.is_NO_REQUEST_SNI() {
				None
			} else if let &ReferenceNameOwned::DnsName(ref sni) = &self.cvinfo.requested_sni {
				Some(sni.deref())
			} else {
				None
			};
			//The ALPNs are always valid UTF-8.
			let alpn = pmsg.get_alpn_protocol_id().and_then(|x|core::str::from_utf8(x.get()).ok());
			callbacks.target_info(TargetInfoParameters {
				sni: sni,
				alpn: alpn,
			});
		}
		//Next up, Certificate message.
		Ok(HandshakeState2::Tls12Certificate(Tls12CertificateFields {
			tls13_shares: self.tls13_shares,
			staple_scts: staple_scts,
			features: self.features,
			client_random: self.client_random,
			server_random: pmsg.get_random(),
			hs_hash: hs_hash,
			protection: ciphersuite.get_protector(),
			prf: ciphersuite.get_prf2(),
			cvinfo: self.cvinfo,
			crypto_algs: self.crypto_algs,
			acme_hash: self.acme_hash,
			client_caps: self.client_caps,
			flags: self.flags.fforce_EMS_ENABLED(pmsg.use_extended_master_secret()),
		}))
	}
	fn __rx_server_hello_tls13(mut self, controller: &mut UpcallReturn,
		pmsg: btls_aux_tls_struct::ServerHelloTls13, bp: BaseMessageParams) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParams{callbacks, rawmsg, config, debug, ssl_key_log, ..} = bp;
		test_failure(&config.flags, FLAGS5_TEST_SH_RECV_FAIL)?;
		//Flags.
		//Check version matches with HRR.
		let version = TlsVersion::by_tls_id(pmsg.get_tls_version(), false)?;
		fail_if!(self.version_assigned.unwrap_or(version).to_code() != version.to_code(),
			error!(version_mismatch_hrr_sh));
		//Server random.
		let random = pmsg.get_random();
		debug!(CRYPTO_CALCS debug, "Server hello: Server random:\n{random}",
			random=DumpHexblock::new(&random, "  ", "Server random"));
		//Read ciphersuite, and initialize PRF. Need to check it matches assigned ciphesuite.
		let cs = pmsg.get_cipher_suite();
		let en_cs = self.client_caps.get_enabled_cs();
		let ciphersuite = Ciphersuite::by_tls_id4(cs, en_cs).ok_or_else(||{
			assert_failure!(F "Parsed server_hello contains bad cipher {cs}")
		})?;
		fail_if!(ciphersuite.get_key_exchange() != KeyExchangeType::Tls13, error!(sh_bad_tls13_cipher cs));
		if self.ciphersuite_assigned.is_none() {
			//No old ciphersuite found.
			debug!(HANDSHAKE_EVENTS debug, "Server Hello: Ciphersuite is {cs}");
			self.crypto_algs.set_ciphersuite(ciphersuite);
			//Do not compress the ClientHello.
			self.hs_hash.convert_to_hash(ciphersuite.get_prf2(), debug.clone(), false)?;
		} else if self.ciphersuite_assigned != Some(ciphersuite) {
			//Ciphersuite mismatch.
			let assigned = self.ciphersuite_assigned.unwrap_or(Ciphersuite::random_suite());
			fail!(error!(ciphersuite_disagreement assigned.tls_id2(), cs));
		}
		let (_kem_group, kem_share) = pmsg.get_key_share().ok_or(error!(tls13_no_key_share))?;
		let en_kem = self.client_caps.get_enabled_kem();
		let kem_group = KEM::by_tls_id2(_kem_group, en_kem).ok_or_else(||{
			assert_failure!(F "Parsed server_hello contains bad KEM {_kem_group}")
		})?;
		//Perform the key agreement and record the group used and the resulting shared key.
		let dh_shared = Self::handle_ext_keyshare((kem_group, kem_share), &mut self.tls13_shares, version,
			self.group_id, &mut self.crypto_algs)?;
		self.crypto_algs.set_kex_kem(dh_shared.get_kem());
		let handshake_secret = Tls13HandshakeSecret::new_client(dh_shared,
			ciphersuite.get_protector(), ciphersuite.get_prf2(), Exportable(config.raw_handshake),
			HasAd(true), ks_debug!(&debug))?;
		//Print summary of parameters.
		debug!(HANDSHAKE_EVENTS debug, "Chosen configuration: Version: {version}, Cipher Suite: {cs}, \
			Exchange group: {_kem_group}");
		//Unwrap the handshake hash and add the server hello message.
		let mut hs_hash = self.hs_hash.unwrap_hash()?;
		hs_hash.add(rawmsg, debug.clone())?;
		//Derive the handshake keys and activate them.
		let sh_hash = hs_hash.checkpoint();
		let (server_hs_write, client_hs_write) = handshake_secret.to_traffic_secrets(&sh_hash,
			ks_debug!(&debug))?;
		if ssl_key_log {
			callbacks.ssl_key_log(KET::ServerHandshakeTrafficSecret, self.client_random,
				server_hs_write.debug_export());
			callbacks.ssl_key_log(KET::ClientHandshakeTrafficSecret, self.client_random,
				client_hs_write.debug_export());
		}
		//Just blindly calling tls13_update_send_key() here will leak information to attacker
		//since if attacker has corrupted the key, and the key changes, the resulting alert
		//will leak more information than just that the guessed key is wrong. For this
		//reason, force alerts to be unencrypted, to not leak information, until
		//EncryptedExtensions is received, which provodes a MAC to verify, restricting
		//attacker to one guess.
		controller.set_force_unencrypted_alerts(true);
		tls13_update_send_key(controller, &client_hs_write, config.raw_handshake, debug.clone(), callbacks)?;
		tls13_update_recv_key(controller, &server_hs_write, config.raw_handshake, debug.clone(), callbacks)?;
		debug!(HANDSHAKE_EVENTS debug, "Received TLS 1.3 serverhello, downstream&upstream encrypted");
		//Delay derivation of master secret until after CertificateVerify.
		//Next up, EncryptedExtensions.
		let no_spontaneous_rekey = config.flags.is_flag(FLAGS0_NO_SPONTANEOUS_REKEY);
		let middlebox_compat = config.flags.is_flag(FLAGS0_MIDDLEBOX_COMPAT);
		Ok(HandshakeState2::Tls13EncryptedExtensions(Tls13EncryptedExtensionsFields {
			cvinfo: self.cvinfo,
			features: self.features,
			hs_secrets: Tls13HsSecretsPre {
				handshake_secret: handshake_secret,
				client_hs_write: client_hs_write,
				server_hs_write: server_hs_write,
			},
			hs_hash: hs_hash,
			crypto_algs: self.crypto_algs,
			acme_hash: self.acme_hash,
			client_caps: self.client_caps,
			client_random: self.client_random,
			//Activate no spontaneous rekey and compatibility mode if needed.
			flags: self.flags.fforce_NO_SPONTANEOUS_REKEY(no_spontaneous_rekey).
				fforce_COMPATIBILITY_MODE(middlebox_compat),
		}))
	}
	pub fn rx_server_hello_2(self, controller: &mut UpcallReturn, msg: &[u8],
		bp: BaseMessageParams, last: bool) -> Result<HandshakeState2, Error>
	{
		//All supported versions use TLS 1.2 records.
		controller.set_use_tls12_records();

		let debug = bp.debug.clone();
		let mut debug_cb = |args:Arguments|debug!(TLS_EXTENSIONS debug, "Server Hello: {args}");
		let enable = |f:btls_aux_tls_struct::RemoteFeature|self.client_caps.enable(&bp.config, f);
		let pmsg = PServerHello::parse(msg, &enable, &|g,k|kem_ciphertext_check2(g,k).is_ok(),
			&mut Some(&mut debug_cb)).
			map_err(|e|error!(msgerr_server_hello e))?;
		match pmsg {
			PServerHello::HelloRetryRequest(r) => self.__rx_hello_retry_request(r, msg, bp),
			PServerHello::ServerHelloTls12(r) => self.__rx_server_hello_tls12(controller, r, bp),
			PServerHello::ServerHelloTls13(r) => {
				fail_if!(!last, error!(server_hello_not_aligned_tls13));
				self.__rx_server_hello_tls13(controller, r, bp)
			},
			_ => sanity_failed!("TLS server_hello mesage of unknown version!")
		}
	}
}
