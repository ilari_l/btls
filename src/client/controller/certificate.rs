use super::super::ClientConnection;
use super::BaseMessageParams;
use super::BaseMessageParamsS;
use super::Flags;
use super::HandshakeState2;
use super::Tls13C2ndFSecrets;
use super::Tls13HsSecrets;
use super::Tls13HsSecretsPre;
use super::TlsClientCert;
use super::TlsCVInfo;
use super::finished::Tls13ClientFinishedFields;
use super::finished::Tls13ServerFinishedFields;
use super::serverkeyexchange::Tls12ServerKeyExchangeFields;
use crate::callbacks::GotUnknownExtensionParameters;
use crate::callbacks::MSG_CERTIFICATEREQUEST;
use crate::callbacks::SelectClientCertificateParameters;
use crate::callbacks::TlsCallbacks;
use crate::certificates::HostSpecificPin;
use crate::client::ClientConfiguration;
use crate::common::certificate_status_to_ocsp;
use crate::common::check_dynamic_spki;
use crate::common::collect_tls13_scts;
use crate::common::CollectedScts;
use crate::common::CryptoTracker;
use crate::common::emit_crypto_parameters_event;
use crate::common::FutureAMO;
use crate::common::is_unknown_extension;
use crate::common::make_tls13_extra;
use crate::common::OcspTag;
use crate::common::ValidateExtra;
use crate::common::ValidateExtraTag;
use crate::common::X509CertMaybeParsed;
use crate::common::XValidationStatus;
use crate::errors::Error;
use crate::features::FLAGS0_KNOWN_CA_ONLY;
use crate::features::FLAGS0_REQUIRE_CT;
use crate::features::FLAGS0_REQUIRE_OCSP;
use crate::messages::ClientCertificateObject;
use crate::messages::DumpHexblock;
use crate::messages::HandshakeHash;
use crate::messages::HashMessages;
use crate::messages::KemKeyPair;
use crate::messages::tx_certificate_client;
use crate::messages::tx_certificate_spki;
use crate::messages::tx_certificate_verify;
use crate::messages::tx_certificate_client_nak;
use crate::record::DowncallInterface;
use crate::record::UpcallReturn;
use crate::stdlib::Arc;
use crate::stdlib::Deref;
use crate::stdlib::from_utf8;
use crate::stdlib::String;
use crate::stdlib::ToOwned;
use crate::stdlib::ToString;
use crate::stdlib::Vec;
use btls_aux_aead::ProtectorType;
use btls_aux_collections::BTreeSet;
use btls_aux_fail::ResultExt;
use btls_aux_hash::HashFunction2;
use btls_aux_ip::IpAddressBlock;
use btls_aux_memory::subslice_to_range;
use btls_aux_random::TemporaryRandomLifetimeTag;
use btls_aux_random::TemporaryRandomStream;
use btls_aux_set2::SetOf;
use btls_aux_signatures::Hasher;
use btls_aux_signatures::sanity_check_spki;
use btls_aux_signatures::SignatureAlgorithm2;
use btls_aux_signatures::SignatureAlgorithmEnabled2;
use btls_aux_signatures::SignatureAlgorithmTls2;
use btls_aux_signatures::SignatureFlags2;
use btls_aux_time::Timestamp;
use btls_aux_tls_struct::CertificateEntry;
use btls_aux_tls_struct::CertificateEntry2;
use btls_aux_tls_struct::CertificateRequestTls13 as PCertificateRequest;
use btls_aux_tls_struct::CertificateStatus;
use btls_aux_tls_struct::CertificateTls12X509 as PCertificate2X;
use btls_aux_tls_struct::CertificateTls12Rpk as PCertificate2R;
use btls_aux_tls_struct::CertificateTls13X509 as PCertificate3X;
use btls_aux_tls_struct::CertificateTls13Rpk as PCertificate3R;
use btls_aux_tls_struct::IntoGenericIterator as ListOf;
use btls_aux_tls_struct::SerializedSCT;
use btls_aux_tlskeyschedule::CheckSignatureType;
use btls_aux_tlskeyschedule::TlsCertificateVerify as KsTlsCertificateVerify;
use btls_aux_x509certparse::CFLAG_MUST_STAPLE;
use btls_aux_x509certparse::CFLAG_UNKNOWN;
use btls_aux_x509certparse::extract_spki;
use btls_aux_x509certvalidation::full_certificate_validation;
use btls_aux_x509certvalidation::FullCertificateValidation;
use btls_aux_x509certvalidation::ip_in_set;
use btls_aux_x509certvalidation::name_in_set2;
use btls_aux_x509certvalidation::ReferenceName;
use btls_aux_x509certvalidation::SanityCheckCertificateChainExtended;
use btls_aux_x509certvalidation::validate_ee_spki2;
use core::fmt::Arguments;

macro_rules! get_cert_field
{
	(RAW $x:expr) => { $x.get_raw() };
	(KEY $x:expr) => { $x.get_key().map_err(|x|error!(cant_extract_spki x))? };
	(PARSE $x:expr) => { $x.get_certificate().map_err(|x|error!(ee_cert_parse_error x))? };
}

macro_rules! unwrap_tls13_cert
{
	($x:expr) => {{
		let x = $x.map_err(|e|error!(msgerr_server_certificate e))?;
		fail_if!(x.get_request_context().len() > 0, error!(cr_context_must_be_empty));
		x.get_certificate_chain().ok_or(error!(server_chain_empty))?
	}}
}

struct DoValidateServerCertChainParams<'a>
{
	requested_sni: ReferenceName<'a>,
	features: u32,
	use_spki_only: bool,
	pins: &'a Vec<HostSpecificPin>,
	killist_empty: bool,
	only_known_ca: bool,
	_flags: Flags,
	_callbacks: &'a mut dyn TlsCallbacks,
	enabled_sig: SignatureAlgorithmEnabled2,
	configuration: &'a ClientConfiguration
}

fn validate_acme_common(cert: &mut X509CertMaybeParsed, hash: [u8;32], name: ReferenceName,
	enabled_sig: SignatureAlgorithmEnabled2) -> Result<(), Error>
{
	//Parse the certificate.
	let cert = get_cert_field!(PARSE cert);
	//Validate that the serial is valid.
	let sa = cert.serial_number.get(0).map(|x|*x).unwrap_or(0);
	let sb = cert.serial_number.get(1).map(|x|*x).unwrap_or(0);
	let syndrome = 2 * sa as u16 + sb as u16 / 128;
	fail_if!(syndrome == 0 || syndrome >= 256, error!(bogus_acme_serial));
	//Validate the self-signature.
	match cert.signature {
		Ok(signature) => signature.verify(cert.pubkey, cert.tbs, enabled_sig, SignatureFlags2::CERTIFICATE).
			set_err(error!(bogus_acme_self_signature))?,
		//These damn signatures are just plain unvalidable.
		Err(_) => fail!(error!(bogus_acme_self_signature))
	}
	//Check hash in ACME extension.
	fail_if!(cert.acme_tls_alpn != Some(&hash[..]), error!(bogus_acme_token));
	//Check that there is exactly one name.
	fail_if!(cert.dnsnames.iter().count() + cert.dnsnames.iter_addrs().count() != 1, error!(bogus_acme_names));
	//Check that SAN contains exactly the name to verify.
	fail_if!(match name {
		ReferenceName::DnsName(x) => !name_in_set2(cert.dnsnames.iter(), x.as_bytes()),
		ReferenceName::Ip(x) => !ip_in_set(cert.dnsnames.iter_addrs(), &IpAddressBlock::from(x)),
		_ => true,
	}, error!(bogus_acme_names));
	//The certificate looks OK.
	Ok(())
}

//Handle main chain validation.
fn do_validate_server_cert_chain<'a,'b,CB>(eecert: &mut X509CertMaybeParsed<'b>, mut icerts: &[&[u8]],
	names: &mut Option<Arc<Vec<String>>>, ips: &mut Option<Arc<Vec<String>>>, time_now: Timestamp, debug: &CB,
	params: DoValidateServerCertChainParams<'a>) -> Result<(bool, ValidateExtra), Error>
	where CB: Fn(Arguments)
{
	let features = params.features;

	//Fucking LE Android hacks that cause problems for everyone else. Find-Fix-Finish this cursed cert.
	if icerts.len() == 2 && icerts[1] == crate::certificates::get_isrg_x1_dst_hack_certificate() {
		icerts = &icerts[..1];
	}
	//Check for key constraints.
	let eecert_spki = get_cert_field!(KEY eecert);
	check_dynamic_spki(eecert_spki, &params.configuration.flags)?;

	//Check the key first. On success, return with no chain (so no OCSP nor names list). If this fails, if
	//UseSpkiOnly is set, fail the handshake, because we do not have fallback authentication. Otherwise
	//continue to chain validation.
	match validate_ee_spki2(eecert_spki, params.pins, params.enabled_sig.hash_subpolicy()) {
		Ok(_) => {
			debug(format_args!("Validated server key. OCSP and SCT will not be checked."));
			return Ok((true, ValidateExtra::bypass_ocsp()));
		},
		Err(_) if params.use_spki_only => fail!(error!(server_key_not_whitelisted)),
		Err(_) => ()
	}

	let empty_map = BTreeSet::new();
	let killist = if params.killist_empty { &empty_map } else { &params.configuration.killist };
	//Main chain validation.
	let mut ocsp_required = params.configuration.flags.is_flag(FLAGS0_REQUIRE_OCSP);
	let mut sct_required = params.configuration.flags.is_flag(FLAGS0_REQUIRE_CT);
	let eecert_p = get_cert_field!(PARSE eecert);
	let vparam = FullCertificateValidation {
		strict_algo: false,
		policy: params.enabled_sig,
		is_client: false,
		feature_flags: features,
		need_ocsp_flag: &mut ocsp_required,
		need_sct_flag: &mut sct_required,
		killist: killist,
		require_disclosed_ca: params.only_known_ca,
		issuers: icerts,
		reference: Some(params.requested_sni),
		time_now: &time_now,
		trust_anchors: &params.configuration.trust_anchors,
		pins: params.pins.deref(),
		extended: SanityCheckCertificateChainExtended::default()
	};
	full_certificate_validation(eecert_p, vparam).map_err(|x|error!(certificate_validation_failed x))?;
	//Certificate is OK, extract SPKI and names out of it. Note that we will not provode the server
	//name list before server Finished is recieved and checked.
	*names = Some(Arc::new(eecert_p.dnsnames.iter().filter_map(|x|from_utf8(x).ok()).map(|x|x.to_owned()).
		collect()));
	*ips = Some(Arc::new(eecert_p.dnsnames.iter_addrs().map(|x|x.to_string()).collect()));
	debug(format_args!("Validation ok"));

	Ok((false, ValidateExtra::with_ocsp(XValidationStatus::<OcspTag>::new(ocsp_required),
		XValidationStatus::new(sct_required), params.enabled_sig, time_now)))
}

pub(crate) struct Tls12CertificateFields
{
	pub cvinfo: TlsCVInfo,					//Server certificate validation info.
	pub tls13_shares: Vec<KemKeyPair>,			//The key shares (also abused for TLS 1.2).
	pub staple_scts: CollectedScts,				//Stapled SCTs.
	pub features: u32,					//Features supported.
	pub client_random: [u8; 32],				//The client random.
	pub server_random: [u8; 32],				//The server random.
	pub hs_hash: HandshakeHash,
	pub protection: ProtectorType,
	pub prf: HashFunction2,
	pub crypto_algs: CryptoTracker,
	pub acme_hash: Option<[u8;32]>,
	pub(crate) client_caps: crate::messages::ClientSupport,
	pub flags: Flags,
}

pub(crate) struct Tls13CertificateFields
{
	pub cvinfo: TlsCVInfo,					//Server certificate validation info.
	pub features: u32,				//Features supported.
	pub hs_secrets: Tls13HsSecretsPre,
	pub hs_hash: HandshakeHash,
	pub cc: TlsClientCert,
	pub crypto_algs: CryptoTracker,
	pub acme_hash: Option<[u8;32]>,
	pub(crate) client_caps: crate::messages::ClientSupport,
	pub client_random: [u8;32],
	pub flags: Flags,
}

pub(crate) struct Tls13CertificateVerifyFields
{
	server_spki: Vec<u8>,			//The SPKI of the server.
	expect_alg: CheckSignatureType,
	hs_secrets: Tls13HsSecretsPre,
	pub hs_hash: HandshakeHash,
	cc: TlsClientCert,
	server_names: Option<Arc<Vec<String>>>,
	server_ips: Option<Arc<Vec<String>>>,
	crypto_algs: CryptoTracker,
	cert_tag: ValidateExtraTag,
	client_caps: crate::messages::ClientSupport,
	client_random: [u8;32],
	flags: Flags,
}

impl Tls13CertificateFields
{
	//Handle TLS 1.3 CertificateRequest message
	pub fn rx_certificate_request(mut self, msg: &[u8], bp: BaseMessageParams) -> Result<HandshakeState2, Error>
	{
		let BaseMessageParams{rawmsg, debug, callbacks, ..} = bp;

		let mut debug_cb = |args:Arguments|debug!(TLS_EXTENSIONS debug, "Client Hello: {args}");
		let pmsg = PCertificateRequest::parse(msg, Some(&mut debug_cb)).
			map_err(|e|error!(msgerr_certificate_request e))?;
		let pmsg_sigalgo = to_signature_algorithm_set(pmsg.get_signature_algorithms(),
			self.client_caps.get_enabled_sig()) & SignatureAlgorithm2::SET_TLS13_OK;
		let pmsg_sigalgo_cert = to_signature_algorithm_set(pmsg.get_signature_algorithms_certificate(),
			self.client_caps.get_enabled_sig());
		//We are only interested about the context.
		fail_if!(pmsg.get_request_context().len() > 0, error!(cr_context_must_be_empty));
		//Select client certificate.
		let client_cert = if !self.flags.is_NO_DATA_TRANSFER() {
			let p = SelectClientCertificateParameters::new(pmsg_sigalgo, pmsg_sigalgo_cert);
			let tmp_cb = callbacks.select_client_certificate(p);
			let status = if tmp_cb.is_some() { "sending certificate" } else { "will refuse" };
			debug!(HANDSHAKE_EVENTS debug, "Client certificate: Received certificate request, {status}.");

			tmp_cb.map(|cert|ClientCertificateObject{
				certificate: cert,
				send_spki_only: self.flags.is_USE_CLIENT_RPK(),	//Server chooses.
				ocsp_enabled: pmsg.may_staple_ocsp(),
				sct_enabled: pmsg.may_staple_sct(),
				trans_enabled: pmsg.may_staple_trans_item(),
			})
		} else {
			//Always NAK the client certificate if NoDataTransfer is set.
			None
		};
		self.hs_hash.add(rawmsg, debug)?;

		if !self.flags.is_NO_DATA_TRANSFER() {
			//Do callbacks on unknown extensions.
			for extension in pmsg.get_extensions() {
				let id = extension.get_type().get();
				if is_unknown_extension(id) {
					callbacks.got_unknown_extension(GotUnknownExtensionParameters {
						id: id,
						payload: extension.get_raw_data(),
					});
				}
			}
			callbacks.end_unknown_extension(MSG_CERTIFICATEREQUEST);
		}

		Ok(HandshakeState2::Tls13Certificate(true, Tls13CertificateFields{
			cc: TlsClientCert {
				client_cert: client_cert,
				peer_sigalgo_mask: pmsg_sigalgo,
			},
			..self
		}))
	}
	pub fn rx_certificate_2(mut self, msg: &[u8], only_spki: bool, bp: BaseMessageParams,
		certificate_reqd: bool) -> Result<HandshakeState2, Error>
	{
		self.hs_hash.add(bp.rawmsg.clone(), bp.debug.clone())?;

		let debug = bp.debug.clone();
		let enable = |f:btls_aux_tls_struct::RemoteFeature|self.client_caps.enable(&bp.config, f);
		let mut debug_cb = |args:Arguments|debug!(TLS_EXTENSIONS debug, "Server Certificate: {args}");
		let ret = if only_spki {
			let pmsg = unwrap_tls13_cert!(PCertificate3R::parse(msg, &enable, &mut Some(&mut debug_cb)));
			self.rx_raw_public_key(pmsg.get_public_key(), bp, certificate_reqd)
		} else {
			let pmsg = unwrap_tls13_cert!(PCertificate3X::parse(msg, &enable, &mut Some(&mut debug_cb)));
			self.rx_x509_cert(pmsg.get_end_entity(), pmsg.get_status(),
				pmsg.get_signed_certificate_timestamps(), pmsg.get_issuer_chain(), bp,
				certificate_reqd)
		};
		ret
	}
	pub fn rx_raw_public_key(self, key: &[u8], bp: BaseMessageParams, certificate_reqd: bool) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParams{debug, ..} = bp;
		let debug = server_cert_msg!(debug);
		sanity_check!(self.acme_hash.is_none(), "Received SPKI-only for ACME validation request.");
		let mut pins = self.cvinfo.pins;
		validate_ee_spki2(key, &pins, self. client_caps.get_enabled_sig().hash_subpolicy()).
			set_err(error!(server_key_not_whitelisted))?;
		debug(format_args!("Validated server key. OCSP and SCT will not be checked."));
		pins.clear();	//Not needed anymore.
		Ok(HandshakeState2::Tls13CertificateVerify(Tls13CertificateVerifyFields {
			server_spki: key.to_owned(),
			expect_alg: CheckSignatureType::no_requirement(),	//DC is not currently supported.
			hs_secrets: self.hs_secrets,
			hs_hash: self.hs_hash,
			cc: self.cc,
			server_names: None,
			server_ips: None,
			crypto_algs: self.crypto_algs,
			cert_tag: ValidateExtra::bypass_ocsp().as_tag(),
			client_caps: self.client_caps,
			client_random: self.client_random,
			flags: self.flags.fforce_REQUESTED_CERTIFICATE(certificate_reqd),
		}))
	}
	pub fn rx_x509_cert<'a>(self, certificate: &[u8], ocsp: Option<CertificateStatus>,
		scts: ListOf<'a,SerializedSCT<'a>>, issuers: ListOf<'a,CertificateEntry2<'a>>,
		bp: BaseMessageParams, certificate_reqd: bool) -> Result<HandshakeState2, Error>
	{
		fail_if!(certificate.len() == 0, error!(ee_certificate_empty));
		let mut certificate = X509CertMaybeParsed::new(certificate);
		let BaseMessageParams{debug, config, ..} = bp;
		let debug = server_cert_msg!(debug);
		//Special validation for ACME.
		if let Some(acmehash) = self.acme_hash {
			//There can not be issuers in acme-tls certificate.
			fail_if!(issuers.count_all(true) != 0, error!(bogus_acme_chain_len));
			validate_acme_common(&mut certificate, acmehash, self.cvinfo.requested_sni.borrow(),
				self.client_caps.get_enabled_sig())?;
			//This should be trivial now validate_acme_common() has parsed the certificate.
			let key = get_cert_field!(KEY certificate);
			return Ok(HandshakeState2::Tls13CertificateVerify(Tls13CertificateVerifyFields {
				server_spki: key.to_owned(),
				expect_alg: CheckSignatureType::no_requirement(),	//No DC.
				hs_secrets: self.hs_secrets,
				hs_hash: self.hs_hash,
				cc: self.cc,
				server_names: None,
				server_ips: None,
				crypto_algs: self.crypto_algs,
				cert_tag: ValidateExtra::bypass_ocsp().as_tag(),
				client_caps: self.client_caps,
				client_random: self.client_random,
				//There still may be certificate request we will NAK.
				flags: self.flags.fforce_REQUESTED_CERTIFICATE(certificate_reqd),
			}));
		}
		let time_now = Timestamp::now();
		let mut pins = self.cvinfo.pins;
		//Always assert STAPLE, because those are checked later. Also assert TLS 1.3-specific flags.
		let only_known_ca = config.flags.is_flag(FLAGS0_KNOWN_CA_ONLY);
		let features = self.features  | CFLAG_MUST_STAPLE | CFLAG_UNKNOWN;
		let issuers: Vec<&[u8]> = issuers.iter().map(|x|x.get_certificate()).collect();
		let mut server_names = None;
		let mut server_ips = None;
		let (no_cert, vextra) = do_validate_server_cert_chain(&mut certificate, &issuers,
			&mut server_names, &mut server_ips, time_now, &debug, DoValidateServerCertChainParams {
			requested_sni: self.cvinfo.requested_sni.borrow(),
			features: features,
			pins: &pins,
			killist_empty: self.cvinfo.flags.is_KILL_LIST_EMPTY(),
			use_spki_only: false,
			only_known_ca: only_known_ca,
			_callbacks: bp.callbacks,
			_flags: self.cvinfo.flags,
			enabled_sig: self.client_caps.get_enabled_sig(),
			configuration: config,
		})?;
		pins.clear();	//Not needed anymore.

		let key = get_cert_field!(KEY certificate);
		let mut ret_tpl = Tls13CertificateVerifyFields {
			server_spki: key.to_owned(),
			expect_alg: CheckSignatureType::no_requirement(),	//DC is not currently supported.
			hs_secrets: self.hs_secrets,
			hs_hash: self.hs_hash,
			cc: self.cc,
			server_names: None,
			server_ips: None,
			crypto_algs: self.crypto_algs,
			cert_tag: ValidateExtraTag::failed(),
			client_caps: self.client_caps,
			client_random: self.client_random,
			flags: self.flags.fforce_REQUESTED_CERTIFICATE(certificate_reqd),
		};
		//If we hit key, short-circuit the validation. Otherwise extract the certificate.
		if no_cert {
			return Ok(HandshakeState2::Tls13CertificateVerify(Tls13CertificateVerifyFields {
				cert_tag: vextra.as_tag(),
				..ret_tpl
			}));
		}
		let eecertp = get_cert_field!(PARSE certificate);
		//Validate SCT and OCSP. Note that validate_extra internally dumps the SCTs from OCSP.
		let staple_scts = collect_tls13_scts(eecertp, scts, &debug);
		let ocsp = certificate_status_to_ocsp(ocsp);
		let extra = make_tls13_extra(eecertp, &issuers, &config.trust_anchors, &config.trusted_logs,
			config.ocsp_maxvalid);
		let cert_ok_tag = vextra.close(ocsp, staple_scts, extra, Some(&mut ret_tpl.crypto_algs),
			&debug)?.as_tag();
		Ok(HandshakeState2::Tls13CertificateVerify(Tls13CertificateVerifyFields {
			server_names: server_names,
			server_ips: server_ips,
			cert_tag: cert_ok_tag,
			..ret_tpl
		}))
	}
}

fn to_signature_algorithm_set<'a>(algorithms: ListOf<'a,btls_aux_tls_iana::SignatureScheme>,
	enabled: SignatureAlgorithmEnabled2) -> SetOf<SignatureAlgorithm2>
{
	let mut tmp = SetOf::<SignatureAlgorithm2>::empty_set();
	for sigalgo in algorithms {
		match SignatureAlgorithmTls2::by_tls_id2(sigalgo) {
			//Do not ignore private-use.
			Some(x) if enabled.allows_tls2(x) => tmp.add(&x),
			_ => continue,
		};
	}
	tmp
}

impl Tls13CertificateVerifyFields
{
	//Handle TLS 1.3 CertificateVerify message.
	pub fn rx_certificate_verify(self, _controller: &mut UpcallReturn, msg: &[u8],
		bp: BaseMessageParams) -> Result<HandshakeState2, Error>
	{
		let BaseMessageParams{rawmsg, debug, config, callbacks, ..} = bp;
		
		let enable = |f:btls_aux_tls_struct::RemoteFeature|self.client_caps.enable(config, f);
		let mut debug_cb = |args:Arguments|debug!(TLS_EXTENSIONS debug, "Certificate Verify: {args}");
		let pmsg = btls_aux_tls_struct::parse_certificate_verify(msg, &enable, None,
			&mut Some(&mut debug_cb)).map_err(|e|error!(msgerr_certificate_verify(e)))?;
		let sig_algorithm = pmsg.get_algorithm();
		let pmsg_algorithm = SignatureAlgorithmTls2::by_tls_id2(sig_algorithm).ok_or_else(||{
			assert_failure!(F "Parsed certificate_verify uses bad algorithm {sig_algorithm}")
		})?;
	
		let mut hs_hash = self.hs_hash;
		//Check certificate verify and derive master secret.
		//Key types (FLAGS0_ONLY_FLOATY_KEYS, FLAGS0_ALLOW_DYNAMIC_RSA) are already checked during
		//certificate receive, no need to check again, so extra_flags can always be ALLOW_RSA_PSS.
		let cvmsg2 = KsTlsCertificateVerify {
			algorithm: pmsg_algorithm,
			signature: pmsg.get_signature(),
		};
		let handshake_secret = self.hs_secrets.handshake_secret.to_handshake_secret(&self.server_spki,
			hs_hash.checkpoint(), &cvmsg2, SignatureFlags2::ALLOW_RSA_PSS, self.expect_alg,
			self.client_caps.get_enabled_sig(), ks_debug!(&debug))?;

		let mut crypto_algs = self.crypto_algs;
		crypto_algs.set_signature(pmsg_algorithm);
		emit_crypto_parameters_event(&crypto_algs, callbacks, self.flags.is_NO_DATA_TRANSFER());
		debug!(HANDSHAKE_EVENTS debug, "Server signature: Algorithm {sig_algorithm}, ok");

		hs_hash.add(rawmsg, debug.clone())?;

		Ok(HandshakeState2::Tls13ServerFinished(Tls13ServerFinishedFields{
			hs_secrets: Tls13HsSecrets {
				master_secret: handshake_secret.to_master_secret(ks_debug!(&debug))?,
				client_hs_write: self.hs_secrets.client_hs_write,
				server_hs_write: self.hs_secrets.server_hs_write,
			},
			hs_hash: hs_hash,
			cc: self.cc,
			server_names: self.server_names,
			server_ips: self.server_ips,
			cert_tag: self.cert_tag,
			client_random: self.client_random,
			flags: self.flags,
		}))
	}
}

macro_rules! ske_state_template
{
	(RAWKEY $selfx:expr, $key:expr, $vextra:expr) => {
		Tls12ServerKeyExchangeFields {
			tls13_shares: $selfx.tls13_shares,
			server_spki: $key.to_owned(),
			staple_scts: CollectedScts::new(),
			staple_ee_cert: Vec::new(),
			staple_issuer_key: None,
			staple_issuer: 0..0,
			staple_serial: 0..0,
			vextra: $vextra,
			client_random: $selfx.client_random,
			server_random: $selfx.server_random,
			hs_hash: $selfx.hs_hash,
			protection: $selfx.protection,
			prf: $selfx.prf,
			certificate_lifetime: 0,	//Only significant for OCSP validation.
			server_names: None,
			server_ips: None,
			crypto_algs: $selfx.crypto_algs,
			client_caps: $selfx.client_caps,
			flags: $selfx.flags,
		}
	};
}

impl Tls12CertificateFields
{
	pub fn rx_certificate_2(mut self, msg: &[u8], only_spki: bool, bp: BaseMessageParams) ->
		Result<HandshakeState2, Error>
	{
		self.hs_hash.add(bp.rawmsg.clone(), bp.debug.clone())?;

		let debug = bp.debug.clone();
		let mut debug_cb = |args:Arguments|debug!(TLS_EXTENSIONS debug, "Server Certificate: {args}");
		let ret = if only_spki {
			let pmsg = PCertificate2R::parse(msg, &mut Some(&mut debug_cb)).
				map_err(|e|error!(msgerr_server_certificate e))?.
				ok_or(error!(server_chain_empty))?;
			self.rx_raw_public_key(pmsg.get_public_key(), bp)
		} else {
			let pmsg = PCertificate2X::parse(msg, &mut Some(&mut debug_cb)).
				map_err(|e|error!(msgerr_server_certificate e))?.
				ok_or(error!(server_chain_empty))?;
			self.rx_x509_cert(pmsg.get_end_entity(), pmsg.get_issuer_chain(), bp)
		};
		ret
	}
	fn rx_raw_public_key(self, key: &[u8], bp: BaseMessageParams) ->
		Result<HandshakeState2, Error>
	{
		fail_if!(key.len() == 0, error!(ee_certificate_empty));
		let BaseMessageParams{debug, config, ..} = bp;
		sanity_check!(self.acme_hash.is_none(), "Received SPKI-only for ACME validation request.");
		//Check for key constraints.
		check_dynamic_spki(key, &config.flags)?;
		//Check the key against the whitelist.
		let mut pins = self.cvinfo.pins;
		validate_ee_spki2(key, &pins, self.client_caps.get_enabled_sig().hash_subpolicy()).
			set_err(error!(server_key_not_whitelisted))?;
		debug!(HANDSHAKE_EVENTS debug,
			"Server certificate: Validated server key. OCSP and SCT will not be checked.");
		pins.clear();	//Not needed anymore.
		//There is no OCSP info.
		Ok(HandshakeState2::Tls12ServerKeyExchange(self.flags.is_MAYBE_SEND_STATUS(),
			ske_state_template!(RAWKEY self, key, ValidateExtra::bypass_ocsp())))
	}
	fn rx_x509_cert<'a>(self, certificate: &[u8], issuers: ListOf<'a,CertificateEntry<'a>>,
		bp: BaseMessageParams) -> Result<HandshakeState2, Error>
	{
		fail_if!(certificate.len() == 0, error!(ee_certificate_empty));
		let BaseMessageParams{debug, config, ..} = bp;
		let debug = server_cert_msg!(debug);
		let mut certificate = X509CertMaybeParsed::new(certificate);
		//Special validation for ACME.
		if let Some(acmehash) = self.acme_hash {
			//There can not be issuers in acme-tls certificate.
			fail_if!(issuers.count_all(true) != 0, error!(bogus_acme_chain_len));
			validate_acme_common(&mut certificate, acmehash, self.cvinfo.requested_sni.borrow(),
				self.client_caps.get_enabled_sig())?;
			//This should be trivial now validate_acme_common() has parsed the certificate.
			let key = get_cert_field!(KEY certificate);
			//Because we did not send OCSP support, there can not be OCSP.
			return Ok(HandshakeState2::Tls12ServerKeyExchange(false,
				ske_state_template!(RAWKEY self, key, ValidateExtra::bypass_ocsp())));
		}
		let time_now = Timestamp::now();
		let only_known_ca = config.flags.is_flag(FLAGS0_KNOWN_CA_ONLY);
		//Always assert STAPLE, because those are checked later.
		let features = self.features | CFLAG_MUST_STAPLE | CFLAG_UNKNOWN;
		let mut pins = self.cvinfo.pins;
		let mut staple_scts = self.staple_scts;
		let mut server_names = None;
		let mut server_ips = None;
		let issuers: Vec<&[u8]> = issuers.iter().map(|x|x.get_certificate()).collect();
		let (no_cert, vextra) = do_validate_server_cert_chain(&mut certificate, &issuers,
			&mut server_names, &mut server_ips, time_now, &debug,
			DoValidateServerCertChainParams {
			requested_sni: self.cvinfo.requested_sni.borrow(),
			features: features,
			pins: &pins,
			killist_empty: self.cvinfo.flags.is_KILL_LIST_EMPTY(),
			use_spki_only: false,
			only_known_ca: only_known_ca,
			_callbacks: bp.callbacks,
			_flags: self.cvinfo.flags,
			enabled_sig: self.client_caps.get_enabled_sig(),
			configuration: config,
		})?;
		pins.clear();	//Not needed anymore.
		//Assume that there are no OCSP information here, we will fixup later if that is not the case.
		let key = get_cert_field!(KEY certificate);
		let ret_tpl = ske_state_template!(RAWKEY self, key, vextra);
		//If we hit key, short-circuit the validation. Otherwise extract the certificate.
		if no_cert {
			return Ok(HandshakeState2::Tls12ServerKeyExchange(
				self.flags.is_MAYBE_SEND_STATUS(), ret_tpl));
		}
		let rawcert = get_cert_field!(RAW certificate);
		let eecertp = get_cert_field!(PARSE certificate);
		let staple_issuer = assert_some!(subslice_to_range(rawcert, eecertp.issuer.0),
			"Certificate issuer not from certificate???");
		let staple_serial = assert_some!(subslice_to_range(rawcert, eecertp.serial_number),
			"Certificate serial not from certificate???");
		staple_scts.add_from_certificate(&eecertp, &debug);
		//Save info for OCSP validation if sent. Also do this if we might need to validate SCTs.
		let (staple_ee_cert, staple_issuer_key) =  if self.flags.is_MAYBE_SEND_STATUS() ||
			staple_scts.has_entries() {
			//Save EE cert and its issuer.
			let staple_ee_cert = rawcert.to_owned();
			let staple_issuer_key = grab_issuer_key!(issuers, Some(eecertp.issuer),
				config.trust_anchors);
			if let Some(isskey) = staple_issuer_key {
				sanity_check_spki(isskey).map_err(|e|error!(bad_entity_spki e))?;
			}
			(staple_ee_cert, staple_issuer_key.map(|x|x.to_owned()))
		} else {
			(Vec::new(), None)
		};

		Ok(HandshakeState2::Tls12ServerKeyExchange(self.flags.is_MAYBE_SEND_STATUS(),
			Tls12ServerKeyExchangeFields {
			staple_scts: staple_scts,
			staple_ee_cert: staple_ee_cert,
			staple_issuer_key: staple_issuer_key,
			staple_issuer: staple_issuer,
			staple_serial: staple_serial,
			certificate_lifetime: eecertp.not_before.delta(eecertp.not_after),
			server_names: server_names,
			server_ips: server_ips,
			..ret_tpl
		}))
	}
}

pub(crate) struct Tls13ClientCertificateFields
{
	pub hs_secrets2: Tls13C2ndFSecrets,
	pub hs_hash: HandshakeHash,
	pub cc: TlsClientCert,
	pub flags: Flags,
}

impl Tls13ClientCertificateFields
{
	//Transmit a certificate message.
	pub fn tx_certificate(mut self, base: &mut impl DowncallInterface, bp: BaseMessageParamsS,
		connection: ClientConnection) -> Result<HandshakeState2, Error>
	{
		let BaseMessageParamsS{debug, sname, ..} = bp;
		if self.flags.is_COMPATIBILITY_MODE() {
			//Send fake CCS.
			base.send_clear_ccs();
		}
		//In TLS 1.3, certificates have context field.
		//This is only used for in-handshake auth, and there context needs to be empty.
		if let Some(certbox) = self.cc.client_cert {
			//tx_certificate_client can not handle RPK, so handle RPK case specially.
			if certbox.send_spki_only {
				let eecert = certbox.certificate.certificate();
				tx_certificate_spki(eecert, true, debug.clone(), base, &mut self.hs_hash, sname)?;
			} else {
				tx_certificate_client(&certbox, debug.clone(), None, base, &mut self.hs_hash, sname)?;
			}
			//Request signature over key exchange.
			let hash = self.hs_hash.checkpoint();
			let mut tbs = |h: &mut Hasher|{
				h.append(&[32;64]);	//Padding for randoms.
				h.append(b"TLS 1.3, client CertificateVerify\0");
				h.append(&hash);
			};
			debug!(CRYPTO_CALCS debug, "Client TBS:\n{tbs}",
				tbs=DumpHexblock::new(&Hasher::collect(&mut tbs), "  ", "TBS"));
			let lifetime = TemporaryRandomLifetimeTag;
			let signature = match certbox.certificate.sign(&mut tbs,
				&mut TemporaryRandomStream::new_system(&lifetime)).read() {
				Ok(signature) => {
					debug!(HANDSHAKE_EVENTS debug, "Client certificate: Immediate signature");
					let mut hs_hash = self.hs_hash;
					tx_certificate_verify(signature, self.cc.peer_sigalgo_mask, None, base,
						&mut hs_hash, sname, None, debug.clone())?;
					return Ok(HandshakeState2::Tls13ClientFinished(Tls13ClientFinishedFields {
						hs_secrets2: self.hs_secrets2,
						hs_hash: hs_hash,
						flags: self.flags,
					}));
				},
				//Signature was not ready immediately. Register a waiter to wait for the future.
				Err(x) => FutureAMO::new(x, connection)
			};
			debug!(HANDSHAKE_EVENTS debug, "Client certificate: Requesting signature");
			Ok(HandshakeState2::Tls13ClientCertificateVerify(Tls13ClientCertificateVerifyFields {
				signature: signature,
				hs_secrets2: self.hs_secrets2,
				hs_hash: self.hs_hash,
				peer_sigalgo_mask: self.cc.peer_sigalgo_mask,
				flags: self.flags,
			}))
		} else {
			//Refuse authentication.
			tx_certificate_client_nak(Some(&[]), debug.clone(), base, &mut self.hs_hash, sname)?;
			Ok(HandshakeState2::Tls13ClientFinished(Tls13ClientFinishedFields{
				hs_secrets2: self.hs_secrets2,
				hs_hash: self.hs_hash,
				flags: self.flags,
			}))
		}
	}
}

pub(crate) struct Tls13ClientCertificateVerifyFields
{
	hs_secrets2: Tls13C2ndFSecrets,
	hs_hash: HandshakeHash,
	signature: FutureAMO<Result<(u16, Vec<u8>), ()>>,
	peer_sigalgo_mask: SetOf<SignatureAlgorithm2>,
	flags: Flags,
}

impl Tls13ClientCertificateVerifyFields
{
	pub fn ready(&self) -> bool { self.signature.ready() }
	pub fn tx_certificate_verify(self, base: &mut impl DowncallInterface, bp: BaseMessageParamsS) ->
		Result<HandshakeState2, Error>
	{
		//If not ready yet, wait more.
		let signature = f_return!(self.signature.take(),
			Ok(HandshakeState2::Tls13ClientCertificateVerify(self)));
		let BaseMessageParamsS{debug, sname, ..} = bp;

		let mut hs_hash = self.hs_hash;
		tx_certificate_verify(signature, self.peer_sigalgo_mask, None, base, &mut hs_hash, sname, None,
			debug.clone())?;

		Ok(HandshakeState2::Tls13ClientFinished(Tls13ClientFinishedFields {
			hs_secrets2: self.hs_secrets2,
			hs_hash: hs_hash,
			flags: self.flags,
		}))
	}
}
