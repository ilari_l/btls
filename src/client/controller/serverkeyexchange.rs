use super::BaseMessageParams;
use super::Flags;
use super::HandshakeState2;
use crate::client::controller::serverhellodone::Tls12ServerHelloDoneFields;
use crate::certificates::TrustedLog;
use crate::common::certificate_status_to_ocsp;
use crate::common::CollectedScts;
use crate::common::CryptoTracker;
use crate::common::emit_crypto_parameters_event;
use crate::common::GetExtraValidationParameters;
use crate::common::ValidateExtra;
use crate::errors::Error;
use crate::messages::HandshakeHash;
use crate::messages::HashMessages;
use crate::messages::KemKeyPair;
use crate::record::DiffieHellmanSharedSecretResponder;
use crate::record::IsEms;
use crate::record::Tls12PremasterSecret;
use crate::stdlib::Arc;
use crate::stdlib::Range;
use crate::stdlib::replace;
use crate::stdlib::String;
use crate::stdlib::Vec;
use btls_aux_aead::ProtectorType;
use btls_aux_dhf::KEM;
use btls_aux_dhf::kem_public_key_check2;
use btls_aux_hash::HashFunction2;
use btls_aux_random::TemporaryRandomLifetimeTag;
use btls_aux_random::TemporaryRandomStream;
use btls_aux_signatures::SignatureAlgorithmTls2;
use btls_aux_signatures::SignatureFlags2;
use btls_aux_tls_struct::EllipticCurve;
use btls_aux_tls_struct::parse_certificate_status;
use btls_aux_tls_struct::ServerKeyExchange as PServerKeyExchange;
use btls_aux_tls_struct::ServerPublicKey;
use btls_aux_tlskeyschedule::TlsServerKeyExchange as KsTlsServerKeyExchange;
use btls_aux_x509certparse::CertificateIssuer;
use core::fmt::Arguments;


static BLANK_U8: [u8;0] = [];

//Issuer key, EE certificate, Issuer range, Serial range, lifetime, maximum lifetime, Trusted logs.
struct Tls12ExtraValidation<'a>(Option<&'a [u8]>, &'a [u8], Range<usize>, Range<usize>, u64, u64, &'a [TrustedLog]);

impl<'b> GetExtraValidationParameters for Tls12ExtraValidation<'b>
{
	fn get_issuer<'a>(&'a self) -> CertificateIssuer<'a> {
		CertificateIssuer(self.1.get(self.2.clone()).unwrap_or(&BLANK_U8[..]))
	}
	fn get_serial<'a>(&'a self) -> &'a [u8] {
		self.1.get(self.3.clone()).unwrap_or(&BLANK_U8[..])
	}
	fn get_issuer_key<'a>(&'a self) -> Option<&'a [u8]> { self.0 }
	fn get_full_certificate<'a>(&'a self) -> &'a [u8] { self.1 }
	fn get_certificate_lifetime<'a>(&self) -> u64 { self.4 }
	fn get_maximum_lifetime<'a>(&self) -> u64 { self.5 }
	fn get_trusted_logs<'a>(&'a self) -> &'a [TrustedLog] { self.6 }
}

pub(crate) struct Tls12ServerKeyExchangeFields
{
	//The key shares that were generated for TLS 1.3. These can be reused for TLS 1.2 key exchange if the
	//algorithm is symmetric (e.g., Diffie-Hellman).
	pub tls13_shares: Vec<KemKeyPair>,
	//The SubjectPublicKeyInfo of the server, in X.509 SPKI format (including the leading SEQUENCE).
	pub server_spki: Vec<u8>,
	//The SignedCertificateTimestamp's (SCTs) that have been collected so far.
	pub staple_scts: CollectedScts,
	//The entiere EE certificate. Used for OCSP and SCT stapling.
	pub staple_ee_cert: Vec<u8>,
	//The key of the CA that issued the EE certificate, if any. Used for OCSP stapling.
	pub staple_issuer_key: Option<Vec<u8>>,
	//The range in EE certificate that contains the issuer. Used for OCSP stapling.
	pub staple_issuer: Range<usize>,
	//The range in EE certificate that contains the serial. Used for OCSP stapling.
	pub staple_serial: Range<usize>,
	//Status of extra validation.
	pub vextra: ValidateExtra,
	//The client random.
	pub client_random: [u8; 32],
	//The server random.
	pub server_random: [u8; 32],
	//The handshake hash so far.
	pub hs_hash: HandshakeHash,
	//The used protector type.
	pub protection: ProtectorType,
	//The used handshake hash function.
	pub prf: HashFunction2,
	//Number of seconds the certificate lives.
	pub certificate_lifetime: u64,
	//The names the server authenticated as.
	pub server_names: Option<Arc<Vec<String>>>,
	//The IPs the server authenticated as.
	pub server_ips: Option<Arc<Vec<String>>>,
	//The active cryptographic algorithms.
	pub crypto_algs: CryptoTracker,
	//Client capabilities.
	pub(crate) client_caps: crate::messages::ClientSupport,
	//The flags for handshake.
	pub flags: Flags,
}

impl Tls12ServerKeyExchangeFields
{
	//Recieve a certificate status message.
	pub fn rx_certificate_status(mut self, msg: &[u8], bp: BaseMessageParams) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParams{rawmsg, debug, config, ..} = bp;
		//Add the message to hash.
		self.hs_hash.add(rawmsg, debug.clone())?;
		//Parse the message.
		let mut debug_cb = |args:Arguments|debug!(TLS_EXTENSIONS debug, "Certificate Status: {args}");
		let enable = |f:btls_aux_tls_struct::RemoteFeature|self.client_caps.enable(config, f);
		let pmsg = parse_certificate_status(msg, &enable, &mut Some(&mut debug_cb)).
			map_err(|e|error!(msgerr_certificate_status e))?;
		let ocsp = certificate_status_to_ocsp(Some(pmsg));
		//Destructure the state.
		let vextra = self.vextra;
		let staple_scts = replace(&mut self.staple_scts, CollectedScts::new());
		let staple_ee_cert = replace(&mut self.staple_ee_cert, Vec::new());
		let staple_issuer_key = replace(&mut self.staple_issuer_key, None);
		let staple_serial = replace(&mut self.staple_serial, 0..0);
		let staple_issuer = replace(&mut self.staple_issuer, 0..0);
		let certificate_lifetime = self.certificate_lifetime;

		//Validate the SCTs and OCSP. There is no need to dump the SCTs from OCSP, since validate_extra()
		//does that by itself. validate_extra() also checks the status required flags.
		let vextra = vextra.close(ocsp, staple_scts,
			Tls12ExtraValidation(staple_issuer_key.as_deref(), &staple_ee_cert, staple_issuer,
			staple_serial, certificate_lifetime, config.ocsp_maxvalid, &config.trusted_logs),
			Some(&mut self.crypto_algs), &server_cert_msg!(debug))?;

		Ok(HandshakeState2::Tls12ServerKeyExchange(false, Tls12ServerKeyExchangeFields {
			//Update the extra validation status.
			vextra: vextra,
			hs_hash: self.hs_hash,
			flags: self.flags.fset_HAD_OCSP_RESPONSE(),
			..self
		}))
	}
	//Recieve a ServerKeyExchange message.
	pub fn rx_server_key_exchange(mut self, msg: &[u8], bp: BaseMessageParams) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParams{rawmsg, debug, config, callbacks, ..} = bp;
		let mut debug_cb = |args:Arguments|debug!(TLS_EXTENSIONS debug, "Server Key Exchange: {args}");
		let enable = |f:btls_aux_tls_struct::RemoteFeature|self.client_caps.enable(config, f);
		//The ciphersuite determines the type. This is close enough, always having correct kind.
		let cs = btls_aux_tls_iana::CipherSuite::ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256;
		let pmsg = PServerKeyExchange::parse(msg, cs, &enable,
			&|g,k|kem_public_key_check2(g,k).is_ok(), &mut Some(&mut debug_cb)).
			map_err(|e|error!(msgerr_server_key_exchange e))?;
		let (kem, point) = match pmsg.get_public_key() {
			Some(ServerPublicKey::Ec{curve:EllipticCurve::Named(a), point:b}) => (a,b),
			Some(ServerPublicKey::Ec{curve:EllipticCurve::ExplicitPrime(_), ..}) =>
				sanity_failed!("Custom prime curves are not supported"),
			Some(ServerPublicKey::Ec{curve:EllipticCurve::ExplicitBinary(_), ..}) =>
				sanity_failed!("Custom binary curves are not supported"),
			_ => sanity_failed!("Non-elliptic key exchange not supported"),
		};
		let signature = assert_some!(pmsg.get_signature(),
			"Only server signature is supported for authentication");
		let algorithm = signature.get_algorithm();
		let pmsg_algorithm = SignatureAlgorithmTls2::by_tls_id2(algorithm).ok_or_else(||{
			assert_failure!(F "Parsed server_key_exchange contains bad signature {algorithm}")
		})?;
		let pmsg_kem = KEM::by_tls_id2(kem, self.client_caps.get_enabled_kem()).ok_or_else(||{
			assert_failure!(F "Parsed server_key_exchange contains bad group {kem}")
		})?;

		let vextra = self.vextra;
		let mut crypto_algs = self.crypto_algs;

		//The certificate_status message is optional. If we did receive one, we already validated the SCT
		//and OCSP. However, if we did not, then we need to validate the SCT (OCSP is not present in that
		//case). validate_extra() does not do its thing twice.
		let cert_ok_tag = vextra.close(None, self.staple_scts,
			Tls12ExtraValidation(self.staple_issuer_key.as_deref(), &self.staple_ee_cert,
			self.staple_issuer, self.staple_serial, self.certificate_lifetime, config.ocsp_maxvalid,
			&config.trusted_logs), Some(&mut crypto_algs), &server_cert_msg!(debug))?.as_tag();

		//Try to pull out a key from TLS 1.3 key shares already generated. This destroys all the keys
		//except the correct one, if any.
		let mut _dhkey = None;
		let mut tls13_shares = self.tls13_shares;
		for KemKeyPair(sk, pk) in tls13_shares.drain(..) {
			if pk.kem() == pmsg_kem {
				//Hit!
				_dhkey = Some((sk, pk));
				break;
			}
		}
		//Key re-use is only possible if the algorithm is symmetric and suitable key was actually found.
		let sk = _dhkey.and_then(|(sk,_)|sk.into_responder());
		let sk = match sk { Some(x) => x, None => {
			//Either we did not generate key of correct group, or the algorithm was not symmetric.
			//In TLS 1.2, we are the responder (the reason for requiring symmetric key is that the
			//already generated TLS 1.3 shares are always initiator shares). Generate the key of
			//correct kind.
			let lifetime = TemporaryRandomLifetimeTag;
			assert_some!(pmsg_kem.responder(&mut TemporaryRandomStream::new_system(&lifetime)), F
				"Can't generate Diffie-Hellman keypair for group {kem}")
		}};
		debug!(HANDSHAKE_EVENTS debug, "Server key exchange: Key exchange is {kem}");

		//Set the premaster secret to DH result. This is TLS 1.2, so no zero block. Also, we are always
		//responder.
		let dhshared = DiffieHellmanSharedSecretResponder::new(sk, point)?;
		let pubkey = dhshared.get_public_key();
		crypto_algs.set_kex_kem(dhshared.get_kem());

		//No need to check if the key is floaty: That is already checked at the time the certificate was
		//received by do_validate_server_cert_chain() in certificate.rs. The same holds true for disabling
		//dynamic RSA. So always use ALLOW_RSA_PKCS1.
		let pmsg2 = KsTlsServerKeyExchange {
			kem: pmsg_kem,
			key_share: point,
			algorithm: pmsg_algorithm,
			signature: signature.get_signature(),
		};
		let premaster_secret = Tls12PremasterSecret::new_client(dhshared, self.protection, self.prf,
			IsEms(self.flags.is_EMS_ENABLED()), &self.server_spki, &self.client_random,
			&self.server_random, &pmsg2, SignatureFlags2::ALLOW_RSA_PKCS1,
			self.client_caps.get_enabled_sig(), ks_debug!(&debug))?;
		crypto_algs.set_signature(pmsg_algorithm);
		debug!(HANDSHAKE_EVENTS debug, "Server key exchange: Key exchange & server signature {algorithm}, ok");

		//Emit the crypto paramters, as now all the parameters are known. Clients are internal only if
		//NoDataTransfer is set.
		emit_crypto_parameters_event(&mut crypto_algs, callbacks,
			self.flags.is_NO_DATA_TRANSFER());
		//Include the message into handshake hash.
		self.hs_hash.add(rawmsg, debug)?;

		//Next wait for TLS 1.2 ServerHelloDone message.
		Ok(HandshakeState2::Tls12ServerHelloDone(false, Tls12ServerHelloDoneFields {
			pubkey: pubkey,
			premaster_secret: premaster_secret,
			client_random: self.client_random,
			server_random: self.server_random,
			hs_hash: self.hs_hash,
			server_names: self.server_names,
			server_ips: self.server_ips,
			cert_tag: cert_ok_tag,
			flags: self.flags,
		}))
	}
}
