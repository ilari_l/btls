use super::BaseMessageParamsS;
use super::Flags;
use super::HandshakeState2;
use crate::callbacks::KeyExportType as KET;
use crate::client::controller::finished::Tls12ClientFinishedFields;
use crate::errors::Error;
use crate::messages::HandshakeHash;
use crate::messages::tx_certificate_client_nak;
use crate::messages::tx_client_key_exchange;
use crate::record::DowncallInterface;
use crate::record::Tls12MasterSecret;
use crate::record::Tls12PremasterSecret;
use crate::stdlib::Arc;
use crate::stdlib::String;
use crate::stdlib::Vec;
use btls_aux_dhf::KemCiphertext;


pub(crate) struct Tls12ClientKeyExchangeFields
{
	pub pubkey: KemCiphertext,			//Public key.
	pub premaster_secret: Tls12PremasterSecret,	//Premaster secret.
	pub client_random: [u8; 32],			//The client random.
	pub server_random: [u8; 32],			//The server random.
	pub hs_hash: HandshakeHash,
	pub server_names: Option<Arc<Vec<String>>>,
	pub server_ips: Option<Arc<Vec<String>>>,
	pub flags: Flags,
}

impl Tls12ClientKeyExchangeFields
{
	//Transmit a certificate message.
	pub fn tx_certificate(mut self, base: &mut impl DowncallInterface, bp: BaseMessageParamsS) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParamsS{debug, sname, ..} = bp;
		//Refuse authentication.
		tx_certificate_client_nak(None, debug, base, &mut self.hs_hash, sname)?;
		Ok(HandshakeState2::Tls12ClientKeyExchange(self))
	}
	//Transmit a ClientKeyExchange message.
	pub fn tx_client_key_exchange(mut self, base: &mut impl DowncallInterface, bp: BaseMessageParamsS) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParamsS{debug, sname, ssl_key_log, callbacks, ..} = bp;
		tx_client_key_exchange(&self.pubkey, debug.clone(), base, &mut self.hs_hash, sname)?;
		let master_secret = Tls12MasterSecret::new(self.premaster_secret, &self.client_random,
			&self.server_random, self.hs_hash.checkpoint(), ks_debug!(&debug))?;
		if ssl_key_log {
			callbacks.ssl_key_log(KET::Tls12MasterSecret, self.client_random,
				master_secret.debug_export());
		}
		Ok(HandshakeState2::Tls12ClientFinished(Tls12ClientFinishedFields {
			master_secret: master_secret,
			hs_hash: self.hs_hash,
			server_names: self.server_names,
			server_ips: self.server_ips,
			flags: self.flags,
		}))
	}
}
