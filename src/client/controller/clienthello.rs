use super::BaseMessageParamsS;
use super::Flags;
use super::HandshakeState2;
use super::TlsCVInfo;
use crate::callbacks::TlsCallbacks;
use crate::client::ClientConfiguration;
use crate::client::controller::serverhello::ServerHelloFields;
use crate::common::Ciphersuite;
use crate::common::CryptoTracker;
use crate::common::supports_hardware_aes;
use crate::common::test_failure;
use crate::common::TlsVersion;
use crate::errors::Error;
use crate::features::FLAGS0_ENABLE_TLS12;
use crate::features::FLAGS0_MIDDLEBOX_COMPAT;
use crate::features::FLAGS0_NSA;
use crate::features::FLAGS5_TEST_CH_SEND_FAIL;
use crate::logging::DebugSetting;
use crate::messages::ClientHelloParameters;
use crate::messages::DumpHexblock;
use crate::messages::EarlyHandshakeRep;
use crate::messages::KemKeyPair;
use crate::messages::SignatureIterator;
use crate::messages::SignatureUsage;
use crate::messages::tx_client_hello;
use crate::record::DowncallInterface;
use crate::stdlib::Vec;
use btls_aux_set2::SetOf;
use btls_aux_signatures::SignatureAlgorithm2;
use btls_aux_tls_iana::NamedGroup;


pub(crate) struct ClientHelloFields
{
	pub cvinfo: TlsCVInfo,					//Server certificate validation info.
	pub cookie: Option<Vec<u8>>,				//Cookie the server passed back.
	pub hs_hash: EarlyHandshakeRep,				//The client hello, plus possible restart (with
								//headers).
	pub tls13_shares: Vec<KemKeyPair>,			//The key shares (also abused for TLS 1.2).
	pub ciphersuite_assigned: Option<Ciphersuite>,		//Assigned ciphersuite (used for HRR in 1.3-draft19+)
	pub version_assigned: Option<TlsVersion>,		//Assigned version (used for HRR).
	pub features: u32,					//Features supported.
	pub group_id: Option<NamedGroup>,			//The group ID from HRR.
	pub client_random: [u8; 32],
	pub crypto_algs: CryptoTracker,
	pub acme_hash: Option<[u8;32]>,				//ACME challenge hash.
	pub(crate) client_caps: crate::messages::ClientSupport,
	pub flags: Flags,
}

impl ClientHelloFields
{
	//This is its own scope so lifetimes work out without SEME lifetimes.
	fn tx_clienthello_inner(&mut self, base: &mut impl DowncallInterface, debug: DebugSetting,
		config: &ClientConfiguration, sname: &str, callbacks: &mut dyn TlsCallbacks) ->
		Result<(), Error>
	{
		//Client random for testing.
		#[cfg(test)] { callbacks.client_random(self.client_random); }
		//Compute set of all enabled algorithms.
		let mut sigs = SetOf::<SignatureAlgorithm2>::empty_set();
		SignatureIterator::new(SignatureUsage::Tls12Server, config.flags,
			self.client_caps.get_enabled_sig().iter_tls()).map(|sig|sigs.add(&sig)).count();

		let mut special_extensions = Vec::new();
		let mut cp = ClientHelloParameters {
			aes_gcm_is_hw: supports_hardware_aes(&config.flags),
			alpn: &config.supported_alpn,
			client_random: &self.client_random,
			cookie: self.cookie.as_deref(),
			enable_ecdsa: (sigs & SignatureAlgorithm2::SET_TLS12_ECDSA).has_elements(),
			enable_rsa: (sigs & SignatureAlgorithm2::SET_TLS12_RSA).has_elements(),
			flags: config.flags,
			is_acme: self.acme_hash.is_some(),
			key_shares: &self.tls13_shares,
			middlebox_compat: config.flags.is_flag(FLAGS0_MIDDLEBOX_COMPAT),
			might_use_spki_only: self.flags.is_MIGHT_USE_SPKI_ONLY(),
			min_ch_size: config.min_ch_size,
			no_data_transfer: self.flags.is_NO_DATA_TRANSFER(),
			offer_tls12: config.flags.is_flag(FLAGS0_ENABLE_TLS12) && !config.raw_handshake,
			sni: if !self.flags.is_NO_REQUEST_SNI() && !config.suppress_sni {
				Some(&self.cvinfo.requested_sni)
			} else {
				None
			},
			special_extensions: &mut special_extensions,
		};
		tx_client_hello(&mut cp, debug.clone(), base, &mut self.hs_hash, sname, callbacks,
			&mut self.client_caps)?;
		Ok(())
	}
	//Send a ClientHello message.
	pub fn tx_clienthello(mut self, base: &mut impl DowncallInterface, bp: BaseMessageParamsS, retry: bool) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParamsS{debug, sname, config, callbacks, ..} = bp;
		#[cfg(not(test))]
		{sanity_check!(config.flags.get_test() == 0, "Flags page 5 (tests) not empty");}
		test_failure(&config.flags, FLAGS5_TEST_CH_SEND_FAIL)?;
		//let _aes_gcm_is_hw = supports_hardware_aes(&config.flags);
		if config.flags.is_flag(FLAGS0_NSA) { self.crypto_algs.nsa_mode(); }

		if !retry {
			debug!(CRYPTO_CALCS debug, "Using client random:\n{random}",
				random=DumpHexblock::new(&self.client_random, "  ", "Client random"));
		} else if self.flags.is_COMPATIBILITY_MODE() {
			//Send fake CCS.
			base.send_clear_ccs();
		}

		self.tx_clienthello_inner(base, debug.clone(), config, sname, callbacks)?;
		base.force_break()?;
		let inner = ServerHelloFields{
			ciphersuite_assigned: self.ciphersuite_assigned,
			version_assigned: self.version_assigned,
			hs_hash: self.hs_hash,
			features: self.features,
			group_id: self.group_id,
			cvinfo: self.cvinfo,
			tls13_shares: self.tls13_shares,
			client_random: self.client_random,
			crypto_algs: self.crypto_algs,
			acme_hash: self.acme_hash,
			client_caps: self.client_caps,
			flags: self.flags,
		};
		let ctor = if retry {
			HandshakeState2::ServerHelloWaitAgain
		} else {
			HandshakeState2::ServerHello
		};
		Ok(ctor(inner))
	}
}
