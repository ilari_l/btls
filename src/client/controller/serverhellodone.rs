use super::BaseMessageParams;
use super::Flags;
use super::HandshakeState2;
use crate::client::controller::clientkeyexchange::Tls12ClientKeyExchangeFields;
use crate::common::ValidateExtraTag;
use crate::errors::Error;
use crate::messages::HandshakeHash;
use crate::messages::HashMessages;
use crate::record::Tls12PremasterSecret;
use crate::stdlib::Arc;
use crate::stdlib::String;
use crate::stdlib::Vec;
use btls_aux_dhf::KemCiphertext;


pub(crate) struct Tls12ServerHelloDoneFields
{
	//The client public key exchange key.
	pub pubkey: KemCiphertext,
	//The TLS 1.2 premaster secret.
	pub premaster_secret: Tls12PremasterSecret,
	//The client random.
	pub client_random: [u8; 32],
	//The server random.
	pub server_random: [u8; 32],
	//The handshake hash so far.
	pub hs_hash: HandshakeHash,
	//The names the server authenticated as.
	pub server_names: Option<Arc<Vec<String>>>,
	//The IPs the server authenticated as.
	pub server_ips: Option<Arc<Vec<String>>>,
	pub cert_tag: ValidateExtraTag,
	//The flags for handshake.
	pub flags: Flags,
}

impl Tls12ServerHelloDoneFields
{
	//Recieve a certificate_request message.
	pub fn rx_certificate_request(self, _data: &[u8], bp: BaseMessageParams) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParams{rawmsg, debug, ..} = bp;

		//We do not support TLS 1.2 certificate requests, so we just mark this message as received so
		//we can NAK the certificate authentication and include it in the handshake hash.
		let mut hs_hash = self.hs_hash;
		hs_hash.add(rawmsg, debug)?;

		//Next up, real ServerHelloDone.
		Ok(HandshakeState2::Tls12ServerHelloDone(true, Tls12ServerHelloDoneFields {
			hs_hash: hs_hash,
			..self
		}))
	}
	//Recieve a server_hello_done message.
	pub fn rx_server_hello_done(self, data: &[u8], bp: BaseMessageParams, certificate_reqd: bool) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParams{rawmsg, debug, ..} = bp;

		//This message is supposed to be empty.
		fail_if!(data.len() != 0, error!(server_hello_done_not_empty data.len()));
		//Check that certificate has been duly validated.
		sanity_check!(self.cert_tag.is_done(), "Improper server certificate validation");

		//Ignore the entiere message, except including it into handshake hash. This message only
		//signifies direction change.
		let mut hs_hash = self.hs_hash;
		hs_hash.add(rawmsg, debug)?;

		//Next up, client_key_exchange.
		let inner = Tls12ClientKeyExchangeFields {
			pubkey: self.pubkey,
			premaster_secret: self.premaster_secret,
			client_random: self.client_random,
			server_random: self.server_random,
			hs_hash: hs_hash,
			server_names: self.server_names,
			server_ips: self.server_ips,
			flags: self.flags,
		};
		//However, if the server demaneded a certificate, we need to send a NAK for that request first.
		let next = if certificate_reqd {
			HandshakeState2::Tls12ClientCertificate
		} else {
			HandshakeState2::Tls12ClientKeyExchange
		};
		Ok(next(inner))
	}
}
