use self::certificate::Tls12CertificateFields;
use self::certificate::Tls13CertificateFields;
use self::certificate::Tls13CertificateVerifyFields;
use self::certificate::Tls13ClientCertificateFields;
use self::certificate::Tls13ClientCertificateVerifyFields;
use self::clienthello::ClientHelloFields;
use self::clientkeyexchange::Tls12ClientKeyExchangeFields;
use self::encryptedextensions::Tls13EncryptedExtensionsFields;
use self::finished::Tls12ClientFinishedFields;
use self::finished::Tls12ServerChangeCipherSpecFields;
use self::finished::Tls12ServerFinishedFields;
use self::finished::Tls13ClientFinishedFields;
use self::finished::Tls13ServerFinishedFields;
use self::serverhello::ServerHelloFields;
use self::serverhellodone::Tls12ServerHelloDoneFields;
use self::serverkeyexchange::Tls12ServerKeyExchangeFields;
use super::ClientConfiguration;
use super::ClientConnection;
use crate::callbacks::TlsCallbacks;
use crate::certificates::HostSpecificPin;
use crate::common::CryptoTracker;
use crate::common::tls13_update_recv_key;
use crate::common::tls13_update_send_key;
use crate::errors::Error;
use crate::features::*;
use crate::logging::DebugSetting;
use crate::messages::ClientCertificateObject;
use crate::messages::ClientSupport;
use crate::messages::EarlyHandshakeRep;
use crate::messages::HandshakeMessage;
use crate::messages::KemKeyPair;
use crate::record::DowncallInterface;
use crate::record::Tls13MasterSecret;
use crate::record::Tls13TrafficSecretIn;
use crate::record::Tls13TrafficSecretOut;
use crate::record::UnsignedTls13HandshakeSecret;
use crate::record::UpcallInterface;
use crate::record::UpcallReturn;
use crate::stdlib::Box;
use crate::stdlib::DerefMut;
use crate::stdlib::FromStr;
use crate::stdlib::replace;
use crate::stdlib::String;
use crate::stdlib::ToOwned;
use crate::stdlib::Vec;
use btls_aux_bitflags::Bitflags;
use btls_aux_dhf::KemEnabled;
use btls_aux_ip::IpAddress;
use btls_aux_random::secure_random;
use btls_aux_random::TemporaryRandomLifetimeTag;
use btls_aux_random::TemporaryRandomStream;
use btls_aux_set2::SetOf;
use btls_aux_signatures::SignatureAlgorithm2;
pub use btls_aux_time::PointInTime;
pub use btls_aux_time::TimeUnit;
use btls_aux_tls_iana::HandshakeType;
use btls_aux_tls_struct::parse_key_update;
use btls_aux_x509certparse::CFLAG_UNKNOWN;
use btls_aux_x509certvalidation::ReferenceName;
use core::fmt::Arguments;


//Macro because of lifetime rules.
macro_rules! server_cert_msg
{
	($debug:expr) => { |ev:Arguments|debug!(HANDSHAKE_EVENTS $debug, "Server certificate: {ev}") }
}

//Macro because of lifetime rules.
macro_rules! grab_issuer_key
{
	($icerts:expr, $name:expr, $talist:expr) => {
		match ($icerts.get(0), $name) {
			(Some(x), _) => Some(extract_spki(x).map_err(|x|error!(cant_parse_issuer_certificate x))?),
			//Grab this from the trustpile.
			(None, Some(name2)) => $talist.get(name2.0).and_then(|x|x.borrow_inner().get_spki()),
			//RPKs can't have OCSP responses.
			(None, None) => None,
		}
	}
}

mod certificate;
mod clienthello;
mod clientkeyexchange;
mod encryptedextensions;
mod finished;
mod serverhello;
mod serverhellodone;
mod serverkeyexchange;

#[derive(Copy,Clone,Debug,Bitflags)]
//If set, the server certificate is just a Raw Public Key, instead of full certificate. This is set in ServerHello
//for TLS 1.2, and EncryptedExtensions for TLS 1.3-22. It is not used for TLS 1.3-(17-21).
#[bitflag(USE_SPKI_ONLY)]
//If set, the server may send back UseSpkiOnly, instead of it being disallowed. This is set at construction time.
#[bitflag(MIGHT_USE_SPKI_ONLY)]
//If set, SNI is not sent in request. This is set at construction time.
#[bitflag(NO_REQUEST_SNI)]
//If set, the key kill list is forced empty. This is set at construction time.
#[bitflag(KILL_LIST_EMPTY)]
//Disable data transfer, immediately exchanging EOFs.
#[bitflag(NO_DATA_TRANSFER)]
//If set, the server indicated it might send certificate status. Only used in TLS 1.2, set in ServerHello
#[bitflag(MAYBE_SEND_STATUS)]
//If set, the server indicated it supports Extended Master Secret. Only used in TLS 1.2, set in ServerHello.
#[bitflag(EMS_ENABLED)]
//If set, OCSP response was received. Only used in TLS 1.2, set in CertificateStatus.
#[bitflag(HAD_OCSP_RESPONSE)]
//If set, the client certificate is just a Raw Public Key, instead of a full certificate. Only used in TLS 1.3, set
//in EncryptedExtensions.
#[bitflag(USE_CLIENT_RPK)]
//If set, TLS 1.3 Compatibility Mode is in use. Only used in TLS 1.3, set in HelloRetryRequest and ServerHello.
#[bitflag(COMPATIBILITY_MODE)]
//If set, The server has requested a client certificate. Only set in TLS 1.3, set in CertificateRequest.
#[bitflag(REQUESTED_CERTIFICATE)]
//If set, do not generate spontaneous rekey events. Only used in TLS 1.3, set in ServerHello.
#[bitflag(NO_SPONTANEOUS_REKEY)]
//If set, a rekey event is in progress. Only used in TLS 1.3, set in KeyUpdate.
#[bitflag(REKEY_IN_PROGRESS)]
pub struct Flags(u8, u8);


pub struct Tls13HsSecrets
{
	server_hs_write: Tls13TrafficSecretIn,
	client_hs_write: Tls13TrafficSecretOut,
	master_secret: Tls13MasterSecret,
}

pub struct Tls13HsSecretsPre
{
	server_hs_write: Tls13TrafficSecretIn,
	client_hs_write: Tls13TrafficSecretOut,
	handshake_secret: UnsignedTls13HandshakeSecret,
}

pub struct Tls13C2ndFSecrets
{
	server_app_write: Tls13TrafficSecretIn,
	client_app_write: Tls13TrafficSecretOut,
	client_hs_write: Tls13TrafficSecretOut,
}

#[derive(Clone,Debug)]
pub enum ReferenceNameOwned
{
	DnsName(String),
	Ip(IpAddress),
}

impl ReferenceNameOwned
{
	fn borrow<'a>(&'a self) -> ReferenceName<'a>
	{
		match self {
			&ReferenceNameOwned::DnsName(ref x) => ReferenceName::DnsName(x),
			&ReferenceNameOwned::Ip(x) => ReferenceName::Ip(x),
		}
	}
}

pub struct TlsCVInfo
{
	requested_sni: ReferenceNameOwned,		//The SNI that has been requested.
	pins: Vec<HostSpecificPin>,			//Host-specific pins
	#[allow(dead_code)]
	flags: Flags,
}

pub struct TlsClientCert
{
	client_cert: Option<ClientCertificateObject>,
	peer_sigalgo_mask: SetOf<SignatureAlgorithm2>,
}

pub(crate) struct Tls12ShowtimeFields
{
}

pub(crate) struct Tls13ShowtimeFields
{
	server_app_write: Tls13TrafficSecretIn,
	client_app_write: Tls13TrafficSecretOut,
	flags: Flags,
}

pub(crate) enum HandshakeState2
{
	ClientHello(ClientHelloFields),
	ServerHello(ServerHelloFields),
	RetryClientHello(ClientHelloFields),
	ServerHelloWaitAgain(ServerHelloFields),
	Tls12Certificate(Tls12CertificateFields),
	Tls12ServerKeyExchange(bool, Tls12ServerKeyExchangeFields),	//Flag is for certificate status.
	Tls12ServerHelloDone(bool, Tls12ServerHelloDoneFields),		//Flag is for certificate requested.
	Tls12ClientCertificate(Tls12ClientKeyExchangeFields),	//This shares fields with CKE.
	Tls12ClientKeyExchange(Tls12ClientKeyExchangeFields),
	Tls12ClientFinished(Tls12ClientFinishedFields),
	Tls12ServerChangeCipherSpec(Tls12ServerChangeCipherSpecFields),
	Tls12ServerFinished(Tls12ServerFinishedFields),
	Tls12Showtime(Tls12ShowtimeFields),
	Tls13EncryptedExtensions(Tls13EncryptedExtensionsFields),
	Tls13Certificate(bool, Tls13CertificateFields),			//Flag is for certificate requested.
	Tls13CertificateVerify(Tls13CertificateVerifyFields),
	Tls13ServerFinished(Tls13ServerFinishedFields),
	Tls13ClientCertificate(Tls13ClientCertificateFields),
	Tls13ClientCertificateVerify(Tls13ClientCertificateVerifyFields),
	Tls13ClientFinished(Tls13ClientFinishedFields),
	Tls13Showtime(Tls13ShowtimeFields),
	Error,
}

#[test]
fn test_handshakestate()
{
	assert_eq!(crate::stdlib::size_of::<HandshakeState2>(), 792);
	assert_eq!(crate::stdlib::size_of::<ClientHelloFields>(), 600);
	assert_eq!(crate::stdlib::size_of::<ServerHelloFields>(), 576);
	assert_eq!(crate::stdlib::size_of::<Tls12CertificateFields>(), 624);
	assert_eq!(crate::stdlib::size_of::<Tls12ServerKeyExchangeFields>(), 664);
	assert_eq!(crate::stdlib::size_of::<Tls12ServerHelloDoneFields>(), 744);
	assert_eq!(crate::stdlib::size_of::<Tls12ClientKeyExchangeFields>(), 744);
	assert_eq!(crate::stdlib::size_of::<Tls12ClientFinishedFields>(), 488);
	assert_eq!(crate::stdlib::size_of::<Tls12ServerChangeCipherSpecFields>(), 488);
	assert_eq!(crate::stdlib::size_of::<Tls12ServerFinishedFields>(), 488);
	assert_eq!(crate::stdlib::size_of::<Tls12ShowtimeFields>(), 0);
	assert_eq!(crate::stdlib::size_of::<Tls13EncryptedExtensionsFields>(), 752);
	assert_eq!(crate::stdlib::size_of::<Tls13CertificateFields>(), 784);
	assert_eq!(crate::stdlib::size_of::<Tls13CertificateVerifyFields>(), 720);
	assert_eq!(crate::stdlib::size_of::<Tls13ServerFinishedFields>(), 648);
	assert_eq!(crate::stdlib::size_of::<Tls13ClientCertificateFields>(), 600);
	assert_eq!(crate::stdlib::size_of::<Tls13ClientCertificateVerifyFields>(), 576);
	assert_eq!(crate::stdlib::size_of::<Tls13ClientFinishedFields>(), 568);
	assert_eq!(crate::stdlib::size_of::<Tls13ShowtimeFields>(), 143);
}

impl HandshakeState2
{
	fn name(&self) -> &'static str
	{
		match self {
			&HandshakeState2::ClientHello(_) => "ClientHello",
			&HandshakeState2::ServerHello(_) => "ServerHello",
			&HandshakeState2::RetryClientHello(_) => "RetryClientHello",
			&HandshakeState2::ServerHelloWaitAgain(_) => "ServerHelloWaitAgain",
			&HandshakeState2::Tls12Certificate(_) => "Tls12Certificate",
			&HandshakeState2::Tls12ServerKeyExchange(false, _) => "Tls12ServerKeyExchange",
			&HandshakeState2::Tls12ServerKeyExchange(true, _) => "Tls12ServerKeyExchange(or CS)",
			&HandshakeState2::Tls12ServerHelloDone(false, _) => "Tls12ServerHelloDone",
			&HandshakeState2::Tls12ServerHelloDone(true, _) => "Tls12ServerHelloDone(with CR)",
			&HandshakeState2::Tls12ClientCertificate(_) => "Tls12ClientCertificate",
			&HandshakeState2::Tls12ClientKeyExchange(_) => "Tls12ClientKeyExchange",
			&HandshakeState2::Tls12ClientFinished(_) => "Tls12ClientFinished",
			&HandshakeState2::Tls12ServerChangeCipherSpec(_) => "Tls12ServerChangeCipherSpec",
			&HandshakeState2::Tls12ServerFinished(_) => "Tls12ServerFinished",
			&HandshakeState2::Tls12Showtime(_) => "Tls12Showtime",
			&HandshakeState2::Tls13EncryptedExtensions(_) => "Tls13EncryptedExtensions",
			&HandshakeState2::Tls13Certificate(false, _) => "Tls13Certificate",
			&HandshakeState2::Tls13Certificate(true, _) => "Tls13Certificate(with CR)",
			&HandshakeState2::Tls13CertificateVerify(_) => "Tls13CertificateVerify",
			&HandshakeState2::Tls13ServerFinished(_) => "Tls13ServerFinished",
			&HandshakeState2::Tls13ClientCertificate(_) => "Tls13ClientCertificate",
			&HandshakeState2::Tls13ClientCertificateVerify(_) => "Tls13ClientCertificateVerify",
			&HandshakeState2::Tls13ClientFinished(_) => "Tls13ClientFinished",
			&HandshakeState2::Tls13Showtime(_) => "Tls13Showtime",
			&HandshakeState2::Error => "Error"
		}
	}
}

pub struct BaseMessageParams<'a,'b,'e>
{
	rawmsg: HandshakeMessage<'a>,
	debug: DebugSetting,
	config: &'b ClientConfiguration,
	ssl_key_log: bool,
	callbacks: &'e mut dyn TlsCallbacks,
	_dummy: (),
}

pub struct BaseMessageParamsS<'a,'b,'d>
{
	debug: DebugSetting,
	config: &'a ClientConfiguration,
	sname: &'b str,
	ssl_key_log: bool,
	callbacks: &'d mut dyn TlsCallbacks,
	_dummy: (),
}


pub(crate) struct HandshakeController
{
	pub config:ClientConfiguration,			//The client configuration.
	hs_state: HandshakeState2,			//The handshake state.
	ssl_key_log: bool,
	pub debug: DebugSetting,
	pub callbacks: Box<dyn TlsCallbacks+Send>,
}

fn _illegal_state() -> Result<Error, Error> { sanity_failed!("Attempt to TX in illegal state!"); }

fn illegal_state() -> Error { match _illegal_state() { Ok(x) => x, Err(x) => x } }

impl UpcallInterface for HandshakeController
{
	type Waker = ClientConnection;
	fn _set_cid(&mut self, cid: u64) { self.debug.set_cid(cid); }
	fn _recv_handshake(&mut self, htype: HandshakeType, data: &[u8], controller: &mut UpcallReturn,
		last: bool) -> Result<(), Error>
	{
		let debug = self.debug.clone();
		let raw_msg2 = HandshakeMessage::new(htype, data);
		let baseparam = BaseMessageParams{
			rawmsg: raw_msg2,
			debug: debug.clone(),
			config: &self.config,
			ssl_key_log: self.ssl_key_log,
			callbacks: self.callbacks.deref_mut(),
			_dummy: ()
		};
		self.hs_state = match (replace(&mut self.hs_state, HandshakeState2::Error), htype) {
			(HandshakeState2::ServerHello(x), HandshakeType::SERVER_HELLO) => {
				x.rx_server_hello_2(controller, data, baseparam, last)?
			},
			(HandshakeState2::ServerHelloWaitAgain(x), HandshakeType::SERVER_HELLO) => {
				x.rx_server_hello_2(controller, data, baseparam, last)?
			},
			(HandshakeState2::Tls12Certificate(x), HandshakeType::CERTIFICATE) => {
				let _use_spki_only = x.flags.is_USE_SPKI_ONLY();
				x.rx_certificate_2(data, _use_spki_only, baseparam)?
			},
			(HandshakeState2::Tls12ServerKeyExchange(_, x), HandshakeType::SERVER_KEY_EXCHANGE) => {
				x.rx_server_key_exchange(data, baseparam)?
			},
			//This requires the MaybeSendStatus flag to be true.
			(HandshakeState2::Tls12ServerKeyExchange(true, x), HandshakeType::CERTIFICATE_STATUS)  => {
				x.rx_certificate_status(data, baseparam)?
			},
			(HandshakeState2::Tls12ServerHelloDone(false, x), HandshakeType::CERTIFICATE_REQUEST) => {
				x.rx_certificate_request(data, baseparam)?
			},
			(HandshakeState2::Tls12ServerHelloDone(y, x), HandshakeType::SERVER_HELLO_DONE) => {
				x.rx_server_hello_done(data, baseparam, y)?
			},
			(HandshakeState2::Tls12ServerFinished(x), HandshakeType::FINISHED) => {
				//Finished.
				fail_if!(!last, error!(finished_not_aligned));
				let tmp = x.rx_finished(controller, data, baseparam)?;
				tmp
			},
			(HandshakeState2::Tls13EncryptedExtensions(x), HandshakeType::ENCRYPTED_EXTENSIONS) => {
				x.rx_encrypted_extensions(controller, data, baseparam)?
			},
			(HandshakeState2::Tls13Certificate(false, x), HandshakeType::CERTIFICATE_REQUEST) => {
				x.rx_certificate_request(data, baseparam)?
			}
			(HandshakeState2::Tls13Certificate(y, x), HandshakeType::CERTIFICATE) => {
				//Certificate
				let _use_spki_only = x.flags.is_USE_SPKI_ONLY();
				x.rx_certificate_2(data, _use_spki_only, baseparam, y)?
			},
			(HandshakeState2::Tls13CertificateVerify(x), HandshakeType::CERTIFICATE_VERIFY) => {
				x.rx_certificate_verify(controller, data, baseparam)?
			},
			(HandshakeState2::Tls13ServerFinished(x), HandshakeType::FINISHED) => {
				//Finished
				fail_if!(!last, error!(finished_not_aligned));
				x.rx_finished(controller, data, baseparam)?
			},
			(HandshakeState2::Tls13Showtime(x), HandshakeType::CERTIFICATE_REQUEST) => {
				//CertificateRequest. Just ignore this.
				debug!(HANDSHAKE_EVENTS debug,
					"Unimplemented: Post-handshake auth. Will not respond.");
				HandshakeState2::Tls13Showtime(x)
			},
			(HandshakeState2::Tls13Showtime(mut x), HandshakeType::KEY_UPDATE) => {
				fail_if!(!last, error!(key_update_not_aligned));
				let debug = baseparam.debug.clone();
				let mut debug_cb = |args:Arguments|debug!(TLS_EXTENSIONS debug, "Key Update: {args}");
				let requested = parse_key_update(data, &mut Some(&mut debug_cb)).
					map_err(|e|error!(msgerr_key_update e))?;
				x.server_app_write.rachet(ks_debug!(&debug))?;
				tls13_update_recv_key(controller, &x.server_app_write,
					self.config.raw_handshake, debug.clone(), baseparam.callbacks)?;
				debug!(HANDSHAKE_EVENTS debug, "Rekey message: Downstream rekeyed");
				if !controller.last_tx_key_update() && !x.flags.is_REKEY_IN_PROGRESS() &&
					requested {
					//These have to be in this order for the handshake to go out with old keys.
					controller.send_key_update(false, "Tls13Showtime");
					x.client_app_write.rachet(ks_debug!(&debug))?;
					tls13_update_send_key(controller, &x.client_app_write,
						self.config.raw_handshake, debug.clone(), baseparam.callbacks)?;
					debug!(HANDSHAKE_EVENTS debug,
						"Rekey message: Upstream rekeyed (reciproal)");
				}
				x.flags.clear_REKEY_IN_PROGRESS();
				HandshakeState2::Tls13Showtime(x)
			}
			(HandshakeState2::Tls13ClientFinished(x), HandshakeType::NEW_SESSION_TICKET) => {
				//Just ignore this, we don't support tickets.
				//This can happen if server sends session ticket with predicted ClientHello and
				//wins the race.
				HandshakeState2::Tls13ClientFinished(x)
			},
			(HandshakeState2::Tls13Showtime(x), HandshakeType::NEW_SESSION_TICKET) => {
				//Just ignore this, we don't support tickets.
				HandshakeState2::Tls13Showtime(x)
			},
			(state, htype2) => {
				fail!(error!(unexpected_handshake_message htype2, state.name()));
			}
		};
		Ok(())
	}
	fn _recv_change_cipher_spec(&mut self, controller: &mut UpcallReturn) -> Result<(), Error>
	{
		//ChangeCipherSpec.
		self.hs_state = match replace(&mut self.hs_state, HandshakeState2::Error) {
			HandshakeState2::Tls12ServerChangeCipherSpec(x) =>
				x.rx_change_cipher_spec(controller, self.debug.clone())?,
			//TLS 1.3 compatibility mode.
			HandshakeState2::RetryClientHello(x) => HandshakeState2::RetryClientHello(x),
			HandshakeState2::ServerHelloWaitAgain(x) => HandshakeState2::ServerHelloWaitAgain(x),
			//CCS is illegal in all other states.
			x => fail!(error!(unexpected_ccs x.name()))
		};
		Ok(())
	}
	fn _get_state_name(&self) -> &'static str { self.hs_state.name() }
	fn handle_tx_update(&mut self, controller: &mut UpcallReturn) -> Result<(), Error>
	{
		let debug = self.debug.clone();
		//These have to be in this order for the handshake to go out with old keys.
		if let &mut HandshakeState2::Tls13Showtime(ref mut x) = &mut self.hs_state {
			//If no spontaneous rekeys flag is set, abort the rekey.
			if x.flags.is_NO_SPONTANEOUS_REKEY() { return Ok(()); }
			controller.send_key_update(true, "Tls13Showtime");
			x.client_app_write.rachet(ks_debug!(&debug))?;
			tls13_update_send_key(controller, &x.client_app_write, self.config.raw_handshake,
				debug.clone(), self.callbacks.deref_mut())?;
			x.flags.set_REKEY_IN_PROGRESS();
			debug!(HANDSHAKE_EVENTS debug, "Rekey message: Upstream rekeyed (spontaneous)");
		}
		Ok(())
	}
	fn _get_callbacks<'a>(&'a mut self) -> &'a mut dyn TlsCallbacks { self.callbacks.deref_mut() }
	fn wants_tx(&self) -> bool
	{
		match &self.hs_state {
			&HandshakeState2::ClientHello(_) => true,
			&HandshakeState2::RetryClientHello(_) => true,
			&HandshakeState2::Tls12ClientCertificate(_) => true,
			&HandshakeState2::Tls12ClientKeyExchange(_) => true,
			&HandshakeState2::Tls12ClientFinished(_) => true,
			&HandshakeState2::Tls13ClientCertificate(_) => true,
			&HandshakeState2::Tls13ClientCertificateVerify(ref x) => x.ready(),
			&HandshakeState2::Tls13ClientFinished(_) => true,
			_ => false
		}
	}
	fn wants_rx(&self) -> bool
	{
		match &self.hs_state {
			&HandshakeState2::ServerHello(_) => true,
			&HandshakeState2::ServerHelloWaitAgain(_) => true,
			&HandshakeState2::Tls12Certificate(_) => true,
			&HandshakeState2::Tls12ServerKeyExchange(_, _) => true,
			&HandshakeState2::Tls12ServerHelloDone(_, _) => true,
			&HandshakeState2::Tls12ServerChangeCipherSpec(_) => true,
			&HandshakeState2::Tls12ServerFinished(_) => true,
			&HandshakeState2::Tls13EncryptedExtensions(_) => true,
			&HandshakeState2::Tls13Certificate(_, _) => true,
			&HandshakeState2::Tls13CertificateVerify(_) => true,
			&HandshakeState2::Tls13ServerFinished(_) => true,
			_ => false
		}
	}
	fn is_blocked(&self) -> bool
	{
		//The blocked states are thos with ready() method, returning false.
		match &self.hs_state {
			&HandshakeState2::Tls13ClientCertificateVerify(ref x) => !x.ready(),
			_ => false,
		}
	}
	fn queue_hs_tx(&mut self, base: &mut impl DowncallInterface, upper: ClientConnection) -> Result<bool, Error>
	{
		let ret = match &self.hs_state {
			&HandshakeState2::Tls12ClientFinished(_) => true,
			&HandshakeState2::Tls13ClientFinished(_) => true,
			_ => false
		};
		let debug = self.debug.clone();
		let sname = self.hs_state.name();
		self.hs_state = {
			let baseparam = BaseMessageParamsS{
				sname: sname,
				debug: debug.clone(),
				config: &self.config,
				ssl_key_log: self.ssl_key_log,
				callbacks: self.callbacks.deref_mut(),
				_dummy: ()
			};
			match replace(&mut self.hs_state, HandshakeState2::Error) {
				HandshakeState2::ClientHello(x) => x.tx_clienthello(base, baseparam, false),
				HandshakeState2::RetryClientHello(x) => x.tx_clienthello(base, baseparam, true),
				HandshakeState2::Tls12ClientCertificate(x) => x.tx_certificate(base, baseparam),
				HandshakeState2::Tls12ClientKeyExchange(x) =>
					x.tx_client_key_exchange(base, baseparam),
				HandshakeState2::Tls12ClientFinished(x) => x.tx_finished(base, baseparam),
				HandshakeState2::Tls13ClientCertificate(x) =>
					x.tx_certificate(base, baseparam, upper),
				HandshakeState2::Tls13ClientCertificateVerify(x) =>
					x.tx_certificate_verify(base, baseparam),
				HandshakeState2::Tls13ClientFinished(x) => x.tx_finished(base, baseparam),
				_ => Err(illegal_state())
			}
		}?;
		Ok(ret)
	}
}

impl HandshakeController
{
	fn init_shares(config: &ClientConfiguration, enabled_kem: KemEnabled) -> Vec<KemKeyPair>
	{
		let mut res = Vec::new();
		if !config.flags.is_flag(FLAGS0_DEBUG_NO_SHARES) {
			let pq = config.flags.is_flag(FLAGS0_POST_QUANTUM_SHARE);
			//Generate share for the first enabled group, if any.
			//Strictly, this is wrong: This key may wind up being used for TLS 1.2, where we would be
			//responder. However, we later clear the key if the KEM is asymmetrical.
			//Also, don't use groups that are not OK for TLS 1.2, as those can be pretty large (unless
			//specifically requested post-quantum).
			//Avoid private-use codepoints unless specifically enabled.
			let lifetime = TemporaryRandomLifetimeTag;
			let mut rng = TemporaryRandomStream::new_system(&lifetime);
			enabled_kem.iter().filter(|kem|{
				if pq { kem.quantum_hard() } else { kem.suitable_for_tls12() }
			}).next().and_then(|f|{
				f.generate_key(&mut rng).ok()
			}).map(|x|{
				res.push(KemKeyPair::pair(x))
			});
		}
		res
	}
	pub fn new(config: &ClientConfiguration, host: &str, debug: DebugSetting,
		callbacks: Box<dyn TlsCallbacks+Send+'static>) -> HandshakeController
	{
		const DUMMY: [HostSpecificPin;0] = [];
		Self::new_pinned(config, host, &DUMMY[..], debug, callbacks)
	}
	pub fn new_pinned(config: &ClientConfiguration, host: &str, pins: &[HostSpecificPin],
		debug: DebugSetting, mut callbacks: Box<dyn TlsCallbacks+Send+'static>) -> HandshakeController
	{
		//Check if we have pins suitable for RPK mode.
		let _might_use_spki_only = {
			let any_pin = {
				let mut any_pin = false;
				for i in pins.iter() { any_pin |= i.pin; }
				any_pin
			};
			let mut _might_use_spki_only = false;
			//Check host pins against EE SPKI.
			for i in pins.iter() {
				//If pin is a CA pin, it does not match.
				if i.ca_flag == Some(true) { continue; }
				//If pin is not a TA, it does not match.
				if !i.ta { continue; }
				//If pin is not a pin and there are pins, it does not match.
				if !i.pin && any_pin { continue; }
				//If pin is not for SPKI, it does not match.
				if !i.spki { continue; }
				//Might match.
				_might_use_spki_only = true;
			}
			_might_use_spki_only
		};
		let mut ip_address = None;
		if let Some(ip_str) = host.strip_prefix("[").and_then(|host|host.strip_suffix("]")) {
			ip_address = IpAddress::from_str(ip_str).ok();
		}
		let mut client_random = [0; 32];
		secure_random(&mut client_random);
		let client_caps = ClientSupport::new(config);
		HandshakeController::handle_deadline(config, callbacks.deref_mut());
		HandshakeController {
			callbacks: callbacks,
			config: config.clone(),
			hs_state: HandshakeState2::ClientHello(ClientHelloFields{
				cookie: None,
				hs_hash: EarlyHandshakeRep::new(),
				tls13_shares: Self::init_shares(config, client_caps.get_enabled_kem()),
				ciphersuite_assigned: None,
				features: CFLAG_UNKNOWN,	//Ignore unknown features.
				group_id: None,
				client_random: client_random,
				cvinfo: TlsCVInfo {
					requested_sni: match ip_address {
						None => ReferenceNameOwned::DnsName(host.to_owned()),
						Some(x) => ReferenceNameOwned::Ip(x),
					},
					pins: pins.to_owned(),
					flags: Flags::none,
				},
				version_assigned: None,
				crypto_algs: CryptoTracker::new(),
				acme_hash: None,
				client_caps: client_caps,
				flags: Flags::MIGHT_USE_SPKI_ONLY.and(_might_use_spki_only),
			}),
			ssl_key_log: false,
			debug: debug.clone(),
		}
	}
	pub fn new_acme_check(config: &ClientConfiguration, debug: DebugSetting,
		mut callbacks: Box<dyn TlsCallbacks+Send+'static>, host: &str, kahash: [u8;32]) ->
		HandshakeController
	{
		let _might_use_spki_only = false;
		let mut ip_address = None;
		if let Some(ip_str) = host.strip_prefix("[").and_then(|host|host.strip_suffix("]")) {
			ip_address = IpAddress::from_str(ip_str).ok();
		}
		let mut client_random = [0; 32];
		secure_random(&mut client_random);
		let client_caps = ClientSupport::new(config);
		HandshakeController::handle_deadline(config, callbacks.deref_mut());
		HandshakeController {
			callbacks: callbacks,
			config: config.clone(),
			hs_state: HandshakeState2::ClientHello(ClientHelloFields{
				cookie: None,
				hs_hash: EarlyHandshakeRep::new(),
				tls13_shares: Self::init_shares(config, client_caps.get_enabled_kem()),
				ciphersuite_assigned: None,
				features: CFLAG_UNKNOWN,	//Ignore unknown features.
				group_id: None,
				client_random: client_random,
				cvinfo: TlsCVInfo {
					//TODO: requested_sni in IP address case does not work. However, this is
					//unclear on the spec.
					requested_sni: match ip_address {
						None => ReferenceNameOwned::DnsName(host.to_owned()),
						Some(x) => ReferenceNameOwned::Ip(x),
					},
					pins: Vec::new(),
					flags: Flags::none,
				},
				version_assigned: None,
				crypto_algs: CryptoTracker::new(),
				acme_hash: Some(kahash),
				client_caps: client_caps,
				flags: Flags::NO_DATA_TRANSFER,
			}),
			ssl_key_log: false,
			debug: debug.clone(),
		}
	}
	//Handle the initial deadline.
	fn handle_deadline(config: &ClientConfiguration, callbacks: &mut (dyn TlsCallbacks+Send+'static))
	{
		if let Some(timelimit) = config.handshake_time_limit {
			callbacks.set_deadline(Some(PointInTime::from_now(TimeUnit::Duration(timelimit))));
		} else {
			callbacks.set_deadline(None);
		}
	}
	pub fn hint_group(&mut self, grpid: u16) { self._hint_group(grpid).ok(); }
	fn _hint_group(&mut self, grpid: u16) -> Result<(), ()>
	{
		if let &mut HandshakeState2::ClientHello(ref mut x) = &mut self.hs_state {
			//Find place for the key.
			let enabled_kem = x.client_caps.get_enabled_kem();
			let mut ordinal = 0;
			let mut xkem = None;
			for kem in enabled_kem.iter() {
				let tlsid =  if ordinal >= x.tls13_shares.len() {
					None
				} else {
					Some(x.tls13_shares[ordinal].1.kem().tls_id())
				};
				if tlsid == Some(grpid) {
					return Ok(());		//Already there.
				} else if tlsid == Some(kem.tls_id()) {
					ordinal += 1;		//Entry in use.
				} else if kem.tls_id() == grpid {
					xkem = Some(kem);
					break;	//Insert here.
				}
			}
			let xkem = dtry!(xkem);

			//Generate a key.
			let lifetime = TemporaryRandomLifetimeTag;
			let mut rng = TemporaryRandomStream::new_system(&lifetime);
			let key = dtry!(xkem.generate_key(&mut rng));
			x.tls13_shares.insert(ordinal, KemKeyPair::pair(key));
		};
		Ok(())
	}
	pub fn enable_ssl_key_log(&mut self) { self.ssl_key_log = true; }
}
