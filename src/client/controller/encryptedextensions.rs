use super::BaseMessageParams;
use super::Flags;
use super::HandshakeState2;
use super::ReferenceNameOwned;
use super::Tls13HsSecretsPre;
use super::TlsClientCert;
use super::TlsCVInfo;
use crate::callbacks::GotUnknownExtensionParameters;
use crate::callbacks::MSG_ENCRYPTEDEXTENSIONS;
use crate::callbacks::TargetInfoParameters;
use crate::client::controller::certificate::Tls13CertificateFields;
use crate::common::CryptoTracker;
use crate::common::is_unknown_extension;
use crate::common::test_failure;
use crate::errors::Error;
use crate::features::FLAGS5_TEST_EE_RECV_FAIL;
use crate::messages::HandshakeHash;
use crate::messages::HashMessages;
use crate::record::UpcallReturn;
use crate::stdlib::Deref;
use btls_aux_set2::SetOf;
use btls_aux_tls_struct::EncryptedExtensions as PEncryptedExtensions;
use core::fmt::Arguments;


pub(crate) struct Tls13EncryptedExtensionsFields
{
	pub cvinfo: TlsCVInfo,				//Server certificate validation info.
	pub features: u32,				//Features supported.
	pub hs_secrets: Tls13HsSecretsPre,
	pub hs_hash: HandshakeHash,
	pub crypto_algs: CryptoTracker,
	pub acme_hash: Option<[u8;32]>,
	pub(crate) client_caps: crate::messages::ClientSupport,
	pub client_random: [u8;32],
	pub flags: Flags,
}

impl Tls13EncryptedExtensionsFields
{
	//Handle received EncryptedExtensions message.
	pub fn rx_encrypted_extensions(self, controller: &mut UpcallReturn, msg: &[u8],
		bp: BaseMessageParams) -> Result<HandshakeState2, Error>
	{
		let BaseMessageParams{rawmsg, debug, config, callbacks, ..} = bp;
		test_failure(&config.flags, FLAGS5_TEST_EE_RECV_FAIL)?;
		//Receiving the EncryptedExtensions checked MAC, so any possible attacker is restricted to one
		//query. We can clear the unencrypted alerts flag now.
		controller.set_force_unencrypted_alerts(false);
		let mut debug_cb = |args:Arguments|debug!(TLS_EXTENSIONS debug, "Encrypted Extensions: {args}");
		let enable = |f:btls_aux_tls_struct::RemoteFeature|self.client_caps.enable(config, f);
		let pmsg = PEncryptedExtensions::parse(msg, &enable, &mut Some(&mut debug_cb)).
			map_err(|e|error!(msgerr_encrypted_extensions e))?;
		//ALPN: Record it. And present or not, it is known.
		if let (None, Some(alpn)) = (self.acme_hash, pmsg.get_alpn_protocol_id()) {
			debug!(HANDSHAKE_EVENTS debug, "Encrypted extensions: Application Layer Protocol is '{alpn}'");
		}
		//If checking ACME, the ALPN must be present and be "acme-tls/1".
		let acme_proto = btls_aux_tls_iana::AlpnProtocolId::ACME_TLS;
		fail_if!(self.acme_hash.is_some() && pmsg.get_alpn_protocol_id() != Some(acme_proto),
			error!(bogus_acme_alpn));

		if let Some(limit) = pmsg.get_record_size_limit() {
			debug!(HANDSHAKE_EVENTS debug,
				"Encrypted extensions: Maximum fragment size supported by peer is {limit}");
			//Subtract 1 byte for TLS 1.3.
			controller.set_max_fragment_length(limit - 1);
		}

		let mut hs_hash = self.hs_hash;
		hs_hash.add(rawmsg, debug)?;

		//Target info.
		if !self.flags.is_NO_DATA_TRANSFER() {
			let sni = if self.flags.is_NO_REQUEST_SNI() {
				None
			} else if let &ReferenceNameOwned::DnsName(ref sni) = &self.cvinfo.requested_sni {
				Some(sni.deref())
			} else {
				None
			};
			//The ALPNs are always valid UTF-8.
			let alpn = pmsg.get_alpn_protocol_id().and_then(|x|core::str::from_utf8(x.get()).ok());
			callbacks.target_info(TargetInfoParameters {
				sni: sni,
				alpn: alpn,
			});

			//Do callbacks on unknown extensions.
			for extension in pmsg.get_extensions() {
				let id = extension.get_type().get();
				if is_unknown_extension(id) {
					callbacks.got_unknown_extension(GotUnknownExtensionParameters {
						id: id,
						payload: extension.get_raw_data(),
					});
				}
			}
			callbacks.end_unknown_extension(MSG_ENCRYPTEDEXTENSIONS);
		}
		let rpk = btls_aux_tls_iana::CertificateType::RAW_PUBLIC_KEY;
		let use_client_rpk = pmsg.get_client_certificate_type() == rpk;
		let use_server_rpk = pmsg.get_server_certificate_type() == rpk;

		Ok(HandshakeState2::Tls13Certificate(false, Tls13CertificateFields{
			features: self.features,
			hs_secrets: self.hs_secrets,
			hs_hash: hs_hash,
			cvinfo: self.cvinfo,
			cc: TlsClientCert {
				client_cert: None,
				peer_sigalgo_mask: SetOf::empty_set(),
			},
			crypto_algs: self.crypto_algs,
			acme_hash: self.acme_hash,
			client_caps: self.client_caps,
			client_random: self.client_random,
			//Handle possible definite SCT in EncryptedExtensions. Note that eemsg.use_server_rpk
			//should never be set without definite_sct.
			flags: self.flags.fforce_USE_CLIENT_RPK(use_client_rpk).
				fforce_USE_SPKI_ONLY(use_server_rpk),
		}))
	}
}
