use crate::ServerConfiguration;
use crate::ServerConnection;
use crate::ClientConfiguration;
use crate::ClientConnection;
use crate::Connection;
use crate::Error;
use crate::callbacks::RequestCertificateParameters;
use crate::callbacks::GotUnknownExtensionParameters;
use crate::callbacks::KeyExportType;
use crate::callbacks::TargetInfoParameters;
use crate::callbacks::TlsCallbacks;
use crate::callbacks::ForceRestartParameters;
use crate::certificates::HostSpecificPin;
use crate::certificates::LocalServerCertificateStore;
use crate::certificates::sign_certificate;
use crate::features::FLAGS0_DEBUG_NO_SHARES;
use crate::stdlib::Arc;
use crate::stdlib::Box;
use crate::stdlib::Deref;
use crate::stdlib::DerefMut;
use crate::stdlib::Duration;
use crate::stdlib::FmtWrite;
use crate::stdlib::from_utf8;
use crate::stdlib::IoError;
use crate::stdlib::IoErrorKind;
use crate::stdlib::IoRead;
use crate::stdlib::IoWrite;
use crate::stdlib::min;
use crate::stdlib::Mutex;
use crate::stdlib::String;
use crate::stdlib::ToOwned;
use crate::stdlib::Vec;
use crate::transport::PullTcpBuffer;
use crate::transport::PushTcpBuffer;
use crate::utils::Queue;
use btls_aux_collections::ToString;
use btls_aux_keypair_local::LocalKeyPair;
use btls_aux_aead::ProtectorType;
use btls_aux_futures::create_future;
use btls_aux_futures::FutureReceiver;
use btls_aux_futures::FutureSender;
use btls_aux_hash::Hash;
use btls_aux_hash::HashFunction2;
use btls_aux_hash::Sha256;
use btls_aux_rope3::rope3;
use btls_aux_rope3::FragmentWriteOps as FragmentWriteOps3;
use btls_aux_random::TemporaryRandomLifetimeTag;
use btls_aux_random::TemporaryRandomStream;
use btls_aux_signatures::Hasher;
use btls_aux_signatures::KeyPair2;
use btls_aux_signatures::SignatureAlgorithm2;
use btls_aux_signatures::SignatureAlgorithmTls2;
use btls_aux_signatures::SignatureFormat;
use btls_aux_time::PointInTime;


struct SlowKeyTrigger
{
	data: Arc<Mutex<Option<(Vec<u8>, SignatureAlgorithm2,
		FutureSender<Result<(SignatureFormat, Vec<u8>), ()>>)>>>,
	chain: Arc<LocalKeyPair>,
}

impl SlowKeyTrigger
{
	fn trigger(&self)
	{
		let mut xdata = self.data.lock();
		let xdata = xdata.deref_mut();
		let xdata = xdata.take();
		let xdata = xdata.unwrap();
		let lifetime = TemporaryRandomLifetimeTag;
		let result = self.chain.sign_data(&xdata.0, xdata.1,
			&mut TemporaryRandomStream::new_system(&lifetime));
		//This should be immediately readable.
		let result = result.read().ok().unwrap();
		(xdata.2).settle(result);
	}
}

struct SlowKey
{
	data: Arc<Mutex<Option<(Vec<u8>, SignatureAlgorithm2,
		FutureSender<Result<(SignatureFormat, Vec<u8>), ()>>)>>>,
	chain: Arc<LocalKeyPair>,
}

impl SlowKey
{
	fn new(kp: LocalKeyPair) -> (SlowKey, SlowKeyTrigger)
	{
		let x = Arc::new(Mutex::new(None));
		let y = Arc::new(kp);
		(SlowKey {
			data: x.clone(),
			chain: y.clone(),
		}, SlowKeyTrigger {
			data: x.clone(),
			chain: y.clone(),
		})
	}
}

impl KeyPair2 for SlowKey
{
	fn sign_data(&self, data: &[u8], algorithm: SignatureAlgorithm2, _rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<(SignatureFormat, Vec<u8>), ()>>
	{
		let (sender, receiver) = create_future();
		let mut xdata = self.data.lock();
		let xdata = xdata.deref_mut();
		*xdata = Some((data.to_owned(), algorithm, sender));
		receiver
	}
	fn sign_callback(&self, algorithm: SignatureAlgorithm2, rng: &mut TemporaryRandomStream,
		msg: &mut dyn FnMut(&mut Hasher)) -> FutureReceiver<Result<(SignatureFormat, Vec<u8>), ()>>
	{
		let msg = Hasher::collect(msg);
		self.sign_data(&msg, algorithm, rng)
	}
	fn get_signature_algorithms<'a>(&'a self) -> &'a [SignatureAlgorithm2]
	{
		self.chain.get_signature_algorithms()
	}
	fn get_public_key2<'a>(&'a self) -> &'a [u8] { self.chain.get_public_key2() }
	fn get_key_type2<'a>(&'a self) -> &'a str { self.chain.get_key_type2() }
}

struct FastKey
{
	chain: Arc<LocalKeyPair>,
}

impl FastKey
{
	fn new(kp: LocalKeyPair) -> FastKey
	{
		FastKey {chain: Arc::new(kp)}
	}
}

impl KeyPair2 for FastKey
{
	fn sign_data(&self, data: &[u8], algorithm: SignatureAlgorithm2, _rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<(SignatureFormat, Vec<u8>), ()>>
	{
		let (sender, receiver) = create_future();
		let lifetime = TemporaryRandomLifetimeTag;
		let result = self.chain.sign_data(data, algorithm,
			&mut TemporaryRandomStream::new_system(&lifetime));
		//This should be immediately readable.
		let result = result.read().ok().unwrap();
		sender.settle(result);
		receiver
	}
	fn sign_callback(&self, algorithm: SignatureAlgorithm2, rng: &mut TemporaryRandomStream,
		msg: &mut dyn FnMut(&mut Hasher)) -> FutureReceiver<Result<(SignatureFormat, Vec<u8>), ()>>
	{
		let msg = Hasher::collect(msg);
		self.sign_data(&msg, algorithm, rng)
	}
	fn get_signature_algorithms<'a>(&'a self) -> &'a [SignatureAlgorithm2]
	{
		self.chain.get_signature_algorithms()
	}
	fn get_public_key2<'a>(&'a self) -> &'a [u8] { self.chain.get_public_key2() }
	fn get_key_type2<'a>(&'a self) -> &'a str { self.chain.get_key_type2() }
}

struct EpCallbacks
{
	test_extensions: bool,
	test_extensions2: bool,
	ext_event_log: String,
	req_cert: bool,
	expect_alpn: Option<String>,
	alpn_checked: bool,
	timeout_active: bool,
	rkeys: Vec<Vec<u8>>,
	skeys: Vec<Vec<u8>>,
	client_random: Option<[u8;32]>,
}

impl EpCallbacks
{
	fn new() -> EpCallbacks
	{
		EpCallbacks {
			test_extensions: false,
			test_extensions2: false,
			ext_event_log: String::new(),
			req_cert: false,
			expect_alpn: None,
			alpn_checked: false,
			timeout_active: false,
			rkeys: Vec::new(),
			skeys: Vec::new(),
			client_random: None,
		}
	}
}

fn maybe_print_error(c: &impl Connection)
{
	if let Some(reason) = c.get_error() {
		println!("Error: {reason}");
	}
}

fn _do_pull_tcp_data(selfc: &mut impl Connection, input: PullTcpBuffer) -> Result<Vec<u8>, Error>
{
	let r = selfc.pull_tcp_data(input)?;
	maybe_print_error(selfc);
	Ok(r)
}

fn _do_push_tcp_data(selfc: &mut impl Connection, data: PushTcpBuffer) -> Result<(Vec<u8>, bool), Error>
{
	let r = selfc.push_tcp_data(data)?;
	maybe_print_error(selfc);
	Ok(r)
}

fn _do_read_file_to_queue<R:IoRead>(selfc: &mut impl Connection, input: &mut R, queue: &mut Queue) ->
	Result<(), String>
{
	let mut ibuf = vec![0;16645];
	let buf1 = &mut ibuf[..];
	let amount = input.read(buf1).map_err(|err|format!("I/O error: {err}"))?;
	fail_if!(amount == 0, "Unexpected EOF on underlying stream");
	let (tmp, eofd) = _do_push_tcp_data(selfc, PushTcpBuffer::u8_size(&buf1, amount)).
		map_err(|err|format!("TLS error: {err}"))?;
	queue.write(&tmp).ok();
	if eofd { queue.send_eof(); }
	Ok(())
}

fn _do_write_buffer_to_queue(selfc: &mut impl Connection, input: PullTcpBuffer, queue: &mut Queue) ->
	Result<(), String>
{
	let data = _do_pull_tcp_data(selfc, input).map_err(|err|format!("TLS error: {err}"))?;
	maybe_print_error(selfc);
	queue.write(&data).map_err(|err|format!("I/O error: {err}"))?;
	Ok(())
}

impl TlsCallbacks for EpCallbacks
{
	fn got_unknown_extension(&mut self, info: GotUnknownExtensionParameters) -> bool
	{
		if !self.test_extensions { return false; }
		let iid = info.id;
		let ipl = from_utf8(info.payload).unwrap();
		writeln!(self.ext_event_log, "Got extension {iid}: {ipl}").ok();
		//Try to reply to all extensions except 123.
		if info.id != 123 { true } else { false }
	}
	fn end_unknown_extension(&mut self, msgtype: u8)
	{
		if !self.test_extensions { return; }
		writeln!(self.ext_event_log, "End of extensions msgid={msgtype}").ok();
	}
	fn respond_unknown_extension(&mut self, id: u16) -> Option<Vec<u8>>
	{
		if !self.test_extensions { return None; }
		writeln!(self.ext_event_log, "Responding to extension {id}").ok();
		return Some(format!("Answer {id}").as_bytes().to_owned());
	}
	fn offer_unknown_extension(&mut self) -> Vec<(u16, Vec<u8>)>
	{
		if !self.test_extensions { return Vec::new(); }
		writeln!(self.ext_event_log, "Offering extensions...").ok();
		let mut exts = Vec::new();
		//Try sending extensions 3, 47, 123, 456 and 789. Do not use 48, as it is not valid in client
		//hello. And be careful what one puts to extensions 3 and 47, as those are valid and do get
		//parsed. And test_extensions2 check is because these extensions are not allowed in cr.
		if self.test_extensions2 {
			exts.push((3, b"\x00\x15\x01ABCDEFGHIJKLMNOPQRST".to_vec()));
			exts.push((47, b"\x00\x16\x00\x14ABCDEFGHIJKLMNOPQRST".to_vec()));
		}
		exts.push((123, b"test extension 123".to_vec()));
		exts.push((456, b"test extension 456".to_vec()));
		exts.push((789, b"test extension 789".to_vec()));
		exts
	}
	fn request_certificate(&mut self, _params: RequestCertificateParameters) -> bool
	{
		self.req_cert
	}
	fn target_info(&mut self, params: TargetInfoParameters)
	{
		if let Some(ref x) = self.expect_alpn.as_ref() {
			assert_eq!(Some(&x[..]), params.alpn);
		}
		self.alpn_checked = true;
	}
	fn set_deadline(&mut self, deadline: Option<PointInTime>)
	{
		self.timeout_active = deadline.is_some();
	}
	fn force_restart(&mut self, _parameters: ForceRestartParameters) -> bool
	{
		true
	}
	fn rawhs_encryption_key2(&mut self, _prf: HashFunction2, _prot: ProtectorType, key: &[u8])
	{
		self.skeys.push(key.to_owned());
	}
	fn rawhs_decryption_key2(&mut self, _prf: HashFunction2, _prot: ProtectorType, key: &[u8])
	{
		self.rkeys.push(key.to_owned());
	}
	fn ssl_key_log(&mut self, ktype: KeyExportType, random: [u8;32], key: &[u8])
	{
		writeln!(self.ext_event_log, "ssl_key_log: {ktype:?} {random:?} {key:?}").ok();
	}
	fn client_random(&mut self, random: [u8;32])
	{
		self.client_random = Some(random);
	}
}

fn test_certificate_kp() -> (Vec<u8>, LocalKeyPair, Vec<u8>)
{
	//The example key from RFC8037.
	let privkey = r#"{
		"kty":"OKP",
		"crv":"Ed25519",
		"d":"nWGxne_9WmC6hEr0kuwsxERJxWl7MmkZcDusAxyuf2A",
		"x":"11qYAYKxCrfVS_7TyWQHOg7hcvPapiMlrwIaaPcHURo"
	}"#;
	let algo = SignatureAlgorithmTls2::by_tls_id(0x0807).unwrap().to_generic();	//Ed25519.
	let mut privkey = privkey.as_bytes();
	let privkey = LocalKeyPair::new(&mut privkey, "test.key").unwrap();
	let pubkey = privkey.get_public_key2().to_owned();

	//Create a self-signed certificate using the private key.
	let tbs = rope3!(SEQUENCE(
		[0](INTEGER 2u8),
		INTEGER 1u8,
		SEQUENCE(
			OBJECT_IDENTIFIER ED25519_KEY_OID
		),
		SEQUENCE(
			SET(SEQUENCE(
				OBJECT_IDENTIFIER X509_SUBJECT_SERIAL_NUMBER,
				PRINTABLE_STRING "I"
			))
		),
		SEQUENCE(
			UTC_TIME "500101000000Z",
			UTC_TIME "491231235959Z"
		),
		SEQUENCE(
			SET(SEQUENCE(
				OBJECT_IDENTIFIER X509_SUBJECT_SERIAL_NUMBER,
				PRINTABLE_STRING "I"
			))
		),
		{pubkey.deref()},
		[3](SEQUENCE(
			SEQUENCE(
				OBJECT_IDENTIFIER X509_EXT_BASIC_CONSTRAINTS,
				BOOLEAN_TRUE,
				OCTET_STRING SEQUENCE()
			),
			SEQUENCE(
				OBJECT_IDENTIFIER X509_EXT_SAN,
				OCTET_STRING SEQUENCE(
					[2] "foo"
				)
			)
		))
	));
	let tbs = FragmentWriteOps3::to_vector(&tbs);
	let cert = sign_certificate(&tbs, &privkey, algo).expect("Failed to sign test certificate");
	(cert, privkey, pubkey)
}

fn test_certificate_store() -> (Vec<u8>, LocalServerCertificateStore)
{
	let (cert, privkey, pubkey) = test_certificate_kp();
	let mut cert = cert.deref();
	let mut certs = LocalServerCertificateStore::new();
	certs.add_privkey(privkey, "test.key");
	certs.add(&mut cert, "test.crt").unwrap();
	(pubkey, certs)
}


fn transfer_packets_hs3<F:Connection,T:Connection>(from: &mut F, to: &mut T)
{
	while from.get_status().is_want_tx() {
		let buf = _do_pull_tcp_data(from, PullTcpBuffer::no_data()).unwrap();
println!("Transfer buffer size={buflen}", buflen=buf.len());
		assert!(to.get_status().is_want_rx());
		receive_tcp_push(to, &buf, buf.len(), false, &[]);
	}
}

fn make_server_ctx<F,G>(mut callbackfn: G, mut configfn: F) -> (ServerConnection, Arc<Mutex<EpCallbacks>>)
	where F: FnMut(&mut ServerConfiguration), G: FnMut(&mut EpCallbacks)
{
	let (_, certs) = test_certificate_store();
	let mut server_callbacks = EpCallbacks::new();
	callbackfn(&mut server_callbacks);
	let server_callbacks = Arc::new(Mutex::new(server_callbacks));
	let mut serverconfig = ServerConfiguration::from(certs.lookup());
	configfn(&mut serverconfig);
	let server = serverconfig.make_connection(Box::new(server_callbacks.clone()));
	(server, server_callbacks)
}

fn make_server_ctx_withcfg<G>(serverconfig: &ServerConfiguration, mut callbackfn: G) ->
	(ServerConnection, Arc<Mutex<EpCallbacks>>) where G: FnMut(&mut EpCallbacks)
{
	let mut server_callbacks = EpCallbacks::new();
	callbackfn(&mut server_callbacks);
	let server_callbacks = Arc::new(Mutex::new(server_callbacks));
	let server = serverconfig.make_connection(Box::new(server_callbacks.clone()));
	(server, server_callbacks)
}

fn make_client_ctx_body<F,G>(mut callbackfn: G, mut configfn: F) ->
	(ClientConfiguration, HostSpecificPin, Arc<Mutex<EpCallbacks>>)
	where F: FnMut(&mut ClientConfiguration), G: FnMut(&mut EpCallbacks)
{
	let (pubkey, _) = test_certificate_store();

	let mut tlsa_record = vec![3,1,0];
	tlsa_record.extend_from_slice(&pubkey);
	let trustanchor = HostSpecificPin::from_tlsa_raw(&tlsa_record).unwrap();
	let mut client_callbacks = EpCallbacks::new();
	callbackfn(&mut client_callbacks);
	let client_callbacks = Arc::new(Mutex::new(client_callbacks));
	let mut clientconfig = ClientConfiguration::new();
	configfn(&mut clientconfig);
	(clientconfig, trustanchor, client_callbacks)
}

fn make_client_ctx<F,G>(callbackfn: G, configfn: F) -> (ClientConnection, Arc<Mutex<EpCallbacks>>)
	where F: FnMut(&mut ClientConfiguration), G: FnMut(&mut EpCallbacks)
{
	let (clientconfig, trustanchor, ccb) = make_client_ctx_body(callbackfn, configfn);
	let trustanchors = [trustanchor];
	let client = clientconfig.make_connection_pinned("foo", &trustanchors, Box::new(ccb.clone()));
	(client, ccb)
}

fn make_acme_client_ctx(acmehash: [u8;32]) -> (ClientConnection, Arc<Mutex<EpCallbacks>>)
{
	let client_callbacks = Arc::new(Mutex::new(EpCallbacks::new()));
	let clientconfig = ClientConfiguration::new();
	let client = clientconfig.make_connection_acme_check("foo", Box::new(client_callbacks.clone()), acmehash);
	(client, client_callbacks)
}

fn make_client_ctx_exphost<F,G>(hostname: &str, callbackfn: G, configfn: F) -> ClientConnection
	where F: FnMut(&mut ClientConfiguration), G: FnMut(&mut EpCallbacks)
{
	let (clientconfig, trustanchor, ccb_send) = make_client_ctx_body(callbackfn, configfn);
	let trustanchors = [trustanchor];
	let client = clientconfig.make_connection_pinned(hostname, &trustanchors, Box::new(ccb_send));
	client
}

//const F_NONE: u8 = 0;
const F_SR: u16 = 1;
const F_SW: u16 = 2;
const F_SRW: u16 = 3;
const F_CR: u16 = 4;
const F_CW: u16 = 8;
const F_CRW: u16 = 12;
const F_ALL: u16 = 15;
//Rendezvous in client to server direction.
const F_CTS: u16 = 16;
//Rendezvous in server to client direction.
const F_STC: u16 = 32;
//The client wants to read, but the server does not want to send.
const F_STC_BLOCKED: u16 = 64;
//The server wants to read, but the client does not want to send.
const F_CTS_BLOCKED: u16 = 128;
//The server should be internal with handshake complete.
const F_SERV_INTERNAL: u16 = 256;

fn assert_raw_handshake_done<T:Connection>(endpoint: &T)
{
	let xflags = endpoint.get_status();
	assert!(xflags.is_want_rx());		//Post-handshake messages.
	assert!(!xflags.is_want_tx());
	assert!(!xflags.is_blocked());
	assert!(!xflags.is_may_rx());		//Raw.
	assert!(!xflags.is_may_tx());		//Raw.
	assert!(!xflags.is_rx_end());		//Raw.
	assert!(!xflags.is_tx_end());		//Raw.
	assert!(!xflags.is_handshaking());
	assert!(!xflags.is_dead());
	assert!(!xflags.is_aborted());
}

fn assert_data2<F:Connection,T:Connection>(client: &F, server: &T, flags: u16)
{
	let cflags = client.get_status();
	let sflags = server.get_status();
	assert!((cflags.is_may_rx() || cflags.is_rx_end()) == (flags & F_CR == F_CR));
	assert!((cflags.is_may_tx() || cflags.is_tx_end()) == (flags & F_CW == F_CW));
	assert!((!cflags.is_handshaking()) == (flags & F_CRW == F_CRW));
	assert!(cflags.is_want_tx() == (flags & F_CTS == F_CTS));
	assert!(cflags.is_want_rx() == ((flags & F_STC == F_STC) || (flags & F_STC_BLOCKED == F_STC_BLOCKED) ||
		(flags & F_CR == F_CR)));

	assert!((sflags.is_may_rx() || sflags.is_rx_end()) == (flags & F_SR == F_SR));
	assert!((sflags.is_may_tx() || sflags.is_tx_end()) == (flags & F_SW == F_SW));
	if flags & F_SERV_INTERNAL == F_SERV_INTERNAL {
		assert!(sflags.is_handshaking());
	} else {
		assert!((!sflags.is_handshaking()) == (flags & F_SRW == F_SRW));
	}
	assert!(sflags.is_want_tx() == (flags & F_STC == F_STC));
	if flags & F_SERV_INTERNAL == F_SERV_INTERNAL {
		assert!(sflags.is_want_rx());
	} else {
		assert!(sflags.is_want_rx() == ((flags & F_CTS == F_CTS) ||
			(flags & F_CTS_BLOCKED == F_CTS_BLOCKED) || (flags & F_SR == F_SR)));
	}
}

const C_NONE: u8 = 0;
const C_TX: u8 = 1;
const C_RX: u8 = 2;
const C_LINK: u8 = 4;
const C_ABORT: u8 = 8;
const C_NOABORT: u8 = 16;

fn assert_close2<T:Connection>(endpoint: &T, flags: u8)
{
	let xflags = endpoint.get_status();
	let rxc = flags & C_RX != 0;
	let txc = flags & C_TX != 0;
	let linkc = (flags & (C_ABORT|C_LINK) != 0) || (flags & (C_RX | C_TX) == (C_RX | C_TX));
	assert!(xflags.is_rx_end() == rxc);
	assert!(xflags.is_tx_end() == txc);
	assert!(xflags.is_dead() == linkc);
	if flags & C_ABORT != 0 { assert!(xflags.is_aborted()); }
	if flags & C_NOABORT != 0 { assert!(!xflags.is_aborted()); }
}

macro_rules! def_handshake_connection
{
	($name:ident $tfn:ident) => {
		fn $name() -> (ClientConnection, Arc<Mutex<EpCallbacks>>, ServerConnection, Arc<Mutex<EpCallbacks>>)
		{
			let (mut server, scb_recv) = make_server_ctx(|_|{
			}, |_|{
			});
		
			let (mut client, ccb_recv) = make_client_ctx(|_|{
			}, |_|{
			});

			//The initial interest should be transfer from client to server...
			assert_data2(&client, &server, F_CTS);
			$tfn(&mut client, &mut server);
			//Now the interest should have reversed... And the server should have run to point where
			//TX is ready.
			assert_data2(&client, &server, F_SW | F_STC);
			$tfn(&mut server, &mut client);
			//Now interests should have reversed again... And server to client is now up. This should
			//also perma-set the client wants read flag. Also, client has run the handshake.
			assert_data2(&client, &server, F_SW | F_CRW | F_CTS);
			$tfn(&mut client, &mut server);
			//Now neither should want to write, and connections are fully up. Both wants read flags
			//should now be perma-set.
			assert_data2(&client, &server, F_ALL);
			//Writeable flags should not be set.
			assert!(!client.get_status().is_want_tx());
			assert!(!server.get_status().is_want_tx());
			//The readable flags are now permanently stuck active.
			assert!(client.get_status().is_want_rx());
			assert!(server.get_status().is_want_rx());
			assert_data2(&client, &server, F_ALL);

			(client, ccb_recv, server, scb_recv)
		}
	}
}

def_handshake_connection!(handshake_connection3 transfer_packets_hs3);


fn do_normal_close3(mut client: ClientConnection, mut server: ServerConnection)
{
	//Neither direction should be closed.
	assert_close2(&client, C_NONE|C_NOABORT);
	assert_close2(&server, C_NONE|C_NOABORT);
	//Do crossed close.
	let buffer1 = _do_pull_tcp_data(&mut client, PullTcpBuffer::no_data().eof()).unwrap();
	let buffer2 = _do_pull_tcp_data(&mut server, PullTcpBuffer::no_data().eof()).unwrap();
	//Receive the EOFs. Before rx should not be closed, after all should be closed.
	assert_close2(&client, C_TX|C_NOABORT);
	assert_close2(&server, C_TX|C_NOABORT);
	receive_tcp_push(&mut server, &buffer1, buffer1.len(), true, &[]);
	receive_tcp_push(&mut client, &buffer2, buffer2.len(), true, &[]);
	assert_close2(&client, C_TX|C_RX|C_NOABORT);
	assert_close2(&server, C_TX|C_RX|C_NOABORT);
}

fn receive_tcp_push<C:Connection>(conn: &mut C, buffer: &[u8], amount: usize, assert_eof: bool, assert_content: &[u8])
{
	let (tmp, eofd) = _do_push_tcp_data(conn, PushTcpBuffer::u8_size(buffer, amount)).unwrap();
	assert!(tmp.len() == assert_content.len());
	assert_eq!(&tmp[..], assert_content);
	assert_eq!(eofd, assert_eof);
}

fn assert_queue(q: &mut Queue, assert_eof: bool, assert_data: &[u8])
{
	let mut v = vec![0;assert_data.len() + 5];
	assert_eq!(q.eof_sent(), assert_eof);
	if assert_data.len() == 0 {
		assert!(q.buffered() == 0);
	} else {
		assert_eq!(q.read(&mut v[..]), Some(assert_data.len()));
		assert_eq!(&v[..assert_data.len()], assert_data);
	}
}


struct PipeSender(Arc<Mutex<(Vec<u8>, bool)>>);
struct PipeReceiver(Arc<Mutex<(Vec<u8>, bool)>>);

fn make_pipe() -> (PipeSender, PipeReceiver)
{
	let v = (Vec::new(), false);
	let v = Arc::new(Mutex::new(v));
	(PipeSender(v.clone()), PipeReceiver(v.clone()))
}

impl IoWrite for PipeSender
{
	fn flush(&mut self) -> Result<(), IoError> { Ok(()) }
	fn write(&mut self, data: &[u8]) -> Result<usize, IoError>
	{
		let mut v = self.0.lock();
		(v.0).extend_from_slice(data);
		Ok(data.len())
	}
}

impl IoRead for PipeReceiver
{
	fn read(&mut self, data: &mut [u8]) -> Result<usize, IoError>
	{
		let mut v = self.0.lock();
		let dlen = data.len();
		let vlen = (v.0).len();
		if dlen == 0 && v.1 {
			return Ok(0);	//EOF.
		}
		let amt = min(dlen, (v.0).len());
		if amt == 0 {
			return Err(IoError::new(IoErrorKind::WouldBlock, "Operation would block"));
		}
		(&mut data[..amt]).copy_from_slice(&(v.0)[..amt]);
		for i in 0..vlen - amt {
			(v.0)[i] = (v.0)[i+amt];
		}
		(v.0).resize(vlen - amt, 0);
		Ok(amt)
	}
}

impl PipeReceiver
{
	fn has_data(&self) -> bool
	{
		let v = self.0.lock();
		(v.0).len() > 0 || v.1
	}
}

static CLIENT_HELLO: &'static [u8] = b"Hello from Client";
static SERVER_HELLO: &'static [u8] = b"Hello from Server";

#[test]
fn read_tcp_push_pull_basic()
{
	//Handshake the connection.
	let (mut client, _, mut server, _) = handshake_connection3();
	//Do crossed write. The EOF flag should not be set and messages should be as expected.
	let buffer1 = _do_pull_tcp_data(&mut client, PullTcpBuffer::write(CLIENT_HELLO)).unwrap();
	let buffer2 = _do_pull_tcp_data(&mut server, PullTcpBuffer::write(SERVER_HELLO)).unwrap();
	receive_tcp_push(&mut server, &buffer1, buffer1.len(), false, CLIENT_HELLO);
	receive_tcp_push(&mut client, &buffer2, buffer2.len(), false, SERVER_HELLO);
	//Close the connection.
	do_normal_close3(client, server);
}

#[test]
fn read_tcp_push_pull_with_close()
{
	//Handshake the connection.
	let (mut client, _, mut server, _) = handshake_connection3();
	//Do crossed write, including EOF messages. The result should be the message, EOF set, and all data should
	//be accepted.
	let buffer1 = _do_pull_tcp_data(&mut client, PullTcpBuffer::write(CLIENT_HELLO).eof()).unwrap();
	let buffer2 = _do_pull_tcp_data(&mut server, PullTcpBuffer::write(SERVER_HELLO).eof()).unwrap();
	receive_tcp_push(&mut server, &buffer1, buffer1.len(), true, CLIENT_HELLO);
	receive_tcp_push(&mut client, &buffer2, buffer2.len(), true, SERVER_HELLO);
	//The connection should be closed now.
	assert_close2(&client, C_TX|C_RX|C_NOABORT);
	assert_close2(&server, C_TX|C_RX|C_NOABORT);
}

#[test]
fn read_tcp_push_pull_with_close_after()
{
	//Handshake the connection.
	let (mut client, _, mut server, _) = handshake_connection3();
	//Do crossed write, including EOF messages. The result should be the message, EOF set, and all data should
	//be accepted.
	let buffer1 = _do_pull_tcp_data(&mut client, PullTcpBuffer::write(CLIENT_HELLO).eof()).unwrap();
	let buffer2 = _do_pull_tcp_data(&mut server, PullTcpBuffer::write(SERVER_HELLO).eof()).unwrap();
	//No data should be accepted.
	assert!(_do_pull_tcp_data(&mut client, PullTcpBuffer::write(&[42])).unwrap_err().to_string().
		contains("Connection already closed for sending"));
	assert!(_do_pull_tcp_data(&mut server, PullTcpBuffer::write(&[42])).unwrap_err().to_string().
		contains("Connection already closed for sending"));
	receive_tcp_push(&mut server, &buffer1, buffer1.len(), true, CLIENT_HELLO);
	receive_tcp_push(&mut client, &buffer2, buffer2.len(), true, SERVER_HELLO);
	//The connection should be closed now.
	assert_close2(&client, C_TX|C_RX|C_NOABORT);
	assert_close2(&server, C_TX|C_RX|C_NOABORT);
}

#[test]
fn datagrams_beyond_update()
{
	//Handshake the connection.
	let (mut client, _, mut server, _) = handshake_connection3();
	//Write 1024 datagrams from client to server. During this operation, the server wants write should have
	//positive edge.
	assert!(!server.get_status().is_want_tx());
	for _ in 0..1024 {
		let buffer1 = _do_pull_tcp_data(&mut client, PullTcpBuffer::write(CLIENT_HELLO)).unwrap();
		receive_tcp_push(&mut server, &buffer1, buffer1.len(), false, CLIENT_HELLO);
	}
	assert!(server.get_status().is_want_tx());
	//Now send from server to client. This should clear the server wants write.
	let buffer2 = _do_pull_tcp_data(&mut server, PullTcpBuffer::write(SERVER_HELLO)).unwrap();
	receive_tcp_push(&mut client, &buffer2, buffer2.len(), false, SERVER_HELLO);
	assert!(!server.get_status().is_want_tx());
	//Close the connection.
	do_normal_close3(client, server);
}

fn call_write_repeat(q: &mut Queue, w: &mut impl IoWrite)
{
	while q.buffered() > 0 { q.call_write(w).expect("call_write failed"); }
}

#[test]
fn client_server_via_pipe()
{
	let (mut csnd, mut srcv) = make_pipe();
	let (mut ssnd, mut crcv) = make_pipe();
	let mut csq = Queue::new();
	let mut crq = Queue::new();
	let mut ssq = Queue::new();
	let mut srq = Queue::new();

	//Make connections.
	let (mut server, _) = make_server_ctx(|_|{}, |_|{});
	let (mut client, _) = make_client_ctx(|_|{}, |_|{});
	//Flight 1, from client to server.
	_do_write_buffer_to_queue(&mut client, PullTcpBuffer::no_data(), &mut csq).unwrap();
	call_write_repeat(&mut csq, &mut csnd);
	_do_read_file_to_queue(&mut server, &mut srcv, &mut srq).unwrap();
	assert_queue(&mut srq, false, &[]);
	//Flight 2, from server to client.
	_do_write_buffer_to_queue(&mut server, PullTcpBuffer::write(b"Hello from server!"), &mut ssq).unwrap();
	call_write_repeat(&mut ssq, &mut ssnd);
	_do_read_file_to_queue(&mut client, &mut crcv, &mut crq).unwrap();
	assert_queue(&mut crq, false, b"Hello from server!");
	//Flight 3, from client to server.
	_do_write_buffer_to_queue(&mut client, PullTcpBuffer::write(b"Hello, from client!"), &mut csq).unwrap();
	call_write_repeat(&mut csq, &mut csnd);
	_do_read_file_to_queue(&mut server, &mut srcv, &mut srq).unwrap();
	assert_queue(&mut srq, false, b"Hello, from client!");
	//Flight 4, from server to client.
	_do_write_buffer_to_queue(&mut server, PullTcpBuffer::write(b"Hello from serveR!"), &mut ssq).unwrap();
	call_write_repeat(&mut ssq, &mut ssnd);
	_do_read_file_to_queue(&mut client, &mut crcv, &mut crq).unwrap();
	assert_queue(&mut crq, false, b"Hello from serveR!");
	//Flight 5, from client to server.
	_do_write_buffer_to_queue(&mut client, PullTcpBuffer::write(b"Hello, from clienT!"), &mut csq).unwrap();
	call_write_repeat(&mut csq, &mut csnd);
	_do_read_file_to_queue(&mut server, &mut srcv, &mut srq).unwrap();
	assert_queue(&mut srq, false, b"Hello, from clienT!");
	//Flight 6: Close from server to client.
	_do_write_buffer_to_queue(&mut server, PullTcpBuffer::no_data().eof(), &mut ssq).unwrap();
	call_write_repeat(&mut ssq, &mut ssnd);
	_do_read_file_to_queue(&mut client, &mut crcv, &mut crq).unwrap();
	assert_queue(&mut crq, true, &[]);
	//Flight 7: Close from client to server.
	_do_write_buffer_to_queue(&mut client, PullTcpBuffer::no_data().eof(), &mut csq).unwrap();
	call_write_repeat(&mut csq, &mut csnd);
	_do_read_file_to_queue(&mut server, &mut srcv, &mut srq).unwrap();
	assert_queue(&mut srq, true, &[]);
	//Now client and server should indicate closed.
	assert_close2(&client, C_TX|C_RX|C_NOABORT);
	assert_close2(&server, C_TX|C_RX|C_NOABORT);
}

#[test]
fn client_server_via_pipe_largemsgs()
{
	let (mut csnd, mut srcv) = make_pipe();
	let (mut ssnd, mut crcv) = make_pipe();
	let mut csq = Queue::new();
	let mut crq = Queue::new();
	let mut ssq = Queue::new();
	let mut srq = Queue::new();

	//Make connections.
	let (mut server, _) = make_server_ctx(|_|{}, |_|{});
	let (mut client, _) = make_client_ctx(|_|{}, |_|{});
	//Flight 1, from client to server.
	_do_write_buffer_to_queue(&mut client, PullTcpBuffer::no_data(), &mut csq).unwrap();
	call_write_repeat(&mut csq, &mut csnd);
	_do_read_file_to_queue(&mut server, &mut srcv, &mut srq).unwrap();
	assert_queue(&mut srq, false, &[]);
	//Flight 2, from server to client.
	_do_write_buffer_to_queue(&mut server, PullTcpBuffer::write(b"Hello from server!"), &mut ssq).unwrap();
	call_write_repeat(&mut ssq, &mut ssnd);
	_do_read_file_to_queue(&mut client, &mut crcv, &mut crq).unwrap();
	assert_queue(&mut crq, false, b"Hello from server!");
	//Flight 3, from client to server.
	_do_write_buffer_to_queue(&mut client, PullTcpBuffer::write(b"Hello, from client!"), &mut csq).unwrap();
	call_write_repeat(&mut csq, &mut csnd);
	_do_read_file_to_queue(&mut server, &mut srcv, &mut srq).unwrap();
	assert_queue(&mut srq, false, b"Hello, from client!");
	//Flight 4, from server to client.
	static DATA1: [u8;384] = [1;384];
	_do_write_buffer_to_queue(&mut server, PullTcpBuffer::write(&DATA1[..]), &mut ssq).unwrap();
	assert_eq!(ssq.buffered(), 384 + 1 * (16 + 1 + 5));		//Should be in 1 record.
	ssq.call_write(&mut ssnd).unwrap();				//Call twice to write all.
	ssq.call_write(&mut ssnd).unwrap();
	_do_read_file_to_queue(&mut client, &mut crcv, &mut crq).unwrap();
	assert_queue(&mut crq, false, &DATA1[..]);
	//Flight 5, from client to server. Use MTU 263, which should give first payload of 258 bytes.
	static DATA2: [u8;384] = [2;384];
	client.set_mss(263, false);
	_do_write_buffer_to_queue(&mut client, PullTcpBuffer::write(&DATA2[..]), &mut csq).unwrap();
	client.set_mss(0, false);
	assert_eq!(csq.buffered(), 384 + 2 * (16 + 1 + 5));		//Should be in 2 records.
	let mut tmp = [0;5];
	assert_eq!(csq.peek(&mut tmp), Some(5));			//Peek the first header.
	assert_eq!((tmp[3] as u16) * 256 + (tmp[4] as u16), 258);	//The length of payload should be 258.
	//Call twice to write all. But interleaved. with read_file_to_queue in order to test partial records.
	csq.call_write(&mut csnd).unwrap();
	_do_read_file_to_queue(&mut server, &mut srcv, &mut srq).unwrap();	
	csq.call_write(&mut csnd).unwrap();
	if srcv.has_data() {
		_do_read_file_to_queue(&mut server, &mut srcv, &mut srq).unwrap();
	}
	assert_queue(&mut srq, false, &DATA2[..]);
	//Flight 6: Server to client, with 8192-byte MTU and WQ_MAX_SIZE.
	server.set_mss(8192, true);
	_do_write_buffer_to_queue(&mut server, PullTcpBuffer::write(&DATA1[..]), &mut ssq).
		unwrap();
	server.set_mss(0, false);
	assert_eq!(ssq.buffered(), 8192);
	ssq.call_write(&mut ssnd).unwrap();				//Call twice to write all.
	ssq.call_write(&mut ssnd).unwrap();
	_do_read_file_to_queue(&mut client, &mut crcv, &mut crq).unwrap();
	assert_queue(&mut crq, false, &DATA1[..]);
	//Flight 7, from client to server. Use MTU WQ_MAX_SIZE.
	client.set_mss(0, true);
	_do_write_buffer_to_queue(&mut client, PullTcpBuffer::write(&DATA2[..]), &mut csq).unwrap();
	client.set_mss(0, false);
	assert_eq!(csq.buffered(), 16406);				//Absolute maximum.
	csq.call_write(&mut csnd).unwrap();				
	_do_read_file_to_queue(&mut server, &mut srcv, &mut srq).unwrap();	
	assert_queue(&mut srq, false, &DATA2[..]);
	//Flight 8: Close from server to client.
	_do_write_buffer_to_queue(&mut server, PullTcpBuffer::no_data().eof(), &mut ssq).unwrap();
	ssq.call_write(&mut ssnd).unwrap();
	_do_read_file_to_queue(&mut client, &mut crcv, &mut crq).unwrap();
	assert_queue(&mut crq, true, &[]);
	//Flight 9: Close from client to server.
	_do_write_buffer_to_queue(&mut client, PullTcpBuffer::no_data().eof(), &mut csq).unwrap();
	csq.call_write(&mut csnd).unwrap();
	_do_read_file_to_queue(&mut server, &mut srcv, &mut srq).unwrap();
	assert_queue(&mut srq, true, &[]);
	//Now client and server should indicate closed.
	assert_close2(&client, C_TX|C_RX|C_NOABORT);
	assert_close2(&server, C_TX|C_RX|C_NOABORT);
}

#[test]
fn test_unknown_extensions()
{
	let (mut server, scb_recv) = make_server_ctx(|server_callbacks|{
		server_callbacks.test_extensions = true;
		server_callbacks.test_extensions2 = true;
	}, |_|{
	});

	let (mut client, ccb_recv) = make_client_ctx(|client_callbacks|{
		client_callbacks.test_extensions = true;
		client_callbacks.test_extensions2 = true;
	}, |_|{
	});

	//Now handshake the connection.
	soft_handshake(&mut client, &mut server);
	//Handshake should have completed.
	assert!(!client.get_status().is_handshaking());
	assert!(!server.get_status().is_handshaking());
	assert_eq!(ccb_recv.with(|x|x.ext_event_log.clone()).deref(),
		r#"Offering extensions...
Got extension 456: Answer 456
Got extension 789: Answer 789
End of extensions msgid=1
"#);
	assert_eq!(scb_recv.with(|x|x.ext_event_log.clone()).deref(),
		"Got extension 3: \u{0}\u{15}\u{1}ABCDEFGHIJKLMNOPQRST
Got extension 47: \u{0}\u{16}\u{0}\u{14}ABCDEFGHIJKLMNOPQRST
Got extension 123: test extension 123
Got extension 456: test extension 456
Got extension 789: test extension 789
End of extensions msgid=0
Responding to extension 456
Responding to extension 789
");

	//Close the connection.
	do_normal_close3(client, server);

}

fn do_base_handshake(client: &mut ClientConnection, server: &mut ServerConnection)
{
	loop {
		let cflags = client.get_status();
		let sflags = server.get_status();
		if cflags.is_want_tx() && sflags.is_want_rx() {
			//Client to server.
			let buffer = _do_pull_tcp_data(client, PullTcpBuffer::no_data()).unwrap();
			let (tmp, eofd) = _do_push_tcp_data(server, PushTcpBuffer::u8(&buffer)).unwrap();
			let status = server.get_status();
			if tmp.len() > 0 || eofd || status.is_rx_end() { unreachable!(); }
		} else if cflags.is_want_rx() && sflags.is_want_tx() {
			//Server to client.
			let buffer = _do_pull_tcp_data(server, PullTcpBuffer::no_data()).unwrap();
			let (tmp, eofd) = _do_push_tcp_data(client, PushTcpBuffer::u8(&buffer)).unwrap();
			let status = client.get_status();
			if tmp.len() > 0 || eofd || status.is_rx_end() { unreachable!(); }
		} else if !cflags.is_handshaking() && !sflags.is_handshaking() {
			return;			//Halt.
		} else {
			assert!(false);		//Deadlock!
		}
	}
}

#[test]
fn test_unknown_extensions_cc()
{
	let (mut server, scb_recv) = make_server_ctx(|server_callbacks|{
		server_callbacks.test_extensions = true;
		server_callbacks.req_cert = true;
	}, |_|{
	});

	let (mut client, ccb_recv) = make_client_ctx(|client_callbacks|{
		client_callbacks.test_extensions = true;
	}, |_|{
	});

	//Now handshake the connection.
	do_base_handshake(&mut client, &mut server);
	assert_eq!(ccb_recv.with(|x|x.ext_event_log.clone()).deref(),
		r#"Offering extensions...
Got extension 456: Answer 456
Got extension 789: Answer 789
End of extensions msgid=1
Got extension 123: test extension 123
Got extension 456: test extension 456
Got extension 789: test extension 789
End of extensions msgid=2
"#);
	assert_eq!(scb_recv.with(|x|x.ext_event_log.clone()).deref(),
		r#"Got extension 123: test extension 123
Got extension 456: test extension 456
Got extension 789: test extension 789
End of extensions msgid=0
Responding to extension 456
Responding to extension 789
Offering extensions...
"#);

	//Close the connection.
	do_normal_close3(client, server);

}

#[test]
fn test_tls13_no_sslkeylog()
{
	let (mut server, serverctx) = make_server_ctx(|_|{}, |_|{});
	let (mut client, clientctx) = make_client_ctx(|_|{}, |_|{});

	do_base_handshake(&mut client, &mut server);
	do_crossed_write(client, server);
	let server_log = serverctx.with(|inner|inner.ext_event_log.clone());
	let client_log = clientctx.with(|inner|inner.ext_event_log.clone());
	for line in server_log.lines() { assert!(!line.starts_with("ssl_key_log:")); }
	for line in client_log.lines() { assert!(!line.starts_with("ssl_key_log:")); }
}

#[test]
fn test_tls13_sslkeylog()
{
	let (mut server, serverctx) = make_server_ctx(|_|{}, |_|{});
	let (mut client, clientctx) = make_client_ctx(|_|{}, |_|{});

	client.enable_ssl_key_log();
	server.enable_ssl_key_log();
	do_base_handshake(&mut client, &mut server);
	do_crossed_write(client, server);
	let random = serverctx.with(|inner|inner.client_random.expect("Client random not set?"));
	let server_log = serverctx.with(|inner|inner.ext_event_log.clone());
	let client_log = clientctx.with(|inner|inner.ext_event_log.clone());
	let mut client_log2: Vec<&str> = server_log.lines().filter(|line|line.starts_with("ssl_key_log:")).collect();
	let mut server_log2: Vec<&str> = client_log.lines().filter(|line|line.starts_with("ssl_key_log:")).collect();
	client_log2.sort();
	server_log2.sort();
	assert!(client_log2.len() == 5);
	assert!(server_log2.len() == 5);
	assert!(client_log2 == server_log2);
	let expect_start = format!("ssl_key_log: ClientApplicationTrafficSecret0 {random:?} ");
	assert!(client_log2[0].starts_with(&expect_start));
	let expect_start = format!("ssl_key_log: ClientHandshakeTrafficSecret {random:?} ");
	assert!(client_log2[1].starts_with(&expect_start));
	let expect_start = format!("ssl_key_log: ExporterMasterSecret {random:?} ");
	assert!(client_log2[2].starts_with(&expect_start));
	let expect_start = format!("ssl_key_log: ServerApplicationTrafficSecret0 {random:?} ");
	assert!(client_log2[3].starts_with(&expect_start));
	let expect_start = format!("ssl_key_log: ServerHandshakeTrafficSecret {random:?} ");
	assert!(client_log2[4].starts_with(&expect_start));
}

#[test]
fn test_0rtt()
{
	//Create connections.
	let (mut server, _) = make_server_ctx(|_|{}, |_|{});
	let (mut client, _) = make_client_ctx(|_|{}, |_|{});
	//Initially client should want to transmit to server and server receive.
	assert_data2(&client, &server, F_CTS);
	transfer_packets_hs3(&mut client, &mut server);
	//Try to transmit 0-RTT. This should just not be accepted.
	assert!(_do_pull_tcp_data(&mut client, PullTcpBuffer::write(&[42])).unwrap_err().to_string().
		contains("Connection not yet ready to send data"));
	//Now the interest should have reversed... And the server should have run to point where TX is ready.
	assert_data2(&client, &server, F_SW | F_STC);
	transfer_packets_hs3(&mut server, &mut client);
	//Now interests should have reversed again... And server to client is now up. This should also perma-
	//set the client wants read flag. Also, client has run the handshake.
	assert_data2(&client, &server, F_SW | F_CRW | F_CTS);
	transfer_packets_hs3(&mut client, &mut server);
	//Now neither should want to write, and connections are fully up. Both wants read flags should now be
	//perma-set.
	assert_data2(&client, &server, F_ALL);
	do_crossed_write(client, server);
}

#[test]
fn test_0dot5rtt()
{
	//Create connections.
	let (mut server, _) = make_server_ctx(|_|{}, |_|{});
	let (mut client, _) = make_client_ctx(|_|{}, |_|{});
	//Initially client should want to transmit to server and server receive.
	assert_data2(&client, &server, F_CTS);
	transfer_packets_hs3(&mut client, &mut server);
	//Try to transmit 0-RTT. This should just not be accepted.
	//Now the interest should have reversed... And the server should have run to point where TX is ready.
	assert_data2(&client, &server, F_SW | F_STC);
	transfer_packets_hs3(&mut server, &mut client);
	//Now interests should have reversed again... And server to client is now up. This should also perma-
	//set the client wants read flag. Also, client has run the handshake.
	assert_data2(&client, &server, F_SW | F_CRW | F_CTS);
	//Do 0.5rtt write.The EOF flag should not be set and messages should be as expected.
	let buffer2 = _do_pull_tcp_data(&mut server, PullTcpBuffer::write(SERVER_HELLO)).unwrap();
	receive_tcp_push(&mut client, &buffer2, buffer2.len(), false, SERVER_HELLO);
	//Client sends its last flight.
	transfer_packets_hs3(&mut client, &mut server);
	//Now neither should want to write, and connections are fully up. Both wants read flags should now be
	//perma-set.
	assert_data2(&client, &server, F_ALL);
	//Close the connection.
	do_normal_close3(client, server);
}

#[test]
fn test_tls13_conn_pad()
{
	//Handshake the connection.
	let (mut client, _, mut server, _) = handshake_connection3();

	//Do crossed write. The EOF flag should not be set and messages should be as expected. The client message
	//should be 8192 bytes.
	client.set_mss(8192, true);
	let buffer1 = _do_pull_tcp_data(&mut client, PullTcpBuffer::write(CLIENT_HELLO)).unwrap();
	let buffer2 = _do_pull_tcp_data(&mut server, PullTcpBuffer::write(SERVER_HELLO)).unwrap();
	assert_eq!(buffer1.len(), 8192);
	assert_eq!(buffer1[3], 31);
	assert_eq!(buffer1[4], 251);
	receive_tcp_push(&mut server, &buffer1, buffer1.len(), false, CLIENT_HELLO);
	receive_tcp_push(&mut client, &buffer2, buffer2.len(), false, SERVER_HELLO);

	//Do crossed write. The EOF flag should not be set and messages should be as expected. The client message
	//should be 16406 bytes (the largest possible)
	client.set_mss(0, true);
	let buffer1 = _do_pull_tcp_data(&mut client, PullTcpBuffer::write(CLIENT_HELLO)).unwrap();
	let buffer2 = _do_pull_tcp_data(&mut server, PullTcpBuffer::write(SERVER_HELLO)).unwrap();
	assert_eq!(buffer1.len(), 16406);
	assert_eq!(buffer1[3], 64);
	assert_eq!(buffer1[4], 17);
	receive_tcp_push(&mut server, &buffer1, buffer1.len(), false, CLIENT_HELLO);
	receive_tcp_push(&mut client, &buffer2, buffer2.len(), false, SERVER_HELLO);

	//Close the connection.
	client.set_mss(0, false);
	do_normal_close3(client, server);
}

fn do_tls12_base_handshake(client: &mut ClientConnection, server: &mut ServerConnection)
{
	//The initial interest should be transfer from client to server...
	assert_data2(client, server, F_CTS);
	transfer_packets_hs3(client, server);
	//Now the interest should have reversed... But TX should not be ready.
	assert_data2(client, server, F_STC);
	transfer_packets_hs3(server, client);
	//Now the interests should have swapped again. Server TX should be ready.
	assert_data2(client, server, F_CTS);
	transfer_packets_hs3(client, server);
	//Now the interest should have reversed.... Client has become fully ready.
	assert_data2(client, server, F_SRW | F_STC);
	transfer_packets_hs3(server, client);
	//Now the handshake should be complete on both sides.
	assert_data2(client, server, F_ALL);
}


#[test]
fn test_tls12_conn()
{
	let (mut server, _) = make_server_ctx(|_|{
	}, |config|{
		config.set_flags(5, 512);
	});

	let (mut client, _) = make_client_ctx(|_|{
	}, |_|{
	});

	do_tls12_base_handshake(&mut client, &mut server);
	do_crossed_write(client, server);
}

#[test]
fn test_tls12_conn_unsafe_start()
{
	let (mut server, _) = make_server_ctx(|_|{}, |config|{
		config.set_flags(5, 512);
	});
	let (mut client, _) = make_client_ctx(|_|{}, |_|{});

	//The initial interest should be transfer from client to server...
	assert_data2(&client, &server, F_CTS);
	transfer_packets_hs3(&mut client, &mut server);
	//Now the interest should have reversed... But TX should not be ready.
	assert_data2(&client, &server, F_STC);
	transfer_packets_hs3(&mut server, &mut client);
	//Now the interests should have swapped again. Server TX should be ready.
	assert_data2(&client, &server, F_CTS);
	transfer_packets_hs3(&mut client, &mut server);
	//Now the interest should have reversed.... Client has become fully ready.
	assert_data2(&client, &server, F_SRW | F_STC);
	//Try to transmit insecure start. This should just not be accepted.
	assert!(_do_pull_tcp_data(&mut client, PullTcpBuffer::write(&[42])).unwrap_err().to_string().
		contains("Connection not yet ready to send data"));
	//Ok, fourth and last flight.
	transfer_packets_hs3(&mut server, &mut client);
	//Now the handshake should be complete on both sides.
	assert_data2(&client, &server, F_ALL);
	do_crossed_write(client, server);
}

#[test]
fn test_tls12_conn_trypad()
{
	let (mut server, _) = make_server_ctx(|_|{
	}, |config|{
		config.set_flags(5, 512);
	});

	let (mut client, _) = make_client_ctx(|_|{
	}, |_|{
	});

	do_tls12_base_handshake(&mut client, &mut server);
	//Do crossed write. The EOF flag should not be set and messages should be as expected. The client message
	//should be less than 128 bytes in size, since padding does not work with TLS 1.2.
	client.set_mss(0, true);
	let buffer1 = _do_pull_tcp_data(&mut client, PullTcpBuffer::write(CLIENT_HELLO)).unwrap();
	let buffer2 = _do_pull_tcp_data(&mut server, PullTcpBuffer::write(SERVER_HELLO)).unwrap();
	client.set_mss(0, false);
	assert!(buffer1.len() < 128);
	receive_tcp_push(&mut server, &buffer1, buffer1.len(), false, CLIENT_HELLO);
	receive_tcp_push(&mut client, &buffer2, buffer2.len(), false, SERVER_HELLO);
	//Close the connection.
	do_normal_close3(client, server);
}

#[test]
fn test_tls12_no_sslkeylog()
{
	let (mut server, serverctx) = make_server_ctx(|_|{
	}, |config|{
		config.set_flags(5, 512);
	});

	let (mut client, clientctx) = make_client_ctx(|_|{
	}, |_|{
	});

	do_tls12_base_handshake(&mut client, &mut server);
	do_crossed_write(client, server);
	let server_log = serverctx.with(|inner|inner.ext_event_log.clone());
	let client_log = clientctx.with(|inner|inner.ext_event_log.clone());
	for line in server_log.lines() { assert!(!line.starts_with("ssl_key_log:")); }
	for line in client_log.lines() { assert!(!line.starts_with("ssl_key_log:")); }
}

#[test]
fn test_tls12_sslkeylog()
{
	let (mut server, serverctx) = make_server_ctx(|_|{
	}, |config|{
		config.set_flags(5, 512);
	});

	let (mut client, clientctx) = make_client_ctx(|_|{
	}, |_|{
	});

	client.enable_ssl_key_log();
	server.enable_ssl_key_log();
	do_tls12_base_handshake(&mut client, &mut server);
	do_crossed_write(client, server);
	let random = serverctx.with(|inner|inner.client_random.expect("Client random not set?"));
	let server_log = serverctx.with(|inner|inner.ext_event_log.clone());
	let client_log = clientctx.with(|inner|inner.ext_event_log.clone());
	let mut client_log2: Vec<&str> = server_log.lines().filter(|line|line.starts_with("ssl_key_log:")).collect();
	let mut server_log2: Vec<&str> = client_log.lines().filter(|line|line.starts_with("ssl_key_log:")).collect();
	client_log2.sort();
	server_log2.sort();
	assert!(client_log2.len() == 1);
	assert!(server_log2.len() == 1);
	assert!(client_log2 == server_log2);
	let expect_start = format!("ssl_key_log: Tls12MasterSecret {random:?} ");
	assert!(client_log2[0].starts_with(&expect_start));
}


#[test]
fn tcp_push_error()
{
	//Handshake the connection.
	let (mut client, _, mut server, _) = handshake_connection3();
	//Raise alert and write it out. (with some data that should not be accepted). Then try to write again, this
	//should fail. And when reading the record, the connection should immediately fail. The first write should
	//shallow the data.
	client.raise_alert(255);
	let bufferx = _do_pull_tcp_data(&mut client, PullTcpBuffer::write(CLIENT_HELLO)).unwrap();
	assert!(bufferx.len() == 24);
	assert!(&_do_pull_tcp_data(&mut client, PullTcpBuffer::write(CLIENT_HELLO)).unwrap_err().to_string().
		contains("Locally raised alert <?255>"));
	assert!(_do_push_tcp_data(&mut server, PushTcpBuffer::u8(&bufferx)).is_err());
	let status = server.get_status();
	if status.is_rx_end() { unreachable!(); }
}

fn do_corrupt_alert<F:Connection,T:Connection>(mut client: F, mut server: T)
{
	assert_close2(&client, C_NOABORT);
	assert_close2(&server, C_NOABORT);
	//Write out the supposed alert. Now the connection should be down on server side. But not on client side.
	let buffer2 = _do_pull_tcp_data(&mut server, PullTcpBuffer::no_data()).unwrap();
	assert_close2(&client, C_NOABORT);
	assert_close2(&server, C_ABORT);
	//Receive the alert. This should fail! And the connection should then be torn down on both sides.
	assert!(_do_push_tcp_data(&mut client, PushTcpBuffer::u8(&buffer2)).is_err());
	assert_close2(&client, C_ABORT);
	assert_close2(&server, C_ABORT);
}

#[test]
fn corrupt_tcp_push_received()
{
	//Handshake the connection.
	let (mut client, _, mut server, _) = handshake_connection3();
	//Write out a message and corrupt it. Receiving should consume the entiere message and emit nothing.
	//But the server should not consider the connection down yet.
	let mut buffer1 = _do_pull_tcp_data(&mut client, PullTcpBuffer::write(CLIENT_HELLO)).unwrap();
	buffer1[6] ^= 1;
	receive_tcp_push(&mut server, &buffer1, buffer1.len(), false, &[]);

	do_corrupt_alert(client, server);
}

#[test]
fn ptb_tcp_push_received()
{
	//Handshake the connection.
	let (client, _, mut server, _) = handshake_connection3();
	//Write out a message with oversize header. Receiving should consume the entiere message and emit nothing.
	//But the server should not consider the connection down yet.
	let buffer1 = [23, 3, 3, 65, 1];
	receive_tcp_push(&mut server, &buffer1, 5, false, &[]);

	do_corrupt_alert(client, server);
}

fn soft_handshake(client: &mut ClientConnection, server: &mut ServerConnection)
{
	loop {
		let cflags = client.get_status();
		let sflags = server.get_status();
		if cflags.is_want_tx() && sflags.is_want_rx() {
			//Client to server.
			let buffer = _do_pull_tcp_data(client, PullTcpBuffer::no_data()).unwrap();
			let ret = _do_push_tcp_data(server, PushTcpBuffer::u8(&buffer)).ok();
			let status = server.get_status();
			if status.is_rx_end() { unreachable!(); }
			if let Some((tmp, eofd)) = ret { if tmp.len() > 0 || eofd { unreachable!(); } }
		} else if cflags.is_want_rx() && sflags.is_want_tx() {
			//Server to client.
			let buffer = _do_pull_tcp_data(server, PullTcpBuffer::no_data()).unwrap();
			let ret = _do_push_tcp_data(client, PushTcpBuffer::u8(&buffer)).ok();
			let status = client.get_status();
			if status.is_rx_end() { unreachable!(); }
			if let Some((tmp, eofd)) = ret { if tmp.len() > 0 || eofd { unreachable!(); } }
		} else if (!cflags.is_handshaking()||cflags.is_aborted()) &&
			(!sflags.is_handshaking()||sflags.is_aborted()) {
			break;			//Halt.
		} else {
			assert!(false);		//Deadlock!
		}
	}
}

fn test_handshake_fail_n(bit: u32)
{
	let flags5 = 1 << bit;

	let (mut server, _) = make_server_ctx(|_|{
	}, |config|{
		config.set_flags(5, flags5);
	});

	let (mut client, _) = make_client_ctx(|_|{
	}, |config|{
		config.set_flags(5, flags5);
	});

	//The client and server should eventually come to halt, with both in error state. We do not care about
	//immediate aborts received.
	soft_handshake(&mut client, &mut server);
	//Handshake should have aborted on both sides:
	assert!(client.get_status().is_aborted());
	assert!(server.get_status().is_aborted());
	//But handshake should have never completed (execpt bit=7 completes on client).
	assert!(client.get_status().is_handshaking() == (bit!=7));
	assert!(server.get_status().is_handshaking());
}

#[test] fn test_handshake_fail0() { test_handshake_fail_n(0); }
#[test] fn test_handshake_fail1() { test_handshake_fail_n(1); }
#[test] fn test_handshake_fail2() { test_handshake_fail_n(2); }
#[test] fn test_handshake_fail3() { test_handshake_fail_n(3); }
#[test] fn test_handshake_fail4() { test_handshake_fail_n(4); }
#[test] fn test_handshake_fail5() { test_handshake_fail_n(5); }
#[test] fn test_handshake_fail6() { test_handshake_fail_n(6); }
#[test] fn test_handshake_fail7() { test_handshake_fail_n(7); }
#[test] fn test_handshake_fail8() { test_handshake_fail_n(8); }


#[test]
fn pipe()
{
	let (mut s, mut r) = make_pipe();
	s.write(b"Hello, World!").unwrap();
	let mut buf = [0;5];
	assert!(r.read(&mut buf).unwrap() == 5);
	assert_eq!(&buf[..], b"Hello");
	assert!(r.read(&mut buf).unwrap() == 5);
	assert_eq!(&buf[..], b", Wor");
	assert!(r.read(&mut buf).unwrap() == 3);
	assert_eq!(&buf[..], b"ld!or");
}

#[test]
fn test_tls12_conn_slowkey()
{
	let (cert, privkey, _) = test_certificate_kp();
	let (privkey, privikey_trigger) = SlowKey::new(privkey);
	let mut cert = cert.deref();
	let mut certs = LocalServerCertificateStore::new();
	certs.add_privkey(privkey, "test.key");
	certs.add(&mut cert, "test.crt").unwrap();

	let server_callbacks = Arc::new(Mutex::new(EpCallbacks::new()));
	let mut serverconfig = ServerConfiguration::from(certs.lookup());
	serverconfig.set_flags(5, 512);
	let mut server = serverconfig.make_connection(Box::new(server_callbacks));

	let (mut client, _) = make_client_ctx(|_|{}, |_|{});

	//The initial interest should be transfer from client to server...
	assert_data2(&client, &server, F_CTS);
	transfer_packets_hs3(&mut client, &mut server);
	//Now the interest should have reversed... But TX should not be ready.
	assert_data2(&client, &server, F_STC_BLOCKED);
	transfer_packets_hs3(&mut server, &mut client);
	//Because the key is slow, the transfer should have suspended, in state where client wants to read, but
	//server does not want to write. But trigger should restore server wanting to write.
	assert_data2(&client, &server, F_STC_BLOCKED);
	privikey_trigger.trigger();
	assert_data2(&client, &server, F_STC);
	transfer_packets_hs3(&mut server, &mut client);
	//Now the interests should have swapped again. Server TX should be ready.
	assert_data2(&client, &server, F_CTS);
	transfer_packets_hs3(&mut client, &mut server);
	//Now the interest should have reversed.... Client has become fully ready.
	assert_data2(&client, &server, F_SRW | F_STC);
	transfer_packets_hs3(&mut server, &mut client);
	//Now the handshake should be complete on both sides.
	assert_data2(&client, &server, F_ALL);
	do_crossed_write(client, server);
}

#[test]
fn test_tls13_conn_slowkey()
{
	let (cert, privkey, _) = test_certificate_kp();
	let (privkey, privikey_trigger) = SlowKey::new(privkey);
	let mut cert = cert.deref();
	let mut certs = LocalServerCertificateStore::new();
	certs.add_privkey(privkey, "test.key");
	certs.add(&mut cert, "test.crt").unwrap();

	let server_callbacks = Arc::new(Mutex::new(EpCallbacks::new()));
	let serverconfig = ServerConfiguration::from(certs.lookup());
	let mut server = serverconfig.make_connection(Box::new(server_callbacks));

	let (mut client, _) = make_client_ctx(|_|{}, |_|{});

	//The initial interest should be transfer from client to server...
	assert_data2(&client, &server, F_CTS);
	transfer_packets_hs3(&mut client, &mut server);
	//Now the interest should have reversed... But TX should not be ready.
	assert_data2(&client, &server, F_STC);
	transfer_packets_hs3(&mut server, &mut client);
	//Because the key is slow, the transfer should have suspended, in state where client wants to read, but
	//server does not want to write. But trigger should restore server wanting to write. However, the server
	//data write flag will not be set.
	assert_data2(&client, &server, F_STC_BLOCKED);
	privikey_trigger.trigger();
	assert_data2(&client, &server, F_STC);
	transfer_packets_hs3(&mut server, &mut client);
	//Now the interests should have swapped again. The client should have run into completion.
	assert_data2(&client, &server, F_CRW | F_SW | F_CTS);
	transfer_packets_hs3(&mut client, &mut server);
	//Now the handshake should be complete on both sides.
	assert_data2(&client, &server, F_ALL);
	do_crossed_write(client, server);
}

#[test]
fn test_tls12_conn_fastkey()
{
	let (cert, privkey, _) = test_certificate_kp();
	let privkey = FastKey::new(privkey);
	let mut cert = cert.deref();
	let mut certs = LocalServerCertificateStore::new();
	certs.add_privkey(privkey, "test.key");
	certs.add(&mut cert, "test.crt").unwrap();

	let server_callbacks = Arc::new(Mutex::new(EpCallbacks::new()));
	let mut serverconfig = ServerConfiguration::from(certs.lookup());
	serverconfig.set_flags(5, 512);
	let mut server = serverconfig.make_connection(Box::new(server_callbacks));

	let (mut client, _) = make_client_ctx(|_|{}, |_|{});

	//The initial interest should be transfer from client to server...
	assert_data2(&client, &server, F_CTS);
	transfer_packets_hs3(&mut client, &mut server);
	//Now the interest should have reversed... But TX should not be ready.
	assert_data2(&client, &server, F_STC);
	transfer_packets_hs3(&mut server, &mut client);
	//Now the interests should have swapped again. Server TX should be ready.
	assert_data2(&client, &server, F_CTS);
	transfer_packets_hs3(&mut client, &mut server);
	//Now the interest should have reversed.... Client has become fully ready.
	assert_data2(&client, &server, F_SRW | F_STC);
	transfer_packets_hs3(&mut server, &mut client);
	//Now the handshake should be complete on both sides.
	assert_data2(&client, &server, F_ALL);
	do_crossed_write(client, server);
}

#[test]
fn test_tls13_conn_fastkey()
{
	let (cert, privkey, _) = test_certificate_kp();
	let privkey = FastKey::new(privkey);
	let mut cert = cert.deref();
	let mut certs = LocalServerCertificateStore::new();
	certs.add_privkey(privkey, "test.key");
	certs.add(&mut cert, "test.crt").unwrap();

	let server_callbacks = Arc::new(Mutex::new(EpCallbacks::new()));
	let serverconfig = ServerConfiguration::from(certs.lookup());
	let mut server = serverconfig.make_connection(Box::new(server_callbacks));

	let (mut client, _) = make_client_ctx(|_|{}, |_|{});

	//The initial interest should be transfer from client to server...
	assert_data2(&client, &server, F_CTS);
	transfer_packets_hs3(&mut client, &mut server);
	//Now the interest should have reversed... But TX should not be ready.
	assert_data2(&client, &server, F_SW | F_STC);
	transfer_packets_hs3(&mut server, &mut client);
	//Now the interests should have swapped again. The client should have run into completion.
	assert_data2(&client, &server, F_CRW | F_SW | F_CTS);
	transfer_packets_hs3(&mut client, &mut server);
	//Now the handshake should be complete on both sides.
	assert_data2(&client, &server, F_ALL);
	do_crossed_write(client, server);
}

fn do_crossed_write(mut client: ClientConnection, mut server: ServerConnection)
{
	//Do crossed write. The EOF flag should not be set and messages should be as expected.
	let buffer1 = _do_pull_tcp_data(&mut client, PullTcpBuffer::write(CLIENT_HELLO)).unwrap();
	let buffer2 = _do_pull_tcp_data(&mut server, PullTcpBuffer::write(SERVER_HELLO)).unwrap();
	receive_tcp_push(&mut server, &buffer1, buffer1.len(), false, CLIENT_HELLO);
	receive_tcp_push(&mut client, &buffer2, buffer2.len(), false, SERVER_HELLO);
	//Close the connection.
	do_normal_close3(client, server);
}

fn do_close_echo_test<F:Connection,T:Connection>(mut client: F, mut server: T)
{
	//Trigger end-of-stream from client side.
	assert!(!client.get_status().is_want_tx());
	let bufferx = _do_pull_tcp_data(&mut client, PullTcpBuffer::no_data().eof()).unwrap();
	assert!(!client.get_status().is_want_tx());
	assert_data2(&client, &server, F_CRW | F_CTS_BLOCKED);
	//After this server should want to transmit, but not have either EOF flag set.
	receive_tcp_push(&mut server, &bufferx, bufferx.len(), false, &[]);
	assert_close2(&server, C_NOABORT);
	assert_data2(&client, &server, F_CRW | F_STC | F_CTS_BLOCKED);
	//Now, transmit to client. This should trigger close from server, and read as EOF with no data. After
	//that, the client should also read as closed. Try to piggyback data, it should not be accepted.
	assert!(&_do_pull_tcp_data(&mut server, PullTcpBuffer::write(SERVER_HELLO)).unwrap_err().to_string().
		contains("Connection not yet ready to send data"));
	let buffer1 = _do_pull_tcp_data(&mut server, PullTcpBuffer::no_data()).unwrap();
	assert_close2(&server, C_LINK|C_NOABORT);
	receive_tcp_push(&mut client, &buffer1, buffer1.len(), true, &[]);
	assert_close2(&client, C_TX|C_RX|C_NOABORT);
}

#[test]
fn test_tls12_close_echo()
{
	let (mut server, _) = make_server_ctx(|_|{}, |config|{
		config.set_flags(5, 512);
	});

	let (mut client, ccb_recv) = make_client_ctx(|client_callbacks|{
		client_callbacks.expect_alpn = Some("test-close-echo".to_owned());
	}, |config|{
		config.add_alpn("test-close-echo");
	});

	//The initial interest should be transfer from client to server...
	assert_data2(&client, &server, F_CTS);
	transfer_packets_hs3(&mut client, &mut server);
	//Now the interest should have reversed... But TX should not be ready.
	assert_data2(&client, &server, F_STC);
	transfer_packets_hs3(&mut server, &mut client);
	//Now the interests should have swapped again. Server TX should be ready.
	assert_data2(&client, &server, F_CTS);
	transfer_packets_hs3(&mut client, &mut server);
	//Now the interest should have reversed.... Client has become fully ready.
	assert_data2(&client, &server, F_STC | F_CTS_BLOCKED);
	transfer_packets_hs3(&mut server, &mut client);
	//Now the handshake should be complete on client side but not on server side. The server side should
	//indicate raw data wants read, but not write.
	assert_data2(&client, &server, F_CRW | F_CTS_BLOCKED);
	assert!(ccb_recv.with(|x|x.alpn_checked));

	do_close_echo_test(client, server);
}

#[test]
fn test_tls13_close_echo()
{
	let (mut server, _) = make_server_ctx(|_|{
	}, |_|{
	});

	let (mut client, ccb_recv) = make_client_ctx(|client_callbacks|{
		client_callbacks.expect_alpn = Some("test-close-echo".to_owned());
	}, |config|{
		config.add_alpn("test-close-echo");
	});

	//The initial interest should be transfer from client to server...
	assert_data2(&client, &server, F_CTS);
	transfer_packets_hs3(&mut client, &mut server);
	//Now the interest should have reversed... But TX should not be ready.
	assert_data2(&client, &server, F_STC);
	transfer_packets_hs3(&mut server, &mut client);
	//Now the handshake should be complete on client side but not on server side. The server side should
	//indicate raw data wants read, but not write.
	assert_data2(&client, &server, F_CRW | F_CTS);
	transfer_packets_hs3(&mut client, &mut server);
	assert_data2(&client, &server, F_CRW | F_CTS_BLOCKED);
	assert!(ccb_recv.with(|x|x.alpn_checked));

	do_close_echo_test(client, server);
}

fn rot13_echo_test_tail<F:Connection,T:Connection>(mut client: F, mut server: T,
	ccb_recv: Arc<Mutex<EpCallbacks>>, scb_recv: Arc<Mutex<EpCallbacks>>)
{
	//The timeouts should now have been cleared for both.
	assert!(!ccb_recv.with(|x|x.timeout_active));
	assert!(!scb_recv.with(|x|x.timeout_active));

	//Send "Hello, World!" to the server. The server should afterwards have data to send.
	let buffer1 = _do_pull_tcp_data(&mut client, PullTcpBuffer::write(b"Hello, World!\n")).unwrap();
	assert!(server.get_status().is_want_rx());
	receive_tcp_push(&mut server, &buffer1, buffer1.len(), false, &[]);
	assert_data2(&client, &server, F_CRW | F_STC | F_SERV_INTERNAL);
	//The server message should read "Y Uryyb, Jbeyq\n". This should clear server wants send.
	assert!(server.get_status().is_want_tx());
	let buffer1 = _do_pull_tcp_data(&mut server, PullTcpBuffer::no_data()).unwrap();
	receive_tcp_push(&mut client, &buffer1, buffer1.len(), false, b"Y Uryyb, Jbeyq!\n");
	assert_data2(&client, &server, F_CRW | F_CTS_BLOCKED | F_SERV_INTERNAL);

	//Send "Uryyb, Jbeyq!" to the server. The server should afterwards have data to send.
	let buffer1 = _do_pull_tcp_data(&mut client, PullTcpBuffer::write(b"Uryyb, Jbeyq!\n")).unwrap();
	assert!(server.get_status().is_want_rx());
	receive_tcp_push(&mut server, &buffer1, buffer1.len(), false, &[]);
	assert_data2(&client, &server, F_CRW | F_STC | F_SERV_INTERNAL);
	//The server message should read "Y Hello, World\n". This should clear server wants send.
	assert!(server.get_status().is_want_tx());
	let buffer1 = _do_pull_tcp_data(&mut server, PullTcpBuffer::no_data()).unwrap();
	receive_tcp_push(&mut client, &buffer1, buffer1.len(), false, b"Y Hello, World!\n");
	assert_data2(&client, &server, F_CRW | F_CTS_BLOCKED | F_SERV_INTERNAL);

	//Trigger end-of-stream from client side.
	assert!(!client.get_status().is_want_tx());
	let buffer1 =  _do_pull_tcp_data(&mut client, PullTcpBuffer::no_data().eof()).unwrap();
	assert!(!client.get_status().is_want_tx());
	assert_data2(&client, &server, F_CRW | F_CTS_BLOCKED | F_SERV_INTERNAL);
	//Afterwards, the serve should want to transmit but not have EOF set.
	receive_tcp_push(&mut server, &buffer1, buffer1.len(), false, &[]);
	//Now server should want to transmit, but not have either EOF flag set.
	assert_close2(&server, C_NOABORT);
	assert_data2(&client, &server, F_CRW | F_STC | F_SERV_INTERNAL);
	//Transmit to client. This should close on client.
	let buffer1 = _do_pull_tcp_data(&mut server, PullTcpBuffer::no_data()).unwrap();
	assert_close2(&server, C_LINK|C_NOABORT);
	receive_tcp_push(&mut client, &buffer1, buffer1.len(), true, &[]);
	assert_close2(&client, C_TX|C_RX|C_NOABORT);
}

#[test]
fn test_tls12_rot13_echo()
{
	let (mut server, scb_recv) = make_server_ctx(|_|{}, |config|{
		config.set_flags(5, 512);
		config.set_deadline(Some(Duration::from_secs(100)));
	});

	let (mut client, ccb_recv) = make_client_ctx(|client_callbacks|{
		client_callbacks.expect_alpn = Some("test-rot13".to_owned());
	}, |config|{
		config.add_alpn("test-rot13");
		config.set_deadline(Some(Duration::from_secs(100)));
	});

	//The timeouts should be active for both.
	assert!(ccb_recv.with(|x|x.timeout_active));
	assert!(scb_recv.with(|x|x.timeout_active));

	//The initial interest should be transfer from client to server...
	assert_data2(&client, &server, F_CTS);
	transfer_packets_hs3(&mut client, &mut server);
	//Now the interest should have reversed... But TX should not be ready.
	assert_data2(&client, &server, F_STC);
	transfer_packets_hs3(&mut server, &mut client);
	//Now the interests should have swapped again. Server TX should be ready.
	assert_data2(&client, &server, F_CTS);
	transfer_packets_hs3(&mut client, &mut server);
	//Now the interest should have reversed.... Client has become fully ready.
	assert_data2(&client, &server, F_STC | F_CTS_BLOCKED);
	//This server final flight contains application data (X READY\n)!
	let mut receivedx = Vec::with_capacity(20480);
	while server.get_status().is_want_tx() {
		let buffer1 = _do_pull_tcp_data(&mut server, PullTcpBuffer::no_data()).unwrap();
		assert!(client.get_status().is_want_rx());
		let (tmp, eofd) = _do_push_tcp_data(&mut client, PushTcpBuffer::u8(&buffer1)).unwrap();
		receivedx.extend_from_slice(&tmp);
		if eofd { panic!("Where did this EOF come from???"); }
	}
	assert_eq!(&receivedx[..], b"X READY\n");
	//Now the handshake should be complete on client side but not on server side. The server side should
	//indicate raw data wants read, but not write.
	assert_data2(&client, &server, F_CRW | F_CTS_BLOCKED);
	assert!(ccb_recv.with(|x|x.alpn_checked));

	rot13_echo_test_tail(client, server, ccb_recv, scb_recv);
}


#[test]
fn test_tls13_rot13_echo()
{
	let (mut server, scb_recv) = make_server_ctx(|_|{}, |config|{
		config.set_deadline(Some(Duration::from_secs(100)));
	});

	let (mut client, ccb_recv) = make_client_ctx(|client_callbacks|{
		client_callbacks.expect_alpn = Some("test-rot13".to_owned());
	}, |config|{
		config.add_alpn("test-rot13");
		config.set_deadline(Some(Duration::from_secs(100)));
	});

	//The timeouts should be active for both.
	assert!(ccb_recv.with(|x|x.timeout_active));
	assert!(scb_recv.with(|x|x.timeout_active));

	//The initial interest should be transfer from client to server...
	assert_data2(&client, &server, F_CTS);
	transfer_packets_hs3(&mut client, &mut server);
	//Now the interest should have reversed... But TX should not be ready.
	assert_data2(&client, &server, F_STC);
	transfer_packets_hs3(&mut server, &mut client);
	//Now the handshake should be complete on client side but not on server side. The server side should
	//indicate raw data wants read and raw data wants write.
	assert_data2(&client, &server, F_CRW | F_CTS);
	transfer_packets_hs3(&mut client, &mut server);
	assert_data2(&client, &server, F_CRW | F_CTS_BLOCKED | F_STC);
	assert!(ccb_recv.with(|x|x.alpn_checked));

	//The timeouts should now have been cleared for both.
	assert!(!ccb_recv.with(|x|x.timeout_active));
	assert!(!scb_recv.with(|x|x.timeout_active));

	//The server message should read "X READY\n". Afterwards server should not have data to send.
	assert!(server.get_status().is_want_tx());
	let buffer1 = _do_pull_tcp_data(&mut server, PullTcpBuffer::no_data()).unwrap();
	assert!(!server.get_status().is_want_tx());
	receive_tcp_push(&mut client, &buffer1, buffer1.len(), false, b"X READY\n");
	assert_data2(&client, &server, F_CRW | F_CTS_BLOCKED);

	rot13_echo_test_tail(client, server, ccb_recv, scb_recv);
}

#[test]
fn test_stateless_restart()
{
	//Create 2 server connections with the same configuration.
	let (_, certs) = test_certificate_store();
	let mut serverconfig = ServerConfiguration::from(certs.lookup());
	serverconfig.set_stateless_reject(true);
	let (mut server1, _) = make_server_ctx_withcfg(&serverconfig, |_|{});
	let (mut server2, _) = make_server_ctx_withcfg(&serverconfig, |_|{});
	let (mut client, _) = make_client_ctx(|_|{}, |_|{});

	//Transfer data from client to server...
	let buffer = _do_pull_tcp_data(&mut client, PullTcpBuffer::no_data()).unwrap();
	assert!(server1.get_status().is_want_rx());
	let error = _do_push_tcp_data(&mut server1, PushTcpBuffer::u8(&buffer));
	//But oops, it turns out this faults.
	let mut error = match error {
		Ok(_) => panic!("How did this succeed?"),
		Err(error) => match error.is_restart() {
			Some(x) => x.to_owned(),
			None => panic!("Stateless restart is_restart=None")
		}
	};
	assert!(server1.get_status().is_aborted());
	//Prepend record header to the error.
	let elen = error.len();
	error.insert(0, 22);
	error.insert(1, 3);
	error.insert(2, 3);
	error.insert(3, (elen >> 8) as u8);
	error.insert(4, elen as u8);

	//The client should be able to receive the error and process it. Afterwards, the handshake between client
	//and server2 should succeed.
	assert!(client.get_status().is_want_rx());
	let (tmp, eofd) = _do_push_tcp_data(&mut client, PushTcpBuffer::u8(&error)).unwrap();
	let status = client.get_status();
	if tmp.len() > 0 || eofd || status.is_rx_end() { panic!("Where did this appdata come from?"); }
	transfer_packets_hs3(&mut client, &mut server2);
	transfer_packets_hs3(&mut server2, &mut client);
	transfer_packets_hs3(&mut client, &mut server2);
	assert_data2(&client, &server2, F_ALL);
	//Close the connection.
	do_normal_close3(client, server2);
}

#[test]
fn test_raw_handshake()
{
	let (mut server, scb_recv) = make_server_ctx(|_|{}, |config|config.set_raw_handshake(true));
	let (mut client, ccb_recv) = make_client_ctx(|_|{}, |config|config.set_raw_handshake(true));

	transfer_packets_hs3(&mut client, &mut server);
	transfer_packets_hs3(&mut server, &mut client);
	transfer_packets_hs3(&mut client, &mut server);
	assert_raw_handshake_done(&client);
	assert_raw_handshake_done(&server);
	//Does not accept data.
	assert!(_do_pull_tcp_data(&mut client, PullTcpBuffer::write(b"Hi")).unwrap_err().to_string().
		contains("Can not send data in raw handshake mode"));
	assert!(_do_pull_tcp_data(&mut server, PullTcpBuffer::write(b"Hi")).unwrap_err().to_string().
		contains("Can not send data in raw handshake mode"));
	//Is not closeable.
	assert!(_do_pull_tcp_data(&mut client, PullTcpBuffer::no_data().eof()).unwrap_err().to_string().
		contains("Can not send data in raw handshake mode"));
	assert!(_do_pull_tcp_data(&mut server, PullTcpBuffer::no_data().eof()).unwrap_err().to_string().
		contains("Can not send data in raw handshake mode"));
	//The keys should be the same on both sides. And there should be 2 of them.
	let crkeys = ccb_recv.with(|x|x.rkeys.clone());
	let cskeys = ccb_recv.with(|x|x.skeys.clone());
	let srkeys = scb_recv.with(|x|x.rkeys.clone());
	let sskeys = scb_recv.with(|x|x.skeys.clone());
	assert_eq!(crkeys.len(), 2);
	assert_eq!(srkeys.len(), 2);
	assert_eq!(&crkeys[..], &sskeys[..]);
	assert_eq!(&srkeys[..], &cskeys[..]);
	//We can not close this connection gracefully. Just destruct the endpoints.
}

#[test]
fn test_raw_handshake_restart()
{
	let (mut server, scb_recv) = make_server_ctx(|_|{}, |config|config.set_raw_handshake(true));
	let (mut client, ccb_recv) = make_client_ctx(|_|{}, |config|{
		config.set_raw_handshake(true);
		config.set_flags(0, FLAGS0_DEBUG_NO_SHARES);
	});

	//Extra round trip.
	transfer_packets_hs3(&mut client, &mut server);
	transfer_packets_hs3(&mut server, &mut client);
	transfer_packets_hs3(&mut client, &mut server);
	transfer_packets_hs3(&mut server, &mut client);
	transfer_packets_hs3(&mut client, &mut server);
	assert_raw_handshake_done(&client);
	assert_raw_handshake_done(&server);
	//Does not accept data.
	assert!(_do_pull_tcp_data(&mut client, PullTcpBuffer::write(b"Hi")).unwrap_err().to_string().
		contains("Can not send data in raw handshake mode"));
	assert!(_do_pull_tcp_data(&mut server, PullTcpBuffer::write(b"Hi")).unwrap_err().to_string().
		contains("Can not send data in raw handshake mode"));
	//Is not closeable.
	assert!(_do_pull_tcp_data(&mut client, PullTcpBuffer::no_data().eof()).unwrap_err().to_string().
		contains("Can not send data in raw handshake mode"));
	assert!(_do_pull_tcp_data(&mut server, PullTcpBuffer::no_data().eof()).unwrap_err().to_string().
		contains("Can not send data in raw handshake mode"));
	//The keys should be the same on both sides. And there should be 2 of them.
	let crkeys = ccb_recv.with(|x|x.rkeys.clone());
	let cskeys = ccb_recv.with(|x|x.skeys.clone());
	let srkeys = scb_recv.with(|x|x.rkeys.clone());
	let sskeys = scb_recv.with(|x|x.skeys.clone());
	assert_eq!(crkeys.len(), 2);
	assert_eq!(srkeys.len(), 2);
	assert_eq!(&crkeys[..], &sskeys[..]);
	assert_eq!(&srkeys[..], &cskeys[..]);
	//We can not close this connection gracefully. Just destruct the endpoints.
}

#[test]
fn test_raw_handshake_withpad()
{
	let (mut server, scb_recv) = make_server_ctx(|_|{}, |config|config.set_raw_handshake(true));
	let (mut client, ccb_recv) = make_client_ctx(|_|{}, |config|{
		config.set_raw_handshake(true);
		config.set_min_client_hello_size(1024);
	});

	transfer_packets_hs3(&mut client, &mut server);
	transfer_packets_hs3(&mut server, &mut client);
	transfer_packets_hs3(&mut client, &mut server);
	assert_raw_handshake_done(&client);
	assert_raw_handshake_done(&server);
	//Does not accept data.
	assert!(_do_pull_tcp_data(&mut client, PullTcpBuffer::write(b"Hi")).unwrap_err().to_string().
		contains("Can not send data in raw handshake mode"));
	assert!(_do_pull_tcp_data(&mut server, PullTcpBuffer::write(b"Hi")).unwrap_err().to_string().
		contains("Can not send data in raw handshake mode"));
	//Is not closeable.
	assert!(_do_pull_tcp_data(&mut client, PullTcpBuffer::no_data().eof()).unwrap_err().to_string().
		contains("Can not send data in raw handshake mode"));
	assert!(_do_pull_tcp_data(&mut server, PullTcpBuffer::no_data().eof()).unwrap_err().to_string().
		contains("Can not send data in raw handshake mode"));
	//The keys should be the same on both sides. And there should be 2 of them.
	let crkeys = ccb_recv.with(|x|x.rkeys.clone());
	let cskeys = ccb_recv.with(|x|x.skeys.clone());
	let srkeys = scb_recv.with(|x|x.rkeys.clone());
	let sskeys = scb_recv.with(|x|x.skeys.clone());
	assert_eq!(crkeys.len(), 2);
	assert_eq!(srkeys.len(), 2);
	assert_eq!(&crkeys[..], &sskeys[..]);
	assert_eq!(&srkeys[..], &cskeys[..]);
	//We can not close this connection gracefully. Just destruct the endpoints.
}

#[test]
fn test_pad_client_hello()
{
	let (mut client, _) = make_client_ctx(|_|{}, |config|{
		config.set_raw_handshake(true);
		config.set_min_client_hello_size(1024);
	});
	assert!(client.get_status().is_want_tx());
	let buffer = _do_pull_tcp_data(&mut client, PullTcpBuffer::no_data()).unwrap();
	assert!(!client.get_status().is_want_tx());
	assert_eq!(buffer.len(), 1024);
}

#[test]
fn test_pad_client_hello_toobig()
{
	let (mut client, _) = make_client_ctx(|_|{}, |config|{
		config.set_raw_handshake(true);
		config.set_min_client_hello_size(17000);
	});
	assert!(client.get_status().is_want_tx());
	let buffer = _do_pull_tcp_data(&mut client, PullTcpBuffer::no_data()).unwrap();
	assert!(!client.get_status().is_want_tx());
	assert_eq!(buffer.len(), 16380);	//Should be 16376+4.
}

#[test]
fn test_pad_client_hello_bare_over()
{
	let (mut client, _) = make_client_ctx(|_|{}, |config|{
		config.set_raw_handshake(true);
	});
	assert!(client.get_status().is_want_tx());
	let buffer = _do_pull_tcp_data(&mut client, PullTcpBuffer::no_data()).unwrap();
	let defaultsize = buffer.len();
	assert!(!client.get_status().is_want_tx());

	let (mut client, _) = make_client_ctx(|_|{}, |config|{
		config.set_raw_handshake(true);
		config.set_min_client_hello_size(defaultsize);
	});
	assert!(client.get_status().is_want_tx());
	let buffer = _do_pull_tcp_data(&mut client, PullTcpBuffer::no_data()).unwrap();
	assert!(!client.get_status().is_want_tx());
	assert_eq!(buffer.len(), defaultsize);
}

#[test]
fn test_pad_client_hello_bare_under()
{
	let (mut client, _) = make_client_ctx(|_|{}, |config|{
		config.set_raw_handshake(true);
	});
	assert!(client.get_status().is_want_tx());
	let buffer = _do_pull_tcp_data(&mut client, PullTcpBuffer::no_data()).unwrap();
	let defaultsize = buffer.len();
	assert!(!client.get_status().is_want_tx());

	let (mut client, _) = make_client_ctx(|_|{}, |config|{
		config.set_raw_handshake(true);
		config.set_min_client_hello_size(defaultsize+1);
	});
	assert!(client.get_status().is_want_tx());
	let buffer = _do_pull_tcp_data(&mut client, PullTcpBuffer::no_data()).unwrap();
	assert!(!client.get_status().is_want_tx());
	assert_eq!(buffer.len(), defaultsize+4);
}

#[test]
fn test_grphint()
{
	let (mut server, _) = make_server_ctx(|_|{}, |_|{});
	let (mut client, _) = make_client_ctx(|_|{}, |config|{
		config.set_flags(0, FLAGS0_DEBUG_NO_SHARES);
	});
	client.hint_group(23);
	client.hint_group(29);
	client.hint_group(25);

	transfer_packets_hs3(&mut client, &mut server);
	transfer_packets_hs3(&mut server, &mut client);
	transfer_packets_hs3(&mut client, &mut server);
	assert_data2(&client, &server, F_ALL);
}

#[test]
fn test_sensitive_sni()
{
	let mut client = make_client_ctx_exphost("top-secret-host", |_|{}, |config|{
		config.set_raw_handshake(true);
		config.set_suppress_sni(true);
	});
	//Send and capture a message.
	assert!(client.get_status().is_want_tx());
	let buffer = _do_pull_tcp_data(&mut client, PullTcpBuffer::no_data()).unwrap();
	assert!(!client.get_status().is_want_tx());
	for i in 0..buffer.len() {
		assert!(!(&buffer[i..]).starts_with(b"top-secret-host"));
	}
}

fn dumb_server_reply<S:Connection>(server: &mut S, data: Vec<u8>) -> Vec<u8>
{
	//The handshake should not be declared complete.
	let sflags = server.get_status();
	assert!(sflags.is_handshaking());
	assert!(!sflags.is_may_rx() && !sflags.is_rx_end());
	assert!(!sflags.is_may_tx() && !sflags.is_tx_end());
	//The server should always want to read.
	assert!(server.get_status().is_want_rx());
	let (tmp, eofd) = _do_push_tcp_data(server, PushTcpBuffer::u8(&data)).unwrap();
	if tmp.len() > 0 || eofd { panic!("Where did this appdata come from???"); }
	//Send back the reply.
	send_to_vector(server, b"")
}

fn send_to_vector<F:Connection>(from: &mut F, senddata: &[u8]) -> Vec<u8>
{
	if senddata.len() > 0 {
		_do_pull_tcp_data(from, PullTcpBuffer::write(senddata)).unwrap()
	} else {
		_do_pull_tcp_data(from, PullTcpBuffer::no_data()).unwrap()
	}
}

fn receive_from_vector<T:Connection>(to: &mut T, data: Vec<u8>, assert_eof: bool, assert_data: &[u8])
{
	assert!(to.get_status().is_want_rx());
	let (output, eofd) = _do_push_tcp_data(to, PushTcpBuffer::u8(&data)).unwrap();
	assert_eq!(eofd, assert_eof);
	assert_eq!(&output[..], assert_data);
}

fn echo_dumbserver_tail<F:Connection,T:Connection>(mut client: F, mut server: T)
{
	//Send "Hello, World!\n" to the server. It should reply with "Y Uryyb, Jbeyq!\n".
	let buffer = send_to_vector(&mut client, b"Hello, World!\n");
	let buffer = dumb_server_reply(&mut server, buffer);
	receive_from_vector(&mut client, buffer, false, b"Y Uryyb, Jbeyq!\n");
	//Send "Uryyb, Jbeyq!\n" to the server. It should reply with "Y Hello, World!\n".
	let buffer = send_to_vector(&mut client, b"Uryyb, Jbeyq!\n");
	let buffer = dumb_server_reply(&mut server, buffer);
	receive_from_vector(&mut client, buffer, false, b"Y Hello, World!\n");

	close_dumbserver_tail(client, server);
}

fn close_dumbserver_tail<F:Connection,T:Connection>(mut client: F, mut server: T)
{
	//Trigger end-of-stream from client side. Server should also close its connection.
	let buffer = _do_pull_tcp_data(&mut client, PullTcpBuffer::no_data().eof()).unwrap();
	let buffer = dumb_server_reply(&mut server, buffer);
	receive_from_vector(&mut client, buffer, true, &[]);
	//Now client and server should indicate closed.
	assert_close2(&client, C_TX|C_RX|C_NOABORT);
	assert_close2(&server, C_LINK|C_NOABORT);
}

#[test]
fn test_tls12_rot13_echo_dumbserver()
{
	let (mut server, _) = make_server_ctx(|_|{}, |config|{
		config.set_flags(5, 512);
	});

	let (mut client, _) = make_client_ctx(|client_callbacks|{
		client_callbacks.expect_alpn = Some("test-rot13".to_owned());
	}, |config|{
		config.add_alpn("test-rot13");
	});

	//1st client and server flights. No data should be transferred.
	let buffer = send_to_vector(&mut client, b"");
	let buffer = dumb_server_reply(&mut server, buffer);
	receive_from_vector(&mut client, buffer, false, &[]);
	//2nd client and server flights. The server should reply with "X READY\n".
	let buffer = send_to_vector(&mut client, b"");
	let buffer = dumb_server_reply(&mut server, buffer);
	receive_from_vector(&mut client, buffer, false, b"X READY\n");

	echo_dumbserver_tail(client, server);
}

#[test]
fn test_tls13_rot13_echo_dumbserver()
{
	let (mut server, _) = make_server_ctx(|_|{}, |_|{});

	let (mut client, _) = make_client_ctx(|client_callbacks|{
		client_callbacks.expect_alpn = Some("test-rot13".to_owned());
	}, |config|{
		config.add_alpn("test-rot13");
	});

	//1st client and server flights. No data should be transferred.
	let buffer = send_to_vector(&mut client, b"");
	let buffer = dumb_server_reply(&mut server, buffer);
	receive_from_vector(&mut client, buffer, false, &[]);
	//2nd client and server flights. The server should reply with "X READY\n".
	let buffer = send_to_vector(&mut client, b"");
	let buffer = dumb_server_reply(&mut server, buffer);
	receive_from_vector(&mut client, buffer, false, b"X READY\n");

	echo_dumbserver_tail(client, server);
}

#[test]
fn test_tls12_acme_conn2()
{
	let (mut server, _) = make_server_ctx(|_|{}, |config|{
		config.set_flags(5, 512);
		config.create_acme_challenge("foo", "bar.baz");
	});
	let (mut client, _) = make_acme_client_ctx(*b"\xfd\xd4\xc4\x2b\x6b\x1d\xce\xde\x32\x34\x6e\
		\x69\x2f\xba\x53\x11\x27\x9c\xde\xf3\x66\x3a\xb4\xb5\xd0\x68\xd6\xab\x07\x59\x95\x61");

	//1st client and server flights. No data should be transferred.
	let buffer = send_to_vector(&mut client, b"");
	let buffer = dumb_server_reply(&mut server, buffer);
	receive_from_vector(&mut client, buffer, false, &[]);
	//2nd client and server flights. No data should be transferred.
	let buffer = send_to_vector(&mut client, b"");
	let buffer = dumb_server_reply(&mut server, buffer);
	receive_from_vector(&mut client, buffer, false, &[]);
	//3rd client and server flights. These should be EOFs.
	let buffer = send_to_vector(&mut client, b"");
	let buffer = dumb_server_reply(&mut server, buffer);
	receive_from_vector(&mut client, buffer, true, &[]);

	//Client should indicate ACME done, server should indicate close.
	assert_close2(&client, C_LINK|C_NOABORT|C_RX|C_TX);	//FIXME: Why C_RX/C_TX?
	assert_close2(&server, C_LINK|C_NOABORT);
}

#[test]
fn test_tls13_acme_conn2()
{
	let (mut server, _) = make_server_ctx(|_|{}, |config|{
		config.create_acme_challenge("foo", "bar.baz");
	});
	let (mut client, _) = make_acme_client_ctx(*b"\xfd\xd4\xc4\x2b\x6b\x1d\xce\xde\x32\x34\x6e\
		\x69\x2f\xba\x53\x11\x27\x9c\xde\xf3\x66\x3a\xb4\xb5\xd0\x68\xd6\xab\x07\x59\x95\x61");

	//1st client and server flights. No data should be transferred.
	let buffer = send_to_vector(&mut client, b"");
	let buffer = dumb_server_reply(&mut server, buffer);
	receive_from_vector(&mut client, buffer, false, &[]);
	//2nd client and server flights. No data should be transferred.
	let buffer = send_to_vector(&mut client, b"");
	let buffer = dumb_server_reply(&mut server, buffer);
	receive_from_vector(&mut client, buffer, true, &[]);

	//Client should indicate ACME done, server should indicate close.
	assert_close2(&client, C_LINK|C_NOABORT|C_RX|C_TX);	//FIXME: Why C_RX/C_TX?
	assert_close2(&server, C_LINK|C_NOABORT);
}

#[test]
fn test_tls12_acme_conn2_wrong()
{
	let (mut server, _) = make_server_ctx(|_|{}, |config|{
		config.set_flags(5, 512);
		config.create_acme_challenge("foo", "bar.baz");
	});
	let (mut client, _) = make_acme_client_ctx(*b"\xfd\xd4\xc4\x2b\x6b\x1d\xce\xde\x32\x34\x6e\
		\x69\x2f\xba\x53\x11\x27\x9c\xde\xf3\x66\x3a\xb4\xb5\xd0\x68\xd6\xab\x07\x59\x95\x62");

	//1st client and server flights. No data should be transferred.
	let buffer = send_to_vector(&mut client, b"");
	let buffer = dumb_server_reply(&mut server, buffer);
	receive_from_vector(&mut client, buffer, false, &[]);
	//2nd client and server flights. No data should be transferred.
	send_to_vector(&mut client, b"");
	//Client should have failed.
	assert_close2(&client, C_ABORT);
}

#[test]
fn test_tls13_acme_conn2_wrong()
{
	let (mut server, _) = make_server_ctx(|_|{}, |config|{
		config.create_acme_challenge("foo", "bar.baz");
	});
	let (mut client, _) = make_acme_client_ctx(*b"\xfd\xd4\xc4\x2b\x6b\x1d\xce\xde\x32\x34\x6e\
		\x69\x2f\xba\x53\x11\x27\x9c\xde\xf3\x66\x3a\xb4\xb5\xd0\x68\xd6\xab\x07\x59\x95\x62");

	//1st client and server flights. No data should be transferred.
	let buffer = send_to_vector(&mut client, b"");
	let buffer = dumb_server_reply(&mut server, buffer);
	receive_from_vector(&mut client, buffer, false, &[]);
	//2nd client and server flights. No data should be transferred.
	send_to_vector(&mut client, b"");
	//Client should have failed.
	assert_close2(&client, C_ABORT);
}

/*
#[test]
fn test_tls13_acme_conn_fastburst()
{
	let (mut server, mut scb_recv) = make_server_ctx(|_|{}, |config|{
		config.create_acme_challenge("foo", "bar.baz");
	});

	let (mut client, mut ccb_recv) = make_client_ctx(|client_callbacks|{
		client_callbacks.bypass_cert = true;
		client_callbacks.expect_alpn = Some("acme-tls/1".to_owned());
	}, |config|{
		config.add_alpn("acme-tls/1");
	});

	//1st client and server flights. No data should be transferred.
	let buffer = send_to_vector(&mut client, b"");
	let buffer = dumb_server_reply(&mut server, buffer);
	receive_from_vector(&mut client, buffer, false, &[]);
	//2nd client and server flights, with fast close. No data should be transferred and server should echo the
	//close.
	let mut buffer = send_to_vector(&mut client, b"");
	let mut buffer1 = [0;250];
	let (_, outsize1) =  deprecated_do_write_eos(&mut client, &mut buffer1[..]).unwrap();
	buffer.extend_from_slice(&buffer1[..outsize1]);
	let buffer = dumb_server_reply(&mut server, buffer);
	receive_from_vector(&mut client, buffer, true, &[]);
	//Now client and server should indicate closed.
	assert_close2(&client, C_TX|C_RX|C_NOABORT);
	assert_close2(&server, C_LINK|C_NOABORT);
}
*/

#[test]
fn tls_prf_works_long()
{
	let mut out1 = [0;48];
	let mut out2 = [0;48];
	let key = [1;521];
	let sha256 = Sha256::function();
	sha256.tls_prf(&key, "foo", |x|{x.append(b"test");}, &mut out1);
	Sha256::tls_prf(&key, "foo", |x|{x.append(b"test");}, &mut out2);
	assert_eq!(&out1[..], &out2[..]);
}

#[test]
fn tls_prf_works()
{
	let mut out1 = [0;48];
	let mut out2 = [0;48];
	let key = b"Keyboard";
	let sha256 = Sha256::function();
	sha256.tls_prf(key, "foo", |x|{x.append(b"test");}, &mut out1);
	Sha256::tls_prf(key, "foo", |x|{x.append(b"test");}, &mut out2);
	assert_eq!(&out1[..], &out2[..]);
}

#[test]
fn tls_prf_precompress_works()
{
	let mut out1 = [0;48];
	let mut out2 = [0;48];
	let key = [1;521];
	let key2 = Sha256::hmac_precompress_key(&key);
	Sha256::tls_prf(&key, "foo", |x|{x.append(b"test");}, &mut out1);
	Sha256::tls_prf(&key2, "foo", |x|{x.append(b"test");}, &mut out2);
	assert_eq!(&out1[..], &out2[..]);
}

#[test]
fn tls_prf_precompress_should_not_trigger()
{
	let mut out1 = [0;48];
	let mut out2 = [0;48];
	let key = [42;17];
	let key2 = Sha256::hmac_precompress_key(&key);
	assert_eq!(&key[..17], &key2[..17]);
	Sha256::tls_prf(&key, "foo", |x|{x.append(b"test");}, &mut out1);
	Sha256::tls_prf(&key2, "foo", |x|{x.append(b"test");}, &mut out2);
	assert_eq!(&out1[..], &out2[..]);
}
