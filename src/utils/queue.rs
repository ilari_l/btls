#![allow(unsafe_code)]
use crate::stdlib::Display;
use crate::stdlib::FmtError;
use crate::stdlib::Formatter;
use crate::stdlib::IoError;
use crate::stdlib::IoErrorKind;
use crate::stdlib::IoWrite;
use crate::stdlib::Vec;
use btls_aux_fail::ResultExt;
use btls_aux_memory::BackedVector;
use btls_aux_memory::split_at;
use core::cmp::min;
use core::ops::DerefMut;
use core::ptr::copy;

//EOF has been written.
const FLAG_EOF: u8 = 1;
//There is a pending write.
const FLAG_PENDING: u8 = 2;
//EOF has been ACKed.
const FLAG_EOF_ACK: u8 = 4;

///A byte queue sink.
pub trait QueueSink
{
	///Write buffers into sink.
	///
	///If scatter-gather is not supported, `buf2` can be ignored.
	///
	///Returns an I/O error or number of bytes written.
	fn write(&mut self, buf: &[u8], buf2: &[u8]) -> Result<usize, IoError>;
	///Close sink for writes.
	///
	///No further writes will occur. It is assumed this can not fail.
	fn close(&mut self);
}

///A byte queue source.
pub trait QueueSource
{
	///Read buffer from sink.
	///
	///If scatter-gather is not supported, `buf2` can be ignored.
	///
	///Returns an I/O error, Ok(None) to signal no further data, or number of bytes read.
	fn read(&mut self, buf: &mut [u8], buf2: &mut [u8]) -> Result<Option<usize>, IoError>;
}

///A byte queue.
///
///The queue functions as an FIFO (First In, First Out) at byte granularity. It can also be closed, in which case
///the close is signaled into read end.
#[derive(Debug)]
pub struct Queue
{
	//The queue backing data.
	backing: Vec<u8>,
	//The queue read pointer.
	///
	///Invariant: read <= backing.len(). Equality impiles buffer is empty and write == 0.
	read: usize,
	//The queue write pointer.
	///
	///Invariant: write < backing.len(), except if both are zero.
	write: usize,
	//Flags.
	flags: u8,
	//Size of pending write.
	pending_size: usize,
}

///Error from queue read operation.
///
///This enumeration models possible errors from queue read operation.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
#[non_exhaustive]
pub enum QueueReadError
{
	///Queue has been EOF'd.
	///
	///This error is given if one tries to use `read()` or `discard()` operations on a closed queue with no data
	///in it.
	Eof,
	///Amount to discard too big.
	///
	///This error is given if one tries to use `discard()` operation on queue, specifying to discard more data
	///than there is in the queue. The argument is the number of bytes attempted to discard.
	NotEnoughData(usize),
}

impl Display for QueueReadError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::QueueReadError::*;
		match self {
			&Eof => fmt.write_str("Buffer already at EOF"),
			&NotEnoughData(remain) => write!(fmt, "Amount to discard too big (remain={remain})"),
		}
	}
}

///Error from queue write operation.
///
///This enumeration models possible errors from queue write operation.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
#[non_exhaustive]
pub enum QueueWriteError
{
	///Queue has been EOF'd.
	///
	///This error is given if one tries to write more data into already closed queue.
	Eof,
	///Queue would become too big.
	///
	///This error is given if the queue would become too big as result of write operation. This can not actually
	///happen, because system will run out of memory first.
	TooBig,
}

impl Display for QueueWriteError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::QueueWriteError::*;
		match self {
			&Eof => fmt.write_str("Queue already EOF'd"),
			&TooBig => fmt.write_str("Queue would become too big"),
		}
	}
}

fn round_up_to_power_of_2(x: usize) -> usize
{
	let mut out = 1;
	while out < x {
		out = out << 1;
		if out == 0 { out = out.wrapping_sub(1); }
	}
	out
}

unsafe trait MaybeU8 {}
unsafe impl MaybeU8 for u8 {}
unsafe impl MaybeU8 for core::mem::MaybeUninit<u8> {}

fn copy_maximum<T:MaybeU8>(to: &mut [T], from: &[u8]) -> usize
{
	let amt = min(to.len(), from.len());
	unsafe {
		core::ptr::copy_nonoverlapping(from.as_ptr(), to.as_mut_ptr().cast(), amt);
	}
	amt
}

fn __bad_data_pointer() -> IoError
{
	IoError::new(IoErrorKind::Other, "BUG: Bad data pointer")
}

impl Queue
{
	#[inline(always)]
	fn __check_invariants(&self)
	{
		debug_assert!(self.write == 0 && self.backing.len() == 0 || self.write < self.backing.len());
		if self.read >= self.backing.len() {
			debug_assert!(self.read == self.backing.len() && self.write == 0);
		}
		if self.flags & FLAG_PENDING != 0 {
			//Check that there is sufficient space for the pending write.
			if self.write <= self.read && self.read < self.backing.len() {
				//println!("Debug: {self:?}");
				debug_assert!(self.read - self.write >= self.pending_size);
			} else {
				debug_assert!(self.backing.len() - self.write >= self.pending_size);
			}
		}
	}
	fn __peek_common<T:MaybeU8>(&self, buf: &mut [T]) -> Option<usize>
	{
		self.__check_invariants();
		//If buffer is empty, it either gives 0 bytes or EOF depending on if EOF is asserted.
		if self.read >= self.backing.len() {
			return if self.flags & FLAG_EOF != 0 { None} else { Some(0) };
		}
		if self.write <= self.read {
			//If write pointer is before or at read pointer and !FLAG_EMPTY, then buffer is in two
			//parts: The first part is read.. and the second is ..write. Both should be valid indices
			//to split on.
			let part1 = self.backing.get(self.read..)?;
			let mut fill = copy_maximum(buf, part1);
			if fill < buf.len() {
				//Continue with part2. The addition can not overflow, as it always at most original
				//buf.len().
				let buf = &mut buf[fill..];
				let part2 = self.backing.get(..self.write)?;
				fill += copy_maximum(buf, part2);
			}
			Some(fill)
		} else {
			//If write pointer is after read pointer, the data is in one part read..write, which should
			//be a valid range.
			let data = self.backing.get(self.read..self.write)?;
			Some(copy_maximum(buf, data))
		}
	}
	///Create a new queue.
	///
	///Initially the queue is empty and not closed.
	///
	///Returns the new queue.
	pub fn new() -> Queue
	{
		let r = Queue{
			backing: Vec::new(),
			read: 0,
			write: 0,
			flags: 0,
			pending_size: 0,
		};
		r.__check_invariants();
		r
	}
	///Read from queue.
	///
	///Reads the maximum possible amount of data from the queue, and returns the amount read. The read data is
	///remove from the queue and written into `buf` If one does not want to remove the read data, use the
	///[`peek()`](#method.peek) method.
	///
	///If the queue is not closed yet or has data in it, returns `Some(produced)`, where `produced` is the number
	///of bytes read (always the lesser of `buf.len()` and amount of data in queue). Note that this can be `0`.
	///If queue is empty and closed, returns `None`.
	pub fn read(&mut self, buf: &mut [u8]) -> Option<usize>
	{
		let amt = self.peek(buf)?;
		self.discard(amt).map(|_|amt).ok()
	}
	///Like `peek()` but peeks into uninitialized buffer, and returns the initialized part of buffer instead of
	///buffer size.
	pub fn peek_uninit<'a>(&self, buf: &'a mut [core::mem::MaybeUninit<u8>]) -> Option<&'a [u8]>
	{
		let mut buf = BackedVector::new(buf);
		let fill = self.__peek_common(buf.unfilled_slice())?;
		unsafe {
			//__peek_common() initializes fill elements.
			buf.assume_init(fill);
		}
		Some(buf.into_inner())
	}
	///Peek a queue.
	///
	///Peeks the maximum possible amount of data from the queue, and returns the amount peeked. The peeked data
	///is not removed from the queue, but is still stored in `buf`. One can then remove specified amount of data
	///using the [`discard()`](#method.discard) method.
	///
	///If the queue is not closed yet or has data in it, returns `Some(produced)`, where `produced` is the number
	///of bytes peeked (always the lesser of `buf.len()` and amount of data in queue). Note that this can be `0`.
	///If queue is empty and closed, returns `None`.
	///
	///The only difference from the [`read()`](#method.read) method is that the data read is not removed from
	///the queue.
	pub fn peek(&self, buf: &mut [u8]) -> Option<usize>
	{
		self.__peek_common(buf)
	}
	///Discard `amount` number of bytes from queue.
	///
	///The discarded bytes are lost, not being stored anywhere.
	///
	///If successful, returns `Ok(())`. If the queue has no data and is closed, returns
	///`Err(QueueReadError::Eof)`. If the queue does not have requested number of bytes, returns
	///`Err(QueueReadError::NotEnoughData(amount))`.
	pub fn discard(&mut self, amount: usize) -> Result<(), QueueReadError>
	{
		use self::QueueReadError::*;
		self.__check_invariants();
		//If buffer is empty, either only 0 bytes is discardable or it gives EOF.
		if self.read >= self.backing.len() {
			fail_if!(self.flags & FLAG_EOF != 0, Eof);
			fail_if!(amount > 0, NotEnoughData(amount));
		}
		//Handle degenerate case amout=0, which guarantees that if read==write after adjusting read, the
		//buffer has become empty.
		if amount == 0 { return Ok(()); }
		if self.write <= self.read {
			//If write pointer is before or at read pointer and !FLAG_EMPTY, then buffer is in two
			//parts: The first part is read.. and the second is ..write. Compute the lengths of the
			//parts.
			let available1 = self.backing.len() - self.read;
			if amount < available1 {
				//The first part is shrunk, does not disappear.
				self.read += amount;
			} else {
				//Calculate total data available. It is sum of parts, the second of which is
				//self.write in size.
				let available = available1 + self.write;
				fail_if!(available < amount, NotEnoughData(amount));
				//The first part will disappear.
				self.read = amount - available1;
			}
		} else {
			//If write pointer is after read pointer, the data is in one part read..write, which should
			//be a valid range.
			fail_if!(self.write - self.read < amount, NotEnoughData(amount));
			self.read += amount;
		}
		//If queue is emptied, reposition it back to zero indices, as this is cheap.
		self.flags &= !FLAG_PENDING;
		if self.read == self.write {
			self.read = self.backing.len();
			self.write = 0;
		}
		self.__check_invariants();
		Ok(())
	}
	///Return first part of readable data as slice.
	///
	///This does not discard the data, and might not return all the data available. However, if there is data,
	///at least 1 byte of it will always be returned.
	pub fn map_read<'a>(&'a self) -> &'a [u8]
	{
		self.__check_invariants();
		//If buffer is empty, return empty slice.
		if self.read >= self.backing.len() { return &[]; }
		if self.write <= self.read && self.read < self.backing.len() {
			//If write pointer is before or at read pointer and !FLAG_EMPTY, then buffer is in two
			//parts: The first part is read.. and the second is ..write. Both should be valid indices
			//to split on. Return the first part.
			&self.backing[self.read..]
		} else {
			//If write pointer is after read pointer, the data is in one part read..write, which should
			//be a valid range. However, it might be that the buffer is also empty.
			self.backing.get(self.read..self.write).unwrap_or(&[])
		}
	}
	///Allocate continuous space for write of size `writesize`.
	///
	///To actually commit the write, call `commit_write()`.
	///
	///This is posssible even if queue has been closed.
	///
	///On success, returns the buffer to write to. If the queue would grow too big (this never happens), returns
	///`Err(QueueWriteError::TooBig)`.
	pub fn prepare_write<'a>(&'a mut self, writesize: usize) -> Result<&'a mut [u8], QueueWriteError>
	{
		self.__check_invariants();
		if writesize > 0 { self.__make_space_for_buffer(writesize, true)?; }
		self.flags |= FLAG_PENDING;
		self.pending_size = writesize;
		self.__check_invariants();
		//This can never actually fail.
		let start = self.write;
		let end = self.write + writesize;
		let output = self.backing.get_mut(start..end).ok_or(QueueWriteError::TooBig)?;
		Ok(output)
	}
	///Commit write of given size.
	///
	///If queue has been closed and the write size is positive, fails.
	///
	///If the size is larger than pending write, returns `Err(QueueWriteError::TooBig)`.
	pub fn commit_write(&mut self, writesize: usize) -> Result<(), QueueWriteError>
	{
		self.__check_invariants();
		fail_if!(writesize > 0 && self.flags & FLAG_EOF != 0, QueueWriteError::Eof);
		fail_if!(self.flags & FLAG_PENDING == 0, QueueWriteError::TooBig);
		fail_if!(self.pending_size < writesize, QueueWriteError::TooBig);
		//writesize==0 is degenerate case. Handle it to unconditionally clear FLAG_EMPTY.
		if writesize == 0 {
			self.flags &= !FLAG_PENDING;
			return Ok(());
		}
		//FLAG_EMPTY is cleared as there will be data after this. FLAG_PENDING is cleared as commit is
		//done.
		if self.read >= self.backing.len() { self.read = 0; }
		self.write += writesize;
		self.flags &= !FLAG_PENDING;
		//The above may take self.write to self.backing.len(). In that case, wrap the write pointer back
		//to zero.
		if self.write >= self.backing.len() { self.write -= self.backing.len(); }
		self.__check_invariants();
		Ok(())
	}
	//Assumes buflen is nonzero.
	fn __make_space_for_buffer(&mut self, buflen: usize, single: bool) -> Result<(), QueueWriteError>
	{
		if self.write <= self.read && self.read < self.backing.len() {
			let available = self.read - self.write;
			//If there is enough space available, do not need to do anything.
			if available >= buflen { return Ok(()); }
			let need = self.backing.len().checked_add(buflen - available).
				ok_or(QueueWriteError::TooBig)?;
			let need = round_up_to_power_of_2(need);
			let len1 = self.backing.len() - self.read;
			let len2 = self.write;
			//Even if with standard backing array lengths it is not possible to fail this
			//condition, it can fail if some clown uses nonstandard backing lengths.
			//E.g.:
			//self.backing.len() = 1920
			//buflen = 1028
			//self.read = 1400
			//self.write = 500
			if len2 < len1 && !single && need - self.backing.len() >= len2 {
				//The second part is smaller: It is cheaper to copy the second part to be after the
				//first part. Do not do this with single part, as this may split the buffer.
				let oldsize = self.backing.len();
				self.backing.resize(need, 0);
				//1) Source has len2 readable bytes.
				//2) target as need - oldsize >= len2 writable bytes.
				unsafe{
					let ptr = self.backing.as_mut_ptr();
					copy(ptr, ptr.add(oldsize), len2);
				}
				self.write = oldsize + len2;
				//It is possible that write == need. Needs wrapping in that case.
				if self.write >= self.backing.len() { self.write -= self.backing.len(); }
				return Ok(());
			}
			//Either the first part is smaller, or some clown called us so that the second part does
			//not fit after the first. Move the first part to end of enlarged buffer.
			let oldsize = self.backing.len();
			//need is always larger than the existing size.
			self.backing.resize(need, 0);
			let newsize = self.backing.len();
			//Copy read..oldsize into x..newsize.
			let taillen = oldsize - self.read;
			let newread = newsize - taillen;
			let ptr = self.backing.as_mut_ptr();
			//1) source has newsize - read >= oldsize - read == taillen elements readable.
			//2) target has newsize - (newsize - taillen) = taillen elements writable.
			unsafe{copy(ptr.add(self.read), ptr.add(newread), taillen);}
			self.read = newread;
		} else if self.read >= self.backing.len() {
			//The existing buffer is empty! Just need big enough buffer. This also works in single
			//mode.
			if buflen > self.backing.len() {
				let need = round_up_to_power_of_2(buflen);
				self.backing.resize(need, 0);
				self.read = self.backing.len();
			}
		} else {
			//Buffer is in 1 part, but there are potentially 2 parts to write to. Only the first is
			//usable in single mode.
			let len1 = self.backing.len() - self.write;
			let len2 = if single { 0 } else { self.read };
			if buflen >= len1 + len2 {
				//The amount buf.len() need exceeds available is number of extra bytes needed.
				let need = self.backing.len().checked_add(buflen - len1 - len2).
					ok_or(QueueWriteError::TooBig)?;
				let need = round_up_to_power_of_2(need);
				//This is always larger than previous.
				self.backing.resize(need, 0);
			}
		}
		Ok(())
	}
	///Write data `buf` into queue.
	///
	///The data is always completely written on success, partial write is not possible.
	///
	///On success, returns `Ok(buf.len())`. If the queue has been closed, returns `Err(QueueWriteError::Eof)`.
	///If the queue would grow too big (this never happens), returns `Err(QueueWriteError::TooBig)`.
	pub fn write(&mut self, buf: &[u8]) -> Result<usize, QueueWriteError>
	{
		self.writev(&[buf])
	}
	///Like `write()`, but scatter-gather version.
	pub fn writev(&mut self, buf: &[&[u8]]) -> Result<usize, QueueWriteError>
	{
		self.__check_invariants();
		fail_if!(self.flags & FLAG_EOF != 0, QueueWriteError::Eof);
		self.flags &= !FLAG_PENDING;
		let total_len = buf.iter().fold(0usize,|x,y|x.saturating_add(y.len()));
		if total_len == 0 { return Ok(0); }
		self.__make_space_for_buffer(total_len, false)?;
		self.__check_invariants();

		for &part in buf.iter() {
			if part.len() == 0 { continue; }	//Skip empty parts.
			//By postconditions of __make_space_for_buffer, we may assume we can just write the stuff
			//without ever hitting pre-existing data. However, the writes may need to be split.
			let remaining = self.backing.len() - self.write;
			let (tpart1, tpart2) = split_at(self.backing.deref_mut(), self.write).
				set_err(QueueWriteError::TooBig)?;
			if remaining >= part.len() {
				//Fits in one part.
				copy_maximum(tpart2, part);
				self.write += part.len();
				if self.write >= self.backing.len() { self.write -= self.backing.len(); }
			} else {
				//This needs two parts. The split_at being in range follows from not going to the
				//above branch.
				let (part1, part2) = part.split_at(remaining);
				copy_maximum(tpart2, part1);
				self.write = copy_maximum(tpart1, part2);
			}
		}
		//Buffer has become non-empty.
		if self.read >= self.backing.len() { self.read = 0; }
		self.__check_invariants();
		Ok(buf.len())
	}
	///Transfer all data from queue to queue.
	///
	///This corresponds to reading this queue and writing all data to the other queue. Followed by transferring
	///the EOF marker if any.
	///
	///Fails if the other queue has already been closed and there is any data to transfer.
	///
	///Returns number of bytes transferred.
	pub fn transfer(&mut self, other: &mut Self) -> Result<usize, QueueWriteError>
	{
		self.__check_invariants();
		other.__check_invariants();
		//The empty case.
		if self.read >= self.backing.len() {
			if self.eof_sent() { other.send_eof(); }
			return Ok(0);
		}
		fail_if!(self.flags & FLAG_EOF != 0, QueueWriteError::Eof);
		let to_transfer = self.buffered();
		other.__make_space_for_buffer(to_transfer, false)?;
		//Use map_read and write for fast transfer. The transfer can require two parts. Assume that the
		//writes and discards can not fail.
		let buf = self.map_read();
		let len = buf.len();
		other.write(buf).ok();
		self.discard(len).ok();
		let buf = self.map_read();
		let len = buf.len();
		other.write(buf).ok();
		self.discard(len).ok();
		//Transfer the EOF flag.
		if self.eof_sent() { other.send_eof(); }
		self.__check_invariants();
		other.__check_invariants();
		Ok(to_transfer)
	}
	///Call write method of file-like object `output` with data.
	///
	///Call `write()` method of `std::io::Write` object `output` using data in the queue. The amount written is
	///the largest possible linear fragment. There is also no guarantee `write()` writes all data. The written
	///data is removed from the queue.
	///
	///Note that this function ignores close status: it does not close the target upon draining all data on
	///closed queue. So if the queue is closed and empty after this call, the output needs to be separatedly
	///closed.
	///
	///On success, returns `Ok(n)`, where `n` is the number of bytes written. On write error, returns
	///`Err(x)`. where `x` is the error. These errors can only be raised by the underlying `write()` method.
	pub fn call_write<W:IoWrite>(&mut self, output: &mut W) -> Result<usize, IoError>
	{
		self.__check_invariants();
		//Empty buffer always writes 0 bytes.
		if self.read >= self.backing.len() { return Ok(0); }
		//Only take one segment, even if there are two.
		let data = if self.write <= self.read {
			//This should always be valid
			self.backing.get(self.read..)
		} else {
			//This should always be valid
			self.backing.get(self.read..self.write)
		}.ok_or_else(__bad_data_pointer)?;
		//This can not overflow because buffer is in one part. However, it might wrap around.
		let r = output.write(data)?;
		self.read += r;
		if self.read >= self.backing.len() { self.read -= self.backing.len(); }
		self.flags &= !FLAG_PENDING;
		if r > 0 && self.read == self.write {
			//Buffer emptied.
			self.read = self.backing.len();
			self.write = 0;
		}
		self.__check_invariants();
		Ok(r)
	}
	fn __handle_read_return(&mut self, ret: Option<usize>) -> Result<bool, IoError>
	{
		let oversize = ||-> IoError { IoError::new(IoErrorKind::Other, "Oversize read") };
		match ret {
			Some(0) => Ok(false),
			None => { self.send_eof(); Ok(true) },
			Some(r) if self.write <= self.read && self.read < self.backing.len() => {
				fail_if!(self.read - self.write < r, oversize());
				//Can not wrap around, since this is at most self.read, which is in range.
				self.write += r;
				Ok(true)
			},
			Some(r) if self.read >= self.backing.len() => {
				self.read = 0;		//No longer empty.
				fail_if!(self.backing.len() < r, oversize());
				self.write = r;
				if self.write >= self.backing.len() { self.write -= self.backing.len(); }
				Ok(true)
			},
			Some(r) => {
				let len1 = self.backing.len() - self.write;
				let len2 = self.read;
				fail_if!(len1 + len2 < r, oversize());
				self.write = if r < len1 { self.write + r } else { r - len1 };
				Ok(true)
			}
		}
	}
	///Source data from queue source.
	///
	///Read is called exactly once.
	pub fn source(&mut self, source: &mut dyn QueueSource, minmax: usize) -> Result<bool, IoError>
	{
		//Do not hit degenerate cases with zero space.
		let minmax = core::cmp::max(minmax, 1);
		self.__make_space_for_buffer(minmax, false).map_err(|_|{
			IoError::new(IoErrorKind::Other, "Queue grew too big")
		})?;
		let r = if self.write <= self.read && self.read < self.backing.len() {
			//This is valid by the conditions above.
			let part1 = &mut self.backing[self.write..self.read];
			source.read(part1, &mut [])
		} else if self.read >= self.backing.len() {
			let part1 = &mut self.backing[..];
			source.read(part1, &mut [])
		} else {
			//if self.read > self.backing.len(), above branch would be taken.
			let (part2, part1) = self.backing.split_at_mut(self.read);
			let part2 = part2.get_mut(..self.write).ok_or_else(__bad_data_pointer)?;
			source.read(part1, part2)
		};
		self.__handle_read_return(r?)
	}
	fn __handle_write_return(&mut self, ret: usize, sink: &mut dyn QueueSink) -> Result<bool, IoError>
	{
		let oversize = ||-> IoError { IoError::new(IoErrorKind::Other, "Oversize write") };
		if ret == 0 { return Ok(false); }
		//Just discard the written data. If this fails, return error.
		self.discard(ret).map_err(|_|oversize())?;
		if self.read >= self.backing.len() && self.flags & (FLAG_EOF|FLAG_EOF_ACK) == FLAG_EOF {
			//Close the sink.
			sink.close();
			self.flags |= FLAG_EOF_ACK;
		}
		Ok(true)
	}
	///Sink data into queue sink.
	///
	///Write is called at most once. If there will not be more data, close is also called. However, close is
	///only called once.
	pub fn sink(&mut self, sink: &mut dyn QueueSink) -> Result<bool, IoError>
	{
		if self.read >= self.backing.len() {
			//The queue is empty. If not yet closed, do close.
			return Ok(if self.flags & (FLAG_EOF|FLAG_EOF_ACK) == FLAG_EOF {
				sink.close();
				self.flags |= FLAG_EOF_ACK;
				true
			} else {
				false
			});
		}
		let r = if self.write <= self.read {
			//If write pointer is before or at read pointer and !FLAG_EMPTY, then buffer is in two
			//parts: The first part is read.. and the second is ..write. Both should be valid indices
			//to split on.
			let part1 = self.backing.get(self.read..).ok_or_else(__bad_data_pointer)?;
			let part2 = self.backing.get(..self.write).ok_or_else(__bad_data_pointer)?;
			sink.write(part1, part2)
		} else {
			//If write pointer is after read pointer, the data is in one part read..write, which should
			//be a valid range.
			let data = self.backing.get(self.read..self.write).ok_or_else(__bad_data_pointer)?;
			sink.write(data, &[])
		};
		self.__handle_write_return(r?, sink)
	}
	///Get amount of data in bytes in the buffer.
	pub fn buffered(&self) -> usize
	{
		self.__check_invariants();
		if self.write <= self.read && self.read < self.backing.len() {
			//2 parts.
			self.backing.len() - self.read + self.write
		} else if self.read >= self.backing.len() {
			//Empty buffer.
			0
		} else {
			//1 part.
			self.write - self.read
		}
	}
	///Abort a queue.
	///
	///Aborting the queue causes queue to be closed and all data discarded. This is mostly useful in the case an
	///error occurs and queue contents are no longer required.
	pub fn abort(&mut self)
	{
		self.__check_invariants();
		self.flags = FLAG_EOF;
		self.backing.clear();
		self.read = 0;
		self.write = 0;
		self.__check_invariants();
	}
	///Close a queue.
	///
	///After being closed, no data can be written into the queue.
	pub fn send_eof(&mut self) { self.__check_invariants(); self.flags |= FLAG_EOF; }
	///Check if queue closed.
	///
	///Note that there still can be data in the closed queue.
	pub fn eof_sent(&self) -> bool { self.__check_invariants(); self.flags & FLAG_EOF != 0 }
	///Check if there can be no more output.
	///
	///This method unlike [`eof_sent()`](#method.eof_sent) also checks that the queue is empty. So if this method
	///returns `true`, there can be no more data read from the queue.
	///
	///Returns `true` if the queue can never yield any more data (it is both closed and empty), otherwise
	///`false`.
	pub fn no_more_output(&self) -> bool
	{
		self.__check_invariants();
		self.flags & FLAG_EOF != 0 && self.read >= self.backing.len()
	}
	///Is empty?
	pub fn is_empty(&self) -> bool { self.read >= self.backing.len() }
}

#[cfg(test)]
mod test;
