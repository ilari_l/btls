#![allow(missing_docs)]		//This stuff has fair amount of hidden re-exports for testing.

pub use self::hsbuffer::*;
pub use self::queue::*;
use crate::stdlib::transmute;
pub use btls_aux_hash::slice_eq_ct;
use core::mem::MaybeUninit;


mod hsbuffer;
mod queue;


pub(crate) fn to_mu8<'a>(x: &'a [u8]) -> &'a [MaybeUninit<u8>] { unsafe{transmute(x)} }
pub(crate) unsafe fn to_mu8_mut<'a>(x: &'a mut [u8]) -> &'a mut [MaybeUninit<u8>] { unsafe{transmute(x)} }
pub(crate) fn to_mu8_slice<'a,'b>(x: &'a [&'b [u8]]) -> &'a [&'b [MaybeUninit<u8>]] { unsafe{transmute(x)} }
