use super::*;

static GIBBERISH: &'static [u8] = include_bytes!("gibberish.bin");

fn prepare_1part_test() -> Queue
{
	use std::iter::repeat;
	let mut queue = Queue {
		backing: repeat(0u8).take(128).collect(),
		read: 24,
		write: 56,
		flags: 0,
		pending_size: 0,
	};
	(&mut queue.backing[24..56]).copy_from_slice(&GIBBERISH[..32]);
	queue
}

fn prepare_2part_test() -> Queue
{
	use std::iter::repeat;
	let mut queue = Queue {
		backing: repeat(0u8).take(128).collect(),
		read: 96,
		write: 32,
		flags: 0,
		pending_size: 0,
	};
	(&mut queue.backing[96..128]).copy_from_slice(&GIBBERISH[..32]);
	(&mut queue.backing[0..32]).copy_from_slice(&GIBBERISH[32..64]);
	queue
}

#[test]
fn map_read_1part()
{
	let queue = prepare_1part_test();
	assert_eq!(queue.map_read(), &GIBBERISH[..32]);
}

#[test]
fn map_read_2part()
{
	let queue = prepare_2part_test();
	assert_eq!(queue.map_read(), &GIBBERISH[..32]);
}

#[test]
fn map_read_empty()
{
	let queue = Queue::new();
	assert_eq!(queue.map_read(), &[][..]);
}

#[test]
fn map_read_whole()
{
	let mut queue = Queue::new();
	queue.write(&GIBBERISH[..1024]).expect("Write should succeed");
	assert_eq!(queue.map_read(), &GIBBERISH[..1024]);
}

#[test]
fn read_1part_1under()
{
	let mut queue = prepare_1part_test();
	let mut buf = [0;31];
	assert!(queue.read(&mut buf).expect("Should succeed") == 31);
	assert!(queue.read == 55);
	assert!(&buf[..] == &GIBBERISH[..31]);
}

#[test]
fn read_1part_exact()
{
	let mut queue = prepare_1part_test();
	let mut buf = [0;32];
	assert!(queue.read(&mut buf).expect("Should succeed") == 32);
	assert!(queue.read == 128);
	assert!(&buf[..] == &GIBBERISH[..32]);
}

#[test]
fn read_1part_1over()
{
	let mut queue = prepare_1part_test();
	let mut buf = [0;33];
	assert!(queue.read(&mut buf).expect("Should succeed") == 32);
	assert!(queue.read == 128);
	assert!(&buf[..32] == &GIBBERISH[..32]);
}

#[test]
fn read_2part_split_1under()
{
	let mut queue = prepare_2part_test();
	let mut buf = [0;31];
	assert!(queue.read(&mut buf).expect("Should succeed") == 31);
	assert!(queue.read == 127);
	assert!(&buf[..] == &GIBBERISH[..31]);
}

#[test]
fn read_2part_split_exact()
{
	let mut queue = prepare_2part_test();
	let mut buf = [0;32];
	assert!(queue.read(&mut buf).expect("Should succeed") == 32);
	assert!(queue.read == 0);
	assert!(&buf[..] == &GIBBERISH[..32]);
}

#[test]
fn read_2part_split_1over()
{
	let mut queue = prepare_2part_test();
	let mut buf = [0;33];
	assert!(queue.read(&mut buf).expect("Should succeed") == 33);
	assert!(queue.read == 1);
	assert!(&buf[..] == &GIBBERISH[..33]);
}

#[test]
fn read_2part_1under()
{
	let mut queue = prepare_2part_test();
	let mut buf = [0;63];
	assert!(queue.read(&mut buf).expect("Should succeed") == 63);
	assert!(queue.read == 31);
	assert!(&buf[..] == &GIBBERISH[..63]);
}

#[test]
fn read_2part_exact()
{
	let mut queue = prepare_2part_test();
	let mut buf = [0;64];
	assert!(queue.read(&mut buf).expect("Should succeed") == 64);
	assert!(queue.read == 128);
	assert!(&buf[..] == &GIBBERISH[..64]);
}

#[test]
fn read_2part_1over()
{
	let mut queue = prepare_2part_test();
	let mut buf = [0;65];
	assert!(queue.read(&mut buf).expect("Should succeed") == 64);
	assert!(queue.read == 128);
	assert!(&buf[..64] == &GIBBERISH[..64]);
}

#[test]
fn discard_empty()
{
	let mut queue = Queue::new();
	queue.discard(0).expect("Should suceed");
	assert!(queue.discard(1).expect_err("Should fail") == QueueReadError::NotEnoughData(1));
	queue.send_eof();
	assert!(queue.discard(0).expect_err("Should fail") == QueueReadError::Eof);
}

#[test]
fn discard_1part_1over()
{
	let mut queue = prepare_1part_test();
	assert!(queue.discard(33).expect_err("Should fail") == QueueReadError::NotEnoughData(33));
	assert!(queue.read == 24);
	queue.discard(32).expect("Should suceed");
	assert!(queue.read == 128);
}

#[test]
fn discard_2part_1over()
{
	let mut queue = prepare_2part_test();
	assert!(queue.discard(65).expect_err("Should fail") == QueueReadError::NotEnoughData(65));
	assert!(queue.read == 96);
	queue.discard(64).expect("Should suceed");
	assert!(queue.read == 128);
}

#[test]
fn twostep_write_empty()
{
	let mut queue = Queue::new();
	let buf = queue.prepare_write(32).expect("Should succeed");
	buf.copy_from_slice(&GIBBERISH[..32]);
	queue.commit_write(32).expect("Should suceed");
	assert!(queue.commit_write(32).expect_err("Should fail") == QueueWriteError::TooBig);
	assert!(queue.backing.len() == 32);
	assert!(&queue.backing[..32] == &GIBBERISH[..32]);
	assert!(queue.read == 0);
	assert!(queue.write == 0);
}

#[test]
fn twostep_write_pseudo_empty()
{
	let mut queue = Queue::new();
	queue.backing.resize(32, 0);
	queue.read = queue.backing.len();
	let buf = queue.prepare_write(32).expect("Should succeed");
	buf.copy_from_slice(&GIBBERISH[..32]);
	queue.commit_write(32).expect("Should suceed");
	assert!(queue.commit_write(32).expect_err("Should fail") == QueueWriteError::TooBig);
	assert!(queue.backing.len() == 32);
	assert!(&queue.backing[..32] == &GIBBERISH[..32]);
	assert!(queue.read == 0);
	assert!(queue.write == 0);
}

#[test]
fn twostep_write_pseudo_empty_commit_1under()
{
	let mut queue = Queue::new();
	queue.backing.resize(32, 0);
	queue.read = queue.backing.len();
	let buf = queue.prepare_write(32).expect("Should succeed");
	buf.copy_from_slice(&GIBBERISH[..32]);
	queue.commit_write(31).expect("Should suceed");
	assert!(queue.commit_write(32).expect_err("Should fail") == QueueWriteError::TooBig);
	assert!(queue.backing.len() == 32);
	assert!(&queue.backing[..32] == &GIBBERISH[..32]);
	assert!(queue.read == 0);
	assert!(queue.write == 31);
}

#[test]
fn twostep_write_pseudo_empty_smaller()
{
	let mut queue = Queue::new();
	queue.backing.resize(16, 0);
	queue.read = queue.backing.len();
	let buf = queue.prepare_write(32).expect("Should succeed");
	buf.copy_from_slice(&GIBBERISH[..32]);
	queue.commit_write(32).expect("Should suceed");
	assert!(queue.commit_write(32).expect_err("Should fail") == QueueWriteError::TooBig);
	assert!(queue.backing.len() == 32);
	assert!(&queue.backing[..32] == &GIBBERISH[..32]);
	assert!(queue.read == 0);
	assert!(queue.write == 0);
}

#[test]
fn twostep_write_pseudo_empty_larger()
{
	let mut queue = Queue::new();
	queue.backing.resize(64, 0);
	queue.read = queue.backing.len();
	let buf = queue.prepare_write(32).expect("Should succeed");
	buf.copy_from_slice(&GIBBERISH[..32]);
	queue.commit_write(32).expect("Should suceed");
	assert!(queue.commit_write(32).expect_err("Should fail") == QueueWriteError::TooBig);
	assert!(queue.backing.len() == 64);
	assert!(&queue.backing[..32] == &GIBBERISH[..32]);
	assert!(queue.read == 0);
	assert!(queue.write == 32);
}

#[test]
fn twostep_write_pseudo_empty_larger_commit_smaller()
{
	let mut queue = Queue::new();
	queue.backing.resize(64, 0);
	queue.read = queue.backing.len();
	let buf = queue.prepare_write(32).expect("Should succeed");
	buf.copy_from_slice(&GIBBERISH[..32]);
	queue.commit_write(24).expect("Should suceed");
	assert!(queue.commit_write(32).expect_err("Should fail") == QueueWriteError::TooBig);
	assert!(queue.backing.len() == 64);
	assert!(&queue.backing[..32] == &GIBBERISH[..32]);
	assert!(queue.read == 0);
	assert!(queue.write == 24);
}

#[test]
fn twostep_write_pseudo_empty_larger_rollback()
{
	let mut queue = Queue::new();
	queue.backing.resize(64, 0);
	queue.read = queue.backing.len();
	let buf = queue.prepare_write(32).expect("Should succeed");
	buf.copy_from_slice(&GIBBERISH[..32]);
	queue.commit_write(0).expect("Should suceed");
	assert!(queue.commit_write(32).expect_err("Should fail") == QueueWriteError::TooBig);
	assert!(queue.read == 64);
	assert!(queue.write == 0);
}

#[test]
fn twostep_write_pseudo_empty_commit_too_big()
{
	let mut queue = Queue::new();
	queue.backing.resize(64, 0);
	queue.read = queue.backing.len();
	let buf = queue.prepare_write(32).expect("Should succeed");
	buf.copy_from_slice(&GIBBERISH[..32]);
	assert!(queue.commit_write(33).expect_err("Should fail") == QueueWriteError::TooBig);
	assert!(queue.read == 64);
	assert!(queue.write == 0);
	queue.commit_write(32).expect("Should suceed");
	assert!(queue.read == 0);
	assert!(queue.write == 32);
}

#[test]
fn twostep_write_1part_1under()
{
	let mut queue = prepare_1part_test();
	let buf = queue.prepare_write(71).expect("Should succeed");
	buf.copy_from_slice(&GIBBERISH[..71]);
	queue.commit_write(71).expect("Should suceed");
	assert!(queue.backing.len() == 128);
	assert!(&queue.backing[56..127] == &GIBBERISH[..71]);
	assert!(queue.read == 24);
	assert!(queue.write == 127);
	
}

#[test]
fn twostep_write_1part_exact()
{
	let mut queue = prepare_1part_test();
	let buf = queue.prepare_write(72).expect("Should succeed");
	buf.copy_from_slice(&GIBBERISH[..72]);
	queue.commit_write(72).expect("Should suceed");
	assert!(queue.backing.len() == 128);
	assert!(&queue.backing[56..128] == &GIBBERISH[..72]);
	assert!(queue.read == 24);
	assert!(queue.write == 0);
	
}

#[test]
fn twostep_write_1part_1over()
{
	let mut queue = prepare_1part_test();
	let buf = queue.prepare_write(73).expect("Should succeed");
	buf.copy_from_slice(&GIBBERISH[..73]);
	queue.commit_write(73).expect("Should suceed");
	assert!(queue.backing.len() == 256);
	assert!(&queue.backing[56..129] == &GIBBERISH[..73]);
	assert!(queue.read == 24);
	assert!(queue.write == 129);
	
}

#[test]
fn twostep_write_2part_1under()
{
	let mut queue = prepare_2part_test();
	let buf = queue.prepare_write(63).expect("Should succeed");
	buf.copy_from_slice(&GIBBERISH[..63]);
	queue.commit_write(63).expect("Should suceed");
	assert!(queue.backing.len() == 128);
	assert!(&queue.backing[32..95] == &GIBBERISH[..63]);
	assert!(queue.read == 96);
	assert!(queue.write == 95);
}

#[test]
fn twostep_write_2part_exact()
{
	let mut queue = prepare_2part_test();
	let buf = queue.prepare_write(64).expect("Should succeed");
	buf.copy_from_slice(&GIBBERISH[..64]);
	queue.commit_write(64).expect("Should suceed");
	assert!(queue.backing.len() == 128);
	assert!(&queue.backing[32..96] == &GIBBERISH[..64]);
	assert!(queue.read == 96);
	assert!(queue.write == 96);
}

#[test]
fn twostep_write_2part_1over()
{
	let mut queue = prepare_2part_test();
	(&mut queue.backing[96..128]).copy_from_slice(&GIBBERISH[500..532]);
	let buf = queue.prepare_write(65).expect("Should succeed");
	buf.copy_from_slice(&GIBBERISH[..65]);
	queue.commit_write(65).expect("Should suceed");
	assert!(queue.backing.len() == 256);
	assert!(&queue.backing[32..97] == &GIBBERISH[..65]);
	assert!(queue.read == 224);
	assert!(queue.write == 97);
	assert!(&queue.backing[224..256] == &GIBBERISH[500..532]);
}

#[test]
fn twostep_write_2part_tightfit_enlarge()
{
	let mut queue = prepare_2part_test();
	(&mut queue.backing[96..128]).copy_from_slice(&GIBBERISH[500..532]);
	let buf = queue.prepare_write(192).expect("Should succeed");
	buf.copy_from_slice(&GIBBERISH[..192]);
	queue.commit_write(192).expect("Should suceed");
	assert!(queue.backing.len() == 256);
	assert!(&queue.backing[32..224] == &GIBBERISH[..192]);
	assert!(queue.read == 224);
	assert!(queue.write == 224);
	assert!(&queue.backing[224..256] == &GIBBERISH[500..532]);
}

#[test]
fn twostep_write_exact_wraparound()
{
	use std::iter::repeat;
	let mut queue = Queue {
		backing: repeat(0u8).take(128).collect(),
		read: 64,
		write: 0,
		flags: 0,
		pending_size: 0,
	};
	(&mut queue.backing[64..128]).copy_from_slice(&GIBBERISH[64..128]);
	let buf = queue.prepare_write(64).expect("Should succeed");
	buf.copy_from_slice(&GIBBERISH[..64]);
	queue.commit_write(64).expect("Should suceed");
	assert!(queue.backing.len() == 128);
	assert!(&queue.backing[0..128] == &GIBBERISH[..128]);
	assert!(queue.read == 64);
	assert!(queue.write == 64);
}

#[test]
fn write_expand_extreme()
{
	let mut queue = Queue::new();
	queue.backing.resize(1024, 0);
	queue.read = 960;
	queue.write = 32;
	(&mut queue.backing[0..32]).copy_from_slice(&GIBBERISH[2000..2032]);
	queue.write(&GIBBERISH[0..1952]).expect("Should suceed");
	assert!(queue.backing.len() == 2048);
	assert!(queue.read == 960);
	assert!(queue.write == 960);
	assert!(&queue.backing[1024..1056] == &GIBBERISH[2000..2032]);
	assert!(&queue.backing[1056..2048] == &GIBBERISH[0..992]);
	assert!(&queue.backing[0..960] == &GIBBERISH[992..1952]);
}

#[test]
fn write_expand_clown()
{
	let mut queue = Queue::new();
	queue.backing.resize(1920, 0);
	queue.read = 1400;
	queue.write = 500;
	(&mut queue.backing[1400..1920]).copy_from_slice(&GIBBERISH[0..520]);
	(&mut queue.backing[0..500]).copy_from_slice(&GIBBERISH[520..1020]);
	queue.write(&GIBBERISH[1020..2048]).expect("Should suceed");
	assert!(queue.backing.len() == 2048);
	assert!(queue.read == 1528);
	assert!(queue.write == 1528);
	assert!(&queue.backing[1528..2048] == &GIBBERISH[0..520]);
	assert!(&queue.backing[0..1528] == &GIBBERISH[520..2048]);
}


#[test]
fn basic_write_freespace()
{
	use std::iter::repeat;
	let mut queue = Queue {
		backing: repeat(0u8).take(128).collect(),
		read: 24,
		write: 56,
		flags: 0,
		pending_size: 0,
	};
	assert_eq!(queue.buffered(), 32);
	queue.write(&[2, 6, 2, 5, 2, 3]).unwrap();
	assert_eq!(queue.write, 62);
	assert_eq!(queue.flags, 0);
	assert_eq!(queue.buffered(), 38);
}

#[test]
fn basic_write_freespace_2part()
{
	use std::iter::repeat;
	let mut queue = Queue {
		backing: repeat(0u8).take(128).collect(),
		read: 56,
		write: 24,
		flags: 0,
		pending_size: 0,
	};
	assert_eq!(queue.buffered(), 96);
	queue.write(&[2, 6, 2, 5, 2, 3]).unwrap();
	assert_eq!(queue.backing.len(), 128);
	assert_eq!(queue.write, 30);
	assert_eq!(queue.flags, 0);
	assert_eq!(queue.buffered(), 102);
}

#[test]
fn basic_write_freespace_warped()
{
	use std::iter::repeat;
	let mut queue = Queue {
		backing: repeat(0u8).take(128).collect(),
		read: 24,
		write: 126,
		flags: 0,
		pending_size: 0,
	};
	assert_eq!(queue.buffered(), 102);
	queue.write(&[2, 6, 2, 5, 2, 3]).unwrap();
	assert_eq!(queue.write, 4);
	assert_eq!(queue.flags, 0);
	assert_eq!(queue.buffered(), 108);
}

#[test]
fn write_realloc_case_a()
{
	use std::iter::repeat;
	let mut queue = Queue {
		backing: repeat(0u8).take(128).collect(),
		read: 12,
		write: 10,
		flags: 0,
		pending_size: 0,
	};
	assert_eq!(queue.buffered(), 126);
	//This is supposed to copy the initial segment.
	queue.write(&[2, 6, 2, 5, 2, 3]).unwrap();
	assert_eq!(queue.backing.len(), 256);
	assert_eq!(queue.write, 144);
	assert_eq!(queue.read, 12);
	assert_eq!(queue.flags, 0);
	assert_eq!(queue.buffered(), 132);
}

#[test]
fn write_realloc_case_b()
{
	use std::iter::repeat;
	let mut queue = Queue {
		backing: repeat(0u8).take(128).collect(),
		read: 120,
		write: 119,
		flags: 0,
		pending_size: 0,
	};
	assert_eq!(queue.buffered(), 127);
	//This is supposed to copy the final segment.
	queue.write(&[2, 6, 2, 5, 2, 3]).unwrap();
	assert_eq!(queue.backing.len(), 256);
	assert_eq!(queue.write, 125);
	assert_eq!(queue.read, 248);
	assert_eq!(queue.flags, 0);
	assert_eq!(queue.buffered(), 133);
}

#[test]
fn write_realloc_case_n()
{
	use std::iter::repeat;
	let mut queue = Queue {
		backing: repeat(0u8).take(128).collect(),
		read: 1,
		write: 126,
		flags: 0,
		pending_size: 0,
	};
	assert_eq!(queue.buffered(), 125);
	//This is supposed to expand the final segment.
	queue.write(&[2, 6, 2, 5, 2, 3]).unwrap();
	assert_eq!(queue.backing.len(), 256);
	assert_eq!(queue.write, 132);
	assert_eq!(queue.read, 1);
	assert_eq!(queue.flags, 0);
	assert_eq!(queue.buffered(), 131);
}

#[test]
fn write_sequence()
{
	let mut sbuf = [0u8; 256];
	let mut dbuf = [0u8; 256];
	for i in 0..256 { sbuf[i] = i as u8; }
	let mut queue = Queue {
		backing: Vec::new(),
		read: 0,
		write: 0,
		flags: 0,
		pending_size: 0,
	};
	assert_eq!(queue.buffered(), 0);
	queue.write(&sbuf[0..31]).unwrap();
	assert_eq!(queue.backing.len(), 32);
	assert_eq!(queue.write, 31);
	assert_eq!(queue.read, 0);
	assert_eq!(queue.buffered(), 31);
	assert_eq!(queue.read(&mut dbuf[0..5]).unwrap(), 5);
	assert_eq!(queue.backing.len(), 32);
	assert_eq!(queue.write, 31);
	assert_eq!(queue.read, 5);
	assert_eq!(queue.buffered(), 26);
	queue.write(&mut sbuf[31..40]).unwrap();
	assert_eq!(queue.backing.len(), 64);
	assert_eq!(queue.write, 40);
	assert_eq!(queue.read, 5);
	assert_eq!(queue.buffered(), 35);
	queue.write(&mut sbuf[40..64]).unwrap();
	assert_eq!(queue.backing.len(), 64);
	assert_eq!(queue.write, 0);
	assert_eq!(queue.read, 5);
	assert_eq!(queue.buffered(), 59);
	queue.write(&mut sbuf[64..68]).unwrap();
	assert_eq!(queue.backing.len(), 64);
	assert_eq!(queue.write, 4);
	assert_eq!(queue.read, 5);
	assert_eq!(queue.buffered(), 63);
	queue.write(&mut sbuf[68..69]).unwrap();
	assert_eq!(queue.backing.len(), 64);
	assert_eq!(queue.write, 5);
	assert_eq!(queue.read, 5);
	assert_eq!(queue.buffered(), 64);
	queue.write(&mut sbuf[69..127]).unwrap();
	assert_eq!(queue.backing.len(), 128);
	assert_eq!(queue.write, 127);
	assert_eq!(queue.read, 5);
	assert_eq!(queue.buffered(), 122);
	assert_eq!(queue.read(&mut dbuf[5..120]).unwrap(), 115);
	assert_eq!(queue.backing.len(), 128);
	assert_eq!(queue.write, 127);
	assert_eq!(queue.read, 120);
	assert_eq!(queue.buffered(), 7);
	queue.write(&mut sbuf[127..227]).unwrap();
	assert_eq!(queue.backing.len(), 128);
	assert_eq!(queue.write, 99);
	assert_eq!(queue.read, 120);
	assert_eq!(queue.buffered(), 107);
	queue.write(&mut sbuf[227..256]).unwrap();
	assert_eq!(queue.eof_sent(), false);
	queue.send_eof();
	assert_eq!(queue.eof_sent(), true);
	assert_eq!(queue.write(&mut sbuf[256..256]), Err(QueueWriteError::Eof));
	assert_eq!(queue.backing.len(), 256);
	assert_eq!(queue.write, 128);
	assert_eq!(queue.read, 248);
	assert_eq!(queue.buffered(), 136);
	assert_eq!(queue.read(&mut dbuf[120..256]).unwrap(), 136);
	assert_eq!(queue.backing.len(), 256);
	assert_eq!(queue.write, 0);
	assert_eq!(queue.read, 256);
	assert_eq!(queue.buffered(), 0);
	assert_eq!(queue.read(&mut dbuf[256..256]), None);
	assert_eq!(queue.eof_sent(), true);
	assert_eq!(&sbuf[..], &dbuf[..]);
}
