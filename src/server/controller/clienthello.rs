use super::BaseMessageParams;
use super::encode_sni_kind;
use super::Flags;
use super::HandshakeState2;
use super::ProtectionSpecial;
use super::SelectProtectionParameters;
use super::select_protection;
use super::sni_to_text;
use super::super::AlpnList;
use super::super::ClientCertificateSupport;
use super::certificate::TlsCommonCertificateFields;
use super::serverhello::ServerHelloFields;
use crate::acme::generate_acme_certificate;
use crate::callbacks::ForceRestartParameters;
use crate::callbacks::GotUnknownExtensionParameters;
use crate::callbacks::MSG_CLIENTHELLO;
use crate::callbacks::RequestCertificateParameters;
use crate::callbacks::SelectAlpnParameters;
use crate::callbacks::SelectAlpnReturn;
use crate::callbacks::TargetInfoParameters;
use crate::callbacks::TlsCallbacks;
use crate::certificates::CertificateLookupCriteria;
use crate::certificates::CertificateLookupError;
use crate::certificates::ServerCertificate;
use crate::common::Ciphersuite;
use crate::common::CryptoTracker;
use crate::common::is_unknown_extension;
use crate::common::test_failure;
use crate::common::TlsVersion;
use crate::errors::Error;
use crate::features::ConfigPages;
use crate::features::FLAGS0_ALLOW_BAD_CRYPTO;
use crate::features::FLAGS0_ASSUME_HW_AES;
use crate::features::FLAGS0_ENABLE_TLS12;
use crate::features::FLAGS0_NO_ASSUME_HW_AES;
use crate::features::FLAGS0_NSA;
use crate::features::FLAGS0_REQUIRE_EMS;
use crate::features::FLAGS5_TEST_CH_RECV_FAIL;
#[cfg(test)] use crate::features::FLAGS5_FORCE_TLS12;
use crate::logging::DebugSetting;
use crate::messages::DumpHexblock;
use crate::messages::HandshakeHash;
use crate::messages::HandshakeMessage;
use crate::messages::HashMessages;
use crate::messages::HRR_MAGIC;
use crate::messages::SessionId;
use crate::messages::fields::FIELD_COOKIE;
use crate::messages::fields::FIELD_EXT_LIST;
use crate::messages::fields::FIELD_SESSION_ID;
use crate::record::DiffieHellmanSharedSecretResponder;
use crate::record::InternalReceiver;
use crate::record::InternalReceiverReply;
use crate::record::UpcallReturn;
use crate::server::ServerConfiguration;
use crate::server::CCSFLAG_CT;
use crate::server::CCSFLAG_OCSP;
use crate::server::CCSFLAG_TRANS;
use crate::stdlib::Arc;
use crate::stdlib::Box;
use crate::stdlib::Cow;
use crate::stdlib::Deref;
use crate::stdlib::from_utf8;
use crate::stdlib::IpAddr;
use crate::stdlib::Mutex;
use crate::stdlib::String;
use crate::stdlib::ToOwned;
use crate::stdlib::Vec;
use crate::system::Instant;
use crate::test::parse_rdns;
use crate::utils::to_mu8_mut;
use btls_aux_dhf::KEM;
use btls_aux_dhf::KemEnabled;
use btls_aux_fail::ResultExt;
use btls_aux_hash::Hash;
use btls_aux_hash::HashOutput2;
use btls_aux_hash::Sha256;
use btls_aux_random::secure_random;
use btls_aux_random::TemporaryRandomLifetimeTag;
use btls_aux_random::TemporaryRandomStream;
use btls_aux_rope3::FragmentSequence as FragmentSequence3;
use btls_aux_rope3::FragmentWrite as FragmentWrite3;
use btls_aux_rope3::FragmentWriteOps as FragmentWriteOps3;
use btls_aux_rope3::rope3;
use btls_aux_rope3::VectorFieldError;
use btls_aux_serialization::Sink;
use btls_aux_serialization::SliceSink;
use btls_aux_serialization::Source;
use btls_aux_serialization::VectorLength;
use btls_aux_set2::SetOf;
use btls_aux_signatures::SignatureAlgorithm2;
use btls_aux_signatures::SignatureAlgorithmEnabled2;
use btls_aux_signatures::SignatureAlgorithmTls2;
use btls_aux_tls_iana::CertificateType;
use btls_aux_tls_iana::ExtensionType as IanaExtension;
use btls_aux_tls_iana::HandshakeType;
use btls_aux_tls_iana::TlsVersion as IanaTlsVersion;
use btls_aux_tlsciphersuites::Ciphersuite as _Ciphersuite;
use btls_aux_tlsciphersuites::CiphersuiteEnabled2;
use btls_aux_tlsciphersuites::KeyExchangeType;


const AUTH_KEY_ROTATE_INTERVAL: u64 = 300;	//5min.

pub struct _RestartAuthKeys
{
	slot1: [u8;32],
	slot2: [u8;32],
	last_epoch: u64,
	basetime: Instant,
}

impl _RestartAuthKeys
{
	fn new() -> _RestartAuthKeys
	{
		_RestartAuthKeys {
			slot1: [0;32],
			slot2: [0;32],
			last_epoch: 0,
			basetime: Instant::now(),
		}
	}
	fn active(&mut self) -> (u8, [u8;32])
	{
		let now = Instant::now();
		let nepoch = if now < self.basetime {
			//This should never happen. Just use epoch 1, which is the first epoch.
			1
		} else {
			//Now, duration from self.basetime to now is guaranteed to exist. Calculate new epoch number.
			now.duration_since(self.basetime).as_secs() / AUTH_KEY_ROTATE_INTERVAL + 1
		};
		//The new epoch number may be:
		//1) <= last epoch number.
		//2) last epoch number + 1.
		//3) > last epoch number + 1.
		if nepoch <= self.last_epoch {
			//Case 1): The keys are up to date.
		} else if nepoch == self.last_epoch + 1 {
			//Case 2): We should regenerate one of the keys and switch to that.
			//If nepoch is odd, reinitialize slot1, if even reinitialize slot2.
			secure_random(if nepoch & 1 == 1 { &mut self.slot1 } else { &mut self.slot2 });
			self.last_epoch = nepoch;
		} else {
			//Case 3): We should regenerate both slots.
			secure_random(&mut self.slot1);
			secure_random(&mut self.slot2);
			self.last_epoch = nepoch;
		}
		//If current epoch is odd, use slot1, otherwise use slot2.
		if self.last_epoch & 1 == 1 { (0, self.slot1) } else { (1, self.slot2) }
	}
	fn slot(&mut self, n: u8) -> Result<[u8;32], ()>
	{
		match n {
			0 => Ok(self.slot1),
			1 => Ok(self.slot2),
			_ => Err(()),
		}
	}
}

#[derive(Clone)]
pub struct RestartAuthKeys(Arc<Mutex<_RestartAuthKeys>>);

impl RestartAuthKeys
{
	pub fn new() -> RestartAuthKeys
	{
		RestartAuthKeys(Arc::new(Mutex::new(_RestartAuthKeys::new())))
	}
	fn active(&self) -> (u8, [u8;32]) { self.0.lock().active() }
	fn slot(&self, n: u8) -> Result<[u8;32], ()> { self.0.lock().slot(n) }
}

pub struct RestartInfo
{
	pub ipaddr: [u8;16],
	pub ch_hash: HashOutput2,
	pub raw_cs: u16,
	pub kex_kem: u16,
	pub hash: [u8; 32],
}

impl RestartInfo
{
	//Assumes that data parts are prefix-free.
	fn compute_mac(key: [u8;32], data: &[&[u8]]) -> [u8;32]
	{
		Sha256::hmac_v(&key[..], data)
	}
	fn to_octets_sink<S:Sink>(&self, sink: &mut S, key: &RestartAuthKeys) -> Result<(), ()>
	{
		//Dirty way to save space: Only include self.ipaddr and self.hash in the MAC.
		let marker = sink.marker_posonly();
		let (keyid, rkey) = key.active();
		sink.write_u8(keyid)?;
		sink.write_u16(self.raw_cs)?;
		sink.write_u16(self.kex_kem)?;
		sink.vector2_fn(LEN_SHORT, |sink|sink.write_slice(&self.ch_hash))?;
		//Compute MAC for restart info.
		let mut mac = [0;32];
		sink.callback_after(marker, &mut |data|{
			mac = RestartInfo::compute_mac(rkey, &[&self.ipaddr[..], &self.hash[..], data][..]);
		});
		sink.write_slice(&mac[..])?;
		Ok(())
	}
	fn to_octets(&self, space: &mut [u8], key: &RestartAuthKeys) -> Result<usize, Error>
	{
		let msgdatalen = {
			let mut sink = SliceSink::new(space);
			assert_some!(self.to_octets_sink(&mut sink, key), "Unable to dump restart info");
			sink.written()
		};
		Ok(msgdatalen)

	}
	fn _from_octets(buffer: &[u8], key: &RestartAuthKeys, ipaddr: [u8;16], hash: [u8;32],
		enabled_cs: CiphersuiteEnabled2) -> Result<RestartInfo, ()>
	{
		//Dirty way to save space: Only include self.ipaddr and self.hash in the MAC.
		let mut buffer = Source::new(buffer);
		let mut mac = [0;32];
		let slot = dtry!(buffer.u8());
		let raw_cs = dtry!(buffer.u16());
		let kex_kem = dtry!(buffer.u16());
		let ch_hash = dtry!(buffer.slice(LEN_SHORT));
		let dlen = buffer.as_slice().len().saturating_sub(mac.len());
		dtry!(buffer.array(&mut mac[..]));
		//Since it is not directly possible to make HashOutput out of slice, do it roundabout way:
		//construct ciphersuite object for raw_cs, get the prf, construct zeroes and mutate that.
		let cs = dtry!(Ciphersuite::by_tls_id3(raw_cs, enabled_cs));
		let mut ch_hash2 = cs.get_prf2().zeroes();
		fail_if!(ch_hash.len() != ch_hash2.len(), ());	//Bad hash length.
		ch_hash2.copy_from_slice(ch_hash);

		//Verify MAC of restart info.
		let mac2 = RestartInfo::compute_mac(key.slot(slot)?,
			&[&ipaddr[..], &hash[..], &buffer.as_slice()[..dlen][..]]);
		let mut syndrome = 0;
		for i in 0..32 { syndrome |= mac[i] ^ mac2[i]; }
		fail_if!(syndrome != 0, ());

		Ok(RestartInfo{
			ipaddr: ipaddr,
			ch_hash: ch_hash2,
			raw_cs: raw_cs,
			kex_kem: kex_kem,
			hash: hash,
		})
	}
	fn from_octets(buffer: &[u8], key: &RestartAuthKeys, ipaddr: [u8;16], hash: [u8;32],
		enabled_cs: CiphersuiteEnabled2) -> Result<RestartInfo, Error>
	{
		RestartInfo::_from_octets(buffer, key, ipaddr, hash, enabled_cs).set_err(error!(bad_cookie))
	}
}

const LEN_SHORT: VectorLength = VectorLength::variable(0, VectorLength::MAX_8BIT);

pub struct AcmeInternalReceiver;

impl InternalReceiver for AcmeInternalReceiver
{
	fn receive_end(&mut self, y: &mut dyn InternalReceiverReply)
	{
		//This should be close_notify, that is, end-of-stream, send one back, which should close the
		//connetion immediately.
		y.send_error(error!(acme_validation_done));
	}
	fn receive(&mut self, _: &[u8], y: &mut dyn InternalReceiverReply)
	{
		//This is actual data. This should not happen!
		y.send_error(error!(appdata_after_eof));
	}
	fn receive_init(&mut self, _y: &mut dyn InternalReceiverReply) {}
}

pub(crate) struct ClientHelloFields
{
	pub restart_auth_keys: RestartAuthKeys,
	pub stateless: bool,
	pub source_ip: IpAddr,
	pub destination_ip: IpAddr,
	pub enabled_kem: KemEnabled,
	pub enabled_sig: SignatureAlgorithmEnabled2,
	pub enabled_cs: CiphersuiteEnabled2,
	pub crypto_algs: CryptoTracker,
	pub flags: Flags,
}

//Select group.
//Note that gmask has already been masked by enabled algorithms.
fn __negotiate_kem(is_tls12: bool, gmask: SetOf<KEM>, p256_hack: bool, secure192: bool, spinal_tap: bool) ->
	Result<KEM, Error>
{
	let mut kem = None;
	let mut any_ok_ignoring_secure192 = false;
	//The hack requires the following:
	// - secure192 is off, as neither X25519 nor P256 is secure enough.
	// - spinal_tap is off, as P256 is preferred to X25519.
	// - p256_hack is on, which signals that P256 share is present, but X25519 share is not.
	// - X25519 is advertised.
	// - [P256 is advertised. Checked separatedly to get KEM for P256.]
	if !secure192 && !spinal_tap && p256_hack && gmask.is_in(&29) {
		if let Some(p256) = gmask.lookup(&23) { return Ok(p256); }
	}
	//Because we only support ECDHE, we can choose group freely.
	let mut maxrank = 0;
	for i in gmask.iter() {
		//Do not pick groups not better than the existing in spinal tap.
		let rank = i.get_security_rank();
		if spinal_tap && rank <= maxrank { continue; }
		//Don't pick unsuitable groups for TLS 1.2.
		if is_tls12 && !i.suitable_for_tls12() { continue; }
		//Restrict groups if secure192 is set. However, still record the fact that there would
		//have been acceptable group without secure192.
		any_ok_ignoring_secure192 = true;
		if secure192 && i.get_classical_security() < 192 { continue; }
		//By being in gmask, this has to be suitable.
		kem = Some(i);
		maxrank = rank;
		if !spinal_tap { break; }	//Scan all in spinal tap.
	}
	//If any would have been OK without secure192, return special error, as that means the client did not
	//advertise support, but there would have been support without that requirement.
	kem.ok_or_else(||if any_ok_ignoring_secure192 {
		error!(secure192_required_not_offered)
	} else {
		error!(no_mutual_groups)
	})
}

fn format_hash_or_write_hrr<'a>(hs_hash: &mut HandshakeHash, version: TlsVersion, kem: KEM,
	ciphersuite: Ciphersuite, session_id: &[u8], cookie: &'a [u8], output: Option<&'a mut [u8]>,
	debug: DebugSetting) -> Result<&'a [u8], Error>
{
	let handle_err = |e:VectorFieldError|error!(serialize_failed "hello_retry_request", e);
	let content3 = rope3!(ERROR_MAP handle_err,
		TlsVersion::TLS_1_2,
		HRR_MAGIC,
		VECTOR FIELD_SESSION_ID(session_id),
		{ciphersuite.tls_id()},
		CompressionMethod::NULL,
		VECTOR FIELD_EXT_LIST(
			EXTENSION SUPPORTED_VERSIONS({version.to_code().get()}),
			EXTENSION KEY_SHARE({kem.tls_id()}),
			if {cookie.len() > 0} EXTENSION COOKIE(VECTOR FIELD_COOKIE(cookie))
		)
	);
	let tsize = content3.length();
	let hdr = [HandshakeType::SERVER_HELLO.get(), (tsize >> 16) as u8, (tsize >> 8) as u8, tsize as u8];
	let fdata = FragmentSequence3::new(&hdr[..], content3);
	//Serialize message.
	Ok(if let Some(output) = output {
		let target = unsafe{to_mu8_mut(output)};
		FragmentWriteOps3::to_slice_u(&fdata, target).
			map_err(|(s,l)|assert_failure!(F "HelloRetryRequest too big ({s} > {l})"))?
	} else {
		hs_hash.add_v3(&fdata, HandshakeType::SERVER_HELLO, debug.clone())?;
		static DUMMY: [u8;0] = [];
		&DUMMY[..]
	})
}


/*******************************************************************************************************************/
use btls_aux_dhf::kem_public_key_check2;
use btls_aux_tls_iana::CompressionMethod;
use btls_aux_tls_iana::NamedGroup;
use btls_aux_tls_struct::ClientHello as PClientHello;
use btls_aux_tls_struct::Optimization::Maximize as OptMax;
use btls_aux_tls_struct::Tiebreak::First as TieFirst;
use core::fmt::Arguments;

fn new_negotiate_tls_version(pmsg: &PClientHello) -> Result<TlsVersion, Error>
{
	//Client sent supported_versions, or one was synthethized, use that to negotiate.
	pmsg.get_tls_versions().optimize_map(false, OptMax, TieFirst, |ver|match ver {
		IanaTlsVersion::TLS_1_2 => Some((0, TlsVersion::Tls12)),
		IanaTlsVersion::TLS_1_3_DRAFT23 => Some((1, TlsVersion::Tls13Draft23)),
		IanaTlsVersion::TLS_1_3_DRAFT28 => Some((2, TlsVersion::Tls13Draft28)),
		IanaTlsVersion::TLS_1_3 => Some((3, TlsVersion::Tls13)),
		_ => None
	}).ok_or(error!(no_mutual_versions))
}

fn decay_opt_string<'a>(x: &'a Option<String>) -> Option<&'a str>
{
	match x { &Some(ref x) => Some(x), &None => None }
}

fn ch_invariant_hash(pmsg: &PClientHello) -> [u8;32]
{
	let mut hash = Sha256::new();
	hash.append(pmsg.get_base_prefix());
	//Ignore volatile extensions, as those can change.
	let mut extensions: Vec<_> = pmsg.get_extensions().iter_all().filter(|e|!e.get_type().is_tls13_volatile()).
		collect();
	//Sort the extensions, in case client changes the ordering.
	extensions.sort_by_key(|e|e.get_type().get());
	for extension in extensions {
		hash.append(&extension.get_type().get().to_le_bytes());
		let v = extension.get_raw_data();
		hash.append(&v.len().to_le_bytes());	//Only need consistency in one execution.
		hash.append(v);
	}
	hash.output()
}


fn __android_hack(pmsg: &PClientHello) -> bool
{
	//The following hard disable the hack:
	//(1) supported_versions.
	//(2) camillia ciphersuite.
	//(3) aria ciphersuite.
	//(4) supported group <23.
	use btls_aux_tls_iana::CipherSuiteEncryption::*;
	if pmsg.has_extension(IanaExtension::SUPPORTED_VERSIONS) { return false; }
	if pmsg.get_cipher_suites().iter().any(|cs|match cs.encryption() {
		Aria128|Aria256|Camellia128|Camellia256 => true,
		_ => false
	}) { return false; }
	if pmsg.get_groups().iter().any(|g|g.get() < 23) { return false; }
	//One of following is needed to enable the hack:
	//(1) old chacha ciphersuite.
	//(2) groups P256+P384+X25519, without P521.
	//(3) Extension 13172
	if pmsg.has_extension(IanaExtension::new(13172)) { return true; }
	if pmsg.get_cipher_suites().iter().any(|cs|{let cs = cs.get(); cs>=0xCC13&&cs<=0xCC15}) { return true; }
	let mut flags = 0;
	for grp in pmsg.get_groups() { match grp {
		NamedGroup::SECP256R1 => flags |= 1,
		NamedGroup::SECP384R1 => flags |= 2,
		NamedGroup::SECP521R1 => flags |= 4,
		NamedGroup::X25519 => flags |= 8,
		_ => ()
	}}
	if flags == 11 { return true; }
	//Not any: disable.
	false
}

fn get_share_for_kem<'a>(pmsg: &PClientHello<'a>, kem: KEM) -> Option<&'a [u8]>
{
	for entry in pmsg.get_key_shares() {
		//This is correct regardless of if share is present, as if the condition hits with no share,
		//there can not be share.
		if kem.tls_id2() == entry.get_kem() { return entry.get_public_key(); }
	}
	None
}

fn is_known_key_exchange(cs: &Ciphersuite) -> bool
{
	use KeyExchangeType::*;
	matches!(cs.get_key_exchange(), Tls12EcdheEcdsa|Tls12EcdheRsa|Tls13)
}

type CertSignResult = Result<ServerCertificate, CertificateLookupError>;

impl ClientHelloFields
{
	fn __handle_kem_ciphersuite_restart_negotiation<'a>(&self, controller: &mut UpcallReturn,
		callbacks: &mut dyn TlsCallbacks, rawmsg: HandshakeMessage, pmsg: &PClientHello<'a>,
		version: TlsVersion, cflags: &ConfigPages, is_ecdsa: bool, debug: DebugSetting) ->
		Result<Option<(KEM, Ciphersuite, Option<&'a [u8]>, HandshakeHash)>, Error>
	{
		//Cast the ip address to binary form, mapping V4 at the same time.
		let ipaddr = match self.source_ip {
			IpAddr::V6(x) => x.octets(),
			IpAddr::V4(x) => {
				let mut b = [0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,0];
				(&mut b[12..]).copy_from_slice(&x.octets());
				b
			}
		};
		//Restart handling. This also handles KEM and ciphersuite negotiation.
		Ok(Some(if let Some(cookie) = pmsg.get_cookie() {
			debug!(HANDSHAKE_EVENTS debug, "Handshake restart: Resuming...");
			//Cookie is set. This means this is a restart.
			let check_hash = ch_invariant_hash(&pmsg);
			let rinfo = RestartInfo::from_octets(cookie, &self.restart_auth_keys, ipaddr, check_hash,
				self.enabled_cs)?;
			//Check IP address (actually, due to dirty hack, this can never fail).
			fail_if!(ipaddr != rinfo.ipaddr, error!(bad_cookie));
			//Recreate the ciphersuite and kem objects in order to check those are legal.
			let ciphersuite = Ciphersuite::by_tls_id3(rinfo.raw_cs, self.enabled_cs).
				ok_or(error!(no_mutual_ciphersuites))?;
			let kem = KEM::by_tls_id(rinfo.kex_kem, self.enabled_kem).ok_or(error!(no_mutual_groups))?;
			let prf = ciphersuite.get_prf2();
			
			//Reinitialize the handshake hash and include HRR and second CH.
			let mut hs_hash = HandshakeHash::new(prf, debug.clone());
			hs_hash.chain_handshake(&rinfo.ch_hash, debug.clone());
			format_hash_or_write_hrr(&mut hs_hash, version, kem, ciphersuite, pmsg.get_session_id(),
				cookie, None, debug.clone())?;
			hs_hash.add(rawmsg.clone(), debug.clone())?;
			//Check that we have a share for selected kem. It better be present, as restart
			//was to get that.
			let kem_share = get_share_for_kem(&pmsg, kem).ok_or(error!(ch_double_restart))?;
			(kem, ciphersuite, Some(kem_share), hs_hash)
		} else {
			//Check for forced restart. This can not be done in TLS 1.2, but there is a check
			//above for that.
			let force_restart = if self.stateless {
				callbacks.force_restart(ForceRestartParameters {
					source_ip: self.source_ip,
					destination_ip: self.destination_ip,
					_dummy: core::marker::PhantomData,
				})
			} else {
				false
			};
			//Negotiate kem and ciphersuite. These two are needed in order to do restart if needed.
			//also grab a share. There is no share in TLS 1.2, but that is harmless as it will just
			//be None. And note that ciphersuite may not be correct for TLS 1.2, due to TLS_ECDHE_RSA_*
			//versus TLS_ECHDHE_ECDSA_* insanity. If that is case, it will be patched later. This
			//could cause handshake failures, but do not care about those as failures only appear
			//with clients sending insane supported ciphers set.
			let hw_aes = match cflags.get_flags() & (FLAGS0_ASSUME_HW_AES | FLAGS0_NO_ASSUME_HW_AES) {
				FLAGS0_ASSUME_HW_AES => Some(true),
				FLAGS0_NO_ASSUME_HW_AES => Some(false),
				_ => None
			};
			let kem = self.__negotiate_kem(&pmsg, version.is_tls12())?;
			debug!(HANDSHAKE_EVENTS debug, "Client Hello: KEM: {groups:?} -> {group}",
				groups=pmsg.get_groups(), group=kem.tls_id2());
			//If TLS 1.2, force share to None. While normally get_share_for_kem() would return None
			//anyway, this does not happen if manufacturing test forces downgrade to TLS 1.2.
			let kem_share = if !version.is_tls12() {
				get_share_for_kem(&pmsg, kem)
			} else {
				None
			};
			let ciphersuite = self.__negotiate_ciphersuite(&pmsg, version.is_tls12(), is_ecdsa,
				hw_aes)?;
			debug!(HANDSHAKE_EVENTS debug, "Client Hello: Ciphersuite {ciphers:?} -> {cipher}",
				ciphers=pmsg.get_cipher_suites(), cipher=ciphersuite.tls_id2());
			//Include client hello to transcript hash.
			let mut hs_hash = HandshakeHash::new(ciphersuite.get_prf2(), debug.clone());
			hs_hash.add(rawmsg.clone(), debug.clone())?;
			//If restart is forced or there is missing share, perform a restart.
			let share_missing = !version.is_tls12() && kem_share.is_none();
			if force_restart || share_missing {
				if force_restart {
					debug!(HANDSHAKE_EVENTS debug, "Handshake restart: Forced");
				} else {
					debug!(HANDSHAKE_EVENTS debug, "Handshake restart: Need share for KEM {kem}",
						kem=kem.tls_id2());
				}
				//Do the restart.
				let rinfo = RestartInfo{
					ipaddr: ipaddr,
					ch_hash: hs_hash.checkpoint(),
					raw_cs: ciphersuite.tls_id(),
					kex_kem: kem.tls_id(),
					hash: ch_invariant_hash(&pmsg),
				};
				//Write the HelloRetryRequest message. 232 bytes should be enough for it (104 for
				//the restart info). This is TLS 1.3, so ciphersuite is always correct.
				let mut rinfob = [0u8;104];
				let mut msgdata = [0u8;232];
				let rinfol = rinfo.to_octets(&mut rinfob, &self.restart_auth_keys)?;
				let rinfo = &rinfob[..rinfol];
				let msgdata = format_hash_or_write_hrr(&mut hs_hash, version, kem,
					ciphersuite, pmsg.get_session_id(), rinfo,
					Some(&mut msgdata[..]), debug.clone())?;
				//In stateless mode, restart is an error.
				fail_if!(self.stateless, error!(restarting_handshake msgdata.to_owned()));
				//TLS 1.2 record type, send the HRR, followed by fake CCS if needed.
				controller.set_use_tls12_records();
				controller.send_hs_msg_clear(&msgdata);
				if pmsg.get_session_id().len() > 0 { controller.send_clear_ccs(); }
				//Wait for second client hello.
				return Ok(None);
			}
			(kem, ciphersuite, kem_share, hs_hash)
		}))
	}
	fn __negotiate_version(&mut self, pmsg: &PClientHello, cflags: &ConfigPages,
		debug: DebugSetting) -> Result<TlsVersion, Error>
	{
		let supports_tls12 = pmsg.get_tls_versions().iter().any(|p|p == IanaTlsVersion::TLS_1_2);
		let version = new_negotiate_tls_version(pmsg)?;
		if version.is_tls12() {
			//For using TLS 1.2, it must be globally enabled, TLS 1.3-only must not be set and
			//stateless mode must not be used. Cookies must not be used. Error on fallback hack,
			//since this is not newest supported version.
			fail_if!(self.stateless, error!(tls12_not_supported));
			fail_if!(self.flags.is_TLS_13_ONLY(), error!(tls13_required_not_offered));
			fail_if!(!cflags.is_flag(FLAGS0_ENABLE_TLS12), error!(tls12_not_supported));
			fail_if!(pmsg.get_cookie().is_some(), error!(version_mismatch_hrr_sh));
			fail_if!(pmsg.client_has_fallen_back(), error!(downgrade_attack_detected));
			let ems_supported = pmsg.supports_extended_master_secret();
			let need_ems = cflags.is_flag(FLAGS0_REQUIRE_EMS);
			fail_if!(need_ems && !ems_supported, error!(vulernable_ths));
			if ems_supported {
				self.flags.set_EMS_OFFERED();
				debug!(HANDSHAKE_EVENTS debug, "Client Hello: Extended master secret supported.");
			}
		}
		//The renegotiation attack check is done not just for TLS 1.2, but even if client just says
		//it supports TLS 1.2.
		if version.is_tls12() || supports_tls12 {
			match pmsg.get_renegotiated_connection() {
				Some(x) if x.is_empty() => (),	//OK.
				Some(_) => fail!(error!(renegotiation_attack_detected)),
				None => fail!(error!(vulernable_renego)),
			}
		}
		if pmsg.get_tls_versions().count_all(false) > 0 {
			debug!(HANDSHAKE_EVENTS debug, "Client Hello: Version {versions:?} -> {version}.",
				versions=pmsg.get_tls_versions(), version=version);
		} else {
			debug!(HANDSHAKE_EVENTS debug, "Client Hello: Legacy version {version}.");
		}
		#[cfg(test)]
		{if cflags.get_test() & FLAGS5_FORCE_TLS12 != 0 { return Ok(TlsVersion::Tls12); }}
		if !version.is_tls12() { self.flags.set_USING_TLS_13(); }
		Ok(version)
	}
	fn __negotiate_kem(&self, pmsg: &PClientHello, tls12: bool) -> Result<KEM, Error>
	{
		const KYBER: NamedGroup = NamedGroup::X25519KYBER768DRAFT00;
		let shares = pmsg.get_key_shares();
		fail_if!(shares.count() == 0, error!(no_supported_groups));
		let mut have_share_for_x25519 = false;
		let mut have_share_for_p256 = false;
		let mut have_kyber = false;
		let mut kems = SetOf::<KEM>::empty_set();
		for entry in shares {
			//Check the share if possible. This is done even for not enabled ones.
			let group = entry.get_kem();
			let kex = entry.get_public_key();
			//Just ignore any non-enabled group.
			let group2 = f_continue!(KEM::by_tls_id2(group, self.enabled_kem));
			kems.add(&group2);
			match group {
				NamedGroup::X25519 => have_share_for_x25519 = kex.is_some(),
				NamedGroup::SECP256R1 => have_share_for_p256 = kex.is_some(),
				KYBER => have_kyber = kex.is_some(),
				_ => (),
			}
		}
		//If spinal-tap is off and have_kyber is set, pick kyber. This way if kyber is offered with share,
		//it gets picked. No need to check Secure192, because Kyber is Secure192.
		if !self.flags.is_SPINAL_TAP() && have_kyber {
			if let Some(kyber) = kems.lookup(&KYBER.get()) { return Ok(kyber); }
		}
		//If client sends share for P256 but not X25519, prefer P256.
		let p256_hack = have_share_for_p256 && !have_share_for_x25519;
		__negotiate_kem(tls12, kems, p256_hack, self.flags.is_SECURE_192(),
			self.flags.is_SPINAL_TAP())
	}
	fn __negotiate_ciphersuite(&self, pmsg: &PClientHello, tls12: bool, is_ecdsa: bool,
		hw_aes: Option<bool>) -> Result<Ciphersuite, Error>
	{
		use btls_aux_tls_iana::CipherSuiteCipher;
		const AES128: CipherSuiteCipher = CipherSuiteCipher::Aes128Gcm;
		const AES256: CipherSuiteCipher = CipherSuiteCipher::Aes256Gcm;
		const CHACHA: CipherSuiteCipher = CipherSuiteCipher::Chacha20Poly1305;
		//Find first cipher that is AES-GCM or Chacha and check which it is. For this, disables are ignored.
		let prefer_chacha = pmsg.get_cipher_suites().iter().find(|c|match c.cipher() {
			AES128|AES256|CHACHA => true,
			_ => false
		}).map(|c|c.cipher() == CHACHA).unwrap_or(false);
		//Compute mask of ciphesuites with correct key exchange.
		let ciphersuite_mask = SetOf::<_Ciphersuite>::from_iterator(pmsg.get_cipher_suites().iter().
			filter_map(|cs|Ciphersuite::by_tls_id4(cs, self.enabled_cs)).
			filter(|cs|match Some(cs.get_key_exchange()) {
				Some(KeyExchangeType::Tls13) => !tls12,
				Some(KeyExchangeType::Tls12EcdheEcdsa) => tls12 && is_ecdsa,
				Some(KeyExchangeType::Tls12EcdheRsa) => tls12 && !is_ecdsa,
				_ => false
			}));
		//Select protector and PRF.
		let (protector, prf) = select_protection(SelectProtectionParameters {
			ciphersuite_mask: ciphersuite_mask,
			prefer_chacha: prefer_chacha,
			tls13: !tls12,
			hw_aes: hw_aes.unwrap_or_else(btls_aux_aead::hw_aes_supported),
			special: ProtectionSpecial {
				secure192: self.flags.is_SECURE_192(),
				spinal_tap: self.flags.is_SPINAL_TAP(),
			},
		})?;
		//There is only one match with correct protector and PRF.
		let selected = ciphersuite_mask.iter::<Ciphersuite>().
			filter(|c|c.get_protector() == protector).filter(|c|c.get_prf2() == prf).
			next().ok_or(error!(no_mutual_ciphersuites))?;
		Ok(selected)
	}
	fn __negotiate_sni(&mut self, pmsg: &PClientHello, debug: DebugSetting) ->
		Result<(bool, Option<String>), Error>
	{
		if let Some(sni) = pmsg.get_server_host_name() {
			//Lowercase the name.
			let lcsni = sni.to_ascii_lowercase();
			//Don't allow using ACME names in regular handshakes.
			if lcsni.ends_with(".acme.invalid") { fail!(error!(acme_reserved_name lcsni)); }
			//Handle Reverse-DNS blocks specially.
			let (rdns_addr, rdns_active) = match parse_rdns(&lcsni) {
				Ok(Some(x)) => (x, true),
				Ok(None) => (self.destination_ip, false),
				Err(_) => fail!(error!(invalid_rdns_name lcsni))
			};
			debug!(HANDSHAKE_EVENTS debug, "Client Hello: Host name '{sni}'");
			//This connection has SNI, even if it is backinterpretted to IP.
			//If not reverse-DNS, set the SNI to use. For reverse-DNS, use the IP address.
			if !rdns_active {
				Ok((true, Some(lcsni)))
			} else {
				self.destination_ip = rdns_addr;
				Ok((true, None))
			}
		} else {
			Ok((false, None))
		}
	}
	fn __negotiate_status_request(&mut self, pmsg: &PClientHello, debug: DebugSetting) -> Result<(), Error>
	{
		if let btls_aux_tls_struct::StatusRequest::Ocsp(resp) = pmsg.get_status_request() {
			//Only accept any responder and no extensions.
			if resp.accepts_any_responder() && resp.get_extensions().len() == 0 {
				self.flags.set_OCSP_V1_OFFERED();
				debug!(HANDSHAKE_EVENTS debug, "Client hello: OCSP stapling is supported");
				self.crypto_algs.set_client_cert_flags(CCSFLAG_OCSP);
			}
		}
		if pmsg.supports_signed_certificate_timestamps() {
			debug!(HANDSHAKE_EVENTS debug, "Client hello: CT stapling is supported");
			self.flags.set_CT_OFFERED();
			self.crypto_algs.set_client_cert_flags(CCSFLAG_CT);
		}
		if pmsg.supports_transparency_items() {
			debug!(HANDSHAKE_EVENTS debug, "Client hello: Transparency Item stapling is supported");
			self.flags.set_TRANS_OFFERED();
			self.crypto_algs.set_client_cert_flags(CCSFLAG_TRANS);
		}
		Ok(())
	}
	fn __negotiate_max_fragment_size(&mut self, pmsg: &PClientHello, controller: &mut UpcallReturn,
		tls12: bool, debug: DebugSetting) -> Result<(), Error>
	{
		//Maximum fragment size extension.
		if let Some(maxfragsize) = pmsg.get_record_size_limit() {
			let maxsize = maxfragsize as usize;
			let offset = if tls12 { 0 } else { 1 };
			let nominal_size = 16384 + offset;
			self.flags.force_REDUCED_RECORD_SIZE(maxsize < nominal_size);
			debug!(HANDSHAKE_EVENTS debug, "Client hello: Maximum fragment size is {maxsize}");
			//Subtract 1 byte for TLS 1.3.
			controller.set_max_fragment_length(maxsize - offset);
		}
		Ok(())
	}
	fn __negotiate_signature_algorithms(&mut self, pmsg: &PClientHello, cs_set: SetOf<_Ciphersuite>,
		debug: DebugSetting) -> Result<(SetOf<SignatureAlgorithm2>, SetOf<SignatureAlgorithm2>), Error>
	{
		debug!(HANDSHAKE_EVENTS debug, "Client hello: Signatures {signatures:?}",
			signatures=pmsg.get_signature_algorithms());
		if pmsg.has_extension(IanaExtension::SIGNATURE_ALGORITHMS_CERT) {
			debug!(HANDSHAKE_EVENTS debug, "Client hello: Signatures (cert) {signatures:?}",
				signatures=pmsg.get_signature_algorithms_cert());
		}
		let sigalg = SetOf::<SignatureAlgorithm2>::from_iterator(pmsg.get_signature_algorithms().iter().
			filter_map(|s|SignatureAlgorithmTls2::by_tls_id2(s)));
		//Mask sigalg according to RSA/ECDSA in TLS 1.2. And filter signatures not valid for TLS 1.3.
		let mut smask = SetOf::<SignatureAlgorithm2>::empty_set();
		for cs in cs_set.iter::<Ciphersuite>() { 
			smask = smask | match Some(cs.get_key_exchange()) {
				Some(KeyExchangeType::Tls12EcdheRsa) => SignatureAlgorithm2::SET_TLS12_RSA,
				Some(KeyExchangeType::Tls12EcdheEcdsa) => SignatureAlgorithm2::SET_TLS12_ECDSA,
				Some(KeyExchangeType::Tls13) => SignatureAlgorithm2::SET_TLS13_OK,
				_ => SetOf::<SignatureAlgorithm2>::empty_set()
			};
		}
		let sigalg = sigalg & smask;
		//Need at least one common signature.
		sigalg.iter::<SignatureAlgorithmTls2>().next().ok_or(error!(no_mutual_signatures))?;

		let sigalg_cert = SetOf::<SignatureAlgorithm2>::from_iterator(pmsg.get_signature_algorithms_cert().
			iter().filter_map(|s|SignatureAlgorithmTls2::by_tls_id2(s)));
		Ok((sigalg, sigalg_cert))
	}
	fn __negotiate_server_certificate_type(&mut self, pmsg: &PClientHello, is_acme: bool, debug: DebugSetting) ->
		Result<(), Error>
	{
		let mut server_certificate_type = 0;
		for ctype in pmsg.get_server_certificate_types() { match ctype {
			CertificateType::X509 => server_certificate_type |= 1,
			CertificateType::RAW_PUBLIC_KEY => server_certificate_type |= 2,
			_ => (),
		}}
		fail_if!(server_certificate_type == 0,  error!(no_mutual_certificate_type));
		fail_if!(is_acme && server_certificate_type & 1 == 0,  error!(no_mutual_certificate_type));
		//Negotiate server certificate type. ACME has special requirement for X509 here.
		let mut negotiated = CertificateType::X509;
		if !is_acme && server_certificate_type & 2 != 0 {
			self.flags.set_SEND_SPKI_ONLY();
			negotiated = CertificateType::RAW_PUBLIC_KEY;
		}
		if pmsg.has_extension(IanaExtension::SERVER_CERTIFICATE_TYPE) {
			debug!(HANDSHAKE_EVENTS debug,
				"Client hello: Supported certificate types {types:?} -> {negotiated}",
				types=pmsg.get_signature_algorithms());
		}
		Ok(())
	}
	fn __negotiate_alpn_internal(&mut self, pmsg: &PClientHello, controller: &mut UpcallReturn,
		debug: DebugSetting) -> Result<Option<String>, Error>
	{
		let mut alpn = None;
		for protocol in pmsg.get_alpn_protocol_ids() {
			if protocol == btls_aux_tls_iana::AlpnProtocolId::ACME_TLS {
				//Set connection to internal, mark as ACME validation and to be teared
				//down as soon as it reaches showtime.
				controller.set_internal_receiver(Box::new(AcmeInternalReceiver) as
					Box<dyn InternalReceiver+Send>);
				self.flags.set_IS_ACME_VALIDATION();
				debug!(HANDSHAKE_EVENTS debug, "ACME: Validation handshake detected");
			}
			#[cfg(test)]
			{if protocol.get() == b"test-close-echo" {
				controller.set_internal_receiver(Box::new(TestInternalReceiver(0)) as
					Box<dyn InternalReceiver+Send>);
			}}
			#[cfg(test)]
			{if protocol.get() == b"test-rot13" {
				controller.set_internal_receiver(Box::new(TestInternalReceiver(1)) as
					Box<dyn InternalReceiver+Send>);
			}}
			if controller.is_internal_only() {
				//The from_utf8 always succeeds.
				alpn = from_utf8(protocol.get()).map(|x|x.to_owned()).ok();
				break;
			}
		}
		Ok(alpn)
	}
	fn __negotiate_alpn(&mut self, pmsg: &PClientHello, sni: Option<&str>, callbacks: &mut dyn TlsCallbacks,
		debug: DebugSetting) -> Result<Option<String>, Error>
	{
		//If there is no ALPNs...
		if pmsg.get_alpn_protocol_ids().count_all(false) == 0 {
			match callbacks.select_alpn(SelectAlpnParameters {
				sni: sni,
				alpn_list: AlpnList::blank(),
			}) {
				SelectAlpnReturn::Implicit => (),	//Don't select.
				SelectAlpnReturn::NoApplicationProtocol => fail!(error!(alpn_no_overlap)),
				_ => fail!(error!(alpn_internal_error))
			}
			//Do we need a client certificate?
			if callbacks.request_certificate(RequestCertificateParameters {
				sni: sni,
				alpn: None,
			}) {
				self.flags.set_ASK_CLIENT_CERT();
			}
			return Ok(None)
		}
		let alpn_list = AlpnList::__new(pmsg.get_alpn_protocol_ids().as_raw_slice());
		//Prompt application for ALPN to use.
		let mut alpn = None;
		match callbacks.select_alpn(SelectAlpnParameters {
			sni: sni,
			alpn_list: alpn_list,
		}) {
			SelectAlpnReturn::Index(index) => {
				let entry = match alpn_list.iter().nth(index) {
					Some(x) => x,
					None => fail!(error!(alpn_internal_error)),
				};
				alpn = Some(entry.to_owned());
			},
			SelectAlpnReturn::Implicit => (),	//Don't select.
			SelectAlpnReturn::NoApplicationProtocol => fail!(error!(alpn_no_overlap)),
			_ => fail!(error!(alpn_internal_error)),
		}
		debug!(HANDSHAKE_EVENTS debug, "Client hello: Application protocol {protocols:?} -> {protocol}",
			protocols=pmsg.get_alpn_protocol_ids(),
			protocol=decay_opt_string(&alpn).unwrap_or("<default>"));
		//Do we need a client certificate?
		if callbacks.request_certificate(RequestCertificateParameters {
			sni: sni,
			alpn: decay_opt_string(&alpn),
		}) {
			self.flags.set_ASK_CLIENT_CERT();
		}
		Ok(alpn)
	}
	fn __handle_certificate_selection(&self, config: &ServerConfiguration, sni: Option<&str>, has_sni: bool,
		sigalgo_mask: SetOf<SignatureAlgorithm2>, sigalgo_mask_cert: SetOf<SignatureAlgorithm2>, tls12: bool,
		spki_only: bool, debug: DebugSetting) -> Result<CertSignResult, Error>
	{
		//Choose the certificate.
		Ok(if self.flags.is_IS_ACME_VALIDATION() {
			//This MUST contain SNI name or an IP address. Thus, the default SNI is always None!
			fail_if!(!has_sni, error!(no_sni_in_acme_validation));
			let sni_name = sni_to_text(sni, None, self.destination_ip).
				ok_or(error!(no_acme_validation_target))?;
			let sni_ref = encode_sni_kind(sni, None, self.destination_ip).
				ok_or(error!(no_acme_validation_target))?;
			let cid = debug.get_cid();
			let acme = config.acme_challenges.read();
			let acme_key = config.acme_key.as_ref().
				ok_or_else(||assert_failure!("Generating ACME key failed"))?;
			let acme_cert = match acme.lookup(sni_name.deref(), cid) {
				Some(x) => generate_acme_certificate(&x, sni_ref, acme_key.clone())?,
				None => fail!(error!(unknown_acme_validation_target sni_name.into_owned()))
			};
			debug!(HANDSHAKE_EVENTS debug, "ACME validation attempt for {sni_name}");
			//clookup is always Ok(()), so the certificate_err does not matter at all, just pick some
			//unit variant.
			Ok(acme_cert)
		} else {
			let name_str = sni_to_text(sni, decay_opt_string(&config.default_sni_name),
				self.destination_ip);
			let name_str = name_str.as_deref();
			debug!(HANDSHAKE_EVENTS debug, "Looking up certificate for {name}",
				name=name_str.unwrap_or("<default>"));
			let criteria = CertificateLookupCriteria {
				sni: name_str,
				sig_flags2: sigalgo_mask_cert,
				ee_flags2: sigalgo_mask,
				tls13: !tls12,
				selfsigned_ok: spki_only,
				nsa: self.flags.is_FORCE_NSA_MODE() || config.flags.is_flag(FLAGS0_NSA),
			};
			config.lookup_cert.lookup(&criteria)
		})
	}
	pub fn rx_client_hello_2(mut self, controller: &mut UpcallReturn, msg: &[u8],
		bp: BaseMessageParams, cc_support: &mut ClientCertificateSupport) -> Result<HandshakeState2, Error>
	{
		let lifetime = TemporaryRandomLifetimeTag;
		let mut rng = TemporaryRandomStream::new_system(&lifetime);
		let BaseMessageParams{debug, callbacks, config, rawmsg, ..} = bp;
		//Page5 flags are only for manufacturing test.
		#[cfg(not(test))] {sanity_check!(config.flags.get_test() == 0, "Flags page 5 (tests) not empty");}
		test_failure(&config.flags, FLAGS5_TEST_CH_RECV_FAIL)?;
		if config.flags.is_flag(FLAGS0_NSA) { self.crypto_algs.nsa_mode(); }

		let mut debug_cb = |args:Arguments|{
			debug!(TLS_EXTENSIONS debug, "Client Hello: {args}");
		};
		let pmsg = PClientHello::parse(msg, &|g,k|kem_public_key_check2(g,k).is_ok(), Some(&mut debug_cb)).
			map_err(|e|error!(msgerr_client_hello e))?;
		let session_id = assert_some!(SessionId::new(pmsg.get_session_id()),
			"Parsed client hello has invalid session ID");
		debug!(CRYPTO_CALCS debug, "Client hello: Received client random:\n{random}",
			random=DumpHexblock::new(&pmsg.get_random(), "  ", "Client random"));
		let is_insecure = pmsg.get_cipher_suites().iter().any(|cs|cs.is_insecure());
		let allow_insecure = config.flags.is_flag(FLAGS0_ALLOW_BAD_CRYPTO);
		fail_if!(is_insecure && !allow_insecure, error!(client_supports_bad_ciphers));
		//Client random for testing.
		#[cfg(test)] { callbacks.client_random(pmsg.get_random()); }

		//First negotiate the TLS version.
		let version = self.__negotiate_version(&pmsg, &config.flags, debug.clone())?;
		self.crypto_algs.set_version(version);
		//Then follow up with preliminary ALPN negotiation. This is needed for getting the ACME flag,
		//which is needed for certificate type negotiation. Which in turn is needed by signature
		//algorithm negoitation for getting the ALPN flag.
		let mut alpn = self.__negotiate_alpn_internal(&pmsg, controller, debug.clone())?;
		self.__negotiate_server_certificate_type(&pmsg, self.flags.is_IS_ACME_VALIDATION(),
			debug.clone())?;
		//Negotiate signature algorithms. This is needed by certificate negoitation. The ciphersuite
		//mask can be constructed by taking all supported algorithms with suitable key exchange.
		//Secure192 affects the acceptable algorithms, and thus needs to be taken into account. NSA
		//mode acts via ciphersuite mask, so it is implicitly taken into account.
		let is_secure192 = self.flags.is_SECURE_192();
		let ciphersuite_mask = SetOf::<_Ciphersuite>::from_iterator(pmsg.get_cipher_suites().iter().
			filter_map(|cs|Ciphersuite::by_tls_id4(cs, self.enabled_cs)).
			filter(|cs|!is_secure192 || cs.get_protector().get_security_level() >= 192).
			filter(is_known_key_exchange));
		let (sigalg, sigalg_cert) = self.__negotiate_signature_algorithms(&pmsg, ciphersuite_mask,
			debug.clone())?;
		//Yet Another part needed for certificate negoitation is SNI negotiation.
		//Finally, can perform certificate negotiation.
		let (has_sni, sni_name) = self.__negotiate_sni(&pmsg, debug.clone())?;
		let certlookup = match self.__handle_certificate_selection(config, decay_opt_string(&sni_name),
			has_sni, sigalg, sigalg_cert, version.is_tls12(), self.flags.is_SEND_SPKI_ONLY(),
			debug.clone())? {
			Ok(x) => x,
			Err(certlookup_err) => {
				//Format the SNI name.
				let pname = sni_name.as_deref();
				let dname = config.default_sni_name.as_deref();
				let ip = self.destination_ip;
				let name = sni_to_text(pname, dname, ip).unwrap_or(Cow::Borrowed("<default>"));
				let t = name.into_owned();
				//Map the error into library error.
				return Err(match certlookup_err {
					CertificateLookupError::NoMatch => error!(no_matching_certificate t),
					CertificateLookupError::NoSuchHost => error!(no_certificate_available t),
					CertificateLookupError::InternalError(msg) =>
						Error::from(assert_failure!(msg))
				});
			}
		};
		debug!(HANDSHAKE_EVENTS debug, "Server certificate: Certificate selected for the connection");
		//With certificate negotiated, negotiate ciphersuite and KEM. The kem_share is always
		//Some for TLS 1.3, and always None for TLS 1.2.
		let is_ecdsa = certlookup.is_ecdsa();
		let (kem, ciphersuite, kem_share, hs_hash) = match 
			self.__handle_kem_ciphersuite_restart_negotiation(controller, callbacks, rawmsg, &pmsg,
			version, &config.flags, is_ecdsa, debug.clone())? {
			Some(x) => x,
			//Wait for second ClientHello.
			None => return Ok(HandshakeState2::ClientHello(self))
		};
		self.crypto_algs.set_kex_kem(kem);
		self.crypto_algs.set_ciphersuite(ciphersuite);
		//Check that uncompressed is supported.
		pmsg.get_compression_methods().iter().filter(|&m|m==CompressionMethod::NULL).next().
			ok_or(error!(client_does_not_support_uncompressed))?;
		//Compute the shared DH secret for TLS 1.3, and generate DH share for TLS 1.2.
		let dh_secret = if let Some(kem_share) = kem_share {
			//TLS 1.3, perform key exchange.
			let own_share = assert_some!(kem.responder(&mut rng), "Failed to respond to KEM {kem}",
				kem=kem.tls_id2());
			let dh_secret = DiffieHellmanSharedSecretResponder::new(own_share, kem_share)?;
			debug!(HANDSHAKE_EVENTS debug, "Key exchange: Key exchange ok");
			Ok(dh_secret)
		} else {
			//This is TLS 1.2, generate key for key exchange.
			let key_share = assert_some!(kem.generate_key(&mut rng), "Failed to initiate KEM {kem}",
				kem=kem.tls_id2());
			Err(key_share)
		};

		//Android ISRG X1 hack.
		if !version.is_tls12() && !self.flags.is_NO_ANDROID_HACK() && __android_hack(&pmsg) {
			self.flags.set_ISRG_X1_DST_HACK();
			debug!(HANDSHAKE_EVENTS debug, "Client hello: Activating IsrgX1DstHack");
		}
		//Negotiate extensions.
		self.__negotiate_status_request(&pmsg, debug.clone())?;
		self.__negotiate_max_fragment_size(&pmsg, controller, version.is_tls12(), debug.clone())?;
		pmsg.get_ec_point_formats().iter().
			filter(|&m|m==btls_aux_tls_iana::EcPointFormat::UNCOMPRESSED).next().
			ok_or(error!(client_does_not_support_uncompressed_ec))?;
		if !controller.is_internal_only() {
			alpn = self.__negotiate_alpn(&pmsg, decay_opt_string(&sni_name), callbacks, debug.clone())?;
		}
		//Fill out information about client certificate support.
		let ccflags = self.crypto_algs.get_client_cert_flags();
		*cc_support = ClientCertificateSupport::new_info(sigalg, sigalg_cert, ccflags);

		let mut interesting_exts = Vec::new();
		if !controller.is_internal_only() {
			//Target info.
			let alpn = alpn.as_deref();
			let sni_name = sni_name.as_deref();
			callbacks.target_info(TargetInfoParameters {
				sni: sni_name,
				alpn: alpn,
			});
		}

		//Do callbacks on unknown extensions.
		for extension in pmsg.get_extensions() {
			let extension_id = extension.get_type().get();
			if !is_unknown_extension(extension_id) { continue; }
			let p = GotUnknownExtensionParameters {
				id: extension_id,
				payload: extension.get_raw_data(),
			};
			if callbacks.got_unknown_extension(p) {
				interesting_exts.push(extension_id);
			}
		}
		callbacks.end_unknown_extension(MSG_CLIENTHELLO);

		Ok(HandshakeState2::ServerHello(ServerHelloFields {
			certlookup: certlookup,
			maybe_secret: dh_secret,
			ccommon: TlsCommonCertificateFields{
				destination_ip: self.destination_ip,
				sni_name: sni_name,
				peer_sigalgo_mask: sigalg,
			},
			chosen_ciphersuite: ciphersuite,
			real_version: version,
			client_random: pmsg.get_random(),
			hs_hash: hs_hash,
			session_id: session_id,
			enabled_sig: self.enabled_sig,
			alpn: alpn,
			interesting_exts: interesting_exts,
			flags: self.flags,
			crypto_algs: self.crypto_algs,
		}))
	}
}

/*******************************************************************************************************************/
#[cfg(test)]
pub struct TestInternalReceiver(u32);

#[cfg(test)]
impl InternalReceiver for TestInternalReceiver
{
	fn receive_end(&mut self, y: &mut dyn InternalReceiverReply)
	{
		if self.0 == 0 {
			//Mode 0: Just send EOF to any data.
			y.send_error(error!(test_done));
		} else if self.0 == 1 {
			y.send_end()	//Close connection.
		}
	}
	fn receive(&mut self, blk: &[u8], y: &mut dyn InternalReceiverReply)
	{
		if self.0 == 0 {
			//Mode 0: Just send EOF to any data.
			y.send_error(error!(test_done));
		} else if self.0 == 1 {
			//ROT13 the input and send back.
			let mut buf = blk.to_owned();
			for i in 0..buf.len() {
				buf[i] = match buf[i] {
					x@65..=77 => x + 13,
					x@78..=90 => x - 13,
					x@97..=109 => x + 13,
					x@110..=122 => x - 13,
					x => x,
				}
			}
			buf.insert(0, b'Y');
			buf.insert(1, b' ');
			y.reply(&buf);
		}
	}
	fn receive_init(&mut self, y: &mut dyn InternalReceiverReply)
	{
		if self.0 == 1 {
			//Send "READY" back.
			y.reply(b"X READY\n");
		}
	}
}

#[cfg(test)]
fn kem_set(x: &[u16]) -> SetOf<KEM>
{
	let mut kset = SetOf::<KEM>::empty_set();
	let unsafe_all = KemEnabled::from_mask(!0);
	for &y in x.iter() { kset.add(&KEM::by_tls_id(y, unsafe_all).unwrap()); }
	kset
}

#[test]
fn negotiate_kem_spinal_tap_1()
{
	//P-521 is preferred over all others, should be selectable in all modes.
	let unsafe_all = KemEnabled::from_mask(!0);
	let mut secure192;
	for i in 0..128 {
		let mut kset = SetOf::<KEM>::empty_set();
		kset.add(&KEM::by_tls_id(25, unsafe_all).unwrap());
		if i & 64 != 0 { kset.add(&KEM::by_tls_id(30, unsafe_all).unwrap()); }
		if i & 32 != 0 { kset.add(&KEM::by_tls_id(24, unsafe_all).unwrap()); }
		if i & 16 != 0 { kset.add(&KEM::by_tls_id(23, unsafe_all).unwrap()); }
		if i & 8 != 0 { kset.add(&KEM::by_tls_id(29, unsafe_all).unwrap()); }
		let tls12 = i & 4 != 0;
		let phack = i & 2 != 0;
		secure192 = i & 1 != 0;
		assert_eq!(__negotiate_kem(tls12, kset, phack, secure192, true).unwrap().tls_id(), 25);
	}
}

#[test]
fn negotiate_kem_spinal_tap_2()
{
	//X448 is second position preferred, selectable in all modes.
	let unsafe_all = KemEnabled::from_mask(!0);
	let mut secure192;
	for i in 0..64 {
		let mut kset = SetOf::<KEM>::empty_set();
		kset.add(&KEM::by_tls_id(30, unsafe_all).unwrap());
		if i & 32 != 0 { kset.add(&KEM::by_tls_id(24, unsafe_all).unwrap()); }
		if i & 16 != 0 { kset.add(&KEM::by_tls_id(23, unsafe_all).unwrap()); }
		if i & 8 != 0 { kset.add(&KEM::by_tls_id(29, unsafe_all).unwrap()); }
		let tls12 = i & 4 != 0;
		let phack = i & 2 != 0;
		secure192 = i & 1 != 0;
		assert_eq!(__negotiate_kem(tls12, kset, phack, secure192, true).unwrap().tls_id(), 30);
	}
}

#[test]
fn negotiate_kem_spinal_tap_3()
{
	//P384 is third position preferred, selectable in all modes.
	let unsafe_all = KemEnabled::from_mask(!0);
	let mut secure192;
	for i in 0..32 {
		let mut kset = SetOf::<KEM>::empty_set();
		kset.add(&KEM::by_tls_id(24, unsafe_all).unwrap());
		if i & 16 != 0 { kset.add(&KEM::by_tls_id(23, unsafe_all).unwrap()); }
		if i & 8 != 0 { kset.add(&KEM::by_tls_id(29, unsafe_all).unwrap()); }
		let tls12 = i & 4 != 0;
		let phack = i & 2 != 0;
		secure192 = i & 1 != 0;
		assert_eq!(__negotiate_kem(tls12, kset, phack, secure192, true).unwrap().tls_id(), 24);
	}
}

#[test]
fn negotiate_kem_spinal_tap_4()
{
	//P256 is fourth position preferred, selectable in non-secure192 mode.
	let unsafe_all = KemEnabled::from_mask(!0);
	for i in 0..16 {
		let mut kset = SetOf::<KEM>::empty_set();
		kset.add(&KEM::by_tls_id(23, unsafe_all).unwrap());
		if i & 8 != 0 { kset.add(&KEM::by_tls_id(29, unsafe_all).unwrap()); }
		let tls12 = i & 4 != 0;
		let phack = i & 2 != 0;
		assert_eq!(__negotiate_kem(tls12, kset, phack, false, true).unwrap().tls_id(), 23);
	}
}

#[test]
fn negotiate_kem_spinal_tap_5()
{
	//X25519 is fifth position preferred, selectable in non-secure192 mode.
	let unsafe_all = KemEnabled::from_mask(!0);
	for i in 0..8 {
		let mut kset = SetOf::<KEM>::empty_set();
		kset.add(&KEM::by_tls_id(29, unsafe_all).unwrap());
		let tls12 = i & 4 != 0;
		let phack = i & 2 != 0;
		assert_eq!(__negotiate_kem(tls12, kset, phack, false, true).unwrap().tls_id(), 29);
	}
}

#[test]
fn negotiate_kem_normal_1()
{
	let kset = kem_set(&[23,24,25,29,30]);
	let mut secure192;
	for i in 0..8 {
		secure192 = i & 4 != 0;
		let tls12 = i & 2 != 0;
		let phack = i & 1 != 0;
		let expect = if secure192 { 30 } else if phack { 23 } else { 29 };
		assert_eq!(__negotiate_kem(tls12, kset, phack, secure192, false).unwrap().tls_id(), expect);
	}
}

#[test]
fn negotiate_kem_normal_2()
{
	let kset = kem_set(&[23,24,25,29]);
	let mut secure192;
	for i in 0..8 {
		secure192 = i & 4 != 0;
		let tls12 = i & 2 != 0;
		let phack = i & 1 != 0;
		let expect = if secure192 { 24 } else if phack { 23 } else { 29 };
		assert_eq!(__negotiate_kem(tls12, kset, phack, secure192, false).unwrap().tls_id(), expect);
	}
}

#[test]
fn negotiate_kem_normal_3()
{
	let kset = kem_set(&[23,25,29]);
	let mut secure192;
	for i in 0..8 {
		secure192 = i & 4 != 0;
		let tls12 = i & 2 != 0;
		let phack = i & 1 != 0;
		let expect = if secure192 { 25 } else if phack { 23 } else { 29 };
		assert_eq!(__negotiate_kem(tls12, kset, phack, secure192, false).unwrap().tls_id(), expect);
	}
}

#[test]
fn negotiate_kem_insecure()
{
	let kset = kem_set(&[23,29]);
	let mut spinal_tap;
	for i in 0..8 {
		spinal_tap = i & 4 != 0;
		let tls12 = i & 2 != 0;
		let phack = i & 1 != 0;
		assert!(__negotiate_kem(tls12, kset, phack, true, spinal_tap).is_err());
	}
}

#[test]
fn negotiate_cipher_spinal_tap_1()
{
	let unsafe_all = CiphersuiteEnabled2::unsafe_any_policy();
	for i in 0..8 {
		let mut p = SelectProtectionParameters{
			prefer_chacha: false,
			ciphersuite_mask: SetOf::<_Ciphersuite>::empty_set(),
			tls13: true,
			hw_aes: true,
			special: ProtectionSpecial {
				secure192: false,
				spinal_tap: true,
			},
		};
		p.ciphersuite_mask.add(&Ciphersuite::by_tls_id3(0x1303, unsafe_all).unwrap());
		if i & 4 != 0 { p.ciphersuite_mask.add(&Ciphersuite::by_tls_id3(0x1302, unsafe_all).unwrap()); }
		if i & 2 != 0 { p.ciphersuite_mask.add(&Ciphersuite::by_tls_id3(0x1301, unsafe_all).unwrap()); }
		p.special.secure192 = i & 1 != 0;
		let (protection, _) = select_protection(p).unwrap();
		assert_eq!(protection.as_string(), "Chacha20-Poly1305");
	}
}

#[test]
fn negotiate_cipher_spinal_tap_2()
{
	let unsafe_all = CiphersuiteEnabled2::unsafe_any_policy();
	for i in 0..4 {
		let mut p = SelectProtectionParameters{
			prefer_chacha: false,
			ciphersuite_mask: SetOf::<_Ciphersuite>::empty_set(),
			tls13: true,
			hw_aes: true,
			special: ProtectionSpecial {
				secure192: false,
				spinal_tap: true,
			},
		};
		p.ciphersuite_mask.add(&Ciphersuite::by_tls_id3(0x1302, unsafe_all).unwrap());
		if i & 2 != 0 { p.ciphersuite_mask.add(&Ciphersuite::by_tls_id3(0x1301, unsafe_all).unwrap()); }
		p.special.secure192 = i & 1 != 0;
		let (protection, _) = select_protection(p).unwrap();
		assert_eq!(protection.as_string(), "AES-256-GCM");
	}
}

#[test]
fn negotiate_cipher_spinal_tap_3()
{
	let unsafe_all = CiphersuiteEnabled2::unsafe_any_policy();
	let mut p = SelectProtectionParameters{
		prefer_chacha: false,
		ciphersuite_mask: SetOf::<_Ciphersuite>::empty_set(),
		tls13: true,
		hw_aes: true,
		special: ProtectionSpecial {
			secure192: false,
			spinal_tap: true,
		},
	};
	p.ciphersuite_mask.add(&Ciphersuite::by_tls_id3(0x1301, unsafe_all).unwrap());
	let (protection, _) = select_protection(p).unwrap();
	assert_eq!(protection.as_string(), "AES-128-GCM");
}

#[test]
fn negotiate_cipher_normal()
{
	let unsafe_all = CiphersuiteEnabled2::unsafe_any_policy();
	for i in 0..4 {
		let mut p = SelectProtectionParameters{
			prefer_chacha: false,
			ciphersuite_mask: SetOf::<_Ciphersuite>::empty_set(),
			tls13: true,
			hw_aes: true,
			special: ProtectionSpecial {
				secure192: false,
				spinal_tap: false,
			},
		};
		p.prefer_chacha = i & 2 != 0;
		p.hw_aes = i & 1 != 0;
		let expect = if i != 1 { "Chacha20-Poly1305" } else { "AES-128-GCM" };
		p.ciphersuite_mask.add(&Ciphersuite::by_tls_id3(0x1303, unsafe_all).unwrap());
		p.ciphersuite_mask.add(&Ciphersuite::by_tls_id3(0x1302, unsafe_all).unwrap());
		p.ciphersuite_mask.add(&Ciphersuite::by_tls_id3(0x1301, unsafe_all).unwrap());
		let (protection, _) = select_protection(p).unwrap();
		assert_eq!(protection.as_string(), expect);
	}
}

#[test]
fn negotiate_cipher_aes256()
{
	let unsafe_all = CiphersuiteEnabled2::unsafe_any_policy();
	let mut p = SelectProtectionParameters{
		prefer_chacha: false,
		ciphersuite_mask: SetOf::<_Ciphersuite>::empty_set(),
		tls13: true,
		hw_aes: true,
		special: ProtectionSpecial {
			secure192: true,
			spinal_tap: false,
		},
	};
	p.ciphersuite_mask.add(&Ciphersuite::by_tls_id3(0x1302, unsafe_all).unwrap());
	p.ciphersuite_mask.add(&Ciphersuite::by_tls_id3(0x1301, unsafe_all).unwrap());
	let (protection, _) = select_protection(p).unwrap();
	assert_eq!(protection.as_string(), "AES-256-GCM");
}


#[test]
fn negotiate_cipher_insufficient_security()
{
	let unsafe_all = CiphersuiteEnabled2::unsafe_any_policy();
	for i in 0..2 {
		let mut p = SelectProtectionParameters{
			prefer_chacha: false,
			ciphersuite_mask: SetOf::<_Ciphersuite>::empty_set(),
			tls13: true,
			hw_aes: true,
			special: ProtectionSpecial {
				secure192: true,
				spinal_tap: true,
			},
		};
		p.ciphersuite_mask.add(&Ciphersuite::by_tls_id3(0x1301, unsafe_all).unwrap());
		p.special.spinal_tap = i & 1 != 0;
		assert!(select_protection(p).is_err());
	}
}
