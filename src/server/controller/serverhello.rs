use super::BaseMessageParamsS;
use super::Flags;
use super::HandshakeState2;
use super::Tls13HsSecrets;
use crate::callbacks::KeyExportType as KET;
use super::certificate::Tls12CertificateFields;
use super::certificate::Tls13CertificateFields;
use super::certificate::TlsCommonCertificateFields;
use crate::certificates::ServerCertificate;
use crate::common::Ciphersuite;
use crate::common::CryptoTracker;
use crate::common::test_failure;
use crate::common::tls13_update_recv_key_base;
use crate::common::tls13_update_send_key_base;
use crate::common::TlsVersion;
use crate::errors::Error;
use crate::features::FLAGS0_NO_SPONTANEOUS_REKEY;
use crate::features::FLAGS5_FORCE_TLS12;
use crate::features::FLAGS5_TEST_SH_SEND_FAIL;
use crate::messages::CertificateObject;
use crate::messages::DumpHexblock;
use crate::messages::EncryptedExtensionsParameters;
use crate::messages::HandshakeHash;
use crate::messages::ServerHelloParameters;
use crate::messages::ServerSupport;
use crate::messages::SessionId;
use crate::messages::tx_certificate_request;
use crate::messages::tx_encrypted_extensions;
use crate::messages::tx_server_hello;
use crate::record::DiffieHellmanSharedSecretResponder;
use crate::record::DowncallInterface;
use crate::record::Exportable;
use crate::record::HasAd;
use crate::record::Tls13HandshakeSecret;
use crate::stdlib::String;
use crate::stdlib::Vec;
use btls_aux_dhf::KemInitiator;
use btls_aux_dhf::KemPublicKey;
use btls_aux_random::secure_random;
use btls_aux_signatures::SignatureAlgorithmEnabled2;


pub(crate) struct ServerHelloFields
{
	pub certlookup: ServerCertificate,
	//TLS 1.3 has the OK branch, TLS 1.2 has the Err branch.
	pub maybe_secret: Result<DiffieHellmanSharedSecretResponder, (KemInitiator, KemPublicKey)>,
	pub ccommon: TlsCommonCertificateFields,
	pub chosen_ciphersuite: Ciphersuite,
	pub real_version: TlsVersion,
	pub client_random: [u8; 32],
	pub hs_hash: HandshakeHash,
	pub session_id: SessionId,
	pub enabled_sig: SignatureAlgorithmEnabled2,
	pub alpn: Option<String>,
	pub interesting_exts: Vec<u16>,
	pub flags: Flags,
	pub crypto_algs: CryptoTracker,
}

impl ServerHelloFields
{
	pub fn tx_server_hello(mut self, base: &mut impl DowncallInterface, bp: BaseMessageParamsS) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParamsS{debug, sname, config, callbacks, ssl_key_log, ..} = bp;
		test_failure(&config.flags, FLAGS5_TEST_SH_SEND_FAIL)?;
		let csuite = self.chosen_ciphersuite;
		let maybe_secret = self.maybe_secret;
		let interesting_exts = self.interesting_exts;
		let mut crypto_algs = self.crypto_algs;
		let tls13 = self.flags.is_USING_TLS_13();
		//Fixup the record version... Some stacks seem to be picky. This applies to all versions.
		base.set_use_tls12_records();
		//The delayed certificate.
		let certificate = self.certlookup;

		//Random: Also include sentinel if supporting 1.3, but offered <1.3.
		let mut server_random = [0; 32];
		secure_random(&mut server_random);
		if !tls13 && config.flags.get_test() & FLAGS5_FORCE_TLS12 == 0 {
			//Downgrade hack.
			const DOWNGRADE_SENTINEL: [u8; 8] = [0x44, 0x4F, 0x57, 0x4E, 0x47, 0x52, 0x44, 0x01];
			//server_random is 32 bytes by type.
			(&mut server_random[24..32]).copy_from_slice(&DOWNGRADE_SENTINEL);
		}
		debug!(CRYPTO_CALCS debug, "Using server random:\n{srand}",
			srand=DumpHexblock::new(&server_random, "  ", "Server random"));
		debug!(HANDSHAKE_EVENTS debug, "Chosen configuration: Using Ciphersuite {csuite:?}");
		let version_code = self.real_version.to_code();
		let cs_id = csuite.tls_id();
		let have_ocsp = certificate.ocsp().is_some();
		let maybe_send_ocsp = self.flags.is_OCSP_V1_OFFERED() && have_ocsp &&
			!self.flags.is_SEND_SPKI_ONLY() && !tls13;

		if self.flags.is_EMS_OFFERED() && !tls13 {
			crypto_algs.enable_ems();
			debug!(HANDSHAKE_EVENTS debug.clone(), "Chosen configuration: Using Extended master secret");
		}

		//If not TLS 1.3, clear the session ID, as TLS 1.2 connections are never resumed. This can
		//happen if connection with unsupported TLS 1.3 draft comes in, as there session ID is used to
		//trigger compatibility mode.
		if !tls13 { self.session_id = SessionId::blank(); }

		tx_server_hello(ServerHelloParameters {
			alpn: self.alpn.as_deref(),
			cs_id: cs_id,
			ct_offered: self.flags.is_CT_OFFERED(),
			trans_offered: self.flags.is_TRANS_OFFERED(),
			dh_secret: maybe_secret.as_ref().ok(),	//Omitted for TLS 1.2.
			ems_offered: self.flags.is_EMS_OFFERED(),
			reduced_record_size: self.flags.is_REDUCED_RECORD_SIZE(),
			maybe_send_ocsp: maybe_send_ocsp,
			//TLS 1.3 does not have these.
			sct_list: if !tls13 { certificate.scts() } else { &[] },
			trans_list: if !tls13 { certificate.trans_items() } else { &[] },
			send_spki_only: self.flags.is_SEND_SPKI_ONLY(),
			server_random: &server_random,
			session_id: self.session_id,
			tls13: tls13,
			version_code: version_code,
		}, debug.clone(), &mut crypto_algs, base, &mut self.hs_hash, sname)?;


		let ret = if tls13 {
			base.force_break()?;		//Key change in TLS 1.3 after ServerHello.
			//Set close to use severity=2.
			base.set_alerts_always_fatal();

			//Middlebox compatibility mode.
			if self.session_id.len() > 0 {
				//Send fake CCS
				base.send_clear_ccs();
			}
			let dh_secret = match maybe_secret {
				Ok(x) => x,
				Err(_) => sanity_failed!("TLS 1.3 key exchange incomplete in server hello")
			};
			crypto_algs.set_kex_kem(dh_secret.get_kem());
			//Everything except drafts 0-24 has ad.
			let draftver = version_code.get().wrapping_sub(0x7f00);
			sanity_check!(draftver >= 23, "TLS 1.3 drafts before 23 not supported");
			let has_ad = draftver > 24;
			let hs_secret = Tls13HandshakeSecret::new_server(dh_secret, csuite.get_protector(),
				csuite.get_prf2(), Exportable(config.raw_handshake), HasAd(has_ad),
				ks_debug!(&debug))?;
			let sh_hash = self.hs_hash.checkpoint();
			let (client_hs_write, server_hs_write) = hs_secret.to_traffic_secrets(&sh_hash,
				ks_debug!(&debug))?;
			if ssl_key_log {
				callbacks.ssl_key_log(KET::ServerHandshakeTrafficSecret, self.client_random,
					server_hs_write.debug_export());
				callbacks.ssl_key_log(KET::ClientHandshakeTrafficSecret, self.client_random,
					client_hs_write.debug_export());
			}
			//Note: There is a nasty special case here: If client chokes on the ServerHello message,
			//it will never switch on encryption, and will send its alert unencrypted, wheras normally
			//it will send an encrypted packet.
			tls13_update_send_key_base(base, &server_hs_write, config.raw_handshake, debug.clone(),
				callbacks)?;
			tls13_update_recv_key_base(base, &client_hs_write, config.raw_handshake, debug.clone(),
				callbacks)?;
			base.next_record_may_be_unencrypted_alert();
			let master_secret = hs_secret.to_master_secret(ks_debug!(&debug))?;
			debug!(HANDSHAKE_EVENTS debug, "Sent TLS 1.3 serverhello, downstream&upstream encrypted");

			tx_encrypted_extensions(EncryptedExtensionsParameters {
				alpn: self.alpn.as_deref(),
				flags: &config.flags,
				reduced_record_size: self.flags.is_REDUCED_RECORD_SIZE(),
				send_spki_only: self.flags.is_SEND_SPKI_ONLY(),
				interesting_exts: interesting_exts,
			}, debug.clone(), base, &mut self.hs_hash, sname, callbacks)?;

			let hs_secrets = Tls13HsSecrets{
				client_hs_write: client_hs_write,
				server_hs_write: server_hs_write,
				master_secret: master_secret,
			};
			let request_client_certificate = self.flags.is_ASK_CLIENT_CERT();
			let mut server_caps = ServerSupport::dummy();
			if request_client_certificate {
				tx_certificate_request(&[], &config.flags, self.enabled_sig, debug.clone(),
					base, &mut self.hs_hash, sname, callbacks, &mut server_caps)?;
				//Callbacks are suppressed for internal connections.
				if !base.is_internal_only() { callbacks.sent_certificate_request(); }
				debug!(HANDSHAKE_EVENTS debug, "Client certificate: Sent certificate request");
			};
			let no_spontaneous_rekey = config.flags.is_flag(FLAGS0_NO_SPONTANEOUS_REKEY);
			HandshakeState2::Tls13Certificate(Tls13CertificateFields {
				certificate: CertificateObject{
					certificate: certificate,
					sct_enabled: self.flags.is_CT_OFFERED(),
					ocsp_enabled: self.flags.is_OCSP_V1_OFFERED(),
					trans_enabled: self.flags.is_TRANS_OFFERED(),
					send_spki_only: self.flags.is_SEND_SPKI_ONLY(),
				},
				common: self.ccommon,
				hs_secrets: hs_secrets,
				hs_hash: self.hs_hash,
				crypto_algs: crypto_algs,
				server_caps: server_caps,
				client_random: self.client_random,
				flags: self.flags.fforce_REQUESTED_CC(request_client_certificate).
					fforce_NO_SPONTANEOUS_REKEY(no_spontaneous_rekey),
			})
		} else {
			let (sk, pk) = match maybe_secret {
				Ok(_) => sanity_failed!("TLS 1.2 key exchange complete in server hello?"),
				Err(f) => f
			};
			HandshakeState2::Tls12Certificate(Tls12CertificateFields {
				certificate: CertificateObject {
					certificate: certificate,
					//Use maybe_send_ocsp as value and not looser OcspV1Offered, as we do not
					//want to try sending OCSP response if we do not have one.
					ocsp_enabled: maybe_send_ocsp,
					sct_enabled: self.flags.is_CT_OFFERED(),
					trans_enabled: self.flags.is_TRANS_OFFERED(),
					send_spki_only: self.flags.is_SEND_SPKI_ONLY(),
				},
				common: self.ccommon,
				key_share: sk,
				public_key: pk,
				client_random: self.client_random,
				server_random: server_random,
				hs_hash: self.hs_hash,
				protection: csuite.get_protector(),
				prf: csuite.get_prf2(),
				crypto_algs: crypto_algs,
				flags: self.flags.fforce_EMS_ENABLED(self.flags.is_EMS_OFFERED()),
			})
		};
		Ok(ret)
	}
}
