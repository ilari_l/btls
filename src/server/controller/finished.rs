use super::BaseMessageParams;
use super::BaseMessageParamsS;
use super::Flags;
use super::HandshakeState2;
use super::Tls12ShowtimeFields;
use super::Tls13C2ndFSecrets;
use super::Tls13HsSecrets;
use super::Tls13ShowtimeFields;
use super::certificate::Tls13ClientCertificateFields;
use crate::callbacks::ClientCertificateInfo;
use crate::callbacks::KeyExportType as KET;
use crate::common::test_failure;
use crate::common::tls13_update_recv_key;
use crate::common::tls13_update_send_key_base;
use crate::errors::Error;
use crate::features::FLAGS5_TEST_CFIN_RECV_FAIL;
use crate::logging::DebugSetting;
use crate::messages::HandshakeHash;
use crate::messages::HashMessages;
use crate::messages::tx_finished;
use crate::record::DowncallInterface;
use crate::record::Tls12MasterSecret;
use crate::record::Tls12PremasterSecret;
use crate::record::UpcallReturn;


pub(crate) struct Tls12ClientFinishedFields
{
	pub master_secret: Tls12MasterSecret,	//The master secret.
	pub client_random: [u8;32],
	pub hs_hash: HandshakeHash,
}

pub(crate) struct Tls12ServerFinishedFields
{
	pub master_secret: Tls12MasterSecret,	//The master secret.
	pub hs_hash: HandshakeHash,
}

pub(crate) struct Tls13ServerFinishedFields
{
	pub hs_secrets: Tls13HsSecrets,
	pub hs_hash: HandshakeHash,
	pub(crate) server_caps: crate::messages::ServerSupport,
	pub client_random: [u8;32],
	pub flags: Flags,
}

pub(crate) struct Tls13ClientFinishedFields
{
	pub hs_secrets2: Tls13C2ndFSecrets,
	pub hs_hash: HandshakeHash,
	pub auth_as: ClientCertificateInfo,
	pub flags: Flags,
}

impl Tls13ServerFinishedFields
{
	pub fn tx_finished(self, base: &mut impl DowncallInterface, bp: BaseMessageParamsS) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParamsS{debug, sname, config, ssl_key_log, callbacks, ..} = bp;
		let mut hs_hash = self.hs_hash;
		tx_finished(&self.hs_secrets.server_hs_write, &mut hs_hash, base, debug.clone(), sname)?;
		let appkeys_hash = hs_hash.checkpoint();
		let (client_app_write,server_app_write, extractor) = self.hs_secrets.master_secret.
			to_traffic_secrets(&appkeys_hash, ks_debug!(&debug))?;
		if ssl_key_log {
			callbacks.ssl_key_log(KET::ServerApplicationTrafficSecret0, self.client_random,
				server_app_write.debug_export());
			callbacks.ssl_key_log(KET::ClientApplicationTrafficSecret0, self.client_random,
				client_app_write.debug_export());
			callbacks.ssl_key_log(KET::ExporterMasterSecret, self.client_random,
				extractor.debug_export());
		}
		tls13_update_send_key_base(base, &server_app_write, config.raw_handshake, debug.clone(), callbacks)?;
		base.change_extractor(extractor);
		debug!(HANDSHAKE_EVENTS debug, "Finished: Sent, upstream rekeyed");

		let keys = Tls13C2ndFSecrets {
			client_app_write: client_app_write,
			server_app_write: server_app_write,
			client_hs_write: self.hs_secrets.client_hs_write,
		};

		let ret = if self.flags.is_REQUESTED_CC() {
			HandshakeState2::Tls13ClientCertificate(Tls13ClientCertificateFields {
				hs_secrets2: keys,
				hs_hash: hs_hash,
				server_caps: self.server_caps,
				flags: self.flags,
			})
		} else {
			HandshakeState2::Tls13ClientFinished(Tls13ClientFinishedFields{
				hs_secrets2: keys,
				hs_hash: hs_hash,
				auth_as: ClientCertificateInfo::new_noauth(),
				flags: self.flags.fclear_HAS_AUTH(),
			})
		};
		Ok(ret)
	}
}

impl Tls13ClientFinishedFields
{
	pub fn rx_finished<'a>(self, msg: &[u8], controller: &mut UpcallReturn, bp: BaseMessageParams) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParams{rawmsg, config, debug, callbacks, ..} = bp;
		test_failure(&config.flags, FLAGS5_TEST_CFIN_RECV_FAIL)?;
		let mut hs_hash = self.hs_hash;
		self.hs_secrets2.client_hs_write.check_finished(&hs_hash.checkpoint(), msg, ks_debug!(&debug))?;
		tls13_update_recv_key(controller, &self.hs_secrets2.client_app_write, config.raw_handshake,
			debug.clone(), callbacks)?;
		debug!(HANDSHAKE_EVENTS debug, "Finished: Received, downstream rekeyed");
		//Authentication check.
		if !controller.is_internal_only() {
			fail_if!(!callbacks.client_authentication(&self.auth_as),
				error!(client_authentication_failed));
		}
		//Signal auth if any.
		if self.flags.is_HAS_AUTH() {
			let auth_as = self.auth_as;
			callbacks.client_certificate(auth_as);
		}

		hs_hash.add(rawmsg, debug)?;	//The final handshake hash. Needed for e.g. RMS.

		//Callbacks.
		if controller.is_internal_only() {
			controller.trigger_internal_service();
		}
		callbacks.set_deadline(None);	//Deadline cleared on end of handshak, even if internal.

		Ok(HandshakeState2::Tls13Showtime(Tls13ShowtimeFields{
			client_app_write: self.hs_secrets2.client_app_write,
			server_app_write: self.hs_secrets2.server_app_write,
			flags: self.flags,
		}))
	}
}

impl Tls12ClientFinishedFields
{
	pub fn rx_finished(self, msg: &[u8], controller: &mut UpcallReturn, bp: BaseMessageParams) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParams{rawmsg, debug, ssl_key_log, callbacks, ..} = bp;

		if ssl_key_log {
			callbacks.ssl_key_log(KET::Tls12MasterSecret, self.client_random,
				self.master_secret.debug_export());
		}
		//Authentication check. There never is certificate available.
		if !controller.is_internal_only() {
			fail_if!(!callbacks.client_authentication(&ClientCertificateInfo::new_noauth()),
				error!(client_authentication_failed));
		}

		let mut hs_hash = self.hs_hash;
		self.master_secret.check_finished(&hs_hash.checkpoint(), ks_debug!(&debug), msg)?;
		controller.set_extractor(self.master_secret.get_exporter(ks_debug!(&debug))?);
		//We compute EMS key in HS demux since we need hash of this message.
		hs_hash.add(rawmsg, debug.clone())?;

		Ok(HandshakeState2::Tls12ServerFinished(Tls12ServerFinishedFields {
			master_secret: self.master_secret,
			hs_hash: hs_hash,
		}))
	}
}


impl Tls12ServerFinishedFields
{
	pub fn tx_finished(self, base: &mut impl DowncallInterface, bp: BaseMessageParamsS) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParamsS{debug, sname, callbacks, ..} = bp;
		base.send_clear_ccs();
		base.change_protector(self.master_secret.get_protector(ks_debug!(&debug))?);
		debug!(HANDSHAKE_EVENTS debug, "ChangeCipherSpec protocol: Sent, upstream encrypted");
		//The CCS is not hashed according to TLS 1.2.

		let mut hs_hash = self.hs_hash;
		tx_finished(&self.master_secret, &mut hs_hash, base, debug.clone(), sname)?;

		//Callbacks.
		if base.is_internal_only() {
			base.trigger_internal_service();
		}
		callbacks.set_deadline(None);	//Deadline cleared on end of handshak, even if internal.

		Ok(HandshakeState2::Tls12Showtime(Tls12ShowtimeFields{
		}))
	}
}

pub(crate) struct Tls12ClientChangeCipherSpecFields
{
	pub premaster_secret: Tls12PremasterSecret,	//Premaster secret.
	pub client_random: [u8; 32],
	pub server_random: [u8; 32],
	pub hs_hash: HandshakeHash,
}

impl Tls12ClientChangeCipherSpecFields
{
	pub fn rx_change_cipher_spec(self, controller: &mut UpcallReturn, debug: DebugSetting) ->
		Result<HandshakeState2, Error>
	{
		//This is not true handshake message, so not added to handshake hash.
		let master_secret = Tls12MasterSecret::new(self.premaster_secret, &self.client_random,
			&self.server_random, self.hs_hash.checkpoint(), ks_debug!(&debug))?;
		controller.set_deprotector(master_secret.get_deprotector(ks_debug!(&debug))?);
		debug!(HANDSHAKE_EVENTS debug, "ChangeCipherSpec protocol: Received, downstream encrypted");

		Ok(HandshakeState2::Tls12ClientFinished(Tls12ClientFinishedFields{
			master_secret: master_secret,
			client_random: self.client_random,
			hs_hash: self.hs_hash,
		}))
	}
}
