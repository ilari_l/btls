use super::BaseMessageParams;
use super::Flags;
use super::HandshakeState2;
use super::finished::Tls12ClientChangeCipherSpecFields;
use crate::errors::Error;
use crate::messages::HandshakeHash;
use crate::messages::HashMessages;
use crate::record::DiffieHellmanSharedSecretInitiator;
use crate::record::IsEms;
use crate::record::Tls12PremasterSecret;
use crate::record::UpcallReturn;
use btls_aux_aead::ProtectorType;
use btls_aux_dhf::kem_ciphertext_check2;
use btls_aux_dhf::KemInitiator;
use btls_aux_hash::HashFunction2;
use btls_aux_tls_struct::ClientKeyExchange as PClientKeyExchange;
use btls_aux_tls_struct::ClientPublicKey;
use core::fmt::Arguments;


pub(crate) struct Tls12ClientKeyExchangeFields
{
	pub key_share: KemInitiator,
	pub client_random: [u8; 32],
	pub server_random: [u8; 32],
	pub hs_hash: HandshakeHash,
	pub protection: ProtectorType,
	pub prf: HashFunction2,
	pub flags: Flags,
}

impl Tls12ClientKeyExchangeFields
{
	pub fn rx_client_key_exchange(mut self, _controller: &mut UpcallReturn, msg: &[u8],
		bp: BaseMessageParams) -> Result<HandshakeState2, Error>
	{
		let BaseMessageParams{rawmsg, debug, ..} = bp;
		let key_share = self.key_share;
		//Parse the message.
		let mut debug_cb = |args:Arguments|debug!(TLS_EXTENSIONS debug, "Client Key Exchange: {args}");
		let kem = key_share.kem().tls_id2();
		//The ciphersuite determines the type. This is close enough, always having correct kind.
		let cs = btls_aux_tls_iana::CipherSuite::ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256;
		let pmsg = PClientKeyExchange::parse(msg, cs, kem, None, &|g,k|kem_ciphertext_check2(g,k).is_ok(),
			&mut Some(&mut debug_cb)).map_err(|e|error!(msgerr_client_key_exchange e))?;
		let point = match pmsg.get_public_key() {
			Some(ClientPublicKey::Ec{point:x}) => x,
			_ => sanity_failed!("Non-elliptic key exchange not supported"),
		};
		//We are always initiator for TLS 1.2.
		let sharedkey = DiffieHellmanSharedSecretInitiator::new(key_share, point)?;
		let premaster_secret = Tls12PremasterSecret::new_server(sharedkey, self.protection, self.prf,
			IsEms(self.flags.is_EMS_ENABLED()), ks_debug!(&debug));
		debug!(HANDSHAKE_EVENTS debug, "Client key exchange: Key exchange with {kem} ok");

		self.hs_hash.add(rawmsg, debug)?;
		Ok(HandshakeState2::Tls12ClientChangeCipherSpec(Tls12ClientChangeCipherSpecFields {
			premaster_secret: premaster_secret,
			client_random: self.client_random,
			server_random: self.server_random,
			hs_hash: self.hs_hash,
		}))
	}
}
