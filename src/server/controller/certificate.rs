use super::super::ServerConnection;
use super::BaseMessageParams;
use super::BaseMessageParamsS;
use super::encode_sni_kind;
use super::Flags;
use super::HandshakeState2;
use super::Tls13C2ndFSecrets;
use super::Tls13HsSecrets;
use super::finished::Tls13ServerFinishedFields;
use super::finished::Tls13ClientFinishedFields;
use super::clientkeyexchange::Tls12ClientKeyExchangeFields;
use crate::callbacks::ClientCertificateInfo;
use crate::callbacks::ServerNamesParameters;
use crate::callbacks::TlsCallbacks;
use crate::certificates::restricted::FastInterlock;
use crate::common::certificate_status_to_ocsp;
use crate::common::check_dynamic_spki;
use crate::common::collect_tls13_scts;
use crate::common::CryptoTracker;
use crate::common::emit_crypto_parameters_event;
use crate::common::FutureAMO;
use crate::common::make_tls13_extra;
use crate::common::OcspTag;
use crate::common::ValidateExtra;
use crate::common::X509CertMaybeParsed;
use crate::common::XValidationStatus;
use crate::errors::Error;
use crate::record::DowncallInterface;
use crate::record::UpcallReturn;
use crate::features::ConfigPages;
use crate::features::FLAGS0_ALLOW_PKCS1_CLIENT;
use crate::features::FLAGS0_ALLOW_PKCS1_SIG;
use crate::features::FLAGS0_REQUIRE_CT;
use crate::features::FLAGS0_REQUIRE_OCSP;
use crate::logging::DebugSetting;
use crate::messages::CertificateObject;
use crate::messages::DigitallySigned;
use crate::messages::DumpHexblock;
use crate::messages::HandshakeHash;
use crate::messages::HashMessages;
use crate::messages::TlsCertificateParameters;
use crate::messages::tx_certificate;
use crate::messages::tx_certificate_spki;
use crate::messages::tx_certificate_verify;
use crate::server::ServerConfiguration;
use crate::stdlib::Arc;
use crate::stdlib::Deref;
use crate::stdlib::from_utf8;
use crate::stdlib::IpAddr;
use crate::stdlib::String;
use crate::stdlib::ToOwned;
use crate::stdlib::ToString;
use crate::stdlib::Vec;
use btls_aux_aead::ProtectorType;
use btls_aux_dhf::KemInitiator;
use btls_aux_dhf::KemPublicKey;
use btls_aux_hash::HashFunction2;
use btls_aux_random::TemporaryRandomLifetimeTag;
use btls_aux_random::TemporaryRandomStream;
use btls_aux_set2::SetOf;
use btls_aux_signatures::Hasher;
use btls_aux_signatures::SignatureAlgorithm2;
use btls_aux_signatures::SignatureAlgorithmEnabled2;
use btls_aux_signatures::SignatureAlgorithmTls2;
use btls_aux_signatures::SignatureFlags2;
use btls_aux_time::Timestamp;
use btls_aux_tls_struct::CertificateStatus;
use btls_aux_tls_struct::CertificateTls13X509 as PCertificate3X;
use btls_aux_tls_struct::IntoGenericIterator as ListOf;
use btls_aux_tls_struct::SerializedSCT;
use btls_aux_x509certparse::CFLAG_MUST_STAPLE;
use btls_aux_x509certparse::CFLAG_UNKNOWN;
use btls_aux_x509certparse::IpAddressBlock;
use btls_aux_x509certparse::ParsedCertificate2;
use btls_aux_x509certvalidation::full_certificate_validation;
use btls_aux_x509certvalidation::FullCertificateValidation;
use btls_aux_x509certvalidation::HostSpecificPin;
use btls_aux_x509certvalidation::ip_in_set;
use btls_aux_x509certvalidation::name_in_set2;
use btls_aux_x509certvalidation::ReferenceName;
use btls_aux_x509certvalidation::SanityCheckCertificateChainExtended;
use core::fmt::Arguments;

macro_rules! get_cert_field
{
	(RAW $x:expr) => { $x.get_raw() };
	(KEY $x:expr) => { $x.get_key().map_err(|x|error!(cant_extract_spki x))? };
	(PARSE $x:expr) => { $x.get_certificate().map_err(|x|error!(ee_cert_parse_error x))? };
}

pub(crate) struct TlsCommonCertificateFields
{
	pub destination_ip: IpAddr,
	pub sni_name: Option<String>,
	pub peer_sigalgo_mask: SetOf<SignatureAlgorithm2>,
}

pub(crate) struct Tls12CertificateFields
{
	pub certificate: CertificateObject,
	pub common: TlsCommonCertificateFields,
	pub public_key: KemPublicKey,
	pub key_share: KemInitiator,
	pub client_random: [u8; 32],
	pub server_random: [u8; 32],
	pub hs_hash: HandshakeHash,
	pub protection: ProtectorType,
	pub prf: HashFunction2,
	pub crypto_algs: CryptoTracker,
	pub flags: Flags,
}

pub(crate) struct Tls13CertificateFields
{
	pub hs_secrets: Tls13HsSecrets,
	pub certificate: CertificateObject,
	pub common: TlsCommonCertificateFields,
	pub hs_hash: HandshakeHash,
	pub crypto_algs: CryptoTracker,
	pub(crate) server_caps: crate::messages::ServerSupport,
	pub client_random: [u8;32],
	pub flags: Flags,
}

pub(crate) struct Tls13CertificateVerifyFields
{
	hs_secrets: Tls13HsSecrets,
	signature: FutureAMO<Result<(u16, Vec<u8>), ()>>,
	hs_hash: HandshakeHash,
	peer_sigalgo_mask: SetOf<SignatureAlgorithm2>,
	crypto_algs: CryptoTracker,
	server_caps: crate::messages::ServerSupport,
	client_random: [u8;32],
	flags: Flags,
}


struct TxCertificateCommonParameters<'a>
{
	tls13: bool,
	certificate: &'a CertificateObject,
	sni_name: Option<String>,
	no_certificate_interlocks: bool,
	crypto_algs: &'a mut CryptoTracker,
	destination_ip: IpAddr,
	isrg_x1_dst_hack: bool,
}

fn call_server_names(list: Arc<Vec<String>>, list_ip: Arc<Vec<String>>, callbacks: &mut dyn TlsCallbacks,
	internal: bool)
{
	if !internal {
		callbacks.server_names(ServerNamesParameters {
			dns_names: list.deref(),
			ip_addresses: list_ip.deref(),
		});
	}
}

fn tx_do_interlocks(base: &mut impl DowncallInterface, params: &TxCertificateCommonParameters,
	config: &ServerConfiguration, ocsp: Option<&[u8]>, fast_interlock: &FastInterlock,
	callbacks: &mut dyn TlsCallbacks) -> Result<(), Error>
{
	let time_now = Timestamp::now();
	let reference_name = encode_sni_kind(params.sni_name.as_deref(), config. default_sni_name.as_deref(),
		params.destination_ip);
	fail_if!(time_now >= fast_interlock.c_expiry,
		error!(interlock_certificate_expired fast_interlock.c_expiry));
	//We need to check that the certificate is valid for host or IP.
	let invalid = match (&reference_name, &fast_interlock.names, &fast_interlock.addresses) {
		(&Some(ReferenceName::DnsName(ref refname)), names, _) =>
			!name_in_set2(names.iter().map(|x|x.deref()), refname.as_bytes()),
		(&Some(ReferenceName::Ip(ref refaddr)), _, addresses) =>
			!ip_in_set(addresses.iter().cloned(), &IpAddressBlock::from(*refaddr)),
		(_, _, _) => true
	};
	if invalid {
		let name = match reference_name {
			Some(ReferenceName::DnsName(ref refname)) => refname.to_string(),
			Some(ReferenceName::Ip(ref refaddr)) => refaddr.to_string(),
			_ => format!("<Unknown>")
		};
		fail!(error!(interlock_certificate_name name));
	}
	//Cached validation. The certificate is known good, so data can be released
	//immediately. Just skip any invalid names.
	let names = Arc::new(fast_interlock.names.iter().filter_map(|x|from_utf8(x).ok()).
		map(|x|x.to_owned()).collect());
	let addresses = Arc::new(fast_interlock.addresses.iter().map(|x|x.to_string()).collect());
	call_server_names(names, addresses, callbacks, base.is_internal_only());
	if ocsp.is_some() {
		fail_if!(time_now >= fast_interlock.o_expiry,
			error!(interlock_ocsp_expired fast_interlock.o_expiry));
	}
	Ok(())
}

fn tx_certificate_common<'a>(base: &mut impl DowncallInterface, params: TxCertificateCommonParameters<'a>,
	debug: DebugSetting, config: &ServerConfiguration, hs_hash: &mut HandshakeHash, sname: &str,
	callbacks: &mut dyn TlsCallbacks) -> Result<(), Error>
{
	let eecert = params.certificate.certificate.certificate();

	if params.certificate.send_spki_only {
		//Bypass the normal interlock checks with RPK.
		return tx_certificate_spki(eecert, params.tls13, debug.clone(), base, hs_hash, sname);
	}

	let chain = params.certificate.certificate.chain();
	let isrg_x1_dst_hack = !params.tls13 && params.certificate.certificate.hack_root_isrg_x1();
	let ocsp = params.certificate.certificate.ocsp();

	//Check if there are dedicated SCTs.
	let ded_scts = params.certificate.certificate.scts();
	let ded_trans = params.certificate.certificate.trans_items();

	//Don't sanity-check the certificate if interlocks have been disabled.
	if !params.no_certificate_interlocks {
		//If there is no interlock info, fail interlock.
		let fast_interlock = params.certificate.certificate.fast_interlock().
			ok_or_else(||error!(interlock_certificate_name format!("<invalid>")))?;
		tx_do_interlocks(base, &params, config, ocsp, fast_interlock, callbacks)?;
	}

	tx_certificate(TlsCertificateParameters {
		certificate: params.certificate,
		eecert: eecert,
		chain: chain,
		ocsp_res: ocsp,
		sct_res: ded_scts,
		trans_res: ded_trans,
		//The ISRG X1 DST hack is enabled if it is enabled by both handshake and certificate, and this
		//is TLS 1.2.
		isrg_x1_dst_hack: !params.tls13 && params.isrg_x1_dst_hack && isrg_x1_dst_hack,
		tls13: params.tls13,
	}, debug.clone(), Some(params.crypto_algs), base, hs_hash, sname)?;
	Ok(())
}

impl Tls12CertificateFields
{
	pub fn tx_certificate(self, base: &mut impl DowncallInterface, bp: BaseMessageParamsS,
		connection: ServerConnection) -> Result<HandshakeState2, Error>
	{
		let BaseMessageParamsS{debug, sname, config, callbacks, ..} = bp;
		let certificate = self.certificate;
		let mut hs_hash = self.hs_hash;
		let mut crypto_algs = self.crypto_algs;
		//We disable certificate interlocks for ACME.
		let no_certificate_interlocks = self.flags.is_IS_ACME_VALIDATION();
		tx_certificate_common(base, TxCertificateCommonParameters {
			tls13: false,
			certificate: &certificate,
			sni_name: self.common.sni_name,
			no_certificate_interlocks: no_certificate_interlocks,
			crypto_algs: &mut crypto_algs,
			destination_ip: self.common.destination_ip,
			isrg_x1_dst_hack: self.flags.is_ISRG_X1_DST_HACK(),
		}, debug.clone(), config, &mut hs_hash, sname, callbacks)?;

		//We are always the initiator, since this is TLS 1.2.
		//Sign the parameters and emit the signature.
		let kem = self.key_share.kem();
		let client_random = &self.client_random;
		let server_random = &self.server_random;
		let pk = self.public_key.as_raw();
		let mut tbs = |h: &mut Hasher|{
			let t = 0x03000000u32 + kem.tls_id() as u32 * 256 + pk.len() as u32;
			h.append(client_random);
			h.append(server_random);
			h.append(&t.to_be_bytes());
			h.append(pk);
		};
		debug!(CRYPTO_CALCS debug, "Server TBS:\n{tbs}",
			tbs=DumpHexblock::new(&Hasher::collect(&mut tbs), "  ", "TBS"));

		let lifetime = TemporaryRandomLifetimeTag;
		let signature = match certificate.certificate.sign(&mut tbs,
			&mut TemporaryRandomStream::new_system(&lifetime)).read() {
			Ok(signature) => {
				debug!(HANDSHAKE_EVENTS debug, "Server certificate: Immediate signature");
				//This also sends ServerHelloDone.
				tx_certificate_verify(signature, self.common.peer_sigalgo_mask,
					Some(&mut crypto_algs), base, &mut hs_hash, sname, Some(&self.public_key),
					debug.clone())?;
				emit_crypto_parameters_event(&crypto_algs, callbacks, base.is_internal_only());
				return Ok(HandshakeState2::Tls12ClientKeyExchange(Tls12ClientKeyExchangeFields{
					key_share: self.key_share,
					client_random: self.client_random,
					server_random: self.server_random,
					hs_hash: hs_hash,
					protection: self.protection,
					prf: self.prf,
					flags: self.flags,
				}));
			},
			//Signature was not ready immediately. Register a waiter to wait for the future.
			Err(x) => FutureAMO::new(x, connection)
		};
		debug!(HANDSHAKE_EVENTS debug, "Server certificate: Requesting signature");
		Ok(HandshakeState2::Tls12ServerKeyExchange(Tls12ServerKeyExchangeFields {
			key_share: self.key_share,
			public_key: self.public_key,
			signature: signature,
			client_random: self.client_random,
			server_random: self.server_random,
			hs_hash: hs_hash,
			protection: self.protection,
			prf: self.prf,
			peer_sigalgo_mask: self.common.peer_sigalgo_mask,
			crypto_algs: crypto_algs,
			flags: self.flags,
		}))
	}
}

impl Tls13CertificateFields
{
	pub fn tx_certificate(self, base: &mut impl DowncallInterface, bp: BaseMessageParamsS,
		connection: ServerConnection) -> Result<HandshakeState2, Error>
	{
		let BaseMessageParamsS{debug, sname, config, callbacks, ..} = bp;
		let mut certificate = self.certificate;
		let mut hs_hash = self.hs_hash;
		let mut crypto_algs = self.crypto_algs;
		//maybe_send_ocsp is always false for TLS 1.3. We disable certificate interlocks for ACME.
		let no_certificate_interlocks = self.flags.is_IS_ACME_VALIDATION();
		tx_certificate_common(base, TxCertificateCommonParameters {
			tls13: true,
			certificate: &mut certificate,
			sni_name: self.common.sni_name,
			no_certificate_interlocks: no_certificate_interlocks,
			crypto_algs: &mut crypto_algs,
			destination_ip: self.common.destination_ip,
			isrg_x1_dst_hack: false,	//Always TLS 1.3.
		}, debug.clone(), config, &mut hs_hash, sname, callbacks)?;
		//Sign the thing and emit the signature.
		let hash = hs_hash.checkpoint();
		let mut tbs = |h: &mut Hasher|{
			h.append(&[32;64]);	//Padding for randoms.
			h.append(b"TLS 1.3, server CertificateVerify\0");
			h.append(&hash);
		};
		debug!(CRYPTO_CALCS debug, "Server TBS:\n{tbs}",
			tbs=DumpHexblock::new(&Hasher::collect(&mut tbs), "  ", "TBS"));
		let lifetime = TemporaryRandomLifetimeTag;
		let signature = match certificate.certificate.sign(&mut tbs,
			&mut TemporaryRandomStream::new_system(&lifetime)).read() {
			//Immedate ready case.
			Ok(signature) => {
				debug!(HANDSHAKE_EVENTS debug, "Server certificate: Immediate signature");
				tx_certificate_verify(signature, self.common.peer_sigalgo_mask,
					Some(&mut crypto_algs), base, &mut hs_hash, sname, None, debug.clone())?;
				emit_crypto_parameters_event(&crypto_algs, callbacks, base.is_internal_only());
				return Ok(HandshakeState2::Tls13ServerFinished(Tls13ServerFinishedFields {
					hs_secrets: self.hs_secrets,
					hs_hash: hs_hash,
					server_caps: self.server_caps,
					client_random: self.client_random,
					flags: self.flags,
				}));
			},
			//Signature was not ready immediately. Register a waiter to wait for the future.
			Err(x) => FutureAMO::new(x, connection)
		};
		debug!(HANDSHAKE_EVENTS debug, "Server certificate: Requesting signature");
		Ok(HandshakeState2::Tls13CertificateVerify(Tls13CertificateVerifyFields {
			signature: signature,
			hs_secrets: self.hs_secrets,
			hs_hash: hs_hash,
			peer_sigalgo_mask: self.common.peer_sigalgo_mask,
			crypto_algs: crypto_algs,
			server_caps: self.server_caps,
			client_random: self.client_random,
			flags: self.flags,
		}))
	}
}

impl Tls13CertificateVerifyFields
{
	pub fn ready(&self) -> bool { self.signature.ready() }
	pub fn tx_certificate_verify(self, base: &mut impl DowncallInterface, bp: BaseMessageParamsS) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParamsS{debug, sname, callbacks, ..} = bp;
		//If not ready yet, wait more.
		let signature = f_return!(self.signature.take(), Ok(HandshakeState2::Tls13CertificateVerify(self)));

		let mut hs_hash = self.hs_hash;
		let mut crypto_algs = self.crypto_algs;
		tx_certificate_verify(signature, self.peer_sigalgo_mask, Some(&mut crypto_algs), base, &mut hs_hash,
			sname, None, debug.clone())?;

		emit_crypto_parameters_event(&crypto_algs, callbacks, base.is_internal_only());
		Ok(HandshakeState2::Tls13ServerFinished(Tls13ServerFinishedFields {
			hs_secrets: self.hs_secrets,
			hs_hash: hs_hash,
			server_caps: self.server_caps,
			client_random: self.client_random,
			flags: self.flags,
		}))
	}
}

//Macro because of lifetime rules.
macro_rules! client_cert_msg
{
	($debug:expr) => { |ev:Arguments|debug!(HANDSHAKE_EVENTS $debug, "Client certificate: {ev}") }
}

pub(crate) struct Tls13ClientCertificateFields
{
	pub hs_secrets2: Tls13C2ndFSecrets,
	pub hs_hash: HandshakeHash,
	pub(crate) server_caps: crate::messages::ServerSupport,
	pub flags: Flags,
}

#[derive(Copy,Clone)]
struct ValidationResult
{
	ocsp: bool,
	sct: bool,
}

//Return is (need_ocsp,need_sct).
fn __check_certificate_only(eecert: &ParsedCertificate2, issuers: &[&[u8]], config: &ServerConfiguration,
	enabled_sig: SignatureAlgorithmEnabled2) ->
	Result<(bool, bool), Error>
{
	static BLANK: [HostSpecificPin;0] = [];
	//Assert STAPLE as that is checked later.
	let feature_flags = CFLAG_MUST_STAPLE | CFLAG_UNKNOWN;
	let mut need_ocsp = config.flags.is_flag(FLAGS0_REQUIRE_OCSP);
	let mut need_sct = config.flags.is_flag(FLAGS0_REQUIRE_CT);
	let vparam = FullCertificateValidation {
		strict_algo: false,
		policy: enabled_sig,
		is_client: true,
		feature_flags: feature_flags,
		need_ocsp_flag: &mut need_ocsp,
		need_sct_flag: &mut need_sct,
		killist: &config.killist,
		require_disclosed_ca: false,	//Not supported on server side.
		issuers: issuers,
		reference: None,
		time_now: &Timestamp::now(),
		trust_anchors: &config.trust_anchors,
		pins: &BLANK[..],
		extended: SanityCheckCertificateChainExtended::default()
	};
	full_certificate_validation(eecert, vparam).map_err(|x|error!(certificate_validation_failed x))?;
	Ok((need_ocsp, need_sct))
}

fn _validate_certificate_chain<'a,CB>(eecert: &ParsedCertificate2<'a>, issuers: &[&[u8]],
	ocsp: Option<CertificateStatus>, scts: ListOf<'a,SerializedSCT<'a>>, config: &ServerConfiguration,
	time_now: Timestamp, debug: &CB, enabled_sig: SignatureAlgorithmEnabled2) -> Result<ValidationResult, Error>
	where CB: Fn(Arguments)
{
	let (need_ocsp, need_sct) = __check_certificate_only(eecert, issuers, config, enabled_sig)?;
	//Validate SCT&OCSP. Note that validate_extra dumps the SCTs from OCSP.
	let vextra = ValidateExtra::with_ocsp(XValidationStatus::<OcspTag>::new(need_ocsp),
		XValidationStatus::new(need_sct), enabled_sig, time_now);
	let staple_scts = collect_tls13_scts(eecert, scts, debug);
	let ocsp = certificate_status_to_ocsp(ocsp);
	let extra = make_tls13_extra(eecert, issuers, &config.trust_anchors, &config.trusted_logs,
		config.ocsp_maxvalid);
	let vextra = vextra.close(ocsp, staple_scts, extra, None, debug)?;

	//OK.
	Ok(ValidationResult{
		ocsp: vextra.is_ocsp_validated(),
		sct: vextra.is_sct_validated(),
	})
}


fn validate_certificate_chain<'a,CB>(eecert: &ParsedCertificate2<'a>, issuers: &[&[u8]], 
	ocsp: Option<CertificateStatus>, scts: ListOf<'a,SerializedSCT<'a>>, config: &ServerConfiguration,
	time_now: Timestamp, debug: CB, enabled_sig: SignatureAlgorithmEnabled2) ->
	Option<ValidationResult> where CB: Fn(Arguments)
{
	match _validate_certificate_chain(eecert, issuers, ocsp, scts, config, time_now, &debug, enabled_sig) {
		Ok(x) => { debug(format_args!("verify ok")); Some(x) },
		Err(err) => { debug(format_args!("verify error: {err}")); None }
	}
}

macro_rules! unwrap_tls13_cert
{
	($x:expr) => {{
		let x = $x.map_err(|e|error!(msgerr_client_certificate e))?;
		fail_if!(x.get_request_context().len() > 0, error!(cr_context_must_be_empty));
		x.get_certificate_chain()
	}}
}

impl Tls13ClientCertificateFields
{
	pub fn rx_certificate_2(mut self, _controller: &mut UpcallReturn, msg: &[u8], bp: BaseMessageParams) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParams{rawmsg, debug, config, ..} = bp;
		self.hs_hash.add(rawmsg, debug.clone())?;
		let time_now = Timestamp::now();
		let enable = |f:btls_aux_tls_struct::RemoteFeature|self.server_caps.enable(config, f);
		let mut debug_cb = |args:Arguments|debug!(TLS_EXTENSIONS debug, "Client Certificate: {args}");
		let pmsg = unwrap_tls13_cert!(PCertificate3X::parse(msg, &enable, &mut Some(&mut debug_cb)));
		let debug = client_cert_msg!(debug);
		let (key, auth_as) = match pmsg {
			Some(m) => {
				let mut certificate = X509CertMaybeParsed::new(m.get_end_entity());
				let rawcert = get_cert_field!(RAW certificate);
				//If requiring floaty keys, check that SPKI algorithm is not RsaEncryption.
				let key = get_cert_field!(KEY certificate);
				check_dynamic_spki(key, &config.flags)?;
				//Do soft parse. On error, do not abort as the certifiate might not be needed.
				let eecert = certificate.get_certificate().ok();
				let issuers: Vec<&[u8]> = m.get_issuer_chain().iter().
					map(|x|x.get_certificate()).collect();
				//Skip this if cert parsing failed.
				let val_res = eecert.as_ref().and_then(|eecert|{
					validate_certificate_chain(eecert, &issuers, m.get_status(),
						m.get_signed_certificate_timestamps(), config, time_now, &debug,
						self.server_caps.get_enabled_sig())
				});
				let auth_as = if let (&Some(ref eecert), Some(val_res)) = (&eecert, val_res) {
					ClientCertificateInfo::new_full_auth(eecert, val_res.ocsp, val_res.sct)
				} else {
					ClientCertificateInfo::new_keyonly(rawcert, key)
				};
				(key, auth_as)
			},
			None => {
				//Client NAKed authentication. Proceed directly to Finished message.
				debug(format_args!("Client refused authentication"));
				return Ok(HandshakeState2::Tls13ClientFinished(Tls13ClientFinishedFields {
					hs_secrets2: self.hs_secrets2,
					hs_hash: self.hs_hash,
					auth_as: ClientCertificateInfo::new_noauth(),
					flags: self.flags.fset_HAS_AUTH(),
				}));
			}
		};
		Ok(HandshakeState2::Tls13ClientCertificateVerify(Tls13ClientCertificateVerifyFields {
			client_spki: key.to_owned(),
			hs_secrets2: self.hs_secrets2,
			hs_hash: self.hs_hash,
			auth_as: auth_as,
			server_caps: self.server_caps,
			flags: self.flags,
		}))
	}
}

fn rsa_restrictions_from_config(flags: &ConfigPages) -> SignatureFlags2
{
	//No need to check FLAGS0_ONLY_FLOATY_KEYS or FLAGS0_ALLOW_DYNAMIC_RSA here, because it was already
	//done at certificate receive time.
	if flags.is_flag(FLAGS0_ALLOW_PKCS1_SIG) {
		//PKCS#1 is allowed, allow all RSA.
		SignatureFlags2::ALLOW_RSA_PKCS1
	} else if flags.is_flag(FLAGS0_ALLOW_PKCS1_CLIENT) {
		//Client certifiate RSA PKCS#1 allowed, allow RSA-PSS + CCERT.
		SignatureFlags2::ALLOW_RSA_PKCS1_CC
	} else {
		//Allow just PSS.
		SignatureFlags2::ALLOW_RSA_PSS
	}
}

pub(crate) struct Tls13ClientCertificateVerifyFields
{
	pub hs_secrets2: Tls13C2ndFSecrets,
	pub hs_hash: HandshakeHash,
	pub client_spki: Vec<u8>,
	auth_as: ClientCertificateInfo,
	server_caps: crate::messages::ServerSupport,
	pub flags: Flags,
}

impl Tls13ClientCertificateVerifyFields
{
	//Handle TLS 1.3 CertificateVerify message.
	pub fn rx_certificate_verify(self, _controller: &mut UpcallReturn, msg: &[u8],
		bp: BaseMessageParams) -> Result<HandshakeState2, Error>
	{
		let BaseMessageParams{rawmsg, debug, config, ..} = bp;

		let enable = |f:btls_aux_tls_struct::RemoteFeature|self.server_caps.enable(config, f);
		let mut debug_cb = |args:Arguments|debug!(TLS_EXTENSIONS debug, "Certificate Verify: {args}");
		let pmsg = btls_aux_tls_struct::parse_certificate_verify(msg, &enable, None,
			&mut Some(&mut debug_cb)).map_err(|e|error!(msgerr_certificate_verify(e)))?;
		let sig_algorithm = pmsg.get_algorithm();
		let pmsg_algorithm = assert_some!(SignatureAlgorithmTls2::by_tls_id2(sig_algorithm), F
			"Parsed certificate_verify uses bad algorithm {sig_algorithm}");
		let cvmsg_signature = DigitallySigned {
			algorithm: pmsg_algorithm,
			signature: pmsg.get_signature(),
		};

		//No need to check FLAGS0_ONLY_FLOATY_KEYS or FLAGS0_ALLOW_DYNAMIC_RSA here, because it was already
		//done at certificate receive time.
		let flags = rsa_restrictions_from_config(&config.flags);

		//Format TBS and verify signature.
		let cv_hash = self.hs_hash.checkpoint();
		let mut tbs = |h: &mut Hasher|{
			h.append(&[32;64]);	//Padding for randoms.
			h.append(b"TLS 1.3, client CertificateVerify\0");
			h.append(&cv_hash);
		};
		debug!(CRYPTO_CALCS debug, "Client TBS:\n{tbs}",
			tbs=DumpHexblock::new(&Hasher::collect(&mut tbs), "  ", "TBS"));
		cvmsg_signature.verify_callback(&self.client_spki, &mut tbs, self.server_caps.get_enabled_sig(),
			flags).map_err(|x|error!(cv_signature_failed x))?;
		debug!(HANDSHAKE_EVENTS debug, "Client certificate: signature {sig_algorithm}, ok");

		let mut hs_hash = self.hs_hash;
		hs_hash.add(rawmsg, debug)?;
		Ok(HandshakeState2::Tls13ClientFinished(Tls13ClientFinishedFields{
			hs_secrets2: self.hs_secrets2,
			hs_hash: hs_hash,
			auth_as: self.auth_as,
			flags: self.flags.fset_HAS_AUTH(),
		}))
	}
}

pub(crate) struct Tls12ServerKeyExchangeFields
{
	pub public_key: KemPublicKey,
	pub signature: FutureAMO<Result<(u16, Vec<u8>), ()>>,
	pub key_share: KemInitiator,
	pub client_random: [u8; 32],
	pub server_random: [u8; 32],
	pub hs_hash: HandshakeHash,
	pub protection: ProtectorType,
	pub prf: HashFunction2,
	pub peer_sigalgo_mask: SetOf<SignatureAlgorithm2>,
	pub crypto_algs: CryptoTracker,
	pub flags: Flags,
}

impl Tls12ServerKeyExchangeFields
{
	pub fn ready(&self) -> bool { self.signature.ready() }
	pub fn tx_server_key_exchange(self, base: &mut impl DowncallInterface, bp: BaseMessageParamsS) ->
		Result<HandshakeState2, Error>
	{
		let BaseMessageParamsS{debug, sname, callbacks, ..} = bp;
		//If not ready yet, wait more.
		let signature = f_return!(self.signature.take(), Ok(HandshakeState2::Tls12ServerKeyExchange(self)));

		//This also sends ServerHelloDone.
		let mut hs_hash = self.hs_hash;
		let mut crypto_algs = self.crypto_algs;
		tx_certificate_verify(signature, self.peer_sigalgo_mask, Some(&mut crypto_algs), base,
			&mut hs_hash, sname, Some(&self.public_key), debug.clone())?;

		emit_crypto_parameters_event(&crypto_algs, callbacks, base.is_internal_only());
		Ok(HandshakeState2::Tls12ClientKeyExchange(Tls12ClientKeyExchangeFields{
			key_share: self.key_share,
			client_random: self.client_random,
			server_random: self.server_random,
			hs_hash: hs_hash,
			protection: self.protection,
			prf: self.prf,
			flags: self.flags,
		}))
	}
}
