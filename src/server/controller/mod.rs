use self::certificate::Tls12CertificateFields;
use self::certificate::Tls12ServerKeyExchangeFields;
use self::certificate::Tls13CertificateFields;
use self::certificate::Tls13CertificateVerifyFields;
use self::certificate::Tls13ClientCertificateFields;
use self::certificate::Tls13ClientCertificateVerifyFields;
use self::clienthello::ClientHelloFields;
pub use self::clienthello::RestartAuthKeys;
use self::clientkeyexchange::Tls12ClientKeyExchangeFields;
use self::finished::Tls12ClientChangeCipherSpecFields;
use self::finished::Tls12ClientFinishedFields;
use self::finished::Tls12ServerFinishedFields;
use self::finished::Tls13ClientFinishedFields;
use self::finished::Tls13ServerFinishedFields;
use self::serverhello::ServerHelloFields;
use super::ClientCertificateSupport;
use super::ServerConfiguration;
use super::ServerConnection;
use crate::callbacks::TlsCallbacks;
use crate::common::Ciphersuite;
use crate::common::CryptoTracker;
use crate::common::tls13_update_recv_key;
use crate::common::tls13_update_send_key;
use crate::errors::Error;
use crate::features::get_algs_enabled_f;
use crate::logging::DebugSetting;
use crate::messages::HandshakeMessage;
use crate::record::DowncallInterface;
use crate::record::Tls13MasterSecret;
use crate::record::Tls13TrafficSecretIn;
use crate::record::Tls13TrafficSecretOut;
use crate::record::UpcallInterface;
use crate::record::UpcallReturn;
use crate::stdlib::Box;
use crate::stdlib::Cow;
use crate::stdlib::DerefMut;
use crate::stdlib::IpAddr;
use crate::stdlib::Ipv6Addr;
use crate::stdlib::replace;
use crate::test::downgrade_to_ipv4;
use btls_aux_aead::ProtectorType;
use btls_aux_bitflags::Bitflags;
use btls_aux_dhf::KemEnabled;
use btls_aux_hash::HashFunction2;
use btls_aux_ip::IpAddress;
use btls_aux_set2::SetOf;
use btls_aux_signatures::SignatureAlgorithmEnabled2;
pub use btls_aux_time::PointInTime;
pub use btls_aux_time::TimeUnit;
use btls_aux_tls_iana::HandshakeType;
use btls_aux_tls_struct::parse_key_update;
use btls_aux_tlsciphersuites::Ciphersuite as _Ciphersuite;
use btls_aux_tlsciphersuites::CiphersuiteEnabled2;
use btls_aux_tlsciphersuites::KeyExchangeType;
use btls_aux_x509certvalidation::ReferenceName;
use core::fmt::Arguments;

mod certificate;
mod clienthello;
mod clientkeyexchange;
mod finished;
mod serverhello;

#[derive(Copy,Clone,Debug,Bitflags)]
//If set, this connection is for ACME validation. Set in ClientHello.
#[bitflag(IS_ACME_VALIDATION)]
//If set, only offer ciphers with at least 192 bits of security.
#[bitflag(SECURE_192)]
//If set, only offer TLS 1.3.
#[bitflag(TLS_13_ONLY)]
//If set, The client has offered OCSP support. Set in ClientHello.
#[bitflag(OCSP_V1_OFFERED)]
//If set, The client has offered CT support. Set in ClientHello.
#[bitflag(CT_OFFERED)]
//If set, The client has offered reduced record size. Set in ClientHello.
#[bitflag(REDUCED_RECORD_SIZE)]
//If set, The client has offered EMS. Set in ClientHello.
#[bitflag(EMS_OFFERED)]
//If set, Send SPKI (Raw Public Key) to the client. Set in ClientHello.
#[bitflag(SEND_SPKI_ONLY)]
//If set, Will request for client certificate (in TLS 1.3). Set from outside.
#[bitflag(ASK_CLIENT_CERT)]
//If set, TLS 1.3 is in use. Set in ClientHello.
#[bitflag(USING_TLS_13)]
//If set, chose highest supported security.
#[bitflag(SPINAL_TAP)]
//No android compat. hack.
#[bitflag(NO_ANDROID_HACK)]
//Force NSA mode.
#[bitflag(FORCE_NSA_MODE)]
//If set, The client has offered Transparency Info support. Set in ClientHello.
#[bitflag(TRANS_OFFERED)]
//If set, EMS is in use. Only used in TLS 1.2, Set in ServerHello.
#[bitflag(EMS_ENABLED)]
//ISRG X1 DST hack.
#[bitflag(ISRG_X1_DST_HACK)]
//If set, the server requested for client certificate. Only used in TLS 1.3, set in ServerHello.
#[bitflag(REQUESTED_CC)]
//If set, the server received client certificate. Only used in TLS 1.3, set in Certificate.
#[bitflag(HAS_AUTH)]
//If set, Do not generate spontaneous rekey events. Only used in TLS 1.3, set in ServerHello.
#[bitflag(NO_SPONTANEOUS_REKEY)]
//If set, Rekey event is in progress. Only used in TLS 1.3, set in KeyUpdate.
#[bitflag(REKEY_IN_PROGRESS)]
pub struct Flags(u8, u8, u8, u8);


pub struct Tls13HsSecrets
{
	server_hs_write: Tls13TrafficSecretOut,
	client_hs_write: Tls13TrafficSecretIn,
	master_secret: Tls13MasterSecret,
}

pub struct Tls13C2ndFSecrets
{
	server_app_write: Tls13TrafficSecretOut,
	client_app_write: Tls13TrafficSecretIn,
	client_hs_write: Tls13TrafficSecretIn,
}

pub struct Tls12ShowtimeFields
{
}

pub struct Tls13ShowtimeFields
{
	server_app_write: Tls13TrafficSecretOut,
	client_app_write: Tls13TrafficSecretIn,
	flags: Flags,
}

pub(crate) enum HandshakeState2
{
	ClientHello(ClientHelloFields),
	ServerHello(ServerHelloFields),
	Tls12Certificate(Tls12CertificateFields),
	Tls12ServerKeyExchange(Tls12ServerKeyExchangeFields),
	Tls12ClientKeyExchange(Tls12ClientKeyExchangeFields),
	Tls12ClientChangeCipherSpec(Tls12ClientChangeCipherSpecFields),
	Tls12ClientFinished(Tls12ClientFinishedFields),
	Tls12ServerFinished(Tls12ServerFinishedFields),
	Tls12Showtime(Tls12ShowtimeFields),
	Tls13Certificate(Tls13CertificateFields),
	Tls13CertificateVerify(Tls13CertificateVerifyFields),
	Tls13ServerFinished(Tls13ServerFinishedFields),
	Tls13ClientCertificate(Tls13ClientCertificateFields),
	Tls13ClientCertificateVerify(Tls13ClientCertificateVerifyFields),
	Tls13ClientFinished(Tls13ClientFinishedFields),
	Tls13Showtime(Tls13ShowtimeFields),
	Error,
}

#[test]
fn test_handshakestate()
{
	assert_eq!(crate::stdlib::size_of::<HandshakeState2>(), 1056);
	assert_eq!(crate::stdlib::size_of::<ClientHelloFields>(), 72);
	assert_eq!(crate::stdlib::size_of::<ServerHelloFields>(), 1048);
	assert_eq!(crate::stdlib::size_of::<Tls12CertificateFields>(), 984);
	assert_eq!(crate::stdlib::size_of::<Tls12ServerKeyExchangeFields>(), 848);
	assert_eq!(crate::stdlib::size_of::<Tls12ClientKeyExchangeFields>(), 688);
	assert_eq!(crate::stdlib::size_of::<Tls12ClientChangeCipherSpecFields>(), 592);
	assert_eq!(crate::stdlib::size_of::<Tls12ClientFinishedFields>(), 504);
	assert_eq!(crate::stdlib::size_of::<Tls12ServerFinishedFields>(), 472);
	assert_eq!(crate::stdlib::size_of::<Tls12ShowtimeFields>(), 0);
	assert_eq!(crate::stdlib::size_of::<Tls13CertificateFields>(), 792);
	assert_eq!(crate::stdlib::size_of::<Tls13CertificateVerifyFields>(), 656);
	assert_eq!(crate::stdlib::size_of::<Tls13ServerFinishedFields>(), 632);
	assert_eq!(crate::stdlib::size_of::<Tls13ClientCertificateFields>(), 600);
	assert_eq!(crate::stdlib::size_of::<Tls13ClientCertificateVerifyFields>(), 688);
	assert_eq!(crate::stdlib::size_of::<Tls13ClientFinishedFields>(), 632);
	assert_eq!(crate::stdlib::size_of::<Tls13ShowtimeFields>(), 145);
}

impl HandshakeState2
{
	fn name(&self) -> &'static str
	{
		match self {
			&HandshakeState2::ClientHello(_) => "ClientHello",
			&HandshakeState2::ServerHello(_) => "ServerHello",
			&HandshakeState2::Tls12Certificate(_) => "Tls12Certificate",
			&HandshakeState2::Tls12ServerKeyExchange(_) => "Tls12ServerKeyExchange",
			&HandshakeState2::Tls12ClientKeyExchange(_) => "Tls12ClientKeyExchange",
			&HandshakeState2::Tls12ClientChangeCipherSpec(_) => "Tls12ClientChangeCipherSpec",
			&HandshakeState2::Tls12ClientFinished(_) => "Tls12ClientFinished",
			&HandshakeState2::Tls12ServerFinished(_) => "Tls12ServerFinished",
			&HandshakeState2::Tls12Showtime(_) => "Tls12Showtime",
			&HandshakeState2::Tls13Certificate(_) => "Tls13Certificate",
			&HandshakeState2::Tls13CertificateVerify(_) => "Tls13CertificateVerify",
			&HandshakeState2::Tls13ServerFinished(_) => "Tls13ServerFinished",
			&HandshakeState2::Tls13ClientCertificate(_) => "Tls13ClientCertificate",
			&HandshakeState2::Tls13ClientCertificateVerify(_) => "Tls13ClientCertificateVerify",
			&HandshakeState2::Tls13ClientFinished(_) => "Tls13ClientFinished",
			&HandshakeState2::Tls13Showtime(_) => "Tls13Showtime",
			&HandshakeState2::Error => "Error",
		}
	}
}

pub struct BaseMessageParams<'a,'b, 'c>
{
	rawmsg: HandshakeMessage<'a>,
	debug: DebugSetting,
	config: &'b mut ServerConfiguration,
	ssl_key_log: bool,
	callbacks: &'c mut dyn TlsCallbacks,
	_dummy: (),
}

pub struct BaseMessageParamsS<'a,'b,'e>
{
	debug: DebugSetting,
	config: &'a ServerConfiguration,
	sname: &'b str,
	ssl_key_log: bool,
	callbacks: &'e mut dyn TlsCallbacks,
	_dummy: (),
}

pub(crate) struct HandshakeController
{
	pub callbacks: Box<dyn TlsCallbacks+Send>,
	pub config: ServerConfiguration,
	hs_state: HandshakeState2,
	ssl_key_log: bool,
	cc_support: ClientCertificateSupport,
	pub debug: DebugSetting,
}

fn _illegal_state() -> Result<Error, Error>
{
	sanity_failed!("Attempt to TX in illegal state!");
}

fn illegal_state() -> Error { match _illegal_state() { Ok(x) => x, Err(x) => x } }

impl UpcallInterface for HandshakeController
{
	type Waker = ServerConnection;
	fn _set_cid(&mut self, cid: u64) { self.debug.set_cid(cid); }
	fn _get_state_name(&self) -> &'static str { self.hs_state.name() }
	fn _recv_handshake(&mut self, htype: HandshakeType, data: &[u8], controller: &mut UpcallReturn,
		last: bool) -> Result<(), Error>
	{
		let debug = self.debug.clone();
		let raw_msg2 = HandshakeMessage::new(htype, data);
		let baseparam = BaseMessageParams{
			rawmsg: raw_msg2,
			debug: debug.clone(),
			config: &mut self.config,
			ssl_key_log: self.ssl_key_log,
			callbacks: self.callbacks.deref_mut(),
			_dummy: ()
		};
		self.hs_state = match (replace(&mut self.hs_state, HandshakeState2::Error), htype) {
			(HandshakeState2::ClientHello(x), HandshakeType::CLIENT_HELLO) => {
				//ClientHello.
				x.rx_client_hello_2(controller, data, baseparam, &mut self.cc_support)?
			},
			(HandshakeState2::Tls12ClientKeyExchange(x), HandshakeType::CLIENT_KEY_EXCHANGE) => {
				x.rx_client_key_exchange(controller, data, baseparam)?
			},
			(HandshakeState2::Tls12ClientFinished(x), HandshakeType::FINISHED) => {
				//Finished
				fail_if!(!last, error!(finished_not_aligned));
				x.rx_finished(data, controller, baseparam)?
			},
			(HandshakeState2::Tls13ClientCertificate(x), HandshakeType::CERTIFICATE) => {
				//Client certificates are never RPK.
				x.rx_certificate_2(controller, data, baseparam)?
			},
			(HandshakeState2::Tls13ClientCertificateVerify(x), HandshakeType::CERTIFICATE_VERIFY) => {
				x.rx_certificate_verify(controller,data,baseparam)?
			},
			(HandshakeState2::Tls13ClientFinished(x), HandshakeType::FINISHED) => {
				//Finished
				fail_if!(!last, error!(finished_not_aligned));
				x.rx_finished(data, controller, baseparam)?
			},
			(HandshakeState2::Tls13Showtime(mut x), HandshakeType::KEY_UPDATE) => {
				fail_if!(!last, error!(key_update_not_aligned));
				let debug = baseparam.debug.clone();
				let mut debug_cb = |args:Arguments|{
					debug!(TLS_EXTENSIONS debug, "Key Update: {args}");
				};
				let requested = parse_key_update(data, &mut Some(&mut debug_cb)).
					map_err(|e|error!(msgerr_key_update e))?;
				x.client_app_write.rachet(ks_debug!(&debug))?;
				tls13_update_recv_key(controller, &x.client_app_write,
					baseparam.config.raw_handshake, debug.clone(), baseparam.callbacks)?;
				debug!(HANDSHAKE_EVENTS debug, "Rekey message: Downstream rekeyed");
				if !controller.last_tx_key_update() && !x.flags.is_REKEY_IN_PROGRESS() &&
					requested {
					//These have to be in this order for the handshake to go out with old keys.
					controller.send_key_update(false, "Tls13Showtime");
					x.server_app_write.rachet(ks_debug!(&debug))?;
					tls13_update_send_key(controller, &x.server_app_write,
						baseparam.config.raw_handshake, debug.clone(), baseparam.callbacks)?;
					debug!(HANDSHAKE_EVENTS debug,
						"Rekey message: Upstream rekeyed (reciproal)");
				}
				x.flags.clear_REKEY_IN_PROGRESS();
				HandshakeState2::Tls13Showtime(x)
			}
			(state, htype2) => fail!(error!(unexpected_handshake_message htype2, state.name()))
		};
		Ok(())
	}
	fn _recv_change_cipher_spec(&mut self, controller: &mut UpcallReturn) -> Result<(), Error>
	{
		//ChangeCipherSpec.
		self.hs_state = match replace(&mut self.hs_state, HandshakeState2::Error) {
			HandshakeState2::Tls12ClientChangeCipherSpec(x) =>
				x.rx_change_cipher_spec(controller, self.debug.clone())?,
			//TLS 1.3 compatibility mode.
			HandshakeState2::ClientHello(x) => HandshakeState2::ClientHello(x),
			//CCS is illegal in other states.
			x => fail!(error!(unexpected_ccs x.name()))
		};
		Ok(())
	}
	fn handle_tx_update(&mut self, controller: &mut UpcallReturn) -> Result<(), Error>
	{
		let debug = self.debug.clone();
		//These have to be in this order for the handshake to go out with old keys.
		if let &mut HandshakeState2::Tls13Showtime(ref mut x) = &mut self.hs_state {
			//If no spontaneous rekeys flag is set, abort the rekey.
			if x.flags.is_NO_SPONTANEOUS_REKEY() { return Ok(()); }
			controller.send_key_update(true, "Tls13Showtime");
			x.server_app_write.rachet(ks_debug!(&debug))?;
			tls13_update_send_key(controller, &x.server_app_write,
				self.config.raw_handshake, debug.clone(), self.callbacks.deref_mut())?;
			x.flags.set_REKEY_IN_PROGRESS();
			debug!(HANDSHAKE_EVENTS debug, "Rekey message: Upstream rekeyed (spontaneous)");
		}
		Ok(())
	}
	fn _get_callbacks<'a>(&'a mut self) -> &'a mut dyn TlsCallbacks { self.callbacks.deref_mut() }
	fn wants_tx(&self) -> bool
	{
		//If sending state, call the ready() method to check if it is ready or not.
		match &self.hs_state {
			&HandshakeState2::ServerHello(_) => true,
			&HandshakeState2::Tls12Certificate(_) => true,
			&HandshakeState2::Tls12ServerKeyExchange(ref x) => x.ready(),
			&HandshakeState2::Tls12ServerFinished(_) => true,
			&HandshakeState2::Tls13Certificate(_) => true,
			&HandshakeState2::Tls13CertificateVerify(ref x) => x.ready(),
			&HandshakeState2::Tls13ServerFinished(_) => true,
			_ => false,
		}
	}
	fn wants_rx(&self) -> bool
	{
		match &self.hs_state {
			&HandshakeState2::ClientHello(_) => true,
			&HandshakeState2::Tls12ClientKeyExchange(_) => true,
			&HandshakeState2::Tls12ClientChangeCipherSpec(_) => true,
			&HandshakeState2::Tls12ClientFinished(_) => true,
			&HandshakeState2::Tls13ClientCertificate(_) => true,
			&HandshakeState2::Tls13ClientCertificateVerify(_) => true,
			&HandshakeState2::Tls13ClientFinished(_) => true,
			_ => false,
		}
	}
	fn is_blocked(&self) -> bool
	{
		//The blocked states are thos with ready() method, returning false.
		match &self.hs_state {
			&HandshakeState2::Tls12ServerKeyExchange(ref x) => !x.ready(),
			&HandshakeState2::Tls13CertificateVerify(ref x) => !x.ready(),
			_ => false,
		}
	}
	fn queue_hs_tx(&mut self, base: &mut impl DowncallInterface, upper: ServerConnection) -> Result<bool, Error>
	{
		let ret = match &self.hs_state {
			&HandshakeState2::Tls12ServerFinished(_) => true,
			&HandshakeState2::Tls13ServerFinished(_) => true,
			_ => false
		};
		let debug = self.debug.clone();
		let sname = self.hs_state.name();
		self.hs_state = {
			let baseparam = BaseMessageParamsS{
				sname: sname,
				debug: debug.clone(),
				config: &self.config,
				ssl_key_log: self.ssl_key_log,
				callbacks: self.callbacks.deref_mut(),
				_dummy: ()
			};
			match replace(&mut self.hs_state, HandshakeState2::Error) {
				HandshakeState2::ServerHello(x) => x.tx_server_hello(base, baseparam),
				HandshakeState2::Tls12Certificate(x) => x.tx_certificate(base, baseparam, upper),
				HandshakeState2::Tls12ServerKeyExchange(x) => x.tx_server_key_exchange(base,
					baseparam),
				HandshakeState2::Tls12ServerFinished(x) => x.tx_finished(base, baseparam),
				HandshakeState2::Tls13Certificate(x) => x.tx_certificate(base, baseparam, upper),
				HandshakeState2::Tls13CertificateVerify(x) => x.tx_certificate_verify(base,
					baseparam),
				HandshakeState2::Tls13ServerFinished(x) => x.tx_finished(base, baseparam),
				_ => Err(illegal_state())
			}
		}?;
		Ok(ret)
	}
}

impl HandshakeController
{
	#[allow(deprecated)]
	pub fn new(config: &ServerConfiguration, debug: DebugSetting,
		mut callbacks: Box<dyn TlsCallbacks+Send+'static>) -> HandshakeController
	{
		let (enabled_kem, enabled_sigx, enabled_cs) = Self::get_algs_enabled(config);
		HandshakeController::handle_deadline(config, callbacks.deref_mut());
		HandshakeController{
			callbacks: callbacks,
			config: config.clone(),
			hs_state: HandshakeState2::ClientHello(ClientHelloFields{
				restart_auth_keys: config.restart_auth_keys.clone(),
				stateless: config.stateless,
				source_ip: IpAddr::V6(Ipv6Addr::from([0;16])),
				destination_ip: IpAddr::V6(Ipv6Addr::from([0;16])),
				enabled_kem: enabled_kem,
				enabled_sig: enabled_sigx,
				enabled_cs: enabled_cs,
				crypto_algs: CryptoTracker::new(),
				flags: Flags::none,
			}),
			ssl_key_log: false,
			cc_support: ClientCertificateSupport::new(),
			debug: debug.clone(),
		}
	}
	//Get algorithms enabled.
	fn get_algs_enabled(config: &ServerConfiguration) ->
		(KemEnabled, SignatureAlgorithmEnabled2, CiphersuiteEnabled2)
	{
		get_algs_enabled_f(&config.flags)
	}
	pub fn get_client_supported_certificates(&self) -> ClientCertificateSupport { self.cc_support.clone() }
	pub fn set_source_address(&mut self, addr: IpAddr) -> Result<(), Error>
	{
		match &mut self.hs_state {
			&mut HandshakeState2::ClientHello(ref mut x) => Ok(x.source_ip = addr),
			_ => sanity_failed!("Can not set addresses of connection after sending data"),
		}
	}
	pub fn set_destination_address(&mut self, addr: IpAddr) -> Result<(), Error>
	{
		let addr = downgrade_to_ipv4(addr);
		match &mut self.hs_state {
			&mut HandshakeState2::ClientHello(ref mut x) => Ok(x.destination_ip = addr),
			_ => sanity_failed!("Can not set addresses of connection after sending data"),
		}
	}
	pub fn set_nsa_mode(&mut self)
	{
		if let &mut HandshakeState2::ClientHello(ref mut x) = &mut self.hs_state {
			x.enabled_kem = KemEnabled::nsa_mode();
			x.enabled_sig = SignatureAlgorithmEnabled2::nsa_mode();
			x.enabled_cs = CiphersuiteEnabled2::nsa_mode();
			x.crypto_algs.nsa_mode();
			x.flags.set_FORCE_NSA_MODE();
		}
	}
	pub fn set_no_android_hack(&mut self)
	{
		if let &mut HandshakeState2::ClientHello(ref mut x) = &mut self.hs_state {
			x.flags.set_NO_ANDROID_HACK();
		}
	}
	pub fn set_best_security(&mut self)
	{
		if let &mut HandshakeState2::ClientHello(ref mut x) = &mut self.hs_state {
			x.flags.set_SPINAL_TAP();
		}
	}
	pub fn set_secure192(&mut self)
	{
		if let &mut HandshakeState2::ClientHello(ref mut x) = &mut self.hs_state {
			x.flags.set_SECURE_192();
		}
	}
	pub fn set_tls13_only(&mut self)
	{
		if let &mut HandshakeState2::ClientHello(ref mut x) = &mut self.hs_state {
			x.flags.set_TLS_13_ONLY();
		}
	}
	//Handle the initial deadline.
	fn handle_deadline(config: &ServerConfiguration, callbacks: &mut (dyn TlsCallbacks+Send+'static))
	{
		if let Some(timelimit) = config.handshake_time_limit {
			callbacks.set_deadline(Some(PointInTime::from_now(TimeUnit::Duration(timelimit))));
		} else {
			callbacks.set_deadline(None);
		}
	}
	pub fn enable_ssl_key_log(&mut self) { self.ssl_key_log = true; }
}


#[derive(Copy,Clone)]
struct ProtectionSpecial
{
	secure192: bool,
	spinal_tap: bool
}

struct SelectProtectionParameters
{
	ciphersuite_mask: SetOf<_Ciphersuite>,
	prefer_chacha: bool,
	tls13: bool,
	hw_aes: bool,
	special: ProtectionSpecial,
}

//Select a protection.
fn select_protection(params: SelectProtectionParameters) -> Result<(ProtectorType, HashFunction2), Error>
{
	//Deprefer AES if either end does so.
	let prefer_aes = params.hw_aes && !params.prefer_chacha;

	let rcs = Ciphersuite::random_suite();
	let mut best_params = (rcs.get_protector(), rcs.get_prf2());
	let mut best_score = 0xFFFFFFFFu32;	//The worst.
	let mut any_ok_ignoring_secure192 = false;
	for i in params.ciphersuite_mask.iter::<Ciphersuite>() {
		let is_tls13 = i.get_key_exchange() == KeyExchangeType::Tls13;
		if is_tls13 != params.tls13 { continue; }
		//Do all other suitability checks before checking for secure192, as error depends on if
		//error is due to secure192 failure.
		any_ok_ignoring_secure192 = true;
		if params.special.secure192 && i.get_protector().get_security_level() < 192 { continue; }
		let score = i.get_priority();
		let score = if params.special.spinal_tap {
			!i.get_security_rank()	// ! flips ranking.
		} else if prefer_aes {
			score.hw
		} else {
			score.sw
		};
		if score < best_score {
			best_params = (i.get_protector(), i.get_prf2());
			best_score = score;
		}
	}
	if best_score < 0xFFFFFFFFu32 {
		Ok(best_params)
	} else if any_ok_ignoring_secure192 {
		fail!(error!(secure192_required_not_offered))
	} else {
		fail!(error!(no_mutual_ciphersuites));
	}
}

fn encode_sni_kind<'a>(sni: Option<&'a str>, fallback_sni: Option<&'a str>, destination_ip: IpAddr) ->
	Option<ReferenceName<'a>>
{
	match sni {
		Some(ref x) => Some(ReferenceName::DnsName(x)),
		//If source address is unspecified, fall back to default sni...
		None if destination_ip.is_unspecified() => fallback_sni.map(|x|ReferenceName::DnsName(x)),
		//But if source address is not unspecified, then fall back to the destination address.
		None => Some(ReferenceName::Ip(IpAddress::from(destination_ip))),
	}
}

fn sni_to_text<'a>(sni: Option<&'a str>, fallback_sni: Option<&'a str>, destination_ip: IpAddr) ->
	Option<Cow<'a, str>>
{
	match sni {
		Some(x) => Some(Cow::Borrowed(x)),
		None if destination_ip.is_unspecified() => fallback_sni.map(|x|Cow::Borrowed(x)),
		None => {
			let destination_ip = IpAddress::from(destination_ip);
			Some(Cow::Owned(format!("[{destination_ip}]")))
		}
	}
}
