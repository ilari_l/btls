use self::controller::HandshakeController;
use self::controller::RestartAuthKeys;
use crate::Connection;
use crate::callbacks::TlsCallbacks;
use crate::certificates::CertificateLookup;
use crate::certificates::TrustAnchor;
use crate::certificates::TrustedLog;
use crate::common::builtin_killist;
use crate::common::FutureFinishedNotifier;
use crate::common::handle_blacklist;
use crate::common::handle_ctlog;
use crate::common::handle_hs_timelimit;
use crate::common::handle_ocsp_maxvalid;
use crate::common::handle_trustanchor;
use crate::errors::Error;
use crate::features::ConfigPages;
use crate::features::ConfigFlagsEntry;
use crate::features::ConfigFlagsError;
use crate::features::ConfigFlagsErrorKind;
use crate::features::ConfigFlagsValue;
use crate::features::DEFAULT_FLAGS;
use crate::features::ENABLE_TLS13_NAME;
use crate::features::split_config_str;
use crate::features::DEBUG_NO_SHARES_NAME;
use crate::features::KNOWN_CA_ONLY;
use crate::features::TLS13_DRAFTS;
use crate::logging::DebugSetting;
use crate::logging::Logging;
use crate::logging::MessageSeverity;
pub use crate::messages::AlpnList;
use crate::record::Session;
use crate::stdlib::Arc;
use crate::stdlib::AtomicBool;
use crate::stdlib::AtomicOrdering;
use crate::stdlib::Box;
use crate::stdlib::Cow;
use crate::stdlib::Debug;
use crate::stdlib::Duration;
use crate::stdlib::FmtError;
use crate::stdlib::FmtWrite;
use crate::stdlib::Formatter;
use crate::stdlib::from_utf8;
use crate::stdlib::IoRead;
use crate::stdlib::IpAddr;
use crate::stdlib::Mutex;
use crate::stdlib::RwLock;
use crate::stdlib::String;
use crate::stdlib::ToOwned;
use crate::stdlib::Vec;
use crate::system::Builder;
use crate::system::File;
use crate::system::Path;
use crate::system::PathBuf;
use crate::system::read_dir;
use btls_aux_collections::BTreeMap;
use btls_aux_collections::BTreeSet;
use btls_aux_filename::Filename;
use btls_aux_inotify::CLOSED_WRITABLE;
use btls_aux_inotify::DELETED;
use btls_aux_inotify::InotifyEventSink;
use btls_aux_inotify::InotifyWatcher;
use btls_aux_inotify::InotifyTrait;
use btls_aux_inotify::MOVED_FROM;
use btls_aux_inotify::MOVED_TO;
use btls_aux_keypair_local::LocalKeyPair;
use btls_aux_random::TemporaryRandomLifetimeTag;
use btls_aux_random::TemporaryRandomStream;
use btls_aux_set2::SetOf;
use btls_aux_signatures::SignatureAlgorithm2;
use btls_aux_x509certvalidation::TrustAnchorOrdered;
use core::fmt::Arguments;


mod controller;

#[derive(Clone)]
struct LogProxy(Option<Arc<Box<dyn Logging+Send+Sync>>>);

impl Debug for LogProxy
{
	fn fmt(&self, _f: &mut Formatter) -> Result<(), FmtError> { Ok(()) }
}

impl Logging for LogProxy
{
	fn message(&self, severity: MessageSeverity, x: Cow<'static, str>)
	{
		if let Some(ref l) = (self.0).as_ref() { l.message(severity, x); }
	}
	fn message_ex(&self, cid: Option<u64>, severity: MessageSeverity, x: Arguments)
	{
		if let Some(ref l) = (self.0).as_ref() { l.message_ex(cid, severity, x); }
	}
}

#[derive(Debug)]
struct AcmeWatchData
{
	cached_content: BTreeMap<String, String>,
	files: BTreeSet<String>,
	files_reliable: bool,
}

struct AcmeWatchEventSink(Arc<RwLock<AcmeWatchData>>, LogProxy);

impl InotifyEventSink for AcmeWatchEventSink
{
	//These are almost always bad news, so assume error severity.
	fn log_message(&self, msg: &str)
	{
		(self.1).message_ex(None, MessageSeverity::Error, format_args!("{msg}"));
	}
	fn inotify_event(&self, name: &[u8], event: u32) -> bool
	{
		//Clear cache on anything mentioned. The names are always valid UTF-8 (as ASCII).
		let host = match from_utf8(name) { Ok(x) => x, Err(_) => return false };
		self.0.with_write(|cache|{
			(self.1).message_ex(None, MessageSeverity::Notice,
				format_args!("ACME: Cleared cache entry for {host}"));
			cache.cached_content.remove(host);
			//If event is CLOSED_WRITABLE or MOVED_TO, record that the file exists.
			if event & (CLOSED_WRITABLE | MOVED_TO) != 0 { cache.files.insert(host.to_owned()); }
			//If event is MOVED_FROM or DELETED, record that the file does not exist.
			if event & (DELETED | MOVED_FROM) != 0 { cache.files.remove(host); }
		});
		false
	}
}

//Object handling ACME challenges.
#[derive(Debug)]
struct AcmeChallengeLookup
{
	internal_map: BTreeMap<String, String>,
	cache: Arc<RwLock<AcmeWatchData>>,
	keep_cache: bool,
	cache_error: AtomicBool,
	challenges_path: Option<PathBuf>,
	logs: LogProxy,
	exit_signal: Arc<AtomicBool>,
}

impl Drop for AcmeChallengeLookup
{
	fn drop(&mut self) { self.kill_cache_thread(); }
}

impl AcmeChallengeLookup
{
	fn kill_cache_thread(&self) { self.exit_signal.store(true, AtomicOrdering::SeqCst); }
	fn initialize_files(path: &Path) -> Result<BTreeSet<String>, ()>
	{
		let mut result = BTreeSet::new();
		for file in dtry!(read_dir(path)) {
			let file = dtry!(file);
			result.insert(file.file_name().to_string_lossy().into_owned());
		}
		Ok(result)
	}
	fn start_cache_thread(&mut self)
	{
		//No thread in case of no directory.
		let path = f_return!(self.challenges_path.as_ref(), ());
		let pathd = Filename::from_path(path);
		//Check that the ACME directory is a valid directory.
		if !path.is_dir() {
			self.logs.message_ex(None, MessageSeverity::Error,
				format_args!("The specified ACME path '{pathd}' is not a directory"));
			self.cache_error.store(true, AtomicOrdering::Relaxed);
			return;
		}
		let flag = Arc::new(AtomicBool::new(false));
		self.exit_signal = flag.clone();
		let sink = AcmeWatchEventSink(self.cache.clone(), self.logs.clone());
		let mut inotify = match InotifyWatcher::new(path.clone(), Box::new(sink), MOVED_FROM | MOVED_TO |
			CLOSED_WRITABLE | DELETED) {
			Ok(x) => x,
			Err(err) => {
				self.logs.message_ex(None, MessageSeverity::Error, 
					format_args!("Internal error: Can't create ACME directory watch: {err}"));
				self.cache_error.store(true, AtomicOrdering::Relaxed);
				return;
			}
		};
		let b = Builder::new();
		let b = b.name(String::from("ACME inotify"));
		let ret = b.spawn(move || {		//Drop the handle to floor (and we can't report errors).
			while !flag.load(AtomicOrdering::SeqCst) {
				//Wait 5s for events.
				inotify.wait(Some(5000));
			}
		});
		if let Err(err) = ret {
			self.logs.message_ex(None, MessageSeverity::Error,
				format_args!("Internal error: Can't start ACME cache cleaner thread: {err}"));
			self.cache_error.store(true, AtomicOrdering::Relaxed);
			return;
		};
		//Probe all files.
		let fset = Self::initialize_files(path).ok();
		self.cache.with_write(|cache|{
			cache.files_reliable = fset.is_some();
			cache.files = fset.unwrap_or(BTreeSet::new());
		});
		self.logs.message_ex(None, MessageSeverity::Notice,
			format_args!("ACME: Monitoring directory {pathd} (cache enabled)"));
		self.cache_error.store(false, AtomicOrdering::Relaxed);
	}
	fn set_log_proxy(&mut self, proxy: LogProxy) { self.logs = proxy; }
	fn new(proxy: LogProxy) -> AcmeChallengeLookup
	{
		//We demand CLOSED_WRITABLE, MOVED_FROM, MOVED_TO and DELETED for keeping cache.
		let req_flags = CLOSED_WRITABLE | MOVED_FROM | MOVED_TO | DELETED;
		let keep_cache = InotifyWatcher::supported_events() & req_flags == req_flags;
		let newcache = AcmeWatchData {
			cached_content: BTreeMap::new(),
			files: BTreeSet::new(),
			files_reliable: false,
		};
		let newcache = Arc::new(RwLock::new(newcache));
		//We do not start the cache thread here, since there is no directory.
		AcmeChallengeLookup {
			internal_map: BTreeMap::new(),
			cache: newcache,
			keep_cache: keep_cache,
			cache_error: AtomicBool::new(false),
			challenges_path: None,
			logs: proxy,
			exit_signal: Arc::new(AtomicBool::new(false)),	//Leads to nowhere.
		}
	}
	fn write_cache_entry(&self, host: &str, value: &str)
	{
		//If cache is not available, do not accumulate cache!
		if !self.keep_cache || self.cache_error.load(AtomicOrdering::Relaxed) { return; }
		self.cache.with_write(|cache|{
			cache.cached_content.insert(host.to_owned(), value.to_owned());
		});
	}
	fn clear_cache_all(&mut self)
	{
		self.cache.with_write(|cache|{
			cache.files_reliable = false;
			cache.files.clear();
			cache.cached_content.clear();
		});
		if self.keep_cache {
			//Kill the current cache thread and start a new one.
			self.kill_cache_thread();
			if self.challenges_path.is_some() { self.start_cache_thread(); }
		} else if let Some(ref path) = self.challenges_path.as_ref()  {
			let pathd = Filename::from_path(path);
			self.logs.message_ex(None, MessageSeverity::Notice,
				format_args!("ACME: Monitoring directory {pathd} (cache not available)"));
		}
	}
	fn consider_path(&self, path: &Path, hostname: &str) -> Option<String>
	{
		if let Some(hostname) = hostname.strip_prefix("[").and_then(|hostname|hostname.strip_suffix("]")) {
			//If name starts and ends with '[]', then it is address. For addresses, the allowed
			//characters are xdigits, '.' and ':'.
			for i in (hostname.as_bytes()).iter() {
				match *i {
					b'0'..=b'9' => (),
					b'A'..=b'F' => (),
					b'a'..=b'f' => (),
					b'.' => (),
					b':' => (),
					_ => return None,			//Bad character.
				}
			}
		} else {
			//Check that hostname consists of valid DNS characters, which are alphanumerics, dash and
			//dot. There must be no two dots and dot must not be first character. This also establishes
			//that the name is valid as filename.
			let mut last_dot = true;
			for i in hostname.as_bytes().iter() {
				match *i {
					b'0'..=b'9' => (),
					b'A'..=b'Z' => (),
					b'a'..=b'z' => (),
					b'-' => (),
					b'_' => (),
					b'.' if last_dot => return None,	//.. is not valid.
					b'.' => (),
					_ => return None,			//Bad character.
				};
				last_dot = *i == b'.';
			}
		}
		//If name has trailing dot, strip it.
		let hostname = hostname.strip_suffix(".").unwrap_or(hostname);
		//Empty name is not valid.
		fail_if_none!(hostname.len() == 0);
		//Lowercase the filename.
		let hostname = if let Ok(y) = String::from_utf8(hostname.as_bytes().iter().
			map(|x|if x.wrapping_sub(65) < 26 { x|32 } else { x|0 }).
			collect::<Vec<_>>()) { y } else { return None };
		//Construct a path to the validation file, and check it exists.
		let file = path.join(&hostname);
		fail_if_none!(!file.is_file());
		//Open the file and read it.
		let mut content = String::new();
		fail_if_none!(File::open(file).and_then(|mut x|x.read_to_string(&mut content)).is_err());
		//Cache the entry.
		self.write_cache_entry(&hostname, &content);
		//Okay.
		Some(content)
	}
	//Look up a challenge response, as key authorization.
	fn lookup(&self, hostname: &str, cid: Option<u64>) -> Option<String>
	{
		let hostnamep = escape_name(hostname);
		//First check internal map.
		if let Some(x) = self.internal_map.get(hostname) {
			self.logs.message_ex(cid, MessageSeverity::Notice,
				format_args!("ACME: Responding to {hostnamep} (explicit entry)"));
			return Some(x.clone());
		}
		//Check cache.
		let ret = self.cache.with_read(|cache|{
			if cache.files_reliable && !cache.files.contains(hostname) {
				self.logs.message_ex(cid, MessageSeverity::Notice,
					format_args!("ACME: Refusing {hostnamep} (negative cache hit)"));
				return Some(None);
			}
			if let Some(x) = cache.cached_content.get(hostname) {
				self.logs.message_ex(cid, MessageSeverity::Notice,
					format_args!("ACME: Responding to {hostnamep} (directory, cache hit)"));
				return Some(Some(x.clone()));
			}
			None
		});
		if let Some(x) = ret { return x; }
		//Then check path if set.
		if let &Some(ref path) = &self.challenges_path {
			if let Some(x) = self.consider_path(path, hostname) {
				self.logs.message_ex(cid, MessageSeverity::Notice,
					format_args!("ACME: Responding to {hostnamep} (directory, cache miss)"));
				return Some(x);
			}
		}
		self.logs.message_ex(cid, MessageSeverity::Warning,
			format_args!("ACME: Refusing {hostnamep} (not configured)"));
		None
	}
}

fn escape_name<'a>(hostname: &'a str) -> Cow<'a, str>
{
	//The ASCII characters except backslash are allowed, others are disallowed.
	let mut need_escape = false;
	for i in hostname.chars() {
		let i = i as u32;
		need_escape |= i < 32 || i == 92 || i > 126;
	}
	if need_escape {
		let mut s = String::new();
		for i in hostname.chars() {
			let j = i as u32;
			if j < 32 || j == 92 || j > 126 {
				write!(s, "\\u{{{j}}}").ok();	//Should never fail.
			} else {
				s.push(i);
			}
		}
		Cow::Owned(s)
	} else {
		Cow::Borrowed(hostname)		//Ok as-is.
	}
}

//Handle blacklist entry.
fn handle_acmedir<F>(value: &str, _a: &mut F, _b: ConfigFlagsEntry, path: &mut AcmeChallengeLookup) where F:
	FnMut(ConfigFlagsError)
{
	path.challenges_path = Some(PathBuf::from(value));
	path.clear_cache_all();
}

///Server configuration.
///
///This structure contains various parts of the server configuration.
///
///# Creating the structure:
///
///In most cases, one can use [`CertificateDirectory`] as the certificate lookup, like this:
///
///```no_run
///let mut cert_store = CertificateDirectory::new(cert_dir_path, logger);
///let mut config = ServerConfiguration::from(&cert_store);
///```
///
///# Setting default SNI name.
///
///The server default SNI name can be changed using the [`set_default_server_name()`] method. This only affects
///connections that do not contain the SNI extension. The default is to have no default, so any certificate can be
///selected.
///
///Note that this option is not compatible with the [`set_destination_address()`] method. If a connection has
///desination address, then the default SNI is ignored, and the SNI is considered to point to the destination
///IP address specified.
///
///# Configuration flags:
///
///Configuration flags are various binary options that configure the behavior of the system. The flags can default
///to either set or unset. The flags can be then configured to be cleared or set. However, some flags only affect
///server and some exist just for backward compatibility, and actually do nothing.
///
///The flags can be confgured using the [`set_flags()`](#method.set_flags) and [`clear_flags()`](#method.clear_flags)
///methods. Additionally, the configuration strings configured using the [`config_flags()`](#method.config_flags)
///method can set or clear flags.
///
///The flags are divided into pages. Currently there are 5 pages:
///
/// * Page #0 contains various flags affecting TLS library behavior.
/// * Page #1 contains the enabled key exchanges.
/// * Page #2 contains the enabled hash algorithms.
/// * Page #3 contains the enabled encryption algorithms.
/// * Page #4 contains the enabled signature algorithms.
///
///See the documentation of the [`features`](features/index.html) module for more documentation about what various
///flags do.
///
///# Client certificates:
///
///Trust anchors are added using the [`add_trust_anchor()`](#method.add_trust_anchor) method. By default there are
///no trust anchors.
///
///The OCSP maximum validity determines the longest lifetime allowed for OCSP response. If the lifetime is longer
///than the limit, the OCSP response is not treated as valid and does not fullfill requirement to staple OCSP, if
///such requirement is present (via configuration or having OCSP must-staple in the certificate). The OCSP maximum
///validity can be set using the [`set_ocsp_maxvalidity()`](#method.set_ocsp_maxvalidity) method.
///
///Also, if the certificate lifetime is less than the OCSP maximum validity, it is treated like it had stapled OCSP,
///even if not actually present. This also fulfills requirement to staple OCSP (weither via configuration or via
///certificate extension.
///
///One can add trusted certificate transparency logs using the [`add_ct_log()`](#method.add_ct_log) method. Note that
///since there are no auditing capabilities, the logs are operated in pure countersignature mode. Also, as
///consequence, all Certificate Transparency items except Signed Certificate Timestamps are ignored. Also,
///signature from any trusted log is deemed sufficient to fullfill requirement for Certificate Transparency
///(specified either via configuration or certificate).
///
///Certificates can be blacklisted by SHA-256 hash of their public key by using the
///[`blacklist()`](#method.blacklist) method. If a blacklisted key is encountered in handshake, the certificate
///validation will be rejected.
///
///# Stateless reject mode:
///
///Stateless reject mode can be set using [`set_stateless_reject()`](#method.set_stateless_reject). When statelss
///reject mode is active, the following changes take place:
///
/// 1. TLS 1.2 is hard-disabled.
/// 2. Handshake restart is a hard error. However, the application can use [`Error::is_restart()`] method to query
///if error was a restart and the sent rejection message.
///
///# ACME challenges
///
///If one binds to port 443, it is *STRONGLY* *RECOMMENDED* to let administrator configure ACME challenges. These
///can come from two different sources, the ACME directory and explicit challenges.
///
///The ACME directory can be set using the [`set_acme_dir()`](#method.set_acme_dir) method. The explicit challenges
///are managed using the [`create_acme_challenge()`](#method.create_acme_challenge) and
///[`delete_acme_challenge()`](#method.delete_acme_challenge) methods. The explicit challenges override the
///challenges loaded from the ACME directory.
///
///# Debugging mode
///
///The debugging mode is controlled by the [`set_debug()`](#method.set_debug) method. This method takes a
///debugging event mask of events to enable and a sink to send the debugging events to.
///
///The following types of debugging events are available (defined in module [`logging`]):
///
/// * `DEBUG_ABORTS`: Debugging message on connection abort event.
/// * `DEBUG_CRYPTO_CALCS`: Debugging message on cryptographic key calculations (requires `enable-key-debug` feature
///when compiling).
/// * `DEBUG_HANDSHAKE_DATA`: Debugging message on received or sent handshake messages, complete with message
///contents.
/// * `DEBUG_HANDSHAKE_EVENTS`: Debugging message on various events (like rekeyings or things being verified) when
///handshaking.
/// * `DEBUG_HANDSHAKE_MSGS`: Debugging message on received or sent handshake messages, but without message contents.
/// * `DEBUG_TLS_EXTENSIONS`: Debugging messages on TLS extensions being received.
///
///# Making connection
///
///When you have it all set up, you can get server connection using [`make_connection()`].
///
///# Configuration examples:
///
///```no_run
///config.set_flags(0, FLAGS0_REQUIRE_EMS);
///```
///
///This sets Extended Master Secret (or TLS 1.3, which is equivalent) to be required. This is useful if you are
///going to use exporters, since those do not work without Extended Master Secret. If the server does not support
///Extended Master Secret (nor TLS 1.3) then the handshake will fail.
///
///```no_run
///config.clear_flags(0, FLAGS0_ENABLE_TLS12);
///```
///
///This disables TLS 1.2 support, leaving just TLS 1.3 supported.
///
///```no_run
///config.set_acme_dir(path_from_config);
///```
///
///This sets the ACME challenge directory (strongly recommended to let admin configure this if binding to port 443
///for an extended time).
///
///[`make_connection()`]: #method.make_connection
///[`set_destination_address()`]: struct.ServerConnection.html#method.set_destination_address
///[`set_default_server_name()`]: #method.set_default_server_name
///[`CertificateDirectory`]: certificates/struct.CertificateDirectory.html
///[`Error::is_restart()`]: struct.Error.html#method.is_restart
///[`logging`]: logging/index.html
pub struct ServerConfiguration
{
	//Trust anchor set.
	trust_anchors: BTreeSet<TrustAnchorOrdered>,
	//Certificate lookup routine.
	lookup_cert: CertificateLookup,
	//Flags page0.
	flags: ConfigPages,
	//Logging config.
	log: DebugSetting,
	//Default SNI name.
	default_sni_name: Option<String>,
	//Certificate Killist.
	killist: BTreeSet<[u8; 32]>,
	//OCSP maximum validitiy.
	ocsp_maxvalid: u64,
	//Trusted logs.
	trusted_logs: Vec<TrustedLog>,
	//Handshake time limit.
	handshake_time_limit: Option<Duration>,
	//Restart authentication keys.
	restart_auth_keys: RestartAuthKeys,
	//Stateless mode.
	stateless: bool,
	//Raw handshake mode.
	raw_handshake: bool,
	//ACME challenges.
	acme_challenges: Arc<RwLock<AcmeChallengeLookup>>,
	//ACME validation key.
	acme_key: Option<Arc<LocalKeyPair>>,
}

impl Clone for ServerConfiguration
{
	fn clone(&self) -> ServerConfiguration
	{
		//This does not deep-clone acme_challenges, so no need to set proxies.
		ServerConfiguration {
			trust_anchors:self.trust_anchors.clone(),
			lookup_cert:self.lookup_cert.clone(),
			flags: self.flags.clone(),
			log:self.log.clone(),
			default_sni_name: self.default_sni_name.clone(),
			killist: self.killist.clone(),
			ocsp_maxvalid: self.ocsp_maxvalid.clone(),
			trusted_logs: self.trusted_logs.clone(),
			handshake_time_limit: self.handshake_time_limit.clone(),
			restart_auth_keys: self.restart_auth_keys.clone(),
			stateless: self.stateless,
			raw_handshake: self.raw_handshake,
			acme_challenges: self.acme_challenges.clone(),
			acme_key: self.acme_key.clone(),
		}
	}
}

impl<'a> From<CertificateLookup> for ServerConfiguration
{
	fn from(lookup: CertificateLookup) -> ServerConfiguration
	{
		let lifetime = TemporaryRandomLifetimeTag;
		let lproxy = LogProxy(None);	//No logger available yet.
		ServerConfiguration {
			trust_anchors: BTreeSet::new(),
			lookup_cert: lookup,
			flags: DEFAULT_FLAGS,
			log: DebugSetting::new(),
			default_sni_name: None,
			killist: builtin_killist(),
			ocsp_maxvalid: 7 * 24 * 60 * 60, 	//7 days.
			trusted_logs: Vec::new(),
			handshake_time_limit: None,
			restart_auth_keys: RestartAuthKeys::new(),
			stateless: false,
			raw_handshake: false,
			acme_challenges: Arc::new(RwLock::new(AcmeChallengeLookup::new(lproxy))),
			acme_key: LocalKeyPair::new_transient_ecdsa_p256(
				&mut TemporaryRandomStream::new_system(&lifetime)).ok().map(|x|Arc::new(x)),
		}
	}
}

impl ServerConfiguration
{
	pub(crate) fn flags0(&self) -> u64 { self.flags.get_flags() }
	///Add a trust anchor to trust pile.
	///
	///This method adds trust anchor `anchor` to the trust anchor pile. This pile is only used for verifying
	///client certificates. The certificates the server sends do *NOT* have to chain to these certificates,
	///nor is server certificate selection affected in *ANY* way.
	///
	///Trust anchors can also be added via the `trustanchor=` option in configuration string (see the method
	///[`config_flags()`](#method.config_flags)).
	///
	///The default trust pile is empty.
	pub fn add_trust_anchor(&mut self, anchor: &TrustAnchor) { self.trust_anchors.insert(anchor.as_ordered()); }
	////Set the OCSP maximum validity limit.
	///
	///This method sets the OCSP maximum validity limit `limit` (expressed in seconds). This is only used for
	///client certificates. Server certificates are not affected in any way.
	///
	///This limit is used in two ways:
	///
	/// - If OCSP response lives longer than this limit, the OCSP response is not considered valid.
	/// - If certificate lives at most this limit, no OCSP response is ever required. This is even if
	///OCSP required flag is set, or if certificate is marked must-staple.
	///
	///This limit can also be modified by `ocsp-maxvalid=` option in configuration string (see the method
	///[`config_flags()`](#method.config_flags)).
	///
	///The default value is 604,800 seconds (i.e., 7 days).
	pub fn set_ocsp_maxvalidity(&mut self, limit: u64) { self.ocsp_maxvalid = limit; }
	///Set flags.
	///
	///This method sets flags specified by bitmask `mask` on configuration page `page`.
	///
	///Flags can affect various options in server behavior. See module [`features`] for the flag constants.
	///
	///If nonexistent page is specified, this method does nothing.
	///
	///[`features`]: features/index.html
	pub fn set_flags(&mut self, page: usize, mask: u64)
	{
		self.flags.set_flags(page, mask);
	}
	///Clear flags.
	///
	///This method clears flags specified by bitmask `mask` on configuration page `page`.
	///
	///Flags can affect various options in server behavior. See module [`features`] for the flag constants.
	///
	///If nonexistent page is specified, this method does nothing.
	///
	///[`features`]: features/index.html
	pub fn clear_flags(&mut self, page: usize, mask: u64)
	{
		self.flags.clear_flags(page, mask);
	}
	///Set the handshake deadline to `x`. If the handshake has not finished by that time, the application is
	///signaled to tear down the connection (see callback `set_deadline()`).
	///
	///Passing `Some(x)` sets deadline to `x` from creation of the connection context, and passing `None`
	///disables the deadline. By default, deadline is disabled.
	///
	///The deadline can also be modified via the `handshake-deadline=` option in configuration string (see the
	///method `.config_flags`).
	pub fn set_deadline(&mut self, x: Option<Duration>) { self.handshake_time_limit = x; }
	///Add a new trusted certificate transparency log.
	///
	///This method adds certificate transprancy log `log` to the list of trusted certificate transparency logs.
	///This list is used for verifying certificate transparency proofs in client certificates. It does not
	///affect Certificate Transparency proofs associated to server certificates in any way.
	///
	///However, note that there is trusted CT log list that affects server certificates. However, that is set
	///via the certificate selector (structure implementing the trait [`CertificateLookup`])
	///
	///CT logs can also be added via the `ctlog=` option in configuration string (see the method
	///[`config_flags()`](#method.config_flags)).
	///
	///The default is not to have any trusted CT logs.
	///
	///[`CertificateLookup`]: certificates/trait.CertificateLookup.html
	pub fn add_ct_log(&mut self, log: &TrustedLog) { self.trusted_logs.push(log.clone()); }
	///Add a new certificate key blacklist entry.
	///
	///This method adds a blacklist entry for certificate key hash `spkihash` (a 32-byte SHA-256 hash of
	///certificate SubjectPublicKeyInfo). Note that this is only used for client certificates, and matching
	///hash will cause the validation to be rejected, which is not connection failure. Additionally, if a
	///blacklist entry matches a trust anchor, that trust anchor is disabled. This blacklist does not affect
	///server certificates in *ANY* way.
	///
	///Default is not to have any blacklist entries.
	pub fn blacklist(&mut self, spkihash: &[u8])
	{
		let mut x = [0; 32];
		if spkihash.len() != x.len() { return; }
		x.copy_from_slice(spkihash);
		self.killist.insert(x);
	}
	///Set ACME challenge database path.
	///
	///This method sets the directory ACME TLS-ALPN-01 challenge database is to `path`.
	///
	///This is STRONGLY RECOMMENDED to be admin-configurable if you bind to port 443.
	///
	///The ACME challenge database contains the pending ACME TLS-ALPN-01 challenges that will be
	///answered if query comes. Note that loading normal certificates in order to pass those WILL NOT WORK.
	///The handshakes will fail via interlock if you try.
	///
	///The database consists of files named by the names to validate and containing the authorization token
	///for the domain..
	///Setting this database does not override challenges crated by [`create_acme_challenge()`] method.
	///
	///This directory can also be modified by `acmedir=` option in configuration string (see the method
	///[`config_flags()`](#method.config_flags)).
	///
	///The default is no ACME challenge database.
	///
	///[`create_acme_challenge()`]: #method.create_acme_challenge
	pub fn set_acme_dir<P:AsRef<Path>>(&mut self, path: P)
	{
		self.acme_challenges.with_write(|x|{
			x.challenges_path = Some(path.as_ref().to_path_buf());
			x.clear_cache_all();	//The cache is now invalid.
		});
	}
	///Create an ACME challenge response endpoint.
	///
	///This method creates an ACME challenge-response endpoint for key authorization `keyauth`, which is
	///specified for name `name`. This key authorization will then be used for trying to validate the name.
	///
	///If handshake is sent with SNI matching a known challenge and using the ACME TLS-ALPN ALPN, the TLS
	///library will immediately disconnect the connection when the connection reaches showtime state. No data
	///transport on challenge connection is possible.
	///
	///The default is to have no challenges (but the challenges in challenge directory set by [`set_acme_dir()`]
	///are also considered).
	///
	///[`set_acme_dir()`]: #method.set_acme_dir
	pub fn create_acme_challenge(&mut self, name: &str, keyauth: &str)
	{
		self.acme_challenges.with_write(|x|{
			x.internal_map.insert(name.to_owned(), keyauth.to_owned());
		});
	}
	///Delete an ACME challenge response endpoint.
	///
	///This method deletes the ACME challenge-response endpoint for name `name`.
	///
	///The default is to have no challenges (but the challenges in challenge directory set by [`set_acme_dir()`]
	///are also considered).
	///
	///[`set_acme_dir()`]: #method.set_acme_dir
	pub fn delete_acme_challenge(&mut self, name: &str)
	{
		self.acme_challenges.with_write(|x|x.internal_map.remove(name));
	}
	///Set/Clear flags according to config string.
	///
	///This method sets or clears configuration options according to configuration string `config` (presumably
	///read from configuration file). If any errors are encountered during processing, the error callback
	///`error_cb` is called to report those errors (possibly multiple times).
	///
	///The configuration string consists of comma-separated elements, with each element being one of:
	///
	/// * flagname -> Enable flag named `flagname`.
	/// * +flagname -> Enable flag named `flagname`.
	/// * -flagname -> Disable flag named `flagname`.
	/// * !flagname -> Disable flag named `flagname`.
	/// * setting=value -> Set setting `setting` to value `value`.
	///
	///Use backslashes to escape backslash or comma in the value.
	///
	/// See description of [`lookup_flag()`](features/fn.lookup_flag.html) for the flag names.
	///
	///# The following settings are supported:
	///
	/// * `acmedir` -> Set the ACME challenge database directory to the specified value.
	/// * `blacklist` -> Blacklist specified SPKI hash (specified as 64 hexadecimal digit representation of
	///SHA-256 hash of the SubjectPublicKeyInfo).
	/// * `trustanchor` -> Read the specified file in DER (possibly concatenated from multiple certificates) or
	///PEM format as series of trust anchors to add.
	/// * `ctlog` -> Read the specified file as series of CT logs (one per line) to add.
	/// * `ocsp-maxvalid` -> Set the OCSP maximum validity limit in seconds.
	/// * `handshake-deadline` -> Set handshake deadline in seconds (or empty value disables the limit).
	///
	///# Format of ctlog files:
	///
	///ctlog files consists of one or more lines, with each line containing a trusted CT log. Each line has one
	///of two forms:
	///
	/// * `v1:<key>:<expiry>:<name>`
	/// * `v2:<id>/<hash>:<key>:<expiry>:<name>`
	///
	///Where:
	///
	/// * `<id>` is base64 encoding of DER encoding the log OID, without the DER tag or length.
	/// * `<hash>` is the log hash function. Currently only `sha256` is supported.
	/// * `<key>`is base64 encoding of log key formatted as X.509 SubjectPublicKeyInfo.
	/// * `<expiry>` is the time log expired in seconds since Unix epoch, or blank if log has not expired yet.
	///Certificate-stapled SCTs are accepted up to expiry date from expired logs, other kinds of SCTs are not
	///accepted.
	/// * `<name>` is the name of the log.
	pub fn config_flags<F>(&mut self, config: &str, mut error_cb: F) where F: FnMut(ConfigFlagsError)
	{
		split_config_str(self, config, |objself, name, value, entry| {
			let bad = if name == "acmedir" {
				let mut cpath = objself.acme_challenges.write();
				config_strval!(handle_acmedir, value, &mut error_cb, entry, &mut cpath)
			} else if name == "blacklist" {
				config_strval!(handle_blacklist, value, &mut error_cb, entry, &mut objself.killist)
			} else if name == "trustanchor" {
				config_strval!(handle_trustanchor, value, &mut error_cb, entry, &mut objself.
					trust_anchors)
			} else if name == "ctlog" {
				config_strval!(handle_ctlog, value, &mut error_cb, entry, &mut objself.
					trusted_logs)
			} else if name == "ocsp-maxvalid" {
				config_strval!(handle_ocsp_maxvalid, value, &mut error_cb, entry, &mut objself.
					ocsp_maxvalid)
			} else if name == "handshake-deadline" {
				config_strval!(handle_hs_timelimit, value, &mut error_cb, entry, &mut objself.
					handshake_time_limit)
			} else {
				false
			};
			if bad {
				return error_cb(ConfigFlagsError {
					entry:entry,
					kind:ConfigFlagsErrorKind::ArgumentRequired(name)
				});
			}
			//All the remaning ones are flags.
			let bad = match value {
				//Some flags no longer have an effect or are not supported client side.
				ConfigFlagsValue::Disabled if name == ENABLE_TLS13_NAME =>
					return error_cb(ConfigFlagsError{
						entry:entry,
						kind:ConfigFlagsErrorKind::NoEffect(name)
					}),
				ConfigFlagsValue::Enabled if name == DEBUG_NO_SHARES_NAME ||
					name == KNOWN_CA_ONLY || name == TLS13_DRAFTS =>
					return error_cb(ConfigFlagsError{
						entry:entry,
						kind:ConfigFlagsErrorKind::NoEffect(name)
					}),
				ConfigFlagsValue::Explicit(_) => return error_cb(ConfigFlagsError{
					entry:entry,
					kind:ConfigFlagsErrorKind::NoArgument(name)
				}),
				ConfigFlagsValue::Disabled => !objself.flags.clear_flag_by_name(name),
				ConfigFlagsValue::Enabled => !objself.flags.set_flag_by_name(name),
			};
			if bad {
				return error_cb(ConfigFlagsError{
					entry:entry,
					kind:ConfigFlagsErrorKind::UnrecognizedSetting(name)
				});
			}
		});
	}
	///Set debugging mode.
	///
	///This method enables debugging mode, with debug event mask of `mask` (constructed from bitwise OR of
	///various `DEBUG_*` constatnts from module [`logging`]) and debug sink `log`.
	///
	///Note that enabling `DEBUG_CRYPTO_CALCS` class requires a build with `enable-key-debug` cargo feature,
	///otherwise that debugging event is just ignored.
	///
	///[`logging`]: logging/index.html
	pub fn set_debug(&mut self, mask: u64, log: Box<dyn Logging+Send+Sync>)
	{
		let alog = Arc::new(log);
		//Update the proxy.
		self.acme_challenges.with_write(|acme|{
			acme.set_log_proxy(LogProxy(Some(alog.clone())));
		});
		self.log.set_debug(mask, alog);
	}
	///Set default server name.
	///
	///This method sets the default server name to `name`.
	///
	///If a request is received without any SNI, then this name is used instead in certificate lookup. If an
	///explicit destination IP is set, then that is used in preference to this.
	///
	///Note, this setting does not affect the SNI reported by [`ServerConnection::get_sni()`].
	///
	///The default is to have no default (any certificate can be sent back).
	///
	///[`ServerConnection::get_sni()`]: struct.ServerConnection.html#method.get_sni
	pub fn set_default_server_name(&mut self, name: &str)
	{
		self.default_sni_name = Some(name.to_owned());
	}
	///Enable/Disable stateless reject mode, setting the enabled flag to `flag`.
	///
	///If stateless reject is enabled then:
	///
	/// 1. TLS 1.2 is disabled.
	/// 2. Sending a handshake restart is a fatal error. The application can use the [`Error::is_restart()`]
	///method to query if error is due to restart, and if so, what was the message sent.
	///
	///Note that the retry attempt must use the same configuration (or clone thereof) as the original.
	///
	///[`Error::is_restart()`]: struct.Error.html#method.is_restart
	pub fn set_stateless_reject(&mut self, flag: bool) { self.stateless = flag; }
	///Set raw handshake mode status to `x`.
	///
	///If raw handshake mode is enabled, then TLS will only run the TLS handshake protocol. The I/O to/from
	///wire will be handshake messages, including the 4-byte message header. Any errors are promptly returned
	///as failures from read/write functions.
	///
	///If raw handshake mode is enabled, then no data is accepted and no data is ever produced. Also,
	///end-of-stream can not be sent or received. Any encryption/decryption keys are passed back to
	///appplication via callbacks.
	//
	//The raw handshake mode is independent of stateless reject mode. Both can be enabled independently.
	pub fn set_raw_handshake(&mut self, x: bool) { self.raw_handshake = x; }
	///Create a server connection with these settings.
	///
	///Note that if you use IP address certificates, then you need to call [`set_destination_address()`]
	///on the created connection in order to set the correct server IP address. This has to be done before
	///sending any data.
	///
	///This method creates a new server-side TLS connection, using setttings set by this object and callbacks
	///`callbacks`.
	///
	///[`set_destination_address()`]: struct.ServerConnection.html#method.set_destination_address
	pub fn make_connection(&self, callbacks: Box<dyn TlsCallbacks+Send>) -> ServerConnection
	{
		let controller = HandshakeController::new(&self, self.log.clone(), callbacks);
		let mut sess = Session::new(self.raw_handshake, controller, self.log.clone());
		//Immediately request RX handshake and readable flag, for receiving the ClientHello.
		sess.request_rx_handshake();
		//Ok.
		ServerConnection(Arc::new(Mutex::new(sess)))
	}
}

///The client side of TLS connection.
///
///Use [`ServerConfiguration::make_connection()`] to create it.
///
///For operations supported, see trait [`Connection`].
///
///If the connection is cloned, both connections refer to the same underlying TLS connection. This can be used to
///refer to the same connection from multiple threads at once.
///
///[`ServerConfiguration::make_connection()`]: struct.ServerConfiguration.html#method.make_connection
///[`ServerConfiguration`]: struct.ServerConfiguration.html
///[`Connection`]: trait.Connection.html
#[derive(Clone)]
pub struct ServerConnection(Arc<Mutex<Session<HandshakeController>>>);

impl_connection_connection!(ServerConnection, true);
impl_session_tls_tx_rx_cycle!(ServerConnection);

///OCSP is supported.
pub const CCSFLAG_OCSP: u8 = 1;
///CT is supported.
pub const CCSFLAG_CT: u8 = 2;
///Transparency Info is supported.
pub const CCSFLAG_TRANS: u8 = 4;

///The client side certificate-related algorithms supported.
///
///This structure contains information about the cryptographic authentication algorithms supported by the client.
///It is mainly useful for Exported Authenticators unsolicted mode, as that mode uses algorithms from ClientHello.
///
///This structure is returned by the [`ServerConnection::get_client_supported_certificates()`] method.
///
///[`ServerConnection::get_client_supported_certificates()`]: ../struct.ServerConnection.html#method.get_client_supported_certificates
#[allow(missing_docs)]
#[derive(Clone)]
pub struct ClientCertificateSupport
{
	#[allow(deprecated)] #[deprecated(since="0.19.5", note="Use .exchange_signatures3 instead")]
	pub exchange_signatures2: btls_aux_signatures::SignatureAlgorithmSet2,
	#[allow(deprecated)] #[deprecated(since="0.19.5", note="Use .chain_signatures3 instead")]
	pub chain_signatures2: btls_aux_signatures::SignatureAlgorithmSet2,
	///The set of signature algorithms the client supports for the exchange signature (the signature in the
	///`CertificateVerify` message).
	pub exchange_signatures3: SetOf<SignatureAlgorithm2>,
	///The set of signature algorithms the client supports for the chain signature (the certificate signatures
	///in the `Certificate` message).
	pub chain_signatures3: SetOf<SignatureAlgorithm2>,
	///The client supports OCSP stapling.
	pub support_ocsp: bool,
	///The client supports Certificate Transparency stapling.
	pub support_ct: bool,
	///The client supports Transparency Item stapling.
	pub support_trans: bool,
	//Expansion.
	_dummy: ()
}

impl ClientCertificateSupport
{
	#[allow(deprecated)]
	fn new() -> ClientCertificateSupport
	{
		let set2 = SetOf::<SignatureAlgorithm2>::empty_set();
		let set = btls_aux_signatures::SignatureAlgorithmSet2::from_set_of(set2);
		ClientCertificateSupport {
			exchange_signatures2: set,
			chain_signatures2: set,
			exchange_signatures3: set2,
			chain_signatures3: set2,
			support_ocsp: false,
			support_ct: false,
			support_trans: false,
			_dummy: ()
		}
	}
	#[allow(deprecated)]
	fn new_info(exchange: SetOf<SignatureAlgorithm2>, chain: SetOf<SignatureAlgorithm2>, flags: u8) ->
		ClientCertificateSupport
	{
		use btls_aux_signatures::SignatureAlgorithmSet2;
		ClientCertificateSupport {
			exchange_signatures2: SignatureAlgorithmSet2::from_set_of(exchange),
			chain_signatures2: SignatureAlgorithmSet2::from_set_of(chain),
			exchange_signatures3: exchange,
			chain_signatures3: chain,
			support_ocsp: flags & CCSFLAG_OCSP != 0,
			support_ct: flags & CCSFLAG_CT != 0,
			support_trans: flags & CCSFLAG_TRANS != 0,
			_dummy: ()
		}
	}
}

impl ServerConnection
{
	fn __call_with_controller<R>(&self, f: impl FnOnce(&HandshakeController) -> R) -> R
	{
		self.0.with(|x|f(x.controller()))
	}
	fn __call_with_controller_mut<R>(&mut self, f: impl FnOnce(&mut HandshakeController) -> R) -> R
	{
		self.0.with(|x|f(x.controller_mut()))
	}
	///Get client supported certificate algorithms.
	///
	///This is mainly useful for Exported Authenticators unsolicted server certificate mode.
	pub fn get_client_supported_certificates(&self) -> ClientCertificateSupport
	{
		self.__call_with_controller(|hs|hs.get_client_supported_certificates())
	}
	///Set source IP address to `addr`.
	///
	///The source IP address is used for validation of source IP address in stateless mode.
	///
	///This has to be set before attempting to receive or send any data.
	pub fn set_source_address(&mut self, addr: IpAddr) -> Result<(), Error>
	{
		self.__call_with_controller_mut(|hs|hs.set_source_address(addr))
	}
	///Set destination IP address to `addr`.
	///
	///The destination IP address is used for certificate selection for IP address certificates.
	///
	///This has to be set before attempting to receive or send any data.
	pub fn set_destination_address(&mut self, addr: IpAddr) -> Result<(), Error>
	{
		self.__call_with_controller_mut(|hs|hs.set_destination_address(addr))
	}
	///Set TLS 1.3 only mode.
	///
	///If called, only TLS 1.3 is accepted, TLS 1.2 gives version negotiation failure.
	///
	///This has to be set before attempting to receive or send any data.
	pub fn set_tls13_only(&mut self) 
	{
		self.__call_with_controller_mut(|hs|hs.set_tls13_only())
	}
	///Set Secure192 mode
	///
	///If called, only ciphers and groups with at least 192 bits of security are accepted.
	///
	///This has to be set before attempting to receive or send any data.
	pub fn set_secure192(&mut self) 
	{
		self.__call_with_controller_mut(|hs|hs.set_secure192())
	}
	///Set best security mode
	///
	///If called, turn the security up to 11, damn performance.
	///
	///This has to be set before attempting to receive or send any data.
	pub fn set_best_security(&mut self)
	{
		self.__call_with_controller_mut(|hs|hs.set_best_security())
	}
	///Disable android compat. hack.
	///
	///This has to be set before attempting to receive or send any data.
	pub fn set_no_android_hack(&mut self)
	{
		self.__call_with_controller_mut(|hs|hs.set_no_android_hack())
	}
	///Enable NSA mode, even if not enabled by global configuration.
	///
	///This has to be set before attempting to receive or send any data.
	pub fn set_nsa_mode(&mut self)
	{
		self.__call_with_controller_mut(|hs|hs.set_nsa_mode())
	}
}
