use crate::callbacks::ClientCertificateInfo;
use crate::callbacks::CryptographicParametersInfo;
use crate::callbacks::ForceRestartParameters;
use crate::callbacks::GotUnknownExtensionParameters;
use crate::callbacks::KeyExportType;
use crate::callbacks::RequestCertificateParameters;
use crate::callbacks::SelectAlpnParameters;
use crate::callbacks::SelectAlpnReturn;
use crate::callbacks::SelectClientCertificateParameters;
use crate::callbacks::ServerNamesParameters;
use crate::callbacks::TargetInfoParameters;
use crate::callbacks::TlsCallbacks;
use crate::certificates::ClientCertificate;
use crate::stdlib::Arc;
use crate::stdlib::Box;
use crate::stdlib::Vec;
use btls_aux_collections::Mutex;
use btls_aux_aead::ProtectorType;
use btls_aux_hash::HashFunction2;
use btls_aux_time::PointInTime;


macro_rules! impl_session_tls_tx_rx_cycle { ($clazz:ident) => {
	impl $clazz {
		fn __borrow_inner<R>(&mut self, f: impl FnOnce(&mut Session<HandshakeController>) -> R) -> R
		{
			if let Some(r) = Arc::get_mut(&mut self.0) { f(r.get_mut()) } else { self.0.with(f) }
		}
		fn do_tls_tx_rx_cycle(&mut self) -> Result<(),Error>
		{
			self.0.with(|x|x.do_tls_tx_rx_cycle(self))
		}
	}
}}

macro_rules! impl_connection_connection { ($clazz:ident, $server:expr) => {
	impl Connection for $clazz
	{
		fn get_status(&self) -> crate::transport::StatusFlags
		{
			self.0.with(|x|x.get_status())
		}
		fn register_on_unblock(&mut self, cb: crate::record::WaitDone) -> bool
		{
			self.__borrow_inner(|x|x.register_on_unblock(cb))
		}
		fn get_error(&self) -> Option<Error>
		{
			self.0.with(|x|x.get_error())
		}
		fn raise_alert(&mut self, alert: u8)
		{
			let alert = btls_aux_tls_iana::Alert::new(alert);
			self.__borrow_inner(|x|x.raise_alert(alert))
		}
		fn push_tcp_data(&mut self, data: crate::transport::PushTcpBuffer) -> Result<(Vec<u8>, bool), Error>
		{
			//By invariants of PushTcpBuffer, enough bytes are initialized.
			use crate::transport::_PushTcpBuffer;
			let res = self.__borrow_inner(|x|match data.0 {
				_PushTcpBuffer::Single(data,max) => unsafe{x.push_tcp_data(&[data], max)},
				_PushTcpBuffer::Multi(data,max) => unsafe{x.push_tcp_data(data, max)},
				//Always return empty vector from pushing EOF, as it never yields any data,
				//and rx has always been EOF'd.
				_PushTcpBuffer::End => x.push_tcp_data_end().map(|_|(Vec::new(), true))
			});
			self.do_tls_tx_rx_cycle()?;
			res
		}
		fn return_buffer_push(&mut self, buf: Vec<u8>)
		{
			self.__borrow_inner(|x|x.return_buffer_push(buf))
		}
		///Get buffer containing internal housekeeping data.
		fn pull_tcp_data(&mut self, data: crate::transport::PullTcpBuffer) -> Result<Vec<u8>, Error>
		{
			//This can happen with waiting for signatures.
			self.do_tls_tx_rx_cycle()?;
			self.__borrow_inner(|x|x.pull_tcp_data(data))
		}
		fn return_buffer_pull(&mut self, buffer: Vec<u8>)
		{
			self.__borrow_inner(|x|x.return_buffer_pull(buffer))
		}
		fn set_mss(&mut self, mss: usize, padmax: bool)
		{
			self.__borrow_inner(|x|x.set_mss(mss, padmax))
		}
		fn exporter(&self, label: &str, context: Option<&[u8]>, buffer: &mut [u8]) -> Result<(), Error>
		{
			self.0.with(|x|x.extractor(label, context, buffer))
		}
		fn set_cid(&mut self, cid: u64)
		{
			self.__borrow_inner(|x|x.set_cid(cid))
		}
		fn enable_ssl_key_log(&mut self)
		{
			self.0.with(|x|x.controller_mut().enable_ssl_key_log())
		}
	}
	impl FutureFinishedNotifier for $clazz
	{
		fn finished(&mut self)
		{
			let wait_done = self.0.with(|y2|{
				y2.do_tx_request();
				//Release the lock, because it should not be called when calling the wait done
				//callback.
				y2.take_wait_done()
			});
			//Call the wait done callback if any.
			if let Some(mut wait_done) = wait_done { wait_done.unblocked(); }
		}
	}
}}

impl<Base:TlsCallbacks> TlsCallbacks for Arc<Mutex<Base>>
{
	fn select_client_certificate(&mut self, params: SelectClientCertificateParameters) ->
		Option<Box<dyn ClientCertificate+Send>>
	{
		self.with(|x|x.select_client_certificate(params))
	}
	fn client_certificate(&mut self, info: ClientCertificateInfo)
	{
		self.with(|x|x.client_certificate(info))
	}
	fn client_authentication(&mut self, info: &ClientCertificateInfo) -> bool
	{
		self.with(|x|x.client_authentication(info))
	}
	fn sent_certificate_request(&mut self)
	{
		self.with(|x|x.sent_certificate_request())
	}
	fn request_certificate(&mut self, params: RequestCertificateParameters) -> bool
	{
		self.with(|x|x.request_certificate(params))
	}
	fn select_alpn(&mut self, params: SelectAlpnParameters) -> SelectAlpnReturn
	{
		self.with(|x|x.select_alpn(params))
	}
	fn target_info(&mut self, params: TargetInfoParameters)
	{
		self.with(|x|x.target_info(params))
	}
	fn server_names(&mut self, params: ServerNamesParameters)
	{
		self.with(|x|x.server_names(params))
	}
	fn cryptographic_parameters(&mut self, info: CryptographicParametersInfo)
	{
		self.with(|x|x.cryptographic_parameters(info))
	}
	fn got_unknown_extension(&mut self, info: GotUnknownExtensionParameters) -> bool
	{
		self.with(|x|x.got_unknown_extension(info))
	}
	fn end_unknown_extension(&mut self, msgtype: u8)
	{
		self.with(|x|x.end_unknown_extension(msgtype));
	}
	fn respond_unknown_extension(&mut self, id: u16) -> Option<Vec<u8>>
	{
		self.with(|x|x.respond_unknown_extension(id))
	}
	fn offer_unknown_extension(&mut self) -> Vec<(u16, Vec<u8>)>
	{
		self.with(|x|x.offer_unknown_extension())
	}
	fn set_deadline(&mut self, deadline: Option<PointInTime>)
	{
		self.with(|x|x.set_deadline(deadline))
	}
	fn force_restart(&mut self, info: ForceRestartParameters) -> bool
	{
		self.with(|x|x.force_restart(info))
	}
	fn rawhs_encryption_key2(&mut self, prf: HashFunction2, prot: ProtectorType, key: &[u8])
	{
		self.with(|x|x.rawhs_encryption_key2(prf, prot, key))
	}
	fn rawhs_decryption_key2(&mut self, prf: HashFunction2, prot: ProtectorType, key: &[u8])
	{
		self.with(|x|x.rawhs_decryption_key2(prf, prot, key))
	}
	fn ssl_key_log(&mut self, ktype: KeyExportType, client_random: [u8;32], secret: &[u8])
	{
		self.with(|x|x.ssl_key_log(ktype, client_random, secret))
	}
	#[cfg(test)]
	fn client_random(&mut self, client_random: [u8;32])
	{
		self.with(|x|x.client_random(client_random))
	}
}
