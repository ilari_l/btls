pub use self::config::*;
use crate::callbacks::CryptographicParametersInfo;
use crate::callbacks::TlsCallbacks;
use crate::certificates::TrustedLog;
use crate::errors::Error;
use crate::features::ConfigPages;
use crate::features::FLAGS0_ALLOW_DYNAMIC_RSA;
use crate::features::FLAGS0_ASSUME_HW_AES;
use crate::features::FLAGS0_NO_ASSUME_HW_AES;
use crate::features::FLAGS0_ONLY_FLOATY_KEYS;
use crate::logging::DebugSetting;
use crate::record::DowncallInterface;
use crate::record::Tls13TrafficSecretIn;
use crate::record::Tls13TrafficSecretOut;
use crate::record::UpcallReturn;
use crate::stdlib::Arc;
use crate::stdlib::Arguments;
use crate::stdlib::AtomicBool;
use crate::stdlib::AtomicOrdering;
use crate::stdlib::Box;
use crate::stdlib::Display;
use crate::stdlib::FmtError;
use crate::stdlib::Formatter;
use crate::stdlib::Mutex;
use crate::stdlib::PhantomData;
use crate::stdlib::replace;
use crate::stdlib::ToOwned;
use crate::stdlib::Vec;
use btls_aux_aead::hw_aes_supported;
use btls_aux_bitflags::Bitflags;
use btls_aux_certificate_transparency::validate_sct_list2;
use btls_aux_collections::BTreeSet;
use btls_aux_dhf::KEM;
use btls_aux_futures::FutureReceiver;
use btls_aux_futures::FutureSink;
use btls_aux_hash::HashFunction2;
use btls_aux_ocsp::extract_scts_from_ocsp;
use btls_aux_ocsp::validate_ocsp3;
use btls_aux_signatures::KeyIdentify2;
use btls_aux_signatures::SignatureAlgorithmEnabled2;
use btls_aux_signatures::SignatureAlgorithmTls2;
use btls_aux_time::Timestamp;
use btls_aux_tls_iana::CertificateStatusType;
use btls_aux_tls_iana::TlsVersion as IanaTlsVersion;
use btls_aux_tls_struct::CertificateStatus;
use btls_aux_tls_struct::IntoGenericIterator as ListOf;
use btls_aux_tls_struct::SerializedSCT;
pub use btls_aux_tlsciphersuites::*;
use btls_aux_x509certparse::CertificateError;
use btls_aux_x509certparse::CertificateIssuer;
use btls_aux_x509certparse::extract_spki;
use btls_aux_x509certparse::ParsedCertificate2;
use btls_aux_x509certvalidation::TrustAnchorOrdered;

//Extension can be treated as unkown.
const EXTENSION_TRIVIAL: u8 = 2;
//Unknown Extension has no response.
const EXTENSION_NORESPONSE: u8 = 4;

macro_rules! ks_debug
{
	($debug:expr) => { Some(&mut |m|$crate::logging::ks_debug($debug,m)) }
}

macro_rules! debug_class
{
	(HANDSHAKE_MSGS) => {crate::logging::DEBUG_HANDSHAKE_MSGS};
	(ABORTS) => {crate::logging::DEBUG_ABORTS};
	(CRYPTO_CALCS) => {crate::logging::DEBUG_CRYPTO_CALCS};
	(HANDSHAKE_DATA) => {crate::logging::DEBUG_HANDSHAKE_DATA};
	(HANDSHAKE_EVENTS) => {crate::logging::DEBUG_HANDSHAKE_EVENTS};
	(TLS_EXTENSIONS) => {crate::logging::DEBUG_TLS_EXTENSIONS};
	(EA_DATA) => {crate::logging::DEBUG_EA_DATA};
	(EA_EVENTS) => {crate::logging::DEBUG_EA_EVENTS};
	(HANDSHAKE_RAWDATA) => {crate::logging::DEBUG_RAWDATA};
}

macro_rules! debug
{
	($class:ident $obj:expr, $($msg:tt)*) => {
		if $obj.prints_for(debug_class!($class)) {
			$obj.debugf(format_args!($($msg)*));
		}
	};
}

#[macro_use]
mod config;
//mod state;
//pub use self::state::*;
#[macro_use] mod session;

//Build built-in SPKI kill-list.
pub fn builtin_killist() -> BTreeSet<[u8; 32]>
{
	let mut x = BTreeSet::new();
	let builtin_kills = include_str!("builtin-cert-blacklist.txt");
	for i in builtin_kills.lines() {
		let mut y = [0u8; 32];
		let mut valid = false;
		for i in i.chars().enumerate() {
			let ch = i.1 as u32;
			let shift = 4 ^ ((i.0 & 1) << 2) as u32;
			if let Some(x) = y.get_mut(i.0>>1) {
				if ch >= 0x30 && ch <= 0x39 {
					*x |= (ch.saturating_sub(0x30)).wrapping_shl(shift) as u8;
				} else if ch >= 0x41 && ch <= 0x46 {
					*x |= (ch.saturating_sub(0x41-10)).wrapping_shl(shift) as u8;
				} else if ch >= 0x61 && ch <= 0x66 {
					*x |= (ch.saturating_sub(0x61-10)).wrapping_shl(shift) as u8;
				} else {
					break;	//Not valid.
				}
			} else {
				break;
			}
			valid |= i.0 == 63;	//Completed.
		}
		if valid { x.insert(y); }
	}
	x
}

#[derive(Copy,Clone,Debug,Bitflags)]
#[bitflag(EMS)]
#[bitflag(OCSP)]
#[bitflag(OCSP_SHORTLIVED)]
#[bitflag(CT)]
#[bitflag(NSA)]
struct CFlags(u8);

pub struct CryptoTracker
{
	version: TlsVersion,
	ciphersuite: Option<Ciphersuite>,
	protection: Option<ProtectorType>,
	hash: Option<HashFunction2>,
	kem: Option<KEM>,
	signature: Option<SignatureAlgorithmTls2>,
	flags: CFlags,
	ccflags: u8,
	overhead: u8,
}

impl CryptoTracker
{
	pub fn new() -> CryptoTracker
	{
		CryptoTracker {
			flags: CFlags::none,
			ccflags: 0,
			version: TlsVersion::Tls12,
			ciphersuite: None,
			protection: None,
			hash: None,
			kem: None,
			signature: None,
			overhead: 0,
		}
	}
	//Set the ciphersuite (and overhead).
	pub fn set_ciphersuite(&mut self, cs: Ciphersuite)
	{
		let mut overhead = 5;	//Record header.
		let is_tls13 = cs.get_key_exchange() == KeyExchangeType::Tls13;
		overhead += if is_tls13 {
			1		//Hidden record type.
		} else if cs.get_protector().is_gcm_style() {
			8		//Explicit nonce.
		} else {
			0		//Nothing extra.
		};
		overhead += cs.get_protector().get_tag_length();	//The MAC tag.
		self.overhead = overhead as u8;
		self.protection = Some(cs.get_protector());
		self.hash = Some(cs.get_prf2());
		self.ciphersuite = Some(cs);
	}
	//set the key exchange group.
	pub fn set_kex_kem(&mut self, newgrp: KEM) { self.kem = Some(newgrp); }
	//set the signature algorithm.
	pub fn set_signature(&mut self, sigalgo: SignatureAlgorithmTls2) { self.signature = Some(sigalgo); }
	//Notify that EMS was enabled.
	pub fn enable_ems(&mut self) { self.flags.set_EMS(); }
	//Notify that TLS 1.3 was enabled.
	pub fn set_version(&mut self, version: TlsVersion) { self.version = version; }
	//Notify validation of CT.
	pub fn ct_validated(&mut self) { self.flags.set_CT(); }
	//Notify validation of OCSP.
	pub fn ocsp_validated(&mut self) { self.flags.set_OCSP() }
	//Notify validation of short-lived certificate.
	pub fn ocsp_shortlived(&mut self) { self.flags |= CFlags::OCSP|CFlags::OCSP_SHORTLIVED; }
	//Notify NSA mode.
	pub fn nsa_mode(&mut self) { self.flags.set_NSA(); }
	//Set client certificate flags.
	pub fn set_client_cert_flags(&mut self, flags: u8) { self.ccflags |= flags; }
	//Get client certificate flags.
	pub fn get_client_cert_flags(&self) -> u8 { self.ccflags }
	//Get connection information.
	pub fn connection_info(&self) -> CryptographicParametersInfo
	{
		//secure192 needs >=192 bit  group and ciphersuite.
		let secure192 = self.kem.map(|x|x.get_classical_security() >= 192).unwrap_or(false) && 
			self.protection.map(|x|x.get_security_level() >= 192).unwrap_or(false);
		CryptographicParametersInfo {
			version: if self.version.is_tls12() { 3 } else { 4 },
			ths_fixed: self.flags.is_EMS() || !self.version.is_tls12(),
			ciphersuite: self.ciphersuite.map(|x|x.tls_id()).unwrap_or(0),
			kex: self.kem.map(|x|x.tls_id()),
			signature: self.signature.map(|x|x.tls_id()),
			version_str: self.version.as_string(),
			protection_str: self.protection.map(|x|x.as_string()).unwrap_or(""),
			hash_str: self.hash.map(|x|x.as_string()).unwrap_or(""),
			kex_str: self.kem.map(|x|x.as_string()).unwrap_or(""),
			signature_str: self.signature.map(|x|x.to_generic().as_string()).unwrap_or(""),
			validated_ct: self.flags.is_CT(),
			validated_ocsp: self.flags.is_OCSP(),
			validated_ocsp_shortlived: self.flags.is_OCSP_SHORTLIVED(),
			record_overhead: self.overhead as usize,
			secure192: secure192,
			nsa: self.flags.is_NSA(),
			pqc: self.kem.map(|x|x.quantum_hard()).unwrap_or(false),
		}
	}
}

pub fn emit_crypto_parameters_event(tracker: &CryptoTracker, callbacks: &mut dyn TlsCallbacks, internal: bool)
{
	if !internal { callbacks.cryptographic_parameters(tracker.connection_info()) }
}


///TLS version.
#[derive(Copy,Clone,Debug)]
pub enum TlsVersion
{
	Tls12,
	Tls13,
	Tls13Draft23,
	Tls13Draft28,
}

impl TlsVersion
{
	pub fn supported_tls13() -> &'static [TlsVersion]
	{
		static RET: [TlsVersion;1] = [TlsVersion::Tls13];
		&RET
	}
	pub fn by_tls_id(ver_id: IanaTlsVersion, allow_draft: bool) -> Result<TlsVersion, Error>
	{
		Ok(match ver_id {
			IanaTlsVersion::TLS_1_2 => TlsVersion::Tls12,			//TLS 1.2
			IanaTlsVersion::TLS_1_3 => TlsVersion::Tls13,			//TLS 1.3.
			IanaTlsVersion::TLS_1_3_DRAFT23 if allow_draft => TlsVersion::Tls13Draft23,
			IanaTlsVersion::TLS_1_3_DRAFT28 if allow_draft => TlsVersion::Tls13Draft28,
			IanaTlsVersion::SSL_3_0 => fail!(error!(ssl3_not_supported)),
			IanaTlsVersion::TLS_1_0 => fail!(error!(tls10_not_supported)),
			IanaTlsVersion::TLS_1_1 => fail!(error!(tls11_not_supported)),
			x => fail!(error!(bogus_sh_version x)),
		})
	}
	pub fn is_tls12(&self) -> bool
	{
		match *self {
			TlsVersion::Tls12 => true,
			_ => false,		//Only Tls12 is TLS 1.2.
		}
	}
	pub fn to_code(&self) -> IanaTlsVersion
	{
		match *self {
			TlsVersion::Tls12 => IanaTlsVersion::TLS_1_2,
			TlsVersion::Tls13 => IanaTlsVersion::TLS_1_3,
			TlsVersion::Tls13Draft23 => IanaTlsVersion::TLS_1_3_DRAFT23,
			TlsVersion::Tls13Draft28 => IanaTlsVersion::TLS_1_3_DRAFT28,
		}
	}
	pub fn as_string(&self) -> &'static str
	{
		match *self {
			TlsVersion::Tls12 => "1.2",
			TlsVersion::Tls13 => "1.3",
			TlsVersion::Tls13Draft23 => "1.3-draft23",
			TlsVersion::Tls13Draft28 => "1.3-draft28",
		}
	}
}

impl Display for TlsVersion
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		write!(fmt, "TLS {ver}", ver=self.as_string())
	}
}

pub fn extension_flags(id: u16) -> u8
{
	use btls_aux_tls_iana::ExtensionType as E;
	const RESV40: E = E::new(40);
	const RESV46: E = E::new(46);
	match E::new(id) {
		//Extensions with nontrivial effects.
		E::SERVER_NAME|E::MAX_FRAGMENT_LENGTH|E::CLIENT_CERTIFICATE_URL|E::TRUNCATED_HMAC|
		E::STATUS_REQUEST|E::USER_MAPPING|E::CLIENT_AUTHZ|E::SERVER_AUTHZ|E::CERT_TYPE|E::SUPPORTED_GROUPS|
		E::EC_POINT_FORMATS|E::SRP|E::SIGNATURE_ALGORITHMS|E::HEARTBEAT|
		E::APPLICATION_LAYER_PROTOCOL_NEGOTIATION|E::STATUS_REQUEST_V2|E::SIGNED_CERTIFICATE_TIMESTAMP|
		E::CLIENT_CERTIFICATE_TYPE|E::SERVER_CERTIFICATE_TYPE|E::PADDING|E::ENCRYPT_THEN_MAC|
		E::EXTENDED_MASTER_SECRET|E::CACHED_INFO|E::TLS_LTS|E::COMPRESS_CERTIFICATE|E::RECORD_SIZE_LIMIT|
		E::PWD_PROTECT|E::PWD_CLEAR|E::PASSWORD_SALT|E::TICKET_PINNING|E::TLS_CERT_WITH_EXTERN_PSK|
		E::DELEGATED_CREDENTIALS|E::SESSION_TICKET|RESV40|E::PRE_SHARED_KEY|E::EARLY_DATA|
		E::SUPPORTED_VERSIONS|E::COOKIE|E::PSK_KEY_EXCHANGE_MODES|RESV46|E::POST_HANDSHAKE_AUTH|
		E::SIGNATURE_ALGORITHMS_CERT|E::KEY_SHARE|E::TRANSPARENCY_INFO|E::TICKET_REQUEST|E::DNSSEC_CHAIN|
		E::RENEGOTIATION_INFO => 0,
		//Extensions that are trivial and have reply.
		E::USE_SRTP|E::TOKEN_BINDING|E::EXTERNAL_ID_HASH|E::EXTERNAL_SESSION_ID|
		E::QUIC_TRANSPORT_PARAMETERS => EXTENSION_TRIVIAL,
		//Extensions that are trivial and have no reply.
		E::TRUSTED_CA_KEYS|E::CERTIFICATE_AUTHORITIES|E::OID_FILTERS =>
			EXTENSION_TRIVIAL|EXTENSION_NORESPONSE,
		//The unknown extensions are assumed not have nontrivial effects.
		_ => EXTENSION_TRIVIAL
	}
}

pub fn is_unknown_extension(id: u16) -> bool { extension_flags(id) & EXTENSION_TRIVIAL != 0 }
pub fn unknown_allowed_in_ee(id: u16) -> bool
{
	extension_flags(id) & (EXTENSION_TRIVIAL|EXTENSION_NORESPONSE) == EXTENSION_TRIVIAL
}


struct DecaySctList<'a>(&'a Vec<(bool, Vec<u8>)>, usize);

impl<'a> Iterator for DecaySctList<'a>
{
	type Item = (bool, &'a [u8]);
	fn next(&mut self) -> Option<(bool, &'a [u8])>
	{
		if self.1 < self.0.len() {
			let x = &self.0[self.1];	//In range by check above.
			self.1 = self.1.saturating_add(1);
			Some((x.0, &x.1))
		} else {
			None
		}
	}
}

struct DecayLogs<'a>(&'a [TrustedLog], usize);

impl<'a> Iterator for DecayLogs<'a>
{
	type Item = &'a TrustedLog;
	fn next(&mut self) -> Option<&'a TrustedLog>
	{
		if self.1 < self.0.len() {
			let x = &self.0[self.1];	//In range by check above.
			self.1 = self.1.saturating_add(1);
			Some(x)
		} else {
			None
		}
	}
}

pub trait ValidationCategory
{
	fn name() -> &'static str;
}

pub struct OcspTag;
impl ValidationCategory for OcspTag
{
	fn name() -> &'static str { "OCSP" }
}

pub struct SctTag;
impl ValidationCategory for SctTag
{
	fn name() -> &'static str { "SCT" }
}

#[derive(Copy,Clone,Debug,Bitflags)]
#[bitflag(REQUIRED)]
#[bitflag(VALIDATED)]
struct VFlags(u8);

impl VFlags
{
	fn validation_done(&mut self)
	{
		self.clear_REQUIRED();
		self.set_VALIDATED();
	}
	fn validated(&self) -> bool { self.is_VALIDATED() }
	fn required(&self) -> bool { self.is_REQUIRED() }
}

pub struct XValidationStatus<Tag:ValidationCategory>
{
	flags: VFlags,
	phantom: PhantomData<Tag>,
}

impl<Tag:ValidationCategory> XValidationStatus<Tag>
{
	pub fn new(init_req: bool) -> XValidationStatus<Tag>
	{
		let flags = VFlags::REQUIRED.and(init_req);
		XValidationStatus{ flags, phantom: PhantomData }
	}
	pub fn is_unsatisfied(&self) -> bool { self.flags.required() }
	pub fn is_validated(&self) -> bool { self.flags.validated() }
}

pub trait GetExtraValidationParameters
{
	//Get the certificate issuer.
	fn get_issuer<'a>(&'a self) -> CertificateIssuer<'a>;
	//Get the certificate serial.
	fn get_serial<'a>(&'a self) -> &'a [u8];
	//Get the issuer key, or None if not available.
	fn get_issuer_key<'a>(&'a self) -> Option<&'a [u8]>;
	//Get the full certificate.
	fn get_full_certificate<'a>(&'a self) -> &'a [u8];
	//Get certificate lifetime.
	fn get_certificate_lifetime<'a>(&self) -> u64;
	//Get the maximum OCSP lifetime.
	fn get_maximum_lifetime<'a>(&self) -> u64;
	//Get the trusted logs list.
	fn get_trusted_logs<'a>(&'a self) -> &'a [TrustedLog];
}

//GetExtraValidationParameters, TLS 1.3-style.
//Issuer key, parsed EE certificate, trusted logs, maximum lifetime.
pub struct Tls13ExtraValidation<'a>(pub Option<&'a [u8]>, pub &'a ParsedCertificate2<'a>, pub &'a [TrustedLog],
	pub u64);

impl<'b> GetExtraValidationParameters for Tls13ExtraValidation<'b>
{
	fn get_issuer<'a>(&'a self) -> CertificateIssuer<'a> { self.1.issuer }
	fn get_serial<'a>(&'a self) -> &'a [u8] { self.1.serial_number }
	fn get_issuer_key<'a>(&'a self) -> Option<&'a [u8]> { self.0 }
	fn get_full_certificate<'a>(&'a self) -> &'a [u8] { self.1.entiere }
	fn get_certificate_lifetime<'a>(&self) -> u64 { self.1.not_before.delta(self.1.not_after) }
	fn get_maximum_lifetime<'a>(&self) -> u64 { self.3 }
	fn get_trusted_logs<'a>(&'a self) -> &'a [TrustedLog] { self.2 }
}

pub struct ValidateExtraTag(bool);

impl ValidateExtraTag
{
	pub fn failed() -> ValidateExtraTag { ValidateExtraTag(false) }
	pub fn is_done(self) -> bool { self.0 }
}

pub struct ValidateExtra
{
	done: bool,
	ocsp_status: XValidationStatus<OcspTag>,
	sct_status: XValidationStatus<SctTag>,
	policy: SignatureAlgorithmEnabled2,
	time_now: Timestamp,
}

impl ValidateExtra
{
	pub fn with_ocsp(ocsp_status: XValidationStatus<OcspTag>, sct_status: XValidationStatus<SctTag>,
		policy: SignatureAlgorithmEnabled2, time_now: Timestamp) -> ValidateExtra
	{
		ValidateExtra {
			done: false,
			ocsp_status: ocsp_status,
			sct_status: sct_status,
			policy: policy,
			time_now: time_now,
		}
	}
	pub fn bypass_ocsp() -> ValidateExtra
	{
		ValidateExtra {
			done: true,
			ocsp_status: XValidationStatus::new(false),
			sct_status: XValidationStatus::new(false),
			//No signatures need to be verified, so set policy to allow-nothing.
			policy: SignatureAlgorithmEnabled2::none(),
			time_now: Timestamp::new(0),	//Does not matter because done=true.
		}
	}
	pub fn as_tag(self) -> ValidateExtraTag { ValidateExtraTag(self.done) }
	pub fn is_ocsp_validated(&self) -> bool { self.done && self.ocsp_status.is_validated() }
	pub fn is_sct_validated(&self) -> bool { self.done && self.sct_status.is_validated() }
	//Perform the OCSP and SCT validation. Also automatically appends the SCTs from OCSP to the list.
	//need_ocsp and need_sct are updated to the actual validaiton status.
	pub fn close<Extra:GetExtraValidationParameters, Cb>(mut self, ocsp: Option<(&[u8], CertificateStatusType)>,
		mut sct: CollectedScts, params: Extra, mut crypto_algs: Option<&mut CryptoTracker>, cb: &Cb) ->
		Result<ValidateExtra, Error> where Cb: Fn(Arguments)
	{
		if self.done { return Ok(self) }
		let time_now = self.time_now;
		//Add SCTs from OCSP even if shortlived, but do not require this to suceed.
		if let Some((ocsp, ocsptype)) = ocsp {
			match sct.add_from_ocsp(ocsp, ocsptype, |x|cb(x)) {
				Ok(_) => (),
				Err(err) => { cb(format_args!("Error reading SCTs from OCSP: {err}")); }
			}
		};

		//Validate SCTs.
		//Hack: Due to sct.validate() consuming crypto_algs, manually assert ct validation from sct_status.
		sct.validate(params.get_full_certificate(), params.get_issuer_key(), &mut self.sct_status,
			params.get_trusted_logs(), None, |x|cb(x), time_now, self.policy)?;
		fail_if!(self.sct_status.is_unsatisfied(), error!(cert_requires_sct));
		if let (true, &mut Some(ref mut x)) = (self.sct_status.is_validated(), &mut crypto_algs) {
			x.ct_validated();
		}

		//Validate OCSP.
		if params.get_certificate_lifetime() <= params.get_maximum_lifetime() {
			validate_ocsp_shortlived(&mut self.ocsp_status, crypto_algs, |x|cb(x));
		} else if let Some((ocsp, ocsptype)) = ocsp {
			validate_ocsp_full(ocsp, ocsptype, params.get_issuer(), params.get_serial(),
				params.get_issuer_key(), params.get_maximum_lifetime(), &mut self.ocsp_status,
				crypto_algs, |x|cb(x), time_now, self.policy)?;
		} else {
			validate_ocsp_none(&mut self.ocsp_status, |x|cb(x));
		}
		fail_if!(self.ocsp_status.is_unsatisfied(), error!(cert_requires_ocsp));

		//Ok.
		self.done = true;
		Ok(self)
	}
}

fn validate_ocsp_none<Cb>(status: &mut XValidationStatus<OcspTag>, cb: Cb) where Cb: Fn(Arguments)
{
	if status.flags.validated() { return; }		//No double-validation.
	cb(format_args!("No stapled OCSP response"));
}

fn validate_ocsp_shortlived<Cb>(status: &mut XValidationStatus<OcspTag>, crypto_algs: Option<&mut CryptoTracker>,
	cb: Cb) where Cb: Fn(Arguments)
{
	if status.flags.validated() { return; }		//No double-validation.
	//OCSP is deemed OK.
	status.flags.validation_done();
	cb(format_args!("Certificate is short-lived: Not checking OCSP"));
	if let Some(algs) = crypto_algs { algs.ocsp_shortlived(); }
}

fn validate_ocsp_full<Cb>(ocsp: &[u8], ocsptype: CertificateStatusType, issuer: CertificateIssuer, serial: &[u8],
	issuer_key: Option<&[u8]>, max_lifetime: u64, status: &mut XValidationStatus<OcspTag>,
	crypto_algs: Option<&mut CryptoTracker>, cb: Cb, time_now: Timestamp,
	policy: SignatureAlgorithmEnabled2) -> Result<(), Error> where Cb: Fn(Arguments)
{
	if ocsptype != CertificateStatusType::OCSP {
		cb(format_args!("Unknown OCSP staple type {ocsptype}, can not verify OCSP"));
		return Ok(());
	}
	if status.flags.validated() { return Ok(()); }		//No double-validation.
	let issuer_key = match issuer_key { Some(x) => x, None => {
		cb(format_args!("No issuer key available, can not verify OCSP"));
		return Ok(());
	}};
	let results = match validate_ocsp3(ocsp, issuer, serial, issuer_key, time_now, policy) {
		Ok(x) => x,
		//Treat is_bad_sigalgo() specially if OCSP is not required (print debug message, but ignore the
		//OCSP otherwise).
		Err(ref e) if !status.flags.required() && e.is_bad_sigalgo() =>
			return Ok(cb(format_args!("Not considering OCSP valid due to unknown/disabled signature"))),
		Err(e) => fail!(error!(invalid_ocsp e))
	};
	if results.response_lifetime > max_lifetime {
		cb(format_args!("Not considering OCSP valid due to excessive lifetime ({lifetime} > {max_lifetime})",
			lifetime=results.response_lifetime));
		//Special error if OCSP stapling is required.
		fail_if!(status.flags.required(),
			error!(ocsp_validity_too_long results.response_lifetime, max_lifetime));
	} else {
		status.flags.validation_done();
		cb(format_args!("Stapled OCSP response validated"));
		if let Some(algs) = crypto_algs { algs.ocsp_validated(); }
	}
	Ok(())
}

pub struct CollectedScts
{
	collected: Vec<(bool, Vec<u8>)>
}

impl CollectedScts
{
	pub fn new() -> CollectedScts { CollectedScts{collected: Vec::new()} }
	pub fn has_entries(&self) -> bool { self.collected.len() > 0 }
	pub fn add_from_certificate<Cb>(&mut self, cert: &ParsedCertificate2, cb: Cb) where Cb: Fn(Arguments)
	{
		for i in cert.scts.iter() {
			cb(format_args!("Received SCT#{num} using certificate.", num=self.collected.len()));
			self.collected.push((true, (*i).to_owned()));
		}
	}
	//Not public, as called only by validate_extra.
	fn add_from_ocsp<Cb>(&mut self, ocsp: &[u8], ocsptype: CertificateStatusType, cb: Cb) -> Result<(), Error>
		where Cb: Fn(Arguments)
	{
		//Don't try to do anything with unknown OCSP types.
		if ocsptype != CertificateStatusType::OCSP { return Ok(()); }
		let sctlist = extract_scts_from_ocsp(ocsp).map_err(|x|error!(cant_extract_sct_ocsp x))?;
		for i in sctlist.list {
			cb(format_args!("Received SCT#{num} using OCSP.", num=self.collected.len()));
			self.collected.push((false, i.to_owned()));
		}
		Ok(())
	}
	pub fn add_raw_final_sct(&mut self, i: &[u8]) -> usize
	{
		let x = self.collected.len();
		self.collected.push((false, i.to_owned()));
		x
	}
	//Not public, as called only by validate_extra.
	fn validate<Cb>(&mut self, certificate: &[u8], issuer_key: Option<&[u8]>,
		status: &mut XValidationStatus<SctTag>, trusted_logs: &[TrustedLog],
		crypto_algs: Option<&mut CryptoTracker>, cb: Cb, time_now: Timestamp,
		policy: SignatureAlgorithmEnabled2) -> Result<(), Error> where Cb: Fn(Arguments)
	{
		if self.collected.len() == 0 { return Ok(()); }
		if status.flags.validated() { return Ok(()); }		//No double-validation.
		let issuer_key = match issuer_key { Some(x) => x, None => {
			cb(format_args!("No issuer key available, can not verify SCT"));
			return Ok(());
		}};
		let mut at_least_one = false;
		validate_sct_list2(certificate, issuer_key, DecaySctList(&self.collected, 0), &mut at_least_one,
			DecayLogs(trusted_logs, 0), &mut |event|cb(format_args!("{event}")), time_now,
			policy).map_err(|x|error!(sct_validation_failed x))?;
		self.collected.clear();
		if at_least_one { status.flags.validation_done(); }
		if let (Some(algs), true) = (crypto_algs, at_least_one) { algs.ct_validated(); }
		Ok(())
	}
}

///Notify about finished future.
pub trait FutureFinishedNotifier
{
	///Notify about finished future.
	fn finished(&mut self);
}

struct _ProcessFutureSink<T:Sized+Send+'static>
{
	value: Mutex<Option<T>>,
	ready: AtomicBool,
	delay: AtomicBool,
}

struct ProcessFutureSink<T:Sized+Send+'static,Target:FutureFinishedNotifier>
{
	inner: Arc<_ProcessFutureSink<T>>,
	target: Option<Target>,
}

impl<T:Sized+Send+'static, Target:FutureFinishedNotifier> FutureSink<T> for ProcessFutureSink<T, Target>
{
	fn sink(&mut self, value: T)
	{
		self.inner.value.with(|x|*x = Some(value));
		//Mark the future as ready, but do not call the notifier unless the future has been delayed.
		//This is necressary, because if future has not been delayed, the call is occuring on the same
		//thread as which is registering the future, so calling the notifier would lead to a deadlock.
		//Whereas delayed futures are called in another thread, so no deadlock will occur, even if the
		//caller has not gotten out of the way yet.
		self.inner.ready.store(true, AtomicOrdering::Release);
		if self.inner.delay.load(AtomicOrdering::Acquire) {
			//Do not call notifier unless delayed.
			if let Some(mut notifier) = replace(&mut self.target, None) { notifier.finished(); }
		}
	}
}

pub struct FutureAMO<T:Sized+Send+'static>(Arc<_ProcessFutureSink<T>>);


impl<T:Sized+Send+'static> FutureAMO<T>
{
	pub fn new(f: FutureReceiver<T>, notifier: impl FutureFinishedNotifier+Send+'static) -> FutureAMO<T>
	{
		let inner = Arc::new(_ProcessFutureSink {
			value: Mutex::new(None),
			ready: AtomicBool::new(false),
			delay: AtomicBool::new(false),
		});
		f.sink_to(Box::new(ProcessFutureSink{
			inner: inner.clone(),
			target: Some(notifier),
		}));
		//The future did not resolve during the sink_to, it is therefore delayed.
		inner.delay.store(true, AtomicOrdering::Release);
		FutureAMO(inner)
	}
	pub fn ready(&self) -> bool
	{
		self.0.ready.load(AtomicOrdering::Acquire)
	}
	pub fn take(&self) -> Option<T>
	{
		self.0.value.with(|x|x.take())
	}
}



pub fn supports_hardware_aes(config: &ConfigPages) -> bool
{
	match config.get_flags() & (FLAGS0_ASSUME_HW_AES | FLAGS0_NO_ASSUME_HW_AES) {
		FLAGS0_ASSUME_HW_AES => true,
		FLAGS0_NO_ASSUME_HW_AES => false,
		_ => hw_aes_supported()
	}
}

pub fn test_failure(flags: &ConfigPages, flag: u64) -> Result<(), Error>
{
	if flags.get_test() & flag != 0 { fail!(error!(test_failure)); }
	Ok(())
}

macro_rules! reveal
{
	(INPUT $irail:expr, $cb:expr) => {
		$irail.reveal(|prf, prot, key|{
			$cb.rawhs_decryption_key2(prf, prot, key);
		})?
	};
	(OUTPUT $orail:expr, $cb:expr) => {
		$orail.reveal(|prf, prot, key|{
			$cb.rawhs_encryption_key2(prf, prot, key);
		})?
	};
}

pub(crate) fn tls13_update_send_key(controller: &mut UpcallReturn, outputrail: &Tls13TrafficSecretOut,
	rawhs: bool, debug: DebugSetting, callbacks: &mut dyn TlsCallbacks) -> Result<(), Error>
{
	if rawhs {
		reveal!(OUTPUT outputrail, callbacks);
	} else {
		controller.set_protector(outputrail.to_protector(ks_debug!(&debug))?);
	}
	Ok(())
}

pub(crate) fn tls13_update_send_key_base(base: &mut impl DowncallInterface, outputrail: &Tls13TrafficSecretOut,
	rawhs: bool, debug: DebugSetting, callbacks: &mut dyn TlsCallbacks) -> Result<(), Error>
{
	if rawhs {
		reveal!(OUTPUT outputrail, callbacks);
	} else {
		base.change_protector(outputrail.to_protector(ks_debug!(&debug))?);
	}
	Ok(())
}

pub(crate) fn tls13_update_recv_key(controller: &mut UpcallReturn, inputrail: &Tls13TrafficSecretIn,
	rawhs: bool, debug: DebugSetting, callbacks: &mut dyn TlsCallbacks) -> Result<(), Error>
{
	if rawhs {
		reveal!(INPUT inputrail, callbacks);
	} else {
		controller.set_deprotector(inputrail.to_deprotector(ks_debug!(&debug))?);
	}
	Ok(())
}

pub(crate) fn tls13_update_recv_key_base(base: &mut impl DowncallInterface, inputrail: &Tls13TrafficSecretIn,
	rawhs: bool, debug: DebugSetting, callbacks: &mut dyn TlsCallbacks) -> Result<(), Error>
{
	if rawhs {
		reveal!(INPUT inputrail, callbacks);
	} else {
		base.change_deprotector(inputrail.to_deprotector(ks_debug!(&debug))?);
	}
	Ok(())
}

pub fn check_dynamic_spki(eecert_spki: &[u8], flags: &ConfigPages) -> Result<(), Error>
{
	let eecert_type = KeyIdentify2::identify(eecert_spki);
	fail_if!(!flags.is_flag(FLAGS0_ALLOW_DYNAMIC_RSA) && eecert_type.is_rsa(), error!(dynamic_rsa_not_allowed));
	fail_if!(flags.is_flag(FLAGS0_ONLY_FLOATY_KEYS) && eecert_type == KeyIdentify2::RsaGeneric,
		error!(key_vulernable_to_drowning));
	Ok(())
}

pub fn collect_tls13_scts<'a,CB>(eecert: &ParsedCertificate2, scts: ListOf<'a,SerializedSCT<'a>>,
	debug: &CB) -> CollectedScts where CB: Fn(Arguments)
{
	let mut staple_scts = CollectedScts::new();
	staple_scts.add_from_certificate(eecert, debug);
	for sct in scts {
		let n = staple_scts.add_raw_final_sct(sct.get_serialized_sct());
		debug(format_args!("Received SCT#{n} using extension."));
	}
	staple_scts
}

pub fn get_issuer_key<'a>(issuers: &[&'a [u8]], issuer: CertificateIssuer,
	tpile: &'a BTreeSet<TrustAnchorOrdered>) -> Option<&'a [u8]>
{
	if let Some(ref x) = tpile.get(issuer.as_raw_issuer()) {
		x.borrow_inner().get_spki()
	} else {
		issuers.get(0).and_then(|x|extract_spki(x).ok())
	}
}

pub fn make_tls13_extra<'a,'b>(eecert: &'a ParsedCertificate2<'b>, issuers: &[&'a [u8]],
	tpile: &'a BTreeSet<TrustAnchorOrdered>, trusted_logs: &'a [TrustedLog], ocsp_maxvalid: u64) ->
	Tls13ExtraValidation<'a>
{
	let issuer_key = get_issuer_key(issuers, eecert.issuer, tpile);
	Tls13ExtraValidation(issuer_key, eecert, trusted_logs, ocsp_maxvalid)
}

pub fn certificate_status_to_ocsp<'a>(s: Option<CertificateStatus<'a>>) -> Option<(&'a [u8], CertificateStatusType)>
{
	s.and_then(|s|s.as_ocsp()).map(|s|(s.get_response(), CertificateStatusType::OCSP))
}


pub(crate) struct X509CertMaybeParsed<'a>
{
	raw: &'a [u8],
	key: Result<Option<&'a [u8]>, CertificateError>,
	cert: Result<Option<ParsedCertificate2<'a>>, CertificateError>,
}

impl<'a> X509CertMaybeParsed<'a>
{
	pub(crate) fn new(raw: &'a [u8]) -> X509CertMaybeParsed<'a>
	{
		X509CertMaybeParsed {
			raw: raw,
			key: Ok(None),
			cert: Ok(None),
		}
	}
	pub(crate) fn get_raw(&self) -> &'a [u8] { self.raw }
	pub(crate) fn get_key(&mut self) -> Result<&'a [u8], CertificateError>
	{
		//If certificate has been parsed, grab key from that.
		if let &Ok(Some(ref c)) = &self.cert { return Ok(c.pubkey); }
		//If not yet parsed, do parse.
		if let &Ok(None) = &self.key { self.key = extract_spki(self.raw).map(|k|Some(k)); }
		//Return the result.
		match &self.key {
			&Ok(Some(k)) => Ok(k),
			//How the fuck is this possible??? Just return some bogus error.
			&Ok(None) => Err(CertificateError::NoExtensions),
			&Err(ref err) => Err(err.clone()),
		}
	}
	pub(crate) fn get_certificate<'b>(&'b mut self) -> Result<&'b ParsedCertificate2<'a>, CertificateError>
	{
		//If key parse has failed, that is going to make certificate parse fail as well, do not even
		//try.
		if let &Err(ref err) = &self.key { fail!(err.clone()); }
		//If not yet parsed, do parse.
		if let &Ok(None) = &self.cert { self.cert = ParsedCertificate2::from(self.raw).map(|k|Some(k)); }
		//Return the result.
		match &self.cert {
			&Ok(Some(ref k)) => Ok(k),
			//How the fuck is this possible??? Just return some bogus error.
			&Ok(None) => Err(CertificateError::NoExtensions),
			&Err(ref err) => Err(err.clone()),
		}
	}
}


#[test]
fn test_builtin_killist()
{
	assert_eq!(builtin_killist().len(), 2);
}
