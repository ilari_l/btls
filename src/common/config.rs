use crate::certificates::TrustedLog;
use crate::certificates::TrustAnchor;
use crate::features::ConfigFlagsEntry;
use crate::features::ConfigFlagsError;
use crate::features::ConfigFlagsErrorKind;
use crate::stdlib::Duration;
use crate::stdlib::FromStr;
use crate::stdlib::IoRead;
use crate::stdlib::String;
use crate::stdlib::ToOwned;
use crate::stdlib::ToString;
use crate::stdlib::Vec;
use crate::system::File;
use btls_aux_certificate_transparency::TrustedLogInfo2;
use btls_aux_certificate_transparency_builtin::BuiltinLogData;
use btls_aux_collections::BTreeSet;
use btls_aux_signatures::Base64Decoder;
use btls_aux_signatures::iterate_pem_or_derseq_fragments;
use btls_aux_signatures::PemDerseqFragment2 as PFragment;
use btls_aux_signatures::PemFragmentKind as PFKind;
use btls_aux_x509certvalidation::TrustAnchorOrdered;

macro_rules! config_strval
{
	($func:expr, $value:expr, $error_cb: expr, $entry:expr, $target:expr) => {
		if let &crate::features::ConfigFlagsValue::Explicit(ref val) = &$value {
			return $func(val, $error_cb, $entry, $target);
		} else {
			true
		}
	}
}

//Handle blacklist entry.
pub fn handle_blacklist<F>(value: &str, error_cb: &mut F, entry: ConfigFlagsEntry,
	killist: &mut BTreeSet<[u8; 32]>) where F: FnMut(ConfigFlagsError)
{
	let mut tmp = [0u8; 32];
	let mut valid = true;
	valid &= value.len() == 64;
	for i in value.chars().enumerate() {
		let val = match i.1 {
			'0' => 0, '1' => 1, '2' => 2, '3' => 3, '4' => 4,
			'5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9,
			'a' => 10, 'b' => 11, 'c' => 12, 'd' => 13, 'e' => 14, 'f' => 15,
			'A' => 10, 'B' => 11, 'C' => 12, 'D' => 13, 'E' => 14, 'F' => 15,
			_ => { valid = false; 0 }
		} as u8;
		if let Some(ptr) = tmp.get_mut(i.0>>1) {
			*ptr |= val << (4 - 4 * (i.0 & 1));
		}
	}
	if valid {
		killist.insert(tmp);
	} else {
		error_cb(ConfigFlagsError {
			entry:entry,
			kind:ConfigFlagsErrorKind::Error("The arguemnt needs to be 64 hexdigits")
		})
	}
}

//Handle trust anchor entry.
pub fn handle_trustanchor<F>(value: &str, error_cb: &mut F, entry: ConfigFlagsEntry,
	trust_anchors: &mut BTreeSet<TrustAnchorOrdered>) where F: FnMut(ConfigFlagsError)
{
	let mut content = Vec::new();
	if let Err(x) = File::open(value).and_then(|mut f|f.read_to_end(&mut content)) {
		return error_cb(ConfigFlagsError{
			entry:entry,
			kind:ConfigFlagsErrorKind::Error(&x.to_string())
		})
	};
	use self::PFragment::*;
	let mut cnt = 1;
	iterate_pem_or_derseq_fragments(&content, |frag|match frag {
		Comment(_) => Ok(()),
		Error => return Err(error_cb(ConfigFlagsError{
			entry:entry,
			kind:ConfigFlagsErrorKind::Error("Trust anchor file is neither PEM nor DERseq")
		})),
		Der(mut cert)|Pem(PFKind::Certificate|PFKind::TrustedCertificate, mut cert) => {
			match TrustAnchor::from_certificate(&mut cert) {
				Ok(x) => {trust_anchors.insert(x.as_ordered());},
				Err(err) => error_cb(ConfigFlagsError{
					entry:entry,
					kind:ConfigFlagsErrorKind::Error(&format!("Cert #{cnt} bad: {err}"))
				})
			};
			cnt += 1;
			Ok(())
		},
		Pem(_,_) => Ok(())	//Ignore unknown.
	}).ok();
}

fn handle_builin_ctlog2(log: &BuiltinLogData, trusted_logs: &mut Vec<TrustedLog>)
{
	trusted_logs.push(TrustedLog::from(log as &dyn TrustedLogInfo2));
}

fn handle_builin_ctlog<F>(value: &str, error_cb: &mut F, entry: ConfigFlagsEntry,
	trusted_logs: &mut Vec<TrustedLog>) where F: FnMut(ConfigFlagsError)
{
	if value == "*" {
		for log in BuiltinLogData::get_all_logs() { handle_builin_ctlog2(log, trusted_logs); }
	} else {
		if let Some(log) = BuiltinLogData::for_log(value) {
			handle_builin_ctlog2(log, trusted_logs);
		} else {
			return error_cb(ConfigFlagsError{
				entry:entry,
				kind:ConfigFlagsErrorKind::Error(&format!("Unknown builin log {value}"))
			})
		}
	}
}

//Handle ctlog entry.
pub fn handle_ctlog<F>(value: &str, error_cb: &mut F, entry: ConfigFlagsEntry,
	trusted_logs: &mut Vec<TrustedLog>) where F: FnMut(ConfigFlagsError)
{
	//Handle builin logs specially.
	if let Some(value) = value.strip_prefix("?") {
		return handle_builin_ctlog(value, error_cb, entry, trusted_logs);
	}
	let mut content = String::new();
	if let Err(x) = File::open(value).and_then(|mut f|f.read_to_string(&mut content)) {
		return error_cb(ConfigFlagsError {
			entry:entry,
			kind:ConfigFlagsErrorKind::Error(&x.to_string())
		});
	};
	for line in content.split('\n').enumerate() {
		if line.1.len() == 0 { continue; }
		let version = line.1.split(':').next();
		let (key, expiry, name) = if version == Some("v1") {
			let mut itr = line.1.splitn(4, ':');
			let _ = itr.next();
			let key = itr.next();
			let expiry = itr.next();
			let name = itr.next();
			(key, expiry, name)
		} else {
			error_cb(ConfigFlagsError {
				entry:entry,
				kind:ConfigFlagsErrorKind::Error(
					&format!("Unknown log version on line {line}", line=line.0)
				)
			});
			continue;
		};
		//Key, expiry and name must be present.
		let (key, expiry, name) = if let (Some(x), Some(y), Some(z)) = (key, expiry, name) {
			let mut _key = Vec::new();
			let mut dec = Base64Decoder::new();
			match dec.data(x, &mut _key).and_then(|_|dec.end(&mut _key)) {
				Ok(_) => (),
				Err(_) => {
					error_cb(ConfigFlagsError {
						entry:entry,
						kind:ConfigFlagsErrorKind::Error(
							&format!("Bad key on line {line}", line=line.0)
						)
					});
					continue;
				}
			};
			let _expiry = if y == "" {
				None
			} else {
				Some(match i64::from_str(y) {
					Ok(x) => x,
					Err(_) => {
						error_cb(ConfigFlagsError {
							entry:entry,
							kind:ConfigFlagsErrorKind::Error(
								&format!("Bad expiry on line {line}", line=line.0)
							)
						});
						continue;
					}
				})
			};
			(_key, _expiry, z)
		} else {
			error_cb(ConfigFlagsError {
				entry:entry,
				kind:ConfigFlagsErrorKind::Error(&format!("Parse error on line {line}", line=line.0))
			});
			continue;
		};
		let log = TrustedLog {
			name: name.to_owned(),
			key: key,
			expiry: expiry
		};
		trusted_logs.push(log);
	}
}

//Handle ocsp maxvalid entry.
pub fn handle_ocsp_maxvalid<F>(value: &str, error_cb: &mut F, entry: ConfigFlagsEntry,
	ocsp_maxvalid: &mut u64) where F: FnMut(ConfigFlagsError)
{
	*ocsp_maxvalid = match u64::from_str(value) {
		Ok(x) => x,
		Err(_) => {
			error_cb(ConfigFlagsError {
				entry:entry,
				kind:ConfigFlagsErrorKind::Error(&format!(
					"Can't parse ocsp-maxvalid value as number"))
			});
			return;
		}
	};
}

//Handle handshake deadline entry.
pub fn handle_hs_timelimit<F>(value: &str, error_cb: &mut F, entry: ConfigFlagsEntry,
	deadline: &mut Option<Duration>) where F: FnMut(ConfigFlagsError)
{
	if value == "" {
		*deadline = None;	//Special.
	} else {
		let delta = match f64::from_str(value) {
			Ok(x) => x,
			Err(_) => {
				error_cb(ConfigFlagsError {
					entry:entry,
					kind:ConfigFlagsErrorKind::Error(&format!(
						"Can't parse handshake-deadline value as number"))
				});
				return;
			}
		};
		if delta < 0.0 {
			error_cb(ConfigFlagsError {
				entry:entry,
				kind:ConfigFlagsErrorKind::Error(&format!("Handshake-deadline can not be negative"))
			});
			return;
		}
		let secs = delta.floor();
		//floor is always smaller or equal to number itself.
		let nsecs = ((delta - secs) * 1000000000.0) as u32;
		*deadline = Some(Duration::new(secs as u64, nsecs));
	}
}
