//This module is internal.
#![allow(missing_docs)]

pub use super::std::error::Error;
pub use super::std::ffi::CStr;
pub use super::std::ffi::CString;
pub use super::std::ffi::OsStr;
pub use super::std::ffi::OsString;
pub use super::std::io::Error as IoError;
pub use super::std::io::ErrorKind as IoErrorKind;
pub use super::std::io::Read as IoRead;
pub use super::std::io::Result as IoResult;
pub use super::std::io::Write as IoWrite;
pub use super::std::net::IpAddr;
pub use super::std::net::Ipv4Addr;
pub use super::std::net::Ipv6Addr;
pub use super::std::time::Duration;
pub use super::std::time::UNIX_EPOCH;
pub use btls_aux_collections::Arc;
pub use btls_aux_collections::Box;
pub use btls_aux_collections::BTreeMap;
pub use btls_aux_collections::Cow;
pub use btls_aux_collections::Mutex;
pub use btls_aux_collections::Once;
pub use btls_aux_collections::Rc;
pub use btls_aux_collections::RefCell;
pub use btls_aux_collections::RwLock;
pub use btls_aux_collections::String;
pub use btls_aux_collections::ToString;
pub use btls_aux_collections::ToOwned;
pub use btls_aux_collections::Vec;
pub use btls_aux_collections::Weak;
pub use core::cmp::Eq;
pub use core::cmp::max;
pub use core::cmp::min;
pub use core::cmp::Ord;
pub use core::cmp::Ordering;
pub use core::cmp::PartialEq;
pub use core::cmp::PartialOrd;
pub use core::convert::AsRef;
pub use core::convert::From;
pub use core::default::Default;
pub use core::fmt::Arguments;
pub use core::fmt::Debug;
pub use core::fmt::Display;
pub use core::fmt::Error as FmtError;
pub use core::fmt::Formatter;
pub use core::fmt::Result as FmtResult;
pub use core::fmt::Write as FmtWrite;
pub use core::hash::Hash;
pub use core::hash::Hasher;
pub use core::i64::MAX as I64_MAX;
pub use core::iter::FromIterator;
pub use core::iter::once;
pub use core::marker::PhantomData;
pub use core::mem::replace;
pub use core::mem::size_of;
pub use core::mem::swap;
pub use core::mem::transmute;
pub use core::mem::zeroed;
pub use core::ops::Add;
pub use core::ops::BitOr;
pub use core::ops::Deref;
pub use core::ops::DerefMut;
pub use core::ops::Range;
pub use core::ops::RangeFrom;
pub use core::ops::RangeFull;
pub use core::ops::RangeTo;
pub use core::ptr::null;
pub use core::ptr::write_volatile;
pub use core::slice::from_raw_parts;
pub use core::slice::Iter as SliceIter;
pub use core::str::from_utf8;
pub use core::str::FromStr;
pub use core::sync::atomic::AtomicBool;
pub use core::sync::atomic::AtomicUsize;
pub use core::sync::atomic::fence;
pub use core::sync::atomic::Ordering as AtomicOrdering;

///Call `f` while borrowing `c` and forward return value.
pub fn refcell_with<T,R,F:FnOnce(&mut T)->R>(c: &RefCell<T>, f: F) -> R
{
	let mut c = c.borrow_mut();
	f(&mut c)
}
