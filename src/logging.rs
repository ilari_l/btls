//!Types related to logging
//!
//!This module contains the following types:
//!
//! * Log sinks:
//!   * The base trait for logging: [`Logging`](trait.Logging.html).
//!   * Null log implementation that just discards all messages: [`NullLog`](struct.NullLog.html).
//!   * Log implementation that just prints the messages to stderr: [`StderrLog`](struct.StderrLog.html).
//!   * Log implementation that sinks the messages into a file: [`FileLog`](struct.FileLog.html).
//! * Print as hexadecimal:
//!   * Function to print an octet string as hexadecimal: [`bytes_as_hex`](fn.bytes_as_hex.html).
//!   * Functions to pretty-print long octet string as hexdump block:
//![`bytes_as_hex_block`](fn.bytes_as_hex_block.html) and
//![`bytes_as_hex_block_iter`](fn.bytes_as_hex_block_iter.html).
//! * Function to pretty-print a TLS handshake message name [`explain_hs_type`](fn.explain_hs_type.html).
//! * The definitions of the `DEBUG_*` constants.

use crate::messages::DumpHexblock;
use crate::stdlib::Arc;
use crate::stdlib::Box;
use crate::stdlib::Cow;
use crate::stdlib::Deref;
use crate::stdlib::IoWrite;
use crate::stdlib::Mutex;
use crate::stdlib::String;
use crate::stdlib::ToString;
use crate::stdlib::UNIX_EPOCH;
use crate::system::File;
use crate::system::stderr;
use crate::system::SystemTime;
use btls_aux_rope3::FragmentWriteDyn as FragmentWriteDyn3;
use btls_aux_time::daynumber_into_date;
use btls_aux_tls_iana::HandshakeType;
use btls_aux_tlskeyschedule::TlsDebug;
use core::fmt::Arguments;


///Message severity.
///
///These values are taken from syslog.
#[derive(Copy,Clone,Debug)]
pub enum MessageSeverity
{
	///System is unusuable
	Emergency,
	///Action must be taken immediately.
	Alert,
	///Critical condition.
	Critical,
	///Error message
	Error,
	///Warning message
	Warning,
	///Significant condition.
	Notice,
	///Informational
	Informational,
	///Debuggig message
	Debug
}

impl MessageSeverity
{
	///Get the textual prefix (e.g. `Error`), for the severity.
	///
	///The used strings are `Emergency`, `Alert`, `Critical`, `Error`, `Warning`, `Notice`, `Informational`
	///and `Debug`.
	pub fn textual_prefix(self) -> &'static str
	{
		match self {
			MessageSeverity::Emergency => "Emergency",
			MessageSeverity::Alert => "Alert",
			MessageSeverity::Critical => "Critical",
			MessageSeverity::Error => "Error",
			MessageSeverity::Warning => "Warning",
			MessageSeverity::Notice => "Notice",
			MessageSeverity::Informational => "Informational",
			MessageSeverity::Debug => "Debug",
		}
	}
	///Get the code prefix (e.g. `<E>`), for the severity.
	///
	///The used letters are `X`, `A`, `C`, `E`, `W`, `N`, `I` and `D`.
	pub fn code_prefix(self) -> &'static str
	{
		match self {
			MessageSeverity::Emergency => "<X>",
			MessageSeverity::Alert => "<A>",
			MessageSeverity::Critical => "<C>",
			MessageSeverity::Error => "<E>",
			MessageSeverity::Warning => "<W>",
			MessageSeverity::Notice => "<N>",
			MessageSeverity::Informational => "<I>",
			MessageSeverity::Debug => "<D>",
		}
	}
}

///Null Log.
///
///This is a null implementation of [`Logging`] trait. The messages are simply just discarded.
///
///[`Logging`]: trait.Logging.html
pub struct NullLog;

impl Logging for NullLog
{
	fn message(&self, _severity: MessageSeverity, _x: Cow<'static, str>)
	{
		//Do nothing.
	}
}

///Stderr Log.
///
///This is a simple implementation of [`Logging`] trait. The messages are simply printed to stderr.
///
///[`Logging`]: trait.Logging.html
pub struct StderrLog;

impl Logging for StderrLog
{
	fn message(&self, severity: MessageSeverity, msg: Cow<'static, str>)
	{
		//Just discard errors.
		writeln!(stderr(), "{pfx}: {msg}", pfx=severity.textual_prefix()).ok();
	}
	fn message_ex(&self, cid: Option<u64>, severity: MessageSeverity, x: Arguments)
	{
		//Just discard errors.
		let pfx = severity.textual_prefix();
		let x = x.to_string();
		for line in x.deref().lines() {
			if let Some(cid) = cid {
				writeln!(stderr(), "{pfx}: [{cid:x}]{line}").ok();
			} else {
				writeln!(stderr(), "{pfx}: {line}").ok();
			}
		}
	}
}

///File log.
///
///This is a simple implementation of [`Logging`] trait. The messages are printed to file, together with timestamps.
///
///This structure is created using the [`new()`](#method.new) method.
///
///[`Logging`]: trait.Logging.html
pub struct FileLog(Mutex<File>);

impl FileLog
{
	///Create a file log logging to File `f`.
	pub fn new(f: File) -> FileLog { FileLog(Mutex::new(f)) }
}

fn format_timestamp() -> String
{
	let (secs, nsecs) = match SystemTime::now().duration_since(UNIX_EPOCH) {
		Ok(x) => (x.as_secs() as i64, x.subsec_nanos() as i64),
		Err(x) => (-(x.duration().as_secs() as i64), -(x.duration().subsec_nanos() as i64))
	};
	let ts = secs.saturating_mul(1000) + nsecs / 1000000;	//Milliseconds relative to epoch.
	let (year, month, day) = daynumber_into_date(ts / 86400000);
	let ts = (ts % 86400000 + 86400000) % 86400000;		//Time of day.
	let hour = ts / 3600000;
	let ts = ts % 3600000;
	let minute = ts / 60000;
	let ts = ts % 60000;
	let second = ts / 1000;
	let msec = ts % 1000;
	let timestamp = format!("{year:04}-{month:02}-{day:02} {hour:02}:{minute:02}:{second:02}.{msec:03}Z");
	timestamp
}

impl Logging for FileLog
{
	fn message(&self, severity: MessageSeverity, x: Cow<'static, str>)
	{
		let timestamp = format_timestamp();
		self.0.with(|f|{
			for line in x.deref().lines() {
				let _ = writeln!(f, "{timestamp} {pfx}: {line}", pfx=severity.textual_prefix());
			}
		})
	}
	fn message_ex(&self, cid: Option<u64>, severity: MessageSeverity, x: Arguments)
	{
		let timestamp = format_timestamp();
		let x = x.to_string();
		self.0.with(|f|{
			for line in x.deref().lines() {
				let pfx = severity.textual_prefix();
				let _ = if let Some(cid) = cid {
					writeln!(f, "{timestamp} {cid:x} {pfx}: {line}")
				} else {
					writeln!(f, "{timestamp} {pfx}: {line}")
				};
			}
		})
	}
}

#[test]
fn test_filelog()
{
	let f = File::create("test.log").unwrap();
	let f = FileLog::new(f);
	f.message(MessageSeverity::Notice, Cow::Borrowed("foo\nbar"));
}

///Debugging:Log handshake messages.
pub const DEBUG_HANDSHAKE_MSGS: u64 = 1;
///Debugging: Log Aborts.
pub const DEBUG_ABORTS: u64 = 2;
///Debugging: Log crypto calculations.
pub const DEBUG_CRYPTO_CALCS: u64 = 4;
///Debugging: Log Handshake message contents.
pub const DEBUG_HANDSHAKE_DATA: u64 = 8;
///Debugging: Log Handshake events.
pub const DEBUG_HANDSHAKE_EVENTS: u64 = 16;
///Debugging: Log TLS extensions.
pub const DEBUG_TLS_EXTENSIONS: u64 = 32;
///Debugging: Log Exported authenticator events.
pub const DEBUG_EA_EVENTS: u64 = 64;
///Debugging: Log Exported authenticator data.
pub const DEBUG_EA_DATA: u64 = 128;
///Debugging: Log raw handshake records.
pub const DEBUG_RAWDATA: u64 = 256;

const RAW_DEBUG_MASK: u64 = 0x1ff;

//The mask of events allowed for debugging.
#[doc(hidden)]
pub const DEBUG_MASK: u64 = RAW_DEBUG_MASK;


///Logging callback.
///
///This trait models a logging callback
pub trait Logging
{
	///This method logs the specified message `msg` at severity `severity`.
	fn message(&self, severity: MessageSeverity, msg: Cow<'static, str>);
	///This method logs the specified message `msg` at severity `severity`, associated with `cid`
	fn message_ex(&self, cid: Option<u64>, severity: MessageSeverity, msg: Arguments)
	{
		let msg =  if let Some(cid) = cid { format!("[cid {cid}]{msg}") } else { format!("{msg}") };
		self.message(severity, Cow::Owned(msg));
	}
}

//Flag this as public, as it is used in lots of places hitting private-in-public error.
#[doc(hidden)]
#[derive(Clone)]
pub struct DebugSetting
{
	cid: Option<u64>,
	target: Option<(u64, Arc<Box<dyn Logging+Send+Sync>>)>,
}

impl DebugSetting
{
	pub(crate) fn new() -> DebugSetting
	{
		DebugSetting {
			cid: None,
			target: None,
		}
	}
	pub(crate) fn set_debug(&mut self, mask: u64, target: Arc<Box<dyn Logging+Send+Sync>>)
	{
		self.target = Some((mask, target));
	}
	pub(crate) fn set_cid(&mut self, cid: u64) { self.cid = Some(cid); }
	pub(crate) fn get_cid(&self) -> Option<u64> { self.cid }
	pub(crate) fn prints_for(&self, event: u64) -> bool
	{
		if let Some((emask, _)) = self.target.as_ref() {
			event & DEBUG_MASK & emask != 0
		} else {
			false
		}
	}
	pub(crate) fn debugf(&self, msg: Arguments)
	{
		if let Some((_, target)) = self.target.as_ref() {
			target.message_ex(self.cid, MessageSeverity::Debug, msg);
		}
	}
}

///Output a octet string `string` as text string using hexadecimal digits.
///
///The length of output is 2 times the length of input, and consists of characters `0-9` and `A-F`.
pub fn bytes_as_hex(string: &[u8]) -> String
{
	let mut out = String::new();
	const HEXES: [char;16] = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];
	for i in string.iter() {
		out.push(HEXES.get((i >> 4) as usize).map(|x|*x).unwrap_or('?'));
		out.push(HEXES.get((i & 15) as usize).map(|x|*x).unwrap_or('?'));
	}
	out
}

///Output a ocet string `string` as hexdump block.
///
///Each line is prefixed with `lpfx` (which may be a empty string to omit prefix). The whole block is named as
///`name`. The name can be at most 35 codepoints. The hexadecimal block is split to many lines using LF characters
///in order to keep the width constant. Note that the last line does not have LF to end it.
///
///This is the same as [`bytes_as_hex_block_iter`](fn.bytes_as_hex_block_iter.html)`(rope_once!(string), 0,`
///`lpfx, name)`.
pub fn bytes_as_hex_block(string: &[u8], lpfx: &str, name: &str) -> String
{
	crate::messages::dump_hexblock(string, lpfx, name)
}

///Output a sequence of fragments `rope` as hexdump block.
pub fn bytes_as_hex_block_iter3(rope: &dyn FragmentWriteDyn3, strip: usize, lpfx: &str, name: &str) -> String
{
	crate::messages::dump_hexblock3(rope, strip, lpfx, name)
}

///Get human-readable name of TLS handshake type `htype`.
///
///E.g., `htype=2` yields `server_hello`.
pub fn explain_hs_type(htype: u8) -> Cow<'static, str>
{
	let htype = HandshakeType::new(htype);
	match htype.get_name() {
		Some(x) => Cow::Borrowed(x),
		None => Cow::Owned(format!("{htype}"))
	}
}

pub(crate) fn ks_debug(debug: &DebugSetting, msg: TlsDebug)
{
	use TlsDebug::*;
	match msg {
		Tls13Binder{psk,binder_type,base_key,finished_key,handshake_hash,calculated_mac,received_mac} =>
			if let Some(received_mac) = received_mac {
			debug!(CRYPTO_CALCS debug, "Received {binder_type} PSK binder:\n{a}\n{b}\n{c}\n{d}\n{e}\n{f}",
				a=DumpHexblock::new(psk, "  ", "PSK"),
				b=DumpHexblock::new(base_key, "  ", "Base key"),
				c=DumpHexblock::new(finished_key, "  ", "Finished key"),
				d=DumpHexblock::new(handshake_hash, "  ", "Handshake hash"),
				e=DumpHexblock::new(calculated_mac, "  ", "Calculated MAC"),
				f=DumpHexblock::new(received_mac, "  ", "Received MAC"));
		} else {
			debug!(CRYPTO_CALCS debug, "Sending {binder_type} PSK binder':\n{a}\n{b}\n{c}\n{d}\n{e}",
				a=DumpHexblock::new(psk, "  ", "PSK"),
				b=DumpHexblock::new(base_key, "  ", "Base key"),
				c=DumpHexblock::new(finished_key, "  ", "Finished key"),
				d=DumpHexblock::new(handshake_hash, "  ", "Handshake hash"),
				e=DumpHexblock::new(calculated_mac, "  ", "Calculated MAC"));
		},
		Tls13Finished{base_key,finished_key,handshake_hash,calculated_mac,received_mac} =>
			if let Some(received_mac) = received_mac {
			debug!(CRYPTO_CALCS debug, "Received TLS 1.3 finished:\n{a}\n{b}\n{c}\n{d}\n{e}",
				a=DumpHexblock::new(base_key, "  ", "Base key"),
				b=DumpHexblock::new(finished_key, "  ", "Finished key"),
				c=DumpHexblock::new(handshake_hash, "  ", "Handshake hash"),
				d=DumpHexblock::new(calculated_mac, "  ", "Calculated MAC"),
				e=DumpHexblock::new(received_mac, "  ", "Received MAC"));
		} else {
			debug!(CRYPTO_CALCS debug, "Sending TLS 1.3 finished':\n{a}\n{b}\n{c}\n{d}",
				a=DumpHexblock::new(base_key, "  ", "Base key"),
				b=DumpHexblock::new(finished_key, "  ", "Finished key"),
				c=DumpHexblock::new(handshake_hash, "  ", "Handshake hash"),
				d=DumpHexblock::new(calculated_mac, "  ", "Calculated MAC"));
		},
		Tls13Exporter{exporter_master_secret,label,per_label_key,context,exported_value} =>
			debug!(CRYPTO_CALCS debug, "TLS 1.3 exporter '{label}':\n{a}\n{b}\n{c}\n{d}",
				a=DumpHexblock::new(exporter_master_secret, "  ", "Master secret"),
				b=DumpHexblock::new(per_label_key, "  ", "Per label key"),
				c=DumpHexblock::new(context, "  ", "Context"),
				d=DumpHexblock::new(exported_value, "  ", "Result")),
		Tls13MasterExporter{master_secret,handshake_hash,exporter_master_secret} =>
			debug!(CRYPTO_CALCS debug, "Calculating TLS 1.3 extractor master key:\n{a}\n{b}\n{c}",
				a=DumpHexblock::new(master_secret, "  ", "Master secret"),
				b=DumpHexblock::new(handshake_hash, "  ", "Handshake hash"),
				c=DumpHexblock::new(exporter_master_secret, "  ", "Master extractor key")),
		Tls13BumpTrafficSecret{is_tx,old,new} =>
			debug!(CRYPTO_CALCS debug, "Bumping {kind} traffic secret:\n{a}\n{b}",
				kind=if is_tx { "tx" } else { "rx" },
				a=DumpHexblock::new(old, "  ", "Old"),
				b=DumpHexblock::new(new, "  ", "New")),
		Tls13TrafficSecret{label,master_secret,handshake_hash,traffic_secret} =>
			debug!(CRYPTO_CALCS debug, "Calculating TLS 1.3 traffic secret '{label}':\n{a}\n{b}\n{c}",
				a=DumpHexblock::new(master_secret, "  ", "Master secret"),
				b=DumpHexblock::new(handshake_hash, "  ", "Handshake hash"),
				c=DumpHexblock::new(traffic_secret, "  ", "Traffic secret")),
		Tls13ResumptionPsk {resumption_master_secret,ticket_nonce,resumption_psk} =>
			debug!(CRYPTO_CALCS debug, "Calculating TLS 1.3 resumption PSK:\n{a}\n{b}\n{c}",
				a=DumpHexblock::new(resumption_master_secret, "  ", "Resumption master secret"),
				b=DumpHexblock::new(ticket_nonce, "  ", "Ticket nonce"),
				c=DumpHexblock::new(resumption_psk, "  ", "Resumption PSK")),
		Tls13PreMixExpansion {old,new} => debug!(CRYPTO_CALCS debug,
			"Pre-mix expansion:\n{oldkey}\n{keyout}", oldkey=DumpHexblock::new(old, "  ", "Old key"),
			keyout=DumpHexblock::new(new, "  ", "Key out")),
		Tls13KeyMix{key_name,old,input,new} => debug!(CRYPTO_CALCS debug,
			"Mixing TLS 1.3 key '{kname}':\n{oldkey}\n{keyin}\n{keyout}", kname=key_name,
			oldkey=DumpHexblock::new(old, "  ", "Old key"),
			keyin=DumpHexblock::new(input, "  ", "Key in"),
			keyout=DumpHexblock::new(new, "  ", "New key")),
		Tls13ResumptionMasterSecret{master_secret,handshake_hash,resumption_master_secret} =>
			debug!(CRYPTO_CALCS debug, "Calculating TLS 1.3 resumption master key:\n{a}\n{b}\n{c}",
				a=DumpHexblock::new(master_secret, "  ", "Master secret"),
				b=DumpHexblock::new(handshake_hash, "  ", "Handshake hash"),
				c=DumpHexblock::new(resumption_master_secret, "  ", "Master resumption key")),
		Tls12Finished{master_secret,handshake_hash,calculated_mac,received_mac} =>
			if let Some(received_mac) = received_mac {
			debug!(CRYPTO_CALCS debug, "Received TLS 1.2 finished:\n{a}\n{b}\n{c}\n{d}",
				a=DumpHexblock::new(master_secret, "  ", "Master secret"),
				b=DumpHexblock::new(handshake_hash, "  ", "Handshake hash"),
				c=DumpHexblock::new(calculated_mac, "  ", "Calculated MAC"),
				d=DumpHexblock::new(received_mac, "  ", "Received MAC"));
		} else {
			debug!(CRYPTO_CALCS debug, "Sending TLS 1.2 finished':\n{a}\n{b}\n{c}",
				a=DumpHexblock::new(master_secret, "  ", "Master secret"),
				b=DumpHexblock::new(handshake_hash, "  ", "Handshake hash"),
				c=DumpHexblock::new(calculated_mac, "  ", "Calculated MAC"));
		},
		Tls12PremasterSecret{premaster_secret,compressed_premaster_secret} =>
			debug!(CRYPTO_CALCS debug, "Setting TLS 1.2 Pre Master Secret:\n{a}\n{b}",
				a=DumpHexblock::new(premaster_secret, "  ", "Raw"),
				b=DumpHexblock::new(compressed_premaster_secret, "  ", "Compressed")),
		Tls12MasterSecret{extended,premaster_secret,handshake_hash,client_random,server_random,
			master_secret} => debug!(CRYPTO_CALCS debug,
			"Calculating TLS 1.2 Master Secret '{kind}master secret':\n{a}\n{b}\n{c}\n{d}\n{e}",
			kind=if extended { "extended " } else { "" },
			a=DumpHexblock::new(premaster_secret, "  ", "Premaster secret"),
			b=DumpHexblock::new(handshake_hash, "  ", "Hash"),
			c=DumpHexblock::new(client_random, "  ", "Client random"),
			d=DumpHexblock::new(server_random, "  ", "Server random"),
			e=DumpHexblock::new(master_secret, "  ", "Master secret")),
		Tls12Exporter{master_secret,label,context,client_random,server_random,exported_value} =>
			if let Some(context) = context {
			debug!(CRYPTO_CALCS debug, "TLS 1.2 exporter '{label}':\n{a}\n{b}\n{c}\n{d}\n{e}",
				a=DumpHexblock::new(master_secret, "  ", "Master secret"),
				b=DumpHexblock::new(context, "  ", "Context"),
				c=DumpHexblock::new(client_random, "  ", "Client random"),
				d=DumpHexblock::new(server_random, "  ", "Server random"),
				e=DumpHexblock::new(exported_value, "  ", "Result"));
		} else {
			debug!(CRYPTO_CALCS debug, "TLS 1.2 exporter '{label}':\n{a}\n{b}\n{c}\n{d}",
				a=DumpHexblock::new(master_secret, "  ", "Master secret"),
				b=DumpHexblock::new(client_random, "  ", "Client random"),
				c=DumpHexblock::new(server_random, "  ", "Server random"),
				d=DumpHexblock::new(exported_value, "  ", "Result"));
		},
		KeyingEncryption{protector,key} => debug!(CRYPTO_CALCS debug,
			"Keying {protector:?} for encryption with:\n{key}", key=DumpHexblock::new(key, "  ", "Key")),
		KeyingDecryption{protector,key} => debug!(CRYPTO_CALCS debug,
			"Keying {protector:?} for decryption with:\n{key}", key=DumpHexblock::new(key, "  ", "Key")),
		TlsServerSignature{public_key,server_tbs,algorithm,signature} => debug!(CRYPTO_CALCS debug,
			"Server signature (algorithm={algorithm:?}):\n{pubkey}\n{tbs}\n{sig}",
			pubkey=DumpHexblock::new(public_key, "  ", "Public key"),
			tbs=DumpHexblock::new(server_tbs, "  ", "TBS"),
			sig=DumpHexblock::new(signature, "  ", "Server signature")),
		FinishedOk{server} => debug!(HANDSHAKE_EVENTS debug, "Finished: {kind} MAC check ok",
			kind=if server { "server" } else { "client" }),
		_ => ()		//Ignore unknown.
	}
}
