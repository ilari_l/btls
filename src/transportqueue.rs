use crate::stdlib::IoError;
use crate::stdlib::IoErrorKind;
use crate::stdlib::IoRead;
use crate::stdlib::IoWrite;
use crate::stdlib::min;
use crate::stdlib::Vec;
use crate::transport::IncomingCiphertext;
use crate::transport::IncomingPlaintext;
use crate::transport::PlaintextBlock;
use crate::transport::PlaintextBlockIterator;
use crate::transport::OutgoingCiphertext;
use crate::transport::OutgoingPlaintext;

///The underlying raw queue.
struct RawQueue
{
	underlying: Vec<u8>,
	eos_flag: bool,
	need_records: bool,		//If True, keep records in one piece.
	unflushed: usize,
}

impl RawQueue
{
	fn new(need_records: bool) -> RawQueue
	{
		RawQueue{
			underlying: Vec::new(),
			eos_flag: false,
			need_records: need_records,
			unflushed: 0,
		}
	}
	fn _borrow_buf_for_incoming(&mut self) -> &mut [u8]
	{
		//This needs to ensure there is always some space to read data to.
		//FIXME: Borrow a buffer.
		unimplemented!();
	}
	fn _accept_into_queue(&mut self, amt: usize)
	{
		//FIXME: Accept the data into queue.
		unimplemented!();
	}
	fn _borrow_buf_for_outgoing(&self) -> &[u8]
	{
		//FIXME: Borrow a buffer.
		unimplemented!();
	}
	fn _discard_from_queue(&mut self, amt: usize)
	{
		//FIXME: Discard the data from queue.
		unimplemented!();
	}
	fn call_read<R:IoRead>(&mut self, stream: &mut R) -> Result<usize, IoError>
	{
		//FIXME: Handle the whole-records mode.
		let amt = {
			let buf = self._borrow_buf_for_incoming();
			stream.read(buf)?
		};
		if amt == 0 {
			self.queue_eos();
			return Ok(0);
		}
		self._accept_into_queue(amt);
		Ok(amt)
	}
	fn call_write<W:IoWrite>(&mut self, stream: &mut W) -> Result<usize, IoError>
	{
		let amt = {
			let buf = self._borrow_buf_for_outgoing();
			stream.write(buf)?
		};
		self._discard_from_queue(amt);
		Ok(amt)
	}
	fn len(&self) -> usize
	{
		//FIXME: Get amount of data in queue.
		unimplemented!();
	}
	fn is_end_of_stream(&self) -> bool
	{
		self.eos_flag && self.len() > 0
	}
	fn do_read(&mut self, frag: &mut [u8]) -> Result<usize, IoError>
	{
		let mut ptr = 0;
		//Do multiple reads until nothing more can be read.
		while self.len() > 0 && ptr < frag.len() {
			let amt = {
				let buf = self._borrow_buf_for_outgoing();
				let amt = min(buf.len(), frag.len() - ptr);
				(&mut frag[ptr..][..amt]).copy_from_slice(&buf[..amt]);
				amt
			};
			self._discard_from_queue(amt);
			ptr += amt;
		}
		if ptr == 0 {
			//If not end-of-stream, return temporary error.
			return if self.is_end_of_stream() { Ok(0) } else { Err(IoError::new(
				IoErrorKind::WouldBlock, "Queue empty but not in end-of-stream")) };
		}
		Ok(ptr)
	}
	fn do_write(&mut self, frag: &[u8]) -> Result<usize, IoError>
	{
		self.do_write_atomic(frag);
		Ok(frag.len())
	}
	fn do_flush(&mut self) -> Result<(), IoError>
	{
		self.unflushed = self.len();
		Ok(())
	}
	//Plaintext data from the decryption.
	fn do_write_atomic(&mut self, frag: &[u8])
	{
		let mut ptr = 0;
		//Do multiple writes until nothing more can be written.
		//FIXME: Handle the whole-records mode. Altough do_write_atomic() can not be directly called for
		//encrypted input queue, do_write() can, which calls this.
		while ptr < frag.len() {
			let amt = {
				let buf = self._borrow_buf_for_incoming();
				let amt = min(buf.len(), frag.len() - ptr);
				(&mut buf[..amt]).copy_from_slice(&frag[ptr..][..amt]);
				amt
			};
			self._accept_into_queue(amt);
			ptr += amt;
		}
	}
	fn queue_eos(&mut self) { self.eos_flag = true; }
	//Encrypted data from the wire.
	fn do_peek_record<'a>(&'a mut self) -> &'a mut [u8]
	{
		//We need to upcast _borrow_buf_for_outgoing() return to mutable slice.
		let (offset, amount) = {
			let baseptr = self.underlying.as_ptr() as usize;
			let buf = self._borrow_buf_for_outgoing();
			(buf.as_ptr() as usize - baseptr, buf.len())
		};
		//These indices are in-range since this is nothing else than buf above.
		&mut self.underlying[offset..][..amount]
	}
	fn do_read_record<'a>(&'a mut self, n: usize) -> &'a [u8]
	{
		let (offset, amount) = {
			let baseptr = self.underlying.as_ptr() as usize;
			let buf = self._borrow_buf_for_outgoing();
			(buf.as_ptr() as usize - baseptr, min(buf.len(), n))
		};
		self._discard_from_queue(amount);
		//This is in-bounds since amount <= buf.len(), which is subbuffer starting from offset.
		&self.underlying[offset..][..amount]
	}
	//Unencrypted data towards encryption.
	fn do_force_flush(&self) -> bool
	{
		//If there is unflushed data, or if at end of stream, force a flush.
		self.unflushed > 0 || self.is_end_of_stream()
	}
	fn do_borrow_plaintext<'a>(&'a self) -> PlaintextBlock<'a>
	{
		if self.is_end_of_stream() { return PlaintextBlock::End; }
		let buf = self._borrow_buf_for_outgoing();
		let buf = &buf[..min(buf.len(), self.unflushed)];	//Do not borrow past flushed part.
		PlaintextBlock::Chunk(buf)
	}
	fn do_acknowledge_plaintext(&mut self, acked_size: usize)
	{
		self._discard_from_queue(acked_size);
	}
	//Encrypted data towards wire.
	fn do_borrow_ciphertext_len(&self, maxsize: usize) -> usize { maxsize }
	fn do_borrow_ciphertext<'a>(&'a mut self, maxsize: usize) -> &'a mut [u8]
	{
		//Here we do not need to handle whole records mode, because do_borrow_ciphertext() can never be
		//called for encrypted input queue.
		let buf = self._borrow_buf_for_incoming();
		//Limit the size to specified maximum if > 0.
		let blen = buf.len();
		&mut buf[..min(blen, if maxsize > 0 { maxsize } else { blen })]
	}
	fn do_acknowledge_ciphertext(&mut self, acked_size: usize) { self._accept_into_queue(acked_size) }
}

///Unencrypted input queue.
pub struct UnencryptedInputQueue(RawQueue);
///Encrypted input queue.
pub struct EncryptedInputQueue(RawQueue);
///Unencrypted output queue.
pub struct UnencryptedOutputQueue(RawQueue);
///Encrypted output queue.
pub struct EncryptedOutputQueue(RawQueue, usize);

impl<'a> IncomingPlaintext<'a> for UnencryptedInputQueue
{
	type Return = ();
	fn received_plaintext(&mut self, blocks: PlaintextBlockIterator<'a>)
	{
		for i in blocks {
			match i {
				PlaintextBlock::Chunk(x) => self.handle_fragment(x),
				PlaintextBlock::End => self.handle_end_of_stream(),
			}
		}
	}
}


impl IoRead for UnencryptedInputQueue
{
	fn read(&mut self, frag: &mut [u8]) -> Result<usize, IoError> { self.0.do_read(frag) }
}


impl IncomingCiphertext for EncryptedInputQueue
{
	type Return = ();
	fn peek_record<'a>(&'a mut self) -> &'a mut [u8] { self.0.do_peek_record() }
	fn read_record<'a>(&'a mut self, n: usize) -> (&'a [u8], ()) { (self.0.do_read_record(n), ()) }
	fn empty_record(&mut self)
	{
		//Do nothing.
	}
}

impl IoWrite for EncryptedInputQueue
{
	fn write(&mut self, frag: &[u8]) -> Result<usize, IoError> { self.0.do_write(frag) }
	fn flush(&mut self) -> Result<(), IoError> { self.0.do_flush() }
}

impl OutgoingPlaintext for UnencryptedOutputQueue
{
	type Return = ();
	fn force_flush(&self) -> bool { self.0.do_force_flush() }
	fn borrow_plaintext<'a>(&'a self) -> PlaintextBlock<'a> { self.0.do_borrow_plaintext() }
	fn acknowledge_plaintext(&mut self, acked_size: usize) { self.0.do_acknowledge_plaintext(acked_size) }
}

impl IoWrite for UnencryptedOutputQueue
{
	fn write(&mut self, frag: &[u8]) -> Result<usize, IoError> { self.0.do_write(frag) }
	fn flush(&mut self) -> Result<(), IoError> { self.0.do_flush() }
}

impl OutgoingCiphertext for EncryptedOutputQueue
{
	type Return = ();
	fn borrow_ciphertext_len(&self) -> usize { self.0.do_borrow_ciphertext_len(self.1) }
	fn borrow_ciphertext<'a>(&'a mut self) -> &'a mut [u8] { self.0.do_borrow_ciphertext(self.1) }
	fn acknowledge_ciphertext(&mut self, acked_size: usize) { self.0.do_acknowledge_ciphertext(acked_size) }
}

impl IoRead for EncryptedOutputQueue
{
	fn read(&mut self, frag: &mut [u8]) -> Result<usize, IoError> { self.0.do_read(frag) }
}


impl UnencryptedInputQueue
{
	///Create a new Unencrypted input queue.
	pub fn new() -> UnencryptedInputQueue { UnencryptedInputQueue(RawQueue::new(false)) }
	///Call `std::io::Write::write()` on data on Unencrypted input queue.
	pub fn call_write<W:IoWrite>(&mut self, stream: &mut W) -> Result<usize, IoError>
	{
		self.0.call_write(stream)
	}
	///Has reached End-of-Stream?
	pub fn is_end_of_stream(&self) -> bool { self.0.is_end_of_stream() }
	///Get amount of pending data in queue.
	pub fn len(&self) -> usize { self.0.len() }
	fn handle_fragment(&mut self, frag: &[u8]) { self.0.do_write_atomic(frag) }
	fn handle_end_of_stream(&mut self) { self.0.queue_eos(); }
}

impl EncryptedInputQueue
{
	///Create a new Eencrypted input queue.
	pub fn new() -> EncryptedInputQueue
	{
		//This differs from others in that records must be kept in one piece.
		EncryptedInputQueue(RawQueue::new(true))
	}
	///Call `std::io::Read::read()` on data on Encrypted input queue.
	pub fn call_read<R:IoRead>(&mut self, stream: &mut R) -> Result<usize, IoError> { self.0.call_read(stream) }
	///Get amount of pending data in queue.
	pub fn len(&self) -> usize { self.0.len() }
}

impl UnencryptedOutputQueue
{
	///Create a new Unencrypted output queue.
	pub fn new() -> UnencryptedOutputQueue { UnencryptedOutputQueue(RawQueue::new(false)) }
	///Call `std::io::Read::read()` on data on Unencrypted output queue.
	pub fn call_read<R:IoRead>(&mut self, stream: &mut R) -> Result<usize, IoError> { self.0.call_read(stream) }
	///Send End-of-Stream.
	pub fn send_end_of_stream(&mut self) { self.0.queue_eos() }
	///Get amount of pending data in queue.
	pub fn len(&self) -> usize { self.0.len() }
}

impl EncryptedOutputQueue
{
	///Create a new Encrypted output queue.
	pub fn new() -> EncryptedOutputQueue { EncryptedOutputQueue(RawQueue::new(false), 0) }
	///Call `std::io::Write::write()` on data on Encrypted output queue.
	pub fn call_write<W:IoWrite>(&mut self, stream: &mut W) -> Result<usize, IoError>
	{
		self.0.call_write(stream)
	}
	///Get amount of pending data in queue.
	pub fn len(&self) -> usize { self.0.len() }
	///Adjust the maximum write size. 0 means unlimited.
	pub fn set_max_write_size(&mut self, maxsize: usize) { self.1 = maxsize; }
}
