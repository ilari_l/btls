use crate::stdlib::Deref;
use crate::stdlib::Display;
use crate::stdlib::Error as ErrorTrait;
use crate::stdlib::FmtError;
use crate::stdlib::Formatter;
use crate::stdlib::String;
use crate::stdlib::Vec;
use crate::utils::HsBufferError;
use btls_aux_assert::AssertFailed;
use btls_aux_certificate_transparency::SctError;
use btls_aux_ocsp::OcspValidationError;
use btls_aux_rope3::VectorFieldError;
use btls_aux_signatures::SignatureError2;
use btls_aux_time::DecodeTsForUser2;
use btls_aux_time::Timestamp;
use btls_aux_tls_iana::Alert;
use btls_aux_tls_iana::CipherSuite;
use btls_aux_tls_iana::ContentType;
use btls_aux_tls_iana::HandshakeType;
use btls_aux_tls_iana::NamedGroup;
use btls_aux_tls_iana::TlsVersion;
use btls_aux_tls_struct::CertificateParseError;
use btls_aux_tls_struct::CertificateRequestParseError;
use btls_aux_tls_struct::CertificateStatusParseError;
use btls_aux_tls_struct::CertificateVerifyParseError;
use btls_aux_tls_struct::ClientHelloParseError;
use btls_aux_tls_struct::ClientKeyExchangeParseError;
use btls_aux_tls_struct::EncryptedExtensionsParseError;
use btls_aux_tls_struct::KeyUpdateParseError;
use btls_aux_tls_struct::ServerHelloParseError;
use btls_aux_tls_struct::ServerKeyExchangeParseError;
use btls_aux_tlskeyschedule::DiffieHellmanError;
use btls_aux_tlskeyschedule::ExtractorError;
use btls_aux_tlskeyschedule::Tls12KeyScheduleError;
use btls_aux_tlskeyschedule::Tls13KeyScheduleError;
use btls_aux_x509certparse::CertificateError;
use btls_aux_x509certvalidation::CertificateValidationError;
use btls_aux_x509certvalidation::MajorCertProblem;


///Error in TLS library.
///
///The error type is opaque, and mostly can just be formatted using the `{}` operator (trait `Display`).
#[derive(Clone,Debug)]
pub struct Error(_Error);

impl Display for Error
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError> { Display::fmt(&self.0, fmt) }
}

impl From<AssertFailed> for Error
{
	fn from(x: AssertFailed) -> Error { Error(_Error::AssertionFailed(x)) }
}

impl ErrorTrait for Error
{
	fn description(&self) -> &str { self.0.shortdesc() }
	fn cause(&self) -> Option<&dyn ErrorTrait>
	{
		use self::_Error::*;
		match &self.0 {
			&AssertionFailed(ref x) => Some(x),
			_ => None
		}
	}
}

impl Error
{
	pub(crate) fn is_promoted_error(&self) -> bool
	{
		use self::_Error::*;
		//AssertionFailed is promoted because it is bad.
		//ReceivedAlert is promoted, because alert immediately tears down the connection.
		//RestartingHandshake is promoted in order to tear down the connection for stateless rejects.
		matches!(&self.0, &AssertionFailed(_)|&ReceivedAlert(_)|RestartingHandshake(_))
	}
	#[doc(hidden)]
	pub fn tls_alert_num(&self) -> u8
	{
		self.tls_alert_num2().get()
	}
	#[doc(hidden)]
	pub fn tls_alert_num2(&self) -> Alert
	{
		use self::_Error::*;
		match &self.0 {
			&AssertionFailed(_) => Alert::INTERNAL_ERROR,

			//Yes, this has close_notify as alert.
			&AcmeValidationDone => Alert::CLOSE_NOTIFY,
			&AdvNoCipherSuites => Alert::INTERNAL_ERROR,
			&AdvNoGroups => Alert::INTERNAL_ERROR,
			&AdvNoSignatures => Alert::INTERNAL_ERROR,
			&AlpnInternalError => Alert::INTERNAL_ERROR,
			&AlpnNoOverlap => Alert::HANDSHAKE_FAILURE,
			&AppdataAfterEof => Alert::UNEXPECTED_MESSAGE,
			&AppdataBeforeFinished => Alert::UNEXPECTED_MESSAGE,
			&BadCookie => Alert::HANDSHAKE_FAILURE,
			&BinderMacFailed => Alert::DECRYPT_ERROR,
			&BogusAcmeAlpn => Alert::BAD_CERTIFICATE,
			&BogusAcmeChainLen => Alert::BAD_CERTIFICATE,
			&BogusAcmeNames => Alert::BAD_CERTIFICATE,
			&BogusAcmeSerial => Alert::BAD_CERTIFICATE,
			&BogusAcmeSignature => Alert::BAD_CERTIFICATE,
			&BogusAcmeToken => Alert::BAD_CERTIFICATE,
			&CcsInMiddleOfHandshakeMessage => Alert::UNEXPECTED_MESSAGE,
			&CertRequiresOcsp => Alert::BAD_CERTIFICATE_STATUS_RESPONSE,
			&CertRequiresSct => Alert::BAD_CERTIFICATE_STATUS_RESPONSE,
			&ChDoubleRestart => Alert::HANDSHAKE_FAILURE,
			&ClientAuthenticationFailed => Alert::ACCESS_DENIED,
			&ClientDoesNotSupportUncompressed => Alert::ILLEGAL_PARAMETER,
			&ClientDoesNotSupportUncompressedEc => Alert::HANDSHAKE_FAILURE,
			&CrContextMustBeEmpty => Alert::DECODE_ERROR,
			&DeprotectionFailed => Alert::BAD_RECORD_MAC,
			&DowngradeAttackDetected => Alert::INAPPROPRIATE_FALLBACK,
			&DynamicRsaNotAllowed => Alert::BAD_CERTIFICATE,
			&EeCertificateEmpty => Alert::DECODE_ERROR,
			&EmptyHandshakeMessage => Alert::HANDSHAKE_FAILURE,
			&FinishedMacFailed => Alert::DECRYPT_ERROR,
			&FinishedNotAligned => Alert::UNEXPECTED_MESSAGE,
			&HandshakeTruncation => Alert::DECRYPT_ERROR,	//Not actually used.
			&InsecureTruncation => Alert::DECRYPT_ERROR,	//Not actually used.
			&InterlockSignatureAlgorithm => Alert::INTERNAL_ERROR,
			&InvalidCcs => Alert::UNEXPECTED_MESSAGE,
			&KexSignatureFailed => Alert::INTERNAL_ERROR,
			&KeyUpdateNotAligned => Alert::UNEXPECTED_MESSAGE,
			&KeyVulernableToDrowning => Alert::INSUFFICIENT_SECURITY,
			&NoAcmeValidationTarget => Alert::MISSING_EXTENSION,
			&NoMutualCertificateType => Alert::INSUFFICIENT_SECURITY,
			&NoMutualCiphersuites => Alert::INSUFFICIENT_SECURITY,
			&NoMutualGroups => Alert::INSUFFICIENT_SECURITY,
			&NoMutualSignatures => Alert::INSUFFICIENT_SECURITY,
			&NoMutualVersions => Alert::PROTOCOL_VERSION,
			&NoSniInAcmeValidation => Alert::MISSING_EXTENSION,
			&NoSupportedGroups => Alert::MISSING_EXTENSION,
			&RenegotiationAttackDetected => Alert::HANDSHAKE_FAILURE,
			&Secure192RequiredNotOffered => Alert::INSUFFICIENT_SECURITY,
			&ServerChainEmpty => Alert::NO_CERTIFICATE,
			&ServerHelloNotAlignedTls13 => Alert::UNEXPECTED_MESSAGE,
			&ServerKeyNotWhitelisted => Alert::UNKNOWN_CA,
			&Ssl3NotSupported => Alert::PROTOCOL_VERSION,
			&TestDone => Alert::CLOSE_NOTIFY,
			&TestFailure => Alert::INTERNAL_ERROR,
			&Tls10NotSupported => Alert::PROTOCOL_VERSION,
			&Tls11NotSupported => Alert::PROTOCOL_VERSION,
			&Tls12NotSupported => Alert::PROTOCOL_VERSION,
			&Tls13NoKeyShare => Alert::MISSING_EXTENSION,
			&Tls13RequiredNotOffered => Alert::INSUFFICIENT_SECURITY,
			&VersionMismatchHrrSh => Alert::ILLEGAL_PARAMETER,
			&VulernableRenego => Alert::INSUFFICIENT_SECURITY,
			&VulernableTHS => Alert::INSUFFICIENT_SECURITY,
			&ClientSupportsBadCiphers => Alert::INSUFFICIENT_SECURITY,
			&AcmeReservedName(_) => Alert::UNRECOGNIZED_NAME,
			&BadEntitySpki(_) => Alert::BAD_CERTIFICATE,
			&BogusHrrTlsVersion(_) => Alert::ILLEGAL_PARAMETER,
			&BogusShVersion(_) => Alert::ILLEGAL_PARAMETER,
			&CantExtractSpki(_) => Alert::UNSUPPORTED_CERTIFICATE,
			&CantExtractSctOcsp(_) => Alert::BAD_CERTIFICATE,
			&CantParseIssuerCertificate(_) => Alert::BAD_CERTIFICATE_STATUS_RESPONSE,
			&CertificateValidationFailed(ref x) => match x.problem_class() {
				MajorCertProblem::Bad => Alert::BAD_CERTIFICATE,
				MajorCertProblem::Unsupported => Alert::UNSUPPORTED_CERTIFICATE,
				MajorCertProblem::Expired => Alert::CERTIFICATE_EXPIRED,
				MajorCertProblem::Ocsp => Alert::BAD_CERTIFICATE_STATUS_RESPONSE,
				MajorCertProblem::Unspecified => Alert::CERTIFICATE_UNKNOWN,
				MajorCertProblem::UnknownCa => Alert::UNKNOWN_CA,
				MajorCertProblem::InternalError => Alert::INTERNAL_ERROR,
			},
			&CvSignatureFailed(_) => Alert::BAD_CERTIFICATE,
			&EECertParseError(_) => Alert::UNSUPPORTED_CERTIFICATE,
			&HrrAlreadyHadShare(_) => Alert::ILLEGAL_PARAMETER,
			&HsMsgBuffer(ref x) => x.tls_code(),
			&InterlockCertificateExpired(_) => Alert::CERTIFICATE_EXPIRED,
			&InterlockCertificateName(_) => Alert::BAD_CERTIFICATE,
			&InterlockOcspExpired(_) => Alert::CERTIFICATE_EXPIRED,
			&InvalidOcsp(ref x) if x.is_revoked() => Alert::CERTIFICATE_REVOKED,
			&InvalidOcsp(_) => Alert::BAD_CERTIFICATE_STATUS_RESPONSE,
			&InvalidRdnsName(_) => Alert::ILLEGAL_PARAMETER,
			&MsgerrCertificateRequest(x) => x.alert(),
			&MsgerrCertificateStatus(x) => x.alert(),
			&MsgerrCertificateVerify(x) => x.alert(),
			&MsgerrClientCertificate(x) => x.alert(),
			&MsgerrClientHello(x) => x.alert(),
			&MsgerrClientKeyExchange(x) => x.alert(),
			&MsgerrEncryptedExtensions(x) => x.alert(),
			&MsgerrKeyUpdate(x) => x.alert(),
			&MsgerrServerCertificate(x) => x.alert(),
			&MsgerrServerHello(x) => x.alert(),
			&MsgerrServerKeyExchange(x) => x.alert(),
			&NoCertificateAvailable(_) => Alert::UNRECOGNIZED_NAME,
			&NoMatchingCertificate(_) => Alert::NO_CERTIFICATE,
			&RaiseAlert(x) => x,			//Special having variable alert number.
			&ReceivedAlert(x) => x,			//Special having variable alert number.
			//Not actually relevant, as this is handled specially.
			&RestartingHandshake(_) => Alert::INTERNAL_ERROR,
			&RxRecordTooBig(_) => Alert::RECORD_OVERFLOW,
			&SctValidationFailed(_) => Alert::BAD_CERTIFICATE_STATUS_RESPONSE,
			&ServerHelloDoneNotEmpty(_) => Alert::UNEXPECTED_MESSAGE,
			&ShBadTls12Cipher(_) => Alert::ILLEGAL_PARAMETER,
			&ShBadTls13Cipher(_) => Alert::ILLEGAL_PARAMETER,
			&ShUnsupportedTlsVersion(_) => Alert::PROTOCOL_VERSION,
			&SkeSignatureFailed(_) => Alert::BAD_CERTIFICATE,
			&UnexpectedCcs(_) => Alert::UNEXPECTED_MESSAGE,
			&UnexpectedRtype(_) => Alert::UNEXPECTED_MESSAGE,
			&UnknownAcmeValidationTarget(_) => Alert::UNRECOGNIZED_NAME,

			&CiphersuiteDisagreement(_,_) => Alert::ILLEGAL_PARAMETER,
			&DiffieHellmanAgreeError(_) => Alert::ILLEGAL_PARAMETER,
			&KeyShareGroupDisagreement(_, _) => Alert::ILLEGAL_PARAMETER,
			&OcspValidityTooLong(_, _) => Alert::BAD_CERTIFICATE_STATUS_RESPONSE,
			&SerializeFailed(_, _) => Alert::INTERNAL_ERROR,
			&UnexpectedHandshakeMessage(_, _) => Alert::UNEXPECTED_MESSAGE,
		}
	}
	#[doc(hidden)]
	pub fn new(err: _Error) -> Error { Error(err) }
	///Is handshake restart error?
	///
	///If this is the handshake restart error, returns Some(msg), where msg is the HRR message to send.
	///Otherwise returns None.
	///
	///This error can only arise if using stateless mode.
	pub fn is_restart<'a>(&'a self) -> Option<&'a [u8]>
	{
		if let &_Error::RestartingHandshake(ref x) = &self.0 { Some(x.deref()) } else { None }
	}
	///Is severe error?
	pub fn is_severe(&self) -> bool
	{
		//Currently the following are severe:
		// - AssertFailed 
		// - KexSignatureFailed
		// - Interlock*.
		use self::_Error::*;
		matches!(&self.0, &AssertionFailed(_)|&KexSignatureFailed|&InterlockCertificateExpired(_)|
			&InterlockCertificateName(_)|&InterlockOcspExpired(_)|&InterlockSignatureAlgorithm)
	}
}

macro_rules! def_err
{
	($id:tt) => { crate::errors::Error::new(crate::errors::_Error::$id) };
	($id:tt $x:expr) => { crate::errors::Error::new(crate::errors::_Error::$id($x)) };
	($id:tt $x:expr, $y:expr) => { crate::errors::Error::new(crate::errors::_Error::$id($x, $y)) };
}

macro_rules! error
{
	(acme_reserved_name $x:expr) => {def_err!(AcmeReservedName $x)};
	(acme_validation_done) => { def_err!(AcmeValidationDone) };
	(adv_no_cipher_suites) => { def_err!(AdvNoCipherSuites) };
	(adv_no_groups) => { def_err!(AdvNoGroups) };
	(adv_no_signatures) => { def_err!(AdvNoSignatures) };
	(alpn_internal_error) => { def_err!(AlpnInternalError) };
	(alpn_no_overlap) => { def_err!(AlpnNoOverlap) };
	(appdata_after_eof) => { def_err!(AppdataAfterEof) };
	(appdata_before_finished) => { def_err!(AppdataBeforeFinished) };
	(bad_cookie) => { def_err!(BadCookie) };
	(bad_entity_spki $x:expr) => { def_err!(BadEntitySpki $x) };
	(binder_mac_failed) => { def_err!(BinderMacFailed) };
	(bogus_acme_alpn) => { def_err!(BogusAcmeAlpn) };
	(bogus_acme_chain_len) => { def_err!(BogusAcmeChainLen) };
	(bogus_acme_names) => { def_err!(BogusAcmeNames) };
	(bogus_acme_serial) => { def_err!(BogusAcmeSerial) };
	(bogus_acme_self_signature) => { def_err!(BogusAcmeSignature) };
	(bogus_acme_token) => { def_err!(BogusAcmeToken) };
	(bogus_hrr_tls_version $x:expr) => { def_err!(BogusHrrTlsVersion $x) };
	(bogus_sh_version $x:expr) => { def_err!(BogusShVersion $x) };
	(cant_extract_sct_ocsp $x:expr) => { def_err!(CantExtractSctOcsp $x) };
	(cant_extract_spki $x:expr) => { def_err!(CantExtractSpki $x) };
	(cant_parse_issuer_certificate $x:expr) => { def_err!(CantParseIssuerCertificate $x) };
	(ccs_in_middle_of_handshake_message) => { def_err!(CcsInMiddleOfHandshakeMessage) };
	(cert_requires_ocsp) => { def_err!(CertRequiresOcsp) };
	(cert_requires_sct) => { def_err!(CertRequiresSct) };
	(certificate_validation_failed $x:expr) => {def_err!(CertificateValidationFailed $x)};
	(ch_double_restart) => { def_err!(ChDoubleRestart) };
	(ciphersuite_disagreement $x:expr, $y:expr) => { def_err!(CiphersuiteDisagreement $x, $y) };
	(client_authentication_failed) => { def_err!(ClientAuthenticationFailed) };
	(client_does_not_support_uncompressed) => { def_err!(ClientDoesNotSupportUncompressed) };
	(client_does_not_support_uncompressed_ec) => { def_err!(ClientDoesNotSupportUncompressedEc) };
	(client_supports_bad_ciphers) => { def_err!(ClientSupportsBadCiphers) };
	(cr_context_must_be_empty) => { def_err!(CrContextMustBeEmpty) };
	(cv_signature_failed $x:expr) => { def_err!(CvSignatureFailed $x) };
	(deprotection_failed) => { def_err!(DeprotectionFailed) };
	(diffie_hellman_agree_error $x:expr) => { def_err!(DiffieHellmanAgreeError $x) };
	(downgrade_attack_detected) => { def_err!(DowngradeAttackDetected) };
	(dynamic_rsa_not_allowed) => { def_err!(DynamicRsaNotAllowed) };
	(ee_cert_parse_error $x:expr) => { def_err!(EECertParseError $x) };
	(ee_certificate_empty) => {def_err!(EeCertificateEmpty)};
	(empty_handshake_message) => { def_err!(EmptyHandshakeMessage) };
	(finished_mac_failed) => { def_err!(FinishedMacFailed) };
	(finished_not_aligned) => { def_err!(FinishedNotAligned) };
	(handshake_truncation) => { def_err!(HandshakeTruncation) };
	(hrr_already_had_share $x:expr) => { def_err!(HrrAlreadyHadShare $x) };
	(hs_msg_buffer $x:expr) => { def_err!(HsMsgBuffer $x) };
	(insecure_truncation) => { def_err!(InsecureTruncation) };
	(interlock_certificate_expired $x:expr) => { def_err!(InterlockCertificateExpired $x) };
	(interlock_certificate_name $x:expr) => { def_err!(InterlockCertificateName $x) };
	(interlock_ocsp_expired $x:expr) => { def_err!(InterlockOcspExpired $x) };
	(interlock_signature_algorithm) => { def_err!(InterlockSignatureAlgorithm) };
	(invalid_ccs) => { def_err!(InvalidCcs) };
	(invalid_rdns_name $x:expr) => { def_err!(InvalidRdnsName $x) };
	(invalid_ocsp $x:expr) => { def_err!(InvalidOcsp $x) };
	(kex_signature_failed) => { def_err!(KexSignatureFailed) };
	(key_share_group_disagreement $x:expr, $y:expr) => { def_err!(KeyShareGroupDisagreement $x, $y) };
	(key_update_not_aligned) => { def_err!(KeyUpdateNotAligned) };
	(key_vulernable_to_drowning) => {def_err!(KeyVulernableToDrowning)};
	(msgerr_certificate_request $x:expr) => { def_err!(MsgerrCertificateRequest $x) };
	(msgerr_certificate_status $x:expr) => { def_err!(MsgerrCertificateStatus $x) };
	(msgerr_certificate_verify $x:expr) => { def_err!(MsgerrCertificateVerify $x) };
	(msgerr_client_certificate $x:expr) => { def_err!(MsgerrClientCertificate $x) };
	(msgerr_client_hello $x:expr) => { def_err!(MsgerrClientHello $x) };
	(msgerr_client_key_exchange $x:expr) => { def_err!(MsgerrClientKeyExchange $x) };
	(msgerr_encrypted_extensions $x:expr) => { def_err!(MsgerrEncryptedExtensions $x) };
	(msgerr_key_update $x:expr) => { def_err!(MsgerrKeyUpdate $x) };
	(msgerr_server_certificate $x:expr) => { def_err!(MsgerrServerCertificate $x) };
	(msgerr_server_hello $x:expr) => { def_err!(MsgerrServerHello $x) };
	(msgerr_server_key_exchange $x:expr) => { def_err!(MsgerrServerKeyExchange $x) };
	(no_acme_validation_target) => { def_err!(NoAcmeValidationTarget) };
	(no_certificate_available $x:expr) => { def_err!(NoCertificateAvailable $x) };
	(no_matching_certificate $x:expr) => { def_err!(NoMatchingCertificate $x) };
	(no_sni_in_acme_validation) => { def_err!(NoSniInAcmeValidation) };
	(no_mutual_certificate_type) => { def_err!(NoMutualCertificateType) };
	(no_mutual_ciphersuites) => { def_err!(NoMutualCiphersuites) };
	(no_mutual_groups) => { def_err!(NoMutualGroups) };
	(no_mutual_signatures) => { def_err!(NoMutualSignatures) };
	(no_mutual_versions) => { def_err!(NoMutualVersions) };
	(no_supported_groups) => { def_err!(NoSupportedGroups) };
	(ocsp_validity_too_long $x:expr, $y:expr) => { def_err!(OcspValidityTooLong $x, $y) };
	(raise_alert $x:expr) => { def_err!(RaiseAlert $x) };
	(received_alert $x:expr) => { def_err!(ReceivedAlert $x) };
	(renegotiation_attack_detected) => { def_err!(RenegotiationAttackDetected) };
	(restarting_handshake $x:expr) => { def_err!(RestartingHandshake $x) };
	(rx_record_too_big $x:expr) => { def_err!(RxRecordTooBig $x) };
	(sct_validation_failed $x:expr) => { def_err!(SctValidationFailed $x) };
	(secure192_required_not_offered) =>{ def_err!(Secure192RequiredNotOffered) };
	(serialize_failed $x:expr, $y:expr) => { def_err!(SerializeFailed $x, $y) };
	(server_chain_empty) => { def_err!(ServerChainEmpty) };
	(server_hello_done_not_empty $x:expr) => { def_err!(ServerHelloDoneNotEmpty $x) };
	(server_hello_not_aligned_tls13) => { def_err!(ServerHelloNotAlignedTls13) };
	(server_key_not_whitelisted) => {def_err!(ServerKeyNotWhitelisted)};
	(sh_bad_tls12_cipher $x:expr) => { def_err!(ShBadTls12Cipher $x) };
	(sh_bad_tls13_cipher $x:expr) => { def_err!(ShBadTls13Cipher $x) };
	(sh_unsupported_tls_version $x:expr) => { def_err!(ShUnsupportedTlsVersion $x) };
	(ske_signature_failed $x:expr) => { def_err!(SkeSignatureFailed $x) };
	(ssl3_not_supported) => { def_err!(Ssl3NotSupported) };
	(test_done) => { def_err!(TestDone) };
	(test_failure) => { def_err!(TestFailure) };
	(tls10_not_supported) => { def_err!(Tls10NotSupported) };
	(tls11_not_supported) => { def_err!(Tls11NotSupported) };
	(tls12_not_supported) => { def_err!(Tls12NotSupported) };
	(tls13_no_key_share) => { def_err!(Tls13NoKeyShare) };
	(tls13_required_not_offered) =>{ def_err!(Tls13RequiredNotOffered) };
	(unexpected_ccs $x:expr) => { def_err!(UnexpectedCcs $x) };
	(unexpected_handshake_message $x:expr, $y:expr) => { def_err!(UnexpectedHandshakeMessage $x, $y) };
	(unexpected_rtype $x:expr) => { def_err!(UnexpectedRtype $x) };
	(unknown_acme_validation_target $x:expr) => { def_err!(UnknownAcmeValidationTarget $x) };
	(version_mismatch_hrr_sh)  => { def_err!(VersionMismatchHrrSh) };
	(vulernable_renego) => { def_err!(VulernableRenego) };
	(vulernable_ths) => { def_err!(VulernableTHS) };
}

impl From<Tls12KeyScheduleError> for Error
{
	fn from(x: Tls12KeyScheduleError) -> Error
	{
		use Tls12KeyScheduleError::*;
		match x {
			InternalError(e) => From::from(e),
			SignatureFailed(e) => error!(ske_signature_failed e),
			BadFinishedMac => error!(finished_mac_failed),
			_ => From::from(assert_failure!("Unknown TLS 1.2 key schedule error"))
		}
	}
}

impl From<Tls13KeyScheduleError> for Error
{
	fn from(x: Tls13KeyScheduleError) -> Error
	{
		use Tls13KeyScheduleError::*;
		match x {
			InternalError(e) => From::from(e),
			SignatureFailed(e) => error!(cv_signature_failed e),
			BadFinishedMac => error!(finished_mac_failed),
			BadBinder => error!(binder_mac_failed),
			_ => From::from(assert_failure!("Unknown TLS 1.3 key schedule error"))
		}
	}
}

impl From<ExtractorError> for Error
{
	fn from(x: ExtractorError) -> Error
	{
		use ExtractorError::*;
		From::from(match x {
			InternalError(e) => e,
			ExtractorNotAllowed => assert_failure!(
				"TLS Extractors require TLS 1.3 or extended master secret"),
			_ => assert_failure!("Unknown TLS extractor error")
		})
	}
}

impl From<DiffieHellmanError> for Error
{
	fn from(x: DiffieHellmanError) -> Error
	{
		use DiffieHellmanError::*;
		match x {
			AgreeError(g) => error!(diffie_hellman_agree_error NamedGroup::new(g)),
			_ => From::from(assert_failure!("Unknown Diffie-Hellman error"))
		}
	}
}

//This is internal, so can be hidden.
#[doc(hidden)]
#[derive(Clone,Debug)]
pub enum _Error
{
	///Assertion failed!
	AssertionFailed(AssertFailed),

	///ACME validation done.
	AcmeValidationDone,
	///No cipher suites to advertise.
	AdvNoCipherSuites,
	///No groups to advertise.
	AdvNoGroups,
	///No signatures to advertise.
	AdvNoSignatures,
	///ALPN selection internal error.
	AlpnInternalError,
	///No overlap between client and server ALPN values.
	AlpnNoOverlap,
	///Tried to send application data after EOF.
	AppdataAfterEof,
	///Tried to send application data before finished.
	AppdataBeforeFinished,
	///Binder MAC check failed.
	BinderMacFailed,
	///Bogus ACME ALPN.
	BogusAcmeAlpn,
	///Bogus ACME chain length.
	BogusAcmeChainLen,
	///Bogus ACME names.
	BogusAcmeNames,
	///Bogus ACME serial number.
	BogusAcmeSerial,
	///Bogus ACME self-signature.
	BogusAcmeSignature,
	///Bogus ACME token.
	BogusAcmeToken,
	///Certificate requires OCSP stapling.
	CertRequiresOcsp,
	///Certificate requires SCT stapling.
	CertRequiresSct,
	///ChangeCipherSpec in middle of handshake message.
	CcsInMiddleOfHandshakeMessage,
	///Attempted to restart handshake twice.
	ChDoubleRestart,
	///Client authentication failed.
	ClientAuthenticationFailed,
	///Client does not support uncompressed coding.
	ClientDoesNotSupportUncompressed,
	///Client does not support uncompressed EC format.
	ClientDoesNotSupportUncompressedEc,
	///Record deprotection failed.
	DeprotectionFailed,
	///Downgrade attack detected.
	DowngradeAttackDetected,
	///Dynamic RSA not allowed.
	DynamicRsaNotAllowed,
	///End-Entity certificate empty.
	EeCertificateEmpty,
	///Empty handshake messages are not allowed.
	EmptyHandshakeMessage,
	///Finished MAC check failed.
	FinishedMacFailed,
	///Finished does not end last fragment it is in.
	FinishedNotAligned,
	///Handshake truncation.
	HandshakeTruncation,
	///Insecure truncation.
	InsecureTruncation,
	///Interlock signature algorithm.
	InterlockSignatureAlgorithm,
	///Invalid ChangeCipherSpec message.
	InvalidCcs,
	///Kex signature failed.
	KexSignatureFailed,
	///KeyUpdate does not end last fragment it is in.
	KeyUpdateNotAligned,
	///Key Vulernable to DROWNing
	KeyVulernableToDrowning,
	///No target for ACME validation.
	NoAcmeValidationTarget,
	///No mutually supported certificate type.
	NoMutualCertificateType,
	///No mutually supported ciphersuites.
	NoMutualCiphersuites,
	///No mutually supported groups.
	NoMutualGroups,
	///No mutually supported signatures.
	NoMutualSignatures,
	///No mutually supported versions.
	NoMutualVersions,
	///No SNI in ACME validation.
	NoSniInAcmeValidation,
	///ClientHello missing supported_groups.
	NoSupportedGroups,
	///Renegotiation attack detected!
	RenegotiationAttackDetected,
	//Secure192 required but not offered.
	Secure192RequiredNotOffered,
	///Server certificate chain empty.
	ServerChainEmpty,
	///ServerHello does not end last fragment it is in.
	ServerHelloNotAlignedTls13,
	///Server key not whitelisted (RPK).
	ServerKeyNotWhitelisted,
	///SSL3 clients not supported.
	Ssl3NotSupported,
	///Test done.
	TestDone,
	///Test failure.
	TestFailure,
	///TLS 1.0 clients not supported.
	Tls10NotSupported,
	///TLS 1.1 clients not supported.
	Tls11NotSupported,
	///TLS 1.2 clients not supported.
	Tls12NotSupported,
	///No key share in TLS 1.3 ServerHello.
	Tls13NoKeyShare,
	//TLS 1.3 required but not offered.
	Tls13RequiredNotOffered,
	///Version mismatch between HRR and SH.
	VersionMismatchHrrSh,
	///Peer vulernable to renegotiation attack.
	VulernableRenego,
	///Peer vulernable to THS.
	VulernableTHS,
	///Client offers bad ciphers.
	ClientSupportsBadCiphers,

	///Name reserved for ACME.
	AcmeReservedName(String),
	///Bad Cookie.
	BadCookie,
	///Bad entity SPKI.
	BadEntitySpki(&'static str),
	///HelloRetryRequest contains bogus TLS version.
	BogusHrrTlsVersion(TlsVersion),
	///Bogus version in ServerHello.
	BogusShVersion(TlsVersion),
	///Can't extract SCTs from OCSP.
	CantExtractSctOcsp(OcspValidationError),
	///Can't extract SPKI from certificate.
	CantExtractSpki(CertificateError),
	///Can't parse immediate issuer certificate.
	CantParseIssuerCertificate(CertificateError),
	///Certificate Validation failed.
	CertificateValidationFailed(CertificateValidationError),
	///CertificateRequest context must be empty.
	CrContextMustBeEmpty,
	///CertificateVerify signature failed verification.
	CvSignatureFailed(SignatureError2),
	///Can't parse EE certificate.
	EECertParseError(CertificateError),
	///We already sent share for this group.
	HrrAlreadyHadShare(NamedGroup),
	///Error in handshake message buffer.
	HsMsgBuffer(HsBufferError),
	///Interlock certificate expired.
	InterlockCertificateExpired(Timestamp),
	///Interlock certificate name.
	InterlockCertificateName(String),
	///Interlock OCSP expired.
	InterlockOcspExpired(Timestamp),
	///Invalid OCSP response (does not validate).
	InvalidOcsp(OcspValidationError),
	//Invalid reverse-DNS name
	InvalidRdnsName(String),
	///Message error parsing certificate status.
	MsgerrCertificateRequest(CertificateRequestParseError),
	///Message error parsing certificate status.
	MsgerrCertificateStatus(CertificateStatusParseError),
	///Message error parsing certificate verify.
	MsgerrCertificateVerify(CertificateVerifyParseError),
	///Message error parsing client certificate.
	MsgerrClientCertificate(CertificateParseError),
	///Message error parsing client hello.
	MsgerrClientHello(ClientHelloParseError),
	///Message error parsing client key exchange.
	MsgerrClientKeyExchange(ClientKeyExchangeParseError),
	///Message error parsing encrypted extensions.
	MsgerrEncryptedExtensions(EncryptedExtensionsParseError),
	///Message error parsing encrypted extensions.
	MsgerrKeyUpdate(KeyUpdateParseError),
	///Message error parsing server certificate.
	MsgerrServerCertificate(CertificateParseError),
	///Message error parsing server hello.
	MsgerrServerHello(ServerHelloParseError),
	///Message error parsing server key exchange.
	MsgerrServerKeyExchange(ServerKeyExchangeParseError),
	///No certificate available for SNI name.
	NoCertificateAvailable(String),
	///No matching certificate available.
	NoMatchingCertificate(String),
	///Locally raised alert.
	RaiseAlert(Alert),
	///Alert received from the remote.
	ReceivedAlert(Alert),
	///Restarting handshake (this is very much magic).
	RestartingHandshake(Vec<u8>),
	///Received record too big.
	RxRecordTooBig(usize),
	///SCT validation failed.
	SctValidationFailed(SctError),
	///ServerHelloDone message is not empty.
	ServerHelloDoneNotEmpty(usize),
	///Selected cipher not valid for TLS 1.2.
	ShBadTls12Cipher(CipherSuite),
	///Selected cipher not valid for TLS 1.3.
	ShBadTls13Cipher(CipherSuite),
	///Unsupported TLS version in ServerHello.
	ShUnsupportedTlsVersion(TlsVersion),
	///ServerKeyExchange signature failed to verify.
	SkeSignatureFailed(SignatureError2),
	///Unexpected ChangeCipherSpec message.
	UnexpectedCcs(&'static str),
	///Unexpected record type.
	UnexpectedRtype(ContentType),
	///Unknown ACME validation target.
	UnknownAcmeValidationTarget(String),

	///Ciphersuites disagree between HRR and SH.
	CiphersuiteDisagreement(CipherSuite, CipherSuite),
	///Error performing Diffie-Hellman key agreemment
	DiffieHellmanAgreeError(NamedGroup),
	///KeyShare groups between HRR and KeyShare disagree.
	KeyShareGroupDisagreement(NamedGroup, NamedGroup),
	///OCSP response lives too long.
	OcspValidityTooLong(u64, u64),
	///Message serialization failed.
	SerializeFailed(&'static str, VectorFieldError),
	///Unexpected handshake message type.
	UnexpectedHandshakeMessage(HandshakeType, &'static str),
}


impl Display for _Error
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::_Error::*;
		match self {
			&AssertionFailed(ref err) => write!(fmt, "Assertion failed: {err}"),

			&AcmeValidationDone => fmt.write_str(self.shortdesc()),
			&AdvNoCipherSuites => fmt.write_str(self.shortdesc()),
			&AdvNoGroups => fmt.write_str(self.shortdesc()),
			&AdvNoSignatures => fmt.write_str(self.shortdesc()),
			&AlpnInternalError => fmt.write_str(self.shortdesc()),
			&AlpnNoOverlap => fmt.write_str(self.shortdesc()),
			&AppdataAfterEof => fmt.write_str(self.shortdesc()),
			&AppdataBeforeFinished => fmt.write_str(self.shortdesc()),
			&BadCookie => fmt.write_str(self.shortdesc()),
			&BinderMacFailed => fmt.write_str(self.shortdesc()),
			&BogusAcmeAlpn => fmt.write_str(self.shortdesc()),
			&BogusAcmeChainLen => fmt.write_str(self.shortdesc()),
			&BogusAcmeNames => fmt.write_str(self.shortdesc()),
			&BogusAcmeSerial => fmt.write_str(self.shortdesc()),
			&BogusAcmeSignature => fmt.write_str(self.shortdesc()),
			&BogusAcmeToken => fmt.write_str(self.shortdesc()),
			&CcsInMiddleOfHandshakeMessage => fmt.write_str(self.shortdesc()),
			&CertRequiresOcsp => fmt.write_str(self.shortdesc()),
			&CertRequiresSct => fmt.write_str(self.shortdesc()),
			&ChDoubleRestart => fmt.write_str(self.shortdesc()),
			&ClientAuthenticationFailed => fmt.write_str(self.shortdesc()),
			&ClientDoesNotSupportUncompressed => fmt.write_str(self.shortdesc()),
			&ClientDoesNotSupportUncompressedEc => fmt.write_str(self.shortdesc()),
			&ClientSupportsBadCiphers => fmt.write_str(self.shortdesc()),
			&CrContextMustBeEmpty => fmt.write_str(self.shortdesc()),
			&DeprotectionFailed => fmt.write_str(self.shortdesc()),
			&DowngradeAttackDetected => fmt.write_str(self.shortdesc()),
			&DynamicRsaNotAllowed => fmt.write_str(self.shortdesc()),
			&EeCertificateEmpty => fmt.write_str(self.shortdesc()),
			&EmptyHandshakeMessage => fmt.write_str(self.shortdesc()),
			&FinishedMacFailed => fmt.write_str(self.shortdesc()),
			&FinishedNotAligned => fmt.write_str(self.shortdesc()),
			&HandshakeTruncation => fmt.write_str(self.shortdesc()),
			&InsecureTruncation => fmt.write_str(self.shortdesc()),
			&InterlockSignatureAlgorithm => fmt.write_str(self.shortdesc()),
			&InvalidCcs => fmt.write_str(self.shortdesc()),
			&KexSignatureFailed => fmt.write_str(self.shortdesc()),
			&KeyUpdateNotAligned => fmt.write_str(self.shortdesc()),
			&KeyVulernableToDrowning => fmt.write_str(self.shortdesc()),
			&NoAcmeValidationTarget => fmt.write_str(self.shortdesc()),
			&NoMutualCertificateType => fmt.write_str(self.shortdesc()),
			&NoMutualCiphersuites => fmt.write_str(self.shortdesc()),
			&NoMutualGroups => fmt.write_str(self.shortdesc()),
			&NoMutualSignatures => fmt.write_str(self.shortdesc()),
			&NoMutualVersions => fmt.write_str(self.shortdesc()),
			&NoSniInAcmeValidation => fmt.write_str(self.shortdesc()),
			&NoSupportedGroups => fmt.write_str(self.shortdesc()),
			&RenegotiationAttackDetected => fmt.write_str(self.shortdesc()),
			&Secure192RequiredNotOffered => fmt.write_str(self.shortdesc()),
			&ServerChainEmpty => fmt.write_str(self.shortdesc()),
			&ServerHelloNotAlignedTls13 => fmt.write_str(self.shortdesc()),
			&ServerKeyNotWhitelisted => fmt.write_str(self.shortdesc()),
			&Ssl3NotSupported => fmt.write_str(self.shortdesc()),
			&TestDone => fmt.write_str(self.shortdesc()),
			&TestFailure => fmt.write_str(self.shortdesc()),
			&Tls10NotSupported => fmt.write_str(self.shortdesc()),
			&Tls11NotSupported => fmt.write_str(self.shortdesc()),
			&Tls12NotSupported => fmt.write_str(self.shortdesc()),
			&Tls13NoKeyShare => fmt.write_str(self.shortdesc()),
			&Tls13RequiredNotOffered => fmt.write_str(self.shortdesc()),
			&VersionMismatchHrrSh => fmt.write_str(self.shortdesc()),
			&VulernableRenego => fmt.write_str(self.shortdesc()),
			&VulernableTHS => fmt.write_str(self.shortdesc()),

			&AcmeReservedName(ref name) => write!(fmt, "SNI value is reserved for ACME: {name}"),
			&BadEntitySpki(err) => write!(fmt, "Bad entity SPKI: {err}"),
			&BogusHrrTlsVersion(ver) => write!(fmt, "HelloRetryRequest contains bogus TLS version {ver}"),
			&BogusShVersion(ver) => write!(fmt, "ServerHello contains bogus TLS version {ver}"),
			&CantExtractSctOcsp(ref err) => write!(fmt, "Error extracting SCTs from OCSP response: {err}"),
			&CantExtractSpki(ref err) => write!(fmt, "Error extracting SPKI: {err}"),
			&CantParseIssuerCertificate(ref err) =>
				write!(fmt, "Error parsing immediate issuer certificate: {err}"),
			&CertificateValidationFailed(ref err) => write!(fmt, "Certificate failed to validate: {err}"),
			&CvSignatureFailed(ref err) => write!(fmt, "Bad CertificateVerify signature: {err}"),
			&EECertParseError(ref err) => write!(fmt, "Error parsing EE certificate: {err}"),
			&HrrAlreadyHadShare(grp) =>
				write!(fmt, "HelloRetryRequest contains share {grp} already present"),
			&HsMsgBuffer(err) => write!(fmt, "Error extracting handshake message: {err}"),
			&InterlockCertificateExpired(ts) => write!(fmt, "Interlock certificate expired on {ts}",
				ts=DecodeTsForUser2(ts)),
			&InterlockCertificateName(ref name) =>
				write!(fmt, "Interlock certificate not valid for {name}"),
			&InterlockOcspExpired(ts) => write!(fmt, "Interlock OCSP expired on {ts}",
				ts=DecodeTsForUser2(ts)),
			&InvalidOcsp(ref err) => write!(fmt, "OCSP response failed to validate: {err}"),
			&InvalidRdnsName(ref name) => write!(fmt, "Invalid Reverse DNS name: {name}"),
			&MsgerrCertificateRequest(err) => write!(fmt, "Certificate Request: {err}"),
			&MsgerrCertificateStatus(err) => write!(fmt, "Certificate Status: {err}"),
			&MsgerrCertificateVerify(err) => write!(fmt, "Certificate Verify: {err}"),
			&MsgerrClientCertificate(err) => write!(fmt, "Client Certificate: {err}"),
			&MsgerrClientHello(err) => write!(fmt, "Client Hello: {err}"),
			&MsgerrClientKeyExchange(err) => write!(fmt, "Client Key Exchange: {err}"),
			&MsgerrEncryptedExtensions(err) => write!(fmt, "Encrypted Extensions: {err}"),
			&MsgerrKeyUpdate(err) => write!(fmt, "Key Update: {err}"),
			&MsgerrServerCertificate(err) => write!(fmt, "Server Certificate: {err}"),
			&MsgerrServerHello(err) => write!(fmt, "Server Hello: {err}"),
			&MsgerrServerKeyExchange(err) => write!(fmt, "Server Key Exchange: {err}"),
			&NoCertificateAvailable(ref name) => write!(fmt, "No certificate available for {name}"),
			&NoMatchingCertificate(ref name) => write!(fmt, "No matching certificate for {name}"),
			&RaiseAlert(alert) => write!(fmt, "Locally raised alert {alert}"),
			&ReceivedAlert(alert) => write!(fmt, "Received TLS alert {alert}"),
			&RestartingHandshake(_) => fmt.write_str(self.shortdesc()),
			&RxRecordTooBig(size) => write!(fmt, "Received record too big ({size})"),
			&SctValidationFailed(ref err) => write!(fmt, "SCTs failed to validate: {err}"),
			&ServerHelloDoneNotEmpty(len) => write!(fmt, "ServerHelloDone is not empty (len={len})"),
			&ShBadTls12Cipher(cipher) =>
				write!(fmt, "ServerHello contains ciphersuite {cipher} not compatible with TLS 1.2"),
			&ShBadTls13Cipher(cipher) =>
				write!(fmt, "ServerHello contains ciphersuite {cipher} not compatible with TLS 1.3"),
			&ShUnsupportedTlsVersion(ver) => write!(fmt, "ServerHello contains unsupported version {ver}"),
			&SkeSignatureFailed(ref err) =>
				write!(fmt, "ServerKeyExchange signature does not verify: {err}"),
			&UnexpectedCcs(state) => write!(fmt, "ChangeCipherSpec received in unexpected state {state}"),
			&UnexpectedRtype(rtype) => write!(fmt, "Received unexpected record type {rtype}"),
			&UnknownAcmeValidationTarget(ref name) => write!(fmt, "Unknown ACME validation target {name}"),

			&CiphersuiteDisagreement(shcipher,hrrcipher) => write!(fmt, "ServerHello contains \
				ciphersuite {shcipher} different from HelloRetryRequest({hrrcipher})"),
			&DiffieHellmanAgreeError(grp) =>
				write!(fmt, "Error doing Diffie-Hellman key agreement (curve #{grp})"),
			&KeyShareGroupDisagreement(shgrp, hrrgrp) => write!(fmt, "ServerHello contains group \
				{shgrp} different from HelloRetryRequest {hrrgrp}"),
			&OcspValidityTooLong(valid, limit) =>
				write!(fmt, "OCSP staple lifetime too long ({valid}s, limit is {limit}s)"),
			&SerializeFailed(msg, ref err) => write!(fmt, "Error serializing {msg}: {err}"),
			&UnexpectedHandshakeMessage(mtype, state) =>
				write!(fmt, "Received unexpected handshake type {mtype} for state {state}"),
		}
	}
}

impl _Error
{
	fn shortdesc(&self) -> &'static str
	{
		use self::_Error::*;
		match self {
			&AssertionFailed(_) => "Internal error, assertion failed",
			&AcmeValidationDone => "ACME validation handshake done",
			&AdvNoCipherSuites => "No cipher suites to advertise",
			&AdvNoGroups => "No groups to advertise",
			&AdvNoSignatures => "No signature algorithms to advertise",
			&AlpnInternalError => "Internal error selecting ALPN",
			&AlpnNoOverlap => "No mutual application protocols",
			&AppdataAfterEof => "Received application data after EOF",
			&AppdataBeforeFinished => "Received application data before Finished",
			&BadCookie => "Bad cookie",
			&BinderMacFailed => "Binder contains bad MAC",
			&BogusAcmeAlpn => "Server did not accept the ACME validation protocol",
			&BogusAcmeChainLen => "ACME validation certificates must not have issuer certificate",
			&BogusAcmeNames => "ACME validation certificate is for wrong host",
			&BogusAcmeSerial => "ACME validation certificate has invalid serial",
			&BogusAcmeSignature => "ACME validation certificate has bad self-signature",
			&BogusAcmeToken => "ACME validation certificate has wrong token",
			&CcsInMiddleOfHandshakeMessage => "ChangeCipherSpec interrupted a handshake message",
			&CertRequiresOcsp => "Certificate requires OCSP stapling",
			&CertRequiresSct => "Certificate requires SCT stapling",
			&ChDoubleRestart => "Double restart in TLS 1.3",
			&ClientAuthenticationFailed => "Client authentication failed",
			&ClientDoesNotSupportUncompressed => "Client does not support NULL compression",
			&ClientDoesNotSupportUncompressedEc => "Client does not support uncompressed EC points",
			&CrContextMustBeEmpty => "CertificateRequest context must be empty",
			&CvSignatureFailed(_) => "CertificateVerify contains bad signature",
			&DeprotectionFailed => "Error deprotecting record",
			&DowngradeAttackDetected => "DOWNGRADE ATTACK DETECTED!!!",
			&DynamicRsaNotAllowed => "Dynamic RSA not allowed",
			&EeCertificateEmpty => "EE certificate is empty",
			&EmptyHandshakeMessage => "Empty Handshake message",
			&FinishedMacFailed => "Finished contains bad MAC",
			&FinishedNotAligned => "Finished does not end at record boundary",
			&HandshakeTruncation => "Peer disconnected before completing the handshake",
			&InsecureTruncation => "Insecure truncation",
			&InterlockSignatureAlgorithm => "Interlock signature algorithm",
			&InvalidCcs => "ChangeCipherSpec invalid ",
			&KexSignatureFailed => "Signing key exchange failed",
			&KeyUpdateNotAligned => "KeyUpdate does not end at record boundary",
			&KeyVulernableToDrowning => "Peer key is vulernable to DROWNing",
			&MsgerrCertificateRequest(_) => "Error parsing certificate request",
			&MsgerrCertificateStatus(_) => "Error parsing certificate status",
			&MsgerrCertificateVerify(_) => "Error parsing certificate verify",
			&MsgerrClientCertificate(_) => "Error parsing client certificate",
			&MsgerrClientHello(_) => "Error parsing client hello",
			&MsgerrClientKeyExchange(_) => "Error parsing client key exchange",
			&MsgerrEncryptedExtensions(_) => "Error parsing encrypted extensions",
			&MsgerrKeyUpdate(_) => "Error parsing key update",
			&MsgerrServerCertificate(_) => "Error parsing server certificate",
			&MsgerrServerHello(_) => "Error parsing server hello",
			&MsgerrServerKeyExchange(_) => "Error parsing server key exchange",
			&NoAcmeValidationTarget => "No ACME validation target known",
			&NoMutualCertificateType => "No certificate type overlap between client and server",
			&NoMutualCiphersuites => "No ciphersuite overlap between client and server",
			&NoMutualGroups => "No group overlap between client and server",
			&NoMutualSignatures => "No signature overlap between client and server",
			&NoMutualVersions => "No version overlap between client and server",
			&NoSniInAcmeValidation => "No SNI in ACME validation request",
			&NoSupportedGroups => "SupportedGroups is required",
			&RenegotiationAttackDetected => "RENEGOTIATION ATTACK DETECTED!!!",
			&Secure192RequiredNotOffered => "Secure192 required but not offered by client",
			&ServerChainEmpty => "Server did not send any certificates",
			&ServerHelloNotAlignedTls13 => "ServerHello(TLS 1.3) does not end at record boundary",
			&ServerKeyNotWhitelisted => "Server key not on whitelist",
			&Ssl3NotSupported => "SSL v3 is not supported",
			&TestDone => "Test done",
			&TestFailure => "Test failure",
			&Tls10NotSupported => "TLS 1.0 is not supported",
			&Tls11NotSupported => "TLS 1.1 is not supported",
			&Tls12NotSupported => "TLS 1.2 is not supported",
			&Tls13NoKeyShare => "TLS 1.3 chosen but no key share sent",
			&Tls13RequiredNotOffered => "TLS 1.3 required but not offered by client",
			&VulernableRenego => "Remote end is vulernable to renegotiation attack",
			&VersionMismatchHrrSh => "TLS version mismatch between HelloRetryRequest and ServerHello",
			&VulernableTHS => "Remote end is vulernable to triple handshake attack",
			&ClientSupportsBadCiphers => "Client offered bad ciphers",

			&AcmeReservedName(_) => "SNI value is reserved for ACME",
			&BadEntitySpki(_) => "Bad entity SPKI",
			&BogusHrrTlsVersion(_) => "HelloRetryRequest contains bogus TLS version",
			&BogusShVersion(_) => "ServerHello contains bogus TLS version",
			&CantExtractSctOcsp(_) => "Erfror extracting SCTs from OCSP response",
			&CantExtractSpki(_) => "Error extracting SPKI",
			&CantParseIssuerCertificate(_) => "Error parsing immediate issuer certificate",
			&CertificateValidationFailed(_) => "Certificate failed to validate",
			&EECertParseError(_) => "Error parsing EE certificate",
			&HrrAlreadyHadShare(_) => "HelloRetryRequest contains share already present",
			&HsMsgBuffer(_) => "Error extracting handshake message",
			&InterlockCertificateExpired(_) => "Interlock certificate expired",
			&InterlockCertificateName(_) => "Interlock certificate not valid for",
			&InterlockOcspExpired(_) => "Interlock OCSP response expired",
			&InvalidOcsp(_) => "OCSP response failed to validate",
			&InvalidRdnsName(_) => "Invalid reverse DNS name",
			&NoCertificateAvailable(_) => "No certificate available",
			&NoMatchingCertificate(_) => "No matching certificate",
			&RaiseAlert(_) => "Locally raised alert",
			&ReceivedAlert(_) => "Received TLS alert",
			&RestartingHandshake(_) => "Restarting handshake",
			&RxRecordTooBig(_) => "Received record too big",
			&SctValidationFailed(_) => "SCTs failed to validate",
			&ServerHelloDoneNotEmpty(_) => "ServerHelloDone is not empty",
			&ShBadTls12Cipher(_) => "ServerHello contains ciphersuite not compatible with TLS 1.2",
			&ShBadTls13Cipher(_) => "ServerHello contains ciphersuite not compatible with TLS 1.3",
			&ShUnsupportedTlsVersion(_) => "ServerHello contains unsupported version",
			&SkeSignatureFailed(_) => "ServerKeyExchange contains bad signature",
			&UnexpectedCcs(_) => "ChangeCipherSpec received in unexpected state",
			&UnexpectedRtype(_) => "Received unexpected record type",
			&UnknownAcmeValidationTarget(_) => "Unknown ACME validation target",

			&CiphersuiteDisagreement(_, _) => "Ciphersuites in HRR and SH disagree",
			&DiffieHellmanAgreeError(_) => "Error doing Diffie-Hellman key exchange",
			&KeyShareGroupDisagreement(_, _) => "Groups in HRR and SH disagree",
			&OcspValidityTooLong(_, _) => "OCSP staple lifetime too long",
			&SerializeFailed(_, _) => "Message serialization failed",
			&UnexpectedHandshakeMessage(_, _) => "Unexpected handshake message",
		}
	}
}
