use crate::certificates::ServerCertificate;
use crate::certificates::sign_certificate;
use crate::errors::Error;
use crate::stdlib::Arc;
use crate::stdlib::Cow;
use crate::stdlib::Deref;
use crate::stdlib::Vec;
use btls_aux_futures::FutureMapperFn;
use btls_aux_futures::FutureReceiver;
use btls_aux_hash::sha1insecure;
use btls_aux_hash::sha256;
use btls_aux_ip::IpAddress;
use btls_aux_keypair_local::LocalKeyPair;
use btls_aux_rope3::rope3;
use btls_aux_rope3::FragmentWriteOps;
use btls_aux_signatures::Hasher;
use btls_aux_signatures::KeyPair2;
use btls_aux_signatures::SignatureAlgorithmTls2;
use btls_aux_signatures::SignatureFormat;
use btls_aux_signatures::TemporaryRandomStream;
use btls_aux_x509certvalidation::ReferenceName;

pub(crate) struct AcmeCertificate
{
	certificate: Arc<Vec<u8>>,
	keypair: Arc<LocalKeyPair>,
	use_algo: SignatureAlgorithmTls2,
}

impl AcmeCertificate
{
	pub(crate) fn sign(&self, data: &mut dyn FnMut(&mut Hasher), rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<(u16, Vec<u8>), ()>>
	{
		//It is LocalKeyPair, which is supposed to sign immediately.
		let signature = self.keypair.sign_callback(self.use_algo.to_generic(), rng, data);
		let talgo = self.use_algo.tls_id();
		signature.map(FutureMapperFn::new(move |x: Result<(SignatureFormat, Vec<u8>), ()>|{
			Ok((talgo, x?.1))
		}))
	}
	pub(crate) fn certificate<'a>(&'a self) -> &'a [u8] { self.certificate.deref().deref() }
}

fn make_certificate(keypair: &LocalKeyPair, hostname: ReferenceName, challenge: &str, algid: SignatureAlgorithmTls2,
	oid: &[u8]) -> Result<Vec<u8>, Error>
{
	const HEXES: [u8;16] = [48,49,50,51,52,53,54,55,56,57,97,98,99,100,101,102];
	let spki = keypair.get_public_key2();
	//Get serial number for hostname and challenge.
	let hoststr = match Some(&hostname) {
		Some(&ReferenceName::DnsName(x)) => Cow::Borrowed(x),
		Some(&ReferenceName::Ip(addr)) => Cow::Owned(format!("[{addr}]")),
		_ => sanity_failed!("Trying to generate ACME validation certificate for unknown address type")
	};
	let hinput = format!("{hoststr}/{challenge}/{algid_tls}", algid_tls=algid.tls_id());
	let mut cert_id = sha1insecure(hinput.as_bytes());
	let validation_response = sha256(challenge.as_bytes());
	cert_id[0] &= 127;	//Mask high bit.
	//If cert_id is not valid INTEGER, fudge it to be valid.
	if cert_id[0] == 0 && cert_id[1] < 128 { cert_id[1] = 128; }
	let mut cert_id2 = [0; 40];
	for i in 0..20 {
		cert_id2[2*i+0] = HEXES[(cert_id[i] >> 4) as usize];
		cert_id2[2*i+1] = HEXES[(cert_id[i] & 15) as usize];
	}
	let mut addrbuf = [0;16];
	let addr_ent = match Some(hostname) {
		Some(ReferenceName::DnsName(x)) => rope3!([2] {x.as_bytes()}),
		Some(ReferenceName::Ip(IpAddress::Ipv4(x))) => {
			(&mut addrbuf[..4]).copy_from_slice(&x);
			rope3!([7] {&addrbuf[..4]})
		},
		Some(ReferenceName::Ip(IpAddress::Ipv6(x))) => {
			for i in 0..16 { addrbuf[i] = (x[i>>1] >> 8 - i % 2 * 8) as u8 }
			rope3!([7] {&addrbuf[..16]})
		},
		_ => sanity_failed!("Trying to generate ACME validation certificate for unknown address type")
	};

	//Serialize the certificate.
	let tbs = rope3!(SEQUENCE(
		//The version number.
		[0](INTEGER 2u8),
		//Serial.
		INTEGER cert_id,
		//Algorithm.
		SEQUENCE(oid),
		//Issuer.
		SEQUENCE(
			SET(SEQUENCE(
				OBJECT_IDENTIFIER X509_SUBJECT_SERIAL_NUMBER,
				PRINTABLE_STRING cert_id2
			))
		),
		//Validity.
		SEQUENCE(
			//1950-2049.
			UTC_TIME "500101000000Z",
			UTC_TIME "491231235959Z"
		),
		//Subject.
		SEQUENCE(
			SET(SEQUENCE(
				OBJECT_IDENTIFIER X509_SUBJECT_SERIAL_NUMBER,
				PRINTABLE_STRING cert_id2
			))
		),
		//SubjectPublicKeyInfo.
		spki,
		//Extensions.
		[3](SEQUENCE(
			//BasicConstraints (critical).
			SEQUENCE(
				OBJECT_IDENTIFIER X509_EXT_BASIC_CONSTRAINTS,
				BOOLEAN_TRUE,
				OCTET_STRING SEQUENCE()
			),
			//SAN.
			SEQUENCE(
				OBJECT_IDENTIFIER X509_EXT_SAN,
				OCTET_STRING SEQUENCE(addr_ent)
			),
			//The valdiation extension (critical).
			SEQUENCE(
				OBJECT_IDENTIFIER X509_EXT_ACME_ALPN,
				BOOLEAN_TRUE,
				OCTET_STRING OCTET_STRING validation_response
			)
		))
	));
	let tbs = FragmentWriteOps::to_vector(&tbs);
	//The keypair is supposed to be able to sign instantly, so waits inside are not a problem.
	Ok(assert_some!(sign_certificate(&tbs, keypair, algid.to_generic()), "Failed to sign ACME certificate"))
}

pub(crate) fn generate_acme_certificate(challenge: &str, hostname: ReferenceName, keypair: Arc<LocalKeyPair>) ->
	Result<ServerCertificate, Error>
{
	let err = "The ACME keypair is not suitable for TLS";
	//The algorithm must have both X.509 ID and TLS ID. Pick first such algorithm.
	let algorithm = keypair.get_signature_algorithms().iter().find(|&&algo|{
		algo.get_oid().is_some() && algo.downcast_tls().is_some()
	});
	let (oid, talgid) = match algorithm {
		Some(&algorithm) => match (algorithm.get_oid(), algorithm.downcast_tls()) {
			(Some(oid), Some(talgid)) => Some((oid, talgid)),
			_ => None
		},
		_ => None
	}.ok_or_else(||{
		assert_failure!(F "{err}")
	})?;
	let cert = make_certificate(keypair.deref(), hostname, challenge, talgid, oid)?;
	Ok(ServerCertificate::Acme(AcmeCertificate{
		use_algo:talgid,
		keypair:keypair,
		certificate:Arc::new(cert)
	}))
}
