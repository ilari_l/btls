use super::ThreadMetrics;
use btls_aux_unix::Address;
use btls_aux_unix::FileDescriptor;
use btls_aux_unix::Path as UnixPath;
use btls_aux_unix::Pid;
use btls_aux_unix::PollFd;
use btls_aux_unix::PollFlags;
use btls_aux_unix::SocketFamily;
use btls_aux_unix::SocketFlags;
use btls_aux_unix::SocketType;
use btls_aux_unix::Umask;
use btls_aux_unix::UnixPath as SUnixPath;
use btls_util_logging::set_logfile_permissions_file_rw;
use btls_util_logging::syslog;
use std::collections::BTreeMap;
use std::fmt::Arguments;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::fmt::Write;
use std::mem::replace;
use std::ops::Add;
use std::ops::Sub;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering;
use std::thread::Builder;
use std::time::Duration;
use std::time::Instant;
use std::time::SystemTime;
use std::time::UNIX_EPOCH;

macro_rules! reterr
{
	($e:expr) => { match $e { Ok(x) => x, Err(e) => return e } }
}


pub trait MetricsCollector: Send
{
	fn fill(&self, to: &mut BTreeMap<u32, usize>);
}

fn collect_metrics_bytes<C:MetricsCollector+'static>(pid: usize, collect: &C) -> Vec<u8>
{
	let mut r = BTreeMap::new();
	r.insert(0x7FFFFFFF, pid);
	collect.fill(&mut r);
	let mut r2 = Vec::with_capacity(12 * r.len() + 4);
	r2.extend_from_slice(&(r.len() as u32).to_le_bytes());
	for (k, v) in r.iter() {
		r2.extend_from_slice(&(*k as u32).to_le_bytes());
		r2.extend_from_slice(&(*v as u64).to_le_bytes());
	}
	r2
}

struct Client
{
	fd: FileDescriptor,
	buf: Vec<u8>,
	bufitr: usize,
	killed: bool,
}

impl Client
{
	fn flush(&mut self) -> Result<(), ()>
	{
		match self.fd.write(&self.buf[self.bufitr..]) {
			Ok(amt) => self.bufitr += amt,
			Err(ref err) if err.is_transient() => return Ok(()),
			Err(err) => fail!(syslog!(WARNING "Metrics send error: {err}"))
		}
		if self.bufitr >= self.buf.len() {
			self.bufitr = 0;
			self.buf.clear();
		}
		Ok(())
	}
	fn wants_send(&self) -> bool { self.bufitr < self.buf.len() }
	fn set_data(&mut self, buf: &[u8]) { self.buf = buf.to_vec(); }
}

pub fn run_metrics_thread<C:MetricsCollector+'static>(mpath: &str, collect: C)
{
	if mpath.len() == 0 { return; }		//No metrics.

	//Try to remove file in the way.
	UnixPath::new(mpath.as_bytes()).and_then(|p|p.unlink().ok());
	let target = Address::Unix(SUnixPath::owned(mpath.as_bytes()));
	let _oldmask = UmaskBackswapper(Umask::PRIVATE.swap());
	let flags = SocketFlags::CLOEXEC;
	let metrics_fd = reterr!(FileDescriptor::socket2(SocketFamily::UNIX, SocketType::STREAM, Default::default(),
		flags).map_err(|err|{
		syslog!(WARNING "Failed to create metrics socket: {err}")
	}));
	reterr!(metrics_fd.bind(&target).map_err(|err|{
		syslog!(WARNING "Failed to bind metrics socket: {err}")
	}));
	reterr!(metrics_fd.listen(10).map_err(|err|{
		syslog!(WARNING "Failed to listen metrics socket: {err}")
	}));
	set_logfile_permissions_file_rw(mpath.as_bytes());		//Set log permissions for socket.
	drop(_oldmask);

	let b = Builder::new();
	let b = b.name(String::from("Metrics thread"));
	let pid = Pid::current().to_inner() as usize;
	let mut clients: Vec<Client> = Vec::new();
	let mut next_update = Instant::now();
	if let Err(err) = b.spawn(move || { loop {
		let now = Instant::now();
		if next_update < now && clients.len() > 0 {
			let r2 = collect_metrics_bytes(pid, &collect);
			for client in clients.iter_mut() {
				//If client has sent is previous batch, send next batch.
				if !client.wants_send() { client.set_data(&r2); }
			}
			next_update = now + Duration::new(0, 250_000_000);
		}
		let maxsleep = if clients.len() == 0 {
			Duration::new(86400, 0)
		} else if next_update > now {
			next_update.duration_since(now)
		} else {
			Duration::new(0, 0)
		};
		//This can not be bigger than 250 (except if clients is 0, and then it is 86400000).
		let millis = maxsleep.as_millis() as i32;
		let mut polls = Vec::new();
		let poll_none = Default::default();
		polls.push(PollFd{fd: metrics_fd.as_fdb(), events:PollFlags::IN, revents: poll_none});
		for client in clients.iter_mut() { if client.wants_send() {
			polls.push(PollFd{fd: client.fd.as_fdb(), events:PollFlags::OUT, revents: poll_none});
		}}
		unsafe{PollFd::poll(&mut polls, millis).ok()};
		//Use the fact that poll vector indices can not change, and wants_send can not change.
		if polls[0].revents.is_in() {
			let flags = SocketFlags::CLOEXEC;
			match metrics_fd.accept2(flags) {
				Ok((fd, _)) => clients.push(Client {
					fd: fd,
					buf: Vec::new(),
					bufitr: 0,
					killed: false
				}),
				Err(err) => syslog!(WARNING "Failed to accept connection: {err}")
			}
		}
		let mut idx = 1;
		for client in clients.iter_mut() { if idx < polls.len() && client.wants_send() {
			if polls[idx].revents.is_out() { client.killed |= client.flush().is_err(); }
			idx += 1;
		}}
		//Kill the clients that have gone away.
		let mut idx = 0;
		while idx < clients.len() {
			if clients[idx].killed {
				clients.remove(idx);
			} else {
				idx += 1;
			}
		}
	}}) {
		syslog!(WARNING "Failed to launch metrics thread: {err}");
	}
}

const LMETRIC_LAST_SLEEP: u32 = 0;
const LMETRIC_LAST_RUN: u32 = 1;
const LMETRIC_TOTAL_SLEEP: u32 = 2;
const LMETRIC_TOTAL_RUN: u32 = 3;
const LMETRIC_TIME_NOW: u32 = 4;
const LMETRIC_WAKEUPS: u32 = 5;
const LMETRIC_FAULTS: u32 = 6;
const LMETRIC_TASKS: u32 = 7;
const LMETRIC_DLE: u32 = 8;
const LMETRIC_WATCHDOG: u32 = 9;

struct LocalMetricsStorage
{
	last_sleep: AtomicUsize,
	last_run: AtomicUsize,
	total_sleep: AtomicUsize,
	total_run: AtomicUsize,
	wakeups: AtomicUsize,
	faults: AtomicUsize,
	tasks: AtomicUsize,
	dle_count: AtomicUsize,
	watchdog_ctr: AtomicUsize,
}

impl LocalMetricsStorage
{
	fn new() -> LocalMetricsStorage
	{
		LocalMetricsStorage {
			last_sleep: AtomicUsize::new(0),
			last_run: AtomicUsize::new(0),
			total_sleep: AtomicUsize::new(0),
			total_run: AtomicUsize::new(0),
			wakeups: AtomicUsize::new(0),
			faults: AtomicUsize::new(0),
			tasks: AtomicUsize::new(0),
			dle_count: AtomicUsize::new(0),
			watchdog_ctr: AtomicUsize::new(0),
		}
	}
	fn report(&self, base: u32, map: &mut BTreeMap<u32, usize>, basetime: Instant)
	{
		map.insert(base + LMETRIC_LAST_SLEEP, self.last_sleep.load(Ordering::Relaxed));
		map.insert(base + LMETRIC_LAST_RUN, self.last_run.load(Ordering::Relaxed));
		map.insert(base + LMETRIC_TOTAL_SLEEP, self.total_sleep.load(Ordering::Relaxed));
		map.insert(base + LMETRIC_TOTAL_RUN, self.total_run.load(Ordering::Relaxed));
		map.insert(base + LMETRIC_TIME_NOW, instant_to_diff(Instant::now(), basetime));
		map.insert(base + LMETRIC_WAKEUPS, self.wakeups.load(Ordering::Relaxed));
		map.insert(base + LMETRIC_FAULTS, self.faults.load(Ordering::Relaxed));
		map.insert(base + LMETRIC_TASKS, self.tasks.load(Ordering::Relaxed));
		map.insert(base + LMETRIC_DLE, self.dle_count.load(Ordering::Relaxed));
		map.insert(base + LMETRIC_WATCHDOG, self.watchdog_ctr.load(Ordering::Relaxed));
	}
	fn get_last_run(&self) -> usize { self.last_run.load(Ordering::Relaxed) }
	fn get_last_sleep(&self) -> usize { self.last_sleep.load(Ordering::Relaxed) }
	fn set_last_run(&self, v: usize) { self.last_run.store(v, Ordering::Relaxed); }
	fn set_last_sleep(&self, v: usize) { self.last_sleep.store(v, Ordering::Relaxed); }
	fn register_run(&self, amt: usize) { self.total_run.fetch_add(amt, Ordering::Relaxed); }
	fn register_sleep(&self, amt: usize) { self.total_sleep.fetch_add(amt, Ordering::Relaxed); }
	fn register_wakeup(&self) { self.wakeups.fetch_add(1, Ordering::Relaxed); }
	fn register_fault(&self) { self.faults.fetch_add(1, Ordering::Relaxed); }
	fn deadline_exceeded(&self) { self.dle_count.fetch_add(1, Ordering::Relaxed); }
	fn new_task(&self) { self.tasks.fetch_add(1, Ordering::Relaxed); }
	fn drop_task(&self) { self.tasks.fetch_sub(1, Ordering::Relaxed); }
	fn watchdog_event(&self) { self.watchdog_ctr.fetch_add(2, Ordering::Relaxed); }
	fn watchdog_sleepwake(&self) { self.watchdog_ctr.fetch_add(1, Ordering::Relaxed); }
}

pub fn instant_to_diff(now: Instant, base: Instant) -> usize
{
	if now > base {
		let delta = now.duration_since(base);
		delta.as_secs() as usize * 1000000 + delta.subsec_nanos() as usize / 1000
	} else {
		0
	}
}

pub struct LocalMetrics
{
	base: u32,
	basetime: Instant,
	storage: LocalMetricsStorage,
}

impl LocalMetrics
{
	pub fn new(ti: u32) -> LocalMetrics
	{
		LocalMetrics {
			base: ti * 65536 + 0x80000000,
			basetime: Instant::now(),
			storage: LocalMetricsStorage::new(),
		}
	}
	pub fn go_to_sleep(&self)
	{
		let ts = instant_to_diff(Instant::now(), self.basetime);
		let bs = self.storage.get_last_run();
		//If last wakeup time is valid, count the part between as run.
		if bs != 0 && ts > bs { self.storage.register_run(ts - bs); }
		self.storage.set_last_sleep(ts);
		self.storage.watchdog_sleepwake();
	}
	pub fn wake_up(&self)
	{
		let ts = instant_to_diff(Instant::now(), self.basetime);
		let bs = self.storage.get_last_sleep();
		//If last sleep time is valid, count the part between as sleep.
		if bs != 0 && ts > bs { self.storage.register_sleep(ts - bs); }
		self.storage.set_last_run(ts);
		self.storage.register_wakeup();
		self.storage.watchdog_sleepwake();
	}
	pub fn register_fault(&self) { self.storage.register_fault() }
	pub fn deadline_exceeded(&self) { self.storage.deadline_exceeded() }
	pub fn new_task(&self) { self.storage.new_task() }
	pub fn drop_task(&self) { self.storage.drop_task() }
	pub fn watchdog_event(&self) { self.storage.watchdog_event() }
	pub fn report(&self, r: &mut BTreeMap<u32, usize>) { self.storage.report(self.base, r, self.basetime); }
}

impl ThreadMetrics for LocalMetrics
{
	fn create(ti: usize) -> LocalMetrics
	{
		LocalMetrics::new(ti as u32)
	}
}

pub struct PrintUptime(pub Option<u64>);

impl Display for PrintUptime
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		if let Some(x) = self.0 {
			let x = x / 1000000;
			let days = x / 86400;
			let hours = x / 3600 % 24;
			let minutes = x / 60 % 60;
			let seconds = x % 60;
			write!(fmt, "{days}:{hours:02}:{minutes:02}:{seconds:02}")
		} else {
			write!(fmt, "N/A")
		}
	}
}

#[derive(Copy,Clone)]
pub struct MetricsArrayEntry(Option<u64>);

impl Sub for MetricsArrayEntry
{
	type Output = MetricsArrayEntry;
	fn sub(self, rhs: MetricsArrayEntry) -> MetricsArrayEntry
	{
		match (self.0, rhs.0) {
			//This is not symmetric: If LHS is present, the RHS defaults to 0. If LHS is not present,
			//the result is not present.
			(Some(x), Some(y)) => MetricsArrayEntry(Some(x.wrapping_sub(y))),
			(Some(x), None) => MetricsArrayEntry(Some(x)),
			(None, _) => MetricsArrayEntry(None),
		}
	}
}

impl Add for MetricsArrayEntry
{
	type Output = MetricsArrayEntry;
	fn add(self, rhs: MetricsArrayEntry) -> MetricsArrayEntry
	{
		match (self.0, rhs.0) {
			(Some(x), Some(y)) => MetricsArrayEntry(Some(x.wrapping_add(y))),
			(Some(x), None) => MetricsArrayEntry(Some(x)),
			(None, Some(x)) => MetricsArrayEntry(Some(x)),
			(None, None) => MetricsArrayEntry(None),
		}
	}
}

impl Display for MetricsArrayEntry
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		if let Some(val) = self.0 {
			write!(fmt, "{val}")
		} else {
			fmt.write_str("N/A")
		}
	}
}

pub struct MetricsMap(BTreeMap<u32, u64>);

impl MetricsMap
{
	pub fn get(&self, id: u32) -> Option<u64> { self.0.get(&id).map(|x|*x) }
	pub fn read(&self, id: u32) -> MetricsArrayEntry { MetricsArrayEntry(self.0.get(&id).map(|x|*x)) }
	pub fn set(&mut self, id: u32, amt: Option<u64>)
	{
		if let Some(amt) = amt { self.0.insert(id, amt); } else { self.0.remove(&id); }
	}
	pub fn swapzero(&mut self, id: u32) -> u64
	{
		let val = self.0.get(&id).map(|x|*x).unwrap_or(0);
		self.0.insert(id, 0);
		val
	}
}

pub fn receive_metrics_from(fd: &FileDescriptor) -> Option<(Duration, MetricsMap)>
{
	let mut buffer = [0u8;65536];
	let mut buffer2 = [0u8;4];
	//Receive first 4 bytes, as these contain the entry count.
	let mut len = 0;
	while len < 4 {
		match fd.read(&mut buffer2[len..]) {
			Ok(0) => return None,
			Ok(r) => len += r,
			Err(ref e) if e.is_transient() => continue,
			Err(_) => return None
		}
	}
	let tlen = 12 * u32::from_le_bytes(buffer2) as usize;
	len = 0;
	while len < tlen {
		match fd.read(&mut buffer[len..tlen]) {
			Ok(0) => return None,
			Ok(r) => len += r,
			Err(ref e) if e.is_transient() => continue,
			Err(_) => return None
		}
	}
	let buffer = &buffer[..tlen];
	//We are since 1970, right?
	let ts = SystemTime::now().duration_since(UNIX_EPOCH).unwrap();
	//Ok, got valid metrics.
	let mut arr = BTreeMap::new();
	for i in 0..buffer.len()/12 {
		let mut k = 0 as u32;
		let mut v = 0 as u64;
		for j in 0..4 { k |= (buffer[12*i+j] as u32) << 8*j; }
		for j in 0..8 { v |= (buffer[12*i+j+4] as u64) << 8*j; }
		arr.insert(k, v);
	}
	Some((ts, MetricsMap(arr)))
}

struct UmaskBackswapper(Umask);

impl Drop for UmaskBackswapper
{
	fn drop(&mut self) { self.0.swap(); }
}

//Really misleading name, this connects, does not bind.
pub fn bind_metrics_socket(name: Option<&str>) -> Result<FileDescriptor, String>
{
	let path = name.ok_or_else(||format!("No path to socket"))?.to_owned();
	let flags = SocketFlags::CLOEXEC;
	let fd = FileDescriptor::socket2(SocketFamily::UNIX, SocketType::STREAM, Default::default(), flags).
		map_err(|err|{
		format!("Unable to create socket: {err}")
	})?;
	let metrics_target = Address::Unix(SUnixPath::borrowed(path.as_bytes()));
	fd.connect(&metrics_target).map_err(|err|{
		format!("Unable to connect to socket: {err}")
	})?;
	Ok(fd)
}

#[derive(Copy,Clone)]
pub struct MicroSeconds(u64);

impl MicroSeconds
{
	pub fn new(m: u64) -> MicroSeconds { MicroSeconds(m) }
	pub fn as_us(&self) -> u64 { self.0 }
}

impl Display for MicroSeconds
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		write!(fmt, "{sec}.{subsec:06}", sec=self.0/1000000, subsec=self.0%1000000)
	}
}

pub struct PerThreadMetrics
{
	pub thread_num: u32,
	pub total_sleep: MicroSeconds,
	pub total_run: MicroSeconds,
	pub wakeups: u64,
	pub sleeping: bool,
	pub current_act: MicroSeconds,
	pub faults: u64,
	pub tasks: u64,
	pub dl_exceeded: u64,
	pub watchdog_ctr: u64,
}

impl PerThreadMetrics
{
	pub fn new(arr: &MetricsMap, tcount: u32) -> Option<PerThreadMetrics>
	{
		let base = 0x80000000 + tcount * 65536;
		fail_if_none!(!arr.0.contains_key(&base));
		let lsleep = arr.get(base + LMETRIC_LAST_SLEEP).unwrap_or(0);
		let lrun = arr.get(base + LMETRIC_LAST_RUN).unwrap_or(0);
		let tsleep = arr.get(base + LMETRIC_TOTAL_SLEEP).unwrap_or(0);
		let wakeups = arr.get(base + LMETRIC_WAKEUPS).unwrap_or(0);
		let trun = arr.get(base + LMETRIC_TOTAL_RUN).unwrap_or(0);
		let clock = arr.get(base + LMETRIC_TIME_NOW).unwrap_or(0);
		let faults = arr.get(base + LMETRIC_FAULTS).unwrap_or(0);
		let tasks = arr.get(base + LMETRIC_TASKS).unwrap_or(0);
		let dl_exceeded = arr.get(base + LMETRIC_DLE).unwrap_or(0);
		let watchdog_ctr = arr.get(base + LMETRIC_WATCHDOG).unwrap_or(0);
		let sleeping = lsleep > lrun;
		let csleep = if sleeping { clock - lsleep } else { 0 };
		let crun = if sleeping { 0 } else { clock - lrun };
		let cact = if sleeping { clock - lsleep } else { clock - lrun };
		let tsleep = tsleep + csleep;
		let trun = trun + crun;
		Some(PerThreadMetrics {
			thread_num: tcount,
			total_sleep: MicroSeconds(tsleep),
			total_run: MicroSeconds(trun),
			wakeups: wakeups,
			sleeping: sleeping,
			current_act: MicroSeconds(cact),
			faults: faults,
			tasks: tasks,
			dl_exceeded: dl_exceeded,
			watchdog_ctr: watchdog_ctr,
		})
	}
}

impl Display for PerThreadMetrics
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		write!(fmt, "Thread#{th}: S:{sleep}, R:{run}, T:{task} W:{wakeup} F:{fault} DLE:{dle} {state}{cur} \
			WD:{wd}",
			th=self.thread_num, sleep=self.total_sleep, run=self.total_run, task=self.tasks,
			wakeup=self.wakeups, fault=self.faults, dle=self.dl_exceeded, wd=self.watchdog_ctr,
			state=if self.sleeping { "S" } else { "R" }, cur=self.current_act)
	}
}

pub struct CpuUsage
{
	delta_run: u64,
	delta_time: u64,
}

impl Display for CpuUsage
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		if self.delta_time == 0 {
			fmt.write_str("N/A")
		} else {
			write!(fmt, "{ppm:.0}(={drun}/{dtime})",
				ppm=1e6*self.delta_run as f64/self.delta_time as f64, drun=self.delta_run,
				dtime=self.delta_time)
		}
	}
}

pub struct TrackCpuUsage
{
	history: [(u64,u64);61],
	history_slot: usize,
	firstsample: bool,
}

impl TrackCpuUsage
{
	pub fn new() -> TrackCpuUsage
	{
		TrackCpuUsage {
			history: [(0,0);61],
			history_slot: 0,
			firstsample: true,
		}
	}
	pub fn process(&mut self, total_run: u64, total_sleep: u64) -> CpuUsage
	{
		let total_time = total_run + total_sleep;
		if self.firstsample { for i in self.history.iter_mut() { *i = (total_run, total_time); } }
		self.firstsample = false;
		self.history[self.history_slot] = (total_run, total_time);
		self.history_slot = (self.history_slot + 1) % self.history.len();
		let next = self.history[self.history_slot];
		let delta_run = total_run.saturating_sub(next.0);
		let delta_time = total_time.saturating_sub(next.1);
		CpuUsage {
			delta_run: delta_run,
			delta_time: delta_time,
		}
	}
}

const LINE_LEN: usize = 79;

pub struct PrintUi
{
	lines: u64,
	label: String,
	line: String,
}

impl PrintUi
{
	fn __commit(&mut self)
	{
		//Do not do anything if there is no uncommitted stuff.
		if self.line.len() == 0 { return; }
		let line = replace(&mut self.line, String::new());
		println!("{line}\x1b[K");
		self.lines += 1;
	}
	pub fn new() -> PrintUi
	{
		println!("");
		PrintUi {
			lines: 0,
			line: String::new(),
			label: String::new(),
		}
	}
	pub fn label(&mut self, label: &str)
	{
		self.__commit();
		self.label = label.to_string();
	}
	pub fn single(&mut self, datum: Arguments)
	{
		let datum = datum.to_string();
		if self.line.len() > 0 && self.line.len() + 2 + datum.len() > LINE_LEN {
			//Break line.
			self.__commit();
		}
		if self.line.len() > 0 {
			write!(self.line, ", {datum}").ok();
		} else {
			write!(self.line, "{label}: {datum}", label=self.label).ok();
		}
	}
	pub fn rewind(&mut self)
	{
		self.__commit();
		print!("\x1b[{count}A", count=self.lines);
		self.lines = 0;
	}
	pub fn show(&mut self, args: Arguments)
	{
		self.__commit();
		println!("{args}\x1b[K");
		self.lines += 1;
	}
}
