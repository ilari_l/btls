#![warn(unsafe_op_in_unsafe_fn)]
extern crate btls_aux_collections;
#[macro_use] extern crate btls_aux_fail;
extern crate btls_aux_inotify;
extern crate btls_aux_ip;
extern crate btls_aux_memory;
extern crate btls_aux_random;
extern crate btls_aux_time;
extern crate btls_aux_unix;
extern crate btls_util_logging;
use btls_aux_collections::Arc;
use btls_aux_collections::GlobalMutex;
use btls_aux_collections::Mutex;
use btls_aux_ip::IpAddress;
use btls_aux_random::secure_random;
use btls_aux_unix::OsError;
use btls_aux_unix::RawSignalT;
use btls_aux_unix::Rlimit;
use btls_aux_unix::Signal;
use btls_aux_unix::SignalHandler;
use btls_aux_unix::SocketFlags;
use btls_aux_unix::Uid;
pub use btls_util_logging::ConnId;
use btls_util_logging::syslog;
pub use config::parse_host_etc_hosts;
pub use config::parse_port_etc_services;
pub use connectionmux::AddTokenCallback;
pub use connectionmux::is_fatal_error;
pub use connectionmux::UpperLayerConnection;
pub use connectionmux::UpperLayerConnectionFactory;
pub use connectionmux::KlineDb;
pub use connectionmux::Listener;
pub use connectionmux::ListenerLoop;
pub use connectionmux::ListenerLoopEvents;
pub use connectionmux::LocalThreadHandle;
pub use connectionmux::RemoteThreadHandle;
pub use connectionmux::SighupPipe;
pub use file_descriptor::decouple_thread_fd_count;
pub use file_descriptor::DowngradedAddresses;
pub use file_descriptor::ensure_stdfd_open;
pub use file_descriptor::escape_rstring;
pub use file_descriptor::FileDescriptor;
pub use file_descriptor::SocketAddrEx;
pub use file_descriptor::thread_local_fd_count;
pub use file_descriptor::unescape_rstring;
use btls_aux_unix::RawFdT;
pub use ipset::FrozenIpSet;
pub use ipset::IpSet;
pub use ipset::parse_netmask;
pub use metrics::*;
pub use io_linux::Event;
pub use io_linux::EventQueue;
pub use io_linux::FdPoller;
pub use io_linux::IO_WAIT_ERROR;
pub use io_linux::IO_WAIT_HUP;
pub use io_linux::IO_WAIT_ONESHOT;
pub use io_linux::IO_WAIT_READ;
pub use io_linux::IO_WAIT_WRITE;
pub use io_linux::Poll;
pub use io_linux::Poller;
pub use rcuhash::ArcTextString;
pub use rcuhash::cloned;
pub use rcuhash::RcuHashMap;
pub use service::call_with_nth_fallback;
pub use service::ConnectionInfo;
pub use service::ConnectionInfoAddress;
pub use service::StringInterner;
pub use service::translate_multiple_address;
pub use sigutils::catch_sighup_default;
pub use sigutils::catch_sigpipe_default;
pub use sigutils::do_mask_sighup;
pub use sigutils::do_sighup_close;
pub use sigutils::do_unshare_files;
pub use sigutils::get_sighup_flag;
pub use sigutils::gettid;
pub use sigutils::set_close_on_sighup;
pub use sigutils::setup_sighup_pipe;
pub use sigutils::ThreadReport;
use std::cmp::min;
use std::collections::BTreeMap;
use std::collections::HashMap;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::hash::Hash;
use std::hash::Hasher;
use std::io::Error as IoError;
use std::io::Result as IoResult;
use std::io::Write;
use std::marker::PhantomData;
use std::mem::size_of_val;
use std::mem::zeroed;
use std::net::IpAddr;
use std::net::Ipv4Addr;
use std::net::Ipv6Addr;
use std::net::ToSocketAddrs;
use std::ops::BitAnd;
use std::ops::BitOr;
use std::ops::Deref;
use std::ops::Div;
use std::ops::Mul;
use std::ops::Rem;
use std::ops::Shl;
use std::ops::Sub;
use std::panic::catch_unwind;
use std::path::Path;
use std::rc::Rc;
use std::str::FromStr;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering as MemOrder;
pub use threading::initialize_from_systemd;
pub use threading::parse_duration;
pub use threading::start_threads;
pub use threading::SystemdStartupInfo;
pub use threading::Thread;
pub use threading::ThreadLocal;
pub use threading::ThreadMetrics;
pub use threading::ThreadNewParams;
pub use threading::ThreadSpawnedParams;

pub const IO_WAIT_INTR: u8 = 32;

#[macro_export]
macro_rules! cow {
	($x:expr) => { ::std::borrow::Cow::Borrowed($x) };
	(F $x:expr) => {{
		use std::string::ToString;
		Cow::Owned(format_args!($x).to_string())
	}};
	($x:expr,$($p:tt)*) => {{
		//Do not assume format! works (format_args! is compiler builtin, so it always works).
		use std::fmt::Write;
		use std::borrow::Cow;
		use std::string::String;
		let mut x: String = String::new();
		write!(x, $x, $($p)*).ok();	//Can not fail.
		//The "cast" is necressary for type inference.
		Cow::Owned(x) as Cow<str>
	}};
}

#[macro_export] macro_rules! scow { ($($p:tt)*) => { Some(cow!($($p)*)) }; }

#[macro_export]
macro_rules! abort {
	($($p:tt)*) => {{
		use std::process::exit;
		syslog!(FATAL $($p)*);
		exit(1);
	}}
}


#[macro_export(local_inner_macros)]
macro_rules! store_err {
	($v:expr, $($p:tt)*) => { $v = scow!($($p)*) };
	(RET $v:expr, $($p:tt)*) => {{ store_err!($v, $($p)*); return; }};
}

mod config;
mod connectionmux;
mod file_descriptor;
mod ipset;
mod metrics;
mod io_linux;
mod rcuhash;
mod threading;
mod service;
mod sigutils;


pub fn decrement_count(c: &AtomicUsize) { c.fetch_sub(1, MemOrder::Relaxed); }
pub fn increment_count(c: &AtomicUsize) { c.fetch_add(1, MemOrder::Relaxed); }

pub fn get_max_files() -> usize
{
	//On failure, assume 1024 entry limit.
	Rlimit::NOFILE.get().map(|(cur,_)|cur).unwrap_or(1024) as usize
}

pub fn running_as_root() -> bool { Uid::get_real().is_super() || Uid::get_effective().is_super() }

pub fn get_option_noarg(arg: &str, pfx: &str) -> Option<()> { if arg == pfx { Some(()) } else { None } }

pub fn generate_launch_ack(fd: Option<i32>)
{
	if let Some(x) = fd {
		let mut fd_h = FileDescriptor::new(x);
		let _ = fd_h.write(&[0x31]);	//"1".
		//The ack fd is dropped to floor here, causing it to be closed.
	}
}

pub fn boolopt(x: bool) -> Option<()> { if x { Some(()) } else { None } }

#[derive(Debug)]
struct _AllocatedToken
{
	parent: TokenAllocatorPool,
	token: usize,
}

impl Drop for _AllocatedToken
{
	fn drop(&mut self) { self.parent.release(self.token); }
}

///Allocated token.
#[derive(Clone,Debug)]
pub struct AllocatedToken(Rc<_AllocatedToken>);

impl AllocatedToken
{
	pub fn value(&self) -> usize { self.0.deref().token }
	pub fn as_irq(&self) -> LocalIrq { LocalIrq(self.value(), PhantomData) }
}

#[derive(Copy,Clone,Debug)]
pub struct LocalIrq(usize, PhantomData<*mut u8>);

impl LocalIrq
{
	pub fn irq(&self)
	{
		ListenerLoop::fire_interrupt_thread_unsafe(self.0);
	}
}

#[derive(Clone)]
pub struct RemoteIrq(RemoteThreadHandle, usize);

impl RemoteIrq
{
	pub fn irq(&self)
	{
		ListenerLoop::fire_interrupt(&self.0, self.1);
	}
}


#[derive(Debug)]
struct _TokenAllocatorPool
{
	first: usize,
	last: usize,
	first_free: usize,
	free_ones: BTreeMap<usize, ()>,
}

impl _TokenAllocatorPool
{
	fn new(begin: usize, count: usize) -> _TokenAllocatorPool
	{
		_TokenAllocatorPool {
			first: begin,
			last: begin.saturating_add(count),
			first_free: begin,
			free_ones: BTreeMap::new(),
		}
	}
	fn allocate(&mut self) -> Result<usize, ()>
	{
		//First try allocating a free one.
		let first_free_one = self.free_ones.iter().next().map(|x|*x.0);
		if let Some(x) = first_free_one {
			self.free_ones.remove(&x);
			return Ok(x);
		}
		//No free ones. Try bumping first_free.
		if self.first_free < self.last {
			let nt = self.first_free;
			self.first_free += 1;
			return Ok(nt);
		}
		//No tokens remain.
		Err(())
	}
	fn release(&mut self, token: usize)
	{
		if token < self.first || token >= self.last { return; }
		//If we released token just below first_free, decrement first_free.
		if self.first_free + 1 == token {
			self.first_free -= 1;
			//Scan for free ones to decremen first_free with.
			loop {
				if self.first_free == self.first { break; }
				let candidate = self.first_free - 1;
				if self.free_ones.contains_key(&candidate) {
					self.free_ones.remove(&candidate);
					self.first_free = candidate;
				} else {
					break;	//No more to merge.
				}
			}
			//Ok.
		} else {
			//Push straight to free_ones.
			self.free_ones.insert(token, ());
		}
	}
}

///Token allocator pool.
///
///Allocates tokens out of specified range.
#[derive(Clone,Debug)]
pub struct TokenAllocatorPool(Arc<Mutex<_TokenAllocatorPool>>);

impl TokenAllocatorPool
{
	///Creates a new allocator pool controlling the specified range.
	///
	///# Parameters:
	///
	/// * `begin`: The first token number in range.
	/// * `count`: Number of tokens on range.
	///
	///# Returns:
	///
	/// * The new pool controlling the range `[begin,begin+count)`.
	pub fn new(begin: usize, count: usize) -> TokenAllocatorPool
	{
		TokenAllocatorPool(Arc::new(Mutex::new(_TokenAllocatorPool::new(begin, count))))
	}
	///Allocate a token.
	///
	///# Parameters:
	///
	/// * `self`: The allocator to allocate from.
	///
	///# Returns:
	///
	/// * The allocated token.
	///
	///# Failures:
	///
	/// * If there are no more tokens available, fails with `()`.
	pub fn allocate(&mut self) -> Result<AllocatedToken, ()>
	{
		let tnum = dtry!(self.0.lock().allocate());
		let _token = _AllocatedToken{
			parent: self.clone(),
			token: tnum
		};
		Ok(AllocatedToken(Rc::new(_token)))
	}
	fn release(&mut self, token: usize)
	{
		self.0.lock().release(token)
	}
}

pub trait CompareToZero: Sized+Copy
{
	fn is_negative(self) -> bool;
}

pub fn incompatible_with_certbot()
{
	//Check presence of Certbot. This daemon is INCOMPATIBLE (in potentially nasty ways) with it.
	if Path::new("/etc/letsencrypt").is_dir() { abort!("Found /etc/letsencrypt. This daemon is INCOMPATIBLE \
		with Certbot. Please uninstall Certbot."); }
}

pub fn incompatible_with_panic_abort()
{
	catch_unwind(||panic!("Testing if panicking is recoverable...")).ok();
	syslog!(INFO "Panics are recoverable, proceeding.");
}


macro_rules! impl_cmpz {
	($xtype:ident) => { impl CompareToZero for $xtype { fn is_negative(self) -> bool { self < 0 }} }
}

impl_cmpz!(i8); impl_cmpz!(i16); impl_cmpz!(i32); impl_cmpz!(i64); impl_cmpz!(isize);

//Negative to error.
pub fn libc_to_error<T:CompareToZero>(v: T) -> Result<T, IoError>
{
	if v.is_negative() {
		Err(IoError::last_os_error())
	} else {
		Ok(v)
	}
}

pub unsafe fn register_sigaction(sig: Signal, handler: unsafe extern fn(RawSignalT))
{
	unsafe{sig.set_handler(SignalHandler::Function(handler));}
}


pub struct FdConnection(FileDescriptor);

impl FdConnection
{
	pub fn local_addr_ex(&self) -> IoResult<SocketAddrEx> { self.0.local_addr_ex() }
	pub fn into_raw_fd(self) -> FileDescriptor { self.0 }
}

pub struct FdListener(FileDescriptor);

impl FdListener
{
	pub fn new_fd(fd: FileDescriptor) -> FdListener { FdListener(fd) }
	pub fn accept_ex(&self) -> IoResult<(FdConnection, SocketAddrEx)>
	{
		self.accept_ex2(Default::default())
	}
	pub fn accept_ex2(&self, flags: SocketFlags) -> IoResult<(FdConnection, SocketAddrEx)>
	{
		let (a, b) = self.0.accept_ex2(flags)?;
		Ok((FdConnection(a), b))
	}
	pub fn set_nonblock(&self) -> IoResult<()> { self.0.set_nonblock() }
	pub fn as_raw_fd(&self) -> RawFdT { self.0.as_raw_fd() }
	pub fn local_addr_ex(&self) -> IoResult<SocketAddrEx> { self.0.local_addr_ex() }
}

#[derive(Clone,Debug)]
pub enum ProxyConfigurationEntry
{
	V4(Ipv4Addr, u8),
	V6(Ipv6Addr, u8),
	Unix(String),
	Abstract(String),
}

impl ProxyConfigurationEntry
{
	fn from_ipv4(x: &str) -> Option<ProxyConfigurationEntry>
	{
		let slash = x.rfind("/");
		let (y, z) = if let Some(slash) = slash {
			let y = &x[..slash];
			let z = &x[slash+1..];
			(y, z)
		} else {
			(x, "32")
		};
		match (Ipv4Addr::from_str(y).ok(), u8::from_str(z).ok()) {
			(Some(x), Some(y)) => {
				fail_if_none!(y > 32);
				Some(ProxyConfigurationEntry::V4(x, y))
			},
			_ => None
		}
	}
	fn from_ipv6(x: &str) -> Option<ProxyConfigurationEntry>
	{
		let slash = x.rfind("/");
		let (y, z) = if let Some(slash) = slash {
			let y = &x[..slash];
			let z = &x[slash+1..];
			(y, z)
		} else {
			(x, "128")
		};
		match (Ipv6Addr::from_str(y).ok(), u8::from_str(z).ok()) {
			(Some(x), Some(y)) => {
				fail_if_none!(y > 128);
				Some(ProxyConfigurationEntry::V6(x, y))
			},
			_ => None
		}
	}
	fn from_unix(x: &str) -> Option<ProxyConfigurationEntry>
	{
		if let Some(x) = x.strip_prefix("@") {
			Some(ProxyConfigurationEntry::Abstract(x.to_owned()))
		} else if x.starts_with("/") {
			Some(ProxyConfigurationEntry::Unix(x.to_owned()))
		} else {
			None
		}
	}
	pub fn from(x: &str) -> ProxyConfigurationEntry
	{
		ProxyConfigurationEntry::from_ipv6(x).or_else(||ProxyConfigurationEntry::from_ipv4(x)).
			or_else(||ProxyConfigurationEntry::from_unix(x)).
			unwrap_or_else(||ProxyConfigurationEntry::Unix(x.to_owned()))
	}
	pub fn from_multi(x: &str) -> Vec<ProxyConfigurationEntry>
	{
		if let Some(a) = ProxyConfigurationEntry::from_ipv6(x).
			or_else(||ProxyConfigurationEntry::from_ipv4(x)).
			or_else(||ProxyConfigurationEntry::from_unix(x)) {
			return vec![a];
		}
		match (x, 1u16).to_socket_addrs() {
			Ok(addrs) => addrs.map(|addr|match addr.ip() {
				IpAddr::V6(a) => ProxyConfigurationEntry::V6(a, 128),
				IpAddr::V4(a) => ProxyConfigurationEntry::V4(a, 32),
			}).collect(),
			Err(_) => Vec::new(),
		}
	}
}

fn check_mask(a: &[u8], b: &[u8], masklen: usize) -> bool
{
	let masklen = min(masklen, 8 * min(a.len(), b.len()));
	let mut xmatch = true;
	for i in 0..masklen as usize {
		let sh = 7 - i%8;
		xmatch &= (a[i/8]>>sh)&1 == (b[i/8]>>sh)&1;
	}
	xmatch
}

pub fn proxy_entry_matches(entry: &ProxyConfigurationEntry, ci: &ConnectionInfo) -> bool
{
	match entry {
		//V4 entry matches corresponding V4 source entry.
		//V4 entry matches corresponding V6 source entry with v4 mapped address.
		&ProxyConfigurationEntry::V4(ref base, masklen) => {
			if let &ConnectionInfoAddress::V4(ref b, _) = &ci.src {
				check_mask(&base.octets(), b, masklen as usize)
			} else if let &ConnectionInfoAddress::V6(ref b, _) = &ci.src {
				let atmp = base.octets();
				let mut a = [0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,0];
				(&mut a[12..]).copy_from_slice(&atmp);
				check_mask(&a, b, masklen.saturating_add(96) as usize)
			} else {
				false
			}
		},
		//V6 entry matches corresponding V6 source entry.
		&ProxyConfigurationEntry::V6(ref base, masklen) => {
			if let &ConnectionInfoAddress::V6(ref b, _) = &ci.src {
				check_mask(&base.octets(), b, masklen as usize)
			} else {
				false
			}
		},
		//Unix entry only matches corresponding unix destination entry.
		&ProxyConfigurationEntry::Unix(ref a) => {
			if let &ConnectionInfoAddress::Unix(ref b) = &ci.orig_dest { a == b } else { false }
		},
		//Abstract entry only matches corresponding abstract destination entry.
		&ProxyConfigurationEntry::Abstract(ref a) => {
			if let &ConnectionInfoAddress::Abstract(ref b) = &ci.orig_dest { a == b } else { false }
		},
	}
}

#[derive(Clone,Debug)]
pub struct ProxyConfiguration
{
	allowed: Vec<ProxyConfigurationEntry>,
}

impl ProxyConfiguration
{
	pub fn new() -> ProxyConfiguration
	{
		ProxyConfiguration {
			allowed: Vec::new(),
		}
	}
	pub fn push(&mut self, entry: ProxyConfigurationEntry) { self.allowed.push(entry) }
	pub fn proxy_allowed(&self, ci: &ConnectionInfo) -> bool
	{
		self.allowed.iter().any(|entry|proxy_entry_matches(entry, ci))
	}
}

pub fn reverse_addr_for(mut x: IpAddr) -> String
{
	//Downgrade to IPv4 if possible. Note that to_ipv4() maps both 0000 and ffff (and 0000 is mapped in a
	//buggy way: ::1 maps to 0.0.0.1, which is definitely wrong), and we only want to map ffff.
	if let IpAddr::V6(y) = x {
		if &y.segments()[..6] == &[0,0,0,0,0,0xffff] { y.to_ipv4().map(|z|x=IpAddr::V4(z)); }
	}
	IpAddress::from(x).to_rdns().to_string()
}

#[test]
fn test_reverse_addr_for()
{
	assert_eq!(reverse_addr_for(IpAddr::from_str("::").unwrap()).deref(),
		"0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa");
	assert_eq!(reverse_addr_for(IpAddr::from_str("::1").unwrap()).deref(),
		"1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa");
	assert_eq!(reverse_addr_for(IpAddr::from_str("::fffe:ffff:ffff").unwrap()).deref(),
		"f.f.f.f.f.f.f.f.e.f.f.f.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa");
	assert_eq!(reverse_addr_for(IpAddr::from_str("::ffff:0:0").unwrap()).deref(),
		"0.0.0.0.in-addr.arpa");
	assert_eq!(reverse_addr_for(IpAddr::from_str("::ffff:89ab:cdef").unwrap()).deref(),
		"239.205.171.137.in-addr.arpa");
	assert_eq!(reverse_addr_for(IpAddr::from_str("::ffff:ffff:ffff").unwrap()).deref(),
		"255.255.255.255.in-addr.arpa");
	assert_eq!(reverse_addr_for(IpAddr::from_str("::1:0:0:0").unwrap()).deref(),
		"0.0.0.0.0.0.0.0.0.0.0.0.1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa");
	assert_eq!(reverse_addr_for(IpAddr::from_str("::1:ffff:0:0").unwrap()).deref(),
		"0.0.0.0.0.0.0.0.f.f.f.f.1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa");
	assert_eq!(reverse_addr_for(IpAddr::from_str("0.0.0.0").unwrap()).deref(),
		"0.0.0.0.in-addr.arpa");
	assert_eq!(reverse_addr_for(IpAddr::from_str("137.171.205.239").unwrap()).deref(),
		"239.205.171.137.in-addr.arpa");
	assert_eq!(reverse_addr_for(IpAddr::from_str("255.255.255.255").unwrap()).deref(),
		"255.255.255.255.in-addr.arpa");
}

//The underlying type must accept all bit patterns.
pub unsafe trait CastU8Arr: Sized
{
	fn as_array_mut<'a>(&'a mut self) -> &'a mut [u8];
	fn as_array<'a>(&'a self) -> &'a [u8];
}

unsafe impl CastU8Arr for [u8;8]
{
	fn as_array_mut<'a>(&'a mut self) -> &'a mut [u8] { &mut self[..] }
	fn as_array<'a>(&'a self) -> &'a [u8] { &self[..] }
}

unsafe impl CastU8Arr for [u8;4]
{
	fn as_array_mut<'a>(&'a mut self) -> &'a mut [u8] { &mut self[..] }
	fn as_array<'a>(&'a self) -> &'a [u8] { &self[..] }
}

unsafe impl CastU8Arr for [u8;2]
{
	fn as_array_mut<'a>(&'a mut self) -> &'a mut [u8] { &mut self[..] }
	fn as_array<'a>(&'a self) -> &'a [u8] { &self[..] }
}

unsafe impl CastU8Arr for [u8;1]
{
	fn as_array_mut<'a>(&'a mut self) -> &'a mut [u8] { &mut self[..] }
	fn as_array<'a>(&'a self) -> &'a [u8] { &self[..] }
}

pub trait RandomSampleable: Sized+Mul<Self, Output=Self>+Div<Self, Output=Self>+Sub<Self, Output=Self>+
	BitAnd<Self, Output=Self>+Rem<Self,Output=Self>+Shl<u32, Output=Self>+BitOr<Self, Output=Self>+
	PartialOrd<Self>+PartialEq<Self>+Copy
{
	type LArray: CastU8Arr;
	fn zero() -> Self;
	fn one() -> Self;
	fn max() -> Self;
	fn from_u8(v: u8) -> Self;
}

macro_rules! define_random_sampleable
{
	($xtype:ident,$xlen:expr,$max:expr) => {
		impl RandomSampleable for $xtype
		{
			type LArray = [u8;$xlen];
			fn zero() -> Self { 0 }
			fn one() -> Self { 1 }
			fn max() -> Self { $max }
			fn from_u8(v: u8) -> Self { v as $xtype }
		}
	}
}

define_random_sampleable!(u64, 8, std::u64::MAX);
#[cfg(target_pointer_width="64")]
define_random_sampleable!(usize, 8, std::usize::MAX);
#[cfg(target_pointer_width="32")]
define_random_sampleable!(usize, 4, std::usize::MAX);
define_random_sampleable!(u32, 4, std::u32::MAX);
define_random_sampleable!(u16, 2, std::u16::MAX);
define_random_sampleable!(u8, 1, std::u8::MAX);

fn os_error_to_io_error(e: OsError) -> IoError { IoError::from_raw_os_error(e.to_inner()) }

pub fn uniform_random<N:RandomSampleable>(n: N) -> N
{
	//If n is 0, return 0 (just not to crash).
	if n == N::zero() { return n; }
	//Use rejection sampling. If n is power of two, no samples are rejected, corresponding to limit of
	//2^64. However, this can not be represented by u64, so use special flag. Otherwise, the limit is 2^64-1
	//rounded down to nearest multiple of n.
	let rndmax = N::max() / n * n;
	let rndmaxen = (n & (n - N::one())) != N::zero();	//Not power of two.
	let mut rnd = N::zero();
	loop {
		let mut buf: <N as RandomSampleable>::LArray = unsafe{zeroed()};
		secure_random(buf.as_array_mut());
		//Do not do shifts if only 8-bit, as it would overflow.
		if size_of_val(&buf) > 1 {
			for i in buf.as_array().iter().cloned() { rnd = (rnd << 8) | N::from_u8(i); }
		} else {
			rnd = N::from_u8(buf.as_array()[0]);
		}
		if !rndmaxen || rnd < rndmax { break; }
	}
	rnd % n
}

#[derive(Debug)]
pub struct CyclicArray<T:Sized+Clone>
{
	item: AtomicUsize,
	table: Vec<T>,
}

impl<T:Sized+Clone> CyclicArray<T>
{
	pub fn new(contents: Vec<T>) -> CyclicArray<T>
	{
		CyclicArray {
			item: AtomicUsize::new(0),
			table: contents,
		}
	}
	pub fn get<'a,F>(&'a self, ok: F) -> Option<&'a T> where F: Fn(&T) -> bool
	{
		self.get_nf(ok).or_else(||{
			fail_if_none!(self.table.len() == 0);
			//None worked, return something.
			let item = self.item.fetch_add(1, MemOrder::AcqRel);
			Some(&self.table[item % self.table.len()])
		})
	}
	pub fn get_nf<'a,F>(&'a self, ok: F) -> Option<&'a T> where F: Fn(&T) -> bool
	{
		//Loop through each service, picking first one that works. This will not divide by zero, because
		//the loop will not execute in that case.
		for _ in 0..self.table.len() {
			let item = self.item.fetch_add(1, MemOrder::AcqRel);
			let s = &self.table[item % self.table.len()];
			if ok(s) { return Some(s); }	//If ok, return this.
		}
		None
	}
	pub fn index<'a>(&'a self, index: usize) -> Option<&'a T> { self.table.get(index) }
	pub fn is_empty(&self) -> bool { self.table.is_empty() }
	pub fn iter<'a>(&'a self) -> ::std::slice::Iter<'a, T> { self.table.iter() }
	pub fn count(&self) -> usize { self.table.len() }
}

#[derive(Debug)]
struct AddressHandleEntry
{
	address: SocketAddrEx,
	refs: AtomicUsize,
	status: AtomicBool,
}

#[derive(Clone,Default)]
struct AddressHandleCache
{
	backward: HashMap<SocketAddrEx, Arc<AddressHandleEntry>>,
}

static ADDRESS_CACHE: GlobalMutex<AddressHandleCache> = GlobalMutex::new();

#[derive(Debug)]
pub struct AddressHandle(Option<Arc<AddressHandleEntry>>);

impl AddressHandle
{
	///Create a new dummy handle.
	pub fn dummy() -> AddressHandle { AddressHandle(None) }
	///Create a new address handle for specified address.
	pub fn new(addr: &SocketAddrEx) -> AddressHandle
	{
		ADDRESS_CACHE.with(|cache_lock|{
			//If the entry already exists, grab a reference. We have the lock, so this can not be freed.
			if let Some(contents) = cache_lock.backward.get(addr) {
				contents.deref().refs.fetch_add(1, MemOrder::AcqRel);
				return AddressHandle(Some(contents.clone()));
			}
			//No existing entry, create a new one.
			let ahandle = Arc::new(AddressHandleEntry {
				address: addr.clone(),
				refs: AtomicUsize::new(1),
				status: AtomicBool::new(true),
			});
			cache_lock.backward.insert(addr.clone(), ahandle.clone());
			AddressHandle(Some(ahandle))
		})
	}
	///Set status of address handle.
	pub fn set_status(&self, nstatus: bool)
	{
		if let Some(inner) = self.0.as_ref() {
			let ostatus = inner.status.swap(nstatus, MemOrder::AcqRel);
			if !ostatus && nstatus {
				syslog!(NOTICE "Marking '{addr}' as good", addr=inner.address);
			} else if ostatus && !nstatus {
				syslog!(WARNING "Marking '{addr}' as bad", addr=inner.address);
			}
		}
	}
	///print address.
	pub fn print_addr(&self, to: &mut Formatter) -> Result<(), FmtError>
	{
		if let Some(inner) = self.0.as_ref() {
			Display::fmt(&inner.address, to)
		} else {
			Display::fmt("<Unknown>", to)
		}
	}
	///Get status of address handle.
	pub fn get_status(&self) -> bool
	{
		if let Some(inner) = self.0.as_ref() {
			inner.deref().status.load(MemOrder::Acquire)
		} else {
			//Nonexistent handle 0 is always good.
			true
		}
	}
	fn __get(&self)
	{
		//Don't do anything with the nonexistent handle 0.
		if let Some(inner) = self.0.as_ref() {
			//This object pins this reference, so reference count is always guaranteed to be >0.
			inner.deref().refs.fetch_add(1, MemOrder::AcqRel);
		}
	}
	fn __put(&self)
	{
		//Don't do anything with the nonexistent handle 0.
		if let Some(inner) = self.0.as_ref() {
			let refs = inner.deref().refs.fetch_sub(1, MemOrder::AcqRel);
			//If references still remain, do not free. This is pre-decrement, so 1 for unique
			//reference.
			if refs > 1 { return; }
			ADDRESS_CACHE.with(|cache_lock|{
				//Check the refecount again. If not 0, then somebody has racily got a reference,
				//and we should not free it. The only way to racily obtain a reference is by
				//creating a new handle to address that is being freed. In that case, the creation
				//must have occured before obtaining cache_lock. So the synchronization is
				//sufficient to ensure it will be destroyed once all references are gone.
				if inner.deref().refs.load(MemOrder::Acquire) > 0 { return; }
				//Free the actual entry, from both forward and backward map.
				cache_lock.backward.remove(&inner.deref().address);
			});
		}
	}
}

impl Clone for AddressHandle
{
	fn clone(&self) -> AddressHandle
	{
		self.__get();
		AddressHandle(self.0.clone())
	}
}

impl Drop for AddressHandle
{
	fn drop(&mut self) { self.__put(); }
}

#[derive(Clone,Debug)]
pub struct SocketAddrExHandle(SocketAddrEx, AddressHandle);

impl SocketAddrExHandle
{
	pub fn new(socket: SocketAddrEx) -> SocketAddrExHandle
	{
		let ah = AddressHandle::new(&socket);
		SocketAddrExHandle(socket, ah)
	}
	pub fn status_ok(&self) -> bool { self.1.get_status() }
	pub fn borrow_addr(&self) -> &SocketAddrEx { &self.0 }
	pub fn borrow_status(&self) -> &AddressHandle { &self.1 }
}

impl Display for SocketAddrExHandle
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError> { Display::fmt(&self.0, fmt) }
}

//The SocketAddrExHandle are compared specially: Equivalence ignores the address handle.
impl PartialEq for SocketAddrExHandle
{
	fn eq(&self, other: &SocketAddrExHandle) -> bool { self.0.eq(&other.0) }
}

impl Hash for SocketAddrExHandle
{
	fn hash<H:Hasher>(&self, hasher: &mut H) { self.0.hash(hasher) }
}

impl Eq for SocketAddrExHandle {}

#[test]
fn test_cow()
{
	use std::borrow::Cow;
	let z = 42;
	let mut _y = None;
	let _x = cow!("foo");
	let _x = cow!("foo {}", "bar");
	let _x = cow!("foo {x}", x="bar");
	assert_eq!(&_x, "foo bar");
	let _x = cow!(F "foo {z}");
	assert_eq!(&_x, "foo 42");
	let _x = scow!("foo");
	let _x = scow!("foo {}", "bar");
	let _x = scow!("foo {x}", x="bar");
	store_err!(_y, "foo");
	store_err!(_y, "foo {}", "bar");
	store_err!(_y, "foo {x}", x="bar");
	store_err!(_y, F "foo {z}");
	if false { store_err!(RET _y, "foo"); }
	if false { store_err!(RET _y, "foo {}", "bar"); }
	if false { store_err!(RET _y, "foo {x}", x="bar"); }
	if false { store_err!(RET _y, F "foo {z}"); }
	if false { abort!("foo"); }
	if false { abort!("foo {}", "bar"); }
	if false { abort!("foo {x}", x="bar"); }
	if false { abort!("foo {z}"); }
}
