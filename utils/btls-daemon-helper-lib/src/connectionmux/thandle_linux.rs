use ::FileDescriptor;
use btls_aux_collections::GlobalMutex;
use btls_aux_collections::Mutex;
use btls_aux_time::PointInTime;
use btls_aux_unix::FileDescriptor as UnixFileDescriptor;
use btls_aux_unix::Pid;
use btls_aux_unix::Signal;
use btls_aux_unix::SignalFdFlags;
use btls_aux_unix::SignalSet;
use btls_util_logging::syslog;
use std::borrow::Cow;
use std::collections::BTreeMap;
use std::marker::PhantomData;
use std::ops::Deref;
use std::sync::Arc;


#[derive(Clone)]
pub(crate) struct LocalThreadHandleGlobal(FileDescriptor);

impl LocalThreadHandleGlobal
{
	pub(crate) fn new() -> Result<LocalThreadHandleGlobal, Cow<'static, str>>
	{
		let mut set = SignalSet::new();
		set.add(Signal::URG);
		set.block_signals().map_err(|err|{
			format!("Failed to block signals: {err}")
		})?;
		let fd = UnixFileDescriptor::signalfd(set, SignalFdFlags::NONBLOCK|SignalFdFlags::CLOEXEC).
			map_err(|err|{
			format!("Failed to create sigfd: {err}")
		})?;
		Ok(LocalThreadHandleGlobal(FileDescriptor::__new(fd)))
	}
}

#[derive(Clone)]
pub struct LocalThreadHandle
{
	sigfd: FileDescriptor,
	target: Option<(Pid, Pid)>,
	queue: Arc<Mutex<Vec<u64>>>,
	marker: PhantomData<*const u8>
}

impl LocalThreadHandle
{
	pub(crate) fn new(glth: LocalThreadHandleGlobal) -> LocalThreadHandle
	{
		LocalThreadHandle {
			sigfd: glth.0,
			target: Some((Pid::current(), Pid::current_thread())),
			queue: Arc::new(Mutex::new(Vec::new())),
			marker: PhantomData,
		}
	}
	pub(super) fn wants_monitor(&self) -> FileDescriptor
	{
		self.sigfd.clone()
	}
	pub fn to_remote(&self) -> RemoteThreadHandle
	{
		RemoteThreadHandle {
			target: self.target,
			queue: self.queue.clone(),
		}
	}
	pub(super) fn handle_irq_event(l: &mut super::ListenerLoop, kind: u8, now: PointInTime)
	{
		//Read event from l.lth.sigfd.
		if let Some(fd) = l.lth.sigfd.as_unix_fd() {
			//Read and ignore siginfo records until there are no more to read. This is guaranteed to
			//process events from the right thread, as threads don't see thread-local signals to
			//other threads. And reading the signal also clears it.
			let us = l.lth.target.map(|(pid,_)|pid.to_inner()).unwrap_or(0);
			let mut send_paranoid_wake = false;
			while unsafe{fd.read_siginfo(|info|{
				send_paranoid_wake |= info.deref().ssi_pid as u32 != us as u32;
			}).is_ok()} {}
			//As paranoia, if the signal was not sent by us, send broadcast wake to guarantee no
			//wakeup was missed.
			if send_paranoid_wake {
				syslog!(WARNING "Received wakeup outside process, waking up all workers");
				LocalThreadHandle::for_each_remote_handle(|r|{
					if let Some((pid, tid)) = r.target {
						pid.tgkill(tid, Signal::URG).ok();
					}
				});
			}
		}
		//Loop over events. For avoiding deadlock, queue must be emptied in one go. However, the lock
		//does not need to be held during calls. If someone racily adds entries in the meantime, we will
		//get to those later, without going into sleep in the meantime.
		loop {
			match {
				let mut guard = l.lth.queue.lock();
				if guard.len() > 0 { Some(guard.swap_remove(0)) } else { None }
			} {
				Some(ev) => l._handle_event(ev as usize, kind, now),
				None => break	//Have processed all events.
			}
		}
	}
	fn get_key(&self) -> Option<(Pid, Pid)> { self.target }
}

impl_lth_list!(LocalThreadHandle);

static REMOTE_HANDLE_LIST: GlobalMutex<BTreeMap<(Pid, Pid), RemoteThreadHandle>> = GlobalMutex::new();

#[derive(Clone)]
pub struct RemoteThreadHandle
{
	target: Option<(Pid, Pid)>,
	queue: Arc<Mutex<Vec<u64>>>,
}

impl RemoteThreadHandle
{
	pub fn dummy() -> RemoteThreadHandle
	{
		RemoteThreadHandle {
			target: None,
			queue: Arc::new(Mutex::new(Vec::new())),
		}
	}
	pub(super) fn fire_interrupt(&self, task: usize)
	{
		//If target is dummy, discard events.
		if self.target.is_none() { return; }
		let mut need_interrupt = false;
		{
			let mut guard = self.queue.lock();
			//If the queue is empty, need interrupt to wake the target. If the target is already
			//awake, and its queue is not empty, then it is going to look at its queue anyway.
			if guard.is_empty() { need_interrupt = true; }
			guard.push(task as u64);
		}
		if need_interrupt {
			//Raise signal to wake the target.
			if let Some((pid, tid)) = self.target { pid.tgkill(tid, Signal::URG).ok(); }
		}
	}
}
