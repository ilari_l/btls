use btls_aux_collections::Arc;
use btls_aux_collections::RwLock;
use std::borrow::Borrow;
use std::collections::HashMap;
use std::fmt::Debug;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::hash::Hash;
use std::hash::Hasher;
use std::ops::Deref;

pub trait Cloneable
{
	type Target: Sized;
	fn do_clone(self) -> <Self as Cloneable>::Target;
}

pub fn cloned<Arg:Cloneable>(arg: Arg) -> <Arg as Cloneable>::Target { arg.do_clone() }

impl<'a,T:Sized+Clone> Cloneable for &'a T
{
	type Target = T;
	fn do_clone(self) -> T { self.clone() }
}

impl<'a,T:Sized+Clone> Cloneable for Option<&'a T>
{
	type Target = Option<T>;
	fn do_clone(self) -> Option<T> { self.map(|x|x.clone()) }
}

#[derive(Clone,PartialEq,PartialOrd,Eq,Ord)]
pub struct ArcTextString(Arc<String>);

impl ArcTextString
{
	pub fn new(x: Arc<String>) -> ArcTextString { ArcTextString(x) }
	pub fn as_arc(&self) -> Arc<String> { self.0.clone() }
	pub fn as_arc_ref(&self) -> &Arc<String> { &self.0 }
	pub fn as_str(&self) -> &str { self.0.deref().deref() }
}

impl Debug for ArcTextString
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		Display::fmt(self.deref(), fmt)
	}
}

impl Display for ArcTextString
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		Display::fmt(self.deref(), fmt)
	}
}

impl<'a> From<&'a str> for ArcTextString
{
	fn from(x: &'a str) -> ArcTextString { ArcTextString(Arc::new(x.to_owned())) }
}

impl<'a> From<&'a String> for ArcTextString
{
	fn from(x: &'a String) -> ArcTextString { ArcTextString(Arc::new(x.clone())) }
}

impl From<Arc<String>> for ArcTextString
{
	fn from(x: Arc<String>) -> ArcTextString { ArcTextString(x) }
}

impl Deref for ArcTextString
{
	type Target = str;
	fn deref(&self) -> &str { self.0.deref().deref() }
}

impl Borrow<str> for ArcTextString
{
	fn borrow(&self) -> &str { self.deref() }
}

impl Hash for ArcTextString
{
	fn hash<H:Hasher>(&self, state: &mut H) { self.0.deref().deref().hash(state); }
}

#[derive(Debug)]
pub struct RcuHashMap<K:Hash+Eq+Send,T:Send>(Arc<RwLock<HashMap<K, Arc<T>>>>);

impl<K:Hash+Eq+Send,T:Send> Clone for RcuHashMap<K,T>
{
	fn clone(&self) -> Self { RcuHashMap(self.0.clone()) }
}

impl<K:Hash+Eq+Send,T:Send> RcuHashMap<K,T>
{
	pub fn new() -> RcuHashMap<K,T>
	{
		RcuHashMap(Arc::new(RwLock::new(HashMap::new())))
	}
	pub fn put_arc(&self, key: K, val: Arc<T>)
	{
		self.0.with_write(|x|x.insert(key, val));
	}
	pub fn delete<Q: ?Sized>(&self, key: &Q) where K: Borrow<Q>, Q: Hash + Eq
	{
		self.0.with_write(|x|x.remove(key));
	}
	pub fn contains<Q:?Sized>(&self, key: &Q) -> bool where K: Borrow<Q>, Q: Hash + Eq
	{
		self.0.with_read(|x|x.contains_key(key))
	}
	pub fn get<Q:?Sized>(&self, key: &Q) -> Option<Arc<T>> where K: Borrow<Q>, Q: Hash + Eq
	{
		self.0.with_read(|x|x.get(key).map(cloned))
	}
	pub fn get_with<Q:?Sized,R:Sized,F:FnOnce(&T) -> R>(&self, key: &Q, f: F) -> Option<R>
		where K: Borrow<Q>, Q: Hash + Eq
	{
		self.0.with_read(|x|x.get(key).map(|y|f(y)))
	}
	pub fn len(&self) -> usize
	{
		self.0.with_read(|x|x.len())
	}
}
