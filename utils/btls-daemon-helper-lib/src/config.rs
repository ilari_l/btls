use btls_aux_memory::Attachment;
use btls_aux_memory::ByteStringLines;
use btls_aux_memory::split_attach_first;
use std::fs::File;
use std::io::Error as IoError;
use std::io::ErrorKind as IoErrorKind;
use std::io::Read as IoRead;
use std::net::IpAddr;
use std::net::Ipv4Addr;
use std::net::Ipv6Addr;
use std::path::Path;
use std::str::from_utf8;
use std::str::FromStr;


fn equals_case_insensitive(a: &[u8], b: &[u8]) -> bool
{
	if a.len() != b.len() { return false; }
	for (c, d) in a.iter().cloned().zip(b.iter().cloned()) {
		let c = if c.wrapping_sub(65) < 26 { c | 32 } else { c };
		let d = if d.wrapping_sub(65) < 26 { d | 32 } else { d };
		if c != d { return false; }
	}
	true
}

struct IterateWords<'a>(&'a [u8]);

impl<'a> Iterator for IterateWords<'a>
{
	type Item = &'a [u8];
	fn next(&mut self) -> Option<&'a [u8]>
	{
		let mut s = 0;
		let v = self.0;
		while s < v.len() && (v[s] == 9 || v[s] == 32) { s += 1; }
		fail_if_none!(s == v.len());
		let mut e = s;
		while e < v.len() && v[e] != 9 && v[e] != 32 { e += 1; }
		self.0 = &v[e..];
		Some(&v[s..e])
	}
}

fn parse_host_etc_hosts_callback<F>(host: &str, filename: &Path, mut cb: F) -> Result<(), IoError>
	where F: FnMut(IpAddr) -> bool
{
	let mut content = Vec::new();
	//If /etc/hosts does not exist, parse empty file instead.
	if let Err(err) = File::open(filename).and_then(|mut fp|fp.read_to_end(&mut content)) {
		fail_if!(err.kind() != IoErrorKind::NotFound, err);
	}
	for linec in ByteStringLines::new(&content) {
		//Strip possible comment.
		let linec = split_attach_first(linec, b"#", Attachment::Right).map(|(t,_)|t).unwrap_or_else(|t|t);
		let mut iter = IterateWords(linec);
		//The first word is address, it has to be valid UTF-8. Otherwise skip record.
		let addr = f_continue!(iter.next());
		let addr = f_continue!(from_utf8(addr));
		//The remaining words are names. Check any match.
		if !iter.any(|entry|equals_case_insensitive(entry, host.as_bytes())) { continue; }
		//If matches, parse address.
		if let Ok(x) = Ipv6Addr::from_str(addr) {
			if !cb(IpAddr::V6(x)) { return Ok(()); }
		} else if let Ok(x) = Ipv4Addr::from_str(addr) {
			if !cb(IpAddr::V4(x)) { return Ok(()); }
		} else {
			//Ignore unknown address.
		}
	}
	Ok(())
}

pub fn parse_host_etc_hosts(host: &str, shosts: Option<&Path>) -> Result<Vec<IpAddr>, IoError>
{
	let mut out = Vec::new();
	parse_host_etc_hosts_callback(host, Path::new("/etc/hosts"), |a|{out.push(a); true})?;
	//If the shosts is set, parse that too.
	if let Some(shosts) = shosts {
		parse_host_etc_hosts_callback(host, shosts, |a|{out.push(a); true})?;
	}
	Ok(out)
}

pub fn parse_port_etc_services(port: &str) -> Result<u16, IoError>
{
	//If port is numeric, parse it.
	if let Ok(port) = u16::from_str(port) { return Ok(port); }
	let mut content = Vec::new();
	//If /etc/services does not exist, parse empty file instead.
	if let Err(err) = File::open("/etc/services").and_then(|mut fp|fp.read_to_end(&mut content)) {
		fail_if!(err.kind() != IoErrorKind::NotFound, err);
	}
	for linec in ByteStringLines::new(&content) {
		//Strip possible comment.
		let linec = split_attach_first(linec, b"#", Attachment::Right).map(|(t,_)|t).unwrap_or_else(|t|t);
		let mut iter = IterateWords(linec);
		//The first part is primary name, and second is port number. The port number has to be valid UTF-8.
		let primary = f_continue!(iter.next());
		let xport = f_continue!(iter.next());
		let xport = f_continue!(from_utf8(xport));
		let xport = f_continue!(xport.strip_suffix("/tcp"));	//TCP only.
		let xport = f_continue!(u16::from_str(xport));		//Parse numeric.
		//Primary matches?
		if equals_case_insensitive(primary, port.as_bytes()) { return Ok(xport); }
		//Some secondary matches?
		if iter.any(|entry|equals_case_insensitive(entry, port.as_bytes())) { return Ok(xport); }
	}
	fail!(IoError::new(IoErrorKind::NotFound, format!("No service named {port} found")));
}

#[test]
fn test_parse_port()
{
	assert_eq!(parse_port_etc_services("ssh").expect("Should find ssh"), 22);
	assert_eq!(parse_port_etc_services("www").expect("Should find www"), 80);
	assert_eq!(parse_port_etc_services("untp").expect("Should find untp"), 119);
}

#[test]
fn test_parse_host()
{
	assert_eq!(parse_host_etc_hosts("ip6-localhost", None).expect("ip6-localhost should work"),
		vec![IpAddr::V6(Ipv6Addr::from_str("::1").unwrap())]);
	assert_eq!(parse_host_etc_hosts("localhost", None).expect("localhost should work"),
		vec![
			IpAddr::V4(Ipv4Addr::from_str("127.0.0.1").unwrap()),
			IpAddr::V6(Ipv6Addr::from_str("::1").unwrap())
		]);
}
