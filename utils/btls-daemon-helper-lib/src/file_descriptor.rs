use ::decrement_count;
use ::increment_count;
use ::os_error_to_io_error;
use btls_aux_unix::Address as UnixAddress;
use btls_aux_unix::AddressIpv4;
use btls_aux_unix::AddressIpv6;
use btls_aux_unix::file_descriptors_ok;
use btls_aux_unix::FileDescriptor as UnixFileDescriptor;
use btls_aux_unix::FileDescriptorB as UnixFileDescriptorB;
use btls_aux_unix::OpenFlags;
use btls_aux_unix::Path as UnixPath;
use btls_aux_unix::PipeFlags;
use btls_aux_unix::RawFdT;
use btls_aux_unix::SendRecvFlags;
use btls_aux_unix::ShutdownFlags;
use btls_aux_unix::SocketFamily;
use btls_aux_unix::SocketFlags;
use btls_aux_unix::SocketProtocol;
use btls_aux_unix::SocketType;
use btls_aux_unix::SpecialFd;
use btls_aux_unix::UnixPath as SUnixPath;
use std::cell::RefCell;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::io::Error as IoError;
use std::io::ErrorKind as IoErrorKind;
use std::io::Read as IoRead;
use std::io::Result as IoResult;
use std::io::Write as IoWrite;
use std::mem::MaybeUninit;
use std::mem::transmute;
use std::net::SocketAddrV4;
use std::net::SocketAddrV6;
use std::net::Ipv4Addr;
use std::net::Ipv6Addr;
use std::ops::Deref;
use std::rc::Rc;
use std::sync::Arc;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering;

#[derive(Debug)]
struct OneDescriptorCount(());

impl Drop for OneDescriptorCount
{
	fn drop(&mut self) { decrement_count(thread_local_fd_count()); }
}

impl OneDescriptorCount
{
	fn new() -> OneDescriptorCount { increment_count(thread_local_fd_count()); OneDescriptorCount(()) }
}

macro_rules! dummy_fd_error
{
	($action:expr) => {{
		let msg = format!("Attempted to {act} on dummy fd", act=$action);
		return Err(IoError::new(IoErrorKind::InvalidInput, msg));
	}}
}

fn append_escape(y: &mut String, z: u8)
{
	const HEXES: [char;16] = ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'];
	y.push('%');
	y.push(HEXES[(z>>4) as usize]);
	y.push(HEXES[(z&15) as usize]);
}

fn do_bad_buffer(y: &mut String, buffer: &mut Vec<u8>)
{
	for d in buffer.iter().cloned() { append_escape(y, d); }
	buffer.clear();
}

pub fn escape_rstring(x: &[u8]) -> String
{
	let mut y = String::new();
	let mut b = Vec::new();
	for c in x.iter().cloned() {
		b.push(c);
		let mut bad = false;
		bad |= b.len() == 1 && b[0] < 0x20;				//Controls.
		bad |= b.len() == 1 && b[0] == 0x25;				//%
		bad |= b.len() == 1 && b[0] >= 0x7F && b[0] <= 0xC1;		//DEL, <bad> start.
		bad |= b.len() == 1 && b[0] > 0xF4;				//<bad> start.
		bad |= b.len() > 1 && b[b.len()-1] & 0xC0 != 0x80;		//<bad> continue.
		//0xC2 0x80-0x9F is not valid (controls).
		bad |= b.len() == 2 && b[0] == 0xC2 && b[1] & 0xE0 == 0x80;
		//0xE0 0x80-0x9F is not valid (overlong).
		bad |= b.len() == 2 && b[0] == 0xE0 && b[1] & 0xE0 == 0x80;
		//0xED 0xA0-0xBF is not valid (surrogates).
		bad |= b.len() == 2 && b[0] == 0xED && b[1] & 0xE0 == 0xA0;
		//0xEF 0xB7 0x90-0xAF is not valid (noncharacters).
		bad |= b.len() == 3 && b[0] == 0xEF && b[1] == 0xB7 && b[2] >= 0x90 && b[2] <= 0xAF;
		//0xEF 0xBF 0xBE-0xBF is not valid (noncharacters).
		bad |= b.len() == 3 && b[0] == 0xEF && b[1] == 0xBF && b[2] >= 0xBE;
		//0xF0 0x80-0x8F is not valid (overlong).
		bad |= b.len() == 2 && b[0] == 0xF0 && b[1] & 0xF0 == 0x80;
		//0xF4 0x90-0xBF is not valid (overlong).
		bad |= b.len() == 2 && b[0] == 0xF4 && b[1] & 0xF0 != 0x80;
		//0xFx 0xxF 0xBF 0xBE-0xBF is not valid (overlong).
		bad |= b.len() == 4 && b[0] & 0xF0 == 0xF0 && b[1] & 0x0F == 0x0F && b[2] == 0xBF && b[3] >= 0xBE;
		if bad {
			do_bad_buffer(&mut y, &mut b);
			continue;
		}
		if b.len() == 1 && b[0] & 0x80 == 0 {
			let z = b[0] as u32;
			y.push(char::from_u32(z).unwrap_or('\u{fffd}'));
			b.clear();
		} else if b.len() == 2 && b[0] & 0xE0 == 0xC0 {
			let z = (b[0] as u32 & 0x1F) * 64 + (b[1] as u32 & 0x3F);
			y.push(char::from_u32(z).unwrap_or('\u{fffd}'));
			b.clear();
		} else if b.len() == 3 && b[0] & 0xF0 == 0xE0 {
			let z = (b[0] as u32 & 0x1F) * 4096 + (b[1] as u32 & 0x3F) * 64 +
				(b[2] as u32 & 0x3F);
			y.push(char::from_u32(z).unwrap_or('\u{fffd}'));
			b.clear();
		} else if b.len() == 4 && b[0] & 0xF8 == 0xF0 {
			let z = (b[0] as u32 & 0x1F) * 262144 + (b[1] as u32 & 0x3F) * 4096 +
				(b[2] as u32 & 0x3F) * 64 + (b[3] as u32 & 0x3F);
			y.push(char::from_u32(z).unwrap_or('\u{fffd}'));
			b.clear();
		} else {	//???
			do_bad_buffer(&mut y, &mut b);
			continue;
		}
	}
	if b.len() > 0 { do_bad_buffer(&mut y, &mut b); }
	y
}

fn unhex(x: u8) -> Option<u8>
{
	match x {
		48..=57 => Some(x - 48),
		65..=70 => Some(x - 55),
		97..=102 => Some(x - 87),
		_ => None
	}
}

pub fn unescape_rstring(x: &str) -> Vec<u8>
{
	let mut b = Vec::new();
	let mut e = 0;
	let mut f = 0;
	for c in x.as_bytes().iter().cloned() {
		if e == 0 {
			if c == 37 { e = 1; } else { b.push(c); }
		} else if e == 1 {
			f = unhex(c).unwrap_or(0);
			e = 2;
		} else if e == 1 {
			b.push(f * 16 + unhex(c).unwrap_or(0));
			e = 0;
		}
	}
	b
}

#[derive(Clone,Debug,Hash,PartialEq,Eq)]
pub enum SocketAddrEx
{
	V4(SocketAddrV4),
	V6(SocketAddrV6),
	Unix(Vec<u8>),
	Abstract(Vec<u8>),
	Symbolic(String),
	Unknown,
}

impl Display for SocketAddrEx
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&SocketAddrEx::V4(addr) => {
				if &addr.ip().octets() == &[0;4] && addr.port() == 0 {
					return fmt.write_str("0.0.0.0");
				}
				write!(fmt, "{addr}")
			},
			&SocketAddrEx::V6(addr) => {
				if &addr.ip().octets() == &[0;16] && addr.port() == 0 {
					return fmt.write_str("::");
				}
				write!(fmt, "{addr}")
			},
			&SocketAddrEx::Unix(ref addr) => {
				if addr.len() == 0 { return fmt.write_str("unix"); }
				fmt.write_str(&escape_rstring(addr))
			},
			&SocketAddrEx::Abstract(ref addr) => {
				if addr.len() == 0 { return fmt.write_str("abstract"); }
				write!(fmt, "@{addr}", addr=&escape_rstring(addr))
			},
			&SocketAddrEx::Symbolic(ref addr) => Display::fmt(addr, fmt),
			&SocketAddrEx::Unknown => fmt.write_str("unknown"),
		}
	}
}

const IPV6_IPV4_MAP_PFX: [u8;12] = [0,0,0,0,0,0,0,0,0,0,255,255];

pub struct DowngradedAddresses(Vec<[u8;12]>);

impl DowngradedAddresses
{
	pub fn new() -> DowngradedAddresses { DowngradedAddresses(Vec::new()) }
	pub fn from_list(list: &[[u8;12]]) -> DowngradedAddresses
	{
		let mut t = list.to_vec();
		t.sort();
		DowngradedAddresses(t)
	}
	fn as_inner(&self) -> &[[u8;12]] { self.0.deref() }
}


impl SocketAddrEx
{
	pub fn is_ip(&self) -> bool
	{
		match self {
			&SocketAddrEx::V4(_) => true,
			&SocketAddrEx::V6(_) => true,
			&SocketAddrEx::Unix(_) => false,
			&SocketAddrEx::Abstract(_) => false,
			&SocketAddrEx::Symbolic(_) => false,
			&SocketAddrEx::Unknown => false,
		}
	}
	pub fn get_family(&self) -> Option<SocketFamily>
	{
		Some(match self {
			&SocketAddrEx::V4(_) => SocketFamily::INET,
			&SocketAddrEx::V6(_) => SocketFamily::INET6,
			&SocketAddrEx::Unix(_) => SocketFamily::UNIX,
			&SocketAddrEx::Abstract(_) => SocketFamily::UNIX,
			&SocketAddrEx::Symbolic(_) => return None,	//Can not create this.
			&SocketAddrEx::Unknown => return None,		//This is not valid.
		})
	}
	pub fn canonicalize(self) -> SocketAddrEx
	{
		self.__canonicalize_pred(|pfx|pfx == &IPV6_IPV4_MAP_PFX)
	}
	pub fn downgrade(self, prefixes: &DowngradedAddresses) -> SocketAddrEx
	{
		self.__canonicalize_pred(|pfx|{
			pfx == &IPV6_IPV4_MAP_PFX || prefixes.as_inner().binary_search(pfx).is_ok()
		})
	}
	fn __canonicalize_pred(self, pred: impl Fn(&[u8;12]) -> bool) -> SocketAddrEx
	{
		//Only V6 addresses can change in canonicalization.
		match self {
			SocketAddrEx::V6(x) => {
				let ip = x.ip().octets();
				let mut pfx = [0;12];
				pfx.copy_from_slice(&ip[..12]);
				if pred(&pfx) {
					let mut ip2 = [0;4];
					ip2.copy_from_slice(&ip[12..]);
					SocketAddrEx::V4(SocketAddrV4::new(Ipv4Addr::from(ip2), x.port()))
				} else {
					SocketAddrEx::V6(x)
				}
			}
			y => y
		}
	}
	fn from_address(addr: &Option<UnixAddress>) -> SocketAddrEx
	{
		match addr {
			&Some(UnixAddress::Ipv6(AddressIpv6{addr, port, scope, flow})) =>
				SocketAddrEx::V6(SocketAddrV6::new(Ipv6Addr::from(addr), port, flow, scope)),
			&Some(UnixAddress::Ipv4(AddressIpv4{addr, port})) =>
				SocketAddrEx::V4(SocketAddrV4::new(Ipv4Addr::from(addr), port)),
			&Some(UnixAddress::Unix(ref addr)) => SocketAddrEx::Unix(addr.deref().to_owned()),
			&Some(UnixAddress::Abstract(ref addr)) => SocketAddrEx::Abstract(addr.deref().to_owned()),
			&None => SocketAddrEx::Unknown
		}
	}
	fn to_address2<'a>(&'a self) -> Option<UnixAddress<'a>>
	{
		Some(match self {
			&SocketAddrEx::V4(x) => UnixAddress::Ipv4(AddressIpv4 {
				addr: x.ip().octets(),
				port: x.port()
			}),
			&SocketAddrEx::V6(x) => UnixAddress::Ipv6(AddressIpv6 {
				addr: x.ip().octets(),
				port: x.port(),
				flow: x.flowinfo(),
				scope: x.scope_id(),
			}),
			&SocketAddrEx::Unix(ref x) => UnixAddress::Unix(SUnixPath::borrowed(x)),
			&SocketAddrEx::Abstract(ref x) => UnixAddress::Abstract(SUnixPath::borrowed(x)),
			&SocketAddrEx::Symbolic(_) => return None,	//Can not create this.
			&SocketAddrEx::Unknown => return None,
		})
	}
}

static MAIN_FD_COUNT: AtomicUsize = AtomicUsize::new(0);
thread_local!(static PER_THREAD_FD_COUNT: Rc<RefCell<Option<AtomicUsize>>> = Rc::new(RefCell::new(None)));

pub fn thread_local_fd_count() -> &'static AtomicUsize
{
	let x = PER_THREAD_FD_COUNT.with(|x|{
		match x.borrow_mut().deref() {
			&Some(ref c) => Some(unsafe{transmute::<&AtomicUsize, &'static AtomicUsize>(&c)}),
			&None => None,
		}
	}).unwrap_or(&MAIN_FD_COUNT);
	//Sanity check for wrapped around file file descriptor count.
	//println!("File descriptor count: {cnt}", cnt=x.load(Ordering::Relaxed));
	//assert!(x.load(Ordering::Relaxed) < 1000000000);
	x
}

pub fn decouple_thread_fd_count()
{
	PER_THREAD_FD_COUNT.with(|x|*x.borrow_mut() = Some(AtomicUsize::new(MAIN_FD_COUNT.load(Ordering::Relaxed))));
}

pub fn ensure_stdfd_open()
{
	MAIN_FD_COUNT.store(file_descriptors_ok() as usize, Ordering::Relaxed);
}

#[derive(Clone,Debug)]
pub struct FileDescriptor(Option<Arc<(UnixFileDescriptor, OneDescriptorCount)>>);

impl FileDescriptor
{
	pub fn blank() -> FileDescriptor { FileDescriptor(None) }
	pub fn new(fd: RawFdT) -> FileDescriptor
	{
		let fd = unsafe{UnixFileDescriptor::new(fd)};
		FileDescriptor::__new(fd)
	}
	pub(crate) fn __new(fd: UnixFileDescriptor) -> FileDescriptor
	{
		let count = OneDescriptorCount::new();
		FileDescriptor(Some(Arc::new((fd, count))))
	}
	pub fn new_open(path: &[u8], flags: OpenFlags) -> IoResult<FileDescriptor>
	{
		let path = UnixPath::new(path).ok_or_else(||IoError::new(IoErrorKind::InvalidInput,
			"NUL character in path to open"))?;
		let fd = path.open(flags).map_err(os_error_to_io_error)?;
		Ok(FileDescriptor::__new(fd))
	}
	//First is read end, second is write.
	pub fn pipe() -> IoResult<(FileDescriptor, FileDescriptor)>
	{
		Self::pipe2(Default::default())
	}
	//First is read end, second is write.
	pub fn pipe2(flags: PipeFlags) -> IoResult<(FileDescriptor, FileDescriptor)>
	{
		let (r, w) = UnixFileDescriptor::pipe2(flags).map_err(os_error_to_io_error)?;
		Ok((FileDescriptor::__new(r), FileDescriptor::__new(w)))
	}
	//The descriptors are not distinguishable.
	pub fn unix_socketpair() -> IoResult<(FileDescriptor, FileDescriptor)>
	{
		Self::unix_socketpair2(Default::default())
	}
	//The descriptors are not distinguishable.
	pub fn unix_socketpair2(flags: SocketFlags) -> IoResult<(FileDescriptor, FileDescriptor)>
	{
		let (x, y) = UnixFileDescriptor::socketpair2(SocketFamily::UNIX, SocketType::STREAM,
			Default::default(), flags).map_err(os_error_to_io_error)?;
		Ok((FileDescriptor::__new(x), FileDescriptor::__new(y)))
	}
	pub fn dup_stderr(&self) -> IoResult<()>
	{
		//The target is open, so don't increment fd counter.
		match &self.0 {
			&Some(ref x) => x.0.dup2(SpecialFd::STDERR).map_err(os_error_to_io_error),
			&None => dummy_fd_error!("dup2")
		}
	}
	pub fn socket(domain: SocketFamily, stype: SocketType, protocol: SocketProtocol) -> IoResult<FileDescriptor>
	{
		Self::socket2(domain, stype, protocol, Default::default())
	}
	pub fn socket2(domain: SocketFamily, stype: SocketType, protocol: SocketProtocol, flags: SocketFlags) ->
		IoResult<FileDescriptor>
	{
		let fd = UnixFileDescriptor::socket2(domain, stype, protocol, flags).map_err(os_error_to_io_error)?;
		Ok(FileDescriptor::__new(fd))
	}
	pub fn borrow(&self) -> Option<&UnixFileDescriptor>
	{
		match &self.0 { &Some(ref x) => Some(&x.0), &None => None }
	}
	pub fn shutdown_write(&self) -> IoResult<()>
	{
		match &self.0 {
			&Some(ref x) => x.0.shutdown(ShutdownFlags::WR).map_err(os_error_to_io_error),
			&None => dummy_fd_error!("shutdown")
		}
	}
	pub fn set_nonblock(&self) -> IoResult<()>
	{
		match &self.0 {
			&Some(ref x) => x.0.set_nonblock(true).map_err(os_error_to_io_error),
			&None => dummy_fd_error!("set noblocking mode")
		}
	}
	pub fn set_cloexec(&self, f: bool) -> IoResult<()>
	{
		match &self.0 {
			&Some(ref x) => x.0.set_cloexec(f).map_err(os_error_to_io_error),
			&None => dummy_fd_error!("Set/Clear cloexec mode")
		}
	}
	pub fn so_error(&self) -> IoResult<IoResult<()>>
	{
		let fd = match &self.0 {
			&Some(ref x) => x.0.as_fdb(),
			&None => dummy_fd_error!("get SO_ERROR")
		};
		match unsafe{fd.so_error()} {
			Ok(Ok(_)) => Ok(Ok(())),
			Ok(Err(e)) => Ok(Err(os_error_to_io_error(e))),
			Err(e) => Err(os_error_to_io_error(e)),
		}
	}
	pub fn set_reuseaddr(&self) -> IoResult<()>
	{
		match &self.0 {
			&Some(ref x) => x.0.set_reuseaddr(true).map_err(os_error_to_io_error),
			&None => dummy_fd_error!("set SO_REUSEADDR")
		}
	}
	pub fn connect_ex(&self, addr: &SocketAddrEx) -> IoResult<bool>
	{
		let addr = addr.to_address2().ok_or_else(||IoError::new(IoErrorKind::Other,
			"Trying to connect to unknown address type"))?;
		match &self.0 {
			&Some(ref x) => x.0.connect(&addr).map_err(os_error_to_io_error),
			&None => dummy_fd_error!("bind")
		}
	}
	pub fn bind_ex(&self, addr: &SocketAddrEx) -> IoResult<()>
	{
		let addr = addr.to_address2().ok_or_else(||IoError::new(IoErrorKind::Other,
			"Trying to bind to unknown address type"))?;
		match &self.0 {
			&Some(ref x) => x.0.bind(&addr).map_err(os_error_to_io_error),
			&None => dummy_fd_error!("bind")
		}
	}
	pub fn listen(&self, backlog: u32) -> IoResult<()>
	{
		match &self.0 {
			&Some(ref x) => x.0.listen(backlog).map_err(os_error_to_io_error),
			&None => dummy_fd_error!("listen")
		}
	}
	pub fn local_addr_ex(&self) -> IoResult<SocketAddrEx>
	{
		match &self.0 {
			&Some(ref x) => x.0.local_address().map_err(os_error_to_io_error).
				map(|addr|SocketAddrEx::from_address(&addr)),
			&None => dummy_fd_error!("getsockname")
		}
	}
	pub fn remote_addr_ex(&self) -> IoResult<SocketAddrEx>
	{
		match &self.0 {
			&Some(ref x) => x.0.remote_address().map_err(os_error_to_io_error).
				map(|addr|SocketAddrEx::from_address(&addr)),
			&None => dummy_fd_error!("getpeername")
		}
	}
	pub fn accept_ex(&self) -> IoResult<(FileDescriptor, SocketAddrEx)>
	{
		self.accept_ex2(Default::default())
	}
	pub fn accept_ex2(&self, flags: SocketFlags) -> IoResult<(FileDescriptor, SocketAddrEx)>
	{
		match &self.0 {
			&Some(ref x) => x.0.accept2(flags).map_err(os_error_to_io_error).
				map(|(fd, addr)|(FileDescriptor::__new(fd), SocketAddrEx::from_address(&addr))),
			&None => dummy_fd_error!("accept")
		}
	}
	pub fn recvfrom(&self, msg: &mut [u8], flags: SendRecvFlags) -> IoResult<(usize, SocketAddrEx)>
	{
		match &self.0 {
			&Some(ref x) => x.0.recvfrom(msg, flags).map_err(os_error_to_io_error).
				map(|(r, addr)|(r, SocketAddrEx::from_address(&addr))),
			&None => dummy_fd_error!("recvfrom")
		}
	}
	pub fn sendto(&self, msg: &[u8], addr: &SocketAddrEx, flags: SendRecvFlags) -> IoResult<usize>
	{
		let addr = addr.to_address2().ok_or_else(||IoError::new(IoErrorKind::Other,
			"Trying to send to unknown address type"))?;
		match &self.0 {
			&Some(ref x) => x.0.sendto(msg, &addr, flags).map_err(os_error_to_io_error),
			&None => dummy_fd_error!("sendto")
		}
	}
	pub fn as_raw_fd(&self) -> RawFdT
	{
		match &self.0 { &Some(ref x) => x.0.as_raw_fd(), &None => -1 }
	}
	pub fn as_unix_fd(&self) -> Option<UnixFileDescriptorB>
	{
		match &self.0 { &Some(ref x) => Some(x.0.as_fdb()), &None => None }
	}
	pub fn readv(&self, buffer: &[&mut [u8]]) -> Result<usize, IoError>
	{
		match &self.0 {
			&Some(ref x) => x.0.readv(buffer).map_err(os_error_to_io_error),
			&None => dummy_fd_error!("readv")
		}
	}
	pub fn writev(&self, buffer: &[&[u8]]) -> Result<usize, IoError>
	{
		match &self.0 {
			&Some(ref x) => x.0.writev(buffer).map_err(os_error_to_io_error),
			&None => dummy_fd_error!("writev")
		}
	}
	pub fn recv(&self, buf: &mut [u8], flags: SendRecvFlags) -> IoResult<usize>
	{
		match &self.0 {
			&Some(ref x) => x.0.recv(buf, flags).map_err(os_error_to_io_error),
			&None => dummy_fd_error!("recv")
		}
	}
	pub fn send(&self, msg: &[u8], flags: SendRecvFlags) -> IoResult<usize>
	{
		match &self.0 {
			&Some(ref x) => x.0.send(msg, flags).map_err(os_error_to_io_error),
			&None => dummy_fd_error!("send")
		}
	}
	pub fn read_const(&self, buf: &mut [u8]) -> IoResult<usize>
	{
		match &self.0 {
			&Some(ref x) => x.0.read(buf).map_err(os_error_to_io_error),
			&None => dummy_fd_error!("read")
		}
	}
	pub fn write_const(&self, buf: &[u8]) -> IoResult<usize>
	{
		match &self.0 {
			&Some(ref x) => x.0.write(buf).map_err(os_error_to_io_error),
			&None => dummy_fd_error!("write")
		}
	}
	pub fn read_uninit2<'a>(&self, buf: &'a mut [MaybeUninit<u8>]) -> IoResult<&'a [u8]>
	{
		match &self.0 {
			&Some(ref x) => x.0.read_uninit2(buf).map_err(os_error_to_io_error),
			&None => dummy_fd_error!("read_uninit")
		}
	}
	pub fn set_tcp_nodelay(&self, status: bool) -> IoResult<()>
	{
		match &self.0 {
			&Some(ref x) => x.0.set_tcp_nodelay(status).map_err(os_error_to_io_error),
			&None => dummy_fd_error!("set_tcp_nodelay")
		}
		
	}
}

impl PartialEq for FileDescriptor
{
	fn eq(&self, other: &FileDescriptor) -> bool {
		match (&self.0, &other.0) {
			(&Some(ref x), &Some(ref y)) => x.0.as_raw_fd() == y.0.as_raw_fd(),
			(&None, &None) => true,
			_ => false,
		}
	}
}

impl Eq for FileDescriptor {}

impl IoRead for FileDescriptor
{
	fn read(&mut self, buf: &mut [u8]) -> IoResult<usize> { self.read_const(buf) }
}

impl IoWrite for FileDescriptor
{
	fn write(&mut self, buf: &[u8]) -> IoResult<usize> { self.write_const(buf) }
	fn flush(&mut self) -> IoResult<()>
	{
		Ok(())		//NOP.
	}
}
