use super::ArcTextString;
use super::boolopt;
use super::parse_host_etc_hosts;
use super::parse_port_etc_services;
use super::SocketAddrEx;
use super::unescape_rstring;
use super::file_descriptor::escape_rstring;
use btls_aux_fail::ResultExt;
use btls_aux_memory::Attachment;
use btls_aux_memory::split_attach_first;
use btls_util_logging::log;
use std::borrow::Cow;
use std::collections::HashSet;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::net::IpAddr;
use std::net::Ipv4Addr;
use std::net::Ipv6Addr;
use std::net::SocketAddr;
use std::net::SocketAddrV4;
use std::net::SocketAddrV6;
use std::net::ToSocketAddrs;
use std::path::Path;
use std::str::from_utf8;
use std::str::FromStr;
use std::sync::Arc;


fn part_after_dot<'a>(s: &'a str) -> &'a str
{
	for i in 0..s.len() { if s.as_bytes()[i] == b'.' { return &s[i+1..]; } }
	""
}

pub fn call_with_nth_fallback<T,F>(sni: &str, mut cb: F) -> Option<T> where F: FnMut(&str) -> Option<T>
{
	//First is original.
	if let Some(x) = cb(sni) { return Some(x); }
	//If SNI is global wildcard, there are no more.
	fail_if_none!(sni == "*");
	//Then **.<sni>.
	if let Some(x) = cb(&format!("**.{sni}")) { return Some(x); }
	let mut tail = part_after_dot(sni);
	if tail != "" {
		//The third one is *.<tail>.
		if let Some(x) = cb(&format!("*.{tail}")) { return Some(x); }
		//Then all **.<postfix>.
		while tail != "" {
			if let Some(x) = cb(&format!("**.{tail}")) { return Some(x); }
			tail = part_after_dot(tail);
		};
	}
	//Finally *.
	if let Some(x) = cb("*") { return Some(x); }
	None

}

pub fn scope_to_id(scope: &str) -> Result<u32, Cow<'static, str>>
{
	use btls_aux_unix::Path as UnixPath;
	//Interface names with NUL in them are definitely unknown.
	let scope2 = UnixPath::new(scope.as_bytes()).ok_or(cow!("unknown interface name"))?;
	scope2.interface_index().ok_or(cow!("unknown interface name"))
}

fn translate_naddress_core(tgt: &str, defport: Option<u16>) -> Result<SocketAddrEx, Cow<'static, str>>
{
	const ILLSYNTAX: &'static str = "Invalid address syntax";
	Ok(if let Some(tgt) = tgt.strip_prefix("@") {
		//These things are only supported on Linux.
		SocketAddrEx::Abstract(unescape_rstring(tgt))
	} else if tgt.starts_with("/") {
		SocketAddrEx::Unix(unescape_rstring(tgt))
	} else if let Some(tgt) = tgt.strip_prefix("[") {
		//This is presumably IPv6 address literial. Find first % or ] to end the address. Note that one
		//of these has to be present or this is invalid.
		let epos = tgt.find(|x|x=='%'||x==']').ok_or(ILLSYNTAX)?;
		let (ip, mut tgt) = tgt.split_at(epos);
		let ip = Ipv6Addr::from_str(ip).set_err(ILLSYNTAX)?;
		let mut ifidx = 0;
		if let Some(tgt2) = tgt.strip_prefix("%") {
			//This has interface specifier in it. Find ] to end that. Note that that part has to be
			//present
			let epos = tgt2.find(|x|x==']').ok_or(ILLSYNTAX)?;
			let (ifname, tgt2) = tgt2.split_at(epos);
			ifidx = u32::from_str(ifname).or_else(|_|scope_to_id(ifname))?;
			tgt = tgt2;
		}
		//The remainder has to start with ], strip that.
		let tgt = tgt.strip_prefix("]").ok_or(ILLSYNTAX)?;
		let port = if tgt == "" {
			defport.ok_or(ILLSYNTAX)?
		} else {
			//The remainder has to start with :, and then port number.
			let tgt = tgt.strip_prefix(":").ok_or(ILLSYNTAX)?;
			parse_port_etc_services(tgt).map_err(|err|{
				format!("/etc/services: {err}")
			})?
		};
		SocketAddrEx::V6(SocketAddrV6::new(ip, port, 0, ifidx))
	} else {
		let (ip, port) = match split_attach_first(tgt, ":", Attachment::Center) {
			Ok((h,t)) => (h, Some(t)),
			Err(h) => (h, None)
		};
		let ip = Ipv4Addr::from_str(ip).set_err(ILLSYNTAX)?;
		let port = if let Some(port) = port {
			parse_port_etc_services(port).map_err(|err|{
				format!("/etc/services: {err}")
			})?
		} else {
			defport.ok_or(ILLSYNTAX)?
		};
		SocketAddrEx::V4(SocketAddrV4::new(ip, port))
	})
}

fn split_dns_name<'a>(tgt: &'a str, err: Cow<'static, str>) -> Result<(&'a str, Option<&'a str>), Cow<'static, str>>
{
	//Split at first ':', and check that part before is valid name.
	let (name, port) = if let Some(epos) = tgt.find(':') {
		(&tgt[..epos], Some(&tgt[epos+1..]))
	} else {
		(tgt, None)
	};
	fail_if!(!valid_dns_name(name), err);
	Ok((name, port))
}

fn valid_dns_name(name: &str) -> bool
{
	let mut last_empty = false;
	//Empty name is not valid.
	if name.len() == 0 { return false; }
	for label in name.split(".") {
		if last_empty { return false; }		//.. in name.
		last_empty = label.len() == 0;
		//The label must consist of alphanumerics, - and _.
		for c in label.as_bytes().iter().cloned() {
			match c { 45|46|48..=57|65..=90|95|97..=122 => (), _ => return false }
		}
	}
	//Ok, this name is valid.
	true
}

//Beware: This can make DNS lookups.
pub fn translate_multiple_address(tgt: &str, defport: Option<u16>, shosts: Option<&Path>) ->
	Result<Vec<SocketAddrEx>, Cow<'static, str>>
{
	translate_naddress_core(tgt, defport).map(|x|vec![x]).or_else(|err|{
		let (name, port) = split_dns_name(tgt, err)?;
		let port = match port {
			Some(port) => parse_port_etc_services(port).map_err(|err|{
				format!("/etc/services: {err}")
			})?,
			None => defport.ok_or_else(||"No default port for address")?
		};
		let mut ips = parse_host_etc_hosts(name, shosts).unwrap_or(Vec::new());
		if ips.len() > 0 {
			//Some stuff in /etc/hosts, use that.
			return Ok(ips.drain(..).map(|a|to_sockaddrx(SocketAddr::new(a, port))).collect());
		}
		let tgt = format!("{name}:{port}");
		Ok(tgt.to_socket_addrs().set_err("Invalid address syntax")?.map(|x|to_sockaddrx(x)).collect())
	})
}

#[derive(Clone,Debug)]
pub enum ConnectionInfoAddress
{
	V4([u8;4], u16),
	V6([u8;16], u16),
	Unix(String),
	Abstract(String),
	Internal(String),
	Local,
	Unknown,
}

impl ConnectionInfoAddress
{
	fn from_socket_addr_ex(s: &SocketAddrEx) -> ConnectionInfoAddress
	{
		match s {
			&SocketAddrEx::V4(ref x) => ConnectionInfoAddress::V4(x.ip().octets(), x.port()),
			&SocketAddrEx::V6(ref x) => ConnectionInfoAddress::V6(x.ip().octets(), x.port()),
			&SocketAddrEx::Unix(ref x) => ConnectionInfoAddress::Unix(escape_rstring(x)),
			&SocketAddrEx::Abstract(ref x) => ConnectionInfoAddress::Abstract(escape_rstring(x)),
			//Symbolic maps to internal, as these two are pure names.
			&SocketAddrEx::Symbolic(ref x) => ConnectionInfoAddress::Internal(x.clone()),
			&SocketAddrEx::Unknown => ConnectionInfoAddress::Unknown,
		}
	}
	pub fn as_maybe_ipaddr(&self) -> Option<IpAddr>
	{
		match self {
			&ConnectionInfoAddress::V4(ref addr, _) =>
				boolopt(addr != &[0;4]).map(|_|IpAddr::V4(Ipv4Addr::from(*addr))),
			&ConnectionInfoAddress::V6(ref addr, _) =>
				boolopt(addr != &[0;16]).map(|_|IpAddr::V6(Ipv6Addr::from(*addr))),
			_ => None	//Other address types are always unknown.
		}
	}
}


macro_rules! seg { ($arr:expr, $idx:expr) => { ($arr[2*$idx] as u16) * 256 + ($arr[2*$idx+1] as u16) } }



impl Display for ConnectionInfoAddress
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&ConnectionInfoAddress::V4(ref addr, port) => {
				if addr == &[0;4] && port == 0 { return fmt.write_str("0.0.0.0"); }
				let addr = Ipv4Addr::new(addr[0], addr[1], addr[2], addr[3]);
				write!(fmt, "{addr}:{port}")
			},
			&ConnectionInfoAddress::V6(ref addr, port) => {
				if addr == &[0;16] && port == 0 { return fmt.write_str("::"); }
				let addr = Ipv6Addr::new(
					seg!(addr, 0), seg!(addr, 1), seg!(addr, 2), seg!(addr, 3),
					seg!(addr, 4), seg!(addr, 5), seg!(addr, 6), seg!(addr, 7)
				);
				write!(fmt, "[{addr}]:{port}")
			},
			&ConnectionInfoAddress::Unix(ref addr) => {
				if addr.len() == 0 { return fmt.write_str("unix"); }
				fmt.write_str(addr)
			},
			&ConnectionInfoAddress::Abstract(ref addr) => {
				if addr.len() == 0 { return fmt.write_str("abstract"); }
				write!(fmt, "@{addr}")
			},
			&ConnectionInfoAddress::Internal(ref addr) => write!(fmt, "{addr}"),
			&ConnectionInfoAddress::Local => fmt.write_str("local"),
			&ConnectionInfoAddress::Unknown => fmt.write_str("unknown"),
		}
	}
}

#[derive(Clone,Debug)]
pub struct ConnectionInfo
{
	pub src: ConnectionInfoAddress,
	pub dest: ConnectionInfoAddress,
	pub orig_dest: ConnectionInfoAddress,
}

macro_rules! compose { ($a:expr,$b:expr) => { ($a as u16) * 256 + ($b as u16) } }

impl ConnectionInfo
{
	fn __new(src: ConnectionInfoAddress, dest: ConnectionInfoAddress) -> ConnectionInfo
	{
		ConnectionInfo {
			src: src,
			dest: dest.clone(),
			orig_dest: dest,
		}
	}
	pub fn from_socket_addrs(src: &SocketAddrEx, dest: &Option<SocketAddrEx>) -> ConnectionInfo
	{
		//Default original destination same as new destination.
		ConnectionInfo::from_socket_addrs_orig(src, dest, dest)
	}
	pub fn from_socket_addrs_orig(src: &SocketAddrEx, dest: &Option<SocketAddrEx>,
		orig_dest: &Option<SocketAddrEx>) -> ConnectionInfo
	{
		let dest = dest.as_ref().map(|dest|ConnectionInfoAddress::from_socket_addr_ex(dest)).
			unwrap_or(ConnectionInfoAddress::Unknown);
		let orig_dest = orig_dest.as_ref().map(|dest|ConnectionInfoAddress::from_socket_addr_ex(dest)).
			unwrap_or(ConnectionInfoAddress::Unknown);
		ConnectionInfo {
			src: ConnectionInfoAddress::from_socket_addr_ex(src),
			dest: dest.clone(),
			orig_dest: orig_dest,
		}
	}
	pub fn internal_to_socket(src: &str, dest: &SocketAddrEx)  -> ConnectionInfo
	{
		ConnectionInfo::__new(ConnectionInfoAddress::Internal(src.to_owned()),
			ConnectionInfoAddress::from_socket_addr_ex(dest))
	}
	pub fn internal_to_internal(src: &str, dest: &str)  -> ConnectionInfo
	{
		ConnectionInfo::__new(ConnectionInfoAddress::Internal(src.to_owned()),
			ConnectionInfoAddress::Internal(dest.to_owned()))
	}
	pub fn local_to_socket(dest: &SocketAddrEx)  -> ConnectionInfo
	{
		ConnectionInfo::__new(ConnectionInfoAddress::Local, ConnectionInfoAddress::from_socket_addr_ex(dest))
	}
	pub fn local_to_internal(dest: &str)  -> ConnectionInfo
	{
		ConnectionInfo::__new(ConnectionInfoAddress::Local, ConnectionInfoAddress::Internal(dest.to_owned()))
	}
	pub fn from_proxyv1(&mut self, line: &[u8], no_notify: bool) -> Option<()>
	{
		let line = from_utf8(line).ok()?;
		let mut words = line.split(' ');
		words.next().and_then(|wh|boolopt(wh == "PROXY"))?;
		let w0 = words.next()?;
		let w1 = words.next()?;
		let w2 = words.next()?;
		let w3 = words.next()?;
		let w4 = words.next()?;
		if w0 == "UNKNOWN" { return Some(()); }		//No update.
		let src = IpAddr::from_str(w1).ok()?;
		let dest = IpAddr::from_str(w2).ok()?;
		let srcp = u16::from_str(w3).ok()?;
		let destp = u16::from_str(w4).ok()?;

		let orig_dest = self.orig_dest.clone();		//orig_dest is preserved!
		*self = ConnectionInfo {
			src: match src {
				IpAddr::V4(x) => ConnectionInfoAddress::V4(x.octets(), srcp),
				IpAddr::V6(x) => ConnectionInfoAddress::V6(x.octets(), srcp),
			},
			dest: match dest {
				IpAddr::V4(x) => ConnectionInfoAddress::V4(x.octets(), destp),
				IpAddr::V6(x) => ConnectionInfoAddress::V6(x.octets(), destp),
			},
			orig_dest: orig_dest,
		};
		if !no_notify {
			//This is PII.
			log!(INFO "Updated addresses: {src} -> {dst}", src=self.src, dst=self.dest);
		}

		Some(())
	}
	pub fn from_proxyv2<'a>(&mut self, data: &'a [u8], no_notify: bool) -> Option<&'a [u8]>
	{
		let orig_dest = self.orig_dest.clone();		//orig_dest is preserved!
		boolopt(data.len() >= 16)?;
		boolopt(&data[..12] == b"\x0D\x0A\x0D\x0A\x00\x0D\x0A\x51\x55\x49\x54\x0A")?;
		let rcode = data[12];
		boolopt(rcode >> 4 == 2)?;	//Version has to be 2.
		let rcode = rcode & 15;
		let opcode = data[13];
		let len = compose!(data[14], data[15]) as usize;
		boolopt(len + 16 == data.len())?;	//data has 16-byte header.
		//Ok, decode.
		let data = &data[16..];
		if opcode == 0 {
			//Empty address records.
			Some(data)
		} else if opcode == 0x11 || opcode == 0x12 {
			//IPv4: 12 bytes of address.
			boolopt(data.len() >= 12)?;
			let mut x = [0;4];
			x.copy_from_slice(&data[0..4]);
			let src = ConnectionInfoAddress::V4(x, compose!(data[8], data[9]));
			x.copy_from_slice(&data[4..8]);
			let dest = ConnectionInfoAddress::V4(x, compose!(data[10], data[11]));
			if rcode == 1 {		//REMOTE.
				*self = ConnectionInfo {src: src, dest: dest, orig_dest:orig_dest};
				//This is PII.
				log!(INFO "Updated addresses: {src} -> {dst}", src=self.src, dst=self.dest);
			}
			Some(&data[12..])
		} else if opcode == 0x21 || opcode == 0x22 {
			//IPv4: 36 bytes of address.
			boolopt(data.len() >= 36)?;
			let mut x = [0;16];
			x.copy_from_slice(&data[0..16]);
			let src = ConnectionInfoAddress::V6(x, compose!(data[32], data[33]));
			x.copy_from_slice(&data[16..32]);
			let dest = ConnectionInfoAddress::V6(x, compose!(data[34], data[35]));
			if rcode == 1 {		//REMOTE.
				*self = ConnectionInfo {src: src, dest: dest, orig_dest:orig_dest};
				//This is PII.
				if !no_notify {
					log!(INFO "Updated addresses: {src} -> {dst}", src=self.src, dst=self.dest);
				}
			}
			Some(&data[36..])
		} else {
			None	//Failed to parse.
		}
	}
}

fn to_sockaddrx(y: SocketAddr) -> SocketAddrEx
{
	match y {
		SocketAddr::V4(x) => SocketAddrEx::V4(x),
		SocketAddr::V6(x) => SocketAddrEx::V6(x),
	}
}

pub struct StringInterner(HashSet<ArcTextString>);

impl StringInterner
{
	pub fn new() -> StringInterner { StringInterner(HashSet::new()) }
	pub fn vacuum(&mut self) -> bool
	{
		let was_len = self.0.len();
		self.0.retain(|val|Arc::strong_count(val.as_arc_ref()) > 1);
		self.0.len() != was_len
	}
	pub fn stats(&self) -> (usize, usize)
	{
		//Attribute 6 words to memory allocations by Arc<PathBuf>.
		let e = 6 * std::mem::size_of::<usize>();
		self.0.iter().fold((0,0),|(c,s),x|(c+1,s+e+x.len()))
	}
	pub fn to_atom(&mut self, s: &str) -> Arc<String>
	{
		//First try internal map.
		if let Some(item) = self.0.get(s) { return item.as_arc(); }
		//Not in internal map. Add and return.
		let s = s.to_string();
		let new = Arc::new(s.clone());
		self.0.insert(From::from(new.clone()));
		new
	}
	pub fn to_ats(&mut self, s: &str) -> ArcTextString { From::from(self.to_atom(s)) }
	pub fn to_ats_existing(&self, s: &str) -> Option<ArcTextString>
	{
		self.0.get(s).map(|x|x.clone())
	}
}

#[test]
fn test_fallbacks_star()
{
	let mut r = Vec::new();
	call_with_nth_fallback::<(),_>("*", |y|{r.push(y.to_owned()); None});
	assert_eq!(&r[..], ["*"]);
}

#[test]
fn test_fallbacks_1label()
{
	let mut r = Vec::new();
	call_with_nth_fallback::<(),_>("foo", |y|{r.push(y.to_owned()); None});
	assert_eq!(&r[..], ["foo","**.foo","*"]);
}

#[test]
fn test_fallbacks_2label()
{
	let mut r = Vec::new();
	call_with_nth_fallback::<(),_>("foo.bar", |y|{r.push(y.to_owned()); None});
	assert_eq!(&r[..], ["foo.bar","**.foo.bar","*.bar","**.bar","*"]);
}

#[test]
fn test_fallbacks_3label()
{
	let mut r = Vec::new();
	call_with_nth_fallback::<(),_>("foo.bar.baz", |y|{r.push(y.to_owned()); None});
	assert_eq!(&r[..], ["foo.bar.baz","**.foo.bar.baz","*.bar.baz","**.bar.baz","**.baz","*"]);
}
