use ::os_error_to_io_error;
use super::FdListener;
use super::FileDescriptor;
use btls_aux_unix::EpollCreateFlags;
use btls_aux_unix::EpollCtlCmd;
use btls_aux_unix::EpollEvent;
use btls_aux_unix::EpollEventType;
use btls_aux_unix::FileDescriptor as UnixFileDescriptor;
use btls_util_logging::syslog;
use std::cmp::min;
use std::io::Error as IoError;
use std::mem::transmute;
use std::slice::Iter;
use std::time::Duration;


pub const IO_DEREGISTER: u8 = 0;
pub const IO_WAIT_READ: u8 = 1;
pub const IO_WAIT_WRITE: u8 = 2;
pub const IO_WAIT_ONESHOT: u8 = 4;
pub const IO_WAIT_HUP: u8 = 8;
pub const IO_WAIT_ERROR: u8 = 16;

fn flags_to_events(flags: u8) -> EpollEventType
{
	let mut events = Default::default();
	if flags & IO_WAIT_READ != 0 { events |= EpollEventType::IN; }
	if flags & IO_WAIT_WRITE != 0 { events |= EpollEventType::OUT; }
	if flags & IO_WAIT_HUP != 0 { events |= EpollEventType::HUP; }
	if flags & IO_WAIT_ERROR != 0 { events |= EpollEventType::ERR; }
	if flags & IO_WAIT_ONESHOT != 0 { events |= EpollEventType::ONESHOT; }
	events
}

pub struct Poller(UnixFileDescriptor);

impl Poller
{
	pub fn new() -> Result<Poller, IoError>
	{
		Ok(Poller(UnixFileDescriptor::epoll_create(EpollCreateFlags::CLOEXEC).
			map_err(os_error_to_io_error)?))
	}
	pub fn poll(&mut self, evq: &mut EventQueue, to: Option<Duration>) -> Result<usize, IoError>
	{
		let timeout = to.map(|to|{
			//Cap waits to ~2.15M seconds (~24.85 days) to avoid overflows.
			min(to.as_secs(), 2147482) as i32 * 1000 + to.subsec_nanos() as i32 / 1000000
		}).unwrap_or(-1);
		let count = self.0.epoll_wait(&mut evq.0, timeout).map_err(os_error_to_io_error)?;
		evq.1 = count as usize;
		Ok(evq.1)
	}
	pub fn register_listen(&mut self, fd: &FdListener, token: usize, flags: u8) -> Result<(), IoError>
	{
		fd.0.as_unix_fd().map(|tfd|{
			unsafe{self.0.epoll_ctlb(tfd, EpollCtlCmd::Add(flags_to_events(flags), token as u64))}
		}).unwrap_or(Ok(())).map_err(os_error_to_io_error)
	}
	pub fn deregister_listen(&mut self, fd: &FdListener) -> Result<(), IoError>
	{
		fd.0.as_unix_fd().map(|tfd|{
			unsafe{self.0.epoll_ctlb(tfd, EpollCtlCmd::Delete)}
		}).unwrap_or(Ok(())).map_err(os_error_to_io_error)
	}
}

pub struct EventQueue(Vec<EpollEvent>, usize);
pub struct EventIterator<'a>(Iter<'a, EpollEvent>);

impl EventQueue
{
	pub fn new(qsize: usize) -> EventQueue
	{
		EventQueue(vec![EpollEvent::new(Default::default(), 0); qsize], 0)
	}
	pub fn iter(&self) -> EventIterator { EventIterator((&self.0[..min(self.1, self.0.len())]).iter()) }
}

impl<'a> Iterator for EventIterator<'a>
{
	type Item = Event;
	fn next(&mut self) -> Option<Event> { self.0.next().map(|e|Event(e.clone())) }
}

pub struct Event(EpollEvent);

impl Event
{
	pub fn token(&self) -> usize { self.0.get_token() as usize }
	pub fn ready(&self) -> u8
	{
		let kind = self.0.get_events();
		let mut kind2 = 0;
		if kind.any_of(EpollEventType::IN) { kind2 |= IO_WAIT_READ; }
		if kind.any_of(EpollEventType::OUT) { kind2 |= IO_WAIT_WRITE; }
		if kind.any_of(EpollEventType::HUP) { kind2 |= IO_WAIT_HUP; }
		if kind.any_of(EpollEventType::ERR) { kind2 |= IO_WAIT_ERROR; }
		kind2
	}
}

pub struct Poll(UnixFileDescriptor);

impl Poll
{
	#[doc(hidden)]
	pub fn __make<'a>(x: &'a mut Poller) -> &'a mut Poll { unsafe{transmute(x)} }
	//Have to expose legacy-style functions, as porting services is too nasty otherwise.
	pub fn register(&mut self, fd: &FileDescriptor, token: usize, flags: u8) -> Result<(), IoError>
	{
		fd.as_unix_fd().map(|tfd|{
			unsafe{self.0.epoll_ctlb(tfd, EpollCtlCmd::Add(flags_to_events(flags), token as u64))}
		}).unwrap_or(Ok(())).map_err(os_error_to_io_error)
	}
	pub fn reregister(&mut self, fd: &FileDescriptor, token: usize, flags: u8) -> Result<(), IoError>
	{
		fd.as_unix_fd().map(|tfd|{
			unsafe{self.0.epoll_ctlb(tfd, EpollCtlCmd::Modify(flags_to_events(flags), token as u64))}
		}).unwrap_or(Ok(())).map_err(os_error_to_io_error)
	}
	pub fn deregister(&mut self, fd: &FileDescriptor) -> Result<(), IoError>
	{
		fd.as_unix_fd().map(|tfd|{
			unsafe{self.0.epoll_ctlb(tfd, EpollCtlCmd::Delete)}
		}).unwrap_or(Ok(())).map_err(os_error_to_io_error)
	}
}

pub struct FdPoller
{
	desc: FileDescriptor,
	token: usize,
	registered: bool,
}

impl Drop for FdPoller
{
	fn drop(&mut self)
	{
		if self.registered {
			syslog!(CRITICAL "Dropping poller for fd={fd} with poll active!", fd=self.desc.as_raw_fd());
		}
	}
}

impl FdPoller
{
	pub fn new(fd: FileDescriptor, token: usize) -> FdPoller
	{
		FdPoller {
			desc: fd,
			token: token,
			registered: false,
		}
	}
	pub fn update(&mut self, poll: &mut Poll, flags: u8) -> Result<(), IoError>
	{
		if flags & (IO_WAIT_ERROR|IO_WAIT_HUP|IO_WAIT_READ|IO_WAIT_WRITE) != 0 {
			let cmd = if self.registered {
				EpollCtlCmd::Modify(flags_to_events(flags), self.token as u64)
			} else {
				EpollCtlCmd::Add(flags_to_events(flags), self.token as u64)
			};
			self.desc.as_unix_fd().map(|tfd|unsafe{poll.0.epoll_ctlb(tfd, cmd)}).unwrap_or(Ok(())).
				map_err(os_error_to_io_error)?;
			self.registered = true;
		} else if self.registered {
			self.desc.as_unix_fd().map(|tfd|{
				unsafe{poll.0.epoll_ctlb(tfd, EpollCtlCmd::Delete)}
			}).unwrap_or(Ok(())).map_err(os_error_to_io_error)?;
			self.registered = false;
		}
		Ok(())
	}
	pub fn remove(&mut self, poll: &mut Poll)
	{
		if let Err(err) = self.update(poll, IO_DEREGISTER) {
			syslog!(ERROR "Failed to unregister poller for fd={fd}: {err}", fd=self.desc.as_raw_fd());
		}
	}
	pub fn inner(&self) -> FileDescriptor { self.desc.clone() }
	pub fn borrow<'a>(&'a self) -> &'a FileDescriptor { &self.desc }
	pub fn borrow_mut<'a>(&'a mut self) -> &'a mut FileDescriptor { &mut self.desc }
}
