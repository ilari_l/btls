use super::AllocatedToken;
use super::ConnectionInfo;
use super::ConnectionInfoAddress;
use super::decrement_count;
use super::DowngradedAddresses;
use super::Event;
use super::EventQueue;
use super::FdListener;
use super::FrozenIpSet;
use super::FileDescriptor;
use super::IO_WAIT_ERROR;
use super::IO_WAIT_HUP;
use super::IO_WAIT_INTR;
use super::IO_WAIT_READ;
use super::increment_count;
use super::IpSet;
use super::LocalMetrics;
use super::Poll;
use super::Poller;
use super::RemoteIrq;
use super::SocketAddrEx;
use super::thread_local_fd_count;
use super::TokenAllocatorPool;
use btls_aux_collections::RwLock;
use btls_aux_time::PointInTime;
use btls_aux_unix::CmsgParser;
use btls_aux_unix::FileDescriptor as UnixFileDescriptor;
use btls_aux_unix::RawFdT;
use btls_aux_unix::SocketFlags;
use btls_util_logging::ConnId;
use btls_util_logging::ConnIdPrefix;
use btls_util_logging::log;
use btls_util_logging::set_current_task;
use btls_util_logging::syslog;
use std::borrow::Cow;
use std::collections::BTreeMap;
use std::collections::BTreeSet;
use std::io::Error as IoError;
use std::io::ErrorKind as IoErrorKind;
use std::mem::replace;
use std::net::IpAddr;
use std::net::Ipv4Addr;
use std::net::Ipv6Addr;
use std::ops::Deref;
use std::cell::RefCell;
use std::collections::HashSet;
use std::mem::MaybeUninit;
use std::panic::catch_unwind;
use std::panic::AssertUnwindSafe;
use std::rc::Rc;
use std::sync::Arc;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering as MemOrder;
use std::time::Duration;


pub fn is_fatal_error(err: &IoError) -> bool
{
	if err.kind() == IoErrorKind::Interrupted { false }
	else if err.kind() == IoErrorKind::WouldBlock { false }
	else { true }
}

fn trans_srcaddr(addr: &ConnectionInfoAddress) -> Option<IpAddr>
{
	match addr {
		&ConnectionInfoAddress::V6(a, _) => Some(IpAddr::V6(Ipv6Addr::from(a))),
		&ConnectionInfoAddress::V4(a, _) => Some(IpAddr::V4(Ipv4Addr::from(a))),
		//Ignore any address that is not IPv4 nor IPv6.
		_ => None
	}
}

pub struct AddTokenCallback
{
	allocator: TokenAllocatorPool,
	tokens: Vec<AllocatedToken>,
	spawns: Vec<(Box<dyn UpperLayerConnection>, Vec<AllocatedToken>, ConnectionInfo, bool)>,
	change_source_to: Option<IpAddr>,
	ignore_error: bool,
}

impl AddTokenCallback
{
	//Set the ignore error flag. This causes connection to not get killed even if it is in error state.
	pub fn set_ignore_error(&mut self) { self.ignore_error = true; }
	pub fn allocator(&mut self) -> &mut TokenAllocatorPool { &mut self.allocator }
	pub fn allocate_token(&mut self) -> Result<AllocatedToken, ()>
	{
		self.allocator.allocate()
	}
	pub fn add(&mut self, token: AllocatedToken) { self.tokens.push(token); }
	pub fn spawn<'a>(&mut self, conn: Box<dyn UpperLayerConnection>, tokens: Cow<'a, [AllocatedToken]>,
		info: ConnectionInfo)
	{
		self.spawns.push((conn, tokens.into_owned(), info, false));
	}
	//Spawn a connection masquerading as its parent. The following special factors apply to such connections:
	//
	// - The connection create and teardown messages are normally (unless cause is fatal exception) not printed.
	// - When calling event handlers, the connection id is set to parent connection id.
	// - If parent connection is torn down, the connections masquerading as it are killed.
	pub fn spawn_masquerade<'a>(&mut self, conn: Box<dyn UpperLayerConnection>,
		tokens: Cow<'a, [AllocatedToken]>, info: ConnectionInfo)
	{
		self.spawns.push((conn, tokens.into_owned(), info, true));
	}
	pub fn set_source(&mut self, addr: &ConnectionInfoAddress)
	{
		trans_srcaddr(addr).map(|a|self.change_source_to=Some(a));
	}
}

pub trait UpperLayerConnection
{
	//Handle a init event (return Err(()) if connection should be killed). Might panic.
	//
	//This is called immediately after connection has been created.
	#[allow(unused_variables)]
	fn handle_init_event(&mut self, cid: ConnId, poll: &mut Poll) -> Result<(), Cow<'static, str>>
	{
		Ok(())
	}
	//Handle quit event.
	fn handle_quit_event(&mut self) {}
	//Handle a timed event (return Err(()) if connection should be killed). Might panic.
	fn handle_timed_event(&mut self, now: PointInTime, poll: &mut Poll) -> Result<(), Cow<'static, str>>;
	//Handle a socket event (return Err(()) if connection should be killed). Might panic.
	fn handle_event(&mut self, poll: &mut Poll, tok: usize, kind: u8, id: ConnId,
		addtoken: &mut AddTokenCallback) -> Result<(), Cow<'static, str>>;
	//Handle a socket fault. The connection is killed afterwards, just like if handle_event returned Err(()).
	//The error defaults to Broken pipe.
	fn handle_fault(&mut self, poll: &mut Poll) -> Option<Cow<'static, str>>;
	//Remove the pollers. Must not panic, including with poisoned locks.
	fn remove_pollers(&mut self, poll: &mut Poll);
	//Get next timed event. Called early and after calling handle_timed_event() or handle_event().
	fn get_next_timed_event(&self, now: PointInTime) -> Option<PointInTime>;
	//Get main file descriptor.
	fn get_main_fd(&self) -> i32;
}

pub trait UpperLayerConnectionFactory
{
	//Create a new connection.
	fn new(&self, poll: &mut Poll, allocator: &mut TokenAllocatorPool, fd: FileDescriptor,
		info: ConnectionInfo) ->
		Result<(Box<dyn UpperLayerConnection>, Vec<AllocatedToken>, bool), Cow<'static, str>>;
	//Remap address.
	fn remap(&self, addr: SocketAddrEx) -> SocketAddrEx
	{
		addr	//Default identity map.
	}
}

struct NewConnection
{
	conn: Box<dyn UpperLayerConnection>,
	tokens: Vec<AllocatedToken>,
	info: ConnectionInfo,
	addr_valid: bool,
}

impl NewConnection
{
	fn new(factory: &dyn UpperLayerConnectionFactory, poll: &mut Poll, allocator: &mut TokenAllocatorPool,
		fd: FileDescriptor, info: ConnectionInfo) -> Result<NewConnection, Cow<'static, str>>
	{
		//Do not care if this fails.
		fd.set_tcp_nodelay(true).ok();
		let (ulc, tokens, addr_valid) = factory.new(poll, allocator, fd, info.clone()).map_err(|err|{
			format!("Failed to construct upper layer connection: {err}")
		})?;
		Ok(NewConnection {
			conn: ulc,
			tokens: tokens,
			info: info,
			addr_valid: addr_valid
		})
	}
}

pub struct Listener
{
	socket: FdListener,
	conn_factory: Arc<dyn UpperLayerConnectionFactory>,
}

impl Listener
{
	pub fn new(socket: FileDescriptor, factory: Arc<dyn UpperLayerConnectionFactory>) -> Listener
	{
		Listener{socket: FdListener::new_fd(socket), conn_factory: factory}
	}
	fn accept(&mut self, poll: &mut Poll, allocator: &mut TokenAllocatorPool, fd_limit: usize,
		events: &dyn ListenerLoopEvents, downgraded: &DowngradedAddresses) ->
		Result<NewConnection, Cow<'static, str>>
	{
		fail_if!(thread_local_fd_count().load(MemOrder::Relaxed) > fd_limit, "");
		let flags = SocketFlags::CLOEXEC;
		let (conn, addr) = self.socket.accept_ex2(flags).map_err(|err|{
			if !is_fatal_error(&err) { return String::new(); }
			format!("Can't accept connection: {err}")
		})?;
		events.incoming();
		//Remap the local address before using. Local addresses are canonicalized, remote ones are
		//downgraded.
		let addr = addr.downgrade(downgraded);
		let oladdr = conn.local_addr_ex().map(|addr|addr.downgrade(downgraded)).ok();
		let laddr = oladdr.as_ref().map(|addr|self.conn_factory.remap(addr.clone()));
		let info = ConnectionInfo::from_socket_addrs_orig(&addr, &laddr, &oladdr);
		let fd = conn.into_raw_fd();
		NewConnection::new(self.conn_factory.deref(), poll, allocator, fd, info)
	}
	//Downgraded must be sorted according to sort().
	fn spawn(&mut self, poll: &mut Poll, allocator: &mut TokenAllocatorPool, fd: FileDescriptor,
		events: &dyn ListenerLoopEvents, downgraded: &DowngradedAddresses) ->
		Result<NewConnection, Cow<'static, str>>
	{
		events.incoming();
		//Unlike accept(), this never remaps the address. However, canonicalization is done. Local
		//addresses are canonicalized, remote ones are downgraded.
		let src = fd.remote_addr_ex().map(|addr|addr.downgrade(downgraded)).unwrap_or(SocketAddrEx::Unknown);
		let dst = fd.local_addr_ex().map(|addr|addr.canonicalize()).ok();
		let info = ConnectionInfo::from_socket_addrs_orig(&src, &dst, &dst);
		NewConnection::new(self.conn_factory.deref(), poll, allocator, fd, info)
	}
	fn as_raw_fd(&self) -> RawFdT { self.socket.as_raw_fd() }
	pub fn local_addr_ex(&self) -> SocketAddrEx { self.socket.local_addr_ex().unwrap_or(SocketAddrEx::Unknown) }
}

#[cfg(target_arch="x86_64")]
fn cpu_cycles() -> u64 { unsafe { std::arch::x86_64::_rdtsc() as u64 } }

#[cfg(not(target_arch="x86_64"))]
fn cpu_cycles() -> u64 { 0 }

struct CpuCycleTracker(u64, u64);

impl CpuCycleTracker
{
	fn start(dl: u64) -> CpuCycleTracker { CpuCycleTracker(cpu_cycles(), dl) }
	fn end(self) -> Option<u64> { let dt = cpu_cycles() - self.0; if dt > self.1 { Some(dt) } else { None } }
}

fn tooslow(local_metrics: &LocalMetrics, cycles: u64, op: &str)
{
	local_metrics.deadline_exceeded();
	syslog!(WARNING "Task handler deadline exceeded (cycles={cycles}, category={op})");
}

pub trait ListenerLoopEvents
{
	fn locals(&self) -> Arc<LocalMetrics>;		//Get local metrics.
	fn incoming(&self);				//Thread did a raw accept.
	fn committed(&self);				//Thread committed accept.
	fn retired(&self);				//Thread retired a connection.
}

struct DummyEvents;

impl ListenerLoopEvents for DummyEvents
{
	fn locals(&self) -> Arc<LocalMetrics> { Arc::new(LocalMetrics::new(0)) }
	fn incoming(&self) {}
	fn committed(&self) {}
	fn retired(&self) {}
}

struct ConnectionContext
{
	upper: Box<dyn UpperLayerConnection>,
	tokens: Vec<AllocatedToken>,
	next_event: Option<PointInTime>,
	incoming: bool,
	as_id: u64,
}

struct _KlineDb
{
	klines: FrozenIpSet,
}

impl _KlineDb
{
	fn set_klines(&mut self, list: FrozenIpSet, generation: &AtomicUsize)
	{
		self.klines = list;
		generation.fetch_add(1, MemOrder::AcqRel);
	}
	fn check_kline(&self, addr: IpAddr) -> bool { self.klines.lookup(addr) }
}

#[derive(Clone)]
pub struct KlineDb
{
	inner: Arc<RwLock<_KlineDb>>,
	generation: Arc<AtomicUsize>,
}

impl KlineDb
{
	pub fn new() -> KlineDb
	{
		KlineDb {
			inner: Arc::new(RwLock::new(_KlineDb {
				klines: FrozenIpSet::new(),
			})),
			generation: Arc::new(AtomicUsize::new(0)),
		}
	}
	pub fn set_klines2(&mut self, nlist: FrozenIpSet)
	{
		self.inner.write().set_klines(nlist, &self.generation)
	}
	//Each entry is K-Lined block with first 16 bytes being the IPv6 address, and 17th byte is netmask.
	pub fn set_klines(&mut self, list: &[[u8;17]])
	{
		let mut nlist = IpSet::new();
		for entry in list {
			let mut ip = [0u8;16];
			ip.copy_from_slice(&entry[..16]);
			if entry[16] <= 128 {
				nlist.add(IpAddr::V6(Ipv6Addr::from(ip)), entry[16], true);
			}
		}
		self.set_klines2(nlist.freeze());
	}
	fn check_kline(&self, addr: IpAddr) -> bool
	{
		self.inner.read().check_kline(addr)
	}
	fn get_generation(&self) -> usize { self.generation.load(MemOrder::Acquire) }
}

thread_local!(static THREAD_UNSAFE_INTERRUPTS: Rc<RefCell<HashSet<usize>>> = Rc::new(RefCell::new(HashSet::new())));

fn with_task<T>(prefix: ConnIdPrefix, id: u64, f: impl FnOnce() -> T) -> T
{
	let old_task = set_current_task(Some(ConnId::new(prefix, id)));
	let f = f();
	set_current_task(old_task);
	f
}

pub struct SighupPipe(FileDescriptor, &'static AtomicBool);

static DUMMY_SIGHUP_FLAG: AtomicBool = AtomicBool::new(false);

impl SighupPipe
{
	pub(crate) fn new(flag: &'static AtomicBool, fd: FileDescriptor) -> SighupPipe { SighupPipe(fd, flag) }
	pub fn dummy() -> SighupPipe { SighupPipe(FileDescriptor::blank(), &DUMMY_SIGHUP_FLAG) }
}

macro_rules! impl_lth_list
{
	($clazz:ident) => {
		impl $clazz
		{
			pub(super) fn set_owned(&self, owned: bool)
			{
				if owned {
					REMOTE_HANDLE_LIST.with(|list|{
						if let Some(target) = self.get_key() {
							list.insert(target, self.to_remote());
						}
					})
				} else {
					//If not initialized, it is not on the list.
					REMOTE_HANDLE_LIST.with_noinit(|list|{
						self.get_key().map(|target|list.remove(&target));
					})
				}
			}
			pub(super) fn for_each_remote_handle(mut f: impl FnMut(&RemoteThreadHandle))
			{
				REMOTE_HANDLE_LIST.with_noinit(|list|{
					for (_, ref item) in list.iter() { f(item); }
				})
			}
		}
	}
}

mod thandle_linux;

pub use self::thandle_linux::*;

pub struct ListenerLoop
{
	tokens: BTreeMap<usize, u64>,
	connections: BTreeMap<u64, ConnectionContext>,
	listeners: BTreeMap<usize, (Listener, AllocatedToken)>,
	gate_listeners: BTreeMap<Vec<u8>, Listener>,
	srcaddrs: BTreeMap<u64, IpAddr>,
	masquerade_bwd: BTreeMap<u64, Vec<u64>>,
	tp_gate: Option<(AllocatedToken, FileDescriptor)>,
	next_conn_seq: u64,
	poll: Poller,
	tpool: TokenAllocatorPool,
	max_fd_count: usize,				//Maximum fd count.
	dumped_listeners: bool,
	sighup_flag: &'static AtomicBool,
	event_q: BTreeSet<(PointInTime, u64)>,
	idprefix: ConnIdPrefix,
	events: Box<dyn ListenerLoopEvents>,
	local_events: Arc<LocalMetrics>,
	klines: Option<(KlineDb, usize)>,
	lth: LocalThreadHandle,
	sighup_pipe: FileDescriptor,
	persistent: bool,
	downgrades: DowngradedAddresses,
	cpu_deadline: u64,
}

static KLINED: Option<&'static str> = Some("K-Lined");
const TOKEN_IRQRECV: usize = 1;
const TOKEN_SIGHUP: usize = 2;
const TOKEN_BROADCAST_WAKE: usize = 3;
const TOKEN_FIRST: usize = 4;

thread_local!(static REMOTE_HANDLE_OF_THIS_THREAD: RefCell<RemoteThreadHandle> =
	RefCell::new(RemoteThreadHandle::dummy()));

impl ListenerLoop
{
	//Create new listener loop with specified maximum file descriptor count and specified sighup flag.
	pub fn new(max_fd: usize, sighup: SighupPipe, lth: LocalThreadHandle) ->
		Result<ListenerLoop, Cow<'static, str>>
	{
		let monitor_fd = lth.wants_monitor();
		let mut poller = Poller::new().map_err(|err|format!("Can't create poller: {err}"))?;
		//Reserve some tokens, so those can be used for various purposes.
		let tpool = TokenAllocatorPool::new(TOKEN_FIRST, 0x7FFFFF00);
		//Hack: Use Poll as it has method to register a fd (not listener). Use reserved token 1 for the
		//irq recevier. No need to save the fd, as lth keeps it alive. Note that the poll must be
		//permanent, because it is never re-armed. Note that the sighup pipe may not exist.
		Poll::__make(&mut poller).register(&monitor_fd, TOKEN_IRQRECV, IO_WAIT_READ).
			map_err(|err|format!("poll.register (irq receiver) error: {err}"))?;
		if sighup.0.as_raw_fd() >= 0 {
			Poll::__make(&mut poller).register(&sighup.0, TOKEN_SIGHUP, IO_WAIT_READ).
				map_err(|err|format!("poll.register (sighup receiver) error: {err}"))?;
		}
		lth.set_owned(true);
		REMOTE_HANDLE_OF_THIS_THREAD.with(|x|*x.borrow_mut() = lth.to_remote()); 

		Ok(ListenerLoop{
			tokens: BTreeMap::new(),
			connections: BTreeMap::new(),
			listeners: BTreeMap::new(),
			gate_listeners: BTreeMap::new(),
			tp_gate: None,
			srcaddrs: BTreeMap::new(),
			masquerade_bwd: BTreeMap::new(),
			next_conn_seq: 0,
			poll: poller,
			tpool: tpool,
			max_fd_count: max_fd,
			dumped_listeners: false,
			sighup_flag: sighup.1,
			event_q: BTreeSet::new(),
			idprefix: ConnIdPrefix::None,
			events: Box::new(DummyEvents),
			local_events: Arc::new(LocalMetrics::new(0)),
			klines: None,
			lth: lth,
			sighup_pipe: sighup.0,
			persistent: false,
			downgrades: DowngradedAddresses::new(),
			cpu_deadline: 9_999_999_999_999_999_999,	//Use huge deadline by default.
		})
	}
	//Set the address prefixes to be downgraded.
	pub fn set_downgraded_prefixes(&mut self, pfx: &[[u8;12]])
	{
		self.downgrades = DowngradedAddresses::from_list(pfx);
	}
	//Set event loop to persist.
	//
	//Warning: Thread with persistent event loop will never die on its own, it must detached thread that is
	//eventually killed by exit().
	pub fn set_persistent(&mut self) { self.persistent = true; }
	//Loop over all events, until no listeners (dumped when sighup is asserted) or active connections remain.
	pub fn main_event_loop(&mut self)
	{
		let mut events = EventQueue::new(1024);
		//If all our file descriptors are gone, exit.
		while !self.connections.is_empty() || !self.listeners.is_empty() || self.tp_gate.is_some() ||
			self.persistent {
			if !self.dumped_listeners && self.sighup_flag.load(MemOrder::Relaxed) {
				//Ok, we got the signal that we should begin draining.
				syslog!(NOTICE "{pfx}Dumping listeners, waiting for all connections to end...",
					pfx=self.idprefix);
				for i in self.listeners.iter() {
					//For each listener, before dumping them, decrement the fd count and
					//unregister the poll. The token is dumped automatically.
					decrement_count(thread_local_fd_count());
					let _ = self.poll.deregister_listen(&((i.1).0).socket);
					self.local_events.watchdog_event();
				}
				//Dump TP gate.
				self.tp_gate = None;
				//Dump all the listeners.
				self.listeners.clear();
				//Send quit event to all connections.
				for (_, conn) in self.connections.iter_mut() {
					//Do not care if this succeeds or not.
					self.local_events.watchdog_event();
					catch_unwind(AssertUnwindSafe(||conn.upper.handle_quit_event())).ok();
				}
				//Okay, processed it already.
				self.dumped_listeners = true;
				continue;
			}
			//Calculate the maximum time to wait (until next timed event), and wait for event.
			let next_event: Option<PointInTime> = self.event_q.iter().next().map(|x|x.clone().0);
			let mut to_wait = next_event.map(|nextev|nextev.time_to().unwrap_or(
				Duration::from_secs(0)));
			//If there are pending unsafe interrupts, wait duration needs to be capped to 0.
			if self.has_unsafe_interrupts() { to_wait = Some(Duration::from_secs(0)); }
			self.local_events.go_to_sleep();
			let r = self.poll.poll(&mut events, to_wait);
			self.local_events.wake_up();
			if let Err(err) = r {
				if is_fatal_error(&err) { syslog!(ERROR "Error in poll: {err}"); }
				continue;
			}
			//Polling can change the clock a lot, so reread the clock.
			let now = PointInTime::now();
			for event in events.iter() {
				self.handle_event(event, now);
			}
			self.broadcast_timed_event(now);
			//In case broacasting timed events caused unsafe interrupts.
			self.process_unsafe_interrupts(now);
		}
		//Signal that dead thread is sleeping, in order to avoid load running away.
		self.local_events.go_to_sleep();
		self.lth.set_owned(false);
	}
	pub fn set_cpu_deadline(&mut self, dl: u64) { self.cpu_deadline = dl; }
	pub fn set_tp_gate(&mut self, fd: FileDescriptor)
	{
		let fd2 = FdListener::new_fd(fd.clone());
		fd2.set_nonblock().ok();		//Avoid hang on race.
		let token = match self.tpool.allocate() {
			Ok(x) => x,
			Err(_) => return syslog!(ERROR "Can't allocate token for listener")
		};
		if let Err(err) = self.poll.register_listen(&fd2, token.value(), IO_WAIT_READ) {
			return syslog!(ERROR "poll.register (listener) error: {err}");
		}
		self.tp_gate = Some((token, fd));
	}
	pub fn add_listener(&mut self, listener: Listener)
	{
		let token = match self.tpool.allocate() {
			Ok(x) => x,
			Err(_) => return syslog!(ERROR "Can't allocate token for listener")
		};
		let listener_seq = token.value();
		listener.socket.set_nonblock().ok();		//Avoid hang on race.
		if let Err(err) = self.poll.register_listen(&listener.socket, listener_seq, IO_WAIT_READ) {
			return syslog!(ERROR "poll.register (listener) error: {err}");
		}
		//We count this listener to ourselves.
		increment_count(thread_local_fd_count());
		let laddr = listener.local_addr_ex().canonicalize();
		let lfd = listener.as_raw_fd();
		self.listeners.insert(listener_seq, (listener, token));
		syslog!(NOTICE "Listening sokcet(fd={pfx}{lfd}): {laddr}", pfx=self.idprefix);
	}
	pub fn add_gate_listener(&mut self, kind: &[u8], listener: Listener)
	{
		//Gate listeners need no tokens.
		self.gate_listeners.insert(kind.to_owned(), listener);
	}
	pub fn set_pid(&mut self, pid: u64) { self.idprefix = ConnIdPrefix::Pid(pid); }
	pub fn set_prefix(&mut self, name: &'static str) { self.idprefix = ConnIdPrefix::Name(name); }
	//Has listeners?
	pub fn has_listeners(&self) -> bool { self.listeners.len() > 0 }
	pub fn remote_irq_handle(task: &AllocatedToken) -> RemoteIrq
	{
		RemoteIrq(REMOTE_HANDLE_OF_THIS_THREAD.with(|h|h.borrow_mut().clone()), task.value())
	}
	pub(crate) fn fire_interrupt(thread: &RemoteThreadHandle, task: usize) { thread.fire_interrupt(task); }
	pub(crate) fn fire_interrupt_thread_unsafe(task: usize)
	{
		THREAD_UNSAFE_INTERRUPTS.with(|y|y.borrow_mut().insert(task));
	}
	pub fn wake_up_all_event_loops()
	{
		LocalThreadHandle::for_each_remote_handle(|r|r.fire_interrupt(TOKEN_BROADCAST_WAKE));
	}
	pub fn set_events(&mut self, events: Box<dyn ListenerLoopEvents>)
	{
		//Swap locals to the new collector.
		self.local_events = events.locals();
		self.events = events;
	}
	pub fn get_token_pool(&self) -> TokenAllocatorPool { self.tpool.clone() }
	pub fn add_connection<'a>(&mut self, conn: Box<dyn UpperLayerConnection>,
		init_tokens: Cow<'a, [AllocatedToken]>, info: ConnectionInfo)
	{
		let now = PointInTime::now();
		let init_tokens = init_tokens.into_owned();
		let (next_seq, cfd) = f_return!(self.__do_spawn_connection_pe(init_tokens, conn, false, None, now));
		Self::new_connection_message(self.idprefix, next_seq, cfd, &info);
	}
	pub fn add_connection_with_poller<F>(&mut self, ctor: F)
		where F: FnOnce(&mut Poll) ->
		Option<(Box<dyn UpperLayerConnection>, Vec<AllocatedToken>, ConnectionInfo)>
	{
		if let Some((conn, tokens, info)) = ctor(Poll::__make(&mut self.poll)) {
			self.add_connection(conn, Cow::Owned(tokens), info);
		}
	}
	pub fn set_kline_db(&mut self, klines: Option<KlineDb>)
	{
		if let Some(klines) = klines {
			let gen = klines.get_generation();
			//Go through the new database.
			let mut to_kline: Vec<u64> = Vec::new();
			for (connid, addr) in self.srcaddrs.iter() {
				if klines.check_kline(*addr) { to_kline.push(*connid); }
			}
			for connid in to_kline.drain(..) { self.kill_connection(connid, KLINED); }
			self.klines = Some((klines, gen));
		} else {
			self.klines = None;	//Remove the database.
		}
	}
	fn broadcast_timed_event(&mut self, now: PointInTime)
	{
		while let Some((ts, connid)) = self.event_q.iter().next().map(|x|x.clone()) {
			if ts > now { break; }			//No more events to process.
			self.event_q.remove(&(ts, connid));		//Remove event about to be dispatched.
			let msg = if let Some(ref mut conn) = self.connections.get_mut(&connid) {
				let evq = &mut self.event_q;
				let poll = &mut self.poll;
				let local_events = &self.local_events;
				let cpu_deadline = self.cpu_deadline;
				//Enters task code, so need to switch the running task.
				match with_task(self.idprefix, conn.as_id, ||catch_unwind(AssertUnwindSafe(||{
					//Type inference.
					if false { return Err(Cow::Borrowed("")); }
					local_events.watchdog_event();
					let tracker = CpuCycleTracker::start(cpu_deadline);
					conn.upper.handle_timed_event(now, Poll::__make(poll))?;
					//No need to remove the event, because it was done above anyway.
					conn.next_event = conn.upper.get_next_timed_event(now);
					if let Some(ts) = conn.next_event.clone() { evq.insert((ts, connid)); }
					if let Some(dt) = tracker.end() {
						tooslow(local_events,  dt, "timed-event");
					}
					Ok(())
				}))) {
					//This connection should be killed, get the cause.
					Ok(Err(msg)) => Some(msg),
					//Ok, leave alone.
					Ok(Ok(())) => continue,
					//Ouch, we took a fatal exception in connection context. Try to
					//kill the connection and continue.
					Err(_) => {
						local_events.register_fault();
						None
					}
				}
			} else {
				continue;
			};
			//This connection needs to be killed due to fault.
			self.kill_connection(connid, msg.as_deref());
		}
	}
	fn __accept_from_gate<'a>(gate: &FileDescriptor, max_fd_count: usize, buffer: &'a mut [u8]) ->
		Option<(&'a [u8], FileDescriptor)>
	{
		let gate = match gate.as_unix_fd() { Some(x) => x, None => return None };
		let mut control = [MaybeUninit::uninit();128];		//Should be big enogugh
		let mut control = CmsgParser::new2(&mut control);
		if thread_local_fd_count().load(MemOrder::Relaxed) > max_fd_count {
			//Ignore message, as there are not enough fd slots. some other thread will
			//presumably get it.
			return None;
		}
		//Unsafety is having the gate open.
		let buffer = match unsafe{gate.recvmsg(false, &[&mut buffer[..]], Some(&mut control),
			Default::default())} {
			Ok((len, _)) => &buffer[..len],
			Err(ref e) if e.is_transient() => return None,	//Ignore temporary errors.
			Err(err) => {
				syslog!(WARNING "Reading gate failed: {err}");
				return None;
			}
		};
		let mut fd = -1;
		control.iterate(|mtype, payload|{
			if false { return Err(()); }		//Type inference.
			use ::btls_aux_unix::CmsgKind;
			if mtype == CmsgKind::SCM_RIGHTS {
				//Ok, pick the file descriptors.
				for i in 0..payload.len() / 4 {
					let mut fdx = [0u8;4];
					fdx.copy_from_slice(&payload[4*i..4*i+4]);
					let fdx = i32::from_le_bytes(fdx);
					if fd < 0 {
						fd = fdx;
					} else {
						//No space for this, close the fd.
						unsafe{UnixFileDescriptor::new(fdx)};
					}
				}
			}
			Ok(())
		}).ok();
		if fd < 0 { return None; }	//No file descriptor.
		let fd = FileDescriptor::new(fd);
		//Set CLOEXEC in case it is not already set.
		if let Err(err) = fd.set_cloexec(true) {
			syslog!(WARNING "Failed to set gate fd O_CLOEXEC: {err}");
		}
		Some((buffer, fd))
	}
	fn __check_gate_klines(klines: &Option<(KlineDb, usize)>, downgrades: &DowngradedAddresses,
		fd: &FileDescriptor) -> Option<()>
	{
		if let Some((ref kdb, _)) = klines.as_ref() {
			let addr = fd.remote_addr_ex().ok();
			let addr = addr.map(|addr|addr.downgrade(downgrades));
			let klined = match addr.as_ref() {
				Some(&SocketAddrEx::V4(x)) => kdb.check_kline(IpAddr::V4(*x.ip())),
				Some(&SocketAddrEx::V6(x)) => kdb.check_kline(IpAddr::V6(*x.ip())),
				_ => false	//Ignore non-IPv4/IPv6.
			};
			if !klined { return Some(()); }
			log!(NOTICE "Received K-Lined address {addr} from tp-gate, dropping",
				addr=addr.unwrap_or(SocketAddrEx::Unknown));
			return None;
		}
		Some(())
	}
	fn __process_kline_db_generation_change(&mut self)
	{
		let mut to_kline: Vec<u64> = Vec::new();
		if let Some(&mut (ref klines, ref mut kline_gen)) = self.klines.as_mut() {
			let kline_ngen = klines.get_generation();
			if *kline_gen != kline_ngen {
				*kline_gen = kline_ngen;
				for (connid, addr) in self.srcaddrs.iter() {
					if klines.check_kline(*addr) { to_kline.push(*connid); }
				}
			}
		}
		for connid in to_kline.drain(..) { self.kill_connection(connid, KLINED); }
	}
	fn __handle_connection_event(&mut self, parent: &mut Option<u64>, add_tokens: &mut AddTokenCallback,
		exit_msg: &mut Option<Option<Cow<'static, str>>>, addrs_new: &mut Vec<(u64, IpAddr)>, connid: u64,
		tok: usize, kind: u8, now: PointInTime) -> Option<()>
	{
		//Use this connection as parent for any possible spawns.
		*parent = Some(connid);
		//Ok, this is event related to a connection.
		let mut conn = self.connections.get_mut(&connid);
		let conn = match conn {
			Some(ref mut x) => x,
			None => {
				//Dangling token, delete.
				self.tokens.remove(&tok);
				return None;
			}
		};
		//Whatever the fault, this should be at least in good enough shape to tear down.
		let tmp_poll = &mut self.poll;
		let evq = &mut self.event_q;
		let local_events = &self.local_events;
		let cpu_deadline = self.cpu_deadline;
		let task_descriptor = ConnId::new(self.idprefix, conn.as_id);
		//Enters task code, so need to switch the running task.
		let old_task = set_current_task(Some(task_descriptor));
		*exit_msg = match catch_unwind(AssertUnwindSafe(||{
			local_events.watchdog_event();
			let tracker = CpuCycleTracker::start(cpu_deadline);
			//Even if we take socket fault (IO_WAIT_ERROR), do run normal event handler
			//once, as it fault might need to be read using SO_ERROR to get proper connection
			//end message. This can not loop, since IO_WAIT_ERROR will forcibly kill the
			//connection if it does not die with event handler.
			add_tokens.ignore_error = false;
			conn.upper.handle_event(Poll::__make(tmp_poll), tok, kind, task_descriptor, add_tokens)?;
			if kind & IO_WAIT_ERROR != 0 && !add_tokens.ignore_error {
				if let Some(dt) = tracker.end() { tooslow(local_events, dt, "fd-event"); }
				//If handle_fault does not give explicit error, use broken pipe.
				if let Some(error) = conn.upper.handle_fault(Poll::__make(tmp_poll)) {
					return Err(error);
				};
				fail!("Broken pipe");
			};
			//Update the event_q and timestamp.
			if let Some(ts) = conn.next_event.clone() { evq.remove(&(ts, connid)); }
			conn.next_event = conn.upper.get_next_timed_event(now);
			if let Some(ts) = conn.next_event.clone() { evq.insert((ts, connid)); }
			if let Some(dt) = tracker.end() { tooslow(local_events, dt, "fd-event"); }
			Ok(())
		})) {
			//If task end message is available, kill the task.
			Ok(Err(msg)) => Some(Some(msg)),
			//Ok, leave alone.
			Ok(Ok(())) => None,
			//Ouch, we took a fatal exception in connection context. Try to kill the
			//connection and continue.
			Err(_) => {
				local_events.register_fault();
				Some(None)
			}
		};
		set_current_task(old_task);
		//Add the address, if any got reported. All the addresses always get added to new.
		let set_addr = replace(&mut add_tokens.change_source_to, None);
		if let Some(addr) = set_addr { addrs_new.push((connid, addr)); }
		//Add the tokens.
		for i in add_tokens.tokens.drain(..) {
			self.tokens.insert(i.value(), connid);
			conn.tokens.push(i);
		}
		Some(())
	}
	fn __check_if_gate(&self, tok: usize) -> Option<FileDescriptor>
	{
		match &self.tp_gate {
			&Some((ref gtoken, ref gate)) if gtoken.value() == tok => Some(gate.clone()),
			_ => None
		}
	}
	fn __do_spawn_connection(&mut self, tokens: Vec<AllocatedToken>, mut conn: Box<dyn UpperLayerConnection>,
		accept: bool, parent: Option<u64>, now: PointInTime) -> Result<(u64, i32), Cow<'static, str>>
	{
		let next_seq = self.next_conn_seq;
		self.next_conn_seq += 1;
		//Add the tokens to lookup. Note that tokens may not be reused during lifetime of the
		//connection context, since ConnectionContext pins them.
		for i in tokens.iter() { self.tokens.insert(i.value(), next_seq); };
		//Need to call handle_init_event() on the connection.
		let poll = &mut self.poll;
		let idprefix = self.idprefix;
		let local_events = &self.local_events;
		let cpu_deadline = self.cpu_deadline;
		let rseq = parent.unwrap_or(next_seq);	//Masquerade replaces connid for handle_init_event().
		match with_task(idprefix, rseq, ||catch_unwind(AssertUnwindSafe(||{
			let tracker = CpuCycleTracker::start(cpu_deadline);
			conn.handle_init_event(ConnId::new(idprefix, rseq), Poll::__make(poll))?;
			//Probe for first timed event here, as there is perfectly good catch_unwind block.
			let ret = conn.get_next_timed_event(now);
			if let Some(dt) = tracker.end() { tooslow(local_events, dt, "init"); }
			local_events.new_task();
			Ok(ret)
		}))) {
			Ok(Ok(timed_ev)) => {
				if let Some(ts) = timed_ev.clone() { self.event_q.insert((ts, next_seq)); }
				let cfd = conn.get_main_fd();
				self.connections.insert(next_seq, ConnectionContext{
					upper: conn,
					tokens: tokens,
					next_event: timed_ev,
					incoming: accept,
					as_id: rseq,
				});
				Ok((next_seq, cfd))
			},
			Ok(Err(e)) => Err(e),
			Err(_) => {
				local_events.register_fault();
				Err(Cow::Borrowed("Fatal exception in handler"))
			}
		}
	}
	fn __do_spawn_connection_pe(&mut self, tokens: Vec<AllocatedToken>, conn: Box<dyn UpperLayerConnection>,
		accept: bool, parent: Option<u64>, now: PointInTime) -> Option<(u64, i32)>
	{
		match self.__do_spawn_connection(tokens, conn, accept, parent, now) {
			Ok(x) => Some(x),
			Err(err) => {
				syslog!(WARNING "Failed to initialize connection: {err}");
				None
			}
		}
	}
	fn __return_from_accept(&mut self, ret: Result<NewConnection, Cow<'static, str>>,
		addrs_new: &mut Vec<(u64, IpAddr)>, now: PointInTime) -> Option<()>
	{
		match ret {
			Ok(nc) => {
				let (next_seq, cfd) = self.__do_spawn_connection_pe(nc.tokens, nc.conn, true,
					None, now)?;
				if let (true, Some(addr)) = (nc.addr_valid, trans_srcaddr(&nc.info.src)) {
					//If addr_valid is set, we know the source address of this
					//connection is what is in info.src. Address gets added to new.
					addrs_new.push((next_seq, addr));
				}
				self.events.committed();
				ListenerLoop::new_connection_message(self.idprefix, next_seq, cfd,
					&nc.info);
			},
			Err(err) => {
				if err.deref().len() > 0 { syslog!(WARNING "Failed to accept connection: {err}"); }
				return None;
			}
		};
		Some(())
	}
	fn __handle_gate_event(&mut self, gate: FileDescriptor, addrs_new: &mut Vec<(u64, IpAddr)>,
		now: PointInTime) -> Option<()>
	{
		let mut buffer = [0;128];
		let (buffer, fd) = Self::__accept_from_gate(&gate, self.max_fd_count, &mut buffer)?;
		Self::__check_gate_klines(&self.klines, &self.downgrades, &fd)?;
		//Look up correct gate type and then listener for that.
		if let Some(ref mut listener) = self.gate_listeners.get_mut(buffer) {
			let r = listener.spawn(Poll::__make(&mut self.poll), &mut self.tpool, fd,
				self.events.deref(), &self.downgrades);
			self.__return_from_accept(r, addrs_new, now)?;
		} else {
			syslog!(NOTICE "Dropped connection of unknown type {buffer:?} from tp-gate");
		}
		Some(())
	}
	fn __spawn_new_connections(&mut self, mut add_tokens: AddTokenCallback, parent: Option<u64>,
		now: PointInTime)
	{
		for (conn, tokens, info, masquerade) in add_tokens.spawns.drain(..) {
			//If not masquerading, the connection has no parent. This way parent is some only if
			//one is to masquerade as it.
			let parent = if masquerade { parent } else { None };
			let (next_seq, cfd) = f_continue!(self.__do_spawn_connection_pe(tokens, conn, false, parent,
				now));
			if let Some(parent) = parent {
				//Record the backward masquerade. The forward one has already been recorded by
				//spawn_connection!().
				self.masquerade_bwd.entry(parent).or_insert_with(||Vec::new()).push(next_seq);
			} else {
				Self::new_connection_message(self.idprefix, next_seq, cfd, &info);
			}
		}
	}
	fn __do_kline_new_connections(&mut self, mut addrs_new: Vec<(u64, IpAddr)>)
	{
		//If connection is not K-lined, copy its source address to self.srcaddrs.
		for (connid, addr) in addrs_new.drain(..) {
			if self.klines.as_ref().map(|klines|klines.0.check_kline(addr)).unwrap_or(false) {
				self.kill_connection(connid, KLINED);
			} else {
				self.srcaddrs.insert(connid, addr);
			}
		}
	}
	fn __handle_event_head(&mut self) -> AddTokenCallback
	{
		let add_tokens = AddTokenCallback{
			allocator: self.tpool.clone(),
			tokens: Vec::new(),
			spawns: Vec::new(),
			change_source_to: None,
			ignore_error: false,
		};
		self.__process_kline_db_generation_change();
		add_tokens
	}
	fn __handle_event_tail(&mut self, tok: usize, add_tokens: AddTokenCallback, addrs_new: Vec<(u64, IpAddr)>,
		exit_msg: Option<Option<Cow<'static, str>>>, parent: Option<u64>, now: PointInTime)
	{
		self.__spawn_new_connections(add_tokens, parent, now);
		self.__do_kline_new_connections(addrs_new);
		//If something did go wrong with the connection, kill it.
		if let Some(exit_msg) = exit_msg {
			let connid = f_return!(self.tokens.get(&tok).map(|x|*x));
			self.kill_connection(connid, exit_msg.as_deref());
		}
	}
	fn _handle_event(&mut self, tok: usize, kind: u8, now: PointInTime)
	{
		let mut addrs_new = Vec::new();
		let mut exit_msg = None;
		let mut parent = None;
		let mut add_tokens = self.__handle_event_head();
		if let Some(connid) = self.tokens.get(&tok).map(|x|*x) {
			f_return!(self.__handle_connection_event(&mut parent, &mut add_tokens, &mut exit_msg,
				&mut addrs_new, connid, tok, kind, now));
		} else if let Some(gate) = self.__check_if_gate(tok) {
			f_return!(self.__handle_gate_event(gate, &mut addrs_new, now));
		} else if let Some(ref mut listener) = self.listeners.get_mut(&tok) {
			//Listener got connection.
			let r = listener.0.accept(Poll::__make(&mut self.poll), &mut self.tpool, self.max_fd_count,
				self.events.deref(), &self.downgrades);
			f_return!(self.__return_from_accept(r, &mut addrs_new, now));
		} else {
			//We have no idea what this is for. Ignore it.
		}
		self.__handle_event_tail(tok, add_tokens, addrs_new, exit_msg, parent, now);
	}
	fn handle_event(&mut self, ev: Event, now: PointInTime)
	{
		let tok = ev.token();
		let kind = ev.ready();
		//If this is SIGHUP on extra pipe, get rid of it to avoid pegging the CPU. Jettisoning the handle
		//object closes the file descriptor.
		if kind & IO_WAIT_HUP != 0 && tok == TOKEN_SIGHUP {
			let _ = Poll::__make(&mut self.poll).deregister(&self.sighup_pipe);
			self.sighup_pipe = FileDescriptor::blank();
		}
		if tok == TOKEN_IRQRECV {
			//Handle reads on IRQ fd specially, dispatching any subevents within. Use event type of
			//IO_WAIT_INTR, since these are interrupts.
			return LocalThreadHandle::handle_irq_event(self, IO_WAIT_INTR, now);
		}
		self._handle_event(tok, kind, now);
		//We do not need to process unsafe interrupts here, all are processed after handling timed
		//events.
	}
	fn process_unsafe_interrupts(&mut self, now: PointInTime)
	{
		//While THREAD_UNSAFE_INTERRUPTS contains stuff, process it. The kind of these events is always
		//IO_WAIT_INTR, since they are interrupts. Furthermore, these are always directed at connections.
		loop {
			let mut addrs_new = Vec::new();
			let mut exit_msg = None;
			let mut parent = None;
			let mut id = None;
			THREAD_UNSAFE_INTERRUPTS.with(|y|{
				let mut y = y.borrow_mut();
				let id2: Option<usize> = y.iter().cloned().next();
				if let Some(id) = id2 { y.remove(&id); }
				id = id2;
			});
			let id = f_break!(id);
			let mut add_tokens = self.__handle_event_head();
			if let Some(connid) = self.tokens.get(&id).map(|x|*x) {
				f_continue!(self.__handle_connection_event(&mut parent, &mut add_tokens,
					&mut exit_msg, &mut addrs_new, connid, id, IO_WAIT_INTR, now));
			}
			self.__handle_event_tail(id, add_tokens, addrs_new, exit_msg, parent, now);
		}
	}
	fn has_unsafe_interrupts(&mut self) -> bool
	{
		THREAD_UNSAFE_INTERRUPTS.with(|y|{
			let y = y.borrow();
			y.iter().cloned().next().is_some()
		})
	}
	fn kill_connection(&mut self, connid: u64, errmsg: Option<&str>)
	{
		if let Some(ref mut conn) = self.connections.get_mut(&connid) {
			//Assume errmsg=None means fatal exception.
			let fatal = errmsg.is_none();
			let cause = errmsg.unwrap_or("Fatal exeception in connection context");
			let event_q = &mut self.event_q;
			let local_events = &self.local_events;
			let poll = &mut self.poll;
			//Note that if there is masquerade, it should only affect messages, not what cleanups
			//are done.
			with_task(self.idprefix, conn.as_id, ||{
				local_events.watchdog_event();
				//If connid == conn.as_id, there is no masquerade taking place, so end message may
				//be printed. fatal means this is fatal exception, and that should be printed
				//regardless.
				if connid == conn.as_id || fatal {
					//This is connection-specific.
					log!(INFO "Connection ending: {cause}");
				}
				//Kill the timed event from queue.
				if let Some(ts) = conn.next_event.clone() { event_q.remove(&(ts, connid)); }
				//Kill the pollers and tokens, also destroys the service context.
				conn.upper.remove_pollers(Poll::__make(poll));
			});
			for i in conn.tokens.iter() { self.tokens.remove(&i.value()); }
			if conn.incoming { self.events.retired(); }
		}
		//Finally, destroy the connection structure, and possible address. If connection gets dropped,
		//signal dropped task.
		self.srcaddrs.remove(&connid);
		if self.connections.remove(&connid).is_some() { self.local_events.drop_task(); }
		//Destroy any connections that were masquerading as this connection.
		let mut masquerading = self.masquerade_bwd.remove(&connid).unwrap_or(Vec::new());
		for masquerader in masquerading.drain(..) {
			//The message should never be printed anyway.
			self.kill_connection(masquerader, Some(""));
		}
	}
	fn new_connection_message(idprefix: ConnIdPrefix, seq: u64, cfd: i32, info: &ConnectionInfo)
	{
		//Can ignore masquerade because this should never be called on such connections.
		with_task(idprefix, seq, ||{
			//This is connection-specific.
			if cfd >= 0 {
				log!(INFO "New connection(fd={cfd}): {src} -> {dst}", src=info.src, dst=info.dest);
			} else {
				log!(INFO "New task: {dst}", dst=info.dest);
			}
		});
	}
}
