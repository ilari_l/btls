use std::cmp::min;
use std::cmp::Ordering;
use std::net::IpAddr;
use std::net::Ipv6Addr;
use std::str::FromStr;
use std::usize::MAX as USIZE_MAX;


#[derive(Clone,Debug)]
pub struct IpSet(Vec<Entry>);

impl IpSet
{
	pub fn new() -> IpSet { IpSet(Vec::new()) }
	pub fn add(&mut self, ip: IpAddr, mask: u8, polarity: bool)
	{
		let (mut ip, mask) = match ip {
			IpAddr::V6(x) => (x.octets(), mask),
			IpAddr::V4(x) => (x.to_ipv6_mapped().octets(), mask.saturating_add(96))
		};
		//Mask out junk bits, as compare in search does not like those.
		for i in (mask as usize / 8 + 1)..16 { ip[i] = 0; }
		if mask % 8 > 0 && mask < 128 { ip[mask as usize / 8] &= 255 << 8 - mask % 8; }
		//Push the entry. Use parent USIZE_MAX, as it is filled later.
		self.0.push(Entry{ip: ip, mask: min(mask, 128), polarity: polarity, parent: USIZE_MAX});
	}
	pub fn freeze(&self) -> FrozenIpSet
	{
		let mut v = self.0.clone();
		//Sort the entries, so binary search can be used. Then remove duplicates. And finally generate
		//parent relationships.
		v.sort();
		v.dedup();
		_fill_parents(&mut v, USIZE_MAX, None, &mut 0);
		FrozenIpSet(v)
	}
}

fn compare_ip_address(a: [u8;16], b: [u8;16], mask: u8) -> Ordering
{
	let prefixlen = min(mask as usize / 8, 16);
	let c1 = a[..prefixlen].cmp(&b[..prefixlen]);
	if c1.is_ne() { return c1; }
	if mask % 8 > 0 && mask < 128 {
		let submask = 255 << 8 - mask % 8;
		(a[prefixlen] & submask).cmp(&(b[prefixlen] & submask))
	} else {
		Ordering::Equal
	}
}

#[derive(Copy,Clone,Eq,Debug)]
struct Entry
{
	ip: [u8;16],
	mask: u8,
	parent: usize,
	polarity: bool,
}

impl PartialOrd for Entry
{
	fn partial_cmp(&self, athr: &Entry) -> Option<Ordering> { Some(self.cmp(athr)) }
}

impl Ord for Entry
{
	fn cmp(&self, athr: &Entry) -> Ordering
	{
		//First compare masked addresses, and if equal, order by mask length. Two addresses will compare
		//unequal if mask lengths are different, even if masked addresses are the same. And these sort
		//in order of mask length.
		compare_ip_address(self.ip, athr.ip, min(self.mask, athr.mask)).
			then_with(||self.mask.cmp(&athr.mask))
	}
}

impl PartialEq for Entry
{
	fn eq(&self, athr: &Entry) -> bool { self.cmp(athr).is_eq() }
}

fn starts_with(child: Entry, parent: Option<Entry>) -> bool
{
	if let Some(parent) = parent {
		parent.mask < child.mask && compare_ip_address(parent.ip, child.ip, parent.mask).is_eq()
	} else {
		true	//Initial entry is super-root.
	}
}

fn _fill_parents(tree: &mut Vec<Entry>, parent: usize, entry: Option<Entry>, index: &mut usize)
{
	while *index < tree.len() && starts_with(tree[*index], entry) {
		//If entry has the same polarity as its parent (the super-root is of negative polarity), remove
		//it as it is redundant.
		if tree[*index].polarity == entry.map(|p|p.polarity).unwrap_or(false) {
			tree.remove(*index);
			continue;
		}
		tree[*index].parent = parent;
		//Attempt to run subordinates from position index + 1 with index as parent. In any case, index has
		//been used.
		let entry = tree[*index];
		let parent = *index;
		*index += 1;
		_fill_parents(tree, parent, Some(entry), index);
	}
}


#[derive(Clone,Debug)]
pub struct FrozenIpSet(Vec<Entry>);

impl FrozenIpSet
{
	pub fn new() -> FrozenIpSet { FrozenIpSet(Vec::new()) }
	pub fn nontrivial(&self) -> bool { self.0.len() > 0 }
	pub fn lookup(&self, ip: IpAddr) -> bool
	{
		let ip = match ip {
			IpAddr::V6(x) => x.octets(),
			IpAddr::V4(x) => x.to_ipv6_mapped().octets(),
		};
		let mut pos = match self.0.binary_search_by(|a|a.ip.cmp(&ip)) {
			Err(0) => return false,			//Before first, definitely not in set.
			Err(x) => x - 1,			//Start search from one before.
			Ok(x) => return self.0[x].polarity	//Exact hit.
		};
		while pos < self.0.len() {
			let entry = &self.0[pos];
			//Does this entry match?
			if compare_ip_address(ip, entry.ip, entry.mask).is_eq() {
				return entry.polarity;		//HIT.
			}
			//No match, scan parent.
			pos = entry.parent;
		}
		//Not in set.
		false
	}
}

pub fn parse_netmask(line: &str) -> Option<(Ipv6Addr, u8)>
{
	//Has /?
	Some(if let Some(split) = line.find('/') {
		//split is guaranteed to be valid by definition of find.
		let (prefix, delta) = match IpAddr::from_str(&line[..split]).ok()? {
			IpAddr::V6(x) => (x, 0u8),
			IpAddr::V4(x) => (x.to_ipv6_mapped(), 96u8),
		};
		let pfxlen = delta.saturating_add(u8::from_str(&line[split+1..]).ok()?);
		fail_if_none!(pfxlen > 128);	//Bad.
		(prefix, pfxlen)
	} else {
		match IpAddr::from_str(line).ok()? {
			IpAddr::V6(x) => (x, 128),
			IpAddr::V4(x) => (x.to_ipv6_mapped(), 128),
		}
	})
}

#[cfg(test)]
fn ipset_test(config: &[(&'static str, u8, bool)], size: usize, tests: &[(&'static str, bool)])
{
	let mut ipset = IpSet::new();
	for (ip, mask, polarity) in config { ipset.add(IpAddr::from_str(ip).unwrap(), *mask, *polarity); }
	let ipset = ipset.freeze();
	println!("{ipset:?}");
	assert_eq!(ipset.0.len(), size);
	for (ip, polarity) in tests {
		assert!(ipset.lookup(IpAddr::from_str(ip).unwrap()) == *polarity);
	}
}

#[test]
fn test_ipset_nested()
{
	ipset_test(&[
		("fc00::", 7, true),
		("fc00::", 8, false),
	], 2, &[
		("::", false),
		("fbff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
		("fc00::", false),
		("fcff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
		("fd00::", true),
		("fdff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", true),
		("fe00::", false),
		("ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
	]);
}

#[test]
fn test_ipset_nested_rootelim()
{
	ipset_test(&[
		("fc00::", 7, false),
		("fc00::", 8, true),
	], 1, &[
		("::", false),
		("fbff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
		("fc00::", true),
		("fcff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", true),
		("fd00::", false),
		("fdff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
		("fe00::", false),
		("ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
	]);
}

#[test]
fn test_ipset_childelim1()
{
	ipset_test(&[
		("fc00::", 7, true),
		("fc00::", 8, true),
	], 1, &[
		("::", false),
		("fbff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
		("fc00::", true),
		("fcff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", true),
		("fd00::", true),
		("fdff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", true),
		("fe00::", false),
		("ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
	]);
}

#[test]
fn test_ipset_childelim2()
{
	ipset_test(&[
		("fc00::", 7, true),
		("fd00::", 8, true),
	], 1, &[
		("::", false),
		("fbff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
		("fc00::", true),
		("fcff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", true),
		("fd00::", true),
		("fdff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", true),
		("fe00::", false),
		("ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
	]);
}

#[test]
fn test_ipset_childelim1b()
{
	ipset_test(&[
		("fc00::", 7, true),
		("fc00::", 8, true),
		("fc00::", 9, false),
	], 2, &[
		("::", false),
		("fbff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
		("fc00::", false),
		("fc7f:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
		("fc80::", true),
		("fcff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", true),
		("fd00::", true),
		("fdff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", true),
		("fe00::", false),
		("ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
	]);
}

#[test]
fn test_ipset_childelim2b()
{
	ipset_test(&[
		("fc00::", 7, true),
		("fd00::", 8, true),
		("fd40::", 10, false),
	], 2, &[
		("::", false),
		("fbff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
		("fc00::", true),
		("fcff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", true),
		("fd00::", true),
		("fd3f:ffff:ffff:ffff:ffff:ffff:ffff:ffff", true),
		("fd40::", false),
		("fd7f:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
		("fd80::", true),
		("fdff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", true),
		("fe00::", false),
		("ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
	]);
}

#[test]
fn test_ipset_childelim2b2()
{
	ipset_test(&[
		("fc00::", 7, true),
		("fd00::", 8, true),
		("fd00::", 9, true),
		("fd40::", 10, false),
	], 2, &[
		("::", false),
		("fbff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
		("fc00::", true),
		("fcff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", true),
		("fd00::", true),
		("fd3f:ffff:ffff:ffff:ffff:ffff:ffff:ffff", true),
		("fd40::", false),
		("fd7f:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
		("fd80::", true),
		("fdff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", true),
		("fe00::", false),
		("ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
	]);
}

#[test]
fn ip_comprare_with_junk()
{
	let a = Entry{ip: [0xfd,0x74,0,0,0,0,0,0,0,0,0,0,0,0,0,0], mask:7, polarity:true, parent:USIZE_MAX};
	let b = Entry{ip: [0xfd,0x54,0,0,0,0,0,0,0,0,0,0,0,0,0,0], mask:10, polarity:true, parent:USIZE_MAX};
	let c = Entry{ip: [0xfd,0x54,0,0,0,0,0,0,0,0,0,0,0,0,0,0], mask:7, polarity:true, parent:USIZE_MAX};
	assert!(b > a);
	assert!(c == a);
}

#[test]
fn test_ipset_childelim2b2_junk()
{
	ipset_test(&[
		("fd74::", 7, true),
		("fd54::", 8, true),
		("fd3a::", 9, true),
		("fd7b::", 10, false),
	], 2, &[
		("::", false),
		("fbff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
		("fc00::", true),
		("fcff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", true),
		("fd00::", true),
		("fd3f:ffff:ffff:ffff:ffff:ffff:ffff:ffff", true),
		("fd40::", false),
		("fd7f:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
		("fd80::", true),
		("fdff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", true),
		("fe00::", false),
		("ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
	]);
}

#[test]
fn test_ipset_all()
{
	ipset_test(&[
		("::", 0, true),
	], 1, &[
		("::", true),
		("ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", true),
	]);
}

#[test]
fn test_ipset_all_ipv4()
{
	ipset_test(&[
		("0.0.0.0", 0, true),
	], 1, &[
		("::", false),
		("::feff:ffff:ffff", false),
		("::ffff:0:0", true),
		("::ffff:ffff:ffff", true),
		("::1:0:0:0", false),
		("ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
		("0.0.0.0", true),
		("255.255.255.255", true),
	]);
}

#[test]
fn test_ipset_all_loopback()
{
	ipset_test(&[
		("127.0.0.0", 8, true),
	], 1, &[
		("::", false),
		("::feff:ffff:ffff", false),
		("::ffff:0:0", false),
		("::ffff:7eff:0", false),
		("::ffff:7f00:0", true),
		("::ffff:7fff:ffff", true),
		("::ffff:8000:0", false),
		("::ffff:ffff:ffff", false),
		("::1:0:0:0", false),
		("ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", false),
		("0.0.0.0", false),
		("126.255.255.255", false),
		("127.0.0.0", true),
		("127.255.255.255", true),
		("128.0.0.0", false),
		("255.255.255.255", false),
	]);
}

#[test]
fn test_ipset_one_after_exceptions()
{
	let mut ipset = IpSet::new();
	ipset.0.push(Entry{ip:[0;16], mask:0, polarity:true, parent:USIZE_MAX});
	for i in 0..128 {
		let i = 127 - i;
		let mut ip = [0;16];
		for j in 0..i { ip[j/8] |= 1 << 7-j%8; }
		ipset.0.push(Entry{ip:ip, mask:i as u8 + 1, polarity:false, parent:USIZE_MAX});
	}
	//println!("{ipset:?}");
	let ipset = ipset.freeze();
	println!("{ipset:?}");
	assert_eq!(ipset.0.len(), 129);
	let tests = [
		("::", false),
		("8000::", false),
		("c000::", false),
		("e000::", false),
		("f000::", false),
		("ffff:ffff:ffff:ffff:ffff:ffff:ffff:fff0", false),
		("ffff:ffff:ffff:ffff:ffff:ffff:ffff:fff8", false),
		("ffff:ffff:ffff:ffff:ffff:ffff:ffff:fffc", false),
		("ffff:ffff:ffff:ffff:ffff:ffff:ffff:fffe", false),
		("ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", true),
	];
	for (ip, polarity) in tests.iter() {
		assert!(ipset.lookup(IpAddr::from_str(ip).unwrap()) == *polarity);
	}
}
