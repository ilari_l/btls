use super::catch_sighup_default;
use super::catch_sigpipe_default;
use super::FileDescriptor;
use super::generate_launch_ack;
use super::get_sighup_flag;
use super::is_fatal_error;
use super::setup_sighup_pipe;
use super::SocketAddrEx;
use btls_aux_fail::ResultExt;
use btls_aux_memory::ByteStringLines;
use btls_aux_memory::EscapeByteString;
use btls_aux_time::PointInTime;
use btls_aux_time::Timer;
use btls_aux_time::TimeUnit;
use btls_aux_unix::AccessFlags;
use btls_aux_unix::FileDescriptor as UnixFileDescriptor;
use btls_aux_unix::FileDescriptorB as UnixFileDescriptorB;
use btls_aux_unix::Gid;
use btls_aux_unix::LogFacility;
use btls_aux_unix::OpenFlags;
use btls_aux_unix::OpenlogFlags;
use btls_aux_unix::OsError;
use btls_aux_unix::Path as UnixPath;
use btls_aux_unix::Pid;
use btls_aux_unix::PipeFlags;
use btls_aux_unix::PollFd;
use btls_aux_unix::PollFlags;
use btls_aux_unix::RawFdT;
use btls_aux_unix::RawSignalT;
use btls_aux_unix::setsid;
use btls_aux_unix::Signal;
use btls_aux_unix::SignalHandler;
use btls_aux_unix::SignalSet;
use btls_aux_unix::SocketFamily;
use btls_aux_unix::SocketType;
use btls_aux_unix::SpecialFd;
use btls_aux_unix::systemd_is_notify;
use btls_aux_unix::Uid;
use btls_aux_unix::Umask;
use btls_aux_unix::WaitAnyChild;
use btls_aux_unix::WaitStatus;
use btls_aux_unix::WaitPidFlags;
use btls_util_logging::call_openlog;
use btls_util_logging::call_openlog_systemd;
use btls_util_logging::do_log_line_raw0;
use btls_util_logging::log_nodetach;
use btls_util_logging::log_sink_get_parent;
use btls_util_logging::log_subprocess;
use btls_util_logging::LogPermissions;
use btls_util_logging::set_log_permissions;
use btls_util_logging::syslog;
use connectionmux::LocalThreadHandle;
use connectionmux::LocalThreadHandleGlobal;
use connectionmux::SighupPipe;
use std::borrow::Cow;
use std::cmp::max;
use std::cmp::min;
use std::collections::HashSet;
use std::env::args_os;
use std::env::set_current_dir;
use std::env::var_os;
use std::ffi::OsString;
use std::fs::File;
use std::fs::remove_file;
use std::io::Error as IoError;
use std::io::Read;
use std::io::Write;
use std::mem::forget;
use std::mem::MaybeUninit;
use std::mem::swap;
use std::net::Ipv4Addr;
use std::net::Ipv6Addr;
use std::net::SocketAddr;
use std::net::SocketAddrV4;
use std::net::SocketAddrV6;
use std::net::ToSocketAddrs;
use std::ops::Deref;
use std::os::unix::ffi::OsStringExt;
use std::path::Path;
use std::path::PathBuf;
use std::process::exit;
use std::ptr::copy;
use std::str::from_utf8;
use std::str::FromStr;
use std::sync::Arc;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering as MemOrdering;
use std::thread::Builder;
use std::thread::sleep;
use std::time::Duration;


const MAXLOGLINE: usize = 8192;

fn os_error_to_io_error(e: OsError) -> IoError { IoError::from_raw_os_error(e.to_inner()) }

fn get_arguments() -> Vec<OsString>
{
	let ret: Vec<_> = args_os().collect();
	if ret.len() == 2 {
		let mut content = String::new();
		if File::open(ret[1].deref()).and_then(|mut fp|fp.read_to_string(&mut content)).is_err() {
			return ret;	//If not valid filename, return raw args.
		}
		let mut args = Vec::new();
		for (lnum, line) in content.lines().enumerate() {
			let a = if lnum == 0 { line.strip_prefix("#!").unwrap_or(line) } else { line };
			args.push(OsString::from(a.to_owned()));
		}
		return args;
	}
	ret
}

fn find_lf(x: &[u8]) -> Option<usize>
{
	for (i, c) in x.iter().cloned().enumerate() { if c == 10 { return Some(i); }}
	None
}

struct StartupInfo
{
	sockets: Vec<RawFdT>,
	threads: u32,
	launch_ack: Option<i32>,
}

pub struct SystemdStartupInfo
{
	sockets: Vec<RawFdT>,
	threads: u32,
	launch_ack: Option<i32>,
	glth: LocalThreadHandleGlobal,
}

impl SystemdStartupInfo
{
	pub fn sockets(&self) -> Vec<RawFdT> { self.sockets.clone() }
	//Spawn a daemon thread. The daemon thread is passed LocalThreadHandle for itself, so it can create an
	//event loop and target IRQs on it.
	pub fn spawn_daemon_thread<F>(&self, name: &str, f: F) -> Result<(), Cow<'static, str>>
		where F: FnOnce(LocalThreadHandle), F: Send+'static
	{
		let b = Builder::new();
		let b = b.name(name.to_owned());
		let glth = self.glth.clone();
		b.spawn(move || {
			//LocalThreadHandle is not allowed to move between threads anyway.
			//If glth.need_pipes() is false, the rp and wp will just be ignored.
			let lth = LocalThreadHandle::new(glth);
			f(lth);
		}).map_err(|err|{
			format!("Failed to spawn thread: {err}")
		})?;
		Ok(())
	}
}

pub trait ThreadMetrics
{
	fn create(ti: usize) -> Self;
}

pub trait ThreadLocal
{
	fn new(ti: usize) -> Self;
}

impl ThreadLocal for ()
{
	fn new(_: usize) -> () {}
}

impl ThreadMetrics for ()
{
	fn create(_: usize) -> () {}
}

pub struct ThreadNewParams<'a,T:Thread>
{
	pub gparameters: <T as Thread>::Parameters,
	pub lparameters: &'a <T as Thread>::Local,
	pub lmetrics: Arc<<T as Thread>::LocalMetrics>,
	pub sigpipe: SighupPipe,
	pub irqpipe: LocalThreadHandle,
	pub tid: Option<u64>,
	_dummy: (),
}

pub struct ThreadSpawnedParams<T:Thread>
{
	pub gparameters: <T as Thread>::Parameters,
	pub gparameters2: <T as Thread>::Parameters2,
	pub contexts: Arc<Vec<Arc<<T as Thread>::LocalMetrics>>>,
	_dummy: (),
}

pub trait Thread: Sized
{
	///Per-thread parameters to pass to new() function.
	type Local: ThreadLocal+Send+Sync+'static;
	///Global parameters to pass to new() and spawned() functions.
	type Parameters: Clone+Send+Sized+'static;
	///Global parameters to pass to the spawned() function.
	type Parameters2: Sized;
	///Local metrics to pass to the new() function and as array to spawned() function.
	type LocalMetrics: ThreadMetrics+Send+Sync+Sized+'static;
	///Called once per thread to create the object. Assumed that if this returns, the thread is ready to
	///run.
	fn new<'a>(p: ThreadNewParams<'a,Self>) -> Self;
	///Called after reporting successful thread launch.
	fn run(self);
	///Called once on main thrad once all threads have been spawned and have reported successful startup.
	fn spawned(p: ThreadSpawnedParams<Self>);
}

pub fn start_threads<T:Thread>(info: SystemdStartupInfo, name: &str, param: T::Parameters,
	param2: T::Parameters2)
{
	let glth = info.glth.clone();
	//Register SIGHUP handler that triggers drain. And ignore SIGPIPE.
	catch_sighup_default();
	catch_sigpipe_default();
	let rsigpipe = setup_sighup_pipe().unwrap_or_else(|err|{
		abort!("Failed to create signal pipe: {err}")
	});

	let allocate = max(info.threads, 1) as usize;
	let mut data = Vec::with_capacity(allocate);
	let mut metrics = Vec::with_capacity(allocate);
	for ti in 0..allocate { data.push(T::Local::new(ti)); }
	for ti in 0..allocate { metrics.push(Arc::new(T::LocalMetrics::create(ti))); }
	let data = Arc::new(data);
	let metrics = Arc::new(metrics);

	//Handle threaded mode if enabled.
	if info.threads > 0 {
		return threadmode_launch::<T>(info, name, param, param2, data, metrics, rsigpipe, glth);
	}

	//This is the non-threaded case.
	let rirqpipe = LocalThreadHandle::new(glth);
	let thread = T::new(ThreadNewParams {
		gparameters: param.clone(),
		lparameters: &data[0],
		irqpipe: rirqpipe,
		lmetrics: metrics[0].clone(),
		sigpipe: SighupPipe::new(get_sighup_flag(), rsigpipe),
		tid: None,
		_dummy: ()
	});
	T::spawned(ThreadSpawnedParams {
		gparameters: param,
		gparameters2: param2,
		contexts: metrics,
		_dummy: ()
	});
	//Acknowledge the successful launch.
	generate_launch_ack(info.launch_ack);
	thread.run();
	//Don't generate any more events.
	syslog!(NOTICE "Daemon main has ended, exiting.");
}

fn for_each_option<F>(mut f: F) where F: FnMut(&str)
{
	for (idx, arg) in get_arguments().iter().enumerate() {
		if idx == 0 { continue; }
		let arg = match arg.to_str() { Some(x) => x, None => continue };
		f(arg)
	}
}

static LISTEN_FD: &'static str = "--listen-fd=";
static ACK: &'static str = "--ack=";
static LOG_AS: &'static str = "--log-as=";
static THREADS: &'static str = "--threads=";
static NO_DETACH: &'static str = "--no-detach";
static USER: &'static str = "--user=";
static LISTEN: &'static str = "--listen=";
static LISTEN_UNIX: &'static str = "--listen-unix=";
static PIDFILE: &'static str = "--pidfile=";
static WORKING_DIRECTORY: &'static str = "--working-directory=";
static PROCESSES: &'static str = "--processes=";
static LOGFILE: &'static str = "--log=";
static LOGFILE_PII: &'static str = "--log-private=";
static OLD_KILL_DELAY: &'static str = "--old-kill-delay=";
static LOG_GROUP: &'static str = "--log-group=";
static LOG_GRANT_USER: &'static str = "--log-grant-user=";
static LOG_GRANT_GROUP: &'static str = "--log-grant-group=";

fn read_pid_from_pidfile(target: &OsString) -> i32
{
	//Target should be a pidfile.
	let p = Path::new(target);
	let mut content = Vec::new();
	if let Err(err) = File::open(p).and_then(|mut fp|{
		fp.read_to_end(&mut content)
	}) {
		eprintln!("Error: Failed to read pidfile {p}: {err}", p=p.display());
		exit(1);
	};
	//Take just the first line. If file is empty, take empty string, as that will fail to parse as integer.
	let content = ByteStringLines::new(&content).next().unwrap_or(b"");
	let pid = from_utf8(content).ok().and_then(|line|i32::from_str(line).ok());
	let pid = match pid {
		Some(pid) if pid > 0 => Ok(pid),
		Some(_) => Err("Bad PID"),
		None => Err("Failed to parse PID"),
	};
	match pid {
		Ok(pid) => pid,
		Err(err) => {
			eprintln!("Error: pidfile {p}: {err}", p=p.display());
			exit(1);
		}
	}
}

fn do_sighup_action() -> bool
{
	let ret: Vec<_> = args_os().collect();
	if ret.len() == 3 && ret[1].to_str() == Some("reload") {
		//Okay, do it.
		let target = &ret[2];
		let pid = match target.to_str().and_then(|t|i32::from_str(t).ok()) {
			Some(pid) if pid > 0 => pid,
			_ => read_pid_from_pidfile(target),
		};
		//Okay, kill this pid.
		let pid = Pid::new(pid as i32);
		if let Err(err) = pid.kill(Signal::HUP) {
			eprintln!("Error: kill({pid}) failed: {err}");
			exit(1);
		}
		true
	} else {
		false
	}
}

#[derive(Copy,Clone)]
struct Config<'a>
{
	secondary_log: Option<&'a Path>,
	secondary_log_pii: Option<&'a Path>,
	log_as: &'a str,
	old_kill_duration: Option<TimeUnit>,
}

pub fn initialize_from_systemd(log_as: &str) -> SystemdStartupInfo
{
	//Unblock all signals, since apparently signal masks survive execve. Do this before setting up glth,
	//as that can possibly block SIGURG. Also unhook all hardware fault signals.
	let unreserved_signals = crate::sigutils::unblock_all_signals();

	//If requested, do SIGHUP on given process.
	if do_sighup_action() { exit(0); }

	//Nobody likes SIGPIPE in daemons.
	crate::sigutils::ignore_sigpipe();

	let mut old_kill_duration = None;
	let mut ctx = StartupInfo {
		sockets: Vec::new(),
		threads: 0,
		launch_ack: None,
	};

	//Parse log owner options. This is done even in child process.
	let mut log_owner = LogPermissions::new();
	for_each_option(|arg|{
		if let Some(arg) = arg.strip_prefix(LOG_GROUP) {
			let gid = Gid::from_str(arg).unwrap_or_else(||abort!("Group {arg} not found"));
			log_owner.set_gid(gid);
		} else if let Some(arg) = arg.strip_prefix(LOG_GRANT_USER) {
			let uid = Uid::from_str(arg).unwrap_or_else(||abort!("User {arg} not found"));
			log_owner.grant_uid(uid);
		} else if let Some(arg) = arg.strip_prefix(LOG_GRANT_GROUP) {
			let gid = Gid::from_str(arg).unwrap_or_else(||abort!("Group {arg} not found"));
			log_owner.grant_gid(gid);
		}
	});
	unsafe{set_log_permissions(log_owner)};

	//Parse the ones valid in child process. And then if ACK is set, launch a child process.
	parse_context_options(&mut ctx);
	if ctx.launch_ack.is_some() {
		//This is a child process that should always report to its parent for logging.
		log_subprocess();
		//Create LocalThreadHandleGlobal, as that only should happen in the child. The reason is that it
		//messes with signal masks in way that is only suitable for child process.
		let glth = LocalThreadHandleGlobal::new().unwrap_or_else(|err|{
			abort!("Lthg new failed: {err}")
		});
		//Return to child main().
		return SystemdStartupInfo {
			sockets: ctx.sockets,
			threads: ctx.threads,
			launch_ack: ctx.launch_ack,
			glth: glth,
		};
	}
	//Block all unreserved signals in the parent, it will use ppoll for when it is ready to receive those.
	unreserved_signals.block_signals().unwrap_or_else(|err|{
		abort!("Failed to block unreserved signals: {err}")
	});

	//Parse global options. If systemd is available, launch. Note that initialize_from_systemd_inner() never
	//returns.
	let log_as = parse_global_options(&mut ctx.sockets, log_as, &mut old_kill_duration);
	//Process the log file option.
	let mut logfile = None;
	let mut logfile_pii = None;
	for_each_option(|arg|{
		if let Some(arg) = arg.strip_prefix(LOGFILE) {
			logfile = Some(Path::new(arg).to_path_buf());
		}
		if let Some(arg) = arg.strip_prefix(LOGFILE_PII) {
			logfile_pii = Some(Path::new(arg).to_path_buf());
		}
	});
	let config = Config {
		log_as: &log_as,
		secondary_log: logfile.as_deref(),
		secondary_log_pii: logfile_pii.as_deref(),
		old_kill_duration: old_kill_duration,
	};
	if systemd_is_notify() {
		initialize_from_systemd_inner(&mut ctx.sockets, config);
	}

	//Finally, process options that are not usable with Systemd.
	let mut no_detach = false;
	let mut pidfile = None;
	let mut working_directory = None;
	let mut processes = None;
	for_each_option(|arg|{
		if arg == NO_DETACH {
			no_detach = true;
		} else if let Some(arg) = arg.strip_prefix(PIDFILE) {
			pidfile = Some(arg.to_owned());
		} else if let Some(arg) = arg.strip_prefix(WORKING_DIRECTORY) {
			working_directory = Some(arg.to_owned());
		} else if let Some(arg) = arg.strip_prefix(PROCESSES) {
			processes = Some(u32::from_str(&arg).
				unwrap_or_else(|_|abort!("Can't parse {PROCESSES}<num>")));
		}
	});
	daemonize(ctx, no_detach, pidfile, working_directory, processes, config)
}

fn parse_context_options(ctx: &mut StartupInfo)
{
	for_each_option(|arg|{
		if let Some(arg) = arg.strip_prefix(LISTEN_FD) {
			ctx.sockets.push(match RawFdT::from_str(arg) {
				Ok(x) if x < 3 => abort!("{opt} can't be less than 3",
					opt=&LISTEN_FD[..LISTEN_FD.len()-1]),
				Ok(x) => x,
				Err(_) => abort!("Can't parse {LISTEN_FD}<fd>")
			})
		} else if let Some(arg) = arg.strip_prefix(ACK) {
			ctx.launch_ack = Some(match i32::from_str(arg) {
				Ok(x) if x < 3 => abort!("{opt} can't be less than 3", opt=&ACK[..ACK.len()-1]),
				Ok(x) => x,
				Err(_) => abort!("Can't parse {ACK}<fd>")
			})
		} else if let Some(arg) = arg.strip_prefix(THREADS) {
			ctx.threads = read_thread_count(arg);
		}
	});
}

pub fn parse_duration(c: &str) -> Result<Duration, String>
{
	//Find first alphabetic character.
	let mut first_alpha = None;
	for (idx, ch) in c.char_indices() {
		if first_alpha.is_none() && ch.is_alphabetic() { first_alpha = Some(idx); }
	}
	let (value, unit) = c.split_at(first_alpha.unwrap_or(c.len()));
	let value = f64::from_str(value).set_err("Error parsing duration value")?;
	fail_if!(value < 0.0, "Duration can not be negative");
	let nanoseconds = match unit {
		"ns" => value,
		"us" => 1_000.0 * value,
		"ms" => 1_000_000.0 * value,
		"" => 1_000_000_000.0 * value,
		"min" => 60_000_000_000.0 * value,
		"h" => 3600_000_000_000.0 * value,
		"d" => 86400_000_000_000.0 * value,
		"w" => 604800_000_000_000.0 * value,
		_ => fail!(format!("Unrecognized unit"))
	} as u64;
	Ok(Duration::new(nanoseconds / 1_000_000_000, (nanoseconds % 1_000_000_000) as u32))
}

fn parse_global_options(sockets: &mut Vec<RawFdT>, log_as: &str, old_kill_duration: &mut Option<TimeUnit>) -> String
{
	//Process LOG_AS, as it is always needed.
	let mut log_as = log_as.to_owned();
	for_each_option(|arg|{
		if let Some(arg) = arg.strip_prefix(LOG_AS) { log_as = arg.to_owned(); }
		if let Some(arg) = arg.strip_prefix(OLD_KILL_DELAY) {
			let duration = parse_duration(arg).unwrap_or_else(|err|{
				abort!("old-kill-delay: {err}")
			});
			*old_kill_duration = Some(TimeUnit::Duration(duration));
		}
	});

	//Parse Systemd socket activation here even if we do not have systemd available, in case somebody uses it
	//with something else than Systemd.
	let listen_pid = var_os("LISTEN_PID").as_ref().and_then(|v|v.to_str()).and_then(|v|i32::from_str(v).ok()).
		unwrap_or(0);
	let mut listen_fds = var_os("LISTEN_FDS").as_ref().and_then(|v|v.to_str()).
		and_then(|v|i32::from_str(v).ok()).unwrap_or(0);
	if listen_pid != Pid::current().to_inner() || listen_fds < 0 { listen_fds = 0; } //Not for us.
	//Ok, this passes the checks.
	let fdrange = 3..listen_fds+3;
	for i in fdrange { sockets.push(i); }	//Systemd passes sockets starting from 3.

	//Next, process LISTEN and LISTEN_UNIX, as those need to happen before switching user. Note that listen_one
	//may return 2 fds.
	for_each_option(|arg|{
		if let Some(arg) = arg.strip_prefix(LISTEN) {
			sockets.extend_from_slice(&listen_one(arg));
		} else if let Some(arg) = arg.strip_prefix(LISTEN_UNIX) {
			sockets.push(listen_one_unix(arg));
		}
	});

	//Then, process USER.
	let mut user = false;
	for_each_option(|arg|{
		if let Some(arg) = arg.strip_prefix(USER) {
			if user { abort!("{USER}<user> can be specified only once"); }
			switch_user(arg);
			user = true;
		}
	});
	log_as
}


//Can not actually return successfully.
pub fn do_execve(mut cmdline: Vec<Vec<u8>>, c_executable: &[u8], ackfd: RawFdT, generation: u64) -> IoError
{
	//Make generation be the first argument, so it appears in bit truncated command lines.
	cmdline.insert(1, format!("--generation={generation}").into_bytes());
	//Ack descriptor is the write side of p2.
	cmdline.push(format!("--ack={ackfd}").into_bytes());
	//Translate to argument vector.
	let cmdline2 = cmdline.iter().map(|x|x.deref()).collect::<Vec<_>>();
	//Perform execv. If this returns, it always has failed.
	os_error_to_io_error(Pid::execv(c_executable, &cmdline2))
}
pub fn get_cmdline(listen: &[RawFdT], force_executable: Option<PathBuf>) -> (Vec<u8>, Vec<Vec<u8>>)
{
	let mut cmdline = get_arguments().iter().cloned().map(|x|x.into_vec()).enumerate().filter(keep_option).
		map(|x|x.1).collect::<Vec<_>>();
	for fd in listen.iter() { cmdline.push(format!("--listen-fd={fd}").into_bytes()); }
	let executable = force_executable.map(|x|x.into_os_string().into_vec()).unwrap_or_else(||cmdline[0].clone());
	(executable, cmdline)
}

pub fn keep_option(x: &(usize, Vec<u8>)) -> bool
{
	let &(idx, ref v) = x;
	if idx == 0 { return true; }	//Position 0 can not be filtered.
	!v.starts_with(LISTEN_FD.as_bytes()) && !v.starts_with(ACK.as_bytes()) &&
		!v.starts_with(LOG_AS.as_bytes()) && v.deref() != NO_DETACH.as_bytes() &&
		!v.starts_with(USER.as_bytes()) && !v.starts_with(LISTEN.as_bytes()) &&
		!v.starts_with(LISTEN_UNIX.as_bytes()) && !v.starts_with(PIDFILE.as_bytes()) &&
		!v.starts_with(WORKING_DIRECTORY.as_bytes()) && !v.starts_with(PROCESSES.as_bytes()) &&
		!v.starts_with(LOGFILE.as_bytes()) && !v.starts_with(LOGFILE_PII.as_bytes())
}


struct PollVector(Vec<PollFd>);

impl PollVector
{
	fn new() -> PollVector { PollVector(Vec::new()) }
	fn borrow(&mut self) -> &mut [PollFd] { &mut self.0 }
	fn remove(&mut self, fd: Option<UnixFileDescriptorB>)
	{
		if let Some(fd) = fd {
			for idx in 0..self.0.len() { if self.0[idx].fd == fd { self.0.swap_remove(idx); return; }}
		}
	}
	fn get_events_by_fd(&self, fd: Option<UnixFileDescriptorB>) -> PollFlags
	{
		if let Some(fd) = fd {
			for idx in 0..self.0.len() { if self.0[idx].fd == fd { return self.0[idx].revents; }}
		}
		Default::default()
	}
	fn push(&mut self, fd: UnixFileDescriptorB)
	{
		self.0.push(PollFd{fd: fd, events: Default::default(), revents: Default::default()});
	}
	fn arm_all_read(&mut self)
	{
		for p in self.0.iter_mut()
		{
			p.events = PollFlags::IN;
			p.revents = Default::default();
		}
	}
}

enum ChildPollResult
{
	Neutral,
	Launched,
	Failed,
}

pub struct ChildProcess
{
	pid: Pid,
	messages: FileDescriptor,
	ack: FileDescriptor,
	msgbuf: Vec<u8>,
	last_severity: char,
	acked: bool,
	expect_sig: Option<Signal>,
}

impl ChildProcess
{
	pub fn new(pid: Pid, messages: FileDescriptor, ack: FileDescriptor) -> ChildProcess
	{
		ChildProcess {
			pid: pid,
			messages: messages,
			ack: ack,
			msgbuf: Vec::new(),
			last_severity: 'N',
			acked: false,
			expect_sig: None,
		}
	}
	pub fn get_pid(&self) -> Pid { self.pid }
	fn poll_child(&mut self, polls: &mut PollVector, single_child: bool) -> ChildPollResult
	{
		let (msgevent, ackevent) = self.revents(polls);
		if msgevent.is_in_or_hup() {
			self.process_msgpipe(polls, single_child);
		}
		if ackevent.is_in_or_hup() {
			if self.process_ackpipe(polls) {
				return ChildPollResult::Launched;
			} else if self.ack_failed() {
				return ChildPollResult::Failed;
			}
		}
		ChildPollResult::Neutral
	}
	fn print_exit_message(&self, status: WaitStatus)
	{
		if self.expect_sig == Some(Signal::KILL) { return; }	//Yes, this has already failed.
		//Print message about child exiting.
		let pid = self.pid;
		if let Some(estatus) = status.exit_status() {
			if estatus > 0 {
				syslog!(ERROR "Child {pid} exited with status {estatus}");
			} else {
				syslog!(NOTICE "Child {pid} exited successfully");
			}
		} else if let Some(ksig) = status.term_sig() {
			if Some(ksig) != self.expect_sig {
				syslog!(ERROR "Child {pid} CRASHED with signal {ksig}");
			} else {
				syslog!(NOTICE "Child {pid} exited as requested");
			}
		} else {
			syslog!(ERROR "Child {pid} unknown abnormal exit");
		}
	}
	fn send_signal(&mut self, sig: Signal, expect_die: bool) -> Result<(), IoError>
	{
		if expect_die { self.expect_sig = Some(sig); }
		self.pid.kill(sig).map_err(os_error_to_io_error)
	}
	fn link_poll(&self, poll: &mut PollVector)
	{
		self.messages.as_unix_fd().map(|fd|poll.push(fd));
		self.ack.as_unix_fd().map(|fd|poll.push(fd));
	}
	fn ack_failed(&self) -> bool { self.ack.as_raw_fd() < 0 && !self.acked }
	fn unlink_poll_message(&mut self, poll: &mut PollVector)
	{
		poll.remove(self.messages.as_unix_fd());
		self.messages = FileDescriptor::blank();
	}
	fn unlink_poll_ack(&mut self, poll: &mut PollVector)
	{
		poll.remove(self.ack.as_unix_fd());
		self.ack = FileDescriptor::blank();
	}
	fn unlink_poll(&mut self, poll: &mut PollVector)
	{
		self.unlink_poll_message(poll);
		self.unlink_poll_ack(poll);
	}
	fn revents(&mut self, poll: &PollVector) -> (PollFlags, PollFlags)
	{
		let ev1 = poll.get_events_by_fd(self.messages.as_unix_fd());
		let ev2 = poll.get_events_by_fd(self.ack.as_unix_fd());
		(ev1, ev2)
	}
	fn print_logline(&mut self, linelen: usize, single: bool)
	{
		self.msgbuf[linelen] = 0;	//Repurpose LF as NUL.
		let line = &self.msgbuf[..linelen];
		//Squash some spam lines.
		if line.starts_with(b"thread 'main' panicked at 'Testing if panicking is recoverable...'") ||
			line.starts_with(b"note: Run with `RUST_BACKTRACE=1` for a backtrace.") {
			return;
		}
		//Update severity if needed.
		let line = if line.len() >= 3 && line[0] == b'<' && line[2] == b'>' {
			match line[1] {
				b'G'|b'A'|b'C'|b'E'|b'W'|b'N'|b'I'|b'D'|b'F'|
				b'g'|b'a'|b'c'|b'e'|b'w'|b'n'|b'i'|b'd'|b'f' =>
					self.last_severity = line[1] as char,
				_ => ()
			};
			&line[3..]
		} else {
			line
		};
		//This is the only caller of do_log_line_raw0(). In turn, print_logline() is only called by
		//process_msgpipe(), which in turn is only called by poll_child().
		//
		//The only callers of poll_child() are the daemon parent process in daemon_loop() and
		//poll_each_child(). poll_each_child() is called by SystemdParent::round() and nodetach_loop().
		//
		//Therefore we are indirectly in one of the following states:
		//
		//1) In daemon parent process. In this case, nothing has set the logging mode, so it is still the
		//   default LOGMODE_STARTUP.
		//2) In SystemdParent::round(). This is only called in Systemd parent process, which has called
		//   ParentProcess::new() with systemd=true. Therefore logging mode is LOGMODE_SYSTEMD.
		//3) In nodetach_loop() called by daemon_loop() grandchild. In this case, ParentProcess::new() will
		//   attempt to call call_openlog*(), which succeeds, as daemon_loop() child sets logging mode to
		//   LOGMODE_SUBPROCESS, which is not locked, so it changes to LOGMODE_SYSLOG or LOGMODE_SYSTEMD.
		//   Therefore, the logging mode is LOGMODE_SYSLOG or LOGMODE_SYSTEMD.
		//4) In nodetach_loop() called directly. In this case, ParentProcess::new() will attempt to call
		//   call_openlog*(), but it will silently fail because log_nodetach() has been called before
		//   calling nodetach_loop(), and LOGMODE_NODETACH is considered locked mode. Therefore the
		//   logging mode is LOGMODE_NODETACH.
		//
		//From this, one can see that the do_log_line_raw0() is never called in LOGMODE_SUBPROCESS.
		do_log_line_raw0(self.last_severity, line, if !single { Some(self.pid) } else { None });
	}
	fn process_msgpipe(&mut self, poll: &mut PollVector, single: bool)
	{
		let mut buf = [MaybeUninit::uninit();4096];
		let buf = match self.messages.read_uninit2(&mut buf) {
			Ok(x) => {
				if x.len() == 0 { self.unlink_poll_message(poll); }
				x
			},
			Err(err) => {
				if is_fatal_error(&err) {
					syslog!(CRITICAL"reading messagepipe failed: {err}");
					self.unlink_poll_message(poll);
				}
				&[]
			}
		};
		let amt = min(buf.len(), MAXLOGLINE.saturating_sub(self.msgbuf.len()));
		self.msgbuf.extend_from_slice(&buf[..amt]);
		while let Some(linelen) = find_lf(&self.msgbuf) {
			self.print_logline(linelen, single);
			//Discard first linelen + 1 bytes.
			let offset = linelen + 1;
			let tail = self.msgbuf.len().saturating_sub(offset);
			if tail > 0 { unsafe{
				copy(self.msgbuf.as_ptr().add(offset), self.msgbuf.as_mut_ptr(), tail);
			}}
			self.msgbuf.truncate(tail);
		}
	}
	fn process_ackpipe(&mut self, poll: &mut PollVector) -> bool
	{
		let mut buf = [MaybeUninit::uninit();1];
		let buf = match self.ack.read_uninit2(&mut buf) {
			Ok(x) => {
				if x.len() == 0 { self.unlink_poll_ack(poll); }
				x
			},
			Err(err) => {
				if is_fatal_error(&err) {
					syslog!(CRITICAL "reading ackpipe failed: {err}");
					self.unlink_poll_ack(poll);
				}
				&[]
			}
		};
		let ack = buf == &[0x31];
		self.acked |= ack;
		ack
	}
}

struct ChildList(Vec<ChildProcess>);

impl ChildList
{
	fn new() -> ChildList { ChildList(Vec::new()) }
	fn push(&mut self, child: ChildProcess) { self.0.push(child) }
	fn for_each_mut<F>(&mut self, mut c: F) where F: FnMut(Pid, &mut ChildProcess)
	{
		for i in self.0.iter_mut() { c(i.pid, i); }
	}
	fn exited(&mut self, status: WaitStatus, polls: &mut PollVector)
	{
		//PID r exited.
		for i in 0..self.0.len() { if self.0[i].get_pid() == status.pid() {
			//Print message about child exiting, dump the descriptors and remove child.
			self.0[i].print_exit_message(status);
			self.0[i].unlink_poll(polls);
			self.0.remove(i);
			break;
		}}
	}
	fn has_children(&self) -> bool { self.0.len() > 0 }
	fn has_many_children(&self) -> bool { self.0.len() > 1 }
}

static SIGHUP_FLAG: AtomicBool = AtomicBool::new(false);
static SIGUSR1_FLAG: AtomicBool = AtomicBool::new(false);
static SIGTERM_FLAG: AtomicBool = AtomicBool::new(false);
static SIGCHLD_FLAG: AtomicBool = AtomicBool::new(false);

extern fn signal_handler(sig: RawSignalT)
{
	//Remap the signals a bit. SIGHUP is actually SIGUSR1 and SIGQUIT is actually SIGHUP.
	if sig == Signal::QUIT.to_inner() { SIGHUP_FLAG.store(true, MemOrdering::SeqCst); }
	if sig == Signal::HUP.to_inner() { SIGUSR1_FLAG.store(true, MemOrdering::SeqCst); }
	if sig == Signal::TERM.to_inner() { SIGTERM_FLAG.store(true, MemOrdering::SeqCst); }
	if sig == Signal::CHLD.to_inner() { SIGCHLD_FLAG.store(true, MemOrdering::SeqCst); }
}

unsafe fn hook_and_mask(set: &mut SignalSet, sig: Signal)
{
	unsafe{sig.set_handler(SignalHandler::Function(signal_handler));}
	set.add(sig);
}

fn setup_signals() -> SignalSet
{
	//Hook SIGHUP, SIGQUIT, SIGTERM and SIGCHLD and block them. And even the returned "empty" mask has SIGPIPE
	//masked, because nobody likes it.
	let mut blocked = SignalSet::new();
	let mut emptymask = SignalSet::new();
	emptymask.add(Signal::PIPE);
	unsafe {
		hook_and_mask(&mut blocked, Signal::HUP);
		hook_and_mask(&mut blocked, Signal::QUIT);
		hook_and_mask(&mut blocked, Signal::TERM);
		hook_and_mask(&mut blocked, Signal::CHLD);
	}
	//This happens early enough to abort.
	blocked.block_signals().unwrap_or_else(|err|{
		abort!("Failed to set signal mask: {err}")
	});
	emptymask
}

pub fn got_sigterm() -> bool { SIGTERM_FLAG.swap(false, MemOrdering::SeqCst) }
pub fn got_sighup() -> bool { SIGHUP_FLAG.swap(false, MemOrdering::SeqCst) }
pub fn got_sigchld() -> bool { SIGCHLD_FLAG.swap(false, MemOrdering::SeqCst) }
pub fn got_sigusr1() -> bool { SIGUSR1_FLAG.swap(false, MemOrdering::SeqCst) }

pub enum WaitResult
{
	Wait,
	ExitSuccess,
	ExitFailure,
}

pub struct ParentProcess
{
	emptymask: SignalSet,			//Empty signal mask.
	polls: PollVector,			//Pipe polls.
	children: ChildList,			//Child processes.
	killed: bool,				//Killed flag.
	sent_sigterm: bool,			//SIGTERM sent.
	inotify_fd: Option<FileDescriptor>,	//The file descriptor for log inotify.
	next_generation: u64,
}

impl ParentProcess
{
	fn get_generation(&mut self) -> u64
	{
		let t = self.next_generation;
		self.next_generation += 1;
		t
	}
	fn new(config: Config, systemd: bool) -> ParentProcess
	{
		let Config{log_as, secondary_log, secondary_log_pii, ..} = config;
		//Catch some signals.
		let emptymask = setup_signals();
		//Open syslog.
		if systemd {
			call_openlog_systemd(secondary_log, secondary_log_pii);
		} else {
			call_openlog(log_as.as_bytes(), OpenlogFlags::NDELAY|OpenlogFlags::PID, LogFacility::Daemon,
				secondary_log, secondary_log_pii);
		}
		let mut pv = PollVector::new();
		let inotify_fd = if let Some(_logparent) = log_sink_get_parent() {
			let fd = self::linux::set_inotify_watch(&_logparent);
			if let Some(ufd) = fd.as_unix_fd() { pv.push(ufd); Some(fd) } else { None }
		} else { None };
		ParentProcess {
			emptymask: emptymask,
			polls: pv,
			children: ChildList::new(),
			killed: false,
			sent_sigterm: false,
			inotify_fd: inotify_fd,
			next_generation: 0,
		}
	}
	pub fn add_child(&mut self, child: ChildProcess)
	{
		syslog!(NOTICE "Created child {pid}", pid=child.pid);
		child.link_poll(&mut self.polls);
		self.children.push(child);
	}
	pub fn maybe_process_sigterm(&mut self)
	{
		//Kill the children with SIGTERM on SIGTERM.
		if !got_sigterm() { return; }
		syslog!(NOTICE "Got SIGTERM");
		self.killed = true;		//Killed, should exit.
		self.sent_sigterm = true;
		self.children.for_each_mut(|pid, child|{
			syslog!(NOTICE "Killing child {pid}");
			if let Err(err) = child.send_signal(Signal::TERM, true) {
				syslog!(CRITICAL "kill({pid}) failed: {err}");
			}
		});
	}
	pub fn maybe_process_sighup(&mut self)
	{
		//Kill the children with SIGHUP on SIGHUP. However, do not do this if SIGTERM has already
		//been sent.
		if !got_sighup() || self.sent_sigterm { return; }
		syslog!(NOTICE "Got SIGQUIT");
		self.killed = true;		//Killed, should exit.
		self.children.for_each_mut(|pid, child|{
			syslog!(NOTICE "Signaling child {pid} to exit");
			if let Err(err) = child.send_signal(Signal::HUP, false) {
				syslog!(CRITICAL "kill({pid}) failed: {err}");
			}
		});
	}
	pub fn maybe_process_sigchld<F>(&mut self, mut on_exit: F) where F: FnMut(Pid)
	{
		//Wait for children on SIGCHLD.
		if !got_sigchld() { return; }
		syslog!(NOTICE "Got SIGCHLD");
		loop { match WaitStatus::wait_pid(WaitAnyChild, WaitPidFlags::NOHANG) {
			Ok(Some(status)) => {
				on_exit(status.pid());
				self.children.exited(status, &mut self.polls);
			},
			_ => break
		}}
	}
	pub fn wait_for_event(&mut self, until: Timer) -> bool
	{
		//Wait for something to happen.
		self.polls.arm_all_read();
		//If until is disarmed, wait without time bound. If until is in past, poll with immediate return.
		//otherwise wait that amount.
		let maxwait = match until.expires() {
			Some(x) => {
				let delay = x.time_to().unwrap_or(Duration::from_secs(0));
				let delay = delay.as_secs().saturating_mul(1000).
					saturating_add(delay.subsec_nanos() as u64 / 1000000);
				min(delay, 2147483647) as i32
			},
			None => -1,
		};
		if let Err(err) = unsafe{PollFd::ppoll(self.polls.borrow(), maxwait, Some(self.emptymask))} {
			//The above ppoll is liable to get interrupted by EINTR if a signal is sent.
			if !err.is_transient() {
				syslog!(CRITICAL "ppoll failed: {err}");
				return false
			}
		}
		true
	}
	pub fn poll_each_child<F>(&mut self, mut c: F) where F: FnMut(Pid)
	{
		let single_child = !self.children.has_many_children();
		let mut failed_children = Vec::new();
		{
			let polls = &mut self.polls;
			self.children.for_each_mut(|pid, child|{
				match child.poll_child(polls, single_child) {
					ChildPollResult::Launched => c(pid),
					ChildPollResult::Failed => failed_children.push(pid),
					ChildPollResult::Neutral => ()
				}
			});
		}
		for fchild in failed_children {
			//Send SIGKILL to given child. Scan the child list to avoid sending to wrong process.
			//all the children on child list are valid PIDs (possibly zombie).
			self.sigkill_children_if(|pid|pid == fchild);
		}
	}
	pub fn sighup_children_if<F>(&mut self, predicate: F) where F: Fn(Pid) -> bool
	{
		self.children.for_each_mut(|pid, child|{ if predicate(pid) {
			syslog!(NOTICE "Signaling child {pid} to exit");
			if let Err(err) = child.send_signal(Signal::HUP, false) {
				syslog!(CRITICAL "kill({pid}) failed: {err}");
			}
		}});
	}
	fn sigkill_children_if<F>(&mut self, predicate: F) where F: Fn(Pid) -> bool
	{
		self.children.for_each_mut(|pid, child|{ if predicate(pid) {
			syslog!(CRITICAL "Child {pid} failed to start");
			if let Err(err) = child.send_signal(Signal::KILL, true) {
				syslog!(CRITICAL "kill({pid}) failed: {err}");
			}
		}});
	}
	pub fn wait(&self) -> WaitResult
	{
		use self::WaitResult::*;
		//Successful exit if killed (so should exit) and no error.
		if self.children.has_children() { Wait } else if !self.killed { ExitFailure } else { ExitSuccess }
	}
}

//cf must be divergent!
pub fn fork_process<P,C,PF,CF>(p: P, c: C, pf: PF, cf: CF) where P: FnOnce(ChildProcess), C: FnOnce(RawFdT),
	PF: FnOnce(String), CF: FnOnce(String)
{
	//These are not inherited, except as STDIN/STDOUT/STDERR, which are forced to be inherited. Also, the
	//ack pipe write end is exceptional, but that is forced below.
	let flags = PipeFlags::CLOEXEC;
	let p1 = FileDescriptor::pipe2(flags).unwrap_or_else(|err|{
		abort!("Failed to create pipe: {err}")
	});
	let p2 = FileDescriptor::pipe2(flags).unwrap_or_else(|err|{
		abort!("Failed to create pipe: {err}")
	});
	let null = UnixPath::dev_null().open(OpenFlags::O_RDWR|OpenFlags::O_CLOEXEC).unwrap_or_else(|err|{
		abort!("Failed to open /dev/null: {err}")
	});
	match Pid::fork() {
		Ok(None) => {
			//Child process. Close read end of p, dup null to 0, 1 and close, dup wpipe to 2. For
			//logging, report to parent.
			let wpipe = p1.1;
			let wpipea = p2.1;
			drop(p1.0);
			drop(p2.0);
			if let Err(err) = wpipea.set_cloexec(false) {
				cf(format!("Failed to set ack pipe !O_CLOEXEC: {err}"));
				unreachable!();
			}
			if let Err(err) = null.dup2(SpecialFd::STDIN) {
				cf(format!("Failed to dup2: {err}"));
				unreachable!();
			}
			if let Err(err) = null.dup2(SpecialFd::STDOUT) {
				cf(format!("Failed to dup2: {err}"));
				unreachable!();
			}
			if let Err(err) = wpipe.dup_stderr() {
				cf(format!("Failed to dup2: {err}"));
				unreachable!();
			}
			log_subprocess();
			let ackfd = wpipea.as_raw_fd();
			forget(wpipea);		//Do not close.
			c(ackfd);
		},
		Ok(Some(pid)) => {
			//Parent process. Close null and write end of p.
			let rpipe = p1.0;
			let rpipea = p2.0;
			drop(p1.1);
			drop(p2.1);
			drop(null);
			p(ChildProcess::new(pid, rpipe, rpipea));
		},
		Err(err) => {
			//Error.
			pf(format!("Failed to fork: {err}"));
		}
	}
}

pub fn mark_inherit(fd: RawFdT)
{
	let f = unsafe{UnixFileDescriptor::new(fd)};
	if let Err(err) = f.set_cloexec(false) {
		syslog!(CRITICAL "Failed to set O_CLOEXEC for fd {fd}: {err}");
		exit(1);
	}
	forget(f);
}

fn daemonize(ctx: StartupInfo, no_detach: bool, pidfile: Option<String>, workdir: Option<String>,
	processes: Option<u32>, config: Config) -> SystemdStartupInfo
{
	if no_detach {
		log_nodetach();	//Set nodetach logging if in nodetach mode.
	}
	//If no_detach is specified, or if in multithread mode, default processes is 1, else 8.
	let processes = processes.unwrap_or(if ctx.threads > 0 || no_detach { 1 } else { 8 });
	//If no_detach is specified, the default workdir is '.', else '/'.
	let workdir = workdir.unwrap_or((if no_detach { "." } else { "/" }).to_owned());
	//Change to given working directory.
	set_current_dir(workdir.deref()).unwrap_or_else(|err|{
		abort!("Failed to change working directory to {workdir}: {err}")
	});
	if no_detach {
		nodetach_loop(processes, &ctx.sockets, -1, None, config);
	} else {
		daemon_loop(processes, pidfile, &ctx.sockets, config);
	}
}

fn check_execute_permissions(executable: &[u8]) -> Result<(), String>
{
	let p = UnixPath::new(executable).
		ok_or_else(||format!("Invalid executable name {exe}", exe=EscapeByteString(&executable)))?;
	if p.access(AccessFlags::X).is_err() {
		fail!(format!("No permissions to execute {exe}", exe=EscapeByteString(&executable)));
	}
	Ok(())
}

fn initialize_from_systemd_inner(_0: &mut Vec<RawFdT>, _1: Config) -> !
{
	self::linux::initialize_from_systemd(_0, _1)
}

fn read_thread_count(_0: &str) -> u32
{
	self::linux::parse_threads(_0)
}

fn threadmode_launch<T:Thread>(_0: SystemdStartupInfo, _1: &str, _2: T::Parameters, _3: T::Parameters2,
	_4: Arc<Vec<<T as Thread>::Local>>, _5: Arc<Vec<Arc<<T as Thread>::LocalMetrics>>>, _6: FileDescriptor,
	_7: LocalThreadHandleGlobal)
{
	self::linux::threadmode_launch::<T>(_0, _1, _2, _3, _4, _5, _6, _7)
}

fn set_privileges_acl(_0: &str, _1: &UnixPath, _2: Vec<String>, _3: Vec<String>, _4: bool)
{
	self::linux::set_privileges_acl(_0, _1, _2, _3, _4)
}

fn read_executable() -> Option<PathBuf> { self::linux::read_executable() }


mod linux
{
	use ::do_mask_sighup;
	use ::do_sighup_close;
	use ::do_unshare_files;
	use ::FileDescriptor;
	use ::generate_launch_ack;
	use ::gettid;
	use ::is_fatal_error;
	use ::os_error_to_io_error;
	use ::ThreadReport;
	use super::check_execute_permissions;
	use super::Config;
	use super::do_execve;
	use super::fork_process;
	use super::get_cmdline;
	use super::get_sighup_flag;
	use super::got_sigusr1;
	use super::LocalThreadHandle;
	use super::LocalThreadHandleGlobal;
	use super::mark_inherit;
	use super::ParentProcess;
	use super::SighupPipe;
	use super::SystemdStartupInfo;
	use super::Thread;
	use super::ThreadNewParams;
	use super::ThreadSpawnedParams;
	use super::THREADS;
	use btls_aux_memory::EscapeByteString;
	use btls_aux_random::secure_random;
	use btls_aux_time::PointInTime;
	use btls_aux_time::Timer;
	use btls_aux_time::TimeUnit;
	use btls_aux_unix::AclPermEntry;
	use btls_aux_unix::FileDescriptor as UnixFileDescriptor;
	use btls_aux_unix::Gid;
	use btls_aux_unix::InotifyEventType as IET;
	use btls_aux_unix::InotifyInitFlags as IIF;
	use btls_aux_unix::OsError;
	use btls_aux_unix::Path as UnixPath;
	use btls_aux_unix::Pid;
	use btls_aux_unix::RawFdT;
	use btls_aux_unix::Signal;
	use btls_aux_unix::systemd_notify_startup;
	use btls_aux_unix::Uid;
	use btls_util_logging::log_sink_process_inotify;
	use btls_util_logging::syslog;
	use std::env::remove_var;
	use std::fs::read_link;
	use std::io::Error as IoError;
	use std::mem::replace;
	use std::os::unix::ffi::OsStrExt;
	use std::process::exit;
	use std::path::Path;
	use std::path::PathBuf;
	use std::str::FromStr;
	use std::sync::Arc;
	use std::thread::Builder;

	pub(crate) fn threadmode_launch<T:Thread>(info: SystemdStartupInfo, name: &str, param: T::Parameters,
		param2: T::Parameters2, data: Arc<Vec<<T as Thread>::Local>>,
		metrics: Arc<Vec<Arc<<T as Thread>::LocalMetrics>>>, rsigpipe: FileDescriptor,
		glth: LocalThreadHandleGlobal)
	{
		//Work around problem with RNG code in presence of threads.
		let mut dummy = [0u8];
		secure_random(&mut dummy);
		//In threaded mode.
		let master = ThreadReport::new();
		//launch the threads. Each thread should have independent file counter.
		for ti in 0..info.threads {
			let b = Builder::new();
			let b = b.name(String::from(name));
			let slave = master.clone();
			let param2 = param.clone();
			let data2 = data.clone();
			let metrics2 = metrics.clone();
			let rsigpipe2 = rsigpipe.clone();
			let launch_ack2 = info.launch_ack;
			let glth2 = glth.clone();
			if let Err(err) = b.spawn(move || {
				//Unshare the file descriptors.
				if let Err(err) = do_unshare_files() {
					abort!("Failed to unshare(CLONE_FILES): {err}");
				}
				//Mask SIGHUP, so we do not handle it, as the pipe is closed.
				do_mask_sighup();
				//Close the SIGHUP handle, we do not want it open in this thread.
				do_sighup_close();
				//Close launch ack if any.
				if let Some(lackfd) = launch_ack2 {
					//Create fd object and immediately drop in order to close the file.
					FileDescriptor::new(lackfd);
				}
				//Get thread id.
				let tid = gettid();
				let rirqpipe = LocalThreadHandle::new(glth2);
				let thread = T::new(ThreadNewParams {
					gparameters: param2,
					lparameters: &data2[ti as usize],
					lmetrics: metrics2[ti as usize].clone(),
					sigpipe: SighupPipe::new(get_sighup_flag(), rsigpipe2),
					irqpipe: rirqpipe,
					tid: Some(tid as u64),
					_dummy: ()
				});
				//Acknowledge launch.
				syslog!(NOTICE "Launched thread {tid}");
				//Report in, do main loop and report out.
				slave.report_in();
				thread.run();
				slave.report_out();
				syslog!(NOTICE "Shutting down thread {tid}");
			}) {
				abort!("Failed to launch thread: {err}");
			}
		}
		//Wait for all threads to report in, generate ACK and wait for all threads to report out.
		let thrcnt = info.threads as usize;
		master.wait(|mc,e|mc >= thrcnt || e > 0).unwrap_or_else(|_|abort!("Lock error (thread crashed?)"));
		if master.errors() > 0 { abort!("Thread exited before all threads reported in"); }
		//Ok, spawned all threads.
		T::spawned(ThreadSpawnedParams {
			gparameters: param,
			gparameters2: param2,
			contexts: metrics,
			_dummy: ()
		});
		generate_launch_ack(info.launch_ack);
		master.wait(|mc,_|mc == 0).unwrap_or_else(|_|abort!("Lock error (thread crashed?)"));
		//Exit.
		syslog!(NOTICE "All threads have ended, exiting.");
	}
	pub fn parse_threads(arg: &str) -> u32
	{
		u32::from_str(&arg).unwrap_or_else(|_|abort!("Can't parse {THREADS}<num>"))
	}

	pub(super) fn initialize_from_systemd(listen: &mut Vec<RawFdT>, config: Config) -> !
	{
		let Config{old_kill_duration, ..} = config;
		//The effect of LISTEN_PID and LISTEN_FDS is already taken into account.
		let listen2 = listen.clone();

		let forced_executable = read_executable();
		let (executable, cmdline) = get_cmdline(&listen, forced_executable.clone());
		check_execute_permissions(&executable).unwrap_or_else(|err|{
			abort!("{err}")
		});
		fork_process(|child|{
			let mut obj = SystemdParent {
				parent: ParentProcess::new(config, true),
				no_ack_yet: true,
				pending_reexec: None,
				listen: listen2,
				old_pid: None,
				old_kill_timer: Timer::new(),
				old_kill_duration: old_kill_duration,
				next_generation: 1,	//generation 0 is below.
			};
			obj.parent.add_child(child);
			exit(obj.wait(&forced_executable));
		}, |ackfd|{
			for fd in listen.iter() { mark_inherit(*fd); }
			mark_inherit(ackfd);
			let err = do_execve(cmdline, &executable, ackfd, 0);
			syslog!(CRITICAL "Failed to execute {exe}: {err}", exe=EscapeByteString(&executable));
			exit(1);
		}, |err|{
			abort!("Failed to execute: {err}")
		}, |err|{
			abort!("{err}")
		});
		//All branches of the above exit.
		unreachable!();
	}

	struct SystemdParent
	{
		parent: ParentProcess,
		no_ack_yet: bool,			//No ACK sent to systemd yet.
		pending_reexec: Option<Pid>,		//Re-exec pending.
		listen: Vec<RawFdT>,
		old_pid: Option<Pid>,			//The old PID during re-exec.
		old_kill_timer: Timer,
		old_kill_duration: Option<TimeUnit>,
		next_generation: u64,
	}

	impl SystemdParent
	{
		fn get_generation(&mut self) -> u64
		{
			let t = self.next_generation;
			self.next_generation += 1;
			t
		}
		fn maybe_process_sigusr1(&mut self, forced_executable: &Option<PathBuf>)
		{
			//Re-exec on SIGUSR1. However, this only works if first ACK has been sent and There is no
			//pending re-exec.
			if !got_sigusr1() || self.no_ack_yet || self.pending_reexec.is_some() { return; }
			syslog!(NOTICE "Got SIGHUP");
			//Remove the systemd-related variables, we use this process to boostrap the other one.
			remove_var("NOTIFY_SOCKET");
			remove_var("LISTEN_FDS");
			remove_var("LISTEN_PID");
			//Collect the command-line, and add fd-related variables.
			let (executable, cmdline) = get_cmdline(&self.listen, forced_executable.clone());
			if let Err(err) = check_execute_permissions(&executable) {
				return syslog!(CRITICAL "{err}")
			};
			let generation = self.get_generation();
			let pending_reexec = &mut self.pending_reexec;
			let parent = &mut self.parent;
			let listen = &self.listen;
			//We should only have one child here.
			let old_pid = parent.children.0.first().map(|x|x.pid);
			let old_pid_ptr = &mut self.old_pid;
			fork_process(|child|{
				//Register polled file descriptors, Create child structure and mark this as
				//re-execing. We do not start old_kill_timer, since it only starts on ACK. Also
				//save the PID of old process for killing it.
				*pending_reexec = Some(child.get_pid());
				*old_pid_ptr = old_pid;
				parent.add_child(child);
			}, |ackfd|{
				//Mark file descriptors up to slimit as not close-on-exec, and similarly on
				//ackfd. If these operations fail, print error to stderr (piping is set up) and exit.
				//The ackfd is always above slimit.
				for fd in listen.iter() { mark_inherit(*fd); }
				mark_inherit(ackfd);
				let err = do_execve(cmdline, &executable, ackfd, generation);
				syslog!(CRITICAL "Failed to execute {exe}: {err}", exe=EscapeByteString(&executable));
				exit(1);
			}, |err|{
				syslog!(CRITICAL "Failed to execute: {err}");
			}, |err|{
				//Abort child on failure.
				syslog!(CRITICAL "{err}");
				exit(1);
			})
		}
		fn round(&mut self, forced_executable: &Option<PathBuf>)
		{
			let old_kill_duration = self.old_kill_duration;
			//Possibly wait for old_kill_timer.
			let old_kill_timer = self.old_kill_timer.clone();
			if !self.parent.wait_for_event(old_kill_timer) { return; }
			if let Some(inotify_fd) = self.parent.inotify_fd.as_ref() {
				if self.parent.polls.get_events_by_fd(inotify_fd.as_unix_fd()).is_in() {
					process_log_inotify(inotify_fd.clone());
				}
			}
			self.parent.maybe_process_sigterm();
			self.parent.maybe_process_sighup();
			self.maybe_process_sigusr1(forced_executable);
			let mut send_sighup_to_not = None;
			let no_ack_yet = &mut self.no_ack_yet;
			let pending_reexec = &mut self.pending_reexec;
			let old_kill_timer = &mut self.old_kill_timer;
			let old_pid = &mut self.old_pid;
			self.parent.maybe_process_sigchld(|r|{
				//If the failure hit process that is performing the pending_reexec, clear it,
				//along with old kill timer. We need to cancel the old_kill_timer in this case!
				if *pending_reexec == Some(r) {
					*pending_reexec = None;
					*old_pid = None;
					old_kill_timer.disarm();
				}
			});
			self.parent.poll_each_child(|pid|{
				//If this is the first ACK, send ACK to Systemd. Otherwise tell all other
				//children to exit, and set pending_reexec to false and start old_kill_timer.
				if replace(no_ack_yet, false) {
					systemd_notify_startup().
						unwrap_or_else(|err|abort!("Failed to ACK startup to systemd: {err}"));
				} else {
					send_sighup_to_not = Some(pid);
					*pending_reexec = None;
					old_kill_timer.rearm(old_kill_duration);
				}
			});
			if send_sighup_to_not.is_some() {
				self.parent.sighup_children_if(|pid|Some(pid) != send_sighup_to_not);
			}
			if old_kill_timer.triggered(PointInTime::now()) {
				if let &mut Some(childpid) = old_pid {
					//Kill the old child.
					self.parent.children.for_each_mut(|pid, child|{ if pid == childpid {
						//Set expect_sig to SIGTERM to squash the error.
						child.expect_sig = Some(Signal::TERM);
						if let Err(err) = child.send_signal(Signal::TERM, true) {
							syslog!(CRITICAL "kill({pid}) failed: {err}");
						}
					}})
				}
				//The old child should be killed, clear old_pid.
				*old_pid = None;
			}
		}
		fn wait(mut self, forced_executable: &Option<PathBuf>) -> i32
		{
			use ::threading::WaitResult::*;
			loop { match self.parent.wait() {
				Wait => self.round(forced_executable),
				ExitSuccess => return 0,
				ExitFailure => return 1,
			}}
		}
	}

	struct DummyAclRead;

	impl ::btls_aux_unix::AclRead for DummyAclRead
	{
		fn set_user(&mut self, _: AclPermEntry) {}
		fn set_users(&mut self, _: Uid, _: AclPermEntry) {}
		fn set_group(&mut self, _: AclPermEntry) {}
		fn set_groups(&mut self, _: Gid, _: AclPermEntry) {}
		fn set_other(&mut self, _: AclPermEntry) {}
	}

	struct Acls
	{
		user: AclPermEntry,
		group: AclPermEntry,
		other: AclPermEntry,
		users: Vec<(Uid, AclPermEntry)>,
		groups: Vec<(Gid, AclPermEntry)>,
	}

	impl ::btls_aux_unix::AclWrite for Acls
	{
		fn get_user(&self) -> AclPermEntry { self.user }
		fn get_users<F>(&self, mut f: F) where F: FnMut(Uid, AclPermEntry)
		{
			for (uid, perm) in self.users.iter().cloned() { f(uid, perm); }
		}
		fn get_group(&self) -> AclPermEntry { self.group }
		fn get_groups<F>(&self, mut f: F) where F: FnMut(Gid, AclPermEntry)
		{
			for (gid, perm) in self.groups.iter().cloned() { f(gid, perm); }
		}
		fn get_other(&self) -> AclPermEntry { self.other }
	}

	pub fn set_privileges_acl(oname: &str, name: &UnixPath, grantu: Vec<String>, grantg: Vec<String>,
		was_group: bool)
	{
		let s = name.stat().unwrap_or_else(|err|abort!("Stat '{oname}' failed: {err}"));
		//Map grants to users. Also, remove entries for owners.
		let grantu = grantu.iter().filter_map(|name|{
			let uid = Uid::from_str(&name);
			let uid = uid.unwrap_or_else(||abort!("No user named '{name}' in user database"));
			if uid == s.uid() { None } else { Some((uid, AclPermEntry::RW)) }
		}).collect::<Vec<_>>();
		let grantg = grantg.iter().filter_map(|name|{
			let gid = Gid::from_str(&name);
			let gid = gid.unwrap_or_else(||abort!("No group named '{name}' in user database"));
			if gid == s.gid() { None } else { Some((gid, AclPermEntry::RW)) }
		}).collect::<Vec<_>>();

		//Attribute data.
		let attrib = Acls {
			user: AclPermEntry::RW,
			group: if was_group { AclPermEntry::RW } else { AclPermEntry::NONE },
			other: AclPermEntry::NONE,
			users: grantu,
			groups: grantg,
		};

		if attrib.users.len() == 0 && attrib.groups.len() == 0 {
			//Read ACLs. If there are no ACLs return quietly.
			match name.getfacl(&mut DummyAclRead) {
				Ok(_) => (),
				//ENODATA and OPNOTSUPP special.
				Err(err) if err == OsError::ENODATA => return,
				Err(err) if err == OsError::EOPNOTSUPP => return,
				Err(err) => abort!("get ACL list from '{oname}' failed: {err}")
			}
		}
		name.setfacl(&attrib).unwrap_or_else(|err|abort!("set ACL list to '{oname}' failed: {err}"));
	}

	pub fn read_executable() -> Option<PathBuf> { read_link(Path::new("/proc/self/exe")).ok() }

	fn do_read_on_inotify(fd: FileDescriptor) -> Result<bool, IoError>
	{
		let fd = match fd.as_unix_fd() { Some(x) => x, None => return Ok(false) };
		if let Err(err) = unsafe{fd.read_inotify(|ev|{
			log_sink_process_inotify(ev.get_name(), ev.get_event_mask().to_inner())
		})} {
			let err = os_error_to_io_error(err);
			fail_if!(is_fatal_error(&err), err);
			return Ok(false);
		}
		Ok(true)
	}
	pub fn process_log_inotify(fd: FileDescriptor)
	{
		loop { match do_read_on_inotify(fd.clone()) {
			Ok(true) => (),
			Ok(false) => return,
			Err(err) => {
				syslog!(ERROR "Error reading log inotify: {err}");
				return;
			}
		}}
	}

	pub fn set_inotify_watch(path: &Path) -> FileDescriptor
	{
		let (xfd, fd) = match UnixFileDescriptor::inotify_init(IIF::NONBLOCK|IIF::CLOEXEC) {
			Ok(x) => (x.as_fdb(), FileDescriptor::__new(x)),
			Err(err) => {
				syslog!(ERROR "Error creating log inotify: {err}");
				return FileDescriptor::blank();
			}
		};
		let path = path.as_os_str().as_bytes();
		let path = match UnixPath::new(path) {
			Some(x) => x,
			None => {
				syslog!(ERROR "NUL character in log path");
				return FileDescriptor::blank();
			}
		};
		if let Err(err) = unsafe{path.inotify_add_watch(&xfd, IET::MOVED_FROM|IET::DELETE)} {
			syslog!(ERROR "Error creating log inotify: {err}");
			return FileDescriptor::blank();
		}
		fd
	}
}

fn fork_children(parent: &mut ParentProcess, processes: u32, listen: &[RawFdT], first_time: bool,
	childlist: &mut HashSet<Pid>, forced_executable: Option<PathBuf>)
{
	//Collect the command-line, and add fd-related variables.
	let (executable, cmdline) = get_cmdline(listen, forced_executable);
	if let Err(err) = check_execute_permissions(&executable) {
		if first_time { abort!("{err}") } else { return syslog!(CRITICAL "{err}") }
	}
	let generation = parent.get_generation();
	for _ in 0..processes {
		let cmdline2 = cmdline.clone();
		let executable2 = executable.clone();
		//In child process, we always abort on error. On parent process, we abort if first_time is set,
		//otherwise just log critical error.
		fork_process(|child|{
			//Register polled file descriptors, Create child structure and mark this as
			//re-execing. Also register on child list.
			childlist.insert(child.get_pid());
			parent.add_child(child);
		}, |ackfd|{
			//Mark file descriptors up to slimit as not close-on-exec, and similarly on
			//ackfd. If these operations fail, print error to stderr (piping is set up) and exit.
			//The ackfd is always above slimit.
			for fd in listen.iter() { mark_inherit(*fd); }
			mark_inherit(ackfd);
			let err = do_execve(cmdline2, &executable2, ackfd, generation);
			abort!("Failed to execute {exe}: {err}", exe=EscapeByteString(&executable));
		}, |err|{
			if first_time {
				abort!("Failed to execute: {err}")
			} else {
				syslog!(CRITICAL "Failed to execute: {err}")
			}
		}, |err|{
			//Abort child on failure.
			abort!("{err}");
		})
	}
}

fn is_superset_of(larger: &HashSet<Pid>, smaller: &HashSet<Pid>) -> bool
{
	for i in smaller.iter() { if !larger.contains(i) { return false; }}
	true
}

fn nodetach_loop(processes: u32, listen: &[RawFdT], daemon_ackfd: RawFdT, pidfile: Option<String>,
	config: Config) -> !
{
	let Config{old_kill_duration, ..} = config;
	let mut old_kill_timer = Timer::new();
	let mut reexecing_children = HashSet::new();
	let mut child_pids = HashSet::new();
	//log_nodetach() prevents internal openlog(), so we can use a _dummy process name.
	let mut parent = ParentProcess::new(config, false);
	let forced_executable = read_executable();
	fork_children(&mut parent, processes, listen, true, &mut child_pids, forced_executable.clone());
	let mut acks = HashSet::new();
	use self::WaitResult::*;
	let mut failed = false;
	let mut startup_complete = false;
	let pidfile = &pidfile;
	//Possibly wait for old_kill_timer.
	loop { match parent.wait() {
		Wait => {
			if failed {
				parent.sigkill_children_if(|_|true);	//Kill all children.
				exit(1)
			}
			//Possibly wait for old_kill_timer.
			if !parent.wait_for_event(old_kill_timer.clone()) { continue; }
			if let Some(inotify_fd) = parent.inotify_fd.as_ref() {
				if parent.polls.get_events_by_fd(inotify_fd.as_unix_fd()).is_in() {
					linux::process_log_inotify(inotify_fd.clone());
				}
			}
			parent.maybe_process_sigterm();
			parent.maybe_process_sighup();
			if daemon_ackfd >= 0 && got_sigusr1() && reexecing_children.is_empty() && startup_complete {
				//Pivot the lists, so current children become old children. Then try starting new
				//children. Clear the ACK list, as this is new set of children. This can not happen
				//in middle of any operation that needs ACK list.
				acks.clear();
				swap(&mut child_pids, &mut reexecing_children);
				fork_children(&mut parent, processes, listen, false, &mut child_pids,
					forced_executable.clone());
				//If there are no children, try to pivot the two lists to cancel out of failed
				//re-exec.
				if child_pids.is_empty() { swap(&mut child_pids, &mut reexecing_children); }
			}
			parent.maybe_process_sigchld(|pid|{
				child_pids.remove(&pid);
				//Clearing child from reexecing_children has effect of clearing the re-exec pending
				//flag when all such children have exited.
				reexecing_children.remove(&pid);
				//If all old children have exited, cancel the timer to kill old children.
				if reexecing_children.is_empty() { old_kill_timer.disarm(); }
				//If there are no children and no re-execing children, we have failed.
				failed |= child_pids.is_empty() && reexecing_children.is_empty();
				//If there are no children, try to pivot the two lists to cancel out of failed
				//re-exec.
				if child_pids.is_empty() { swap(&mut child_pids, &mut reexecing_children); }
			});
			parent.poll_each_child(|pid|{
				acks.insert(pid);
				syslog!(NOTICE "Child {pid} is up");
				//Initial startup is complete when acks is superset of children.
				if is_superset_of(&acks, &child_pids) && daemon_ackfd >= 0 && !startup_complete {
					failed |= make_pidfile(pidfile).is_err();
					if failed { return; }
					//Signal the parent we are ready. The write should never fail. The
					//descriptor is dropped afterwards, which closes it. We take advantage of
					//the fact that this code can only be executed once.
					let daemon_ackfd = FileDescriptor::new(daemon_ackfd);
					daemon_ackfd.write_const(&[0x31]).ok();
					startup_complete = true;
				}
			});
			//If all new children have reached ack, send SIGHUP to all the old ones, so they go away
			//and re-exec cycle finishes. Start the kill timer.
			if is_superset_of(&acks, &child_pids) && !reexecing_children.is_empty() {
				parent.sighup_children_if(|pid|reexecing_children.contains(&pid));
				old_kill_timer.rearm(old_kill_duration);
			}
			//If the kill timer has expired, kill all old children remaining.
			if old_kill_timer.triggered(PointInTime::now()) {
				for childpid in reexecing_children.iter().cloned() {
					parent.children.for_each_mut(|pid, child|{ if pid == childpid {
						//Set expect_sig to SIGTERM to squash the error.
						child.expect_sig = Some(Signal::TERM);
						if let Err(err) = child.send_signal(Signal::TERM, true) {
							syslog!(CRITICAL "kill({pid}) failed: {err}");
						}
					}})
				}
			}
		},
		ExitSuccess => exit(0),
		ExitFailure => {
			syslog!(ALERT "All child processes unexpectedly quit");
			exit(1);
		}
	}}
}

fn make_pidfile(pidfile: &Option<String>) -> Result<(), ()>
{
	if let &Some(ref pidfile) = pidfile {
		File::create(pidfile.deref()).
			and_then(|mut fp|fp.write_all(format!("{pid}\n", pid=Pid::current()).as_bytes())).
			map_err(|err|{
			syslog!(CRITICAL "Failed to create pidfile {pidfile}: {err}")
		})
	} else {
		Ok(())
	}
}

fn set_privileges(oname: &str, user: Option<String>, group: Option<String>, grantu: Vec<String>,
	grantg: Vec<String>, was_world: bool)
{
	let mask = if was_world { 0x1B6 } else if group.is_some() { 0x1B0 } else { 0x180 };
	let path = UnixPath::new(oname.as_bytes()).unwrap_or_else(||abort!("Illegal file path '{oname}'"));
	let (mut uid, mut gid) = (Uid::get_real(), Gid::get_real());
	group.as_ref().map(|group|gid = Gid::from_str(group).
		unwrap_or_else(||abort!("Group '{group}' does not exist in user database")));
	user.as_ref().map(|user|uid = Uid::from_str(user).
		unwrap_or_else(||abort!("User '{user}' does not exist in user database")));
	if user.is_some() || group.is_some() {
		let user = user.as_deref().unwrap_or("<current>");
		let group = group.as_deref().unwrap_or("<current>");
		path.chown(uid, gid).
			unwrap_or_else(|err|abort!("Chown '{oname}' to '{user}:{group}' failed: {err}"));
	}
	path.chmod(mask).unwrap_or_else(|err|abort!("Chmod '{oname}' failed: {err}"));
	if !was_world { set_privileges_acl(oname, path, grantu, grantg, group.is_some()) }
}


fn listen_one_unix(spec: &str) -> RawFdT
{
	let mut itr = 0;
	let mut user = None;
	let mut group = None;
	let mut users = Vec::new();
	let mut groups = Vec::new();
	let mut world = false;
	let path;
	loop {
		let remainder = &spec[itr..];
		let nextsplit = remainder.find(',');
		if let Some(split) = nextsplit {
			let tpart = &remainder[..split];
			itr = itr + split + 1;
			if tpart.starts_with("/") {
				//This is a path.
				path = remainder;
				break;
			}
			//tpart should be some sort of directive...
			if tpart == "w" { world = true; }
			else if let Some(tpart) = tpart.strip_prefix("U=") { user = Some(tpart.to_owned()); }
			else if let Some(tpart) = tpart.strip_prefix("G=") { group = Some(tpart.to_owned()); }
			else if let Some(tpart) = tpart.strip_prefix("u=") { users.push(tpart.to_owned()); }
			else if let Some(tpart) = tpart.strip_prefix("g=") { groups.push(tpart.to_owned()); }
			else { abort!("Bad attribute '{tpart}'"); }
		} else {
			//This must be a path.
			path = remainder;
			break;
		}
	}
	if path.len() == 0 { abort!("No path present in unix socket"); }
	//First set umask to 077, so the socket privileges are not excessive.
	let old_umask = Umask::PRIVATE.swap();
	//Create the socket. These sockets do inherit.
	let filedesc = FileDescriptor::socket(SocketFamily::UNIX, SocketType::STREAM, Default::default()).
		unwrap_or_else(|err|abort!("Failed to create socket: {err}"));
	//Try unlink the file to make space for socket.
	Path::new(path).symlink_metadata().map(|_|remove_file(path).
		unwrap_or_else(|err|abort!("Failed to unlink socket: {err}"))).ok();
	//Bind the socket and set to listen.
	filedesc.bind_ex(&SocketAddrEx::Unix(path.to_owned().into_bytes())).unwrap_or_else(|err|{
		abort!("Failed to bind socket: {err}")
	});
	filedesc.listen(10).unwrap_or_else(|err|{
		abort!("Failed to listen socket: {err}")
	});
	//Restore umask and set privileges.
	old_umask.swap();
	set_privileges(path, user, group, users, groups, world);
	//Forget the filedesc in order to not close it.
	let fd = filedesc.as_raw_fd();
	forget(filedesc);
	fd
}

fn make_socket_common(addr: SocketAddrEx, secondary: bool) -> RawFdT
{
	let af = addr.get_family().unwrap_or_else(||abort!("Trying to create unknown type socket"));
	//Create the socket. These sockets do inherit.
	let fd = FileDescriptor::socket(af, SocketType::STREAM, Default::default()).unwrap_or_else(|err|{
		abort!("Failed to create socket: {err}")
	});
	fd.set_reuseaddr().unwrap_or_else(|err|{
		abort!("Failed to set SO_REUSEADDR: {err}")
	});
	if let Err(err) = fd.bind_ex(&addr) {
		//If secondary, allow bind failure due to address in use.
		if err.raw_os_error() == Some(OsError::EADDRINUSE.to_inner()) && secondary { return -1; }
		abort!("Failed to bind to socket: {err}")
	}
	fd.listen(10).unwrap_or_else(|err|{
		abort!("Failed to listen to socket: {err}")
	});
	let fdesc = fd.as_raw_fd();
	forget(fd);
	fdesc
}

fn make_socket_wild(port: u16, ipv6: bool) -> RawFdT
{
	make_socket_common(if ipv6 {
		SocketAddrEx::V6(SocketAddrV6::new(Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 0), port, 0, 0))
	} else {
		SocketAddrEx::V4(SocketAddrV4::new(Ipv4Addr::new(0, 0, 0, 0), port))
	}, !ipv6)
}

fn make_socket_spec(addr: SocketAddr, spec: &str) -> RawFdT
{
	make_socket_common(match addr {
		SocketAddr::V4(x) => if x.ip().is_unspecified() { Err(x.port()) } else { Ok(SocketAddrEx::V4(x)) },
		SocketAddr::V6(x) => if x.ip().is_unspecified() { Err(x.port()) } else { Ok(SocketAddrEx::V6(x)) },
	}.unwrap_or_else(|p|abort!("Bad listener spec '{spec}' (did you mean '*:{p}'?)")), false)
}


fn listen_one(spec: &str) -> Vec<RawFdT>
{
	let last_colon = spec.rfind(':').unwrap_or_else(||{
		abort!("Expected at least one ':' in listener address specification '{spec}' \
			(did you mean '*:{spec}'?)")
	});
	let addr = &spec[..last_colon];
	let port = &spec[last_colon+1..];
	let portnum = u16::from_str(port).unwrap_or_else(|_|{
		abort!("Invalid port number '{port}' in listener address specification")
	});

	let mut ret = Vec::new();
	if addr == "*" {
		//Some OSes (especially *BSD) doesn't support V6ONLY, so we need to complicate this code.
		ret.push(make_socket_wild(portnum, true));
		//make_socket_wild with IPv6 = false may return -1 if socket is already in use. This happens if
		//V6ONLY is false by default (we bound to that socket above).
		let wild4 = make_socket_wild(portnum, false);
		if wild4 >= 0 { ret.push(wild4); }
	} else {
		let saddr = spec.to_socket_addrs().unwrap_or_else(|err|{
			abort!("Error parsing listener specification '{spec}': {err}")
		});
		for i in saddr {
			ret.push(make_socket_spec(i, spec));
		}
		if ret.len() == 0 {
			abort!("Listener address specification '{spec}' does not match any addresses")
		}
	}
	ret
}

fn switch_user(user: &str)
{
	let ent = Uid::record_by_name(user.as_bytes()).
		unwrap_or_else(||abort!("User {user} not found in user database"));
	let uid = ent.uid();
	let gid = ent.gid();
	let extra: &[Gid] = ent.extra_gids();
	//Change credentials.
	Gid::change_extra(extra).unwrap_or_else(|err|abort!("Setting supplementary groups failed: {err}"));
	gid.change().unwrap_or_else(|err|abort!("Failed to change group: {err}"));
	uid.change().unwrap_or_else(|err|abort!("Failed to change user: {err}"));
	if Uid::get_real() != uid { abort!("OS bug: setuid() failed silently!"); }
	if Gid::get_real() != gid { abort!("OS bug: setgid() failed silently!"); }
}

fn daemon_loop(processes: u32, pidfile: Option<String>, listen: &[RawFdT], config: Config) -> !
{
	let emptymask = SignalSet::new();
	//Create signal pipe and message pipe. The message pipe is connected to STDERR, which clears O_CLOEXEC
	//anyway. The ACK pipe should never be leaked to the child, so it should be O_CLOEXEC. null2 is duped to
	//STDIN/STDOUT, which are forcibly !O_CLOEXEC anyway.
	let flags = PipeFlags::CLOEXEC;
	let (rmsg, wmsg) = FileDescriptor::pipe2(flags).unwrap_or_else(|err|{
		abort!("Failed to create message pipe: {err}")
	});
	let (rack, wack) = FileDescriptor::pipe2(flags).unwrap_or_else(|err|{
		abort!("Failed to create ack pipe: {err}")
	});
	let null2 = UnixPath::dev_null().open(OpenFlags::O_RDWR|OpenFlags::O_CLOEXEC).unwrap_or_else(|err|{
		abort!("Failed to open /dev/null: {err}")
	});

	//First fork twice.
	match Pid::fork() {
		Ok(Some(pid)) => {
			//Close write ends of pipes.
			drop(wmsg);
			drop(wack);
			let mut child = ChildProcess::new(pid, rmsg, rack);
			let mut pv = PollVector::new();
			child.link_poll(&mut pv);
			loop {
				pv.arm_all_read();
				if let Err(err) = unsafe{PollFd::ppoll(pv.borrow(), -1, Some(emptymask))} {
					//The above ppoll is liable to get interrupted by EINTR if a signal is sent.
					if !err.is_transient() {
						syslog!(CRITICAL "ppoll failed: {err}");
						continue
					}
				}
				match child.poll_child(&mut pv, true) {
					ChildPollResult::Launched => {
						//This means that the daemon launched successfully. Kill the
						//child task and exit successfully.
						if let Err(err) = child.send_signal(Signal::KILL, true) {
							syslog!(CRITICAL "kill({pid}) failed: {err}");
						}
						exit(0);
					},
					ChildPollResult::Failed => {
						//The launch failed. The error has already been printed.
						if let Err(err) = child.send_signal(Signal::KILL, true) {
							syslog!(CRITICAL "kill({pid}) failed: {err}");
						}
						exit(1);
					},
					ChildPollResult::Neutral => ()
				}
			}
		},
		Ok(None) => {
			//Close read ends of pipes, dup wmsg into stderr and null out stdin/stdout. For logging,
			//log to parent. Note that the grandchildren will call nodetach_loop(), which will in turn
			//call ParentProcess::new(), which in turn sets logging mode. Since this is daemon_loop(),
			//it is not possible to have logging mode LOGMODE_NODETACH, as nodetach calls
			//nodetach_loop() directly.
			drop(rmsg);
			drop(rack);
			null2.dup2(SpecialFd::STDIN).unwrap_or_else(|err|{
				abort!("Failed to dup2: {err}")
			});
			null2.dup2(SpecialFd::STDOUT).unwrap_or_else(|err|{
				abort!("Failed to dup2: {err}")
			});
			wmsg.dup_stderr().unwrap_or_else(|err|{
				abort!("Failed to dup2: {err}")
			});
			log_subprocess();
			setsid().ok();		//This should never fail.
			match Pid::fork() {
				Ok(Some(_)) => {
					drop(wack);	//Close this pipe, so our child dying will drop it.
					//Just hang around, the parent task will kill us.
					loop { sleep(Duration::from_secs(60)); }
				},
				Ok(None) => {
					//This the child task.
					nodetach_loop(processes, listen, wack.as_raw_fd(), pidfile, config);
				},
				Err(err) => {
					//Failed to start child task.
					abort!("Fork failed: {err}");
				}
			}
		},
		Err(err) => abort!("Fork failed: {err}")
	}
}
