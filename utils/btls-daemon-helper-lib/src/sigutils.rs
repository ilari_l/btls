use super::FileDescriptor;
use super::register_sigaction;
use btls_aux_collections::Mutex;
use btls_aux_collections::Condvar;
use btls_aux_unix::FileDescriptorB;
use btls_aux_unix::Pid;
use btls_aux_unix::RawFdT;
use btls_aux_unix::RawSignalT;
use btls_aux_unix::Signal;
use btls_aux_unix::SignalHandler;
use btls_aux_unix::SignalSet;
use btls_util_logging::syslog;
use std::mem::forget;
use std::io::Error as IoError;
use std::sync::Arc;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering as MemOrder;

static CLOSE_ON_SIGHUP: AtomicUsize = AtomicUsize::new(0);
static GLOBAL_SIGHUP_FLAG: AtomicBool = AtomicBool::new(false);

unsafe extern "C" fn sighup_handler(_signum: RawSignalT)
{
	GLOBAL_SIGHUP_FLAG.store(true, MemOrder::Relaxed);
	do_sighup_close();
}

unsafe extern "C" fn dummy_handler(_signum: RawSignalT) {}

pub fn catch_sighup_default() { unsafe { register_sigaction(Signal::HUP, sighup_handler); } }
pub fn catch_sigpipe_default() { unsafe { register_sigaction(Signal::PIPE, dummy_handler); } }
pub fn get_sighup_flag() -> &'static AtomicBool { &GLOBAL_SIGHUP_FLAG }
pub fn set_close_on_sighup(fd: i32) { CLOSE_ON_SIGHUP.store(fd as usize + 1, MemOrder::SeqCst); }

pub fn setup_sighup_pipe() -> Result<FileDescriptor, IoError>
{
	let (rsigpipe, wsigpipe) = FileDescriptor::pipe()?;
	set_close_on_sighup(wsigpipe.as_raw_fd());
	forget(wsigpipe);	//Must not be closed.
	Ok(rsigpipe)
}


pub fn do_sighup_close()
{
	//If CLOSE_ON_SIGHUP is set, close that file descriptor in order to quickly exit other tasks.
	let fd = CLOSE_ON_SIGHUP.load(MemOrder::SeqCst);
	if fd > 0 { unsafe { FileDescriptorB::new((fd - 1) as RawFdT).close(); }}
}

pub fn gettid() -> i32 { Pid::current_thread().to_inner() as i32 }


pub fn do_mask_sighup()
{
	//Block SIGHUP. Some other thread that does not have SIGHUP blocked will take it.
	let mut sighup = SignalSet::new();
	sighup.add(Signal::HUP);
	if let Err(err) = sighup.block_signals() {
		syslog!(CRITICAL "Failed to set signal mask: {err}");
	}
}

pub fn do_unshare_files() -> Result<(), IoError>
{
	use super::decouple_thread_fd_count;
	use ::os_error_to_io_error;
	use btls_aux_unix::UnshareFlags;
	UnshareFlags::FILES.unshare().map_err(os_error_to_io_error)?;
	decouple_thread_fd_count();
	Ok(())
}

#[derive(Clone)]
pub struct ThreadReport
{
	lock: Arc<Mutex<()>>,
	cond: Arc<Condvar>,
	count: Arc<AtomicUsize>,
	rout: Arc<AtomicUsize>,
}

impl ThreadReport
{
	pub fn new() -> ThreadReport
	{
		ThreadReport {
			lock: Arc::new(Mutex::new(())),
			cond: Arc::new(Condvar::new()),
			count: Arc::new(AtomicUsize::new(0)),
			rout: Arc::new(AtomicUsize::new(0)),
		}
	}
	pub fn report_in(&self)
	{
		self.count.fetch_add(1, MemOrder::SeqCst);
		let _dummy = self.lock.lock();
		self.cond.notify_all();
	}
	pub fn report_out(&self)
	{
		self.count.fetch_sub(1, MemOrder::SeqCst);
		self.rout.fetch_add(1, MemOrder::SeqCst);
		let _dummy = self.lock.lock();
		self.cond.notify_all();
	}
	pub fn errors(&self) -> usize { self.rout.load(MemOrder::SeqCst) }
	pub fn wait<C>(&self, condition: C) -> Result<(), ()> where C: Fn(usize, usize) -> bool
	{
		loop {
			let guard = self.lock.lock();
			//If all threads are not in yet, wait.
			let c1 = self.count.load(MemOrder::SeqCst);
			let c2 = self.rout.load(MemOrder::SeqCst);
			if condition(c1, c2) { break; }
			self.cond.wait(guard);
		}
		Ok(())
	}
}

pub fn ignore_sigpipe()
{
	//Set SIGPIPE to ignore.
	unsafe{Signal::PIPE.set_handler(SignalHandler::Ignore);}
}

///Unblock all signals and return set of unreserved signals.
pub fn unblock_all_signals() -> SignalSet
{
	SignalSet::blockables().set_signals().unwrap_or_else(|err|{
		abort!("block signals: {err}")
	});
	crate::sigutils::unhook_hardware_errors();
	let mut sigs = SignalSet::new().set_signals().unwrap_or_else(|err|{
		abort!("unblock signals: {err}")
	});
	//Consider SIGPIPE reserved.
	sigs.delete(Signal::PIPE);
	sigs
}

///Unhook all signal handlers of hardware error type.
pub fn unhook_hardware_errors()
{
	//Take SIGABRT to be hardware-type error, even if it is not really.
	let hardware_errors = [
		Signal::ABRT,Signal::BUS,Signal::FPE,Signal::ILL,Signal::SEGV,Signal::SYS,Signal::TRAP,Signal::XCPU,
		Signal::XFSZ
	];
	let mut set = SignalSet::new();
	for &signal in hardware_errors.iter() {
		set.add(signal);
		unsafe{signal.set_handler(SignalHandler::Default);}
	}
	set.unblock_signals().unwrap_or_else(|err|{
		abort!("Failed to unblock hardware error signals: {err}")
	});
}
