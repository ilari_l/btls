#![warn(unsafe_op_in_unsafe_fn)]
#[macro_use] extern crate btls_aux_fail;
use btls_aux_collections::Condvar;
use btls_aux_collections::Mutex;
use btls_aux_memory::iterate_mostly_utf8_fragments;
use btls_aux_memory::MUtf8StringFragment;
use btls_aux_memory::PrintBuffer;
use btls_aux_memory::SmallVec;
use btls_aux_time::ShowTime;
use btls_aux_unix::AclPermEntry;
use btls_aux_unix::AclWrite;
use btls_aux_unix::FileDescriptor;
use btls_aux_unix::FileDescriptorB;
use btls_aux_unix::Gid;
use btls_aux_unix::LogFacility;
use btls_aux_unix::Logger;
use btls_aux_unix::LogLevel;
use btls_aux_unix::OpenFlags;
use btls_aux_unix::OpenlogFlags;
use btls_aux_unix::OsError;
use btls_aux_unix::Path as UnixPath;
use btls_aux_unix::Pid;
use btls_aux_unix::Uid;
use std::borrow::Cow;
use std::cell::Cell;
use std::cell::RefCell;
use std::cmp::min;
use std::fmt::Arguments;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::fmt::Write as FmtWrite;
use std::io::Error as IoError;
use std::io::ErrorKind as IoErrorKind;
use std::io::stderr;
use std::io::Write;
use std::mem::MaybeUninit;
use std::mem::replace;
use std::mem::transmute;
use std::ops::DerefMut;
use std::os::unix::ffi::OsStrExt;
use std::os::unix::ffi::OsStringExt;
use std::path::Path;
use std::path::PathBuf;
use std::ptr::null_mut;
use std::rc::Rc;
use std::str::from_utf8;
use std::sync::Arc;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::AtomicPtr;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering as MemOrder;
use std::thread::Builder as ThreadBuilder;

#[macro_export]
macro_rules! log
{
	(_ $level:expr, $($items:tt)*) => {{ $crate::do_log_line($level, format_args!($($items)*)); () }};
	(DEBUG $($items:tt)*) => { log!(_ 'D', $($items)*) };
	(INFO $($items:tt)*) => { log!(_ 'I', $($items)*) };
	(NOTICE $($items:tt)*) => { log!(_ 'N', $($items)*) };
	(WARNING $($items:tt)*) => { log!(_ 'W', $($items)*) };
	(ERROR $($items:tt)*) => { log!(_ 'E', $($items)*) };
	(CRITICAL $($items:tt)*) => { log!(_ 'C', $($items)*) };
	(ALERT $($items:tt)*) => { log!(_ 'A', $($items)*) };
}

#[macro_export]
macro_rules! syslog
{
	(_ $level:expr, $($items:tt)*) => {{ $crate::do_log_line($level, format_args!($($items)*)); () }};
	(DEBUG $($items:tt)*) => { syslog!(_ 'd', $($items)*) };
	(INFO $($items:tt)*) => { syslog!(_ 'i', $($items)*) };
	(NOTICE $($items:tt)*) => { syslog!(_ 'n', $($items)*) };
	(WARNING $($items:tt)*) => { syslog!(_ 'w', $($items)*) };
	(ERROR $($items:tt)*) => { syslog!(_ 'e', $($items)*) };
	(CRITICAL $($items:tt)*) => { syslog!(_ 'c', $($items)*) };
	(ALERT $($items:tt)*) => { syslog!(_ 'a', $($items)*) };
	(FATAL $($items:tt)*) => { syslog!(_ 'f', $($items)*) };
}

fn os_error_to_io_error(e: OsError) -> IoError { IoError::from_raw_os_error(e.to_inner()) }


//0 => Startup mode: Explicit prefixes are printed.
//1 => Subprocess mode: The priority info is printed.
//2 => Syslog mode: Logs to syslog.
//3 => Nodetach mode: Like startup mode, but openlog silently fails.
//4 => Systemd mode: Logs to Systemd.
static LOGMODE: AtomicUsize = AtomicUsize::new(0);
const LOGMODE_STARTUP: usize = 0;
const LOGMODE_SUBPROCESS: usize = 1;
const LOGMODE_SYSLOG: usize = 2;
const LOGMODE_NODETACH: usize = 3;
const LOGMODE_SYSTEMD: usize = 4;

fn log_locked(mode: usize) -> bool
{
	mode == LOGMODE_SYSLOG || mode == LOGMODE_NODETACH || mode == LOGMODE_SYSTEMD
}

fn call_openlog_logfile(logpath: Option<&Path>, mut logpath_pii: Option<&Path>)
{
	//If split logging is disabled, logfiles are the same.
	if get_no_syslog() { logpath_pii = None; }
	//If there is secondary logfile specified, open it. Note that if logfile_pii is set, then logfile must be
	//as well. Default for logfile_pii is logfile.
	if let Some(logpath) = logpath {
		let logpath_pii = logpath_pii.unwrap_or(logpath);
		log_sink_init(logpath, logpath_pii);
	}
}

pub fn call_openlog_systemd(logpath: Option<&Path>, logpath_pii: Option<&Path>)
{
	if log_locked(LOGMODE.load(MemOrder::Relaxed)) { return; }
	call_openlog_logfile(logpath, logpath_pii);
	LOGMODE.store(LOGMODE_SYSTEMD, MemOrder::Release);
}

pub fn call_openlog(ident: &[u8], option: OpenlogFlags, facility: LogFacility, logpath: Option<&Path>,
	logpath_pii: Option<&Path>)
{
	if log_locked(LOGMODE.load(MemOrder::Relaxed)) { return; }
	Logger::openlog(ident, option, facility);
	call_openlog_logfile(logpath, logpath_pii);
	LOGMODE.store(LOGMODE_SYSLOG, MemOrder::Release);
}

pub fn log_subprocess() { LOGMODE.store(LOGMODE_SUBPROCESS, MemOrder::Relaxed); }
pub fn log_nodetach() { LOGMODE.store(LOGMODE_NODETACH, MemOrder::Relaxed); }

fn priority_to_string(priority: char) -> &'static str
{
	match priority {
		'D'|'d' => "Debug",
		'I'|'i' => "Informational",
		'N'|'n' => "Notice",
		'W'|'w' => "Warning",
		'E'|'e' => "Error",
		'C'|'c' => "Critical",
		'A'|'a' => "Alert",
		'F'|'f' => "Fatal",
		'G'|'g' => "Emergency",
		_ => "???"
	}
}

fn priority_to_syslog(priority: char) -> LogLevel
{
	match priority {
		'D'|'d' => LogLevel::Debug,
		'I'|'i' => LogLevel::Informational,
		'N'|'n' => LogLevel::Notice,
		'W'|'w' => LogLevel::Warning,
		'E'|'e' => LogLevel::Error,
		'C'|'c' => LogLevel::Critical,
		'A'|'a' => LogLevel::Alert,
		'F'|'f' => LogLevel::Alert,		//Map fatal errors to LOG_ALERT.
		'G'|'g' => LogLevel::Emergency,
		_ => LogLevel::Warning,		//???
	}
}

fn priority_is_high(priority: char) -> bool
{
	match priority {
		//Warning, Error, Critical, Alert, Fatal, emerGency. Except for non-system stuff, threshold is
		//Critical.
		        'C'|'A'|'F'|'G' => true,
		'w'|'e'|'c'|'a'|'f'|'g' => true,
		_ => false
	}
}

struct LogSinkInner
{
	queue: Vec<(bool, String)>,
	fd: Option<Arc<FileDescriptor>>,
	fd_sys: Option<Arc<FileDescriptor>>,
}

struct LogSinkMid
{
	data: Mutex<LogSinkInner>,
	condition: Condvar,
}

struct LogSink
{
	mid: LogSinkMid,
	filepath: PathBuf,
	filepath_pii: PathBuf,
	error: Cell<bool>,
	overflow: Cell<bool>,
}

//Assumes LOG_SINK is set!
fn log_sink_thread()
{
	let log_sink = get_log_sink().expect("LOG_SINK needs to be set in log_sink_thread()");
	let log_sink = &log_sink.mid;
	let sysmask = get_no_syslog();
	loop {
		//Wait for something to appear in the queue.
		let mut guard = log_sink.data.lock();
		while guard.queue.is_empty() {
			guard = log_sink.condition.wait(guard);
		}
		//Read the contents of the guard and drop it. The actual write calls MUST occur with guard
		//unlocked, since those can be slow. If fd does not exist, just drop the messages.
		let mut queue = replace(&mut guard.queue, Vec::new());
		let fd = guard.fd.clone();
		let fd_sys = guard.fd_sys.clone();
		drop(guard);

		//Write all accumulated lines and free the memory.
		for (system,logline) in queue.drain(..) {
			let fdesc = if system || sysmask { fd_sys.as_ref() } else { fd.as_ref() };
			//Just drop messages going to nowhere.
			let fdesc = f_continue!(fdesc);
			let mut itr = 0;
			let logline = logline.as_bytes();
			while itr < logline.len() {
				match fdesc.write(&logline[itr..]) {
					Ok(r) => itr += r,
					//Retry these.
					Err(x) if x == OsError::EINTR || x == OsError::EAGAIN ||
						x == OsError::EWOULDBLOCK => (),	//Retry these.
					//On any other error, just drop data, as we have no idea how to handle
					//this.
					Err(err) => {
						let err = format!("<1>Error writing to log, dropping data: {err}");
						stderr().write_all(err.as_bytes()).ok();
						return;
					},
				}
			}
		}
	}
}

static LOG_SINK: AtomicPtr<LogSink> = AtomicPtr::new(null_mut());

fn get_log_sink() -> Option<&'static LogSink>
{ unsafe {
	//NULL pointer optimization makes the layouts compatible.
	transmute(LOG_SINK.load(MemOrder::Relaxed))
}}

fn set_log_sink(b: LogSink)
{
	LOG_SINK.store(Box::into_raw(Box::new(b)), MemOrder::Release)
}

fn log_sink_init(logpath: &Path, logpath_pii: &Path)
{
	let dual_logs = logpath != logpath_pii;
	let sink = LogSink {
		mid: LogSinkMid {
			data: Mutex::new(LogSinkInner {
				queue: Vec::new(),
				fd: None,
				fd_sys: None,
			}),
			condition: Condvar::new(),
		},
		filepath: logpath.to_path_buf(),
		filepath_pii: logpath_pii.to_path_buf(),
		error: Cell::new(false),
		overflow: Cell::new(false),
	};
	//Start thread. If this fails, set the error flag.
	set_log_sink(sink);
	let builder = ThreadBuilder::new();
	if let Some(log_sink) = get_log_sink() {
		log_sink.error.set(builder.spawn(log_sink_thread).is_err());
	}
	_log_sink_reopen(true);	//Actually open the log.
	//Only call non-system open if file paths differ.
	if dual_logs { _log_sink_reopen(false); }
}

pub fn log_sink_get_parent() -> Option<PathBuf>
{
	if let Some(log_sink) = get_log_sink() {
		//Use current directory if parent() gives None or empty.
		let parent = log_sink.filepath.parent().unwrap_or_else(||Path::new("."));
		let parent = if parent.to_str() != Some("") { parent } else { Path::new(".") };
		let parent = parent.canonicalize().ok()?;
		Some(parent)
	} else {
		None
	}
}

pub fn log_sink_process_inotify(file: &[u8], mask: u32)
{
	if let Some(log_sink) = get_log_sink() {
		let fname = f_return!(log_sink.filepath.file_name());
		let fname_pii = f_return!(log_sink.filepath_pii.file_name());
		//Reopen the logfile in order to disown the previous log and start new.
		//Only react to MOVED_FROM and DELETED on the actual logfile or logfile_pii path.
		//Note that fname and fname_pii might be the same, in this case the logs should be merged.
		if fname.as_bytes() == file && mask & 576 != 0 {
			_log_sink_reopen(true);
		} else if fname_pii.as_bytes() == file && mask & 576 != 0 {
			_log_sink_reopen(false);
		}
	}
}

fn open_file(path: &[u8], flags: OpenFlags) -> Result<FileDescriptor, IoError>
{
	let path = UnixPath::new(path).
		ok_or_else(||IoError::new(IoErrorKind::InvalidInput, "NUL character in path to open"))?;
	let fd = path.open(flags).map_err(os_error_to_io_error)?;
	Ok(fd)
}

fn open_logfile(fpath: &Path) -> Option<Arc<FileDescriptor>>
{
	//384 => mode 600, as information may be sensitive.
	let fflags = OpenFlags::O_WRONLY | OpenFlags::O_CREAT | OpenFlags::O_APPEND | OpenFlags::O_CLOEXEC;
	let path = fpath.to_path_buf().into_os_string().into_vec();
	let fd = open_file(&path, fflags).map_err(|err|{
		do_log_line('E', format_args!("Failed to open {fpath}: {err}", fpath=fpath.display()))
	}).ok()?;
	//Set permissions on the newly opened log.
	unsafe { set_logfile_permissions(fd.as_fdb()); }
	Some(Arc::new(fd))
}

fn _log_sink_reopen(system: bool)
{
	if let Some(log_sink) = get_log_sink() {
		//Do not open the file if logger thread could not be created.
		if log_sink.error.get() { return; }
		let single_log = log_sink.filepath == log_sink.filepath_pii;
		//Split the fd/fd_sys.
		let fpath = if system { &log_sink.filepath } else { &log_sink.filepath_pii };
		let fd = open_logfile(fpath);
		//Switch the log. This closes the old logfile even if new one can not be opened.
		//The fd log is switched if !system or if there is only a single log.
		//The fd_sys log is switched if system or if there is only a single log.
		let mut guard = log_sink.mid.data.lock();
		if !system || single_log { guard.fd = fd.clone(); }
		if system || single_log { guard.fd_sys = fd; }
	}
}

fn log_sink_syslog(line: Arguments, system: bool) -> bool
{
	if let Some(log_sink) = get_log_sink() {
		//If error is set, do not accept.
		if log_sink.error.get() { return false; }
		//If no FD set, do not accept.
		let mut guard = log_sink.mid.data.lock();
		let guard: &mut LogSinkInner = guard.deref_mut();
		let queue = &mut guard.queue;
                let fd = if system { &guard.fd_sys } else { &guard.fd };
		if fd.is_none() { return false; }
		let need_wake = queue.is_empty();
		//Guard against queue growing too big.
		if need_wake  {
			//Queue has cleared.
			log_sink.overflow.set(false);
		} else if log_sink.overflow.get() {
			//Message will just get lost.
			return true;
		} else if queue.len() > 5000 {
			//Log message about lost messages and lose this message. The queue is never empty, so
			//wakeup is never required (there is already one pending). This message is always
			//considered system.
			let msg = "Critical: Disk unable to keep up! Messages will be lost!\n";
			queue.push((true, msg.to_string()));
			log_sink.overflow.set(true);
			return true;
		}
		queue.push((system, format!("{line}\n")));
		//If there were no events before this, wake up the log writer.
		if need_wake { log_sink.mid.condition.notify_all(); }
		//Accepted.
		true
	} else {
		false           //Reject write.
	}
}


struct OptPID(Option<Pid>);
struct TimeStampNow;

impl Display for OptPID
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		match self.0 { Some(pid) => write!(fmt, "[pid {pid}] "), None => Ok(()) }
	}
}

impl Display for TimeStampNow
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		Display::fmt(&ShowTime::now().format3(), fmt)
	}
}

fn need_syslog(lmode: usize, cbr: bool, priority: char) -> Option<(Logger, LogLevel)>
{
	if !cbr || priority_is_high(priority) {
		let syslog_prio = priority_to_syslog(priority);
		let facility = if lmode == LOGMODE_SYSTEMD {
			Logger::StderrSystemD
		} else {
			Logger::Syslog
		};
		Some((facility, syslog_prio))
	} else {
		None
	}
}

fn deal_with_non_utf8_crap<'a>(x: &'a [u8]) -> Cow<'a, str>
{
	//If input is valid UTF-8, return it as-is. This should always happen.
	if let Ok(x) = from_utf8(x) { return Cow::Borrowed(x); }
	let mut y = String::new();
	for part in iterate_mostly_utf8_fragments(x) { match part {
		MUtf8StringFragment::String(z) => y.push_str(z),
		//0xE000-0xE0FF is always valid char.
		MUtf8StringFragment::Byte(z) => y.push(unsafe{char::from_u32_unchecked(0xE000 + z as u32)})
	}}
	Cow::Owned(y)
}

//do_log_line_raw0() is only used by msgpipe code, which means it can never originate messages. Instead, all
//originating messages use do_log_line(). Which means there is no need to pay attention to CURRENT_TASK here.
pub fn do_log_line_raw0(priority: char, line0: &[u8], pid: Option<Pid>)
{
	let mut buf = SmallVec::<u8,8192>::new();

	match LOGMODE.load(MemOrder::Acquire) {
		LOGMODE_STARTUP|LOGMODE_NODETACH => {
			buf.extend_fitting(priority_to_string(priority).as_bytes());
			buf.extend_fitting(b": ");
			if let Some(pid) = pid { buf.extend_fitting(format!("[pid {pid}] ").as_bytes()); }
			buf.extend_fitting(line0);
			//Force linefeed to last position if line is too long.
			//swap_element() has interesting property of extending the slice if it hits position adjacent
			//to last filled.
			let len = min(buf.len(), buf.capacity() - 1);
			buf.swap_element(len, 10);
			stderr().write_all(buf.as_inner()).ok();
		},
		//do_log_line_raw0() is never supposed to be called in LOGMODE_SUBPROCESS. See the comment in
		//threading.rs above the call, which is the only caller of do_log_line_raw0().
		lmode => {
			//Syslog or systemd.
			let line0 = deal_with_non_utf8_crap(line0);
			let system = !priority.is_ascii_uppercase();
			let cbr = log_sink_syslog(format_args!("{ts} {prio}: {pid}{line0}",
				ts=TimeStampNow, prio=priority_to_string(priority), pid=OptPID(pid)), system);
			if let Some((facility, syslog_prio)) = need_syslog(lmode, cbr, priority) {
				if let Some(pid) = pid {
					facility.log(None, syslog_prio, format_args!("[pid {pid}] {line0}"));
				} else {
					facility.log(None, syslog_prio, format_args!("{line0}"));
				}
			}
		}
	}
}

#[doc(hidden)]
pub fn do_log_line(priority: char, args: std::fmt::Arguments)
{
	let mut buf = [MaybeUninit::uninit();8192];
	let mut buf = PrintBuffer::new(&mut buf);
	//Lowercase priority letters mean system message with no task shown.
	let show_task = PrintCurrentTask(priority.is_ascii_uppercase());
	match LOGMODE.load(MemOrder::Relaxed) {
		LOGMODE_STARTUP|LOGMODE_NODETACH => {
			write!(buf, "{prio}: {show_task}{args}", prio=priority_to_string(priority)).ok();
			buf.force_last_linefeed();
			stderr().write_all(buf.into_inner_bytes()).ok();
		},
		LOGMODE_SUBPROCESS => {
			write!(buf, "<{priority}>{show_task}{args}").ok();
			buf.force_last_linefeed();
			stderr().write_all(buf.into_inner_bytes()).ok();
		},
		lmode => {
			let system = !priority.is_ascii_uppercase();
			let cbr = log_sink_syslog(format_args!("{ts} {prio}: {show_task}{args}",
				ts=TimeStampNow, prio=priority_to_string(priority)), system);
			if let Some((facility, syslog_prio)) = need_syslog(lmode, cbr, priority) {
				facility.log(None, syslog_prio, format_args!("{show_task}{args}"));
			}
		}
	}
}

static LOG_PERMS: AtomicPtr<LogPermissions> = AtomicPtr::new(null_mut());

//Not thread-safe.
pub unsafe fn set_log_permissions(perms: LogPermissions)
{
	let ptr = LOG_PERMS.load(MemOrder::Acquire);
	if ptr.is_null() {
		//Create a new box.
		let ptr = Box::into_raw(Box::new(perms));
		LOG_PERMS.store(ptr, MemOrder::Release);
	} else {
		//Just reassign the existing box.
		unsafe{*ptr = perms;}
	}
}

//Wrapper for translating permissions.
struct LogPermissionsX<'a>(&'a LogPermissions, AclPermEntry, AclPermEntry);
#[derive(Clone)]
pub struct LogPermissions
{
	gid: Option<Gid>,
	grant_uid: Vec<Uid>,
	grant_gid: Vec<Gid>,
}

impl LogPermissions
{
	pub fn new() -> LogPermissions
	{
		LogPermissions {
			gid: None,
			grant_uid: Vec::new(),
			grant_gid: Vec::new(),
		}
	}
	pub fn set_gid(&mut self, gid: Gid) { self.gid = Some(gid); }
	pub fn grant_uid(&mut self, uid: Uid) { self.grant_uid.push(uid); }
	pub fn grant_gid(&mut self, gid: Gid) { self.grant_gid.push(gid); }
	//The file has to be open
	unsafe fn set_permissions(&self, fd: FileDescriptorB) -> Result<(), String>
	{
		let perms = LogPermissionsX(self, AclPermEntry::R, AclPermEntry::RW);
		if let Some(gid) = self.gid {
			unsafe{fd.chown(Uid::get_effective(), gid)}.map_err(|err|format!("chown: {err}"))?;
		}
		unsafe{fd.chmod(perms.get_mode())}.map_err(|err|format!("chmod: {err}"))?;
		unsafe{fd.setfacl(&perms)}.map_err(|err|format!("setfacl: {err}"))?;
		Ok(())
	}
	fn set_permissions_file_rw(&self, file: &[u8]) -> Result<(), String>
	{
		let file = UnixPath::new(file).ok_or_else(||format!("Invalid filename"))?;
		let perms = LogPermissionsX(self, AclPermEntry::RW, AclPermEntry::RW);
		if let Some(gid) = self.gid {
			file.chown(Uid::get_effective(), gid).map_err(|err|format!("chown: {err}"))?;
		}
		file.chmod(perms.get_mode()).map_err(|err|format!("chmod: {err}"))?;
		file.setfacl(&perms).map_err(|err|format!("setfacl: {err}"))?;
		Ok(())
	}
}

pub unsafe fn set_logfile_permissions(fd: FileDescriptorB)
{
	//NULL is guranteed to remap to None.
	let perms: Option<&LogPermissions> = unsafe{transmute(LOG_PERMS.load(MemOrder::Acquire))};
	if let Some(perms) = perms {
		if let Err(err) = unsafe{perms.set_permissions(fd)} {
			log!(WARNING "Failed to set logfile permissions: {err}");
		}
	}
}

pub fn set_logfile_permissions_file_rw(path: &[u8])
{
	//NULL is guranteed to remap to None.
	let perms: Option<&LogPermissions> = unsafe{transmute(LOG_PERMS.load(MemOrder::Acquire))};
	if let Some(perms) = perms {
		if let Err(err) = perms.set_permissions_file_rw(path) {
			log!(WARNING "Failed to set logfile permissions: {err}");
		}
	}
}

impl<'a> LogPermissionsX<'a>
{
	fn get_mode(&self) -> u32
	{
		let o = self.2.to_numeric() as u32;
		let g = self.1.to_numeric() as u32;
		64 * o + (if self.0.gid.is_some() { 8 * g } else { 0 })
	}
}

impl<'a> AclWrite for LogPermissionsX<'a>
{
	fn get_user(&self) -> AclPermEntry { self.2 }
	fn get_users<F>(&self, mut f: F) where F: FnMut(Uid, AclPermEntry)
	{
		for uid in self.0.grant_uid.iter() { f(*uid, self.1); }
	}
	fn get_group(&self) -> AclPermEntry
	{
		if self.0.gid.is_some() { self.1 } else { AclPermEntry::NONE }
	}
	fn get_groups<F>(&self, mut f: F) where F: FnMut(Gid, AclPermEntry)
	{
		for gid in self.0.grant_gid.iter() { f(*gid, self.1); }
	}
	fn get_other(&self) -> AclPermEntry { AclPermEntry::NONE }
}

thread_local!(static CURRENT_TASK: Rc<RefCell<Option<ConnId>>> = Rc::new(RefCell::new(None)));

///Set current task.
///
///Returns the previous task.
pub fn set_current_task(newtask: Option<ConnId>) -> Option<ConnId>
{
	CURRENT_TASK.with(|task|replace(task.borrow_mut().deref_mut(), newtask))
}

///Get current task.
pub fn get_current_task() -> Option<ConnId>
{
	CURRENT_TASK.with(|task|*task.borrow_mut().deref_mut())
}


#[derive(Copy,Clone)]
struct PrintCurrentTask(bool);

impl Display for PrintCurrentTask
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		if !self.0 { return Ok(()); }   //Squash task.
		if let Some(task) = CURRENT_TASK.with(|task|*task.borrow()) {
			write!(f, "[{task}] ")
		} else {
			Ok(())
		}
	}
}

#[derive(Copy,Clone,Debug)]
pub enum ConnIdPrefix
{
	None,
	Pid(u64),
	Name(&'static str),
}

impl Display for ConnIdPrefix
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&ConnIdPrefix::None => Ok(()),
			&ConnIdPrefix::Pid(p) => write!(fmt, "{p}/"),
			&ConnIdPrefix::Name(n) => write!(fmt, "{n}/"),
		}
	}
}

#[derive(Copy,Clone,Debug)]
pub struct ConnId(Option<(ConnIdPrefix, u64)>);

impl ConnId
{
	pub fn unknown() -> ConnId { ConnId(None) }
	pub fn new(pfx: ConnIdPrefix, num: u64) -> ConnId { ConnId(Some((pfx, num))) }
}

impl Display for ConnId
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		if let Some((pfx, num)) = self.0 {
			write!(fmt, "{pfx}#{num}")
		} else {
			fmt.write_str("<unknown>")
		}
	}
}


static NO_SYSLOG: AtomicBool = AtomicBool::new(false);
fn get_no_syslog() -> bool { NO_SYSLOG.load(MemOrder::Relaxed) }
pub fn set_no_syslog() { NO_SYSLOG.store(true, MemOrder::Relaxed); }
