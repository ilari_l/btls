#![warn(unsafe_op_in_unsafe_fn)]
use btls_aux_fail::dtry;
use btls_aux_fail::f_return;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::ResultExt;
use btls_aux_filename::Filename;
use btls_aux_filename::FilenameBuf;
use btls_aux_http::http::equals_case_insensitive;
use btls_aux_http::http::is_tchar;
use btls_aux_http::http::ReceiveBuffer;
use btls_aux_http::http::Source;
use btls_aux_http::http::StandardHttpHeader;
use btls_aux_http::http1::Element;
use btls_aux_http::http1::ElementSink;
use btls_aux_http::http1::IoOrStreamError;
use btls_aux_http::http1::Stream;
use btls_aux_memory::PrintBuffer;
use btls_aux_memory::trim_ascii;
use btls_aux_time::daynumber_into_date;
use btls_aux_time::number_of_day;
use btls_aux_time::PointInTime;
use btls_aux_time::ShowTime;
use btls_aux_time::TimeUnit;
use btls_aux_unix::FileDescriptor as UnixFileDescriptor;
use btls_aux_unix::FileDescriptorB as UnixFileDescriptorB;
use btls_aux_unix::OpenFlags;
use btls_aux_unix::OsError;
use btls_aux_unix::RawFdT;
use btls_aux_unix::SeekTarget;
use btls_aux_unix::Stat;
use btls_aux_unix::StatModeFormat;
use btls_daemon_helper_lib::abort;
use btls_daemon_helper_lib::AddTokenCallback;
use btls_daemon_helper_lib::AllocatedToken;
use btls_daemon_helper_lib::ConnectionInfo;
use btls_daemon_helper_lib::cow;
use btls_daemon_helper_lib::ensure_stdfd_open;
use btls_daemon_helper_lib::FdPoller;
use btls_daemon_helper_lib::FileDescriptor;
use btls_daemon_helper_lib::get_max_files;
use btls_daemon_helper_lib::incompatible_with_panic_abort;
use btls_daemon_helper_lib::initialize_from_systemd;
use btls_daemon_helper_lib::IO_WAIT_ONESHOT;
use btls_daemon_helper_lib::IO_WAIT_READ;
use btls_daemon_helper_lib::IO_WAIT_WRITE;
use btls_daemon_helper_lib::is_fatal_error;
use btls_daemon_helper_lib::Listener;
use btls_daemon_helper_lib::ListenerLoop;
use btls_daemon_helper_lib::LocalThreadHandle;
use btls_daemon_helper_lib::Poll;
use btls_daemon_helper_lib::running_as_root;
use btls_daemon_helper_lib::SighupPipe;
use btls_daemon_helper_lib::SocketAddrEx;
use btls_daemon_helper_lib::start_threads;
use btls_daemon_helper_lib::Thread;
use btls_daemon_helper_lib::ThreadNewParams;
use btls_daemon_helper_lib::ThreadSpawnedParams;
use btls_daemon_helper_lib::TokenAllocatorPool;
use btls_daemon_helper_lib::UpperLayerConnection;
use btls_daemon_helper_lib::UpperLayerConnectionFactory;
use btls_util_logging::ConnId;
use btls_util_logging::syslog;
use std::borrow::Cow;
use std::cmp::min;
use std::cmp::max;
use std::fmt::Display;
use std::fmt::Write;
use std::io::Error as IoError;
use std::mem::MaybeUninit;
use std::mem::transmute;
use std::ops::Range;
use std::str::from_utf8;
use std::str::FromStr;
use std::sync::Arc;

include!("mimetypes.inc");

macro_rules! printbuffer
{
	($buf:expr, $($args:tt)*) => {{
		use std::fmt::Write;
		let mut buffer = PrintBuffer::new(&mut $buf);
		write!(buffer, $($args)*).ok();		//This should work.
		buffer.into_inner_bytes()
	}}
}

const MAX_FDS_REDUCE: usize = 100;

#[derive(Clone)]
struct HttpWorkerParams
{
	maxfiles: usize,
	listener_fds: Vec<RawFdT>,
	configuration: Arc<Configuration>,
}

struct HttpWorker
{
	gstate: ListenerLoop
}

impl Thread for HttpWorker
{
	type Local = ();
	type LocalMetrics = ();
	type Parameters = HttpWorkerParams;
	type Parameters2 = ();
	fn new<'a>(p: ThreadNewParams<'a,Self>) -> Self
	{
		let ThreadNewParams{gparameters: gp, sigpipe: sp, tid, irqpipe, ..} = p;
		let gstate = setup_listener_loop(gp.maxfiles, sp, &gp.listener_fds, tid, irqpipe,
			gp.configuration.clone());
		HttpWorker {gstate: gstate}
	}
	fn spawned(_: ThreadSpawnedParams<Self>)
	{
		//No need to do anything here, as there are no global threads.
	}
	fn run(mut self) { self.gstate.main_event_loop(); }
}

fn main()
{
	ensure_stdfd_open();
	let sinfo = initialize_from_systemd("httpfileserv");
	incompatible_with_panic_abort();
	if running_as_root() { abort!("Can't run program as root"); }

	let listeners = sinfo.sockets();
	if listeners.len() == 0 { abort!("No ports to listen"); }

	let maxfiles = get_max_files() - MAX_FDS_REDUCE;
	syslog!(NOTICE "Using maximum of {maxfiles} file descriptors");

	let root = std::env::current_dir().unwrap_or_else(|err|abort!("Failed to obtain current directory: {err}"));
	let configuration = Configuration {
		root: FilenameBuf::from_pathbuf(root),
	};

	start_threads::<HttpWorker>(sinfo, "HTTP worker", HttpWorkerParams {
		maxfiles: maxfiles,
		listener_fds: listeners,
		configuration: Arc::new(configuration),
	}, ());
}

fn setup_listener_loop(maxfiles: usize, sigpipe: SighupPipe, listener_fds: &Vec<RawFdT>, tid: Option<u64>,
	irqpipe: LocalThreadHandle, configuration: Arc<Configuration>) -> ListenerLoop
{
	let factory: Arc<dyn UpperLayerConnectionFactory> = Arc::new(ConnectionFactory(configuration));
	//Construct listener loop. Each thread should have its own.
	let mut gstate = ListenerLoop::new(maxfiles, sigpipe, irqpipe).
		unwrap_or_else(|err|abort!("Creating event loop: {err}"));
	if let Some(tid) = tid { gstate.set_pid(tid); }
	//Add the file descriptors.
	for i in listener_fds.iter() {
		let j = Listener::new(FileDescriptor::new(*i), factory.clone());
		gstate.add_listener(j);
	}
	gstate
}

const TIMEOUT_DELAY: u64 = 300;

#[derive(Clone)]
struct ConnectionFactory(Arc<Configuration>);

impl UpperLayerConnectionFactory for ConnectionFactory
{
	fn new(&self, poll: &mut Poll, allocator: &mut TokenAllocatorPool, fd: FileDescriptor, _: ConnectionInfo) ->
		Result<(Box<dyn UpperLayerConnection>, Vec<AllocatedToken>, bool), Cow<'static, str>>
	{
		let token = allocator.allocate().set_err("Can't allocate Tokens")?;
		let fd = FdPoller::new(fd, token.value());
		let mut recv = Stream::new(b"http");
		recv.set_use_std_flag();	//Use standard headers.
		let mut connection = Connection {
			recv: recv,
			sink: ConnectionSink {
				receiving: true,
				error: None,
				configuration: self.0.clone(),
				method: Method::Other,
				accept_encoding: String::new(),
				authority: String::new(),
				if_modified_since: None,
				if_none_match: String::new(),
				if_range: String::new(),
				path: String::new(),
				range: String::new(),
				send_buf: Vec::new(),
				send_buf_itr: 0,
				body: None,
				body_left: None,
				connection_close: false,
			},
			token: token,
			fd: fd,
			error: None,
			timeout_close: PointInTime::from_now(TimeUnit::Seconds(TIMEOUT_DELAY)),
			rshutdown: false,
			error_buffer: Vec::new(),
			error_buffer_iter: 0,
			http_error: String::new(),
		};
		let tokens = vec![connection.token.clone()];
		//Wait for read on the new socket.
		connection.fd.update(poll, IO_WAIT_READ | IO_WAIT_ONESHOT).
			map_err(|err|format!("Failed to register socket: {err}"))?;
		//The address_valid is always false, because these connections can not be K-Lined at all.
		Ok((Box::new(connection), tokens, false))
	}
	//All remaps are trivial.
	fn remap(&self, addr: SocketAddrEx) -> SocketAddrEx { addr }
}

const MAX_READ: usize = 16384;

//Safety: Ensure that all the bytes up to rwb.1 are initialized in rwb.0! Otherwise borrow() is UB.
struct ReaderWriterBuffer([MaybeUninit<u8>;MAX_READ], usize);

impl ReceiveBuffer for ReaderWriterBuffer
{
	fn new() -> ReaderWriterBuffer
	{
		ReaderWriterBuffer([MaybeUninit::uninit();MAX_READ], 0)
	}
	fn borrow<'a>(&'a mut self) -> &'a mut [u8]
	{
		let s0len = self.0.len();
		unsafe{transmute(&mut self.0[..min(self.1, s0len)])}
	}
}

struct FdReader<'a>
{
	socket: FileDescriptor,
	bump_timeout: &'a mut bool,
	shutdown: &'a mut bool,
}

impl<'a> FdReader<'a>
{
	fn _readc<F>(&mut self, f: F) -> Result<usize, IoError>
		where F: FnOnce(&FileDescriptor) -> Result<usize, IoError>
	{
		//Do not read after shutdown.
		if *self.shutdown { return Ok(0); }
		let amt = f(&self.socket)?;
		//Assert shutdown if needed.
		*self.shutdown |= amt == 0;
		//Also bump the timestamp if 0 bytes got read, because that signifies EOF.
		*self.bump_timeout = true;
		Ok(amt)
	}
	fn _readu(&mut self, data: &mut [MaybeUninit<u8>]) -> Result<usize, IoError>
	{
		self._readc(|socket|Ok(socket.read_uninit2(data)?.len()))
	}
}

impl<'a> Source for FdReader<'a>
{
	type Buffer = ReaderWriterBuffer;
	type Error = IoError;
	fn read(&mut self, buf: &mut ReaderWriterBuffer) -> Result<bool, IoError>
	{
		match self._readu(&mut buf.0) {
			Ok(amt) => {
				buf.1 = amt;
				Ok(amt > 0)	//0 means EOF.
			},
			Err(e) => if is_fatal_error(&e) {
				fail!(e);
			} else {
				buf.1 = 0;	//No data here.
				Ok(true)	//Assume more data.
			}
		}
	}
}

fn check_directory_traversal(path: &[u8]) -> bool
{
	let mut state = 0x2F2F2F2F;
	for b in path.iter().cloned() {
		state = state << 8 | b as u32;
		if state == 0x2F2E2E2F || state << 8 == 0x2F2E2F00 { return true; }
	}
	state << 8 == 0x2F2E2E00 || state << 16 == 0x2F2E0000
}

fn bad_utf8() -> Option<String> { Some(format!("Bad UTF-8 in header")) }

struct Configuration
{
	root: FilenameBuf,
}

#[derive(Copy,Clone,PartialEq,Eq)]
enum Method
{
	GET,
	HEAD,
	Options,
	Other,
}

struct ConnectionSink
{
	receiving: bool,
	error: Option<String>,
	configuration: Arc<Configuration>,
	//Request.
	method: Method,
	authority: String,
	path: String,
	range: String,
	accept_encoding: String,
	if_range: String,
	if_modified_since: Option<i64>,
	if_none_match: String,
	connection_close: bool,
	//Transmission.
	send_buf: Vec<u8>,					//Also includes the error body.
	send_buf_itr: usize,
	body: Option<UnixFileDescriptor>,
	body_left: Option<u64>,					//If None, transfer is chunked.
}

macro_rules! file_read_err
{
	($bfd:expr,$buf:expr) => {
		match unsafe{$bfd.read_uninit2($buf)} {
			Ok(buf) => buf,
			Err(ref err) if err.is_transient() => continue,
			Err(err) => fail!(format!("Read(file): {err}"))
		}
	}
}

macro_rules! socket_write_err
{
	($x:expr) => {
		match $x {
			Ok(amt) => amt,
			Err(ref err) if !is_fatal_error(err) => 0,
			Err(err) => fail!(format!("Write: {err}"))
		}
	}
}

static PRODUCT_NAME: &'static str = "httpfileserve/0.9.1";

fn add_date_header(to: &mut Vec<u8>)
{
	static WEEKDAYS: [&'static str;7] = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
	static MONTHS: [&'static str;12] = [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
	];
	let mut tmp = [MaybeUninit::uninit();128];
	let ts = ShowTime::now();
	let daynum = ts.number_of_day();
	let (year, month, day) = daynumber_into_date(daynum);
	let year = year % 10000;
	let (hour, minute, second) = ts.time_of_day();
	let weekday = (daynum as usize + 3) % 7;	//1.1.1970 is Thursday.
	let tmp = {
		let mut p = PrintBuffer::new(&mut tmp);
		write!(p, "date:{wd}, {day:02} {mon} {year:04} {hour:02}:{minute:02}:{second:02} GMT\r\n",
			wd=WEEKDAYS[weekday], mon=MONTHS[month as usize - 1]).ok();
		p.into_inner_bytes()
	};
	to.extend_from_slice(tmp);
}

fn format_error(to: &mut Vec<u8>, code: u16, msg: &str, len: u64)
{
	let mut tmp = [MaybeUninit::uninit();128];
	to.extend_from_slice(b"HTTP/1.1 ");
	to.push(48 + (code / 100) as u8); 
	to.push(48 + (code / 10 % 10) as u8);
	to.push(48 + (code % 10) as u8);
	to.push(32);
	to.extend_from_slice(msg.as_bytes());
	to.extend_from_slice(b"\r\n");
	if code == 405 { to.extend_from_slice(b"allow:GET,HEAD\r\n"); }
	if code == 416 {
		to.extend_from_slice(printbuffer!(tmp, "content-range:bytes */{len}\r\n"));
	}
	to.extend_from_slice(printbuffer!(tmp, "server:{pn}\r\n", pn=PRODUCT_NAME));
	to.extend_from_slice(printbuffer!(tmp, "content-length:{len}\r\n", len=msg.len() + 1));
	add_date_header(to);
	to.extend_from_slice(b"\r\n");
	to.extend_from_slice(msg.as_bytes());
	to.push(10);
}


impl ConnectionSink
{
	fn __data_in_sendbuf(&self) -> bool
	{
		self.send_buf_itr < self.send_buf.len()
	}
	//Returns true if something was (attempted to be) transmitted.
	fn __try_transmit_sendbuf(&mut self, fd: &FileDescriptor) -> Result<bool, Cow<'static, str>>
	{
		Ok(if self.__data_in_sendbuf() {
			match fd.write_const(&self.send_buf[self.send_buf_itr..]) {
				Ok(amt) => self.send_buf_itr += amt,
				Err(ref err) if !is_fatal_error(err) => return Ok(true),
				Err(err) => fail!(format!("Write: {err}"))
			}
			//All done?
			if self.send_buf_itr >= self.send_buf.len() {
				self.send_buf_itr = 0;
				self.send_buf.clear();
			}
			true
		} else {
			false
		})
	}
	//Flush zeroes.
	fn __flush_zeroes(body_left: &mut u64, fd: &FileDescriptor) -> Result<(), Cow<'static, str>>
	{
		static ZEROES: [u8;16384] = [0;16384];
		let maxunit = min(*body_left, ZEROES.len() as u64) as usize;
		match fd.write_const(&ZEROES[..maxunit]) {
			Ok(amt) => *body_left -= amt as u64,
			Err(ref err) if !is_fatal_error(err) => return Ok(()),
			Err(err) => fail!(format!("Write: {err}"))
		};
		return Ok(())
	}
	//Read bytes from source. bfd must be open.
	fn __read_source<'a>(buf: &'a mut [MaybeUninit<u8>], body_left: &mut u64, bfd: UnixFileDescriptorB) ->
		Result<&'a [u8], Cow<'static, str>>
	{
		//Cap buffer to body_left.
		let maxunit = min(*body_left, buf.len() as u64) as usize;
		let buf = &mut buf[..maxunit];
		//We must write once, thus repeat any transient errors. The odd way of doing things is due to the
		//borrow checker not grokking the straightforward way.
		//Safety: The file is open here.
		loop {
			let buflen = file_read_err!(bfd, buf).len();
			*body_left -= buflen as u64;
			//Safety: This has all been initialized by the read.
			return Ok(unsafe{transmute(&buf[..buflen])});
		}
	}
	//Read bytes from source. bfd must be open.
	fn __read_source_chunk<'a>(buf: &'a mut [MaybeUninit<u8>], bfd: UnixFileDescriptorB) ->
		Result<&'a [u8], Cow<'static, str>>
	{
		//We must write once, thus repeat any transient errors.  The odd way of doing things is due to the
		//borrow checker not grokking the straightforward way.
		//Safety: The file is open here.
		loop {
			let buflen = file_read_err!(bfd, buf).len();
			//Safety: This has all been initialized by the read.
			return Ok(unsafe{transmute(&buf[..buflen])});
		}
	}
	//The bfd must be open!
	fn __try_transmit_body(&mut self, fd: &FileDescriptor, bfd: UnixFileDescriptorB) ->
		Result<(), Cow<'static, str>>
	{
		let mut buf = [MaybeUninit::uninit();16384];
		if let Some(ref mut body_left) = self.body_left {
			//Safety: The file is open.
			let buf = Self::__read_source(&mut buf, body_left, bfd)?;
			//If EOF (either by read or body_left running out), close the file. The code will
			//zero-fill in that case.
			if buf.len() == 0 || *body_left == 0 { self.body = None; }
			//Try writing the entiere buffer. Any overflowing parts get added to sendbuf.
			let amt = socket_write_err!(fd.write_const(buf));
			self.send_buf.extend_from_slice(&buf[amt..]);
		} else {
			//Safety: The file is open.
			let buf = Self::__read_source_chunk(&mut buf, bfd)?;
			//Emit chunk header. If EOF, close the file.
			let mut chdr = [MaybeUninit::uninit();32];
			let mut chdr = PrintBuffer::new(&mut chdr);
			if buf.len() > 0 {
				write!(chdr, "{blen:x}\r\n", blen=buf.len()).ok();
			} else {
				write!(chdr, "0\r\n\r\n").ok();
				self.body = None;	//Close the FD, there is no more data needed.
			}
			let chdr = chdr.into_inner_bytes();
			//Try writing the entiere buffer. Any overflowing parts get added to sendbuf.
			//This is bit special, as the amt is for the concatenation.
			let amt = socket_write_err!(fd.writev(&[chdr, buf]));
			if amt < chdr.len() { self.send_buf.extend_from_slice(&chdr[amt..]); }
			self.send_buf.extend_from_slice(&buf[amt-chdr.len()..]);
		}
		Ok(())
	}
	//Flush any output to specified fd. This output must include any headers and file contents. End of
	//transmission must be indicated by raising receiving flag.
	fn flush_data_out(&mut self, fd: &FileDescriptor) -> Result<(), Cow<'static, str>>
	{
		//The file is not supposed to be open if body_left = Some(0).
		if self.body_left == Some(0) { self.body = None; }
		//Flush send buffer, if possible. If not, try flushing data from file. If no file open but there
		//is remainder, send zeroes.
		if self.__try_transmit_sendbuf(fd)? {
			//Flushed data from send buffer. This consitutes the one write. Need buffer check,
			//which is done below.
		} else if let Some(bfd) = self.body.as_ref().map(|fd|fd.as_fdb()) {
			//There is no data in send buffer, but file is still open. Try sending file contents.
			self.__try_transmit_body(fd, bfd)?;
		} else if let Some(body_left) = self.body_left.as_mut() { if *body_left > 0 {
			//File is not open, but body_left is still nonzero. Flush out zeroes to complete the
			//response.
			Self::__flush_zeroes(body_left, fd)?;
		}}
		//If sendbuf is empty, there is no body fd open and body_left is 0 or None, switch to receiving.
		//Note that __try_transmit_body() can alter these conditions.
		if !self.__data_in_sendbuf() && self.body.is_none() && self.body_left.unwrap_or(0) == 0 {
			//If connection: close is set, close the connection.
			fail_if!(self.connection_close, "Close requested by connection:close");
			self.receiving = true;
		}
		Ok(())
	}
	fn __process_request_with_path(&mut self, mut fullpath: FilenameBuf, is_head: bool) ->
		Result<(), RequestError>
	{
		let mut tmp = [MaybeUninit::uninit();128];
		let mut st = complete_file_path(&mut fullpath)?;
		let mtime = st.mtime();
		let (content_encoding, typech) = negotiate_content_encoding(&mut fullpath, &self.accept_encoding,
			mtime, &mut st);
		let mut rawsize = st.size();
		let etag = write_etag(typech, st);
		let mut range: Option<Range<u64>> = None;
		//Content-Type.
		let mut content_type = "application/octet-stream";
		let extension = fullpath.extension_unicode().unwrap_or("");
		//extension_unicode can yield bogus results with / in them. MIMETYPES should not have / in it,
		//nor entry with empty extension.
		if let Ok(p) = MIMETYPES.binary_search_by(|(x,_)|x.cmp(&extension)) {
			if let Some(&(_,t)) = MIMETYPES.get(p) { content_type = t; }
		}
		let content_type = more_specific_content_type(&mut fullpath, content_type);
		//If not HEAD, need to open the file.
		if !is_head {
			st = open_file(&fullpath, &mut self.body, &mut rawsize)?;
			let mut err = Ok(());
			if let Err(e) = parse_range_header(&mut range, &self.range, rawsize) {
				err = Err(e);
			}
			err?;
			//If resulting range is trivial, remove it.
			if range == Some(0..rawsize.wrapping_sub(1)) { range = None; }
		} else {
			self.body_left = Some(0);	//No body.
		}
		let mut unmodified1 = false;
		let mut unmodified2 = false;
		let mut ifnonematch = false;
		//If-Range, if-modified-since and if-none-match
		for value in self.if_range.split(',') {
			let value = value.trim();
			if value.starts_with("\"") {
				//Etag. If no match, clear range.
				if value.as_bytes() != &etag[..] { range = None; }
			} else {
				//Timestamp. If mtime is bigger, clear range.
				if st.mtime().0 > parse_imfdate(value.as_bytes()) { range = None; }
			}
		}
		if let Some(if_modified_since) = self.if_modified_since {
			if st.mtime().0 <= if_modified_since { unmodified1 = true; };
		}
		for item in self.if_none_match.split(',') {
			let item = trim_ascii(item);
			if item.len() > 0 { ifnonematch = true; }
			if item.as_bytes() == &etag[..] { unmodified2 = true; }
		}
		let unmodified = unmodified2 || (!ifnonematch && unmodified1);
		let content_length = if let Some(range) = range.as_ref() {
			range.end.wrapping_sub(range.start).wrapping_add(1)
		} else {
			rawsize
		};
		//Format headers.
		if unmodified {
			self.send_buf.extend_from_slice(b"HTTP/1.1 304 Not modified\r\n");
			self.send_buf.extend_from_slice(printbuffer!(tmp, "content-length:{content_length}\r\n"));
			self.body = None;		//Close file.
			self.body_left = Some(0);	//No body.
			return Ok(());
		} else if let Some(range) = range.as_ref() {
			self.send_buf.extend_from_slice(b"HTTP/1.1 206 Partial content\r\n");
			self.send_buf.extend_from_slice(printbuffer!(tmp, "content-range:bytes \
				{start}-{end}/{rawsize}\r\n", start=range.start, end=range.end));
		} else {
			self.send_buf.extend_from_slice(b"HTTP/1.1 200 OK\r\n");
		}
		self.send_buf.extend_from_slice(printbuffer!(tmp, "content-length:{content_length}\r\n"));
		self.send_buf.extend_from_slice(printbuffer!(tmp, "server:{pn}\r\n", pn=PRODUCT_NAME));
		self.send_buf.extend_from_slice(b"etag:");
		self.send_buf.extend_from_slice(&etag);
		self.send_buf.extend_from_slice(b"\r\ncontent-type:");
		self.send_buf.extend_from_slice(content_type.as_bytes());
		self.send_buf.extend_from_slice(b"\r\n");
		if self.connection_close {
			self.send_buf.extend_from_slice(b"connection:close\r\n");
		}
		if content_encoding.len() > 0 {
			self.send_buf.extend_from_slice(b"content-encoding:");
			self.send_buf.extend_from_slice(content_encoding);
			self.send_buf.extend_from_slice(b"\r\n");
		}
		add_date_header(&mut self.send_buf);
		self.send_buf.extend_from_slice(b"\r\n");	//End headers.
		//Jump to starting point, and set body length.
		self.body_left = Some(content_length);
		if let (Some(range), Some(body)) = (range.as_ref(), self.body.as_ref()) {
			if let Err(err) = body.seek(SeekTarget::Set(range.start)) {
				syslog!(ERROR "Unable to seek file {fullpath}: {err}");
				fail!(RequestError::InternalServerError("Unable to seek file"));
			}
		}
		Ok(())
	}
	//Process request. Headers will be written to send_buf and body will be opened for body. In case of error,
	//return Err().
	fn __process_request(&mut self) -> Result<(), RequestError>
	{
		//The only allowed methods are HEAD and GET.
		fail_if!(self.method == Method::Other, RequestError::UnsupportedMethod);
		//If OPTIONS, just throw some dummy response.
		if self.method == Method::Options {
			let mut tmp = [MaybeUninit::uninit();512];
			self.send_buf.extend_from_slice(printbuffer!(tmp,
				"HTTP/1.1 200 OK\r\nserver:{pn}\r\ncontent-length:0\r\n\r\n", pn=PRODUCT_NAME));
			self.body_left = Some(0);	//No body.
			return Ok(());
		}
		let is_head = self.method == Method::HEAD;
		//Note: Authority is already lowercased. Join path.
		//If path does not start with /, return not found as the concat result can be bogus.
		let mut path = self.configuration.root.clone();
		fail_if!(!self.path.starts_with("/"), RequestError::FileNotFound);
		path.push("/");	//Slash between root and authority.
		path.push(&self.authority);	//Can not contain a slash.
		path.push(&self.path);
		self.__process_request_with_path(path, is_head)
	}
	//Proess request. Headers will be written to send_buf and body will be opened for body. In case error
	//occurs, receiving flag must still be lowered, and send_buf must be filled with error headers and body.
	fn process_request(&mut self)
	{
		if let Err(error) = self.__process_request() {
			self.body = None;	//In case it got opened.
			use self::RequestError::*;
			match error {
				BadRange(len, msg) => format_error(&mut self.send_buf, 416, msg, len),
				FileNotFound => format_error(&mut self.send_buf, 404, "File not found", 0),
				Forbidden => format_error(&mut self.send_buf, 403, "Forbidden", 0),
				InternalServerError(msg) => format_error(&mut self.send_buf, 500, msg, 0),
				UnsupportedMethod => format_error(&mut self.send_buf, 405, "Unsupported method", 0),
			}
		}
	}
}

struct ContentEncoding
{
	extension: &'static str,
	encoding: &'static [u8],
	mletter: u8,			//u is reserved.
}

//This is encoded from least preferred to most preferred. It can have maximum of 64 entries.
static CONTENT_ENCODINGS: [ContentEncoding;3] = [
	ContentEncoding{extension:".gz", encoding:b"gzip", mletter:b'g'},
	ContentEncoding{extension:".zst", encoding:b"zstd", mletter:b'z'},
	ContentEncoding{extension:".br", encoding:b"br", mletter:b'b'},
];

fn negotiate_content_encoding(fullpath: &mut FilenameBuf, accept_encoding: &str, mtime: (i64, u32),
	st: &mut Stat) -> (&'static [u8], u8)
{
	let mut formatc = b'u';
	let mut format: &'static [u8] = b"";
	let mut ext = "";
	//Content-Encoding support.
	let mut sbits = 0u64;	//64 is enough, right?
	for item in accept_encoding.split(',') {
		let item = trim_ascii(item.split(';').next().unwrap_or(""));
		for (slevel, edata) in CONTENT_ENCODINGS.iter().enumerate() {
			if equals_case_insensitive(item.as_bytes(), edata.encoding) { sbits |= 1 << slevel; }
		}
	}
	for (slevel, edata) in CONTENT_ENCODINGS.iter().enumerate() {
		if sbits & 1 << slevel == 0 { continue; }
		let olen = fullpath.len();
		fullpath.push(edata.extension);
		if let Some(Ok(st2)) = fullpath.into_unix_path().map(|p|p.stat()) {
			if st2.mtime() >= mtime {
				format = edata.encoding;
				formatc = edata.mletter;
				ext = edata.extension;
				*st = st2;
			}
		}
		fullpath.truncate(olen);
	}
	fullpath.push(ext);
	(format, formatc)
}

fn complete_file_path(fullpath: &mut FilenameBuf) -> Result<Stat, RequestError>
{
	//Just check if this is directory.
	if let Some(Ok(x)) = fullpath.into_unix_path().map(|p|p.stat()) {
		if x.format() == StatModeFormat::DIR { fullpath.push("/index.html"); }
	}
	let st = match fullpath.into_unix_path().map(|p|p.stat()) {
		Some(Ok(st)) if st.format() == StatModeFormat::REG => Ok(st),
		Some(Ok(_)) => Err(syslog!(NOTICE "File is irregular {fullpath}")),
		Some(Err(err)) if err == OsError::ENOENT =>
			Err(syslog!(NOTICE "File does not exist {fullpath}")),
		Some(Err(err)) => Err(syslog!(ERROR "Failed to stat {fullpath}: {err}")),
		None => Err(syslog!(ERROR "Failed to stat {fullpath}: File name contains NUL")),
	}.set_err(RequestError::FileNotFound)?;
	Ok(st)
}

macro_rules! write_byte
{
	($buf:expr, $itr:expr, $c:expr) => {{ $buf.get_mut($itr).map(|t|*t=$c); $itr = $itr + 1; }}
}

fn write_num(buffer: &mut [u8], itr: &mut usize, mut n: u64)
{
	for _ in 0..10 {
		static SYMBOLS: &'static [u8;85] = b"#$&()*+,-.0123456789:<=>?ABCDEFGHIJKLMNOPQRSTUVWXYZ[]^_\
			abcdefghijklmnopqrstuvwxyz{|}~";
		let x = (n % SYMBOLS.len() as u64) as usize;
		write_byte!(buffer, *itr, SYMBOLS[x]);
		n /= SYMBOLS.len() as u64;
	}
}

fn write_etag(vcode: u8, st: Stat) -> [u8;53]
{
	let mut buffer = [0;53];
	let mut itr = 0;
	write_byte!(buffer, itr, b'\"');
	write_byte!(buffer, itr, vcode);
	write_num(&mut buffer, &mut itr, st.device());
	write_num(&mut buffer, &mut itr, st.inode());
	write_num(&mut buffer, &mut itr, st.mtime().0 as u64);
	write_num(&mut buffer, &mut itr, st.mtime().1 as u64);
	write_num(&mut buffer, &mut itr, st.size());
	write_byte!(buffer, itr, b'\"');
	assert_eq!(itr, 53);
	buffer
}

fn open_file(fullpath: &Filename, handle: &mut Option<UnixFileDescriptor>, size: &mut u64) ->
	Result<Stat, RequestError>
{
	let path = fullpath.into_unix_path().ok_or_else(||{
		syslog!(ERROR "Failed to open {fullpath}: Illegal path");
		RequestError::FileNotFound
	})?;
	let fd = path.open(OpenFlags::O_RDONLY|OpenFlags::O_CLOEXEC).map_err(|err|{
		syslog!(ERROR "Failed to open {fullpath}: {err}");
		if err == OsError::ENOENT {
			RequestError::FileNotFound
		} else if err == OsError::EACCES || err == OsError::EPERM {
			RequestError::Forbidden
		} else {
			RequestError::InternalServerError("I/O error trying to open file")
		}
	})?;
	//Update size if possible.
	match fd.stat() {
		Ok(st) => {
			*size = st.size();
			*handle = Some(fd);
			Ok(st)
		},
		Err(err) => {
			syslog!(ERROR "Failed to fstat {fullpath}: {err}");
			Err(RequestError::InternalServerError("fstat on opened file failed"))
		}
	}
}

fn readvalue(x: &str) -> Result<Option<u64>, ()>
{
	Ok(if x != "" { Some(dtry!(u64::from_str(x))) } else { None })
}
fn parse_range_header(range: &mut Option<Range<u64>>, hdrval: &str, fsize: u64) ->  Result<(), RequestError>
{
	static ILLSYNTAX: &'static str = "Illegal range syntax";
	static RANGE_OOB: &'static str = "Range goes out of bounds";
	static RANGE_BW: &'static str = "Range bounds are backwards";
	use self::RequestError::BadRange;
	//Split at =.
	let mut itr = hdrval.splitn(2, '=');
	if itr.next() != Some("bytes") { return Ok(()); }	//Unsupported unit.
	let hdrval = itr.next().ok_or(BadRange(fsize, ILLSYNTAX))?;
	for r in hdrval.split(',') {
		//Split at -.
		let mut itr = r.splitn(2, '-');
		let rstart = itr.next().ok_or(BadRange(fsize, ILLSYNTAX))?;
		let rend = itr.next().ok_or(BadRange(fsize, ILLSYNTAX))?;
		let rstart = readvalue(rstart).set_err(BadRange(fsize, ILLSYNTAX))?;
		let rend = readvalue(rend).set_err(BadRange(fsize, ILLSYNTAX))?;
		let (rstart, rend) = match (rstart, rend) {
			(Some(rstart), Some(rend)) => {
				fail_if!(rend >= fsize, BadRange(fsize, RANGE_OOB));
				fail_if!(rstart > rend, BadRange(fsize, RANGE_BW));
				(rstart, rend)
			},
			(Some(rstart), None) => {
				fail_if!(rstart >= fsize, BadRange(fsize, RANGE_OOB));
				(rstart, fsize - 1)	//fsize=0 unavoidably trips the above check.
			},
			(None, Some(rend)) => {
				fail_if!(rend > fsize || rend == 0, BadRange(fsize, RANGE_OOB));
				(fsize - rend, fsize - 1)	//fsize=0 unavoidably trips the above check.
			},
			(None, None) => fail!(BadRange(fsize, ILLSYNTAX))
		};
		*range = Some(if let Some(range) = range.clone() {
			min(range.start, rstart)..max(range.end, rend)
		} else {
			rstart..rend
		});
	}
	Ok(())
}

fn _check_mimetype<'a>(x: &str) -> Result<(), ()>
{
	let mut state = 0;
	for c in x.chars() {
		let a = match c as u32 { x@0..=127 => x as u8, _ => 128 };
		state = match state {
			//States 3, 8 and 11 may end the string, so accept CR and LF.
			3|8|11 if a == 13 || a == 10 => break,
			//For all states, restrict a to 9 (tab), 32-126 (ASCII printables) and 128 (high-plane).
			_ if a != 9 && (a < 32 || a > 126) && a != 128 => fail!(()),
			//State 0 is start of string. The next character must be tchar. If so, transition to state 1.
			0 if is_tchar(a) => 1,
			//State 1 is main type continuing. Next character must be either tchar (eat it) or solidus
			//(transition to state 2).
			1 if is_tchar(a) => 1,
			1 if c == '/' => 2,
			//State 2 is separator between types. The next character must be tchar. If so, transition to
			//state 3.
			2 if is_tchar(a) => 3,
			//State 3 is main type continuing. Next character must be either tchar (eat it) or space
			//or tab (transition to state 4) or ';' (transition to state 5). The string may end here.
			3 if is_tchar(a) => 3,
			3 if c == ' ' || c == '\t' => 4,
			3 if c == ';' => 5,
			//State 4 is eating whitespace before ';'. Next character must be either space or tab (eat
			//it) or ';' (transition to state 5).
			4 if c == ' ' || c == '\t' => 4,
			4 if c == ';' => 5,
			//State 5 is ; eating whitespace after ';' and empty parameter name. Next character must
			//be either tchar (transition to state 6), space or tab (eat it).
			5 if is_tchar(a) => 6,
			5 if c == ' ' || c == '\t' => 5,
			//State 6 is non-empty parameter name. Next character must be either tchar (eat it) or
			//'=' (transition to state 7).
			6 if is_tchar(a) => 6,
			6 if c == '=' => 7,
			//State 7 is on parameter '='. Next character mus either be tchar (transtion to state 8) or
			//'"' (transition to state 9).
			7 if is_tchar(a) => 8,
			7 if c == '\"' => 9,
			//State 8 is non-empty token parameter value. Next character must be either tchar (eat it)
			//space or tab (transition to state 4). The string may end here.
			8 if is_tchar(a) => 8,
			8 if c == ' ' || c == '\t' => 4,
			//State 9 is innards of quoted string value. Next character must either be '\' (transition
			//to state 10), '"' (transition to state 11), or other character (eat it).
			9 if c == '\\' => 10,
			9 if c == '\"' => 11,
			9  => 9,
			//State 10 is escape in quoted string. It must any character (transition to state
			//9)
			10 => 9,
			//State 11 is end of quoted string. This behaves like state 4, except it accepts, so string
			//may end here.
			11 if c == ' ' || c == '\t' => 4,
			11 if c == ';' => 5,
			//All others are errors.
			_ => fail!(())
		};
	}
	//Now, the string can end in states 3, 8 and 11.
	if state == 3 || state == 8 || state == 11 { Ok(()) } else { Err(()) }
}

pub fn check_mimetype<'a>(x: &'a str) -> Option<&'a str>
{
	match _check_mimetype(x) {
		//Truncate at first CR or LF.
		Ok(_) => Some(&x[..x.find(|x|x=='\r'||x=='\n').unwrap_or(x.len())]),
		Err(_) => None,
	}
}

fn more_specific_content_type(fullpath: &mut FilenameBuf, content_type: &'static str) -> Cow<'static, str>
{
	let default = Cow::Borrowed(content_type);
	let olen = fullpath.len();
	//Note, this must be restored before returning.
	fullpath.push(".mimeoverride");
	let fp = {
		//Ignore illegal path (use ENOENT, which gets squashed anyway)
		let p = fullpath.into_unix_path().ok_or(OsError::ENOENT);
		match p.and_then(|p|p.open(OpenFlags::O_RDONLY|OpenFlags::O_CLOEXEC)) {
			Ok(x) => Some(x),
			Err(err) => {
				//Ignore ENOENT.
				if err != OsError::ENOENT {
					syslog!(WARNING "Failed to open {fullpath}.mimeoverride: {err}");
				}
				None
			}
		}
	};
	fullpath.truncate(olen);
	//If we did not open any file, we still need to truncate (above) and return default.
	let fp = f_return!(fp, default);
	let mut buf = [MaybeUninit::uninit();512];
	match fp.read_uninit2(&mut buf) {
		Ok(buf) => match from_utf8(buf).ok().and_then(|x|check_mimetype(x)) {
			Some(x) => return Cow::Owned(x.to_owned()),
			None => syslog!(WARNING "Not valid mimetype in {fullpath}.mimeoverride"),
		},
		Err(err) => syslog!(WARNING "Failed to read {fullpath}.mimeoverride: {err}")
	}
	default
}

enum RequestError
{
	BadRange(u64, &'static str),
	FileNotFound,
	Forbidden,
	InternalServerError(&'static str),
	UnsupportedMethod,
}


macro_rules! rd2d
{
	($buf:expr, $itr:expr) => {
		(*$buf.get($itr).unwrap_or(&0)&15).wrapping_mul(10).wrapping_add(*$buf.get($itr+1).unwrap_or(&0)&15)
	}
}

fn parse_imfdate(date: &[u8]) -> i64
{
	let day = rd2d!(date, 5);
	let month = match rd2d!(date, 9) {
		24 => 1, 52 => 2, 12 => 3, 2 => 4, 19 => 5, 64 => 6, 62 => 7, 57 => 8, 50 => 9, 34 => 10,
		156 => 11, 53 => 12, _ => 1
	};
	let century = rd2d!(date, 12);
	let year = rd2d!(date, 14);
	let hour = rd2d!(date, 17);
	let minute = rd2d!(date, 20);
	let second = rd2d!(date, 23);
	let year = (century as i64) * 100 + (year as i64);
	let year = year - 10000 * ((year - 1) >> 63);
//println!("{year}-{month}-{day} {hour}-{minute}-{second}");
	let dnum = number_of_day(year, month, 1).unwrap_or(0);
	dnum * 86400 + day as i64 * 86400 + hour as i64 * 3600 + minute as i64 * 60 + second as i64 - 86400
}

impl ElementSink for ConnectionSink
{
	fn sink(&mut self, elem: Element)
	{
		match elem {
			Element::StartOfMessage => {
				self.authority.clear();
				self.path.clear();
				self.range.clear();
				self.accept_encoding.clear();
				self.if_range.clear();
				self.if_modified_since = None;
				self.if_none_match.clear();
			},
			Element::Method(method) => self.method = if method == b"GET" {
				Method::GET 
			} else if method == b"HEAD" {
				Method::HEAD
			} else if method == b"OPTIONS" {
				Method::Options
			} else {
				Method::Other
			},
			//Reuse the buffer.
			Element::Authority(authority) => if let Ok(authority) = from_utf8(authority) {
				self.authority.push_str(authority);
			} else {
				self.error = bad_utf8();
			},
			Element::Path(path) => {
				if check_directory_traversal(path) {
					return self.error = Some(format!("Directory traversal attempt!"));
				}
				//Reuse the buffer.
				if let Ok(path) = from_utf8(path) {
					self.path.push_str(path);
				} else {
					self.error = bad_utf8();
				}
			},
			//No need to handle any nonstandard headers because no interesting header is nonstandard.
			Element::ConnectionHeaderStd(StandardHttpHeader::Close) => self.connection_close = true,
			Element::HeaderStd(name, value) => {
				//Only consider limited number of headers.
				let hdr = match name {
					StandardHttpHeader::Range => &mut self.range,
					StandardHttpHeader::AcceptEncoding => &mut self.accept_encoding,
					StandardHttpHeader::IfRange => &mut self.if_range,
					//Handle i-m-f specially.
					StandardHttpHeader::IfModifiedSince =>
						return self.if_modified_since = Some(parse_imfdate(value)),
					StandardHttpHeader::IfNoneMatch => &mut self.if_none_match,
					_ => return	//Ignore any others.
				};
				if hdr.len() > 0 { hdr.push(','); }
				if let Ok(value) = from_utf8(value) {
					hdr.push_str(value);
				} else {
					self.error = bad_utf8();
				}
			},
			Element::EndOfMessage => {
				self.receiving = false;		//In reply.
				self.process_request()
			},
			Element::HeadersFaulty(err) => self.error = Some(err.to_string()),
			Element::Abort(err) => self.error = Some(err.to_string()),
			//No other event is of any interest.
			_ => (),
		}
	}
	fn protocol_supported(&self, _: &[u8]) -> bool { false }	//No upgrades supported.
}

struct Connection
{
	recv: Stream,
	sink: ConnectionSink,
	token: AllocatedToken,
	fd: FdPoller,
	error: Option<Cow<'static, str>>,
	timeout_close: PointInTime,
	rshutdown: bool,
	http_error: String,
	error_buffer: Vec<u8>,
	error_buffer_iter: usize,
}

impl Connection
{
	fn __return_hard_error_or_ok(&self) -> Result<(), Cow<'static, str>>
	{
		match &self.error {
			&Some(ref x) => Err(x.clone()),
			&None => Ok(()),
		}
	}
	fn __reset_timer(&mut self)
	{
		self.timeout_close = PointInTime::from_now(TimeUnit::Seconds(TIMEOUT_DELAY));
	}
	fn rearm_poller(&mut self, poll: &mut Poll) -> Result<(), Cow<'static, str>>
	{
		let wflag = if !self.sink.receiving || self.error_buffer.len() > 0 {
			IO_WAIT_WRITE
		} else {
			IO_WAIT_READ
		};
		self.fd.update(poll, wflag).map_err(|err|format!("Unable to rearm poller: {err}"))?;
		Ok(())
	}
	fn __format_http_error(err: impl Display) -> (String, Vec<u8>)
	{
		let err = err.to_string();
		let http_error = err.clone();
		let http = format!("HTTP/1.1 400 Bad Request\r\n\
			connection:close\r\n\
			content-type:text/plain;charset=utf-8\r\n\
			content-length: {elen}\r\n\
			\r\n\
			{err}", elen=err.len());
		(http_error, http.into_bytes())
	}
	fn handle_read_event(&mut self, poll: &mut Poll) -> Result<(), Cow<'static, str>>
	{
		let mut bump_timeout = false;
		{
			let mut source = FdReader {
				socket: self.fd.inner(),
				bump_timeout: &mut bump_timeout,
				shutdown: &mut self.rshutdown,
			};
			match self.recv.push(&mut source, &mut self.sink) {
				Ok(true) => (),
				Ok(false) => fail!("EOF from peer"),
				//If read fails, just immediately tear down the connection.
				Err(IoOrStreamError::Io(err)) => fail!(format!("Read Error: {err}")),
				Err(IoOrStreamError::Stream(err)) => self.sink.error = Some(err.to_string()),
			}
			//Even if this nominally succeded, error may be set.
			if let Some(e) = self.sink.error.as_ref() {
				//Try to send bad request back and kill the conection.
				let (errs, errl) = Self::__format_http_error(e);
				self.http_error = errs;
				self.error_buffer = errl;
				//Need to wait for write before sending.
				self.fd.update(poll, IO_WAIT_WRITE).
					map_err(|err|format!("Unable to rearm poller: {err}"))?;
				return Ok(());
			}
		}
		if bump_timeout { self.__reset_timer(); }
		self.rearm_poller(poll)?;
		Ok(())
	}
	fn handle_write_event(&mut self, poll: &mut Poll) -> Result<(), Cow<'static, str>>
	{
		//If error buffer is set, send from that.
		if self.error_buffer_iter < self.error_buffer.len() {
			//Flush part of error buffer to wire.
			match self.fd.borrow().write_const(&self.error_buffer[self.error_buffer_iter..]) {
				Ok(amt) => self.error_buffer_iter += amt,
				Err(ref err) if is_fatal_error(err) => fail!(format!("Write error: {err}")),
				Err(_) => (),	//Ignore transient errors.
			}
			if self.error_buffer_iter >= self.error_buffer.len() {
				//End the connection, as entiere error has been sent.
				return Err(Cow::Owned(self.http_error.clone()));
			}
			self.rearm_poller(poll)?;
			return Ok(());
		}
		self.sink.flush_data_out(self.fd.borrow())?;
		self.rearm_poller(poll)?;
		Ok(())
	}
}

impl UpperLayerConnection for Connection
{
	fn handle_fault(&mut self, _: &mut Poll) -> Option<Cow<'static, str>>
	{
		self.__return_hard_error_or_ok().err()
	}
	//Remove the pollers. Must not panic, including with poisoned locks.
	fn remove_pollers(&mut self, poll: &mut Poll) { self.fd.remove(poll); }
	fn get_main_fd(&self) -> i32 { self.fd.borrow().as_raw_fd() }
	//Handle a timed event (return Err(()) if connection should be killed). Might panic.
	fn handle_timed_event(&mut self, now: PointInTime, _: &mut Poll) -> Result<(), Cow<'static, str>>
	{
		if now >= self.timeout_close { return Err(cow!("Connection timed out")); }
		Ok(())
	}
	//Get next timed event. Called early and after calling handle_timed_event() or handle_event().
	fn get_next_timed_event(&self, _: PointInTime) -> Option<PointInTime>
	{
		Some(self.timeout_close)
	}
	//Handle a socket event (return Err(()) if connection should be killed). Might panic.
	fn handle_event(&mut self, poll: &mut Poll, tok: usize, kind: u8, _: ConnId,
		_: &mut AddTokenCallback) -> Result<(), Cow<'static, str>>
	{
		if tok == self.token.value() && kind & IO_WAIT_READ != 0 {
			self.handle_read_event(poll)?;
		}
		if tok == self.token.value() && kind & IO_WAIT_WRITE != 0 {
			self.handle_write_event(poll)?;
		}
		self.__return_hard_error_or_ok()
	}
}
