use std::env::var;
use std::fs::File;
use std::io::Write;
use std::path::Path;

fn main()
{
	const VERSION_OVERRIDE: &'static str = "version.override";
	use ::std::io::Read as IR;
	let mut override_version = String::new();
	::std::fs::File::open(VERSION_OVERRIDE).and_then(|mut fp|fp.read_to_string(&mut override_version)).ok();

	let out_dir = var("OUT_DIR").unwrap();
	let product = var("CARGO_PKG_NAME").unwrap();
	let version = var("CARGO_PKG_VERSION").unwrap();
	let version = if override_version.len() > 0 { override_version } else { version };
	let dest_path = Path::new(&out_dir).join("product.inc");
	let mut f = File::create(&dest_path).unwrap();
	writeln!(f, "static PRODUCT_NAME: &'static str = \"{product}\";").unwrap();
	writeln!(f, "static PRODUCT_VERSION: &'static str = \"{vers}\";", vers=version.trim()).unwrap();

	//Only emit version.override as dependency if it exists, since otherwise cargo will pointlessly rebuild
	//the crate every time if the file is not found. On the other hand, if version override exists, it needs
	//to be listed as dependency as otherwise version number will be stuck on rebuilds.
	if Path::new(VERSION_OVERRIDE).exists() { println!("cargo:rerun-if-changed={VERSION_OVERRIDE}"); }
	//Emit dummy dependency to signal explicit list.
	println!("cargo:rerun-if-changed=build.rs");
}
