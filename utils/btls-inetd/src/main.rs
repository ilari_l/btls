#![warn(unsafe_op_in_unsafe_fn)]
#[macro_use] extern crate btls_aux_fail;
use btls::ServerConfiguration;
use btls::callbacks::TimeUnit;
use btls::certificates::CertificateDirectory;
use btls::features::FLAGS0_ENABLE_TLS12;
use btls::logging::Logging;
use btls::logging::MessageSeverity;
use btls::utility::Mutex;
use btls_aux_inotify::CLOSED_WRITABLE;
use btls_aux_inotify::CREATED;
use btls_aux_inotify::DELETED;
use btls_aux_inotify::InotifyEventSink;
use btls_aux_inotify::InotifyTrait;
use btls_aux_inotify::InotifyWatcher;
use btls_aux_inotify::MOVED_FROM;
use btls_aux_inotify::MOVED_TO;
use btls_aux_unix::RawFdT;
use btls_aux_keypair_local::LocalKeyPair;
use btls_daemon_helper_lib::abort;
use btls_daemon_helper_lib::cloned;
use btls_daemon_helper_lib::cow;
use btls_daemon_helper_lib::ensure_stdfd_open;
use btls_daemon_helper_lib::FileDescriptor;
use btls_daemon_helper_lib::get_max_files;
use btls_daemon_helper_lib::incompatible_with_certbot;
use btls_daemon_helper_lib::incompatible_with_panic_abort;
use btls_daemon_helper_lib::initialize_from_systemd;
use btls_daemon_helper_lib::IpSet;
use btls_daemon_helper_lib::instant_to_diff;
use btls_daemon_helper_lib::Listener;
use btls_daemon_helper_lib::ListenerLoop;
use btls_daemon_helper_lib::ListenerLoopEvents;
use btls_daemon_helper_lib::LocalMetrics;
use btls_daemon_helper_lib::LocalThreadHandle;
use btls_daemon_helper_lib::MetricsCollector;
use btls_daemon_helper_lib::parse_netmask;
use btls_daemon_helper_lib::ProxyConfiguration;
use btls_daemon_helper_lib::ProxyConfigurationEntry;
use btls_daemon_helper_lib::run_metrics_thread;
use btls_daemon_helper_lib::running_as_root;
use btls_daemon_helper_lib::SighupPipe;
use btls_daemon_helper_lib::SocketAddrEx;
use btls_daemon_helper_lib::SocketAddrExHandle;
use btls_daemon_helper_lib::start_threads;
use btls_daemon_helper_lib::Thread;
use btls_daemon_helper_lib::ThreadNewParams;
use btls_daemon_helper_lib::ThreadSpawnedParams;
use btls_daemon_helper_lib::translate_multiple_address;
use btls_daemon_helper_lib::UpperLayerConnectionFactory;
use btls_inetd::AcmeServiceParams;
use btls_inetd::ConfigT;
use btls_inetd::ConnectionFactory2;
use btls_inetd::DebugLogger;
use btls_inetd::dlog_set_directory;
use btls_inetd::ExtendedPort;
use btls_inetd::GlobalConfiguration;
use btls_inetd::GlobalConfigurationWrapper;
use btls_inetd::GlobalMetrics as GlobalMetrics2;
use btls_inetd::HostkeyLookup;
use btls_inetd::ParseOldHost;
use btls_inetd::ParseNewHost;
use btls_inetd::ParsingNameProperties;
use btls_inetd::ParsingServiceProperties;
use btls_inetd::ParsingServiceTarget;
use btls_inetd::ServiceCacheName as ServiceCacheName2;
use btls_inetd::ServiceName as ServiceName2;
use btls_inetd::set_cipherstats_file;
use btls_inetd::SFLAG_NO_ANDROID_HACK;
use btls_inetd::SFLAG_NO_LOG;
use btls_inetd::SFLAG_NSA_MODE;
use btls_inetd::SFLAG_SECURE192;
use btls_inetd::SFLAG_SPINAL_TAP;
use btls_inetd::SFLAG_TLS13_ONLY;
use btls_inetd::WritableFrozenIpSet;
use btls_inetd::legacy_service::Service;
use btls_util_logging::syslog;
use std::borrow::Cow;
use std::collections::BTreeMap;
use std::collections::BTreeSet;
use std::collections::HashMap;
use std::env::args_os;
use std::fs::File;
use std::fs::Metadata;
use std::io::ErrorKind as IoErrorKind;
use std::io::Read as IoRead;
use std::mem::replace;
use std::mem::swap;
use std::mem::transmute;
use std::net::IpAddr;
use std::ops::Deref;
use std::os::unix::fs::MetadataExt;
use std::path::Path;
use std::path::PathBuf;
use std::str::from_utf8;
use std::sync::Arc;
use std::sync::atomic::Ordering;
use std::thread::Builder as ThreadBuilder;
use std::thread::sleep;
use std::time::Duration;
use std::time::Instant;


const MAX_FDS_REDUCE: usize = 100;

fn parse_duration(optname: &str, x: &str) -> Duration
{
	btls_daemon_helper_lib::parse_duration(x).unwrap_or_else(|err|{
		abort!("{optname}: {err}")
	})
}

fn trans_any_target(tgt: &str) -> Result<SocketAddrEx, Cow<'static, str>>
{
	translate_multiple_address(tgt, None, None).map_err(|err|{
		err.to_string()
	})?.get(0).map(cloned).ok_or_else(||{
		Cow::Borrowed("No addresses")
	})
}

struct Config
{
	metrics_path: String,				//Report metrics to.
	cdpath: String,					//Certificate directory path.
	blpath: Option<String>,				//Certificate blacklist path.
	config: String,					//Configuration string.
	services_dir: String,				//Services dir.
	debug: bool,					//Debug mode.
	trans_to: Option<Duration>,			//Transfer timeout.
	proxy_config: ProxyConfiguration,		//Proxy configuration.
	special_acme_target: Option<SocketAddrEx>,	//Special address to redirect ACME requests to.
	acme_control_hostkey: Option<String>,		//Hostkey for ACME control.
	disallow_exec: bool,				//Disallow execution.
	handshake_timeout: Duration,
	flush_delay: TimeUnit,
	shutdown_delay: TimeUnit,
	allow_nct_wildcard: bool,
	dremap_table: HashMap<SocketAddrEx, SocketAddrEx>,
	shosts: Option<PathBuf>,
	dlog_directory: Option<String>,
	cipherstats_file: Option<PathBuf>,
}

impl Config
{
	fn from_cmdline() -> Config
	{
		let mut cdpath = None;
		let mut blpath = None;
		let mut config = String::new();
		let mut services = None;
		let mut debug = false;
		let mut dummy = true;
		let mut trans_to = None;
		let mut special_acme_target = None;
		let mut acme_control_hostkey = None;
		let mut disallow_exec = true;
		let mut dlog_directory = None;
		let mut handshake_timeout = Duration::from_secs(20);
		let mut flush_delay = TimeUnit::Seconds(5);
		let mut shutdown_delay = TimeUnit::Milliseconds(250);
		let mut allow_nct_wildcard = false;
		let mut metrics_path = String::new();
		let mut proxy_config = ProxyConfiguration::new();
		let mut dremap_table = HashMap::new();
		let mut cipherstats_file = None;
		let mut shosts = None;
		for i in args_os() {
			if dummy { dummy = false; continue; }
			let xstr = i.clone().into_string().unwrap_or_else(|err|{
				abort!("Invalid argument: `{err}`", err=err.to_string_lossy())
			});
			//--listen-fd, --threads, --ack and --log-as are handled elsewhere.
			xstr.strip_prefix("--certificates=").map(|x|cdpath = Some(x.to_owned()));
			xstr.strip_prefix("--certificates-blacklist=").map(|x|blpath = Some(x.to_owned()));
			xstr.strip_prefix("--config=").map(|x|config = x.to_owned());
			xstr.strip_prefix("--services=").map(|x|services = Some(x.to_owned()));
			xstr.strip_prefix("--timeout=").map(|x|{
				trans_to = Some(parse_duration("--timeout=<sec>", x));
			});
			xstr.strip_prefix("--handshake-timeout=").map(|x|{
				handshake_timeout = parse_duration("--handshake-timeout=<sec>", x);
			});
			xstr.strip_prefix("--flush-timeout=").map(|x|{
				flush_delay = TimeUnit::Duration(parse_duration("--flush-timeout=<sec>", x));
			});
			xstr.strip_prefix("--shutdown-timeout=").map(|x|{
				shutdown_delay = TimeUnit::Duration(parse_duration("--shutdown-timeout=<sec>", x));
			});
			xstr.strip_prefix("--metrics=").map(|x|{
				metrics_path = x.to_owned();
			});
			xstr.strip_prefix("--secondary-etc-hosts=").map(|x|{
				shosts = Some(PathBuf::from(x.to_owned()));
			});
			xstr.strip_prefix("--assume-nat=").map(|x|{
				let mut itr = x.splitn(2, "~");
				let a = itr.next().and_then(|x|trans_any_target(x).ok());
				let b = itr.next().and_then(|x|trans_any_target(x).ok());
				if let (Some(a), Some(b)) = (a, b) {
					dremap_table.insert(b, a);	//This is reversed.
				} else {
					abort!("Invalid --assume-name= spec")
				}
			});
			xstr.strip_prefix("--allow-proxy=").map(|x|{
				proxy_config.push(ProxyConfigurationEntry::from(x));
			});
			xstr.strip_prefix("--module=").map(|module|{
				LocalKeyPair::load_module_trusted(module).unwrap_or_else(|err|{
					abort!("Failed to load {module}: {err}")
				});
			});
			xstr.strip_prefix("--redirect-acme=").map(|x|{
				special_acme_target = trans_any_target(x).ok().
					or_else(||abort!("Invalid --redirect-acme= target"));
			});
			xstr.strip_prefix("--acme-hostkey=").map(|x|{
				acme_control_hostkey = Some(x.to_owned());
			});
			xstr.strip_prefix("--tls-handshake-log-directory=").map(|x|{
				dlog_directory = Some(x.to_owned());
			});
			xstr.strip_prefix("--cipherstats-file=").map(|x|{
				cipherstats_file = Some(Path::new(x).to_path_buf());
			});

			if xstr == "--allow-exec" { disallow_exec = false; }
			if xstr == "--allow-wildcard-decode" { allow_nct_wildcard = true; }
			if xstr == "--debug" { debug = true; }
			if xstr == "--disable-modules" { LocalKeyPair::disable_module_loading(); }
		}
		let cdpath = cdpath.unwrap_or_else(||abort!("Need --certificates=<path>"));
		let services = services.unwrap_or_else(||abort!("Need --services=<path>"));
		Config {
			metrics_path: metrics_path,
			cdpath: cdpath,
			blpath: blpath,
			config: config,
			services_dir: services,
			debug: debug,
			trans_to: trans_to,
			proxy_config: proxy_config,
			special_acme_target: special_acme_target,
			acme_control_hostkey: acme_control_hostkey,
			disallow_exec: disallow_exec,
			handshake_timeout: handshake_timeout,
			flush_delay: flush_delay,
			shutdown_delay: shutdown_delay,
			allow_nct_wildcard: allow_nct_wildcard,
			dremap_table: dremap_table,
			shosts: shosts,
			dlog_directory: dlog_directory,
			cipherstats_file: cipherstats_file,
		}
	}
}

#[derive(Clone)]
struct TlsWorkerParams
{
	global: ConfigT,
	server_config: ServerConfiguration,
	maxfiles: usize,
	listener_fds: Vec<RawFdT>,
	mpath: String,
}

struct Metrics
{
	g: Arc<GlobalMetrics>,
	l: Arc<Vec<Arc<LocalMetrics>>>,
}

impl MetricsCollector for Metrics
{
	fn fill(&self, r: &mut BTreeMap<u32, usize>)
	{
		self.g.report(r);
		for i in self.l.iter() { i.report(r); }
	}
}

struct TlsWorker
{
	gstate: ListenerLoop
}

impl Thread for TlsWorker
{
	type Local = ();
	type LocalMetrics = LocalMetrics;
	type Parameters = TlsWorkerParams;
	type Parameters2 = ();
	fn new<'a>(p: ThreadNewParams<'a, Self>) -> Self
	{
		let ThreadNewParams{gparameters: gp, lmetrics: lm, sigpipe: sp, tid, irqpipe, ..} = p;
		let gstate = setup_listener_loop(gp.global, gp.server_config, gp.maxfiles,
			sp, &gp.listener_fds, tid, irqpipe, lm.clone());
		TlsWorker {gstate: gstate}
	}
	fn spawned(p: ThreadSpawnedParams<Self>)
	{
		let ThreadSpawnedParams{gparameters: gp, contexts: lm, ..} = p;
		run_metrics_thread(&gp.mpath, Metrics {
			g: Arc::new(GlobalMetrics::new(gp.global.metrics_arc())),
			l: lm,
		});
	}
	fn run(mut self)
	{
		self.gstate.main_event_loop();
	}
}

struct LogKeysMessages;

impl btls_aux_keypair_local::LogSink for LogKeysMessages
{
	fn print(&mut self, prio: u8, msg: &str)
	{
		static SEV: [char;8] = ['g','a','c','e','w','n','i','d'];
		let prio = SEV[prio as usize & 7];
		syslog!(_ prio, "Keys: {msg}");
	}
}

fn main()
{
	ensure_stdfd_open();
	let sinfo = initialize_from_systemd("btls-inetd");
	incompatible_with_panic_abort();
	incompatible_with_certbot();
	if running_as_root() { abort!("Can't run program as root"); }

	let listeners = sinfo.sockets();
	if listeners.len() == 0 { abort!("No ports to listen"); }

	let maxfiles = get_max_files() - MAX_FDS_REDUCE;
	syslog!(NOTICE "Using maximum of {maxfiles} file descriptors");

	let config = Config::from_cmdline();

	//Create substates.
	let store = CertificateDirectory::new_blacklist(&config.cdpath, config.blpath.as_ref(), StderrSink2).
		unwrap_or_else(|err|{
		abort!("Can't load certificate store: {err}")
	});
	let mut server_config = ServerConfiguration::from(store.lookup());
	//Log messages from keys subsystem.
	LocalKeyPair::set_module_log_sink(Box::new(LogKeysMessages));
	//Log the following debug events: Handshake messages, Aborts, Handshake data, Handshake Events, TLS
	//extensions, and raw handshake records.
	server_config.set_debug(315, Box::new(DebugLogger));		//For printing messages if any.
	server_config.set_deadline(Some(config.handshake_timeout));	//20s timeout by default.
	server_config.config_flags(&config.config, |error|syslog!(WARNING "{error}"));
	let services_dir = PathBuf::from(&config.services_dir).canonicalize().unwrap_or_else(|err|{
		abort!("Services directory does not exist: {err}")
	});
	let mut scache = ServiceCacheName::new(&services_dir, config.shosts);
	if !config.disallow_exec { scache.enable_exec(); }

	let cdpath = &config.cdpath;
	let special_acme_config = config.acme_control_hostkey.as_ref().map(|hostkey|{
		let hostkey2 = HostkeyLookup::from_file(hostkey).unwrap_or_else(|err|{
			abort!("Failed to load --acme-hostkey= target: {err}")
		});
		let mut config2 = ServerConfiguration::from(hostkey2.as_lookup());
		//Disable TLS 1.2.
		config2.clear_flags(0, FLAGS0_ENABLE_TLS12);
		AcmeServiceParams {
			config: config2,
			authorized: hostkey2,
			main_config: server_config.clone(),
			certpath: Path::new(cdpath).to_owned(),
		}
	});
	if let Some(dlog_directory) = config.dlog_directory.as_ref() { dlog_set_directory(dlog_directory); }

	let global_config = GlobalConfiguration {
		debug: config.debug,
		flush_delay: config.flush_delay,
		handshake_timeout: TimeUnit::Duration(config.handshake_timeout),
		proxy_config: config.proxy_config.clone(),
		scache: scache,
		shutdown_delay: config.shutdown_delay,
		trans_timeout: config.trans_to,
		dremap_table: config.dremap_table,
		allow_nct_wildcard: config.allow_nct_wildcard,
		special_acme_target: config.special_acme_target,
		special_acme_config: special_acme_config,
		//Stats blacklist is not currently supported.
		stats_blacklist: WritableFrozenIpSet::new(),
	};

	//Global metrics.
	let global_metrics = Arc::new(GlobalMetrics2::new());
	let global_config = Box::new(GlobalConfigurationWrapper::new(global_config, global_metrics.clone()));
	let global_config: ConfigT = unsafe{transmute(Box::into_raw(global_config))};
	if let Some(csfile) = config.cipherstats_file { set_cipherstats_file(&csfile, global_config); }

	//Wait a bit for certificate load to complete.
	sleep(Duration::from_millis(1000));

	start_threads::<TlsWorker>(sinfo, "TLS worker", TlsWorkerParams {
		global: global_config,
		server_config: server_config,
		maxfiles: maxfiles,
		listener_fds: listeners,
		mpath: config.metrics_path,
	}, ());
}

fn setup_listener_loop(global: ConfigT, server_config: ServerConfiguration, maxfiles: usize,
	sigpipe: SighupPipe, listener_fds: &Vec<RawFdT>, tid: Option<u64>, irqpipe: LocalThreadHandle,
	local_metrics: Arc<LocalMetrics>) -> ListenerLoop
{
	//Construct connection factory.
	let factory: Arc<dyn UpperLayerConnectionFactory> = Arc::new(ConnectionFactory2{
		config: server_config,
		global: global,
	});
	let collector = ListenerLoopCollector::new(global.metrics_arc(), local_metrics);
	//Construct listener loop. Each thread should have its own.
	let mut gstate = ListenerLoop::new(maxfiles, sigpipe, irqpipe).unwrap_or_else(|err|{
		abort!("Creating event loop: {err}")
	});
	if let Some(tid) = tid { gstate.set_pid(tid); }
	gstate.set_events(collector);
	//Add the file descriptors.
	for i in listener_fds.iter() {
		let j = Listener::new(FileDescriptor::new(*i), factory.clone());
		gstate.add_listener(j);
	}
	gstate
}

struct StderrSink2;

impl Logging for StderrSink2
{
	fn message(&self, severity: MessageSeverity, msg: Cow<'static, str>)
	{
		let y = severity.code_prefix();
		let y = y.chars().nth(1).unwrap_or('N');
		syslog!(_ y, "{msg}");
	}
}

/*******************************************************************************************************************/
pub const GMETRIC_UPTIME: u32 = 30;
pub const GMETRIC_CIPHER_BASE: u32 = 0x1000;


pub struct ListenerLoopCollector(Arc<GlobalMetrics2>, Arc<LocalMetrics>);

impl ListenerLoopCollector
{
	pub fn new(global_metrics: Arc<GlobalMetrics2>, local_metrics: Arc<LocalMetrics>) ->
		Box<ListenerLoopCollector>
	{
		Box::new(ListenerLoopCollector(global_metrics, local_metrics))
	}
}

impl ListenerLoopEvents for ListenerLoopCollector
{
	fn locals(&self) -> Arc<LocalMetrics> { self.1.clone() }
	fn incoming(&self) { self.0.conn_accept.fetch_add(1, Ordering::Relaxed); }
	fn committed(&self) { self.0.conn_commit.fetch_add(1, Ordering::Relaxed); }
	fn retired(&self) { self.0.conn_retire.fetch_add(1, Ordering::Relaxed); }
}

pub struct GlobalMetrics
{
	basetime: Instant,
	inner: Arc<GlobalMetrics2>,
}

impl GlobalMetrics
{
	pub fn new(inner: Arc<GlobalMetrics2>) -> GlobalMetrics
	{
		GlobalMetrics {
			basetime: Instant::now(),
			inner: inner,
		}
	}
}

macro_rules! define_tls_gmetrics
{
	($([$value:tt $symbol:ident $variable:ident];)*) => {
		impl GlobalMetrics
		{
			pub fn report(&self, r: &mut BTreeMap<u32, usize>)
			{
				let m = &self.inner;
				r.insert(GMETRIC_UPTIME, instant_to_diff(Instant::now(), self.basetime));
				$(r.insert($value, m.$variable.load(Ordering::Relaxed));)*
				for i in 0..16 { for j in 0..16 {
					r.insert(GMETRIC_CIPHER_BASE + (i * 16 + j) as u32,
						m.cipher_flags[i][j].load(Ordering::Relaxed));
				}}
			}
		}
	}
}

//Define the metrics.
include!("metrics.inc");

/*******************************************************************************************************************/


#[test]
fn test_fallbacks_star()
{
	let mut r = Vec::new();
	::btls_daemon_helper_lib::call_with_nth_fallback::<(),_>("*", |y|{r.push(y.to_owned()); None});
	assert_eq!(&r[..], ["*"]);
}

#[test]
fn test_fallbacks_1label()
{
	let mut r = Vec::new();
	::btls_daemon_helper_lib::call_with_nth_fallback::<(),_>("foo", |y|{r.push(y.to_owned()); None});
	assert_eq!(&r[..], ["foo","**.foo","*"]);
}

#[test]
fn test_fallbacks_2label()
{
	let mut r = Vec::new();
	::btls_daemon_helper_lib::call_with_nth_fallback::<(),_>("foo.bar", |y|{r.push(y.to_owned()); None});
	assert_eq!(&r[..], ["foo.bar","**.foo.bar","*.bar","**.bar","*"]);
}

#[test]
fn test_fallbacks_3label()
{
	let mut r = Vec::new();
	::btls_daemon_helper_lib::call_with_nth_fallback::<(),_>("foo.bar.baz", |y|{r.push(y.to_owned()); None});
	assert_eq!(&r[..], ["foo.bar.baz","**.foo.bar.baz","*.bar.baz","**.bar.baz","**.baz","*"]);
}


/*******************************************************************************************************************/
pub struct ServiceCacheName;

impl ServiceCacheName
{
	pub fn new(dir: &Path, shosts: Option<PathBuf>) -> ServiceCacheName2
	{
		let files = ConfigFiles::new(dir, shosts);
		let cache = files.cache.clone();
		let files = Arc::new(Mutex::new(files));
		let files2 = files.clone();
		let sink = Box::new(InotifySink { cache: files.clone() });
		let inotify = InotifyWatcher::new(dir, sink, CLOSED_WRITABLE | MOVED_FROM | MOVED_TO |
			CREATED | DELETED).ok();
		let inotify_enable = if let Some(inotify) = inotify {
			let mut inotify: InotifyWatcher = inotify;
			//Create a thread and pass inotify to it.
			let b = ThreadBuilder::new();
			let b = b.name("SC inotify".to_owned());
			b.spawn(move ||{
				loop {
					inotify.wait(None);
					files2.lock().execute_rebuild();
				}
			}).is_ok()
		} else {
			let b = ThreadBuilder::new();
			let b = b.name("SC inotify".to_owned());
			b.spawn(move ||{
				loop {
					sleep(Duration::from_secs(300));
					files2.lock().full_rescan();
				}
			}).ok();
			false		//Can not be enabled.
		};
		//Do full rescan to discover the files.
		files.lock().full_rescan();
		if inotify_enable { syslog!(NOTICE "Service cache inotify enabled"); }
		cache
	}
}

struct ConfigFile
{
	namedata: ParsingNameProperties,
	maindata: ParsingServiceProperties,
	dfltalpn: HashMap<ExtendedPort, String>,
	alpns: HashMap<String, ParsingServiceProperties>,
	wildcard: bool,
	alpn_map: Vec<(Option<ExtendedPort>, String, String)>,
	force_new: bool,
}

impl ConfigFile
{
	fn check_bad_includes(fname: &str) -> bool
	{
		fname.chars().any(|c|c=='/'||c=='\0') || fname == "." || fname == ".."
	}
	fn parse_config_contents(&mut self, content: &str, include_q: &mut Vec<String>,
		include_s: &mut BTreeSet<String>, shosts: Option<&Path>) ->
		Result<(), Cow<'static, str>>
	{
		let mut ipset = IpSet::new();
		for (linenum, cfgline) in content.lines().enumerate() {
			let cfgline = canonicalize_line(cfgline);
			let cfgline = cfgline.deref();
			if cfgline == "" { continue; }
			if let Some(fname) = cfgline.strip_prefix("include ") {
				if Self::check_bad_includes(fname) {
					fail!(format!("Line {line}: Disallowed include {fname}", line=linenum+1));
				}
				if !include_s.contains(fname) {
					let fname = fname.to_owned();
					include_s.insert(fname.clone());
					include_q.push(fname);
				}
			} else if let Some(cfgline) = cfgline.strip_prefix("alpn ") {
				let (alpn, directive) = deescape_alpn_directive(cfgline)?;
				//This should not ever modify namedata or dfltalpn if alpn=true,
				parse_config_directive(self.alpns.entry(alpn.into_owned()).
					or_insert_with(||ParsingServiceProperties::new()), &mut self.namedata,
					directive, true, shosts, &mut ipset, &mut self.dfltalpn)?;
			} else if let Some(cfgline) = cfgline.strip_prefix("alpn-map ") {
				let (port, alpn, service) = deescape_alpnmap_directive(cfgline)?;
				let port = if port != "*" {
					Some(ExtendedPort::parse(&port).
						ok_or("Invalid port number in alpn-map")?)
				} else {
					None
				};
				self.alpn_map.push((port, alpn.into_owned(), service.into_owned()));
			} else if cfgline.starts_with("new-style-config") {
				self.force_new = true;
			} else {
				parse_config_directive(&mut self.maindata, &mut self.namedata, cfgline, false,
					shosts, &mut ipset, &mut self.dfltalpn).map_err(|err|{
					format!("Line {line}: {err}", line=linenum+1)
				})?;
			}
		}
		//Only set ip filter mask if nontrivial.
		let ipset = ipset.freeze();
		if ipset.nontrivial() { self.namedata.ip_blacklist = Some(Arc::new(ipset)); }
		Ok(())
	}
	fn parse_file(&mut self, filename: &Path, include_q: &mut Vec<String>,
		include_s: &mut BTreeSet<String>, hard_enoent: bool, shosts: Option<&Path>) ->
		Result<bool, Cow<'static, str>>
	{
		let mut content = Vec::new();
		if let Err(err) = File::open(filename).and_then(|mut fp|fp.read_to_end(&mut content)) {
			if err.kind() == IoErrorKind::NotFound && !hard_enoent {
				return Ok(false);	//File not found.
			} else {
				fail!(format!("Failed to read {file}: {err}", file=filename.display()));
			}
		};
		let content = String::from_utf8(content).map_err(|_|{
			format!("Failed to read {file}: File is not UTF-8", file=filename.display())
		})?;
		self.parse_config_contents(&content, include_q, include_s, shosts).map_err(|err|{
			format!("File {file} {err}", file=filename.display())
		})?;
		Ok(true)
	}
	fn parse_all_files(&mut self, root: &Path, incbase: &Path, include_s: &mut BTreeSet<String>,
		shosts: Option<&Path>) -> Result<bool, Cow<'static, str>>
	{
		let mut include_q = Vec::new();
		let ok = self.parse_file(root, &mut include_q, include_s, false, shosts)?;
		if !ok { return Ok(false); }	//Root file not found.
		while let Some(inc) = include_q.pop() {
			let path = incbase.join(inc.deref());
			self.parse_file(&path, &mut include_q, include_s, true, shosts)?;
		}
		Ok(true)
	}
	//On success, returns the service data. On failure returns the error string. Additionally unconditionally
	//returns the files considered. This can be incomplete in case of error, but the set of files will be
	//sufficient to possibly change if error happens on next iteration.
	fn parse_servicename(root: &str, incbase: &Path, wildcard: bool, shosts: Option<&Path>) ->
		(Result<Option<ServiceName2>, Cow<'static, str>>, BTreeSet<String>)
	{
		if Self::check_bad_includes(root) {
			return (Err(cow!(F "Bad service name {root}")), BTreeSet::new());
		}
		let mut x = ConfigFile {
			namedata: ParsingNameProperties::new(),
			maindata: ParsingServiceProperties::new(),
			dfltalpn: HashMap::new(),
			alpns: HashMap::new(),
			wildcard: wildcard,
			alpn_map: Vec::new(),
			force_new: false,
		};
		let mut include_s = BTreeSet::new();
		include_s.insert(root.to_owned());
		let root = incbase.join(root);
		(match x.parse_all_files(&root, incbase, &mut include_s, shosts) {
			Ok(true) => Ok(Some(if x.alpn_map.len() > 0 || x.force_new {
				let mut h = ParseNewHost::new(x.namedata, x.maindata, x.wildcard);
				for (service, data) in x.alpns.drain() { h.add_service(&service, data); }
				for (port, alpn, service) in x.alpn_map.drain(..) { match port {
					Some(port) => h.add_local_alpn(&alpn, &service, port),
					None => h.add_global_alpn(&alpn, &service),
				}}
				h.done()
			} else {
				let mut h = ParseOldHost::new(x.namedata, x.maindata, x.wildcard);
				for (alpn, data) in x.alpns.drain() { h.add_alpn(&alpn, data); }
				h.done()
			})),
			Ok(false) => Ok(None),
			Err(e) => Err(e)
		}, include_s)
	}
}

fn delete_setvalue(map: &mut BTreeMap<String, BTreeSet<String>>, key: &str, value: &str)
{
	let mut now_empty = false;
	if let Some(v) = map.get_mut(key) {
		v.remove(value);
		now_empty = v.is_empty();
	}
	if now_empty { map.remove(key); }
}

fn insert_setvalue(map: &mut BTreeMap<String, BTreeSet<String>>, key: &str, value: &str)
{
	if let Some(v) = map.get_mut(key) {
		if !v.contains(value) { v.insert(value.to_owned()); }
		return;
	}
	//Not in map, create a new entry.
	let mut set = BTreeSet::new();
	set.insert(value.to_owned());
	map.insert(key.to_owned(), set);
}

struct ConfigFiles
{
	//Bidirectional set of all resources.
	resources_to_configs: BTreeMap<String, BTreeSet<String>>,
	configs_to_resources: BTreeMap<String, BTreeSet<String>>,
	//Scratch buffer for configs to rebuild.
	to_rebuild: BTreeSet<String>,
	//Root path.
	root: PathBuf,
	//Secondary /etc/hosts.
	shosts: Option<PathBuf>,
	//Metadata cache.
	metadata: BTreeMap<String, Metadata>,
	//Cache.
	cache: ServiceCacheName2,
}

impl ConfigFiles
{
	fn new(path: &Path, shosts: Option<PathBuf>) -> ConfigFiles
	{
		ConfigFiles {
			resources_to_configs: BTreeMap::new(),
			configs_to_resources: BTreeMap::new(),
			to_rebuild: BTreeSet::new(),
			root: path.to_path_buf(),
			shosts: shosts,
			metadata: BTreeMap::new(),
			cache: ServiceCacheName2::new(),
		}
	}
	fn _set_resources_for_config(&mut self, config: &String, resources: BTreeSet<String>)
	{
		//If this already exists in configs_to_resources, iterate through it, removing any entry that does
		//not exist anymore, and If entry did not exist before, add it.
		if let Some(old_resources) = self.configs_to_resources.get_mut(config) {
			for r in old_resources.iter() { if !resources.contains(r) {
				delete_setvalue(&mut self.resources_to_configs, r, config);
			}}
			for r in resources.iter() { if !old_resources.contains(r) {
				insert_setvalue(&mut self.resources_to_configs, r, config);
			}}
			*old_resources = resources;
			return;
		}
		//Otherwise if old set does not exist, add all resources.
		for r in resources.iter() {
			insert_setvalue(&mut self.resources_to_configs, r, config);
		}
		self.configs_to_resources.insert(config.clone(), resources);
	}
	fn _configs_to_rebuild(&mut self, resource: &str)
	{
		//If this is valid config, add it itself to be rebuilt, even if not seen before. Note that
		//even if this happens, it can be subcomponent of previous config.
		if Self::_is_valid_config_name(resource) {
			self.to_rebuild.insert(resource.to_string());
		}
		if let Some(configs) = self.resources_to_configs.get(resource) {
			for c in configs.iter() { self.to_rebuild.insert(c.clone()); }
		}
	}
	fn _is_valid_config_name(name: &str) -> bool
	{
		//'*' is special, it is allowed.
		if name == "*" { return true; }
		//Names can never end in '.'.
		if name.ends_with(".") { return false; }
		//Otherwise, the syntax has to be <domain>, *.<domain> or **.<domain>, where <domain> is non-empty
		//DNS name.
		let name = if let Some(name) = name.strip_prefix("**.") {
			name
		} else if let Some(name) = name.strip_prefix("*.") {
			name
		} else {
			name
		};
		valid_dns_name(name)
	}
	fn consider_changed(&mut self, name: &[u8])
	{
		//Ignore files that are not valid UTF-8. Otherwise add those to to_rebuild set.
		if let Ok(x) = from_utf8(name) { self._configs_to_rebuild(x); }
	}
	fn full_rescan(&mut self)
	{
		let mut new_metadata = replace(&mut self.metadata, BTreeMap::new());
		let ditr = match self.root.read_dir() {
			Ok(x) => x,
			Err(err) => {
				syslog!(WARNING "Can not read directory {root}: {err}", root=self.root.display());
				return;
			}
		};
		for ent in ditr {
			let ent = match ent {
				Ok(x) => x,
				Err(err) => {
					syslog!(WARNING "Can not read directory {root}: {err}",
						root=self.root.display());
					return;
				}
			};
			let fname = match ent.file_name().into_string() {
				Ok(x) => x,
				Err(_) => continue	//Skip any file with name not valid UTF-8.
			};
			match ent.metadata() {
				Ok(x) => {new_metadata.insert(fname, x);},
				Err(err) => if err.kind() == IoErrorKind::NotFound {
					//Ignore racy delete.
				} else {
					syslog!(WARNING "Can not stat {path}: {err}", path=ent.path().display());
					return;
				}
			};
		}
		//Ok, now new_metadata is built, check for changes.
		for (f, nm) in new_metadata.iter() {
			let need_update = if let Some(om) = self.metadata.get(f) {
				nm.dev() != om.dev() || nm.ino() != om.ino() || nm.size() != om.size() ||
					nm.mtime() != om.mtime() || nm.mtime_nsec() != om.mtime_nsec()
			} else {
				true	//New file, need unconditional update.
			};
			if need_update { self.consider_changed(f.as_bytes()); }
		}
		swap(&mut self.metadata, &mut new_metadata);
		for (f, _) in new_metadata.iter() {
			//unconditional update on all deleted files. We can ignore changes, as all were captured
			//in above loop.
			if !self.metadata.contains_key(f) { self.consider_changed(f.as_bytes()); }
		}
		self.execute_rebuild();
	}
	fn execute_rebuild(&mut self)
	{
		let to_rebuild = replace(&mut self.to_rebuild, BTreeSet::new());
		let root = self.root.clone();
		for f in to_rebuild.iter() {
			let wildcard = f.deref() == "*" || f.starts_with("**.");
			let (result, resources) = ConfigFile::parse_servicename(f, &root, wildcard,
				self.shosts.as_deref());
			self._set_resources_for_config(f, resources);
			match result {
				Ok(Some(config)) => {
					syslog!(NOTICE "Configuration changed: Updating {f}");
					self.cache.create(f.clone(), config);
				},
				Ok(None) => {
					syslog!(NOTICE "Configuration changed: Deleting {f}");
					self.cache.delete(f);
				},
				Err(err) => {
					syslog!(WARNING "{err}");
					self.cache.create_error(f.clone(), err);
				}
			}
		}
	}
}

pub struct InotifySink
{
	cache: Arc<Mutex<ConfigFiles>>,
}


impl InotifyEventSink for InotifySink
{
	fn log_message(&self, message: &str)
	{
		syslog!(WARNING "{message}");
	}
	fn inotify_event(&self, filename: &[u8], event: u32) -> bool
	{
		//Check if event is interesting.
		if event & (CLOSED_WRITABLE | MOVED_FROM | MOVED_TO | CREATED | DELETED) == 0 { return false; }
		//Find the last '/' in filename and split from there.
		let mut split = 0;
		for i in 0..filename.len() { if filename[i] == b'/' { split = i + 1; }}
		let sni = match from_utf8(&filename[split..]) { Ok(x) => x, Err(_) => return false };
		//Clear the entry.
		if !sni.starts_with(".") { syslog!(NOTICE "Flushing configuration entry '{sni}'"); }
		self.cache.lock().consider_changed(sni.as_bytes());
		true		//Execute queued events after batch.
	}
}

fn call_with_alpn_specific<F>(data: &mut ParsingServiceTarget, f: F)
	where F: FnOnce(&mut Vec<Arc<Service>>, &mut Option<bool>)
{
	let mut iservices = Vec::new();
	let mut idecrypt = None;
	if let &mut ParsingServiceTarget::Explicit(ref mut xservices, xdecrypt) = data {
		swap(&mut iservices, xservices);
		idecrypt = xdecrypt;
	}
	f(&mut iservices, &mut idecrypt);
	*data = ParsingServiceTarget::Explicit(iservices, idecrypt);
}

fn parse_config_directive(scfg: &mut ParsingServiceProperties, name: &mut ParsingNameProperties, directive: &str,
	alpn: bool, shosts: Option<&Path>, ip_blocks: &mut IpSet,
	default_ports: &mut HashMap<ExtendedPort, String>) -> Result<(), Cow<'static, str>>
{
	Ok(if is_service(directive) {
		let sdata = parse_target_line(directive, shosts)?;
		call_with_alpn_specific(&mut scfg.service, |iservices,_|{
			//This can produce many services at once.
			match sdata {
				OneOrMany::One(x) => iservices.push(x),
				OneOrMany::Many(mut x) => iservices.extend(x.drain(..)),
			}
		});
	} else if directive == "enable" {
		//This is actually ignored on this level.
	} else if !alpn && directive == "tls13-only" {
		name.special_flags |= SFLAG_TLS13_ONLY;
	} else if !alpn && directive == "high-encryption" {
		name.special_flags |= SFLAG_SECURE192;
	} else if !alpn && directive == "best-encryption" {
		name.special_flags |= SFLAG_SPINAL_TAP;
	} else if !alpn && directive == "no-fail-log" {
		name.special_flags |= SFLAG_NO_LOG;
	} else if !alpn && directive == "no-android-hack" {
		name.special_flags |= SFLAG_NO_ANDROID_HACK;
	} else if !alpn && directive == "nsa-mode" {
		name.special_flags |= SFLAG_NSA_MODE;
	} else if directive == "no-timeout" {
		scfg.notimeout = Some(true);
	} else if directive == "timeout" {
		scfg.notimeout = Some(false);
	} else if directive == "decrypt" {
		call_with_alpn_specific(&mut scfg.service, |_,idecrypt|*idecrypt=Some(true));
	} else if directive == "no-decrypt" {
		call_with_alpn_specific(&mut scfg.service, |_,idecrypt|*idecrypt=Some(false));
	} else if directive == "request-certificate" {
		scfg.clientcert = Some(true);
	} else if directive == "no-request-certificate" {
		scfg.clientcert = Some(false);
	} else if directive == "delay" {
		scfg.delayed = Some(true);
	} else if directive == "no-delay" {
		scfg.delayed = Some(false);
	} else if directive == "service-timeout-only" {
		scfg.service_fatal_eof = Some(true);
	} else if directive == "no-service-timeout-only" {
		scfg.service_fatal_eof = Some(false);
	} else if let (false, Some(directive)) = (alpn, directive.strip_prefix("ip-allow ")) {
		match parse_netmask(directive) {
			Some((ip,mask)) => ip_blocks.add(IpAddr::V6(ip), mask, false),
			None => return Ok(syslog!(WARNING "Bad IP address"))
		};
	} else if let (false, Some(directive)) = (alpn, directive.strip_prefix("ip-deny ")) {
		match parse_netmask(directive) {
			Some((ip,mask)) => ip_blocks.add(IpAddr::V6(ip), mask, true),
			None => return Ok(syslog!(WARNING "Bad IP address"))
		};
	} else if let Some(arg) = directive.strip_prefix("allow-spki ") {
		if arg.len() != 64 {
			syslog!(WARNING "Bad SPKI {arg}");
			return Ok(());
		}
		let mut bin = [0;32];
		let mut bad = false;
		for i in 0..64 {
			let d = arg.as_bytes()[i];
			bin[i/2] |= match d {
				48..=57 => d - 48,
				65..=70 => d - 55,
				97..=102 => d - 87,
				_ => {bad = true; 0 }
			} << 4 - i % 2 * 4;
		}
		if bad {
			syslog!(WARNING "Bad SPKI {arg}");
			return Ok(());
		}
		scfg.acl.insert(bin);
		scfg.clientcert = Some(true);
		call_with_alpn_specific(&mut scfg.service, |_,idecrypt|*idecrypt=Some(true));
	} else if !alpn && directive == "intercept-acme" {
		name.intercept_acme = true;
	} else if let (false, Some(val)) = (alpn, directive.strip_prefix("default-alpn ")) {
		let mut itr = val.splitn(2, " ");
		let a = itr.next();
		let b = itr.next();
		let (a, b) = match (a, b) {
			(Some(a), Some(b)) => (a, b),
			_ => return Ok(syslog!(WARNING "Bad default-alpn {directive}"))
		};
		match ExtendedPort::parse(a) {
			Some(ExtendedPort::Tcp(443)) =>
				return Ok(syslog!(WARNING "Setting default-alpn on port 443 not allowed")),
			Some(a) => default_ports.insert(a, b.to_owned()),
			None => return Ok(syslog!(WARNING "Bad default-alpn {directive}"))
		};
	} else {
		syslog!(WARNING "Unrecognized directive '{directive}'");
	})
}

fn skip_whitespace(l: &str, spos: &mut usize) -> bool
{
	loop {
		if *spos == l.len() { return false; }
		let b = l.as_bytes()[*spos];
		if !isspace(b) { break; }
		*spos += 1;
	}
	true
}

fn deescape_word<'a>(l: &'a str, spos: &mut usize) -> Cow<'a, str>
{
	let mut need_canon = false;
	let mut escape = false;
	let ospos = *spos;
	while *spos < l.len() {
		let b = l.as_bytes()[*spos];
		if !escape && isspace(b) { break; }
		escape = if escape { false } else { l.as_bytes()[*spos] == b'\\' };
		need_canon |= escape;
		*spos += 1;
	}
	let raw = &l[ospos..*spos];
	if !need_canon { return Cow::Borrowed(raw); }
	let mut s = String::with_capacity(l.len());
	escape = false;
	for c in raw.chars() {
		if !escape {
			if c == '\\' { escape = true; } else { s.push(c); }
		} else {
			s.push(c);
			escape = false;
		}
	}
	Cow::Owned(s)
}

fn deescape_alpn_directive<'a>(l: &'a str) -> Result<(Cow<'a, str>, &'a str), Cow<'static, str>>
{
	let mut spos = 0;
	let alpn = deescape_word(l, &mut spos);
	fail_if!(!skip_whitespace(l, &mut spos), "No space after ALPN in alpn line");
	Ok((alpn, &l[spos..]))
}

fn deescape_alpnmap_directive<'a>(l: &'a str) ->
	Result<(Cow<'a, str>, Cow<'a, str>, Cow<'a, str>), Cow<'static, str>>
{
	let mut spos = 0;
	let port = deescape_word(l, &mut spos);
	fail_if!(!skip_whitespace(l, &mut spos), "No space after port in alpn-map line");
	let alpn = deescape_word(l, &mut spos);
	fail_if!(!skip_whitespace(l, &mut spos), "No space after ALPN in alpn-map line");
	let service = deescape_word(l, &mut spos);
	fail_if!(skip_whitespace(l, &mut spos), "Junk after service in alpn-map line");
	Ok((port, alpn, service))
}

fn isspace(c: u8) -> bool { c == 9 || c == 32 }

fn canonicalize_line<'a>(l: &'a str) -> Cow<'a, str>
{
	let mut hpos = 0;
	let mut escaped = false;
	while hpos < l.len() {
		if !escaped && l.as_bytes()[hpos] == b'#' { break; }
		escaped = if escaped { false } else { l.as_bytes()[hpos] == b'\\' };
		hpos += 1;
	}
	let l = &l[..hpos];
	let mut spos = 0;
	let mut epos = l.len();
	while spos < l.len() && isspace(l.as_bytes()[spos]) { spos += 1; }
	while epos > spos && isspace(l.as_bytes()[epos-1]) { epos -= 1; }
	let l = &l[spos..epos];

	let mut need_canon = false;
	let mut escape = false;
	let mut last_space = false;
	for c in l.chars() {
		if !escape {
			need_canon |= c == '\t' || (c == ' ' && last_space);
			last_space = c == ' ';
			escape = c == '\\';
		} else {
			//Last_space always false.
			escape = false;
		}
	}
	if !need_canon { return Cow::Borrowed(l); }
	let mut s = String::with_capacity(l.len());
	escape = false;
	for c in l.chars() {
		if !escape {
			if last_space && c == ' ' {
				//Skip this.
			} else  if c == '\t' {
				s.push(' ');
			} else if c == '\\' {
				escape = true;
				s.push(c);
			} else {
				s.push(c);
			}
		} else {
			escape = false;
			s.push(c);
		}
	}
	Cow::Owned(s)
}

macro_rules! trans_addr
{
	($ts:expr, $rest:expr, $proxykind:expr, $shosts:expr) => {
		match translate_multiple_address($rest, None, $shosts) {
			Ok(mut x) => OneOrMany::Many(x.drain(..).
				map(|y|Arc::new(Service::Gsocket(SocketAddrExHandle::new(y), $proxykind))).
				collect()),
			Err(_) => fail!(format!("Invalid target address '{tgt}' for socket", tgt=$rest))
		}
	};
}

const TARGET_EXECUTE: &'static str = "execute";
const TARGET_SOCK: &'static str = "socket";
const TARGET_SOCK_V1: &'static str = "socket-proxy";
const TARGET_SOCK_V2: &'static str = "socket-proxy-v2";
const TARGET_TCP: &'static str = "tcp";
const TARGET_TCP_V1: &'static str = "tcp-proxy";
const TARGET_TCP_V2: &'static str = "tcp-proxy-v2";
const TARGET_DNS: &'static str = "dns";
const TARGET_DNS_V1: &'static str = "dns-proxy";
const TARGET_DNS_V2: &'static str = "dns-proxy-v2";
const TARGET_TGT: &'static str = "target";
const TARGET_TGT_V1: &'static str = "target-proxy";
const TARGET_TGT_V2: &'static str = "target-proxy-v2";

enum OneOrMany<T>
{
	One(T),
	Many(Vec<T>),
}

fn parse_target_line(content: &str, shosts: Option<&Path>) -> Result<OneOrMany<Arc<Service>>, Cow<'static, str>>
{
	let spacepos = content.find(' ').ok_or_else(||"No space on target line")?;
	let protocol = &content[..spacepos];
	let rest = &content[spacepos+1..];	//spacepos is an index, and thus not near overflow.
	Ok(if protocol == TARGET_SOCK || protocol == TARGET_TCP || protocol == TARGET_DNS {
		syslog!(WARNING "Deprecated configuration directive {content}");
		trans_addr!(ts, rest, 0, shosts)
	} else if protocol == TARGET_SOCK_V1 || protocol == TARGET_TCP_V1 || protocol == TARGET_DNS_V1 {
		syslog!(WARNING "Deprecated configuration directive {content}");
		trans_addr!(ts, rest, 1, shosts)
	} else if protocol == TARGET_SOCK_V2 || protocol == TARGET_TCP_V2 || protocol == TARGET_DNS_V2 {
		syslog!(WARNING "Deprecated configuration directive {content}");
		trans_addr!(ts, rest, 2, shosts)
	} else if protocol == TARGET_TGT {
		trans_addr!(ts, rest, 0, shosts)
	} else if protocol == TARGET_TGT_V1 {
		trans_addr!(ts, rest, 1, shosts)
	} else if protocol == TARGET_TGT_V2 {
		trans_addr!(ts, rest, 2, shosts)
	} else if protocol == TARGET_EXECUTE {
		let mut components = Vec::new();
		let mut component = String::new();
		let mut escaped = false;
		for i in rest.chars() {
			if !escaped {
				if i == ' ' {
					if component.len() > 0 { components.push(component.clone()); }
					component = String::new();
				} else if i == '\\' {
					escaped = true;
				} else {
					component.push(i);
				}
			} else {
				escaped = false;
				component.push(i);
			}
		}
		if component.len() > 0 { components.push(component.clone()); }
		fail_if!(components.len() == 0, "Empty command line");
		OneOrMany::One(Arc::new(Service::Command(components)))
	} else {
		fail!(format!("Invalid target type '{protocol}'"));
	})
}

fn has_word_prefix(haystack: &str, needle: &str) -> bool
{
	let after = f_return!(haystack.strip_prefix(needle), false);
	after.starts_with(" ")
}

fn is_service(content: &str) -> bool
{
	has_word_prefix(content, TARGET_EXECUTE) || has_word_prefix(content, TARGET_SOCK) ||
		has_word_prefix(content, TARGET_SOCK_V1) || has_word_prefix(content, TARGET_SOCK_V2) ||
		has_word_prefix(content, TARGET_TCP) || has_word_prefix(content, TARGET_TCP_V1) ||
		has_word_prefix(content, TARGET_TCP_V2) || has_word_prefix(content, TARGET_DNS) ||
		has_word_prefix(content, TARGET_DNS_V1) || has_word_prefix(content, TARGET_DNS_V2) ||
		has_word_prefix(content, TARGET_TGT) || has_word_prefix(content, TARGET_TGT_V1) ||
		has_word_prefix(content, TARGET_TGT_V2)
}

fn valid_dns_name(name: &str) -> bool
{
	let mut last_empty = false;
	//Empty name is not valid.
	//Name starting with ., unless it is . is not valid.
	if name.len() == 0 || (name.starts_with(".") && name != ".") { return false; }
	for label in name.split(".") {
		if last_empty { return false; }		//.. in name.
		last_empty = label.len() == 0;
		//The label must consist of alphanumerics, - and _.
		for c in label.as_bytes().iter().cloned() {
			match c { 45|46|48..=57|65..=90|95|97..=122 => (), _ => return false }
		}
	}
	//Ok, this name is valid.
	true
}
