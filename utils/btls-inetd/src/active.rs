use crate::acme::AcmeService;
use crate::common::ActiveCommon;
use crate::common::ActiveCommonParameters;
use crate::Connection2;
use crate::ConnectionService;
use crate::common::ErrorKind;
use crate::common::ServiceTrait;
use crate::legacy_service::ServiceLaunchParameters;
use crate::metrics_dp::RaiiActive;
use crate::services_dp::LaunchInfoExtended;
use crate::tls::TlsState;
use btls::callbacks::PointInTime;
use btls::transport::PullTcpBuffer;
use btls_aux_hash::Checksum;
use btls_aux_x509certparse::dn_extract_cn;
use btls_daemon_helper_lib::AddTokenCallback;
use btls_daemon_helper_lib::cow;
use btls_daemon_helper_lib::IO_WAIT_HUP;
use btls_daemon_helper_lib::IO_WAIT_READ;
use btls_daemon_helper_lib::IO_WAIT_WRITE;
use btls_daemon_helper_lib::Poll;
use std::borrow::Cow;
use std::ops::Deref;


//Fully active connection. Service state, TLS state and TCP state all exist.
pub struct ActiveConnection<Service:ServiceTrait>
{
	common: ActiveCommon<Service>,		//Common stuff.
	service_sent_data: bool,		//Service has sent _any_ data.
	tls: TlsState,				//The TLS state.
	tcp_eof_seen: bool,			//TCP has seen incoming EOF.
	raii: RaiiActive,			//Hold state.
}

impl<Service:ServiceTrait> ActiveConnection<Service>
{
	pub fn new(cparams: ActiveCommonParameters<Service>, tls: TlsState, raii: RaiiActive) ->
		ActiveConnection<Service>
	{
		ActiveConnection {
			common: ActiveCommon::new(cparams),
			service_sent_data: false,
			tls: tls,
			tcp_eof_seen: false,
			raii: raii,
		}
	}
	//Flush all data from TLS to queue.
	fn flush_tls(&mut self, signal_errors: bool) -> Result<(), ErrorKind>
	{
		match self.tls.tls_pull_tcp_data_buffer(PullTcpBuffer::no_data()) {
			Ok(buf) => {
				self.common.try_send_immediately(&buf)?;
				self.tls.tls_return_buffer_pull(buf);
			},
			Err(_) if !signal_errors => (),
			Err(e) => fail!(ErrorKind::Tls(e))
		}
		Ok(())
	}
	//Perform shutdown after TLS error. This is appropriate after TLS indicates error status.
	fn handle_tls_error(mut self, poll: &mut Poll, cause: Cow<'static, str>) -> Result<Connection2, Self>
	{
		self.flush_tls(false).ok();
		let raii = self.raii;
		Ok(self.common.handle_tls_error(poll, cause, self.tcp_eof_seen, raii))
	}
	//Send a fake internal_error.
	fn fake_internal_error(self, poll: &mut Poll, cause: Cow<'static, str>) -> Result<Connection2, Self>
	{
		let raii = self.raii;
		self.common.shutdown(poll, cause, raii)	//This is NOT supposed to happen.
	}
	//Perform shutdown due to failed service.
	fn service_failed(mut self, poll: &mut Poll, cause: Cow<'static, str>, code: u8) -> Result<Connection2, Self>
	{
		self.tls.raise_code(code);
		self.flush_tls(false).ok();
		let raii = self.raii;
		Ok(self.common.handle_tls_error(poll, cause, self.tcp_eof_seen, raii))
	}
	fn handle_timed_event_inner(mut self, poll: &mut Poll, now: PointInTime) -> Result<Connection2, Self>
	{
		//If service fatal EOF is set, connection never timeouts from active state.
		if self.common.backend_timeouts_only(true) { return Err(self); }
		if let Err(err) = self.tls.maybe_handle_timeout(now) {
			self.common.global.metrics(|m|m.timeout_error());
			let raii = self.raii;
			return self.common.shutdown(poll, err, raii);
		}
		Err(self)
	}
	pub fn get_next_timed_event(&self, _now: PointInTime) -> Option<PointInTime>
	{
		//If service fatal EOF is set, connection never timeouts from active state after launch.
		fail_if_none!(self.common.backend_timeouts_only(true));
		self.tls.get_timeout()
	}
	//Handle TX interrupt if token matches.
	fn maybe_handle_tx_interrupt(&mut self, tok: usize) -> Result<(), ErrorKind>
	{
		if self.tls.is_tx_interrupt(tok) {
			//Flush data in order to continue handshake.
			self.flush_tls(true)?;
		}
		Ok(())
	}
	fn handle_data_from_tcp(&mut self, buffer: &[u8]) -> Result<(), ErrorKind>
	{
		//This function should only be called with empty output buffer. Optimize for that case.
		let mut stolen = false;
		if let Some(tmp) = self.common.incoming.steal_buffer() {
			self.tls.tls_return_buffer_push(tmp);
			stolen = true;
		}
		let (tmp, eofd) = self.tls.tls_push_tcp_data_buffer(buffer).map_err(ErrorKind::Tls)?;
		//If olen != 0, Bump secondary timeout.
		if tmp.len() > 0 { self.tls.update_secondary_timeout(false); }
		if stolen {
			self.common.incoming.unsteal_buffer(tmp);
		} else {
			//The extra copy here is not on the fastpath.
			self.common.incoming.append(&tmp);
			self.tls.tls_return_buffer_push(tmp);
		}
		if eofd { self.common.incoming.set_eof(); }
		Ok(())
	}
	fn handle_eof_from_tcp(&mut self) -> Result<(), ErrorKind>
	{
		self.tcp_eof_seen = true;
		//If tls-level EOF has been seen, ignore this. If not, raise bad_record_mac and call
		//handle_tls_error().
		self.tls.handle_eof()?;
		Ok(())
	}
	fn handle_data_from_service(&mut self, buffer: &[u8]) -> Result<(), ErrorKind>
	{
		//Bump secondary timeout if buffer is not empty. Also, the service has now sent data.
		if buffer.len() == 0 { return Ok(()); }
		self.tls.update_secondary_timeout(false);
		self.service_sent_data = true;
		//Send the data.
		let buf = self.tls.tls_pull_tcp_data_buffer(PullTcpBuffer::write(buffer)).
			map_err(ErrorKind::Tls)?;
		self.common.try_send_immediately(&buf)?;
		self.tls.tls_return_buffer_pull(buf);
		Ok(())
	}
	fn handle_eof_from_service(&mut self) -> Result<(), ErrorKind>
	{
		//Bump secondary timeout.
		self.tls.update_secondary_timeout(false);
		self.common.set_service_eof_seen();
		//Send EOF and try flush it immediately.
		let buf = self.tls.tls_pull_tcp_data_buffer(PullTcpBuffer::no_data().eof()).
			map_err(|e|ErrorKind::Tls(e))?;
		self.common.try_send_immediately(&buf)?;
		self.tls.tls_return_buffer_pull(buf);
		//If incoming EOF (on TCP or TLS level) has been seen, shut down the connection.
		fail_if!(self.common.incoming.is_acked_eof(), ErrorKind::NormalClose);
		//If service fatal EOF is set, also shut down the connection.
		fail_if!(self.common.backend_timeouts_only(false), ErrorKind::ServiceTimeoutEof);
		Ok(())
	}
	fn __launch_service(&mut self, poll: &mut Poll, addtoken: &mut AddTokenCallback) -> Result<(), &'static str>
	{
		//Obtain the connection information.
		let info = self.common.tcpinfo().clone();
		//Get the SNI and ALPN used.
		let target = self.tls.get_sni_and_alpn().ok_or("Error getting TLS callback data")?;
		fail_if!(target.sni.deref() == "", "No SNI sent");
		//Obtain the certificate CN and SPKI hash.
		let certificate = self.tls.pop_client_certificate();
		let subject_dn = certificate.as_ref().and_then(|x|x.get_subject());
		let subject_cn = subject_dn.as_ref().and_then(|x|dn_extract_cn(x).ok());
		let spkihash = certificate.as_ref().and_then(|x|x.get_public_key_hash2(Checksum::Sha256));
		let spkihash = spkihash.as_ref().map(|x|x.as_ref());
		let lparams = LaunchInfoExtended {
			properties: self.tls.get_properties(),
			requested_cert: self.tls.requested_cert(),
			terr_cell: self.common.get_timeout_error_cell(),
		};
		//Actually launch the service.
		let (conninfo, version) = self.tls.get_version_info();
		let service = target.sprops.preresolve_service(&target.hostdata);
		self.common.service_launch(poll, ServiceLaunchParameters {
			info: info,
			tlsver: version,
			sni: &target.sni,
			raw_alpn: &target.raw_alpn,
			routeas_alpn: target.sprops.get_service_name(&target.hostdata),
			conninfo: &conninfo,
			spkihash: spkihash,
			cn: subject_cn.as_ref().map(|x|x.as_bytes()),
			//ACME ignores access control.
			ignore_access_control: target.sflags.is_acme(),
		}, &service, addtoken, lparams);
		Ok(())
	}
	fn handle_event_inner(mut self, poll: &mut Poll, tok: usize, kind: u8, addtoken: &mut AddTokenCallback) ->
		Result<Connection2, Self>
	{
		self.common.service_handle_event(poll, tok, kind);		//Handle service events.
		handle_fault!(self.maybe_handle_tx_interrupt(tok), self, poll);
		if kind & IO_WAIT_WRITE != 0 && self.common.is_tcp_event(tok) {
			//The only thing done with TCP write events is flushing buffers.
			handle_fault!(self.common.flush_buffers_to_tcp(), self, poll);
		}
		if kind & (IO_WAIT_READ|IO_WAIT_HUP) != 0 && self.common.is_tcp_event(tok) && !self.tcp_eof_seen &&
			self.tls.tls_wants_rx() {
			let mut buffer = [0;16700];
			let buffer = handle_fault!(self.common.read_from_tcp(&mut buffer), self, poll);
			handle_fault!(if let Some(buffer) = buffer {
				self.handle_data_from_tcp(buffer)
			} else {
				self.handle_eof_from_tcp()
			}, self, poll);
		}
		//If we have gotten far enough in handshake, set up the service to be launched.
		//If should launch is set, actually perform the launch.
		//If service is in error state, do service lost.
		if self.tls.ready_for_launch() { self.common.service_signal_launch(); }
		if self.common.service_should_launch() {
			//Count post-quantum commit.
			if self.tls.is_post_quantum() { self.common.global.metrics(|gm|gm.commit_post_quantum()); }
			if let Err(err) = self.__launch_service(poll, addtoken) {
				self.common.service_set_error(Cow::Borrowed(err));
			}
		}
		if self.common.service_pending_shutdown() {
			let emsg = self.common.service_get_error();
			let ekind = if self.common.service_is_access_denied() {
				ErrorKind::AccessDenied(emsg)
			} else if !self.service_sent_data {
				ErrorKind::ServiceFailedWith(emsg)
			} else {
				ErrorKind::ServiceShutdown(emsg)
			};
			handle_fault!(Err(ekind), self, poll);
		}

		//Perform I/O with service.
		let (w, fw, r) = self.common.service_rendezvous_request();
		if r && !self.common.have_unsent_data() {
			//Read from service, push to TLS. Note that this can only be reached once with closed service
			//stdout. Both handle_data_from_service and handle_eof_from_service try to instantly flush,
			//so no need to call flush_buffers_to_tcp.
			let mut buffer = [0;16384];
			handle_fault!(match self.common.service_read_from(&mut buffer) {
				Ok(Some(x)) => self.handle_data_from_service(&buffer[..x]),
				Ok(None) => self.handle_eof_from_service(),
				Err(err) => Err(ErrorKind::ServiceRead(err)),
			}, self, poll);
		}
		//Perform output to service.
		if handle_fault!(self.common.do_service_out(poll, w, fw), self, poll) {
			self.tls.update_secondary_timeout(false);
		}
		//TLS flags.
		let flags = self.tls.tls_get_status();
		let mut transmit_now = flags.is_want_tx();
		//This can race. In that case, behave like is_want_tx() is set.
		if flags.is_blocked() { transmit_now |= self.tls.wait_for_txirq(); }
		if transmit_now { self.tls.fire_irq_local(); }
		//Check status of TLS just to be sure not to deadlock due to nothing trying to use failed TLS.
		if flags.is_dead() {
			let err = self.tls.tls_get_error();
			handle_fault!(Err(if let Some(err) = err {
				ErrorKind::Tls(err)
			} else {
				ErrorKind::NormalClose
			}), self, poll);
		}

		handle_fault!(self.common.retrigger_polls(poll, self.tcp_eof_seen), self, poll);
		Err(self)
	}
}

define_active_special!(ActiveConnection<ConnectionService>, |y|Connection2::Active(y));
define_active_special!(ActiveConnection<AcmeService>, |y|Connection2::Acme(y));
