use crate::conninit::ACME_CONTROL_PROTO;
use crate::conninit::ACME_TLS_PROTO;
use crate::legacy_service::Service;
use crate::legacy_service::ServiceLaunchParameters;
use crate::legacy_service::ServiceSet;
use crate::tls::SFLAG_TLS13_ONLY;
use btls::utility::RwLock;
use btls_aux_bitflags::Bitflags;
use btls_daemon_helper_lib::AddTokenCallback;
use btls_daemon_helper_lib::call_with_nth_fallback;
use btls_daemon_helper_lib::ConnectionInfoAddress;
use btls_daemon_helper_lib::cow;
use btls_daemon_helper_lib::FileDescriptor;
use btls_daemon_helper_lib::FrozenIpSet;
use btls_daemon_helper_lib::Poll;
use btls_daemon_helper_lib::RcuHashMap;
use btls_util_logging::syslog;
use std::borrow::Cow;
use std::cell::RefCell;
use std::collections::BTreeMap;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fmt::Debug;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::iter::FromIterator;
use std::net::IpAddr;
use std::ops::Deref;
use std::rc::Rc;
use std::str::FromStr;
use std::sync::Arc;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering;

const ACME_SERVICE: &'static str = "$acme";

pub trait FdConnector: Send+Sync
{
	fn create3(&self, poll: &mut Poll, info_ex: &ServiceLaunchParameters, addtoken: &mut AddTokenCallback,
		_extended: Option<&LaunchInfoExtended>) -> Result<FileDescriptor, Cow<'static, str>>;
}

#[derive(Clone)]
pub struct FdConnectorBox(Arc<dyn FdConnector>);

impl FdConnectorBox
{
	pub fn new(c: Arc<dyn FdConnector>) -> FdConnectorBox { FdConnectorBox(c) }
}

impl Debug for FdConnectorBox
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> { f.write_str("FdConnectorBox") }
}

#[derive(Copy,Clone,Debug)]
pub struct ConnectionProperties(u8);
pub(crate) const CPROP_TLS13: u8 = 1 << 0;
pub(crate) const CPROP_SECURE192: u8 = 1 << 1;
pub(crate) const CPROP_WEAK: u8 = 1 << 2;
pub(crate) const CPROP_NSA: u8 = 1 << 3;

impl ConnectionProperties
{
	pub(crate) fn wrap(f: u8) -> ConnectionProperties { ConnectionProperties(f) }
	pub fn is_tls13(self) -> bool { self.0 & CPROP_TLS13 != 0 }
	pub fn is_secure192(self) -> bool { self.0 & CPROP_SECURE192 != 0 }
	pub fn is_weak_ciphers(self) -> bool { self.0 & CPROP_WEAK != 0 }
	pub fn is_nsa(self) -> bool { self.0 & CPROP_NSA != 0 }
}

pub struct LaunchInfoExtended
{
	//This is set if algorithms are good for secure192, not only if secure192 was requested.
	pub properties: ConnectionProperties,
	pub requested_cert: bool,
	pub terr_cell: Rc<RefCell<Option<String>>>,
}

pub type BackendMapT = RcuHashMap<String, (bool, ServiceSet)>;

#[derive(Clone,Debug)]
enum AlpnSpecificInner
{
	None,
	Explicit(Arc<ServiceSet>, bool),
	Indirect(BackendMapT, Option<String>),
	Connector(FdConnectorBox, bool, u32, Arc<String>),
}

#[derive(Clone,Debug)]
struct AlpnSpecific
{
	data: AlpnSpecificInner,
	clientcert: bool,
	acl: Option<Arc<HashSet<[u8;32]>>>,
	notimeout: bool,
	delayed: bool,
	service_fatal_eof: bool,
}

impl AlpnSpecific
{
	fn get_decrypt(&self) -> bool
	{
		match &self.data {
			&AlpnSpecificInner::None => true,
			&AlpnSpecificInner::Explicit(_, decrypt) => decrypt,
			&AlpnSpecificInner::Indirect(_, None) => true,	//Not found.
			&AlpnSpecificInner::Indirect(ref map, Some(ref key)) =>
				map.get_with(key, |&(d,_)|d).unwrap_or(false),
			&AlpnSpecificInner::Connector(_, decrypt, _, _) => decrypt,
		}
	}
}


#[derive(Clone,Debug,Hash,PartialEq,Eq)]
pub enum ExtendedPort
{
	Tcp(u16),
	Unix(String),
	Abstract(String),
}

impl ExtendedPort
{
	pub fn parse(port: &str) -> Option<ExtendedPort>
	{
		if port.starts_with("/") {
			Some(ExtendedPort::Unix(port.to_owned()))
		} else if let Some(port) = port.strip_prefix("@") {
			Some(ExtendedPort::Abstract(port.to_owned()))
		} else if let Some(port) = u16::from_str(&port).ok() {
			Some(ExtendedPort::Tcp(port))
		} else {
			None
		}
	}
	pub fn borrow<'a>(&'a self) -> ExtendedPortBorrow<'a>
	{
		match self {
			&ExtendedPort::Tcp(port) => ExtendedPortBorrow::Tcp(port),
			&ExtendedPort::Unix(ref port) => ExtendedPortBorrow::Unix(port),
			&ExtendedPort::Abstract(ref port) => ExtendedPortBorrow::Abstract(port),
		}
	}
}

#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum ExtendedPortBorrow<'a>
{
	Tcp(u16),
	Unix(&'a str),
	Abstract(&'a str),
}

impl<'a> ExtendedPortBorrow<'a>
{
	fn from_cia(port: &'a ConnectionInfoAddress) -> Option<ExtendedPortBorrow<'a>>
	{
		use self::ExtendedPortBorrow::*;
		Some(match port {
			&ConnectionInfoAddress::V4(_, port)|&ConnectionInfoAddress::V6(_, port) => Tcp(port),
			&ConnectionInfoAddress::Unix(ref path) => Unix(path),
			&ConnectionInfoAddress::Abstract(ref path) => Abstract(path),
			_ => return None,
		})
	}
}

impl Display for ExtendedPort
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&ExtendedPort::Tcp(addr) => write!(f, "tcp:{addr}"),
			&ExtendedPort::Unix(ref addr) => write!(f, "unix:{addr}"),
			&ExtendedPort::Abstract(ref addr) => write!(f, "abstract:{addr}"),
		}
	}
}

#[derive(Clone,Debug)]
struct PortKeyMap<T:Clone+Debug>
{
	port: HashMap<u16,T>,
	abstr: HashMap<String,T>,
	unix: HashMap<String,T>,
}

impl<T:Clone+Debug> PortKeyMap<T>
{
	fn new() -> PortKeyMap<T>
	{
		PortKeyMap {
			port: HashMap::new(),
			abstr: HashMap::new(),
			unix: HashMap::new(),
		}
	}
	fn add(&mut self, ent: ExtendedPort, val: T) -> Option<T>
	{
		match ent {
			ExtendedPort::Tcp(port) => self.port.insert(port, val),
			ExtendedPort::Unix(port) => self.unix.insert(port, val),
			ExtendedPort::Abstract(port) => self.abstr.insert(port, val),
		}
	}
	fn get(&self, ent: ExtendedPortBorrow) -> Option<&T>
	{
		match ent {
			ExtendedPortBorrow::Tcp(port) => self.port.get(&port),
			ExtendedPortBorrow::Unix(port) => self.unix.get(port),
			ExtendedPortBorrow::Abstract(port) => self.abstr.get(port),
		}
	}
	fn entry(&mut self, ent: ExtendedPort, v: impl FnOnce() -> T) -> &mut T
	{
		match ent {
			ExtendedPort::Tcp(port) => self.port.entry(port).or_insert_with(v),
			ExtendedPort::Unix(port) => self.unix.entry(port).or_insert_with(v),
			ExtendedPort::Abstract(port) => self.abstr.entry(port).or_insert_with(v),
		}
	}
}

impl<T:Clone+Debug> FromIterator<(ExtendedPort, T)> for PortKeyMap<T>
{
	fn from_iter<I:IntoIterator<Item=(ExtendedPort, T)>>(iter: I) -> PortKeyMap<T>
	{
		let mut c = PortKeyMap::new();
		for (k,v) in iter { c.add(k, v); }
		c
	}
}

#[derive(Debug)]
struct NameSpecific
{
	base: AlpnSpecific,
	//The fields not in AlpnSpecific.
	intercept_acme: bool,
	ip_blacklist: Option<Arc<FrozenIpSet>>,
	special_flags: u32,
}

#[derive(Debug,Clone)]
pub enum ParsingServiceTarget
{
	///Inherit from parent.
	Inherit,
	///Explicit none.
	None,
	///Explicit list.
	Explicit(Vec<Arc<Service>>, Option<bool>),
	///Indirect list. Note that if key is None, this actually inherits.
	Indirect(BackendMapT, Option<String>),
	///Direct creator.
	DirectFd(FdConnectorBox, Option<bool>, u32, String),
}

#[derive(Debug,Clone)]
pub struct ParsingNameProperties
{
	pub intercept_acme: bool,
	pub ip_blacklist: Option<Arc<FrozenIpSet>>,
	pub special_flags: u32,
	_dummy: ()
}

impl ParsingNameProperties
{
	pub fn new() -> ParsingNameProperties
	{
		ParsingNameProperties {
			intercept_acme: false,
			ip_blacklist: None,
			special_flags: 0,
			_dummy: ()
		}
	}
}

#[derive(Debug,Clone)]
pub struct ParsingServiceProperties
{
	pub service: ParsingServiceTarget,
	pub clientcert: Option<bool>,
	pub acl: HashSet<[u8;32]>,
	pub notimeout: Option<bool>,
	pub delayed: Option<bool>,
	pub service_fatal_eof: Option<bool>,
	_dummy: ()
}

impl ParsingServiceProperties
{
	pub fn new() -> ParsingServiceProperties
	{
		ParsingServiceProperties {
			service: ParsingServiceTarget::Inherit,
			clientcert: None,
			acl: HashSet::new(),
			notimeout: None,
			delayed: None,
			service_fatal_eof: None,
			_dummy: ()
		}
	}
	fn __to_alpn_specific(self, base: Option<&NameSpecific>) -> AlpnSpecific
	{
		//The base _AlpnSpecific structure, if any.
		let base: Option<&AlpnSpecific> = base.as_ref().map(|base|&base.base);
		let dflt_decrypt = base.map(|b|b.get_decrypt()).unwrap_or(true);
		AlpnSpecific {
			data: match &self.service {
				//If base is set, its data field is inherited. Otherwise nothing is.
				//Indirect with None as symbolic backend also inherits.
				&ParsingServiceTarget::Inherit =>
					base.map(|x|x.data.clone()).unwrap_or(AlpnSpecificInner::None),
				&ParsingServiceTarget::None => AlpnSpecificInner::None,
				&ParsingServiceTarget::Explicit(ref services, decrypt) =>
					AlpnSpecificInner::Explicit(Arc::new(ServiceSet::new(services)),
					decrypt.unwrap_or(dflt_decrypt)),
				//Indirect with No reference inherits, but does not become none.
				&ParsingServiceTarget::Indirect(_, None) if base.is_some() =>
					base.map(|x|x.data.clone()).unwrap_or(AlpnSpecificInner::None),
				&ParsingServiceTarget::Indirect(ref map, ref key) =>
					AlpnSpecificInner::Indirect(map.clone(), key.clone()),
				&ParsingServiceTarget::DirectFd(ref ctor, decrypt, version, ref name) =>
					AlpnSpecificInner::Connector(ctor.clone(), decrypt.unwrap_or(dflt_decrypt),
					version, Arc::new(name.clone())),
			},
			acl: if self.acl.len() > 0 {
				Some(Arc::new(self.acl))
			} else {
				None
			},
			//For these, fall back to inherit and then to default.
			clientcert: self.clientcert.or(base.map(|b|b.clientcert)).unwrap_or(false),
			notimeout: self.notimeout.or(base.map(|b|b.notimeout)).unwrap_or(false),
			delayed: self.delayed.or(base.map(|b|b.delayed)).unwrap_or(false),
			service_fatal_eof: self.service_fatal_eof.or(base.map(|b|b.service_fatal_eof)).
				unwrap_or(false),
		}
	}
	fn __make_name_specific(self, ns: ParsingNameProperties) -> NameSpecific
	{
		NameSpecific {
			intercept_acme: ns.intercept_acme,
			ip_blacklist: ns.ip_blacklist.clone(),
			special_flags: ns.special_flags,
			base: self.__to_alpn_specific(None),
		}
	}
	fn __make_service_specific(self, base: &NameSpecific) -> AlpnSpecific
	{
		self.__to_alpn_specific(Some(base))
	}
}

fn add_service(a: &mut Vec<AlpnSpecific>, b: &mut Vec<String>, id: &mut BTreeMap<String,usize>, c: AlpnSpecific,
	d: String) -> usize
{
	let i = a.len();
	a.push(c);
	b.push(d.clone());
	id.insert(d, i);
	i
}

fn undef_service(a: &mut BTreeMap<String,usize>, b: &mut Vec<String>, c: String) -> usize
{
	if let Some(&i) = a.get(&c) { return i; }
	let i = b.len();
	a.insert(c.clone(), i);
	b.push(c);
	i
}

pub struct ParseOldHost
{
	name: ParsingNameProperties,
	main: ParsingServiceProperties,
	wildcard: bool,
	alpns: HashMap<String, ParsingServiceProperties>,
	default_ports: HashMap<ExtendedPort, String>,
}

impl ParseOldHost
{
	pub fn new(name: ParsingNameProperties, main: ParsingServiceProperties, wildcard: bool) -> ParseOldHost
	{
		ParseOldHost {
			name,
			main,
			wildcard,
			alpns: HashMap::new(),
			default_ports: HashMap::new(),
		}
	}
	pub fn add_alpn(&mut self, alpn: &str, data: ParsingServiceProperties)
	{
		self.alpns.insert(alpn.to_owned(), data);
	}
	pub fn add_port_default(&mut self, port: ExtendedPort, alpn: &str)
	{
		self.default_ports.insert(port, alpn.to_owned());
	}
	pub fn done(mut self) -> ServiceName
	{
		let main = self.main.__make_name_specific(self.name);
		let mut alpns2 = BTreeMap::new();
		let mut alpnlist = BTreeMap::new();
		let mut services = Vec::new();
		let mut service_names = Vec::new();
		let mut name_indices = BTreeMap::new();
		let empty = String::new();
		let mut local_alpnlist: PortKeyMap<BTreeMap<String,usize>> = PortKeyMap::new();

		//The main base service is always #0.
		add_service(&mut services, &mut service_names, &mut name_indices, main.base.clone(), String::new());
		for (alpn, config) in self.alpns.drain() {
			let service = config.__make_service_specific(&main);
			let service_index = add_service(&mut services, &mut service_names, &mut name_indices,
				service.clone(), alpn.clone());
			alpns2.insert(alpn.clone(), service_index);
			//For now, make alpnlist identity mapping.
			alpnlist.insert(alpn, service_index);
		}
		//Stuff the default ALPNs into local_alpnlist "" ALPN entries.
		for (port, dfltalpn) in self.default_ports.drain() {
			let index = undef_service(&mut name_indices, &mut service_names, dfltalpn);
			local_alpnlist.entry(port, ||BTreeMap::new()).insert(empty.clone(), index);
		}
		//The default entry for port 443 is fixed.
		let index = undef_service(&mut name_indices, &mut service_names, "http/1.1".to_owned());
		local_alpnlist.entry(ExtendedPort::Tcp(443), ||BTreeMap::new()).insert(empty.clone(), index);

		ServiceName(Arc::new(_ServiceName{
			services: services,
			service_names: service_names,
			alpn: alpns2,
			global_alpns: alpnlist,
			local_alpns: local_alpnlist,
			intercept_acme: main.intercept_acme,
			wildcard: self.wildcard,
			ip_blacklist: main.ip_blacklist,
			use_new_mapping: false,
			special_flags: main.special_flags,
		}))
	}
}
pub struct ParseNewHost
{
	name: ParsingNameProperties,
	main: ParsingServiceProperties,
	wildcard: bool,
	alpn_map: HashMap<(Option<ExtendedPort>, String), String>,
	services: HashMap<String, ParsingServiceProperties>,
}

impl ParseNewHost
{
	pub fn new(name: ParsingNameProperties, main: ParsingServiceProperties, wildcard: bool) -> ParseNewHost
	{
		ParseNewHost {
			name,
			main,
			wildcard,
			alpn_map: HashMap::new(),
			services: HashMap::new(),
		}
	}
	pub fn add_service(&mut self, service: &str, data: ParsingServiceProperties)
	{
		self.services.insert(service.to_owned(), data);
	}
	pub fn add_global_alpn(&mut self, alpn: &str, service: &str)
	{
		self.alpn_map.insert((None, alpn.to_owned()), service.to_owned());
	}
	pub fn add_local_alpn(&mut self, alpn: &str, service: &str, port: ExtendedPort)
	{
		//If called with alpn == "" and service == "", delete entry.
		if alpn.len() == 0 && service.len() == 0 {
			self.alpn_map.remove(&(Some(port), String::new()));
			return;
		}
		self.alpn_map.insert((Some(port), alpn.to_owned()), service.to_owned());
	}
	pub fn done(self) -> ServiceName
	{
		let main = self.main.__make_name_specific(self.name);
		let mut alpnlist: BTreeMap<String, usize> = BTreeMap::new();
		let mut local_alpns: PortKeyMap<BTreeMap<String, usize>> = PortKeyMap::new();
		let mut services = Vec::new();
		let mut service_names = Vec::new();
		let mut name_indices = BTreeMap::new();
		let mut services2: BTreeMap<String, usize> = BTreeMap::new();

		//In new mode, the default service is not used for anything, so service0 is normal service.
		//The services being in map guaratees there are no conflicts.
		for (service, sdata) in self.services.iter() {
			let servicedata = sdata.clone().__make_service_specific(&main);
			let service_index = add_service(&mut services, &mut service_names, &mut name_indices,
				servicedata.clone(), service.clone());
			services2.insert(service.clone(), service_index);
		}
		//The alpn_map being in map guaratees there are no conflicts.
		for ((port, alpn), service) in self.alpn_map.iter() {
			//Port = None, alpn = "" is not valid, as it would be default-alpn entry on unknown
			//port.
			if port.is_none() && alpn.len() == 0 { continue; }
			//Service must be present in services map.
			let service = *f_continue!(services2.get(service));
			//Insert to correct map.
			let map: &mut BTreeMap<String, usize> = if let Some(port) = port {
				local_alpns.entry(port.clone(), ||BTreeMap::new())
			} else {
				&mut alpnlist
			};
			map.insert(alpn.clone(), service);
		}

		ServiceName(Arc::new(_ServiceName{
			services: services,
			service_names: service_names,
			alpn: services2,
			global_alpns: alpnlist,
			local_alpns: local_alpns,
			intercept_acme: main.intercept_acme,
			wildcard: self.wildcard,
			ip_blacklist: main.ip_blacklist,
			use_new_mapping: true,
			special_flags: main.special_flags,
		}))
	}
}

fn __dafuq_no_service0() -> BadService
{
	syslog!(CRITICAL "__get_properties_for_alpn_old: Service0 does not exist???");
	BadService::NoDefault
}

#[derive(Debug)]
struct _ServiceName
{
	services: Vec<AlpnSpecific>,
	service_names: Vec<String>,
	alpn: BTreeMap<String, usize>,
	global_alpns: BTreeMap<String,usize>,
	local_alpns: PortKeyMap<BTreeMap<String,usize>>,
	intercept_acme: bool,
	wildcard: bool,
	ip_blacklist: Option<Arc<FrozenIpSet>>,
	use_new_mapping: bool,
	special_flags: u32,
}

impl _ServiceName
{
	fn __get_service_name<'a>(&'a self, index: usize) -> &'a str
	{
		self.service_names.get(index).map(|x|x.deref()).unwrap_or("(null)")
	}
	fn __do_local_remap(&self, port: Option<ExtendedPortBorrow>, alpn: &str) -> Option<usize>
	{
		port.and_then(|port|self.local_alpns.get(port)).and_then(|alpns|alpns.get(alpn)).map(|x|*x)
	}
	fn __get_properties_for_alpn_old(&self, port: Option<ExtendedPortBorrow>, alpn: Option<&str>) ->
		Result<ServiceProperties, BadService>
	{
		fail_if!(self.services.len() == 0, __dafuq_no_service0());
		//This is not ACME.
		Ok(if let Some(alpn) = alpn {
			ServiceProperties(_ServiceProperties::Generic(
				self.alpn.get(alpn).map(|x|*x).unwrap_or(0), None))
		} else {
			//First try to pull remap for "" from local remap. This is done even in legacy mode.
			//In legacy mode service indices out of range are mapped to the main service.
			if let Some(remapped_alpn) = self.__do_local_remap(port, "") {
				//Weird: In legacy mode, timeout disable for default is global timeout disable,
				//even if remap happened via port defaults.
				ServiceProperties(_ServiceProperties::Generic(remapped_alpn,
					self.services.get(0).map(|s|s.notimeout)))
			} else {
				//Use host-global default.
				ServiceProperties(_ServiceProperties::Generic(0, None))
			}
		})
	}
	fn __get_properties_for_alpn_new(&self, port: Option<ExtendedPortBorrow>, alpn: Option<&str>,
		alpn_present: bool) -> Result<ServiceProperties, BadService>
	{
		let (failed, remapped_alpn) = if let Some(alpn) = alpn {
			//Try pull remap for local remap, and then from global remap..
			let mut remapped_alpn = self.__do_local_remap(port, alpn);
			remapped_alpn = remapped_alpn.or_else(||self.global_alpns.get(alpn).map(|x|*x));
			(false, remapped_alpn)
		} else {
			//First try to pull remap for "" from local remap.
			let remapped_alpn = self.__do_local_remap(port, "");
			(true, remapped_alpn)
		};
		let remapped_alpn = remapped_alpn.ok_or(BadService::NoDefault)?;
		//In new mode, service_names can be larger than services, containing the names of the non-existent
		//services.
		let s = self.services.get(remapped_alpn).
			ok_or_else(||BadService::NoService(self.__get_service_name(remapped_alpn).to_owned()))?;
		//If ALPN was present but negotiation failed, only pass cut-through stuff.
		fail_if!(alpn_present && failed && s.get_decrypt(), BadService::NegotiationFailed);
		Ok(ServiceProperties(_ServiceProperties::Generic(remapped_alpn, None)))
	}
	fn __handle_get_properties_acme(&self, is_acmecontrol: bool) -> ServiceProperties
	{
		//ACME is handled specially. Firstly, if specific service exists for ACME_TLS_PROTO, use
		//that.
		let legacy_amode = if self.intercept_acme { AcmeMode::Capture } else { AcmeMode::Default };
		let acme_service = if self.use_new_mapping { ACME_SERVICE } else { ACME_TLS_PROTO };
		if let Some(&sidx) = self.alpn.get(acme_service) { if sidx < self.services.len() {
			let mode = if self.use_new_mapping { AcmeMode::ForceCapture } else { legacy_amode };
			return ServiceProperties(_ServiceProperties::SpecialAcme(sidx, mode, is_acmecontrol));
		}}
		//In legacy mode, if default handler exists, use it.
		if !self.use_new_mapping && self.services.len() > 0 {
			return ServiceProperties(_ServiceProperties::SpecialAcme(0, legacy_amode, is_acmecontrol));
		}
		//Otherwise force decoding.
		ServiceProperties(_ServiceProperties::DefaultAcme(is_acmecontrol))
	}
}

#[derive(Copy,Clone,PartialEq)]
pub enum AcmeMode
{
	//Default mode: Apply global forward if set, otherwise use vhost settings.
	Default,
	//Capture mode: Apply global forward if set, otherwise capture ACME.
	Capture,
	//Force capture mode: Use vhost settings, whatever those are.
	ForceCapture,
	//Not ACME.
	None
}

#[derive(Copy,Clone)]
pub struct LookedUpService<'a>
{
	s: Option<&'a AlpnSpecific>,
	is_acme: bool,
	ip_blacklist: &'a Option<Arc<FrozenIpSet>>,
}

impl<'a> LookedUpService<'a>
{
	pub fn is_banned_ip(&self, ip: Option<IpAddr>) -> bool
	{
		if let (Some(blacklist), Some(addr)) = (self.ip_blacklist.as_ref(), ip) {
			blacklist.lookup(addr)
		} else {
			false
		}
	}
	pub(crate) fn check_access(&self, spki: Option<&[u8]>, force: bool) -> bool
	{
		//ACME has its own check.
		if force || self.is_acme { return true; }
		//If service is invalid, fail check.
		let s = f_return!(self.s, false);
		//If there is no ACL, pass check.
		let acl = f_return!(s.acl.as_ref(), true);
		//If there is no SPKI, fail check, as there is nontrivial ACL.
		let spki = f_return!(spki, false);
		//Consult the ACL.
		acl.contains(spki)
	}
	pub(crate) fn choose_service(&self) -> Option<Arc<Service>>
	{
		let s = self.s?;
		match &s.data {
			&AlpnSpecificInner::None => None,
			&AlpnSpecificInner::Explicit(ref sset, _) => sset.choose(),
			&AlpnSpecificInner::Indirect(_, None) => None,	//Not found.
			&AlpnSpecificInner::Indirect(ref map, Some(ref key)) =>
				map.get_with(key, |(_,s)|s.choose()).unwrap_or(None),
			&AlpnSpecificInner::Connector(_, _, _, _) => None,
		}
	}
	pub(crate) fn direct_fd_ex(&self, poll: &mut Poll, info_ex: &ServiceLaunchParameters, 
		addtoken: &mut AddTokenCallback, eparams: Option<&LaunchInfoExtended>) ->
		Option<Result<(FileDescriptor, u32, Arc<String>), Cow<'static, str>>>
	{
		let s = self.s?;
		match &s.data {
			&AlpnSpecificInner::Connector(ref func, _, version, ref name) =>
				Some(func.0.create3(poll, info_ex, addtoken, eparams).
				map(|fd|(fd, version, name.clone()))),
			_ => None,	//Not directly connectable.
		}
	}
}

#[derive(Copy,Clone,Debug,Bitflags)]
#[bitflag(Wildcard)]		//Is wildcard.
#[bitflag(DisableTimeout)]	//Timeout disabled.
#[bitflag(CutThrough)]		//Cut through (no decryption).
#[bitflag(FatalEof)]		//EOFs are fatal.
#[bitflag(RequestCertificate)]	//Request certificate.
#[bitflag(Acme)]		//Is ACME connection.
#[bitflag(Delayed)]		//Init delayed.
struct ServiceFlags(u8);

#[derive(Copy,Clone)]
pub struct LookedUpServiceFlags
{
	flags: ServiceFlags,
	acme_mode: AcmeMode,
	special_flags: u32,
}

impl LookedUpServiceFlags
{
	pub(crate) fn is_wildcard(&self) -> bool { self.flags.is_Wildcard() }
	pub(crate) fn is_disable_timeout(&self) -> bool { self.flags.is_DisableTimeout() }
	pub(crate) fn is_cut_through(&self) -> bool { self.flags.is_CutThrough() }
	pub(crate) fn is_fatal_eof(&self) -> bool { self.flags.is_FatalEof() }
	pub(crate) fn is_request_certificate(&self) -> bool { self.flags.is_RequestCertificate() }
	pub(crate) fn is_acme(&self) -> bool { self.flags.is_Acme() }
	pub(crate) fn is_delayed(&self) -> bool { self.flags.is_Delayed() }
	pub(crate) fn get_acme_mode(&self) -> AcmeMode { self.acme_mode }
	pub(crate) fn get_special_flags(&self) -> u32 { self.special_flags }
}

#[derive(Clone)]
pub struct ServiceProperties(_ServiceProperties);
#[derive(Clone)]
enum _ServiceProperties
{
	Generic(usize, Option<bool>),
	DefaultAcme(bool),
	SpecialAcme(usize, AcmeMode, bool),
}

fn __acme_special_flags(control: bool) -> u32
{
	//Control always has flags SFLAG_TLS13_ONLY, acme never has special flags.
	if control { SFLAG_TLS13_ONLY } else { 0 }
}

impl ServiceProperties
{
	pub(crate) fn preresolve_service<'a>(&self, name: &'a ServiceName) -> LookedUpService<'a>
	{
		use self::_ServiceProperties as SP;
		let s = self.__get_alpn_specific(name);
		LookedUpService {
			ip_blacklist: match &self.0 {
				&SP::Generic(_,_) => &name.0.ip_blacklist,
				//ACME can not be restricted.
				&SP::DefaultAcme(_)|&SP::SpecialAcme(_,_,_) => &None,
			},
			is_acme: match &self.0 {
				&SP::Generic(_,_) => false,
				&SP::DefaultAcme(_)|&SP::SpecialAcme(_,_,_) => true,
			},
			s: s,
		}
	}
	pub(crate) fn preresolve_service_flags(&self, name: &ServiceName) -> LookedUpServiceFlags
	{
		use self::_ServiceProperties as SP;
		let s = self.__get_alpn_specific(name);
		let mut flags = ServiceFlags::none;
		if let Some(s) = s {
			flags.force_Wildcard(name.0.wildcard);
			flags.force_DisableTimeout(s.notimeout);
			flags.force_CutThrough(!s.get_decrypt());
			flags.force_FatalEof(s.service_fatal_eof);
			flags.force_RequestCertificate(s.clientcert || s.acl.is_some());
			//ACME is not set here.
			flags.force_Delayed(s.delayed);
		}
		if let &SP::DefaultAcme(control)|&SP::SpecialAcme(_,_,control) = &self.0 {
			//ACME always clears Wildcard and sets Acme. Client certificate exactly for control
			//connections.
			flags.clear_Wildcard();
			flags.force_RequestCertificate(control);
			flags.set_Acme();
		}
		if let &SP::DefaultAcme(_)|&SP::SpecialAcme(_,_,false) = &self.0 {
			//Default-ACME and non-control ACME may not have disable timeout.
			flags.clear_DisableTimeout();
		} else if let &SP::Generic(_, Some(dto)) = &self.0 {
			//Weird case: Overriding disable_timeout from global. This for some reason happens in
			//legacy mode when ALPN is assigned via port default.
			flags.force_DisableTimeout(dto);
		}

		LookedUpServiceFlags {
			flags: flags,
			acme_mode: match &self.0 {
				&SP::Generic(_,_) => AcmeMode::None,
				&SP::DefaultAcme(_) => AcmeMode::Default,
				&SP::SpecialAcme(_,acme_mode,_) => acme_mode,
			},
			special_flags: match &self.0 {
				&SP::Generic(_,_) => name.0.special_flags,
				&SP::DefaultAcme(control)|&SP::SpecialAcme(_,_,control) =>
					__acme_special_flags(control),
			},
		}
	}
	pub(crate) fn get_service_name<'a>(&self, name: &'a ServiceName) -> &'a str
	{
		match &self.0 {
			//No fallback here, since string index should always be valid.
			&_ServiceProperties::Generic(index,_) => match name.0.service_names.get(index) {
				Some(name) => name.deref(),
				None => "(null)"
			},
			&_ServiceProperties::DefaultAcme(_)|&_ServiceProperties::SpecialAcme(_,_,_) => "$acme",
		}
	}
	pub(crate) fn is_banned_ip(&self, name: &ServiceName, ip: Option<IpAddr>) -> bool
	{
		//ACME can not be restricted.
		match &self.0 {
			&_ServiceProperties::Generic(_,_) => match (&name.0.ip_blacklist, ip) {
				(&Some(ref blacklist), Some(addr)) => blacklist.lookup(addr),
				_ => false,
			},
			&_ServiceProperties::DefaultAcme(_)|&_ServiceProperties::SpecialAcme(_,_,_) => false,
		}
	}
	fn __get_alpn_specific<'a>(&self, name: &'a ServiceName) -> Option<&'a AlpnSpecific>
	{
		let index = match &self.0 {
			&_ServiceProperties::Generic(index,_)|&_ServiceProperties::SpecialAcme(index,_,_) => index,
			//This is never dispatched anyway.
			&_ServiceProperties::DefaultAcme(_) => return None,
		};
		let ns = &name.0.services;
		if let Some(s) = ns.get(index) { return Some(s); }
		if let Some(s) = ns.get(0) { return Some(s); }
		None
	}
}

#[derive(Clone)]
pub enum BadService
{
	NoDefault,
	NegotiationFailed,
	NoService(String),
}

thread_local!(static DUMMY_SERVICE: ServiceName = ServiceName(Arc::new(_ServiceName {
	services: Vec::new(),
	service_names: Vec::new(),
	alpn: BTreeMap::new(),
	global_alpns: BTreeMap::new(),
	local_alpns: PortKeyMap::new(),
	intercept_acme: true,
	wildcard: false,
	ip_blacklist: None,
	use_new_mapping: true,
	special_flags: 0,
})));

#[derive(Clone,Debug)]
pub struct ServiceName(Arc<_ServiceName>);

impl ServiceName
{
	pub fn default_acme(control: bool) -> (ServiceName, ServiceProperties)
	{
		(DUMMY_SERVICE.with(Clone::clone), ServiceProperties(_ServiceProperties::DefaultAcme(control)))
	}
	pub fn get_properties_for_alpn(&self, alpn: Option<&str>, alpn_present: bool,
		port: &ConnectionInfoAddress) -> Result<ServiceProperties, BadService>
	{
		let selfx = &self.0;
		let is_acme = alpn == Some(ACME_TLS_PROTO);
		let is_acmecontrol = alpn == Some(ACME_CONTROL_PROTO);
		if is_acme || is_acmecontrol {
			return Ok(selfx.__handle_get_properties_acme(is_acmecontrol));
		}
		//This is not ACME.
		let port = ExtendedPortBorrow::from_cia(port);
		if selfx.use_new_mapping {
			selfx.__get_properties_for_alpn_new(port, alpn, alpn_present)
		} else {
			selfx.__get_properties_for_alpn_old(port, alpn)
		}
	}
	pub fn alpns_supported_by_sni<'a>(&'a self, laddr: &ConnectionInfoAddress) -> AlpnSet<'a>
	{
		let port = ExtendedPortBorrow::from_cia(laddr);
		AlpnSet {
			global: Some(&self.0.global_alpns),
			local: port.and_then(|p|self.0.local_alpns.get(p)),
		}
	}
}

fn canonical_sni<'a>(sni: &'a str) -> Result<Cow<'a, str>, Cow<'static, str>>
{
	//See that there is no characters in SNI that would cause problems.
	//The following characters are illegal: 0-31, slash, backslash, 127-159.
	let mut has_not_dot = false;
	for i in sni.chars() {
		let chval = i as u32;
		has_not_dot |= i != '.';
		fail_if!(chval < 32 || i == '/' || i == '\\' || (chval >= 127 && chval < 160),
			cow!("Client sent illegal SNI"));
	}
	//Furthermore, disallow SNI with no characters
	fail_if!(!has_not_dot, cow!("Client sent illegal SNI"));
	//Any uppercase letters? If no, return borrowed string.
	if !sni.chars().any(|x|x as u32 >= 65 && x as u32 <= 90) { return Ok(Cow::Borrowed(sni)); }
	//ASCII-Lowercase the sni, these things are case-insensitive!
	let sni = sni.to_ascii_lowercase();
	Ok(Cow::Owned(sni))
}

#[derive(Clone)]
pub struct ServiceCacheName
{
	snis: Arc<RwLock<BTreeMap<String, Result<ServiceName, Cow<'static, str>>>>>,
	exec_allowed: Arc<AtomicBool>,
}

impl ServiceCacheName
{
	pub fn new() -> ServiceCacheName
	{
		ServiceCacheName {
			snis: Arc::new(RwLock::new(BTreeMap::new())),
			exec_allowed: Arc::new(AtomicBool::new(false)),
		}
	}
	pub fn delete(&self, sni: &str)
	{
		self.snis.with_write(|d|d.remove(sni));
	}
	pub fn create(&self, sni: String, value: ServiceName)
	{
		self.__write(sni, Ok(value))
	}
	pub fn create_error(&self, sni: String, value: Cow<'static, str>)
	{
		self.__write(sni, Err(value))
	}
	fn __write(&self, sni: String, value: Result<ServiceName, Cow<'static, str>>)
	{
		self.snis.with_write(|d|{
			if let Some(v) = d.get_mut(&sni) {
				*v = value;
				return;
			}
			d.insert(sni, value);
		})
	}
	//Lookup handled for default SNI.
	pub fn lookup_by_sni_default(&self) -> Result<Option<ServiceName>, Cow<'static, str>>
	{
		//The only entry to consider is "*".
		self.snis.with_read(|d|match d.get("*") {
			Some(&Ok(ref x)) => Ok(Some(x.clone())),
			Some(&Err(ref e)) => Err(e.clone()),
			None => Ok(None)
		})
	}
	//Lookup handler for SNI.
	pub fn lookup_by_sni(&self, sni: &str) -> Result<Option<ServiceName>, Cow<'static, str>>
	{
		let sni = canonical_sni(sni)?;
		self.snis.with_read(|d|call_with_nth_fallback(&sni, |sni|Some(match d.get(sni)? {
			&Ok(ref x) => Ok(Some(x.clone())),
			&Err(ref e) => Err(e.clone()),
		})).unwrap_or(Ok(None)))
	}
	pub(crate) fn exec_is_allowed(&self) -> bool { self.exec_allowed.load(Ordering::Acquire) }
	pub fn enable_exec(&mut self) { self.exec_allowed.store(true, Ordering::Release); }
}

#[derive(Debug,Clone)]
pub struct AlpnSet<'a>
{
	global: Option<&'a BTreeMap<String,usize>>,
	local: Option<&'a BTreeMap<String,usize>>,
}

impl<'a> AlpnSet<'a>
{
	pub fn blank() -> AlpnSet<'static>
	{
		AlpnSet {
			local: None,
			global: None,
		}
	}
	pub fn contains(&self, alpn: &str) -> bool
	{
		//"" is not a valid alpn.
		if alpn == "" { return false; }
		//Local has priority to global.
		if let Some(ref set) = self.local { if set.contains_key(alpn) { return true; } }
		if let Some(ref set) = self.global { if set.contains_key(alpn) { return true; } }
		false
	}
}
