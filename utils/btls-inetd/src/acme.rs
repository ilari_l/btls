use crate::common::ServiceTrait;
use crate::legacy_service::LaunchFate;
use crate::legacy_service::ServiceLaunchParameters;
use crate::services_dp::LaunchInfoExtended;
use crate::services_dp::LookedUpService;
use crate::tls::EventInner;
use btls::ServerConfiguration;
use btls::certificates::CertificateLookup;
use btls::certificates::KeyPair2;
use btls::certificates::LocalKeyPair;
use btls_aux_fail::ResultExt;
use btls_aux_signatures::base64url;
use btls_aux_signatures::iterate_pem_or_derseq_fragments;
use btls_aux_signatures::PemDerseqFragment2 as PFragment;
use btls_aux_signatures::PemFragmentKind as PFKind;
use btls_daemon_helper_lib::AddTokenCallback;
use btls_daemon_helper_lib::AllocatedToken;
use btls_daemon_helper_lib::cloned;
use btls_daemon_helper_lib::cow;
use btls_daemon_helper_lib::ListenerLoop;
use btls_daemon_helper_lib::Poll;
use btls_daemon_helper_lib::RemoteIrq;
use btls_daemon_helper_lib::TokenAllocatorPool;
use btls_util_logging::syslog;
use std::borrow::Cow;
use std::cmp::min;
use std::collections::BTreeMap;
use std::fs::File;
use std::fs::read_dir;
use std::mem::swap;
use std::io::Read;
use std::ops::Deref;
use std::path::Path;
use std::path::PathBuf;
use std::str::from_utf8;
use std::sync::Arc;


struct _HostkeyLookup
{
	hostkey: CertificateLookup,
	auth_data: BTreeMap<Vec<u8>, String>,
}

impl _HostkeyLookup
{
	fn from_keypair(keypair: Box<dyn KeyPair2+Send+Sync>) -> Result<_HostkeyLookup, Cow<'static, str>>
	{
		Ok(_HostkeyLookup {
			hostkey: CertificateLookup::hostkey(Some(keypair)),
			auth_data: BTreeMap::new(),
		})
	}
	fn add_authorization(&mut self, keyhash: Vec<u8>, owner: String)
	{
		self.auth_data.insert(keyhash, owner);
	}
	fn from_file(path: &str) -> Result<_HostkeyLookup, Cow<'static, str>>
	{
		let mut authdata = String::new();
		File::open(format!("{path}.auth")).and_then(|mut fp|fp.read_to_string(&mut authdata)).map_err(|err|{
			format!("Failed to read ACME control auth file ({path}.auth): {err}")
		})?;
		let mut auth_data = BTreeMap::new();
		for aline in authdata.lines() {
			let mut idx = aline.len();
			for (i, c) in aline.as_bytes().iter().cloned().enumerate() {
				if c == b'#' { idx = i; break; }
			}
			let aline = (&aline[..idx]).trim();
			if aline.len() == 0 { continue; }
			let mut itr = aline.splitn(2, ' ');
			let a = itr.next().unwrap_or("");
			let b = itr.next().unwrap_or("");
			let mut c = [0;32];
			let mut bad = false;
			for (i, d) in a.as_bytes().iter().cloned().enumerate() {
				c.get_mut(i/2).map(|x|*x |= match d {
					48..=57 => d - 48,
					65..=70 => d - 55,
					97..=102 => d - 87,
					_ => {bad = true; 0 }
				} << 4 - i % 2 * 4);
			}
			if bad || a.len() != 64 || b == "" {
				syslog!(WARNING "Bad ACME control auth line: {aline}");
				continue;
			}
			auth_data.insert((&c[..]).to_owned(), b.to_owned());
		}
		let kp = LocalKeyPair::new_file(path).map_err(|err|{
			format!("Failed to read ACME control key: {err}")
		})?;
		Ok(_HostkeyLookup {
			hostkey: CertificateLookup::hostkey(Some(Box::new(kp))),
			auth_data: auth_data,
		})
	}
	pub fn is_authorized(&self, spkihash: &[u8]) -> Option<String>
	{
		self.auth_data.get(spkihash).map(cloned)
	}
}

pub struct BuildHostkeyLookup(_HostkeyLookup);

impl BuildHostkeyLookup
{
	pub fn new(kp: Box<dyn KeyPair2+Send+Sync>) -> Result<BuildHostkeyLookup, Cow<'static, str>>
	{
		Ok(BuildHostkeyLookup(_HostkeyLookup::from_keypair(kp)?))
	}
	pub fn add_authorization(&mut self, keyhash: &[u8], owner: &str)
	{
		self.0.add_authorization(keyhash.to_owned(), owner.to_owned());
	}
	pub fn to_hostkey_lookup(self) -> HostkeyLookup
	{
		HostkeyLookup(Arc::new(Some(self.0)))
	}
}

#[derive(Clone)]
pub struct HostkeyLookup(Arc<Option<_HostkeyLookup>>);

impl HostkeyLookup
{
	pub fn disabled() -> HostkeyLookup
	{
		HostkeyLookup(Arc::new(None))
	}
	pub fn from_file(path: &str) -> Result<HostkeyLookup, Cow<'static, str>>
	{
		Ok(HostkeyLookup(Arc::new(Some(_HostkeyLookup::from_file(path)?))))
	}
	pub fn is_authorized(&self, spkihash: &[u8]) -> Option<String>
	{
		match self.0.deref() {
			&Some(ref obj) => obj.is_authorized(spkihash),
			&None => None
		}
	}
	pub fn as_lookup(&self) -> CertificateLookup
	{
		match self.0.deref() {
			Some(lookup) => lookup.hostkey.clone(),
			None => CertificateLookup::hostkey(None)
		}
	}
}

#[derive(Clone)]
pub struct AcmeServiceParams
{
	pub authorized: HostkeyLookup,
	pub config: ServerConfiguration,
	pub main_config: ServerConfiguration,
	pub certpath: PathBuf,
}

pub struct AcmeService
{
	service_error: Option<Cow<'static, str>>,
	sparams: AcmeServiceParams,
	link_closed: bool,
	launched: bool,
	txbuf: Vec<u8>,
	txbuf_ptr: usize,
	txeof: bool,
	rxbuf: Vec<u8>,
	irq_tgt: RemoteIrq,
	_irq_token: AllocatedToken,
	access_denied: bool,
}

impl AcmeService
{
	pub fn new(params: AcmeServiceParams, allocator: &mut TokenAllocatorPool) ->
		Result<(AcmeService, AllocatedToken), Cow<'static, str>>
	{
		let irq_token = allocator.allocate().set_err("Can't allocate Tokens")?;
		let irq_tgt = ListenerLoop::remote_irq_handle(&irq_token);
		Ok((AcmeService {
			service_error: None,
			sparams: params,
			launched: false,
			link_closed: false,
			txbuf: Vec::new(),
			txbuf_ptr: 0,
			txeof: false,
			rxbuf: Vec::new(),
			irq_tgt: irq_tgt,
			_irq_token: irq_token.clone(),
			access_denied: false,
		}, irq_token))
	}
	fn do_cmd(&mut self, cmd: &[u8])
	{
		let cmd = match from_utf8(cmd) {
			Ok(cmd) => cmd,
			Err(_) => return self.reply_err("Not UTF-8")
		};
		let mut itr = cmd.split(' ');
		let cmdc = itr.next().unwrap_or("");
		if cmdc == "set" {
			let name = itr.next().unwrap_or("");
			let keyauth = itr.next().unwrap_or("");
			return if name != "" && keyauth != "" {
				self.sparams.main_config.create_acme_challenge(name, keyauth);
				self.reply_ok(&cmd, None)
			} else {
				self.reply_err("invalid arguments")
			};
		} else if cmdc == "clear" {
			let name = itr.next().unwrap_or("");
			return if name != "" {
				self.sparams.main_config.delete_acme_challenge(name);
				self.reply_ok(&cmd, None)
			} else {
				self.reply_err("invalid arguments")
			};
		} else if cmdc == "download" {
			let name = itr.next().unwrap_or("");
			return if name != "" && !name.contains('/') && !name.contains('\0') {
				let filename = self.sparams.certpath.join(Path::new(
					&format!("{name}.crt/certificate")));
				let mut contents = Vec::new();
				match File::open(&filename).and_then(|mut fp|fp.read_to_end(&mut contents)) {
					Ok(_) => (),
					Err(err) => return self.reply_err(&format!("Unable to read: {err}")),
				};
				//Try to decode PEM.
				use self::PFragment::*;
				let mut contents2 = None;
				let r = iterate_pem_or_derseq_fragments(&contents, |frag|match frag {
					Comment(_) => Ok(()),
					Error => Err(format!("File is neither PEM nor DERseq")),
					Der(cert)|Pem(PFKind::Certificate, cert) => {
						fail_if!(contents2.is_some(),
							format!("File contains multiple certificates"));
						contents2 = Some(cert.to_owned());
						Ok(())
					},
					Pem(_, _) => Ok(()),
				});
				if let Err(e) = r { return self.reply_err(&e); }
				match contents2 {
					Some(contents) => {
						let contents = base64url(&contents);
						self.reply_ok(&cmd, Some(&contents));
					},
					None => self.reply_err("File contains no certificates")
				}
			} else {
				self.reply_err("invalid arguments")
			};
		} else if cmdc == "list" {
			let mut ents = String::new();
			let itr = match read_dir(&self.sparams.certpath) {
				Ok(x) => x,
				Err(err) => return self.reply_err(&format!("Unable to read: {err}")),
			};
			for ent in itr {
				let ent3 = match ent {
					Ok(x) => x.file_name().to_string_lossy().into_owned(),
					Err(_) => break
				};
				if ent3.ends_with(".crt") {
					let ent2 = &ent3[..ent3.len()-4];
					if !ent2.contains('/') && !ent2.contains(' ') && !ent2.contains('\0') {
						if ents.len() > 0 { ents.push(' '); }
						ents.push_str(ent2);
					}
				}
			}
			return self.reply_ok(&cmd, Some(&ents));
		} else {
			self.txbuf.extend_from_slice(b"BADCOMMAND\n");
			self.raise_txirq();
		}
	}
	fn reply_err(&mut self, message: &str)
	{
		self.txbuf.extend_from_slice(format!("ERROR {message}\n").as_bytes());
		self.raise_txirq();
	}
	fn reply_ok(&mut self, cmd: &str, payload: Option<&str>)
	{
		syslog!(NOTICE "Execute {cmd}");
		if let Some(payload) = payload {
			self.txbuf.extend_from_slice(format!("OK {payload}\n").as_bytes());
		} else {
			self.txbuf.extend_from_slice(b"OK\n");
		}
		self.raise_txirq();
	}
	fn raise_txirq(&self) { self.irq_tgt.irq(); }
}

impl ServiceTrait for AcmeService
{
	fn pending_shutdown(&self) -> bool
	{
		self.service_error.is_some()
	}
	fn set_error(&mut self, err: Cow<'static, str>)
	{
		self.service_error = Some(err);
	}
	fn get_error(&self) -> Cow<'static, str>
	{
		self.service_error.as_ref().map(cloned).unwrap_or(Cow::Borrowed("Unknown error???"))
	}
	fn remove_pollers(&mut self, _: &mut Poll)
	{
		//There are no pollers, so this does nothing.
	}
	fn retrigger_service(&mut self, _: &mut Poll)
	{
		//There are no pollers, so this does nothing.
	}
	fn prefix_sent(&self) -> bool
	{
		//This internally obtains what it needs on launch, so no prefix to send.
		true
	}
	fn handle_event(&mut self, _: &mut Poll, _: usize, _: u8)
	{
		//There are no pollers, so this does nothing.
	}
	fn rendezvous_request(&self) -> (bool, bool, bool)
	{
		//Read if launched and link not closed yet, write if anything in write buffer. Forced writes
		//are never performed.
		(self.launched && !self.link_closed, false,
			self.txbuf_ptr < self.txbuf.len() || self.txeof)
	}
	fn read_from_service(&mut self, buf: &mut [u8]) -> Result<Option<usize>, Cow<'static, str>>
	{
		//Just flush output buffer to wire.
		if self.txbuf_ptr < self.txbuf.len() {
			let t = min(self.txbuf.len() - self.txbuf_ptr, buf.len());
			(&mut buf[..t]).copy_from_slice(&self.txbuf[self.txbuf_ptr..][..t]);
			self.txbuf_ptr += t;
			if self.txbuf_ptr == self.txbuf.len() {
				self.txbuf_ptr = 0;
				self.txbuf.clear();
			}
			return Ok(Some(t));
		}
		if self.txeof { Ok(None) } else { Ok(Some(0)) }
	}
	fn close_link_to_service(&mut self, _: &mut Poll) -> Result<(), Cow<'static, str>>
	{
		//There will be no more reads or output.
		self.link_closed = true;
		self.txeof = true;
		self.raise_txirq();	//Cause connection to be closed.
		Ok(())
	}
	fn fate(&self) -> Option<LaunchFate>
	{
		if self.launched { Some(LaunchFate::Successful) } else { None }
	}
	fn tls_config_override(&self, ctrl: &mut EventInner) -> Option<ServerConfiguration>
	{
		ctrl.force_certificate_request = true;		//Always prompt certificate.
		ctrl.secondary_timeout_period = None;		//Disable timeouts after handshake.
		Some(self.sparams.config.clone())		//Return special configuration.
	}
	fn launch_service<'a>(&mut self, _: &mut Poll, _: &LookedUpService, params: ServiceLaunchParameters<'a>,
		_: &mut AddTokenCallback, _: Option<&LaunchInfoExtended>)
	{
		//Require client certificate.
		self.access_denied = true;	//Speculate on this.
		let client_spki = match params.spkihash.as_ref() {
			Some(hash) => hash.deref(),
			None => return self.service_error = Some(cow!("No client certificate presented"))
		};
		match self.sparams.authorized.is_authorized(client_spki) {
			Some(name) => {
				syslog!(NOTICE "ACME control session opened from {src} by {name}",
					src=params.info.src);
			},
			None => return self.service_error = Some(cow!("Unrecognized client certificate"))
		};
		self.access_denied = false;	//Nope.
		//Ok.
		self.txbuf = (&b"READY\n"[..]).to_owned();
		self.raise_txirq();
		self.launched = true;
	}
	fn write_to_service(&mut self, buf: &[u8], _: &mut Poll) -> Result<usize, Cow<'static, str>>
	{
		//Do not accept writes before launch.
		if !self.launched { return Ok(0); }
		self.rxbuf.extend_from_slice(buf);
		let mut rxbuf = Vec::new();
		swap(&mut rxbuf, &mut self.rxbuf);
		while rxbuf.len() > 0 {
			let mut off = 0;
			while rxbuf[off] != 10 { off += 1; }
			if off < rxbuf.len() {
				{
					let cmd = &rxbuf[..off];
					self.do_cmd(cmd);
				}
				//Remove the command. The count() is to consume the iterator.
				rxbuf.drain(..off+1).count();
			} else {
				break;
			}
		}
		swap(&mut rxbuf, &mut self.rxbuf);
		Ok(buf.len())
	}
	fn is_access_denied(&self) -> bool
	{
		self.access_denied
	}
}
