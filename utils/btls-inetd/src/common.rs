use crate::ConfigT;
use crate::Connection2;
use crate::ConnectionService;
use crate::format_alert;
use crate::legacy_service::LaunchFate;
use crate::legacy_service::ServiceLaunchParameters;
use crate::metrics_dp::RaiiActive;
use crate::metrics_dp::RaiiCutThrough;
use crate::metrics_dp::RaiiServiceFailed;
use crate::metrics_dp::RaiiShutdown;
use crate::services_dp::LaunchInfoExtended;
use crate::services_dp::LookedUpService;
use crate::shutdown::ServiceFailed;
use crate::shutdown::Shutdown;
use crate::tcp::TcpBuffer;
use crate::tcp::TcpState;
use crate::tls::EventInner;
use btls::Error;
use btls::ServerConfiguration;
use btls::callbacks::PointInTime;
use btls::callbacks::TimeUnit;
use btls_daemon_helper_lib::AddTokenCallback;
use btls_daemon_helper_lib::ConnectionInfo;
use btls_daemon_helper_lib::IO_WAIT_HUP;
use btls_daemon_helper_lib::IO_WAIT_READ;
use btls_daemon_helper_lib::IO_WAIT_WRITE;
use btls_daemon_helper_lib::Poll;
use std::cell::RefCell;
use std::io::Error as IoError;
use std::io::Read;
use std::borrow::Cow;
use std::rc::Rc;


pub enum ErrorKind
{
	Tls(Error),
	TcpRead(IoError),
	TcpWrite(IoError),
	ServiceRead(Cow<'static, str>),
	ServiceWrite(Cow<'static, str>),
	AccessDenied(Cow<'static, str>),
	ServiceShutdown(Cow<'static, str>),
	ServiceFailedWith(Cow<'static, str>),
	RegisterFailed(Cow<'static, str>),
	CutThroughFailedWith(Cow<'static, str>),
	CutThroughFailed,
	NormalClose,
	FailedToSinkData,
	InsecureTruncation,
	ServiceTimeoutEof,
}

macro_rules! handle_fault
{
	($e:expr, $selfx:expr, $poll:expr) => {
		match $e {
			Ok(x) => x,
			Err(ErrorKind::Tls(err)) => {
				let txcount = $selfx.common.tx_count;
				$selfx.common.global.metrics(|m|m.tls_error());
				let msg = cow!(F "TLS error: {err} [in={txcount}, out=?]");
				return $selfx.handle_tls_error($poll, msg)
			},
			Err(ErrorKind::TcpRead(err)) => {
				let txcount = $selfx.common.tx_count;
				$selfx.common.global.metrics(|m|m.transport_error());
				let raii = $selfx.raii;
				let msg = cow!(F "TCP read error: {err} [in={txcount}, out=?]");
				return $selfx.common.shutdown($poll, msg, raii);
			},
			Err(ErrorKind::TcpWrite(err)) => {
				let txcount = $selfx.common.tx_count;
				$selfx.common.global.metrics(|m|m.transport_error());
				let raii = $selfx.raii;
				let msg = cow!(F "TCP write error: {err} [in={txcount}, out=?]");
				return $selfx.common.shutdown($poll, msg, raii);
			},
			Err(ErrorKind::FailedToSinkData) => {
				let txcount = $selfx.common.tx_count;
				$selfx.common.global.metrics(|m|m.tls_error());
				let raii = $selfx.raii;
				let msg = cow!(F "TLS refused to process sent data [in={txcount}, out=?]");
				return $selfx.common.shutdown($poll, msg, raii);
			},
			Err(ErrorKind::ServiceRead(err)) => {
				let txcount = $selfx.common.tx_count;
				let rxcount = $selfx.common.rx_count;
				$selfx.common.global.metrics(|m|m.service_error());
				let msg = cow!(F "Service read error: {err} [in={txcount}, out={rxcount}]");
				return $selfx.service_failed($poll, msg, 80);
			},
			Err(ErrorKind::ServiceWrite(err)) => {
				let txcount = $selfx.common.tx_count;
				let rxcount = $selfx.common.rx_count;
				$selfx.common.global.metrics(|m|m.service_error());
				let msg = cow!(F "Service write error: {err} [in={txcount}, out={rxcount}]");
				return $selfx.service_failed($poll, msg, 80);
			},
			Err(ErrorKind::ServiceTimeoutEof) => {
				let txcount = $selfx.common.tx_count;
				let rxcount = $selfx.common.rx_count;
				$selfx.common.global.metrics(|m|m.timeout_error());
				let terr_cell = $selfx.common.timeout_error.borrow();
				let msg = if let Some(ref terr) = *terr_cell {
					cow!(F "{terr} [in={txcount}, out={rxcount}]")
				} else {
					cow!(F "Service timed connection out [in={txcount}, out={rxcount}]")
				};
				drop(terr_cell);
				return $selfx.service_failed($poll, msg, 0);
			},
			Err(ErrorKind::ServiceShutdown(err)) => {
				let txcount = $selfx.common.tx_count;
				let rxcount = $selfx.common.rx_count;
				$selfx.common.global.metrics(|m|m.service_error());
				let msg = cow!(F "{err} [in={txcount}, out={rxcount}]");
				return $selfx.service_failed($poll, msg, 80);
			},
			Err(ErrorKind::AccessDenied(err)) => {
				$selfx.common.global.metrics(|m|m.service_error_nodata());
				let msg = cow!(F "{err} [in=none, out=none]");
				return $selfx.service_failed($poll, msg, 49);
			},
			Err(ErrorKind::ServiceFailedWith(err)) => {
				let txcount = $selfx.common.tx_count;
				let rxcount = $selfx.common.rx_count;
				$selfx.common.global.metrics(|m|m.service_error_nodata());
				let msg = cow!(F "{err} [in={txcount}, out={rxcount}]");
				return $selfx.service_failed($poll, msg, 80);
			},
			Err(ErrorKind::RegisterFailed(err)) => {
				let txcount = $selfx.common.tx_count;
				$selfx.common.global.metrics(|m|m.poll_error());
				let raii = $selfx.raii;
				let msg = cow!(F "Failed to reregister poll: {err} [in={txcount}, out=?]");
				return $selfx.common.shutdown($poll, msg, raii);
			}
			Err(ErrorKind::CutThroughFailedWith(err)) => {
				$selfx.common.global.metrics(|m|m.service_error_nodata());
				return $selfx.fake_internal_error($poll, err);
			},
			Err(ErrorKind::CutThroughFailed) => {
				$selfx.common.global.metrics(|m|m.service_error_nodata());
				let msg = cow!("Empty reply from cut-through");
				return $selfx.fake_internal_error($poll, msg);
			},
			Err(ErrorKind::NormalClose) => {
				let txcount = $selfx.common.tx_count;
				let rxcount = $selfx.common.rx_count;
				$selfx.common.global.metrics(|m|m.normal_close());
				let msg = cow!(F "Normal close [in={txcount}, out={rxcount}]");
				return $selfx.handle_tls_error($poll, msg)
			},
			Err(ErrorKind::InsecureTruncation) => {
				let txcount = $selfx.common.tx_count;
				let rxcount = $selfx.common.rx_count;
				$selfx.common.global.metrics(|m|m.insecure_truncation());
				let msg = cow!(F "Incoming stream insecurely truncated [in={txcount}, out={rxcount}]");
				return $selfx.handle_tls_error($poll, msg)
			},
		}
	}
}

macro_rules! reserve_commit
{
	($buffer:expr, $amt:expr, $f:expr) => {{
		let old = $buffer.data_length();
		$buffer.resize_data(old + $amt);
		let amt: Result<usize, ErrorKind> = $f(old);
		let len = match amt { Ok(x) => x, Err(_) => 0 };
		$buffer.resize_data(old + len);
		amt.map(|_|())
	}}
}

macro_rules! define_active_special
{
	($name:path,$backwrapper:expr) => {
		impl $name
		{
			pub fn handle_timed_event(self, now: PointInTime, poll: &mut Poll) -> Connection2
			{
				match self.handle_timed_event_inner(poll, now) {
					Ok(x) => return x,
					Err(x) => $backwrapper(x)
				}
			}
			pub fn handle_event(self, poll: &mut Poll, tok: usize, kind: u8,
				addtoken: &mut AddTokenCallback) -> Connection2
			{
				match self.handle_event_inner(poll, tok, kind, addtoken) {
					Ok(x) => return x,
					Err(x) => $backwrapper(x)
				}
			}
			pub fn handle_fault(self, poll: &mut Poll) -> Connection2
			{
				//We do not have any better error than broken pipe. Also emulate one of immediate
				//shutdown paths.
				let txcount = self.common.tx_count;
				self.common.global.metrics(|m|m.transport_error());
				let raii = self.raii;
				let msg = cow!(F "Broken pipe [in={txcount}, out=?]");
				self.common.shutdown_forced(poll, msg, raii)
			}
		}
	}
}

pub trait ActiveRaiiTrait
{
	fn to_service_failed(self) -> RaiiServiceFailed;
	fn to_shutdown(self) -> RaiiShutdown;
}

impl ActiveRaiiTrait for RaiiActive
{
	fn to_service_failed(self) -> RaiiServiceFailed { self.to_service_failed() }
	fn to_shutdown(self) -> RaiiShutdown { self.to_shutdown() }
}

impl ActiveRaiiTrait for RaiiCutThrough
{
	fn to_service_failed(self) -> RaiiServiceFailed { self.to_service_failed() }
	fn to_shutdown(self) -> RaiiShutdown { self.to_shutdown() }
}

pub trait ServiceTrait
{
	fn remove_pollers(&mut self, poll: &mut Poll);
	fn retrigger_service(&mut self, poll: &mut Poll);
	fn prefix_sent(&self) -> bool;
	fn rendezvous_request(&self) -> (bool, bool, bool);
	fn read_from_service(&mut self, buf: &mut [u8]) -> Result<Option<usize>, Cow<'static, str>>;
	fn write_to_service(&mut self, buf: &[u8], poll: &mut Poll) -> Result<usize, Cow<'static, str>>;
	fn pending_shutdown(&self) -> bool;
	fn set_error(&mut self, err: Cow<'static, str>);
	fn get_error(&self) -> Cow<'static, str>;
	fn handle_event(&mut self, poll: &mut Poll, tok: usize, kind: u8);
	fn close_link_to_service(&mut self, poll: &mut Poll) -> Result<(), Cow<'static, str>>;
	fn launch_service<'a>(&mut self, poll: &mut Poll, service: &LookedUpService,
		params: ServiceLaunchParameters<'a>, addtoken: &mut AddTokenCallback,
		extended: Option<&LaunchInfoExtended>);
	fn fate(&self) -> Option<LaunchFate>;
	fn tls_config_override(&self, _ctrl: &mut EventInner) -> Option<ServerConfiguration>
	{
		//By default, do nothing.
		None
	}
	fn is_access_denied(&self) -> bool
	{
		false	//Assume no access control.
	}
	//By default no.
	fn is_tls13_only(&self) -> bool { false }
	//By default no.
	fn is_secure192(&self) -> bool { false }
}

impl ServiceTrait for ConnectionService
{
	fn remove_pollers(&mut self, poll: &mut Poll)
	{
		self.remove_pollers(poll)
	}
	fn retrigger_service(&mut self, poll: &mut Poll)
	{
		self.retrigger_service(poll)
	}
	fn prefix_sent(&self) -> bool
	{
		self.prefix_sent()
	}
	fn rendezvous_request(&self) -> (bool, bool, bool)
	{
		self.rendezvous_request()
	}
	fn read_from_service(&mut self, buf: &mut [u8]) -> Result<Option<usize>, Cow<'static, str>>
	{
		self.read_from_service(buf)
	}
	fn write_to_service(&mut self, buf: &[u8], poll: &mut Poll) -> Result<usize, Cow<'static, str>>
	{
		self.write_to_service(buf, poll)
	}
	fn pending_shutdown(&self) -> bool
	{
		self.pending_shutdown()
	}
	fn set_error(&mut self, err: Cow<'static, str>)
	{
		self.set_error(err)
	}
	fn get_error(&self) -> Cow<'static, str>
	{
		self.get_error()
	}
	fn handle_event(&mut self, poll: &mut Poll, tok: usize, kind: u8)
	{
		self.handle_event(poll, tok, kind)
	}
	fn close_link_to_service(&mut self, poll: &mut Poll) -> Result<(), Cow<'static, str>>
	{
		self.close_link_to_service(poll)
	}
	fn launch_service<'a>(&mut self, poll: &mut Poll, service: &LookedUpService,
		params: ServiceLaunchParameters<'a>, addtoken: &mut AddTokenCallback,
		extended: Option<&LaunchInfoExtended>)
	{
		self.launch_service_ex(poll, service, params, addtoken, extended)
	}
	fn fate(&self) -> Option<LaunchFate>
	{
		self.fate()
	}
	fn is_access_denied(&self) -> bool
	{
		self.is_access_denied()
	}
}

pub struct ActiveCommonParameters<Service:ServiceTrait>
{
	pub global: ConfigT,
	pub service: Service,
	pub tcp: TcpState,
	pub data: Vec<u8>,
	pub service_fatal_eof: bool,
	pub flush_delay: TimeUnit,
	pub shutdown_delay: TimeUnit,
	pub cipher_flags: u128,
}

pub struct ActiveCommon<Service:ServiceTrait>
{
	pub global: ConfigT,					//Global config.
	service: Service,					//The connected service.
	fate_ok_ack: bool,					//ACK about fate ok.
	tcp: TcpState,						//The TCP state.
	outgoing: TcpBuffer,					//Outgoing encrypted data.
	pub incoming: TcpBuffer,				//Incoming decrypted data.
	service_eof_seen: bool,					//Service has seen EOF.
	flush_delay: TimeUnit,					//Delay for flush.
	shutdown_delay: TimeUnit,				//Delay for shutdown.
	launch: bool,						//Ready for launch.
	launch_ack: bool,					//Launch acknowledged.
	service_fatal_eof: bool,				//EOF from service is fatal.
	pub rx_count: u64,					//Amount of data received from service.
	pub tx_count: u64,					//Amount of data sent to service.
	cipher_flags: u128,					//Ciphersuite offer flags.
	pub timeout_error: Rc<RefCell<Option<String>>>,		//Timeout error override.
}

impl<Service:ServiceTrait> ActiveCommon<Service>
{
	pub fn new(params: ActiveCommonParameters<Service>) -> ActiveCommon<Service>
	{
		ActiveCommon {
			global: params.global,
			service: params.service,
			fate_ok_ack: false,
			tcp: params.tcp,
			outgoing: TcpBuffer::new(),
			incoming: TcpBuffer::with_data(params.data),
			service_eof_seen: false,
			flush_delay: params.flush_delay,
			shutdown_delay: params.shutdown_delay,
			launch: false,
			launch_ack: false,
			service_fatal_eof: params.service_fatal_eof,
			rx_count: 0,
			tx_count: 0,
			cipher_flags: params.cipher_flags,
			timeout_error: Rc::new(RefCell::new(None)),
		}
	}
	pub fn get_timeout_error_cell(&self) -> Rc<RefCell<Option<String>>> { self.timeout_error.clone() }
	//The "service timeouts only" is ignored before there is a service.
	pub fn backend_timeouts_only(&self, need_service: bool) -> bool
	{
		(!need_service || self.launch_ack) && self.service_fatal_eof
	}
	pub fn shutdown_forced<RAII:ActiveRaiiTrait>(mut self, poll: &mut Poll, cause: Cow<'static, str>,
		raii: RAII) -> Connection2
	{
		//Move immediately to shut down state, shutting down the service and TCP.
		self.service.remove_pollers(poll);
		self.tcp.deregister_poll(poll);
		Connection2::Shutdown(Shutdown {
			tls_error: cause,
			global: self.global,
			raii: raii.to_shutdown(),
		})
	}
	//Perform immediate shutdown. This is only appropriate when encountering low-level fatal TCP errors.
	pub fn shutdown<T:Sized, RAII:ActiveRaiiTrait>(self, poll: &mut Poll, cause: Cow<'static, str>,
		raii: RAII) -> Result<Connection2, T>
	{
		Ok(self.shutdown_forced(poll, cause, raii))
	}
	//Perform shutdown after TLS error. This is appropriate after TLS indicates error status.
	pub fn handle_tls_error<RAII:ActiveRaiiTrait>(mut self, poll: &mut Poll, cause: Cow<'static, str>,
		tcp_eof_seen: bool, raii: RAII) -> Connection2
	{
		//Jettison the service.
		self.service.remove_pollers(poll);
		let s = ServiceFailed::new(
			cause,						//Cause
			self.tcp,					//TCP.
			self.outgoing,					//Outgoing buffer.
			PointInTime::from_now(self.flush_delay),	//DL for flush.
			tcp_eof_seen,					//TCP input failed.
			self.shutdown_delay,				//Shutdown delay.
			self.global,					//Global config.
			raii.to_service_failed()
		);
		//Perform tail processing for failed service.
		s.tail_processing(poll)
	}
	//Send a fake internal_error.
	pub fn fake_internal_error<RAII:ActiveRaiiTrait>(mut self, poll: &mut Poll, cause: Cow<'static, str>,
		tcp_eof_seen: bool, raii: RAII) -> Connection2
	{
		//Jettison the service.
		self.service.remove_pollers(poll);
		let tcpbuf = format_alert(80);
		let s = ServiceFailed::new(
			cause,							//Cause
			self.tcp,						//TCP.
			//Synthethize response internal_error.
			TcpBuffer::with_data(tcpbuf),				//Outgoing buffer.
			PointInTime::from_now(self.flush_delay),		//DL for flush.
			tcp_eof_seen,						//TCP input failed.
			self.shutdown_delay,					//Shutdown delay.
			self.global,						//Global config.
			raii.to_service_failed()
		);
		//Perform tail processing for failed service.
		s.tail_processing(poll)
	}
	pub fn set_service_eof_seen(&mut self)
	{
		self.service_eof_seen = true;
	}
	pub fn try_send_immediately(&mut self, data: &[u8]) -> Result<(), ErrorKind>
	{
		//If there is data in outgoing buffer, buffer this data, it can not be sent now.
		if self.outgoing.has_content() {
			self.outgoing.append(data);
			return Ok(())
		}
		//This is up next, try write...
		//Assume TcpState::write() never returns non-fatal errors.
		let x = self.tcp.write(data).map_err(ErrorKind::TcpWrite)?;
		self.global.metrics(|m|m.write_tcp(x));
		//This might not have written everything. Buffer the rest, it can not be sent right now.
		if let Some(data) = data.get(x..) { self.outgoing.append(data); }
		Ok(())
	}
	fn __handle_possible_eof(&mut self) -> Result<(), ErrorKind>
	{
		//If eof flag is set, do EOF.
		if !self.outgoing.has_content() && self.outgoing.is_eof() && !self.outgoing.is_acked_eof() {
			self.tcp.fd().shutdown_write().map_err(ErrorKind::TcpWrite)?;
			self.outgoing.ack_eof();
		}
		Ok(())
	}
	pub fn try_eof_immediately(&mut self) -> Result<(), ErrorKind>
	{
		self.outgoing.set_eof();
		self.__handle_possible_eof()
	}
	pub fn have_unsent_data(&self) -> bool { self.outgoing.has_content() }
	//Flush buffer to TCP.
	pub fn flush_buffers_to_tcp(&mut self) -> Result<(), ErrorKind>
	{
		self.__handle_possible_eof()?;
		//Assume TcpState::write() never returns non-fatal errors.
		let x = self.tcp.write(self.outgoing.contents()).map_err(ErrorKind::TcpWrite)?;
		self.global.metrics(|m|m.write_tcp(x));
		self.outgoing.pop(x);
		Ok(())
	}
	//Read from TCP.
	pub fn read_from_tcp<'a>(&mut self, buffer: &'a mut [u8]) -> Result<Option<&'a [u8]>, ErrorKind>
	{
		match self.tcp.fd().read(buffer) {
			Ok(0) => Ok(None),
			Ok(x) => {
				self.global.metrics(|m|m.read_tcp(x));
				Ok(Some(&buffer[..x]))
			},
			Err(e) => Err(ErrorKind::TcpRead(e))
		}
	}
	pub fn tcpinfo<'a>(&'a self) -> &'a ConnectionInfo
	{
		self.tcp.info()
	}
	pub fn is_tcp_event(&self, tok: usize) -> bool
	{
		self.tcp.is_tcp_interrupt(tok)
	}
	pub fn retrigger_polls(&mut self, poll: &mut Poll, tcp_eof_seen: bool) -> Result<(), ErrorKind>
	{
		//Retrigger polls. It is OK if this does not retrigger the TCP connection, as then wakeup by
		//service or txirq will retrigger it later.
		self.service.retrigger_service(poll);
		let mut tcpstatus = 0;
		//If there is data in queue, schedule write. If there is no data in queue, schedule read.
		if self.outgoing.has_content() { tcpstatus = tcpstatus | IO_WAIT_WRITE; }
		if !self.incoming.has_content() && !tcp_eof_seen { tcpstatus = tcpstatus | IO_WAIT_READ; }
		if tcpstatus == 0 && !tcp_eof_seen { tcpstatus = tcpstatus | IO_WAIT_HUP; }
		if tcpstatus != 0 {
			self.tcp.rearm_poll(poll, tcpstatus).map_err(|e|ErrorKind::RegisterFailed(e))?
		}
		Ok(())
	}
	pub fn do_service_out(&mut self, poll: &mut Poll, w: bool, fw: bool) -> Result<bool, ErrorKind>
	{
		let mut flag = false;
		if fw && !self.service.prefix_sent() {
			//Service force-write event to flush the headers.
			self.service.write_to_service(&[], poll).map_err(|e|ErrorKind::ServiceWrite(e))?;
		}
		if w && (self.incoming.has_content() || self.incoming.is_eof()) && !self.incoming.is_acked_eof() &&
			self.service.prefix_sent() {
			//Ok, service accepts data and we have data.
			if self.incoming.has_content() {
				let amt = self.service.write_to_service(self.incoming.contents(), poll).
					map_err(|e|ErrorKind::ServiceWrite(e))?;
				self.global.metrics(|m|m.write_service(amt));
				self.tx_count = self.tx_count.saturating_add(amt as u64);
				self.incoming.pop(amt);
			}
			if !self.incoming.has_content() && self.incoming.is_eof() {
				self.incoming.ack_eof();
				//Bump timeout.
				flag = true;
				//Specifically ignore any error from link close, as otherwise one gets a spurious
				//error if service closes its output first.
				self.service.close_link_to_service(poll).ok();
				//If the service stdin has been closed, then enter shutdown sequence.
				fail_if!(self.service_eof_seen, ErrorKind::NormalClose);
			}
		}
		Ok(flag)
	}
	pub fn service_set_error(&mut self, err: Cow<'static, str>)
	{
		self.service.set_error(err)
	}
	pub fn service_launch(&mut self, poll: &mut Poll, params: ServiceLaunchParameters, service: &LookedUpService,
		addtoken: &mut AddTokenCallback, extended: LaunchInfoExtended)
	{
		self.launch_ack = true;
		//let service2 = entry.preresolve_service(name);
		self.service.launch_service(poll, service, params, addtoken, Some(&extended));
		self.check_fate();
	}
	pub fn service_handle_event(&mut self, poll: &mut Poll, tok: usize, kind: u8)
	{
		self.service.handle_event(poll, tok, kind);
		self.check_fate();
	}
	pub fn service_should_launch(&self) -> bool
	{
		self.launch && !self.launch_ack
	}
	pub fn service_signal_launch(&mut self)
	{
		self.launch = true;
	}
	pub fn service_pending_shutdown(&self) -> bool
	{
		self.service.pending_shutdown()
	}
	pub fn service_get_error(&self) -> Cow<'static, str>
	{
		self.service.get_error()
	}
	pub fn service_is_access_denied(&self) -> bool
	{
		self.service.is_access_denied()
	}
	pub fn service_rendezvous_request(&self) -> (bool, bool, bool)
	{
		self.service.rendezvous_request()
	}
	pub fn service_read_from(&mut self, buf: &mut [u8]) -> Result<Option<usize>, Cow<'static, str>>
	{
		let r = self.service.read_from_service(buf);
		if let Ok(Some(r)) = r {
			self.rx_count = self.rx_count.saturating_add(r as u64);
			self.global.metrics(|m|m.read_service(r));
		}
		r
	}
	fn check_fate(&mut self)
	{
		let cfl = self.cipher_flags;
		if !self.fate_ok_ack {
			let f = self.service.fate();
			self.global.metrics(|gm|match f {
				Some(LaunchFate::Successful) => {
					//Do cipher flags on launch.
					for i in 0..128 { if cfl & 1 << i != 0 { gm.cipher_flags(i+128); }}
					gm.launch_successful();
				},
				Some(LaunchFate::FailedNoTarget) => gm.launch_failed_no_target(),
				Some(LaunchFate::FailedError) => gm.launch_failed_error(),
				Some(LaunchFate::FailedAccessDenied) => gm.launch_failed_denied(),
				None => (),
			});
			self.fate_ok_ack = f.is_some();
		}
	}
}
