#![warn(unsafe_op_in_unsafe_fn)]
#[macro_use] extern crate btls_aux_fail;
use crate::acme::AcmeService;
pub use crate::acme::AcmeServiceParams;
pub use crate::acme::BuildHostkeyLookup;
pub use crate::acme::HostkeyLookup;
use crate::active::ActiveConnection;
use crate::conninit::InitializingConnection;
pub use crate::conninit::WritableFrozenIpSet;
use crate::cutthrough::CutThrough;
use crate::legacy_service::ConnectionService as ConnectionServiceT;
pub use crate::metrics_dp::GlobalMetrics;
pub use crate::services_dp::BackendMapT;
pub use crate::services_dp::ExtendedPort;
pub use crate::services_dp::ExtendedPortBorrow;
pub use crate::services_dp::FdConnector;
pub use crate::services_dp::FdConnectorBox;
pub use crate::services_dp::LaunchInfoExtended;
pub use crate::services_dp::ParseOldHost;
pub use crate::services_dp::ParseNewHost;
pub use crate::services_dp::ParsingNameProperties;
pub use crate::services_dp::ParsingServiceProperties;
pub use crate::services_dp::ParsingServiceTarget;
pub use crate::services_dp::ServiceCacheName;
pub use crate::services_dp::ServiceName;
use crate::shutdown::ServiceFailed;
use crate::shutdown::Shutdown;
use crate::shutdown::ShutdownHold;
pub use crate::tls::DebugLogger;
pub use crate::tls::dlog_set_directory;
pub use crate::tls::SFLAG_SECURE192;
pub use crate::tls::SFLAG_SPINAL_TAP;
pub use crate::tls::SFLAG_TLS13_ONLY;
pub use crate::tls::SFLAG_NO_ANDROID_HACK;
pub use crate::tls::SFLAG_NO_LOG;
pub use crate::tls::SFLAG_NSA_MODE;
use btls::ServerConfiguration;
use btls::callbacks::PointInTime;
use btls::callbacks::TimeUnit;
use btls::utility::RwLock;
use btls_aux_filename::Filename;
use btls_aux_unix::MmapFlags;
use btls_aux_unix::MmapProtection;
use btls_aux_unix::OpenFlags;
use btls_daemon_helper_lib::AddTokenCallback;
use btls_daemon_helper_lib::AllocatedToken;
use btls_daemon_helper_lib::cloned;
use btls_daemon_helper_lib::ConnectionInfo;
use btls_daemon_helper_lib::ConnectionInfoAddress;
use btls_daemon_helper_lib::cow;
use btls_daemon_helper_lib::FileDescriptor;
use btls_daemon_helper_lib::Poll;
use btls_daemon_helper_lib::ProxyConfiguration;
use btls_daemon_helper_lib::SocketAddrEx;
use btls_daemon_helper_lib::TokenAllocatorPool;
use btls_daemon_helper_lib::UpperLayerConnection;
use btls_daemon_helper_lib::UpperLayerConnectionFactory;
use btls_util_logging::ConnId;
use btls_util_logging::syslog;
use std::borrow::Cow;
use std::collections::HashMap;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::mem::replace;
use std::mem::transmute;
use std::net::IpAddr;
use std::net::Ipv4Addr;
use std::net::Ipv6Addr;
use std::ops::Deref;
use std::path::Path;
use std::ptr::null_mut;
use std::sync::Arc;
use std::time::Duration;

#[macro_use] mod common;
mod acme;
mod active;
mod conninit;
mod crypto_props;
mod cutthrough;
mod earlydata;
pub mod legacy_service;
mod metrics_dp;
mod services_dp;
mod shutdown;
mod tcp;
mod tls;

type ConnectionService = ConnectionServiceT;

#[derive(Copy,Clone)]
pub enum ParseChSubErr
{
	//The client hello has been truncated.
	Truncated,
	//The client hello should have protocol of TLS handshake protocol.
	BadProtocol,
	//The client hello should have record major version 3.
	BadRecordMajorVersion,
	//The client hello should have record minor version 0-3.
	BadRecordMinorVersion,
	//The client hello record size is unexpected.
	BadChRecordSize,
	//The client hello is not actually client hello.
	NotClientHello,
	//The client hello message size is unexpected.
	BadChMessageSize,
	//Bad TLS version in client hello (too old client)
	BadTlsVersion(u16),
	//Junk after end of client hello.
	JunkAfterEnd,
	//Extension truncated in client hello.
	ExtensionTruncated,
	//Parse error in SNI extension.
	SniParseError,
	//DNS name too long in SNI extension.
	DnsNameTooLong,
	//Bad DNS name in SNI extension.
	BadDnsName,
	//Reverse DNS name is bad.
	InvalidReverseDns,
	//Reverse DNS name does not match target.
	ReverseDnsMismatch,
	//Parse error in ALPN extension.
	AlpnParseError,
}

impl Display for ParseChSubErr
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::ParseChSubErr::*;
		match *self {
			Truncated => fmt.write_str("Client hello truncated"),
			BadProtocol => fmt.write_str("Bad TLS protocol"),
			BadRecordMajorVersion => fmt.write_str("Bad record major version"),
			BadRecordMinorVersion => fmt.write_str("Bad record minor version"),
			BadChRecordSize => fmt.write_str("Bad client hello record size"),
			NotClientHello => fmt.write_str("Not client hello"),
			BadChMessageSize => fmt.write_str("Bad client hello message size"),
			BadTlsVersion(v) => return write!(fmt, "Bad TLS version {major}.{minor}",
				major=(v >> 8) as i16 - 2, minor=(v & 255) as i16 - 1),
			JunkAfterEnd => fmt.write_str("Junk after end of client hello"),
			ExtensionTruncated => fmt.write_str("Client hello extension truncated"),
			SniParseError => fmt.write_str("SNI parse error"),
			DnsNameTooLong => fmt.write_str("DNS name too long"),
			BadDnsName => fmt.write_str("Bad DNS name"),
			InvalidReverseDns => fmt.write_str("Invalid reverse DNS name"),
			ReverseDnsMismatch => fmt.write_str("Mismatching reverse DNS"),
			AlpnParseError => fmt.write_str("ALPN parse error"),
		}
	}
}

pub enum ParseSniAlpnError
{
	BadClientHello(ParseChSubErr),
	NoHandlerFound(String),
	BadService(Cow<'static, str>),
	AlpnFail(crate::services_dp::BadService),
}

fn format_alert(code: u8) -> Vec<u8> { vec![21, 3, 3, 0, 2, 2, code] }

fn connect_address_as_ip(addr: &ConnectionInfoAddress) -> Option<IpAddr>
{
	match addr {
		&ConnectionInfoAddress::V4(ref x, _) => {
			fail_if_none!(x == &[0u8;4]);		//Unspecified.
			Some(IpAddr::V4(Ipv4Addr::from(x.clone())))
		},
		&ConnectionInfoAddress::V6(ref x, _) => {
			fail_if_none!(x == &[0u8;16]);		//Unspecified.
			Some(IpAddr::V6(Ipv6Addr::from(x.clone())))
		},
		_ => None,	//Not IP.
	}
}

pub struct ConnectionFactory2
{
	pub config: ServerConfiguration,
	pub global: ConfigT,
}

impl UpperLayerConnectionFactory for ConnectionFactory2
{
	fn new(&self, poll: &mut Poll, allocator: &mut TokenAllocatorPool, fd: FileDescriptor,
		info: ConnectionInfo) ->
		Result<(Box<dyn UpperLayerConnection>, Vec<AllocatedToken>, bool), Cow<'static, str>>
	{
		let (c, t) = InitializingConnection::new(poll, allocator, &self.config, fd, info,
			self.global)?;
		//The address valid flag is always false because some connections do not get K-Lined. The
		//source address is set later if necressary.
		Ok((Box::new(c), t, false))
	}
	fn remap(&self, addr: SocketAddrEx) -> SocketAddrEx
	{
		self.global.with(|g|g.dremap_table.get(&addr).map(cloned)).unwrap_or(addr)
	}
}

pub enum Connection2
{
	Initializing(InitializingConnection),
	Acme(ActiveConnection<AcmeService>),
	Active(ActiveConnection<ConnectionService>),
	CutThrough(CutThrough<ConnectionService>),
	ServiceFailed(ServiceFailed),
	ShutdownHold(ShutdownHold),
	Shutdown(Shutdown),
	Error,
}

impl Connection2
{
	fn __hard_error_or_ok(&self) -> Result<(), Cow<'static, str>>
	{
		//If in either of these two states, shut down.
		match self {
			&Connection2::Shutdown(ref s) => Err(s.tls_error.clone()),
			&Connection2::Error => Err(cow!("Connection internal error")),
			_ => Ok(())
		}
	}
}

impl UpperLayerConnection for Connection2
{
	//Handle a timed event (return Err(()) if connection should be killed). Might panic.
	fn handle_timed_event(&mut self, now: PointInTime, poll: &mut Poll) -> Result<(), Cow<'static, str>>
	{
		let state = replace(self, Connection2::Error);
		*self = match state {
			Connection2::Initializing(c) => c.handle_timed_event(now, poll),
			Connection2::Acme(c) => c.handle_timed_event(now, poll),
			Connection2::Active(c) => c.handle_timed_event(now, poll),
			Connection2::CutThrough(c) => c.handle_timed_event(now, poll),
			Connection2::ServiceFailed(c) => c.handle_timed_event(now, poll),
			Connection2::ShutdownHold(c) => c.handle_timed_event(now, poll),
			Connection2::Shutdown(c) => Connection2::Shutdown(c),
			Connection2::Error => Connection2::Error,
		};
		self.__hard_error_or_ok()
	}
	//Handle a socket event (return Err(()) if connection should be killed). Might panic.
	fn handle_event(&mut self, poll: &mut Poll, tok: usize, kind: u8, _: ConnId,
		addtoken: &mut AddTokenCallback) -> Result<(), Cow<'static, str>>
	{
		let state = replace(self, Connection2::Error);
		*self = match state {
			Connection2::Initializing(c) => c.handle_event(poll, tok, kind, addtoken),
			Connection2::Acme(c) => c.handle_event(poll, tok, kind, addtoken),
			Connection2::Active(c) => c.handle_event(poll, tok, kind, addtoken),
			Connection2::CutThrough(c) => c.handle_event(poll, tok, kind, addtoken),
			Connection2::ServiceFailed(c) => c.handle_event(poll, tok, kind),
			Connection2::ShutdownHold(c) => c.handle_event(poll, tok, kind),
			Connection2::Shutdown(c) => Connection2::Shutdown(c),
			Connection2::Error => Connection2::Error,
		};
		self.__hard_error_or_ok()
	}
	fn handle_fault(&mut self, poll: &mut Poll) -> Option<Cow<'static, str>>
	{
		let state = replace(self, Connection2::Error);
		*self = match state {
			Connection2::Initializing(c) => c.handle_fault(poll),
			Connection2::Acme(c) => c.handle_fault(poll),
			Connection2::Active(c) => c.handle_fault(poll),
			Connection2::CutThrough(c) => c.handle_fault(poll),
			Connection2::ServiceFailed(c) => c.handle_fault(poll),
			Connection2::ShutdownHold(c) => c.handle_fault(poll),
			Connection2::Shutdown(c) => Connection2::Shutdown(c),
			Connection2::Error => Connection2::Error,
		};
		self.__hard_error_or_ok().err()
	}
	//Remove the pollers. Must not panic, including with poisoned locks.
	fn remove_pollers(&mut self, _poll: &mut Poll)
	{
		//Since we only ever signal to dump the connection in shutdown, we never need to remove any
		//pollers on shutdown.
	}
	//Get next timed event. Called early and after calling handle_timed_event() or handle_event().
	fn get_next_timed_event(&self, now: PointInTime) -> Option<PointInTime>
	{
		match self {
			&Connection2::Initializing(ref c) => c.get_next_timed_event(now),
			&Connection2::Acme(ref c) => c.get_next_timed_event(now),
			&Connection2::Active(ref c) => c.get_next_timed_event(now),
			&Connection2::CutThrough(ref c) => c.get_next_timed_event(now),
			&Connection2::ServiceFailed(ref c) => c.get_next_timed_event(now),
			&Connection2::ShutdownHold(ref c) => c.get_next_timed_event(now),
			&Connection2::Shutdown(_) => return Some(now),
			&Connection2::Error => return Some(now),
		}
	}
	fn get_main_fd(&self) -> i32
	{
		//This is only supposed to be called early on, and then the connection is always in Initializing
		//state.
		if let &Connection2::Initializing(ref s) = self { s.get_main_fd() } else { -1 }
	}
}

#[derive(Clone)]
pub struct GlobalConfiguration
{
	pub proxy_config: ProxyConfiguration,
	pub special_acme_target: Option<SocketAddrEx>,
	pub special_acme_config: Option<AcmeServiceParams>,
	pub flush_delay: TimeUnit,
	pub shutdown_delay: TimeUnit,
	pub trans_timeout: Option<Duration>,
	pub handshake_timeout: TimeUnit,
	pub scache: ServiceCacheName,
	pub debug: bool,
	pub allow_nct_wildcard: bool,
	pub stats_blacklist: WritableFrozenIpSet,
	pub dremap_table: HashMap<SocketAddrEx, SocketAddrEx>,
}

//HACK: Force Sync on GlobalConfigurationWrapper. RwLock for some reason will not give Sync without inner data
//being sync, but Mutex will.
unsafe impl std::marker::Sync for GlobalConfigurationWrapper {}

pub struct GlobalConfigurationWrapper(Arc<RwLock<GlobalConfiguration>>, Arc<GlobalMetrics>);

impl GlobalConfigurationWrapper
{
	pub fn new(config: GlobalConfiguration, metrics: Arc<GlobalMetrics>) -> GlobalConfigurationWrapper
	{
		GlobalConfigurationWrapper(Arc::new(RwLock::new(config)), metrics)
	}
	pub fn with<T,F>(&self, f: F) -> T where F: FnOnce(&GlobalConfiguration) -> T
	{
		f(self.0.read().deref())
	}
	pub fn metrics<T,F>(&self, f: F) -> T where F: FnOnce(&GlobalMetrics) -> T
	{
		f(self.1.deref())
	}
	pub fn metrics_arc(&self) -> Arc<GlobalMetrics>
	{
		self.1.clone()
	}
}

pub type ConfigT = &'static GlobalConfigurationWrapper;

pub fn set_cipherstats_file(path: &Path, config: &'static GlobalConfigurationWrapper)
{
	let ppath = path.display();
	//Assume that the cipherstats file already exists and is of suitable size.
	let path = match Filename::from_path(path).into_unix_path() {
		Some(x) => x,
		None => return syslog!(WARNING "Invalid cipherstats file {ppath}")
	};
	let fp = match path.open(OpenFlags::O_RDWR|OpenFlags::O_CLOEXEC) {
		Ok(x) => x,
		Err(err) => return syslog!(WARNING "Failed to open cipherstats file {ppath}: {err}")
	};
	let ptr = match unsafe{fp.mmap(null_mut(), 4096, MmapProtection::READ|MmapProtection::WRITE,
		MmapFlags::SHARED, 0)} {
		Ok(x) => x,
		Err(err) => return syslog!(WARNING "Failed to mmap cipherstats file {ppath}: {err}")
	};
	//NULL address is not valid, so mmap can not place anything there.
	unsafe { config.metrics(|m|m.set_secondary_ciphermetrics(transmute(ptr))); }
	syslog!(INFO "Cipherstats file set to {ppath}");
}


include!(concat!(env!("OUT_DIR"), "/product.inc"));

pub fn get_product_name() -> &'static str { PRODUCT_NAME }
pub fn get_product_version() -> &'static str { PRODUCT_VERSION }
