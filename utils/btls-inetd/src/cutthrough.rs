use crate::Connection2;
use crate::ConnectionService;
use crate::common::ErrorKind;
use crate::common::ActiveCommon;
use crate::common::ActiveCommonParameters;
use crate::common::ServiceTrait;
use crate::metrics_dp::RaiiCutThrough;
use btls::callbacks::PointInTime;
use btls::callbacks::TimeUnit;
use btls_daemon_helper_lib::AddTokenCallback;
use btls_daemon_helper_lib::cow;
use btls_daemon_helper_lib::IO_WAIT_READ;
use btls_daemon_helper_lib::IO_WAIT_WRITE;
use btls_daemon_helper_lib::Poll;
use std::borrow::Cow;
use std::time::Duration;


//Fully active connection. Service state, TLS state and TCP state all exist.
pub struct CutThrough<Service:ServiceTrait>
{
	common: ActiveCommon<Service>,		//Common stuff.
	service_sent_data: bool,		//Service has sent _any_ data.
	timeout_pit: Option<PointInTime>,	//The currrent connection timeout.
	trans_to: Option<Duration>,		//Transfer timeout.
	raii: RaiiCutThrough,
}

impl<Service:ServiceTrait> CutThrough<Service>
{
	pub fn new(cparams: ActiveCommonParameters<Service>, timeout_pit: Option<PointInTime>,
		trans_to: Option<Duration>, raii: RaiiCutThrough) -> CutThrough<Service>
	{
		CutThrough {
			common: ActiveCommon::new(cparams),
			service_sent_data: false,
			timeout_pit: timeout_pit,
			trans_to: trans_to,
			raii: raii,
		}
	}
	//Perform shutdown after TLS error. This is appropriate after TLS indicates error status.
	fn handle_tls_error(self, poll: &mut Poll, cause: Cow<'static, str>) -> Result<Connection2, Self>
	{
		let e = self.common.incoming.is_eof();
		let raii = self.raii;
		Ok(self.common.handle_tls_error(poll, cause, e, raii))
	}
	//Alias for handle_tls_error. Code is never interesting.
	fn service_failed(self, poll: &mut Poll, cause: Cow<'static, str>, _: u8) ->
		Result<Connection2, Self>
	{
		let e = self.common.incoming.is_eof();
		let raii = self.raii;
		Ok(self.common.handle_tls_error(poll, cause, e, raii))
	}
	//Send a fake internal_error.
	fn fake_internal_error(self, poll: &mut Poll, cause: Cow<'static, str>) ->
		Result<Connection2, Self>
	{
		let e = self.common.incoming.is_eof();
		let raii = self.raii;
		Ok(self.common.fake_internal_error(poll, cause, e, raii))
	}
	fn handle_timed_event_inner(self, poll: &mut Poll, _now: PointInTime) -> Result<Connection2, Self>
	{
		//If service fatal EOF is set, connection never timeouts from active state.
		if self.common.backend_timeouts_only(true) { return Err(self); }
		if let Some(timeout_pit) = self.timeout_pit {
			if timeout_pit.in_past() {
				self.common.global.metrics(|m|m.timeout_error());
				let raii = self.raii;
				return self.common.shutdown(poll, cow!("Connection timed out"), raii);
			}
		}
		Err(self)
	}
	pub fn get_next_timed_event(&self, _now: PointInTime) -> Option<PointInTime>
	{
		//If service fatal EOF is set, connection never timeouts from active state.
		fail_if_none!(self.common.backend_timeouts_only(true));
		self.timeout_pit
	}
	fn handle_data_from_tcp(&mut self, buffer: &[u8]) -> Result<(), ErrorKind>
	{
		reserve_commit!(self.common.incoming, buffer.len(), |ptr|{
			self.common.incoming.mutably_borrow_after(ptr).copy_from_slice(buffer);
			//If olen != 0, Bump secondary timeout.
			if buffer.len() != 0 {
				self.timeout_pit = self.trans_to.
					map(|tp|PointInTime::from_now(TimeUnit::Duration(tp)));
			}
			Ok(buffer.len())
		})
	}
	fn handle_data_from_service(&mut self, buffer: &[u8]) -> Result<(), ErrorKind>
	{
		//Bump secondary timeout if buffer is not empty. Also, the service has now sent data.
		if buffer.len() == 0 { return Ok(()); }
		self.timeout_pit = self.trans_to.map(|tp|PointInTime::from_now(TimeUnit::Duration(tp)));
		self.service_sent_data = true;
		self.common.try_send_immediately(buffer)?;
		Ok(())
	}
	fn handle_eof_from_service(&mut self) -> Result<(), ErrorKind>
	{
		//Bump secondary timeout.
		self.timeout_pit = self.trans_to.map(|tp|PointInTime::from_now(TimeUnit::Duration(tp)));
		self.common.set_service_eof_seen();
		//Special case: If no data has been received from the service, fake an internal_error alert.
		if !self.service_sent_data { return Err(ErrorKind::CutThroughFailed); }
		self.common.try_eof_immediately()?;
		//If incoming EOF (on TCP or TLS level) has been seen, shut down the connection.
		if self.common.incoming.is_acked_eof() { return Err(ErrorKind::NormalClose); }
		//If service fatal EOF is set, also shut down the connection.
		fail_if!(self.common.backend_timeouts_only(false), ErrorKind::ServiceTimeoutEof);
		Ok(())
	}
	fn handle_event_inner(mut self, poll: &mut Poll, tok: usize, kind: u8,
		_: &mut AddTokenCallback) -> Result<Connection2, Self>
	{
		self.common.service_handle_event(poll, tok, kind);		//Handle service events.
		if kind & IO_WAIT_WRITE != 0 && self.common.is_tcp_event(tok) {
			//The only thing done with TCP write events is flushing buffers.
			handle_fault!(self.common.flush_buffers_to_tcp(), self, poll);
		}
		if kind & IO_WAIT_READ != 0 && self.common.is_tcp_event(tok) && !self.common.incoming.is_eof() {
			let mut buffer = [0;16700];
			let buffer = handle_fault!(self.common.read_from_tcp(&mut buffer), self, poll);
			handle_fault!(if let Some(buffer) = buffer {
				self.handle_data_from_tcp(buffer)
			} else {
				Ok(self.common.incoming.set_eof())
			}, self, poll);
		}
		//If service is in error state, do service lost.
		if self.common.service_pending_shutdown() {
			//Special case: If and no data has been received from the service, fake a internal_error
			//alert.
			if !self.service_sent_data {
				handle_fault!(Err(ErrorKind::CutThroughFailedWith(self.common.service_get_error())),
					self, poll);
			}
			handle_fault!(Err(ErrorKind::ServiceShutdown(self.common.service_get_error())), self, poll);
		}

		//Perform I/O with service.
		let (w, fw, r) = self.common.service_rendezvous_request();
		if r && !self.common.have_unsent_data() {
			//Read from service, push to TLS. Note that this can only be reached once with closed service
			//stdout. handle_data_from_service and handle_eof_from_service both queue any sends if
			//possible, so no need to call flush_buffers_to_tcp.
			let mut buffer = [0;16384];
			handle_fault!(match self.common.service_read_from(&mut buffer) {
				Ok(Some(x)) => self.handle_data_from_service(&buffer[..x]),
				Ok(None) => self.handle_eof_from_service(),
				Err(err) => Err(ErrorKind::ServiceRead(err)),
			}, self, poll);
		}
		//Perform output to service.
		if handle_fault!(self.common.do_service_out(poll, w, fw), self, poll) {
			self.timeout_pit = self.trans_to.map(|tp|PointInTime::from_now(TimeUnit::Duration(tp)));
		}

		let e = self.common.incoming.is_eof();
		handle_fault!(self.common.retrigger_polls(poll, e), self, poll);
		Err(self)
	}
}

define_active_special!(CutThrough<ConnectionService>, |y|Connection2::CutThrough(y));
