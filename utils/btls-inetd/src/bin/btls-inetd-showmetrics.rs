extern crate btls_daemon_helper_lib;
use btls_aux_fail::f_break;
use btls_daemon_helper_lib::bind_metrics_socket;
use btls_daemon_helper_lib::MetricsArrayEntry;
use btls_daemon_helper_lib::MetricsMap;
use btls_daemon_helper_lib::PerThreadMetrics;
use btls_daemon_helper_lib::PrintUi;
use btls_daemon_helper_lib::PrintUptime;
use btls_daemon_helper_lib::receive_metrics_from;
use btls_daemon_helper_lib::TrackCpuUsage;
use std::io::stderr;
use std::io::Write;
use std::time::Duration;


const GMETRIC_UPTIME: u32 = 30;

macro_rules! define_tls_gmetrics
{
	($([$value:tt $symbol:ident $variable:ident];)*) => {
		$(pub const $symbol: u32 = $value;)*
	}
}

//Define the metrics.
include!("../metrics.inc");

macro_rules! show
{
	($ui:expr, $($args:tt)*) => { $ui.show(format_args!($($args)*)) }
}

macro_rules! single
{
	($ui:expr, $($args:tt)*) => { $ui.single(format_args!($($args)*)) }
}

fn do_print_ui(ui: &mut PrintUi, cpu: &mut TrackCpuUsage, cflags: &[MetricsArrayEntry;74], arr: &MetricsMap,
	ts: Duration)
{
	let [
		c_verybad, c_blacklist, c_grease, c_priv, c_reserved,				//0-4
		c_unassigned, c_scsv, c_kex_srsa, c_kex_sdh, c_kex_psk,				//5-9
		c_kex_dh, c_kex_ecdh, c_kex_krb5, c_kex_srp6a, c_kex_dfly,			//10-14
		c_auth_null, c_auth_psk, c_auth_rsasig, c_auth_dss, c_auth_ecdsa,		//15-19
		c_mac_md5, c_mac_64, c_enc_null, c_enc_rc4, c_enc_idea,				//20-24
		c_enc_3des, c_enc_aescbc, c_enc_camcbc, c_enc_seed, c_enc_aesgcm,		//25-29
		c_enc_ariacbc, c_enc_ariagcm, c_enc_camgcm, c_enc_aesccm, c_enc_chacha20,	//30-34
		c_enc_sm4, c_enc_kuz, c_enc_magma, c_grp_bcrv, c_grp_p256,			//35-39
		c_grp_p384, c_grp_p521, c_grp_bpool, c_grp_25519, c_grp_448,			//40-44
		c_grp_gost, c_grp_sm2, c_grp_ff, c_grp_cecc, c_sig_md5,				//45-49
		c_sig_sha1, c_sig_sha224, c_sig_rsa, c_sig_dss, c_sig_ecdsa,			//50-54
		c_sig_gost, c_sig_rsapss, c_sig_sm3, c_sig_ibc, c_sig_25519,			//55-59
		c_sig_448, c_export, _c_total, c_kex_tls13, c_kex_gost,				//60-64
		c_modernc, c_moderng, c_moderns, c_enc_oldchacha, _c_total2, 			//65-69
		c_enc_aegis, c_total3, c_pq, c_total4,						//70-73
	] = cflags;
	//Global metrics.
	let g_tcp_read = arr.read(GMETRIC_TCP_READ);
	let g_tcp_write = arr.read(GMETRIC_TCP_WRITE);
	let g_serv_read = arr.read(GMETRIC_SERV_READ);
	let g_serv_write = arr.read(GMETRIC_SERV_WRITE);
	let g_in_acc = arr.read(GMETRIC_CONN_ACCEPT);
	let g_in_comm = arr.read(GMETRIC_CONN_COMMIT);
	let g_in_ret = arr.read(GMETRIC_CONN_RETIRE);
	let g_in_uncomm = g_in_acc - g_in_comm;
	let g_in_act = g_in_comm - g_in_ret;
	let g_conn_decode = arr.read(GMETRIC_ACTIVE_TRANSITIONS);
	let g_conn_cut = arr.read(GMETRIC_CT_TRANSITIONS);
	let g_conn_acme = arr.read(GMETRIC_REDIRECTED_ACMES);
	let g_lf_ok = arr.read(GMETRIC_LFATE_SUCCESS);
	let g_lf_conferr = arr.read(GMETRIC_LFATE_CONFIGERR);
	let g_lf_connerr = arr.read(GMETRIC_LFATE_CONNECTERR);
	let g_lf_deny = arr.read(GMETRIC_LFATE_DENIED);
	let g_lf_unk = arr.read(GMETRIC_LFATE_UNKNOWN);
	let g_st_init = arr.read(GMETRIC_STATE_INIT);
	let g_st_act = arr.read(GMETRIC_STATE_ACTIVE);
	let g_st_cut = arr.read(GMETRIC_STATE_CUT_THROUGH);
	let g_st_fail = arr.read(GMETRIC_STATE_SERVICE_FAILED);
	let g_st_shold = arr.read(GMETRIC_STATE_SHUTDOWN_HOLD);
	let g_st_dead = arr.read(GMETRIC_STATE_SHUTDOWN);
	let g_fa_normal = arr.read(GMETRIC_NORMAL_CLOSES);
	let g_fa_trunc = arr.read(GMETRIC_INSECURE_TRUNCATIONS);
	let g_fa_serr = arr.read(GMETRIC_SERVICE_ERRORS);
	let g_fa_nodata = arr.read(GMETRIC_SERVICE_ERROR_NODATAS);
	let g_fa_perr = arr.read(GMETRIC_POLL_ERRORS);
	let g_fa_transerr = arr.read(GMETRIC_TRANSPORT_ERRORS);
	let g_fa_tlserr = arr.read(GMETRIC_TLS_ERRORS);
	let g_fa_toerr = arr.read(GMETRIC_TIMEOUT_ERRORS);
	let g_fa_launcherr = arr.read(GMETRIC_SERVICE_LAUNCH_ERRORS);
	let g_fa_nohand = arr.read(GMETRIC_NO_HANDLER);
	let g_fa_fhand = arr.read(GMETRIC_FAULTY_HANDLER);
	let g_fa_mserv = arr.read(GMETRIC_MISSING_SERVICE);
	let g_fa_cherr = arr.read(GMETRIC_CH_ERROR);
	let g_fa_initerr = arr.read(GMETRIC_INITIALIZE_ERRORS);
	let g_fa_initerr = g_fa_initerr - g_fa_nohand - g_fa_fhand - g_fa_cherr - g_fa_mserv;
	let g_badcipher = arr.read(GMETRIC_BAD_CIPHER);
	let g_cpq = arr.read(GMETRIC_COMMIT_PQ);


	ui.rewind();
	let tss = ts.subsec_nanos();
	let ts = ts.as_secs();
	let uptime = PrintUptime(arr.get(GMETRIC_UPTIME));
	show!(ui, "Received at: {ts}.{tss:09} (Uptime: {uptime})");

	ui.label("Bytes");
	single!(ui, "In: {g_tcp_read}->{g_serv_write}");
	single!(ui, "Out: {g_serv_read}->{g_tcp_write}");

	show!(ui, "Incoming: {g_in_acc}=({g_in_uncomm} uncommit + {g_in_act} active + {g_in_ret} dead)");

	ui.label("Connections");
	single!(ui, "{g_conn_decode} Decode");
	single!(ui, "{g_conn_cut} CutThrough");
	single!(ui, "{g_conn_acme} ACME");

	ui.label("Launches");
	single!(ui, "{g_lf_ok} Ok");
	single!(ui, "{g_lf_conferr} Conferr");
	single!(ui, "{g_lf_connerr} ConnErr");
	single!(ui, "{g_lf_deny} Deny");
	single!(ui, "{g_lf_unk} ???");

	ui.label("States");
	single!(ui, "{g_st_init} Init");
	single!(ui, "{g_st_act} Dec");
	single!(ui, "{g_st_cut} Cut");
	single!(ui, "{g_st_fail} Fail");
	single!(ui, "{g_st_shold} Hold");
	single!(ui, "{g_st_dead} Dead");

	ui.label("Fate");
	single!(ui, "{g_fa_normal} Close");
	single!(ui, "{g_fa_trunc} Trunc");
	single!(ui, "{g_fa_serr} SFail");
	single!(ui, "{g_fa_nodata} SFailND");
	single!(ui, "{g_fa_perr} PollErr");
	single!(ui, "{g_fa_transerr} TCP");
	single!(ui, "{g_fa_tlserr} TLS");
	single!(ui, "{g_fa_toerr} Tout");
	single!(ui, "{g_fa_launcherr} LaunchErr");
	single!(ui, "{g_fa_initerr} InitErr");
	single!(ui, "{g_fa_nohand} NoHnd");
	single!(ui, "{g_fa_fhand} BadHnd");
	single!(ui, "{g_fa_mserv} NoServ");
	single!(ui, "{g_fa_cherr} BadCh");

	show!(ui, "Badcipher: {g_badcipher}");

	ui.label("eoffer");
	single!(ui, "N:{c_enc_null}");
	single!(ui, "EX:{c_export}");
	single!(ui, "RC4:{c_enc_rc4}");
	single!(ui, "IDEA:{c_enc_idea}");
	single!(ui, "3D:{c_enc_3des}");
	single!(ui, "AES:{c_enc_aescbc}/{c_enc_aesgcm}/{c_enc_aesccm}");
	single!(ui, "CAM:{c_enc_camcbc}/{c_enc_camgcm}");
	single!(ui, "ARIA:{c_enc_ariacbc}/{c_enc_ariagcm}");
	single!(ui, "SEED:{c_enc_seed}");
	single!(ui, "CHA:{c_enc_chacha20}");
	single!(ui, "SM4:{c_enc_sm4}");
	single!(ui, "KZ:{c_enc_kuz}");
	single!(ui, "MA:{c_enc_magma}");
	single!(ui, "OCHA: {c_enc_oldchacha}");
	single!(ui, "AEGIS:{c_enc_aegis}");

	ui.label("koffer");
	single!(ui, "rsa:{c_kex_srsa}");
	single!(ui, "sdh:{c_kex_sdh}");
	single!(ui, "psk:{c_kex_psk}");
	single!(ui, "dh:{c_kex_dh}");
	single!(ui, "ecdh:{c_kex_ecdh}");
	single!(ui, "krb5:{c_kex_krb5}");
	single!(ui, "srp:{c_kex_srp6a}");
	single!(ui, "df:{c_kex_dfly}");
	single!(ui, "gost:{c_kex_gost}");
	single!(ui, "tls13:{c_kex_tls13}");

	ui.label("aoffer");
	single!(ui, "anon:{c_auth_null}");
	single!(ui, "psk:{c_auth_psk}");
	single!(ui, "rsa:{c_auth_rsasig}");
	single!(ui, "dss:{c_auth_dss}");
	single!(ui, "ecdsa:{c_auth_ecdsa}");

	ui.label("goffer");
	single!(ui, "b:{c_grp_bcrv}");
	single!(ui, "256:{c_grp_p256}");
	single!(ui, "384:{c_grp_p384}");
	single!(ui, "521:{c_grp_p521}");
	single!(ui, "25519:{c_grp_25519}");
	single!(ui, "448:{c_grp_448}");
	single!(ui, "bp:{c_grp_bpool}");
	single!(ui, "gost:{c_grp_gost}");
	single!(ui, "sm2:{c_grp_sm2}");
	single!(ui, "ff:{c_grp_ff}");
	single!(ui, "cus:{c_grp_cecc}");
	single!(ui, "pqc:{c_pq}");

	ui.label("soffer");
	single!(ui, "rsa:{c_sig_rsa}");
	single!(ui, "rsap:{c_sig_rsapss}");
	single!(ui, "dss:{c_sig_dss}");
	single!(ui, "ecdsa:{c_sig_ecdsa}");
	single!(ui, "25519:{c_sig_25519}");
	single!(ui, "448:{c_sig_448}");
	single!(ui, "gost:{c_sig_gost}");
	single!(ui, "sm3:{c_sig_sm3}");
	single!(ui, "ibc:{c_sig_ibc}");
	single!(ui, "md5:{c_sig_md5}");
	single!(ui, "sha1:{c_sig_sha1}");
	single!(ui, "sha224:{c_sig_sha224}");

	ui.label("xoffer");
	single!(ui, "grease:{c_grease}");
	single!(ui, "priv:{c_priv}");
	single!(ui, "resv:{c_reserved}");
	single!(ui, "ukwn:{c_unassigned}");
	single!(ui, "scsv:{c_scsv}");
	single!(ui, "mmd5:{c_mac_md5}");
	single!(ui, "m64:{c_mac_64}");

	ui.label("modern");
	single!(ui, "cs:{c_modernc}");
	single!(ui, "group:{c_moderng}");
	single!(ui, "signature:{c_moderns}");

	ui.label("offer");
	single!(ui, "verybad:{c_verybad}");
	single!(ui, "blacklist:{c_blacklist}");
	single!(ui, "sample3:{c_total3}");
	single!(ui, "sample4:{c_total4}");
	single!(ui, "cpq:{g_cpq}");

	let mut tcount = 0;
	let mut total_run = 0;
	let mut total_sleep = 0;
	loop {
		let m = f_break!(PerThreadMetrics::new(&arr, tcount));
		show!(ui, "{m}");	//Standard form.
		tcount += 1;
		total_run += m.total_run.as_us();
		total_sleep += m.total_sleep.as_us();
	}
	show!(ui, "CPU usage: {cpu}", cpu=cpu.process(total_run, total_sleep));
}

fn main()
{
	let mut cpu = TrackCpuUsage::new();
	let mut itr = std::env::args();
	itr.next();
	let fd = match bind_metrics_socket(itr.next().as_deref()) {
		Ok(x) => x,
		Err(err) => return writeln!(stderr(), "{err}").unwrap()
	};
	let mut ui = PrintUi::new();
	loop {
		let (ts, arr) = f_break!(receive_metrics_from(&fd));
		let dummy = arr.read(0);
		let mut cflags = [dummy;74];
		//0 to 71 are currently in use in cipher flags.
		for i in 0..cflags.len() { cflags[i] = arr.read(30000 + i as u32); }
		do_print_ui(&mut ui, &mut cpu, &mut cflags, &arr, ts);
	}
}
