use crate::common::ErrorKind;
use crate::common::ServiceTrait;
use crate::services_dp::ConnectionProperties;
use crate::services_dp::CPROP_NSA;
use crate::services_dp::CPROP_SECURE192;
use crate::services_dp::CPROP_TLS13;
use crate::services_dp::CPROP_WEAK;
use crate::services_dp::ServiceName;
use crate::services_dp::LookedUpServiceFlags;
use crate::services_dp::ServiceProperties;
use btls::Connection;
use btls::Error;
use btls::ServerConfiguration;
use btls::ServerConnection;
use btls::callbacks::ClientCertificateInfo;
use btls::callbacks::CryptographicParametersInfo;
use btls::callbacks::PointInTime;
use btls::callbacks::RequestCertificateParameters;
use btls::callbacks::SelectAlpnParameters;
use btls::callbacks::SelectAlpnReturn;
use btls::callbacks::TimeUnit;
use btls::callbacks::TlsCallbacks;
use btls::logging::Logging;
use btls::logging::MessageSeverity;
use btls::transport::PullTcpBuffer;
use btls::transport::PushTcpBuffer;
use btls::transport::StatusFlags;
use btls::utility::Mutex;
use btls_aux_fail::ResultExt;
use btls_aux_time::Timestamp;
use btls_aux_time::Timer;
use btls_aux_unix::FileDescriptorB;
use btls_daemon_helper_lib::AllocatedToken;
use btls_daemon_helper_lib::cow;
use btls_daemon_helper_lib::ListenerLoop;
use btls_daemon_helper_lib::LocalIrq;
use btls_daemon_helper_lib::RemoteIrq;
use btls_daemon_helper_lib::TokenAllocatorPool;
use btls_util_logging::syslog;
use btls_util_logging::set_logfile_permissions;
use std::borrow::Cow;
use std::collections::HashMap;
use std::cell::RefCell;
use std::fmt::Arguments;
use std::fmt::Write as FmtWrite;
use std::fs::File;
use std::io::Write;
use std::marker::PhantomData;
use std::mem::transmute;
use std::net::IpAddr;
use std::ops::Deref;
use std::ops::DerefMut;
use std::ptr::null_mut;
use std::os::unix::io::AsRawFd;
use std::rc::Rc;
use std::sync::Arc;
use std::sync::atomic::AtomicPtr;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering as MemOrdering;
use std::time::Duration;

thread_local!(static ACTIVE_TLS_SESSION_ID: Rc<RefCell<u64>> = Rc::new(RefCell::new(0)));
thread_local!(static USED_TLS_SESSION_ID: Rc<RefCell<u64>> = Rc::new(RefCell::new(0)));
//TLS sessions are never transferred across threads, so this can be in TLS, avoiding unsafe code.
thread_local!(static TLS_SESSION_LOG: Rc<RefCell<HashMap<u64, String>>> = Rc::new(RefCell::new(HashMap::new())));
static DLOG_SEQUENCE: AtomicUsize = AtomicUsize::new(0);
static DLOG_TS: AtomicUsize = AtomicUsize::new(0);
static DLOG_DIR: AtomicPtr<String> = AtomicPtr::new(null_mut());

fn incret(x: &mut u64) -> u64 { *x += 1; *x }

//RAII handle for TLS session IDs.
struct TlsSessionId(u64);
impl Drop for TlsSessionId
{
	fn drop(&mut self) { dlog_discard(self.0); }
}

impl TlsSessionId
{
	fn as_inner(&self) -> u64 { self.0 }
	fn new() -> TlsSessionId
	{
		//The used id is maximum used, so preincrement. Also create the log to start logging to it.
		let tls_id = USED_TLS_SESSION_ID.with(|x|incret(x.borrow_mut().deref_mut()));
		TLS_SESSION_LOG.with(|logs|logs.borrow_mut().insert(tls_id, String::new()));
		TlsSessionId(tls_id)
	}
}

pub fn dlog_set_directory(d: &str)
{
	let d = Box::new(d.to_owned());
	DLOG_DIR.store(Box::into_raw(d), MemOrdering::Release);
}

fn dlog_add(tls_id: u64, msg: Arguments)
{
	if tls_id == 0 { return; }	//What TLS session is this?
	//Only log to created and active logs.
	TLS_SESSION_LOG.with(|logs|if let Some(log) = logs.borrow_mut().get_mut(&tls_id) {
		log.write_fmt(msg).ok();
		log.push('\n');
	});
}

fn dlog_discard(tls_id: u64)
{
	if tls_id == 0 { return; }	//What TLS session is this?
	//Just remove the log entry. This will stop logging to it.
	TLS_SESSION_LOG.with(|logs|logs.borrow_mut().remove(&tls_id));
}

fn dlog_save(tls_id: u64)
{
	if tls_id == 0 { return; }	//What TLS session is this?
	//Saving log also disables logging by removing the log.
	TLS_SESSION_LOG.with(|logs|if let Some(log) = logs.borrow_mut().remove(&tls_id) {
		let filename = dlog_filename();
		if filename.len() == 0 { return; }	//Do not save log.
		let mut fp = match File::create(&filename) {
			Ok(fp) => fp,
			Err(err) => return syslog!(ERROR "Failed to save {filename}: {err}")
		};
		//Safety: This file is open.
		unsafe{set_logfile_permissions(FileDescriptorB::new(fp.as_raw_fd()))};
		if let Err(err) = fp.write_all(log.as_bytes()) {
			syslog!(ERROR "Failed to save {filename}: {err}");
		}
	});
}

fn dlog_filename() -> String
{
	//No need to care about leap seconds because the only place that would matter is if this got called on
	//second before leap second, then process restarted and this got called again on the following leap second.
	let ts = Timestamp::now().unix_time() as usize;
	//Just ignore errors, as this can not spuriously fail.
	DLOG_TS.compare_exchange(0, ts, MemOrdering::AcqRel, MemOrdering::Relaxed).ok();
	let ts = DLOG_TS.load(MemOrdering::Acquire);
	let seq = DLOG_SEQUENCE.fetch_add(1, MemOrdering::AcqRel);
	let dir = DLOG_DIR.load(MemOrdering::Relaxed);
	if !dir.is_null() {
		unsafe{format!("{base}/tlsfail.log.{ts}.{seq}", base=transmute::<_,&String>(dir))}
	} else {
		String::new()
	}
}

pub struct DebugLogger;

impl Logging for DebugLogger
{
	fn message(&self, severity: MessageSeverity, msg: Cow<'static, str>)
	{
		//Do not forward Debug messages, but forward all others.
		match severity {
			MessageSeverity::Debug => (),
			severity => {
				let y = severity.code_prefix();
				let y = y.chars().nth(1).unwrap_or('N');
				syslog!(_ y, "{msg}");
			}
		}
	}
	fn message_ex(&self, cid: Option<u64>, severity: MessageSeverity, msg: Arguments)
	{
		//Do not forward Debug messages, but forward all others.
		match severity {
			MessageSeverity::Debug => (),
			severity => {
				let y = severity.code_prefix();
				let y = y.chars().nth(1).unwrap_or('N');
				syslog!(_ y, "{msg}");
			}
		}
		if let Some(cid) = cid { dlog_add(cid, msg); }
	}
}


fn update_secondary_timeout(deadline: &mut Timer, timeout: Option<Duration>, flag: bool)
{
	if flag { *deadline = Timer::new_target(timeout); }
}

#[derive(Clone)]
pub struct TargetRouteAddress
{
	pub hostdata: ServiceName,
	pub sprops: ServiceProperties,
	pub sflags: LookedUpServiceFlags,
	pub sni: Arc<String>,
	pub raw_alpn: Arc<String>,
	dummy: PhantomData<u8>,
}

pub struct EventInner
{
	pub prompt_certificate: bool,
	pub certificate: Option<ClientCertificateInfo>,
	pub force_certificate_request: bool,
	pub target: Option<TargetRouteAddress>,
	pub hs_complete: bool,
	pub algo_info: Option<CryptographicParametersInfo>,
	pub timeout: Timer,
	pub secondary_timeout: bool,
	pub secondary_timeout_period: Option<Duration>,
	pub tls_sess_id: u64,		//ID of TLS session this is for.
}

impl EventInner
{
	fn with_target<R>(&self, dflt: R, func: impl FnOnce(&TargetRouteAddress) -> R) -> R
	{
		match &self.target {
			&Some(ref target) => func(target),
			&None => dflt
		}
	}
}

impl TlsCallbacks for EventInner
{
	fn sent_certificate_request(&mut self)
	{
		self.prompt_certificate = true;
	}
	fn client_certificate(&mut self, cert: ClientCertificateInfo)
	{
		self.certificate = Some(cert);
	}
	fn request_certificate(&mut self, _: RequestCertificateParameters) -> bool
	{
		let sp_cert = self.with_target(false, |t|t.sflags.is_request_certificate());
		sp_cert || self.force_certificate_request
	}
	fn select_alpn(&mut self, params: SelectAlpnParameters) -> SelectAlpnReturn
	{
		//The ALPN selection needs to consider raw ALPN on the wire, not what it was remapped to.
		let req_alpn = match &self.target {
			&Some(ref t) => t.raw_alpn.deref(),
			&None => return SelectAlpnReturn::InternalError
		};
		if req_alpn != "" {
			for (ordinal, value) in params.alpn_list.iter().enumerate() {
				if value == req_alpn { return SelectAlpnReturn::Index(ordinal); }
			}
			SelectAlpnReturn::InternalError
		} else {
			SelectAlpnReturn::Implicit
		}
	}
	fn cryptographic_parameters(&mut self, info: CryptographicParametersInfo)
	{
		self.algo_info = Some(info);
	}
	fn set_deadline(&mut self, deadline: Option<PointInTime>)
	{
		self.secondary_timeout = deadline.is_none();
		self.timeout = Timer::new_target(deadline);
		//If secondary timeout period starts, then update deadline for that.
		update_secondary_timeout(&mut self.timeout, self.secondary_timeout_period, self.secondary_timeout);
	}
}

struct KickInner(RemoteIrq);
impl btls::transport::WaitBlockCallback for KickInner
{
	fn unblocked(&mut self) { self.0.irq(); }
}


fn _get_version_info(event_cb: &Arc<Mutex<EventInner>>) -> (String, u8)
{
	event_cb.with(|x|match x.algo_info.as_ref() {
		Some(tlsinfo) => {
			let &CryptographicParametersInfo{version_str, kex_str, protection_str, hash_str,
				signature_str, validated_ct, validated_ocsp, version, ths_fixed, ..} = tlsinfo;
			let variant = if version >= 4 { "" } else if ths_fixed { "(EMS)" } else { "(LEGACY)" };
			let ocsp = if validated_ocsp { ", OCSP stapled" } else {""};
			let ct = if validated_ct {", CT stapled" } else {""};
			let msg = format!("TLS {version_str}{variant}/{kex_str}/{protection_str}/{hash_str} \
				({signature_str}{ocsp}{ct})");
			(msg, version)
		},
		None => (format!("TLS <UNKNOWN CRYPTOGRAPHIC PARAMETERS>"), 3)
	})
}

fn _get_sni_and_alpn(event_cb: &Arc<Mutex<EventInner>>) -> Option<TargetRouteAddress>
{
	event_cb.with(|x|x.target.clone())
}

fn borrow_error<'a,T:Sized,E:Sized>(x: &'a Result<T,E>) -> Option<&'a E>
{
	match x { &Ok(_) => None, &Err(ref e) => Some(e) }
}


pub struct TlsState
{
	//The primary TLS state.
	tls: ServerConnection,
	//Interrupt to use for delayed wake.
	irq: RemoteIrq,
	local_irq: LocalIrq,
	//TLS event callback.
	event_cb: Arc<Mutex<EventInner>>,
	//Poll token for TX interrupt receiver.
	txirq_poll_token: AllocatedToken,
	//Weak ciphers?
	weak_ciphers: bool,
	//Log running?
	log_running: bool,
	//The ID of this session.
	tls_sess_id: TlsSessionId,
	//Cipher flags.
	cipher_flags: u128,
}

impl TlsState
{
	fn __handle_maybe_bad(&self, err: &Error)
	{
		//Only interested in severe errors.
		if !err.is_severe() { return; }
		syslog!(CRITICAL "Critical TLS error: {err}");
	}
	fn __handle_log(&mut self, bad: Option<&Error>)
	{
		//If log is not running, do not do log ops.
		if !self.log_running { return; }
		let sess_id = self.tls_sess_id.as_inner();
		let flags = self.tls.get_status();
		//If connection faulted, save log.
		if bad.is_some() || flags.is_aborted() {
			//Grab terminal cause and shove it to log. Prefer returned error, and failing that
			//fall back to get_error().
			if let Some(err) = bad {
				self.__handle_maybe_bad(&err);
				dlog_add(sess_id, format_args!("Terminal cause: {err}"));
			} else if let Some(err) = self.tls.get_error() {
				self.__handle_maybe_bad(&err);
				dlog_add(sess_id, format_args!("Terminal cause: {err}"));
			} else {
				dlog_add(sess_id, format_args!("Terminal cause: ???"))
			};
			self.log_running = false;
			return dlog_save(sess_id);
		}
		//If is_handshaking goes to false, handshake was completed. Discard log.
		if !flags.is_handshaking() {
			self.log_running = false;
			return dlog_discard(sess_id);
		}
	}
	pub fn tls_push_tcp_data_buffer(&mut self, input: &[u8]) -> Result<(Vec<u8>, bool), Error>
	{
		let ret = self.tls.push_tcp_data(PushTcpBuffer::u8(input));
		self.__handle_log(borrow_error(&ret));
		ret
	}
	pub fn tls_return_buffer_push(&mut self, b: Vec<u8>) { self.tls.return_buffer_push(b); }
	pub fn tls_pull_tcp_data_buffer(&mut self, buffer: PullTcpBuffer) -> Result<Vec<u8>, Error>
	{
		let ret = self.tls.pull_tcp_data(buffer);
		self.__handle_log(borrow_error(&ret));
		ret
	}
	pub fn tls_return_buffer_pull(&mut self, buf: Vec<u8>) { self.tls.return_buffer_pull(buf); }
	pub fn tls_wants_rx(&self) -> bool { self.tls.get_status().is_want_rx() }
	pub fn tls_get_status(&self) -> StatusFlags { self.tls.get_status() }
	pub fn tls_get_error(&self) -> Option<Error> { self.tls.get_error() }
	pub fn maybe_handle_timeout(&mut self, now: PointInTime) -> Result<(), Cow<'static, str>>
	{
		//No need to tell apart error from no timeout.
		let (err, secondary) = self.event_cb.with(|x|(x.timeout.triggered(now), x.secondary_timeout));
		if err {
			let errpfx = if secondary { "Connection" } else { "Handshake" };
			return Err(cow!(F "{errpfx} timed out"));
		}
		Ok(())
	}
	pub fn get_timeout(&self) -> Option<PointInTime>
	{
		self.event_cb.with(|x|x.timeout.expires())
	}
	pub fn get_cipher_flags(&self) -> u128 { self.cipher_flags }
	pub fn is_tx_interrupt(&self, token: usize) -> bool
	{
		self.txirq_poll_token.value() == token
	}
	pub fn update_secondary_timeout(&self, force: bool)
	{
		self.event_cb.with(|x|{
			x.secondary_timeout |= force;
			update_secondary_timeout(&mut x.timeout, x.secondary_timeout_period, x.secondary_timeout);
		});
	}
	pub fn raise_internal_error(&mut self)
	{
		self.tls.raise_alert(80);
	}
	pub fn raise_code(&mut self, code: u8)
	{
		self.tls.raise_alert(code);
	}
	pub fn ready_for_launch(&self) -> bool
	{
		let flags = self.tls.get_status();
		let can_tx = flags.is_may_tx() || flags.is_tx_end();
		if !can_tx { return false; }		//Definitely not ready.
		let hs_complete = !flags.is_handshaking();
		if hs_complete { return true; }		//Definitely ready.
		//Otherwise it is ready if neither prompt_certificate nor sp_delay is set.
		self.event_cb.with(|x|{
			let sp_delay = x.target.as_ref().map(|t|t.sflags.is_delayed()).unwrap_or(false);
			!x.prompt_certificate && !sp_delay
		})
	}
	pub fn get_version_info(&self) -> (String, u8)
	{
		_get_version_info(&self.event_cb)
	}
	pub fn is_post_quantum(&self) -> bool
	{
		self.event_cb.with(|x|x.algo_info.as_ref().map(|y|y.pqc).unwrap_or(false))
	}
	pub fn pop_client_certificate(&self) -> Option<ClientCertificateInfo>
	{
		self.event_cb.with(|x|x.certificate.take())
	}
	pub fn get_sni_and_alpn(&self) -> Option<TargetRouteAddress>
	{
		_get_sni_and_alpn(&self.event_cb)
	}
	pub fn handle_eof(&mut self) -> Result<(), ErrorKind>
	{
		let flags = self.tls.get_status();
		let tls_eof_recvd = flags.is_rx_end();
		let handshake_finished = !flags.is_handshaking();
		//If handshake has finished, but TLS eof has not been received, raise bad_record_mac. The reason
		//not to do this if handshake is not finished is not to cause TLS error if the client just closed
		//the connection in middle of a handshake.
		if handshake_finished && !tls_eof_recvd { self.tls.raise_alert(20); }
		//If TLS EOF has not been received, signal InsecureTruncation.
		fail_if!(!tls_eof_recvd, ErrorKind::InsecureTruncation);
		//Clean close.
		Ok(())
	}
	pub fn get_properties(&self) -> ConnectionProperties
	{
		let mut f = 0;
		if self.weak_ciphers { f |= CPROP_WEAK; }
		self.event_cb.with(|x|x.algo_info.as_ref().map(|y|{
			if y.secure192 { f |= CPROP_SECURE192; }
			if y.version > 3 { f |= CPROP_TLS13; }
			if y.nsa { f |= CPROP_NSA; }
		}));
		ConnectionProperties::wrap(f)
	}
	pub fn requested_cert(&self) -> bool
	{
		self.event_cb.with(|x|x.prompt_certificate)
	}
	pub fn wait_for_txirq(&mut self) -> bool
	{
		!self.tls.register_on_unblock(Box::new(KickInner(self.irq.clone())))
	}
	pub fn fire_irq_local(&self) { self.local_irq.irq(); }
}

pub const SFLAG_TLS13_ONLY: u32 = 1;
pub const SFLAG_SECURE192: u32 = 2;
pub const SFLAG_NO_LOG: u32 = 4;
pub const SFLAG_SPINAL_TAP: u32 = 8;
pub const SFLAG_NO_CSTATS: u32 = 16;
pub const SFLAG_NO_ANDROID_HACK: u32 = 32;
pub const SFLAG_NSA_MODE: u32 = 64;

pub struct PreTlsState
{
	//The configuration.
	config: ServerConfiguration,
	///Irq to use for delayed wake.
	irq: RemoteIrq,
	local_irq: LocalIrq,
	//TLS event callback.
	event_cb: Arc<Mutex<EventInner>>,
	//Poll token for TX interrupt receiver.
	txirq_poll_token: AllocatedToken,
	//Ultimate timeout.
	timeout: PointInTime,
	//Session ID.
	tls_sess_id: TlsSessionId,
	//Weak ciphers?
	weak_ciphers: bool,
	//Special flags.
	special_flags: u32,
	//Cipher flags.
	cipher_flags: u128,
}

impl PreTlsState
{
	pub fn init(config: ServerConfiguration, trans_to: Option<Duration>, allocator: &mut TokenAllocatorPool,
		timeout: TimeUnit) -> Result<PreTlsState, Cow<'static, str>>
	{
		//Generate a fresh session ID.
		let tls_sess_id = TlsSessionId::new();
		//Create the TLS session and set TX IRQ. Setting TX IRQ causes IRQ to immediately happen if the
		//session is ready to transmit.
		let event_cb = EventInner {
			certificate: None,
			prompt_certificate: false,
			force_certificate_request: false,
			target: None,
			hs_complete: false,
			algo_info: None,
			timeout: match trans_to {
				Some(tp) => Timer::new_target(tp),
				None => Timer::new()
			},
			secondary_timeout: true,
			secondary_timeout_period: trans_to,
			tls_sess_id: tls_sess_id.as_inner(),
		};

		let txirq_poll_token = allocator.allocate().set_err("Can't allocate Tokens")?;
		let txirq = ListenerLoop::remote_irq_handle(&txirq_poll_token);

		Ok(PreTlsState {
			config: config,
			irq: txirq,
			local_irq: txirq_poll_token.as_irq(),
			event_cb: Arc::new(Mutex::new(event_cb)),
			txirq_poll_token: txirq_poll_token,
			timeout: PointInTime::from_now(timeout),
			tls_sess_id: tls_sess_id,
			weak_ciphers: false,
			special_flags: 0,
			cipher_flags: 0,
		})
	}
	pub fn set_weak_ciphers(&mut self) { self.weak_ciphers = true; }
	pub fn set_special_flags(&mut self, sflags: u32) { self.special_flags = sflags; }
	pub fn set_cipher_flags(&mut self, cflags: u128) { self.cipher_flags = cflags; }
	pub fn initialize<Service:ServiceTrait>(mut self, source_ip: Option<IpAddr>, target_ip: Option<IpAddr>,
		service: &Service, clear_trans_to: bool) -> TlsState
	{
		//Initialze the TLS.
		let mut tls = self.event_cb.with(|ev|{
			if clear_trans_to { ev.secondary_timeout_period = None; }
			service.tls_config_override(ev)
		}).unwrap_or(self.config).make_connection(Box::new(self.event_cb.clone()));
		tls.set_cid(self.tls_sess_id.as_inner());
		if let Some(ip) = source_ip { tls.set_source_address(ip).ok(); }
		if let Some(ip) = target_ip { tls.set_destination_address(ip).ok(); }
		if self.special_flags & SFLAG_TLS13_ONLY != 0 { tls.set_tls13_only(); }
		if self.special_flags & SFLAG_SECURE192 != 0 { tls.set_secure192(); }
		if self.special_flags & SFLAG_SPINAL_TAP != 0 { tls.set_best_security(); }
		if self.special_flags & SFLAG_NO_ANDROID_HACK != 0 { tls.set_no_android_hack(); }
		if self.special_flags & SFLAG_NSA_MODE != 0 { tls.set_nsa_mode(); }
		if self.special_flags & SFLAG_NO_LOG != 0 { dlog_discard(self.tls_sess_id.as_inner()); }
		if self.special_flags & (SFLAG_NO_CSTATS|SFLAG_NO_LOG) != 0 { self.cipher_flags = 0; }
		TlsState {
			tls: tls,
			irq: self.irq,
			local_irq: self.local_irq,
			event_cb: self.event_cb,
			txirq_poll_token: self.txirq_poll_token,
			weak_ciphers: self.weak_ciphers,
			tls_sess_id: self.tls_sess_id,
			log_running: self.special_flags & SFLAG_NO_LOG == 0,
			cipher_flags: self.cipher_flags,
		}
	}
	pub fn maybe_handle_timeout(&mut self) -> Result<(), Cow<'static, str>>
	{
		fail_if!(self.timeout.in_past(), cow!("Connection headers timed out"));
		Ok(())
	}
	pub fn push_tokens(&self, tokens: &mut Vec<AllocatedToken>)
	{
		tokens.push(self.txirq_poll_token.clone());
	}
	pub fn get_txirq_token(&self) -> usize
	{
		self.txirq_poll_token.value()
	}
	pub fn set_sni_alpn(&self, sni: &str, raw_alpn: &str, hostdata: ServiceName, sprops: ServiceProperties,
		sflags: LookedUpServiceFlags)
	{
		self.event_cb.with(|cb|cb.target = Some(TargetRouteAddress {
			hostdata: hostdata,
			sprops: sprops,
			sflags: sflags,
			sni: Arc::new(sni.to_owned()),
			raw_alpn: Arc::new(raw_alpn.to_owned()),
			dummy: PhantomData,
		}));
	}
	pub fn get_timeout(&self) -> Option<PointInTime>
	{
		Some(self.timeout)
	}
	pub fn get_sni_and_alpn(&self) -> Option<TargetRouteAddress>
	{
		_get_sni_and_alpn(&self.event_cb)
	}
}
