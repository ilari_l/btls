use super::ConfigT;
use super::connect_address_as_ip;
use super::Connection2;
use super::ConnectionService;
use super::format_alert;
use super::ParseSniAlpnError;
use crate::acme::AcmeService;
use crate::acme::AcmeServiceParams;
use crate::active::ActiveConnection;
use crate::common::ActiveCommonParameters;
use crate::common::ServiceTrait;
use crate::cutthrough::CutThrough;
use crate::earlydata::ClientHelloProperties;
use crate::earlydata::EarlyDataBuffer;
use crate::earlydata::EarlyParseResult;
use crate::legacy_service::ServiceLaunchParameters;
use crate::metrics_dp::RaiiInit;
use crate::services_dp::AcmeMode;
use crate::services_dp::BadService;
use crate::services_dp::ServiceCacheName;
use crate::shutdown::ServiceFailed;
use crate::shutdown::Shutdown;
use crate::tcp::TcpBuffer;
use crate::tcp::TcpState;
use crate::tls::PreTlsState;
use crate::tls::SFLAG_NO_CSTATS;
use btls::ServerConfiguration;
use btls::callbacks::PointInTime;
use btls::callbacks::TimeUnit;
use btls::utility::RwLock;
use btls_daemon_helper_lib::AddTokenCallback;
use btls_daemon_helper_lib::AllocatedToken;
use btls_daemon_helper_lib::ConnectionInfo;
use btls_daemon_helper_lib::ConnectionInfoAddress;
use btls_daemon_helper_lib::cow;
use btls_daemon_helper_lib::FileDescriptor;
use btls_daemon_helper_lib::FrozenIpSet;
use btls_daemon_helper_lib::IO_WAIT_HUP;
use btls_daemon_helper_lib::IO_WAIT_READ;
use btls_daemon_helper_lib::IO_WAIT_WRITE;
use btls_daemon_helper_lib::Poll;
use btls_daemon_helper_lib::SocketAddrEx;
use btls_daemon_helper_lib::TokenAllocatorPool;
use btls_util_logging::log;
use btls_util_logging::syslog;
use std::borrow::Cow;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::io::ErrorKind as IoErrorKind;
use std::net::IpAddr;
use std::ops::Deref;
use std::ops::DerefMut;
use std::sync::Arc;
use std::time::Instant;
use std::time::Duration;


struct NoneAsQ(Option<usize>);

impl Display for NoneAsQ
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		match self.0 {
			Some(val) => write!(fmt, "{val}"),
			None => fmt.write_str("?")
		}
	}
}

const MODE_CUT_THROUGH: u32 = 1;
const MODE_ACME_CONTROL: u32 = 4;
pub const ACME_TLS_PROTO: &'static str = "acme-tls/1";
pub const ACME_CONTROL_PROTO: &'static str = "$btls-acme-control";

macro_rules! handle_service_launch_error
{
	($clazz:ident $selfx:expr, $poll:expr, $err:expr) => {{
		$selfx.global.metrics(|m|m.service_launch_error());
		return $selfx.send_tls_alert::<$clazz>($poll, 80, $err, None);
	}}
}

macro_rules! make_active_connection_tail
{
	($selfx:expr, $data:expr, $tls:expr, $poll:expr, $service:expr, $addtoken:expr) => {{
		//Initialize the TLS, as we will not be doing cut-through, and then dump the handshake data
		//to it. This should never give any output.
		let saddr = connect_address_as_ip(&$selfx.tcp.info().src);
		let daddr = connect_address_as_ip(&$selfx.tcp.info().dest);
		let txirq_token = $tls.get_txirq_token();
		let mut tls = $tls.initialize(saddr, daddr, &$service, $selfx.trans_to.is_none());
		let cipher_flags = tls.get_cipher_flags();
		if let Err(err) = tls.tls_push_tcp_data_buffer(&$data) {
			let err = cow!(F "Error sending ClientHello to TLS: {err}");
			$selfx.global.metrics(|m|m.service_launch_error());
			return $selfx.send_tls_alert($poll, 80, err, Some($service));
		};
		//Construct the active connection, with no incoming contents and arrange it
		//to immediately get txirq.
		let c = ActiveConnection::new(
			ActiveCommonParameters {
				global: $selfx.global,
				service: $service,
				tcp: $selfx.tcp,
				data: Vec::new(),
				service_fatal_eof: $selfx.service_fatal_eof,
				flush_delay: $selfx.flush_delay,
				shutdown_delay: $selfx.shutdown_delay,
				cipher_flags: cipher_flags,
			},
			tls,						//TLS.
			$selfx.raii.to_active()
		);
		c.handle_event($poll, txirq_token, 0,  $addtoken)
	}}
}

#[derive(Clone)]
struct FrozenIpSetRef(Arc<FrozenIpSet>);

impl FrozenIpSetRef
{
	fn get(&self) -> &FrozenIpSet { self.0.deref() }
}

#[derive(Clone)]
pub struct WritableFrozenIpSet(Arc<RwLock<FrozenIpSetRef>>);

impl WritableFrozenIpSet
{
	pub fn new() -> WritableFrozenIpSet
	{
		WritableFrozenIpSet(Arc::new(RwLock::new(FrozenIpSetRef(Arc::new(FrozenIpSet::new())))))
	}
	pub fn write(&self, set: FrozenIpSet)
	{
		let n = FrozenIpSetRef(Arc::new(set));
		*self.0.write().deref_mut() = n;
	}
	fn read(&self) -> FrozenIpSetRef
	{
		self.0.read().deref().clone()
	}
}


struct InitializingConnectionNoTls
{
	scache: ServiceCacheName,			//The service cache.
	tcp: TcpState,					//The TCP state.
	scratch_buffer: EarlyDataBuffer,		//Scratch buffer.
	special_acme_target: Option<SocketAddrEx>,	//Special address to redirect ACME requests to.
	special_acme_config: Option<AcmeServiceParams>,	//Special ACME configuration.
	flush_delay: TimeUnit,				//Delay for flush.
	shutdown_delay: TimeUnit,			//Delay for shutdown.
	allow_nct_wildcard: bool,			//Allow non-cut-through with wildcard.
	service_fatal_eof: bool,			//EOF from service is fatal.
	create_time: Instant,				//Creation time of the connection.
	trans_to: Option<Duration>,			//Transfer timeout.
	debug: bool,					//Debug mode.
	allocator: TokenAllocatorPool,			//Token allocator pool.
	global: ConfigT,				//Global config.
	special_flags: u32,				//Special flags.
	stats_blacklist: FrozenIpSetRef,		//Statistics blacklist.
	raii: RaiiInit,
}

impl InitializingConnectionNoTls
{
	//Perform immediate shutdown. This is only appropriate when encountering low-level fatal TCP errors.
	fn shutdown<Service:ServiceTrait>(self, poll: &mut Poll, cause: Cow<'static, str>,
		service: Option<Service>) -> Connection2
	{
		//Move immediately to shut down state, shutting down the service and TCP.
		if let Some(mut service) = service { service.remove_pollers(poll); }
		self.tcp.deregister_poll(poll);
		Connection2::Shutdown(Shutdown {
			tls_error: cause,
			global: self.global,
			raii: self.raii.to_shutdown(),
		})
	}
	fn send_tls_alert<Service:ServiceTrait>(self, poll: &mut Poll, code: u8, cause: Cow<'static, str>,
		service: Option<Service>) -> Connection2
	{
		//Handle code0 alert specially, and send nothing through. This is used to reply to invalid TLS
		//handshakes. There is no need to hold the connection, as nothing was ever sent.
		if code == 0 { return self.shutdown(poll, cause, service); }
		//This is a fatal error. Due to context, handle this specially by
		//queueing TLS decode_error message and then jumping into service failed
		//with tcp_in_fail set. Polling has to be rearmed for write.
		if let Err(err) = self.tcp.rearm_poll(poll, IO_WAIT_WRITE) {
			self.global.metrics(|m|m.poll_error());
			return self.shutdown(poll, cow!(F "Failed to reregister poll: {err}"), service);
		}
		if let Some(mut service) = service { service.remove_pollers(poll); }
		let tcpbuf = format_alert(code);
		return Connection2::ServiceFailed(ServiceFailed::new(
			cause,							//Cause
			self.tcp,						//TCP.
			TcpBuffer::with_data(tcpbuf),				//Outgoing buffer.
			PointInTime::from_now(self.flush_delay),		//DL for flush.
			true,							//TCP input failed.
			self.shutdown_delay,					//Shutdown delay.
			self.global,						//Global state.
			self.raii.to_service_failed()
		));
	}
	//Launch the service in cut-through mode.
	fn launch_service<Service:ServiceTrait>(&mut self, poll: &mut Poll, tls: &mut PreTlsState,
		service: &mut Service, addtoken: &mut AddTokenCallback) -> Result<(), (u8, Cow<'static, str>)>
	{
		//Obtain the connection information.
		let info = self.tcp.info().clone();
		//Get the SNI and ALPN used. This is valid for both cut-through and not.
		let tgt = tls.get_sni_and_alpn().ok_or_else(||(80, cow!("Error getting TLS callback data")))?;
		fail_if!(tgt.sni.deref() == "", (109, cow!("No SNI sent")));

		//Do the actual launch. The ALPN and certificates are never present. There is also no extended
		//information, as that is reserved for decoded connections.
		let service2 = tgt.sprops.preresolve_service(&tgt.hostdata);
		service.launch_service(poll, &service2, ServiceLaunchParameters {
			info: info,
			tlsver: 0,
			sni: &tgt.sni,
			raw_alpn: &tgt.raw_alpn,
			routeas_alpn: tgt.sprops.get_service_name(&tgt.hostdata),
			conninfo: "cut through",
			spkihash: None,
			cn: None,
			ignore_access_control: false,	//No access control for cut-through.
		}, addtoken, None);
		//If service failed, return the error (it never sent data, so send alert back).
		fail_if!(service.pending_shutdown(), (80, service.get_error()));
		Ok(())
	}
	fn make_service(&mut self, addtoken: &mut AddTokenCallback) -> Result<ConnectionService, Cow<'static, str>>
	{
		let s = ConnectionService::new(&mut self.allocator, self.scache.clone(), self.debug)?;
		let mut ts = s.get_tokens();
		for t in ts.drain(..) { addtoken.add(t); }
		Ok(s)
	}
	fn make_service_acme(&mut self, poll: &mut Poll, target: &SocketAddrEx, tls: &PreTlsState,
		addtoken: &mut AddTokenCallback) -> Result<ConnectionService, (u8, Cow<'static, str>)>
	{
		let mut s = self.make_service(addtoken).map_err(|e|(80, e))?;
		//We need the tcp info and SNI.
		let info = self.tcp.info().clone();
		let tgt = tls.get_sni_and_alpn().ok_or_else(||(80, cow!("Error getting TLS callback data")))?;
		fail_if!(tgt.sni.deref() == "", (109, cow!("No SNI sent")));
		//This goes to both logs.
		log!(NOTICE "Redirecting ACME validation request for '{sni}'", sni=tgt.sni);
		syslog!(NOTICE "Redirecting ACME validation request for '{sni}'", sni=tgt.sni);
		self.global.metrics(|m|m.redirected_acme());
		s.launch_service_explicit(poll, target, info);
		//If pending_shutdown is set on the redirected connection, that means connect_ex() failed.
		//Print an error message and shut down connection (instead of timing it out).
		if s.pending_shutdown() {
			let error = s.get_error();
			let errmsg = format!("ACME backend: {error}");
			syslog!(WARNING "{errmsg}");
			fail!((80, Cow::Owned(errmsg)));
		}
		Ok(s)
	}
	fn make_cut_through_connection_tail(self, data: Vec<u8>, service: ConnectionService) ->
		Connection2
	{
		//Calculate new timeout.
		let to_pit = self.trans_to.map(|tp|PointInTime::from_now(TimeUnit::Duration(tp)));
		//Construct the active connection, with incoming contents data. No need to immediately get txirq.
		Connection2::CutThrough(CutThrough::new(
			ActiveCommonParameters {
				global: self.global,
				service: service,
				tcp: self.tcp,
				data: data,
				service_fatal_eof: self.service_fatal_eof,
				flush_delay: self.flush_delay,
				shutdown_delay: self.shutdown_delay,
				cipher_flags: 0,	//No cipher flags on cut-through.
			},
			to_pit,					//Timeout at.
			self.trans_to,				//Transfer timeout.
			self.raii.to_cut_through(),
		))
	}
	fn make_cut_through_connection(mut self, data: Vec<u8>, poll: &mut Poll, mut tls: PreTlsState,
		addtoken: &mut AddTokenCallback) -> Connection2
	{
		//Handle ACME redirects specially.
		if let Some(special_acme_target) = self.special_acme_target.clone() {
			return match self.make_service_acme(poll, &special_acme_target, &tls, addtoken) {
				Ok(service) => self.make_cut_through_connection_tail(data, service),
				Err((code, err)) => {
					self.global.metrics(|m|m.service_launch_error());
					return self.send_tls_alert::<ConnectionService>(poll, code, err, None);
				}
			};
		}
		match self.make_service(addtoken) {
			Ok(mut service) => {
				if let Err((code, err)) = self.launch_service(poll, &mut tls, &mut service,
					addtoken) {
					self.global.metrics(|m|m.service_launch_error());
					return self.send_tls_alert(poll, code, cow!(F "{err}"), Some(service));
				};
				self.make_cut_through_connection_tail(data, service)
			},
			Err(err) => handle_service_launch_error!(ConnectionService self, poll, err)
		}
	}
	fn make_active_connection(mut self, data: Vec<u8>, poll: &mut Poll, tls: PreTlsState,
		addtoken: &mut AddTokenCallback) -> Connection2
	{
		match self.make_service(addtoken) {
			Ok(x) => make_active_connection_tail!(self, data, tls, poll, x, addtoken),
			Err(err) => handle_service_launch_error!(ConnectionService self, poll, err)
		}
	}
	fn make_acmecontrol_connection(mut self, data: Vec<u8>, poll: &mut Poll, tls: PreTlsState,
		addtoken: &mut AddTokenCallback) -> Connection2
	{
		let (serv, token) = match self.special_acme_config.clone() {
			Some(config) => match AcmeService::new(config, &mut self.allocator) {
				Ok((serv, token)) => (serv, token),
				Err(err) => {
					let msg = cow!(F "Can not create ACME control: {err}");
					handle_service_launch_error!(ConnectionService self, poll, msg)
				}
			},
			None => {
				let msg = cow!("ACME control not configured");
				handle_service_launch_error!(ConnectionService self, poll, msg)
			}
		};
		addtoken.add(token);
		make_active_connection_tail!(self, data, tls, poll, serv, addtoken)
	}
	fn handle_readable_tcp(&mut self) -> Result<(bool, usize), (u8, Cow<'static, str>)>
	{
		let was_empty = !self.scratch_buffer.has_at_least(1);
		if let Err(err) = self.scratch_buffer.fill(self.tcp.fd(), self.global) {
			if was_empty {
				fail!((50, cow!(F "Connection closed before anything was received: {err}")));
			} else if let Some((amt, limit)) = self.scratch_buffer.received_proxy() {
				let msg = cow!("Error receiving client hello: {err} \
					(received {amt} out of {lim} bytes)", lim=NoneAsQ(limit));
				fail!((50, msg));
			} else {
				fail!((50, cow!(F "Error receiving PROXY data: {err}")));
			}
		}
		if was_empty && self.scratch_buffer.has_at_least(1) {
			//This is the first byte.
			let now = Instant::now();
			let diff = if now > self.create_time {
				now.duration_since(self.create_time)
			} else {
				Duration::from_secs(0)
			};
			if diff > Duration::from_millis(1000) {
				//This is connection-specific.
				log!(WARNING "Connection creation to first receive took too long: {diff:?}");
			}
		}
		//Check for PROXY header.
		let status = match self.scratch_buffer.process_proxy(self.tcp.info_mut()) {
			Ok(x) => x,
			Err(err) => if err.kind() == IoErrorKind::PermissionDenied {
				fail!((49, cow!(F "{err}")))
			} else {
				fail!((50, cow!(F "{err}")));
			}
		};
		//If first byte has been received, but is not 22, this is not valid TLS connection. Tear it down
		//quickly.
		if self.scratch_buffer.first_byte().unwrap_or(22) != 22 {
			fail!((0, cow!("Not TLS connection")));
		}
		let chsize = if status {
			self.scratch_buffer.get_clienthello_size()
		} else {
			None
		};
		Ok(if let Some(chsize) = chsize {
			if !self.scratch_buffer.can_fit(chsize) {
				fail!((22, cow!(F "Client hello too big (size {chsize})")));
			}
			(self.scratch_buffer.has_at_least(chsize), chsize)
		} else {
			(false, 0)
		})
	}
}

macro_rules! addr_check
{
	($selfx:ident $addtoken:ident $poll:ident $no_kline:ident) => {{
		if !$no_kline {
			//Report address for global kline.
			$addtoken.set_source(&$selfx.remainder.tcp.info().src);
			//IP Address, if available.
			let addr = match &$selfx.remainder.tcp.info().src {
				&ConnectionInfoAddress::V4(ip, _) => Some(IpAddr::from(ip)),
				&ConnectionInfoAddress::V6(ip, _) => Some(IpAddr::from(ip)),
				_ => None
			};
			if let Some(s) = $selfx.tls.get_sni_and_alpn() {
				if s.sprops.is_banned_ip(&s.hostdata, addr) {
					let addr = addr.unwrap_or(IpAddr::from([0;16]));
					//IP is blocked, send fatal alert 49 (access_denied).
					let msg = cow!("IP address {addr} banned from vhost {sni}", sni=s.sni);
					return $selfx.remainder.send_tls_alert::<ConnectionService>($poll, 49,
						msg, None);
				}
			}
		}
	}}
}

//Initializing connection. Service state, uninitialized TLS state and TCP state all exist. However, buffers do not.
pub struct InitializingConnection
{
	tls: PreTlsState,				//The TLS state.
	remainder: InitializingConnectionNoTls,		//The remaining state.
}

struct CutThroughCheckResult
{
	is_acme: bool,
	is_acmecontrol: bool,
	do_ct: bool,
}

impl InitializingConnection
{
	pub fn new(poll: &mut Poll, allocator: &mut TokenAllocatorPool, config: &ServerConfiguration,
		socket: FileDescriptor, socketinfo: ConnectionInfo, global: ConfigT) ->
		Result<(Connection2, Vec<AllocatedToken>), Cow<'static, str>>
	{
		let create_time = Instant::now();
		let raii = RaiiInit::new(global.metrics_arc());
		global.with(|global_config|{
			let allow_proxy = global_config.proxy_config.proxy_allowed(&socketinfo);
			let stats_blacklist = global_config.stats_blacklist.read();
			//Fill the structure. The TLS is not initialized yet.
			let tls = PreTlsState::init(config.clone(), global_config.trans_timeout, allocator,
				global_config.handshake_timeout)?;
			let tcp = TcpState::init(socket, socketinfo, allocator, poll)?;
			let mut tokens = Vec::new();
			tcp.push_tokens(&mut tokens);
			tls.push_tokens(&mut tokens);
			Ok((Connection2::Initializing(InitializingConnection {
				tls: tls,
				remainder: InitializingConnectionNoTls {
					tcp: tcp,
					scratch_buffer: EarlyDataBuffer::init(allow_proxy),
					special_acme_target: global_config.special_acme_target.clone(),
					special_acme_config: global_config.special_acme_config.clone(),
					scache: global_config.scache.clone(),
					flush_delay: global_config.flush_delay,
					shutdown_delay: global_config.shutdown_delay,
					allow_nct_wildcard: global_config.allow_nct_wildcard,
					service_fatal_eof: false,
					create_time: create_time,
					trans_to: global_config.trans_timeout,
					debug: global_config.debug,
					allocator: allocator.clone(),
					global: global,
					stats_blacklist: stats_blacklist,
					raii: raii,
					special_flags: 0,
				}
			}), tokens))
		})
	}
	fn do_check_ch_cutthrough(&mut self, rsize: usize, sample_crypto: bool) ->
		Result<CutThroughCheckResult, (u8, Cow<'static, str>)>
	{
		let dest_ip_raw = &self.remainder.tcp.info().dest;
		let tls = &mut self.tls;
		let chello = self.remainder.scratch_buffer.peek_buffer(rsize);
		let scache = &self.remainder.scache;
		let mut props = ClientHelloProperties::default();

		let sni_fn = |sni:&str|if sni != "" {
			scache.lookup_by_sni(sni)
		} else {
			scache.lookup_by_sni_default()
		};
		let ret = EarlyDataBuffer::scratch_hello_get_sni_alpn(chello, rsize, dest_ip_raw, &mut props,
			sni_fn);
		//Increment bad_cipher on CP_BLACKLISTED, but require CP_VERY_BAD to set weak_ciphers.
		self.remainder.global.metrics(|m|{
			if sample_crypto {
				for i in 0..128 { if props.crypto_flags & 1 << i != 0 { m.cipher_flags(i); }}
			}
			if props.crypto_flags & crate::crypto_props::CP_BLACKLIST != 0 { m.bad_cipher(); }
		});
		if sample_crypto { tls.set_cipher_flags(props.crypto_flags); }
		if props.crypto_flags & crate::crypto_props::CP_VERY_BAD != 0 { tls.set_weak_ciphers(); }
		let EarlyParseResult{ sni, alpn, sprops, hostdata } = match ret {
			Ok(tuple) => tuple,
			Err(ParseSniAlpnError::BadClientHello(err)) =>
				return Err((10, cow!(F "Error parsing TLS client hello: {err}"))),
			Err(ParseSniAlpnError::BadService(err)) =>
				return Err((80, cow!(F "Error parsing service: {err}"))),
			Err(ParseSniAlpnError::NoHandlerFound(name)) =>
				return Err((112, cow!(F "No handler found for {name}"))),
			Err(ParseSniAlpnError::AlpnFail(BadService::NoDefault)) =>
				return Err((120, cow!("No default ALPN service for port"))),
			Err(ParseSniAlpnError::AlpnFail(BadService::NegotiationFailed)) =>
				return Err((120, cow!("ALPN negotiation failed"))),
			Err(ParseSniAlpnError::AlpnFail(BadService::NoService(service))) =>
				return Err((120, cow!(F "No service '{service}' for vhost"))),
		};
		//If no ALPN, check if that was due to extension not present or negotiation failure.
		let alpn: &str = alpn.as_deref().unwrap_or("");

		//Initialize self.remainder.special_flags.
		let sflags = sprops.preresolve_service_flags(&hostdata);
		self.remainder.special_flags = sflags.get_special_flags();
		if sflags.is_disable_timeout() { self.remainder.trans_to = None; }

		//There is also route as ALPN. Some special protocols set it, and it is also set to default if there
		//is no ALPN.
		let (is_acme, is_acmecontrol) = if alpn == ACME_CONTROL_PROTO {
			(true, false)
		} else if alpn == ACME_TLS_PROTO {
			(true, true)
		} else {
			(false, false)
		};

		//ACME can alter things...
		let mut do_ct = sflags.is_cut_through();
		match sflags.get_acme_mode() {
			//In default mode, if special_acme_target is set, force cut-through in order to
			//redirect the connection by default.
			AcmeMode::Default => do_ct |= self.remainder.special_acme_target.is_some(),
			//Capture mode does cut-through or not based on if special_acme_target is
			//set, as it needs to intercept if special_acme_target is not set.
			AcmeMode::Capture => do_ct = self.remainder.special_acme_target.is_some(),
			//Do not do anything to do_ct, as whatever do_check_ch_cutthrough() decided is
			//correct. Clear special_acme_target, as we do not want to use it in any case.
			AcmeMode::ForceCapture => self.remainder.special_acme_target = None,
			AcmeMode::None => (),
		}
		self.remainder.service_fatal_eof = sflags.is_fatal_eof();

		//Wildcard targets must be cut-through.
		if !do_ct && !is_acme && sflags.is_wildcard() && !self.remainder.allow_nct_wildcard {
			return Err((80, cow!("Default target must be cut-through mode")));
		}

		tls.set_sni_alpn(&sni, alpn, hostdata, sprops, sflags);

		Ok(CutThroughCheckResult { do_ct, is_acme, is_acmecontrol })
	}
	fn got_client_hello(&mut self, chsize: usize, no_kline: &mut bool, sample_crypto: bool) ->
		Result<(u32, Vec<u8>), (u8, Cow<'static, str>)>
	{
		let res = self.do_check_ch_cutthrough(chsize, sample_crypto)?;
		*no_kline |= res.is_acme;
		if res.do_ct {
			//If not ACME, just clear the ACME special target so we do not use it.
			if !res.is_acme { self.remainder.special_acme_target = None; }
			Ok((MODE_CUT_THROUGH, self.remainder.scratch_buffer.swap_clear().to_owned()))
		} else if res.is_acmecontrol {
			Ok((MODE_ACME_CONTROL, self.remainder.scratch_buffer.swap_clear().to_owned()))
		} else {
			Ok((0, self.remainder.scratch_buffer.swap_clear().to_owned()))
		}
	}
	pub fn get_next_timed_event(&self, _now: PointInTime) -> Option<PointInTime>
	{
		self.tls.get_timeout()
	}
	pub fn handle_fault(self, poll: &mut Poll) -> Connection2
	{
		//No better error than broken pipe.
		self.remainder.shutdown::<ConnectionService>(poll, cow!("Broken pipe"), None)
	}
	pub fn handle_event(mut self, poll: &mut Poll, tok: usize, kind: u8, addtoken: &mut AddTokenCallback) ->
		Connection2
	{
		//Only TCP reads matter.
		if kind & (IO_WAIT_READ|IO_WAIT_HUP) != 0 && self.remainder.tcp.is_tcp_interrupt(tok) {
			match self.remainder.handle_readable_tcp() {
				Ok((true, chsize)) => {
					let mut no_kline = false;
					let (mode, data) = match self.got_client_hello(chsize, &mut no_kline,
						check_sampling(self.remainder.stats_blacklist.get(),
						self.remainder.tcp.info())) {
						Ok(data) => data,
						Err((code, err)) => {
							self.remainder.global.metrics(|m|{
								m.initialize_error();
								if code == 10 { m.chello_error(); }
								if code == 80 { m.faulty_handler(); }
								if code == 112 { m.missing_handler(); }
								if code == 120 { m.missing_service(); }
							});
							return self.remainder.
								send_tls_alert::<ConnectionService>(poll, code,
								cow!(F "{err}"), None)
						}
					};
					//Do not report address for ACME connetions. Otherwise do report the
					//address, so these connections are affected by K-Line. At the same time,
					//do local IP address check.
					addr_check!(self addtoken poll no_kline);
					return if mode == MODE_CUT_THROUGH {
						self.remainder.make_cut_through_connection(data, poll, self.tls,
							addtoken)
					} else if mode == MODE_ACME_CONTROL {
						self.tls.set_special_flags(SFLAG_NO_CSTATS);
						self.remainder.make_acmecontrol_connection(data, poll, self.tls,
							addtoken)
					} else {
						self.tls.set_special_flags(self.remainder.special_flags);
						self.remainder.make_active_connection(data, poll, self.tls,
							addtoken)
					};
				},
				Ok((false, _)) => (),
				Err((code, msg)) => {
					self.remainder.global.metrics(|m|m.initialize_error());
					return self.remainder.send_tls_alert::<ConnectionService>(poll, code, msg,
						None)
				}
			};
		}
		//Rearm read on TCP socket.
		if let Err(err) = self.remainder.tcp.rearm_poll(poll, IO_WAIT_READ) {
			self.remainder.global.metrics(|m|m.poll_error());
			let msg = cow!(F "Failed to reregister poll: {err}");
			return self.remainder.shutdown::<ConnectionService>(poll, msg, None);
		}
		Connection2::Initializing(self)
	}
	pub fn handle_timed_event(mut self, _now: PointInTime, poll: &mut Poll) -> Connection2
	{
		if let Err(err) = self.tls.maybe_handle_timeout() {
			self.remainder.global.metrics(|m|m.timeout_error());
			return self.remainder.shutdown::<ConnectionService>(poll, err, None);
		}
		Connection2::Initializing(self)
	}
	pub fn get_main_fd(&self) -> i32
	{
		self.remainder.tcp.as_fd()
	}
}

fn check_sampling(blacklist: &FrozenIpSet, x: &ConnectionInfo) -> bool
{
	let addr = match &x.src {
		&ConnectionInfoAddress::V4(ip, _) => IpAddr::from(ip),
		&ConnectionInfoAddress::V6(ip, _) => IpAddr::from(ip),
		_ => return true,	//Unknown address.
	};
	!blacklist.lookup(addr)
}
