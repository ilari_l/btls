use crate::services_dp::LaunchInfoExtended;
use crate::services_dp::LookedUpService;
use crate::services_dp::ServiceCacheName;
use btls_aux_fail::ResultExt;
use btls_aux_memory::Hexdump;
use btls_aux_memory::SafeShowByteString;
use btls_aux_unix::SocketFlags;
use btls_aux_unix::SocketType;
use btls_daemon_helper_lib::AddressHandle;
use btls_daemon_helper_lib::AddTokenCallback;
use btls_daemon_helper_lib::AllocatedToken;
use btls_daemon_helper_lib::cloned;
use btls_daemon_helper_lib::ConnectionInfo;
use btls_daemon_helper_lib::ConnectionInfoAddress;
use btls_daemon_helper_lib::cow;
use btls_daemon_helper_lib::CyclicArray;
use btls_daemon_helper_lib::FileDescriptor;
use btls_daemon_helper_lib::IO_WAIT_HUP;
use btls_daemon_helper_lib::IO_WAIT_ONESHOT;
use btls_daemon_helper_lib::IO_WAIT_READ;
use btls_daemon_helper_lib::IO_WAIT_WRITE;
use btls_daemon_helper_lib::is_fatal_error;
use btls_daemon_helper_lib::Poll;
use btls_daemon_helper_lib::SocketAddrEx;
use btls_daemon_helper_lib::SocketAddrExHandle;
use btls_daemon_helper_lib::store_err;
use btls_daemon_helper_lib::TokenAllocatorPool;
use btls_util_logging::log;
use std::borrow::Cow;
use std::cmp::min;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::fmt::Write;
use std::io::ErrorKind as IoErrorKind;
use std::io::Read as IoRead;
use std::io::Write as IoWrite;
use std::mem::replace;
use std::ops::Deref;
use std::os::unix::io::IntoRawFd;
use std::process::Command;
use std::process::Stdio;
use std::str::from_utf8;
use std::sync::Arc;


const PP2_TYPE_SSL: u8 = 0x20;
const PP2_SUBTYPE_SSL_VERSION: u8 = 0x21;
const PP2_SUBTYPE_SSL_CN: u8 = 0x22;
const PP2_SUBTYPE_SSL_SPKI_SHA256_EXT: u8 = 0xE0;
const PP2_TYPE_ALPN: u8 = 0x01;
const PP2_TYPE_AUTHORITY: u8 = 0x02;

const PP2_CLIENT_SSL: u8 = 0x01;
const PP2_CLIENT_CERT_CONN: u8 = 0x02;

const PP2_VERSION_ID: u8 = 0x20;
const PP2_COMMAND_REMOTE: u8 = 0x01;

const PP2_ADDRESS_IPV4: u8 = 0x10;
const PP2_ADDRESS_IPV6: u8 = 0x20;
const PP2_TRANS_TCP: u8 = 0x01;


macro_rules! deregister_poll
{
	($poll:expr, $maybe_fd:expr) => { if let &Some(ref x) = &$maybe_fd { let _ = $poll.deregister(x); } }
}

#[derive(Copy,Clone,PartialEq,Eq)]
enum PollStatus
{
	//Not known to be readable/writable.
	Unknown,
	//Active readable/wriable.
	Active,
	//Closed.
	Closed,
}

#[derive(Copy,Clone,PartialEq,Eq)]
enum LaunchState
{
	//Nothing is happening yet.
	Idle,
	//Service is being launched.
	Launching,
	//Service has been launched.
	Launched,
	//No target name present
	LaunchFailedNoTarget,
	//Launching service failed.
	LaunchFailed,
	//Launching service failed.
	LaunchFailedAccessDenied,
}

#[derive(Copy,Clone,PartialEq,Eq)]
#[non_exhaustive]
pub enum LaunchFate
{
	Successful,
	FailedError,
	FailedNoTarget,
	FailedAccessDenied,
}

#[derive(Clone,Debug,Hash,PartialEq,Eq)]
pub enum Service
{
	Gsocket(SocketAddrExHandle, u32),
	Command(Vec<String>),
}

fn service_status(service: &Service) -> bool
{
	match service {
		&Service::Gsocket(ref addr, _) => addr.status_ok(),
		&Service::Command(_) => true,
	}
}

#[derive(Debug)]
pub struct ServiceSet(CyclicArray<Arc<Service>>);

impl ServiceSet
{
	pub fn new(services: &[Arc<Service>]) -> ServiceSet { ServiceSet(CyclicArray::new(services.to_owned())) }
	pub fn choose(&self) -> Option<Arc<Service>> { self.0.get(|s|service_status(s)).map(cloned) }
}

pub struct ServiceLaunchParameters<'a>
{
	pub info: ConnectionInfo,
	pub tlsver: u8,
	pub sni: &'a str,
	pub raw_alpn: &'a str,
	pub routeas_alpn: &'a str,
	pub conninfo: &'a str,
	pub spkihash: Option<&'a [u8]>,
	pub cn: Option<&'a [u8]>,
	pub ignore_access_control: bool,
}

fn add_proxyv2_tls<'a>(ret: &mut Vec<u8>, p: &ServiceLaunchParameters<'a>)
{
	let dport = match &p.info.dest {
		&ConnectionInfoAddress::V4(_, port) => Some(port),
		&ConnectionInfoAddress::V6(_, port) => Some(port),
		_ => None
	};
	//TLS information. If TLS is in use, we add SSL, ALPN and SNI info. p.tlsver = 0 would really mean
	//SSL 3.0, but considering how broken that is, it is fair to repurpose that for "no TLS".
	if p.tlsver > 0 {		//tlsver=0 is really SSL 3.0, but that is worth nothing.
		add_proxyv2_tlv(ret, PP2_TYPE_SSL, |ret|{
			//This is TLS connection. Possibly with client certificate.
			//Always set PP2_CLIENT_SSL in flags, also set PP2_CLIENT_CERT_CONN if client
			//certificate was validated (CN is set).
			let mut sslflags = PP2_CLIENT_SSL;
			if p.cn.is_some() { sslflags |= PP2_CLIENT_CERT_CONN; }
			ret.push(sslflags);
			//There is four-byte verify status field. This field is always zeroes.
			ret.extend_from_slice(&[0;4]);
			//Add "SSL version" sub-TLV. This is textual version.
			add_proxyv2_tlv(ret, PP2_SUBTYPE_SSL_VERSION, |ret|{
				let mut version = [84,76,83,32,49,46,50];
				//Assume that TLS 1.10 is way off:
				version[6] = p.tlsver.wrapping_add(47);
				ret.extend_from_slice(&version);
			});
			//If validated client CN is available, include "client CN" sub-TLV.
			if let Some(ref cn) = p.cn {
				add_proxyv2_tlv(ret, PP2_SUBTYPE_SSL_CN, |ret|{
					ret.extend_from_slice(cn);
				});
			}
			//If client sent a certificate (even non-validated), include "client spki hash"
			//sub-TLV.
			if let Some(ref spkihash) = p.spkihash {
				add_proxyv2_tlv(ret, PP2_SUBTYPE_SSL_SPKI_SHA256_EXT, |ret|{
					ret.extend_from_slice(spkihash);
				});
			}
		});
		//Include the ALPN, if any. Note that this is not sub-TLV of SSL info.
		if p.raw_alpn.deref() != "" {
			add_proxyv2_tlv(ret, PP2_TYPE_ALPN, |ret|{
				ret.extend_from_slice(p.raw_alpn.as_bytes());
			});
		}
		//Include the SNI, if any. Note that this is not sub-TLV of SSL info.
		if p.sni.len() > 0 {
			add_proxyv2_tlv(ret, PP2_TYPE_AUTHORITY, |ret|{
				ret.extend_from_slice(p.sni.as_bytes());
				//The authority info also has a port (if any was present in target address!)
				if let Some(dport) = dport {
					ret.extend_from_slice(format!(":{dport}").as_bytes());
				}
			});
		}
	}
}

fn encode_addrs_as_ipv6(ret: &mut Vec<u8>, src: &ConnectionInfoAddress, dest: &ConnectionInfoAddress)
{
	const IPV4_PFX: [u8;12] = [0,0,0,0,0,0,0,0,0,0,255,255];
	let srcport = match src {
		&ConnectionInfoAddress::V4(ref x, port) => {
			ret.extend_from_slice(&IPV4_PFX);
			ret.extend_from_slice(x);
			port
		},
		&ConnectionInfoAddress::V6(ref x, port) => {
			ret.extend_from_slice(x);
			port
		},
		_ => {	//Unknown other.
			ret.extend_from_slice(&[0;16]);
			0
		},
	};
	let destport = match dest {
		&ConnectionInfoAddress::V4(ref x, port) => {
			ret.extend_from_slice(&IPV4_PFX);
			ret.extend_from_slice(x);
			port
		},
		&ConnectionInfoAddress::V6(ref x, port) => {
			ret.extend_from_slice(x);
			port
		},
		_ => {	//Unknown other.
			ret.extend_from_slice(&[0;16]);
			0
		},
	};
	ret.push((srcport >> 8) as u8);
	ret.push(srcport as u8);
	ret.push((destport >> 8) as u8);
	ret.push(destport as u8);
}

fn encode_addrs_as_ipv4(ret: &mut Vec<u8>, src: &ConnectionInfoAddress, dest: &ConnectionInfoAddress)
{
	const UNKNOWN_SRC: [u8;4] = [240,0,0,1];
	const UNKNOWN_DEST: [u8;4] = [240,0,0,2];
	const IPV4_PFX: [u8;12] = [0,0,0,0,0,0,0,0,0,0,255,255];
	let srcport = match src {
		&ConnectionInfoAddress::V4(ref x, port) => {
			ret.extend_from_slice(x);
			port
		},
		&ConnectionInfoAddress::V6(ref x, port) => {
			if &x[..12] == &IPV4_PFX[..] {
				ret.extend_from_slice(&x[12..]);	//Mapped address.
			} else {
				ret.extend_from_slice(&UNKNOWN_SRC);
			}
			port
		},
		_ => {	//Unknown other.
			ret.extend_from_slice(&UNKNOWN_SRC);
			0
		},
	};
	let destport = match dest {
		&ConnectionInfoAddress::V4(ref x, port) => {
			ret.extend_from_slice(x);
			port
		},
		&ConnectionInfoAddress::V6(ref x, port) => {
			if &x[..12] == &IPV4_PFX[..] {
				ret.extend_from_slice(&x[12..]);	//Mapped address.
			} else {
				ret.extend_from_slice(&UNKNOWN_DEST);
			}
			port
		},
		_ => {	//Unknown other.
			ret.extend_from_slice(&UNKNOWN_DEST);
			0
		},
	};
	ret.push((srcport >> 8) as u8);
	ret.push(srcport as u8);
	ret.push((destport >> 8) as u8);
	ret.push(destport as u8);
}

fn is_ipv4_addr(src: &ConnectionInfoAddress) -> bool
{
	const IPV4_PFX: [u8;12] = [0,0,0,0,0,0,0,0,0,0,255,255];
	match src {
		&ConnectionInfoAddress::V4(_, _) => true,
		&ConnectionInfoAddress::V6(ref x, _) => &x[..12] == &IPV4_PFX[..],
		_ => false
	}
}

fn format_proxyv2_header<'a>(p: &ServiceLaunchParameters<'a>) -> Vec<u8>
{
	let addrkind = if is_ipv4_addr(&p.info.src) { PP2_ADDRESS_IPV4 } else { PP2_ADDRESS_IPV6 };
	let mut ret = Vec::new();
	const MAGIC:[u8;12] = [13,10,13,10,0,13,10,0x51,0x55,0x49,0x54,10];
	ret.extend_from_slice(&MAGIC);
	ret.push(PP2_VERSION_ID | PP2_COMMAND_REMOTE);
	ret.push(addrkind | PP2_TRANS_TCP);
	with_proxyv2_len(&mut ret, |ret|{
		//Base address and port information.
		if addrkind == PP2_ADDRESS_IPV6 {
			encode_addrs_as_ipv6(ret, &p.info.src, &p.info.dest);
		} else if addrkind == PP2_ADDRESS_IPV4 {
			encode_addrs_as_ipv4(ret, &p.info.src, &p.info.dest);
		}
		add_proxyv2_tls(ret, p);
	});
	ret
}

fn format_ipv6_address(addr: &ConnectionInfoAddress) -> (String, u16)
{
	match addr {
		&ConnectionInfoAddress::V4(ref addr, port) => {
			let [a,b,c,d] = *addr;
			let s = format!("0000:0000:0000:0000:0000:ffff:{a:02x}{b:02x}:{c:02x}{d:02x}");
			(s, port)
		},
		&ConnectionInfoAddress::V6(ref addr, port) => {
			let mut s = String::new();
			for i in 0..16 {
				write!(s, "{octet:02x}", octet=addr[i]).ok();
				if i & 1 == 1 && i < 15 { s.push(':'); }
			}
			(s, port)
		},
		_ => {		//Can not format.
			let mut s = String::new();
			s.push_str("::");
			(s, 0)
		}
	}
}

fn format_ipv4_address(addr: &ConnectionInfoAddress, dest: bool) -> (String, u16)
{
	let unknown = if dest { "240.0.0.2" } else { "240.0.0.1" };
	const IPV4_PFX: [u8;12] = [0,0,0,0,0,0,0,0,0,0,255,255];
	match addr {
		&ConnectionInfoAddress::V4(ref addr, port) => {
			let mut s = String::new();
			let [a,b,c,d] = addr;
			write!(s, "{a}.{b}.{c}.{d}").ok();
			(s, port)
		},
		&ConnectionInfoAddress::V6(ref addr, port) => {
			let mut s = String::new();
			if &addr[..12] == &IPV4_PFX[..] {
				let [_0,_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,_11,a,b,c,d] = addr;
				write!(s, "{a}.{b}.{c}.{d}").ok();
			} else {
				s.push_str(unknown);
			}
			(s, port)
		},
		_ => {		//Can not format.
			let mut s = String::new();
			s.push_str(unknown);
			(s, 0)
		}
	}
}

fn with_proxyv2_len<F>(out: &mut Vec<u8>, mut cb: F) where F: FnMut(&mut Vec<u8>) -> ()
{
	let pos = out.len();
	out.push(0);
	out.push(0);
	let lbase = out.len();
	cb(out);
	let len = out.len() - lbase;
	out[pos+0] = (len >> 8) as u8;
	out[pos+1] = len as u8;
}

fn add_proxyv2_tlv<F>(out: &mut Vec<u8>, kind: u8, cb: F) where F: FnMut(&mut Vec<u8>) -> ()
{
	out.push(kind);
	with_proxyv2_len(out, cb)
}

fn format_proxyv1_header(info: &ConnectionInfo) -> String {
	if is_ipv4_addr(&info.src) {
		let (srcaddr, srcport) = format_ipv4_address(&info.src, false);
		let (dstaddr, dstport) = format_ipv4_address(&info.dest, true);
		format!("PROXY TCP4 {srcaddr} {dstaddr} {srcport} {dstport}\r\n")
	} else {
		let (srcaddr, srcport) = format_ipv6_address(&info.src);
		let (dstaddr, dstport) = format_ipv6_address(&info.dest);
		format!("PROXY TCP6 {srcaddr} {dstaddr} {srcport} {dstport}\r\n")
	}
}

pub struct ConnectionService
{
	//True if service uses different file descriptors for stdin and stdout (executable).
	//False if service uses one file descriptor for both stdin and stdout (socket).
	service_split_rw: bool,
	//The state of service launch.
	launch_state: LaunchState,
	//Name of socket in delayed initialization.
	delayed_socket_name: String,
	//Service error. If set, triggers shutdown of connection.
	service_error: Option<Cow<'static, str>>,
	///Address handle.
	address_handle: Option<AddressHandle>,

	//Service stdin. These are TO service.
	//File descriptor.
	stdin_fd: Option<FileDescriptor>,
	//Poll token.
	stdin_poll_token: AllocatedToken,
	//Original poll token.
	stdin_poll_token_orig: AllocatedToken,
	//Stdin writable poll status.
	stdin_poll_status: PollStatus,
	//Stdin unsent prefix.
	stdin_prefix: Option<(Vec<u8>, usize)>,
	//Stdin byte counter.
	stdin_counter: u64,
	//Stdin previous poll state.
	stdin_prev_poll: u8,

	//Service stdout. These are FROM service.
	//File descriptor. This may or may not be the same as stdin_fd.
	stdout_fd: Option<FileDescriptor>,		//Service read side (incoming).
	//Poll token. If stdin_fd is the same as stdout_fd, then this is the same as stdin_poll_token.
	stdout_poll_token: AllocatedToken,
	//Stdout readable poll status.
	stdout_poll_status: PollStatus,
	//Stdout byte counter.
	stdout_counter: u64,
	//Stdout previous poll state.
	stdout_prev_poll: u8,

	//Service stderr. These are FROM service.
	//File descriptor.
	stderr_fd: Option<FileDescriptor>,
	//Poll token.
	stderr_poll_token: AllocatedToken,
	//Incomplete error line.
	stderr_buf: Vec<u8>,
	//True => stderr_buf has overflowed. False => stderr_buf has not overflowed.
	stderr_buf_overflow: bool,

	//Global shared service cache.
	global_service_cache: ServiceCacheName,

	//Debug flag.
	debug: bool,
	//Access denied flag.
	access_denied: bool,
}

fn unwrap_alpn<'a>(x: &'a str) -> &'a str
{
	if x != "" { x } else { "<None>" }
}

struct PrintAlpn<'a>(&'a str);

impl<'a> Display for PrintAlpn<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		if self.0 != "" { write!(f, "/{alpn}", alpn=self.0) } else { Ok(()) }
	}
}

fn show_conninfo(params: &ServiceLaunchParameters)
{
	//This is connection-specific.
	log!(INFO "{sni}{alpn}: Using {info} to {target}",
		sni=&params.sni, alpn=PrintAlpn(&params.raw_alpn), info=params.conninfo, target=params.routeas_alpn);
}

impl ConnectionService
{
	//Create a new service.
	pub fn new(allocator: &mut TokenAllocatorPool, scache: ServiceCacheName, debug: bool) ->
		Result<ConnectionService, Cow<'static, str>>
	{
		let stdin_poll_token = allocator.allocate().set_err("Can't allocate Tokens")?;
		let stdout_poll_token = allocator.allocate().set_err("Can't allocate Tokens")?;
		let stderr_poll_token = allocator.allocate().set_err("Can't allocate Tokens")?;
		Ok(ConnectionService {
			service_split_rw: false,
			launch_state: LaunchState::Idle,
			delayed_socket_name: String::new(),
			service_error: None,
			address_handle: None,
			stdin_fd: None,
			stdin_poll_token: stdin_poll_token.clone(),
			stdin_poll_token_orig: stdin_poll_token,
			stdin_poll_status: PollStatus::Unknown,
			stdin_prefix: None,
			stdin_counter: 0,
			stdin_prev_poll: 0,
			stdout_fd: None,
			stdout_poll_token: stdout_poll_token,
			stdout_poll_status: PollStatus::Unknown,
			stdout_counter: 0,
			stdout_prev_poll: 0,
			stderr_fd: None,
			stderr_poll_token: stderr_poll_token,
			stderr_buf: Vec::new(),
			stderr_buf_overflow: false,
			global_service_cache: scache,
			debug: debug,
			access_denied: false,
		})
	}
	//Get up count.
	pub fn get_up_count(&self) -> u64 { self.stdin_counter }
	//Get down count.
	pub fn get_down_count(&self) -> u64 { self.stdout_counter }
	//Stdin prefix has already been sent?
	pub fn prefix_sent(&self) -> bool { self.stdin_prefix.is_none() }
	//Stdin has been closed?
	pub fn stdin_is_closed(&self) -> bool { self.stdin_poll_status == PollStatus::Closed }
	//Get final fate.
	pub fn fate(&self) -> Option<LaunchFate>
	{
		match self.launch_state {
			LaunchState::Idle => None,
			LaunchState::Launching => None,
			LaunchState::Launched => Some(LaunchFate::Successful),
			LaunchState::LaunchFailedNoTarget => Some(LaunchFate::FailedNoTarget),
			LaunchState::LaunchFailed => Some(LaunchFate::FailedError),
			LaunchState::LaunchFailedAccessDenied => Some(LaunchFate::FailedAccessDenied),
		}
	}
	//Get rendezvous requests: Write, force-Write and Read.
	pub fn rendezvous_request(&self) -> (bool, bool, bool)
	{
		(
			self.stdin_poll_status == PollStatus::Active,
			self.stdin_prefix.is_some() && self.stdin_poll_status == PollStatus::Active,
			self.stdout_poll_status == PollStatus::Active,
		)
	}

	//Read from service.
	pub fn read_from_service(&mut self, buf: &mut [u8]) -> Result<Option<usize>, Cow<'static, str>>
	{
		let mut srfd = if let &Some(ref fd) = &self.stdout_fd { fd.clone() } else { return Ok(Some(0)); };
		//Don't read closed file descriptors.
		if self.stdout_poll_status == PollStatus::Closed { return Ok(Some(0)); }
		//We do a read, so we do not know if this is readable anymore. So ask again.
		self.stdout_poll_status = PollStatus::Unknown;
		let r = match srfd.read(buf) {
			Ok(x) => x,
			Err(ref x) if !is_fatal_error(x) => {
				if self.debug { log!(DEBUG "Read from service interrupted."); }
				return Ok(Some(0));
			},
			Err(err) => fail!(format!("Service read error: {err}"))
		};

		if self.debug && r > 0 {
			log!(DEBUG "Read {r} bytes from service.");
		}
		self.stdout_counter += r as u64;
		//If read produced 0, the service EOF'd, so we forward EOF over TLS. Also, dump stdout.
		if r == 0 {
			if self.debug { log!(DEBUG "Service sent EOF."); }
			self.stdout_poll_status = PollStatus::Closed;
			return Ok(None);
		}
		Ok(Some(r))
	}

	//Wants write to service?
	pub fn wants_write_to_service(&self) -> bool
	{
		//Need stdin.
		if self.stdin_fd.is_none() { return false; }
		//Not interested if pending shutdown.
		if self.pending_shutdown() { return false; }
		//If EOF has already been sent, don't process anymore.
		if self.stdin_poll_status == PollStatus::Closed { return false; }
		//Otherwise, we can take write.
		true
	}
	//Write to service.
	pub fn write_to_service(&mut self, buf: &[u8], poll: &mut Poll) -> Result<usize, Cow<'static, str>>
	{
		//The file descriptor to write to.
		let mut swfd = if let &Some(ref fd) = &self.stdin_fd { fd.clone() } else { return Ok(0); };
		let ret = {
			//If there is still prefix, drain it.
			let buf = if let &Some((ref x, c)) = &self.stdin_prefix { &x[c..] } else { buf };
			//We are doing a write, so we no longer know about writablity. However, we know the status
			//can not be closed, because the above check would fail if so.
			self.stdin_poll_status = PollStatus::Unknown;
			swfd.write(&buf)
		};
		//Handle errors and see how much we got.
		let amount = match ret {
			Ok(x) => x,
			Err(err) => {
				if !is_fatal_error(&err) {
					if self.debug { log!(DEBUG "Write to service interrupted."); }
					return Ok(0);
				}
				if err.kind() == IoErrorKind::BrokenPipe {
					//This is special.
					if self.debug {
						log!(DEBUG "Write to service closed by EPIPE.");
					}
					self.close_link_to_service(poll)?;
					return Ok(0);
				}
				//Other errors are fatal.
				fail!(format!("Service write error: {err}"));
			}
		};
		if self.debug { log!(DEBUG "Wrote {amount} bytes to service."); }
		//The prefix is not included in up count.
		if self.stdin_prefix.is_none() { self.stdin_counter += amount as u64; }
		//Record advance in prefix buffer and dump it if needed.
		let dump_prefix = if let &mut Some((ref x, ref mut c)) = &mut self.stdin_prefix {
			//This is at most buffer length, since amount is at most buffer length - c.
			*c += amount;
			*c >= x.len()
		} else {
			//Accept as much as was written.
			return Ok(amount);
		};
		//Delete prefix buffer if we reached end of it. And in any case, don't accept any input.
		if dump_prefix {
			if self.debug { log!(DEBUG "Prefix sent to service"); }
			self.stdin_prefix = None;
		}
		Ok(0)
	}

	//Is there pending error?
	pub fn pending_shutdown(&self) -> bool { self.service_error.is_some() }
	//Set service error.
	pub fn set_error(&mut self, err: Cow<'static, str>) { self.service_error = Some(err); }
	//Assuming pending_shutdown()=true, Get service error.
	pub fn get_error(&self) -> Cow<'static, str>
	{
		self.service_error.as_ref().map(cloned).unwrap_or(Cow::Borrowed("Unknown error???"))
	}

	//Did route lookup fail?
	pub fn is_route_lookup_failed(&self) -> bool { self.launch_state == LaunchState::LaunchFailedNoTarget }
	//Did connect fail?
	pub fn is_connect_failed(&self) -> bool
	{
		self.launch_state == LaunchState::LaunchFailed ||
			self.launch_state == LaunchState::LaunchFailedNoTarget
	}
	//Handle event for service.
	pub fn handle_event(&mut self, poll: &mut Poll, tok: usize, kind: u8)
	{
		let mut maybe_delayed_init = false;
		let is_read = kind & (IO_WAIT_READ|IO_WAIT_HUP) != 0;
		let is_write = kind & IO_WAIT_WRITE != 0;
		let stdin_unknown = self.stdin_poll_status == PollStatus::Unknown;
		let stdout_unknown = self.stdout_poll_status == PollStatus::Unknown;
		if self.stdout_poll_token.value() == tok && is_write && self.launch_state == LaunchState::Launching {
			//We are handling delayed socket initialization, and just got notification that
			//the socket should be ready. Fetch the SO_ERROR to determine the status.
			self.handle_delayed_socket();
			maybe_delayed_init = true;
		}
		if self.stdout_poll_token.value() == tok && is_read && stdout_unknown && !maybe_delayed_init {
			//Operating system just signaled service stdout is readable.
			self.stdout_poll_status = PollStatus::Active;
			self.stdout_prev_poll = 0;
			if self.debug { log!(DEBUG "Service ready for read"); }
		}
		if self.stdin_poll_token.value() == tok && is_write && stdin_unknown {
			//Operating system just signaled service stdin is writable.
			self.stdin_poll_status = PollStatus::Active;
			if self.service_split_rw {
				self.stdin_prev_poll = 0;
			} else {
				self.stdout_prev_poll = 0;
			}
			if self.debug { log!(DEBUG "Service ready for write"); }
		}
		if self.stderr_poll_token.value() == tok && is_read {
			self.handle_service_error_event(poll);
		}
	}

	//Re-arm pollers for the service.
	pub fn retrigger_service(&mut self, poll: &mut Poll)
	{
		//We don't process the service at all on pending shutdown.
		if self.service_error.is_some() { return; }
		let trigger_stdin = self.stdin_poll_status == PollStatus::Unknown;
		let trigger_stdout = self.stdout_poll_status == PollStatus::Unknown;
		//Retrigger service polls for the stdout fd.
		let r1 = if let &Some(ref rd) = &self.stdout_fd {
			let mut ready = 0;
			if trigger_stdout { ready = ready | IO_WAIT_READ; }
			//If service_split_rw is set, stdin is not the same as stdout, and is processed below.
			if !self.service_split_rw && trigger_stdin { ready = ready | IO_WAIT_WRITE; }
			if ready == 0 { None } else { Some((rd.clone(), self.stdout_poll_token.value(), ready)) }
		} else {
			None
		};
		//Retrigger service polls for the stdin fd, if that is different from the stdout fd.
		let r2 = if let (true, &Some(ref wd)) = (self.service_split_rw, &self.stdin_fd) {
			let mut ready = 0;
			if trigger_stdin { ready = ready | IO_WAIT_WRITE; }
			if ready == 0 { None } else { Some((wd.clone(), self.stdin_poll_token.value(), ready)) }
		} else {
			None
		};
		if let Some((fd, token, ready)) = r1 { if ready != self.stdout_prev_poll {
			if self.debug { log!(DEBUG "Service stdout waiting for {ready:?}"); }
			match poll.reregister(&fd, token, ready | IO_WAIT_ONESHOT) {
				Ok(_) => self.stdout_prev_poll = ready,
				Err(err) => store_err!(self.service_error, F "poll.reregister({fd:?}) error: {err}")
			};
		}}
		if let Some((fd, token, ready)) = r2 { if ready != self.stdin_prev_poll {
			if self.debug { log!(DEBUG "Service stdin waiting for {ready:?}"); }
			match poll.reregister(&fd, token, ready | IO_WAIT_ONESHOT) {
				Ok(_) => self.stdin_prev_poll = ready,
				Err(err) => store_err!(self.service_error, F "poll.reregister({fd:?}) error: {err}")
			};
		}}
	}
	//Handle a delayed socket initialization.
	fn handle_delayed_socket(&mut self)
	{
		//In case this fails in non-fatal way, service retriggers re-arms the poll.
		let serv_stdout = if let &Some(ref x) = &self.stdout_fd { x.clone() }  else { return; };
		match serv_stdout.so_error() {
			Ok(Ok(_)) => {
				//This is used by TLS, which has no idea of success/failure better than connection
				//establishing or not.
				self.address_handle.as_ref().map(|a|a.set_status(true));
				log!(INFO "Using backend {name}", name=self.delayed_socket_name);
				self.delayed_socket_name.clear();
				serv_stdout.set_tcp_nodelay(true).ok();		//Don't care if fails.
				self.launch_state = LaunchState::Launched;
				return;
			}
			Ok(Err(err)) => {
				if !is_fatal_error(&err) { return; }
				//Connection failing is definite failure.
				self.address_handle.as_ref().map(|a|a.set_status(false));
				self.launch_state = LaunchState::LaunchFailed;
				store_err!(RET self.service_error, "Error connecting to TCP socket `{name}`: {err}",
					name=self.delayed_socket_name)
			},
			Err(err) => {
				if !is_fatal_error(&err) { return; }
				self.launch_state = LaunchState::LaunchFailed;
				store_err!(RET self.service_error, F "Error reading SO_ERROR: {err}");
			}
		}
	}
	//Handle line fragment from stderr.
	fn handle_stderr_frag(&mut self, frag: &[u8])
	{
		const MAX_ERROR_LINE: usize = 512;
		if self.stderr_buf_overflow { return; }
		let maxfrag = MAX_ERROR_LINE - self.stderr_buf.len();
		self.stderr_buf_overflow = maxfrag < frag.len();
		let frag = &frag[..min(frag.len(), maxfrag)];
		self.stderr_buf.extend_from_slice(frag);
	}
	//Handle event on service error.
	fn handle_service_error_event(&mut self, poll: &mut Poll)
	{
		//This is handled even if stream is in error.
		let mut sefd = if let &Some(ref fd) = &self.stderr_fd { fd.clone() } else { return; };
		let mut buf = [0u8; 8192];
		let r = match sefd.read(&mut buf) {
			//EOF from error. Close the stderr and unregister from polling.
			Ok(0) => return deregister_poll!(poll, replace(&mut self.stderr_fd, None)),
			Ok(x) => x,
			Err(x) => {
				//Treat fatal errors as EOF.
				if is_fatal_error(&x) {
					return deregister_poll!(poll, replace(&mut self.stderr_fd, None));
				}
				0
			}
		};
		let mut buf = &buf[..r];
		while buf.len() > 0 {
			//If there is a 10 in buffer, split at that position, concatenating with stderr_buf.
			//Otherwise, throw the rest into stderr_buf.
			let mut lfpos = None;
			for i in buf.iter().enumerate() {
				if *i.1 == 10 {
					lfpos = Some(i.0);
					break;
				}
			}
			if let Some(x) = lfpos {
				let line_remainder = &buf[..x];
				self.handle_stderr_frag(line_remainder);
				log!(WARNING "{s}", s=SafeShowByteString(&self.stderr_buf));
				self.stderr_buf_overflow = false;
				self.stderr_buf.clear();
				buf = &buf[(r + 1)..];
			} else {
				self.handle_stderr_frag(buf);
				buf = &[];
			}
		}
		//Retrigger the poll.
		match poll.reregister(&sefd, self.stderr_poll_token.value(), IO_WAIT_READ|IO_WAIT_ONESHOT) {
			Ok(_) => (),
			Err(err) => store_err!(self.service_error, F "poll.reregister error: {err}")
		};
	}
	pub fn close_link_to_service(&mut self, poll: &mut Poll) -> Result<(), Cow<'static, str>>
	{
		//Do not close stdin twice.
		if self.stdin_poll_status == PollStatus::Closed { return Ok(()); }
		if self.debug { log!(DEBUG "Closing link to service"); }
		//If we have already lost stdout, this is full service connection loss, which is fatal error.
		fail_if!(self.stdout_poll_status == PollStatus::Closed, "Connection to service lost");
		//If we have non-split fd (i.e., a socket), then shutdown writes to cause EOF. Otherwise deregister
		//stdin (no alias via stdout). In any case, dump stdin and mark it closed.
		let old_stdin = replace(&mut self.stdin_fd, None);
		if self.service_split_rw {
			deregister_poll!(poll, old_stdin);
		} else {
			old_stdin.and_then(|x|x.shutdown_write().ok());
		}
		self.stdin_poll_status = PollStatus::Closed;
		Ok(())
	}
	//Launch the service.
	pub fn launch_service_ex(&mut self, poll: &mut Poll, service: &LookedUpService,
		params: ServiceLaunchParameters, addtoken: &mut AddTokenCallback, eparams: Option<&LaunchInfoExtended>)
	{
		//If access is restricted, check for access. Note that access to ACME can not be restricted.
		if !service.check_access(params.spkihash, params.ignore_access_control) {
			self.access_denied = true;
			self.launch_state = LaunchState::LaunchFailedAccessDenied;
			store_err!(RET self.service_error, "Access denied for SNI '{sni}' ALPN '{alpn}'",
				sni=params.sni, alpn=unwrap_alpn(&params.routeas_alpn))
		}
		//Special direct file descriptor. These do not set TCP_NODELAY.
		match service.direct_fd_ex(poll, &params, addtoken, eparams) {
			Some(Ok((fd, version, sname))) => {
				//Special creation successful.
				show_conninfo(&params);
				self.launch_service_socket_fd(poll, fd, version, params, &sname, false);
				return self.set_launch_state_post_launch();
			},
			Some(Err(err)) => store_err!(RET self.service_error, F "Error connecting to service: {err}"),
			None => (),	//Do nothing.
		};
		//Choose a backend.
		let service = match service.choose_service() {
			Some(x) => x,
			None => {
				self.launch_state = LaunchState::LaunchFailedNoTarget;
				store_err!(RET self.service_error,
					"No backends available for SNI '{sni}' ALPN '{alpn}'",
					sni=params.sni, alpn=unwrap_alpn(&params.routeas_alpn))
			}
		};
		show_conninfo(&params);
		match service.deref() {
			&Service::Gsocket(ref x, version) => self.launch_service_gsock(poll,
				x.borrow_addr(), x.borrow_status().clone(), version, params),
			&Service::Command(ref x) => {
				if self.global_service_cache.exec_is_allowed() {
					self.launch_service_command(poll, x, params)
				} else {
					store_err!(RET self.service_error,
						"Bad handler for SNI '{sni}': Commands are not allowed as targets",
						sni=params.sni)
				}
			}
		};
		self.set_launch_state_post_launch();
	}
	//Launch the service with explicit sokcet target.
	pub fn launch_service_explicit(&mut self, poll: &mut Poll, target: &SocketAddrEx, info: ConnectionInfo)
	{
		let p = ServiceLaunchParameters {
			info: info,
			tlsver: 0,
			sni: "",
			conninfo: "explicit cutthrough",
			raw_alpn: "",
			routeas_alpn: "",
			spkihash: None,
			cn: None,
			ignore_access_control: false,	//No access control for cut-through.
		};
		//Version is always 0 (no PROXY). This does not perform any health detection, so address handle is
		//always a dummy one.
		self.launch_service_gsock(poll, &target, AddressHandle::dummy(), 0, p);
		self.set_launch_state_post_launch();
	}
	fn set_launch_state_post_launch(&mut self)
	{
		if self.service_error.is_some() && !self.is_connect_failed() {
			//If service_error is set after launch, assume connect failed. However, do not overwrite
			//status codes that already mean launch failed.
			self.launch_state = LaunchState::LaunchFailed;
		}
	}
	//Launch a generic socket (PROXY) service.
	fn launch_service_gsock<'a>(&mut self, poll: &mut Poll, addr: &SocketAddrEx, ahandle: AddressHandle,
		version: u32, p: ServiceLaunchParameters<'a>)
	{
		self.address_handle = Some(ahandle);
		let sname = addr.to_string();
		//Create socket fd.
		let af = match addr.get_family() {
			Some(x) => x,
			None => store_err!(RET self.service_error, F
				"Failed to create socket `{sname}`: Unknown address type")
		};
		let flags = SocketFlags::NONBLOCK|SocketFlags::CLOEXEC;
		let fd = match FileDescriptor::socket2(af, SocketType::STREAM, Default::default(), flags) {
			Ok(x) => x,
			Err(err) => store_err!(RET self.service_error, F "Failed to create socket `{sname}`: {err}")
		};
		let delayed = match fd.connect_ex(addr) {
			Ok(true) => {
				//Successful connection, take as success because there is no finer success
				//condition at TLS level.
				self.address_handle.as_ref().map(|a|a.set_status(true));
				false
			},
			Ok(false) => true,
			Err(err) => {
				//Connection failing is definite failure.
				self.address_handle.as_ref().map(|a|a.set_status(false));
				store_err!(RET self.service_error, F "Failed to connect to `{sname}`: {err}");
			}
		};
		if !delayed {
			fd.set_tcp_nodelay(true).ok();		//Don't care if fails.
			log!(INFO "Using backend {sname}");
		}
		self.launch_service_socket_fd(poll, fd, version, p, &sname, delayed);
	}
	fn launch_service_socket_fd<'a>(&mut self, poll: &mut Poll, fd: FileDescriptor, version: u32,
		p: ServiceLaunchParameters<'a>, sname: &str, delayed: bool)
	{
		let tmp;
		let tmp2;
		let proxy_header = match version {
			0 => &[],
			1 => {
				tmp = format_proxyv1_header(&p.info);
				tmp.as_bytes()
			},
			2 => {
				tmp2 = format_proxyv2_header(&p);
				&tmp2[..]
			},
			ver => store_err!(RET self.service_error, F
				"Illegal proxy version {ver} on socket `{sname}`")
		};
		let fd1 = fd.clone();
		let fd2 = fd;
		self.launch_service_finish(poll, fd1, fd2, proxy_header, if delayed { Some(sname.to_owned())
			} else { None });
	}
	//Launch a command service.
	fn launch_service_command<'a>(&mut self, poll: &mut Poll, name: &[String], p: ServiceLaunchParameters<'a>)
	{
		let (srcaddr, srcport) = format_ipv6_address(&p.info.src);
		let (dstaddr, dstport) = format_ipv6_address(&p.info.dest);
		let no_tls = p.tlsver == 0;
		let tlsver = p.tlsver.to_string();
		if name.len() == 0 {
			store_err!(RET self.service_error, "Empty command specified");
		}
		let mut cmd = Command::new(&name[0]);
		for i in name.iter().skip(1) { cmd.arg(i); }
		cmd.stdin(Stdio::piped());
		cmd.stdout(Stdio::piped());
		cmd.stderr(Stdio::piped());
		cmd.env("BTLS_INETD_SRC_ADDR", &srcaddr);
		cmd.env("BTLS_INETD_DST_ADDR", &dstaddr);
		cmd.env("BTLS_INETD_SRC_PORT", &srcport.to_string());
		cmd.env("BTLS_INETD_DST_PORT", &dstport.to_string());
		if !no_tls {
			cmd.env("BTLS_INETD_TLS_VERSION", &tlsver);
		}
		cmd.env("BTLS_INETD_SNI", p.sni.deref());
		if p.raw_alpn.deref() != "" { cmd.env("BTLS_INETD_ALPN", p.raw_alpn.deref()); }
		//CN is always supposed to be valid unicode.
		if let Some(ref cn) = p.cn.and_then(|cn|{
			from_utf8(cn).ok()
		}) {
			cmd.env("BTLS_INETD_CLIENT_CN", cn);
		}
		if let Some(ref spkihash) = p.spkihash {
			let out = Hexdump(spkihash).to_string();
			cmd.env("BTLS_INETD_CLIENT_SPKI_SHA256", out.deref());
		}
		let mut handle = match cmd.spawn() {
			Ok(x) => x,
			Err(err) => store_err!(RET self.service_error, F "Failed to execute command: {err}")
		};
		let stdin = replace(&mut handle.stdin, None);
		let stdout = replace(&mut handle.stdout, None);
		let stderr = replace(&mut handle.stderr, None);
		let (stdin, stdout, stderr) = match (stdin, stdout, stderr) {
			(Some(x), Some(y), Some(z)) => (x, y, z),
			_ => store_err!(RET self.service_error, "Failed to get child file descriptors")
		};
		let w_stdin = FileDescriptor::new(stdin.into_raw_fd());
		let w_stdout = FileDescriptor::new(stdout.into_raw_fd());
		let w_stderr = FileDescriptor::new(stderr.into_raw_fd());
		self.launch_service_finish(poll, w_stdout, w_stdin, &[], None);
		if self.service_error.is_some() { return; }
		self.stderr_fd = Some(w_stderr);
		match poll.register(&self.stderr_fd.clone().unwrap(), self.stderr_poll_token.value(),
			IO_WAIT_READ|IO_WAIT_ONESHOT) {
			Ok(_) => (),
			Err(err) => store_err!(self.service_error, F "poll.register error: {err}")
		};
	}
	//Note: if delayed is not none, rfd must equal wfd.
	fn launch_service_finish(&mut self, poll: &mut Poll, rfd: FileDescriptor, wfd: FileDescriptor,
		initial_data: &[u8], delayed: Option<String>)
	{
		//We don't process the service at all on pending shutdown.
		if self.service_error.is_some() { return; }
		//We need to register the file descriptors. Take note if the fds are the same or not.
		//Also, write possible initial data into unsent data buffer, so we prepend it to material sent.
		self.stdout_fd = Some(rfd.clone());
		self.stdin_fd = Some(wfd.clone());
		self.service_split_rw = rfd != wfd;
		if initial_data.len() > 0 { self.stdin_prefix = Some((initial_data.to_owned(), 0)); }
		//If socket is delayed, save the name. Otherwise declare the launch complete.
		if let Some(delayed) = delayed {
			self.launch_state = LaunchState::Launching;
			self.delayed_socket_name = delayed;
		} else {
			self.launch_state = LaunchState::Launched;
		}
		//If not split, the read/write share tokens.
		if !self.service_split_rw { self.stdin_poll_token = self.stdout_poll_token.clone(); }

		//The initial state is always waiting for write, and waiting for read if t_out_ready is asserted.
		//Note that any errors here trigger pending error instead of immediate error, as they are
		//interpretted as service errors.
		let r1 = {
			let mut ready = 0;
			//If not split rw, register for write. This is either needed for launching or
			//registration.
			if !self.service_split_rw { ready = ready | IO_WAIT_WRITE; }
			//If not launching, register read.
			if self.launch_state != LaunchState::Launching { ready = ready | IO_WAIT_READ; }
			if ready == 0 { None } else { Some((rfd, self.stdout_poll_token.value(), ready)) }
		};
		let r2 = if self.service_split_rw {
			Some((wfd, self.stdin_poll_token.value(), IO_WAIT_WRITE))
		} else {
			None
		};
		if let Some((fd, token, ready)) = r1 {
			match poll.register(&fd, token, ready | IO_WAIT_ONESHOT) {
				Ok(_) => (),
				Err(err) => store_err!(RET self.service_error, F "poll.register error: {err}")
			};
		}
		if let Some((fd, token, ready)) = r2 {
			match poll.register(&fd, token, ready | IO_WAIT_ONESHOT) {
				Ok(_) => (),
				Err(err) => store_err!(RET self.service_error, F "poll.register error: {err}")
			};
		}
	}
	pub fn get_tokens(&self) -> Vec<AllocatedToken>
	{
		//Always return the original stdin token.
		let mut tokens = Vec::new();
		tokens.push(self.stdout_poll_token.clone());
		tokens.push(self.stdin_poll_token_orig.clone());
		tokens.push(self.stderr_poll_token.clone());
		tokens
	}
	pub fn remove_pollers(&mut self, poll: &mut Poll)
	{
		deregister_poll!(poll, replace(&mut self.stdout_fd, None));
		//The stdin and stdout can be one or two file descriptors.
		if self.service_split_rw { deregister_poll!(poll, replace(&mut self.stdin_fd, None)); }
		deregister_poll!(poll, replace(&mut self.stderr_fd, None));
	}
	pub fn is_access_denied(&self) -> bool
	{
		self.access_denied
	}
}
