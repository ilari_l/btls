use std::sync::Arc;
use std::sync::atomic::AtomicPtr;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering;

pub const SECPTR_ELEMENTS: usize = 256;

#[derive(Copy,Clone)]
enum State
{
	Init,
	Active,
	CutThrough,
	ServiceFailed,
	ShutdownHold,
	Shutdown,
}

macro_rules! int_to
{
	($symbol:ident $state:expr) => {{
		Self::__decrement(&$state.0, $state.1);
		$state.1 = State::$symbol;
		Self::__increment(&$state.0, $state.1);
		$state
	}}
}

macro_rules! trans_meth
{
	($meth:ident $xtype:ident) => {
		pub fn $meth(self) -> $xtype { $xtype((self.0).$meth()) }
	}
}

struct _RaiiInternal(Arc<GlobalMetrics>, State);

impl _RaiiInternal
{
	fn __getsym<'a>(x: &'a Arc<GlobalMetrics>, y: State) -> &'a AtomicUsize
	{
		match y {
			State::Init => &x.state_init,
			State::Active => &x.state_active,
			State::CutThrough => &x.state_cut_through,
			State::ServiceFailed => &x.state_service_failed,
			State::ShutdownHold => &x.state_shutdown_hold,
			State::Shutdown => &x.state_shutdown,
		}
	}
	fn __decrement(x: &Arc<GlobalMetrics>, y: State) { Self::__getsym(x, y).fetch_sub(1, Ordering::Relaxed); }
	fn __increment(x: &Arc<GlobalMetrics>, y: State) { Self::__getsym(x, y).fetch_add(1, Ordering::Relaxed); }
	fn new(x: Arc<GlobalMetrics>, y: State) -> _RaiiInternal
	{
		Self::__increment(&x, y);
		_RaiiInternal(x, y)
	}
	fn to_active(mut self) -> Self { int_to!(Active self) }
	fn to_cut_through(mut self) -> Self { int_to!(CutThrough self) }
	fn to_service_failed(mut self) -> Self { int_to!(ServiceFailed self) }
	fn to_shutdown_hold(mut self) -> Self { int_to!(ShutdownHold self) }
	fn to_shutdown(mut self) -> Self { int_to!(Shutdown self) }
	fn count_active_trans(&self) { self.0.active_transitions.fetch_add(1, Ordering::Relaxed); }
	fn count_cut_through_trans(&self) { self.0.ct_transitions.fetch_add(1, Ordering::Relaxed); }
}

impl Drop for _RaiiInternal
{
	fn drop(&mut self) { Self::__decrement(&self.0, self.1); }
}

pub struct RaiiInit(_RaiiInternal);
impl RaiiInit
{
	pub fn new(global_metrics: Arc<GlobalMetrics>) -> RaiiInit
	{
		RaiiInit(_RaiiInternal::new(global_metrics, State::Init))
	}
	trans_meth!(to_shutdown RaiiShutdown);
	trans_meth!(to_service_failed RaiiServiceFailed);
	pub fn to_active(self) -> RaiiActive
	{
		self.0.count_active_trans();
		RaiiActive(self.0.to_active())
	}
	pub fn to_cut_through(self) -> RaiiCutThrough
	{
		self.0.count_cut_through_trans();
		RaiiCutThrough(self.0.to_cut_through())
	}
}

pub struct RaiiActive(_RaiiInternal);

impl RaiiActive
{
	trans_meth!(to_shutdown RaiiShutdown);
	trans_meth!(to_service_failed RaiiServiceFailed);
}

pub struct RaiiCutThrough(_RaiiInternal);

impl RaiiCutThrough
{
	trans_meth!(to_shutdown RaiiShutdown);
	trans_meth!(to_service_failed RaiiServiceFailed);
}

pub struct RaiiServiceFailed(_RaiiInternal);

impl RaiiServiceFailed
{
	trans_meth!(to_shutdown RaiiShutdown);
	trans_meth!(to_shutdown_hold RaiiShutdownHold);
}

pub struct RaiiShutdownHold(_RaiiInternal);

impl RaiiShutdownHold
{
	trans_meth!(to_shutdown RaiiShutdown);
}

pub struct RaiiShutdown(_RaiiInternal);

impl RaiiShutdown
{
}

macro_rules! define_tls_gmetrics
{
	($([$value:tt $symbol:ident $variable:ident];)*) => {
		#[derive(Default)]
		pub struct GlobalMetrics
		{
			$(pub $variable: AtomicUsize,)*
			//Divide cipher_flags to nested array to work around Rust limiations.
			pub cipher_flags: [[AtomicUsize;16];16],
			pub cipher_flags_s: AtomicPtr<[AtomicUsize;SECPTR_ELEMENTS]>,
		}
	}
}

//Define the metrics.
include!("metrics.inc");

impl GlobalMetrics
{
	pub fn new() -> GlobalMetrics { GlobalMetrics {..Default::default()} }
	pub fn read_tcp(&self, size: usize) { self.tcp_read.fetch_add(size, Ordering::Relaxed); }
	pub fn write_tcp(&self, size: usize) { self.tcp_write.fetch_add(size, Ordering::Relaxed); }
	pub fn read_service(&self, size: usize) { self.serv_read.fetch_add(size, Ordering::Relaxed); }
	pub fn write_service(&self, size: usize) { self.serv_write.fetch_add(size, Ordering::Relaxed); }
	pub fn insecure_truncation(&self) { self.insecure_truncations.fetch_add(1, Ordering::Relaxed); }
	pub fn normal_close(&self) { self.normal_closes.fetch_add(1, Ordering::Relaxed); }
	pub fn service_error_nodata(&self) { self.service_error_nodatas.fetch_add(1, Ordering::Relaxed); }
	pub fn service_error(&self) { self.service_errors.fetch_add(1, Ordering::Relaxed); }
	pub fn poll_error(&self) { self.poll_errors.fetch_add(1, Ordering::Relaxed); }
	pub fn tls_error(&self) { self.tls_errors.fetch_add(1, Ordering::Relaxed); }
	pub fn transport_error(&self) { self.transport_errors.fetch_add(1, Ordering::Relaxed); }
	pub fn timeout_error(&self) { self.timeout_errors.fetch_add(1, Ordering::Relaxed); }
	pub fn service_launch_error(&self) { self.service_launch_errors.fetch_add(1, Ordering::Relaxed); }
	pub fn initialize_error(&self) { self.initialize_errors.fetch_add(1, Ordering::Relaxed); }
	pub fn missing_service(&self) { self.missing_services.fetch_add(1, Ordering::Relaxed); }
	pub fn redirected_acme(&self) { self.redirected_acmes.fetch_add(1, Ordering::Relaxed); }
	pub fn launch_failed_no_target(&self) { self.lfate_configerr.fetch_add(1, Ordering::Relaxed); }
	pub fn launch_failed_error(&self) { self.lfate_connecterr.fetch_add(1, Ordering::Relaxed); }
	pub fn launch_failed_other(&self) { self.lfate_unknown.fetch_add(1, Ordering::Relaxed); }
	pub fn launch_failed_denied(&self) { self.lfate_denied.fetch_add(1, Ordering::Relaxed); }
	pub fn launch_successful(&self) { self.lfate_success.fetch_add(1, Ordering::Relaxed); }
	pub fn bad_cipher(&self) { self.bad_cipher.fetch_add(1, Ordering::Relaxed); }
	pub fn missing_handler(&self) { self.handler_missing.fetch_add(1, Ordering::Relaxed); }
	pub fn faulty_handler(&self) { self.handler_faulty.fetch_add(1, Ordering::Relaxed); }
	pub fn chello_error(&self) { self.ch_parse_error.fetch_add(1, Ordering::Relaxed); }
	pub fn commit_post_quantum(&self) { self.commit_pq.fetch_add(1, Ordering::Relaxed); }
	pub fn cipher_flags(&self, i: u32)
	{
		let i = i as usize;
		if let Some(arr) = self.cipher_flags.get(i / 16) {
			if let Some(arr) = arr.get(i % 16) { arr.fetch_add(1, Ordering::Relaxed); }
		}
		//If secondary cipherflags are set, increment that too.
		let secptr = self.cipher_flags_s.load(Ordering::Relaxed);
		if !secptr.is_null() && i < SECPTR_ELEMENTS { unsafe {
			if let Some(entry) = (*secptr).get(i) { entry.fetch_add(1, Ordering::AcqRel); }
		}}
	}
	pub fn set_secondary_ciphermetrics(&self, cm: &'static [AtomicUsize;SECPTR_ELEMENTS])
	{
		self.cipher_flags_s.store(cm as *const _ as *mut _, Ordering::Relaxed);
	}
}
