pub(crate) const CP_VERY_BAD: u128 = 1 << 0 | CP_BLACKLIST;		//This is something very bad.
pub(crate) const CP_BLACKLIST: u128 = 1 << 1;				//Blacklisted.
pub(crate) const CP_GREASE: u128 = 1 << 2;				//Grease.
pub(crate) const CP_PRIVATE_USE: u128 = 1 << 3;				//Private use.
pub(crate) const CP_RESERVED: u128 = 1 << 4;				//Reserved codepoint.
pub(crate) const CP_UNASSIGNED: u128 = 1 << 5;				//Unassigned codepoint.
pub(crate) const CP_SCSV: u128 = 1 << 6;				//Signaling CipherSuite Value.
pub(crate) const CP_STATIC_RSA: u128 = 1 << 7;				//Static RSA key exchange.
pub(crate) const CP_STATIC_DH: u128 = 1 << 8;				//Static (EC)DH key exchange.
pub(crate) const CP_PSK: u128 = 1 << 9;					//Pure PSK
pub(crate) const CP_DHE: u128 = 1 << 10;				//DHE.
pub(crate) const CP_ECDHE: u128 = 1 << 11;				//ECDHE.
pub(crate) const CP_KRB5: u128 = 1 << 12;				//Kerberos5.
pub(crate) const CP_SRP: u128 = 1 << 13;				//SRP
pub(crate) const CP_ECCPWD: u128 = 1 << 14;				//ECCPWD (Dragonfly)
pub(crate) const CP_AUTH_ANON: u128 = 1 << 15;				//Anon auth.
pub(crate) const CP_AUTH_PSK: u128 = 1 << 16;				//PSK auth.
pub(crate) const CP_AUTH_RSA: u128 = 1 << 17;				//RSA auth.
pub(crate) const CP_AUTH_DSS: u128 = 1 << 18;				//DSS auth.
pub(crate) const CP_AUTH_ECDSA: u128 = 1 << 19;				//ECDSA auth.
pub(crate) const CP_MAC_MD5: u128 = 1 << 20;				//HMAC-MD5
pub(crate) const CP_MAC_SHORT: u128 = 1 << 21;				//MAC with short tag.
pub(crate) const CP_ENC_NULL: u128 = 1 << 22 | CP_BLACKLIST;		//NULL "encryption".
pub(crate) const CP_ENC_RC4: u128 = 1 << 23 | CP_VERY_BAD;		//RC4 "encryption".
pub(crate) const CP_ENC_IDEA: u128 = 1 << 24 | CP_BLACKLIST;		//IDEA "encryption".
pub(crate) const CP_ENC_3DES: u128 = 1 << 25;				//Triple DES encryption.
pub(crate) const CP_ENC_AES_BLOCK: u128 = 1 << 26;			//Block AES encryption.
pub(crate) const CP_ENC_CAMELLIA_BLOCK: u128 = 1 << 27;			//Block Camellia encryption.
pub(crate) const CP_ENC_SEED: u128 = 1 << 28;				//Block SEED encryption.
pub(crate) const CP_ENC_AES_GCM: u128 = 1 << 29;			//AES-GCM encryption.
pub(crate) const CP_ENC_ARIA_BLOCK: u128 = 1 << 30;			//Block ARIA encryption.
pub(crate) const CP_ENC_ARIA_GCM: u128 = 1 << 31;			//ARIA-GCM encryption.
pub(crate) const CP_ENC_CAMELLIA_GCM: u128 = 1 << 32;			//CAMELLIA-GCM encryption.
pub(crate) const CP_ENC_AES_CCM: u128 = 1 << 33;			//AES-CCM encryption.
pub(crate) const CP_ENC_CHACHA: u128 = 1 << 34;				//Chacha encryption.
pub(crate) const CP_ENC_SM4: u128 = 1 << 35;				//SM4 encryption.
pub(crate) const CP_ENC_KUZNYECHIK: u128 = 1 << 36 | CP_BLACKLIST;	//KUZNYECHIK "encryption".
pub(crate) const CP_ENC_MAGMA: u128 = 1 << 37 | CP_BLACKLIST;		//Magma encryption.
pub(crate) const CP_GRP_BINARY: u128 = 1 << 38;				//Binary ECC group.
pub(crate) const CP_GRP_P256: u128 = 1 << 39 | CP_MODERN_GROUP;
pub(crate) const CP_GRP_P384: u128 = 1 << 40 | CP_MODERN_GROUP;
pub(crate) const CP_GRP_P521: u128 = 1 << 41 | CP_MODERN_GROUP;
pub(crate) const CP_GRP_BRAINPOOL: u128 = 1 << 42;
pub(crate) const CP_GRP_X25519: u128 = 1 << 43 | CP_MODERN_GROUP;
pub(crate) const CP_GRP_X448: u128 = 1 << 44 | CP_MODERN_GROUP;
pub(crate) const CP_GRP_GOST: u128 = 1 << 45;
pub(crate) const CP_GRP_SM2: u128 = 1 << 46;
pub(crate) const CP_GRP_FFDHE: u128 = 1 << 47;
pub(crate) const CP_GRP_CUSTOM: u128 = 1 << 48;
pub(crate) const CP_SIG_MD5: u128 = 1 << 49 | CP_VERY_BAD;
pub(crate) const CP_SIG_SHA1: u128 = 1 << 50;
pub(crate) const CP_SIG_SHA224: u128 = 1 << 51;
pub(crate) const CP_SIG_RSA: u128 = 1 << 52;
pub(crate) const CP_SIG_DSS: u128 = 1 << 53;
pub(crate) const CP_SIG_ECDSA: u128 = 1 << 54;
pub(crate) const CP_SIG_GOST: u128 = 1 << 55;
pub(crate) const CP_SIG_RSA_PSS: u128 = 1 << 56;
pub(crate) const CP_SIG_SM3: u128 = 1 << 57;
pub(crate) const CP_SIG_IBC: u128 = 1 << 58;
pub(crate) const CP_SIG_ED25519: u128 = 1 << 59 | CP_MODERN_SIG;
pub(crate) const CP_SIG_ED448: u128 = 1 << 60 | CP_MODERN_SIG;
pub(crate) const CP_CRYPT_BROKEN_2000: u128 = 1 << 61 | CP_VERY_BAD;
pub(crate) const CP_SAMPLE: u128 = 1 << 62;
pub(crate) const CP_TLS13: u128 = 1 << 63;
pub(crate) const CP_KEX_GOST: u128 = 1 << 64;
pub(crate) const CP_MODERN_CIPHER: u128 = 1 << 65;
pub(crate) const CP_MODERN_GROUP: u128 = 1 << 66;
pub(crate) const CP_MODERN_SIG: u128 = 1 << 67;
pub(crate) const CP_ENC_OLDCHACHA: u128 = 1 << 68;
pub(crate) const CP_SAMPLE2: u128 = 1 << 69 | CP_SAMPLE;
pub(crate) const CP_ENC_AEGIS: u128 = 1 << 70;				//AEGIS.
pub(crate) const CP_SAMPLE3: u128 = 1 << 71 | CP_SAMPLE2;
pub(crate) const CP_POST_QUANTUM: u128 = 1 << 72 | CP_MODERN_GROUP;
pub(crate) const CP_SAMPLE4: u128 = 1 << 73 | CP_SAMPLE3;


pub(crate) fn ciphersuite_properties(cs: u16) -> (u16, u128)
{
	if cs % 4112 == 2570 { return (cs, CP_GREASE); }
	if cs >> 8 == 255 { return (cs, CP_PRIVATE_USE); }
	(cs, match cs {
		0x0001 => CP_STATIC_RSA|CP_ENC_NULL|CP_MAC_MD5,
		0x0002 => CP_STATIC_RSA|CP_ENC_NULL,
		0x0003|0x0006 => CP_STATIC_RSA|CP_CRYPT_BROKEN_2000|CP_MAC_MD5,
		0x0004 => CP_STATIC_RSA|CP_ENC_RC4|CP_MAC_MD5,
		0x0005 => CP_STATIC_RSA|CP_ENC_RC4,
		0x0007 => CP_STATIC_RSA|CP_ENC_IDEA,
		0x0008|0x0009 => CP_STATIC_RSA|CP_CRYPT_BROKEN_2000,
		0x000A => CP_STATIC_RSA|CP_ENC_3DES,
		0x000B|0x000C => CP_STATIC_DH|CP_AUTH_DSS|CP_CRYPT_BROKEN_2000,
		0x000D => CP_STATIC_DH|CP_AUTH_DSS|CP_ENC_3DES,
		0x000E|0x000F => CP_STATIC_DH|CP_AUTH_RSA|CP_CRYPT_BROKEN_2000,
		0x0010 => CP_STATIC_DH|CP_AUTH_RSA|CP_ENC_3DES,
		0x0011|0x0012 => CP_DHE|CP_AUTH_DSS|CP_CRYPT_BROKEN_2000,
		0x0013 => CP_DHE|CP_AUTH_DSS|CP_ENC_3DES,
		0x0014|0x0015 => CP_DHE|CP_AUTH_RSA|CP_CRYPT_BROKEN_2000,
		0x0016 => CP_DHE|CP_AUTH_RSA|CP_ENC_3DES,
		0x0017 => CP_DHE|CP_AUTH_ANON|CP_CRYPT_BROKEN_2000|CP_MAC_MD5,
		0x0018 => CP_DHE|CP_AUTH_ANON|CP_ENC_RC4|CP_MAC_MD5,
		0x0019|0x001A => CP_DHE|CP_AUTH_ANON|CP_CRYPT_BROKEN_2000,
		0x001B => CP_DHE|CP_AUTH_ANON|CP_ENC_3DES,
		0x001E|0x0026..=0x0028 => CP_KRB5|CP_CRYPT_BROKEN_2000,
		0x001F => CP_KRB5|CP_ENC_3DES,
		0x0020 => CP_KRB5|CP_ENC_RC4,
		0x0021 => CP_KRB5|CP_ENC_IDEA,
		0x0022|0x0029..=0x002B => CP_KRB5|CP_CRYPT_BROKEN_2000|CP_MAC_MD5,
		0x0023 => CP_KRB5|CP_ENC_3DES|CP_MAC_MD5,
		0x0024 => CP_KRB5|CP_ENC_RC4|CP_MAC_MD5,
		0x0025 => CP_KRB5|CP_ENC_IDEA|CP_MAC_MD5,
		0x002C => CP_PSK|CP_ENC_NULL,
		0x002D => CP_DHE|CP_AUTH_PSK|CP_ENC_NULL,
		0x002E => CP_STATIC_RSA|CP_AUTH_PSK|CP_ENC_NULL,
		0x002F|0x0035|0x003C|0x003D => CP_STATIC_RSA|CP_ENC_AES_BLOCK,
		0x0030|0x0036|0x003E|0x0068 => CP_STATIC_DH|CP_AUTH_DSS|CP_ENC_AES_BLOCK,
		0x0031|0x0037|0x003F|0x0069 => CP_STATIC_DH|CP_AUTH_RSA|CP_ENC_AES_BLOCK,
		0x0032|0x0038|0x0040|0x006A => CP_DHE|CP_AUTH_DSS|CP_ENC_AES_BLOCK,
		0x0033|0x0039|0x0067|0x006B => CP_DHE|CP_AUTH_RSA|CP_ENC_AES_BLOCK,
		0x0034|0x003A|0x006C|0x006D => CP_DHE|CP_AUTH_ANON|CP_ENC_AES_BLOCK,
		0x003B => CP_STATIC_RSA|CP_ENC_NULL,
		0x0041|0x0084|0x00BA|0x00C0 => CP_STATIC_RSA|CP_ENC_CAMELLIA_BLOCK,
		0x0042|0x0085|0x00BB|0x00C1 => CP_STATIC_DH|CP_AUTH_DSS|CP_ENC_CAMELLIA_BLOCK,
		0x0043|0x0086|0x00BC|0x00C2 => CP_STATIC_DH|CP_AUTH_RSA|CP_ENC_CAMELLIA_BLOCK,
		0x0044|0x0087|0x00BD|0x00C3 => CP_DHE|CP_AUTH_DSS|CP_ENC_CAMELLIA_BLOCK,
		0x0045|0x0088|0x00BE|0x00C4 => CP_DHE|CP_AUTH_RSA|CP_ENC_CAMELLIA_BLOCK,
		0x0046|0x0089|0x00BF|0x00C5 => CP_DHE|CP_AUTH_ANON|CP_ENC_CAMELLIA_BLOCK,
		//0x005D-0x005F Unassigned
		//0x006E-0x0083 Unassigned
		0x008A => CP_PSK|CP_ENC_RC4,
		0x008B => CP_PSK|CP_ENC_3DES,
		0x008C|0x008D|0x00AE|0x00AF => CP_PSK|CP_ENC_AES_BLOCK,
		0x008E => CP_DHE|CP_AUTH_PSK|CP_ENC_RC4,
		0x008F => CP_DHE|CP_AUTH_PSK|CP_ENC_3DES,
		0x0090|0x0091|0x00B2|0x00B3 => CP_DHE|CP_AUTH_PSK|CP_ENC_AES_BLOCK,
		0x0092 => CP_STATIC_RSA|CP_AUTH_PSK|CP_ENC_RC4,
		0x0093 => CP_STATIC_RSA|CP_AUTH_PSK|CP_ENC_3DES,
		0x0094|0x0095|0x00B6|0x00B7 => CP_STATIC_RSA|CP_AUTH_PSK|CP_ENC_AES_BLOCK,
		0x0096 => CP_STATIC_RSA|CP_ENC_SEED,
		0x0097 => CP_STATIC_DH|CP_AUTH_DSS|CP_ENC_SEED,
		0x0098 => CP_STATIC_DH|CP_AUTH_RSA|CP_ENC_SEED,
		0x0099 => CP_DHE|CP_AUTH_DSS|CP_ENC_SEED,
		0x009A => CP_DHE|CP_AUTH_RSA|CP_ENC_SEED,
		0x009B => CP_DHE|CP_AUTH_ANON|CP_ENC_SEED,
		0x009C|0x009D => CP_STATIC_RSA|CP_ENC_AES_GCM,
		0x009E|0x009F => CP_DHE|CP_AUTH_RSA|CP_ENC_AES_GCM,
		0x00A0|0x00A1 => CP_STATIC_DH|CP_AUTH_RSA|CP_ENC_AES_GCM,
		0x00A2|0x00A3 => CP_DHE|CP_AUTH_DSS|CP_ENC_AES_GCM,
		0x00A4|0x00A5 => CP_STATIC_DH|CP_AUTH_DSS|CP_ENC_AES_GCM,
		0x00A6|0x00A7 => CP_DHE|CP_AUTH_ANON|CP_ENC_AES_GCM,
		0x00A8|0x00A9 => CP_PSK|CP_ENC_AES_GCM,
		0x00AA|0x00AB => CP_DHE|CP_AUTH_PSK|CP_ENC_AES_GCM,
		0x00AC|0x00AD => CP_STATIC_RSA|CP_AUTH_PSK|CP_ENC_AES_GCM,
		0x00B0|0x00B1 => CP_PSK|CP_ENC_NULL,
		0x00B4|0x00B5 => CP_DHE|CP_AUTH_PSK|CP_ENC_NULL,
		0x00B8|0x00B9 => CP_STATIC_RSA|CP_AUTH_PSK|CP_ENC_NULL,
		0x00C6|0x00C7 => CP_ENC_SM4|CP_TLS13,
		//0x00C8-0x00FE Unassigned
		//0x0100-0x1300 Unassigned
		0x1301|0x1302 => CP_ENC_AES_GCM|CP_TLS13|CP_MODERN_CIPHER,
		0x1303 => CP_ENC_CHACHA|CP_TLS13|CP_MODERN_CIPHER,
		0x1304 => CP_ENC_AES_CCM|CP_TLS13,
		0x1305 => CP_ENC_AES_CCM|CP_TLS13|CP_MAC_SHORT,
		0x1306|0x1307 => CP_ENC_AEGIS|CP_TLS13,
		//0x1308-0x55FF Unassigned
		//0x5601-0xC000 Unassigned
		0xC001 => CP_STATIC_DH|CP_AUTH_ECDSA|CP_ENC_NULL,
		0xC002 => CP_STATIC_DH|CP_AUTH_ECDSA|CP_ENC_RC4,
		0xC003 => CP_STATIC_DH|CP_AUTH_ECDSA|CP_ENC_3DES,
		0xC004|0xC005|0xC025|0xC026 => CP_STATIC_DH|CP_AUTH_ECDSA|CP_ENC_AES_BLOCK,
		0xC006 => CP_ECDHE|CP_AUTH_ECDSA|CP_ENC_NULL,
		0xC007 => CP_ECDHE|CP_AUTH_ECDSA|CP_ENC_RC4,
		0xC008 => CP_ECDHE|CP_AUTH_ECDSA|CP_ENC_3DES,
		0xC009|0xC00A|0xC023|0xC024 => CP_ECDHE|CP_AUTH_ECDSA|CP_ENC_AES_BLOCK,
		0xC00B => CP_STATIC_DH|CP_AUTH_RSA|CP_ENC_NULL,
		0xC00C => CP_STATIC_DH|CP_AUTH_RSA|CP_ENC_RC4,
		0xC00D => CP_STATIC_DH|CP_AUTH_RSA|CP_ENC_3DES,
		0xC00E|0xC00F|0xC029|0xC02A => CP_STATIC_DH|CP_AUTH_RSA|CP_ENC_AES_BLOCK,
		0xC010 => CP_ECDHE|CP_AUTH_RSA|CP_ENC_NULL,
		0xC011 => CP_ECDHE|CP_AUTH_RSA|CP_ENC_RC4,
		0xC012 => CP_ECDHE|CP_AUTH_RSA|CP_ENC_3DES,
		0xC013|0xC014|0xC027|0xC028 => CP_ECDHE|CP_AUTH_RSA|CP_ENC_AES_BLOCK,
		0xC015 => CP_ECDHE|CP_AUTH_ANON|CP_ENC_NULL,
		0xC016 => CP_ECDHE|CP_AUTH_ANON|CP_ENC_RC4,
		0xC017 => CP_ECDHE|CP_AUTH_ANON|CP_ENC_3DES,
		0xC018|0xC019 => CP_ECDHE|CP_AUTH_ANON|CP_ENC_AES_BLOCK,
		0xC01A => CP_SRP|CP_ENC_3DES,
		0xC01B => CP_SRP|CP_AUTH_RSA|CP_ENC_3DES,
		0xC01C => CP_SRP|CP_AUTH_DSS|CP_ENC_3DES,
		0xC01D|0xC020 => CP_SRP|CP_ENC_AES_BLOCK,
		0xC01E|0xC021 => CP_SRP|CP_AUTH_RSA|CP_ENC_AES_BLOCK,
		0xC01F|0xC022 => CP_SRP|CP_AUTH_DSS|CP_ENC_AES_BLOCK,
		0xC02B|0xC02C => CP_ECDHE|CP_AUTH_ECDSA|CP_ENC_AES_GCM|CP_MODERN_CIPHER,
		0xC02D|0xC02E => CP_STATIC_DH|CP_AUTH_ECDSA|CP_ENC_AES_GCM,
		0xC02F|0xC030 => CP_ECDHE|CP_AUTH_RSA|CP_ENC_AES_GCM,
		0xC031|0xC032 => CP_STATIC_DH|CP_AUTH_RSA|CP_ENC_AES_GCM,
		0xC033 => CP_ECDHE|CP_AUTH_PSK|CP_ENC_RC4,
		0xC034 => CP_ECDHE|CP_AUTH_PSK|CP_ENC_3DES,
		0xC035..=0xC038 => CP_ECDHE|CP_AUTH_PSK|CP_ENC_AES_BLOCK,
		0xC039..=0xC03B => CP_ECDHE|CP_AUTH_PSK|CP_ENC_NULL,
		0xC03C|0xC03D => CP_STATIC_RSA|CP_ENC_ARIA_BLOCK,
		0xC03E|0xC03F => CP_STATIC_DH|CP_AUTH_DSS|CP_ENC_ARIA_BLOCK,
		0xC040|0xC041 => CP_STATIC_DH|CP_AUTH_RSA|CP_ENC_ARIA_BLOCK,
		0xC042|0xC043 => CP_DHE|CP_AUTH_DSS|CP_ENC_ARIA_BLOCK,
		0xC044|0xC045 => CP_DHE|CP_AUTH_RSA|CP_ENC_ARIA_BLOCK,
		0xC046|0xC047 => CP_DHE|CP_AUTH_ANON|CP_ENC_ARIA_BLOCK,
		0xC048|0xC049 => CP_ECDHE|CP_AUTH_ECDSA|CP_ENC_ARIA_BLOCK,
		0xC04A|0xC04B => CP_STATIC_DH|CP_AUTH_ECDSA|CP_ENC_ARIA_BLOCK,
		0xC04C|0xC04D => CP_ECDHE|CP_AUTH_RSA|CP_ENC_ARIA_BLOCK,
		0xC04E|0xC04F => CP_STATIC_DH|CP_AUTH_RSA|CP_ENC_ARIA_BLOCK,
		0xC050|0xC051 => CP_STATIC_RSA|CP_ENC_ARIA_GCM,
		0xC052|0xC053 => CP_DHE|CP_AUTH_RSA|CP_ENC_ARIA_GCM,
		0xC054|0xC055 => CP_STATIC_DH|CP_AUTH_RSA|CP_ENC_ARIA_GCM,
		0xC056|0xC057 => CP_DHE|CP_AUTH_DSS|CP_ENC_ARIA_GCM,
		0xC058|0xC059 => CP_STATIC_DH|CP_AUTH_DSS|CP_ENC_ARIA_GCM,
		0xC05A|0xC05B => CP_DHE|CP_AUTH_ANON|CP_ENC_ARIA_GCM,
		0xC05C|0xC05D => CP_ECDHE|CP_AUTH_ECDSA|CP_ENC_ARIA_GCM,
		0xC05E|0xC05F => CP_STATIC_DH|CP_AUTH_ECDSA|CP_ENC_ARIA_GCM,
		0xC060|0xC061 => CP_ECDHE|CP_AUTH_RSA|CP_ENC_ARIA_GCM,
		0xC062|0xC063 => CP_STATIC_DH|CP_AUTH_RSA|CP_ENC_ARIA_GCM,
		0xC064|0xC065 => CP_PSK|CP_ENC_ARIA_BLOCK,
		0xC066|0xC067 => CP_DHE|CP_AUTH_PSK|CP_ENC_ARIA_BLOCK,
		0xC068|0xC069 => CP_STATIC_RSA|CP_AUTH_PSK|CP_ENC_ARIA_BLOCK,
		0xC06A|0xC06B => CP_PSK|CP_ENC_ARIA_GCM,
		0xC06C|0xC06D => CP_DHE|CP_AUTH_PSK|CP_ENC_ARIA_GCM,
		0xC06E|0xC06F => CP_STATIC_RSA|CP_AUTH_PSK|CP_ENC_ARIA_GCM,
		0xC070|0xC071 => CP_ECDHE|CP_AUTH_PSK|CP_ENC_ARIA_BLOCK,
		0xC072|0xC073 => CP_ECDHE|CP_AUTH_ECDSA|CP_ENC_CAMELLIA_BLOCK,
		0xC074|0xC075 => CP_STATIC_DH|CP_AUTH_ECDSA|CP_ENC_CAMELLIA_BLOCK,
		0xC076|0xC077 => CP_ECDHE|CP_AUTH_RSA|CP_ENC_CAMELLIA_BLOCK,
		0xC078|0xC079 => CP_STATIC_DH|CP_AUTH_RSA|CP_ENC_CAMELLIA_BLOCK,
		0xC07A|0xC07B => CP_STATIC_RSA|CP_ENC_CAMELLIA_GCM,
		0xC07C|0xC07D => CP_DHE|CP_AUTH_RSA|CP_ENC_CAMELLIA_GCM,
		0xC07E|0xC07F => CP_STATIC_DH|CP_AUTH_RSA|CP_ENC_CAMELLIA_GCM,
		0xC080|0xC081 => CP_DHE|CP_AUTH_DSS|CP_ENC_CAMELLIA_GCM,
		0xC082|0xC083 => CP_STATIC_DH|CP_AUTH_DSS|CP_ENC_CAMELLIA_GCM,
		0xC084|0xC085 => CP_DHE|CP_AUTH_ANON|CP_ENC_CAMELLIA_GCM,
		0xC086|0xC087 => CP_ECDHE|CP_AUTH_ECDSA|CP_ENC_CAMELLIA_GCM,
		0xC088|0xC089 => CP_STATIC_DH|CP_AUTH_ECDSA|CP_ENC_CAMELLIA_GCM,
		0xC08A|0xC08B => CP_ECDHE|CP_AUTH_RSA|CP_ENC_CAMELLIA_GCM,
		0xC08C|0xC08D => CP_STATIC_DH|CP_AUTH_RSA|CP_ENC_CAMELLIA_GCM,
		0xC08E|0xC08F => CP_PSK|CP_ENC_CAMELLIA_GCM,
		0xC090|0xC091 => CP_DHE|CP_AUTH_PSK|CP_ENC_CAMELLIA_GCM,
		0xC092|0xC093 => CP_STATIC_RSA|CP_AUTH_PSK|CP_ENC_CAMELLIA_GCM,
		0xC094|0xC095 => CP_PSK|CP_ENC_CAMELLIA_BLOCK,
		0xC096|0xC097 => CP_DHE|CP_AUTH_PSK|CP_ENC_CAMELLIA_BLOCK,
		0xC098|0xC099 => CP_STATIC_RSA|CP_AUTH_PSK|CP_ENC_CAMELLIA_BLOCK,
		0xC09A|0xC09B => CP_ECDHE|CP_AUTH_PSK|CP_ENC_CAMELLIA_BLOCK,
		0xC09C|0xC09D => CP_STATIC_RSA|CP_ENC_AES_CCM,
		0xC09E|0xC09F => CP_DHE|CP_AUTH_RSA|CP_ENC_AES_CCM,
		0xC0A0|0xC0A1 => CP_STATIC_RSA|CP_ENC_AES_CCM|CP_MAC_SHORT,
		0xC0A2|0xC0A3 => CP_DHE|CP_AUTH_RSA|CP_ENC_AES_CCM|CP_MAC_SHORT,
		0xC0A4|0xC0A5 => CP_PSK|CP_ENC_AES_CCM,
		0xC0A6|0xC0A7 => CP_DHE|CP_AUTH_PSK|CP_ENC_AES_CCM,
		0xC0A8|0xC0A9 => CP_PSK|CP_ENC_AES_CCM|CP_MAC_SHORT,
		0xC0AA|0xC0AB => CP_DHE|CP_AUTH_PSK|CP_ENC_AES_CCM|CP_MAC_SHORT,
		0xC0AC|0xC0AD => CP_ECDHE|CP_AUTH_ECDSA|CP_ENC_AES_CCM,
		0xC0AE|0xC0AF => CP_ECDHE|CP_AUTH_ECDSA|CP_ENC_AES_CCM|CP_MAC_SHORT,
		0xC0B0|0xC0B1 => CP_ECCPWD|CP_ENC_AES_GCM,
		0xC0B2|0xC0B3 => CP_ECCPWD|CP_ENC_AES_CCM,
		0xC0B4|0xC0B5 => CP_ENC_NULL|CP_TLS13,
		//0xC0B6-0xC0FF Unassigned
		0xC100 => CP_ENC_KUZNYECHIK|CP_KEX_GOST,
		0xC101|0xC102 => CP_ENC_MAGMA|CP_KEX_GOST,
		0xC103|0xC105 => CP_ENC_KUZNYECHIK|CP_TLS13,
		0xC104|0xC106 => CP_ENC_MAGMA|CP_TLS13,
		//0xC107-0xCCA7 Unassigned
		0xCCA8 => CP_ECDHE|CP_AUTH_RSA|CP_ENC_CHACHA,
		0xCCA9 => CP_ECDHE|CP_AUTH_ECDSA|CP_ENC_CHACHA|CP_MODERN_CIPHER,
		0xCCAA => CP_DHE|CP_AUTH_RSA|CP_ENC_CHACHA,
		0xCCAB => CP_PSK|CP_ENC_CHACHA,
		0xCCAC => CP_ECDHE|CP_AUTH_PSK|CP_ENC_CHACHA,
		0xCCAD => CP_DHE|CP_AUTH_PSK|CP_ENC_CHACHA,
		0xCCAE => CP_STATIC_RSA|CP_AUTH_PSK|CP_ENC_CHACHA,
		//0xCCAF-0xD000 Unassigned
		0xD001|0xD002 => CP_ECDHE|CP_AUTH_PSK|CP_ENC_AES_GCM,
		0xD003 => CP_ECDHE|CP_AUTH_PSK|CP_ENC_AES_CCM|CP_MAC_SHORT,
		//0xD004   Unassigned
		0xD005 => CP_ECDHE|CP_AUTH_PSK|CP_ENC_AES_CCM,
		//0xD006-0xFEFD Unassigned
		//Reserved and unassigned ranges.
		0x00FF|0x5600 => CP_SCSV,
		0x0000 => CP_RESERVED|CP_VERY_BAD,
		0x001C..=0x001D|0x0047..=0x005C|0x0060..=0x0066|0xFEFE..=0xFEFF => CP_RESERVED,
		0xCC13|0xCC14|0xCC15 => CP_ENC_OLDCHACHA|CP_UNASSIGNED,
		_ => CP_UNASSIGNED
	})
}

pub(crate) fn group_properties(cs: u16) -> (u16, u128)
{
	if cs % 4112 == 2570 { return (cs, CP_GREASE); }
	if cs >> 8 == 254 { return (cs, CP_PRIVATE_USE); }
	(cs, match cs {
		0 => CP_RESERVED|CP_BLACKLIST,
		1..=5 => CP_GRP_BINARY|CP_VERY_BAD,
		6..=14 => CP_GRP_BINARY|CP_BLACKLIST,
		15..=19 => CP_VERY_BAD,
		20..=22 => CP_BLACKLIST,
		23 => CP_GRP_P256,
		24 => CP_GRP_P384,
		25 => CP_GRP_P521,
		26..=28|31..=33 => CP_GRP_BRAINPOOL,
		29 => CP_GRP_X25519,
		30 => CP_GRP_X448,
		34..=40 => CP_GRP_GOST,
		41 => CP_GRP_SM2,
		//42-255  Unassigned
		256..=260 => CP_GRP_FFDHE,
		//261-507  Unassigned
		508..=511 => CP_GRP_FFDHE|CP_PRIVATE_USE,
		//512-25496 Unassigned
		25497..=25498 => CP_POST_QUANTUM,
		//25498-65023  Unassigned
		//65280  Unassigned
		65281 => CP_GRP_CUSTOM,
		65282 => CP_GRP_CUSTOM,
		//65283-65535  Unassigned
		_ => CP_UNASSIGNED
	})
}

pub(crate) fn signature_properties(cs: u16) -> (u16, u128)
{
	if cs >> 9 == 127 { return (cs, CP_PRIVATE_USE); }
	if cs >> 8 >= 7 && cs % 256 <= 3 { return (cs, CP_RESERVED); }
	if cs >> 8 >= 1 && cs % 256 == 0 { return (cs, CP_RESERVED); }
	if cs <= 3 { return (cs, CP_RESERVED); }
	(cs, match cs {
		0x0101 => CP_SIG_RSA|CP_SIG_MD5|CP_VERY_BAD|CP_BLACKLIST,
		0x0102 => CP_SIG_DSS|CP_SIG_MD5|CP_VERY_BAD|CP_BLACKLIST,
		0x0103 => CP_SIG_ECDSA|CP_SIG_MD5|CP_VERY_BAD|CP_BLACKLIST,
		0x0201 => CP_SIG_RSA|CP_SIG_SHA1|CP_BLACKLIST,
		0x0202 => CP_SIG_DSS|CP_SIG_SHA1|CP_BLACKLIST,
		0x0203 => CP_SIG_ECDSA|CP_SIG_SHA1|CP_BLACKLIST,
		0x0301 => CP_SIG_RSA|CP_SIG_SHA224,
		0x0302 => CP_SIG_DSS|CP_SIG_SHA224,
		0x0303 => CP_SIG_ECDSA|CP_SIG_SHA224,
		0x0401|0x0501|0x0601 => CP_SIG_RSA,
		0x0402|0x0502|0x0602 => CP_SIG_DSS,
		0x0403|0x0503|0x0603 => CP_SIG_ECDSA|CP_MODERN_SIG,
		0x0420|0x0520|0x0620 => CP_SIG_RSA,
		0x0704..=0x0707 => CP_SIG_IBC,
		0x0708 => CP_SIG_SM3,
		0x0709..=0x070F => CP_SIG_GOST,
		//0x0710..0x07FF  Unassigned
		0x0804..=0x0806 => CP_SIG_RSA,
		0x0807 => CP_SIG_ED25519,
		0x0808 => CP_SIG_ED448,
		0x0809..=0x080B => CP_SIG_RSA|CP_SIG_RSA_PSS,
		//0x080C-0x0819 Unassigned
		0x081A..=0x081C => CP_SIG_ECDSA,
		//0x081D..0x08FF  Unassigned
		//0x0900..0xFDFF  Unassigned
		_ => CP_UNASSIGNED
	})
}

#[cfg(test)]
fn unalloc_flags(i: u16) -> u128
{
	if i >> 8 == i & 255 && i & 15 == 10 { CP_GREASE } else { CP_UNASSIGNED } 
}

#[cfg(test)]
fn sig_unused_hash(i: u16) -> u128
{
	if i & 255 <= 3 { CP_RESERVED } else { CP_UNASSIGNED } 
}

#[test]
fn check_ciphersuite_properties()
{
	let normal = CP_GREASE|CP_PRIVATE_USE|CP_RESERVED|CP_UNASSIGNED|CP_SCSV;
	for i in 0..16 { assert_eq!(ciphersuite_properties(0x1010*i+0x0A0A).1, CP_GREASE); }
	assert_eq!(ciphersuite_properties(0).1, CP_RESERVED|CP_VERY_BAD);
	for i in 0x0001..=0x001B { assert!(ciphersuite_properties(i).1 & normal == 0); }
	for i in 0x001C..=0x001D { assert_eq!(ciphersuite_properties(i).1, CP_RESERVED); }
	for i in 0x001E..=0x0046 { assert!(ciphersuite_properties(i).1 & normal == 0); }
	for i in 0x0047..=0x005C { assert_eq!(ciphersuite_properties(i).1, CP_RESERVED); }
	for i in 0x005D..=0x005F { assert_eq!(ciphersuite_properties(i).1, unalloc_flags(i)); }
	for i in 0x0060..=0x0066 { assert_eq!(ciphersuite_properties(i).1, CP_RESERVED); }
	for i in 0x0067..=0x006D { assert!(ciphersuite_properties(i).1 & normal == 0); }
	for i in 0x006E..=0x0083 { assert_eq!(ciphersuite_properties(i).1, unalloc_flags(i)); }
	for i in 0x0084..=0x00C7 { assert!(ciphersuite_properties(i).1 & normal == 0); }
	for i in 0x00C8..=0x00FE { assert_eq!(ciphersuite_properties(i).1, unalloc_flags(i)); }
	assert_eq!(ciphersuite_properties(0x00FF).1, CP_SCSV);
	for i in 0x0100..=0x1300 { assert_eq!(ciphersuite_properties(i).1, unalloc_flags(i)); }
	for i in 0x1301..=0x1307 { assert!(ciphersuite_properties(i).1 & normal == 0); }
	for i in 0x1308..=0x55FF { assert_eq!(ciphersuite_properties(i).1, unalloc_flags(i)); }
	assert_eq!(ciphersuite_properties(0x5600).1, CP_SCSV);
	for i in 0x5601..=0xC000 { assert_eq!(ciphersuite_properties(i).1, unalloc_flags(i)); }
	for i in 0xC001..=0xC0B5 { assert!(ciphersuite_properties(i).1 & normal == 0); }
	for i in 0xC0B6..=0xC0FF { assert_eq!(ciphersuite_properties(i).1, unalloc_flags(i)); }
	for i in 0xC100..=0xC106 { assert!(ciphersuite_properties(i).1 & normal == 0); }
	for i in 0xC107..=0xCC12 { assert_eq!(ciphersuite_properties(i).1, unalloc_flags(i)); }
	for i in 0xCC13..=0xCC15 { assert_eq!(ciphersuite_properties(i).1, CP_ENC_OLDCHACHA|CP_UNASSIGNED); }
	for i in 0xCC16..=0xCCA7 { assert_eq!(ciphersuite_properties(i).1, unalloc_flags(i)); }
	for i in 0xCCA8..=0xCCAE { assert!(ciphersuite_properties(i).1 & normal == 0); }
	for i in 0xCCAF..=0xD000 { assert_eq!(ciphersuite_properties(i).1, unalloc_flags(i)); }
	for i in 0xD001..=0xD003 { assert!(ciphersuite_properties(i).1 & normal == 0); }
	assert_eq!(ciphersuite_properties(0xD004).1, CP_UNASSIGNED);
	assert!(ciphersuite_properties(0xD005).1 & normal == 0);
	for i in 0xD006..=0xFEFD { assert_eq!(ciphersuite_properties(i).1, unalloc_flags(i)); }
	for i in 0xFEFE..=0xFEFF { assert_eq!(ciphersuite_properties(i).1, CP_RESERVED); }
	for i in 0..256 { assert_eq!(ciphersuite_properties(0xFF00+i).1, CP_PRIVATE_USE); }
	let tls13 = [0x00C6,0x00C7,0x1301,0x1302,0x1303,0x1304,0x1305,0x1306,0x1307,0xC0B4,0xC0B5,0xC103,0xC104,
		0xC105,0xC106];
	for &i in tls13.iter() { assert!(ciphersuite_properties(i).1 & CP_TLS13 != 0); }
	for i in 0..=0xFEFF {
		let v = if tls13.binary_search(&i).is_ok() { CP_TLS13 } else { 0 };
		assert!(ciphersuite_properties(i).1 & CP_TLS13 == v);
	}
	let modern = [0x1301,0x1302,0x1303,0xC02B,0xC02C,0xCCA9];
	for i in 0..=0xFEFF {
		let v = if modern.binary_search(&i).is_ok() { CP_MODERN_CIPHER } else { 0 };
		assert!(ciphersuite_properties(i).1 & CP_MODERN_CIPHER == v);
	}
}

#[test]
fn check_group_properties()
{
	let normal = CP_GREASE|CP_PRIVATE_USE|CP_RESERVED|CP_UNASSIGNED|CP_SCSV;
	for i in 0..16 { assert_eq!(group_properties(0x1010*i+0x0A0A).1, CP_GREASE); }
	assert_eq!(group_properties(0).1, CP_RESERVED|CP_BLACKLIST);
	for i in 1..=41 { assert!(group_properties(i).1 & normal == 0); }
	for i in 42..=255 { assert_eq!(group_properties(i).1, unalloc_flags(i)); }
	for i in 256..=260 { assert!(group_properties(i).1 & normal == 0); }
	for i in 261..=507 { assert_eq!(group_properties(i).1, unalloc_flags(i)); }
	for i in 508..=511 { assert_eq!(group_properties(i).1, CP_GRP_FFDHE|CP_PRIVATE_USE); }
	for i in 512..=25496 { assert_eq!(group_properties(i).1, unalloc_flags(i)); }
	assert_eq!(group_properties(25497).1, CP_POST_QUANTUM);
	assert_eq!(group_properties(25498).1, CP_POST_QUANTUM);
	for i in 25499..=65023 { assert_eq!(group_properties(i).1, unalloc_flags(i)); }
	for i in 65024..=65279 { assert_eq!(group_properties(i).1, CP_PRIVATE_USE); }
	assert_eq!(group_properties(65280).1, CP_UNASSIGNED);
	for i in 65281..=65282 { assert_eq!(group_properties(i).1, CP_GRP_CUSTOM); }
	for i in 3..=255 { assert_eq!(group_properties(0xFF00+i).1, CP_UNASSIGNED); }
	let modern = [23,24,25,29,30,25497,25498];
	for i in 0..=65023 {
		let v = if modern.binary_search(&i).is_ok() { CP_MODERN_GROUP } else { 0 };
		assert!(group_properties(i).1 & CP_MODERN_GROUP == v);
	}
}

#[test]
fn check_signature_properties()
{
	let normal = CP_GREASE|CP_PRIVATE_USE|CP_RESERVED|CP_UNASSIGNED|CP_SCSV;
	for i in 0x0000..=0x0003 { assert_eq!(signature_properties(i).1, CP_RESERVED); }
	for i in 0x0004..=0x00FF { assert_eq!(signature_properties(i).1, CP_UNASSIGNED); }
	for i in 1..3 {
		assert_eq!(signature_properties(256*i).1, CP_RESERVED);
		for j in 0x01..=0x03 { assert!(signature_properties(256*i+j).1 & normal == 0); }
		for j in 0x04..=0xFF { assert_eq!(signature_properties(256*i+j).1, CP_UNASSIGNED); }
	}
	for i in 4..6 {
		assert_eq!(signature_properties(256*i).1, CP_RESERVED);
		for j in 0x01..=0x03 { assert!(signature_properties(256*i+j).1 & normal == 0); }
		for j in 0x04..=0x19 { assert_eq!(signature_properties(256*i+j).1, CP_UNASSIGNED); }
		assert!(signature_properties(256*i+0x20).1 & normal == 0);
		for j in 0x21..=0xFF { assert_eq!(signature_properties(256*i+j).1, CP_UNASSIGNED); }
	}
	for i in 0x0700..=0x0703 { assert_eq!(signature_properties(i).1, CP_RESERVED); }
	for i in 0x0704..=0x070F { assert!(signature_properties(i).1 & normal == 0); }
	for i in 0x0710..=0x07FF { assert_eq!(signature_properties(i).1, CP_UNASSIGNED); }
	for i in 0x0800..=0x0803 { assert_eq!(signature_properties(i).1, CP_RESERVED); }
	for i in 0x0804..=0x080B { assert!(signature_properties(i).1 & normal == 0); }
	for i in 0x080C..=0x0819 { assert_eq!(signature_properties(i).1, CP_UNASSIGNED); }
	for i in 0x081A..=0x081C { assert!(signature_properties(i).1 & normal == 0); }
	for i in 0x081D..=0x08FF { assert_eq!(signature_properties(i).1, CP_UNASSIGNED); }
	for i in 0x0900..0xFDFF { assert_eq!(signature_properties(i).1, sig_unused_hash(i)); }
	for i in 0..512 { assert_eq!(signature_properties(0xFE00+i).1, CP_PRIVATE_USE); }
	let modern = [0x0403,0x0503,0x0603,0x0807,0x0808];
	for i in 0x0000..=0x08FF {
		let v = if modern.binary_search(&i).is_ok() { CP_MODERN_SIG } else { 0 };
		assert!(signature_properties(i).1 & CP_MODERN_SIG == v);
	}
}

