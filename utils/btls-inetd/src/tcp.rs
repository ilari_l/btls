use btls_aux_fail::ResultExt;
use btls_daemon_helper_lib::AllocatedToken;
use btls_daemon_helper_lib::ConnectionInfo;
use btls_daemon_helper_lib::FileDescriptor;
use btls_daemon_helper_lib::IO_WAIT_ONESHOT;
use btls_daemon_helper_lib::IO_WAIT_READ;
use btls_daemon_helper_lib::Poll;
use btls_daemon_helper_lib::TokenAllocatorPool;
use std::borrow::Cow;
use std::io::Error as IoError;
use std::io::ErrorKind as IoErrorKind;
use std::io::Write;
use std::mem::replace;


pub struct TcpState
{
	//The TLS socket file descriptor.
	socket_fd: FileDescriptor,
	//Poll token for TLS socket file descriptor.
	socket_poll_token: AllocatedToken,
	//Socket connection information.
	socket_conninfo: ConnectionInfo,
}

impl TcpState
{
	pub fn init(socket: FileDescriptor, conninfo: ConnectionInfo, allocator: &mut TokenAllocatorPool,
		poll: &mut Poll) -> Result<TcpState, Cow<'static, str>>
	{
		let socket_poll_token = allocator.allocate().set_err("Can't allocate Tokens")?;
		//The TCP socket needs to be non-blocking!
		socket.set_nonblock().map_err(|err|{
			format!("Failed to set nonblocking mode: {err}")
		})?;
		//Need to register readabilty for the TLS socket and the IRQ notifier.
		poll.register(&socket, socket_poll_token.value(), IO_WAIT_READ | IO_WAIT_ONESHOT).
			set_err("Can't register main socket for polling")?;
		Ok(TcpState {
			socket_fd: socket,
			socket_poll_token: socket_poll_token,
			socket_conninfo: conninfo,
		})
	}
	pub fn is_tcp_interrupt(&self, token: usize) -> bool
	{
		self.socket_poll_token.value() == token
	}
	pub fn rearm_poll(&self, poll: &mut Poll, ready: u8) -> Result<(), Cow<'static, str>>
	{
		if ready != 0 {
			poll.reregister(&self.socket_fd, self.socket_poll_token.value(), ready | IO_WAIT_ONESHOT).
				map_err(|err|{
				format!("poll.reregister error: {err}")
			})?;
		} else {
			//This does not like empty flags.
		}
		Ok(())
	}
	pub fn deregister_poll(&self, poll: &mut Poll)
	{
		poll.deregister(&self.socket_fd).ok();
	}
	pub fn push_tokens(&self, tokens: &mut Vec<AllocatedToken>)
	{
		tokens.push(self.socket_poll_token.clone());
	}
	pub fn info<'a>(&'a self) -> &'a ConnectionInfo
	{
		&self.socket_conninfo
	}
	pub fn info_mut<'a>(&'a mut self) -> &'a mut ConnectionInfo
	{
		&mut self.socket_conninfo
	}
	pub fn as_fd(&self) -> i32
	{
		self.socket_fd.as_raw_fd() as i32
	}
	pub fn fd<'a>(&'a mut self) -> &'a mut FileDescriptor
	{
		&mut self.socket_fd
	}
	pub fn write(&mut self, mut buffer: &[u8]) -> Result<usize, IoError>
	{
		let olen = buffer.len();
		while buffer.len() > 0 { match self.socket_fd.write(buffer) {
			Ok(amt) => buffer = buffer.get(amt..).unwrap_or(&[]),
			//Retry Interrupted, bail on WouldBlock, otherwise return error.
			Err(ref e) if e.kind() == IoErrorKind::Interrupted => (),
			Err(ref e) if e.kind() == IoErrorKind::WouldBlock => break,
			Err(e) => fail!(e)
		}}
		Ok(olen - buffer.len())
	}
}

pub struct TcpBuffer
{
	data: Vec<u8>,
	offset: usize,
	eof: bool,
	eof_ack: bool,
}

impl TcpBuffer
{
	pub fn new() -> TcpBuffer
	{
		TcpBuffer {
			data: Vec::new(),
			offset: 0,
			eof: false,
			eof_ack: false,
		}
	}
	pub fn with_data(data: Vec<u8>) -> TcpBuffer
	{
		TcpBuffer {
			data: data,
			offset: 0,
			eof: false,
			eof_ack: false,
		}
	}
	pub fn steal_buffer(&mut self) -> Option<Vec<u8>>
	{
		//Only empty buffer can be stolen.
		if self.data.len() > 0 { return None; }
		Some(replace(&mut self.data, Vec::new()))
	}
	pub fn unsteal_buffer(&mut self, buffer: Vec<u8>)
	{
		self.offset = 0;
		self.data = buffer;
	}
	pub fn append(&mut self, data: &[u8])
	{
		self.data.extend_from_slice(data);
	}
	pub fn contents<'a>(&'a self) -> &'a [u8]
	{
		&self.data[self.offset..]
	}
	pub fn has_content(&self) -> bool
	{
		self.offset < self.data.len()
	}
	pub fn pop(&mut self, amt: usize)
	{
		self.offset += amt;
		if self.offset >= self.data.len() {
			self.offset = 0;
			self.data.clear();
		}
	}
	pub fn is_eof(&self) -> bool
	{
		self.eof
	}
	pub fn set_eof(&mut self)
	{
		self.eof = true;
	}
	pub fn ack_eof(&mut self)
	{
		self.eof_ack = true;
	}
	pub fn is_acked_eof(&self) -> bool
	{
		self.eof_ack
	}
	pub fn resize_data(&mut self, newsize: usize)
	{
		self.data.resize(newsize, 0);
	}
	pub fn data_length(&self) -> usize
	{
		self.data.len()
	}
	pub fn mutably_borrow_after<'a>(&'a mut self, offset: usize) -> &'a mut [u8]
	{
		&mut self.data[offset..]
	}
	pub fn mutably_borrow_range<'a>(&'a mut self, offset: usize, len: usize) -> &'a mut [u8]
	{
		&mut self.data[offset..][..len]
	}
}
