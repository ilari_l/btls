use crate::ConfigT;
use crate::ParseChSubErr;
use crate::ParseSniAlpnError;
use crate::conninit::ACME_CONTROL_PROTO;
use crate::conninit::ACME_TLS_PROTO;
use crate::crypto_props::CP_BLACKLIST;
use crate::services_dp::ServiceName;
use crate::services_dp::ServiceProperties;
use btls::test::check_sni_hostname;
use btls::test::downgrade_to_ipv4;
use btls::test::parse_rdns;
use btls_aux_fail::ResultExt;
use btls_aux_serialization::Source;
use btls_aux_tls_struct::LEN_CIPHER_SUITE_LIST;
use btls_aux_tls_struct::LEN_COMPRESSION_METHOD_LIST;
use btls_aux_tls_struct::LEN_EXTENSION_DATA;
use btls_aux_tls_struct::LEN_EXTENSION_LIST;
use btls_aux_tls_struct::LEN_NAMED_GROUP_LIST;
use btls_aux_tls_struct::LEN_PROTOCOL_LIST;
use btls_aux_tls_struct::LEN_PROTOCOL_NAME;
use btls_aux_tls_struct::LEN_RANDOM;
use btls_aux_tls_struct::LEN_SERVER_NAME_LIST;
use btls_aux_tls_struct::LEN_SERVER_NAME_NAME;
use btls_aux_tls_struct::LEN_SESSION_ID;
use btls_aux_tls_struct::LEN_SIGNATURE_SCHEME_LIST;
use btls_aux_tlsciphersuites::Ciphersuite;
use btls_aux_tlsciphersuites::CiphersuiteEnabled2;
use btls_daemon_helper_lib::ConnectionInfo;
use btls_daemon_helper_lib::ConnectionInfoAddress;
use btls_daemon_helper_lib::is_fatal_error;
use btls_daemon_helper_lib::reverse_addr_for;
use btls_util_logging::log;
use std::borrow::Cow;
use std::cmp::min;
use std::fmt::Write;
use std::io::Error as IoError;
use std::io::ErrorKind as IoErrorKind;
use std::io::Read as IoRead;
use std::net::IpAddr;
use std::net::Ipv6Addr;
use std::str::from_utf8;

struct CsIterator<'a>(Source<'a>);

impl<'a> Iterator for CsIterator<'a>
{
	type Item = u16;
	fn next(&mut self) -> Option<u16> { self.0.u16().ok() }
}

fn known_ciphersuite(cs: u16) -> bool
{
	Ciphersuite::by_tls_id3(cs, CiphersuiteEnabled2::unsafe_any_policy()).is_some()
}

fn check_groups(src: Source, cipher_flags: &mut u128)
{
	let mut errmsg = String::new();
	//Can just abuse CsIterator for this.
	for (bad_grp, flags) in CsIterator(src.clone()).map(crate::crypto_props::group_properties) {
		*cipher_flags |= flags;
		if flags & CP_BLACKLIST != 0 {
			if errmsg.len() > 0 { errmsg.push_str(", "); }
			write!(errmsg, "{bad_grp:04x}").ok();
		}
	}
	if errmsg.len() > 0 {
		//This is connection-specific.
		log!(NOTICE "Connection advertises black list groups: {errmsg}");
	}
}

fn check_signatures(src: Source, cipher_flags: &mut u128)
{
	let mut errmsg = String::new();
	//Can just abuse CsIterator for this.
	for (bad_sig, flags) in CsIterator(src.clone()).map(crate::crypto_props::signature_properties) {
		*cipher_flags |= flags;
		if flags & CP_BLACKLIST != 0 {
			if errmsg.len() > 0 { errmsg.push_str(", "); }
			write!(errmsg, "{bad_sig:04x}").ok();
		}
	}
	if errmsg.len() > 0 {
		//This is connection-specific.
		log!(NOTICE "Connection advertises black list signatures: {errmsg}");
	}
}

fn check_decodeable(src: Source, cipher_flags: &mut u128) -> bool
{
	let known = CsIterator(src.clone()).any(known_ciphersuite);
	let mut errmsg = String::new();
	for (bad_cs, flags) in CsIterator(src.clone()).map(crate::crypto_props::ciphersuite_properties) {
		*cipher_flags |= flags;
		if flags & CP_BLACKLIST != 0 {
			if errmsg.len() > 0 { errmsg.push_str(", "); }
			write!(errmsg, "{bad_cs:04x}").ok();
		}
	}
	if errmsg.len() > 0 {
		//This is connection-specific.
		log!(NOTICE "Connection advertises black list ciphers: {errmsg}");
	}
	known
}

pub struct ClientHelloProperties
{
	//Crypto flags
	pub crypto_flags: u128,
}

impl Default for ClientHelloProperties
{
	fn default() -> ClientHelloProperties
	{
		ClientHelloProperties {
			crypto_flags: crate::crypto_props::CP_SAMPLE4,
		}
	}
}

fn is_grease_alpn(alpn: &[u8]) -> bool
{
	alpn.len() == 2 && alpn[0] == alpn[1] && alpn[0] & 15 == 10
}

fn unwrap_sni<'a>(x: &'a str) -> &'a str
{
	if x != "" { x }  else { "<NO SNI>" }
}

struct AlpnList<'a>(&'a [u8]);

impl<'a> AlpnList<'a>
{
	fn iter(&self) -> AlpnIterator<'a> { AlpnIterator(self.0) }
}

struct AlpnIterator<'a>(&'a [u8]);

impl<'a> Iterator for AlpnIterator<'a>
{
	type Item = &'a str;
	fn next(&mut self) -> Option<&'a str>
	{
		loop {
			let len = *self.0.first()? as usize;	//Length of entry, or end iteration on EoL.
			//Split into two parts, this should succeed, but if not, end of iteration.
			let alpn = self.0.get(1..len+1)?;
			self.0 = self.0.get(len+1..)?;
			if is_grease_alpn(alpn) || alpn.len() == 0 { continue; }	//Ignore GREASE and empty.
			//Return all ALPN entries that are valid UTF-8.
			if let Ok(alpn) = from_utf8(alpn) { return Some(alpn); }
		}
	}
}

struct ClientHello<'a>
{
	//The inner ALPN list.
	alpn_list: AlpnList<'a>,
	//The host_name.
	host_name: &'a str,
	//Decode OK flag.
	decode_ok: bool,
	//ALPN extension present.
	alpn_present: bool,
}

impl<'a> ClientHello<'a>
{
	fn parse(record: &'a [u8], rsize: usize, cipher_flags: &mut u128) ->
		Result<ClientHello<'a>, ParseChSubErr>
	{
		use crate::ParseChSubErr::*;
		let mut chello = Source::new(record);
		//Content type, should be 22 (TLS handshake protocol).
		fail_if!(chello.u8().set_err(Truncated)? != 22, BadProtocol);
		//Record major version, should be 3.
		fail_if!(chello.u8().set_err(Truncated)? != 3, BadRecordMajorVersion);
		//Record minor version, should be 0, 1, 2 or 3.
		fail_if!(chello.u8().set_err(Truncated)? > 3, BadRecordMinorVersion);
		//Length of record. Should be size of client hello - 5 bytes.
		fail_if!(chello.u16().set_err(Truncated)? as usize != rsize.wrapping_sub(5),
			BadChRecordSize);
		//Client Hello content type.
		fail_if!(chello.u8().set_err(Truncated)? != 1, NotClientHello);
		//Length of message. Should be length of client hello - 9 bytes.
		fail_if!(chello.u24().set_err(Truncated)? as usize != rsize.wrapping_sub(9),
			BadChMessageSize);
		//TLS 1.2/1.3.
		let cversion = chello.u16().set_err(Truncated)?;
		fail_if!(cversion.wrapping_sub(0x303) > 252, BadTlsVersion(cversion));
		chello.slice(LEN_RANDOM).set_err(Truncated)?;
		chello.slice(LEN_SESSION_ID).set_err(Truncated)?;
		let cslist = chello.source(LEN_CIPHER_SUITE_LIST).set_err(Truncated)?;
		let decode_ok = check_decodeable(cslist, cipher_flags);
		chello.slice(LEN_COMPRESSION_METHOD_LIST).set_err(Truncated)?;
		let exts = chello.source(LEN_EXTENSION_LIST).set_err(Truncated)?;
		chello.expect_end().set_err(JunkAfterEnd)?;
		//Do normal extension parse that can fail as second.
		let mut alpn_list = AlpnList(&[]);
		let mut host_name = "";
		let mut exts3 = exts.clone();
		let mut alpn_present = false;
		while exts3.more() {
			let eid = exts3.u16().set_err(ExtensionTruncated)?;
			let mut payload = exts3.source(LEN_EXTENSION_DATA).set_err(ExtensionTruncated)?;
			if eid == 0 {
				let mut list = payload.source(LEN_SERVER_NAME_LIST).set_err(SniParseError)?;
				while list.more() {
					let ntype = list.u8().set_err(SniParseError)?;
					let name = list.slice(LEN_SERVER_NAME_NAME).set_err(SniParseError)?;
					if ntype == 0 {
						//host_name can not be empty.
						fail_if!(name == b"", SniParseError);
						host_name = check_sni_hostname(name).
							map_err(|f|if f {DnsNameTooLong} else {BadDnsName})?;
					}
				}
				payload.expect_end().set_err(SniParseError)?;
			} else if eid == 10 {
				//Supported groups.
				let list = payload.source(LEN_NAMED_GROUP_LIST).set_err(ExtensionTruncated)?;
				check_groups(list, cipher_flags);
			} else if eid == 13 {
				//Signature algorithms.
				let list = payload.source(LEN_SIGNATURE_SCHEME_LIST).set_err(ExtensionTruncated)?;
				check_signatures(list, cipher_flags);
			} else if eid == 16 {
				let mut list = payload.source(LEN_PROTOCOL_LIST).set_err(AlpnParseError)?;
				alpn_list = AlpnList(list.as_slice());
				//Just check that the list is decodeable.
				while list.more() {
					list.slice(LEN_PROTOCOL_NAME).set_err(AlpnParseError)?;
				}
				alpn_present = true;
				payload.expect_end().set_err(AlpnParseError)?;
			}
		}
		//OK, results.
		Ok(ClientHello {
			decode_ok: decode_ok,
			alpn_list: alpn_list,
			host_name: host_name,
			alpn_present: alpn_present,
		})
	}
}

pub struct EarlyParseResult
{
	pub sni: String,
	pub alpn: Option<String>,
	pub hostdata: ServiceName,
	pub sprops: ServiceProperties,
}

pub struct EarlyDataBuffer
{
	//The buffered data.
	buffer: Vec<u8>,
	//The size of buffered data.
	size: usize,
	//Proxy allowed.
	allow_proxy: bool,
	//Proxy received.
	received_proxy: bool,
}

impl EarlyDataBuffer
{
	pub fn init(allow_proxy: bool) -> EarlyDataBuffer
	{
		EarlyDataBuffer {
			buffer: vec![0;16384],
			size: 0,
			allow_proxy: allow_proxy,
			received_proxy: false,
		}
	}
	pub fn fill<SRC:IoRead>(&mut self, src: &mut SRC, global: ConfigT) -> Result<(), IoError>
	{
		//Do not overfill.
		if self.size >= self.buffer.len() { return Ok(()); }
		//Read to buffer.
		let amt = match src.read(&mut self.buffer[self.size..]) {
			Ok(0) => return Err(IoError::new(IoErrorKind::UnexpectedEof,
				"Unexpected end of file reading TLS client hello")),
			Ok(x) => {
				global.metrics(|m|m.read_tcp(x));
				x
			},
			Err(x) => {
				if is_fatal_error(&x) { return Err(x)?; }
				return Ok(());
			}
		};
		//Record received bytes.
		self.size += amt;
		Ok(())
	}
	fn pop_data(&mut self, amt: usize)
	{
		for i in 0..self.size.saturating_sub(amt) {
			self.buffer[i] = self.buffer[i + amt];
		}
		self.size = self.size.saturating_sub(amt);
	}
	fn extract_proxyv1<'a>(&'a self) -> Option<&'a [u8]>
	{
		let mut len = self.size;
		for i in 0..self.size - 1 {
			if self.buffer[i] == 13 && self.buffer[i+1] == 10 {
				len = i;
				break;
			}
		}
		fail_if_none!(len >= self.size - 1);	//Too short.
		Some(&self.buffer[..len])
	}
	fn extract_proxyv2<'a>(&'a self) -> Option<&'a [u8]>
	{
		fail_if_none!(self.size < 16);		//Too short.
		let len = (self.buffer[14] as usize) * 256 + (self.buffer[15] as usize);
		fail_if_none!(self.size < 16 + len);	//Too short.
		Some(&self.buffer[..len+16])
	}
	pub fn get_clienthello_size(&self) -> Option<usize>
	{
		if self.size >= 5 {
			let mut rsize = 5;
			rsize += 256 * self.buffer[3] as usize;
			rsize += self.buffer[4] as usize;
			Some(rsize)
		} else {
			None		//wait for more data.
		}
	}
	pub fn peek_buffer<'a>(&'a self, amt: usize) -> &'a [u8]
	{
		&self.buffer[..min(amt, self.buffer.len())]
	}
	pub fn swap_clear<'a>(&'a mut self) -> &'a [u8]
	{
		let old = &self.buffer[..self.size];
		self.size = self.buffer.len();
		old
	}
	pub fn first_byte(&self) -> Option<u8>
	{
		if self.size >= 1 { Some(self.buffer[0]) } else { None }
	}
	pub fn has_at_least(&self, amt: usize) -> bool
	{
		self.size >= amt
	}
	pub fn can_fit(&self, amt: usize) -> bool
	{
		self.buffer.len() >= amt
	}
	//No SNI/ALPN is represented by empty string, as SNI/ALPN can not be zero-length, and zero-length strings
	//do not allocate.
	pub fn scratch_hello_get_sni_alpn<F>(chello: &[u8], rsize: usize, dest_ip_raw: &ConnectionInfoAddress,
		properties: &mut ClientHelloProperties, mut sni_info_fn: F) ->
		Result<EarlyParseResult, ParseSniAlpnError>
		where F: FnMut(&str) -> Result<Option<ServiceName>, Cow<'static, str>>
	{
		use crate::ParseChSubErr::*;
		//Parse ClientHello.
		let chdata = ClientHello::parse(chello, rsize, &mut properties.crypto_flags).
			map_err(|x|ParseSniAlpnError::BadClientHello(x))?;
		//Downgrade dest_ip in case caller does not have done that already an DS IPv6 socket is used.
		let port = dest_ip_raw;
		let dest_ip = dest_ip_raw.as_maybe_ipaddr().map(|x|downgrade_to_ipv4(x));
		//Parse host_name.
		let mut sni = chdata.host_name.to_owned();
		if sni != "" {
			let _rdns_addr = match parse_rdns(&sni) {
				Ok(Some(x)) => x,
				Ok(None) => dest_ip.unwrap_or(IpAddr::V6(Ipv6Addr::from([0;16]))),
				Err(_) => fail!(ParseSniAlpnError::BadClientHello(InvalidReverseDns))
			};
		} else {
			//No SNI, try IP address.
			if let Some(dest_ip) = dest_ip { sni = reverse_addr_for(dest_ip); }
		}
		//Is ACME connection?
		for alpn in chdata.alpn_list.iter() {
			if alpn == ACME_TLS_PROTO || alpn == ACME_CONTROL_PROTO {
				//If there is no valid SNI information, use default to force decode attempt.
				//Use None as address since this is ACME, which can not be restricted.
				let (hostdata, sprops) = match sni_info_fn(&sni) {
					Ok(Some(x)) => {
						let sprops = x.get_properties_for_alpn(Some(alpn), true, port).
							map_err(ParseSniAlpnError::AlpnFail)?;
						(x, sprops)
					},
					_ => {
						let control = alpn == ACME_CONTROL_PROTO; 
						ServiceName::default_acme(control)
					},
				};
				return Ok(EarlyParseResult { sni, hostdata, sprops, alpn: Some(alpn.to_owned()) });
			}
		}
		//Grab service information. For any non-ACME connection, defaults are not used.
		let hostdata = sni_info_fn(&sni).map_err(|e|ParseSniAlpnError::BadService(e))?.
			ok_or_else(||ParseSniAlpnError::NoHandlerFound(unwrap_sni(&sni).to_owned()))?;
		//If !chdata.decode_ok, we do not know what the heck this is to complete the handshake, so offer
		//chance for second dispatch.
		let alpnlist = hostdata.alpns_supported_by_sni(dest_ip_raw);
		const SECOND_CHANCE_DISPATCH_ALPN: &'static str = "$btls-no-supported-ciphersuites";
		if !chdata.decode_ok && alpnlist.contains(SECOND_CHANCE_DISPATCH_ALPN) {
			let alpn = SECOND_CHANCE_DISPATCH_ALPN;
			let sprops = hostdata.get_properties_for_alpn(Some(alpn), true, port).
				map_err(ParseSniAlpnError::AlpnFail)?;
			return Ok(EarlyParseResult { sni, hostdata, sprops, alpn: Some(alpn.to_owned()) });
		}
		//This is not ACME connection, do normal ALPN negotiation.
		for alpn in chdata.alpn_list.iter() { if alpnlist.contains(alpn) {
			//Ok, on list, negotiate this.
			let sprops = hostdata.get_properties_for_alpn(Some(alpn), true, port).
				map_err(ParseSniAlpnError::AlpnFail)?;
			return Ok(EarlyParseResult {sni, hostdata, sprops, alpn: Some(alpn.to_owned()) });
		}}
		//ALPN negotiation falls back to default. Note that this needs to signal if ALPN was present or
		//not.
		let sprops = hostdata.get_properties_for_alpn(None, chdata.alpn_present, port).
			map_err(ParseSniAlpnError::AlpnFail)?;
		let dummy_alpn = if chdata.alpn_present { Some(String::new()) } else { None };
		Ok(EarlyParseResult {sni, hostdata, sprops, alpn: dummy_alpn })
	}
	fn proxy_error(&mut self) -> Result<(), IoError>
	{
		if !self.allow_proxy {
			return Err(IoError::new(IoErrorKind::PermissionDenied,
				"PROXY protocol not allowed on this connection"));
		}
		Ok(())
	}
	fn mark_proxy_received_and_pop(&mut self, poplen: usize) -> Result<(), IoError>
	{
		self.proxy_error()?;
		self.pop_data(poplen);
		self.received_proxy = true;
		Ok(())
	}
	fn do_proxyv1(&mut self, tcpinfo: &mut ConnectionInfo) -> Result<bool, IoError>
	{
		self.proxy_error()?;
		//PROXYv1.
		let len = {
			let data = match self.extract_proxyv1() { Some(x) => x, None => return Ok(false) };
			if tcpinfo.from_proxyv1(data, false).is_none() {
				return Err(IoError::new(IoErrorKind::InvalidData, "Bad PROXYv1 header"));
			};
			data.len()
		};
		self.mark_proxy_received_and_pop(len + 2)?;	//+2 for CRLF.
		Ok(true)
	}
	fn do_proxyv2(&mut self, tcpinfo: &mut ConnectionInfo) -> Result<bool, IoError>
	{
		self.proxy_error()?;
		//PROXYv2.
		let len = {
			let data = match self.extract_proxyv2() { Some(x) => x, None => return Ok(false) };
			if tcpinfo.from_proxyv2(data, false).is_none() {
				return Err(IoError::new(IoErrorKind::InvalidData, "Bad PROXYv2 header"));
			};
			data.len()
		};
		self.mark_proxy_received_and_pop(len)?;
		Ok(true)
	}
	pub fn process_proxy(&mut self, tcpinfo: &mut ConnectionInfo) -> Result<bool, IoError>
	{
		if !self.received_proxy {
			match self.first_byte() {
				Some(80) => self.do_proxyv1(tcpinfo),
				Some(13) => self.do_proxyv2(tcpinfo),
				Some(_) => Ok(true),	//No header, so always complete.
				None => Ok(false),	//Incomplete. But we need to wait for CH anyway.
			}
		} else {
			Ok(true)	//Already received, so always complete.
		}
	}
	pub fn received_proxy(&self) -> Option<(usize, Option<usize>)>
	{
		//22 means TLS handshake starts immediately. Also if !allow_proxy and self.size == 0, that means
		//the PROXY header is not allowed, so presumably it would not be sent, so behave like client hello
		//was next.
		if self.received_proxy || self.first_byte() == Some(22) || (self.size == 0 && !self.allow_proxy) {
			Some((self.size, self.get_clienthello_size()))
		} else {
			None
		}
	}
}
