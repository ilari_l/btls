use super::ConfigT;
use super::Connection2;
use crate::metrics_dp::RaiiServiceFailed;
use crate::metrics_dp::RaiiShutdownHold;
use crate::metrics_dp::RaiiShutdown;
use crate::tcp::TcpBuffer;
use crate::tcp::TcpState;
use btls::callbacks::PointInTime;
use btls::callbacks::TimeUnit;
use btls_daemon_helper_lib::IO_WAIT_HUP;
use btls_daemon_helper_lib::IO_WAIT_READ;
use btls_daemon_helper_lib::IO_WAIT_WRITE;
use btls_daemon_helper_lib::is_fatal_error;
use btls_daemon_helper_lib::Poll;
use btls_util_logging::log;
use std::borrow::Cow;
use std::io::Read;
use std::io::Write;


//Service has been shut down, TLS state jettisoned (after writing alert to buffers). The TCP socket is read/write.
//This is also usable if TCP socket has already seen EOF, in that case tcp_in_fail needs to be set.
pub struct ServiceFailed
{
	global: ConfigT,				//Global config.
	tls_error: Cow<'static, str>,			//This logically exists.
	tcp: TcpState,					//The TCP state.
	outgoing: TcpBuffer,				//Buffer for outgoing data.
	flush_dl: PointInTime,				//Deadline for flushing.
	tcp_in_fail: bool,				//TCP input has failed? If yes, go for shutdown after this
							//state has completed.
	shutdown_delay: TimeUnit,			//Delay for shutdown.
	raii: RaiiServiceFailed,
}

impl ServiceFailed
{
	pub fn new(tls_error: Cow<'static, str>, tcp: TcpState, outgoing: TcpBuffer, flush_dl: PointInTime,
		tcp_in_fail: bool, shutdown_delay: TimeUnit, global: ConfigT, raii: RaiiServiceFailed) ->
		ServiceFailed
	{
		ServiceFailed {
			global: global,
			tls_error: tls_error,
			tcp: tcp,
			outgoing: outgoing,
			flush_dl: flush_dl,
			tcp_in_fail: tcp_in_fail,
			shutdown_delay: shutdown_delay,
			raii: raii,
		}
	}
	fn do_shutdown(self, poll: &mut Poll) -> Shutdown
	{
		//Shutdown the TCP.
		self.tcp.deregister_poll(poll);
		Shutdown {
			global: self.global,
			tls_error: self.tls_error,
			raii: self.raii.to_shutdown(),
		}
	}
	pub fn handle_fault(self, poll: &mut Poll) -> Connection2
	{
		Connection2::Shutdown(self.do_shutdown(poll))
	}
	fn after_flush_complete(mut self, poll: &mut Poll) -> Connection2
	{
		//If TCP input has failed, move to shutdown immediately. Otherwise enter shutdown hold.
		if self.tcp_in_fail {
			Connection2::Shutdown(self.do_shutdown(poll))
		} else {
			//There are no pollers to remove. However, the file descriptor needs to be EOF'd. It is
			//assumed that if output pipe has been closed, then ServiceFailed state is never entered.
			self.tcp.fd().shutdown_write().ok();
			if let Err(err) = self.tcp.rearm_poll(poll, IO_WAIT_READ) {
				log!(WARNING "failed to reregister poll: {err}");
				return Connection2::Shutdown(self.do_shutdown(poll));
			}
			Connection2::ShutdownHold(ShutdownHold {
				global: self.global,
				tls_error: self.tls_error,
				tcp: self.tcp,
				end_at: PointInTime::from_now(self.shutdown_delay),
				raii: self.raii.to_shutdown_hold(),
			})
		}
	}
	pub fn get_next_timed_event(&self, _now: PointInTime) -> Option<PointInTime>
	{
		Some(self.flush_dl)
	}
	pub fn handle_timed_event(self, now: PointInTime, poll: &mut Poll) -> Connection2
	{
		//If flush deadline has been exceeded, shut down immediately. Otherwise continue.
		if now >= self.flush_dl {
			Connection2::Shutdown(self.do_shutdown(poll))
		}  else {
			Connection2::ServiceFailed(self)
		}
	}
	pub fn handle_event(mut self, poll: &mut Poll, tok: usize, kind: u8) -> Connection2
	{
		//If TCP is writeable, flush.
		if kind & IO_WAIT_WRITE != 0 && self.tcp.is_tcp_interrupt(tok) {
			//Flush the output buffer. We do not log any errors.
			match self.tcp.fd().write(self.outgoing.contents()) {
				Ok(x) => {
					self.global.metrics(|m|m.write_tcp(x));
					self.outgoing.pop(x)
				},
				Err(x) => if is_fatal_error(&x) {
					return Connection2::Shutdown(self.do_shutdown(poll));
				}
			}
		}
		//If TCP is readable, discard
		if kind & (IO_WAIT_READ|IO_WAIT_HUP) != 0 && self.tcp.is_tcp_interrupt(tok) && !self.tcp_in_fail {
			//Discard the incoming data. We do not log any errors.
			let mut buffer = [0;16384];
			match self.tcp.fd().read(&mut buffer) {
				Ok(0) => self.tcp_in_fail = true,			//EOF.
				Err(x) => if is_fatal_error(&x) {
					return Connection2::Shutdown(self.do_shutdown(poll));
				},
				Ok(x) => self.global.metrics(|m|m.read_tcp(x)),		//Discard.
			}
		}
		self.tail_processing(poll)
	}
	pub fn tail_processing(self, poll: &mut Poll) -> Connection2
	{
		let mut ready = 0;
		//If output buffer still has data, assert writable. If tcp is open, assert readable.
		if !self.tcp_in_fail { ready = ready | IO_WAIT_READ; }
		if self.outgoing.has_content() { ready = ready | IO_WAIT_WRITE; }
		if ready & IO_WAIT_WRITE == 0 {
			//Move to shutdown hold.
			return self.after_flush_complete(poll);
		}
		//Rearm poller. Note that writable is always set. If this fails, shut down immediately.
		if let Err(err) = self.tcp.rearm_poll(poll, ready) {
			log!(WARNING "failed to reregister poll: {err}");
			return Connection2::Shutdown(self.do_shutdown(poll));
		}
		Connection2::ServiceFailed(self)
	}
}

//Service has been shut down, TLS state jettisoned. The TCP socket is in read-only mode.
pub struct ShutdownHold
{
	global: ConfigT,				//Global config.
	tls_error: Cow<'static, str>,			//Shut down connections always have tls_error.
	tcp: TcpState,					//The TCP state is still held.
	end_at: PointInTime,				//The PiT the hold ends at.
	raii: RaiiShutdownHold,
}

impl ShutdownHold
{
	fn do_shutdown(self, poll: &mut Poll) -> Shutdown
	{
		//Move the socket to shutdown state. This means shutting down TCP.
		//and creating new shutdown context.
		self.tcp.deregister_poll(poll);
		Shutdown {
			global: self.global,
			tls_error: self.tls_error,
			raii: self.raii.to_shutdown(),
		}
	}
	pub fn handle_fault(self, poll: &mut Poll) -> Connection2
	{
		Connection2::Shutdown(self.do_shutdown(poll))
	}
	pub fn get_next_timed_event(&self, _now: PointInTime) -> Option<PointInTime>
	{
		Some(self.end_at)
	}
	pub fn handle_timed_event(self, now: PointInTime, poll: &mut Poll) -> Connection2
	{
		if now >= self.end_at {
			Connection2::Shutdown(self.do_shutdown(poll))
		} else {
			Connection2::ShutdownHold(self)
		}
	}
	pub fn handle_event(mut self, poll: &mut Poll, tok: usize, kind: u8) -> Connection2
	{
		//We only ever care about read events on TCP descriptor.
		if kind & (IO_WAIT_READ|IO_WAIT_HUP) != 0 && self.tcp.is_tcp_interrupt(tok) {
			if let Err(err) = self.tcp.rearm_poll(poll, IO_WAIT_READ) {
				log!(WARNING "failed to reregister poll: {err}");
				return Connection2::Shutdown(self.do_shutdown(poll));
			}
			//Discard the incoming data. If encountering either EOF or fatal error, move to shutdown
			//immedately.
			let mut buffer = [0;16384];
			match self.tcp.fd().read(&mut buffer) {
				Ok(0) => return Connection2::Shutdown(self.do_shutdown(poll)),
				Err(x) => if is_fatal_error(&x) {
					return Connection2::Shutdown(self.do_shutdown(poll));
				},
				Ok(x) => self.global.metrics(|m|m.read_tcp(x)),		//Discard.
			}
		}
		Connection2::ShutdownHold(self)
	}
}

//Service is dead.
pub struct Shutdown
{
	pub global: ConfigT,					//Global metrics.
	pub tls_error: Cow<'static, str>,			//Shut down connections always have tls_error.
	pub raii: RaiiShutdown,
}
