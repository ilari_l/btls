use acmed2_memory::String;
use acmed2_memory::ToOwned;
use acmed2_memory::Vec;
use acmed2_formats::asn1_bit_string;
use acmed2_formats::asn1_csc;
use acmed2_formats::asn1_csp;
use acmed2_formats::asn1_integer;
use acmed2_formats::asn1_object_identifier;
use acmed2_formats::asn1_octet_string;
use acmed2_formats::asn1_printable_string;
use acmed2_formats::asn1_sequence;
use acmed2_formats::asn1_set;
use acmed2_formats::asn1_utf8_string;
use acmed2_formats::Rope;
use acmed2_formats::rope_concat;
use acmed2_formats::RopeAsn1;
use acmed2_formats::RopeIterator;

macro_rules! sequence
{
	($($tokens:expr),*) => { asn1_sequence(rope_concat!($($tokens),*)) };
}

macro_rules! oid
{
	($($oid:tt)*) => { asn1_object_identifier(&[$($oid),*]) };
}

#[derive(Copy,Clone)]
enum SignAlgorithm
{
	EcdsaSha256,
	EcdsaSha384,
	EcdsaSha512,
	Ed25519,
	Ed448,
}

impl SignAlgorithm
{
	fn to_x509<'a>(&'a self) -> impl Rope+'a
	{
		match self {
			&SignAlgorithm::EcdsaSha256 => sequence!(oid!(1 2 840 10045 4 3 2)),
			&SignAlgorithm::EcdsaSha384 => sequence!(oid!(1 2 840 10045 4 3 3)),
			&SignAlgorithm::EcdsaSha512 => sequence!(oid!(1 2 840 10045 4 3 4)),
			&SignAlgorithm::Ed25519 => sequence!(oid!(1 3 101 112)),
			&SignAlgorithm::Ed448 => sequence!(oid!(1 3 101 113)),
		}
	}
	fn to_tls(&self) -> u16
	{
		match self {
			&SignAlgorithm::EcdsaSha256 => 0x403,
			&SignAlgorithm::EcdsaSha384 => 0x503,
			&SignAlgorithm::EcdsaSha512 => 0x603,
			&SignAlgorithm::Ed25519 => 0x807,
			&SignAlgorithm::Ed448 => 0x808,
		}
	}
}

enum KeyAlgorithm
{
	NistP256,
	NistP384,
	NistP521,
	Ed25519,
	Ed448,
}

impl KeyAlgorithm
{
	fn to_x509<'a>(&'a self) -> impl Rope+'a
	{
		let ed25519 = oid!(1 3 101 112);
		let ed448 = oid!(1 3 101 113);
		let p256 = oid!(1 2 840 10045 3 1 7);
		let p384 = oid!(1 3 132 0 34);
		let p521 = oid!(1 3 132 0 35);
		let ecdsa = oid!(1 2 840 10045 2 1);
		match self {
			&KeyAlgorithm::NistP256 => sequence!(ecdsa, Some(p256)),
			&KeyAlgorithm::NistP384 => sequence!(ecdsa, Some(p384)),
			&KeyAlgorithm::NistP521 => sequence!(ecdsa, Some(p521)),
			&KeyAlgorithm::Ed25519 => sequence!(ed25519, None),
			&KeyAlgorithm::Ed448 => sequence!(ed448, None),
		}
	}
	fn to_sig_algo(&self) -> SignAlgorithm
	{
		match self {
			&KeyAlgorithm::NistP256 => SignAlgorithm::EcdsaSha256,
			&KeyAlgorithm::NistP384 => SignAlgorithm::EcdsaSha384,
			&KeyAlgorithm::NistP521 => SignAlgorithm::EcdsaSha512,
			&KeyAlgorithm::Ed25519 => SignAlgorithm::Ed25519,
			&KeyAlgorithm::Ed448 => SignAlgorithm::Ed448,
		}
	}
}

pub fn is_printable(x: &[u8]) -> bool
{
	let mut m = [0u32;8];
	for &b in x.iter() { m[(b >> 5) as usize] |= 1 << b%32; }
	let a = m[0]|m[4]|m[5]|m[6]|m[7];
	let (b,c) = (m[1],m[2]|m[3]);
	(a|b&0x5800047E|c&0xF8000001)==0
}

fn string_encode<'a>(x: &'a str) -> impl Rope+'a
{
	//Autoselect printable string / UTF-8 string.
	if is_printable(x.as_bytes())  { asn1_printable_string(x) } else { asn1_utf8_string(x) }
}

pub struct CsrParameters
{
	subject: Vec<(Vec<u64>,String)>,
	san_dns: Vec<Vec<u8>>,
	san_ip: Vec<Vec<u8>>,
	san_other: Vec<((u8,u64),Vec<u8>)>,
	extensions: Vec<(Vec<u64>,Vec<u8>)>,
	algo: KeyAlgorithm,
	rawkey: Vec<u8>,
}

impl CsrParameters
{
	pub fn new(spki: &[u8]) -> Result<CsrParameters, String>
	{
		const NISTP256_PREFIX: [u8;27] = [
			48,89,48,19,6,7,42,134,72,206,61,2,1,6,8,42,134,72,206,61,3,1,7,3,66,0,4
		];
		const NISTP384_PREFIX: [u8;24] = [
			48,118,48,16,6,7,42,134,72,206,61,2,1,6,5,43,129,4,0,34,3,98,0,4
		];
		const NISTP521_PREFIX: [u8;26] = [
			48,129,155,48,16,6,7,42,134,72,206,61,2,1,6,5,43,129,4,0,35,3,129,134,0,4
		];
		const ED25519_PREFIX: [u8;12] = [48,42,48,5,6,3,43,101,112,3,33,0];
		const ED448_PREFIX: [u8;12] = [48,67,48,5,6,3,43,101,113,3,58,0];
		let (algo, rawkey) = if spki.len() == 91 && spki.starts_with(&NISTP256_PREFIX) {
			(KeyAlgorithm::NistP256, &spki[26..])
		} else if spki.len() == 120 && spki.starts_with(&NISTP384_PREFIX) {
			(KeyAlgorithm::NistP384, &spki[23..])
		} else if spki.len() == 158 && spki.starts_with(&NISTP521_PREFIX) {
			(KeyAlgorithm::NistP521, &spki[25..])
		} else if let (44, Some(key)) = (spki.len(), spki.strip_prefix(&ED25519_PREFIX)) {
			(KeyAlgorithm::Ed25519, key)
		} else if let (69, Some(key)) = (spki.len(), spki.strip_prefix(&ED448_PREFIX)) {
			(KeyAlgorithm::Ed448, key)
		} else {
			return Err("Unrecognized key type".to_owned());
		};
		Ok(CsrParameters {
			subject: Vec::new(),
			san_dns: Vec::new(),
			san_ip: Vec::new(),
			san_other: Vec::new(),
			extensions: Vec::new(),
			algo: algo,
			rawkey: rawkey.to_vec(),
		})
	}
	pub fn add_subject_entry(&mut self, oid: &[u64], name: &str)
	{
		self.subject.push((oid.to_vec(),name.to_owned()));
	}
	pub fn add_dns_name(&mut self, name: &str)
	{
		self.san_dns.push(name.as_bytes().to_vec());
	}
	pub fn add_ipv4_address(&mut self, addr: [u8;4])
	{
		self.san_ip.push(addr.to_vec());
	}
	pub fn add_ipv6_address(&mut self, addr: [u8;16])
	{
		self.san_ip.push(addr.to_vec());
	}
	pub fn add_san_other(&mut self, tag: (u8,u64), value: &[u8])
	{
		self.san_other.push((tag, value.to_vec()));
	}
	pub fn add_extension(&mut self, oid: &[u64], value: impl Rope)
	{
		self.extensions.push((oid.to_vec(), value.as_vec()));
	}
	pub fn prepare(&self) -> CsrPending
	{
		let spki = sequence!(
			self.algo.to_x509(),
			asn1_bit_string(0, &self.rawkey)
		);
		let san_ext = sequence!(
			oid!(2 5 29 17),
			asn1_octet_string(sequence!(
				RopeIterator(self.san_dns.iter().map(|n|asn1_csp(2,n))),
				RopeIterator(self.san_ip.iter().map(|n|asn1_csp(7,n))),
				RopeIterator(self.san_other.iter().map(|&(t,ref n)|RopeAsn1(t,n)))
			))
		);
		let other_exts = RopeIterator(self.extensions.iter().map(|&(ref k,ref v)|sequence!(
			asn1_object_identifier(k),
			asn1_octet_string(v)
		)));
		let subject = sequence!(RopeIterator(self.subject.iter().map(|(k,v)|asn1_set(sequence!(
			asn1_object_identifier(k),
			string_encode(v)
		)))));
		let tbs = sequence!(
			asn1_integer(0u8),
			subject,				
			spki,
			asn1_csc(0, sequence!(
				oid!(1 2 840 113549 1 9 14),
				asn1_set(sequence!(
					san_ext,
					other_exts
				))
			))
		).as_vec();
		CsrPending {
			tbs: tbs,
			siginfo: self.algo.to_sig_algo(),
		}
	}
}

pub struct CsrPending
{
	tbs: Vec<u8>,
	siginfo: SignAlgorithm,
}

impl CsrPending
{
	pub fn get_tbs<'a>(&'a self) -> (u16, &'a [u8])
	{
		(self.siginfo.to_tls(), &self.tbs)
	}
	pub fn commit(self, signature: &[u8]) -> Vec<u8>
	{
		sequence!(
			&self.tbs,
			self.siginfo.to_x509(),
			asn1_bit_string(0, signature)
		).as_vec()
	}
}

#[cfg(test)]
static PRINTABLE: &'static [u8] = 
	b"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 '()+,-./:=?";

#[test]
fn test_all_printable()
{
	assert!(is_printable(PRINTABLE));
}

#[test]
fn test_all_nonprintables()
{
	'x: for i in 0..256u32 {
		for &b in PRINTABLE.iter() {
			if b == i as u8 { continue 'x; }
		}
		let x = [i as u8];
		assert!(!is_printable(&x));
	}
}

#[test]
fn test_csr_assembly()
{
	use acmed2_cryptos::Hash;
	use acmed2_cryptos::HashUpdate;
	use acmed2_cryptos::Sha256;
	let sig = [
		0x60, 0xED, 0xA1, 0x32, 0x64, 0x38, 0x53, 0x27, 0x86, 0xC8, 0xF6, 0x5D, 0xBD, 0x30, 0x83, 0x55,
		0xA8, 0xC2, 0x13, 0x7D, 0x9C, 0xE8, 0x1D, 0x37, 0x23, 0x29, 0x9C, 0x7B, 0x76, 0x7B, 0x20, 0x38,
		0x7D, 0x40, 0xB7, 0x88, 0x7D, 0xB4, 0xCF, 0x47, 0xB2, 0xDC, 0x86, 0xA7, 0xA0, 0x73, 0x87, 0xCE,
		0x6F, 0x59, 0x4A, 0x16, 0xA1, 0x11, 0x90, 0x59, 0xF5, 0x9D, 0x57, 0x8A, 0x0E, 0x62, 0x6B, 0x0C,
	];
	let spki = [
		48,42,48,5,6,3,43,101,112,3,33,0,
		0x29, 0x8C, 0xC3, 0xF0, 0xD0, 0xBC, 0x82, 0x12, 0x71, 0x65, 0x15, 0xA8, 0x07, 0x8A, 0x0F, 0xAD,
		0xE5, 0xE3, 0xB0, 0x86, 0xD8, 0xF2, 0xAB, 0x01, 0xF0, 0x05, 0xEB, 0x34, 0xE1, 0x45, 0x25, 0xE0,
	];
	let reference = [
		0xd0, 0x52, 0xfd, 0x0c, 0xb3, 0x4e, 0x66, 0x29, 0x76, 0x34, 0xd4, 0x2e, 0x67, 0xdc, 0xb4, 0x59,
		0x3a, 0x3e, 0xd2, 0x09, 0x21, 0xca, 0xfa, 0x20, 0xde, 0xf9, 0x9a, 0xec, 0x39, 0x4e, 0x84, 0x95,
	];
	let treference = [
		0xf4, 0xaf, 0xb0, 0x39, 0xe0, 0x10, 0xc3, 0x62, 0x5e, 0xc3, 0xee, 0x21, 0xf6, 0xdc, 0x0d, 0xb4,
		0xd3, 0x2d, 0xba, 0xda, 0x6f, 0x83, 0x46, 0x54, 0xd6, 0x68, 0xd6, 0xad, 0xd1, 0x88, 0x64, 0xde,
	];
	let mut csr = CsrParameters::new(&spki).expect("CsrParameters::new() failed");
	csr.add_subject_entry(&[2,5,4,3], "bar");
	csr.add_dns_name("bar");
	let csr = csr.prepare();
	let (alg, tbs) = csr.get_tbs();
	assert_eq!(alg, 0x807);
	let mut tbsh = Sha256::new();
	tbsh.update(&tbs);
	assert_eq!(tbsh.hash(), &treference[..]); 
	let csr = csr.commit(&sig);
	let mut csrh = Sha256::new();
	csrh.update(&csr);
	assert_eq!(csrh.hash(), &reference[..]); 
}

#[test]
fn test_no_subject()
{
	let spki = [
		48,42,48,5,6,3,43,101,112,3,33,0,
		0x29, 0x8C, 0xC3, 0xF0, 0xD0, 0xBC, 0x82, 0x12, 0x71, 0x65, 0x15, 0xA8, 0x07, 0x8A, 0x0F, 0xAD,
		0xE5, 0xE3, 0xB0, 0x86, 0xD8, 0xF2, 0xAB, 0x01, 0xF0, 0x05, 0xEB, 0x34, 0xE1, 0x45, 0x25, 0xE0,
	];
	let csr = CsrParameters::new(&spki).expect("CsrParameters::new() failed");
	let csr = csr.prepare();
	let (alg, tbs) = csr.get_tbs();
	assert_eq!(alg, 0x807);
	assert_eq!(&tbs[2..7], &[2,1,0,48,0]);
}

#[cfg(test)]
fn echo_spki(spki: &[u8], expect_alg: u16)
{
	let csr = CsrParameters::new(&spki).expect("CsrParameters::new() failed");
	let csr = csr.prepare();
	let (alg, tbs) = csr.get_tbs();
	assert_eq!(alg, expect_alg);
	let o = if tbs[1] == 129 { 8 } else { 7 };
	assert_eq!(&tbs[o..][..spki.len()], spki);
}

#[test]
fn echo_spki_p256()
{
	echo_spki(&[
		48, 89, 48, 19, 6, 7, 42, 134, 72, 206, 61, 2, 1, 6, 8, 42, 134, 72, 206, 61, 3, 1, 7, 3, 66, 0,
		4, 66, 162, 98, 133, 88, 198, 188, 73, 53, 200, 185, 102, 197, 76, 187, 255, 160, 178, 28, 40,
		110, 144, 138, 120, 240, 83, 222, 10, 124, 142, 134, 254, 113, 142, 195, 189, 82, 44, 240, 201,
		1, 61, 178, 1, 222, 119, 94, 3, 118, 202, 248, 84, 52, 107, 186, 224, 225, 124, 169, 172, 123,
		130, 233, 118
	], 0x403);
}

#[test]
fn echo_spki_p384()
{
	echo_spki(&[
		48, 118, 48, 16, 6, 7, 42, 134, 72, 206, 61, 2, 1, 6, 5, 43, 129, 4, 0, 34, 3, 98, 0, 4, 171,
		237, 24, 154, 68, 172, 168, 175, 226, 95, 206, 241, 105, 143, 53, 101, 82, 53, 179, 196, 115,
		247, 152, 202, 26, 189, 86, 231, 226, 141, 124, 44, 157, 116, 228, 141, 231, 72, 166, 137, 211,
		58, 179, 180, 247, 16, 3, 128, 186, 95, 164, 47, 144, 122, 181, 113, 16, 179, 137, 182, 16, 140,
		69, 81, 241, 172, 173, 130, 246, 210, 51, 85, 241, 241, 166, 6, 52, 144, 54, 62, 219, 133, 26,
		115, 148, 28, 226, 104, 85, 194, 221, 145, 150, 93, 16, 82
	], 0x503);
}

#[test]
fn echo_spki_p521()
{
	echo_spki(&[
		48, 129, 155, 48, 16, 6, 7, 42, 134, 72, 206, 61, 2, 1, 6, 5, 43, 129, 4, 0, 35, 3, 129, 134, 0,
		4, 1, 58, 186, 88, 90, 155, 189, 178, 121, 219, 178, 19, 179, 137, 101, 210, 177, 3, 220, 224,
		176, 108, 114, 65, 93, 65, 43, 52, 164, 197, 71, 76, 146, 50, 162, 166, 136, 90, 142, 166, 20,
		18, 176, 49, 102, 54, 17, 57, 117, 143, 141, 216, 190, 88, 116, 178, 246, 243, 123, 3, 77, 222,
		100, 83, 7, 53, 1, 131, 2, 64, 191, 114, 197, 115, 21, 213, 135, 17, 94, 45, 145, 69, 104, 114,
		61, 219, 78, 226, 114, 32, 188, 243, 226, 83, 175, 149, 72, 166, 207, 50, 68, 73, 108, 81, 137,
		232, 137, 125, 73, 180, 166, 187, 254, 3, 122, 2, 2, 82, 10, 177, 219, 57, 14, 92, 180, 118, 66,
		254, 177, 96, 14, 202
	], 0x603);
}

#[test]
fn echo_spki_25519()
{
	echo_spki(&[
		48, 42, 48, 5, 6, 3, 43, 101, 112, 3, 33, 0, 225, 36, 77, 126, 63, 191, 223, 88, 170, 176, 17, 163,
		70, 121, 212, 75, 33, 84, 249, 176, 31, 145, 164, 92, 20, 154, 56, 99, 11, 228, 153, 130
	], 0x807);
}

#[test]
fn echo_spki_448()
{
	echo_spki(&[
		48, 67, 48, 5, 6, 3, 43, 101, 113, 3, 58, 0, 220, 199, 57, 124, 126, 112, 233, 132, 221, 245, 85,
		251, 100, 28, 165, 146, 167, 152, 191, 134, 57, 189, 22, 163, 230, 181, 154, 134, 37, 207, 18, 95,
		242, 199, 64, 1, 170, 244, 21, 83, 117, 20, 237, 237, 45, 51, 109, 46, 219, 205, 242, 37, 32, 143,
		155, 70, 128
	], 0x808);
}
