use super::*;
use acmed2_memory::Cow;
use acmed2_memory::ToString;


#[test]
fn test_cert_chain_parse()
{
	let ee = include_bytes!("test-ee.crt"); let ee = &ee[..];
	let ca = include_bytes!("test-ca.crt"); let ca = &ca[..];
	let root = include_bytes!("test-root.crt"); let root = &root[..];
	let root_s = X509SignedObj::new(root).expect("Root cert sigparse failed");
	let root_p = root_s.parse_certificate().expect("Root cert parse failed");
	let mut trust_anchors = TrustAnchorSet::new();
	trust_anchors.add(&root_p);
	let refname = SanEntry::Name(Cow::Borrowed(b"LSNES.TASBOT.NET"));
	let timenow = 1592866288;
	let timenow1 = 1592866289-90*86400;
	let pchain = [ee, ca];		let pchain = pchain.iter().cloned();
	let fchain = [ee, ca, root];	let fchain = fchain.iter().cloned();

	trust_anchors.verify_chain(pchain.clone(), &[refname.clone()], timenow).expect("Chain validation failed");
	trust_anchors.verify_chain(fchain.clone(), &[refname.clone()], timenow).expect("Chain validation failed");
	trust_anchors.verify_chain(pchain.clone(), &[refname.clone()], timenow1).expect("Chain validation failed");
	trust_anchors.verify_chain(fchain.clone(), &[refname.clone()], timenow1).expect("Chain validation failed");
}

#[test]
fn y2k_start()
{
	assert_eq!(text_to_timestamp(b"20000101000000").unwrap(), 946684800);
}

#[test]
fn cycle_days()
{
	let mut year = 1;
	let mut month = 1;
	let mut day = 1;
	let mut expect = -62135596800;
	let mut buf = [0,0,0,0,0,0,0,0,48,48,48,48,48,48];
	while year < 10000 {
		buf[0] = 48 + ((year / 1000) % 10) as u8;
		buf[1] = 48 + ((year / 100) % 10) as u8;
		buf[2] = 48 + ((year / 10) % 10) as u8;
		buf[3] = 48 + ((year / 1) % 10) as u8;
		buf[4] = 48 + ((month / 10) % 10) as u8;
		buf[5] = 48 + ((month / 1) % 10) as u8;
		buf[6] = 48 + ((day / 10) % 10) as u8;
		buf[7] = 48 + ((day / 1) % 10) as u8;
		assert_eq!(text_to_timestamp(&buf).unwrap(), expect);
		expect += 86400;
		day += 1;
		let dbnd = match month {
			1|3|5|7|8|10|12 => 32,
			4|6|9|11 => 31,
			2 if year % 4 == 0 && (year % 400 == 0 || year % 100 != 0) => 30,
			2 => 29,
			_ => 0,
		};
		if day == dbnd { day = 1; month += 1; }
		if month == 13 { month = 1; year += 1; }
	}
}

#[test]
fn cycle_seconds()
{
	let mut hour = 0;
	let mut minute = 0;
	let mut second = 0;
	let mut expect = 0;
	let mut buf = [49,57,55,48,48,49,48,49,0,0,0,0,0,0];
	while hour < 24 {
		buf[8] = 48 + ((hour / 10) % 10) as u8;
		buf[9] = 48 + ((hour / 1) % 10) as u8;
		buf[10] = 48 + ((minute / 10) % 10) as u8;
		buf[11] = 48 + ((minute / 1) % 10) as u8;
		buf[12] = 48 + ((second / 10) % 10) as u8;
		buf[13] = 48 + ((second / 1) % 10) as u8;
		assert_eq!(text_to_timestamp(&buf).unwrap(), expect);
		expect += 1;
		second += 1;
		if second == 60 { second = 0; minute += 1; }
		if minute == 60 { minute = 0; hour += 1; }
	}
}

#[test]
fn bad_timestamps()
{
	assert!(text_to_timestamp(b"00000101000000").is_none());	//Year 0
	assert!(text_to_timestamp(b"20000001000000").is_none());	//Month 0.
	assert!(text_to_timestamp(b"20001301000000").is_none());	//Month 13.
	assert!(text_to_timestamp(b"20000100000000").is_none());	//Day 0.
	assert!(text_to_timestamp(b"20000132000000").is_none());	//January 32nd.
	assert!(text_to_timestamp(b"20010229000000").is_none());	//February 29th.
	assert!(text_to_timestamp(b"20000230000000").is_none());	//February 30th.
	assert!(text_to_timestamp(b"20000332000000").is_none());	//March 32nd.
	assert!(text_to_timestamp(b"20000431000000").is_none());	//April 31st.
	assert!(text_to_timestamp(b"20000532000000").is_none());	//May 32nd.
	assert!(text_to_timestamp(b"20000631000000").is_none());	//June 31st.
	assert!(text_to_timestamp(b"20000732000000").is_none());	//July 32nd.
	assert!(text_to_timestamp(b"20000832000000").is_none());	//August 32nd.
	assert!(text_to_timestamp(b"20000931000000").is_none());	//September 31st.
	assert!(text_to_timestamp(b"20001032000000").is_none());	//October 32nd.
	assert!(text_to_timestamp(b"20001131000000").is_none());	//November 31st.
	assert!(text_to_timestamp(b"20001232000000").is_none());	//December 32nd.
	assert!(text_to_timestamp(b"20000101240000").is_none());	//Hour 24.
	assert!(text_to_timestamp(b"20000101006000").is_none());	//Minute 60.
	assert!(text_to_timestamp(b"20000101000061").is_none());	//Second 61.
	assert!(text_to_timestamp(b"20000101225960").is_none());	//Second 60.
	assert!(text_to_timestamp(b"20000101235860").is_none());	//Second 60.
	assert!(text_to_timestamp(b"20000:01000000").is_none());
}

#[test]
fn cert_chain_parse()
{
	let ee = include_bytes!("test-ee.crt");
	let ca = include_bytes!("test-ca.crt");
	let root = include_bytes!("test-root.crt");
	let ee = X509SignedObj::new(ee).expect("EE cert sigparse failed");
	let ca = X509SignedObj::new(ca).expect("CA cert sigparse failed");
	let root = X509SignedObj::new(root).expect("Root cert sigparse failed");
	let _ee_p = ee.parse_certificate().expect("EE cert parse failed");
	let ca_p = ca.parse_certificate().expect("CA cert parse failed");
	let root_p = root.parse_certificate().expect("Root cert parse failed");
	ee.check_signature(__borrow_key(&ca_p.key)).expect("EE cert signature check failed");
	ca.check_signature(__borrow_key(&root_p.key)).expect("CA cert signature check failed");
	root.check_signature(__borrow_key(&root_p.key)).expect("Root cert signature check failed");
}

#[test]
fn test_parse_csr()
{
	let ocsr = include_bytes!("test.csr");
	let csr = X509SignedObj::new(ocsr).expect("CSR sigparse failed");
	let csr = csr.parse_csr().expect("CSR parse failed");
	assert_eq!(&csr.san[..], &[SanEntry::Name(Cow::Borrowed(b"LSNES.TASBOT.NET"))]);
	assert_eq!(csr.key_algo, &[6,7,42,134,72,206,61,2,1,6,8,42,134,72,206,61,3,1,7][..]);
	assert_eq!(csr.key,  &b"\x04\x74\x71\x25\x99\x08\xCA\x82\x67\xA7\x61\x40\x3B\x64\x71\xDE\
				\x7A\xA0\x6C\x5C\xDC\x44\x52\xD1\xCE\xBD\x23\x25\xC2\xD2\xC3\x6E\
				\x0A\x49\x89\x3E\x7C\x4A\x8A\x86\x8A\x8B\x12\xA6\x83\xC4\x62\x30\
				\x42\xD3\xD7\x10\xDA\x56\xF7\xAD\x88\x87\x4B\xFC\xA8\x30\x00\x9A\
				\xED"[..]);
	assert_eq!(csr.tbs, &ocsr[4..178]);
	assert_eq!(csr.subject, &ocsr[10..39]);
	assert_eq!(csr.spki, &ocsr[39..130]);
	assert_eq!(csr.sigalg, &ocsr[180..190]);
	assert_eq!(csr.signature, &ocsr[193..264]);
}

#[test]
fn test_cert_csr_consistent()
{
	let ee = include_bytes!("test-ee.crt");
	let csr = include_bytes!("test.csr");
	let ee = parse_x509v3_certificate(ee).expect("EE cert parse failed");
	let csr = parse_x509v3_csr(csr).expect("CSR parse failed");
	check_certificate_consistent_with_csr(&ee, &csr).expect("Certificate consistency failed");
}

#[cfg(test)]
fn format(msg: impl Display) -> String
{
	msg.to_string()
}

#[test]
fn san_entry_ipv4()
{
	assert_eq!(&format(SanEntry::Ipv4([1,2,3,4])), "[1.2.3.4]");
}

#[test]
fn san_entry_ipv6()
{
	assert_eq!(&format(SanEntry::Ipv6([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])),
		"[::]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0])),
		"[1::]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,0,0,0,0,0,0,0,0,0,0,0])),
		"[1:2::]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,3,0,0,0,0,0,0,0,0,0,0])),
		"[1:2:3::]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,3,0,4,0,0,0,0,0,0,0,0])),
		"[1:2:3:4::]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,3,0,4,0,5,0,0,0,0,0,0])),
		"[1:2:3:4:5::]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,3,0,4,0,5,0,6,0,0,0,0])),
		"[1:2:3:4:5:6::]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,3,0,4,0,5,0,6,0,7,0,0])),
		"[1:2:3:4:5:6:7::]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,3,0,4,0,5,0,6,0,7,0,8])),
		"[1:2:3:4:5:6:7:8]");
	assert_eq!(&format(SanEntry::Ipv6([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8])),
		"[::8]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,8])),
		"[1::8]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,0,0,0,0,0,0,0,0,0,0,8])),
		"[1:2::8]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,3,0,0,0,0,0,0,0,0,0,8])),
		"[1:2:3::8]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,3,0,4,0,0,0,0,0,0,0,8])),
		"[1:2:3:4::8]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,3,0,4,0,5,0,0,0,0,0,8])),
		"[1:2:3:4:5::8]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,3,0,4,0,5,0,6,0,0,0,8])),
		"[1:2:3:4:5:6::8]");
	assert_eq!(&format(SanEntry::Ipv6([0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,8])),
		"[::7:8]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,0,0,0,0,0,0,0,0,0,0,7,0,8])),
		"[1::7:8]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,0,0,0,0,0,0,0,0,7,0,8])),
		"[1:2::7:8]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,3,0,0,0,0,0,0,0,7,0,8])),
		"[1:2:3::7:8]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,3,0,4,0,0,0,0,0,7,0,8])),
		"[1:2:3:4::7:8]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,3,0,4,0,5,0,0,0,7,0,8])),
		"[1:2:3:4:5::7:8]");
	assert_eq!(&format(SanEntry::Ipv6([0,0,0,0,0,0,0,0,0,0,0,6,0,7,0,8])),
		"[::6:7:8]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,0,0,0,0,0,0,0,0,6,0,7,0,8])),
		"[1::6:7:8]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,0,0,0,0,0,0,6,0,7,0,8])),
		"[1:2::6:7:8]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,3,0,0,0,0,0,6,0,7,0,8])),
		"[1:2:3::6:7:8]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,3,0,4,0,0,0,6,0,7,0,8])),
		"[1:2:3:4::6:7:8]");
	assert_eq!(&format(SanEntry::Ipv6([0,0,0,0,0,0,0,0,0,5,0,6,0,7,0,8])),
		"[::5:6:7:8]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,0,0,0,0,0,0,5,0,6,0,7,0,8])),
		"[1::5:6:7:8]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,0,0,0,0,5,0,6,0,7,0,8])),
		"[1:2::5:6:7:8]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,3,0,0,0,5,0,6,0,7,0,8])),
		"[1:2:3::5:6:7:8]");
	assert_eq!(&format(SanEntry::Ipv6([0,0,0,0,0,0,0,4,0,5,0,6,0,7,0,8])),
		"[::4:5:6:7:8]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,0,0,0,0,4,0,5,0,6,0,7,0,8])),
		"[1::4:5:6:7:8]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,2,0,0,0,4,0,5,0,6,0,7,0,8])),
		"[1:2::4:5:6:7:8]");
	assert_eq!(&format(SanEntry::Ipv6([0,0,0,0,0,3,0,4,0,5,0,6,0,7,0,8])),
		"[::3:4:5:6:7:8]");
	assert_eq!(&format(SanEntry::Ipv6([0,1,0,0,0,3,0,4,0,5,0,6,0,7,0,8])),
		"[1::3:4:5:6:7:8]");
	assert_eq!(&format(SanEntry::Ipv6([0,0,0,2,0,3,0,4,0,5,0,6,0,7,0,8])),
		"[::2:3:4:5:6:7:8]");
}

#[test]
fn san_entry_name()
{
	assert_eq!(&format(SanEntry::Name(Cow::Borrowed(b"foo.bar"))), "foo.bar");
	assert_eq!(&format(SanEntry::Name(Cow::Borrowed(b"foo\\bar"))), "foo\\\\bar");
	assert_eq!(&format(SanEntry::Name(Cow::Borrowed(b"foo bar"))), "foo\\ bar");
	assert_eq!(&format(SanEntry::Name(Cow::Borrowed(b"foo\nbar"))), "foo\\x0abar");
	assert_eq!(&format(SanEntry::Name(Cow::Borrowed(b"foo\x7Fbar"))), "foo\\x7fbar");
	assert_eq!(&format(SanEntry::Name(Cow::Borrowed(b"foo\xc3\xb6bar"))), "foo\\xc3\\xb6bar");
}
