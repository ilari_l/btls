use acmed2_cryptos as cryptos;
use acmed2_formats as formats;
use acmed2_memory as alloc;
use super::__parse_basic_constraints;
use super::__parse_certificate_base;
use super::__parse_extended_key_usage;
use super::__parse_extensions;
use super::__parse_key_usage;
use super::__parse_subject_alternative_names;
use super::_X509SignedObjParseError;
use super::BIT_STRING;
use super::BOOLEAN;
use super::CertificateBaseError;
use super::GENERALIZED_TIME;
use super::INTEGER;
use super::OBJECT_IDENTIFIER;
use super::OCTET_STRING;
use super::PRINTABLE_STRING;
use super::SEQUENCE;
use super::SET;
use super::SanEntry;
use super::X509_CRT_EXTENSIONS;
use super::X509_CRT_VERSION;
use super::X509_DNS_NAME;
use super::X509_IP_ADDRESS;
use super::X509SignedObj;
use super::X509SignedObjParseError;
use crate::signature::VerifyX509Error;
use alloc::Vec;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::ops::Deref;
use cryptos::Drbg;
use cryptos::Ed25519PrivateKey;
use cryptos::Hash;
use cryptos::HashUpdate;
use cryptos::Sha256;
use formats::Rope;
use formats::rope_concat;
use formats::RopeAsn1;
use formats::RopeOid;


///Error from verifying ACME TLS authorization certificate.
#[derive(Copy,Clone,Debug)]
pub struct AcmeAuthError(_AcmeAuthError);
#[derive(Copy,Clone,Debug)]
enum _AcmeAuthError
{
	EmptyChain,
	MultipleCertificates,
	NotX509v3,
	Malformed,
	BadSerial,
	BadNotBefore,
	BadNotAfter,
	WrongSigalg,
	UnrecognizedCritical,
	NameConstraintsPresent,
	AcmeAlpnNotCritical,
	NotSelfSigned,
	WrongNumberOfSans,
	WrongValidationName,
	SanExtensionMissing,
	AlpnValidationExtensionMissing,
	WrongKeyAuthorization,
	UnsupportedSignatureAlgorithm,
	CertificateCanNotSign,
	BadSelfSignature(VerifyX509Error),
}

impl From<X509SignedObjParseError> for AcmeAuthError
{
	fn from(e: X509SignedObjParseError) -> AcmeAuthError
	{
		match e.0 {
			_X509SignedObjParseError::Malformed => AcmeAuthError(_AcmeAuthError::Malformed),
		}
	}
}

impl Display for AcmeAuthError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::_AcmeAuthError::*;
		match self.0 {
			EmptyChain => f.write_str("Empty certificate chain"),
			MultipleCertificates => f.write_str("Multiple certificates in chain"),
			NotX509v3 => f.write_str("Certificate not X.509 v3"),
			Malformed => f.write_str("Certificate malformed"),
			BadSerial => f.write_str("Bad certificate serial number"),
			BadNotBefore => f.write_str("Bad certificate not before"),
			BadNotAfter => f.write_str("Bad certificate not after"),
			WrongSigalg => f.write_str("Signature algorithms do not match"),
			UnrecognizedCritical => f.write_str("Unrecognized critical extension"),
			NameConstraintsPresent => f.write_str("Name constraint present"),
			AcmeAlpnNotCritical => f.write_str("ACME ALPN extension not critical"),
			NotSelfSigned => f.write_str("Certificate not self-signed"),
			WrongNumberOfSans => f.write_str("Wrong number of SANs"),
			WrongValidationName => f.write_str("Validation name does not match"),
			SanExtensionMissing => f.write_str("SAN extension missing"),
			AlpnValidationExtensionMissing => f.write_str("ACME ALPN extension missing"),
			WrongKeyAuthorization => f.write_str("Key authorization wrong"),
			UnsupportedSignatureAlgorithm => f.write_str("Signature algorithm not supported"),
			CertificateCanNotSign => f.write_str("DigitalSignature KU not asserted"),
			BadSelfSignature(err) => write!(f, "Self-signature: {err}"),
		}
	}
}


fn _verify_acme_authorization<'a>(mut certificates: impl Iterator<Item=&'a [u8]>, name: SanEntry, nonce: &str,
	key_hash: &str) -> Result<(&'a [u8], &'a [u8]), _AcmeAuthError>
{
	use self::_AcmeAuthError::*;
	//Hash the key authorization.
	let mut key_authorization = Sha256::new();
	key_authorization.update(nonce.as_bytes());
	key_authorization.update(b".");
	key_authorization.update(key_hash.as_bytes());
	let key_authorization = key_authorization.hash();

	//Grab the EE certificate. It must be the sole certificate.
	let raw = certificates.next().ok_or(EmptyChain)?;
	if certificates.next().is_some() { return Err(MultipleCertificates); }
	//Parse EE certificate.
	let obj = X509SignedObj::new(raw).map_err(|e|AcmeAuthError::from(e).0)?;
	let mut base = __parse_certificate_base(&obj).map_err(|e|match e {
		CertificateBaseError::NotX509v3 => NotX509v3,
		CertificateBaseError::Malformed => Malformed,
		CertificateBaseError::BadSerial => BadSerial,
		CertificateBaseError::BadNotBefore => BadNotBefore,
		CertificateBaseError::BadNotAfter => BadNotAfter,
		CertificateBaseError::WrongSigalg => WrongSigalg,
	})?;
	//Few checks.
	if base.issuer != base.subject { return Err(NotSelfSigned); }
	if base.sigalg != &[6,3,43,101,112] { return Err(UnsupportedSignatureAlgorithm); }
	obj.check_signature((base.key_algo, base.key)).map_err(|e|BadSelfSignature(e))?;

	let mut can_sign = true;
	let mut tls_server = true;
	let mut checks = 0;
	__parse_extensions(&mut base.extensions, Malformed, UnrecognizedCritical, |extid, critical, eval|{
		Ok(if extid == &[85,29,19] {
			__parse_basic_constraints(eval, &mut false, &mut None).ok_or(Malformed)?;
			true
		} else if extid == &[85,29,15] {
			__parse_key_usage(eval, &mut can_sign, &mut false).ok_or(Malformed)?;
			true
		} else if extid == &[85,29,37] {
			__parse_extended_key_usage(eval, &mut false, &mut tls_server).ok_or(Malformed)?;
			true
		} else if extid == &[85,29,30] {
			return Err(NameConstraintsPresent);
		} else if extid == &[85,29,17] {
			let mut altnames = Vec::new();
			__parse_subject_alternative_names(eval, &mut altnames).ok_or(Malformed)?;
			if altnames.len() != 1 { return Err(WrongNumberOfSans); }
			if altnames[0] != name { return Err(WrongValidationName); }
			checks |= 1;
			true
		} else if extid == &[43,6,1,5,5,7,1,31] {
			if !critical { return Err(AcmeAlpnNotCritical); }
			//32 byte OCTET SRING.
			eval.u16().and_then(|v|if v==0x0420 { Some(()) } else { None }).ok_or(Malformed)?;
			if eval.remainder() != key_authorization { return Err(WrongKeyAuthorization); }
			checks |= 2;
			true
		} else {
			false
		})
	})?;
	if !can_sign { return Err(CertificateCanNotSign); }
	if checks & 1 == 0 { return Err(SanExtensionMissing); }
	if checks & 2 == 0 { return Err(AlpnValidationExtensionMissing); }

	Ok((base.key_algo, base.key))
}

///Verify ACME authorization, and return the key on success.
///
/// * `certificates`: A chain of certificates.
/// * `name`: the name to verify the authorization for.
/// * `nonce`: the nonce from the CA.
/// * `key_hash`: JWK thumbprint of ACME account key, computed using SHA-256.
/// * On success: Returns a tuple `(keytype,keydata)`, that can then be passed to `verify_tls_sig`.
/// * On error: returns `Err(err)`, where `err` describes the error.
///
pub fn verify_acme_authorization<'a>(certificates: impl Iterator<Item=&'a [u8]>, name: SanEntry, nonce: &str,
	key_hash: &str) -> Result<(&'a [u8], &'a [u8]), AcmeAuthError>
{
	_verify_acme_authorization(certificates, name, nonce, key_hash).map_err(|e|AcmeAuthError(e))
}


const ED25519_ID: RopeAsn1<RopeAsn1<RopeOid<'static>>> =
	RopeAsn1(SEQUENCE, RopeAsn1(OBJECT_IDENTIFIER, RopeOid(&[1,3,101,112])));

fn __generate_acme_certificate(keypair: &Ed25519PrivateKey, name: impl Rope, nonce: &str, key_hash: &str,
	rng: &mut (impl Drbg+?Sized)) -> Vec<u8>
{
	let mut serial = [0;20];
	//Compute authorization.
	let mut keyauth_hash = Sha256::new();
	keyauth_hash.update(nonce.as_bytes());
	keyauth_hash.update(b".");
	keyauth_hash.update(key_hash.as_bytes());
	let keyauth_hash = keyauth_hash.hash();
	//Sample until valid serial.
	while serial[0] < 1 || serial[0] >= 128 { rng.fill(&mut serial); }
	let signer = RopeAsn1(SET, RopeAsn1(SEQUENCE, rope_concat!(
		RopeAsn1(OBJECT_IDENTIFIER, RopeOid(&[2, 5, 4, 3])),
		RopeAsn1(PRINTABLE_STRING, "ACME TLS-ALPN")
	)));
	let tbs = RopeAsn1(SEQUENCE, rope_concat!{
		RopeAsn1(X509_CRT_VERSION, RopeAsn1(INTEGER, 2u8)),	//Version 3.
		RopeAsn1(INTEGER, &serial[..]),
		ED25519_ID,
		RopeAsn1(SEQUENCE, signer.clone()),
		RopeAsn1(SEQUENCE, rope_concat!(
			RopeAsn1(GENERALIZED_TIME, &b"00010101000000Z"[..]),
			RopeAsn1(GENERALIZED_TIME, &b"99991231235959Z"[..])
		)),
		RopeAsn1(SEQUENCE, signer),
		RopeAsn1(SEQUENCE, rope_concat!(
			ED25519_ID,
			RopeAsn1(BIT_STRING, rope_concat!(0u8, keypair.pubkey()))
		)),
		RopeAsn1(X509_CRT_EXTENSIONS, RopeAsn1(SEQUENCE, rope_concat!(
			RopeAsn1(SEQUENCE, rope_concat!( 
				RopeAsn1(OBJECT_IDENTIFIER, RopeOid(&[1,3,6,1,5,5,7,1,31])),
				RopeAsn1(BOOLEAN, 0xFFu8),	//Critical.
				RopeAsn1(OCTET_STRING, RopeAsn1(OCTET_STRING, keyauth_hash))
			)),
			RopeAsn1(SEQUENCE, rope_concat!(
				RopeAsn1(OBJECT_IDENTIFIER, RopeOid(&[2,5,29,17])),
				RopeAsn1(BOOLEAN, 0xFFu8),				//Critical.
				RopeAsn1(OCTET_STRING, RopeAsn1(SEQUENCE, name))	//One name.
			))
		)))
	});
	let signature = keypair.sign_nonce(rng, |h|tbs.call(&mut |p|h.update(p)));

	RopeAsn1(SEQUENCE, rope_concat!{
		tbs,
		ED25519_ID,
		RopeAsn1(BIT_STRING, rope_concat!(0u8, &signature[..]))
	}).as_vec()
}

///Generate ACME validation certificate.
///
///Parameters:
///
/// * keypair: The Ed25519 keypair to sign the certificate with.
/// * name: The name to validate.
/// * nonce: The nonce given by CA.
/// * key_hash: The thumbprint of the ACME account key.
/// * rng: Random number generator instance to use.
///
///Returns: The raw X.509 validation certificate
pub fn generate_acme_certificate(keypair: &Ed25519PrivateKey, name: SanEntry, nonce: &str, key_hash: &str,
	rng: &mut (impl Drbg+?Sized)) -> Vec<u8>
{
	let name = match &name {
		&SanEntry::Name(ref name) => RopeAsn1(X509_DNS_NAME, name.deref()),
		&SanEntry::Ipv4(ref addr) => RopeAsn1(X509_IP_ADDRESS, &addr[..]),
		&SanEntry::Ipv6(ref addr) => RopeAsn1(X509_IP_ADDRESS, &addr[..]),
	};
	__generate_acme_certificate(keypair, name, nonce, key_hash, rng)
}
