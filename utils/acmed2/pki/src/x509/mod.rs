use acmed2_formats as formats;
use acmed2_memory as alloc;
use alloc::BTreeMap;
use alloc::Cow;
use alloc::String;
use alloc::ToOwned;
use alloc::Vec;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::fmt::Write;
use core::ops::Deref;
use formats::Asn1Tlv;
use formats::GParse;

mod acme;
pub use acme::*;
mod name;
pub use name::*;


///Error from parsing X.509 signed object.
#[derive(Copy,Clone,Debug)]
pub(crate) struct X509SignedObjParseError(_X509SignedObjParseError);
#[derive(Copy,Clone,Debug)]
enum _X509SignedObjParseError
{
	Malformed,
}

impl Display for X509SignedObjParseError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::_X509SignedObjParseError::*;
		match self.0 {
			Malformed => f.write_str("Signed object malformed"),
		}
	}
}


///Error parsing X.509v3 certificate.
#[derive(Copy,Clone,Debug)]
pub struct X509v3ParseError(_X509v3ParseError);
#[derive(Copy,Clone,Debug)]
enum _X509v3ParseError
{
	Malformed,
	WrongSigalg,
	UnrecognizedCritical,
	AcmeCertificate,
	DelegationUsageCritical,
	NotX509v3,
	BadSerial,
	BadNotBefore,
	BadNotAfter,
	CaCanNotSignCerts,
	NonCaCanSignCerts,
}

impl From<X509SignedObjParseError> for X509v3ParseError
{
	fn from(e: X509SignedObjParseError) -> X509v3ParseError
	{
		match e.0 {
			_X509SignedObjParseError::Malformed => X509v3ParseError(_X509v3ParseError::Malformed),
		}
	}
}

impl Display for X509v3ParseError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::_X509v3ParseError::*;
		match self.0 {
			Malformed => f.write_str("Certificate malformed"),
			WrongSigalg => f.write_str("Signature algorithms do not match"),
			UnrecognizedCritical => f.write_str("Unrecognized critical extension"),
			AcmeCertificate => f.write_str("ACME TLS validation certificate"),
			DelegationUsageCritical => f.write_str("Delegation usage extension marked critical"),
			NotX509v3 => f.write_str("Certificate not X.509 v3"),
			BadSerial => f.write_str("Bad certificate serial number"),
			BadNotBefore => f.write_str("Bad certificate not before"),
			BadNotAfter => f.write_str("Bad certificate not after"),
			CaCanNotSignCerts => f.write_str("CA does not have keyCertSign KU"),
			NonCaCanSignCerts => f.write_str("non-CA has keyCertSign KU"),
		}
	}
}

///Error parsing X.509v3 certificate signing request.
#[derive(Copy,Clone,Debug)]
pub struct X509v3CsrParseError(_X509v3CsrParseError);
#[derive(Copy,Clone,Debug)]
enum _X509v3CsrParseError
{
	Malformed,
	NotCsr,
	UnrecognizedCritical,
	NotForEeCertificate,
	NotForSigCertificate,
	UnknownFeatures,
	BadSelfSignature(super::VerifyX509Error),
}

impl From<X509SignedObjParseError> for X509v3CsrParseError
{
	fn from(e: X509SignedObjParseError) -> X509v3CsrParseError
	{
		match e.0 {
			_X509SignedObjParseError::Malformed => X509v3CsrParseError(_X509v3CsrParseError::Malformed),
		}
	}
}

impl Display for X509v3CsrParseError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::_X509v3CsrParseError::*;
		match self.0 {
			Malformed => f.write_str("CSR malformed"),
			NotCsr => f.write_str("Not Certificate Signing Request"),
			UnrecognizedCritical => f.write_str("Unrecognized critical extension"),
			NotForEeCertificate => f.write_str("Request not for EE certificate"),
			NotForSigCertificate => f.write_str("Request not for signature certificate"),
			UnknownFeatures => f.write_str("Request for unknown features"),
			BadSelfSignature(err) => write!(f, "Self-signature: {err}"),
		}
	}
}

///Error in certificate consistency.
#[derive(Clone,Debug)]
pub struct CertificateConsistencyError(_CertificateConsistencyError);
#[derive(Clone,Debug)]
enum _CertificateConsistencyError
{
	KeyMismatch,
	NameNotInCertificate(String),
	MustStapleMismatch,
}

impl Display for CertificateConsistencyError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::_CertificateConsistencyError::*;
		match &self.0 {
			KeyMismatch => f.write_str("Wrong key in certificate"),
			NameNotInCertificate(ref n) => write!(f, "SAN {n} not in certificate"),
			MustStapleMismatch => f.write_str("Wrong must-staple flag in certificate"),
		}
	}
}

const BOOLEAN: (u8, u64) = (0, 1);
const INTEGER: (u8, u64) = (0, 2);
const BIT_STRING: (u8, u64) = (0, 3);
const OCTET_STRING: (u8, u64) = (0, 4);
const OBJECT_IDENTIFIER: (u8, u64) = (0, 6);
const PRINTABLE_STRING: (u8, u64) = (0, 19);
const GENERALIZED_TIME: (u8, u64) = (0, 24);
const SEQUENCE: (u8, u64) = (1, 16);
const SET: (u8, u64) = (1, 17);
const X509_DNS_NAME: (u8, u64) = (4, 2);
const X509_IP_ADDRESS: (u8, u64) = (4, 7);
const X509_CRT_VERSION: (u8, u64) = (5, 0);
const X509_CRT_EXTENSIONS: (u8, u64) = (5, 3);


///Extract subject and key from X.509 certificate.
///
/// * `cert`: The raw X.509 certificate.
/// * On success: Returns `Some((subject, spki))`.
///   * `subject`: the raw subject of the certificate.
///   * `spki`: the raw subjectPublicKeyInfo of the certificate.
/// * On error: Returns `None`.
pub fn certificate_subject_and_key<'a>(cert: &'a [u8]) -> Option<(&'a [u8], &'a [u8])>
{
	let mut top = GParse::new(cert);
	let mut toplevel = top.asn1()?.gparse;
	let mut tbs = toplevel.asn1()?.gparse;
	//Read version or serialNumber.
	let t = tbs.asn1()?;
	if t.tag == X509_CRT_VERSION { tbs.asn1()?; }	//This is serialNumber.
	tbs.asn1()?;					//signature
	tbs.asn1()?;					//issuer
	tbs.asn1()?;					//validity.
	let subject = tbs.asn1_typed(SEQUENCE)?;	//subject should be SEQUENCE.
	let key = tbs.asn1_typed(SEQUENCE)?;		//key should be SEQUENCE.
	Some((subject.entiere, key.entiere))
}


fn _parse_x509_key<'a>(spki: &mut GParse<'a>) -> Option<(&'a [u8], &'a [u8])>
{
	let keyalg = spki.asn1_typed(SEQUENCE)?.value;
	let key = spki.asn1_typed(BIT_STRING)?.value;
	if key.len() == 0 || key[0] > 7 { return None; }
	Some((keyalg, &key[1..]))
}

///X.509v3 certificate.
#[derive(Clone,Debug)]
pub struct X509v3Certificate<'a>
{
	///Serial number.
	pub serial_number: Cow<'a, [u8]>,
	///Issuer.
	pub issuer: Cow<'a, [u8]>,
	///Subject.
	pub subject: Cow<'a, [u8]>,
	///Public key.
	pub key: (Cow<'a, [u8]>, Cow<'a, [u8]>),
	///Not before, as unix timestamp.
	pub not_before: i64,	//Unix timestamp.
	///Not after, as unix timestamp.
	pub not_after: i64,	//Unix timestamp.
	///Delegation usage allowed.
	pub delegation_usage: bool,
	///AIA OCSP URL, if present.
	pub aia_ocsp_url: Option<Cow<'a, [u8]>>,
	///AIA issuer URL, if present.
	pub aia_issuer_url: Option<Cow<'a, [u8]>>,
	///Embedded Signed Certificate Timestamps.
	pub scts: Vec<Cow<'a, [u8]>>,
	///Must staple OCSP.
	pub must_staple: bool,
	///Unknown features present.
	pub unknown_feature: bool,
	///Usable for TLS client authentication.
	pub tls_client: bool,
	///Usable for TLS server authentication.
	pub tls_server: bool,
	///Is CA certificate.
	pub is_ca: bool,
	///Can sign data.
	pub can_sign: bool,
	///Maximum CA depth.
	pub maxdepth: Option<u64>,
	///Subject alternative names.
	pub altnames: Vec<SanEntry<'a>>,
	///Name constraints allowed.
	pub const_allow: Vec<ConstraintEntry<'a>>, 
	///Name constraints forbidden.
	pub const_forbid: Vec<ConstraintEntry<'a>>,
	///Raw SubjectPublicKeyInfo field.
	pub rawspki: Cow<'a, [u8]>,
	///Raw TBS.
	pub tbs: Cow<'a, [u8]>,
	///Raw signature.
	pub signature: Cow<'a, [u8]>,
}

impl<'a> X509v3Certificate<'a>
{
	pub fn as_static(&self) -> X509v3Certificate<'static>
	{
		X509v3Certificate {
			serial_number: __own_cow(&self.serial_number),
			issuer: __own_cow(&self.issuer),
			subject: __own_cow(&self.subject),
			key: (__own_cow(&self.key.0), __own_cow(&self.key.1)),
			not_before: self.not_before,
			not_after: self.not_after,
			delegation_usage: self.delegation_usage,
			aia_ocsp_url: self.aia_ocsp_url.as_ref().map(|x|__own_cow(x)),
			aia_issuer_url: self.aia_issuer_url.as_ref().map(|x|__own_cow(x)),
			scts: self.scts.iter().map(|x|__own_cow(x)).collect(),
			must_staple: self.must_staple,
			unknown_feature: self.unknown_feature,
			tls_client: self.tls_client,
			tls_server: self.tls_server,
			is_ca: self.is_ca,
			can_sign: self.can_sign,
			maxdepth: self.maxdepth,
			altnames: self.altnames.iter().map(|x|x.as_static()).collect(),
			const_allow: self.const_allow.iter().map(|x|x.as_static()).collect(),
			const_forbid: self.const_forbid.iter().map(|x|x.as_static()).collect(),
			rawspki: __own_cow(&self.rawspki),
			tbs: __own_cow(&self.tbs),
			signature: __own_cow(&self.signature),
		}
	}
}

///X.509v3 certificate signing request.
#[derive(Clone,Debug)]
pub struct X509v3CSR<'a>
{
	///Subject.
	subject: Cow<'a, [u8]>,
	///Raw SubjectPublicKeyInfo.
	spki: Cow<'a, [u8]>,
	///The key algorithm.
	key_algo: Cow<'a, [u8]>,
	///The raw key.
	key: Cow<'a, [u8]>,
	///Requested SubjectAlternativeName.
	san: Vec<SanEntry<'a>>,
	///Must-staple?
	must_staple: bool,
	///The data to be signed.
	tbs: Cow<'a, [u8]>,
	///Signature algorithm.
	sigalg: Cow<'a, [u8]>,
	///Raw signature.
	signature: Cow<'a, [u8]>,
}

impl<'a> X509v3CSR<'a>
{
	pub fn as_static(&self) -> X509v3CSR<'static>
	{
		X509v3CSR {
			subject: __own_cow(&self.subject),
			spki: __own_cow(&self.spki),
			key_algo: __own_cow(&self.key_algo),
			key: __own_cow(&self.key),
			san: self.san.iter().map(|x|x.as_static()).collect(),
			must_staple: self.must_staple,
			tbs: __own_cow(&self.tbs),
			sigalg: __own_cow(&self.sigalg),
			signature: __own_cow(&self.signature),
		}
	}
}


///Signed X.509 object.
#[derive(Clone,Debug)]
pub(crate) struct X509SignedObj<'a>
{
	contents: GParse<'a>,
	sigalg: &'a [u8],
	tbs: &'a [u8],
	signature: &'a [u8],
}

impl<'a> X509SignedObj<'a>
{
	pub(crate) fn new(obj: &'a [u8]) -> Result<X509SignedObj<'a>, X509SignedObjParseError>
	{
		Self::_new(obj).map_err(|e|X509SignedObjParseError(e))
	}
	fn _new(obj: &'a [u8]) -> Result<X509SignedObj<'a>, _X509SignedObjParseError>
	{
		use self::_X509SignedObjParseError::Malformed as MF;
		let mut top = GParse::new(obj);
		let mut toplevel = top.asn1_typed(SEQUENCE).ok_or(MF)?.gparse;
		if top.more() { return Err(MF); }
		let tbs = toplevel.asn1_typed(SEQUENCE).ok_or(MF)?;
		let sigalg = toplevel.asn1_typed(SEQUENCE).ok_or(MF)?;
		let signature = toplevel.asn1_typed(BIT_STRING).ok_or(MF)?;
		if signature.value.len() == 0 || signature.value[0] > 7 || toplevel.more() { return Err(MF); }
		Ok(X509SignedObj {
			contents: tbs.gparse,
			sigalg: sigalg.value,
			tbs: tbs.entiere,
			signature: &signature.value[1..],	//Strip bit indicator.
		})
	}
	pub(crate) fn get_contents(&self) -> GParse<'a> { self.contents.clone() }
	pub(crate) fn check_sigalg(&self, sigalg: &[u8]) -> Result<(), ()>
	{
		if self.sigalg == sigalg { Ok(()) } else { Err(()) }
	}
	pub(crate) fn check_signature(&self, key: (&'a [u8], &'a [u8])) -> Result<(), super::VerifyX509Error>
	{
		super::verify_x509_sig(key, self.tbs, (self.sigalg, self.signature))
	}
	pub(crate) fn parse_certificate(&self) -> Result<X509v3Certificate<'a>, X509v3ParseError>
	{
		self._parse_certificate().map_err(|e|X509v3ParseError(e))
	}
	fn _parse_certificate(&self) -> Result<X509v3Certificate<'a>, _X509v3ParseError>
	{
		let mut delegation_usage = false;
		let mut aia_issuer_url = None;
		let mut aia_ocsp_url = None;
		let mut scts = Vec::new();
		let mut const_allow = Vec::new();
		let mut const_forbid = Vec::new();
		let mut altnames = Vec::new();
		let mut must_staple = false;
		let mut unknown_feature = false;
		let mut tls_client = true;
		let mut tls_server = true;
		let mut is_ca = false;
		let mut ku_present = false;
		let mut can_sign = true;
		let mut can_sign_cert = true;
		let mut maxdepth = None;

		use self::_X509v3ParseError::Malformed as MF;
		use self::_X509v3ParseError::*;

		let mut base = __parse_certificate_base(self).map_err(|e|match e {
			CertificateBaseError::NotX509v3 => NotX509v3,
			CertificateBaseError::Malformed => Malformed,
			CertificateBaseError::BadSerial => BadSerial,
			CertificateBaseError::BadNotBefore => BadNotBefore,
			CertificateBaseError::BadNotAfter => BadNotAfter,
			CertificateBaseError::WrongSigalg => WrongSigalg,
		})?;
		//Few checks.
		self.check_sigalg(base.sigalg).map_err(|_|WrongSigalg)?;

		__parse_extensions(&mut base.extensions, Malformed, UnrecognizedCritical, |extid, crit, eval|{
			Ok(if extid == &[85,29,19] {
				__parse_basic_constraints(eval, &mut is_ca, &mut maxdepth).ok_or(MF)?;
				true
			} else if extid == &[85,29,15] {
				ku_present = true;
				__parse_key_usage(eval, &mut can_sign, &mut can_sign_cert).ok_or(MF)?;
				true
			} else if extid == &[43,6,1,5,5,7,1,1] {
				__parse_authority_information_access(eval, &mut aia_ocsp_url,
					&mut aia_issuer_url).ok_or(MF)?;
				true
			} else if extid == &[85,29,37] {
				__parse_extended_key_usage(eval, &mut tls_client, &mut tls_server).ok_or(MF)?;
				true
			} else if extid == &[85,29,30] {
				__parse_name_constraints(eval, &mut const_allow, &mut const_forbid).ok_or(MF)?;
				true
			} else if extid == &[43,6,1,4,1,214,121,2,4,2] {
				__parse_signed_certificate_timestamp(eval, &mut scts).ok_or(MF)?;
				true
			} else if extid == &[43,6,1,4,1,130,218,75,44] {
				if crit { return Err(DelegationUsageCritical); }
				__parse_delegation_usage(eval, &mut delegation_usage).ok_or(MF)?;
				true
			} else if extid == &[85,29,17] {
				__parse_subject_alternative_names(eval, &mut altnames).ok_or(MF)?;
				true
			} else if extid == &[43,6,1,5,5,7,1,24] {
				__parse_tls_feature(eval, &mut must_staple, &mut unknown_feature).ok_or(MF)?;
				true
			} else if extid == &[43,6,1,5,5,7,1,31] {
				return Err(AcmeCertificate)		//ACME ALPN.
			} else if extid == &[85,29,14] {
				eval.remainder();
				true		//Ignore subjectKeyIdentifier.
			} else if extid == &[85,29,35] {
				eval.remainder();
				true		//Ignore authorityKeyIdefntifier.
			} else if extid == &[43,6,1,5,5,7,1,3] || extid == &[85,29,54] ||
				extid == &[85,29,36] || extid == &[85,29,33] {
				eval.remainder();
				true		//Ignore policy crap.
			} else {
				false
			})
		})?;
		if is_ca && !can_sign_cert { return Err(CaCanNotSignCerts); }
		if !is_ca && can_sign_cert && ku_present { return Err(NonCaCanSignCerts); }
		if is_ca { can_sign = false; }

		Ok(X509v3Certificate {
			serial_number: Cow::Borrowed(base.serial),
			key: (Cow::Borrowed(base.key_algo), Cow::Borrowed(base.key)),
			issuer: Cow::Borrowed(base.issuer),
			subject: Cow::Borrowed(base.subject), 
			not_before: base.not_before,
			not_after: base.not_after,
			rawspki: Cow::Borrowed(base.spki),
			tbs: Cow::Borrowed(self.tbs),
			signature: Cow::Borrowed(self.signature),
			aia_issuer_url: aia_issuer_url.map(|x|Cow::Borrowed(x)),
			aia_ocsp_url: aia_ocsp_url.map(|x|Cow::Borrowed(x)),

			delegation_usage, scts, must_staple, unknown_feature,
			tls_client, tls_server, is_ca, can_sign, maxdepth, altnames, const_allow, const_forbid,
		})
	}
	pub(crate) fn parse_csr(&self) -> Result<X509v3CSR<'a>, X509v3CsrParseError>
	{
		self._parse_csr().map_err(|e|X509v3CsrParseError(e))
	}
	fn _parse_csr(&self) -> Result<X509v3CSR<'a>, _X509v3CsrParseError>
	{
		use self::_X509v3CsrParseError::*;
		let mut g = self.get_contents();
		//Integer 0.
		if g.asn1_typed(INTEGER).ok_or(Malformed)?.value != &[0] { return Err(NotCsr); }
		//Subject.
		let subject = g.asn1_typed(SEQUENCE).ok_or(Malformed)?.entiere;
		//SubjectPublicKeyInfo.
		let spki = g.clone().asn1_typed(SEQUENCE).ok_or(Malformed)?.entiere;	//Also extract raw SPKI.
		let (key_algo, key) = g.asn1_fn(SEQUENCE, |spki|{
			let keyalg = spki.asn1_typed(SEQUENCE).ok_or(Malformed)?.value;
			let key = spki.asn1_typed(BIT_STRING).ok_or(Malformed)?.value;
			if key.len() == 0 || key[0] > 7 { return Err(Malformed); }
			Ok((keyalg, &key[1..]))
		}).unwrap_or(Err(Malformed))?;
		//Extensions.
		let mut san = Vec::new();
		let mut must_staple = false;
		g.asn1_fn_list((5, 0), |extensions|{
			extensions.asn1_fn(SEQUENCE, |extension|{
				let extid = extension.asn1_typed(OBJECT_IDENTIFIER).ok_or(Malformed)?.value;
				if extid == &[42,134,72,134,247,13,1,9,14] {
					//Extension request. The payload of this thing is a SET, which
					//has single SEQUENCE as child.
					extension.asn1_fn((1, 17), |extlist|{
						__parse_extension_request_set(extlist, &mut san, &mut must_staple)
					}).unwrap_or(Err(Malformed))?;
				} else {
					//Unknown, Ignore.
					extension.remainder();
				}
				Ok(())
			}).unwrap_or(Err(Malformed))
		}).unwrap_or(Err(Malformed))?;
		if g.more() { return Err(Malformed); }
		//Self-verify.
		self.check_signature((key_algo, key)).map_err(|e|BadSelfSignature(e))?;

		Ok(X509v3CSR {
			tbs: Cow::Borrowed(self.tbs),
			sigalg: Cow::Borrowed(self.sigalg),
			signature: Cow::Borrowed(self.signature),
			key_algo: Cow::Borrowed(key_algo),
			key: Cow::Borrowed(key),
			spki: Cow::Borrowed(spki),
			subject: Cow::Borrowed(subject),

			san, must_staple,
		})
	}
}

fn __parse_extension_request_set<'a>(exts: &mut GParse<'a>, san: &mut Vec<SanEntry<'a>>, must_staple: &mut bool) ->
	Result<(), _X509v3CsrParseError>
{
	use self::_X509v3CsrParseError::*;
	__parse_extensions(exts, Malformed, UnrecognizedCritical, |extid, _, eval|{
		Ok(if extid == &[85,29,19] {
			//Basic constraints. This extension must be empty sequence.
			if eval.remainder() != &[30, 0] { return Err(NotForEeCertificate); }
			true
		} else if extid == &[85,29,15] {
			let mut can_sign = false;
			let mut can_sign_cert = false;
			__parse_key_usage(eval, &mut can_sign, &mut can_sign_cert).ok_or(Malformed)?;
			if !can_sign { return Err(NotForSigCertificate); }
			if can_sign_cert { return Err(NotForEeCertificate); }
			true
		} else if extid == &[85,29,37] {
			//Only check this is well-formed.
			__parse_extended_key_usage(eval, &mut false, &mut false).ok_or(Malformed)?;
			true
		} else if extid == &[85,29,17] {
			__parse_subject_alternative_names(eval, san).ok_or(Malformed)?;
			true
		} else if extid == &[43,6,1,5,5,7,1,24] {
			let mut unknown_feature = false;
			__parse_tls_feature(eval, must_staple, &mut unknown_feature).ok_or(Malformed)?;
			if unknown_feature { return Err(UnknownFeatures); }
			true
		} else {
			false		//Unknown extension
		})
	})?;
	Ok(())
}

fn __parse_basic_constraints(extvalue: &mut GParse, is_ca: &mut bool, maxdepth: &mut Option<u64>) ->  Option<()>
{
	extvalue.asn1_fn(SEQUENCE, |list|{
		if list.more() {
			let flag = list.asn1_typed(BOOLEAN)?.value;
			if flag != &[0xFF] { return None; }
			*is_ca = true;
		}
		if list.more() {
			let depth = list.asn1_typed(INTEGER)?.value;
			let mut t = 0u64;
			for b in depth.iter() {
				t=t.saturating_mul(256).saturating_add(*b as u64);
			}
			*maxdepth = Some(t);
		}
		Some(())
	}).unwrap_or(None)
}

fn __parse_key_usage(extvalue: &mut GParse, can_sign: &mut bool, can_sign_cert: &mut bool) ->  Option<()>
{
	*can_sign = false;
	*can_sign_cert = false;
	extvalue.asn1_fn(BIT_STRING, |usage|{
		let usage = usage.remainder();
		if usage.len() < 2 || usage[0] > 7 { return None; }
		*can_sign = usage[1] & 128 != 0;
		*can_sign_cert = usage[1] & 4 != 0;
		Some(())
	}).unwrap_or(None)
}

fn __parse_authority_information_access<'a>(extvalue: &mut GParse<'a>, aia_ocsp_url: &mut Option<&'a [u8]>,
	aia_issuer_url: &mut Option<&'a [u8]>) ->  Option<()>
{
	extvalue.asn1_fn_list(SEQUENCE, |list|{
		let mut entry = list.asn1_typed(SEQUENCE)?.gparse;
		let kind = entry.asn1_typed(OBJECT_IDENTIFIER)?.value;
		if kind == &[43,6,1,5,5,7,48,1] {
			*aia_ocsp_url = Some(entry.asn1_typed((4,6))?.value);
		} else if kind == &[43,6,1,5,5,7,48,2] {
			*aia_issuer_url = Some(entry.asn1_typed((4,6))?.value);
		}
		Some(())
	}).unwrap_or(None)
}

fn __parse_extended_key_usage(extvalue: &mut GParse, tls_client: &mut bool, tls_server: &mut bool) ->  Option<()>
{
	*tls_client = false;	//Reset defaults.
	*tls_server = false;
	extvalue.asn1_fn_list(SEQUENCE, |list|{
		let purpose = list.asn1_typed(OBJECT_IDENTIFIER)?.value;
		if purpose == &[43,6,1,5,5,7,3,1] {
			*tls_server = true;
		} else if purpose == &[43,6,1,5,5,7,3,2] {
			*tls_client = true;
		} else if purpose == &[85,29,37,0] {
			*tls_server = true;
			*tls_client = true;
		}
		Some(())
	}).unwrap_or(None)
}

fn __parse_name_constraint_one<'a>(entry: &mut GParse<'a>, list: &mut Vec<ConstraintEntry<'a>>) ->  Option<()>
{
	let mut entry = entry.asn1_typed(SEQUENCE)?.gparse;
	let ientry = entry.asn1()?;
	if ientry.tag == X509_DNS_NAME {
		//Name.
		list.push(ConstraintEntry::Name(Cow::Borrowed(ientry.value)));
	} else if ientry.tag == X509_IP_ADDRESS {
		//Ip address.
		let ip = ientry.value;
		if ip.len() == 32 {
			let mut x = [0;16];
			x.copy_from_slice(&ip[..16]);
			let mask = __calculate_mask(&ip[16..])?;
			list.push(ConstraintEntry::Ipv6(x, mask));
		} else if ip.len() == 8 {
			let mut x = [0;4];
			x.copy_from_slice(&ip[..4]);
			let mask = __calculate_mask(&ip[4..])?;
			list.push(ConstraintEntry::Ipv4(x, mask));
		}
	}
	if entry.more() { return None; }
	Some(())
}

fn __calculate_mask(addr: &[u8]) -> Option<u8>
{
	let mut amt: u8 = 0;
	//Strip all 0xFF in beginning and 0x00 at end, and add 8 for ones at beginning.
	let mut s = 0;
	let mut e = addr.len();
	while s < addr.len() && addr[s] == 0xFF { amt = amt.saturating_add(8); s += 1; }
	while e > 0 && addr[e-1] == 0 { e -= 1; }
	let addr = &addr[s..e];
	//Handle the remaining byte if any.
	match addr.len() {
		 0 => Some(amt),	//No intermediate term.
		 1 => match addr[0] {
			0x80 => Some(amt.saturating_add(1)),
			0xC0 => Some(amt.saturating_add(2)),
			0xE0 => Some(amt.saturating_add(3)),
			0xF0 => Some(amt.saturating_add(4)),
			0xF8 => Some(amt.saturating_add(5)),
			0xFC => Some(amt.saturating_add(6)),
			0xFE => Some(amt.saturating_add(7)),
			_ => None,		//No other pattern that can appear here is legal.
		 },
		 _ => None,		//Multiple bytes is definitely illegal.
	}
}

fn __parse_name_constraints<'a>(extvalue: &mut GParse<'a>, const_allow: &mut Vec<ConstraintEntry<'a>>,
	const_forbid: &mut Vec<ConstraintEntry<'a>>) ->  Option<()>
{
	extvalue.asn1_fn_list(SEQUENCE, |list|{
		let mut entry = list.asn1()?;
		if entry.tag == (5,0) {
			while entry.gparse.more() {
				__parse_name_constraint_one(&mut entry.gparse, const_allow)?;
			}
		} else if entry.tag == (5,1) {
			while entry.gparse.more() {
				__parse_name_constraint_one(&mut entry.gparse, const_forbid)?;
			}
		} else {
			return None;
		}
		Some(())
	}).unwrap_or(None)
}

fn __parse_signed_certificate_timestamp<'a>(extvalue: &mut GParse<'a>, scts: &mut Vec<Cow<'a, [u8]>>) -> Option<()>
{
	extvalue.asn1_fn(OCTET_STRING, |list2|{
		let mut list = list2.gparse_u16()?;
		while list.more() { scts.push(Cow::Borrowed(list.slice_u16()?)); }
		Some(())
	}).unwrap_or(None)
}

fn __parse_delegation_usage(extvalue: &mut GParse, delegation_usage: &mut bool) -> Option<()>
{
	extvalue.asn1_fn((0, 5), |_|{
		//This only goes through with empty NULL.
		*delegation_usage = true;
		Some(())
	}).unwrap_or(None)
}

fn __parse_san_one<'a>(entry: &mut GParse<'a>, list: &mut Vec<SanEntry<'a>>) ->  Option<()>
{
	let ientry = entry.asn1()?;
	if ientry.tag == X509_DNS_NAME {
		//Name.
		list.push(SanEntry::Name(Cow::Borrowed(ientry.value)));
	} else if ientry.tag == X509_IP_ADDRESS {
		//Ip address.
		let ip = ientry.value;
		if ip.len() == 16 {
			let mut x = [0;16];
			x.copy_from_slice(ip);
			list.push(SanEntry::Ipv6(x));
		} else if ip.len() == 4 {
			let mut x = [0;4];
			x.copy_from_slice(ip);
			list.push(SanEntry::Ipv4(x));
		}
	}
	Some(())
}

fn __parse_subject_alternative_names<'a>(extvalue: &mut GParse<'a>, sans: &mut Vec<SanEntry<'a>>) -> Option<()>
{
	extvalue.asn1_fn_list(SEQUENCE, |list|__parse_san_one(list, sans)).unwrap_or(None)
}

fn __parse_tls_feature(extvalue: &mut GParse, must_staple: &mut bool, unknown_feature: &mut bool) -> Option<()>
{
	extvalue.asn1_fn_list(SEQUENCE, |list|{
		let feature = list.asn1_typed(INTEGER)?.value;
		if feature == &[5] {
			*must_staple = true;
		} else {
			*unknown_feature = true;
		}
		Some(())
	}).unwrap_or(None)
}

fn parse_x509_timestamp(ts: Asn1Tlv) -> Option<i64>
{
	match ts.tag {
		(0, 24) => {
			//GeneralizedTime
			if ts.value.len() != 15 || ts.value[14] != b'Z' { return None; }
			let mut t = [0;14];
			t.copy_from_slice(&ts.value[..14]);
			text_to_timestamp(&t)
		},
		(0, 23) => {
			//UTCTime
			if ts.value.len() != 13 || ts.value[12] != b'Z' { return None; }
			let mut t = [0;14];
			(&mut t[2..]).copy_from_slice(&ts.value[..12]);
			if ts.value[0] < b'5' { t[0] = b'2'; t[1] = b'0' } else { t[0] = b'1'; t[1] = b'9' };
			text_to_timestamp(&t)
		},
		_ => None	//???
	}
}

fn decimal_digit(x: u8) -> Option<u32>
{
	match x { 48..=57 => Some(x as u32 - 48), _ => None }
}

fn text_to_timestamp(val: &[u8;14]) -> Option<i64>
{
	let mut year = 0;
	let mut month = 0;
	let mut day = 0;
	let mut hour = 0;
	let mut minute = 0;
	let mut second = 0;
	year += 1000 * decimal_digit(val[0])?;
	year += 100 * decimal_digit(val[1])?;
	year += 10 * decimal_digit(val[2])?;
	year += 1 * decimal_digit(val[3])?;
	month += 10 * decimal_digit(val[4])?;
	month += 1 * decimal_digit(val[5])?;
	day += 10 * decimal_digit(val[6])?;
	day += 1 * decimal_digit(val[7])?;
	hour += 10 * decimal_digit(val[8])?;
	hour += 1 * decimal_digit(val[9])?;
	minute += 10 * decimal_digit(val[10])?;
	minute += 1 * decimal_digit(val[11])?;
	second += 10 * decimal_digit(val[12])?;
	second += 1 * decimal_digit(val[13])?;
	//Basic range check.
	if year == 0 || month == 0 || month > 12 || day == 0 || day > 31 { return None; }
	if hour > 23 || minute > 59 || second > 60 { return None; }
	if second == 60 && (hour != 23 || minute != 59) { return None; }
	let dayts = 3600 * hour + 60 * minute + second;
	//Timestamp for midnight December 31st on year -2.
	let mut ts: i64 = -62167305600;
	//Adjust ts for 400-year supercycles.
	ts += year as i64 / 400 * 12622780800;
	year %= 400;
	let is_leap = year % 4 == 0 && (year % 400 == 0 || year % 100 != 0);
	if !is_leap && month == 2 && day == 29 { return None; }		//No February 29th on non-leap years.
	//Pretend there is a leap day every four years.
	if year > 100 || (year == 100 && month > 2) { ts -= 86400; }
	if year > 200 || (year == 200 && month > 2) { ts -= 86400; }
	if year > 300 || (year == 300 && month > 2) { ts -= 86400; }
	//Adjust ts for 4-year cycles.
	ts += year as i64 / 4 * 126230400;
	year %= 4;
	//Pretend there is a leap day every year.
	if year > 1 || (year == 1 && month > 2) { ts -= 86400; }
	if year > 2 || (year == 2 && month > 2) { ts -= 86400; }
	if year > 3 || (year == 3 && month > 2) { ts -= 86400; }
	//Adjust ts for 1-year cycles.
	ts += year as i64 * 31622400;
	//Now ts is the timestamp for midnight December 31st on the previous year, as-if year was leap year.
	ts += (match month {
		1 if day <= 31 => 0,
		2 if day <= 29 => 31,
		3 if day <= 31 => 60,
		4 if day <= 30 => 91,
		5 if day <= 31 => 121,
		6 if day <= 30 => 152,
		7 if day <= 31 => 182,
		8 if day <= 31 => 213,
		9 if day <= 30 => 244,
		10 if day <= 31 => 274,
		11 if day <= 30 => 305,
		12 if day <= 31 => 335,
		_ => return None
	} + day) as i64 * 86400;
	Some(ts + dayts as i64)
}


#[derive(Debug)]
struct CertificateBaseSection<'a>
{
	serial: &'a [u8],
	sigalg: &'a [u8],
	issuer: &'a [u8],
	subject: &'a [u8],
	key_algo: &'a [u8],
	key: &'a [u8],
	spki: &'a [u8],
	extensions: GParse<'a>,
	not_before: i64,
	not_after: i64,
}

enum CertificateBaseError
{
	NotX509v3,
	Malformed,
	BadSerial,
	BadNotBefore,
	BadNotAfter,
	WrongSigalg,
}

fn __parse_certificate_base<'a>(obj: &X509SignedObj<'a>) -> Result<CertificateBaseSection<'a>, CertificateBaseError>
{
	use self::CertificateBaseError::*;

	let mut g = obj.get_contents();
	//Check certificate is V3.
	if g.if_asn(X509_CRT_VERSION).map(|v|v.as_slice() != &[2,1,2]).unwrap_or(true) {
		return Err(NotX509v3);
	}
	//Serial.
	let serial = g.asn1_typed(INTEGER).ok_or(Malformed)?.value;
	if serial.len() == 0 || serial.len() > 20 || serial[0] >= 128 || serial == &[0] { return Err(BadSerial); }
	//Signature algorithm.
	let sigalg = g.asn1_typed(SEQUENCE).ok_or(Malformed)?.value;
	obj.check_sigalg(sigalg).map_err(|_|WrongSigalg)?;
	//Issuer.
	let issuer = g.asn1_typed(SEQUENCE).ok_or(Malformed)?.entiere;
	//Validity.
	let (not_before, not_after) = g.asn1_fn(SEQUENCE, |validity|{
		let not_before = validity.asn1().ok_or(Malformed)?;
		let not_after = validity.asn1().ok_or(Malformed)?;
		let not_before = parse_x509_timestamp(not_before).ok_or(BadNotBefore)?;
		let not_after = parse_x509_timestamp(not_after).ok_or(BadNotAfter)?;
		Ok((not_before, not_after))
	}).unwrap_or(Err(Malformed))?;
	//Subject.
	let subject = g.asn1_typed(SEQUENCE).ok_or(Malformed)?.entiere;
	//SPKI.
	let spki = g.clone().asn1_typed(SEQUENCE).ok_or(Malformed)?.entiere;	//Also extract raw SPKI.
	let (key_algo, key) = g.asn1_fn(SEQUENCE, |spki|{
		let keyalg = spki.asn1_typed(SEQUENCE).ok_or(Malformed)?.value;
		let key = spki.asn1_typed(BIT_STRING).ok_or(Malformed)?.value;
		if key.len() == 0 || key[0] > 7 { return Err(Malformed); }
		Ok((keyalg, &key[1..]))
	}).unwrap_or(Err(Malformed))?;
	//There may be some junk before extensions (5,3).
	let mut extensions = g.asn1().ok_or(Malformed)?;
	while extensions.tag != X509_CRT_EXTENSIONS { extensions = g.asn1().ok_or(Malformed)?; }
	let extensions = extensions.gparse;
	if g.more() { return Err(Malformed); }

	//Ok.
	Ok(CertificateBaseSection {
		serial, sigalg, issuer, subject, key_algo, key, spki, extensions, not_before, not_after,
	})
}

fn __parse_extensions<'a,E:Copy>(g: &mut GParse<'a>, malformed: E, unrecog_critical: E,
	mut ext_fn: impl FnMut(&[u8], bool, &mut GParse<'a>) -> Result<bool, E>) -> Result<(), E>
{
	g.asn1_fn_list(SEQUENCE, |extension|{
		extension.asn1_fn(SEQUENCE, |extension|{
			let extid = extension.asn1_typed(OBJECT_IDENTIFIER).ok_or(malformed)?.value;
			let critical = if let Some(critical) = extension.if_asn(BOOLEAN) {
				if critical.as_slice() != &[0xFF] { return Err(malformed); }
				true
			} else {
				false
			};
			extension.asn1_fn(OCTET_STRING, |extvalue|{
				if ext_fn(extid, critical, extvalue)? {
					//Already handled.
				} else if critical {
					return Err(unrecog_critical);
				} else {
					extvalue.remainder();	//Ignore unknown extension.
				}
				Ok(())
			}).unwrap_or(Err(malformed))
		}).unwrap_or(Err(malformed))
	}).ok_or(malformed)??;
	Ok(())
}

///Parse a X.509 v3 certificate.
///
/// * `cert`: The raw certificate in DER format to parse.
/// * On success: Returns a parse of the certificate contents.
/// * On error: returns `Err(err)`, where `err` describes the error.
pub fn parse_x509v3_certificate<'a>(cert: &'a [u8]) -> Result<X509v3Certificate<'a>, X509v3ParseError>
{
	let sobj = X509SignedObj::new(cert)?;
	Ok(sobj.parse_certificate()?)
}

///Parse a X.509 v3 certificate signing request.
///
/// * `csr`: The raw CSR in DER format to parse.
/// * On success: Returns a parse of the CSR contents.
/// * On error: returns `Err(err)`, where `err` describes the error.
pub fn parse_x509v3_csr<'a>(csr: &'a [u8]) -> Result<X509v3CSR<'a>, X509v3CsrParseError>
{
	let sobj = X509SignedObj::new(csr)?;
	Ok(sobj.parse_csr()?)
}

///Error from X509 chain verification.
#[derive(Copy,Clone,Debug)]
pub struct VerifyX509ChainError(_VerifyX509ChainError);
#[derive(Copy,Clone,Debug)]
enum _VerifyX509ChainError
{
	///Empty certificate chain.
	EmptyChain,
	///X509v3 certificate parse error.
	CertParseError(usize, X509v3ParseError),
	///Certificate issuers do not chain properly.
	BadIssuerChaining(usize),
	///Certificate has bad signature.
	BadSignature(usize, crate::signature::VerifyX509Error),
	///EE certificate is CA.
	EeIsCa,
	///EE certificate as CA.
	NotCa,
	///Certificate issued by unknown CA.
	IssuedByUnknownCa,
	///Not valid for reference name.
	NotValidForName(usize),
	///Name constraint violation.
	NameConstraintViolation,
	///Certificate not yet valid.
	NotYetValid(i64,i64),
	///Certificate expired.
	CertificateExpired(i64,i64),
	///Issuer certificate not yet valid.
	IssuerNotYetValid(i64,i64),
	///Issuer certificate expired.
	IssuerCertificateExpired(i64,i64),
	///Unknown features present.
	UnknownFeatures,
	///Unknown features present in issuer certificate.
	IssuerUnknownFeatures,
}

impl VerifyX509ChainError
{
	///Determine if error is certificate error, as opposed to chain error.
	pub fn is_cert_error(&self) -> bool
	{
		use self::_VerifyX509ChainError::*;
		match self.0 {
			//The following errors are certificate errors:
			// - Parse error in the first certificate.
			CertParseError(0, _) => true,
			// - EE certificate is CA.
			EeIsCa => true,
			// - Not valid for name.
			NotValidForName(_) => true,
			// - Not yet valid / expired.
			NotYetValid(_,_) => true,
			CertificateExpired(_,_) => true,
			// - Unknown features.
			UnknownFeatures => true,

			//All others are chain errors.
			EmptyChain => false,
			CertParseError(_, _) => false,
			BadIssuerChaining(_) => false,
			BadSignature(_, _) => false,
			NotCa => false,
			IssuedByUnknownCa => false,
			NameConstraintViolation => false,
			IssuerNotYetValid(_,_) => false,
			IssuerCertificateExpired(_,_) => false,
			IssuerUnknownFeatures => false,
		}
	}
}

impl Display for VerifyX509ChainError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::_VerifyX509ChainError::*;
		match self.0 {
			EmptyChain => f.write_str("Certificate chain empty"),
			CertParseError(ord, err) => write!(f, "Certificate #{ord} parse error: {err}"),
			BadIssuerChaining(ord) => write!(f, "Certificate #{ord} does not chain to next"),
			BadSignature(ord, err) => write!(f, "Certificate #{ord} signature error: {err}"),
			EeIsCa => f.write_str("EE certificate is marked as CA"),
			NotCa => f.write_str("Certificate issued by non-CA"),
			IssuedByUnknownCa => f.write_str("Certificate issued by unknown CA"),
			NotValidForName(_) => f.write_str("Certificate not valid for name"),
			NameConstraintViolation => f.write_str("Certificate name constraints violated"),
			NotYetValid(_,_) => f.write_str("Certificate not yet valid"),
			CertificateExpired(_,_) => f.write_str("Certificate expired"),
			IssuerNotYetValid(_,_) => f.write_str("Issuer certificate not yet valid"),
			IssuerCertificateExpired(_,_) => f.write_str("Issuer certificate expired"),
			UnknownFeatures => f.write_str("Unknown features present"),
			IssuerUnknownFeatures => f.write_str("Unknown features present in issuer certificate"),
		}
	}
}

fn __parse_chain<'a>(chain: &[&'a [u8]]) ->
	Result<(Vec<X509SignedObj<'a>>, Vec<X509v3Certificate<'a>>), _VerifyX509ChainError>
{
	use self::_VerifyX509ChainError::*;
	let mut chain_sigobj = Vec::new();
	let mut chain_cert = Vec::new();
	if chain.len() == 0 { return Err(EmptyChain); }
	for (idx, ent) in chain.iter().cloned().enumerate() {
		let sobj = X509SignedObj::new(ent).map_err(|e|CertParseError(idx, From::from(e)))?;
		let cert = sobj.parse_certificate().map_err(|e|CertParseError(idx, e))?;
		chain_sigobj.push(sobj);
		chain_cert.push(cert);
	}
	Ok((chain_sigobj, chain_cert))
}

fn __check_ee_certificate(cert: &X509v3Certificate, names: &[SanEntry], time_now: i64) ->
	Result<(), _VerifyX509ChainError>
{
	//Check that the EE certificate is not marked as CA.
	if cert.is_ca { return Err(_VerifyX509ChainError::EeIsCa); }
	//Master must not have unknown features.
	if cert.unknown_feature { return Err(_VerifyX509ChainError::UnknownFeatures); }
	//Check expiry.
	if time_now < cert.not_before {
		return Err(_VerifyX509ChainError::NotYetValid(time_now, cert.not_before));
	}
	if time_now >= cert.not_after {
		return Err(_VerifyX509ChainError::CertificateExpired(time_now, cert.not_after));
	}
	//Check that names given 
	'next_name: for (nidx, name) in names.iter().enumerate() {
		for vname in cert.altnames.iter() {
			if name == vname { continue 'next_name; }
		}
		return Err(_VerifyX509ChainError::NotValidForName(nidx));
	}
	Ok(())
}

fn __check_ca_certificate(master: &X509v3Certificate, slave: &X509v3Certificate, slaveobj: &X509SignedObj,
	ee: &X509v3Certificate, time_now: i64, midx: usize) -> Result<(), _VerifyX509ChainError>
{
	use self::_VerifyX509ChainError::*;
	//Master must be a CA.
	if !master.is_ca { return Err(NotCa); }
	//Master must not have unknown features.
	if master.unknown_feature { return Err(IssuerUnknownFeatures); }
	//Master must be issuer of the slave, and slave must not have itself as issuer.
	if slave.issuer != master.subject || slave.issuer == slave.subject {
		return Err(BadIssuerChaining(midx-1));
	}
	//If master has maxdepth, slave must have smaller maxdepth. However, this does not apply on the first round
	//where slave is the EE certificate.
	match (slave.is_ca, slave.maxdepth, master.maxdepth) {
		//Path length must decrease.
		(true, Some(x), Some(y)) if x >= y => return Err(BadIssuerChaining(midx-1)),
		//Path length must be preserved.
		(true, None, Some(_)) => return Err(BadIssuerChaining(midx-1)),
		_ => ()
	};
	//Check expiry of master (slave has already been checked).
	if time_now < master.not_before { return Err(IssuerNotYetValid(time_now, master.not_before)); }
	if time_now >= master.not_after { return Err(IssuerCertificateExpired(time_now, master.not_after)); }
	//Check name constraints.
	for vname in ee.altnames.iter() {
		let mut ok = true;
		for constraint in master.const_forbid.iter() { ok &= !constraint.matches(vname); }
		for constraint in master.const_allow.iter() { ok |= constraint.matches(vname); }
		if !ok { return Err(NameConstraintViolation); }
	}
	//Check signature. Note that only the last certificate may be self-signed, and nothing checks its
	//signature.
	slaveobj.check_signature(__borrow_key(&master.key)).map_err(|e|BadSignature(midx-1, e))?;
	Ok(())
}

///A set of trust anchors.
pub struct TrustAnchorSet(BTreeMap<Vec<u8>, (Vec<u8>, Vec<u8>)>);

impl TrustAnchorSet
{
	///Create a new empty trust anchor set.
	pub fn new() -> TrustAnchorSet { TrustAnchorSet(BTreeMap::new()) }
	///Add a new certificate to the trust anchor set.
	pub fn add(&mut self, cert: &X509v3Certificate)
	{
		self.0.insert(cert.subject.deref().to_owned(),
			(cert.key.0.deref().to_owned(), cert.key.1.deref().to_owned()));
	}
	///Verify a certificate chain against this trust anchor.
	///
	/// * `chain`: The certificate chain.
	/// * `names`: Reference names to verify the certificate against.
	/// * `time_now`: Current unix timestamp.
	/// * On success: Returns `Ok(())`.
	/// * On error: returns `Err(err)`, where `err` describes the error.
	pub fn verify_chain<'a>(&self, chain: impl Iterator<Item=&'a [u8]>, names: &[SanEntry], time_now: i64) ->
		Result<(), VerifyX509ChainError>
	{
		self._verify_chain(chain, names, time_now).map_err(|e|VerifyX509ChainError(e))
	}
	fn _verify_chain<'a>(&self, mut chain: impl Iterator<Item=&'a [u8]>, names: &[SanEntry], time_now: i64) ->
		Result<(), _VerifyX509ChainError>
	{
		use self::_VerifyX509ChainError::*;
		let mut idx = 0;
		//Grab and parse EE certificate. It must always be present.
		let ee_raw = chain.next().ok_or(EmptyChain)?;
		let ee_obj = X509SignedObj::new(ee_raw).
			map_err(|e|CertParseError(idx, From::from(e)))?;
		let ee_crt = ee_obj.parse_certificate().map_err(|e|CertParseError(idx, e))?;
		__check_ee_certificate(&ee_crt, names, time_now)?;
		//On first iteration, slave and last is EE certificate.
		let mut slave_obj = ee_obj.clone();
		let mut slave_crt = ee_crt.clone();
		let mut last_obj = ee_obj.clone();
		let mut last_crt = ee_crt.clone();
		//Go through all certificates.
		let mut validated_root = false;
		while let Some(master_raw) = chain.next() {
			idx += 1;
			let master_obj = X509SignedObj::new(master_raw).
				map_err(|e|CertParseError(idx, From::from(e)))?;
			let master_crt = master_obj.parse_certificate().map_err(|e|CertParseError(idx, e))?;
			//Check the certificate. If master subject and key matches that of trust anchor, chain has
			//been successfully validated. However, still check the entiere chain.
			__check_ca_certificate(&master_crt, &slave_crt, &slave_obj, &ee_crt, time_now, idx)?;
			if let Some(key) = self.0.get(master_crt.subject.deref()) {
				let key = (key.0.deref(), key.1.deref());
				validated_root |= key == __borrow_key(&master_crt.key);
			}
			//Now, master becomes slave and last is updated.
			last_obj = master_obj.clone();
			last_crt = master_crt.clone();
			slave_obj = master_obj;
			slave_crt = master_crt;
		}
		if validated_root { return Ok(()); }
		//Root is missing. It must sign the last certificate.
		if let Some(key) = self.0.get(last_crt.issuer.deref()) {
			let key = (key.0.deref(), key.1.deref());
			last_obj.check_signature(key).map_err(|e|BadSignature(idx, e))?;
			return Ok(());
		}
		//None hit. This is issued by unknown ca.
		Err(IssuedByUnknownCa)
	}
}

fn __borrow_key<'a,'b:'a>(key: &'a (Cow<'b,[u8]>, Cow<'b,[u8]>)) -> (&'a [u8], &'a [u8])
{
	(key.0.deref(), key.1.deref())
}

///Check that certificate is consistent with a CSR.
///
/// * `cert`: The certificate to check.
/// * `csr`: The CSR to check.
/// * on success: Returns `Ok(())`.
/// * on error: Returns `Err(err)`, where `err` describes the error.
pub fn check_certificate_consistent_with_csr(cert: &X509v3Certificate, csr: &X509v3CSR) ->
	Result<(), CertificateConsistencyError>
{
	use self::_CertificateConsistencyError::*;
	use self::CertificateConsistencyError as CCE;
	//The SPKIs of certificate and CSR must be identical.
	if cert.rawspki != csr.spki { return Err(CCE(KeyMismatch)); }
	//Every requested SAN must be in certificate.
	'next_name: for san in csr.san.iter() {
		for csan in cert.altnames.iter() { if san == csan { continue 'next_name; } }
		let mut bad_name = String::new();
		write!(bad_name, "{san}").ok();
		return Err(CCE(NameNotInCertificate(bad_name)));
	}
	//Must-staple must match.
	if cert.must_staple != csr.must_staple { return Err(CCE(MustStapleMismatch)); }
	//Ok.
	Ok(())
}


#[cfg(test)]
mod test;
