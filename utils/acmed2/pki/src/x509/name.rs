use acmed2_memory as alloc;
use alloc::Cow;
use alloc::ToOwned;
use core::cmp::Ordering;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;

fn equals_case_insensitive(x: &[u8], y: &[u8]) -> bool
{
	if x.len() != y.len() { return false; }
	for (&x, &y) in x.iter().zip(y.iter()) {
		let x = if x.wrapping_sub(65) < 26 { x + 32 } else { x };
		let y = if y.wrapping_sub(65) < 26 { y + 32 } else { y };
		if x != y { return false; }
	}
	true
}

fn cmp_case_insensitive(x: &[u8], y: &[u8]) -> Ordering
{
	for (&x, &y) in x.iter().zip(y.iter()) {
		let x = if x.wrapping_sub(65) < 26 { x + 32 } else { x };
		let y = if y.wrapping_sub(65) < 26 { y + 32 } else { y };
		let t = x.cmp(&y);
		if t != Ordering::Equal { return t; }
	}
	//If prefixes did not resolve order, compare lengths.
	x.len().cmp(&y.len())
}

//This is needed by some other x509 stuff.
pub(super) fn __own_cow<'a,T:?Sized+ToOwned>(x: &Cow<'a,T>) -> Cow<'static,T> where <T as ToOwned>::Owned: Clone
{
	let y: <T as ToOwned>::Owned = match x {
		Cow::Borrowed(x) => T::to_owned(x),
		Cow::Owned(ref x) => x.clone(),
	};
	Cow::Owned(y)
}

///SubjectAlternativeName entry
#[derive(Clone,Debug)]
pub enum SanEntry<'a>
{
	///IPv4 address.
	Ipv4([u8;4]),
	///IPv6 address.
	Ipv6([u8;16]),
	///DNS name.
	Name(Cow<'a, [u8]>),
}

impl<'a> SanEntry<'a>
{
	pub fn as_static(&self) -> SanEntry<'static>
	{
		match self {
			SanEntry::Ipv4(x) => SanEntry::Ipv4(*x),
			SanEntry::Ipv6(x) => SanEntry::Ipv6(*x),
			SanEntry::Name(ref x) => SanEntry::Name(__own_cow(x)),
		}
	}
}

impl<'a> Eq for SanEntry<'a> {}

impl<'a> PartialEq for SanEntry<'a>
{
	fn eq(&self, othr: &Self) -> bool
	{
		match (self, othr) {
			(SanEntry::Ipv6(x), SanEntry::Ipv6(y)) => x == y,
			(SanEntry::Ipv4(x), SanEntry::Ipv4(y)) => x == y,
			(SanEntry::Name(x), SanEntry::Name(y)) => equals_case_insensitive(x, y),
			(_, _) => false,
		}
	}
}

impl<'a> PartialOrd for SanEntry<'a>
{
	fn partial_cmp(&self, othr: &Self) -> Option<Ordering> { Some(self.cmp(othr)) }
}


impl<'a> Ord for SanEntry<'a>
{
	fn cmp(&self, othr: &Self) -> Ordering
	{
		//This ordering does not have to be sensible, just valid.
		use Ordering::*;
		match (self, othr) {
			(SanEntry::Ipv6(x), SanEntry::Ipv6(y)) => x.cmp(y),
			(SanEntry::Ipv6(_), SanEntry::Ipv4(_)) => Less,
			(SanEntry::Ipv6(_), SanEntry::Name(_)) => Less,

			(SanEntry::Ipv4(_), SanEntry::Ipv6(_)) => Greater,
			(SanEntry::Ipv4(x), SanEntry::Ipv4(y)) => x.cmp(y),
			(SanEntry::Ipv4(_), SanEntry::Name(_)) => Less,

			(SanEntry::Name(_), SanEntry::Ipv6(_)) => Less,
			(SanEntry::Name(_), SanEntry::Ipv4(_)) => Less,
			(SanEntry::Name(x), SanEntry::Name(y)) => cmp_case_insensitive(x, y),
		}
	}
}

fn print_ipv6(s: [u16;8], m: u8, x: &mut Formatter) -> Result<(), FmtError>
{
	let (a,b,c,d,e,f,g,h) = (s[0],s[1],s[2],s[3],s[4],s[5],s[6],s[7]);
	if m & 255 == 0 {		//-------- (8)
		write!(x, "[::]")
	} else if m & 254 == 0 {	//X------- (7)
		write!(x, "[{a:x}::]")
	} else if m & 127 == 0 {	//-------X (7)
		write!(x, "[::{h:x}]")
	} else if m & 252 == 0 {	//XX------ (6)
		write!(x, "[{a:x}:{b:x}::]")
	} else if m & 126 == 0 {	//X------X (6)
		write!(x, "[{a:x}::{h:x}]")
	} else if m & 63 == 0 {		//------XX (6)
		write!(x, "[::{g:x}:{h:x}]")
	} else if m & 248 == 0 {	//XXX----- (5)
		write!(x, "[{a:x}:{b:x}:{c:x}::]")
	} else if m & 124 == 0 {	//XX-----X (5)
		write!(x, "[{a:x}:{b:x}::{h:x}]")
	} else if m & 62 == 0{		//X-----XX (5)
		write!(x, "[{a:x}::{g:x}:{h:x}]")
	} else if m & 31 == 0 {		//-----XXX (5)
		write!(x, "[::{f:x}:{g:x}:{h:x}]")
	} else if m & 240 == 0 {	//XXXX---- (4)
		write!(x, "[{a:x}:{b:x}:{c:x}:{d:x}::]")
	} else if m & 120 == 0 {	//XXX----X (4)
		write!(x, "[{a:x}:{b:x}:{c:x}::{h:x}]")
	} else if m & 60 == 0{		//XX----XX (4)
		write!(x, "[{a:x}:{b:x}::{g:x}:{h:x}]")
	} else if m & 30 == 0 {		//X----XXX (4)
		write!(x, "[{a:x}::{f:x}:{g:x}:{h:x}]")
	} else if m & 15 == 0 {		//----XXXX (4)
		write!(x, "[::{e:x}:{f:x}:{g:x}:{h:x}]")
	} else if m & 224 == 0 {	//XXXXX--- (3)
		write!(x, "[{a:x}:{b:x}:{c:x}:{d:x}:{e:x}::]")
	} else if m & 112 == 0 {	//XXXX---X (3)
		write!(x, "[{a:x}:{b:x}:{c:x}:{d:x}::{h:x}]")
	} else if m & 56 == 0{		//XXX---XX (3)
		write!(x, "[{a:x}:{b:x}:{c:x}::{g:x}:{h:x}]")
	} else if m & 28 == 0 {		//XX---XXX (3)
		write!(x, "[{a:x}:{b:x}::{f:x}:{g:x}:{h:x}]")
	} else if m & 14 == 0 {		//X---XXXX (3)
		write!(x, "[{a:x}::{e:x}:{f:x}:{g:x}:{h:x}]")
	} else if m & 7 == 0 {		//---XXXXX (3)
		write!(x, "[::{d:x}:{e:x}:{f:x}:{g:x}:{h:x}]")
	} else if m & 192 == 0 {	//XXXXXX-- (2)
		write!(x, "[{a:x}:{b:x}:{c:x}:{d:x}:{e:x}:{f:x}::]")
	} else if m & 96 == 0 {		//XXXXX--X (2)
		write!(x, "[{a:x}:{b:x}:{c:x}:{d:x}:{e:x}::{h:x}]")
	} else if m & 48 == 0{		//XXXX--XX (2)
		write!(x, "[{a:x}:{b:x}:{c:x}:{d:x}::{g:x}:{h:x}]")
	} else if m & 24 == 0 {		//XXX--XXX (2)
		write!(x, "[{a:x}:{b:x}:{c:x}::{f:x}:{g:x}:{h:x}]")
	} else if m & 12 == 0 {		//XX--XXXX (2)
		write!(x, "[{a:x}:{b:x}::{e:x}:{f:x}:{g:x}:{h:x}]")
	} else if m & 6 == 0 {		//X--XXXXX (2)
		write!(x, "[{a:x}::{d:x}:{e:x}:{f:x}:{g:x}:{h:x}]")
	} else if m & 3 == 0 {		//--XXXXXX (2)
		write!(x, "[::{c:x}:{d:x}:{e:x}:{f:x}:{g:x}:{h:x}]")
	} else if m & 128 == 0 {	//XXXXXXX- (1)
		write!(x, "[{a:x}:{b:x}:{c:x}:{d:x}:{e:x}:{f:x}:{g:x}::]")
	} else if m & 64 == 0 {		//XXXXXX-X (1)
		write!(x, "[{a:x}:{b:x}:{c:x}:{d:x}:{e:x}:{f:x}::{h:x}]")
	} else if m & 32 == 0{		//XXXXX-XX (1)
		write!(x, "[{a:x}:{b:x}:{c:x}:{d:x}:{e:x}::{g:x}:{h:x}]")
	} else if m & 16 == 0 {		//XXXX-XXX (1)
		write!(x, "[{a:x}:{b:x}:{c:x}:{d:x}::{f:x}:{g:x}:{h:x}]")
	} else if m & 8 == 0 {		//XXX-XXXX (1)
		write!(x, "[{a:x}:{b:x}:{c:x}::{e:x}:{f:x}:{g:x}:{h:x}]")
	} else if m & 4 == 0 {		//XX-XXXXX (1)
		write!(x, "[{a:x}:{b:x}::{d:x}:{e:x}:{f:x}:{g:x}:{h:x}]")
	} else if m & 2 == 0 {		//X-XXXXXX (1)
		write!(x, "[{a:x}::{c:x}:{d:x}:{e:x}:{f:x}:{g:x}:{h:x}]")
	} else if m & 1 == 0 {		//-XXXXXXX (1)
		write!(x, "[::{b:x}:{c:x}:{d:x}:{e:x}:{f:x}:{g:x}:{h:x}]")
	} else {			//XXXXXXXX (0)
		write!(x, "[{a:x}:{b:x}:{c:x}:{d:x}:{e:x}:{f:x}:{g:x}:{h:x}]")
	}
}

impl<'a> Display for SanEntry<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			SanEntry::Ipv6(x) => {
				let mut s = [0;8];
				let mut m = 0u8;
				for i in 0..8 {
					let mut t = [0;2];
					t.copy_from_slice(&x[2*i..2*i+2]);
					s[i] = u16::from_be_bytes(t);
					m |= if s[i] != 0 { 1 << i } else { 0 };
				}
				print_ipv6(s, m, f)
			},
			SanEntry::Ipv4(x) => {
				let [a, b, c, d] = x;
				write!(f, "[{a}.{b}.{c}.{d}]")
			},
			SanEntry::Name(x) => {
				for &b in x.iter() {
					if b >= 33 && b <= 126 && b != 92 {
						let c = b as char;
						write!(f, "{c}")?;
					} else if b == 92 {
						f.write_str("\\\\")?;
					} else if b == 32 {
						f.write_str("\\ ")?;
					} else {
						write!(f, "\\x{b:02x}")?;
					}
				}
				Ok(())
			},
		}
	}
}

///Name constraint entry
#[derive(Clone,Debug)]
pub enum ConstraintEntry<'a>
{
	///IPv4 address.
	Ipv4([u8;4],u8),
	///IPv6 address.
	Ipv6([u8;16], u8),
	///DNS name.
	Name(Cow<'a, [u8]>),
}

impl<'a> ConstraintEntry<'a>
{
	pub fn as_static(&self) -> ConstraintEntry<'static>
	{
		match self {
			ConstraintEntry::Ipv4(x,m) => ConstraintEntry::Ipv4(*x, *m),
			ConstraintEntry::Ipv6(x,m) => ConstraintEntry::Ipv6(*x, *m),
			ConstraintEntry::Name(ref x) => ConstraintEntry::Name(__own_cow(x)),
		}
	}
	pub fn matches(&self, san: &SanEntry) -> bool
	{
		match (self, san) {
			(ConstraintEntry::Ipv6(x, m), SanEntry::Ipv6(y)) => {
				let mut fbit = 128;
				for b in 0..128 {
					if (x[b/8] ^ y[b/8]) >> 7 - b%8 != 0 { fbit = b; break; }
				}
				fbit as u8 >= *m
			},
			(ConstraintEntry::Ipv4(x, m), SanEntry::Ipv4(y)) => {
				let mut fbit = 32;
				for b in 0..32 {
					if (x[b/8] ^ y[b/8]) >> 7 - b%8 != 0 { fbit = b; break; }
				}
				fbit as u8 >= *m
			},
			(ConstraintEntry::Name(ref x), SanEntry::Name(ref y)) => {
				if let Some(base) = x.strip_prefix(b".") {
					if equals_case_insensitive(base, y) {
						true	//Base domain.
					} else if y.len() >= x.len() {
						equals_case_insensitive(&x[x.len()-y.len()..], y)
					} else {
						false	//Not even postfix.
					}
				} else {
					equals_case_insensitive(x, y)
				}
			},
			(_, _) => false,
		}
	}
}
