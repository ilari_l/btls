use acmed2_cryptos as cryptos;
use acmed2_formats as formats;
use formats::Asn1Tlv;
use formats::GParse;
use core::fmt::Display;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;

fn __verify_ed25519(key: &[u8], msg: &[&[u8]], sig: &[u8], is_ed25519: bool) -> Result<(), Ed25519VerificationError>
{
	use self::Ed25519VerificationError::*;
	if !is_ed25519 { return Err(NotEd25519Key); }
	if key.len() != 32 { return Err(BadKeyFormat); }
	if sig.len() != 64 { return Err(BadSignatureFormat); }
	let mut key1 = [0;32]; key1.copy_from_slice(key);
	let mut sig1 = [0;64]; sig1.copy_from_slice(sig);
	cryptos::ed25519_verify(&key1, &sig1, |h|for part in msg.iter() { h.update(part); }).map_err(|e|match e {
		cryptos::LEd25519VerifyError::InvalidKey => InvalidKey,
		cryptos::LEd25519VerifyError::InvalidSignature => InvalidSignature,
		cryptos::LEd25519VerifyError::VerifyFailed => VerifyFailed,
	})
}

fn __verify_ecdsa(key: &[u8], msg: &[u8], sig: &[u8], h: u8, c_oid: Option<&[u8]>) ->
	Result<(), EcdsaVerificationError>
{
	use self::EcdsaVerificationError::*;
	if c_oid.is_none() { return Err(NotEcdsaKey); }
	let (exp_keylen, crvid) = if c_oid == Some(&[6,8,42,134,72,206,61,3,1,7]) {
		(65, cryptos::EcdsaCurve::P256)
	} else if c_oid == Some(&[6,5,43,129,4,0,34]) {
		(97, cryptos::EcdsaCurve::P384)
	} else {
		return Err(UnsupportedCurve);
	};
	//Key must start with 0x04 and be of correct length.
	if key.len() != exp_keylen || key[0] != 4 { return Err(BadKeyFormat); }
	let key = &key[1..];
	//For some insane reason, signature is encoded as ASN.1
	let mut sig = GParse::new(sig);
	let (r, s) = sig.asn1_fn((1, 16), |sigtop|{
		let r = sigtop.asn1().and_then(|v|__check_asn1_uint(v))?;
		let s = sigtop.asn1().and_then(|v|__check_asn1_uint(v))?;
		Some((r, s))
	}).unwrap_or(None).ok_or(BadSignatureFormat)?;
	if sig.more() { return Err(BadSignatureFormat); }
	
	match h {
		0 => cryptos::ecdsa_verify_sha256(key, r, s, crvid, |h|h.update(msg)),
		1 => cryptos::ecdsa_verify_sha384(key, r, s, crvid, |h|h.update(msg)),
		_ => return Err(UnsupportedHash),	//??? hash.
	}.map_err(|e|match e {
		cryptos::EcdsaVerifyError::BadKey => BadKey,
		cryptos::EcdsaVerifyError::BadSignature => BadSignature,
		cryptos::EcdsaVerifyError::VerifyFailed => VerifyFailed,
	})
}

fn __check_asn1_uint<'a>(value: Asn1Tlv<'a>) -> Option<&'a [u8]>
{
	//Tag must be INTEGER (0,2).
	if value.tag != (0,2) { return None; }
	let value = value.value;
	//If first nonzero is >= 128, there must be 0 prefix so it is positive.
	if value.len() > 1 && value[0] == 0 && value[1] >= 128 { return Some(&value[1..]); }
	//If first nonzero is <128, it is positive.
	if value.len() > 0 && value[0] < 128 { return Some(value); }
	//All other cases bad.
	None
}

fn __verify_rsa(key: &[u8], msg: &[u8], sig: &[u8], h: u8, is_rsa: bool) -> Result<(), RsaVerificationError>
{
	use self::RsaVerificationError::*;
	if !is_rsa { return Err(NotRsaKey); }
	//For some insane reason, key is encoded as ASN.1
	let mut key = GParse::new(key);
	let mut keytop = key.asn1_typed((1,16)).ok_or(BadKeyFormat)?.gparse;
	let n = keytop.asn1().and_then(|v|__check_asn1_uint(v)).ok_or(BadKeyFormat)?;
	let e = keytop.asn1().and_then(|v|__check_asn1_uint(v)).ok_or(BadKeyFormat)?;
	if keytop.more() { return Err(BadKeyFormat); }

	match h {
		0 => cryptos::rsa_pkcs1_verify_sha256(n, e, sig, |h|h.update(msg)),
		1 => cryptos::rsa_pkcs1_verify_sha384(n, e, sig, |h|h.update(msg)),
		2 => cryptos::rsa_pkcs1_verify_sha512(n, e, sig, |h|h.update(msg)),
		_ => return Err(UnsupportedHash),	//??? hash.
	}.map_err(|e|match e {
		cryptos::RsaVerifyError::BadKey => BadKey,
		cryptos::RsaVerifyError::BadSignature => BadSignature,
		cryptos::RsaVerifyError::UnsupportedKey => UnsupportedKey,
		cryptos::RsaVerifyError::InsufficientSecurity => InsufficientSecurity,
		cryptos::RsaVerifyError::VerifyFailed => VerifyFailed,
	})
}

fn __verify_tls_sig(key: (&[u8], &[u8]), msg: &[&[u8]], sig: (u16, &[u8])) -> Result<(), _VerifyTlsError>
{
	use self::_VerifyTlsError::*;
	let is_ed25519_key = key.0 == &[6,3,43,101,112];
	match sig.0 {
		0x0807 => __verify_ed25519(key.1, msg, sig.1, is_ed25519_key).map_err(|e|Ed25519Failed(e)),
		//Only Ed25519 is suported.
		_ => Err(UnsupportedAlgorithm)
	}
}

///Verify TLS signature.
///
/// * `key`: a tuple `(keytype,keydata)`.
///   * `keytype`: the inner contents of X.509 ubjectPublicKeyInfo.algorithm field.
///   * `keydata`: the is the inner contents (with any padding removed) of the
///X.509 subjectPublicKeyInfo.subjectPublicKey field.
/// * `msg`: the raw message to verify. The fragments are concatenated.
/// * `sig`: a tuple `(sigtype,signature)`.
///   * `sigtype`: the value of TLS CertificateVerify.algorithm field.
///   * `signature`: the inner contents of the CertificateVerify.signature field.
/// * On success: returns `Ok(())`.
/// * On error: returns `Err(err)`, where `err` describes the error.
///
///As of currently, the following signature algorithms are supported.
///
/// * Ed25519
pub fn verify_tls_sig(key: (&[u8], &[u8]), msg: &[&[u8]], sig: (u16, &[u8])) -> Result<(), VerifyTlsError>
{
	__verify_tls_sig(key, msg, sig).map_err(|e|VerifyTlsError(e))
}

///Error from Ed25519 signature verification.
#[derive(Copy,Clone,Debug)]
enum Ed25519VerificationError
{
	///Not Ed25519 key.
	NotEd25519Key,
	///Key length is bad.
	BadKeyFormat,
	///Signature length is bad.
	BadSignatureFormat,
	///Invalid key.
	InvalidKey,
	///Invalid signature.
	InvalidSignature,
	///Signature fails validation.
	VerifyFailed,
}

impl Display for Ed25519VerificationError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		f.write_str(match self {
			Ed25519VerificationError::NotEd25519Key => "Not Ed25519 key",
			Ed25519VerificationError::BadKeyFormat => "Bad key format",
			Ed25519VerificationError::BadSignatureFormat => "Bad signature format",
			Ed25519VerificationError::InvalidKey => "Invalid key",
			Ed25519VerificationError::InvalidSignature => "Invalid signature",
			Ed25519VerificationError::VerifyFailed => "Signature does not verify",
		})
	}
}

#[derive(Copy,Clone,Debug)]
enum EcdsaVerificationError
{
	NotEcdsaKey,
	BadKeyFormat,
	BadSignatureFormat,
	UnsupportedHash,
	UnsupportedCurve,
	BadKey,
	BadSignature,
	VerifyFailed,
}

impl Display for EcdsaVerificationError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		f.write_str(match self {
			EcdsaVerificationError::NotEcdsaKey => "Not ECDSA key",
			EcdsaVerificationError::BadKeyFormat => "Bad key format",
			EcdsaVerificationError::BadSignatureFormat => "Bad signature format",
			EcdsaVerificationError::UnsupportedHash => "Unsupported hash function",
			EcdsaVerificationError::UnsupportedCurve => "Unsupported curve",
			EcdsaVerificationError::BadKey => "Invalid key",
			EcdsaVerificationError::BadSignature => "Invalid signature",
			EcdsaVerificationError::VerifyFailed => "Signature does not verify",
		})
	}
}

#[derive(Copy,Clone,Debug)]
enum RsaVerificationError
{
	NotRsaKey,
	BadKeyFormat,
	UnsupportedHash,
	BadKey,
	BadSignature,
	UnsupportedKey,
	InsufficientSecurity,
	VerifyFailed,
}

impl Display for RsaVerificationError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		f.write_str(match self {
			RsaVerificationError::NotRsaKey => "Not RSA key",
			RsaVerificationError::BadKeyFormat => "Bad key format",
			RsaVerificationError::UnsupportedHash => "Unsupported hash function",
			RsaVerificationError::UnsupportedKey => "Unsupported key",
			RsaVerificationError::BadKey => "Invalid key",
			RsaVerificationError::BadSignature => "Invalid signature",
			RsaVerificationError::InsufficientSecurity => "Modulus too short",
			RsaVerificationError::VerifyFailed => "Signature does not verify",
		})
	}
}

///Error from X509 signature verification.
#[derive(Copy,Clone,Debug)]
pub struct VerifyX509Error(_VerifyX509Error);
#[derive(Copy,Clone,Debug)]
enum _VerifyX509Error
{
	///RSA signature verification failed.
	RsaFailed(RsaVerificationError),
	///ECDSA signature verification failed
	EcdsaFailed(EcdsaVerificationError),
	///Ed25519 signature verification failed.
	Ed25519Failed(Ed25519VerificationError),
	///Unsupported signature algorithm.
	UnsupportedAlgorithm,
}


impl Display for VerifyX509Error
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self.0 {
			_VerifyX509Error::RsaFailed(err) => write!(f, "RSA: {err}"),
			_VerifyX509Error::EcdsaFailed(err) => write!(f, "ECDSA: {err}"),
			_VerifyX509Error::Ed25519Failed(err) => write!(f, "Ed25519: {err}"),
			_VerifyX509Error::UnsupportedAlgorithm => f.write_str("Unsupported signature algorithm"),
		}
	}
}

///Error from TLS signature verification.
#[derive(Copy,Clone,Debug)]
pub struct VerifyTlsError(_VerifyTlsError);
#[derive(Copy,Clone,Debug)]
enum _VerifyTlsError
{
	///Ed25519 signature verification failed.
	Ed25519Failed(Ed25519VerificationError),
	///Unsupported key/signed algorithm pair.
	UnsupportedAlgorithm,
}

impl Display for VerifyTlsError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self.0 {
			_VerifyTlsError::Ed25519Failed(err) => write!(f, "Ed25519: {err}"),
			_VerifyTlsError::UnsupportedAlgorithm => f.write_str("Unsupported signature algorithm"),
		}
	}
}

fn __verify_x509_sig(key: (&[u8], &[u8]), msg: &[u8], sig: (&[u8], &[u8])) -> Result<(), _VerifyX509Error>
{
	use self::_VerifyX509Error::*;
	let is_rsa_key = key.0 == &[6,9,42,134,72,134,247,13,1,1,1,5,0];
	let is_ecdsa_key = key.0.strip_prefix(&[6,7,42,134,72,206,61,2,1]);
	let is_ed25519_key = key.0 == &[6,3,43,101,112];
	if sig.0 == &[6,3,43,101,112] {
		__verify_ed25519(key.1, &[msg], sig.1, is_ed25519_key).map_err(|e|Ed25519Failed(e))
	} else if sig.0 == &[6,8,42,134,72,206,61,4,3,2] {
		__verify_ecdsa(key.1, msg, sig.1, 0, is_ecdsa_key).map_err(|e|EcdsaFailed(e))
	} else if sig.0 == &[6,8,42,134,72,206,61,4,3,3] {
		__verify_ecdsa(key.1, msg, sig.1, 1, is_ecdsa_key).map_err(|e|EcdsaFailed(e))
	} else if sig.0 == &[6,9,42,134,72,134,247,13,1,1,11,5,0] {
		__verify_rsa(key.1, msg, sig.1, 0, is_rsa_key).map_err(|e|RsaFailed(e))
	} else if sig.0 == &[6,9,42,134,72,134,247,13,1,1,12,5,0] {
		__verify_rsa(key.1, msg, sig.1, 1, is_rsa_key).map_err(|e|RsaFailed(e))
	} else if sig.0 == &[6,9,42,134,72,134,247,13,1,1,13,5,0] {
		__verify_rsa(key.1, msg, sig.1, 2, is_rsa_key).map_err(|e|RsaFailed(e))
	} else {
		//???
		Err(UnsupportedAlgorithm)
	}
}

///Verify X.509 signature.
///
/// * `key`: a tuple `(keytype,keydata)`.
///   * `keytype`: the inner contents of X.509 ubjectPublicKeyInfo.algorithm field.
///   * `keydata`: the inner contents (with any padding removed) of the X.509 subjectPublicKeyInfo.subjectPublicKey
///field.
/// * `msg`: the raw message to verify.
/// * `sig`: a tuple `(sigtype,signature)`.
///   * `sigtype`: the inner contents of the X.509 signatureAlgorithm field.
///   * `signature`: the inner contents (with any padding removed) of the signatureValue field.
/// * On success: returns `Ok(())`.
/// * On error: returns `Err(err)`, where `err` describes the error.
///
///As of currently, the following signature algorithms are supported.
///
/// * Ed25519
/// * ECDSA
///   * With P-256 and P-384 curves.
///   * With SHA-256 and SHA-384 hashes.
/// * RSA
///   * PKCS#1 v1.5 signature.
///   * Key size 2048 to 4096 bits, in multiple of 64.
///   * Odd e in range 3 to 2^64-1.
pub fn verify_x509_sig(key: (&[u8], &[u8]), msg: &[u8], sig: (&[u8], &[u8])) -> Result<(), VerifyX509Error>
{
	__verify_x509_sig(key, msg, sig).map_err(|e|VerifyX509Error(e))
}
