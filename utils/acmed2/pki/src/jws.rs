use super::base64url;
use acmed2_formats::JsonSValue;
use acmed2_memory::BTreeMap;
use acmed2_memory::Cow;
use acmed2_memory::String;
use acmed2_memory::ToOwned;
use acmed2_memory::Vec;

#[derive(Copy,Clone,Eq,PartialEq,Debug)]
pub enum JwsKeyType
{
	EcdsaP256,
	EcdsaP384,
	EcdsaP521,
	Ed25519,
	Ed448,
	//RSA is legacy, so no reason to have it here.
}

impl JwsKeyType
{
	pub fn default_algorithm_tls(&self) -> u16
	{
		match self {
			&JwsKeyType::EcdsaP256 => 0x403,
			&JwsKeyType::EcdsaP384 => 0x503,
			&JwsKeyType::EcdsaP521 => 0x603,
			&JwsKeyType::Ed25519 => 0x807,
			&JwsKeyType::Ed448 => 0x808,
		}
	}
	pub fn default_algorithm_jose(&self) -> &'static str
	{
		match self {
			&JwsKeyType::EcdsaP256 => "ES256",
			&JwsKeyType::EcdsaP384 => "ES384",
			&JwsKeyType::EcdsaP521 => "ES512",
			&JwsKeyType::Ed25519 => "EdDSA",
			&JwsKeyType::Ed448 => "EdDSA",
		}
	}
	fn default_algorithm_encoding(&self) -> SignatureEncoding
	{
		match self {
			&JwsKeyType::EcdsaP256 => SignatureEncoding::EcdsaP256,
			&JwsKeyType::EcdsaP384 => SignatureEncoding::EcdsaP384,
			&JwsKeyType::EcdsaP521 => SignatureEncoding::EcdsaP521,
			&JwsKeyType::Ed25519 => SignatureEncoding::Identity,
			&JwsKeyType::Ed448 => SignatureEncoding::Identity,
		}
	}
}

#[derive(Copy,Clone,Eq,PartialEq,Debug)]
enum SignatureEncoding
{
	EcdsaP256,
	EcdsaP384,
	EcdsaP521,
	Identity,
}

impl SignatureEncoding
{
	fn encode<'a>(&self, sig: &'a [u8]) -> Option<Cow<'a, [u8]>>
	{
		Some(match self {
			&SignatureEncoding::Identity => Cow::Borrowed(sig),
			&SignatureEncoding::EcdsaP256 => Cow::Owned(Self::encode_ecdsa(sig,32)?),
			&SignatureEncoding::EcdsaP384 => Cow::Owned(Self::encode_ecdsa(sig,48)?),
			&SignatureEncoding::EcdsaP521 => Cow::Owned(Self::encode_ecdsa(sig,66)?),
		})
	}
	fn encode_ecdsa(sig: &[u8], cl: usize) -> Option<Vec<u8>>
	{
		//Component lengths that are so long that inner part can bust 255 octets or components can bust
		//127 octets are not supported.
		if cl > 124 { return None; }
		//If signature is less than 8 bytes, then main header or shortest possible components will not
		//fit, so reject it. Also reject if main header is not SEQUENCE.
		if sig.len() < 8 || sig[0] != 0x30 { return None; }
		//If component length is above 60 octets, the outer length may overflow one octet. Only set the
		//flag if it actually does.
		let long = cl > 60 && sig[1] == 129;
		//Split innards.
		let sig = if long {
			//Length better be in range and correct. The minimum length is 128 as this is long form.
			if sig[2] < 128 || sig[2] > 6 + 2 * cl as u8 || sig.len() != 3 + sig[2] as usize {
				return None;
			}
			&sig[3..]
		} else {
			//Length better be in range and correct. Note that invalid lengths come here, and cl
			//can still be above 60, so check short-form length is not above 127.
			if sig[1] < 6 || sig[1] > 6 + 2 * cl as u8 || sig[1] > 127 ||
				sig.len() != 2 + sig[1] as usize {
				return None;
			}
			&sig[2..]
		};
		//By above check, sig is at least 5 bytes, so can read the length of r. Check that tags of r and s
		//are both 2, and that lengths match up. Then strip headers.
		if sig.len() < 5 + sig[1] as usize { return None; }
		let (r, s) = sig.split_at(2 + sig[1] as usize);	//2+sig[1]<5+sig[1].
		let mut out = Vec::new();
		Self::push_asn1_integer(&mut out, r, cl)?;
		Self::push_asn1_integer(&mut out, s, cl)?;
		Some(out)
	}
	fn push_asn1_integer(out: &mut Vec<u8>, x: &[u8], cl: usize) -> Option<()>
	{
		static ZEROS: [u8;127] = [0;127];
		if x.len() < 3 || x[0] != 2 || x.len() != 2 + x[1] as usize || cl > 127 { return None; }
		let x = &x[2..];
		//High byte start is illegal. Zero gets stripped.
		if x.len() >= 2 && x[0] == 0 && x[1] < 128 { return None; }
		let x = if x[0] > 127 { return None; } else if x[0] == 0 { &x[1..] } else { x };
		if x.len() > cl {
			return None;					//How on earth?
		} else if x.len() < cl {
			out.extend_from_slice(&ZEROS[..cl-x.len()]);	//Zeropad.
			out.extend_from_slice(x);
		} else {
			out.extend_from_slice(x);			//Exact fit.
		}
		Some(())
	}
}

#[derive(Clone)]
pub struct UnsignedJws
{
	encoding: SignatureEncoding,
	algorithm: u16,
	header: String,		//base64url-encoded.
	payload: String,	//base64url-encoded.
}

impl UnsignedJws
{
	///Create unsigned JWS. Note that alg header is always written.
	pub fn create(mut header: BTreeMap<String, JsonSValue>, payload: &[u8], key: JwsKeyType) ->
		UnsignedJws
	{
		//Need to add alg.
		header.insert("alg".to_owned(), JsonSValue::from(key.default_algorithm_jose().to_owned()));
		UnsignedJws {
			encoding: key.default_algorithm_encoding(),
			algorithm: key.default_algorithm_tls(),
			header: base64url(JsonSValue::Object(header).as_string().as_bytes()),
			payload: base64url(payload),
		}
	}
	///Extract algorithm and TBS.
	pub fn prepare_sign(&self) -> (u16, Vec<u8>)
	{
		let mut out = Vec::with_capacity(self.header.len()+self.payload.len()+1);
		out.extend_from_slice(self.header.as_bytes());
		out.push(b'.');
		out.extend_from_slice(self.payload.as_bytes());
		(self.algorithm, out)
	}
	///Attach signature to the message.
	pub fn commit_sign(&self, sig: &[u8]) -> Option<String>
	{
		//If using ECDSA, the signature needs to be re-encoded.
		let sig = self.encoding.encode(sig)?;
		let mut out = String::new();
		out.push_str("{protected:\"");
		out.push_str(&self.header);
		out.push_str("\",payload:\"");
		out.push_str(&self.payload);
		out.push_str("\",signature:\"");
		out.push_str(&base64url(&sig));
		out.push_str("\"}");
		Some(out)
	}
}

#[cfg(test)]
fn munge_word(mut w: &[u8]) -> Vec<u8>
{
	while w.len() > 0 && w[0] == 0 { w = &w[1..]; }
	let mut w = w.to_owned();
	if w.len() > 0 && w[0] > 127 { w.insert(0, 0); }
	w
}

#[cfg(test)]
fn encode_signature(sig: &[u8]) -> Vec<u8>
{
	if sig.len() != 64 && sig.len() != 96 && sig.len() != 132 { panic!("Unsupported signature length"); }
	let (r,s) = sig.split_at(sig.len()/2);
	let r = munge_word(r);
	let s = munge_word(s);
	let mut out = Vec::new();
	let len = r.len() + s.len() + 4;
	out.push(48);
	if len > 127 { out.push(129); }
	out.push(len as u8);
	out.push(2);
	out.push(r.len() as u8);
	out.extend_from_slice(&r);
	out.push(2);
	out.push(s.len() as u8);
	out.extend_from_slice(&s);
	out
}

#[test]
fn test_encode_ecdsa_p256()
{
	use std::fs::File;
	use std::io::Read;
	let mut fp = File::open("/dev/urandom").expect("Open failed");
	let mut buf = [0;64];
	for _ in 0..10000 {
		fp.read_exact(&mut buf).expect("Read failed");
		let sig = encode_signature(&buf);
		let y = SignatureEncoding::EcdsaP256.encode(&sig).expect("Transcode failed");
		assert_eq!(y, &buf[..]);
	}
}

#[test]
fn test_encode_ecdsa_p384()
{
	use std::fs::File;
	use std::io::Read;
	let mut fp = File::open("/dev/urandom").expect("Open failed");
	let mut buf = [0;96];
	for _ in 0..10000 {
		fp.read_exact(&mut buf).expect("Read failed");
		let sig = encode_signature(&buf);
		let y = SignatureEncoding::EcdsaP384.encode(&sig).expect("Transcode failed");
		assert_eq!(y, &buf[..]);
	}
}

#[test]
fn test_encode_ecdsa_p521()
{
	use std::fs::File;
	use std::io::Read;
	let mut fp = File::open("/dev/urandom").expect("Open failed");
	let mut buf = [0;132];
	for _ in 0..10000 {
		fp.read_exact(&mut buf).expect("Read failed");
		let sig = encode_signature(&buf);
		let y = SignatureEncoding::EcdsaP521.encode(&sig).expect("Transcode failed");
		assert_eq!(y, &buf[..]);
	}
}

#[test]
fn test_encode_ecdsa_p256_zero()
{
	let input: [u8;8] = [48, 6, 2, 1, 0, 2, 1, 0];
	let y = SignatureEncoding::EcdsaP256.encode(&input).expect("Transcode failed");
	assert_eq!(y, &[0;64][..]);
}

#[test]
fn test_encode_ecdsa_p384_zero()
{
	let input: [u8;8] = [48, 6, 2, 1, 0, 2, 1, 0];
	let y = SignatureEncoding::EcdsaP384.encode(&input).expect("Transcode failed");
	assert_eq!(y, &[0;96][..]);
}

#[test]
fn test_encode_ecdsa_p521_zero()
{
	let input: [u8;8] = [48, 6, 2, 1, 0, 2, 1, 0];
	let y = SignatureEncoding::EcdsaP521.encode(&input).expect("Transcode failed");
	assert_eq!(y, &[0;132][..]);
}

#[test]
fn test_encode_ecdsa_p521_abnormal_short()
{
	let input: [u8; 129] = [48, 127,
		2, 62,
		1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
		21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
		41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
		61, 62,
		2, 61,
		101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120,
		121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140,
		141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160,
		161,
	];
	let ans: [u8;132] = [
		0, 0, 0, 0,
		1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
		21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
		41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
		61, 62,
		0,0,0,0,0,
		101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120,
		121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140,
		141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160,
		161,
	];
	let y = SignatureEncoding::EcdsaP521.encode(&input).expect("Transcode failed");
	assert_eq!(y, &ans[..]);
}

#[test]
fn test_encode_ecdsa_p256_bad()
{
	let enc = SignatureEncoding::EcdsaP256;
	assert!(enc.encode(&[]).is_none());
	assert!(enc.encode(&[48]).is_none());
	assert!(enc.encode(&[48,0]).is_none());
	assert!(enc.encode(&[48,3,2,1,0]).is_none());
	assert!(enc.encode(&[49,6,2,1,0,2,1,0]).is_none());
	assert!(enc.encode(&[48,6,3,1,0,2,1,0]).is_none());
	assert!(enc.encode(&[48,6,2,1,0,3,1,0]).is_none());
	assert!(enc.encode(&[48,6,2,2,3,4,2,1]).is_none());
	assert!(enc.encode(&[48,7,2,2,3,4,2,1]).is_none());
}
