use acmed2_memory::String;

//0-25 => 65-90 (0-25)
//26-51 => 97-122 (32-57)
//52-61 => 48-57 (239-248)
//62 => 45 (236)
//63 => 95 (30).

fn base64urlch(x: u8) -> u8
{
	match x {
		0..=25 => x.wrapping_add(65),
		26..=51 => x.wrapping_add(71),
		52..=61 => x.wrapping_add(252),
		62 => 45,
		63 => 95,
		_ => 0,
	}
}

fn base64ch(x: u8) -> u8
{
	match x {
		0..=25 => x.wrapping_add(65),
		26..=51 => x.wrapping_add(71),
		52..=61 => x.wrapping_add(252),
		62 => 43,
		63 => 47,
		_ => 0,
	}
}

#[inline(always)]
fn __base64_generic(x: &[u8], ch: fn(u8)->u8, pad: bool, warp: bool) -> String
{
	let mut s = String::new();
	let mut x = x.chunks_exact(3);
	let mut blocks = 0;
	while let Some(x) = x.next() {
		if warp && blocks == 16 { s.push('\n'); blocks = 0; }
		let a = x[0] >> 2;
		let b = x[0] << 4 & 63 | x[1] >> 4;
		let c = x[1] << 2 & 63 | x[2] >> 6;
		let d = x[2] & 63;
		s.push(ch(a) as char);
		s.push(ch(b) as char);
		s.push(ch(c) as char);
		s.push(ch(d) as char);
		blocks += 1;
	}
	let x = x.remainder();
	if x.len() == 1 {
		let a = x[0] >> 2;
		let b = x[0] << 4 & 63;
		s.push(ch(a) as char);
		s.push(ch(b) as char);
		if pad { s.push('='); s.push('='); }
	} else if x.len() == 2 {
		let a = x[0] >> 2;
		let b = x[0] << 4 & 63 | x[1] >> 4;
		let c = x[1] << 2 & 63;
		s.push(ch(a) as char);
		s.push(ch(b) as char);
		s.push(ch(c) as char);
		if pad { s.push('='); }
	}
	s
}

pub fn base64url(x: &[u8]) -> String
{
	__base64_generic(x, base64urlch, false, false)
}

pub fn base64pem(x: &[u8]) -> String
{
	__base64_generic(x, base64ch, true, true)
}
