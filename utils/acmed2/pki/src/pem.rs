use acmed2_memory::Vec;
use core::mem::replace;
use core::ops::Range;


fn decode_base64(data: &[u8]) -> Option<Vec<u8>>
{
	let mut out = Vec::new();
	let mut s = 1u32;
	let mut padc = 0u32;
	for &c in data.iter() {
		//Ignore whitespace in all states.
		if c>=9&&c<=11||c==13||c==32 { continue; }
		//If padding character has been seen, any non-whitespace must be =.
		else if padc > 0 { if c == 61 { padc += 1; continue; } else { return None; } }
		//Decode Base64 data characters.
		else if c >= 65 && c <= 90 { s = 64 * s + c as u32 - 65; }
		else if c >= 97 && c <= 122 { s = 64 * s + c as u32 - 71; }
		else if c >= 48 && c <= 57 { s = 64 * s + c as u32 + 4; }
		else if c == 43 { s = 64 * s + 62; }
		else if c == 47 { s = 64 * s + 63; }
		//First =.
		else if c == 61 { padc = 1; }
		//Anything else is not valid.
		else { return None; }	//Not valid.
		if s >> 24 > 0 {
			let x = replace(&mut s, 1) & 0xFFFFFF;
			out.push((x >> 16) as u8);
			out.push((x >> 8) as u8);
			out.push(x as u8);
		}
	}
	if s >> 18 > 0 {
		if padc != 0 && padc != 1 || s & 3 != 0 { return None; }	//0 or 1 padding, clear 2 bits.
		out.push((s >> 10) as u8);
		out.push((s >> 2) as u8);
	} else if s >> 12 > 0 {
		if padc != 0 && padc != 2 || s & 15 != 0 { return None; }	//0 or 2 padding, clear 4 bits.
		out.push((s >> 4) as u8);
	} else if s >> 6 > 0 {
		//Single character in group. This is not legal.
		return None;
	}
	Some(out)
}

struct Lines<'a>(&'a [u8], usize);

impl<'a> Iterator for Lines<'a>
{
	type Item=&'a [u8];
	fn next(&mut self) -> Option<&'a [u8]>
	{
		if self.1 >= self.0.len() { return None; }
		let mut nptr = self.1;
		while nptr < self.0.len() && self.0[nptr] != 10 && self.0[nptr] != 13 { nptr += 1; }
		let line = &self.0[self.1..nptr];
		if nptr + 1 < self.0.len() && self.0[nptr] == 13 && self.0[nptr+1] == 10 {
			self.1 = nptr+2;
		} else {
			self.1 = nptr+1;
		}
		Some(line)
	}
}

///PEM decoder interface.
pub trait PemDecoder
{
	///Encontered PEM block. Decode?
	fn request_decode(&mut self, name: &[u8]) -> bool;
	///Pass decoded block.
	fn decoded(&mut self, lrange: Result<Range<usize>, usize>, block: &[u8]);
	///Error.
	fn error(&mut self, lrange: Result<Range<usize>, usize>, errmsg: &str, fatal: bool);
}

///Read a PEM or DER file.
pub fn read_pem(data: &[u8], callback: &mut impl PemDecoder)
{
	let mut at_least_one = false;
	let mut decoding = false;
	let mut in_blk = None;
	let mut cert_buf = Vec::new();
	for (num,line) in Lines(data, 0).enumerate() {
		if let Some((sline, bkind)) = in_blk {
			if line.starts_with(b"-----END ") && line.ends_with(b"-----") &&
				bkind == &line[9..line.len()-5] {
				//The line range for the block.
				let line_range = sline+1..num+1;
				in_blk = None;
				//Found at least one block, so this is PEM file.
				at_least_one = true;
				//If not interested, skip decoding.
				if !decoding { continue; }
				//Decode the block as base64.
				match decode_base64(&replace(&mut cert_buf, Vec::new())) {
					Some(x) => callback.decoded(Ok(line_range), &x),
					None => callback.error(Ok(line_range), "Bad Base64 encoding", false)
				};
			} else {
				//Append line to certificate Base64 encoding.
				if decoding { cert_buf.extend_from_slice(line); }
			}
		} else if line.starts_with(b"-----BEGIN ") && line.ends_with(b"-----") {
			let name = &line[11..line.len()-5];
			in_blk = Some((num, name));
			decoding = callback.request_decode(name);
		}
	}
	if at_least_one { return; }
	//It is not PEM, try concatenation of DER.
	let mut data = data;
	let osize = data.len();
	while data.len() > 0 {
		let p = osize-data.len();
		match Asn1Tlv::parse_asn1(&mut data) {
			Some(tlv) => callback.decoded(Err(p), tlv.outer),
			None => {
				callback.error(Err(p), "Truncated ASN.1 TLV", true);
				break;
			}
		}
	}
}

///ASN.1 TLV
pub struct Asn1Tlv<'a>
{
	pub tag: &'a [u8],
	pub value: &'a [u8],
	pub outer: &'a [u8],
}

impl<'a> Asn1Tlv<'a>
{
	///Parse ASN.1 tag.
	pub fn parse_asn1(data: &mut &'a [u8]) -> Option<Asn1Tlv<'a>>
	{
		let tmp = *data;
		let (tag, l1) = read_asn1_tag(data)?;
		let (len, l2) = read_asn1_len(data)?;
		if data.len() < len || tmp.len() < l1 + l2 + len { return None; }
		let (inner, tail) = data.split_at(len);
		let outer = &tmp[..l1+l2+len];
		*data = tail;
		Some(Asn1Tlv {
			tag: tag,
			value: inner,
			outer: outer,
		})
	}
}

fn read_asn1_tag<'a>(data: &mut &'a [u8]) -> Option<(&'a [u8], usize)>
{
	let mut len = 1;
	if data.len() < len { return None; }	//At end, no tag.
	let t = data[0];
	while t & 31 == 31 {
		//Extended tag.
		len += 1;
		if data.len() < len { return None; }	//Tag runs off.
		if data[len-1] & 128 == 0 { break; }
	}
	let (head, tail) = data.split_at(len);
	*data = tail;
	Some((head, len))
}

fn read_asn1_len<'a>(data: &mut &'a [u8]) -> Option<(usize, usize)>
{
	if data.len() < 1 { return None; }	//At end, no length.
	let (len, plen) = match data[0] {
		l@0..=127 => (1, l as usize),
		129 => {
			if data.len() < 2 { return None; }	//Length runs off.
			(2, data[1] as usize)
		},
		130 => {
			if data.len() < 3 { return None; }	//Length runs off.
			(3, (data[1] as usize) * 256 + (data[2] as usize))
		},
		131 => {
			if data.len() < 4 { return None; }	//Length runs off.
			(4, (data[1] as usize) * 65536 + (data[2] as usize) * 256 + (data[3] as usize))
		},
		_ => return None,	//Bad length.
	};
	*data = &data[len..];
	Some((plen, len))
}
