#![forbid(unsafe_code)]		//Hard requirement: Needs to be here.
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]
#[cfg(test)] #[macro_use] extern crate std;

mod base64;
pub use base64::base64pem;
pub use base64::base64url;
mod csr;
pub use csr::CsrParameters;
pub use csr::CsrPending;
mod jws;
pub use jws::UnsignedJws;
pub use jws::JwsKeyType;
mod pem;
pub use pem::Asn1Tlv;
pub use pem::PemDecoder;
pub use pem::read_pem;
mod signature;
pub use signature::verify_tls_sig;
pub use signature::verify_x509_sig;
pub use signature::VerifyTlsError;
pub use signature::VerifyX509Error;
mod x509;
pub use x509::certificate_subject_and_key;
pub use x509::CertificateConsistencyError;
pub use x509::check_certificate_consistent_with_csr;
pub use x509::ConstraintEntry;
pub use x509::generate_acme_certificate;
pub use x509::parse_x509v3_certificate;
pub use x509::parse_x509v3_csr;
pub use x509::SanEntry;
pub use x509::TrustAnchorSet;
pub use x509::X509v3Certificate;
pub use x509::X509v3ParseError;
pub use x509::verify_acme_authorization;
pub use x509::AcmeAuthError;
