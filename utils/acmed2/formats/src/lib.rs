//!acmed2 formats handling
#![forbid(unsafe_code)]		//Hard requirement: Needs to be here.
#![deny(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]
#[cfg(test)] #[macro_use] extern crate std;

///Generic parsing.
mod gparse;
pub use gparse::Asn1Tlv;
pub use gparse::GParse;
//JSON.
mod json;
pub use json::JsonNumber;
pub use json::JsonSValue;
pub use json::JsonValue;
//Ropes.
#[macro_use] mod rope;
pub use rope::asn1_bit_string;
pub use rope::asn1_csc;
pub use rope::asn1_csp;
pub use rope::asn1_ia5_string;
pub use rope::asn1_integer;
pub use rope::asn1_null;
pub use rope::asn1_object_identifier;
pub use rope::asn1_octet_string;
pub use rope::asn1_printable_string;
pub use rope::asn1_sequence;
pub use rope::asn1_set;
pub use rope::asn1_utf8_string;
pub use rope::Rope;
pub use rope::RopeArray;
pub use rope::RopeAsn1;
#[doc(hidden)] pub use rope::RopeConcat;	//Internal implementation detail of rope_concat!().
pub use rope::RopeDeref;
pub use rope::RopeIterator;
pub use rope::RopeOid;
pub use rope::RopeTlsExt;
pub use rope::RopeTlsMsg;
pub use rope::RopeVec8;
pub use rope::RopeVec16;
pub use rope::RopeVec24;
