use acmed2_memory as alloc;
use alloc::String;
use alloc::Vec;
use core::cmp::max;
use core::mem::size_of;
use core::ops::Deref;

///Trait: Rope.
pub trait Rope
{
	///Invoke closure on all parts of rope.
	fn call(&self, cb: &mut impl FnMut(&[u8]));
	///Get length of rope in bytes.
	fn length(&self) -> usize;
	///Transform rope into vector.
	fn as_vec(&self) -> Vec<u8>
	{
		let mut v = Vec::new();
		self.extend_vec(&mut v);
		v
	}
	///Extend vector with contents of rope.
	fn extend_vec(&self, v: &mut Vec<u8>)
	{
		self.call(&mut |y|v.extend_from_slice(y));
	}
}

impl Rope for ()
{
	fn call(&self, _: &mut impl FnMut(&[u8])) {}
	fn length(&self) -> usize { 0 }
}

impl<T:Rope,E:Rope> Rope for Result<T,E>
{
	fn call(&self, cb: &mut impl FnMut(&[u8]))
	{
		match self { Ok(x) => x.call(cb), Err(x) => x.call(cb) }
	}
	fn length(&self) -> usize { match self { Ok(x) => x.length(), Err(x) => x.length() } }
}

impl<T:Rope> Rope for Option<T>
{
	fn call(&self, cb: &mut impl FnMut(&[u8]))
	{
		match self { Some(x) => x.call(cb), None => () }
	}
	fn length(&self) -> usize { match self { Some(x) => x.length(), None => 0 } }
}

///Concatenation of two ropes.
#[derive(Clone)]
pub struct RopeConcat<T:Rope,U:Rope>(pub T, pub U);

impl<T:Rope,U:Rope> Rope for RopeConcat<T,U>
{
	fn call(&self, cb: &mut impl FnMut(&[u8]))
	{
		self.0.call(cb);
		self.1.call(cb);
	}
	fn length(&self) -> usize { self.0.length() + self.1.length() }
}

///Concatenate the given ropes.
#[macro_export]
macro_rules! rope_concat
{
	($a:expr) => { $a };
	($a:expr, $($b:expr),*) => { $crate::RopeConcat($a, rope_concat!($($b),*)) };
}

macro_rules! impl_integer
{
	($name:ident $len:tt) => {
		impl Rope for $name
		{
			fn call(&self, cb: &mut impl FnMut(&[u8])) { cb(&self.to_be_bytes()); }
			fn length(&self) -> usize { size_of::<$name>() }
		}
	}
}

impl_integer!(u8 1);
impl_integer!(u16 2);
impl_integer!(u32 4);
impl_integer!(u64 8);

impl<'a> Rope for &'a [u8]
{
	fn call(&self, cb: &mut impl FnMut(&[u8])) { cb(self); }
	fn length(&self) -> usize { self.len() }
}

impl<'a> Rope for &'a str
{
	fn call(&self, cb: &mut impl FnMut(&[u8])) { cb(self.as_bytes()); }
	fn length(&self) -> usize { self.len() }
}

impl<'a> Rope for &'a Vec<u8>
{
	fn call(&self, cb: &mut impl FnMut(&[u8])) { cb(self.deref()); }
	fn length(&self) -> usize { self.len() }
}

impl<'a> Rope for &'a String
{
	fn call(&self, cb: &mut impl FnMut(&[u8])) { cb(self.as_bytes()); }
	fn length(&self) -> usize { self.len() }
}

//Can not use AsRef<[u8]> because it can conflict.
pub trait ToBytes
{
	///Cast to byte slice.
	fn as_slice(&self) -> &[u8];
}

impl<T:ToBytes> Rope for T
{
	fn call(&self, cb: &mut impl FnMut(&[u8])) { cb(self.as_slice()); }
	fn length(&self) -> usize { self.as_slice().len() }
}

impl ToBytes for Vec<u8>
{
	fn as_slice(&self) -> &[u8] { self }
}

impl ToBytes for String
{
	fn as_slice(&self) -> &[u8] { self.as_bytes() }
}

impl ToBytes for [u8;12] { fn as_slice(&self) -> &[u8] { &self[..] } }
impl ToBytes for [u8;32] { fn as_slice(&self) -> &[u8] { &self[..] } }

///Rope: ASN.1 TLV.length
#[derive(Clone)]
pub struct RopeAsn1<T:Rope>(pub (u8, u64), pub T);

impl<T:Rope> Rope for RopeAsn1<T>
{
	fn call(&self, cb: &mut impl FnMut(&[u8]))
	{
		let tlen = self.1.length();
		let tag = self.0;
		if tag.1 < 31 {
			cb(&[tag.0 << 5 | tag.1 as u8]);
		} else {
			//Not great for performance, but this is expected to be very rare case.
			cb(&[tag.0 << 5 | 31]);
			RopeOid::__call(tag.1, cb);
		}
		match tlen {
			0..=127 => cb(&[tlen as u8]),
			128..=255 => cb(&[129, tlen as u8]),
			256..=65535 => cb(&[130, (tlen >> 8) as u8, tlen as u8]),
			//Not right for >=2^24, but that is not supported anyway.
			_ => cb(&[131, (tlen >> 16) as u8, (tlen >> 8) as u8, tlen as u8]),
		};
		self.1.call(cb);
	}
	fn length(&self) -> usize
	{
		let tlen = self.1.length();
		tlen + match tlen {
			0..=127 => 2,
			128..=255 => 3,
			256..=65535 => 4,
			//Not right for >=2^24, but that is not supported anyway.
			_ => 5,
		}
	}
}

///Rope: ASN.1 INTEGER
pub fn asn1_integer<T:Rope>(arg: T) -> RopeAsn1<T> { RopeAsn1((0,2),arg) }
///Rope: ASN.1 BIT STRING
pub fn asn1_bit_string<T:Rope>(strip: u8, arg: T) -> RopeAsn1<RopeConcat<u8,T>>
{
	RopeAsn1((0,3),RopeConcat(strip,arg))
}
///Rope: ASN.1 OCTET STRING
pub fn asn1_octet_string<T:Rope>(arg: T) -> RopeAsn1<T> { RopeAsn1((0,4),arg) }
///Rope: ASN.1 NULL.
pub fn asn1_null() -> RopeAsn1<()> { RopeAsn1((0,5),()) }
///Rope: ASN.1 OBJECT IDENTIFIER
pub fn asn1_object_identifier<'a>(arg: &'a[u64]) -> RopeAsn1<RopeOid<'a>> { RopeAsn1((0,6),RopeOid(arg)) }
///Rope: ASN.1 UTF-8 STRING
pub fn asn1_utf8_string<'a>(arg: &'a str) -> RopeAsn1<&'a str> { RopeAsn1((0,12),arg) }
///Rope: SEQUENCE.
pub fn asn1_sequence<T:Rope>(arg: T) -> RopeAsn1<T> { RopeAsn1((1,16),arg) }
///Rope: SET.
pub fn asn1_set<T:Rope>(arg: T) -> RopeAsn1<T> { RopeAsn1((1,17),arg) }
///Rope: ASN.1 PRINTABLE STRING
pub fn asn1_printable_string<'a>(arg: &'a str) -> RopeAsn1<&'a str> { RopeAsn1((0,19),arg) }
///Rope: ASN.1 IA5 STRING
pub fn asn1_ia5_string<'a>(arg: &'a str) -> RopeAsn1<&'a str> { RopeAsn1((0,22),arg) }
///Rope: Context-specific primitive.
pub fn asn1_csp<T:Rope>(tag: u64, arg: T) -> RopeAsn1<T> { RopeAsn1((4,tag),arg) }
///Rope: Context-specific constructed.
pub fn asn1_csc<T:Rope>(tag: u64, arg: T) -> RopeAsn1<T> { RopeAsn1((5,tag),arg) }

///Rope: TLS 8-bit vector.
#[derive(Clone)]
pub struct RopeVec8<T:Rope>(pub T);

impl<T:Rope> Rope for RopeVec8<T>
{
	fn call(&self, cb: &mut impl FnMut(&[u8]))
	{
		let tlen = self.0.length();
		cb(&[tlen as u8]);
		self.0.call(cb);
	}
	fn length(&self) -> usize { 1 + self.0.length() }
}

///Rope: TLS 16-bit vector.
#[derive(Clone)]
pub struct RopeVec16<T:Rope>(pub T);

impl<T:Rope> Rope for RopeVec16<T>
{
	fn call(&self, cb: &mut impl FnMut(&[u8]))
	{
		let tlen = self.0.length();
		cb(&[(tlen >> 8) as u8, tlen as u8]);
		self.0.call(cb);
	}
	fn length(&self) -> usize { 2 + self.0.length() }
}

///Rope: TLS 24-bit vector.
#[derive(Clone)]
pub struct RopeVec24<T:Rope>(pub T);

impl<T:Rope> Rope for RopeVec24<T>
{
	fn call(&self, cb: &mut impl FnMut(&[u8]))
	{
		let tlen = self.0.length();
		cb(&[(tlen >> 16) as u8, (tlen >> 8) as u8, tlen as u8]);
		self.0.call(cb);
	}
	fn length(&self) -> usize { 3 + self.0.length() }
}

///Rope: TLS Extension.
#[derive(Clone)]
pub struct RopeTlsExt<T:Rope>(pub u16, pub T);

impl<T:Rope> Rope for RopeTlsExt<T>
{
	fn call(&self, cb: &mut impl FnMut(&[u8]))
	{
		let tlen = self.1.length();
		cb(&[(self.0 >> 8) as u8, self.0 as u8, (tlen >> 8) as u8, tlen as u8]);
		self.1.call(cb);
	}
	fn length(&self) -> usize { 4 + self.1.length() }
}

///Rope: TLS Handshake message.
#[derive(Clone)]
pub struct RopeTlsMsg<T:Rope>(pub u8, pub T);

impl<T:Rope> Rope for RopeTlsMsg<T>
{
	fn call(&self, cb: &mut impl FnMut(&[u8]))
	{
		let tlen = self.1.length();
		cb(&[self.0 as u8, (tlen >> 16) as u8, (tlen >> 8) as u8, tlen as u8]);
		self.1.call(cb);
	}
	fn length(&self) -> usize { 4 + self.1.length() }
}

///Rope: Dereference.
#[derive(Clone)]
pub struct RopeDeref<'a, T:Rope>(pub &'a T);

impl<'a,T:Rope> Rope for RopeDeref<'a,T>
{
	fn call(&self, cb: &mut impl FnMut(&[u8])) { self.0.call(cb); }
	fn length(&self) -> usize { self.0.length() }
}

///Rope: Concatenate array of ropes.
#[derive(Clone)]
pub struct RopeArray<'a, T:Rope>(pub &'a [T]);

impl<'a,T:Rope> Rope for RopeArray<'a,T>
{
	fn call(&self, cb: &mut impl FnMut(&[u8])) { for item in self.0.iter() { item.call(cb); } }
	fn length(&self) -> usize { self.0.iter().fold(0,|size,item|size + item.length()) }
}

///Rope: Concatenate iterator of ropes.
#[derive(Clone)]
pub struct RopeIterator<T:Rope,I:Iterator<Item=T>+Clone>(pub I);

impl<T:Rope,I:Iterator<Item=T>+Clone> Rope for RopeIterator<T,I>
{
	fn call(&self, cb: &mut impl FnMut(&[u8])) { for item in self.0.clone() { item.call(cb); } }
	fn length(&self) -> usize { self.0.clone().fold(0,|size,item|size + item.length()) }
}


///Rope: OID.
#[derive(Clone)]
pub struct RopeOid<'a>(pub &'a [u64]);

impl<'a> RopeOid<'a>
{
	fn __octet(p: &mut u64, f: &mut bool, sh: u32, cb: &mut impl FnMut(&[u8]))
	{
		if *f || *p >= (1 << sh) { cb(&[128 + (*p >> sh) as u8 % 128]); *p %= 1 << sh; *f = true; }
	}
	fn __call(mut p: u64, cb: &mut impl FnMut(&[u8]))
	{
		let mut f = false;
		for i in 0..9 {
			assert!(63-7*i>0);
			Self::__octet(&mut p, &mut f, 63-7*i, cb);
		}
		cb(&[p as u8 % 128]);
	}
	fn __length(p: u64) -> usize
	{
		let z = p.leading_zeros();
		max(10 - (z + 6) / 7, 1) as usize
	}
	fn __split(&self) -> (u64, &'a [u64])
	{
		let a = self.0.get(0).map(|x|*x).unwrap_or(0);
		let b = self.0.get(1).map(|x|*x).unwrap_or(0);
		let f = 40u64.wrapping_mul(a).wrapping_add(b);
		let rest = if self.0.len() > 2 { &self.0[2..] } else { &[] };
		(f, rest)
	}
}

impl<'a> Rope for RopeOid<'a>
{
	fn call(&self, cb: &mut impl FnMut(&[u8]))
	{
		let (f, rest) = self.__split();
		Self::__call(f, cb);
		for p in rest.iter() { Self::__call(*p, cb); }
	}
	fn length(&self) -> usize
	{
		let (f, rest) = self.__split();
		Self::__length(f) + rest.iter().fold(0, |acc, p|acc + Self::__length(*p))
	}
}

#[cfg(test)]
mod test;
