use super::*;
use acmed2_cryptos::chacha20poly1305_encrypt;
use std::string::ToString;


fn assert_rope(content: &[u8], rope: impl Rope)
{
	let mut rope_content2 = Vec::new();
	rope.call(&mut |y|rope_content2.extend_from_slice(y));
	let rope_content = rope.as_vec();
	assert_eq!(rope.length(), content.length());
	assert_eq!(rope_content, content);
	assert_eq!(rope_content2, content);
}

#[test]
fn test_unit()
{
	assert_rope(&[], ());
}

#[test]
fn test_u8()
{
	assert_rope(&[42], 42u8);
}

#[test]
fn test_u16()
{
	assert_rope(&[0x12,0x34], 0x1234u16);
}

#[test]
fn test_u32()
{
	assert_rope(&[0x12,0x34,0x56,0x78], 0x12345678u32);
}

#[test]
fn test_u64()
{
	assert_rope(&[0x12,0x34,0x56,0x78,0x9a,0xbc,0xde,0xf0], 0x123456789abcdef0u64);
}

#[test]
fn test_result()
{
	let okay: Result<&str, &str> = Ok("Okay");
	let error: Result<&str, &str> = Err("Error");
	assert_rope(b"Okay", okay);
	assert_rope(b"Error", error);
}

#[test]
fn test_option()
{
	let present: Option<&str> = Some("Present");
	let absent: Option<&str> = None;
	assert_rope(b"Present", present);
	assert_rope(b"", absent);
}

#[test]
fn test_concat()
{
	assert_rope(b"abcdefghi", rope_concat!("abc", "def", "ghi"));
}

#[test]
fn test_slice_u8()
{
	assert_rope(b"abcdefghi", &b"abcdefghi"[..]);
}

#[test]
fn test_vec_u8()
{
	assert_rope(b"abcdefghi", b"abcdefghi".to_vec());
}

#[test]
fn test_slice_str()
{
	assert_rope(b"abcdefghi", "abcdefghi");
}

#[test]
fn test_string()
{
	assert_rope(b"abcdefghi", "abcdefghi".to_string());
}

#[test]
fn test_slice_special()
{
	assert_rope(b"0123456789ab", *b"0123456789ab");
	assert_rope(b"0123456789abcdef0123456789abcdef", *b"0123456789abcdef0123456789abcdef");
}

fn extend_with_crap(pfx: &[u8], len: usize) -> Vec<u8>
{
	let mut result = pfx.to_vec();
	result.resize(pfx.len() + len, 0);
	let mut iv = [0;12];
	(&mut iv[..8]).copy_from_slice(&(len as u64).to_le_bytes());
	chacha20poly1305_encrypt(&[0;32], &iv, &[], &mut result[pfx.len()..], &mut [0;16]);
	result
}

fn test_rope_asn1_with_crap(pfx: &[u8], len: usize)
{
	assert_rope(&extend_with_crap(pfx, len), RopeAsn1((0, 4), extend_with_crap(&[], len)));
}

#[test]
fn test_rope_asn1()
{
	assert_rope(&[48, 0], RopeAsn1((1, 16), ()));
	assert_rope(&[48, 1, 42], RopeAsn1((1, 16), 42u8));
	test_rope_asn1_with_crap(&[4, 127], 127);
	test_rope_asn1_with_crap(&[4, 129, 128], 128);
	test_rope_asn1_with_crap(&[4, 129, 255], 255);
	test_rope_asn1_with_crap(&[4, 130, 1, 0], 256);
	test_rope_asn1_with_crap(&[4, 130, 1, 2], 258);
	test_rope_asn1_with_crap(&[4, 130, 255, 255], 65535);
	test_rope_asn1_with_crap(&[4, 131, 1, 0, 0], 65536);
	test_rope_asn1_with_crap(&[4, 131, 1, 2, 3], 66051);
}

#[test]
fn test_rope_tlsvec_8()
{
	assert_rope(b"\x03abc", RopeVec8("abc"));
}

#[test]
fn test_rope_tlsvec_16()
{
	let l = 258; assert_rope(&extend_with_crap(&[1, 2], l), RopeVec16(extend_with_crap(&[], l)));
}

#[test]
fn test_rope_tlsvec_24()
{
	let l = 66051; assert_rope(&extend_with_crap(&[1, 2, 3], l), RopeVec24(extend_with_crap(&[], l)));
}

#[test]
fn test_rope_tlsext()
{
	let l = 258;
	assert_rope(&extend_with_crap(&[0x12, 0x34, 1, 2], l), RopeTlsExt(0x1234, extend_with_crap(&[], l)));
}

#[test]
fn test_rope_tlsmsg()
{
	let l = 66051;
	assert_rope(&extend_with_crap(&[42, 1, 2, 3], l), RopeTlsMsg(42, extend_with_crap(&[], l)));
}

#[test]
fn test_dereference()
{
	let v = b"abc".to_vec();
	assert_rope(b"abc", RopeDeref(&v));
}

#[test]
fn test_dereference2()
{
	let v = "abc".to_string();
	assert_rope(b"abc", RopeDeref(&v));
}

#[test]
fn test_array()
{
	assert_rope(&[42, 152, 211], RopeArray(&[42u8, 152u8, 211u8]));
}

#[test]
fn test_array_iter()
{
	assert_rope(&[0,42, 0,152, 0,211], RopeIterator([42u16, 152u16, 211u16].iter().cloned()));
}

fn itov(test: &[u8]) -> u64
{
	let mut v = 0u64;
	for b in test.iter() { v = v * 128 + (*b as u64) % 128; } 
	v
}

const TESTS: &'static [&'static [u8]] = &[
	&[0],
	&[127],
	&[129,0],
	&[129,2],
	&[255,127],
	&[129,128,0],
	&[129,130,3],
	&[255,255,127],
	&[129,128,128,0],
	&[129,130,131,4],
	&[255,255,255,127],
	&[129,128,128,128,0],
	&[129,130,131,132,5],
	&[255,255,255,255,127],
	&[129,128,128,128,128,0],
	&[129,130,131,132,133,6],
	&[255,255,255,255,255,127],
	&[129,128,128,128,128,128,0],
	&[129,130,131,132,133,134,7],
	&[255,255,255,255,255,255,127],
	&[129,128,128,128,128,128,128,0],
	&[129,130,131,132,133,134,135,8],
	&[255,255,255,255,255,255,255,127],
	&[129,128,128,128,128,128,128,128,0],
	&[129,130,131,132,133,134,135,136,9],
	&[255,255,255,255,255,255,255,255,127],
	&[129,128,128,128,128,128,128,128,128,0],
	&[129,130,131,132,133,134,135,136,137,10],
	&[129,255,255,255,255,255,255,255,255,127],
];

#[test]
fn test_rope_oid_f2()
{
	for test in TESTS.iter() {
		let v = itov(test);
		if v < 40 {
			assert_rope(test, RopeOid(&[0,v]));
		} else if v < 80 {
			assert_rope(test, RopeOid(&[1,v-40]));
		} else {
			assert_rope(test, RopeOid(&[2,v-80]));
		}
	}
}

fn concat(x: &[&[u8]]) -> Vec<u8>
{
	let mut v = Vec::new();
	for a in x.iter() { v.extend_from_slice(a); }
	v
}

#[test]
fn test_rope_oid_f3()
{
	for i in TESTS.iter() {
		assert_rope(&concat(&[&[42], i]), RopeOid(&[1,2,itov(i)]));
	}
}

#[test]
fn test_rope_oid_f4()
{
	for i in TESTS.iter() {
		for j in TESTS.iter() {
			assert_rope(&concat(&[&[42], i, j]), RopeOid(&[1,2,itov(i),itov(j)]));
		}
	}
}
