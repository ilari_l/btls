//!JSON
use acmed2_memory as alloc;
use alloc::BTreeMap;
use alloc::String;
use alloc::ToOwned;
use alloc::Vec;
use core::iter::repeat;

fn split_digits<'a>(x: &'a [u8]) -> (&'a [u8], &'a [u8])
{
	let mut count = 0;
	while count < x.len() && x[count].wrapping_sub(48) < 10 { count += 1; }
	//LLVM is too stupid to realize that count <= x.len() here. So do dummy check in order to avoid potential
	//panic.
	if count > x.len() { return (x, b""); }
	x.split_at(count)
}

#[derive(Copy,Clone,Debug)]
enum _JsonNumber
{
	Unsigned(u64),
	Signed(i64),
	Nan,	//No float support yet.
}

///JSON number.
#[derive(Copy,Clone,Debug)]
pub struct JsonNumber(_JsonNumber);

impl JsonNumber
{
	///Get the unsigned integer value of number if exactly representable.
	pub fn as_unsigned(&self) -> Option<u64>
	{
		match &self.0 {
			&_JsonNumber::Unsigned(x) => Some(x),
			//Unsigned is preferred presentation, so all the others are out-of-range.
			&_JsonNumber::Signed(_) => None,
			&_JsonNumber::Nan => None,
		}
	}
	///Get the signed integer value of number if exactly representable.
	pub fn as_signed(&self) -> Option<i64>
	{
		match &self.0 {
			//Note that only unsigned numbers < 2^63 are in range.
			&_JsonNumber::Unsigned(x) if x >> 63 == 0 => Some(x as i64),
			&_JsonNumber::Unsigned(_) => None,
			&_JsonNumber::Signed(x) => Some(x),
			//Float is depreferred presentation, so it is out-of-range.
			&_JsonNumber::Nan => None,
		}
	}
	fn parse(input: &str, ptr: &mut usize) -> Option<JsonNumber>
	{
		//Numbers correspond to regular expression -?(0|[1-9][0-9]*)(.[0-9]+)?([eE][+-]?[0-9]+)?
		//Save the original input pointer for computing final pointer, and cast input to byte array to
		//not worry about multibyte stuff.
		let mut __input = input;	//Save original input pointer for computing final pointer.
		let mut input = input.as_bytes().get(*ptr..)?;
		//Sign. If number starts with '-', it is negative, else positive.
		let sign = if input.len() > 0 && input[0] == b'-' {
			input = &input[1..];
			true
		} else {
			false
		};
		//Integral part.
		//Integral part is not allowed to be empty, or be anything but 0 if it starts with 0.
		let (integral, ninput) = split_digits(input); input = ninput;
		if integral == b"" || (integral.len() > 1 && integral[0] == b'0') {
			return None;
		}
		//If remainder starts with ., that is fractional part. It is not allowed to be empty.
		let fractional = if input.len() > 0 && input[0] == b'.' {
			input = &input[1..];		//Remove .
			let (tmp, ninput) = split_digits(input); input = ninput;
			if tmp == b"" { return None; }
			tmp
		} else {
			b""	//Take missing fractional part as empty.
		};
		//If remainder starts with e or E, that is exponential part. It is not allowed to be empty if
		//present.
		let (exp_sign, exponent) = if input.len() > 0 && input[0] & 0xdf == b'E' {
			input = &input[1..];	//Remove e/E
			//Handle sign of exponent.
			let tmp2 = if input.len() > 0 && input[0] == b'-' {
				input = &input[1..];		//Remove -
				true				//Negative
			} else if input.len() > 0 && input[0] == b'+' {
				input = &input[1..];		//Remove +
				false				//Positive.
			} else {
				false				//Positive (implicit sign).
			};
			let (tmp, ninput) = split_digits(input); input = ninput;
			if tmp == b"" { return None; }
			(tmp2, tmp)
		} else {
			(false, &b"0"[..])	//Take missing exponent to be +0.
		};
		//Compute the final pointer and floating-point parse. There are no accuracy guarantees, so take
		//result as 0 if parse fails.
		let eptr = input.as_ptr() as usize - __input.as_ptr() as usize;
		//This is the point of no return.
		*ptr = eptr;
		match __parse_json_number(sign, integral, fractional, exp_sign, exponent) {
			Some(x) => Some(JsonNumber(x)),
			//TODO: Parse floating-point.
			None => Some(JsonNumber(_JsonNumber::Nan)),
		}
	}
}

#[derive(Debug)]
enum JsonToken
{
	Primitive(JsonValue),
	LeftSB,
	RightSB,
	LeftCB,
	RightCB,
	Colon,
	Comma,
	EOF
}

fn __is_ws(b: &u8) -> bool { *b == 9 || *b == 10 || *b == 13 || *b == 32 }

impl JsonToken
{
	fn next_token(input: &str, ptr: &mut usize) -> Option<JsonToken>
	{
		//Skip whitespace.
		while input.as_bytes().get(*ptr).map(__is_ws).unwrap_or(false) {
			*ptr += 1;
		}
		let inputx = input.get(*ptr..)?;
		Some(match input.as_bytes().get(*ptr).map(|x|*x) {
			Some(34) => JsonToken::Primitive(JsonValue::String(__parse_json_string(input, ptr)?)),
			Some(44) => {*ptr+=1; JsonToken::Comma },
			Some(45|48..=57) => JsonToken::Primitive(JsonValue::Number(JsonNumber::parse(input, ptr)?)),
			Some(58) => {*ptr+=1; JsonToken::Colon },
			Some(91) => {*ptr+=1; JsonToken::LeftSB },
			Some(93) => {*ptr+=1; JsonToken::RightSB },
			Some(102) if inputx.starts_with("false") =>
				{ *ptr += 5; JsonToken::Primitive(JsonValue::Boolean(false)) },
			Some(110) if inputx.starts_with("null") =>
				{ *ptr += 4; JsonToken::Primitive(JsonValue::Null) },
			Some(116) if inputx.starts_with("true") =>
				{ *ptr += 4; JsonToken::Primitive(JsonValue::Boolean(true)) },
			Some(123) => {*ptr+=1; JsonToken::LeftCB },
			Some(125) => {*ptr+=1; JsonToken::RightCB },
			Some(_) => return None,	//All others invalid.
			None => JsonToken::EOF
		})
	}
}

#[derive(Debug)]
enum JsonState
{
	Initial,
	ArrayStart(Vec<JsonValue>),
	ArrayElement(Vec<JsonValue>),
	ArrayComma(Vec<JsonValue>),
	ObjectStart(BTreeMap<String, JsonValue>),
	ObjectName(BTreeMap<String, JsonValue>),
	ObjectColon(BTreeMap<String, JsonValue>, String),
	ObjectValue(BTreeMap<String, JsonValue>, String),
	ObjectComma(BTreeMap<String, JsonValue>),
}

enum JsonStackState
{
	Array(Vec<JsonValue>),
	Object(BTreeMap<String, JsonValue>, String),
}

fn __assert_end(input: &str, ptr: &mut usize) -> Option<()>
{
	match JsonToken::next_token(input, ptr)? {
		JsonToken::EOF => Some(()),
		_ => None
	}
}

///JSON value.
#[derive(Clone,Debug)]
pub enum JsonValue
{
	///NULL.
	Null,
	///Boolean.
	Boolean(bool),
	///Number.
	Number(JsonNumber),
	///String.
	String(String),
	///Array.
	Array(Vec<JsonValue>),
	///Object
	Object(BTreeMap<String, JsonValue>),
}

impl JsonValue
{
	///Return NULL value.
	pub fn null() -> JsonValue { JsonValue::Null }
	///Parse given document as JSON.
	pub fn parse(input: &str) -> Option<JsonValue>
	{
		use self::JsonState::*;
		use self::JsonToken::*;
		let mut state = JsonState::Initial;
		let mut ptr = 0;
		let mut stack: Vec<JsonStackState> = Vec::new();
		loop {
			state = match JsonToken::next_token(input, &mut ptr)? {
				//Primitive value can appear in the following states:
				//Initial => Return value at top level,
				//ArrayStart|ArrayElement => Push value, go to ArrayComma,
				//ObjectStart (string only),
				//ObjectName (string only),
				//ObjectValue,
				Primitive(v) => match state {
					Initial => {
						__assert_end(input, &mut ptr)?;
						return Some(v);
					},
					ArrayStart(mut arr)|ArrayElement(mut arr) => {
						arr.push(v);
						ArrayComma(arr)
					},
					ObjectStart(obj)|ObjectName(obj) => if let JsonValue::String(key) = v {
						ObjectColon(obj, key)
					} else {
						return None;	//Needs to be string.
					},
					ObjectValue(mut obj, key) => {
						obj.insert(key, v);
						ObjectComma(obj)
					},
					_ => return None	//Illegal here.
				},
				//Left SB can appear in the following states:
				//Initial,
				//ArrayStart,
				//ArrayElement,
				//ObjectValue
				//
				//In all cases, go to ArrayStart, possibly pushing state.
				LeftSB => match state {
					Initial => ArrayStart(Vec::new()),
					ArrayStart(arr)|ArrayElement(arr) => {
						stack.push(JsonStackState::Array(arr));
						ArrayStart(Vec::new())
					},
					ObjectValue(obj, key) => {
						stack.push(JsonStackState::Object(obj, key));
						ArrayStart(Vec::new())
					},
					_ => return None	//Illegal here.
				},
				//Right SB can appear in the following states:
				//ArrayStart,
				//ArrayComma.
				//
				//In all cases, write up.
				RightSB => match state {
					ArrayStart(arr)|ArrayComma(arr) => {
						let arr = JsonValue::Array(arr);
						match stack.pop() {
							Some(JsonStackState::Array(mut parr)) => {
								parr.push(arr);
								JsonState::ArrayComma(parr)
							},
							Some(JsonStackState::Object(mut pobj, pkey)) => {
								pobj.insert(pkey, arr);
								JsonState::ObjectComma(pobj)
							},
							None => {
								__assert_end(input, &mut ptr)?;
								return Some(arr);
							}
						}
					},
					_ => return None	//Illegal here.
				},
				//Left CB can appear in the following states:
				//Initial,
				//ArrayStart,
				//ArrayElement,
				//ObjectValue
				//
				//In all cases, go to ObjectStart, possibly pushing state.
				LeftCB => match state {
					Initial => ObjectStart(BTreeMap::new()),
					ArrayStart(arr)|ArrayElement(arr) => {
						stack.push(JsonStackState::Array(arr));
						ObjectStart(BTreeMap::new())
					},
					ObjectValue(obj, key) => {
						stack.push(JsonStackState::Object(obj, key));
						ObjectStart(BTreeMap::new())
					},
					_ => return None	//Illegal here.
				},
				//Right CB can appear in the following states:
				//ObjectStart,
				//ObjectComma.
				//
				//In all cases, write up.
				RightCB => match state {
					ObjectStart(obj)|ObjectComma(obj) => {
						let obj = JsonValue::Object(obj);
						match stack.pop() {
							Some(JsonStackState::Array(mut parr)) => {
								parr.push(obj);
								JsonState::ArrayComma(parr)
							},
							Some(JsonStackState::Object(mut pobj, pkey)) => {
								pobj.insert(pkey, obj);
								JsonState::ObjectComma(pobj)
							},
							None => {
								__assert_end(input, &mut ptr)?;
								return Some(obj);
							}
						}
					},
					_ => return None	//Illegal here.
				},
				//Colon can appear only in ObjectComma.
				Colon => match state {
					ObjectColon(obj, key) => ObjectValue(obj, key),
					_ => return None	//Illegal here.
				},
				//Comma can appear in the following states:
				//ArrayComma,
				//ObjectComma,
				//
				//Go to next state.
				Comma => match state {
					ArrayComma(arr) => ArrayElement(arr),
					ObjectComma(obj) => ObjectName(obj),
					_ => return None	//Illegal here.
				},
				//EOF is always illegal.
				EOF => return None
			};
		}
	}
	///Cast JSON value to null if possible.
	pub fn as_null(&self) -> Option<()>
	{
		if let &JsonValue::Null = self { Some(()) } else { None }
	}
	///Cast JSON value to boolean if possible.
	pub fn as_boolean(&self) -> Option<bool>
	{
		if let &JsonValue::Boolean(x) = self { Some(x) } else { None }
	}
	///Cast JSON value to number if possible.
	pub fn as_number(&self) -> Option<JsonNumber>
	{
		if let &JsonValue::Number(x) = self { Some(x) } else { None }
	}
	///Cast JSON value to string if possible.
	pub fn as_string<'a>(&'a self) -> Option<&'a str>
	{
		if let &JsonValue::String(ref x) = self { Some(x) } else { None }
	}
	///Cast JSON value to array if possible.
	pub fn as_array<'a>(&'a self) -> Option<&'a [JsonValue]>
	{
		if let &JsonValue::Array(ref x) = self { Some(x) } else { None }
	}
	///Cast JSON value to object if possible.
	pub fn as_object<'a>(&'a self) -> Option<&'a BTreeMap<String, JsonValue>>
	{
		if let &JsonValue::Object(ref x) = self { Some(x) } else { None }
	}
	///Get number of elements in this array. Returns 0 if not an array.
	pub fn array_len(&self) -> usize
	{
		if let &JsonValue::Array(ref x) = self { x.len() } else { 0 }
	}
	///Get specified element of this value as array. Returns None if not array or out of range.
	pub fn array_get(&self, index: usize) -> Option<&JsonValue>
	{
		if let &JsonValue::Array(ref x) = self { x.get(index) } else { None }
	}
	///Get iterator over elements of this value as array.
	pub fn array_iter<'a>(&'a self) -> impl Iterator<Item=&'a JsonValue>
	{
		if let &JsonValue::Array(ref x) = self {
			x.iter()
		} else {
			static X: &'static [JsonValue] = &[];
			X.iter()
		}
	}
	///Read specified key of this value as object.
	pub fn object_get(&self, key: &str) -> Option<&JsonValue>
	{
		if let &JsonValue::Object(ref x) = self { x.get(key) } else { None }
	}
	///Check if this value is object with specified key.
	pub fn object_has_key(&self, key: &str) -> bool
	{
		if let &JsonValue::Object(ref x) = self { x.contains_key(key) } else { false }
	}
	///Cast JSON value to unsigned integer if possible.
	pub fn as_unsigned(&self) -> Option<u64>
	{
		if let &JsonValue::Number(x) = self { x.as_unsigned() } else { None }
	}
	///Cast JSON value to signed integer if possible.
	pub fn as_signed(&self) -> Option<i64>
	{
		if let &JsonValue::Number(x) = self { x.as_signed() } else { None }
	}
}

///JSON value that can be serialized.
#[derive(Clone,Debug)]
pub enum JsonSValue
{
	///NULL.
	Null,
	///Boolean.
	Boolean(bool),
	///Number.
	Number(i64),
	///String.
	String(String),
	///Array.
	Array(Vec<JsonSValue>),
	///Object
	Object(BTreeMap<String, JsonSValue>),
}

impl From<()> for JsonSValue
{
	fn from(_: ()) -> JsonSValue { JsonSValue::Null }
}

impl From<bool> for JsonSValue
{
	fn from(x: bool) -> JsonSValue { JsonSValue::Boolean(x) }
}

impl From<i64> for JsonSValue
{
	fn from(x: i64) -> JsonSValue { JsonSValue::Number(x) }
}

impl<'a> From<&'a str> for JsonSValue
{
	fn from(x: &'a str) -> JsonSValue { JsonSValue::String(x.to_owned()) }
}

impl From<String> for JsonSValue
{
	fn from(x: String) -> JsonSValue { JsonSValue::String(x) }
}

impl<'a> From<&'a [JsonSValue]> for JsonSValue
{
	fn from(x: &'a [JsonSValue]) -> JsonSValue { JsonSValue::Array(x.to_owned()) }
}

impl<'a> From<&'a [(String, JsonSValue)]> for JsonSValue
{
	fn from(x: &'a [(String, JsonSValue)]) -> JsonSValue { JsonSValue::Object(x.iter().cloned().collect()) }
}

impl<'a,'b> From<&'a [(&'b str, JsonSValue)]> for JsonSValue
{
	fn from(x: &'a [(&'b str, JsonSValue)]) -> JsonSValue
	{
		JsonSValue::Object(x.iter().map(|(x,y)|((*x).to_owned(),y.clone())).collect())
	}
}

impl JsonSValue
{
	///Serialize JSON value into string.
	pub fn as_string(&self) -> String
	{
		let mut s = String::new();
		self.__as_string(&mut s);
		s
	}
	fn __as_string(&self, s: &mut String)
	{
		use core::fmt::Write;
		match self {
			&JsonSValue::Null => s.push_str("null"),
			&JsonSValue::Boolean(false) => s.push_str("false"),
			&JsonSValue::Boolean(true) => s.push_str("true"),
			&JsonSValue::Number(num) => {write!(s, "{num}").ok();},
			&JsonSValue::String(ref string) => __serialize_string(s, string),
			&JsonSValue::Array(ref arr) => {
				s.push('[');
				for (i,e) in arr.iter().enumerate() {
					if i != 0 { s.push(','); }
					e.__as_string(s);
				}
				s.push(']');
			},
			&JsonSValue::Object(ref obj) => {
				s.push('{');
				for (i,(k,v)) in obj.iter().enumerate() {
					if i != 0 { s.push(','); }
					__serialize_string(s, k);
					s.push(':');
					v.__as_string(s);
				}
				s.push('}');
			},
		}
	}
}

fn __serialize_string(s: &mut String, v: &str)
{
	use core::fmt::Write;
	s.push('\"');
	for c in v.chars() {
		match c as u32 {
			8 => s.push_str("\\b"),
			9 => s.push_str("\\t"),
			10 => s.push_str("\\n"),
			12 => s.push_str("\\f"),
			13 => s.push_str("\\r"),
			0..=7|11|14..=31 => {write!(s, "\\u{cp:04x}", cp=c as u32).ok();},
			34 => s.push_str("\\\""),
			92 => s.push_str("\\\\"),
			_ => s.push(c)
		}
	}
	s.push('\"');
}

//pub fn test_float_conversion(x: &str) -> Option<f64> { f64::from_str(x).ok() }

fn hexval(c: char) -> Option<u32>
{
	match c as u32 {
		x@48..=57 => Some(x-48),
		x@65..=70 => Some(x-55),
		x@97..=102 => Some(x-87),
		_ => None
	}
}

fn __parse_json_string(input: &str, ptr: &mut usize) -> Option<String>
{
	//Cut beginning of input.
	let input: &str = input.get(*ptr..)?;
	//Now, should start with ".
	let mut citr = input.char_indices();
	match citr.next() { Some((_,'\"')) => (), _ => return None };
	//Loop over other characters.
	let mut escape = 0u32;
	let mut found_q = false;
	let mut string = String::new();
	while let Some((_, character)) = citr.next() {
		match escape {
			0 => {
				//Not escape.
				match character {
					'\"' => {
						//The dquote that ends the string.
						found_q = true;
						break;
					},
					'\\' => {
						//Start of escape.
						escape = 1;
					},
					c if c as u32 >= 32 => string.push(c),	//Normal character.
					_ => return None			//Illegal control character.
				}
			},
			1 => {
				//Immediately after \.
				match character {
					'\"' => { string.push('\"'); escape = 0; },
					'\\' => { string.push('\\'); escape = 0; },
					'/' => { string.push('/'); escape = 0; },
					'b' => { string.push('\x08'); escape = 0; },
					't' => { string.push('\x09'); escape = 0; },
					'n' => { string.push('\x0a'); escape = 0; },
					'f' => { string.push('\x0c'); escape = 0; },
					'r' => { string.push('\x0d'); escape = 0; },
					'u' => escape = 2,
					_ => return None			//Illegal escape.
				}
			},
			//Immediately after \u.
			2 => escape = 0x10 + hexval(character)?,
			//Immediately after \u<hex>.
			0x10..=0x1F => escape = 16 * escape + hexval(character)?,
			//Immediately after \u<hex><hex>.
			0x100..=0x1FF => escape = 16 * escape + hexval(character)?,
			//Immediately after \u<hex><hex><hex>. Note that 0x1DC0..0x1DFF is invalid, and
			//1D80..1DBF is lower surrogates.
			0x1000..=0x1D7F|0x1E00..=0x1FFF => {
				//Normal hex escape.
				string.push(char::from_u32(16 * escape + hexval(character)? - 0x10000)?);
				escape = 0;
			},
			0x1D80..=0x1DBF => escape = 16 * escape + hexval(character)? - 0x1B800,
			//Immediately after \u<lowersurrogate>.
			0x2000..=0x23FF => {
				if character != '\\' { return None; }
				escape += 0x400;
			},
			//Immediately after \u<lowersurrogate>\.
			0x2400..=0x27FF => {
				if character != 'u' { return None; }
				escape += 0x400;
			},
			//Immediately after \u<lowersurrogate>\u.
			0x2800..=0x2BFF => {
				if hexval(character)? != 13 { return None; }
				escape += 0x400;
			},
			//Immediately after \u<lowersurrogate>\ud.
			0x2C00..=0x2FFF => {
				let hval = hexval(character)?;
				if hval < 12 { return None; }
				escape = 4 * escape + hval - 0x800C;
			},
			//Immediately after \u<lowersurrogate>\ud<hex>.
			0x3000..=0x3FFF => escape = 16 * escape + hexval(character)? - 0x20000,
			//Immediately after \u<lowersurrogate>\ud<hex><hex>.
			0x10000..=0x1FFFF => {
				//Double hex escape.
				string.push(char::from_u32(16 * escape + hexval(character)? - 0xF0000)?);
				escape = 0;
			},
			//Any other is Illegal.
			_ => return None,
		}
	}
	if !found_q { return None; }	//No terminating ".
	//Next position. If at end of string, 
	let npos = citr.next().map(|(p,_)|p).unwrap_or(input.len());
	*ptr += npos; 
	Some(string)
}

fn __parse_json_number(sign: bool, integral: &[u8], mut fractional: &[u8], exp_sign: bool,
	exponent: &[u8]) -> Option<_JsonNumber>
{
	//Strip any trailing zeroes in fractional part.
	while fractional.len() > 0 && fractional[fractional.len()-1] == b'0' {
		fractional = &fractional[..fractional.len()-1];
	}
	//Now, if integral == "0" && fractional == "", this is zero, independent of exponent.
	if integral == b"0" && fractional == b"" { return Some(_JsonNumber::Unsigned(0)); }
	//Parse the exponent part. If this is too big, there is no way the number is an integer, as it is either
	//huge, or exponent is too large to be compensated.
	let mut _exponent = 0usize;
	for c in exponent.iter() {
		_exponent = _exponent.checked_mul(10)?.checked_add(c.wrapping_sub(48) as usize)?;
	}
	let val = if exp_sign {
		//Fractional part must be empty, and integral part must have at least exponent
		//trailing zeroes.
		if fractional.len() > 0 || _exponent > integral.len() { return None; }
		let (integral, tz) = integral.split_at(integral.len() - _exponent);
		if tz.iter().any(|c|*c != b'0') { return None; }
		let mut val = 0u64;
		for c in integral.iter().cloned() {
			val = val.checked_mul(10)?.checked_add(c.wrapping_sub(48) as u64)?;
		}
		val
	} else {
		//Exponent is positive. Integer if fractional.len() <= exponent.
		let trailing_zeroes = _exponent.checked_sub(fractional.len())?;
		//Parse integer part, fractional part and trailing zeroes.
		let mut val = 0u64;
		for c in integral.iter().cloned().chain(fractional.iter().cloned()).
			chain(repeat(b'0').take(trailing_zeroes)) {
			val = val.checked_mul(10)?.checked_add(c.wrapping_sub(48) as u64)?;
		}
		val
	};
	if sign {
		if val <= 1u64 << 63 {
			let v2 = (!val).wrapping_add(1);
			Some(_JsonNumber::Signed(v2 as i64))
		} else {
			None	//Out of range.
		}
	} else {
		Some(_JsonNumber::Unsigned(val))
	}
}

#[cfg(test)]
fn assert_json_parse_u(sval: &str, cut: u64)
{
	let pval = JsonNumber::parse(sval, &mut 0).unwrap_or_else(||{
		panic!("JSON parse error on '{sval}'")
	});
	let nval = pval.as_unsigned().unwrap_or_else(||{
		panic!("JSON '{sval}' not unsigned")
	});
	if nval != cut { panic!("Test error on '{sval}': {nval} != {cut}"); }
}

#[test]
fn test_json_integers_basic()
{
	use core::fmt::Write;
	let cuts = [0, 1, 9, 10, 99, 100, 999, 1000, 9999, 10000, 99999, 100000, 999999, 1000000, 9999999,
		10000000, 99999999, 100000000, 999999999, 1000000000, 9999999999, 10000000000, 99999999999,
		100000000000, 999999999999, 1000000000000, 9999999999999, 10000000000000, 99999999999999,
		100000000000000, 999999999999999, 1000000000000000, 9999999999999999, 10000000000000000,
		99999999999999999, 100000000000000000, 999999999999999999, 1000000000000000000, 9999999999999999999,
		10000000000000000000, 18446744073709551615];
	for cut in cuts.iter() {
		for exp in -25..25 {
			let mut sval = alloc::String::new();
			write!(sval, "{cut}").ok();
			if exp == 0 {
			} else if exp > 0 {
				let uexp = exp as usize;
				sval = if uexp >= sval.len() {
					let z = uexp - sval.len();
					let mut sval2 = alloc::String::new();
					sval2.push_str("0.");
					for _ in 0..z { sval2.push_str("0"); }
					sval2.push_str(&sval);
					sval2
				} else {
					let mut sval2 = alloc::String::new();
					let sp = sval.len() - uexp;
					write!(sval2, "{head}.{tail}", head=&sval[..sp], tail=&sval[sp..]).ok();
					sval2
				};
				write!(sval, "e{exp}").ok();
			} else {
				//Zeropad and exponent.
				if *cut != 0 { for _ in 0..-exp { sval.push_str("0"); } }
				write!(sval, "e{exp}").ok();
			}
			assert_json_parse_u(&sval, *cut);
		}
		for exp in 1..25 {
			let mut sval = alloc::String::new();
			write!(sval, "{cut}").ok();
			let uexp = exp as usize;
			if uexp >= sval.len() { continue; }	//Has decimal part.
			let mut sval2 = alloc::String::new();
			let sp = sval.len() - uexp;
			if sval[sp..].chars().any(|z|z != '0') { continue; }	//Has nonzero decimal part.
			write!(sval2, "{mantissa}e{exp}", mantissa=&sval[..sp]).ok();
			assert_json_parse_u(&sval2, *cut);
		}
	}
}

#[test]
fn json_integers()
{
	use core::str::FromStr;
	let input = include_str!("json-number-tests.dat");
	for line in input.lines() {
		if line.starts_with("#") || line == "" { continue; }
		let parts: alloc::Vec<_> = line.split_whitespace().collect();
		if parts[0] == "unsigned" {
			let val1 = parts[1];
			let val2 = u64::from_str(parts[2]).expect("Parse error on numeric value");
			let val3 = match JsonNumber::parse(val1, &mut 0) {
				Some(v) => v,
				None => panic!("JSON parse error on '{val1}'")
			};
			let val3 = match val3.as_unsigned() {
				Some(v) => v,
				None => panic!("JSON '{val1}' is not unsigned")
			};
			if val3 != val2 { panic!("Test error on '{val1}': {val3} != {val2}"); }
		} else if parts[0] == "notunsigned" {
			let val1 = parts[1];
			let val3 = JsonNumber::parse(val1, &mut 0).
				unwrap_or_else(||panic!("JSON parse error on '{val1}'"));
			if val3.as_unsigned().is_some() {
				panic!("JSON '{val1}' should not be unsigned");
			}
		} else if parts[0] == "signed" {
			let val1 = parts[1];
			let val2 = i64::from_str(parts[2]).expect("Parse error on numeric value");
			let val3 = match JsonNumber::parse(val1, &mut 0) {
				Some(v) => v,
				None => panic!("JSON parse error on '{val1}'")
			};
			let val3 = match val3.as_signed() {
				Some(v) => v,
				None => panic!("JSON '{val1}' is not signed")
			};
			if val3 != val2 { panic!("Test error on '{val1}': {val3} != {val2}"); }
		} else if parts[0] == "notsigned" {
			let val1 = parts[1];
			let val3 = JsonNumber::parse(val1, &mut 0).
				unwrap_or_else(||panic!("JSON parse error on '{val1}'"));
			if val3.as_signed().is_some() {
				panic!("JSON '{val1}' should not be signed");
			}
		} else {
			panic!("Unknown test type '{type}'", type=parts[0]);
		}
	}
}

#[cfg(test)]
fn complete_and_test(s: &mut String, r: &mut String)
{
	use core::ops::Deref;
	let mut p = 0;
	s.push('\"');
	let x = __parse_json_string(&s, &mut p).unwrap_or_else(||panic!("Parsing '{s}' failed"));
	assert_eq!(p, s.len());
	assert_eq!(x.deref(), r.deref());
	s.clear();
	r.clear();
	s.push('\"');
}

#[test]
fn test_json_parse_string_basic()
{
	let mut s = String::new();
	let mut r = String::new();
	s.push('\"');
	for i in 32u32..0x10FFFFu32 {
		if i == 34 || i == 92 || (i >= 0xD800 && i <= 0xDFFF) { continue; }
		s.push(char::from_u32(i).unwrap());
		r.push(char::from_u32(i).unwrap());
		if s.len() > 1024 { complete_and_test(&mut s, &mut r); }
	}
	if r.len() > 0 { complete_and_test(&mut s, &mut r); }
}

#[test]
fn test_json_parse_string_escapes()
{
	use core::fmt::Write;
	let mut s = String::new();
	let mut r = String::new();
	s.push('\"');
	for i in 0u32..0x10FFFFu32 {
		if i >= 0xD800 && i <= 0xDFFF { continue; }
		if i <= 0xFFFF {
			write!(s, "\\u{i:04x}").ok();
		} else {
			write!(s, "\\u{ls:04x}\\u{hs:04x}", ls=i/1024+0xD7C0, hs=i%1024+0xDC00).ok();
		}
		r.push(char::from_u32(i).unwrap());
		if s.len() > 1024 { complete_and_test(&mut s, &mut r); }
	}
	if r.len() > 0 { complete_and_test(&mut s, &mut r); }
}

#[test]
fn test_json_parse_string_escapes_uc()
{
	use core::fmt::Write;
	let mut s = String::new();
	let mut r = String::new();
	s.push('\"');
	for i in 0u32..0x10FFFFu32 {
		if i >= 0xD800 && i <= 0xDFFF { continue; }
		if i <= 0xFFFF {
			write!(s, "\\u{i:04X}").ok();
		} else {
			write!(s, "\\u{ls:04X}\\u{hs:04X}", ls=i/1024+0xD7C0, hs=i%1024+0xDC00).ok();
		}
		r.push(char::from_u32(i).unwrap());
		if s.len() > 1024 { complete_and_test(&mut s, &mut r); }
	}
	if r.len() > 0 { complete_and_test(&mut s, &mut r); }
}

#[test]
fn test_json_parse_std_escapes()
{
	use core::ops::Deref;
	let s = "\"\\b\\f\\n\\r\\t\"";
	let r = "\x08\x0c\x0a\x0d\x09";
	let mut p = 0;
	let x = __parse_json_string(&s, &mut p).unwrap_or_else(||panic!("Parsing '{s}' failed"));
	assert_eq!(p, s.len());
	assert_eq!(x.deref(), r);
}

#[test]
fn test_json_null()
{
	assert!(JsonValue::parse("null").expect("Parse error").as_null().is_some());
}

#[test]
fn test_json_false()
{
	assert!(JsonValue::parse("false").expect("Parse error").as_boolean() == Some(false));
}

#[test]
fn test_json_true()
{
	assert!(JsonValue::parse("true").expect("Parse error").as_boolean() == Some(true));
}

#[test]
fn test_json_nontrivial()
{
	let json = JsonValue::parse("[true,null,[],{},[42],{\"foo\":false,\"bar\":[\"x\",\"y\"]}]").
		expect("Parse failed");
	let json = json.as_array().expect("Not array");
	assert!(json.len() == 6);
	assert!(json[0].as_boolean().expect("Not boolean") == true);
	assert!(json[1].as_null().expect("Not null") == ());
	assert!(json[2].as_array().expect("Not array").len() == 0);
	assert!(json[3].as_object().expect("Not object").len() == 0);
	let arr = json[4].as_array().expect("Not array");
	let obj = json[5].as_object().expect("Not object");
	assert!(arr.len() == 1);
	assert!(arr[0].as_number().expect("Not number").as_unsigned().expect("Not unsigned") == 42);
	assert!(obj.len() == 2);
	let a = obj.get("foo").expect("No foo");
	let b = obj.get("bar").expect("No bar").as_array().expect("Not array");
	assert!(a.as_boolean().expect("Not boolean") == false);
	assert!(b.len() == 2);
	assert!(b[0].as_string().expect("Not string") == "x");
	assert!(b[1].as_string().expect("Not string") == "y");
}

#[test]
fn serialize_json()
{
	let json = JsonSValue::from(&[
		JsonSValue::from(()),
		JsonSValue::from(false),
		JsonSValue::from(true),
		JsonSValue::from(42i64),
		JsonSValue::from("foo"),
		JsonSValue::from("bar".to_owned()),
		JsonSValue::from(&[
			JsonSValue::from("\x01\x1F\"\\"),
		][..]),
		JsonSValue::from(&[
			("x",JsonSValue::from(false)),
			("y",JsonSValue::from(true)),
		][..]),
	][..]);
	assert_eq!(json.as_string(), "[null,false,true,42,\"foo\",\"bar\",[\"\\u0001\\u001f\\\"\\\\\"],\
		{\"x\":false,\"y\":true}]");
}
