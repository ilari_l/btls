use core::cmp::min;
use core::fmt::Debug;
use core::fmt::Error as FmtError;
use core::fmt::Formatter;
use core::mem::replace;


///Parsed ASN.1 TLV
#[derive(Clone)]
pub struct Asn1Tlv<'a>
{
	///Tag. First element is the upper 3 type bits.
	///
	///E.g., SEQUENCE is (1, 16), as SEQUENCE is constructed type with universal tag 16.
	pub tag: (u8, u64),
	///Value.
	pub value: &'a [u8],
	///Value as GParse.
	pub gparse: GParse<'a>,
	///The entiere tag.
	pub entiere: &'a [u8],
}

///Generic stream parser.
#[derive(Clone)]
pub struct GParse<'a>(&'a [u8], usize);

impl<'a> Debug for GParse<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		let remainder = &self.0[min(self.0.len(),self.1)..];
		f.write_str("GParse[")?;
		for b in remainder.iter() { write!(f, "{b:02x}")?; }
		f.write_str("]")
	}
}

macro_rules! slice_x
{
	($selfx:ident $func:ident) => {{
		let ptrsave = $selfx.1;
		match $selfx.$func().and_then(|len|$selfx.slice(len as usize)) {
			Some(x) => Some(x),
			None => {$selfx.1 = ptrsave; None},	//Undo advance on failure.
		}
	}}
}

macro_rules! iprim
{
	($selfx:ident $ptype:ident $len:tt) => {{
		let s = $selfx.slice($len)?;
		let mut t = [0;$len];
		t.copy_from_slice(s);
		Some($ptype::from_be_bytes(t))
	}}
}

pub trait Try
{
	fn is_err(&self) -> bool;
	fn ok() -> Self;
}

impl Try for Option<()>
{
	fn is_err(&self) -> bool { self.is_none() }
	fn ok() -> Self { Some(()) }
}

impl<E:Sized> Try for Result<(),E>
{
	fn is_err(&self) -> bool { self.is_err() }
	fn ok() -> Self { Ok(()) }
}


impl<'a> GParse<'a>
{
	///Create new parser parsing `x`.
	pub fn new(x: &'a [u8]) -> GParse<'a> { GParse(x, 0) }
	///There is more data to come?
	pub fn more(&self) -> bool { self.1 < self.0.len() }
	///Get entiere thing as slice.
	pub fn as_slice(&self) -> &'a [u8] { self.0 }
	///Read remainder
	pub fn remainder(&mut self) -> &'a [u8]
	{
		let p = replace(&mut self.1, self.0.len());
		&self.0[min(p,self.0.len())..]
	}
	///Parse a uint8.
	pub fn u8(&mut self) -> Option<u8> { iprim!(self u8 1) }
	///Parse a uint16.
	pub fn u16(&mut self) -> Option<u16> { iprim!(self u16 2) }
	///Parse a uint32.
	pub fn u32(&mut self) -> Option<u32> { iprim!(self u32 4) }
	///Parse a uint64.
	pub fn u64(&mut self) -> Option<u64> { iprim!(self u64 8) }
	///Parse a uint24.
	pub fn u24(&mut self) -> Option<u32>
	{
		let s = self.slice(3)?;
		let mut t = [0;4];
		(&mut t[1..]).copy_from_slice(s);
		Some(u32::from_be_bytes(t))
	}
	///Parse a slice with fixed length.
	pub fn slice(&mut self, len: usize) -> Option<&'a [u8]>
	{
		let s = self.0.get(self.1..self.1+len)?; self.1 += len; Some(s)
	}
	///Parse a slice with u8 length.
	pub fn slice_u8(&mut self) -> Option<&'a [u8]> { slice_x!(self u8) }
	///Parse a slice with u16 length.
	pub fn slice_u16(&mut self) -> Option<&'a [u8]> { slice_x!(self u16) }
	///Parse a slice with u24 length.
	pub fn slice_u24(&mut self) -> Option<&'a [u8]> { slice_x!(self u24) }
	///Parse a gparse with fixed length.
	pub fn gparse(&mut self, len: usize) -> Option<GParse<'a>> { self.slice(len).map(|s|GParse::new(s)) }
	///Parse a gparse with u8 length.
	pub fn gparse_u8(&mut self) -> Option<GParse<'a>> { self.slice_u8().map(|s|GParse::new(s)) }
	///Parse a gparse with u16 length.
	pub fn gparse_u16(&mut self) -> Option<GParse<'a>> { self.slice_u16().map(|s|GParse::new(s)) }
	///Parse a gparse with u24 length.
	pub fn gparse_u24(&mut self) -> Option<GParse<'a>> { self.slice_u24().map(|s|GParse::new(s)) }
	fn __asn1(&mut self) -> Option<Asn1Tlv<'a>>
	{
		let start = self.1;
		let mtag = self.u8()?;
		let tag = if mtag & 31 == 31 {
			//Extended tag
			let mut etag = 0u64;
			loop {
				let t = self.u8()?;
				etag = etag.checked_mul(128)?.checked_add(t as u64 & 127)?;
				if t & 128 == 0 { break; }	//End of tag.
			}
			if etag < 31 { return None; }		//Bad tag.
			(mtag >> 5, etag)
		} else {
			//Basic tag.
			(mtag >> 5, mtag as u64 & 31)
		};
		//Length.
		let len = match self.u8()? {
			l@0..=127 => l as usize,	//Length in length of length byte.
			129 => {			//1 byte length.
				let l = self.u8()?;
				if l < 128 { return None; }	//Bad length.
				l as usize
			},
			130 => {			//2 byte length.
				let l = self.u16()?;
				if l < 256 { return None; }	//Bad length.
				l as usize
			},
			131 => {			//3 byte length.
				let l = self.u24()?;
				if l < 65536 { return None; }	//Bad length.
				l as usize
			},
			_ => return None,		//Length too big or invalid.
		};
		//Value.
		let gparse = self.gparse(len)?;
		let value = gparse.as_slice();
		let entiere = self.0.get(start..self.1)?;

		Some(Asn1Tlv{tag, value, gparse, entiere})
	}
	///Parse ASN.1 tag.
	pub fn asn1(&mut self) -> Option<Asn1Tlv<'a>>
	{
		let ptrsave = self.1;
		match self.__asn1() {
			Some(x) => Some(x),
			None => { self.1 = ptrsave; None }	//Undo advance on failure.	
		}
	}
	///Parse ASN.1 tag with given type.
	pub fn asn1_typed(&mut self, tag: (u8, u64)) -> Option<Asn1Tlv<'a>>
	{
		let ptrsave = self.1;
		let tlv = self.asn1()?;
		if tlv.tag != tag { self.1 = ptrsave; return None; }
		Some(tlv)
	}
	///If type matches, return gparse.
	pub fn if_asn(&mut self, tag: (u8, u64)) -> Option<GParse<'a>>
	{	
		self.asn1_typed(tag).map(|g|g.gparse)
	}
	///If element is TLV with specified tag, call f on it.
	pub fn asn1_fn<T>(&mut self, tag: (u8, u64), f: impl FnOnce(&mut GParse<'a>) -> T) -> Option<T>
	{
		if let Some(mut gparse) = self.if_asn(tag) {
			let ret = f(&mut gparse);
			if gparse.more() { return None; }
			Some(ret)
		} else {
			None
		}
	}
	///If element is TLV with specified tag, call f on all its sub-TLV.
	pub fn asn1_fn_list<T:Try>(&mut self, tag: (u8, u64), mut f: impl FnMut(&mut GParse<'a>) -> T) -> Option<T>
	{
		if let Some(mut gparse) = self.if_asn(tag) {
			while gparse.more() {
				let ret = f(&mut gparse);
				if ret.is_err() { return Some(ret); }
			}
			Some(<T as Try>::ok())
		} else {
			None
		}
	}
}
