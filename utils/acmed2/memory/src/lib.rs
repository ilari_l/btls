#![forbid(unsafe_code)]		//Hard requirement: Needs to be here.
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]

#[cfg(feature="std")] 
mod alloc {
	extern crate std;
	pub use std::sync::Arc;
	pub use std::boxed::Box;
	pub use std::borrow::Cow;
	pub use std::borrow::ToOwned;
	pub use std::collections::BTreeMap;
	pub use std::string::String;
	pub use std::string::ToString;
	pub use std::vec::Vec;
	pub use std::collections::VecDeque;
}
#[cfg(not(feature="std"))] 
mod alloc {
	extern crate alloc;
	pub use alloc::sync::Arc;
	pub use alloc::boxed::Box;
	pub use alloc::borrow::Cow;
	pub use alloc::borrow::ToOwned;
	pub use alloc::collections::BTreeMap;
	pub use alloc::string::String;
	pub use alloc::string::ToString;
	pub use alloc::vec::Vec;
	pub use alloc::collections::VecDeque;
}
pub use alloc::*;

#[macro_export]
macro_rules! fmt
{
	($($x:tt)*) => {{
		use core::fmt::Write;
		let mut s = String::new();
		write!(s, $($x)*).ok();		//This should never fail.
		s
	}}
}
