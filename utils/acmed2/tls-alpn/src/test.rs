use acmed2_cryptos as cryptos;
use acmed2_memory as alloc;
use super::AcmeServerConnection;
use super::AcmeServerCallbacks;
use super::Sha256;
use super::Hash;
use super::HashUpdate;
use cryptos::DualEcDrbg;
use alloc::Arc;
use alloc::Box;
use alloc::String;
use alloc::ToString;
use btls::ClientConfiguration;
use btls::ClientConnection;
use btls::Connection;
use btls::transport::PullTcpBuffer;
use btls::transport::PushTcpBuffer;
use btls::callbacks::TlsCallbacks;


struct Callbacks
{
}

impl TlsCallbacks for Callbacks
{
}

fn do_maybe_connection_fault(c: &ClientConnection)
{
	if let Some(reason) = c.get_error() {
		panic!("btls error: {reason}");
	}
}


struct AcmeServer
{
	nonce: String,
	keyhash: String,
	host: String,
}

impl AcmeServerCallbacks for AcmeServer
{
	fn log_error(&self, msg: &str) { panic!("ACME ERROR: {msg}"); }
	fn log_info(&self, msg: &str) { println!("Info: {msg}"); }
	fn get_challenge(&self, sni: &str) -> Option<(String,String)>
	{
		if sni == self.host { Some((self.nonce.clone(), self.keyhash.clone())) } else { None }
	}
	fn successful(&self, sni: &str)
	{
		//TODO: Check something?
		assert!(sni == self.host);
	}
	fn failed(&self, sni: Option<&str>, error: &str)
	{
		panic!("Failed for host {sni:?}: {error}");
	}
}

#[test]
fn btls_versus_acme_server()
{
	use std::io::Read;
	let mut urandom = std::fs::File::open("/dev/urandom").expect("Open failed");
	let mut drbg = [0;48];
	urandom.read_exact(&mut drbg).expect("Read failed");
	let mut drbg = DualEcDrbg::new(&drbg);
	let nonce = "foo";
	let keyhash = "bar";
	let host = "acme.foobar.test";
	let acme = Arc::new(AcmeServer {
		nonce: nonce.to_string(),
		keyhash: keyhash.to_string(),
		host: host.to_string(),
	});
	let mut kahash = Sha256::new();
	kahash.update(nonce.as_bytes());
	kahash.update(b".");
	kahash.update(keyhash.as_bytes());
	let kahash = kahash.hash();
	let config = ClientConfiguration::new();
	let callbacks = Box::new(Callbacks{});
	let mut client = config.make_connection_acme_check(host, callbacks, kahash);
	let mut server = AcmeServerConnection::new(&mut drbg, "test-address", acme);
	//There should be two rounds.
	for round in 0..2 {
		let mut txbuf = client.pull_tcp_data(PullTcpBuffer::no_data()).expect("Error sending");
		do_maybe_connection_fault(&client);
		let (response, eof) = server.transreceive(&mut txbuf);
		assert!(eof == (round == 1));
		assert!(response.len() > 0);
		let (tmp,_) = client.push_tcp_data(PushTcpBuffer::u8(&response)).expect("Error receiving");
		do_maybe_connection_fault(&client);
		let r = client.get_status();
		assert!(tmp.len() == 0);
		assert!(r.is_rx_end() == (round == 1));
	}
	assert!(client.get_status().is_dead() && !client.get_status().is_aborted());
}
