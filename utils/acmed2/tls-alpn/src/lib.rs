#![warn(unsafe_op_in_unsafe_fn)]
use acmed2_cryptos as cryptos;
use acmed2_formats as formats;
use acmed2_memory as alloc;
use acmed2_pki as pki;
use alloc::Arc;
use alloc::Box;
use alloc::Cow;
use alloc::fmt;
use alloc::String;
use alloc::ToString;
use alloc::Vec;
use core::cmp::min;
use core::fmt::Display;
use core::mem::replace;
use core::ops::Deref;
use core::ops::DerefMut;
use core::str::FromStr;
pub use cryptos::Drbg;
pub use cryptos::DualEcDrbg;
use cryptos::Ed25519PrivateKey;
use cryptos::Hash;
use cryptos::HashUpdate;
use cryptos::HashValue;
use cryptos::Sha256;
use cryptos::Sha256Value;
use cryptos::TlsProtector;
use cryptos::TlsRecordKey;
use cryptos::x25519;
use cryptos::x25519g;
use formats::GParse;
use formats::Rope;
use formats::rope_concat;
use formats::RopeTlsExt;
use formats::RopeVec8;
use formats::RopeVec16;
use formats::RopeVec24;
use pki::SanEntry;
use pki::generate_acme_certificate;


struct ClientHello
{
	fake_session: [u8;32],
	fake_session_len: u8,
	x25519_key: [u8;32],
	sni: String,
	transcript: Sha256,
}

impl ClientHello
{
	fn get_fake_session<'a>(&'a self) -> &'a [u8]
	{
		let len = min(self.fake_session_len as usize, self.fake_session.len());
		&self.fake_session[..len]
	}
	fn get_x25519_key(&self) -> [u8;32] { self.x25519_key }
	fn get_sni<'a>(&'a self) -> &'a str { &self.sni }
	fn get_transcript(&self) -> Sha256 { self.transcript.clone() }
	fn parse(payload: &[u8]) -> ClientHello
	{
		let mut transcript = Sha256::new();
		transcript.update(payload);
		let mut payload = GParse::new(payload);
		payload.slice(38);						//Headers, Version and random.
		let mut fake_session = [0;32];
		let _fake_session = payload.slice_u8().unwrap_or(b"");		//Fake session.
		let fslen = min(_fake_session.len(), fake_session.len());
		(&mut fake_session[..fslen]).copy_from_slice(&_fake_session[..fslen]);
		payload.slice_u16();						//Ciphersuites.
		payload.slice_u8();						//Compression methods.
		let mut extensions = payload.gparse_u16().unwrap_or(GParse::new(b""));
		let mut x25519_key = [0;32];
		let mut sni = Vec::new();
		while extensions.more() {
			let i = extensions.u16().unwrap_or(65535);
			let mut d = extensions.gparse_u16().unwrap_or(GParse::new(b""));
			if i == 0 {
				let mut names = d.gparse_u16().unwrap_or(GParse::new(b""));
				while names.more() {
					let k = names.u8().unwrap_or(255);
					let n = names.slice_u16().unwrap_or(b"");
					if k == 0 { sni = n.to_vec(); }
				}
			}
			if i == 51 {						//Key share.
				let mut shares = d.gparse_u16().unwrap_or(GParse::new(b""));
				while shares.more() {
					let g = shares.u16().unwrap_or(0);
					let s = shares.slice_u16().unwrap_or(b"");
					if g == 29 && s.len() == 32 {		//X25519.
						x25519_key.copy_from_slice(s);
					}
				}
			}
		}
		let mut sni = String::from_utf8(sni).unwrap_or(String::new());
		sni.make_ascii_lowercase();
		ClientHello {
			fake_session: fake_session,
			fake_session_len: fslen as u8,
			x25519_key: x25519_key,
			sni: sni,
			transcript: transcript,
		}
	}
}

fn __server_hello<'a>(random: &'a [u8;32], fake_session: &'a [u8], x25519_key: &'a [u8;32]) -> impl Rope+'a
{
	rope_concat!{
		2u8,
		RopeVec24(rope_concat!{
			0x0303u16,
			&random[..],
			RopeVec8(fake_session),
			0x1303u16,
			0u8,
			RopeVec16(rope_concat!{
				RopeTlsExt(43, 0x0304u16),
				RopeTlsExt(51, rope_concat!{29u16, RopeVec16(&x25519_key[..])})
			})
		})
	}
}

fn __encrypted_extensions() -> impl Rope
{
	rope_concat!{
		8u8,							//Encrypted extensions.
		RopeVec24(RopeVec16(rope_concat!{
			//ALPN... Bonus points for nested lengths...
			RopeTlsExt(16, RopeVec16(RopeVec8(&b"acme-tls/1"[..])))
		}))
	}
}

fn __certificate<'a>(cert: &'a [u8]) -> impl Rope+'a
{
	rope_concat!{
		11u8,
		RopeVec24(rope_concat!{
			RopeVec8(()),	//Context.
			RopeVec24(rope_concat!{
				RopeVec24(cert),
				RopeVec16(())
			})
		})
	}
}

fn __certificate_verify<'a>(sig: &'a [u8;64]) -> impl Rope+'a
{
	rope_concat!{15u8, RopeVec24(rope_concat!{0x0807u16, RopeVec16(&sig[..])})}
}

fn __finished<'a>(mac: &'a [u8]) -> impl Rope+'a
{
	rope_concat!{20u8, RopeVec24(&mac[..])}
}

fn __add_message(msg: impl Rope, transcript: &mut Sha256, buf: &mut Vec<u8>)
{
	msg.call(&mut |part|{
		transcript.update(part);
		buf.extend_from_slice(part);
	})
}

fn __try_translate_san_ipv6(name: &str) -> Option<[u8;16]>
{
	let name = name.strip_suffix(".ip6.arpa")?;
	let mut parts = name.split('.');
	//There should be 32 parts.
	let mut out = [0;16];
	for i in 0..32 {
		out[15-i/2] |= match parts.next()? {
			"0" => 0, "1" => 1, "2" => 2, "3" => 3, "4" => 4, "5" => 5, "6" => 6, "7" => 7,
			"8" => 8, "9" => 9, "a" => 10, "b" => 11, "c" => 12, "d" => 13, "e" => 14, "f" => 15,
			"A" => 10, "B" => 11, "C" => 12, "D" => 13, "E" => 14, "F" => 15, _ => return None
		} << i % 2 * 4;
	}
	if parts.next().is_some() { return None; }	//More than 32 parts.
	Some(out)
}

fn __try_translate_san_ipv4(name: &str) -> Option<[u8;4]>
{
	let name = name.strip_suffix(".in-addr.arpa")?;
	let mut parts = name.split('.');
	//There should be 4 parts, spelling the address in reverse.
	let mut out = [0;4];
	for i in 0..4 { out[3-i] = u8::from_str(parts.next()?).ok()?; }
	if parts.next().is_some() { return None; }	//More than 4 parts.
	Some(out)
}

fn __sni_to_san<'a>(sni: &'a str) -> SanEntry<'a>
{
	if let Some(ip) = __try_translate_san_ipv6(&sni) { return SanEntry::Ipv6(ip); }
	if let Some(ip) = __try_translate_san_ipv4(&sni) { return SanEntry::Ipv4(ip); }
	SanEntry::Name(Cow::Borrowed(sni.as_bytes()))
}

struct ServerReply
{
	records: Vec<u8>,
	client_handshake_secret: Sha256Value,
	client_application_secret: Sha256Value,
	server_key: TlsRecordKey,
}

fn server_reply(fake_session: &[u8], x25519_peer: &[u8;32], sni: &str, nonce: &str, key_hash: &str,
	drbg: &mut (impl Drbg+?Sized), sigkey: &Ed25519PrivateKey, transcript: &mut Sha256) -> ServerReply
{
	let mut random = [0;32];
	let mut x25519_private = [0;32];
	drbg.fill(&mut random);
	drbg.fill(&mut x25519_private);

	let name = __sni_to_san(sni);
	let cert = generate_acme_certificate(sigkey, name, nonce, key_hash, drbg);
	let x25519_key = x25519g(&x25519_private);
	let shared_secret = x25519(&x25519_private, x25519_peer);
	let hss = Sha256Value::handshake_secret(&shared_secret);
	let mut send_key = TlsRecordKey::new();
	let mut output = Vec::new();
	let mut frag = Vec::new();
	__add_message(__server_hello(&random, fake_session, &x25519_key), transcript, &mut frag);
	send_key.encrypt(&mut output, &frag, 22);
	//Maybe fake CSS.
	if fake_session.len() > 0 { send_key.encrypt(&mut output, &[1], 20); }

	//Derive handshake keys.
	let chts = hss.client_hts(transcript);
	let shts = hss.server_hts(transcript);
	let mss = hss.master_secret();
	send_key.set_1303_key(&shts);

	//Clear fragment buffer, as second part is indepdendent fragment.
	frag.clear();

	__add_message(__encrypted_extensions(), transcript, &mut frag);
	__add_message(__certificate(&cert), transcript, &mut frag);
	let sig = sigkey.sign_nonce(drbg, |h|{
		h.update(&[32;64]);
		h.update(b"TLS 1.3, server CertificateVerify\0");
		h.update(&transcript.clone().hash());
	});
	__add_message(__certificate_verify(&sig), transcript, &mut frag);
	let mac = shts.finished(&transcript);
	__add_message(__finished(mac.__deref()), transcript, &mut frag);
	send_key.encrypt(&mut output, &frag, 22);

	//Derive application keys.
	#[cfg(test)] { println!("transcript: {hash:?}", hash=transcript.clone().hash()); }
	let cats0 = mss.client_ats0(transcript);
	let sats0 = mss.server_ats0(transcript);
	#[cfg(test)] { println!("client_ats0: {cats0:?}", cats0=cats0.__deref()); }
	send_key.set_1303_key(&sats0);

	ServerReply {
		records: output,
		client_handshake_secret: chts,
		client_application_secret: cats0,
		server_key: send_key,
	}
}

pub trait AcmeServerCallbacks
{
	fn log_error(&self, msg: &str);
	fn log_info(&self, msg: &str);
	fn get_challenge(&self, sni_lowercased: &str) -> Option<(String,String)>;
	fn successful(&self, sni: &str);
	fn failed(&self, sni: Option<&str>, error: &str);
}

pub struct AcmeServerConnection
{
	crec: Vec<u8>,
	decrypt: TlsRecordKey,
	encrypt: TlsRecordKey,
	drbg: Box<dyn Drbg+'static>,
	key: Ed25519PrivateKey,
	c_ats0: Sha256Value,
	addr: String,
	sni: String,
	cb: Arc<dyn AcmeServerCallbacks+'static>,
	recv_record: u32,
}

impl AcmeServerConnection
{
	pub fn new(drbg: &mut (impl Drbg+?Sized), addr: &str, cb: Arc<dyn AcmeServerCallbacks+'static>) ->
		AcmeServerConnection
	{
		let mut pkey = [0;32];
		drbg.fill(&mut pkey);
		let key = Ed25519PrivateKey::new(pkey);
		AcmeServerConnection {
			crec: Vec::new(),
			decrypt: TlsRecordKey::new(),
			encrypt: TlsRecordKey::new(),
			drbg: drbg.fork_boxed(),
			key: key,
			c_ats0: Sha256Value::__zeroes(),
			addr: addr.to_string(),
			sni: String::new(),
			cb: cb,
			recv_record: 0,
		}
	}
	fn __catastrophic_fault(&mut self, alert: Option<u8>, msg: impl Display, output: &mut Vec<u8>,
		eof_flag: &mut bool)
	{
		let msg = fmt!("{addr}: {msg}", addr=self.addr);
		if alert != Some(0) {	//close_notify is not fatal.
			//On failure, sni might not have been filled out. In that case, self.sni is empty.
			let sni = if self.sni.is_empty() { None } else { Some(self.sni.deref()) };
			self.cb.failed(sni, &msg.to_string());
			self.cb.log_error(&msg);
		} else {
			//This __catastrophic_fault with close_notify is only called on success.
			self.cb.successful(&self.sni);
			self.cb.log_info(&msg);
		}
		if let Some(alert) = alert {
			self.encrypt.encrypt(output, &[2, alert], 21);	//Send alert.
		}
		*eof_flag = true;
	}
	fn __transreceive_record(&mut self, record: &[u8], output: &mut Vec<u8>, eof_flag: &mut bool)
	{
		if record.len() < 5 { return; }		//Should not happen.
		let rtype = record[0];
		let payload = &record[5..];
		self.recv_record += 1;
		if rtype == 23 {
			//Encrypted record. Decrypt the record.
			let mut out = Vec::new();
			let rrec = self.recv_record;
			let rtype = match self.decrypt.decrypt(&mut out, record) {
				Ok(Some((rtype, amt))) if amt == record.len() => rtype,
				//Decrypt failure, things have gone terribly wrong.
				_ => return self.__catastrophic_fault(Some(20),
					format_args!("Failed to decrypt record #{rrec}"), output, eof_flag)
			};
			if rtype == 21 && out.len() >= 2 { //Encrypted alert.
				let alert = out[1];
				if alert > 0 {
					self.__catastrophic_fault(None, format_args!("Received alert #{alert}"),
						output, eof_flag);
				} else {
					//Not actually fault, but can be handled as such. ACK shutdown.
					let msg = fmt!("Succesful for '{sni}'", sni=self.sni);
					self.__catastrophic_fault(Some(0), msg, output, eof_flag);
				};
				return;
			} else if rtype == 22 {
				//This should be client finished. This would affect transcript, but we do not
				//need transcript anymore. It however switches keys.
				self.decrypt.set_1303_key(&self.c_ats0);
			}
			//Ignore other types, as those are not needed.
		} else if rtype == 21 && payload.len() >= 2 {
			//Unencrypted alert, things have gone terribly wrong.
			return self.__catastrophic_fault(None, &fmt!("Received alert #{alert}", alert=payload[1]),
				output, eof_flag);
		} else if rtype == 22 {
			//Client Hello.
			let chello = ClientHello::parse(payload);
			let fake_session = chello.get_fake_session();
			let x25519_peer = chello.get_x25519_key();
			let sni = chello.get_sni();
			let mut transcript = chello.get_transcript();
			let (nonce, key_hash) = match self.cb.get_challenge(sni) {
				Some(x) => x,
				None => return self.__catastrophic_fault(Some(112),
					format_args!("No challenge entry found for '{sni}'"), output, eof_flag)
			};
			let drbg: &mut dyn Drbg = self.drbg.deref_mut();
			let sreply = server_reply(fake_session, &x25519_peer, sni, &nonce, &key_hash,
				drbg, &self.key, &mut transcript);
			//Update the keys and add the records.
			self.decrypt.set_1303_key(&sreply.client_handshake_secret);
			output.extend_from_slice(&sreply.records);
			self.c_ats0 = sreply.client_application_secret;
			self.encrypt = sreply.server_key;
			self.sni = sni.to_string();
		}
		//Ignore any other types (e.g., 23 which should not be used.).
	}
	pub fn transreceive(&mut self, mut recv: &[u8]) -> (Vec<u8>, bool)
	{
		let mut output = Vec::new();
		let mut eof_flag = false;
		while !eof_flag {
			let len = if self.crec.len() >= 5 {
				//+5 for header.
				self.crec[3] as usize * 256 + self.crec[4] as usize + 5
			} else {
				5	//Header.
			};
			//Fill current record to len bytes.
			if self.crec.len() < len {
				let amt = min(len - self.crec.len(), recv.len());
				//If there is nothing more to process and need data, break out.
				if amt == 0 { break; }
				//amt <= recv.len() and self.crec.len() + amt <= len
				let (head,tail) = recv.split_at(amt);
				self.crec.extend_from_slice(head);
				recv = tail;
				continue;
			}
			//If we got here, self.crec.len() == len, which means record is complete.
			let rec = replace(&mut self.crec, Vec::new());
			self.__transreceive_record(&rec, &mut output, &mut eof_flag);
		}
		(output, eof_flag)
	}
}

#[cfg(test)] mod test;
