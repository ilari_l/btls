//!Low level cryptos.
#![forbid(unsafe_code)]		//Hard requirement: Needs to be here.
#![deny(missing_docs)]
#![warn(unsafe_op_in_unsafe_fn)]
#![no_std]
#[cfg(test)] #[macro_use] extern crate std;

use acmed2_memory as alloc;
use alloc::Box;

//Big integers.
mod bigint;
//Chacha20-Poly1305
mod chacha20poly1305;
pub use crate::chacha20poly1305::chacha20poly1305_decrypt;
pub use crate::chacha20poly1305::chacha20poly1305_encrypt;
pub use crate::chacha20poly1305::DualEcDrbg;
//ECDSA verification
mod ecdsa;
pub use crate::ecdsa::Curve as EcdsaCurve;
pub use crate::ecdsa::ecdsa_verify_sha256;
pub use crate::ecdsa::ecdsa_verify_sha384;
pub use crate::ecdsa::VerifyError as EcdsaVerifyError;
//X25519, Ed25519 signing and verification.
mod f25519;
pub use crate::f25519::ed25519_verify;
pub use crate::f25519::Ed25519PrivateKey;
pub use crate::f25519::LEd25519VerifyError;
pub use crate::f25519::x25519;
pub use crate::f25519::x25519g;
//Hashes: SHA-256, SHA-384 and SHA-512.
mod hash;
pub use crate::hash::ByteArray;
pub use crate::hash::Hash;
pub use crate::hash::HashUpdate;
pub use crate::hash::hmac_sha256;
pub use crate::hash::Sha256;
pub use crate::hash::Sha384;
pub use crate::hash::Sha512;
//RSA verification.
mod rsa;
pub use crate::rsa::rsa_pkcs1_verify_sha256;
pub use crate::rsa::rsa_pkcs1_verify_sha384;
pub use crate::rsa::rsa_pkcs1_verify_sha512;
pub use crate::rsa::VerifyError as RsaVerifyError;
//TLS low-level.
mod tls_low;
pub use tls_low::HashValue;
pub use tls_low::Sha256Value;
pub use tls_low::TlsProtector;
pub use tls_low::TlsRecordKey;


///DRBG
pub trait Drbg
{
	///Fill the buffer with random octets from this DRBG.
	fn fill(&mut self, buf: &mut [u8]);
	///Fork this DRBG by reading key from this DRBG and using it to initialize a new DRBG.
	fn fork_boxed(&mut self) -> Box<dyn Drbg>;
}
