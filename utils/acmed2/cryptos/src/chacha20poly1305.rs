use crate::Drbg;
use crate::alloc::Box;

macro_rules! M
{
	($a:expr,$b:expr) => { ($a as u128) * ($b as u128) }
}

//Note: r has base 2^0 and 2^66 (60 and 58 bits). n has impiled 2^128 term.
fn poly1305_aam(a: (u64, u64, u64), r: (u64, u64), n: (u64, u64)) -> (u64, u64, u64)
{
	//print!("(({a0}+{a1}*2^64+{a2}*2^128+{n0}+{n1}*2^64+2^128)*({r0}+{r1}*2^66)",
	//	a0=a.0,a1=a.1,a2=a.2,n0=n.0,n1=n.1,r0=r.0,r1=r.1);
	//Accumulate.
	let w = a.0 as u128 + n.0 as u128;
	let a0 = w as u64;
	let w = a.1 as u128 + n.1 as u128 + (w >> 64);
	let a1 = w as u64;
	let w = a.2 as u128 + 1 + (w >> 64);
	let a2 = w as u64;
	//Raw products at 2^0, 2^64 and 2^128 level.
	let mut u0 = M!(a0, r.0)+M!(a1, 5*r.1);
	let mut u1 = M!(a1, r.0)+M!(5*r.1, a2)+M!(4*r.1,a0);
	let mut u2 = a2 * r.0;
	//Truncate u1 and u2 into u0.
	u0 += (5 * (u1 >> 66) as u64 + 5 * (u2 >> 2)) as u128;
	u1 &= 0x3FFFFFFFFFFFFFFFF;
	u2 &= 3;
	//Carrychain.
	u1 += u0 >> 64;
	let u0 = u0 as u64;
	u2 += (u1 >> 64) as u64;
	let u1 = u1 as u64;
	//println!("-({u0}+{u1}*2^64+{u2}*2^128))%(2^130-5)");
	(u0,u1,u2)
}

fn poly1305_data(mut a: (u64, u64, u64), r: (u64, u64), data: &[u8]) -> (u64, u64, u64)
{
	let (mut data, tail) = data.split_at(data.len() & !15);
	while data.len() >= 16 {
		let mut t0 = [0;8];
		let mut t1 = [0;8];
		t0.copy_from_slice(&data[0..8]);
		t1.copy_from_slice(&data[8..16]);
		let n0 = u64::from_le_bytes(t0);
		let n1 = u64::from_le_bytes(t1);
		a = poly1305_aam(a, r, (n0, n1));
		data = &data[16..];
	}
	//tail.len() < 16 will always be true. The condition is here to get rid of possible crash.
	if tail.len() > 0 && tail.len() < 16 {	
		let mut buf = [0;16];
		(&mut buf[..tail.len()]).copy_from_slice(tail);
		let mut t0 = [0;8];
		let mut t1 = [0;8];
		t0.copy_from_slice(&buf[0..8]);
		t1.copy_from_slice(&buf[8..16]);
		let n0 = u64::from_le_bytes(t0);
		let n1 = u64::from_le_bytes(t1);
		a = poly1305_aam(a, r, (n0, n1));
	}
	a
}

fn quarterround(mut a: u32, mut b: u32, mut c: u32, mut d: u32) -> (u32, u32, u32, u32)
{
	a = a.wrapping_add(b); d ^= a; d = d.rotate_left(16);
	c = c.wrapping_add(d); b ^= c; b = b.rotate_left(12);
	a = a.wrapping_add(b); d ^= a; d = d.rotate_left(8);
	c = c.wrapping_add(d); b ^= c; b = b.rotate_left(7);
	(a,b,c,d)
}

fn chacha20_xfrm(mout: &mut [u32;16], min: &[u32;16])
{
	*mout = *min;
	for _ in 0..10 {
		let (a0,a4,a8,a12) = quarterround(mout[0], mout[4], mout[8], mout[12]);
		let (a1,a5,a9,a13) = quarterround(mout[1], mout[5], mout[9], mout[13]);
		let (a2,a6,a10,a14) = quarterround(mout[2], mout[6], mout[10], mout[14]);
		let (a3,a7,a11,a15) = quarterround(mout[3], mout[7], mout[11], mout[15]);
		let (a0,a5,a10,a15) = quarterround(a0,a5,a10,a15);
		let (a1,a6,a11,a12) = quarterround(a1,a6,a11,a12);
		let (a2,a7,a8,a13) = quarterround(a2,a7,a8,a13);
		let (a3,a4,a9,a14) = quarterround(a3,a4,a9,a14);
		mout[0] = a0; mout[1] = a1; mout[2] = a2; mout[3] = a3;
		mout[4] = a4; mout[5] = a5; mout[6] = a6; mout[7] = a7;
		mout[8] = a8; mout[9] = a9; mout[10] = a10; mout[11] = a11;
		mout[12] = a12; mout[13] = a13; mout[14] = a14; mout[15] = a15;
	}
	for (t,s) in mout.iter_mut().zip(min.iter()) { *t = t.wrapping_add(*s); }
}

fn load_matrix(matrix: &mut [u32;16], key: &[u8;32], nonce: &[u8;12])
{
	matrix[0] = 0x61707865;
	matrix[1] = 0x3320646e;
	matrix[2] = 0x79622d32;
	matrix[3] = 0x6b206574;
	//Load key words.
	for i in 0..8 {
		let mut t = [0;4];
		t.copy_from_slice(&key[4*i..4*i+4]);
		matrix[i+4] = u32::from_le_bytes(t);
	}
	//Load nonce words.
	for i in 0..3 {
		let mut t = [0;4];
		t.copy_from_slice(&nonce[4*i..4*i+4]);
		matrix[i+13] = u32::from_le_bytes(t);
	}
}

fn xcrypt_data(outmat: &mut [u32;16], matrix: &mut [u32;16], buf: &mut [u8])
{
	let buflen = buf.len();
	let (mut buf, tail) = buf.split_at_mut(buflen & !63);
	while buf.len() >= 64 {
		matrix[12] += 1;
		chacha20_xfrm(outmat, matrix);
		for i in 0..16 {
			let mut t = [0;4];
			t.copy_from_slice(&buf[4*i..4*i+4]);
			let t = (u32::from_le_bytes(t) ^ outmat[i]).to_le_bytes();
			(&mut buf[4*i..4*i+4]).copy_from_slice(&t);
		}
		buf = &mut buf[64..];
	}
	//tail.len() < 16 will always be true. The condition is here to get rid of possible crash.
	if tail.len() > 0 && tail.len() < 64 {	
		let mut buf = [0;64];
		(&mut buf[..tail.len()]).copy_from_slice(tail);
		matrix[12] += 1;
		chacha20_xfrm(outmat, matrix);
		for i in 0..16 {
			let mut t = [0;4];
			t.copy_from_slice(&buf[4*i..4*i+4]);
			let t = (u32::from_le_bytes(t) ^ outmat[i]).to_le_bytes();
			(&mut buf[4*i..4*i+4]).copy_from_slice(&t);
		}
		tail.copy_from_slice(&buf[..tail.len()]);
	}
}

fn finish_mac(mac_accum: (u64,u64,u64), adlen: usize, buflen: usize, r: (u64, u64), s: (u64, u64)) -> (u64,u64)
{
	let mac_accum = poly1305_aam(mac_accum, r, (adlen as u64, buflen as u64));
	//If mac_accum > 2^130-5, need add 5.
	let w = mac_accum.0 as u128 + 5;
	let w = mac_accum.1 as u128 + (w >> 64);
	let w = mac_accum.2 + (w >> 64) as u64;
	let carry = 5 * (w >> 2);
	//println!("(({m0}+{m1}*2^64+{m2}*2^128)%(2^130-5)+({s0}+{s1}*2^64))%2^128",
	//	m0=mac_accum.0, m1=mac_accum.1, m2=mac_accum.2, s0=s.0, s1=s.1);
	let w0 = mac_accum.0 as u128 + s.0 as u128 + carry as u128;
	let w1 = (mac_accum.1 as u128 + s.1 as u128 + (w0 >> 64)) as u64;
	let w0 = w0 as u64;
	//println!("{w0}+{w1}*2^64");
	(w0, w1)
}

fn derive_rs_from_matrix(outmat: &[u32;16]) -> ((u64,u64),(u64,u64))
{
	let r0 = (outmat[0] as u64 |(outmat[1] as u64)<<32) & 0x0FFFFFFC0FFFFFFF;
	let r1 = (outmat[2] as u64 |(outmat[3] as u64)<<32) & 0x0FFFFFFC0FFFFFFC;
	let s0 = outmat[4] as u64 |(outmat[5] as u64)<<32;
	let s1 = outmat[6] as u64 |(outmat[7] as u64)<<32;
	((r0,r1 >> 2),(s0,s1))
}

///Encrypt message with Chacha20-Poly1305.
pub fn chacha20poly1305_encrypt(key: &[u8;32], nonce: &[u8;12], ad: &[u8], buf: &mut [u8], tag: &mut [u8;16])
{
	let mut matrix = [0;16];
	let mut mac_accum = (0,0,0);
	load_matrix(&mut matrix, key, nonce);
	let mut outmat = [0;16];
	chacha20_xfrm(&mut outmat, &matrix);
	let (r,s) = derive_rs_from_matrix(&outmat);
	mac_accum = poly1305_data(mac_accum, r, ad);
	xcrypt_data(&mut outmat, &mut matrix, buf);
	mac_accum = poly1305_data(mac_accum, r, buf);
	let (w0, w1) = finish_mac(mac_accum, ad.len(), buf.len(), r, s);
	(&mut tag[..8]).copy_from_slice(&w0.to_le_bytes());
	(&mut tag[8..]).copy_from_slice(&w1.to_le_bytes());
}

///Decrypt message with Chacha20-Poly1305.
pub fn chacha20poly1305_decrypt(key: &[u8;32], nonce: &[u8;12], ad: &[u8], buf: &mut [u8], tag: &[u8;16]) ->
	Result<(), ()>
{
	let mut matrix = [0;16];
	let mut mac_accum = (0,0,0);
	load_matrix(&mut matrix, key, nonce);
	let mut outmat = [0;16];
	chacha20_xfrm(&mut outmat, &matrix);
	let (r,s) = derive_rs_from_matrix(&outmat);
	mac_accum = poly1305_data(mac_accum, r, ad);
	mac_accum = poly1305_data(mac_accum, r, buf);
	let (w0, w1) = finish_mac(mac_accum, ad.len(), buf.len(), r, s);
	let mut u0 = [0;8];
	let mut u1 = [0;8];
	u0.copy_from_slice(&tag[..8]);
	u1.copy_from_slice(&tag[8..]);
	let u0 = u64::from_le_bytes(u0);
	let u1 = u64::from_le_bytes(u1);
	if (u0 ^ w0) | (u1 ^ w1) != 0 { return Err(()); }	//MAC failed.
	xcrypt_data(&mut outmat, &mut matrix, buf);
	Ok(())
}

///DUAL_EC_DRBG (somewhat buggy implementation).
pub struct DualEcDrbg([u32;12],[u32;16],u8);

impl DualEcDrbg
{
	fn __refill(&mut self)
	{
		let mut matrix = [0;16];
		matrix[0] = 0x61707865;
		matrix[1] = 0x3320646e;
		matrix[2] = 0x79622d32;
		matrix[3] = 0x6b206574;
		(&mut matrix[4..]).copy_from_slice(&self.0);
		chacha20_xfrm(&mut self.1, &matrix);
		for i in 8..12 {
			self.0[i] = self.0[i].wrapping_add(1);
			if self.0[i] != 0 { break; }
		}
		self.2 = 0;
	}
	///Create a new Dual-EC DRBG instance from given seed.
	pub fn new(seed: &[u8;48]) -> DualEcDrbg
	{
		let mut matrix = [0;12];
		for i in 0..12 {
			let mut t = [0;4];
			t.copy_from_slice(&seed[4*i..4*i+4]);
			matrix[i] = u32::from_le_bytes(t);
		}
		DualEcDrbg(matrix,[0;16],64)
	}
	///Fork this DRBG by reading key from this DRBG and using it to initialize a new DRBG.
	pub fn fork(drbg: &mut impl Drbg) -> DualEcDrbg
	{
		let mut seed = [0;48];
		drbg.fill(&mut seed);
		DualEcDrbg::new(&mut seed)
	}
}

impl Drbg for DualEcDrbg
{
	fn fill(&mut self, mut buf: &mut [u8])
	{
		while buf.len() > 0 {
			if self.2 >= 64 { self.__refill(); }
			if buf.len() >= 4 && self.2 % 4 == 0 {
				let t = self.1[self.2 as usize / 4].to_le_bytes();
				(&mut buf[..4]).copy_from_slice(&t);
				buf = &mut buf[4..];
				self.2 += 4;
			} else {
				buf[0] = (self.1[self.2 as usize/ 4] >> self.2 as usize % 4 * 8) as u8;
				buf = &mut buf[1..];
				self.2 += 1;
			}
		}
	}
	fn fork_boxed(&mut self) -> Box<dyn Drbg> { Box::new(DualEcDrbg::fork(self)) }
}


#[test]
fn mul_max()
{
	const M: u64 = 0xFFFFFFFFFFFFFFFF;
	const R0: u64 = 0x0FFFFFFFFFFFFFFF;
	const R1: u64 = 0x03FFFFFFFFFFFFFF;
	const A2: u64 = 7;
	poly1305_aam((M,M,A2), (R0, R1), (M,M));
}

#[test]
fn test_qround()
{
	assert_eq!(quarterround(0x11111111, 0x01020304, 0x9b8d6f43, 0x01234567),
		(0xea2a92f4, 0xcb1cf8ce, 0x4581472e, 0x5881c4bb));
}

#[test]
fn test_chacha_xfrm()
{
	let input = [
		0x61707865, 0x3320646e, 0x79622d32, 0x6b206574,
		0x03020100, 0x07060504, 0x0b0a0908, 0x0f0e0d0c,
		0x13121110, 0x17161514, 0x1b1a1918, 0x1f1e1d1c,
		0x00000001, 0x09000000, 0x4a000000, 0x00000000,
	];
	let refout = [
		0xe4e7f110, 0x15593bd1, 0x1fdd0f50, 0xc47120a3,
		0xc7f4d1c7, 0x0368c033, 0x9aaa2204, 0x4e6cd4c3,
		0x466482d2, 0x09aa9f07, 0x05d7c214, 0xa2028bd9,
		0xd19c12b5, 0xb94e16de, 0xe883d0cb, 0x4e3c50a2,
	];
	let mut out = [0;16];
	chacha20_xfrm(&mut out, &input);
	assert_eq!(&out, &refout);
}

#[test]
fn test_chacha20poly1305()
{
	let plaintext = b"Ladies and Gentlemen of the class of '99: If I could offer you only one tip for the \
		future, sunscreen would be it.";
	let ad = b"\x50\x51\x52\x53\xc0\xc1\xc2\xc3\xc4\xc5\xc6\xc7";
	let key = b"\x80\x81\x82\x83\x84\x85\x86\x87\x88\x89\x8a\x8b\x8c\x8d\x8e\x8f\
		\x90\x91\x92\x93\x94\x95\x96\x97\x98\x99\x9a\x9b\x9c\x9d\x9e\x9f";
	let nonce = b"\x07\x00\x00\x00@ABCDEFG";
	let refct = b"\xd3\x1a\x8d\x34\x64\x8e\x60\xdb\x7b\x86\xaf\xbc\x53\xef\x7e\xc2\
		\xa4\xad\xed\x51\x29\x6e\x08\xfe\xa9\xe2\xb5\xa7\x36\xee\x62\xd6\
		\x3d\xbe\xa4\x5e\x8c\xa9\x67\x12\x82\xfa\xfb\x69\xda\x92\x72\x8b\
		\x1a\x71\xde\x0a\x9e\x06\x0b\x29\x05\xd6\xa5\xb6\x7e\xcd\x3b\x36\
		\x92\xdd\xbd\x7f\x2d\x77\x8b\x8c\x98\x03\xae\xe3\x28\x09\x1b\x58\
		\xfa\xb3\x24\xe4\xfa\xd6\x75\x94\x55\x85\x80\x8b\x48\x31\xd7\xbc\
		\x3f\xf4\xde\xf0\x8e\x4b\x7a\x9d\xe5\x76\xd2\x65\x86\xce\xc6\x4b\
		\x61\x16";
	let reftag = b"\x1a\xe1\x0b\x59\x4f\x09\xe2\x6a\x7e\x90\x2e\xcb\xd0\x60\x06\x91";
	let mut tag = [0;16];
	let mut buf: [u8;114] = *plaintext;
	chacha20poly1305_encrypt(key, nonce, ad, &mut buf, &mut tag);
	assert_eq!(&buf[..], &refct[..]);
	assert_eq!(&tag[..], &reftag[..]);
	chacha20poly1305_decrypt(key, nonce, ad, &mut buf, &tag).expect("Decrypt failed");
	assert_eq!(&buf[..], &plaintext[..]);
}

#[test]
fn drbg_split_indep()
{
	let mut state1 = DualEcDrbg::new(&[61;48]);
	let mut state2 = DualEcDrbg::new(&[61;48]);
	let mut state3 = DualEcDrbg::new(&[62;48]);
	let mut buf1 = [0;256];
	let mut buf2 = [0;256];
	let mut buf3 = [0;256];
	state1.fill(&mut buf1);
	state2.fill(&mut buf2[..127]);
	state2.fill(&mut buf2[127..]);
	state3.fill(&mut buf3);
	assert_eq!(&buf1[..], &buf2[..]);
	assert!(&buf1[..] != &buf3[..]);
}

#[test]
fn drbg_stat_random()
{
	const STDDEV: usize = 64;
	const SIGMA: usize = 5;
	let mut state = DualEcDrbg::new(&[42;48]);
	let mut buf = crate::alloc::Vec::new();
	buf.resize(256*STDDEV*STDDEV, 0);
	let mut count = [0usize;256];
	state.fill(&mut buf);
	for &b in buf.iter() { count[b as usize] += 1; }
	let mincnt = count.iter().cloned().fold(0xFFFFFFFF,std::cmp::min);
	let maxcnt = count.iter().cloned().fold(0,std::cmp::max);
	println!("counts: {mincnt} to {maxcnt}");
	assert!(mincnt >= STDDEV*(STDDEV-SIGMA));
	assert!(maxcnt <= STDDEV*(STDDEV+SIGMA));
}
