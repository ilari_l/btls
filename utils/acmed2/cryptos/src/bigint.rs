use super::alloc::Vec;


macro_rules! M
{
	($a:expr,$b:expr) => { ($a as u128) * ($b as u128) }
}

pub(crate) fn bigint_mul(r: &mut [u64], a: &[u64], b: &[u64])
{
	let mut carry = 0u128;
	for i in 0..r.len() {
		//Dump carry from previous word
		r[i] = carry as u64;
		carry = carry >> 64;
		for j in (i+1).saturating_sub(b.len())..a.len()  {
			if j > i { break; }		//Would warparound.
			let k = i-j;
			if k >= b.len() { continue; }	//Can not actually happen.
			let u = M!(a[j],b[k])+r[i] as u128;
			r[i] = u as u64;
			carry += u >> 64;
		}
	}
}

pub(crate) fn bigint_mac(r: &mut [u64], a: &[u64], b: &[u64])
{
	let mut carry = 0u128;
	for i in 0..r.len() {
		//Dump carry from previous word
		let u = r[i] as u128 + carry;
		r[i] = u as u64;
		carry = u >> 64;
		for j in (i+1).saturating_sub(b.len())..a.len()  {
			if j > i { break; }		//Would warparound.
			let k = i-j;
			if k >= b.len() { continue; }	//Can not actually happen.
			let u = M!(a[j],b[k])+r[i] as u128;
			r[i] = u as u64;
			carry += u >> 64;
		}
	}
}

pub(crate) fn bigint_mac1(r: &mut [u64], a: &[u64], b: u64) -> u64
{
	let mut carry = 0u128;
	for i in 0..r.len() {
		let u = if i < a.len() { M!(a[i],b)+r[i] as u128 } else { r[i] as u128 } + carry;
		r[i] = u as u64;
		carry = u >> 64;
	}
	carry as u64
}

pub(crate) fn bigint_add(r: &mut [u64], x: &[u64]) -> u64
{
	let mut carry = 0u128;
	for i in 0..r.len() {
		let u = if i < x.len() { r[i] as u128 + x[i] as u128 } else { r[i] as u128 } + carry;
		r[i] = u as u64;
		carry = u >> 64;
	}
	carry as u64
}

//If x > r, gives nothing sane.
pub(crate) fn bigint_sub(r: &mut [u64], x: &[u64])
{
	let mut carry = 0u128;
	for i in 0..r.len() {
		let u = if i < x.len() {
			(r[i] as u128).wrapping_sub(x[i] as u128)
		} else {
			r[i] as u128
		}.wrapping_sub(carry);
		r[i] = u as u64;
		carry = u >> 127;
	}
}

fn truncate_slice<'a>(x: &'a[u64]) -> &'a [u64]
{
	let mut last_nz = 0;
	for (idx, v) in x.iter().cloned().enumerate() { if v != 0 { last_nz = idx + 1; } }
	&x[..last_nz]
}

fn truncate_slice_mut<'a>(x: &'a mut [u64]) -> &'a mut [u64]
{
	let mut last_nz = 0;
	for (idx, v) in x.iter().cloned().enumerate() { if v != 0 { last_nz = idx + 1; } }
	&mut x[..last_nz]
}

fn magnitude_of(x: &[u64]) -> usize
{
	if x.len() > 0 { x.len() * 64 - x[x.len()-1].leading_zeros() as usize } else { 0 }
}

//Calculate a %= b.
pub(crate) fn bigint_mod_nct(mut a: &mut [u64], b: &[u64])
{
	a = truncate_slice_mut(a);
	let b = truncate_slice(b);
	if b.len() == 0 || a.len() == 0 {
		//a is 0 or division by zero. Return zero remainder. Note that truncate_slice_mut can only
		//truncate zeroes, so leading digits of a will be zeroes.
		for t in a.iter_mut() { *t = 0; }
		return;
	}
	//Compute magnitude of b. Invariant: Last word of b is always nonzero.
	let bmag = magnitude_of(&b);
	let mut d = Vec::new();
	let mut k = Vec::new();
	loop {
		//Compute magnitude of a. Invariant: Last word of a is always nonzero.
		let amag = magnitude_of(&a);
		//If magnitude of a is less than magnitude of b, we are done.
		if amag < bmag { break; }
		//If magnitude of a is equal to magnitude of b, then compare a and b. If a >= b, the remainder
		//is a-b. If a < b, we are done.
		if amag == bmag {
			let mut lt = false;
			for (a,b) in a.iter().zip(b.iter()) {
				//lt only changes if values are different. If they are the same, keep the
				//existing value.
				if *a > *b { lt = false; }
				if *a < *b { lt = true; }
			}
			if !lt {
				//a >= b, so need one last a -= b. After subtraction, truncate leading zeros.
				bigint_sub(&mut a, &b);
				truncate_slice_mut(a);
			}
			//Done.
			break;
		}
		//If magnitude of a is greater than magnitude of b, chainshift a by magnitude of b, multiply
		//result by b, and subtract from a. The product is guaranteed less than a.
		let dmag = amag - bmag;
		d.resize((dmag+63)/64, 0);
		k.resize(a.len(), 0);
		for (i,w) in d.iter_mut().enumerate() {
			let s1 = bmag/64+i;
			let s2 = bmag/64+i+1;
			*w = 0;
			if s1 < a.len() { *w |= a[s1] >> bmag%64; }
			if s2 < a.len() && bmag%64 != 0 { *w |= a[s2] << 64-bmag%64; }
		}
		bigint_mul(&mut k, &d, b);
		bigint_sub(&mut a, &k);
		a = truncate_slice_mut(a);
	}
	//The leading digits of a will be zeros because truncate_slice_mut() can only truncate zeros.
}



pub(crate) fn bytes_to_words(t: &mut [u64], s: &[u8])
{
	for t in t.iter_mut() { *t = 0; }
	let mut i = 0;
	loop {
		if i/8 >= t.len() || i >= s.len() { break; }	//Reached end.
		t[i/8] |= (s[i] as u64) << i%8*8;
		i += 1;
	}
}

pub(crate) fn bytes_to_words_be(t: &mut [u64], s: &[u8])
{
	for t in t.iter_mut() { *t = 0; }
	let mut i = 0;
	loop {
		if i/8 >= t.len() || i >= s.len() { break; }	//Reached end.
		t[i/8] |= (s[s.len()-i-1] as u64) << i%8*8;
		i += 1;
	}
}

pub(crate) fn words_to_bytes(t: &mut [u8], s: &[u64])
{
	let mut i = 0;
	loop {
		if i/8 >= s.len() || i >= t.len() { break; }	//Reached end.
		t[i] = (s[i/8] >> i%8*8) as u8;
		i += 1;
	}
}
