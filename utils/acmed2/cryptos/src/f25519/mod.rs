#![allow(non_snake_case)]
use super::bigint::bigint_add;
use super::bigint::bigint_mac;
use super::bigint::bigint_mac1;
use super::bigint::bigint_sub;
use super::bigint::words_to_bytes;
use super::Drbg;
use super::hash::Hash;
use super::hash::HashUpdate;
use super::hash::Sha512;
use core::ops::Add;
use core::ops::Mul;
use core::ops::Neg;
use core::ops::Sub;


const MASK: u64 = 0x7FFFFFFFFFFFFu64;

macro_rules! to_words
{
	($wcount:tt $bs:expr) => {{
		let mut tmp = [0u64;$wcount];
		crate::bigint::bytes_to_words(&mut tmp, $bs);
		tmp
	}};
}

#[derive(Copy,Clone,Debug)]
struct F25519([u64;5]);

macro_rules! ge { ($a:expr, $b:expr) => { 1-($a.wrapping_sub($b) >> 63) } }
macro_rules! gt { ($a:expr, $b:expr) => { $b.wrapping_sub($a) >> 63 } }

impl F25519
{
	fn __normalize(&self) -> [u64;5]
	{
		let mut r = [0;5];
		let w = self.0[0];		r[0] = w & MASK;
		let w = self.0[1]+(w>>51);	r[1] = w & MASK;
		let w = self.0[2]+(w>>51);	r[2] = w & MASK;
		let w = self.0[3]+(w>>51);	r[3] = w & MASK;
		let w = self.0[4]+(w>>51);	r[4] = w;
		let need_s = gt!(r[4],MASK) | ge!(r[4],MASK) & ge!(r[3],MASK) & ge!(r[2],MASK) & ge!(r[1],MASK) &
			gt!(r[0],MASK-19);
		let w = need_s * 19;
		let w = r[0]+w;			r[0] = w & MASK;
		let w = r[1]+(w>>51);		r[1] = w & MASK;
		let w = r[2]+(w>>51);		r[2] = w & MASK;
		let w = r[3]+(w>>51);		r[3] = w & MASK;
		let w = r[4]+(w>>51);		r[4] = w & MASK;
		r
	}
	fn zero() -> F25519 { F25519([0;5]) }
	fn one() -> F25519 { F25519([1,0,0,0,0]) }
	fn from_bytes(b: &[u8;32]) -> F25519
	{
		let mut r = [0;5];
		let mut x = [0;8];
		x.copy_from_slice(&b[0..8]);	r[0] = u64::from_le_bytes(x) & MASK;
		x.copy_from_slice(&b[6..14]);	r[1] = (u64::from_le_bytes(x)>>3) & MASK;
		x.copy_from_slice(&b[12..20]);	r[2] = (u64::from_le_bytes(x)>>6) & MASK;
		x.copy_from_slice(&b[19..27]);	r[3] = (u64::from_le_bytes(x)>>1) & MASK;
		x.copy_from_slice(&b[24..32]);	r[4] = (u64::from_le_bytes(x)>>12) & MASK;
		F25519(r)
	}
	fn to_bytes(&self) -> [u8;32]
	{
		let r = self.__normalize();
		let mut out = [0;32];
		let x = (r[0]|r[1]<<51).to_le_bytes(); (&mut out[0..8]).copy_from_slice(&x);
		let x = (r[1]>>13|r[2]<<38).to_le_bytes(); (&mut out[8..16]).copy_from_slice(&x);
		let x = (r[2]>>26|r[3]<<25).to_le_bytes(); (&mut out[16..24]).copy_from_slice(&x);
		let x = (r[3]>>39|r[4]<<12).to_le_bytes(); (&mut out[24..32]).copy_from_slice(&x);
		out
	}
	fn sign(self) -> u64
	{
		let mut r = [0;5];
		let w = self.0[0];		r[0] = w & MASK;
		let w = self.0[1]+(w>>51);	r[1] = w & MASK;
		let w = self.0[2]+(w>>51);	r[2] = w & MASK;
		let w = self.0[3]+(w>>51);	r[3] = w & MASK;
		let w = self.0[4]+(w>>51);	r[4] = w;
		let need_s = gt!(r[4],MASK) | ge!(r[4],MASK) & ge!(r[3],MASK) & ge!(r[2],MASK) & ge!(r[1],MASK) &
			gt!(r[0],MASK-19);
		(r[0] & 1) ^ need_s
	}
	fn square(self) -> F25519 { F25519(raw_sqr(self.0)) }
	fn inv(self) -> F25519
	{
		let mut A = self.square();					//A <- 2
		let C = A*self;							//C <- 3
		let mut B = A.square();						//B <- 4
		B = B.square();							//B <- 8
		let D = B*C;							//D <- 11
		A = D*C;							//A <- 14
		let mut B = A.square(); for _ in 0..2 { B = B.square(); }	//B <- 112
		A = A*B*self;							//A <- 127
		A = A.square();							//A <- 254=2^8-2
		B = A.square(); for _ in 0..6 { B = B.square(); }		//B <- 2^15-2^8
		A = A*B*self;							//A <- 2^15-1
		A = A.square();							//A <- 2^16-2
		B = A.square(); for _ in 0..14 { B = B.square(); }		//B <- 2^31-2^16
		A = A*B*self;							//A <- 2^31-1
		B = A.square(); for _ in 0..30 { B = B.square(); }		//B <- 2^62-2^31
		A = A*B;							//A <- 2^62-1
		A = A.square();							//A <- 2^63-2
		B = A.square(); for _ in 0..61 { B = B.square(); }		//B <- 2^125-2^63
		A = A*B*self;							//A <- 2^125-1
		for _ in 0..5 { A = A.square(); }				//A <- 2^130-2^5
		B = A.square(); for _ in 0..124 { B = B.square(); }		//B <- 2^255-2^130
		A*B*D								//r <- 2^255-21
	}
	//if m=1, return y, if m=0, return x, otherwise soft-UB.
	fn select(x: &F25519, y: &F25519, m: u64) -> F25519
	{
		let mx = m.wrapping_sub(1);
		let my = !mx;
		let mut r = [0;5];
		for i in 0..5 { r[i] = x.0[i] & mx | y.0[i] & my; }
		F25519(r)
	}
	fn cload(&mut self, y: &F25519, m: u64)
	{
		let mx = m.wrapping_sub(1);
		let my = !mx;
		for i in 0..5 { self.0[i] = self.0[i] & mx | y.0[i] & my; }
	}
	//if m=1, swap, if m=0 do nothing. otherwise soft-UB.
	fn cswap(x: &mut F25519, y: &mut F25519, m: u64)
	{
		let m = !(m.wrapping_sub(1));
		for i in 0..5 {
			let d = (x.0[i] ^ y.0[i]) & m;
			x.0[i] ^= d;
			y.0[i] ^= d;
		}
	}
	fn montgomery_x(&self, m: &[u8]) -> F25519
	{
		let mut a1 = M25519::infinity();
		let mut a2 = M25519::new(*self);
		let mut lbit = 0;
		for i in 0..m.len() {
			let mut b = m[m.len() - 1 - i];
			for _ in 0..8 {
				let bit = (b >> 7) as u64;
				F25519::cswap(&mut a1.x, &mut a2.x, bit^lbit);
				F25519::cswap(&mut a1.z, &mut a2.z, bit^lbit);
				let (b1, b2) = a1.diffadd(&a2, self);
				a1 = b1; a2 = b2;
				lbit = bit;
				b <<= 1;
			}
		}
		F25519::cswap(&mut a1.x, &mut a2.x, lbit);
		F25519::cswap(&mut a1.z, &mut a2.z, lbit);
		a1.to_affine()
	}
}

impl Mul for F25519
{
	type Output = F25519;
	fn mul(self, x: F25519) -> F25519 { F25519(raw_mul(self.0, x.0)) }
}

impl Mul<u64> for F25519
{
	type Output = F25519;
	fn mul(self, x: u64) -> F25519 { F25519(raw_smul(self.0, x)) }
}

impl Add for F25519
{
	type Output = F25519;
	fn add(self, x: F25519) -> F25519
	{
		let mut r = [0;5];
		let w = self.0[0] + x.0[0];		r[0] = w & MASK;
		let w = self.0[1] + x.0[1] + (w>>51);	r[1] = w & MASK;
		let w = self.0[2] + x.0[2] + (w>>51);	r[2] = w & MASK;
		let w = self.0[3] + x.0[3] + (w>>51);	r[3] = w & MASK;
		let w = self.0[4] + x.0[4] + (w>>51);	r[4] = w & MASK;
		let w = r[0] + 19 * (w>>51);		r[0] = w;
		F25519(r)
	}
}

impl Sub for F25519
{
	type Output = F25519;
	fn sub(self, x: F25519) -> F25519
	{
		let mut r = [0;5];
		//The bias term is 2^256-38, so no effect on result.
		let w = 2*MASK-36 + self.0[0] - x.0[0];			r[0] = w & MASK;
		let w = 2*MASK    + self.0[1] - x.0[1] + (w>>51);	r[1] = w & MASK;
		let w = 2*MASK    + self.0[2] - x.0[2] + (w>>51);	r[2] = w & MASK;
		let w = 2*MASK    + self.0[3] - x.0[3] + (w>>51);	r[3] = w & MASK;
		let w = 2*MASK    + self.0[4] - x.0[4] + (w>>51);	r[4] = w & MASK;
		let w = r[0] + 19 * (w>>51);				r[0] = w;
		F25519(r)
	}
}

impl Neg for F25519
{
	type Output = F25519;
	fn neg(self) -> F25519
	{
		let mut r = [0;5];
		//The bias term is 2^256-38, so no effect on result.
		let w = 2*MASK-36 - self.0[0];			r[0] = w & MASK;
		let w = 2*MASK    - self.0[1] + (w>>51);	r[1] = w & MASK;
		let w = 2*MASK    - self.0[2] + (w>>51);	r[2] = w & MASK;
		let w = 2*MASK    - self.0[3] + (w>>51);	r[3] = w & MASK;
		let w = 2*MASK    - self.0[4] + (w>>51);	r[4] = w & MASK;
		let w = r[0] + 19 * (w>>51);			r[0] = w;
		F25519(r)
	}
}

impl PartialEq for F25519
{
	fn eq(&self, x: &Self) -> bool { self.__normalize() == x.__normalize() }
}

///X25519 key agreement.
///
/// * `private`: a X25519 private key.
/// * `public`: peer's public key.
/// * Returns: the corresponding X25519 shared secret.
pub fn x25519(private: &[u8;32], public: &[u8;32]) -> [u8;32]
{
	F25519::from_bytes(public).montgomery_x(private).to_bytes()
}

///X25519 key generation.
///
/// * `private`: a X25519 private key.
/// * Returns: the corresponding X25519 public key.
pub fn x25519g(private: &[u8;32]) -> [u8;32]
{
	F25519([9,0,0,0,0]).montgomery_x(private).to_bytes()
}

#[derive(Copy,Clone,Debug)]
struct E25519
{
	x: F25519,
	y: F25519,
	z: F25519,
	t: F25519,
}

impl E25519
{
	fn new() -> E25519
	{
		E25519{x: F25519::zero(), y: F25519::one(), z: F25519::one(), t: F25519::zero()}
	}
	fn stdbase() -> E25519
	{
		let gx = F25519([1738742601995546,1146398526822698,2070867633025821,562264141797630,587772402128613]);
		let gy = F25519([1801439850948184,1351079888211148,450359962737049,900719925474099,1801439850948198]);
		E25519{x: gx, y: gy, z: F25519::one(), t: gx*gy}
	}
	fn encode(&self) -> [u8;32]
	{
		let (x,y) = self.to_affine();
		let mut e = y.to_bytes();
		e[31] |= (x.sign() as u8) << 7;
		e
	}
	fn decode(p: &[u8;32]) -> Option<E25519>
	{
		let d = F25519([929955233495203,466365720129213,1662059464998953,2033849074728123,1442794654840575]);
		let f = F25519([1718705420411056,234908883556509,2233514472574048,2117202627021982,765476049583133]);
		let y = F25519::from_bytes(p);					//Ignores the sign bit.
		let y2 = y.square();
		let u = y2 - F25519::one();
		let v = d*y2 + F25519::one();
		let w = u * v;
		let mut A = w.square();						//A <- 2
		let mut B = A.square();						//B <- 4
		A = A*B*w;							//A <- 7
		A = A.square();							//A <- 14
		B = A.square(); for _ in 0..2 { B = B.square(); }		//B <- 112
		A = A*B*w;							//A <- 2^7-1
		A = A.square();							//A <- 2^8-2^1
		B = A.square(); for _ in 0..6 { B = B.square(); }		//B <- 2^15-2^8
		A = A*B*w;							//A <- 2^15-1
		A = A.square();							//A <- 2^16-2^1
		B = A.square(); for _ in 0..14 { B = B.square(); }		//B <- 2^31-2^16
		A = A*B*w;							//A <- 2^31-1
		B = A.square(); for _ in 0..30 { B = B.square(); }		//B <- 2^62-2^31
		A = A*B;							//A <- 2^62-1
		A = A.square();							//A <- 2^63-2^1
		B = A.square(); for _ in 0..61 { B = B.square(); }		//B <- 2^125-2^63
		A = A*B*w;							//A <- 2^125-1
		A = A.square(); A = A.square();					//A <- 2^127-2^2
		B = A.square(); for _ in 0..124 { B = B.square(); }		//B <- 2^252-2^127
		let mut x = u*A*B*w;						//A <- 2^252-3
		if v*x.square() == -u { x = x * f; }				//Fixup.
		else if v*x.square() != u { return None; }			//No solution!
		if x == F25519::zero() && p[31] >> 7 != 0 { return None; }	//If x=0 then sign must be +.
		let x = F25519::select(&x, &-x, x.sign() ^ (p[31] as u64) >> 7);//Flip sign to match.
		Some(E25519{x: x, y: y, z: F25519::one(), t: x*y})
	}
	fn double(&self) -> E25519
	{
		let (x, y, z, t) = ed25519_dbl(self.x, self.y, self.z, self.t);
		E25519{x, y, z, t}
	}
	fn to_affine(&self) -> (F25519, F25519)
	{
		let z = self.z.inv();
		(self.x*z, self.y*z)
	}
}

impl<'a> Mul<&'a [u64]> for E25519
{
	type Output = E25519;
	fn mul(self, a: &'a [u64]) -> E25519
	{
		let mut X = [E25519::new();16];
		X[1] = self;
		for i in 1..8 { X[2*i] = X[i].double(); X[2*i+1] = X[2*i] + self; }
		let mut r = E25519::new();
		for i in 0..a.len() { for j in 0..16 {
			for _ in 0..4 { r = r.double(); }
			if a.len()-i-1 >= a.len() { continue; }		//Can not happen.
			let w = a[a.len()-i-1] >> (15-j)*4;
			//Load X[w] and add to r.
			let mut s = E25519::new();
			for i in 1..16 {
				let f = w^i;
				let f = (f>>2)|f;
				let f = ((f>>1)|f)&1;
				let f = 1-f;
				let X = &X[i as usize];
				s.x.cload(&X.x, f);
				s.y.cload(&X.y, f);
				s.z.cload(&X.z, f);
				s.t.cload(&X.t, f);
			}
			r = r + s;
		}}
		r
	}
}

impl Add for E25519
{
	type Output = E25519;
	fn add(self, x: E25519) -> E25519
	{
		let (x, y, z, t) = ed25519_add(self.x, self.y, self.z, self.t, x.x, x.y, x.z, x.t);
		E25519{x, y, z, t}
	}
}

impl Neg for E25519
{
	type Output = E25519;
	fn neg(self) -> E25519
	{
		E25519{x: -self.x, y: self.y, z: self.z, t: -self.t}
	}
}

impl PartialEq for E25519
{
	fn eq(&self, x: &Self) -> bool { self.x*x.z == self.z*x.x && self.y*x.z == self.z*x.y }
}

#[derive(Copy,Clone,Debug)]
struct M25519
{
	x: F25519,
	z: F25519,
}

impl M25519
{
	fn new(x: F25519) -> M25519 { M25519{x: x, z: F25519::one()} }
	fn infinity() -> M25519 { M25519{x: F25519::one(), z: F25519::zero()} }
	fn to_affine(&self) -> F25519 { self.z.inv() * self.x }
	//Returns (2*self, self+x), where d = x-self.
	fn diffadd(&self, x: &M25519, d: &F25519) -> (M25519,M25519)
	{
		let (x, z, u, v) = x25519_diffadd(*d, self.x, self.z, x.x, x.z);
		(M25519{x: x, z: z}, M25519{x: u, z: v})
	}
}

///Ed25519 private key.
pub struct Ed25519PrivateKey
{
	a: [u64;4],
	A: [u8;32],
}

impl Ed25519PrivateKey
{
	///Load 32-byte key for Ed25519.
	///
	/// * `a`: The raw Ed25519 private key.
	///
	///Note: The Ed25519 private key is genrated by taking 256 random bits.
	pub fn new(a: [u8;32]) -> Ed25519PrivateKey
	{
		let a = to_words!(4 &a);
		let A = E25519::stdbase() * &a;
		Ed25519PrivateKey {
			a: a,
			A: A.encode(),
		}
	}
	///Return public key corresponding to this private key.
	///
	///Note: The value returned is directly suitable for inner contents of the
	///subjectPublicKeyInfo.subjectPublicKey field.
	pub fn pubkey(&self) -> [u8;32] { self.A }
	///Sign message.
	///
	/// * `self`: The private key to sign with.
	/// * `drbg`: DRBG context.
	///   * Repeating r for two different messages has CATASTROPHIC impact.
	/// * `hramf`: The message to sign.
	/// * Returns: The signature.
	pub fn sign_nonce(&self, drbg: &mut (impl Drbg+?Sized), hramf: impl Fn(&mut dyn HashUpdate)) -> [u8;64]
	{
		let mut r = [0;32];
		drbg.fill(&mut r);
		let r = to_words!(4 &r);
		let R = E25519::stdbase() * &r;
		let Renc = R.encode();
		let mut hram = Sha512::new();
		hram.update(&Renc);
		hram.update(&self.A);
		hramf(&mut hram);
		let hram = to_words!(8 &hram.hash());
		let s = ed25519_scalar(r, hram, self.a);
		let mut ret = [0;64];
		(&mut ret[..32]).copy_from_slice(&Renc);
		words_to_bytes(&mut ret[32..], &s);
		ret
	}
}

///Error verifying Ed25519 signature.
#[derive(Copy,Clone,Debug)]
pub enum LEd25519VerifyError
{
	///Invalid key.
	InvalidKey,
	///Invalid signature.
	InvalidSignature,
	///Signature does not correspond to message.
	VerifyFailed,
}

///Verify Ed25519 signature.
pub fn ed25519_verify(key: &[u8;32], signature: &[u8;64], hramf: impl Fn(&mut dyn HashUpdate)) ->
	Result<(), LEd25519VerifyError>
{
	let A = E25519::decode(key).ok_or(LEd25519VerifyError::InvalidKey)?;
	let mut R = [0;32]; R.copy_from_slice(&signature[..32]);
	let s = to_words!(4 &signature[32..]);
	let R = E25519::decode(&R).ok_or(LEd25519VerifyError::InvalidSignature)?;
	let (g1, e1) = (s[3] > 0x1000000000000000, s[3] == 0x1000000000000000);
	let (g2, e2) = (s[2] > 0, s[2] == 0);
	let (g3, e3) = (s[1] > 0x14def9dea2f79cd6, s[1] == 0x14def9dea2f79cd6);
	let g4 = s[0] >= 0x5812631a5cf5d3ed;
	if g1 || (e1 && g2) || (e1 && e2 && g3) || (e1 && e2 && e3 && g4) {
		return Err(LEd25519VerifyError::InvalidSignature);
	}
	let mut hram = Sha512::new();
	hram.update(&signature[..32]);
	hram.update(key);
	hramf(&mut hram);
	let hram = to_words!(8 &hram.hash());
	if E25519::stdbase() * &s == R + A * &hram { Ok(()) } else { Err(LEd25519VerifyError::VerifyFailed) }
}

//Calculate Ed25519 scalar.
fn ed25519_scalar(r: [u64;4], hram: [u64;8], a: [u64;4]) -> [u64;4]
{
	//First, partially reduce hram so it fits into 4 words.
	let hram = partial_reduce_l(hram);
	//Calculate r+hram*a and reduce it.
	let mut rhrama = [0;8];
	(&mut rhrama[..4]).copy_from_slice(&r);
	bigint_mac(&mut rhrama, &hram, &a);
	full_reduce_l(partial_reduce_l(rhrama))
}

macro_rules! M
{
	($a:expr,$b:expr) => { ($a as u128) * ($b as u128) }
}

//Do first round of partial reduction mod l. The input is <2^512. The output is <2^385.
fn partial_reduce_l_round1(w: [u64;8]) -> [u64;7]
{
	let mut r = [11819153939886771969u64,14991950615390032711u64,14910419812499177061u64,259310039853996605u64,
		9306180268226068176u64,5615880889498717541u64,1u64];
	let mut s = [0,0,w[4],w[5],w[6],w[7],0];	//2^128*w_h term.
	bigint_mac(&mut s, &[9306180268226068176,5615880889498717541], &w[4..8]);
	bigint_add(&mut r, &w[..4]);
	bigint_sub(&mut r, &s);
	r
}

//Do second round of partial reduction mod l. The input is <2^385. The output is <2^258.
fn partial_reduce_l_round2(w: [u64;7]) -> [u64;5]
{
	let mut r = [8287822139597571298u64,7824158307293051690u64,3u64,11529215046068469760u64,2u64];
	let mut s = [0,0,w[4],w[5],w[6]];	//2^128*w_h term.
	bigint_mac(&mut s, &[9306180268226068176,5615880889498717541], &w[4..7]);
	bigint_add(&mut r, &w[..4]);
	bigint_sub(&mut r, &s);
	r
}

//Do third round of partial reduction mod l. The input is <2^258. The output is <2^253.
fn partial_reduce_l_round3(w: [u64;5]) -> [u64;4]
{
	let h = w[3] >> 60 | w[4] << 4;
	let mut r = [6346243789798364141u64,1503914060200516822u64,0u64,1152921504606846976u64];
	let mut s = [0;4];
	bigint_mac1(&mut s, &[6346243789798364141,1503914060200516822], h);
	bigint_add(&mut r, &w[0..4]);
	//The upper bits of w[3] are there, this is a dirty hack to discard them.
	r[3] = r[3].wrapping_sub(w[3] & 0xF000000000000000);
	bigint_sub(&mut r, &s);
	r
}

//Takes in number <2^512 and partially reduces it mod l (result is <2^253)
fn partial_reduce_l(num: [u64;8]) -> [u64;4]
{
	partial_reduce_l_round3(partial_reduce_l_round2(partial_reduce_l_round1(num)))
}

fn is_lt(a: u64, b: u64) -> u64 { ((a as u128).wrapping_sub(b as u128) >> 127) as u64 }
fn is_ng(a: u64, b: u64) -> u64 { 1-((b as u128).wrapping_sub(a as u128) >> 127) as u64 }

//Takes in number <2^253 and fully reduces it mod l.
fn full_reduce_l(mut r: [u64;4]) -> [u64;4]
{
	let b = [6346243789798364141u64,1503914060200516822u64,0u64,1152921504606846976u64];
	//Do chained compare of r to b.
	let rltb = is_lt(r[3], b[3]) |
		is_ng(r[3], b[3]) & is_lt(r[2], b[2]) |
		is_ng(r[3], b[3]) & is_ng(r[2], b[2]) & is_lt(r[1], b[1]) |
		is_ng(r[3], b[3]) & is_ng(r[2], b[2]) & is_ng(r[1], b[1]) & is_lt(r[0], b[0]);
	let mask = rltb.wrapping_sub(1);
	let mut carry = 0;
	for i in 0..4 {
		let u = (r[i] as u128).wrapping_sub((b[i] & mask) as u128).wrapping_sub(carry);
		r[i] = u as u64;
		carry = u >> 127;
	}
	r
}

fn raw_mul(a: [u64;5], b: [u64;5]) -> [u64;5]
{
	let c = [0, 19*a[1], 19*a[2], 19*a[3], 19*a[4]];
	let mut r = [0;5];
	//level-0 terms.
	let w = M!(a[0],b[0])+M!(c[1],b[4])+M!(c[2],b[3])+M!(c[3],b[2])+M!(c[4],b[1]);
	r[0] = (w as u64) & MASK;
	//level-1 terms.
	let w = M!(a[0],b[1])+M!(a[1],b[0])+M!(c[2],b[4])+M!(c[3],b[3])+M!(c[4],b[2])+(w>>51);
	r[1] = (w as u64) & MASK;
	//level-2 terms.
	let w = M!(a[0],b[2])+M!(a[1],b[1])+M!(a[2],b[0])+M!(c[3],b[4])+M!(c[4],b[3])+(w>>51);
	r[2] = (w as u64) & MASK;
	//level-3 terms.
	let w = M!(a[0],b[3])+M!(a[1],b[2])+M!(a[2],b[1])+M!(a[3],b[0])+M!(c[4],b[4])+(w>>51);
	r[3] = (w as u64) & MASK;
	//level-4 terms.
	let w = M!(a[0],b[4])+M!(a[1],b[3])+M!(a[2],b[2])+M!(a[3],b[1])+M!(a[4],b[0])+(w>>51);
	r[4] = (w as u64) & MASK;
	let w = (w >> 51) as u64 * 19 + r[0];
	r[0] = (w as u64) & MASK;
	r[1] += w >> 51;
	r
}

fn raw_smul(a: [u64;5], b: u64) -> [u64;5]
{
	let mut r = [0;5];
	//level-0 terms.
	let w = M!(a[0],b);		r[0] = (w as u64) & MASK;
	//level-1 terms.
	let w = M!(a[1],b)+(w>>51);	r[1] = (w as u64) & MASK;
	//level-2 terms.
	let w = M!(a[2],b)+(w>>51);	r[2] = (w as u64) & MASK;
	//level-3 terms.
	let w = M!(a[3],b)+(w>>51);	r[3] = (w as u64) & MASK;
	//level-4 terms.
	let w = M!(a[4],b)+(w>>51);	r[4] = (w as u64) & MASK;
	let w = (w >> 51) as u64 * 19 + r[0];
	r[0] = (w as u64) & MASK;
	r[1] += w >> 51;
	r
}

fn raw_sqr(a: [u64;5]) -> [u64;5]
{
	let c = [0, 19*a[1], 19*a[2], 19*a[3], 19*a[4]];
	let d = [0, 2*a[1], 2*a[2], 2*a[3], 2*a[4]];
	let mut r = [0;5];
	//level-0 terms.
	let w = M!(a[0],a[0])+M!(c[1],d[4])+M!(c[2],d[3]);
	r[0] = (w as u64) & MASK;
	//level-1 terms.
	let w = M!(a[0],d[1])+M!(c[2],d[4])+M!(c[3],a[3])+(w>>51);
	r[1] = (w as u64) & MASK;
	//level-2 terms.
	let w = M!(a[0],d[2])+M!(a[1],a[1])+M!(c[3],d[4])+(w>>51);
	r[2] = (w as u64) & MASK;
	//level-3 terms.
	let w = M!(a[0],d[3])+M!(a[1],d[2])+M!(c[4],a[4])+(w>>51);
	r[3] = (w as u64) & MASK;
	//level-4 terms.
	let w = M!(a[0],d[4])+M!(a[1],d[3])+M!(a[2],a[2])+(w>>51);
	r[4] = (w as u64) & MASK;
	let w = (w >> 51) as u64 * 19 + r[0];
	r[0] = (w as u64) & MASK;
	r[1] += w >> 51;
	r
}

fn ed25519_add(X1: F25519, Y1: F25519, Z1: F25519, T1: F25519, X2: F25519, Y2: F25519, Z2: F25519, T2: F25519) ->
	(F25519, F25519, F25519, F25519)
{
	//k=2*d.
	let k = F25519([1859910466990425,932731440258426,1072319116312658,1815898335770999,633789495995903]);
	let A = (Y1-X1)*(Y2-X2);
	let B = (Y1+X1)*(Y2+X2);
	let C = T1*T2*k;
	let D = Z1*(Z2+Z2);
	let (E,H) = (B-A,B+A);
	let (F,G) = (D-C,D+C);
	(E*F, G*H, F*G, E*H)
}

fn ed25519_dbl(X1: F25519, Y1: F25519, Z1: F25519, _: F25519) -> (F25519, F25519, F25519, F25519)
{
	let A = X1.square();
	let B = Y1.square();
	let t0 = Z1.square();
	let C = t0+t0;
	let t3 = (X1+Y1).square()-A;
	let (G,H) = (A-B,A+B);
	let (E,F) = (B-t3,G+C);
	(E*F, G*H, F*G, E*H)
}

//P1 difference.
//P2 point A.
//P3 point B.
//(double,sum)
fn x25519_diffadd(X1: F25519, X2: F25519, Z2: F25519, X3: F25519, Z3: F25519) ->
	(F25519, F25519, F25519, F25519)
{
	let A = X2+Z2;
	let AA = A.square();
	let B = X2-Z2;
	let BB = B.square();
	let E = AA-BB;
	let (C,D) = (X3+Z3,X3-Z3);
	let (DA,CB) = (D*A, C*B);
	let (t0,t1) = (DA+CB, DA-CB);
	let t4 = BB+E*121666u64;
	(AA*BB, E*t4, t0.square(), X1*t1.square())
}

#[cfg(test)] mod test;
