use super::*;

#[test]
fn encode_decode_ignore_high()
{
	let x = [0xED, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
		 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF];
	let y = F25519::from_bytes(&x);
	let z = y.to_bytes();
	assert_eq!(z, [0;32]);
}

#[test]
fn encode_decode_mod_p()
{
	let x = [0xED, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
		 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F];
	let y = F25519::from_bytes(&x);
	let z = y.to_bytes();
	assert_eq!(z, [0;32]);
}

#[test]
fn encode_decode_max()
{
	let x = [0xEC, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
		 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F];
	let y = F25519::from_bytes(&x);
	let z = y.to_bytes();
	assert_eq!(z, x);
}

#[test]
fn encode_over_0()
{
	let y = F25519([MASK+1,MASK,MASK,MASK,MASK]);
	let z = y.to_bytes();
	assert_eq!(z, [19,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
}

#[test]
fn encode_over_1()
{
	let y = F25519([MASK,MASK+1,MASK,MASK,MASK]);
	let z = y.to_bytes();
	assert_eq!(z, [18,0,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
}

#[test]
fn minus_1_times_minus_1()
{
	let a = F25519([MASK-19,MASK,MASK,MASK,MASK]);
	let b = a * a;
	let c = b.to_bytes();
	assert_eq!(c, [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
}

#[test]
fn minus_1_square()
{
	let a = F25519([MASK-19,MASK,MASK,MASK,MASK]);
	let b = a.square();
	let c = b.to_bytes();
	assert_eq!(c, [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
}

#[test]
fn minus_zero()
{
	let x = F25519([0;5]);
	assert_eq!((-x).to_bytes(), [0;32]);
}

#[test]
fn random_field_laws()
{
	use std::io::Read;
	let mut x = [0;32];
	let mut fp = std::fs::File::open("/dev/urandom").expect("Failed to open urandom");
	let z = F25519([0;5]);
	let o = F25519([1,0,0,0,0]);
	for _ in 0..10000 {
		fp.read_exact(&mut x).expect("Failed to read urandom"); let a = F25519::from_bytes(&x);
		fp.read_exact(&mut x).expect("Failed to read urandom"); let b = F25519::from_bytes(&x);
		fp.read_exact(&mut x).expect("Failed to read urandom"); let c = F25519::from_bytes(&x);
		//The sum better be associative, commutative, have neutral element and have inverse.
		assert_eq!((a+b)+c, a+(b+c));
		assert_eq!(a+b, b+a);
		assert_eq!(a+z, a);
		assert_eq!(a+(-a), z);
		//The field better be distributive.
		assert_eq!((a+b)*c, a*c+b*c);
		//The product better be associative, commutative, have neutral element and have inverse.
		assert_eq!((a*b)*c, a*(b*c));
		assert_eq!(a*b, b*a);
		assert_eq!(a*o, a);
		assert_eq!(a*1u64, a);
		assert_eq!(a*a.inv(), o);
		//Square.
		assert_eq!(a*a, a.square());
		//Properties of minus and negation.
		assert_eq!(b-z, b);
		assert_eq!(z-b, -b);
		assert_eq!(a-b, a+(-b));
		assert_eq!((a-b)+(b-a), z);
		assert_eq!(-(a-b), b-a);
		assert_eq!(a-a, z);
		assert_eq!(a+b-b, a);
		assert_eq!(a+b-a, b);
		//Properties of inverse.
		assert_eq!(a*b*a.inv(), b);
		assert_eq!(a*b*b.inv(), a);
		//Small multiply.
		let ds = c.0[0];
		let dl = F25519([ds,0,0,0,0]);
		assert_eq!(a*ds, a*dl);
	}
}

#[test]
fn edwards25519_basepoint_on_curve()
{
	let o = F25519::one();
	let gx = F25519([1738742601995546,1146398526822698,2070867633025821,562264141797630,587772402128613]);
	let gy = F25519([1801439850948184,1351079888211148,450359962737049,900719925474099,1801439850948198]);
	let d = F25519([929955233495203,466365720129213,1662059464998953,2033849074728123,1442794654840575]);
	assert_eq!(-gx.square() + gy.square(), o + d * gx.square() * gy.square());
}

#[test]
fn edwards25519_add_neg()
{
	let a = E25519::stdbase();
	let b = -E25519::stdbase();
	let r = a+b;
	assert_eq!(r, E25519::new());
}

#[test]
fn edwards25519_double_add()
{
	let g = E25519::stdbase();
	let r = g+g;
	let s = g.double();
	assert_eq!(r,s);
	assert_eq!(r.to_affine(),s.to_affine());
}

#[test]
fn edwards25519_basepoint_order()
{
	let mut g = E25519::stdbase();
	let mut r = E25519::new();
	let ord: [u8;32] = [0xED,0xD3,0xF5,0x5C,0x1A,0x63,0x12,0x58,0xD6,0x9C,0xF7,0xA2,0xDE,0xF9,0xDE,0x14,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0x10];
	for i in 0..256 {
		if ord[i/8] >> i%8 & 1 != 0 { r = r + g; }
		let gc = g.double();
		g = g + g;
		assert_eq!(g,gc);
	}
	assert!(g != E25519::new());
	assert_eq!(r, E25519::new());
}

#[test]
fn curve25519_basepoint_order()
{
	let g = F25519([9,0,0,0,0]);
	let ord: [u8;32] = [0xED,0xD3,0xF5,0x5C,0x1A,0x63,0x12,0x58,0xD6,0x9C,0xF7,0xA2,0xDE,0xF9,0xDE,0x14,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0x10];
	let z = g.montgomery_x(&ord);
	assert_eq!(z, F25519::zero());
}

#[test]
fn curve25519()
{
	use std::io::Read;
	let mut fp = std::fs::File::open("/dev/urandom").expect("Failed to open urandom");
	for _ in 0..1000 {
		let mut ask = [0;32];
		let mut bsk = [0;32];
		fp.read_exact(&mut ask).expect("Failed to read urandom");
		fp.read_exact(&mut bsk).expect("Failed to read urandom");
		let apk = x25519g(&ask);
		let bpk = x25519g(&bsk);
		let ass = x25519(&ask, &bpk);
		let bss = x25519(&bsk, &apk);
		assert_eq!(ass, bss);
	}
}

#[test]
fn edwards25519_basepoint_encode()
{
	let g = E25519::stdbase();
	let ge = g.encode();
	let g2 = E25519::decode(&ge).expect("Failed to decode basepoint");
	assert_eq!(g, g2);
}

#[test]
fn preduce_round1a()
{
	let y = [7022417605932793135,4981313948026604336,5873672025814030855,6653815903065541375,
		11775011825194660448,15415474666465260620,15117184815993767565,7246205144592064052];
	let z = partial_reduce_l_round1(y);
	assert_eq!(z, [14942079905582456368,6989789666529637861,17720254303944481235,9507850259746374200,
		14136594931529797103,14610403145413749213,0]);
}

#[test]
fn preduce_round1b()
{
	let y = [4217024478530784321,3197554456665600022,14606970768017525428,10519797948440675454,
		18394321258024848185,12857040809659057787,560072100907679260,488023684478990163];
	let z = partial_reduce_l_round1(y);
	assert_eq!(z, [17011940922192981746,688485246566106479,13443924009850488845,2227904737836210080,
		10065408929395882429,4979284488211976134,1]);
}

#[test]
fn preduce_round2a()
{
	let y = [3274849601533426647,659230912764042927,530084175682910446,11481310849734241049,
		12016185156540770046,7370184520477718447,0];
	let z = partial_reduce_l_round2(y);
	assert_eq!(z, [2443548478266691161,16334596626561254452,14907088775751954802,13396580729259567626,2]);
}

#[test]
fn preduce_round2b()
{
	let y = [16724121928982929069,8423922126770060981,17289872208812188877,17477898016013384195,
		6317806675675290841,8107389009230647502,1];
	let z = partial_reduce_l_round2(y);
	assert_eq!(z, [10804277578189038911,4828923868695380121,17988928975181997702,12815649816128669353,1]);
}

#[test]
fn preduce_round3a()
{
	let y = [17772858391521285589,9432955130080659517,4524304918852494729,5128764914148232775,2];
	let z = partial_reduce_l_round3(y);
	assert_eq!(z, [17015254633093160046,12136195244191225583,4524304918852494726,1670000400327691847]);
}

#[test]
fn preduce_round3b()
{
	let y = [11887696415336221416,17432934028947982406,1282782816876715249,8824648468551065586,3];
	let z = partial_reduce_l_round3(y);
	assert_eq!(z, [1231925092996486890,10008551072958280464,1282782816876715245,1907119440909983730]);
}

#[test]
fn preduce_roundxa()
{
	let y = [11546156176632840708,6357475624714365769,8645488713285986749,18010254893107429520,
		12464079822453853092,5689054471271691630,9894762547679628988,9301236003601119551];
	let z = partial_reduce_l(y);
	assert_eq!(z, [10612689015043483915,3296971918393768796,9904903103546652934,1258804684796339992]);
}

#[test]
fn preduce_e25519()
{
	use std::io::Read;
	let mut x = [0;64];
	let mut fp = std::fs::File::open("/dev/urandom").expect("Failed to open urandom");
	for _ in 0..1000 {
		fp.read_exact(&mut x).expect("Failed to read urandom");
		let y = to_words!(8 &x);
		let z = partial_reduce_l(y);
		let mut g = E25519::stdbase();
		let mut r1 = E25519::new();
		let mut r2 = E25519::new();
		for i in 0..512 {
			if y[i/64] >> i%64 & 1 != 0 { r1 = r1 + g; }
			if i<256 { if z[i/64] >> i%64 & 1 != 0 { r2 = r2 + g; } }
			g = g.double();
		}
		assert_eq!(r1, r2);
	}
}

#[test]
fn freduce_e25519()
{
	use std::io::Read;
	let mut x = [0;32];
	let mut fp = std::fs::File::open("/dev/urandom").expect("Failed to open urandom");
	for _ in 0..1000 {
		fp.read_exact(&mut x).expect("Failed to read urandom");
		let mut y = to_words!(4 &x);
		y[3] &= 0x1FFFFFFFFFFFFFFF;
		let z = full_reduce_l(y);
		let mut g = E25519::stdbase();
		let mut r1 = E25519::new();
		let mut r2 = E25519::new();
		for i in 0..256 {
			if y[i/64] >> i%64 & 1 != 0 { r1 = r1 + g; }
			if z[i/64] >> i%64 & 1 != 0 { r2 = r2 + g; }
			g = g.double();
		}
		//Not quite correct, but extremely unlikely to fail.
		assert!(z[3] < 1152921504606846976);
		assert_eq!(r1, r2);
	}
}

#[cfg(test)]
fn ed25519_mul(a: &[u64], mut G: E25519) -> E25519
{
	let mut r = E25519::new();
	for i in 0..64*a.len() {
		if a[i/64] >> i%64 & 1 != 0 { r = r + G; }
		G = G.double();
	}
	r
}

#[test]
fn test_ed25519_scalar()
{
	use std::io::Read;
	let mut x = [0;128];
	let mut fp = std::fs::File::open("/dev/urandom").expect("Failed to open urandom");
	for _ in 0..1000 {
		fp.read_exact(&mut x).expect("Failed to read urandom");
		let r = to_words!(4 &x[0..32]);
		let a = to_words!(4 &x[32..64]);
		let hram = to_words!(8 &x[64..128]);
		let s = ed25519_scalar(r, hram, a);
		let A = ed25519_mul(&a, E25519::stdbase());
		let hramA = ed25519_mul(&hram, A);
		let R = ed25519_mul(&r, E25519::stdbase());
		let RhramA = R+hramA;
		let sG = ed25519_mul(&s, E25519::stdbase());
		assert_eq!(sG, RhramA);
	}
}

#[test]
fn test_e25519_mul_by1()
{
	let G = E25519::stdbase();
	for i in 0..256 {
		let r = [i];
		assert_eq!(G * &r, ed25519_mul(&r,G));
	}
}

#[test]
fn test_e25519_mul_by()
{
	use std::io::Read;
	let mut x = [0;32];
	let mut fp = std::fs::File::open("/dev/urandom").expect("Failed to open urandom");
	for _ in 0..1000 {
		fp.read_exact(&mut x).expect("Failed to read urandom");
		let G = E25519::stdbase();
		let r = to_words!(4 &x);
		assert_eq!(G * &r, ed25519_mul(&r,G));
	}
}

#[test]
fn test_e25519_sign()
{
	use crate::DualEcDrbg;
	use std::io::Read;
	let mut key = [0;32];
	let mut nonce = [0;48];
	let mut fp = std::fs::File::open("/dev/urandom").expect("Failed to open urandom");
	fp.read_exact(&mut nonce).expect("Failed to read urandom");
	let mut rng = DualEcDrbg::new(&nonce);
	for _ in 0..1000 {
		fp.read_exact(&mut key).expect("Failed to read urandom");
		let privkey = Ed25519PrivateKey::new(key);
		let pubkey = privkey.pubkey();
		let sig = privkey.sign_nonce(&mut rng, |h|h.update(b"Test"));
		ed25519_verify(&pubkey, &sig, |h|h.update(b"Test")).expect("Verify failed");
	}
}

#[test]
fn ed25519_testvector()
{
	let key = b"\xec\x17\x2b\x93\xad\x5e\x56\x3b\xf4\x93\x2c\x70\xe1\x24\x50\x34\xc3\x54\x67\xef\x2e\xfd\
		\x4d\x64\xeb\xf8\x19\x68\x34\x67\xe2\xbf";
	let signature = b"\xdc\x2a\x44\x59\xe7\x36\x96\x33\xa5\x2b\x1b\xf2\x77\x83\x9a\x00\x20\x10\x09\xa3\xef\
		\xbf\x3e\xcb\x69\xbe\xa2\x18\x6c\x26\xb5\x89\x09\x35\x1f\xc9\xac\x90\xb3\xec\xfd\xfb\xc7\xc6\
		\x64\x31\xe0\x30\x3d\xca\x17\x9c\x13\x8a\xc1\x7a\xd9\xbe\xf1\x17\x73\x31\xa7\x04";
	let message = b"\xdd\xaf\x35\xa1\x93\x61\x7a\xba\xcc\x41\x73\x49\xae\x20\x41\x31\x12\xe6\xfa\x4e\x89\
		\xa9\x7e\xa2\x0a\x9e\xee\xe6\x4b\x55\xd3\x9a\x21\x92\x99\x2a\x27\x4f\xc1\xa8\x36\xba\x3c\x23\
		\xa3\xfe\xeb\xbd\x45\x4d\x44\x23\x64\x3c\xe8\x0e\x2a\x9a\xc9\x4f\xa5\x4c\xa4\x9f";
	ed25519_verify(&key, &signature, |h|h.update(message)).expect("Verify failed");
  
}
