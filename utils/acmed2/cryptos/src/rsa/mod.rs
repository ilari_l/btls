use super::bigint::bigint_mul;
use super::bigint::bigint_mod_nct;
use super::hash::Hash;
use super::hash::HashUpdate;
use super::hash::Sha256;
use super::hash::Sha384;
use super::hash::Sha512;
use core::cmp::min;

fn truncate_slice<'a>(x: &'a[u64]) -> &'a [u64]
{
	let mut last_nz = 0;
	for (idx, v) in x.iter().cloned().enumerate() { if v != 0 { last_nz = idx + 1; } }
	&x[..last_nz]
}

//Compute m=c^e (mod n).
fn rsa_power(m: &mut [u64], c: &[u64], n: &[u64], mut e: u64) -> Result<(), VerifyError>
{
	use self::VerifyError::*;
	let c = truncate_slice(c);
	let n = truncate_slice(n);
	//Sanity checks:
	//N: Must be odd, at least 2048 bits, and at most 4096 bits.
	if n.len() < 32 || (n.len() == 32 && n[n.len()-1] >> 63 == 0) { return Err(InsufficientSecurity); }
	if n[0] & 1 == 0 || n[n.len()-1] >> 63 == 0 { return Err(UnsupportedKey); }
	//E: Must be odd, >1.
	if e & 1 == 0 || e == 1 { return Err(BadKey); }
	//C: Must not be longer than n.
	if c.len() > n.len() { return Err(BadSignature); }
	let mut r = [0;128];
	let mut t = [0;128];
	let nlen = n.len();				//<=64.
	r.first_mut().map(|t|*t=1);			//Set r to 1.
	let mut active = false;
	for _ in 0..64 {
		if active {
			//Square r.
			bigint_mul(&mut t, &r[..nlen], &r[..nlen]);
			bigint_mod_nct(&mut t, n);
			(&mut r[..nlen]).copy_from_slice(&t[..nlen]);
		}
		if e >> 63 != 0 {
			//r *= c;
			bigint_mul(&mut t, &r[..nlen], &c);
			bigint_mod_nct(&mut t, n);
			(&mut r[..nlen]).copy_from_slice(&t[..nlen]);
			active = true;
		}
		e <<= 1;
	}
	//Copy r to m.
	for t in m.iter_mut() { *t = 0; }
	let mlen = min(m.len(), n.len());
	(&mut m[..mlen]).copy_from_slice(&r[..mlen]);
	Ok(())
}

fn __rsa_sigpower_generic<'a>(n: &[u8], e: u64, signature: &[u8], m: &'a mut [u64;64]) ->
	Result<&'a [u64], VerifyError>
{
	use self::VerifyError::*;
	let mut t = [0;8];
	let mut n2 = [0;64];
	let mut signature2 = [0;64];
	//Only key lengths multiple of 64 bits up to 4096 are supported.
	if n.len() % 8 != 0 || n.len() == 0 || n[0] & 128 == 0 || n.len() > 512 { return Err(UnsupportedKey); }
	//The length of signature must equal length of n.
	if signature.len() != n.len() { return Err(BadSignature); }
	//Read n and signature as big-endian.
	for i in 0..n.len() / 8 {
		if i >= 64 { break; }	//Can not happen.
		let k = signature.len()-8*i;
		t.copy_from_slice(&signature[k-8..k]);
		signature2[i] = u64::from_be_bytes(t);
		t.copy_from_slice(&n[k-8..k]);
		n2[i] = u64::from_be_bytes(t);
	}
	rsa_power(m, &signature2, &n2, e)?;
	Ok(&m[..n.len()/8])	//Truncate result.
}

macro_rules! pkcs_verify
{
	($n:ident $e:ident $signature:ident $msg:ident $hash:ident $h:tt [$v1:expr, $v2:expr, $v3:expr]) => {{
		use self::VerifyError::*;
		let mut syndrome = 0;
		let mut t = [0;8];
		let mut m = [0;64];
		if $e.len() > 8 { return Err(UnsupportedKey); }	//use 64-bit presentation for e.
		(&mut t[8-$e.len()..]).copy_from_slice($e);
		let e = u64::from_be_bytes(t);
		if e < 3 || e % 2 == 0 { return Err(BadKey); }	//e must be at least 3 and odd.
		let m = __rsa_sigpower_generic($n, e, $signature, &mut m)?;
		const H: usize = $h;
		let mut hash = $hash::new();
		$msg(&mut hash);
		let hash = hash.hash();
		for (idx, v) in m.iter().cloned().enumerate() {
			syndrome |= match idx {
				_ if m.len() < 32 => 0xffffffffffffffff,	//Unrecoverable fault.
				i if i < H => {
					t.copy_from_slice(&hash[8*(H-i-1)..8*(H-i)]);
					v ^ u64::from_be_bytes(t)
				},
				i if i == H+0 => v ^ $v1,
				i if i == H+1 => v ^ $v2,
				i if i == H+2 => v ^ $v3,
				i if i + 1 == m.len() => v ^ 0x0001ffffffffffff,
				_ => !v 
			};
		}
		if syndrome == 0 { Ok(()) } else { Err(VerifyFailed) }
	}}
}

///Error in verifying RSA signature.
#[derive(Debug)]
pub enum VerifyError
{
	///Bad key.
	BadKey,
	///Bad signature.
	BadSignature,
	///Unsupported key type.
	UnsupportedKey,
	///Insufficient security.
	InsufficientSecurity,
	///Signature does not correspond to message.
	VerifyFailed,
}

///Verify RSA (2048-4096 bits in multiples of 64) PKCS#1 SHA-512 signature.
pub fn rsa_pkcs1_verify_sha512(n: &[u8], e: &[u8], signature: &[u8], msg: impl Fn(&mut dyn HashUpdate)) ->
	Result<(), VerifyError>
{
	pkcs_verify!(n e signature msg Sha512 8 [0x0304020305000440,0x0d06096086480165,0xffffffff00305130])
}

///Verify RSA (2048-4096 bits in multiples of 64) PKCS#1 SHA-384 signature.
pub fn rsa_pkcs1_verify_sha384(n: &[u8], e: &[u8], signature: &[u8], msg: impl Fn(&mut dyn HashUpdate)) ->
	Result<(), VerifyError>
{
	pkcs_verify!(n e signature msg Sha384 6 [0x0304020205000430,0x0d06096086480165,0xffffffff00304130])
}

///Verify RSA (2048-4096 bits in multiples of 64) PKCS#1 SHA-256 signature.
pub fn rsa_pkcs1_verify_sha256(n: &[u8], e: &[u8], signature: &[u8], msg: impl Fn(&mut dyn HashUpdate)) ->
	Result<(), VerifyError>
{
	pkcs_verify!(n e signature msg Sha256 4 [0x0304020105000420,0x0d06096086480165,0xffffffff00303130])
}

#[cfg(test)] mod test;
