use crate::alloc::Vec;
use crate::Hash;
use crate::hmac_sha256;
use crate::Sha256;

///TLS protect/deprotect ops.
pub trait TlsProtector
{
	///Encrypt data using TLS protector.
	///
	/// * `self`: The key to encrypt with.
	/// * `output`: Buffer to place complete record to (may get overwritten even on error).
	/// * `input`: The record data to encrypt, maximum of 16384 bytes.
	/// * `rtype`: The TLS record type for data (usually 21 => Alert, 22 => Handshake or 23 => Application data).
	fn encrypt(&mut self, output: &mut Vec<u8>, input: &[u8], rtype: u8);
	///Decrypt data using TLS protector 1303.
	///
	/// * `self`: The key to decrypt with.
	/// * `output`: Buffer to place record payload to (may get overwritten even on error).
	/// * `input`: The record data to decrypt, maximum of 16645 bytes.
	/// * On success: Returns `Ok(Some((rtype, used)))`, where `rtype` is the record type, and `used` is the
	///length of record (including headers) in bytes.
	/// * On insufficient input data: Returns `Ok(None)`.
	/// * On error: Returns `Err(())`.
	fn decrypt(&mut self, output: &mut Vec<u8>, input: &[u8]) -> Result<Option<(u8, usize)>, ()>;
}

fn __tls_null_encrypt(output: &mut Vec<u8>, input: &[u8], rtype: u8)
{
	let length = input.len();
	let ad = [rtype,3,3,(length >> 8) as u8, length as u8];
	output.extend_from_slice(&ad);
	output.extend_from_slice(&input);
}
fn __tls_null_decrypt(output: &mut Vec<u8>, input: &[u8]) -> Result<Option<(u8, usize)>, ()>
{
	//Wait for complete record.
	if input.len() < 5 { return Ok(None); }			//No complete record.
	let length = (input[3] as usize) * 256 + (input[4] as usize);
	if &input[1..3] != &[3,3] || length > 16384 || input[0] == 23 { return Err(()); }
	if input.len() < 5 + length { return Ok(None); }	//No complete record.
	output.clear();
	output.extend_from_slice(&input[5..length+5]);	
	Ok(Some((input[0], length+5)))
}

fn __tls_1303_encrypt(output: &mut Vec<u8>, input: &[u8], rtype: u8, key: &[u8;32], iv: &[u8;12], rrsn: &mut u64)
{
	let mut iv: [u8;12] = *iv;
	let rsn = rrsn.to_be_bytes();
	for i in 0..8 { iv[i] ^= rsn[i]; }	//XGCM.
	let length = input.len() + 17;		//inner content type and tag.
	let ad = [23,3,3,(length >> 8) as u8, length as u8];
	output.extend_from_slice(&ad);
	let offset = output.len();
	output.extend_from_slice(&input);
	output.push(rtype);
	let mut tag = [0;16];
	crate::chacha20poly1305_encrypt(key, &iv, &ad, &mut output[offset..], &mut tag);
	output.extend_from_slice(&tag);
	*rrsn += 1;
}

fn __tls_1303_decrypt(output: &mut Vec<u8>, input: &[u8], key: &[u8;32], iv: &[u8;12], rrsn: &mut u64,
	maybe_unencrypted_alert: &mut bool) ->
	Result<Option<(u8, usize)>, ()>
{
	let mut iv: [u8;12] = *iv;
	let rsn = rrsn.to_be_bytes();
	for i in 0..8 { iv[i] ^= rsn[i]; }	//XGCM.
	//Wait for complete record. The record header must start with 23,3,3 and length >= 17.
	if input.len() < 5 { return Ok(None); }			//No complete record.
	let length = (input[3] as usize) * 256 + (input[4] as usize);
	if input.len() < 5 + length { return Ok(None); }	//No complete record.
	//ChangeCipherSpec. This needs to be ignored.
	if *maybe_unencrypted_alert && input[0] == 20 {
		output.clear();
		return Ok(Some((20, 5 + length)));
	}
	//unencrypted alert.
	if *maybe_unencrypted_alert && length == 2 && input.len() >= 7 && input[0] == 21 {
		output.clear();
		output.extend_from_slice(&input[5..7]);
		return Ok(Some((21, 7)));
	}
	*maybe_unencrypted_alert = false;	//Close after first encrypted record.
	if &input[..3] != &[23,3,3] || length < 17 || length > 16640 { return Err(()); }
	//Decrypt.
	let mut tag = [0;16];
	if length-11 < 5 || length-11 > length+5 || length-11 > input.len() || length+5 > input.len() {
		//WTF? None of these conditions can be true here!
		return Err(());
	}
	tag.copy_from_slice(&input[length-11..length+5]);
	output.clear();
	output.extend_from_slice(&input[5..length-11]);	//Ciphertext.
	crate::chacha20poly1305_decrypt(key, &iv, &input[..5], &mut output[..], &tag)?;
	//RSN used.
	*rrsn += 1;
	//Scan output backwards to find the rtype.
	let mut itr = output.len();
	while itr > 0 && output.get(itr-1) == Some(&0) { itr -= 1; }
	if itr == 0 { return Err(()); }						//Bad type.
	//Now itr points one past end.
	itr -= 1;
	let rtype = *output.get(itr).ok_or(())?;
	output.truncate(itr);							//Discard padding.
	Ok(Some((rtype, length+5)))
}

///TLS record protector/deprotector key.
pub struct TlsRecordKey(_TlsRecordKey);
enum _TlsRecordKey
{
	Null,
	C1303([u8;32],[u8;12],u64,bool)
}

impl TlsRecordKey
{
	///Create a new TLS record key, initially with NULL encryption.
	pub fn new() -> TlsRecordKey { TlsRecordKey(_TlsRecordKey::Null) }
	///Generate a key for TLS protector 1303 from traffic secret, and set it.
	///
	/// * `self`: The context to modify.
	/// * `traffic_secret`: The traffic secret to generate key from.
	pub fn set_1303_key(&mut self, traffic_secret: &Sha256Value)
	{
		let traffic_secret = traffic_secret.__deref();
		let key = hmac_sha256(traffic_secret, |d|d.update(b"\x00\x20\x09tls13 key\x00\x01"));
		let riv = hmac_sha256(traffic_secret, |d|d.update(b"\x00\x0c\x08tls13 iv\x00\x01"));
		let mut iv = [0;12];
		iv.copy_from_slice(&riv[..12]);		//Tuncate IV to 12 bytes.
		*self = TlsRecordKey(_TlsRecordKey::C1303(key, iv, 0, true))
	}
}

impl TlsProtector for TlsRecordKey
{
	fn encrypt(&mut self, output: &mut Vec<u8>, input: &[u8], rtype: u8)
	{
		match &mut self.0 {
			_TlsRecordKey::Null => __tls_null_encrypt(output, input, rtype),
			_TlsRecordKey::C1303(ref key, ref iv, ref mut rsn, _) =>
				__tls_1303_encrypt(output, input, rtype, key, iv, rsn),
		}
	}
	fn decrypt(&mut self, output: &mut Vec<u8>, input: &[u8]) -> Result<Option<(u8, usize)>, ()>
	{
		match &mut self.0 {
			_TlsRecordKey::Null => __tls_null_decrypt(output, input),
			_TlsRecordKey::C1303(ref key, ref iv, ref mut rsn, ref mut flag) =>
				__tls_1303_decrypt(output, input, key, iv, rsn, flag),
		}
	}
}

///A hash value.
pub trait HashValue: Sized
{
	///The associated hash function.
	type HashF: Hash+Clone;
	///Perform TLS 1.3 derive_secret.
	fn __derive_secret(&self, label: &str, transcript: &<Self as HashValue>::HashF) -> Self;
	///Mix in TLS 1.3 stage separtor.
	fn __stage_separator(&self) -> Self;
	///Mix in shared secret.
	fn __mix_ss(&self, ss: Option<&[u8]>) -> Self;
	///Construct block of zeroes.
	fn __zeroes() -> Self;
	///Cast to slice.
	fn __deref(&self) -> &[u8];
	///Compute Finished MAC.
	///
	///Parameters:
	///
	/// * Self: The handshake secret.
	/// * ts_ch_to_xcv: Transcript hash from client hello to CertificateVerify immediately preceeding.
	///
	///Returns: The finished MAC.
	fn finished(&self, ts_ch_to_xcv: &<Self as HashValue>::HashF) -> Self;
	///Compute handshake secret.
	///
	///Parameters:
	///
	/// * dhe_ss: The raw Diffie-Hellman key agreement result.
	///
	///Returns: The handshake secret.
	fn handshake_secret(dhe_ss: &[u8]) -> Self
	{
		Self::__zeroes().__mix_ss(None).__stage_separator().__mix_ss(Some(dhe_ss))
	}
	///Compute master secret.
	///
	///Parameters:
	///
	/// * self: The handshake secret.
	///
	///Returns: The master secret.
	fn master_secret(&self) -> Self { self.__stage_separator().__mix_ss(None) }
	///Compute client_application_traffic_secret_0
	///
	///Parameters:
	///
	/// * self: The master secret
	/// * ts_ch_to_sf: Transcript hash from client hello to server finished.
	///
	///Returns: client_application_traffic_secret_0.
	fn client_ats0(&self, ts_ch_to_sf: &<Self as HashValue>::HashF) -> Self
	{
		self.__derive_secret("c ap traffic", ts_ch_to_sf)
	}
	///Compute server_application_traffic_secret_0
	///
	///Parameters:
	///
	/// * self: The master secret
	/// * ts_ch_to_sf: Transcript hash from client hello to server finished.
	///
	///Returns: server_application_traffic_secret_0.
	fn server_ats0(&self, ts_ch_to_sf: &<Self as HashValue>::HashF) -> Self
	{
		self.__derive_secret("s ap traffic", ts_ch_to_sf)
	}
	///Compute client_handshake_traffic_secret
	///
	///Parameters:
	///
	/// * self: The handshake secret.
	/// * ts_ch_to_sh: Transcript hash from client hello to server hello.
	///
	///Returns: client_handshake_traffic_secret.
	fn client_hts(&self, ts_ch_to_sh: &<Self as HashValue>::HashF) -> Self
	{
		self.__derive_secret("c hs traffic", ts_ch_to_sh)
	}
	///Compute server_handshake_traffic_secret
	///
	///Parameters:
	///
	/// * self: The handshake secret.
	/// * ts_ch_to_sh: Transcript hash from client hello to server hello.
	///
	///Returns: server_handshake_traffic_secret.
	fn server_hts(&self, ts_ch_to_sh: &<Self as HashValue>::HashF) -> Self
	{
		self.__derive_secret("s hs traffic", ts_ch_to_sh)
	}
}

///Hash value for SHA-256.
pub struct Sha256Value([u8;32]);

impl HashValue for Sha256Value
{
	type HashF = Sha256;
	fn __derive_secret(&self, label: &str, transcript: &Sha256) -> Self
	{
		let val = transcript.clone().hash();
		Sha256Value(hmac_sha256(&self.0, |d|{
			d.update(&[0, 32, 6+label.len() as u8, 116, 108, 115, 49, 51, 32]);
			d.update(label.as_bytes());
			d.update(&[32]);
			d.update(&val);
			d.update(&[1]);
		}))
	}
	fn __stage_separator(&self) -> Self
	{
		self.__derive_secret("derived", &Sha256::new())
	}
	fn __mix_ss(&self, ss: Option<&[u8]>) -> Self
	{
		Sha256Value(if let Some(ss) = ss {
			hmac_sha256(&self.0, |d|d.update(ss))
		} else {
			hmac_sha256(&self.0, |d|d.update(&[0;32]))
		})
	}
	fn __zeroes() -> Self { Sha256Value([0;32]) }
	fn finished(&self, ts_ch_to_xcv: &Sha256) -> Self
	{
		let val = ts_ch_to_xcv.clone().hash();
		let fk = hmac_sha256(&self.0, |d|d.update(b"\x00\x20\x0etls13 finished\x00\x01"));
		Sha256Value(hmac_sha256(&fk, |d|d.update(&val)))
	}
	fn __deref(&self) -> &[u8] { &self.0 }
}

#[test]
fn test_enc_dec()
{
	let mut imm = Vec::new();
	let mut fin = Vec::new();
	__tls_1303_encrypt(&mut imm, b"Hello, World!", 23, &[63;32], &[42;12], &mut 12345);
	let ret = __tls_1303_decrypt(&mut fin, &imm, &[63;32], &[42;12], &mut 12345, &mut false).
		expect("Decrypt failed");
	assert_eq!(ret, Some((23, imm.len())));
	assert_eq!(&fin[..], b"Hello, World!");
}

#[test]
fn test_key_schedule()
{
	use crate::HashUpdate;
	let test_ss = include_bytes!("testdata/ss.bin");
	let test_hs = include_bytes!("testdata/hs.bin");
	let test_ms = include_bytes!("testdata/ms.bin");
	let test_ch = include_bytes!("testdata/ch.bin");
	let test_sh = include_bytes!("testdata/sh.bin");
	let test_ee = include_bytes!("testdata/ee.bin");
	let test_sf = include_bytes!("testdata/sf.bin");
	let test_cf = include_bytes!("testdata/cf.bin");
	let trans_1 = include_bytes!("testdata/trans1.bin");
	let trans_2 = include_bytes!("testdata/trans2.bin");
	let trans_3 = include_bytes!("testdata/trans3.bin");
	let te_chts = include_bytes!("testdata/chts.bin");
	let te_shts = include_bytes!("testdata/shts.bin");
	let te_sats = include_bytes!("testdata/sats.bin");
	let te_cats = include_bytes!("testdata/cats.bin");
	let tk_shts = include_bytes!("testdata/shtsk.bin");

	let hss = Sha256Value::handshake_secret(test_ss);
	assert_eq!(&hss.0, test_hs);
	let mut transcript = Sha256::new();
	transcript.update(test_ch);
	transcript.update(test_sh);
	assert_eq!(&transcript.clone().hash(), trans_1);
	let chts = hss.client_hts(&transcript);
	let shts = hss.server_hts(&transcript);
	assert_eq!(&chts.0, te_chts);
	assert_eq!(&shts.0, te_shts);
	let mss = hss.master_secret();
	assert_eq!(&mss.0, test_ms);
	let fsplit = test_ee.len()-36;
	transcript.update(&test_ee[..fsplit]);
	assert_eq!(&transcript.clone().hash(), trans_3);
	let sf = shts.finished(&transcript);
	assert_eq!(&sf.0, test_sf);
	transcript.update(&test_ee[fsplit..]);
	assert_eq!(&transcript.clone().hash(), trans_2);
	let cats = mss.client_ats0(&transcript);
	let sats = mss.server_ats0(&transcript);
	assert_eq!(&cats.0, te_cats);
	assert_eq!(&sats.0, te_sats);
	let cf = chts.finished(&transcript);
	assert_eq!(&cf.0, test_cf);
	let mut shtsk = TlsRecordKey::new();
	shtsk.set_1303_key(&shts);
	match &shtsk.0 {
		_TlsRecordKey::C1303(x, y, z, _) => {
			assert_eq!(x, &tk_shts[..32]);
			assert_eq!(y, &tk_shts[32..]);
			assert_eq!(*z, 0);
		},
		_ => panic!("Wrong key type!")
	};
}
