#![allow(non_snake_case)]
use super::Hash;
use super::HashUpdate;

//The SHA512 IV.
static SHA512_IV: [u64;8] = [
	0x6a09e667f3bcc908, 0xbb67ae8584caa73b, 0x3c6ef372fe94f82b, 0xa54ff53a5f1d36f1,
	0x510e527fade682d1, 0x9b05688c2b3e6c1f, 0x1f83d9abfb41bd6b, 0x5be0cd19137e2179
];

//The SHA384 IV.
static SHA384_IV: [u64;8] = [
	0xcbbb9d5dc1059ed8, 0x629a292a367cd507, 0x9159015a3070dd17, 0x152fecd8f70e5939,
	0x67332667ffc00b31, 0x8eb44a8768581511, 0xdb0c2e0d64f98fa7, 0x47b5481dbefa4fa4
];

//The SHA-512 round constants I.
static SHA512_RK1: [u64;16] = [
	0x428a2f98d728ae22, 0x7137449123ef65cd, 0xb5c0fbcfec4d3b2f, 0xe9b5dba58189dbbc,
	0x3956c25bf348b538, 0x59f111f1b605d019, 0x923f82a4af194f9b, 0xab1c5ed5da6d8118,
	0xd807aa98a3030242, 0x12835b0145706fbe, 0x243185be4ee4b28c, 0x550c7dc3d5ffb4e2,
	0x72be5d74f27b896f, 0x80deb1fe3b1696b1, 0x9bdc06a725c71235, 0xc19bf174cf692694,
];

//The SHA-512 round constants II.
static SHA512_RK2: [u64;16] = [
	0xe49b69c19ef14ad2, 0xefbe4786384f25e3, 0x0fc19dc68b8cd5b5, 0x240ca1cc77ac9c65,
	0x2de92c6f592b0275, 0x4a7484aa6ea6e483, 0x5cb0a9dcbd41fbd4, 0x76f988da831153b5,
	0x983e5152ee66dfab, 0xa831c66d2db43210, 0xb00327c898fb213f, 0xbf597fc7beef0ee4,
	0xc6e00bf33da88fc2, 0xd5a79147930aa725, 0x06ca6351e003826f, 0x142929670a0e6e70,
];

//The SHA-512 round constants III.
static SHA512_RK3: [u64;16] = [
	0x27b70a8546d22ffc, 0x2e1b21385c26c926, 0x4d2c6dfc5ac42aed, 0x53380d139d95b3df,
	0x650a73548baf63de, 0x766a0abb3c77b2a8, 0x81c2c92e47edaee6, 0x92722c851482353b,
	0xa2bfe8a14cf10364, 0xa81a664bbc423001, 0xc24b8b70d0f89791, 0xc76c51a30654be30,
	0xd192e819d6ef5218, 0xd69906245565a910, 0xf40e35855771202a, 0x106aa07032bbd1b8,
];

//The SHA-512 round constants IV.
static SHA512_RK4: [u64;16] = [
	0x19a4c116b8d2d0c8, 0x1e376c085141ab53, 0x2748774cdf8eeb99, 0x34b0bcb5e19b48a8,
	0x391c0cb3c5c95a63, 0x4ed8aa4ae3418acb, 0x5b9cca4f7763e373, 0x682e6ff3d6b2b8a3,
	0x748f82ee5defb2fc, 0x78a5636f43172f60, 0x84c87814a1f0ab72, 0x8cc702081a6439ec,
	0x90befffa23631e28, 0xa4506cebde82bde9, 0xbef9a3f7b2c67915, 0xc67178f2e372532b,
];

//The SHA-512 round constants V.
static SHA512_RK5: [u64;16] = [
	0xca273eceea26619c, 0xd186b8c721c0c207, 0xeada7dd6cde0eb1e, 0xf57d4f7fee6ed178,
	0x06f067aa72176fba, 0x0a637dc5a2c898a6, 0x113f9804bef90dae, 0x1b710b35131c471b,
	0x28db77f523047d84, 0x32caab7b40c72493, 0x3c9ebe0a15c9bebc, 0x431d67c49c100d4c,
	0x4cc5d4becb3e42b6, 0x597f299cfc657e2a, 0x5fcb6fab3ad6faec, 0x6c44198c4a475817
];


fn rotate_r(num: u64, places: u64) -> u64 { (num >> places) | (num << 64 - places) }
fn majority(a: u64, b: u64, c: u64) -> u64 { (a & b) ^ (a & c) ^ (b & c) }
fn choose(k: u64, a: u64, b: u64) -> u64 { (k & a) | ((!k) & b) }
fn sigma0(num: u64) -> u64 { rotate_r(num, 28) ^ rotate_r(num, 34) ^ rotate_r(num, 39) }
fn sigma1(num: u64) -> u64 { rotate_r(num, 14) ^ rotate_r(num, 18) ^ rotate_r(num, 41) }
fn esigma0(num: u64) -> u64 { rotate_r(num, 1) ^ rotate_r(num, 8) ^ (num >> 7) }
fn esigma1(num: u64) -> u64 { rotate_r(num, 19) ^ rotate_r(num, 61) ^ (num >> 6) }

fn sha512_drnd(s: &mut [u64;8], A: usize, B: usize, C: usize, D: usize, E: usize, F: usize, G: usize, H: usize,
	W: u64, K: u64)
{
	let x = s[H].wrapping_add(K).wrapping_add(W).wrapping_add(sigma1(s[E])).
		wrapping_add(choose(s[E], s[F], s[G]));
	s[D] = s[D].wrapping_add(x);
	s[H] = x.wrapping_add(sigma0(s[A])).wrapping_add(majority(s[A], s[B], s[C]));
}

fn sha512_wrnd(W0: u64, W1: u64, W2: u64, W3: u64) -> u64
{
	let xsigma0 = esigma0(W1);
	let xsigma1 = esigma1(W3);
	W0.wrapping_add(xsigma0).wrapping_add(xsigma1).wrapping_add(W2)
}

fn do_sha2_16_rounds_1(s: &mut [u64;8], DB: &mut [u64;16], RK: &[u64;16])
{
	sha512_drnd(s,0,1,2,3,4,5,6,7,DB[ 0], RK[ 0]);
	sha512_drnd(s,7,0,1,2,3,4,5,6,DB[ 1], RK[ 1]);
	sha512_drnd(s,6,7,0,1,2,3,4,5,DB[ 2], RK[ 2]);
	sha512_drnd(s,5,6,7,0,1,2,3,4,DB[ 3], RK[ 3]);
	sha512_drnd(s,4,5,6,7,0,1,2,3,DB[ 4], RK[ 4]);
	sha512_drnd(s,3,4,5,6,7,0,1,2,DB[ 5], RK[ 5]);
	sha512_drnd(s,2,3,4,5,6,7,0,1,DB[ 6], RK[ 6]);
	sha512_drnd(s,1,2,3,4,5,6,7,0,DB[ 7], RK[ 7]);
	sha512_drnd(s,0,1,2,3,4,5,6,7,DB[ 8], RK[ 8]);
	sha512_drnd(s,7,0,1,2,3,4,5,6,DB[ 9], RK[ 9]);
	sha512_drnd(s,6,7,0,1,2,3,4,5,DB[10], RK[10]);
	sha512_drnd(s,5,6,7,0,1,2,3,4,DB[11], RK[11]);
	sha512_drnd(s,4,5,6,7,0,1,2,3,DB[12], RK[12]);
	sha512_drnd(s,3,4,5,6,7,0,1,2,DB[13], RK[13]);
	sha512_drnd(s,2,3,4,5,6,7,0,1,DB[14], RK[14]);
	sha512_drnd(s,1,2,3,4,5,6,7,0,DB[15], RK[15]);
}

fn do_sha2_16_rounds_2(s: &mut [u64;8], DB: &mut [u64;16], RK: &[u64;16])
{
	DB[ 0] = sha512_wrnd(DB[ 0], DB[ 1], DB[ 9], DB[14]); sha512_drnd(s,0,1,2,3,4,5,6,7,DB[ 0], RK[ 0]);
	DB[ 1] = sha512_wrnd(DB[ 1], DB[ 2], DB[10], DB[15]); sha512_drnd(s,7,0,1,2,3,4,5,6,DB[ 1], RK[ 1]);
	DB[ 2] = sha512_wrnd(DB[ 2], DB[ 3], DB[11], DB[ 0]); sha512_drnd(s,6,7,0,1,2,3,4,5,DB[ 2], RK[ 2]);
	DB[ 3] = sha512_wrnd(DB[ 3], DB[ 4], DB[12], DB[ 1]); sha512_drnd(s,5,6,7,0,1,2,3,4,DB[ 3], RK[ 3]);
	DB[ 4] = sha512_wrnd(DB[ 4], DB[ 5], DB[13], DB[ 2]); sha512_drnd(s,4,5,6,7,0,1,2,3,DB[ 4], RK[ 4]);
	DB[ 5] = sha512_wrnd(DB[ 5], DB[ 6], DB[14], DB[ 3]); sha512_drnd(s,3,4,5,6,7,0,1,2,DB[ 5], RK[ 5]);
	DB[ 6] = sha512_wrnd(DB[ 6], DB[ 7], DB[15], DB[ 4]); sha512_drnd(s,2,3,4,5,6,7,0,1,DB[ 6], RK[ 6]);
	DB[ 7] = sha512_wrnd(DB[ 7], DB[ 8], DB[ 0], DB[ 5]); sha512_drnd(s,1,2,3,4,5,6,7,0,DB[ 7], RK[ 7]);
	DB[ 8] = sha512_wrnd(DB[ 8], DB[ 9], DB[ 1], DB[ 6]); sha512_drnd(s,0,1,2,3,4,5,6,7,DB[ 8], RK[ 8]);
	DB[ 9] = sha512_wrnd(DB[ 9], DB[10], DB[ 2], DB[ 7]); sha512_drnd(s,7,0,1,2,3,4,5,6,DB[ 9], RK[ 9]);
	DB[10] = sha512_wrnd(DB[10], DB[11], DB[ 3], DB[ 8]); sha512_drnd(s,6,7,0,1,2,3,4,5,DB[10], RK[10]);
	DB[11] = sha512_wrnd(DB[11], DB[12], DB[ 4], DB[ 9]); sha512_drnd(s,5,6,7,0,1,2,3,4,DB[11], RK[11]);
	DB[12] = sha512_wrnd(DB[12], DB[13], DB[ 5], DB[10]); sha512_drnd(s,4,5,6,7,0,1,2,3,DB[12], RK[12]);
	DB[13] = sha512_wrnd(DB[13], DB[14], DB[ 6], DB[11]); sha512_drnd(s,3,4,5,6,7,0,1,2,DB[13], RK[13]);
	DB[14] = sha512_wrnd(DB[14], DB[15], DB[ 7], DB[12]); sha512_drnd(s,2,3,4,5,6,7,0,1,DB[14], RK[14]);
	DB[15] = sha512_wrnd(DB[15], DB[ 0], DB[ 8], DB[13]); sha512_drnd(s,1,2,3,4,5,6,7,0,DB[15], RK[15]);
}

#[derive(Clone,Debug)]
struct _Sha512
{
	state: [u64;8],
	partial: [u64;16],
	size_lo: u64,		//In bits!
}

impl _Sha512
{
	fn __core(state: &mut [u64;8], block: &mut [u64;16])
	{
		let mut s: [u64;8] = *state;
		do_sha2_16_rounds_1(&mut s, block, &SHA512_RK1);
		do_sha2_16_rounds_2(&mut s, block, &SHA512_RK2);
		do_sha2_16_rounds_2(&mut s, block, &SHA512_RK3);
		do_sha2_16_rounds_2(&mut s, block, &SHA512_RK4);
		do_sha2_16_rounds_2(&mut s, block, &SHA512_RK5);
		//Add the state back.
		for (t,s) in state.iter_mut().zip(s.iter()) { *t = t.wrapping_add(*s); }
	}
	fn new(iv: &[u64;8]) -> _Sha512
	{
		_Sha512 {
			state: *iv,
			partial: [0;16],
			size_lo: 0,
		}
	}
	fn update(&mut self, data: &[u8])
	{
		for b in data.iter().cloned() {
			let word = (self.size_lo / 64 % 16) as usize;
			let offset = 56 - self.size_lo % 64;
			self.partial[word] |= (b as u64) << offset;
			//Assume that data is <2EB, so size_lo never overflows.
			self.size_lo = self.size_lo.wrapping_add(8);
			//If block is full, Compress the partial and zero it for next block.
			if self.size_lo % 1024 == 0 {
				Self::__core(&mut self.state, &mut self.partial);
				for w in self.partial.iter_mut() { *w = 0; }
			}
		}
	}
	fn hash(mut self) -> [u64;8]
	{
		//Write the end bit byte. And if length does not fit, start new block.
		let word = (self.size_lo / 64 % 16) as usize;
		let offset = 56 - self.size_lo % 64;
		self.partial[word] |= 0x80u64 << offset;
		if self.size_lo % 1024 > 888 {
			Self::__core(&mut self.state, &mut self.partial);
			for w in self.partial.iter_mut() { *w = 0; }
		}
		//Write the length, and do final compress.
		self.partial[15] = self.size_lo;
		Self::__core(&mut self.state, &mut self.partial);
		//Read out the state.
		self.state
	}
}

///SHA-512
#[derive(Clone,Debug)]
pub struct Sha512(_Sha512);

impl HashUpdate for Sha512
{
	fn update(&mut self, data: &[u8]) { self.0.update(data) }
}

impl Hash for Sha512
{
	type Output = [u8;64];
	fn new() -> Sha512 { Sha512(_Sha512::new(&SHA512_IV)) }
	fn hash(self) -> [u8;64]
	{
		//Read out the state.
		let r = self.0.hash();
		let mut out = [0;64];
		for i in 0..64 { out[i] = (r[i/8] >> 56 - i % 8 * 8) as u8; }
		out
	}
	fn blocklen() -> usize { 128 }
	fn duplicate(&self) -> Self { Clone::clone(self) }
}

///SHA-384
#[derive(Clone,Debug)]
pub struct Sha384(_Sha512);

impl HashUpdate for Sha384
{
	fn update(&mut self, data: &[u8]) { self.0.update(data) }
}

impl Hash for Sha384
{
	type Output = [u8;48];
	fn new() -> Sha384 { Sha384(_Sha512::new(&SHA384_IV)) }
	fn hash(self) -> [u8;48]
	{
		//Read out the state.
		let r = self.0.hash();
		let mut out = [0;48];
		for i in 0..48 { out[i] = (r[i/8] >> 56 - i % 8 * 8) as u8; }
		out
	}
	fn blocklen() -> usize { 128 }
	fn duplicate(&self) -> Self { Clone::clone(self) }
}

#[test]
fn test_abc_384()
{
	let mut s = Sha384::new();
	s.update(b"abc");
	assert_eq!(&s.hash()[..], &b"\xcb\x00\x75\x3f\x45\xa3\x5e\x8b\xb5\xa0\x3d\x69\x9a\xc6\x50\x07\x27\x2c\
		\x32\xab\x0e\xde\xd1\x63\x1a\x8b\x60\x5a\x43\xff\x5b\xed\x80\x86\x07\x2b\xa1\xe7\xcc\x23\x58\
		\xba\xec\xa1\x34\xc8\x25\xa7"[..]);
}

#[test]
fn test_abc_512()
{
	let mut s = Sha512::new();
	s.update(b"abc");
	assert_eq!(&s.hash()[..], &b"\xdd\xaf\x35\xa1\x93\x61\x7a\xba\xcc\x41\x73\x49\xae\x20\x41\x31\x12\xe6\
		\xfa\x4e\x89\xa9\x7e\xa2\x0a\x9e\xee\xe6\x4b\x55\xd3\x9a\x21\x92\x99\x2a\x27\x4f\xc1\xa8\x36\
		\xba\x3c\x23\xa3\xfe\xeb\xbd\x45\x4d\x44\x23\x64\x3c\xe8\x0e\x2a\x9a\xc9\x4f\xa5\x4c\xa4\x9f"[..]);
}
