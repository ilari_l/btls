mod sha256;
mod sha512;
pub use sha256::Sha256;
pub use sha512::Sha384;
pub use sha512::Sha512;

///Byte array.
pub trait ByteArray: Copy
{
	///Cast to byte slice.
	fn as_slice(&self) -> &[u8];
}

impl ByteArray for [u8;1] { fn as_slice(&self) -> &[u8] { &self[..] } }
impl ByteArray for [u8;2] { fn as_slice(&self) -> &[u8] { &self[..] } }
impl ByteArray for [u8;4] { fn as_slice(&self) -> &[u8] { &self[..] } }
impl ByteArray for [u8;8] { fn as_slice(&self) -> &[u8] { &self[..] } }
impl ByteArray for [u8;32] { fn as_slice(&self) -> &[u8] { &self[..] } }
impl ByteArray for [u8;48] { fn as_slice(&self) -> &[u8] { &self[..] } }
impl ByteArray for [u8;64] { fn as_slice(&self) -> &[u8] { &self[..] } }


///Hash function update.
pub trait HashUpdate
{
	///Append data to be hashed.
	fn update(&mut self, data: &[u8]);
}

///Hash function.
pub trait Hash: HashUpdate
{
	///Output type.
	type Output: ByteArray;
	///Make new state.
	fn new() -> Self;
	///Get hash of state.
	fn hash(self) -> <Self as Hash>::Output;
	///Hash block length (used for HMAC).
	fn blocklen() -> usize;
	///Append a iterator of octet strings.
	fn update_rope<'a>(&mut self, rope: impl Iterator<Item=&'a [u8]>)
	{
		for part in rope { self.update(part); }
	}
	///Clone state.
	fn duplicate(&self) -> Self;
}

///Compute HMAC-SHA256.
///
/// * `key`: The key to use for MAC computation.
/// * `msg`: The message to compute MAC of.
/// * Returns: The computed MAC.
pub fn hmac_sha256(key: &[u8], msg: impl Fn(&mut dyn HashUpdate)) -> [u8;32]
{
	let mut ekey = [0;64];
	if key.len() > 64 {
		let mut tmp = Sha256::new();
		tmp.update(key);
		(&mut ekey[..32]).copy_from_slice(&tmp.hash());
	} else {
		(&mut ekey[..key.len()]).copy_from_slice(key);
	}
	for t in ekey.iter_mut() { *t ^= 0x36; }
	let mut tmp = Sha256::new();
	tmp.update(&ekey);
	msg(&mut tmp);
	for t in ekey.iter_mut() { *t ^= 0x6A; }
	let mut tmp2 = Sha256::new();
	tmp2.update(&ekey);
	tmp2.update(&tmp.hash());
	tmp2.hash()
}
