#![allow(non_snake_case)]
use super::Hash;
use super::HashUpdate;

//The SHA256 IV.
static SHA256_IV: [u32;8] = [
	0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a, 0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19
];

//The SHA-256 round constants I.
static SHA256_RK1: [u32;16] = [
	0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
	0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
];

//The SHA-256 round constants II.
static SHA256_RK2: [u32;16] = [
	0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
	0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
];

//The SHA-256 round constants III.
static SHA256_RK3: [u32;16] = [
	0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
	0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
];

//The SHA-256 round constants IV.
static SHA256_RK4: [u32;16] = [
	0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
	0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
];

fn rotate_r(num: u32, places: u32) -> u32 { (num >> places) | (num << 32 - places) }
fn majority(a: u32, b: u32, c: u32) -> u32 { (a & b) ^ (a & c) ^ (b & c) }
fn choose(k: u32, a: u32, b: u32) -> u32 { (k & a) | ((!k) & b) }
fn sigma0(num: u32) -> u32 { rotate_r(num, 2) ^ rotate_r(num, 13) ^ rotate_r(num, 22) }
fn sigma1(num: u32) -> u32 { rotate_r(num, 6) ^ rotate_r(num, 11) ^ rotate_r(num, 25) }
fn esigma0(num: u32) -> u32 { rotate_r(num, 7) ^ rotate_r(num, 18) ^ (num >> 3) }
fn esigma1(num: u32) -> u32 { rotate_r(num, 17) ^ rotate_r(num, 19) ^ (num >> 10) }

fn sha256_drnd(s: &mut [u32;8], A: usize, B: usize, C: usize, D: usize, E: usize, F: usize, G: usize, H: usize,
	W: u32, K: u32)
{
	let x = s[H].wrapping_add(K).wrapping_add(W).wrapping_add(sigma1(s[E])).
		wrapping_add(choose(s[E], s[F], s[G]));
	s[D] = s[D].wrapping_add(x);
	s[H] = x.wrapping_add(sigma0(s[A])).wrapping_add(majority(s[A], s[B], s[C]));
}

fn sha256_wrnd(W0: u32, W1: u32, W2: u32, W3: u32) -> u32
{
	let xsigma0 = esigma0(W1);
	let xsigma1 = esigma1(W3);
	W0.wrapping_add(xsigma0).wrapping_add(xsigma1).wrapping_add(W2)
}

fn do_sha2_16_rounds_1(s: &mut [u32;8], DB: &mut [u32;16], RK: &[u32;16])
{
	sha256_drnd(s,0,1,2,3,4,5,6,7,DB[ 0], RK[ 0]);
	sha256_drnd(s,7,0,1,2,3,4,5,6,DB[ 1], RK[ 1]);
	sha256_drnd(s,6,7,0,1,2,3,4,5,DB[ 2], RK[ 2]);
	sha256_drnd(s,5,6,7,0,1,2,3,4,DB[ 3], RK[ 3]);
	sha256_drnd(s,4,5,6,7,0,1,2,3,DB[ 4], RK[ 4]);
	sha256_drnd(s,3,4,5,6,7,0,1,2,DB[ 5], RK[ 5]);
	sha256_drnd(s,2,3,4,5,6,7,0,1,DB[ 6], RK[ 6]);
	sha256_drnd(s,1,2,3,4,5,6,7,0,DB[ 7], RK[ 7]);
	sha256_drnd(s,0,1,2,3,4,5,6,7,DB[ 8], RK[ 8]);
	sha256_drnd(s,7,0,1,2,3,4,5,6,DB[ 9], RK[ 9]);
	sha256_drnd(s,6,7,0,1,2,3,4,5,DB[10], RK[10]);
	sha256_drnd(s,5,6,7,0,1,2,3,4,DB[11], RK[11]);
	sha256_drnd(s,4,5,6,7,0,1,2,3,DB[12], RK[12]);
	sha256_drnd(s,3,4,5,6,7,0,1,2,DB[13], RK[13]);
	sha256_drnd(s,2,3,4,5,6,7,0,1,DB[14], RK[14]);
	sha256_drnd(s,1,2,3,4,5,6,7,0,DB[15], RK[15]);
}

fn do_sha2_16_rounds_2(s: &mut [u32;8], DB: &mut [u32;16], RK: &[u32;16])
{
	DB[ 0] = sha256_wrnd(DB[ 0], DB[ 1], DB[ 9], DB[14]); sha256_drnd(s,0,1,2,3,4,5,6,7,DB[ 0], RK[ 0]);
	DB[ 1] = sha256_wrnd(DB[ 1], DB[ 2], DB[10], DB[15]); sha256_drnd(s,7,0,1,2,3,4,5,6,DB[ 1], RK[ 1]);
	DB[ 2] = sha256_wrnd(DB[ 2], DB[ 3], DB[11], DB[ 0]); sha256_drnd(s,6,7,0,1,2,3,4,5,DB[ 2], RK[ 2]);
	DB[ 3] = sha256_wrnd(DB[ 3], DB[ 4], DB[12], DB[ 1]); sha256_drnd(s,5,6,7,0,1,2,3,4,DB[ 3], RK[ 3]);
	DB[ 4] = sha256_wrnd(DB[ 4], DB[ 5], DB[13], DB[ 2]); sha256_drnd(s,4,5,6,7,0,1,2,3,DB[ 4], RK[ 4]);
	DB[ 5] = sha256_wrnd(DB[ 5], DB[ 6], DB[14], DB[ 3]); sha256_drnd(s,3,4,5,6,7,0,1,2,DB[ 5], RK[ 5]);
	DB[ 6] = sha256_wrnd(DB[ 6], DB[ 7], DB[15], DB[ 4]); sha256_drnd(s,2,3,4,5,6,7,0,1,DB[ 6], RK[ 6]);
	DB[ 7] = sha256_wrnd(DB[ 7], DB[ 8], DB[ 0], DB[ 5]); sha256_drnd(s,1,2,3,4,5,6,7,0,DB[ 7], RK[ 7]);
	DB[ 8] = sha256_wrnd(DB[ 8], DB[ 9], DB[ 1], DB[ 6]); sha256_drnd(s,0,1,2,3,4,5,6,7,DB[ 8], RK[ 8]);
	DB[ 9] = sha256_wrnd(DB[ 9], DB[10], DB[ 2], DB[ 7]); sha256_drnd(s,7,0,1,2,3,4,5,6,DB[ 9], RK[ 9]);
	DB[10] = sha256_wrnd(DB[10], DB[11], DB[ 3], DB[ 8]); sha256_drnd(s,6,7,0,1,2,3,4,5,DB[10], RK[10]);
	DB[11] = sha256_wrnd(DB[11], DB[12], DB[ 4], DB[ 9]); sha256_drnd(s,5,6,7,0,1,2,3,4,DB[11], RK[11]);
	DB[12] = sha256_wrnd(DB[12], DB[13], DB[ 5], DB[10]); sha256_drnd(s,4,5,6,7,0,1,2,3,DB[12], RK[12]);
	DB[13] = sha256_wrnd(DB[13], DB[14], DB[ 6], DB[11]); sha256_drnd(s,3,4,5,6,7,0,1,2,DB[13], RK[13]);
	DB[14] = sha256_wrnd(DB[14], DB[15], DB[ 7], DB[12]); sha256_drnd(s,2,3,4,5,6,7,0,1,DB[14], RK[14]);
	DB[15] = sha256_wrnd(DB[15], DB[ 0], DB[ 8], DB[13]); sha256_drnd(s,1,2,3,4,5,6,7,0,DB[15], RK[15]);
}

///SHA-256
#[derive(Clone,Debug)]
pub struct Sha256
{
	state: [u32;8],
	partial: [u32;16],
	size_lo: u32,		//In bits!
	size_hi: u32
}

impl Sha256
{
	fn __core(state: &mut [u32;8], block: &mut [u32;16])
	{
		let mut s: [u32;8] = *state;
		do_sha2_16_rounds_1(&mut s, block, &SHA256_RK1);
		do_sha2_16_rounds_2(&mut s, block, &SHA256_RK2);
		do_sha2_16_rounds_2(&mut s, block, &SHA256_RK3);
		do_sha2_16_rounds_2(&mut s, block, &SHA256_RK4);
		//Add the state back.
		for (t,s) in state.iter_mut().zip(s.iter()) { *t = t.wrapping_add(*s); }
	}
}

impl HashUpdate for Sha256
{
	fn update(&mut self, data: &[u8])
	{
		for b in data.iter().cloned() {
			let word = (self.size_lo / 32 % 16) as usize;
			let offset = 24 - self.size_lo % 32;
			self.partial[word] |= (b as u32) << offset;
			self.size_lo = self.size_lo.wrapping_add(8);
			//Assume that data is <2EB, so size_hi never overflows.
			if self.size_lo == 0 { self.size_hi += 1; }
			//If block is full, Compress the partial and zero it for next block.
			if self.size_lo % 512 == 0 {
				Self::__core(&mut self.state, &mut self.partial);
				for w in self.partial.iter_mut() { *w = 0; }
			}
		}
	}
}

impl Hash for Sha256
{
	type Output = [u8;32];
	fn new() -> Sha256
	{
		Sha256 {
			state: SHA256_IV,
			partial: [0;16],
			size_lo: 0,
			size_hi: 0,
		}
	}
	fn hash(mut self) -> [u8;32]
	{
		//Write the end bit byte. And if length does not fit, start new block.
		let word = (self.size_lo / 32 % 16) as usize;
		let offset = 24 - self.size_lo % 32;
		self.partial[word] |= 0x80u32 << offset;
		if self.size_lo % 512 > 440 {
			Self::__core(&mut self.state, &mut self.partial);
			for w in self.partial.iter_mut() { *w = 0; }
		}
		//Write the length, and do final compress.
		self.partial[14] = self.size_hi;
		self.partial[15] = self.size_lo;
		Self::__core(&mut self.state, &mut self.partial);
		//Read out the state.
		let mut out = [0;32];
		for i in 0..32 { out[i] = (self.state[i/4] >> 24 - i % 4 * 8) as u8; }
		out
	}
	fn blocklen() -> usize { 64 }
	fn duplicate(&self) -> Self { Clone::clone(self) }
}

#[test]
fn test_abc()
{
	let mut s = Sha256::new();
	s.update(b"abc");
	assert_eq!(&s.hash()[..], &b"\xba\x78\x16\xbf\x8f\x01\xcf\xea\x41\x41\x40\xde\x5d\xae\x22\x23\xb0\x03\
		\x61\xa3\x96\x17\x7a\x9c\xb4\x10\xff\x61\xf2\x00\x15\xad"[..]);
}
