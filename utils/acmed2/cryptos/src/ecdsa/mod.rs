#![allow(non_snake_case)]
use super::bigint::bigint_add;
use super::bigint::bigint_mac;
use super::bigint::bigint_mac1;
use super::bigint::bigint_mul;
use super::bigint::bigint_sub;
use super::hash::Hash;
use super::hash::HashUpdate;
use super::hash::Sha256;
use super::hash::Sha384;
use core::num::Wrapping as W;
use core::ops::Add;
use core::ops::Mul;
use core::ops::Neg;
use core::ops::Sub;

macro_rules! to_words
{
	($wcount:tt $bs:expr) => {{
		let mut tmp = [0u64;$wcount];
		crate::bigint::bytes_to_words_be(&mut tmp, $bs);
		tmp
	}};
}

const P256_INV_POWER: [u64; 4] = [0xFFFFFFFFFFFFFFFD,0xFFFFFFFF,0,0xFFFFFFFF00000001];
const P256_MODULO: [u64; 4] = [0xFFFFFFFFFFFFFFFF,0xFFFFFFFF,0,0xFFFFFFFF00000001];
const P256_ORDER: [u64; 4] = [0xf3b9cac2fc632551,0xbce6faada7179e84,0xffffffffffffffff,0xffffffff00000000];
const P256_ORDER_K: [u64; 4] = [0x0C46353D039CDAAF,0x4319055258E8617B,0,0xFFFFFFFF];
const P256_ORDER_INV: [u64; 4] = [0xf3b9cac2fc63254F,0xbce6faada7179e84,0xffffffffffffffff,0xffffffff00000000];
const P384_INV_POWER: [u64; 6] = [0x00000000FFFFFFFD,0xFFFFFFFF00000000,0xFFFFFFFFFFFFFFFE,
			0xFFFFFFFFFFFFFFFF,0xFFFFFFFFFFFFFFFF,0xFFFFFFFFFFFFFFFF];
const P384_MODULO: [u64; 6] = [0x00000000FFFFFFFF,0xFFFFFFFF00000000,0xFFFFFFFFFFFFFFFE,
			0xFFFFFFFFFFFFFFFF,0xFFFFFFFFFFFFFFFF,0xFFFFFFFFFFFFFFFF];
const P384_ORDER: [u64; 6] = [0xecec196accc52973,0x581a0db248b0a77a,0xc7634d81f4372ddf,
			0xffffffffffffffff,0xffffffffffffffff,0xffffffffffffffff];
const P384_ORDER_K: [u64; 3] = [0x1313E695333AD68D,0xA7E5F24DB74F5885,0x389CB27E0BC8D220];
const P384_ORDER_INV: [u64; 6] = [0xecec196accc52971,0x581a0db248b0a77a,0xc7634d81f4372ddf,
			0xffffffffffffffff,0xffffffffffffffff,0xffffffffffffffff];


//Signing with ECDSA is not supported, so this does not even try to be constant-time.
macro_rules! split_u
{
	($to:expr,$u:expr) => {{
		let u = $u;
		$to = u & W(0xFFFFFFFF);
		u >> 32
	}}
}

fn is_geq_p256_order(s: &[u64;4], carry: u64) -> bool
{
	let p = P256_ORDER;
	carry > 0 || s[3] > p[3] || s[3] == p[3] && s[2] > p[2] || s[3] == p[3] && s[2] == p[2] && s[1] > p[1] ||
		s[3] == p[3] && s[2] == p[2] && s[1] == p[1] && s[0] >= p[0]
}

fn is_geq_p384_order(s: &[u64;6], carry: u64) -> bool
{
	let p = P384_ORDER;
	carry > 0 || (s[5]&s[4]&s[3]) == 0xFFFFFFFFFFFFFFFF && s[2] > p[2] ||
		(s[5]&s[4]&s[3]) == 0xFFFFFFFFFFFFFFFF && s[2] == p[2] && s[1] > p[1] ||
		(s[5]&s[4]&s[3]) == 0xFFFFFFFFFFFFFFFF && s[2] == p[2] && s[1] > p[1] && s[0] >= p[0]
}

//Reduce number <2^512 mod order of P-256.
fn reduce_mod_p256_order(x: &[u64;8]) -> [u64;4]
{
	let mut r = [0;8];
	let mut s = [0;7];
	bigint_mul(&mut r[..8], &x[4..8], &P256_ORDER_K);
	bigint_add(&mut r[..8], &x[0..4]);
	bigint_mul(&mut s[..7], &r[4..8], &P256_ORDER_K);
	bigint_add(&mut s[..7], &r[0..4]);
	//Now, s < 2^448.
	bigint_mul(&mut r[..7], &s[4..7], &P256_ORDER_K);
	bigint_add(&mut r[..7], &s[0..4]);
	bigint_mul(&mut s[..6], &r[4..7], &P256_ORDER_K);
	bigint_add(&mut s[..6], &r[0..4]);
	//Now, s < 2^384.
	bigint_mul(&mut r[..6], &s[4..6], &P256_ORDER_K);
	bigint_add(&mut r[..6], &s[0..4]);
	bigint_mul(&mut s[..5], &r[4..6], &P256_ORDER_K);
	bigint_add(&mut s[..5], &r[0..4]);
	//Now, s < 2^320.
	bigint_mul(&mut r[..5], &s[4..5], &P256_ORDER_K);
	bigint_add(&mut r[..5], &s[0..4]);
	bigint_mul(&mut s[..4], &r[4..5], &P256_ORDER_K);
	let carry = bigint_add(&mut s[..4], &r[0..4]);
	//Now, s < 2*l. So conditional add suffices.
	let mut ret = [0;4];
	ret.copy_from_slice(&s[..4]);
	if is_geq_p256_order(&ret, carry) { bigint_add(&mut ret, &P256_ORDER_K); }
	ret
}

//Reduce number <2^768 mod order of P-384
fn reduce_mod_p384_order(x: &[u64;12]) -> [u64;6]
{
	let mut r = [0;9];
	let mut s = [0;8];
	bigint_mul(&mut r, &x[6..12], &P384_ORDER_K);
	bigint_add(&mut r, &x[0..6]);
	//Now r < 2^574.
	bigint_mul(&mut s, &r[6..9], &P384_ORDER_K);
	let carry = bigint_add(&mut s, &r[0..6]);
	//Now s < 2*l. So conditional add suffices.
	let mut ret = [0;6];
	ret.copy_from_slice(&s[..6]);
	if is_geq_p384_order(&ret, carry) { bigint_add(&mut ret, &P384_ORDER_K); }
	ret
}

#[derive(Copy,Clone,Debug,PartialEq,Eq)]
struct SP256([u64;4]);
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
struct SP384([u64;6]);

impl SP256
{
	fn inv(self) -> SP256
	{
		let mut g = self;
		let mut r = SP256([1,0,0,0]);
		for i in 0..256 {
			if P256_ORDER_INV[i/64] >> i%64&1 > 0 { r = r * g; }
			g = g * g;
		}
		r
	}
}

impl Mul for SP256
{
	type Output = SP256;
	fn mul(self, othr: SP256) -> SP256
	{
		let mut t = [0;8];
		bigint_mul(&mut t, &self.0, &othr.0);
		SP256(reduce_mod_p256_order(&t))
	}
}

impl SP384
{
	fn inv(self) -> SP384
	{
		let mut g = self;
		let mut r = SP384([1,0,0,0,0,0]);
		for i in 0..384 {
			if P384_ORDER_INV[i/64] >> i%64&1 > 0 { r = r * g; }
			g = g * g;
		}
		r
	}
}

impl Mul for SP384
{
	type Output = SP384;
	fn mul(self, othr: SP384) -> SP384
	{
		let mut t = [0;12];
		bigint_mul(&mut t, &self.0, &othr.0);
		SP384(reduce_mod_p384_order(&t))
	}
}


#[derive(Copy,Clone,Debug,PartialEq,Eq)]
struct FP256([u64;4]);
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
struct FP384([u64;6]);

impl FP256
{
	fn zero() -> FP256 { FP256([0;4]) }
	fn one() -> FP256 { FP256([1,0,0,0]) }
	fn from_bytes(x: &[u8;32]) -> Option<FP256>
	{
		let r = to_words!(4 x);
		const L3: u64 = 0xFFFFFFFF00000001;
		const L1: u64 = 0xFFFFFFFF;
		const L0: u64 = 0xFFFFFFFFFFFFFFFF;
		if r[3] > L3 { return None; }
		if r[3] == L3 && r[2] > 0 { return None; }
		if r[3] == L3 && r[1] > L1 { return None; }
		if r[3] == L3 && r[1] == L1 && r[0] == L0 { return None; }
		Some(FP256(r))
	}
	fn square(self) -> FP256 { self * self }
	fn inv(self) -> FP256
	{
		let mut g = self;
		let mut r = FP256([1,0,0,0]);
		for i in 0..256 {
			if P256_INV_POWER[i/64] >> i%64&1 > 0 { r = r * g; }
			g = g*g;
		}
		r
	}
	fn mul3(self) -> FP256
	{
		let mut r = [0;4];
		let carry = bigint_mac1(&mut r, &self.0, 3);
		let mut T = r.split();
		__reduce_p256(&mut T, W(carry));
		FP256(Splittable::merge(&T))
	}
}

impl FP384
{
	fn zero() -> FP384 { FP384([0;6]) }
	fn one() -> FP384 { FP384([1,0,0,0,0,0]) }
	fn from_bytes(x: &[u8;48]) -> Option<FP384>
	{
		let r = to_words!(6 x);
		const L5: u64 = 0xFFFFFFFFFFFFFFFF;
		const L2: u64 = 0xFFFFFFFFFFFFFFFE;
		const L1: u64 = 0xFFFFFFFF00000000;
		const L0: u64 = 0x00000000FFFFFFFF;
		if r[5]&r[4]&r[3] == L5 && r[2] > L2 { return None; }
		if r[5]&r[4]&r[3] == L5 && r[2] == L2 && r[1] > L1 { return None; }
		if r[5]&r[4]&r[3] == L5 && r[2] == L2 && r[1] == L1 && r[0] >= L0 { return None; }
		Some(FP384(r))
	}
	fn square(self) -> FP384 { self * self }
	fn inv(self) -> FP384
	{
		let mut g = self;
		let mut r = FP384([1,0,0,0,0,0]);
		for i in 0..384 {
			if P384_INV_POWER[i/64] >> i%64&1 > 0 { r = r * g; }
			g = g*g;
		}
		r
	}
	fn mul3(self) -> FP384
	{
		let mut r = [0;6];
		let carry = bigint_mac1(&mut r, &self.0, 3);
		let mut T = r.split();
		__reduce_p384(&mut T, W(carry));
		FP384(Splittable::merge(&T))
	}
}

impl Neg for FP256
{
	type Output = FP256;
	fn neg(self) -> FP256
	{
		//Inverting 0 gives 0.
		if self.0[0]|self.0[1]|self.0[2]|self.0[3] == 0 { return self; }
		let mut r = P256_MODULO;
		bigint_sub(&mut r, &self.0);
		FP256(r)
	}
}

impl Sub for FP256
{
	type Output = FP256;
	fn sub(self, x: FP256) -> FP256
	{
		//This does not blow limits in add_p256, even if reduce after subtract is missing.
		let mut r = P256_MODULO;
		bigint_sub(&mut r, &x.0);
		FP256(add_p256(self.0, r))
	}
}

impl Add for FP256
{
	type Output = FP256;
	fn add(self, x: FP256) -> FP256 { FP256(add_p256(self.0, x.0)) }
}

impl Mul for FP256
{
	type Output = FP256;
	fn mul(self, othr: FP256) -> FP256
	{
		const X: W<u64> = W(0x500000000);
		let mut x = [0u64;8];
		bigint_mac(&mut x, &self.0, &othr.0);
		let T = x.split();
		let mut carry;
		let mut U = [W(0);8];
		carry = split_u!(U[0], X-W(5)+T[0]+T[8]+T[9]-T[11]-T[12]-T[13]-T[14]);
		carry = split_u!(U[1], X-W(5)+T[1]+T[9]+T[10]-T[12]-T[13]-T[14]-T[15]+carry);
		carry = split_u!(U[2], X-W(5)+T[2]+T[10]+T[11]-T[13]-T[14]-T[15]+carry);
		carry = split_u!(U[3], X+T[3]-T[8]-T[9]+W(2)*T[11]+W(2)*T[12]+T[13]-T[15]+carry);
		carry = split_u!(U[4], X-W(5)+T[4]-T[9]-T[10]+W(2)*T[12]+W(2)*T[13]+T[14]+carry);
		carry = split_u!(U[5], X-W(5)+T[5]-T[10]-T[11]+W(2)*T[13]+W(2)*T[14]+T[15]+carry);
		carry = split_u!(U[6], X+T[6]-T[8]-T[9]+T[13]+W(3)*T[14]+W(2)*T[15]+carry);
		carry = split_u!(U[7], X-W(10)+T[7]+T[8]-T[10]-T[11]-T[12]-T[13]+W(3)*T[15]+carry);
		__reduce_p256(&mut U, carry);
		FP256(Splittable::merge(&U))
	}
}

impl Mul<u64> for FP256
{
	type Output = FP256;
	fn mul(self, x: u64) -> FP256
	{
		const X: W<u64> = W(0x300000000);
		let mut r = [0;4];
		let carry = bigint_mac1(&mut r, &self.0, x);
		let carry_l = W(carry & 0xFFFFFFFF);
		let carry_h = W(carry >> 32);
		let mut carry;
		let mut T = r.split();
		carry = split_u!(T[0], X-W(3)+T[0]+carry_l+carry_h);
		carry = split_u!(T[1], X-W(3)+T[1]+carry_h+carry);
		carry = split_u!(T[2], X-W(3)+T[2]+carry);
		carry = split_u!(T[3], X+T[3]-carry_l-carry_h+carry);
		carry = split_u!(T[4], X-W(3)+T[4]-carry_h+carry);
		carry = split_u!(T[5], X-W(3)+T[5]+carry);
		carry = split_u!(T[6], X+T[6]-carry_l-carry_h+carry);
		carry = split_u!(T[7], X-W(6)+T[7]+carry_l+carry);
		__reduce_p256(&mut T, carry);
		FP256(Splittable::merge(&T))
	}
}

impl Neg for FP384
{
	type Output = FP384;
	fn neg(self) -> FP384
	{
		//Inverting 0 gives 0.
		if self.0[0]|self.0[1]|self.0[2]|self.0[3]|self.0[4]|self.0[5] == 0 { return self; }
		let mut r = P384_MODULO;
		bigint_sub(&mut r, &self.0);
		FP384(r)
	}
}

impl Sub for FP384
{
	type Output = FP384;
	fn sub(self, x: FP384) -> FP384
	{
		//This does not blow limits in add_p384, even if reduce after subtract is missing.
		let mut r = P384_MODULO;
		bigint_sub(&mut r, &x.0);
		FP384(add_p384(self.0, r))
	}
}

impl Add for FP384
{
	type Output = FP384;
	fn add(self, x: FP384) -> FP384 { FP384(add_p384(self.0, x.0)) }
}

impl Mul for FP384
{
	type Output = FP384;
	fn mul(self, othr: FP384) -> FP384
	{
		let mut x = [0u64;12];
		bigint_mac(&mut x, &self.0, &othr.0);
		let T = x.split();
		const X: W<u64> = W(0x400000000);
		let mut carry;
		let mut U = [W(0);12];
		carry = split_u!(U[0], X-W(4)+T[0]+T[12]+T[20]+T[21]-T[23]);
		carry = split_u!(U[1], X+T[1]-T[12]+T[13]-T[20]+T[22]+T[23]+carry);
		carry = split_u!(U[2], X-W(4)+T[2]-T[13]+T[14]-T[21]+T[23]+carry);
		carry = split_u!(U[3], X-W(8)+T[3]+T[12]-T[14]+T[15]+T[20]+T[21]-T[22]-T[23]+carry);
		carry = split_u!(U[4], X-W(8)+T[4]+T[12]+T[13]-T[15]+T[16]+T[20]+W(2)*T[21]+T[22]-W(2)*T[23]+carry);
		carry = split_u!(U[5], X-W(4)+T[5]+T[13]+T[14]-T[16]+T[17]+T[21]+W(2)*T[22]+T[23]+carry);
		carry = split_u!(U[6], X-W(4)+T[6]+T[14]+T[15]-T[17]+T[18]+T[22]+W(2)*T[23]+carry);
		carry = split_u!(U[7], X-W(4)+T[7]+T[15]+T[16]-T[18]+T[19]+T[23]+carry);
		carry = split_u!(U[8], X-W(4)+T[8]+T[16]+T[17]-T[19]+T[20]+carry);
		carry = split_u!(U[9], X-W(4)+T[9]+T[17]+T[18]-T[20]+T[21]+carry);
		carry = split_u!(U[10], X-W(4)+T[10]+T[18]+T[19]-T[21]+T[22]+carry);
		carry = split_u!(U[11], X-W(4)+T[11]+T[19]+T[20]-T[22]+T[23]+carry);
		__reduce_p384(&mut U, carry);
		FP384(Splittable::merge(&U))
	}
}

impl Mul<u64> for FP384
{
	type Output = FP384;
	fn mul(self, x: u64) -> FP384
	{
		const X: W<u64> = W(0x200000000);
		let mut r = [0;6];
		let carry = bigint_mac1(&mut r, &self.0, x);
		let carry_l = W(carry & 0xFFFFFFFF);
		let carry_h = W(carry >> 32);
		let mut carry;
		let mut T = r.split();
		carry = split_u!(T[0], X-W(2)+T[0]+carry_l);
		carry = split_u!(T[1], X+T[1]-carry_l+carry_h+carry);
		carry = split_u!(T[2], X-W(2)+T[2]-carry_h+carry);
		carry = split_u!(T[3], X-W(4)+T[3]+carry_l+carry);
		carry = split_u!(T[4], X-W(4)+T[4]+carry_l+carry_h+carry);
		carry = split_u!(T[5], X-W(2)+T[5]+carry_h+carry);
		carry = split_u!(T[6], X-W(2)+T[6]+carry);
		carry = split_u!(T[7], X-W(2)+T[7]+carry);
		carry = split_u!(T[8], X-W(2)+T[8]+carry);
		carry = split_u!(T[9], X-W(2)+T[9]+carry);
		carry = split_u!(T[10], X-W(2)+T[10]+carry);
		carry = split_u!(T[11], X-W(2)+T[11]+carry);
		__reduce_p384(&mut T, carry);
		FP384(Splittable::merge(&T))
	}
}

#[derive(Copy,Clone,Debug)]
struct Weierstrass<F:Field>
{
	x: F,
	y: F,
	z: F,
}

impl<F:Field> Weierstrass<F>
{
	fn new() -> Weierstrass<F> { Weierstrass { x: F::zero(), y: F::zero(), z: F::zero() } }
	fn double(&self) -> Weierstrass<F>
	{
		if self.z.is_zero() { return *self; }	//Infinity.
		let delta = self.z.square();
		let gamma = self.y.square();
		let beta = self.x*gamma;
		let alpha = (self.x-delta)*(self.x+delta)*3;
		let X3 = alpha.square()-beta*8;
		let Z3 = (self.y+self.z).square()-gamma-delta;
		let Y3 = alpha*(beta*4-X3)-gamma.square()*8;
		Weierstrass{ x:X3, y:Y3, z:Z3 }
	}
}

impl<F:Field> PartialEq for Weierstrass<F>
{
	fn eq(&self, othr: &Weierstrass<F>) -> bool
	{
		let zf1 = self.z.is_zero();
		let zf2 = othr.z.is_zero();
		if zf1 || zf2 { return zf1 && zf2; }	//If either is zero, equal only if both are.
		let z1s = self.z.square();
		let z2s = othr.z.square();
		self.x*z2s == othr.x*z1s && self.y*othr.z*z2s == othr.y*self.z*z1s
	}
}

impl<F:Field> Eq for Weierstrass<F> {}

impl<'a,F:Field> Mul<&'a [u64]> for Weierstrass<F>
{
	type Output = Weierstrass<F>;
	fn mul(self, n: &'a [u64]) -> Weierstrass<F>
	{
		let mut G = self;
		let mut r = Weierstrass::new();
		for i in 0..n.len() { for j in 0..64 {
			if n[i] >> j & 1 > 0 { r = r + G; }
			G = G.double();
		}}
		r
	}
}

impl<F:Field> Neg for Weierstrass<F>
{
	type Output = Weierstrass<F>;
	fn neg(self) -> Weierstrass<F> { Weierstrass{ x: self.x, y: -self.y, z: self.z } }
}

impl<F:Field> Add for Weierstrass<F>
{
	type Output = Weierstrass<F>;
	fn add(self, othr: Weierstrass<F>) -> Weierstrass<F>
	{
		//Points at infinity as arguments.
		if self.z.is_zero() { return othr; }
		if othr.z.is_zero() { return self; }
		//Now it is known that neither point is point at infinity.
		let zsqr1 = self.z.square();
		let zsqr2 = othr.z.square();
		let expx1 = self.x*zsqr2;
		let expx2 = othr.x*zsqr1;
		let expy1 = self.y*zsqr2*othr.z;
		let expy2 = othr.y*zsqr1*self.z;
		if expx1 != expx2 {
			//x different. Use standard addition.
			let H = expx2-expx1;
			let I = (H+H).square();
			let J = H*I;
			let t3 = expy2-expy1;
			let r = t3+t3;
			let V = expx1*I;
			let X3 = r.square()-J-(V+V);
			let t8 = expy1*J;
			let Y3 = r*(V-X3)-(t8+t8);
			let Z3 = ((self.z+othr.z).square()-zsqr1-zsqr2)*H;
			Weierstrass{ x:X3, y:Y3, z:Z3 }
		} else {
			//x is the same. This is either doubling or adding opposites.
			if expy1 == expy2 {
				//(x,y) the same. Use doubling. Does not matter which point is chosen.
				self.double()
			} else {
				//(x,y) and (x,-y). Result is point at infinity.
				Weierstrass::new()
			}
		}
	}
}

macro_rules! define_weierstrass_curve
{
	($name:ident $field:ident) => {
		#[derive(Copy,Clone,Debug)]
		struct $name(Weierstrass<$field>);

		impl $name
		{
			#[cfg(test)]
			fn new() -> $name { $name(Weierstrass::new()) }
		}

		impl PartialEq for $name
		{
			fn eq(&self, x: &$name) -> bool { self.0 == x.0 }
		}

		impl Eq for $name {}

		impl<'a> Mul<&'a [u64]> for $name
		{
			type Output = $name;
			fn mul(self, n: &'a [u64]) -> $name { $name(self.0 * n) }
		}

		impl Neg for $name
		{
			type Output = $name;
			fn neg(self) -> $name { $name(-self.0) }
		}

		impl Add for $name
		{
			type Output = $name;
			fn add(self, x: $name) -> $name { $name(self.0 + x.0) }
		}
	}
}

define_weierstrass_curve!(P256 FP256);
define_weierstrass_curve!(P384 FP384);

impl P256
{
	fn stdbase() -> P256
	{
		P256(Weierstrass {
			x: FP256([0xf4a13945d898c296,0x77037d812deb33a0,0xf8bce6e563a440f2,0x6b17d1f2e12c4247]),
			y: FP256([0xcbb6406837bf51f5,0x2bce33576b315ece,0x8ee7eb4a7c0f9e16,0x4fe342e2fe1a7f9b]),
			z: FP256::one(),
		})
	}
}

impl P384
{
	fn stdbase() -> P384
	{
		P384(Weierstrass {
			x: FP384([0x3a545e3872760ab7,0x5502f25dbf55296c,0x59f741e082542a38,0x6e1d3b628ba79b98,
				0x8eb1c71ef320ad74,0xaa87ca22be8b0537]),
			y: FP384([0x7a431d7c90ea0e5f,0x0a60b1ce1d7e819d,0xe9da3113b5f0b8c0,0xf8f41dbd289a147c,
				0x5d9e98bf9292dc29,0x3617de4a96262c6f]),
			z: FP384::one(),
		})
	}
}


macro_rules! split_s
{
	($to:expr, $u:expr) => {{
		let u = $u;
		$to = u & W(0xFFFFFFFF);
		W(((u.0 as i64) >> 32) as u64)
	}}
}

macro_rules! derive_splittable
{
	($n:tt) => {
		impl Splittable for [u64;$n]
		{
			type Output = [W<u64>;2*$n];
			fn split(&self) -> [W<u64>;2*$n]
			{
				let mut T = [W(0u64);2*$n];
				for i in 0..$n { T[2*i+0] = W(self[i] & 0xFFFFFFFF); T[2*i+1] = W(self[i] >> 32); }
				T
			}
			fn merge(x: &[W<u64>;2*$n]) -> [u64;$n]
			{
				let mut r = [0;$n];
				for i in 0..$n { r[i] = x[2*i].0 | x[2*i+1].0 << 32; }
				r
			}
		}
	}
}

trait Splittable
{
	type Output: Sized;
	fn split(&self) -> <Self as Splittable>::Output;
	fn merge(x: &<Self as Splittable>::Output) -> Self;
}

derive_splittable!(4);
derive_splittable!(6);
derive_splittable!(8);
derive_splittable!(12);

fn add_p256(a: [u64;4], b: [u64;4]) -> [u64;4]
{
	let mut x = a;
	let carry = bigint_add(&mut x, &b);
	let mut T = x.split();
	__reduce_p256(&mut T, W(carry));
	Splittable::merge(&T)
}

fn add_p384(a: [u64;6], b: [u64;6]) -> [u64;6]
{
	let mut x = a;
	let carry = bigint_add(&mut x, &b);
	let mut T = x.split();
	__reduce_p384(&mut T, W(carry));
	Splittable::merge(&T)
}

fn __reduce_p256(T: &mut [W<u64>;8], mut carry: W<u64>)
{
	let mut carry2;
	carry += T[7] >> 32;	//Shift to carry in order to avoid carry2 > 1.
	T[7] &= W(0xFFFFFFFF);
	carry2 = split_s!(T[0], T[0]+carry);
	carry2 = split_s!(T[1], T[1]+carry2);
	carry2 = split_s!(T[2], T[2]+carry2);
	carry2 = split_s!(T[3], T[3]+carry2-carry);
	carry2 = split_s!(T[4], T[4]+carry2);
	carry2 = split_s!(T[5], T[5]+carry2);
	carry2 = split_s!(T[6], T[6]+carry2-carry);
	carry2 = split_s!(T[7], T[7]+carry2+carry);
	let c8 = carry2 > W(0);
	let c7i = T[7] < W(0xFFFFFFFF);
	let c6 = T[6] > W(1);
	let c6i = T[6] < W(1);
	let c5 = (T[5]|T[4]|T[3]) > W(0);
	let c2i = (T[2]&T[1]&T[0]) < W(0xFFFFFFFF);
	if c8 || (!c7i && c6) || (!c7i && !c6i && c5) || (!c7i && !c6i && !c2i) {
		//Need another reduction.
		carry2 = split_s!(T[0], T[0]+W(1));
		carry2 = split_s!(T[1], T[1]+carry2);
		carry2 = split_s!(T[2], T[2]+carry2);
		carry2 = split_s!(T[3], T[3]+carry2-W(1));
		carry2 = split_s!(T[4], T[4]+carry2);
		carry2 = split_s!(T[5], T[5]+carry2);
		carry2 = split_s!(T[6], T[6]+carry2-W(1));
		split_s!(T[7], T[7]+carry2+W(1));
	}
}


fn __reduce_p384(T: &mut [W<u64>;12], mut carry: W<u64>)
{
	let mut carry2;
	carry += T[11] >> 32;	//Shift to carry in order to avoid carry2 > 1.
	T[11] &= W(0xFFFFFFFF);
	carry2 = split_s!(T[0], T[0]+carry);
	carry2 = split_s!(T[1], T[1]+carry2-carry);
	carry2 = split_s!(T[2], T[2]+carry2);
	carry2 = split_s!(T[3], T[3]+carry2+carry);
	carry2 = split_s!(T[4], T[4]+carry2+carry);
	carry2 = split_s!(T[5], T[5]+carry2);
	carry2 = split_s!(T[6], T[6]+carry2);
	carry2 = split_s!(T[7], T[7]+carry2);
	carry2 = split_s!(T[8], T[8]+carry2);
	carry2 = split_s!(T[9], T[9]+carry2);
	carry2 = split_s!(T[10], T[10]+carry2);
	carry2 = split_s!(T[11], T[11]+carry2);
	let c12 = carry2 > W(0);
	let c11i = (T[11]&T[10]&T[9]&T[8]&T[7]&T[6]&T[5]) < W(0xFFFFFFFF);
	let c4i = T[4] < W(0xFFFFFFFE);
	let c4 = T[4] > W(0xFFFFFFFE);
	let c3i = T[3] < W(0xFFFFFFFF);
	let c2 = (T[2]|T[1]) > W(0);
	let c0i = T[0] < W(0xFFFFFFFF);
	if c12 || (!c11i && c4) || (!c11i && !c4i && !c3i && c2) || (!c11i && !c4i && !c3i && !c0i) {
		//Need another reduction.
		carry2 = split_s!(T[0], T[0]+W(1));
		carry2 = split_s!(T[1], T[1]+carry2-W(1));
		carry2 = split_s!(T[2], T[2]+carry2);
		carry2 = split_s!(T[3], T[3]+carry2+W(1));
		carry2 = split_s!(T[4], T[4]+carry2+W(1));
		carry2 = split_s!(T[5], T[5]+carry2);
		carry2 = split_s!(T[6], T[6]+carry2);
		carry2 = split_s!(T[7], T[7]+carry2);
		carry2 = split_s!(T[8], T[8]+carry2);
		carry2 = split_s!(T[9], T[9]+carry2);
		carry2 = split_s!(T[10], T[10]+carry2);
		split_s!(T[11], T[11]+carry2);
	}
}

fn __allzero(x: &[u64]) -> bool { x.iter().fold(0u64,|a,x|a|x) == 0 }

macro_rules! __ecdsa_verify
{
	($name:ident $wcnt:tt $geq:ident $scalar:ident $curve:ident $order_k:ident) => {
		fn $name(QA: $curve, r: [u8;8*$wcnt], s: [u8;8*$wcnt], hash: [u8;8*$wcnt]) ->
		Result<(), VerifyError>
		{
			use self::VerifyError::*;
			let r = to_words!($wcnt &r);
			let s = to_words!($wcnt &s);
			let z = to_words!($wcnt &hash);
			//1 <= r < n, 1 <= s < n.
			if $geq(&r, 0) || $geq(&s, 0) || __allzero(&r) || __allzero(&s) {
				return Err(BadSignature);
			}
			let sinv = $scalar(s).inv();
			let (u1,u2) = ($scalar(z)*sinv,$scalar(r)*sinv);
			let P = ($curve::stdbase() * &u1.0 + QA * &u2.0).0;
			let mut s = (P.z.square().inv() * P.x).0;
			//r < n already, so only Px needs reducing.
			if $geq(&s, 0) { bigint_add(&mut s, &$order_k); }
			if r == s { Ok(()) } else { Err(VerifyFailed) }
		}
	}
}

__ecdsa_verify!(__ecdsa_verify_p256 4 is_geq_p256_order SP256 P256 P256_ORDER_K);
__ecdsa_verify!(__ecdsa_verify_p384 6 is_geq_p384_order SP384 P384 P384_ORDER_K);

fn decode_p256(k: &[u8]) -> Option<P256>
{
	if k.len() != 64 { return None; }
	let mut x = [0;32];
	let mut y = [0;32];
	x.copy_from_slice(&k[..32]);
	y.copy_from_slice(&k[32..]);
	let x = FP256::from_bytes(&x)?;
	let y = FP256::from_bytes(&y)?;
	//Check on curve.
	if y.square() != (x.square()*x)-x.mul3()+FP256([0x3BCE3C3E27D2604B,0x651D06B0CC53B0F6,
		0xB3EBBD55769886BC,0x5AC635D8AA3A93E7]) {
		return None;
	}
	Some(P256(Weierstrass{x, y, z: FP256::one()}))
}

fn decode_p384(k: &[u8]) -> Option<P384>
{
	if k.len() != 96 { return None; }
	let mut x = [0;48];
	let mut y = [0;48];
	x.copy_from_slice(&k[..48]);
	y.copy_from_slice(&k[48..]);
	let x = FP384::from_bytes(&x)?;
	let y = FP384::from_bytes(&y)?;
	//Check on curve.
	if y.square() != (x.square()*x)-x.mul3()+FP384([0x2A85C8EDD3EC2AEF,0xC656398D8A2ED19D,
		0x0314088F5013875A,0x181D9C6EFE814112,0x988E056BE3F82D19,0xB3312FA7E23EE7E4]) {
		return None;
	}
	Some(P384(Weierstrass{x, y, z: FP384::one()}))
}

fn octets32(x: &[u8]) -> Option<[u8;32]>
{
	if x.len() <= 32 {
		let mut y = [0;32];
		(&mut y[32-x.len()..]).copy_from_slice(x);
		Some(y)
	} else {
		None
	}
}

fn octets48(x: &[u8]) -> Option<[u8;48]>
{
	if x.len() <= 48 {
		let mut y = [0;48];
		(&mut y[48-x.len()..]).copy_from_slice(x);
		Some(y)
	} else {
		None
	}
}

///Elliptic curve to apply ECDSA to.
#[derive(Debug)]
pub enum Curve
{
	///NIST P-256 curve.
	P256,
	///NIST P-384 curve.
	P384,
}

///Error from signature verification.
#[derive(Debug)]
pub enum VerifyError
{
	///The key is bad.
	BadKey,
	///The signature is bad.
	BadSignature,
	///The signature is not valid for message.
	VerifyFailed,
}

fn p256_sigdec(key: &[u8], r: &[u8], s: &[u8]) -> Result<(P256, [u8;32], [u8;32]), VerifyError>
{
	let QA = decode_p256(key).ok_or(VerifyError::BadKey)?;
	let r = octets32(r).ok_or(VerifyError::BadSignature)?;
	let s = octets32(s).ok_or(VerifyError::BadSignature)?;
	Ok((QA, r, s))
}

fn p384_sigdec(key: &[u8], r: &[u8], s: &[u8]) -> Result<(P384, [u8;48], [u8;48]), VerifyError>
{
	let QA = decode_p384(key).ok_or(VerifyError::BadKey)?;
	let r = octets48(r).ok_or(VerifyError::BadSignature)?;
	let s = octets48(s).ok_or(VerifyError::BadSignature)?;
	Ok((QA, r, s))
}

///Verify ECDSA SHA-256 signature (P-256 or P-384).
///
///The key, r and s are in raw format.
pub fn ecdsa_verify_sha384(key: &[u8], r: &[u8], s: &[u8], crvid: Curve, msg: impl Fn(&mut dyn HashUpdate)) ->
	Result<(),VerifyError>
{
	let mut hash = Sha384::new();
	msg(&mut hash);
	let hash = hash.hash();
	match crvid {
		Curve::P256 => {
			let (QA, r, s) = p256_sigdec(key, r, s)?;
			let mut hash2 = [0;32];
			hash2.copy_from_slice(&hash[..32]);
			__ecdsa_verify_p256(QA, r, s, hash2)
		},
		Curve::P384 => {
			let (QA, r, s) = p384_sigdec(key, r, s)?;
			__ecdsa_verify_p384(QA, r, s, hash)
		},
	}
}

///Verify ECDSA SHA-384 signature (P-256 or P-384).
///
///The key, r and s are in raw format.
pub fn ecdsa_verify_sha256(key: &[u8], r: &[u8], s: &[u8], crvid: Curve, msg: impl Fn(&mut dyn HashUpdate)) ->
	Result<(),VerifyError>
{
	let mut hash = Sha256::new();
	msg(&mut hash);
	let hash = hash.hash();
	match crvid {
		Curve::P256 => {
			let (QA, r, s) = p256_sigdec(key, r, s)?;
			__ecdsa_verify_p256(QA, r, s, hash)
		},
		Curve::P384 => {
			let (QA, r, s) = p384_sigdec(key, r, s)?;
			let mut hash2 = [0;48];
			(&mut hash2[16..48]).copy_from_slice(&hash);
			__ecdsa_verify_p384(QA, r, s, hash2)
		},
	}
}

trait Field: Copy+Mul<Self,Output=Self>+Add<Self,Output=Self>+Sub<Self,Output=Self>+Mul<u64,Output=Self>+
	Neg<Output=Self>+Eq
{
	fn square(self) -> Self;
	fn is_zero(self) -> bool;
	fn zero() -> Self;
}

impl Field for FP256
{
	fn square(self) -> FP256 { self.square() }
	fn is_zero(self) -> bool { self.0 == [0;4] }
	fn zero() -> FP256 { Self::zero() }
}

impl Field for FP384
{
	fn square(self) -> FP384 { self.square() }
	fn is_zero(self) -> bool { self.0 == [0;6] }
	fn zero() -> FP384 { Self::zero() }
}

#[cfg(test)] mod test;
