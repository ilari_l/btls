use super::*;


fn __test_noreduce256(x: [i32;8])
{
	let mut r = [W(0);8];
	for i in 0..8 { r[i] = W(x[i] as u32 as u64); }
	let mut T = r;
	__reduce_p256(&mut T, W(0));
	assert_eq!(T, r);
}

fn __test_reduce256(x: [i32;8], y: [i32;8])
{
	let mut r = [W(0);8];
	let mut s = [W(0);8];
	for i in 0..8 { r[i] = W(x[i] as u32 as u64); }
	for i in 0..8 { s[i] = W(y[i] as u32 as u64); }
	__reduce_p256(&mut r, W(0));
	assert_eq!(r, s);
}

fn __test_noreduce384(x: [i32;12])
{
	let mut r = [W(0);12];
	for i in 0..12 { r[i] = W(x[i] as u32 as u64); }
	let mut T = r;
	__reduce_p384(&mut T, W(0));
	assert_eq!(T, r);
}

fn __test_reduce384(x: [i32;12], y: [i32;12])
{
	let mut r = [W(0);12];
	let mut s = [W(0);12];
	for i in 0..12 { r[i] = W(x[i] as u32 as u64); }
	for i in 0..12 { s[i] = W(y[i] as u32 as u64); }
	__reduce_p384(&mut r, W(0));
	assert_eq!(r, s);
}

#[test]
fn test_reduce256()
{
	const M1: W<u64> = W(0xFFFFFFFF);
	const M2: W<u64> = W(0xFFFFFFFE);
	const P0: W<u64> = W(0);
	const P1: W<u64> = W(1);
	//The overflow case.
	let mut T = [M1,M1,M1,P0,P0,P0,P1,M1];
	__reduce_p256(&mut T, P1);
	assert_eq!(T, [P1,P0,P0,M1,M1,M1,M2,P0]);
	__test_reduce256([-1,-1,-1,0,0,0,1,-1], [0,0,0,0,0,0,0,0]);	//Just at the edge.
	__test_reduce256([0,0,0,1,0,0,1,-1], [1,0,0,0,0,0,0,0]);	//Too large term3.
	__test_reduce256([0,0,0,0,1,0,1,-1], [1,0,0,-1,0,0,0,0]);	//Too large term4.
	__test_reduce256([0,0,0,0,0,1,1,-1], [1,0,0,-1,-1,0,0,0]);	//Too large term5.
	__test_reduce256([0,0,0,0,0,0,2,-1], [1,0,0,-1,-1,-1,0,0]);	//Too large term6.
	__test_noreduce256([-2,-1,-1,0,0,0,1,-1]);	//Too small term0
	__test_noreduce256([-1,-2,-1,0,0,0,1,-1]);	//Too small term1
	__test_noreduce256([-1,-1,-2,0,0,0,1,-1]);	//Too small term2
	__test_noreduce256([-1,-1,-1,-1,-1,-1,0,-1]);	//Too small term6
	__test_noreduce256([-1,-1,-1,-1,-1,-1,-1,-2]);	//Too small term7

}

#[test]
fn test_reduce384()
{
	const M1: W<u64> = W(0xFFFFFFFF);
	const M2: W<u64> = W(0xFFFFFFFE);
	const P0: W<u64> = W(0);
	const P1: W<u64> = W(1);
	//The overflow case.
	let mut T = [M1,P0,P0,M1,M2,M1,M1,M1,M1,M1,M1,M1];
	__reduce_p384(&mut T, P1);
	assert_eq!(T, [P1,M1,M1,P0,P1,P0,P0,P0,P0,P0,P0,P0]);
	//Just at the edge.
	__test_reduce384([-1,0,0,-1,-2,-1,-1,-1,-1,-1,-1,-1], [0,0,0,0,0,0,0,0,0,0,0,0]);
	//Too large.
	__test_reduce384([0,1,0,-1,-2,-1,-1,-1,-1,-1,-1,-1], [1,0,0,0,0,0,0,0,0,0,0,0]);
	__test_reduce384([0,0,1,-1,-2,-1,-1,-1,-1,-1,-1,-1], [1,-1,0,0,0,0,0,0,0,0,0,0]);
	__test_reduce384([0,0,0,0,-1,-1,-1,-1,-1,-1,-1,-1], [1,-1,-1,0,0,0,0,0,0,0,0,0]);
	//Too small.
	__test_noreduce384([-2,0,0,-1,-2,-1,-1,-1,-1,-1,-1,-1]);
	__test_noreduce384([-1,-1,-1,-2,-2,-1,-1,-1,-1,-1,-1,-1]);
	__test_noreduce384([-1,-1,-1,-1,-3,-1,-1,-1,-1,-1,-1,-1]);
	__test_noreduce384([-1,-1,-1,-1,-1,-2,-1,-1,-1,-1,-1,-1]);
	__test_noreduce384([-1,-1,-1,-1,-1,-1,-2,-1,-1,-1,-1,-1]);
	__test_noreduce384([-1,-1,-1,-1,-1,-1,-1,-2,-1,-1,-1,-1]);
	__test_noreduce384([-1,-1,-1,-1,-1,-1,-1,-1,-2,-1,-1,-1]);
	__test_noreduce384([-1,-1,-1,-1,-1,-1,-1,-1,-1,-2,-1,-1]);
	__test_noreduce384([-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-2,-1]);
	__test_noreduce384([-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-2]);
}

#[test]
fn random_field_laws_p256()
{
	use std::io::Read;
	let mut a = [0;32];
	let mut b = [0;32];
	let mut c = [0;32];
	let mut fp = std::fs::File::open("/dev/urandom").expect("Failed to open urandom");
	let z = FP256([0;4]);
	let o = FP256([1,0,0,0]);
	for _ in 0..10000 {
		fp.read_exact(&mut a).expect("Failed to read urandom");
		fp.read_exact(&mut b).expect("Failed to read urandom");
		fp.read_exact(&mut c).expect("Failed to read urandom");
		let a = match FP256::from_bytes(&a) { Some(x) => x, None => continue };
		let b = match FP256::from_bytes(&b) { Some(x) => x, None => continue };
		let c = match FP256::from_bytes(&c) { Some(x) => x, None => continue };
		//The sum better be associative, commutative, have neutral element and have inverse.
		assert_eq!((a+b)+c, a+(b+c));
		assert_eq!(a+b, b+a);
		assert_eq!(a+z, a);
		assert_eq!(a+(-a), z);
		//The field better be distributive.
		assert_eq!((a+b)*c, a*c+b*c);
		//The product better be associative, commutative, have neutral element and have inverse.
		assert_eq!((a*b)*c, a*(b*c));
		assert_eq!(a*b, b*a);
		assert_eq!(a*o, a);
		//assert_eq!(a*1u64, a);
		assert_eq!(a*a.inv(), o);
		//Square.
		assert_eq!(a*a, a.square());
		//Properties of minus and negation.
		assert_eq!(b-z, b);
		assert_eq!(z-b, -b);
		assert_eq!(a-b, a+(-b));
		assert_eq!((a-b)+(b-a), z);
		assert_eq!(-(a-b), b-a);
		assert_eq!(a-a, z);
		assert_eq!(a+b-b, a);
		assert_eq!(a+b-a, b);
		//Properties of inverse.
		assert_eq!(a*b*a.inv(), b);
		assert_eq!(a*b*b.inv(), a);
		//Small multiply.
		//let ds = c.0[0];
		//let dl = F25519([ds,0,0,0,0]);
		//assert_eq!(a*ds, a*dl);
	}
}

#[test]
fn random_field_laws_p384()
{
	use std::io::Read;
	let mut a = [0;48];
	let mut b = [0;48];
	let mut c = [0;48];
	let mut fp = std::fs::File::open("/dev/urandom").expect("Failed to open urandom");
	let z = FP384([0;6]);
	let o = FP384([1,0,0,0,0,0]);
	for _ in 0..10000 {
		fp.read_exact(&mut a).expect("Failed to read urandom");
		fp.read_exact(&mut b).expect("Failed to read urandom");
		fp.read_exact(&mut c).expect("Failed to read urandom");
		let a = match FP384::from_bytes(&a) { Some(x) => x, None => continue };
		let b = match FP384::from_bytes(&b) { Some(x) => x, None => continue };
		let c = match FP384::from_bytes(&c) { Some(x) => x, None => continue };
		//The sum better be associative, commutative, have neutral element and have inverse.
		assert_eq!((a+b)+c, a+(b+c));
		assert_eq!(a+b, b+a);
		assert_eq!(a+z, a);
		assert_eq!(a+(-a), z);
		//The field better be distributive.
		assert_eq!((a+b)*c, a*c+b*c);
		//The product better be associative, commutative, have neutral element and have inverse.
		assert_eq!((a*b)*c, a*(b*c));
		assert_eq!(a*b, b*a);
		assert_eq!(a*o, a);
		//assert_eq!(a*1u64, a);
		assert_eq!(a*a.inv(), o);
		//Square.
		assert_eq!(a*a, a.square());
		//Properties of minus and negation.
		assert_eq!(b-z, b);
		assert_eq!(z-b, -b);
		assert_eq!(a-b, a+(-b));
		assert_eq!((a-b)+(b-a), z);
		assert_eq!(-(a-b), b-a);
		assert_eq!(a-a, z);
		assert_eq!(a+b-b, a);
		assert_eq!(a+b-a, b);
		//Properties of inverse.
		assert_eq!(a*b*a.inv(), b);
		assert_eq!(a*b*b.inv(), a);
		//Small multiply.
		//let ds = c.0[0];
		//let dl = F25519([ds,0,0,0,0]);
		//assert_eq!(a*ds, a*dl);
	}
}

#[test]
fn p256_add_double_consistent()
{
	let g1 = P256::stdbase();
	let g2 = g1 + g1;
	let g3a = g2 + g1;
	let g3b = g1 + g2;
	let g4a = g3a + g1;
	let g4b = g1 + g3a;
	let g4c = g2 + g2;
	assert_eq!(g3a,g3b); assert_eq!(g4a,g4b); assert_eq!(g4a,g4c); assert_eq!(g4b,g4c);
	assert!(g1!=g2); assert!(g1!=g3a); assert!(g1!=g4a); assert!(g2!=g3a); assert!(g2!=g4a);
	assert!(g3a!=g4a); assert!(g1!=P256::new());
}

#[test]
fn p384_add_double_consistent()
{
	let g1 = P384::stdbase();
	let g2 = g1 + g1;
	let g3a = g2 + g1;
	let g3b = g1 + g2;
	let g4a = g3a + g1;
	let g4b = g1 + g3a;
	let g4c = g2 + g2;
	assert_eq!(g3a,g3b); assert_eq!(g4a,g4b); assert_eq!(g4a,g4c); assert_eq!(g4b,g4c);
	assert!(g1!=g2); assert!(g1!=g3a); assert!(g1!=g4a); assert!(g2!=g3a); assert!(g2!=g4a);
	assert!(g3a!=g4a); assert!(g1!=P384::new());
}

#[test]
fn p256_order()
{
	assert_eq!(P256::stdbase() * &P256_ORDER, P256::new());
}

#[test]
fn p384_order()
{
	assert_eq!(P384::stdbase() * &P384_ORDER, P384::new());
}


#[test]
fn mulmod_fp256()
{
	use std::io::Read;
	let mut a = [0;32];
	let mut b = [0;32];
	let mut fp = std::fs::File::open("/dev/urandom").expect("Failed to open urandom");
	for _ in 0..10000 {
		fp.read_exact(&mut a).expect("Failed to read urandom");
		fp.read_exact(&mut b).expect("Failed to read urandom");
		let a = match FP256::from_bytes(&a) { Some(x) => x, None => continue };
		let b = match FP256::from_bytes(&b) { Some(x) => x, None => continue };
		let c = a * b;
		let mut d = [0;8];
		bigint_mac(&mut d, &a.0, &b.0);
		crate::bigint::bigint_mod_nct(&mut d, &P256_MODULO);
		assert_eq!(&d[..4], &c.0);
		assert_eq!(&d[4..], &[0;4]);
	}
}

#[test]
fn mulmod_fp384()
{
	use std::io::Read;
	let mut a = [0;48];
	let mut b = [0;48];
	let mut fp = std::fs::File::open("/dev/urandom").expect("Failed to open urandom");
	for _ in 0..10000 {
		fp.read_exact(&mut a).expect("Failed to read urandom");
		fp.read_exact(&mut b).expect("Failed to read urandom");
		let a = match FP384::from_bytes(&a) { Some(x) => x, None => continue };
		let b = match FP384::from_bytes(&b) { Some(x) => x, None => continue };
		let c = a * b;
		let mut d = [0;12];
		bigint_mac(&mut d, &a.0, &b.0);
		crate::bigint::bigint_mod_nct(&mut d, &P384_MODULO);
		assert_eq!(&d[..6], &c.0);
		assert_eq!(&d[6..], &[0;6]);
	}
}

#[test]
fn decode_p256_stdbase()
{
	let p = b"\x6b\x17\xd1\xf2\xe1\x2c\x42\x47\xf8\xbc\xe6\xe5\x63\xa4\x40\xf2\x77\x03\x7d\x81\x2d\xeb\x33\
		\xa0\xf4\xa1\x39\x45\xd8\x98\xc2\x96\x4f\xe3\x42\xe2\xfe\x1a\x7f\x9b\x8e\xe7\xeb\x4a\x7c\x0f\
		\x9e\x16\x2b\xce\x33\x57\x6b\x31\x5e\xce\xcb\xb6\x40\x68\x37\xbf\x51\xf5";
	let g = decode_p256(p).expect("Standard base does not decode");
	assert_eq!(g, P256::stdbase());
}

#[test]
fn decode_p384_stdbase()
{
	let p = b"\xaa\x87\xca\x22\xbe\x8b\x05\x37\x8e\xb1\xc7\x1e\xf3\x20\xad\x74\x6e\x1d\x3b\x62\x8b\xa7\x9b\
		\x98\x59\xf7\x41\xe0\x82\x54\x2a\x38\x55\x02\xf2\x5d\xbf\x55\x29\x6c\x3a\x54\x5e\x38\x72\x76\
		\x0a\xb7\x36\x17\xde\x4a\x96\x26\x2c\x6f\x5d\x9e\x98\xbf\x92\x92\xdc\x29\xf8\xf4\x1d\xbd\x28\
		\x9a\x14\x7c\xe9\xda\x31\x13\xb5\xf0\xb8\xc0\x0a\x60\xb1\xce\x1d\x7e\x81\x9d\x7a\x43\x1d\x7c\
		\x90\xea\x0e\x5f";
	let g = decode_p384(p).expect("Standard base does not decode");
	assert_eq!(g, P384::stdbase());
}

#[test]
fn decode_p256_badbase()
{
	let p = b"\x6b\x17\xd1\xf2\xe1\x2c\x42\x47\xf8\xbc\xe6\xe5\x63\xa4\x40\xf2\x77\x03\x7d\x81\x2d\xeb\x33\
		\xa0\xf4\xa1\x39\x45\xd8\x98\xc2\x96\x4f\xe3\x42\xe2\xfe\x1a\x7f\x9b\x8e\xe7\xeb\x4a\x7c\x0f\
		\x9e\x16\x2b\xce\x33\x57\x6b\x31\x5e\xce\xcb\xb6\x40\x68\x37\xbf\x51\xf6";
	assert!(decode_p256(p).is_none());
}

#[test]
fn decode_p384_badbase()
{
	let p = b"\xaa\x87\xca\x22\xbe\x8b\x05\x37\x8e\xb1\xc7\x1e\xf3\x20\xad\x74\x6e\x1d\x3b\x62\x8b\xa7\x9b\
		\x98\x59\xf7\x41\xe0\x82\x54\x2a\x38\x55\x02\xf2\x5d\xbf\x55\x29\x6c\x3a\x54\x5e\x38\x72\x76\
		\x0a\xb7\x36\x17\xde\x4a\x96\x26\x2c\x6f\x5d\x9e\x98\xbf\x92\x92\xdc\x29\xf8\xf4\x1d\xbd\x28\
		\x9a\x14\x7c\xe9\xda\x31\x13\xb5\xf0\xb8\xc0\x0a\x60\xb1\xce\x1d\x7e\x81\x9d\x7a\x43\x1d\x7c\
		\x90\xea\x0e\x5e";
	assert!(decode_p384(p).is_none());
}

#[test]
fn test_p256_sig()
{
	let tbs=[32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32,
		32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32,
		32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 84, 76, 83, 32, 49, 46, 51, 44,
		32, 115, 101, 114, 118, 101, 114, 32, 67, 101, 114, 116, 105, 102, 105, 99, 97, 116, 101, 86,
		101, 114, 105, 102, 121, 0, 233, 178, 72, 40, 242, 28, 208, 76, 222, 233, 207, 82, 159, 142,
		184, 205, 240, 242, 3, 203, 131, 236, 78, 82, 28, 70, 179, 193, 161, 213, 163, 167];
	let key: [u8;64] = [116, 113, 37, 153, 8, 202, 130, 103, 167, 97, 64, 59, 100, 113, 222, 122, 160, 108, 92,
		220, 68, 82, 209, 206, 189, 35, 37, 194, 210, 195, 110, 10, 73, 137, 62, 124, 74, 138, 134, 138,
		139, 18, 166, 131, 196, 98, 48, 66, 211, 215, 16, 218, 86, 247, 173, 136, 135, 75, 252, 168, 48,
		0, 154, 237];
	let r: [u8;32] = [182, 232, 26, 27, 175, 251, 245, 130, 94, 28, 211, 174, 228, 170, 29, 147, 137, 251,
		72, 129, 38, 159, 16, 220, 233, 5, 191, 245, 111, 100, 120, 104];
	let s: [u8;32] = [84, 218, 141, 236, 221, 86, 220, 184, 120, 81, 111, 150, 33, 38, 175, 144, 248, 69, 152,
		74, 136, 102, 251, 2, 51, 185, 92, 192, 28, 6, 201, 121];
	ecdsa_verify_sha256(&key, &r, &s, super::Curve::P256, |h|h.update(&tbs)).expect("Signature failed");
}

#[test]
fn test_p256_bad_sig()
{
	let tbs=[32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32,
		32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32,
		32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 84, 76, 83, 32, 49, 46, 51, 44,
		32, 115, 101, 114, 118, 101, 114, 32, 67, 101, 114, 116, 105, 102, 105, 99, 97, 116, 101, 86,
		101, 114, 105, 102, 121, 0, 233, 178, 72, 40, 242, 28, 208, 76, 222, 233, 207, 82, 159, 142,
		184, 205, 240, 242, 3, 203, 131, 236, 78, 82, 28, 70, 179, 193, 161, 213, 163, 168];
	let key: [u8;64] = [116, 113, 37, 153, 8, 202, 130, 103, 167, 97, 64, 59, 100, 113, 222, 122, 160, 108, 92,
		220, 68, 82, 209, 206, 189, 35, 37, 194, 210, 195, 110, 10, 73, 137, 62, 124, 74, 138, 134, 138,
		139, 18, 166, 131, 196, 98, 48, 66, 211, 215, 16, 218, 86, 247, 173, 136, 135, 75, 252, 168, 48,
		0, 154, 237];
	let r: [u8;32] = [182, 232, 26, 27, 175, 251, 245, 130, 94, 28, 211, 174, 228, 170, 29, 147, 137, 251,
		72, 129, 38, 159, 16, 220, 233, 5, 191, 245, 111, 100, 120, 104];
	let s: [u8;32] = [84, 218, 141, 236, 221, 86, 220, 184, 120, 81, 111, 150, 33, 38, 175, 144, 248, 69, 152,
		74, 136, 102, 251, 2, 51, 185, 92, 192, 28, 6, 201, 121];
	assert!(ecdsa_verify_sha256(&key, &r, &s, super::Curve::P256, |h|h.update(&tbs)).is_err());
}
