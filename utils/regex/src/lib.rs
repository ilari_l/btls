#![allow(dead_code)]
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::ResultExt;
use btls_aux_memory::asciiz_slice;
use btls_aux_memory::Attachment;
use btls_aux_memory::split_attach_first;
use std::cmp::min;
use std::ops::Range;
use std::str::from_utf8;
use std::str::FromStr;

#[repr(C)]
struct __RegexHandle(());

#[repr(C)]
#[derive(Copy,Clone,Debug)]
pub struct RegexMatch(usize, usize);

impl RegexMatch
{
	pub fn new() -> RegexMatch { RegexMatch(!0,!0) }
	pub fn new_range(r: Range<usize>) -> RegexMatch { RegexMatch(r.start, r.end) }
	pub fn slice<'a>(&self, s: &'a [u8]) -> Option<&'a [u8]> { s.get(self.0..self.1) }
}

pub const REG_EXTENDED: i32 = 1;
pub const REG_ICASE: i32 = 2;
pub const REG_NOSUB: i32 = 4;
pub const REG_NEWLINE: i32 = 8;
pub const REG_AUTOANCHOR: i32 = 16;	//Auto-add ^ and $ anchors.
const REG_INT_MASK: i32 = 15;
pub const REG_NOTBOL: i32 = 1;
pub const REG_NOTEOL: i32 = 2;
pub const REG_STARTEND: i32 = 4;

#[allow(improper_ctypes)]
extern {
	fn httpdemux2_regex_comp(regex: *const u8, flags: i32, error: *mut u8, errorlen: usize) ->
		*mut __RegexHandle;
	fn httpdemux2_regex_exec(preg: *const __RegexHandle, string: *const u8, nmatch: usize,
		pmatch: *mut RegexMatch, flags: i32, error: *mut u8, errorlen: usize) -> i32;
	fn httpdemux2_regex_free(preg: *mut __RegexHandle);
}

#[derive(Debug)]
pub struct RegexHandle(*mut __RegexHandle);



//Regexec is MT-safe, so..
unsafe impl Send for RegexHandle {}
unsafe impl Sync for RegexHandle {}

impl Drop for RegexHandle
{
	fn drop(&mut self)
	{
		//It is invariant of RegexHandle to own __RegexHandle.
		unsafe{httpdemux2_regex_free(self.0)};
	}
}

impl RegexHandle
{
	pub fn new<'e>(regex: &[u8], flags: i32, error: &'e mut [u8]) -> Result<RegexHandle, &'e [u8]>
	{
		//Allocate temporary copy, since NUL needs to be added.
		let mut regex2 = if flags & REG_AUTOANCHOR != 0 { anchor_expression(regex) } else { regex.to_vec() };
		if regex2.iter().any(|&x|x==0) {
			return Err(b"Embedded NUL in regular expression is not allowed");
		}
		regex2.push(0);
		let handle = unsafe{httpdemux2_regex_comp(regex2.as_ptr(), flags & REG_INT_MASK, error.as_mut_ptr(),
			error.len())};
		if handle.is_null() {
			Err(asciiz_slice(error))
		} else {
			Ok(RegexHandle(handle))
		}
	}
	pub fn match_string<'e>(&self, string: &[u8], notbol: bool, noteol: bool, pmatch: &mut [RegexMatch],
		error: &'e mut [u8]) -> Result<bool, &'e [u8]>
	{
		let mut flags = REG_STARTEND;
		if notbol { flags |= REG_NOTBOL; }
		if noteol { flags |= REG_NOTEOL; }
		let mut pmatch2 = [RegexMatch(0, string.len())];	//Temporary pmatch.
		let (pmatchptr, pmatchlen) = if pmatch.len() > 0 {
			pmatch[0].0 = 0;
			pmatch[0].1 = string.len();
			(pmatch.as_mut_ptr(), pmatch.len())
		} else {
			(pmatch2.as_mut_ptr(), pmatch2.len())
		};
		let r = unsafe{httpdemux2_regex_exec(self.0, string.as_ptr(), pmatchlen, pmatchptr,
			flags, error.as_mut_ptr(), error.len())};
		if r > 0 {
			Ok(true)
		} else if r == 0 {
			Ok(false)
		} else {
			Err(asciiz_slice(error))
		}
	}
}

#[derive(Copy,Clone)]
struct PartInfo
{
	//If there is any string that part accepts, then empty string is one of those.
	maybe_empty: bool,
	//Any non-empty string accpted by part starts with /.
	start_slash: bool,
}

#[derive(Copy,Clone)]
struct PrefixState
{
	//Current write position.
	pos: usize,
	//Maximum point output has been to.
	maxpos: usize,
	//Mismatch flag:
	//0 => No mismatch.
	//1 => Mismatch occurs at maxpos. No writes past maxpos have been seen.
	//2 => Mismatch occurs at maxpos. Writes past have been seen, all are slashes.
	//3 => Mismatch occurs at maxpos. Writes past have been seen, some are non-slashes.
	mismatch: u8,
}

#[derive(Copy,Clone)]
pub enum RegexSpecial
{
	ExactMatch,
	AlwaysMatch,
	AlwaysMatchNC,
}

#[derive(Copy,Clone)]
pub struct InvarantPrefixResult<'a>
{
	pub invariant: &'a [u8],
	pub next_slash: bool,
	pub special: Option<RegexSpecial>,
}

fn ret_special_ipr<'a>(output: &'a [u8], outlen: usize, next_slash: bool, special: RegexSpecial) ->
	InvarantPrefixResult<'a>
{
	let invariant = if outlen > output.len() { output } else { &output[..outlen] };
	InvarantPrefixResult { invariant, next_slash, special: Some(special) }
}

pub fn get_regex_invariant_prefix<'a>(mut regex: &[u8], output: &'a mut [u8]) ->
	Result<InvarantPrefixResult<'a>, String>
{
	//This is first time, so the whole output can be written.
	let mut pstate = PrefixState {
		pos: 0,
		maxpos: 0,
		mismatch: 0,
	};
	if let Some((outlen, remainder)) = inv_parse_specialprefix(regex, output) {
		match remainder {
			b"" => return Ok(ret_special_ipr(output, outlen, false, RegexSpecial::ExactMatch)),
			b".*" => return Ok(ret_special_ipr(output, outlen, false, RegexSpecial::AlwaysMatchNC)),
			b"(.*)" => return Ok(ret_special_ipr(output, outlen, false, RegexSpecial::AlwaysMatch)),
			b"(/.*)?" => return Ok(ret_special_ipr(output, outlen, true, RegexSpecial::AlwaysMatch)),
			//The remaining forms are not magic.
			_ => ()
		}
	}
	inv_parse_regex(&mut regex, output, &mut pstate)?;
	//If remainder starts with ), then there is unbalanced ). Otherwise all input should have been consumed.
	fail_if!(regex.starts_with(b")"), "Unpaired )");
	fail_if!(regex.len() > 0, "BUG: Unexpected character after inv_parse_regex()");
	Ok(InvarantPrefixResult {
		invariant: if pstate.maxpos > output.len() { output } else { &output[..pstate.maxpos] },
		next_slash: pstate.mismatch == 2,
		special: None,
	})
}

fn write_special_byte(output: &mut [u8], outidx: &mut usize, val: u8) -> Option<()>
{
	output.get_mut(*outidx).map(|t|*t=val)?;
	*outidx += 1;
	Some(())
}

fn inv_parse_specialprefix<'a>(regex: &'a [u8], output: &mut [u8]) -> Option<(usize, &'a [u8])>
{
	let mut escaped = false;
	let mut outidx = 0;
	let mut inidx = 0;
	while inidx < regex.len() {
		let b = regex[inidx];
		if escaped {
			//If this overflows the buffer, the string gets marked as not special.
			write_special_byte(output, &mut outidx, b)?;
			escaped = false;
		} else { match b {
			//Escape.
			b'\\' => escaped = true,
			//Specials are not allowed, and get returned as remainder.
			b'('|b')'|b'['|b']'|b'{'|b'}'|b'|'|b'+'|b'*'|b'?'|b'^'|b'$'|b'.' => break,
			//If this overflows the buffer, the string gets marked as not special.
			b => write_special_byte(output, &mut outidx, b)?
		}}
		inidx += 1;
	}
	//Okay, return the remainder.
	Some((outidx, &regex[inidx..]))
}

fn inv_parse_regex(regex: &mut &[u8], output: &mut [u8], pstate: &mut PrefixState) -> Result<PartInfo, String>
{
	//Regexp contains list of branches, All branches must have the same prefix. Write position is reset
	//every iteration. If returned pos is not the same as existing, mismatch happens at minimum position.
	let pos = pstate.pos;
	let mut last_maxpos = None;
	let mut maybe_empty = regex.len() == 0;	//Empty regex accepts empty.
	let mut start_slash = true;
	while regex.len() > 0 {
		pstate.pos = pos;
		let pinfo = inv_parse_branch(regex, output, pstate)?;
		maybe_empty |= pinfo.maybe_empty;
		start_slash &= pinfo.start_slash;
		//If branches produce different lengths, truncate at smallest offset.
		if let Some(last_maxpos) = last_maxpos {
			if last_maxpos != pstate.pos {
				let mpos = min(last_maxpos, pstate.pos);
				mismatch(pstate, output, Some(mpos), false);
			}
		} else {
			last_maxpos = Some(pstate.pos);		//Just save.
		}
		//The next character after branch may be | or ). | is consumed, ) ends subregex.
		*regex = match regex.split_first() {
			Some((&b'|', tail)) => tail,
			Some((&b')', _)) => break,
			Some(_) => fail!("BUG: Unsxpected character after inv_parse_branch()"),
			None => break,	//Empty.
		}
	}
	Ok(PartInfo {
		maybe_empty: maybe_empty,
		start_slash: start_slash,
	})
}

fn inv_parse_branch(regex: &mut &[u8], output: &mut [u8], pstate: &mut PrefixState) -> Result<PartInfo, String>
{
	//Branch consists of concatenated list of expressions. All those act in succession.
	let mut maybe_empty = true;
	let mut start_slash = true;
	let mut empty_so_far = true;
	while regex.len() > 0 {
		//If the next character is | or ), end branch.
		let c = regex[0];
		if c == b'|' || c == b')' { break; }
		let pinfo = inv_parse_expresion(regex, output, pstate)?;
		maybe_empty &= pinfo.maybe_empty;
		if empty_so_far { start_slash &= pinfo.start_slash; }
		empty_so_far &= pinfo.maybe_empty;
	}
	Ok(PartInfo {
		maybe_empty: maybe_empty,
		start_slash: start_slash,
	})
}

fn inv_parse_expresion(regex: &mut &[u8], output: &mut [u8], pstate: &mut PrefixState) -> Result<PartInfo, String>
{
	let (first, tail) = regex.split_first().
		ok_or("BUG: called inv_parse_expresion() with empty input")?;
	*regex = tail;
	let rep_start = pstate.pos;
	let mut maybe_empty = false;
	let mut start_slash = false;
	match *first {
		b'|' => fail!("BUG: called inv_parse_expresion() with |"),
		b')' => fail!("BUG: called inv_parse_expresion() with )"),
		b'*'|b'+'|b'?'|b'{' => fail!("Dangling repeat expression"),
		b'(' => {
			let pinfo = inv_parse_regex(regex, output, pstate)?;
			maybe_empty = pinfo.maybe_empty;
			start_slash = pinfo.start_slash;
			//This must end at ), which is consumed.
			*regex = match regex.split_first() {
				Some((&b')',tail)) => tail,
				_ => fail!("Unpaired (")
			};
		},
		b'[' => {
			if regex.starts_with(b"/]") { start_slash = true; }
			inv_parse_bracket(regex, output, pstate)?;
			//This must end at ], which is consumed.
			*regex = match regex.split_first() {
				Some((&b']',tail)) => tail,
				_ => fail!("Unpaired [")
			};
		},
		b'\\' => {
			//Consume next character.
			let (first, tail) = regex.split_first().ok_or("Dangling \\")?;
			*regex = tail;
			match *first {
				//This turns specials into non-speial character.
				b'^'|b'.'|b'['|b'$'|b'('|b')'|b'|'|b'*'|b'+'|b'?'|b'{'|b'\\' =>
					inv_append_byte(output, pstate, *first),
				_ => fail!("Invalid escape sequence")
			}
		},
		//Anchors are ignored. Incorrect use of anchors makes the regex not match anything, but that
		//does not alter invariant prefix, as it only applies to matching expression.
		b'^'|b'$' => maybe_empty = true,
		//'.' always mismatches, and never is empty. Might not be /, so hard conflict.
		b'.' => mismatch(pstate, output, None, true),
		//Anything else matches itself and never is empty.
		other => inv_append_byte(output, pstate, other)
	}
	if *first == b'/' || *first == b'^' || *first == b'$' { start_slash = true; }
	//Handle repeats. Repeats never modify start_slash.
	let mut min_rep = 1u64;
	let mut more_rep = false;
	loop {
		match regex.split_first() {
			Some((&b'*', tail))|Some((&b'?', tail)) => {
				min_rep = 0;
				more_rep = true;
				*regex = tail;
			},
			Some((&b'+', tail)) => {
				more_rep = true;
				*regex = tail;
			},
			Some((&b'{', tail)) => {
				*regex = tail;
				let (minc, maxc) = parse_duplication(regex)?;
				fail_if!(minc == 0 && maxc == Some(0), "Repeat count 0 not allowed");
				min_rep = min_rep.saturating_mul(minc);
				more_rep |= maxc != Some(minc);
				//This must end at }, which is consumed.
				*regex = match regex.split_first() {
					Some((&b'}',tail)) => tail,
					_ => fail!("Unpaired {")
				};
			},
			//Others are not repeat expressions.
			_ => break
		}
	}
	//If repeat is trivial, do not ever mismatch.
	if pstate.pos > rep_start {
		if min_rep > 0 {
			//Repeat the expression and then mismatch if there might be more.
			repeat_n(output, pstate, rep_start, min_rep);
			if more_rep {
				mismatch(pstate, output, None, false);
				//If at mismatch point, and there is no write past, do speculative write of slash
				//or non-slash according to if base part can start with slash or not.
				if pstate.pos == pstate.maxpos && pstate.mismatch == 1 {
					pstate.mismatch = if start_slash { 2 } else { 3 };
				}
			}
		} else {
			//The expression might not be included, so mismatch at start. more_rep is guaranteed to
			//be set. This always sets back mismatch position, so information about write one past
			//mismatch is always set.
			maybe_empty = true;
			mismatch(pstate, output, Some(rep_start), false);
		}
	}
	Ok(PartInfo {
		maybe_empty: maybe_empty,
		start_slash: start_slash,
	})
}

fn mismatch(pstate: &mut PrefixState, output: &[u8], itr: Option<usize>, hard: bool)
{
	let itr = itr.unwrap_or(pstate.pos);
	//If maxpos is reduced, speculate slash if first cut away character is '/'. Do not set mismatch if position
	//is out of range.
	if itr < pstate.maxpos {
		pstate.maxpos = itr;
		pstate.mismatch = match output.get(itr) { Some(&b'/') => 2, _ => 3 };
	} else if itr == pstate.maxpos {
		pstate.mismatch = if hard { 3 } else { 1 };
	}
}

fn inv_append_byte(output: &mut [u8], pstate: &mut PrefixState, ch: u8)
{
	//If writing slash one past maxpos, and nothing past maxpos has been seen, mark write as slash.
	if pstate.pos == pstate.maxpos && ch == b'/' && pstate.mismatch == 1 { pstate.mismatch = 2; }
	//If writing one past maxpos and the character is not slash, clear the speculation on slash.
	if pstate.pos == pstate.maxpos && ch != b'/' && pstate.mismatch > 0 { pstate.mismatch = 3; }
	if pstate.pos >= pstate.maxpos {
		//This is farthest this has ever been. If not in mismatch, push character.
		if pstate.mismatch == 0 && pstate.pos < output.len() {
			output[pstate.pos] = ch;
			pstate.maxpos = pstate.pos.saturating_add(1);
		}
	} else {
		//Check that written value matches what was there previously. If not equal, mismatch happens.
		//Set some non-slash has been seen, as one of the writes is a non-slash (or too far to keep
		//track of).
		if pstate.pos >= output.len() || output[pstate.pos] != ch {
			pstate.mismatch = 3;
			pstate.maxpos = min(pstate.pos, pstate.maxpos);
		}
	}
	pstate.pos = pstate.pos.saturating_add(1);
}

fn repeat_n(output: &mut [u8], pstate: &mut PrefixState, rep_start: usize, n: u64)
{
	if n < 2 { return; }	//Trivial operation.
	let bytes = (n-1).saturating_mul((pstate.pos - rep_start) as u64);
	let mut i = 0;
	while i < bytes {
		let j = rep_start + i as usize;
		if j >= output.len() { break; }
		let b = output[j];
		inv_append_byte(output, pstate, b);
		i+=1;
	}
}


fn inv_parse_bracket(regex: &mut &[u8], output: &mut [u8], pstate: &mut PrefixState) -> Result<(), String>
{
	let mut single = None;
	//0: Init
	//1: After start.
	//2: After start and -.
	//3: After end or init negated.
	//4: After class.
	//5: After class and -.
	let mut state = 0;
	loop {
		if regex.len() == 0 || regex.starts_with(b"]") { break; }
		fail_if!(regex.starts_with(b"[="), "Equivalence classes are not supported");
		fail_if!(regex.starts_with(b"[."), "Collating symbols are not supported");
		if let Some(tail) = regex.strip_prefix(b"[:") {
			let (cclass, tail) = split_attach_first(tail, b":]", Attachment::Center).
				set_err("Only standard character classes are allowed")?;
			//Whitelist of allowed character classes.
			match cclass {
				b"alnum"|b"alpha"|b"blank"|b"cntrl"|b"digit"|b"graph"|b"lower"|b"print"|
					b"punct"|b"space"|b"upper"|b"xdigit" => (),
				_ => fail!("Only standard character classes are allowed")
			}
			*regex = tail;
			fail_if!(state == 2 || state == 5, "Class can not be used as range endpoint");
			state = 4;
			single = Some(None);
		} else {
			//Single element, ^ or -.
			state = match (state, regex[0]) {
				(0, b'^') => 3,
				(0, b'-') => 1,
				(0, _) => 1,
				(1, b'-') => 2,
				(1, _) => 1,
				(2, _) => 3,
				(3, b'-') => 1,
				(3, _) => 1,
				(4, b'-') => 5,
				(4, _) => 1,
				(5, _) => fail!("Class can not be used as range endpoint"),
				(_, _) => fail!("BUG: Invalid state in inv_parse_bracket()")
			};
			single = Some(if single.is_some() { None } else { Some(regex[0]) });
			*regex = &regex[1..];
		}
	}
	match single {
		None => fail!("Empty set not allowed"),
		Some(Some(x)) => inv_append_byte(output, pstate, x),
		//This might not be /, so hard conflict.
		Some(None) => mismatch(pstate, output, None, true),
	}
	Ok(())
}

fn parse_duplication(regex: &mut &[u8]) -> Result<(u64, Option<u64>), String>
{
	let mut i = 0;
	while i < regex.len() {
		if regex[i] == b'}' { break; }
		i += 1;
	}
	let (head, tail) = regex.split_at(i);
	fail_if!(tail.len() == 0, "Unpaired {");
	*regex = tail;
	//n or n, or n,m.
	let mut i = 0;
	while i < head.len() {
		if head[i] == b',' { break; }
		i += 1;
	}
	let (n,m) = if i < head.len() {
		(&head[..i], &head[i+1..])
	} else {
		let n = from_utf8(head).ok().and_then(|n|u64::from_str(n).ok()).ok_or("Invalid repeat count")?;
		return Ok((n, Some(n)));
	};
	let n = from_utf8(n).ok().and_then(|n|u64::from_str(n).ok()).ok_or("Invalid repeat count")?;
	let m = if m.len() > 0 {
		let m = from_utf8(m).ok().and_then(|n|u64::from_str(n).ok()).ok_or("Invalid repeat count")?;
		fail_if!(m <= n, "Max repeat count not greater than min repeat count");
		Some(m)
	} else {
		None
	};
	Ok((n, m))
}

//This downright assumes the regex is valid, returning wild values if it isn't. Return value always points to |
//or end of string.
fn find_next_top_alternate(regex: &[u8], mut pos: usize) -> usize
{
	let mut paren_depth = 0usize;
	let mut bracket_depth = 0usize;
	let mut escape = false;
	while pos < regex.len() {
		//If | exists at depth 0, it is top alternation.
		if !escape && paren_depth == 0 && regex[pos] == b'|' { return pos; }
		//If ( exists at bracket depth 0, it increases paren depth.
		if !escape && bracket_depth == 0 && regex[pos] == b'(' {
			paren_depth = paren_depth.saturating_add(1);
		}
		//If ) exists at bracket depth 0, it decreases paren depth.
		if !escape && bracket_depth == 0 && regex[pos] == b')' {
			paren_depth = paren_depth.saturating_sub(1);
		}
		//If [ appears at bracket depth 0, it sets bracket depth to 1.
		if !escape && bracket_depth == 0 && regex[pos] == b'[' { bracket_depth = 1; }
		//] decreases bracket depth.
		if bracket_depth > 0 && regex[pos] == b']' {
			bracket_depth = bracket_depth.saturating_sub(1);
		}
		//This is special: [. [: or [= at bracket depth 1 sets depth to 2.
		if bracket_depth == 1 && regex[pos] == b'[' {
			let c = *regex.get(pos + 1).unwrap_or(&0);
			if c == b'.' || c == b':' || c == b'=' { bracket_depth = 2; }
		}
		//If \ appears at bracket depth 0, it sets escape.
		escape = bracket_depth == 0 && regex[pos] == b'\\';
		pos += 1;
	}
	pos
}

pub fn anchor_expression(regex: &[u8]) -> Vec<u8>
{
	let mut s = 0;
	let mut out = Vec::new();
	while s <= regex.len() {
		let e = find_next_top_alternate(regex, s);
		let subexpr = &regex[s..e];
		if !subexpr.starts_with(b"^") { out.push(b'^'); }
		out.extend_from_slice(subexpr);
		if !subexpr.ends_with(b"$") { out.push(b'$'); }
		if e < regex.len() { out.push(b'|'); }
		s = e + 1;
	}
	out
}

//Count number of subexpressions. This assumes the regex is valid.
pub fn count_subexpressions(regex: &[u8]) -> usize
{
	let mut subexprs = 0;
	let mut bracket_depth = 0usize;
	let mut escape = false;
	let mut pos = 0;
	while pos < regex.len() {
		if !escape && bracket_depth == 0 && regex[pos] == b'(' { subexprs+=1; }
		if bracket_depth > 0 && regex[pos] == b']' { bracket_depth -= 1; }
		if !escape && bracket_depth == 0 && regex[pos] == b'[' { bracket_depth = 1; }
		if bracket_depth == 1 && regex[pos] == b'[' {
			let c = *regex.get(pos + 1).unwrap_or(&0);
			if c == b'.' || c == b':' || c == b'=' { bracket_depth = 2; }
		}
		escape = bracket_depth == 0 && regex[pos] == b'\\';
		pos += 1;
	}
	subexprs
}

#[cfg(test)]
fn test_regex(regex: &[u8], invariant: &[u8])
{
	let mut buf = [0;4096];
	let ret = get_regex_invariant_prefix(regex, &mut buf).expect("Bad regex");
	assert_eq!(invariant, ret.invariant);
}

#[test]
fn simple_regex()
{
	test_regex(b"Hello, World!", b"Hello, World!");
}

#[test]
fn cut_multiple()
{
	test_regex(b"Hello, +World!", b"Hello, ");
	test_regex(b"Hello, *World!", b"Hello,");
	test_regex(b"Hello, ?World!", b"Hello,");
}

#[test]
fn cut_any()
{
	test_regex(b"Hello,.World!", b"Hello,");
}

#[test]
fn simple_fixed_rep()
{
	test_regex(b"Hello, {3}World!", b"Hello,   World!");
}

#[test]
fn simple_rep_cut()
{
	test_regex(b"Hello, {3,}World!", b"Hello,   ");
	test_regex(b"Hello, {3,4}World!", b"Hello,   ");
}

#[test]
fn repeat_trivial()
{
	test_regex(b"Hello, ()+World!", b"Hello, World!");
	test_regex(b"Hello, ()*World!", b"Hello, World!");
	test_regex(b"Hello, ()?World!", b"Hello, World!");
}

#[test]
fn multiple_alternatives()
{
	test_regex(b"foobaz|foobar", b"fooba");
}

#[test]
fn bracket_set_one()
{
	test_regex(b"Hello[,] World!", b"Hello, World!");
}

#[test]
fn bracket_set_multiple()
{
	test_regex(b"Hello, [WX]orld!", b"Hello, ");
	test_regex(b"Hello, [W-X]orld!", b"Hello, ");
	test_regex(b"Hello, [[:upper:]]orld!", b"Hello, ");
}

#[test]
fn alternate_more()
{
	test_regex(b"foo(|/)", b"foo");
	test_regex(b"foo(/|)", b"foo");
	test_regex(b"foo(bar|barzot)", b"foobar");
	test_regex(b"foo(barzot|bar)", b"foobar");
}

#[test]
fn next_slash_test_alternate_q()
{
	let mut buf = [0;4097];
	let x = get_regex_invariant_prefix(b"hello(foo/bar|foo/?)", &mut buf).expect("Bad regex");
	assert!(x.next_slash);
}

#[test]
fn next_slash_test_alternate_q_opposite()
{
	let mut buf = [0;4097];
	let x = get_regex_invariant_prefix(b"hello(foo/?|foo/bar)", &mut buf).expect("Bad regex");
	assert!(x.next_slash);
}

#[test]
fn anchor_simple()
{
	assert_eq!(&anchor_expression(b"^(foo|bar)$"), b"^(foo|bar)$");
	assert_eq!(&anchor_expression(b"^(foo|bar)"), b"^(foo|bar)$");
	assert_eq!(&anchor_expression(b"(foo|bar)$"), b"^(foo|bar)$");
	assert_eq!(&anchor_expression(b"(foo|bar)"), b"^(foo|bar)$");
}

#[test]
fn anchor_multi()
{
	assert_eq!(&anchor_expression(b"^foo|bar$"), b"^foo$|^bar$");
	assert_eq!(&anchor_expression(b"^foo|bar"), b"^foo$|^bar$");
	assert_eq!(&anchor_expression(b"foo|bar$"), b"^foo$|^bar$");
	assert_eq!(&anchor_expression(b"foo|bar"), b"^foo$|^bar$");
}

#[test]
fn test_inv_special()
{
	let mut buf = [0;4097];
	assert_eq!(inv_parse_specialprefix(b"Hello", &mut buf), Some((5, &b""[..])));
	assert_eq!(inv_parse_specialprefix(b"Hello?", &mut buf), Some((5, &b"?"[..])));
	assert_eq!(inv_parse_specialprefix(b"Hello+", &mut buf), Some((5, &b"+"[..])));
	assert_eq!(inv_parse_specialprefix(b"Hello*", &mut buf), Some((5, &b"*"[..])));
	assert_eq!(inv_parse_specialprefix(b"Hello.*", &mut buf), Some((5, &b".*"[..])));
	assert_eq!(inv_parse_specialprefix(b"Hello(.*)", &mut buf), Some((5, &b"(.*)"[..])));
	assert_eq!(inv_parse_specialprefix(b"Hello[xyz]", &mut buf), Some((5, &b"[xyz]"[..])));
	assert_eq!(inv_parse_specialprefix(b"Hello{5,6}", &mut buf), Some((5, &b"{5,6}"[..])));
	assert_eq!(inv_parse_specialprefix(b"Hello\\??", &mut buf), Some((6, &b"?"[..])));
	assert_eq!(inv_parse_specialprefix(b"Hello\\++", &mut buf), Some((6, &b"+"[..])));
	assert_eq!(inv_parse_specialprefix(b"Hello\\?*", &mut buf), Some((6, &b"*"[..])));
}

#[test]
fn test_inv_special_tooshort()
{
	let mut buf = [0;4];
	assert_eq!(inv_parse_specialprefix(b"Hello", &mut buf), None);
}
