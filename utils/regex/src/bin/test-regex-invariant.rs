use btls_aux_memory::SafeShowByteString;
use btls_util_regex::anchor_expression;
use btls_util_regex::count_subexpressions;
use btls_util_regex::get_regex_invariant_prefix;
use btls_util_regex::REG_EXTENDED;
use btls_util_regex::REG_NOSUB;
use btls_util_regex::RegexHandle;

fn main()
{
	let args: Vec<String> = std::env::args().collect();
	let regex = &args[1];
	let mut buf = [0;4096];
	let mut err = [0;256];
	let ret = get_regex_invariant_prefix(regex.as_bytes(), &mut buf).expect("Bad regex");
	let _regex = RegexHandle::new(regex.as_bytes(), REG_EXTENDED|REG_NOSUB, &mut err).expect("Regex error");
	let anchored = anchor_expression(regex.as_bytes());
	println!("Invariant prefix:{invariant}", invariant=SafeShowByteString(ret.invariant));
	println!("Anchored:{anchored}", anchored=SafeShowByteString(&anchored));
	println!("Subexpression count:{exprc}", exprc=count_subexpressions(regex.as_bytes()));
	println!("Next slash flag:{ns}", ns=ret.next_slash);
}
