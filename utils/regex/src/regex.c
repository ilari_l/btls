#include <regex.h>
#include <stdlib.h>

#define H_REG_EXTENDED 1
#define H_REG_ICASE 2
#define H_REG_NOSUB 4
#define H_REG_NEWLINE 8
#define H_REG_NOTBOL 1
#define H_REG_NOTEOL 2
#define H_REG_STARTEND 4

struct rhandle
{
	regex_t regex;
};

struct match
{
	size_t start;
	size_t end;
};

struct rhandle* httpdemux2_regex_comp(const char* regex, int flags, char* error, size_t errorlen)
{
	struct rhandle* apreg = malloc(sizeof(struct rhandle));
	if(!apreg) {
		if(error) *error = REG_ESPACE;
		return NULL;
	}
	//Do not directly pass flags.
	int flags2 = 0;
	if(flags & H_REG_EXTENDED) flags2 |= REG_EXTENDED;
	if(flags & H_REG_ICASE) flags2 |= REG_ICASE;
	if(flags & H_REG_NOSUB) flags2 |= REG_NOSUB;
	if(flags & H_REG_NEWLINE) flags2 |= REG_NEWLINE;
	int r = regcomp(&apreg->regex, regex, flags2);
	if(r) {
		//Error, free the memory and set error.
		regerror(r, &apreg->regex, error, errorlen);
		free(apreg);
		apreg = NULL;
	}
	return apreg;
}

#define MAX_STACK_NMATCH 32

inline static size_t backconvert_offset(regoff_t off)
{
	if(off == -1) return ~(size_t)0;
	if(sizeof(regoff_t) == sizeof(unsigned)) return (unsigned)off;
	return off;
}

int httpdemux2_regex_exec(const struct rhandle* preg, const char* string, size_t nmatch, struct match pmatch[],
	int flags, char* error, size_t errorlen)
{
	//pmatch can not be directly passed.
	regmatch_t* pmatch2 = alloca(MAX_STACK_NMATCH * sizeof(regmatch_t));
	if(nmatch > MAX_STACK_NMATCH) { pmatch2 = malloc(nmatch * sizeof(regmatch_t)); }
	//Do not directly pass flags.
	int flags2 = 0;
	if(flags & H_REG_NOTBOL) flags2 |= REG_NOTBOL;
	if(flags & H_REG_NOTEOL) flags2 |= REG_NOTEOL;
	if(flags & H_REG_STARTEND) {
		//H_REG_STARTEND handling: pmatch2[0] is always valid.
		pmatch2[0].rm_so = pmatch[0].start;
		pmatch2[0].rm_eo = pmatch[0].end;
		flags2 |= REG_STARTEND;
	}
	int r = regexec(&preg->regex, string, nmatch, pmatch2, flags2);
	if(r == REG_NOMATCH) {
		//Did not match.
		r = 0;
	} else if(r) {
		//Error.
		regerror(r, &preg->regex, error, errorlen);
		r = -1;
	} else {
		//Copy nmatch entries from pmatch2 to pmatch.
		for(size_t i = 0; i < nmatch; i++) {
			pmatch[i].start = backconvert_offset(pmatch2[i].rm_so);
			pmatch[i].end = backconvert_offset(pmatch2[i].rm_eo);
		}
		r = 1;
	}
	//If temporary space was allocated for matches, free it.
	if(nmatch > MAX_STACK_NMATCH) { free(pmatch2); }
	return r;
}

void httpdemux2_regex_free(struct rhandle* preg)
{
	regfree(&preg->regex);
	free(preg);
}
