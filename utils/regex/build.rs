fn main()
{
	let mut config = cc::Build::new();
	config.file("src/regex.c");
	config.compile("libhttpdemux2regex.a");

	println!("cargo:rerun-if-changed=src/regex.c");
}
