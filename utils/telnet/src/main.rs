#![warn(unsafe_op_in_unsafe_fn)]
extern crate btls;
extern crate btls_aux_serialization;
extern crate btls_aux_signatures;
extern crate btls_aux_unix;
use btls::ClientConnection;
use btls::Connection;
use btls::ClientConfiguration;
use btls::callbacks::TlsCallbacks;
use btls::callbacks::SelectClientCertificateParameters;
use btls::callbacks::PointInTime;
use btls::certificates::LocalKeyPair;
use btls::certificates::ClientCertificate;
use btls::certificates::Hasher;
use btls::certificates::KeyPair2;
use btls::certificates::SignatureAlgorithmTls2;
use btls::certificates::TemporaryRandomStream;
use btls::certificates::FutureReceiver;
use btls::certificates::TrustAnchor;
use btls::certificates::HostSpecificPin;
use btls::features::FLAGS0_POST_QUANTUM_KEM;
use btls::transport::Queue;
use btls::transport::PullTcpBuffer;
use btls::transport::PushTcpBuffer;
use btls::transport::StatusFlags;
use btls_aux_serialization::Source;
use btls_aux_signatures::iterate_pem_or_derseq_fragments;
use btls_aux_signatures::PemDerseqFragment2 as PFragment;
use btls_aux_signatures::PemFragmentKind as PFKind;
use btls_aux_unix::FileDescriptor;
use btls_aux_unix::FileDescriptorB;
use btls_aux_unix::OpenFlags;
use btls_aux_unix::Path as UnixPath;
use btls_aux_unix::Pid;
use btls_aux_unix::PollFd;
use btls_aux_unix::PollFlags;
use btls_aux_unix::ShutdownFlags;
use btls_aux_unix::SpecialFd;
use std::env::args;
use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::io::stdin;
use std::io::stdout;
use std::io::stderr;
use std::io::ErrorKind as IoErrorKind;
use std::mem::zeroed;
use std::net::TcpStream;
use std::net::TcpListener;
use std::net::SocketAddr;
use std::ops::Deref;
use std::path::Path;
use std::path::PathBuf;
use std::process::exit;
use std::str::FromStr;
use std::sync::Arc;
use std::os::unix::io::AsRawFd;

macro_rules! fatal
{
	($($args:tt)*) => {{
		writeln!(stderr(), $($args)*).unwrap();
		exit(1)
	}}
}

//A certificate that has been selected for signing.
#[derive(Clone)]
struct SelectedCertificate
{
	//The keypair that does the signing.
	keypair: Arc<Box<dyn KeyPair2+Send+Sync>>,
	//The TLS algorithm number to use.
	tls_algorithm: SignatureAlgorithmTls2,
	//The certificate.
	certificate: Arc<Vec<u8>>,
	//The certificate chain to send.
	chain: Arc<Vec<Vec<u8>>>,
}

impl ClientCertificate for SelectedCertificate
{
	fn sign(&self, data: &mut dyn FnMut(&mut Hasher), rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<(u16, Vec<u8>), ()>>
	{
		btls::certificates::tls_sign_helper(self.keypair.deref().deref(), data, self.tls_algorithm, rng)
	}
	fn certificate<'a>(&'a self) -> &'a [u8] { self.certificate.deref().deref() }
	fn chain<'a>(&'a self) -> &'a [Vec<u8>] { self.chain.deref().deref() }
	//We don't implement get_ocsp nor get_scts. These get replaced by defaults.
}

struct Controller
{
	deadline: Option<PointInTime>,
	certificate: Option<(PathBuf, PathBuf)>,
}

impl Controller
{
	fn new(certificate: Option<(PathBuf, PathBuf)>) -> Controller
	{
		Controller {
			deadline: None,
			certificate: certificate,
		}
	}
}

impl TlsCallbacks for Controller
{
	fn set_deadline(&mut self, dl: Option<PointInTime>) { self.deadline = dl; }
	fn select_client_certificate(&mut self, criteria: SelectClientCertificateParameters) ->
		Option<Box<dyn ClientCertificate+Send>>
	{
		if let Some(&(ref keyfile, ref certfile)) = self.certificate.as_ref() {
			let kp = LocalKeyPair::new_file(keyfile).unwrap_or_else(|err|{
				fatal!("Can't load private key: {err}")
			});
			let available = kp.get_signature_algorithms().iter().
				filter_map(|x|x.downcast_tls().map(|y|y.tls_id()));
			let algo = criteria.choose_sigalg(available).
				unwrap_or_else(||fatal!("Can't find acceptable signature algorithm"));
			let mut certificates = Vec::new();
			let mut fp = File::open(certfile).unwrap_or_else(|err|{
				fatal!("Can't open certificate file: {err}")
			});
			let mut certfile = Vec::new();
			fp.read_to_end(&mut certfile).unwrap_or_else(|err|{
				fatal!("Can't read certificate file: {err}")
			});
			let mut certfile = Source::new(&certfile);
			while certfile.more() { match certfile.asn1_sequence_out() {
				Ok(x) => certificates.push(x.to_owned()),
				Err(err) => fatal!("Can't read a certificate: {err}")
			}}
			if certificates.len() == 0 { fatal!("No certificates in certificate file"); }
			let cert = certificates.remove(0);
			Some(Box::new(SelectedCertificate{
				keypair: Arc::new(Box::new(kp)),
				tls_algorithm: algo,
				certificate: Arc::new(cert),
				chain: Arc::new(certificates),
			}))
		} else {
			None
		}
	}
}

fn make_telnet() -> TcpStream
{
	let listener = TcpListener::bind("[::1]:0").unwrap_or_else(|err|fatal!("Failed to listen port 0: {err}"));
	let laddr = listener.local_addr().unwrap_or_else(|err|fatal!("Failed to get local address: {err}"));
	let port = if let SocketAddr::V6(x) = laddr { x.port() } else { fatal!("Not IPv6?"); };
	let parg = port.to_string();
	let args = [&b"telnet"[..], &b"::1"[..], parg.as_bytes()];
	match Pid::fork() {
		Err(err) => fatal!("Failed to fork: {err}"),
		Ok(None) => {
			let err = Pid::execvp(args[0], &args);
			fatal!("Failed to exec telnet: {err}");
		},
		Ok(Some(_)) => (),	//Parent.
	}
	match listener.accept() {
		Ok((x, _)) => x,
		Err(err) => fatal!("Failed to accept: {err}")
	}
}

struct TlsTransport
{
	tcp: TcpStream,
	decrypted: Queue,
	encrypted: Queue,
	tls: ClientConnection,
	telnet: Option<TcpStream>,
	devnull: FileDescriptor,
	black_in_eof: bool,
	read_temp: Vec<u8>,
	seen_rx_eof: bool,
	seen_finished: bool,
	out_close_ack: bool,
}

impl TlsTransport
{
	fn new(target: Option<&str>, port: Option<u16>, name: &str, alpn: Option<&str>,
		certificate: Option<(PathBuf, PathBuf)>, trust_anchors: &[TrustAnchor], pins: &[HostSpecificPin],
		ssh: bool, pqc: bool) -> TlsTransport
	{
		//This is used to close stdout.
		let flags = OpenFlags::O_WRONLY|OpenFlags::O_CLOEXEC;
		let devnull = UnixPath::dev_null().open(flags).unwrap_or_else(|err|{
			fatal!("Unable to open /dev/null: {err}")
		});
		let port = port.unwrap_or(443);
		let target = target.unwrap_or(name);
		let alpn = alpn.unwrap_or(if !ssh { "telnets" } else { "ssh" });
		let target = if target.contains(":") {
			format!("[{target}]:{port}")
		} else {
			format!("{target}:{port}")
		};
		let tcp = TcpStream::connect(target.deref()).unwrap_or_else(|err|{
			fatal!("Unable to connect to {target}: {err}")
		});
		let mut config = ClientConfiguration::new();
		if pqc { config.set_flags(0, FLAGS0_POST_QUANTUM_KEM); }
		config.add_alpn(alpn);
		for ta in trust_anchors { config.add_trust_anchor(ta); }
		let controller = Controller::new(certificate);
		let tls = config.make_connection_pinned(name, &pins, Box::new(controller));
		let telnet = if !ssh { Some(make_telnet()) } else { None };
		TlsTransport {
			tcp: tcp,
			decrypted: Queue::new(),
			encrypted: Queue::new(),
			read_temp: vec![0;16645],
			tls: tls,
			telnet: telnet,
			seen_finished: false,
			seen_rx_eof: false,
			out_close_ack: false,
			black_in_eof: false,
			devnull: devnull,
		}
	}
	fn event_loop(&mut self)
	{
		loop {
			let mut p: [PollFd;3] = [unsafe{zeroed()};3];
			let tcpfd = FileDescriptorB::new(self.tcp.as_raw_fd());
			let stdin = FileDescriptorB::new(self.telnet.as_ref().map(|t|t.as_raw_fd()).unwrap_or(0));
			let stdout = FileDescriptorB::new(self.telnet.as_ref().map(|t|t.as_raw_fd()).unwrap_or(1));
			p[0].fd = tcpfd;
			p[1].fd = stdin;
			p[2].fd = stdout;
			let tls_status = self.tls.get_status();
			if tls_status.is_dead() {
				//Connection died, print message.
				if let Some(abort) = self.tls.get_error() {
					writeln!(stderr(), "TLS ERROR: {abort}").unwrap();
				} else {
					writeln!(stderr(), "<Connection closed>").unwrap();
				}
				return;
			}
			if tls_status.is_blocked() {
				//Conserve CPU.
				std::thread::sleep(std::time::Duration::from_millis(50));
				continue;
			}
			if self.wants_tcp_read(tls_status) { p[0].events = p[0].events | PollFlags::IN; }
			if self.wants_tcp_write(tls_status) { p[0].events = p[0].events | PollFlags::OUT; }
			if self.wants_telnet_read(tls_status) { p[1].events = p[1].events | PollFlags::IN; }
			if self.wants_telnet_write() { p[2].events = p[2].events | PollFlags::OUT; }
			let mut wsz = 0;
			for rsz in 0..3 { if !p[rsz].events.is_none() { p[wsz as usize] = p[rsz]; wsz += 1; } }
			if wsz == 0 && self.seen_finished { break; }	//Quit if we have nothing anymore.
			let timeout = if self.seen_finished { -1 } else { 50 };
			unsafe{PollFd::poll(&mut p[..wsz], timeout).ok()};
			for q in p.iter() {
				if q.fd == tcpfd && q.revents.is_in_or_hup() { self.do_tcp_read(); }
				if q.fd == tcpfd && q.revents.is_out() { self.do_tcp_write(); }
				if q.fd == stdin && q.revents.is_in_or_hup() { self.do_telnet_read(); }
				if q.fd == stdout && q.revents.is_out() { self.do_telnet_write(); }
			}
		}
	}
	fn wants_tcp_read(&self, flags: StatusFlags) -> bool { flags.is_want_rx() && !self.black_in_eof }
	fn wants_tcp_write(&self, flags: StatusFlags) -> bool { flags.is_want_tx() || self.encrypted.buffered() > 0 }
	//Only read from Telnet if it can be transmitted.
	fn wants_telnet_read(&self, flags: StatusFlags) -> bool { flags.is_may_tx() }
	fn wants_telnet_write(&self) -> bool
	{
		!self.out_close_ack && (self.decrypted.buffered() > 0 || self.decrypted.no_more_output())
	}
	fn do_tcp_read(&mut self)
	{
		let tls_error = |err:btls::Error|fatal!("TLS ERROR: {err}");
		let tls_flags = self.tls.get_status();
		if !tls_flags.is_want_rx() { return; }
		let amt = match self.tcp.read(&mut self.read_temp) {
			Ok(0) => { 
				self.tls.push_tcp_data(PushTcpBuffer::end()).unwrap_or_else(tls_error);
				self.black_in_eof = true;
				return;
			},
			Ok(r) => r,
			Err(err) => match err.kind() {
				IoErrorKind::WouldBlock|IoErrorKind::Interrupted => return,
				_ => fatal!("TCP read error: {err}")
			}
		};
		let (tmp, eofd) = self.tls.push_tcp_data(PushTcpBuffer::u8_size(&self.read_temp, amt)).
			unwrap_or_else(tls_error);
		self.decrypted.write(&tmp).ok();
		if eofd { self.decrypted.send_eof(); }
		let tls_flags = self.tls.get_status();
		self.seen_finished |= !tls_flags.is_handshaking();
	}
	fn do_tcp_write(&mut self)
	{
		let tls_flags = self.tls.get_status();
		//Only consider is_want_tx() because this never sends any application data. So only TLS internal
		//sends are interesting.
		if tls_flags.is_want_tx() {
			//Flush to queue.
			let tmp = self.tls.pull_tcp_data(PullTcpBuffer::no_data()).
				unwrap_or_else(|err|fatal!("TLS ERROR: {err}"));
			self.encrypted.write(&tmp).ok();	//This should never fail.
			let tls_flags = self.tls.get_status();
			self.seen_finished |= !tls_flags.is_handshaking();
		}
		match self.encrypted.call_write(&mut self.tcp) {
			Ok(_) => (),
			Err(err) => match err.kind() {
				IoErrorKind::WouldBlock|IoErrorKind::Interrupted => return,
				_ => fatal!("TCP write error: {err}")
			}
		};
	}
	fn do_telnet_read(&mut self)
	{
		let mut data = [0;8192];
		let readres = match self.telnet.as_mut() {
			Some(telnet) => telnet.read(&mut data),
			None => stdin().read(&mut data),
		};
		let data = match readres {
			Ok(amt) => data.get(..amt).unwrap_or(&data),
			Err(err) => match err.kind() {
				IoErrorKind::WouldBlock|IoErrorKind::Interrupted => return,
				_ => fatal!("Telnet read error: {err}")
			}
		};
		let tmp = if data.len() > 0 {
			PullTcpBuffer::write(data)
		} else {
			self.seen_rx_eof = true;
			PullTcpBuffer::no_data().eof()	//Send EOF.
		};
		let tmp = self.tls.pull_tcp_data(tmp).unwrap_or_else(|err|fatal!("TLS ERROR: {err}"));
		self.encrypted.write(&tmp).ok();	//This should never fail.
	}
	fn do_telnet_write(&mut self)
	{
		let writeres = match self.telnet.as_mut() {
			Some(telnet) => self.decrypted.call_write(telnet),
			None => self.decrypted.call_write(&mut stdout()),
		};
		match writeres {
			Ok(_) => (),
			Err(err) => match err.kind() {
				IoErrorKind::WouldBlock|IoErrorKind::Interrupted => return,
				_ => fatal!("Telnet write error: {err}")
			}
		};
		if self.telnet.is_none() { stdout().flush().ok(); }
		if self.decrypted.no_more_output() { unsafe {
			self.out_close_ack = true;
			match self.telnet.as_mut() {
				Some(telnet) => FileDescriptorB::new(telnet.as_raw_fd()).
					shutdown(ShutdownFlags::WR).ok(),
				//The way to close STDOUT is to dup /dev/null over it.
				None => self.devnull.dup2(SpecialFd::STDOUT).ok()
			};
		}}
	}
}

fn main()
{
	let mut host = None;
	let mut port = None;
	let mut name = None;
	let mut alpn = None;
	let mut certificate = None;
	let mut pkey = None;
	let mut cert = None;
	let mut ssh = false;
	let mut pqc = false;
	let mut trust_anchors = Vec::new();
	let mut pins = Vec::new();
	for arg in args().skip(1) {
		if let Some(host2) = arg.strip_prefix("--host=") {
			host = Some(host2.to_owned());
		} else if let Some(port2) = arg.strip_prefix("--port=") {
			port = Some(u16::from_str(port2).unwrap_or_else(|_|fatal!("Invalid port")));
		} else if let Some(alpn2) = arg.strip_prefix("--alpn=") {
			alpn = Some(alpn2.to_owned());
		} else if let Some(pkey2) = arg.strip_prefix("--pkey=") {
			pkey = Some(pkey2.to_owned());
		} else if let Some(cert2) = arg.strip_prefix("--cert=") {
			cert = Some(cert2.to_owned());
		} else if let Some(fname) = arg.strip_prefix("--trust=") {
			use PFragment::*;
			use PFKind::*;
			let mut contents = Vec::new();
			File::open(fname).and_then(|mut fp|{
				fp.read_to_end(&mut contents)
			}).unwrap_or_else(|err|{
				fatal!("Failed to read {fname}: {err}")
			});
			//Try to decode PEM.
			iterate_pem_or_derseq_fragments(&contents, |frag|match frag {
				Error => Err(format!("Not in PEM nor DERseq format")),
				Der(mut cert)|Pem(Certificate|TrustedCertificate, mut cert) => {
					let crt = TrustAnchor::from_certificate(&mut cert).map_err(|err|{
						format!("Failed to make trust anchor: {err}")
					})?;
					Ok(trust_anchors.push(crt))
				},
				_ => Ok(())	//Comments, unknown PEM fragments.
			}).unwrap_or_else(|err|fatal!("{fname}: {err}"));
		} else if let Some(pdata) = arg.strip_prefix("--pin=") {
			let mut c = [0;32];
			let mut bad = false;
			for (i, d) in pdata.as_bytes().iter().cloned().enumerate() {
				c.get_mut(i/2).map(|x|*x |= match d {
					48..=57 => d - 48,
					65..=70 => d - 55,
					97..=102 => d - 87,
					_ => {bad = true; 0 }
				} << 4 - i % 2 * 4);
			}
			if pdata.len() != 64 || bad { fatal!("Bad hash {pdata}"); }
			pins.push(HostSpecificPin::trust_server_key_by_sha256(&c, false, false));
		} else if arg == "--ssh" {
			ssh = true;
		} else if arg == "--post-quantum" {
			pqc = true;
		} else if arg.starts_with("-") {
			fatal!("Bad option {arg}");
		} else {
			name = Some(arg);
		}
	}
	if let (Some(cert), Some(pkey)) = (cert, pkey) {
		certificate = Some((Path::new(&pkey).to_path_buf(), Path::new(&cert).to_path_buf()));
	}
	let name = if let Some(name) = name { name } else { fatal!("Name to connect to is required"); };
	let mut trans = TlsTransport::new(host.as_deref(), port, &name, alpn.as_deref(), certificate,
		&trust_anchors, &pins, ssh, pqc);
	trans.event_loop();
}
