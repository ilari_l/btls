#!/usr/bin/lua52

tmap = {};

for line in io.stdin:lines() do
	if line == "" or string.sub(line, 1, 1) == "#" then goto skip2; end
	first = true;
	for item in string.gmatch(line, "%S+") do
		if first then
			mimetype = item;
		else
			tmap[item] = mimetype;
		end
		first = false;
	end

	::skip2::
	;
end

ksort = {};
for a, b in pairs(tmap) do
	table.insert(ksort, a);
end
table.sort(ksort);

for i=1,#ksort do
	print(string.format("\t(\"%s\",\"%s\"),", ksort[i], tmap[ksort[i]]));
end
