use btls_daemon_helper_lib::ConnectionInfoAddress;
use std::borrow::Borrow;
use std::cell::RefCell;
use std::cmp::min;
use std::cmp::Ordering;
use std::collections::BTreeSet;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::hash::Hash;
use std::hash::Hasher;
use std::net::Ipv4Addr;
use std::net::Ipv6Addr;
use std::ops::Deref;
use std::ops::DerefMut;
use std::rc::Rc;


pub fn cia_to_ip(x: &ConnectionInfoAddress) -> String
{
	match x {
		&ConnectionInfoAddress::V6(x, _) => Ipv6Addr::from(x).to_string(),
		&ConnectionInfoAddress::V4(x, _) => Ipv4Addr::from(x).to_string(),
		&ConnectionInfoAddress::Unix(ref addr) => addr.to_string(),
		&ConnectionInfoAddress::Abstract(ref addr) => format!("@{addr}"),
		_ => "<unknown>".to_owned(),
	}
}

pub fn strip_port<'a>(x: &'a [u8]) -> &'a [u8]
{
	let mut se = x.len();
	while se > 0 && x[se-1].wrapping_sub(48) < 10 { se -= 1; }
	if se > 0 && x[se-1] == b':' { &x[..se-1] } else { x }
}

pub struct Buffer
{
	content: Vec<u8>,
	pointer: usize,
	bound: usize,
	speculative: usize,
}

impl Buffer
{
	pub fn new() -> Buffer
	{
		Self::new_bounded(::std::usize::MAX)
	}
	pub fn new_bounded(bound: usize) -> Buffer
	{
		Buffer {
			content: Vec::new(),
			pointer: 0,
			bound: bound,
			speculative: 0,
		}
	}
	pub fn append(&mut self, data: &[u8]) -> Result<(),()>
	{
		fail_if!(dtry!(self.content.len().checked_add(data.len())) > self.bound, ());
		self.content.extend_from_slice(data);
		Ok(())
	}
	pub fn as_slice<'a>(&'a self) -> &'a [u8]
	{
		&self.content[min(self.content.len(), self.pointer)..]
	}
	pub fn discard(&mut self, amt: usize)
	{
		self.pointer = min(self.pointer.saturating_add(amt), self.content.len());
		if self.is_empty() { self.clear(); }
	}
	pub fn clear(&mut self)
	{
		self.content.clear();
		self.pointer = 0;
		self.speculative = 0;
	}
	pub fn clear_drop(&mut self)
	{
		self.clear();
		self.content = Vec::new();	//Free memory.
	}
	pub fn has_data(&self) -> bool { self.pointer < self.content.len().saturating_sub(self.speculative) }
	pub fn is_empty(&self) -> bool { !self.has_data() }
	pub fn freespace(&self, bias: usize) -> usize
	{
		self.bound.saturating_sub(self.content.len()).saturating_sub(bias)
	}
}

macro_rules! str_compare_traits
{
	($clazz:ident) => {
		impl PartialEq<str> for $clazz
		{
			fn eq(&self, x: &str) -> bool { self.deref() == x }
		}
		impl PartialEq for $clazz
		{
			fn eq(&self, x: &Self) -> bool { self.deref() == x.deref() }
		}
		impl Eq for $clazz {}
		impl PartialOrd<Self> for $clazz
		{
			fn partial_cmp(&self, x: &Self) -> Option<Ordering> { Some(self.cmp(x)) }
		}
		impl PartialOrd<str> for $clazz
		{
			fn partial_cmp(&self, x: &str) -> Option<Ordering> { Some(self.deref().cmp(x)) }
		}
		impl Ord for $clazz
		{
			fn cmp(&self, x: &Self) -> Ordering { self.deref().cmp(x.deref()) }
		}
	}
}

#[derive(Clone,Debug)]
pub struct RcTextString(Rc<String>);

impl Hash for RcTextString
{
	fn hash<H:Hasher>(&self, h: &mut H) { self.deref().hash(h) }
}

impl Deref for RcTextString
{
	type Target = str;
	fn deref(&self) -> &str { self.0.deref().deref() }
}

impl Borrow<str> for RcTextString
{
	fn borrow(&self) -> &str { self.deref() }
}

str_compare_traits!(RcTextString);

impl Drop for RcTextString
{
	fn drop(&mut self)
	{
		//If the reference count of rc is 2, then this is the last remaining string (the other reference
		//is the interner key). Clean up the interner in that case.
		if Rc::strong_count(&self.0) <= 2 {
			RC_INTERNER.with(|interner|{
				let mut interner = interner.borrow_mut();
				let x: &str = self.deref();
				interner.deref_mut().remove(x);
			});
		}
	}
}

impl Display for RcTextString
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> { Display::fmt(self.deref(), f) }
}

impl RcTextString
{
	pub(crate) fn new(x: &str) -> RcTextString
	{
		RC_INTERNER.with(|interner|{
			let mut interner = interner.borrow_mut();
			if let Some(ret) = interner.deref().get(x) { return RcTextString(ret.0.clone()) }
			let k = Rc::new(x.to_owned());
			interner.deref_mut().insert(RcInternerKey(k.clone()));
			RcTextString(k)
		})
	}
}

struct RcInternerKey(Rc<String>);

impl Borrow<str> for RcInternerKey
{
	fn borrow(&self) -> &str { self.deref() }
}

impl Deref for RcInternerKey
{
	type Target = str;
	fn deref(&self) -> &str { self.0.deref().deref() }
}

str_compare_traits!(RcInternerKey);

thread_local!(static RC_INTERNER: RefCell<BTreeSet<RcInternerKey>> = RefCell::new(BTreeSet::new()));
