#![deny(unused_must_use)]
#![warn(unsafe_op_in_unsafe_fn)]

#[macro_use] extern crate btls_aux_fail;
pub use crate::connection::KryptedTarget;
pub use crate::connection_krypt_tls::ClientCertificate;
pub use crate::connection_krypt_tls::TlsClient;
pub use crate::connection_krypt_tls::TlsServer;
pub use crate::connection_lowlevel::Connection;
use crate::http::ConnectionPropertiesI;
pub use crate::http::HeaderRewrite;
pub use crate::http::set_product_name_and_version;
pub use crate::http::set_secondary_product_name_and_version;
pub use crate::http::test_set_plain_response;
pub use crate::metrics_dp::GlobalMetrics;
pub use crate::routing_dp::BackendData;
pub use crate::routing_dp::check_placeholder;
pub use crate::routing_dp::DisableMode;
pub use crate::routing_dp::HostRoutingData;
pub use crate::routing_dp::HostRoutingDataFactory;
pub use crate::routing_dp::InsecureMode;
pub use crate::routing_dp::is_valid_uri;
pub use crate::routing_dp::PathData;
pub use crate::routing_dp::RedirectMode;
pub use crate::routing_dp::RegularExpression;
pub use crate::routing_dp::RegularExpressionInsecure as RInsecure;
pub use crate::routing_dp::REntry;
pub use crate::routing_dp::RoutingContext;
pub use crate::routing_dp::UrlParseError;
pub use crate::staticfile::check_mimetype;
use btls::utility::RwLock;
use btls_aux_memory::BackedVector;
use btls_aux_memory::slice_tail_mut;
use btls_aux_serialization::Sink;
use btls_aux_serialization::SinkMarker;
use btls_aux_time::TimeUnit;
use btls_aux_unix::SocketFlags;
use btls_daemon_helper_lib::AddTokenCallback;
use btls_daemon_helper_lib::AllocatedToken;
use btls_daemon_helper_lib::cloned;
use btls_daemon_helper_lib::ConnectionInfo;
use btls_daemon_helper_lib::ConnectionInfoAddress;
use btls_daemon_helper_lib::cow;
use btls_daemon_helper_lib::FileDescriptor;
use btls_daemon_helper_lib::Poll;
use btls_daemon_helper_lib::ProxyConfiguration;
use btls_daemon_helper_lib::SocketAddrEx;
use btls_daemon_helper_lib::TokenAllocatorPool;
use btls_daemon_helper_lib::UpperLayerConnection;
use btls_daemon_helper_lib::UpperLayerConnectionFactory;
use std::borrow::Cow;
use std::cmp::min;
use std::collections::HashMap;
use std::fmt::Debug;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::mem::MaybeUninit;
use std::mem::transmute;
use std::ops::Deref;
use std::sync::Arc;
use std::sync::atomic::AtomicUsize;


macro_rules! increment_metric
{
	($metric:ident [$amt:expr] $globalcfg:expr) => { $globalcfg.increment_metric($amt, |m|&m.$metric) };
	($metric:ident $globalcfg:expr) => { $globalcfg.increment_metric(1, |m|&m.$metric) };
}

//Sanity check.
#[cfg_attr(not(any(target_pointer_width="32",target_pointer_width="64")), unsupported_pointer_width)]

#[macro_use]
mod bitmask;
mod connection;
mod connection_in_h1;
mod connection_in_h2;
mod connection_krypt_tls;
mod connection_logical;
mod connection_lowlevel;
mod connection_midlevel;
mod connection_out;
mod headerblock;
mod http;
mod metrics_dp;
mod routing_dp;
mod staticfile;
mod utils;

pub struct GlobalConfiguration
{
	pub dremap_table: HashMap<SocketAddrEx, SocketAddrEx>,
	pub proxy_config: ProxyConfiguration,
	pub proxy_name: Arc<Vec<u8>>,
	pub connect_timeout: TimeUnit,
	pub connect_attempt_timeout: TimeUnit,
	pub transfer_timeout: TimeUnit,
	pub proxy_timeout: TimeUnit,
	pub keepalive_timeout: TimeUnit,
	pub shutdown_timeout: TimeUnit,
	pub http_idle_timeout: TimeUnit,
	pub routetable: RoutingContext,
	pub chained_mode: bool,
	pub global_logflags: Arc<AtomicUsize>,
	pub incoming_tls: Option<Arc<TlsServer>>,
}

pub struct GlobalConfigurationWrapper(Arc<RwLock<GlobalConfiguration>>, Arc<GlobalMetrics>);

impl GlobalConfigurationWrapper
{
	pub fn new(config: GlobalConfiguration, metrics: Arc<GlobalMetrics>) -> GlobalConfigurationWrapper
	{
		GlobalConfigurationWrapper(Arc::new(RwLock::new(config)), metrics)
	}
	pub fn with<T,F>(&self, f: F) -> T where F: FnOnce(&GlobalConfiguration) -> T
	{
		f(self.0.read().deref())
	}
	pub fn metrics<T,F>(&self, f: F) -> T where F: FnOnce(&GlobalMetrics) -> T
	{
		f(self.1.deref())
	}
	pub fn metrics_arc(&self) -> Arc<GlobalMetrics>
	{
		self.1.clone()
	}
	pub(crate) fn metrics_ref<'a>(&'a self) -> &'a GlobalMetrics
	{
		self.1.deref()
	}
	pub fn increment_metric<'a,F>(&'a self, amt: usize, f: F)
		where F: FnOnce(&'a GlobalMetrics) -> &'a AtomicUsize
	{
		self.1.deref().increment_metric(amt, f)
	}
}

impl Debug for GlobalConfigurationWrapper
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		fmt.write_str("GlobalConfigurationWrapper")
	}
}

pub type ConfigT = &'static GlobalConfigurationWrapper;

pub struct ConnectionFactory2(ConfigT);


pub struct HttpBridgeProxyEx<'a>
{
	pub source: &'a ConnectionInfoAddress,
	pub is_h2: bool,
	pub is_secure: bool,
	pub client_cn: Option<&'a str>,
	pub client_spkihash: Option<&'a str>,
	pub properties: ConnectionProperties,
	pub conn_sni: Arc<String>,
	pub requested_cert: bool,
}

#[derive(Copy,Clone,Debug)]
pub struct ConnectionProperties
{
	pub secure192: bool,
	pub tls13: bool,
}

impl ConnectionProperties
{
	fn to_internal(self) -> ConnectionPropertiesI
	{
		ConnectionPropertiesI::to_internal(self)
	}
}

impl ConnectionFactory2
{
	pub fn new2(g: ConfigT) -> Arc<dyn UpperLayerConnectionFactory>
	{
		let factory: Arc<dyn UpperLayerConnectionFactory> = Arc::new(ConnectionFactory2(g));
		factory
	}
	pub fn wrap(g: ConfigT) -> ConnectionFactory2 { ConnectionFactory2(g) }
	fn fd_pair() -> Result<(FileDescriptor, FileDescriptor), Cow<'static, str>>
	{
		let flags = SocketFlags::NONBLOCK|SocketFlags::CLOEXEC;
		let (fd1, fd2) = FileDescriptor::unix_socketpair2(flags).map_err(|err|{
			cow!(F "Failed to create socket pair: {err}")
		})?;
		Ok((fd1, fd2))
	}
	pub fn connect_to(&self, poll: &mut Poll, info: ConnectionInfo, addtoken: &mut AddTokenCallback) ->
		Result<FileDescriptor, Cow<'static, str>>
	{
		let (fd1, fd2) = Self::fd_pair()?;
		//Always accept proxy, as this is internal link. And since this is proxy, address never gets set
		//here. Always ignore address_valid signal, as address is never valid (because of proxy).
		//Never use cryptos, as this is internal link.
		let (ctx, tokens, _) = Connection::accept(poll, addtoken.allocator(), fd2, (), info.clone(), true,
			self.0)?;
		addtoken.spawn_masquerade(Box::new(ctx), Cow::Owned(tokens), info);
		Ok(fd1)
	}
	pub fn connect_to_ex(&self, poll: &mut Poll, addtoken: &mut AddTokenCallback, info: HttpBridgeProxyEx) ->
		Result<FileDescriptor, Cow<'static, str>>
	{
		let (fd1, fd2) = Self::fd_pair()?;
		let (ctx, tokens) = Connection::accept_ex(poll, addtoken.allocator(), fd2, info, self.0)?;
		//Since this connection masquerades as the TLS connection, it does not matter what the heck
		//the connection info contains. It does not even matter for kline because killing parent connection
		//auto-kills all the linked connections.
		let cinfo = ConnectionInfo::internal_to_internal("", "");
		addtoken.spawn_masquerade(Box::new(ctx), Cow::Owned(tokens), cinfo);
		Ok(fd1)
	}
}

impl UpperLayerConnectionFactory for ConnectionFactory2
{
	fn new(&self, poll: &mut Poll, allocator: &mut TokenAllocatorPool, fd: FileDescriptor,
		info: ConnectionInfo) ->
		Result<(Box<dyn UpperLayerConnection>, Vec<AllocatedToken>, bool), Cow<'static, str>>
	{
		//Possibly use TLS.
		let krypt = self.0.with(|g|g.incoming_tls.clone());
		//Proxy according to standard configuration.
		let (ctx, tokens, addr_valid) = Connection::accept(poll, allocator, fd, krypt, info, false,
			self.0)?;
		Ok((Box::new(ctx), tokens, addr_valid))
	}
	fn remap(&self, addr: SocketAddrEx) -> SocketAddrEx
	{
		self.0.with(|g|g.dremap_table.get(&addr).map(cloned)).unwrap_or(addr)
	}
}


struct UninitializedSink<'a>(&'a mut [MaybeUninit<u8>], usize);

impl<'a> UninitializedSink<'a>
{
	fn new(backing: &'a mut [MaybeUninit<u8>]) -> UninitializedSink { UninitializedSink(backing, 0) }
	fn into_slice(self) -> &'a [u8]
	{
		unsafe{transmute(&self.0[..min(self.0.len(), self.1)])}
	}
}

impl<'a> Sink for UninitializedSink<'a>
{
	fn write_slice(&mut self, data: &[u8]) -> Result<(),()>
	{
		let mut tmp = BackedVector::new(slice_tail_mut(self.0, self.1));
		self.1 += dtry!(tmp.extend(data));
		Ok(())
	}
	fn _alter(&mut self, ptr: usize, data: u8)
	{
		match self.0.get_mut(ptr) { Some(x) => *x = MaybeUninit::new(data), None => () };
	}
	fn _readback(&self, ptr: usize) -> u8
	{
		//Don't read anything after self.1, it is toxic.
		if ptr > self.1 || ptr > self.0.len() { return 0; }
		unsafe{self.0[ptr].assume_init()}
	}
	fn written(&self) -> usize { self.1 }
	fn _pop(&mut self, amount: usize) { self.1 = self.1.saturating_sub(amount); }
	fn callback_after(&self, marker: SinkMarker, cb: &mut dyn FnMut(&[u8])->())
	{
		//Don't read anything after self.1, it is toxic.
		let end = min(self.0.len(), self.1);
		let start = min(marker.position(), end);
		unsafe{cb(transmute(&self.0[start..end]))}
	}
}

pub fn decode_hex<'a>(x: &'a mut [u8], src: &[u8]) -> Option<&'a [u8]>
{
	fail_if_none!(src.len() != 2 * x.len());	//Must be correct length.
	for (idx, h) in src.iter().enumerate() {
		let h = match *h {
			h@48..=57 => h - 48,
			h@65..=70 => h - 55,
			h@97..=102 => h - 87,
			_ => return None
		};
		x[idx/2] |= h << 4 - idx % 2 * 4;
	}
	Some(x)
}

fn is_dangerous_header_request(name: &[u8]) -> bool
{
	//The only dangerous request header that is not standard is X-Forwarded-*.
	name.starts_with(b"x-forwarded-")
}
