use crate::connection_midlevel::_Krypted;
use crate::connection_midlevel::Dekryptize;
use crate::connection_midlevel::Kryptize;
use btls::ClientConfiguration;
use btls::ClientConnection;
use btls::Connection;
use btls::Error;
use btls::ServerConfiguration;
use btls::ServerConnection;
use btls::callbacks::SelectClientCertificateParameters;
use btls::callbacks::TlsCallbacks;
use btls::callbacks::ClientCertificateInfo;
use btls::callbacks::RequestCertificateParameters;
use btls::certificates::CertificateLookup;
use btls::certificates::ClientCertificate as ClientCertificateT;
use btls::certificates::FutureReceiver;
use btls::certificates::HostSpecificPin;
use btls::certificates::KeyPair2;
use btls::certificates::LocalServerCertificateStore;
use btls::certificates::TemporaryRandomStream;
use btls::features::FLAGS0_ALLOW_DYNAMIC_RSA;
use btls::features::FLAGS0_ENABLE_TLS12;
use btls::transport::PullTcpBuffer;
use btls::transport::PushTcpBuffer;
use btls_aux_hash::Checksum;
use btls_aux_memory::BackedVector;
use btls_aux_memory::ByteStringLines;
use btls_aux_publiccalist::is_public_ca;
use btls_aux_serialization::Source;
use btls_aux_signatures::convert_key_from_sexpr;
use btls_aux_signatures::EcdsaKeyPair;
use btls_aux_signatures::EcdsaCurve;
use btls_aux_signatures::Ed25519KeyPair;
use btls_aux_signatures::Ed448KeyPair;
use btls_aux_signatures::Hasher;
use btls_aux_signatures::KcKeyFormat;
use btls_aux_signatures::SignatureAlgorithm2;
use btls_aux_signatures::SignatureAlgorithmTls2;
use btls_aux_signatures::SignatureFormat;
use btls_aux_signatures::SignatureKeyPair;
use btls_aux_signatures::SignatureKeyPair3;
use btls_aux_x509certparse::CertificateIssuer;
use btls_aux_x509certparse::ParsedCertificate2;
use btls_daemon_helper_lib::is_fatal_error;
use btls_daemon_helper_lib::RemoteIrq;
use btls_util_logging::syslog;
use std::cmp::min;
use std::collections::HashSet;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::fs::File;
use std::io::Error as IoError;
use std::io::ErrorKind as IoErrorKind;
use std::io::Read;
use std::mem::MaybeUninit;
use std::mem::replace;
use std::ops::Deref;
use std::path::Path;
use std::path::PathBuf;
use std::sync::Arc;

fn tls_to_io_error(e: Error) -> IoError { IoError::new(IoErrorKind::Other, e.to_string()) }

struct TlsState
{
	whitelisted_keys: Option<Arc<HashSet<[u8;32]>>>,
	client_cert: Option<ClientCertificate>,
}

impl TlsState
{
	fn new(client_cert: Option<ClientCertificate>, whitelisted_keys: Option<Arc<HashSet<[u8;32]>>>) -> TlsState
	{
		TlsState{
			//Keys whitelisted.
			whitelisted_keys: whitelisted_keys,
			//Client certificate.
			client_cert: client_cert,
		}
	}
}

impl TlsCallbacks for TlsState
{
	fn request_certificate(&mut self, _: RequestCertificateParameters) -> bool
	{
		self.whitelisted_keys.is_some()
	}
	fn client_authentication(&mut self, info: &ClientCertificateInfo) -> bool
	{
		if let Some(ref kset) = self.whitelisted_keys.as_ref() {
			//Check that the client certificate hash is in the whitelist.
			if let Some(khash) = info.get_public_key_hash2(Checksum::Sha256) {
				let khash = khash.deref();
				if khash.len() == 32 {
					let mut khash2 = [0;32];
					khash2.copy_from_slice(khash);
					//If matches, OK.
					return kset.contains(&khash2);
				}
			}
			//Did not match, abort handshake.
			false
		} else {
			//There are no whitelisted keys, accept.
			true
		}
	}
	fn select_client_certificate(&mut self, criteria: SelectClientCertificateParameters) ->
		Option<Box<dyn ClientCertificateT+Send>>
	{
		self.client_cert.as_ref().and_then(|x|x.select(criteria).ok())
	}
}

struct SendIrq(RemoteIrq);

impl btls::transport::WaitBlockCallback for SendIrq
{
	fn unblocked(&mut self) { self.0.irq(); }
}

fn reschedule_tx<C:Connection>(tls: &mut C, irq: RemoteIrq)
{
	//Only needed in blocked case. Races are handled anyway.
	if tls.get_status().is_blocked() { tls.register_on_unblock(Box::new(SendIrq(irq))); }
}

macro_rules! impl_kryptize
{
	($clazz:ident) => {
		impl Kryptize for $clazz
		{
			//return (insize, outsize)
			fn kryptize(&mut self, input: &[&[u8]]) -> Result<Vec<u8>, IoError>
			{
				let input = if input.len() > 0 {
					PullTcpBuffer::writev(input)
				} else {
					PullTcpBuffer::no_data()
				};
				let ret = self.tls.pull_tcp_data(input).map_err(tls_to_io_error)?;
				reschedule_tx(&mut self.tls, self.irq.clone());
				Ok(ret)
			}
			//return (outsize, was_all)
			fn kryptize_eof(&mut self) -> Result<Vec<u8>, IoError>
			{
				let ret = self.tls.pull_tcp_data(PullTcpBuffer::no_data().eof()).
					map_err(tls_to_io_error)?;
				reschedule_tx(&mut self.tls, self.irq.clone());
				Ok(ret)
			}
			fn return_buffer(&mut self, b: Vec<u8>)
			{
				self.tls.return_buffer_pull(b);
			}
			//Wants to tx data for handshake?
			fn need_tx(&self) -> bool
			{
				self.tls.get_status().is_want_tx()
			}
			//Is ready?
			fn is_ready(&self) -> bool
			{
				!self.tls.get_status().is_handshaking()
			}
			//Get abort reason.
			fn get_abort_reason(&self) -> Option<String>
			{
				self.tls.get_error().map(|x|x.to_string())
			}
			fn print_parameters(&self, f: &mut Formatter) -> Result<(), FmtError>
			{
				f.write_str("TLS")
			}
		}
	}
}

macro_rules! impl_dekryptize
{
	($clazz:ident) => {
		impl Dekryptize for $clazz
		{
			//Obtain dekryptized input.
			fn dekryptized_input(&mut self, buf: &mut [MaybeUninit<u8>]) -> Option<(usize, bool)>
			{
				//Try copying as much data as possible.
				let buflen = buf.len();
				let mut buf = BackedVector::new(buf);
				let sptr = min(self.pending_itr, self.pending.len());
				let eptr = min(sptr + buflen, self.pending.len());
				let copied = buf.extend_fitting(&self.pending[sptr..eptr]);
				self.pending_itr = eptr;
				if self.pending_itr < self.pending.len() {
					//The entiere buffer did not fit -> More.
					Some((copied, true))
				} else {
					//The data buffer is no longer needed.
					self.pending.clear();
					self.pending_itr = 0;
					//There is only more if EOF is set. Otherwise no. If copied = 0 and
					//EOF is set, this is EOF.
					if copied > 0 || !self.pending_eof {
						Some((copied, self.pending_eof))
					} else {
						None
					}
				}
			}
			fn has_output(&self) -> bool { self.pending_itr < self.pending.len() || self.pending_eof }
			fn dekryptize(&mut self, buf: &[u8]) -> Result<(), IoError>
			{
				//TLS dekryptize with pending data is not supported.
				if self.pending.len() > 0 {
					fail!(IoError::new(IoErrorKind::Other,
						"Internal error: Attempted TLS dekryptize with pending output"))
				}
				//Recycle the old received buffer.
				let tmp = replace(&mut self.pending, Vec::new());
				self.tls.return_buffer_push(tmp);
				let (tmp, eofd) = self.tls.push_tcp_data(PushTcpBuffer::u8(buf)).
					map_err(tls_to_io_error)?;
				self.pending = tmp;
				self.pending_eof |= eofd;
				reschedule_tx(&mut self.tls, self.irq.clone());
				self.pending_itr = 0;
				Ok(())
			}
			fn need_rx(&self) -> bool
			{
				self.tls.get_status().is_want_rx()
			}
			fn is_ready(&self) -> bool
			{
				!self.tls.get_status().is_handshaking()
			}
		}
	}
}

struct TlsClientKrypt
{
	tls: ClientConnection,
	irq: RemoteIrq,
}

struct TlsClientDekrypt
{
	tls: ClientConnection,
	irq: RemoteIrq,
	pending: Vec<u8>,
	pending_itr: usize,
	pending_eof: bool,
}

pub struct TlsClient
{
	hostname: String,
	alpn: Option<String>,
	pins: Vec<HostSpecificPin>,
	client_cert: Option<ClientCertificate>,
}

impl _Krypted for TlsClient
{
	fn __new_krypted(&self, irq: RemoteIrq) -> (Box<dyn Kryptize+'static>, Box<dyn Dekryptize+'static>)
	{
		let mut config = ClientConfiguration::new();
		//Disable RSA signatures, as this is not legacy application.
		config.clear_flags(0, FLAGS0_ALLOW_DYNAMIC_RSA);
		//Set ALPN if needed.
		if let Some(ref alpn) = self.alpn.as_ref() { config.add_alpn(alpn); }
		//TODO: More configuration tweaking?

		//This is not a server, so whitelisted_keys is always None.
		let callbacks = TlsState::new(self.client_cert.clone(), None);
		let krypt = TlsClientKrypt {
			tls: config.make_connection_pinned(&self.hostname, &self.pins, Box::new(callbacks)),
			irq: irq.clone(),
		};
		let dekrypt = TlsClientDekrypt {
			tls: krypt.tls.clone(),
			irq: irq,
			pending: Vec::new(),
			pending_itr: 0,
			pending_eof: false,
		};
		(Box::new(krypt), Box::new(dekrypt))
	}
}

impl TlsClient
{
	pub fn new(hostname: &str, alpn: Option<&str>, keys: impl Iterator<Item=[u8;32]>,
		cert: Option<ClientCertificate>) -> TlsClient
	{
		TlsClient {
			hostname: hostname.to_string(),
			alpn: alpn.map(|a|a.to_string()),
			pins: keys.map(|k|HostSpecificPin::trust_server_key_by_sha256(&k, false, false)).collect(),
			client_cert: cert,
		}
	}
}

impl_kryptize!(TlsClientKrypt);
impl_dekryptize!(TlsClientDekrypt);

#[allow(dead_code)]
struct TlsServerKrypt
{
	tls: ServerConnection,
	irq: RemoteIrq,
}

#[allow(dead_code)]
struct TlsServerDekrypt
{
	tls: ServerConnection,
	irq: RemoteIrq,
	pending: Vec<u8>,
	pending_itr: usize,
	pending_eof: bool,
}

enum PrivateKey
{
	Ecdsa(EcdsaKeyPair, EcdsaCurve, Vec<u8>),
	Ed25519(Ed25519KeyPair, Vec<u8>),
	Ed448(Ed448KeyPair, Vec<u8>),
}

fn errstr(e: impl Display) -> String { e.to_string() }

impl PrivateKey
{
	fn new(key: &[u8]) -> Result<(PrivateKey, Vec<u8>), String>
	{
		let (kf, raw) = convert_key_from_sexpr(key).map_err(errstr)?;
		Ok(match kf {
			KcKeyFormat::Ecdsa => {
				let (curve, sk, pk) = EcdsaKeyPair::keypair_load(&raw).map_err(errstr)?;
				(PrivateKey::Ecdsa(sk, curve, pk.clone()), pk)
			},
			KcKeyFormat::Ed25519 => {
				let (_, sk, pk) = Ed25519KeyPair::keypair_load(&raw).map_err(errstr)?;
				(PrivateKey::Ed25519(sk, pk.clone()), pk)
			},
			KcKeyFormat::Ed448 => {
				let (_, sk, pk) = Ed448KeyPair::keypair_load(&raw).map_err(errstr)?;
				(PrivateKey::Ed448(sk, pk.clone()), pk)
			},
			_ => fail!("Unknown key type")
		})
	}
}

fn __with_sign_fmt<'a>(output: &'a mut [u8], f: impl FnOnce(&mut [u8]) -> Result<usize, ()>) -> Result<&'a [u8], ()>
{
	let siglen = dtry!(f(output));
	let sig = dtry!(output.get(..siglen));
	Ok(sig)
}

fn with_sign_fmt(f: impl FnOnce(&mut [u8]) -> Result<usize, ()>) ->
	FutureReceiver<Result<(SignatureFormat, Vec<u8>), ()>>
{
	//The format does not actually matter.
	let fmt = SignatureFormat::Unknown;
	let mut output = [0;256];	//Should be big enough for ECDSA.
	FutureReceiver::from(__with_sign_fmt(&mut output, f).map(|x|(fmt, x.to_owned())))
}

impl KeyPair2 for PrivateKey
{
	fn get_signature_algorithms<'b>(&'b self) -> &'b [SignatureAlgorithm2]
	{
		match self {
			&PrivateKey::Ecdsa(ref k,_,_) => k.get_signature_algorithms(),
			&PrivateKey::Ed25519(ref k,_) => k.get_signature_algorithms(),
			&PrivateKey::Ed448(ref k,_) => k.get_signature_algorithms(),
		}
	}
	fn sign_data(&self, data: &[u8], alg: SignatureAlgorithm2, rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<(SignatureFormat, Vec<u8>), ()>>
	{
		with_sign_fmt(|output|match self {
			&PrivateKey::Ecdsa(ref k,_,_) => k.sign_data(data, alg, output, rng),
			&PrivateKey::Ed25519(ref k,_) => k.sign_data(data, alg, output, rng),
			&PrivateKey::Ed448(ref k,_) => k.sign_data(data, alg, output, rng),
		})
	}
	fn sign_callback(&self, alg: SignatureAlgorithm2, rng: &mut TemporaryRandomStream,
		msg: &mut dyn FnMut(&mut Hasher)) -> FutureReceiver<Result<(SignatureFormat, Vec<u8>), ()>>
	{
		with_sign_fmt(|output|match self {
			&PrivateKey::Ecdsa(ref k,_,_) => k.sign_callback(output, alg, rng, msg),
			&PrivateKey::Ed25519(ref k,_) => k.sign_callback(output, alg, rng, msg),
			&PrivateKey::Ed448(ref k,_) => k.sign_callback(output, alg, rng, msg),
		})
	}
	fn get_public_key2<'b>(&'b self) -> &'b [u8]
	{
		match self {
			&PrivateKey::Ecdsa(_,_,ref k) => k,
			&PrivateKey::Ed25519(_,ref k) => k,
			&PrivateKey::Ed448(_,ref k) => k,
		}
	}
	fn get_key_type2<'b>(&'b self) -> &'b str
	{
		match self {
			&PrivateKey::Ecdsa(_,ref crv,_) => crv.get_ecdsa_name(),
			&PrivateKey::Ed25519(_,_) => "ed25519",
			&PrivateKey::Ed448(_,_) => "ed448",
		}
	}
}

#[allow(dead_code)]
#[derive(Clone)]
struct RpServerCertificate
{
	certificate: Arc<Vec<u8>>,
	chain: Arc<Vec<Vec<u8>>>,
	pkey: Arc<PrivateKey>,
}

impl RpServerCertificate
{
	fn load_certificate(cert: &[u8], pk: &[u8]) -> Result<(Vec<u8>, Vec<Vec<u8>>, Vec<u8>), String>
	{
		const ERR_WEBPKI: &'static str = "WebPKI certificates are not allowed";
		const ERR_SELFSIGN: &'static str = "Self-signed certificate must be in last position";
		const ERR_BADCHAIN: &'static str = "Next certificate in chain does not sign previous";
		const ERR_BADKEY: &'static str = "Wrong private key given for certificate";
		const ERR_IP: &'static str = "IP addresses not allowed in SAN";
		const ERR_DNS_MULTI: &'static str = "There must be exactly one DNS SAN";
		let mut cert = Source::new(cert);
		let mut chain = Vec::new();
		let mut last_issuer: Option<CertificateIssuer> = None;
		let mut last_selfsigned = false;
		let mut xname: Option<Vec<u8>> = None;
		while cert.more() {
			let cert = cert.asn1_sequence_out().map_err(|err|{
				format!("Not in DER format: {err}")
			})?;
			chain.push(cert.to_vec());
			fail_if!(last_selfsigned, ERR_SELFSIGN);
			let cert_p = ParsedCertificate2::from(cert).map_err(|err|{
				format!("Parse error: {err}")
			})?;
			fail_if!(is_public_ca(cert_p.issuer.as_raw_issuer()), ERR_WEBPKI);
			//Check that this is subject of last_issuer, if any.
			if let Some(last_issuer) = last_issuer {
				fail_if!(!last_issuer.same_as(cert_p.subject), ERR_BADCHAIN);
			}
			last_issuer = Some(cert_p.issuer);
			last_selfsigned = cert_p.issuer.same_as(cert_p.subject);
			if chain.len() == 1 {
				fail_if!(pk != cert_p.pubkey, ERR_BADKEY);
				fail_if!(cert_p.dnsnames.iter_addrs().count() > 0, ERR_IP);
				let mut iter = cert_p.dnsnames.iter();
				let name = iter.next().ok_or(ERR_DNS_MULTI)?;
				fail_if!(iter.next().is_some(), ERR_DNS_MULTI);
				xname = Some(name.to_owned());
			}
		}
		let xname = xname.ok_or("Empty certificate chain not allowed")?;
		let cert = chain.remove(0);	//The previous check guarantees chain is not empty.
		Ok((cert, chain, xname))
	}
}

//A certificate that has been loaded signing.
#[derive(Clone)]
pub struct ClientCertificate
{
	//The keypair that does the signing.
	keypair: Arc<Box<dyn KeyPair2+Send+Sync>>,
	//The certificate.
	certificate: Arc<Vec<u8>>,
	//The certificate chain to send.
	chain: Arc<Vec<Vec<u8>>>,
}

impl ClientCertificate
{
	pub fn new(keypair: Arc<Box<dyn KeyPair2+Send+Sync>>, certificate: Vec<u8>, chain: Vec<Vec<u8>>) ->
		ClientCertificate
	{
		ClientCertificate {
			keypair: keypair,
			certificate: Arc::new(certificate),
			chain: Arc::new(chain),
		}
	}
	fn select(&self, criteria: SelectClientCertificateParameters) -> Result<Box<dyn ClientCertificateT+Send>, ()>
	{
		let available = self.keypair.get_signature_algorithms().iter().
			filter_map(|x|x.downcast_tls().map(|y|y.tls_id()));
		let algo = dtry!(criteria.choose_sigalg(available));
		Ok(Box::new(SelectedCertificate {
			keypair: self.keypair.clone(),
			tls_algorithm: algo,
			certificate: self.certificate.clone(),
			chain: self.chain.clone(),
		}))
	}
}


//A certificate that has been selected for signing.
#[derive(Clone)]
struct SelectedCertificate
{
	//The keypair that does the signing.
	keypair: Arc<Box<dyn KeyPair2+Send+Sync>>,
	//The TLS algorithm number to use.
	tls_algorithm: SignatureAlgorithmTls2,
	//The certificate.
	certificate: Arc<Vec<u8>>,
	//The certificate chain to send.
	chain: Arc<Vec<Vec<u8>>>,
}

impl ClientCertificateT for SelectedCertificate
{
	fn sign(&self, data: &mut dyn FnMut(&mut Hasher), rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<(u16, Vec<u8>), ()>>
	{
		btls::certificates::tls_sign_helper(self.keypair.deref().deref(), data, self.tls_algorithm, rng)
	}
	fn certificate<'a>(&'a self) -> &'a [u8] { self.certificate.deref().deref() }
	fn chain<'a>(&'a self) -> &'a [Vec<u8>] { self.chain.deref().deref() }
	//We don't implement get_ocsp nor get_scts. These get replaced by defaults.
}

fn read_file_max(path: &Path, maxsize: usize, enoent: &mut bool) -> Result<Vec<u8>, String>
{
	const CHUNK: usize = 16384;
	let mut data = Vec::new();
	let mut fp = File::open(path).map_err(|e|{
		*enoent |= e.kind() == IoErrorKind::NotFound;	//ENOENT?
		e.to_string()
	})?;
	let mut size = 0;
	loop {
		data.resize(size + CHUNK, 0);
		match fp.read(&mut data[size..]) {
			Ok(amt) => {
				size += amt;
				if amt == 0 {
					//Completed.
					data.truncate(size);
					return Ok(data);
				}	
				fail_if!(size > maxsize, "File too big");
			},
			Err(ref e) if !is_fatal_error(&e) => (),	//Temporary error.
			Err(e) => fail!(e.to_string())			//Error.
		}
	}
}

fn read_whitelist_from_file(path: &Path, content: Option<&[u8]>) -> Option<HashSet<[u8;32]>>
{
	if let Some(content) = content {
		let mut out = HashSet::new();
		for (linenum, line) in ByteStringLines::new(&content).enumerate() {
			if line.len() == 0 || line.starts_with(b"#") { continue; }
			let mut item = [0;32];
			let mut ok = line.len() == 64;
			for (i,b) in line.iter().copied().enumerate() {
				let h = match b {
					48..=57 => b - 48,
					65..=70 => b - 55,
					97..=102 => b - 87,
					_ => { ok = false; break; }
				};
				item.get_mut(i/2).map(|x|*x |= h << 4 - i % 2 * 4);
			}
			if ok {
				out.insert(item);
			} else {
				syslog!(WARNING "{path}:{line}: Bad whitelist line",
					path=path.display(), line=linenum+1);
			}
		}
		Some(out)
	} else {
		None
	}
}

#[allow(dead_code)]
pub struct TlsServer
{
	lookup: CertificateLookup,
	name: Vec<u8>,
	whitelisted_keys: Option<Arc<HashSet<[u8;32]>>>,
}

fn keyerr(key: &Path, err: impl Display) -> String
{
	format!("Error loading key '{key}': {err}", key=key.display())
}

fn certerr(cert: &Path, err: impl Display) -> String
{
	format!("Error loading certificate '{cert}': {err}", cert=cert.display())
}

fn whiteerr(klist: &Path, err: impl Display) -> String
{
	format!("Error loading whitelist '{klist}': {err}", klist=klist.display())
}

impl TlsServer
{
	pub fn new(base: &Path) -> Result<TlsServer, String>
	{
		//base.key -> key, base.crt -> cert.
		let mut key = base.as_os_str().to_owned();	key.push(".key");
		let mut cert = base.as_os_str().to_owned();	cert.push(".crt");
		let mut klist = base.as_os_str().to_owned();	klist.push(".whitelist");
		let key = PathBuf::from(key);
		let cert = PathBuf::from(cert);
		let klist = PathBuf::from(klist);

		const MAXFILE: usize = 1 << 20;		//1MB.
		let keydata = read_file_max(&key, MAXFILE, &mut false).map_err(|err|{
			keyerr(&key, err)
		})?;
		let certdata = read_file_max(&cert, MAXFILE, &mut false).map_err(|err|{
			certerr(&cert, err)
		})?;
		let mut whitelist_np = false;
		let whitelist = match read_file_max(&klist, MAXFILE, &mut whitelist_np) {
			Ok(content) => Some(content),
			Err(_) if whitelist_np => None,		//Distinct from empty.
			Err(e) => Err(whiteerr(&klist, e))?,
		};
		let (sk, pk) = PrivateKey::new(&keydata).map_err(|err|{
			keyerr(&key, err)
		})?;
		let (eecert, chain, name) = RpServerCertificate::load_certificate(&certdata, &pk).map_err(|err|{
			certerr(&cert, err)
		})?;
		let mut store = LocalServerCertificateStore::new();
		store.add_privkey(sk, "private_key");
		//Can just drop the handle, it does not have Drop to clean it up.
		let errpfx = "Failed to load server certificate:";
		let name = String::from_utf8(name).map_err(|_|{
			format!("{errpfx} Not valid UTF-8")
		})?;
		store.add_explicit_chain(eecert, Arc::new(chain), &name).map_err(|err|{
			format!("{errpfx} {err}")
		})?;
		let whitelisted_keys = read_whitelist_from_file(&klist, whitelist.as_deref());
		Ok(TlsServer {
			name: name.into_bytes(),
			lookup: store.lookup(),
			whitelisted_keys: whitelisted_keys.map(|s|Arc::new(s.clone())),
		})
	}
	pub fn get_name(&self) -> Vec<u8> { self.name.clone() }
}

impl _Krypted for TlsServer
{
	fn __new_krypted(&self, irq: RemoteIrq) -> (Box<dyn Kryptize+'static>, Box<dyn Dekryptize+'static>)
	{
		let mut config = ServerConfiguration::from(self.lookup.clone());
		//Disable TLS 1.2, as it can not be safely used due to being 4-flight.
		config.clear_flags(0, FLAGS0_ENABLE_TLS12);
		//Disable RSA signatures, as this is not legacy application.
		config.clear_flags(0, FLAGS0_ALLOW_DYNAMIC_RSA);
		//TODO: Some more config tweaking?

		//Client certificate is always None.
		let callbacks = TlsState::new(None, self.whitelisted_keys.clone());
		let krypt = TlsServerKrypt {
			tls: config.make_connection(Box::new(callbacks)),
			irq: irq.clone(),
		};
		let dekrypt = TlsServerDekrypt {
			tls: krypt.tls.clone(),
			irq: irq,
			pending: Vec::new(),
			pending_itr: 0,
			pending_eof: false,
		};
		(Box::new(krypt), Box::new(dekrypt))
	}
	fn __is_server_slave(&self) -> bool { self.whitelisted_keys.is_some() }
}

impl_kryptize!(TlsServerKrypt);
impl_dekryptize!(TlsServerDekrypt);
