use crate::ConfigT;
use crate::is_dangerous_header_request;
use crate::connection::BackendName;
use crate::connection::Connector;
use crate::connection::print_backend_response;
use crate::connection::print_internal_response;
use crate::connection_midlevel::Interrupter;
use crate::connection_lowlevel::CID;
use crate::connection_midlevel::BackwardInterface;
use crate::connection_midlevel::BackwardInterfaceError;
use crate::connection_midlevel::ReaderWriterM;
use crate::connection_midlevel::SinkError;
use crate::connection_midlevel::WriterB;
use crate::connection_midlevel::WriterM;
use crate::headerblock::HeaderBlock;
use crate::headerblock::HeaderBlockTrait;
use crate::headerblock::HeaderError;
use crate::headerblock::HeaderKind;
use crate::headerblock::OwnedTrailerBlock;
use crate::headerblock::serialize_headers;
use crate::headerblock::serialize_trailers;
use crate::headerblock::ShdrAuthorityRaw;
use crate::headerblock::ShdrMethod;
use crate::headerblock::ShdrPathRaw;
use crate::headerblock::ShdrProtocol;
use crate::headerblock::ShdrQuery;
use crate::headerblock::ShdrScheme;
use crate::headerblock::ShdrVersion;
use crate::http::RequestInfo;
use crate::http::ClientInfoIn;
use crate::http::CONNECT_ERROR;
use crate::http::CONNECTION_LOST;
use crate::http::DfltIRWError;
use crate::http::InternalResponse;
use crate::http::InternalResponseWriter;
use crate::http::RequestWriteReturn;
use crate::http::TransactionStateIn;
use crate::metrics_dp::IncomingConnectionState;
use crate::utils::strip_port;
use btls_aux_http::http::StandardHttpHeader;
use btls_aux_http::http2::Connection as HttpConnection;
use btls_aux_http::http2::Element;
use btls_aux_http::http2::ElementSink;
use btls_aux_http::http2::HeaderEncoder;
use btls_aux_http::http2::HTTP2_SUPPORTED_SETTINGS;
use btls_aux_http::http2::IoOrStreamError;
use btls_aux_http::http2::NO_PADDING;
use btls_aux_http::http2::RespondWith;
use btls_aux_http::http2::SendEvent;
use btls_aux_http::http2::StreamId;
use btls_aux_http::http2::StreamIdNonZero;
use btls_aux_http::http2::WindowIncrement;
use btls_aux_http::status::BAD_GATEWAY;
use btls_aux_http::status::GATEWAY_TIMEOUT;
use btls_aux_memory::SafeShowByteString;
use btls_aux_memory::split_at;
use btls_daemon_helper_lib::cow;
use btls_util_logging::ConnId;
use btls_util_logging::log;
use btls_util_logging::syslog;
use std::borrow::Cow;
use std::cmp::max;
use std::cmp::min;
use std::collections::BTreeSet;
use std::collections::HashMap;
use std::collections::HashSet;
use std::collections::VecDeque;
use std::fmt::Display;
use std::mem::MaybeUninit;
use std::str::from_utf8;
use std::str::FromStr;
use std::usize::MAX as USIZE_MAX;


static PRECAN_HEADER: &'static [u8] = b"\x20\
\x8e\
\x00\x8a\xf2\xb4\xe9\x4d\x62\x5a\xc2\x8d\x07\xab\x98\xc9\xa7\x7e\x0d\xba\x61\xce\xa6\x0d\xbb\x75\x6d\xb8\xa5\xfd\x5c\x2e\xe7\xc0\xa6\x18\x78\x67\xdf\
\x0f\x10\x91\x49\x7c\xa5\x8a\xe8\x19\xaa\xfb\x24\xe3\xb1\x05\x4c\x16\xa6\x55\x9e\
\x00\x90\x21\xea\x49\x6a\x4a\xc8\x29\x2d\xb0\xc9\xf4\xb5\x67\xa0\xc4\xf5\x8e\x90\xb2\x8e\xda\x12\xb2\x2c\x22\x9f\xea\xa3\xd4\x5f\xf5\
\x00\x90\xf2\xb1\x0f\x52\x4b\x52\x56\x4f\xaa\xca\xb1\xeb\x49\x8f\x52\x3f\x85\xa8\xe8\xa8\xd2\xcb\
";
const MAX_HBLOCK_SIZE: usize = 16000;
const GIVE_CREDIT_THRESHOLD: i32 = 1000000000;
const DEFAULT_PRIO: u8 = 16;
const DEFAULT_IPRIO: u8 = 255 - DEFAULT_PRIO;
const GIVE_CREDIT_AMOUNT: u32 = 1000000000;
const BUFFER_EMERG_SPACE: usize = 8192;


fn do_trace_header(sid: u32, name: &[u8], value: &[u8])
{
	//This is PII.
	log!(DEBUG  "Trace header: [sid {sid}] '{name}' => '{value}'",
		name=SafeShowByteString(name), value=SafeShowByteString(value));
}

fn do_trace_header_fragment(sid: u32, fragment: &[u8], soh: bool, eoh: bool)
{
	//This is PII.
	use std::fmt::Write;
	let bkind = if soh { if eoh { "only" } else { "start" }} else if eoh { "end" } else { "internal" };
	log!(DEBUG  "Trace header: [sid {sid}] {bkind} block of length {flen}:", flen=fragment.len());
	let mut i = 0;
	while i < fragment.len() {
		let l = min(fragment.len() - i, 16);
		let mut t = [0;16];
		//l <= 16 = t.len(), i + l <= i + fragment.len() - i = fragment.len().
		(&mut t[..l]).copy_from_slice(&fragment[i..][..l]);
		let mut s = String::new();
		write!(s, "> {t:03x}|", t=i/16).ok();
		//This is not a normal hexdump due to the spaces.
		for i in 0..l { write!(s, "{t:02x} ", t=t[i]).ok(); }
		log!(DEBUG "{s}");
		i += 16;
	}
}

//Monitor ReapStream event for given stream. This is useful as event sink for sending headers or data, as stream
//reaps are the only type of event those can give. Additionally, resets also give stream reaps.
struct ReapMonitor(bool);

impl SendEvent for ReapMonitor
{
	fn reaped(&mut self) { self.0 = true; }
	fn aborted(&mut self, _: u32) {}
}

struct RWriteWrapper2<'a,'c:'a,W:WriterM>(&'a mut W, &'a mut RespondWith<'c>, ReapMonitor);

impl<'a,'c:'a,W:WriterM> InternalResponseWriter for RWriteWrapper2<'a,'c,W>
{
	type Encoder = HeaderEncoder;
	type Error = DfltIRWError;
	fn query_maxsize(&self) -> usize { self.1.get_max_data() }
	fn df_precan_headers() -> &'static [u8] { PRECAN_HEADER }
	fn have_content_length(&self) -> bool { false }
	fn get_cid_and_sid(&self) -> (CID, u32)
	{
		(self.0.get_cid(), self.1.get_sid().into_inner())
	}
	fn flush_headers_only(&mut self, h: &[u8]) -> Result<(), DfltIRWError>
	{
		//Just ignore write if it is to closed stream.
		self.1.send_headers_frame(self.0, &mut self.2, true, true, NO_PADDING, None, h).map_err(|err|{
			DfltIRWError::new(format_args!("Write error: {err}"))
		})?;
		Ok(())
	}
	fn flush_headers_and_payload(&mut self, h: &[u8], p: &[u8]) -> Result<(), DfltIRWError>
	{
		//Truncate to 16kB.
		let p = &p[..min(p.len(), 16384)];
		//Just ignore write if it is to closed stream.
		self.1.send_headers_frame(self.0, &mut self.2, false, true, NO_PADDING, None, h).and_then(|_|{
			self.1.send_data_frame(self.0, &mut self.2, true, NO_PADDING, p)
		}).map_err(|err|{
			DfltIRWError::new(format_args!("Write error: {err}"))
		})?;
		Ok(())
	}
	fn error_forward_failure<D:Display>(&mut self, cause: D) -> Result<(), DfltIRWError>
	{
		//Just ignore write if it is to closed stream.
		self.1.send_rst_stream_frame(self.0, &mut self.2, 2).map(|_|()).map_err(|err|{
			DfltIRWError::new(format_args!("Write error: {err} [cause: {cause}]"))
		})
	}
}

fn alt_prio(urg: Option<u8>, inc: bool) -> u8
{
	255 - urg.unwrap_or(3) % 128 * 2 - inc as u8
}

const STREAMLIST_RANDOM: u8 = 0;
const STREAMLIST_LOWEST: u8 = 1;
const STREAMLIST_FIFO: u8 = 2;

enum StreamListMode
{
	Random(HashSet<u32>),
	Lowest(BTreeSet<u32>),
	Fifo(VecDeque<u32>),
}

impl StreamListMode
{
	fn new(mode: u8) -> StreamListMode
	{
		match mode {
			STREAMLIST_FIFO => StreamListMode::Fifo(VecDeque::new()),
			STREAMLIST_LOWEST  => StreamListMode::Lowest(BTreeSet::new()),
			STREAMLIST_RANDOM  => StreamListMode::Random(HashSet::new()),
			_ => StreamListMode::Random(HashSet::new()),    //Default.
		}
	}
	//True if list is empty now.
	fn pop_first(&mut self) -> (bool, Option<u32>)
	{
		match self {
			//Random and Lowest have the same code that extracts the first iterated element. However, the
			//two do not do the same thing, because the list types are different.
			&mut StreamListMode::Random(ref mut list) => {
				let entry = list.iter().copied().next();
				if let Some(entry) = entry { list.remove(&entry); }
				(list.is_empty(), entry)
			},
			&mut StreamListMode::Lowest(ref mut list) => {
				let entry = list.iter().copied().next();
				if let Some(entry) = entry { list.remove(&entry); }
				(list.is_empty(), entry)
			},
			&mut StreamListMode::Fifo(ref mut list) => {
				let entry = list.pop_front();
				(list.is_empty(), entry)
			}
		}
	}
	//First is list empty flag, second is element removed flag.
	fn remove(&mut self, strm: u32) -> (bool, bool)
	{
		match self {
			&mut StreamListMode::Random(ref mut list) => {
				let df = list.remove(&strm);
				(list.is_empty(), df)
			},
			&mut StreamListMode::Lowest(ref mut list) => {
				let df = list.remove(&strm);
				(list.is_empty(), df)
			},
			&mut StreamListMode::Fifo(ref mut list) => {
				let index = list.iter().position(|&strmid|{
					strmid == strm
				});
				if let Some(index) = index { list.remove(index); }
				(list.is_empty(), index.is_some())
			}
		}
	}
	fn insert(&mut self, strm: u32)
	{
		match self {
			&mut StreamListMode::Random(ref mut list) => {
				list.insert(strm);
			},
			&mut StreamListMode::Lowest(ref mut list) => {
				list.insert(strm);
			},
			&mut StreamListMode::Fifo(ref mut list) => {
				list.push_back(strm);
			}
		};
	}
}

struct ReadyStreamTable
{
	//Stream priorities. Every stream should have a priority value in this table. Note that priorities are
	//inverted, with 0 being highest and 255 being lowest, instead of other way they are in HTTP/2.
	priorities: HashMap<u32, u8>,
	//Per-priority queues of ready SIDs. The key is inverted priority value. Only streams with data ready in
	//secondary o2i buffer should be present.
	readyq: HashMap<u8, StreamListMode>,
	//Alt priority mode.
	alt_mode: bool,
}

impl ReadyStreamTable
{
	fn new() -> ReadyStreamTable
	{
		ReadyStreamTable {
			priorities: HashMap::new(),
			readyq: HashMap::new(),
			alt_mode: false,
		}
	}
	fn delete_by_sid(&mut self, sid: u32) -> bool
	{
		//Get priority. Then remove entry in correct queue. If this blanks a queue, delete it.
		let default_iprio = self.default_iprio();
		let prio = self.priorities.get(&sid).map(|x|*x).unwrap_or(default_iprio);
		let (nuke_list, deleted) = self.readyq.get_mut(&prio).map(|x|x.remove(sid)).
			unwrap_or((false, false));
		if nuke_list {self.readyq.remove(&prio); }
		deleted
	}
	fn add_default_prio(&mut self, sid: u32)
	{
		self.delete_by_sid(sid);
		let default_iprio = self.default_iprio();
		self.priorities.insert(sid, default_iprio);
		//We do not push this to readyq, since slaves start transmitting by themselves and only request
		//adding to readyq if blocked.
	}
	fn insert_by_sid(&mut self, sid: u32)
	{
		let default_iprio = self.default_iprio();
		let prio = self.priorities.get(&sid).map(|x|*x).unwrap_or(default_iprio);
		self.__mark_ready(sid, prio);
	}
	fn reprioritize_by_sid(&mut self, sid: u32, prio: u8)
	{
		let prio = 255 - prio;	//Note that priorities are inverted.
		let exists = self.delete_by_sid(sid);
		self.priorities.insert(sid, prio);
		if exists { self.__mark_ready(sid, prio); }
	}
	fn __mark_ready(&mut self, sid: u32, iprio: u8)
	{
		let mode = if self.alt_mode {
			//In alt mode, even inverse priorities (higher) are LOWEST, and odd are FIFO.
			if iprio & 1 == 0 {
				STREAMLIST_LOWEST
			} else {
				STREAMLIST_FIFO
			}
		} else {
			//In non-alt mode, all are RANDOM.
			STREAMLIST_RANDOM
		};
		self.readyq.entry(iprio).or_insert_with(||StreamListMode::new(mode)).insert(sid);
        }
	fn pop_highest_priority(&mut self) -> Option<u32>
	{
		//Find the highest-priority queue.
		let (nuke_prio, elem) = self.readyq.iter_mut().next().map(|(prio, x)|{
			let (nuke_list, y) = x.pop_first();
			(if nuke_list { Some(*prio) } else { None }, y)
		}).unwrap_or((None, None));
		nuke_prio.map(|prio|self.readyq.remove(&prio));
		elem
	}
	fn uses_alt_priorities(&self) -> bool { self.alt_mode }
	fn enable_alt_mode(&mut self)
	{
		//Alt mode can not be enabled if there are any streams open.
		if !self.priorities.is_empty() { return; }
		self.alt_mode = true;
	}
	fn default_iprio(&self) -> u8
	{
		//In alt mode, default iprio is 6 (progressive 3).
		if self.alt_mode { 6 } else { DEFAULT_IPRIO }
	}
	fn get_priority_for_sid(&self, sid: u32) -> Option<u8>
	{
		Some(255 - self.priorities.get(&sid)?)
	}
}

const SETTINGS_NO_RFC7540_PRIORITIES: u16 = 0x0009;
const PRIORITY_UPDATE: u8 = 0x10;

struct Sink
{
	//It is guaranteed that there are only one set of pending headers at a time.
	headers: HeaderBlock,
	client_info: ClientInfoIn,
	streams: HashMap<u32, TransactionStateIn>,
	//Unlike in HTTP/1.1, where Content-Length is always correct, HTTP/2 allows incorrect Content-Length
	//values, so enforce that Content-Length is actually correct. Only streams that have content-length exist
	//in this map.
	countdown: HashMap<u32, u64>,
	//Blocked flag.
	blocked_steams: HashSet<u32>,
	//Ready flags and priorities.
	ready_streams: ReadyStreamTable,
	maxstream: u32,
	//Alt prio parser.
	h2extprio: H2ExtPriority,
	//Global configuration.
	g: ConfigT,
}

impl Sink
{
	fn reap_if(&mut self, sid: u32, c: bool)
	{
		if c {
			//Remove stream from all tables it is potentially in: Main streams, data countdown, blocked
			//streams and priority tables.
			self.streams.remove(&sid);
			self.countdown.remove(&sid);
			self.blocked_steams.remove(&sid);
			self.ready_streams.delete_by_sid(sid);
		}
	}
	fn do_forward_fault(&mut self, sid: &mut RespondWith, ecode: Option<u32>, msg: Cow<'static, str>,
		fatal: bool, sink: &mut impl WriterM) -> Result<(), SinkError>
	{
		let nsid = sid.get_sid().into_inner();
		let mut status = Ok(());
		//Some faults need to send stream resets!
		let mut sink = RWriteWrapper2(sink, sid, ReapMonitor(false));
		if let Some(ecode) = ecode {
			sink.1.send_rst_stream_frame(sink.0, &mut sink.2, ecode).map_err(|e|
				match e.extract_io_error() {
				Ok(e) => SinkError::WriteFailed(DfltIRWError::new(e)),
				Err(e) => SinkError::ResetError(e.to_string()),
			})?;
		}
		{
			let headers = &self.headers;
			self.streams.get_mut(&nsid).map(|strm|{
				//If ecode is set, close RX because it may not be closed otherwise.
				if ecode.is_some() { strm.close_backward(); }
				status = strm.do_forward_error(msg, &mut sink, fatal, headers);
			});
		}
		self.reap_if(nsid, (sink.2).0);	//If stream got reaped, remove it.
		status.map_err(|e|SinkError::WriteFailed(e))
	}
	fn do_forward_headers(&mut self, sid: &mut RespondWith, has_body: bool, sink: &mut impl WriterM,
		connector: &mut Connector) -> Result<(), SinkError>
	{
		let nsid = sid.get_sid().into_inner();
		if self.ready_streams.uses_alt_priorities() {
			self.ready_streams.reprioritize_by_sid(nsid,
				alt_prio(self.h2extprio.priority, self.h2extprio.incremental));
		}
		self.maxstream = max(self.maxstream, nsid);
		//Transform the headers to HTTP/1 headers.
		self.headers.transform_h2_to_h1(has_body);
		//Forward the HTTP/1 headers, and thne reap the stream if it got removed.
		let (reaped, status) = {
			let block = &mut self.headers;
			let id = sink.get_id();
			let mut sink = RWriteWrapper2(sink, sid, ReapMonitor(false));
			let mut status = Ok(());
			self.streams.get_mut(&nsid).map(|strm|{
				//HTTP/2 is always secure.
				status = strm.do_forward_headers(&mut sink, block, connector, has_body, id);
			});
			((sink.2).0, status)
		};
		self.reap_if(nsid, reaped);
		self.headers.clear();	//Clear the headers, for the case where there are trivial trailers.
		status.map_err(|e|SinkError::WriteFailed(e))
	}
	fn do_forward_trailers(&mut self, sid: &mut RespondWith, sink: &mut impl WriterM) ->
		Result<(), SinkError>
	{
		let nsid = sid.get_sid().into_inner();
		//Check that data countdown either does not exist or is at 0. In HTTP/2, it is possible for the
		//message length to be bad.
		if self.countdown.get(&nsid).map(|x|*x).unwrap_or(0) != 0 {
			return self.do_forward_fault(sid, None, cow!("Content-Length does not match data length"),
				false, sink);
		}
		//Otherwise forward the trailers, and reap the stream if it got removed.
		let mut status = Ok(());
		let reaped = {
			let block = &mut self.headers;
			let mut sink = RWriteWrapper2(sink, sid, ReapMonitor(false));
			self.streams.get_mut(&nsid).map(|strm|{
				status = strm.do_forward_trailers(&mut sink, block);
			});
			(sink.2).0
		};
		self.reap_if(nsid, reaped);	//If stream got reaped, remove it.
		self.headers.clear();	//Clear the headers, for the case where there are trivial trailers.
		status.map_err(|e|SinkError::WriteFailed(e))
	}
	fn do_forward_data(&mut self, sid: &mut RespondWith, d: &[u8], sink: &mut impl WriterM) ->
		Result<(), SinkError>
	{
		let nsid = sid.get_sid().into_inner();
		//Check that data countdown either does not exist or does not undeflow. In HTTP/2, it is possible
		//for the message length to be bad.
		if d.len() as u64 > self.countdown.get(&nsid).map(|x|*x).unwrap_or(d.len() as u64) {
			return self.do_forward_fault(sid, None, cow!("Content-Length does not match data length"),
				false, sink);
		}
		if let Some(data_countdown) = self.countdown.get_mut(&nsid) {
			*data_countdown -= d.len() as u64;
		}
		//Blocked is map that stores the blocked streams. Update it according to if forwarding data blocked
		//or not. Finally reap the stream if it got removed.
		let mut blocked = self.blocked_steams.contains(&nsid);
		let mut status = Ok(());
		let mut sink = RWriteWrapper2(sink, sid, ReapMonitor(false));
		self.streams.get_mut(&nsid).map(|strm|{
			status = strm.do_forward_data(d, &mut sink, &mut blocked);
		});
		if blocked { self.blocked_steams.insert(nsid) } else { self.blocked_steams.remove(&nsid) };
		self.reap_if(nsid, (sink.2).0);
		status.map_err(|e|SinkError::WriteFailed(e))
	}
	fn do_start_message(&mut self, sid: &mut RespondWith) -> Result<(), SinkError>
	{
		let nsid = sid.get_sid().into_inner();
		self.streams.insert(nsid, TransactionStateIn::new(self.client_info.clone(), self.g));
		self.ready_streams.add_default_prio(nsid);
		Ok(())
	}
	fn sink(&mut self, elem: Element, sink: &mut impl WriterM, connector: &mut Connector)
	{
		if sink.aborted() { return; }
		let r: Result<(), SinkError> = match elem {
			//Ensure that headers are cleared for each block received. Set the IP address info
			//immedately, to ensure it is available. HTTP/2 can never be chained, so no need to
			//check for chaining.
			Element::StartHeaderBlock => {
				self.headers.clear();
				self.client_info.set_client_info(&mut self.headers);
				self.headers.set_secure(true);
				self.h2extprio = H2ExtPriority::new();	//Reset.
				Ok(())
			},
			//In case of abort, send forward fault. Mark this as not fatal to actually send the error,
			//as there is no connection abort to fall back on. However, faults with error code are
			//still marked as fatal.
			Element::Abort(sid, err) => {
				let errcode = err.get_errorcode();
				let err = if errcode.is_some() {
					cow!(F "Stream error: {err}")
				} else {
					cow!(F "{err}")
				};
				self.do_forward_fault(sid, errcode, err, errcode.is_some(), sink)
			},
			Element::Authority(_, a) => {
				let a = strip_port(a);
				//Also set X-Forwarded-Host and X-Forwarded-Server.
				self.headers.set_authority(a);
				Ok(())
			},
			Element::ContentLength(sid, l) => {
				self.headers.set_content_length(l);
				self.countdown.insert(sid.get_sid().into_inner(), l);
				Ok(())
			}
			Element::Data(sid, d) => self.do_forward_data(sid, d, sink),
			Element::EndOfHeaders(sid) => self.do_forward_headers(sid, true, sink, connector),
			//This is illegal (being only for responses). However, there is no guarantee whatever
			//will follow is from same stream. As consequence, do forward fault.
			Element::EndOfHeadersInterim(sid) => self.do_forward_fault(sid, None,
				cow!("Received End of interim headers"), false, sink),
			Element::EndOfMessage(sid) => {
				let is_headers = match self.streams.get(&sid.get_sid().into_inner()) {
					Some(entry) => entry.rx_phase_is_headers(),
					None => false
				};
				if is_headers {
					self.do_forward_headers(sid, false, sink, connector)
				} else {
					self.do_forward_trailers(sid, sink)
				}
			},
			Element::HeaderStd(_, n,v) => {
				//Ignore dangerous headers.
				if self.ready_streams.uses_alt_priorities() && n == StandardHttpHeader::Priority {
					self.h2extprio.handle_line(v);
				}
				if !n.is_dangerous_request() { self.headers.set_header_std(n, v) }
				Ok(())
			},
			Element::Header(_, n,v) => {
				//Ignore dangerous headers. Priority use HeaderStd event.
				if !is_dangerous_header_request(n) { self.headers.set_header(n, v); }
				Ok(())
			},
			Element::HeadersFaulty(sid, err) =>
				self.do_forward_fault(sid, None, cow!(F "{err}"), false, sink),
			Element::Method(sid, m) => {
				if let Some(entry) = self.streams.get_mut(&sid.get_sid().into_inner()) {
					entry.mark_as_head(m == b"HEAD")
				}
				self.headers.set_special(ShdrMethod, m);
				Ok(())
			},
			Element::Path(_, p) => Ok(self.headers.set_path(p)),
			Element::Protocol(_, p) => Ok(self.headers.set_special(ShdrProtocol, p)),
			Element::Query(_, q) => Ok(self.headers.set_special(ShdrQuery, q)),
			Element::Scheme(_, s) => Ok(self.headers.set_special(ShdrScheme, s)),
			Element::StartOfMessage(sid) => self.do_start_message(sid),
			Element::TrailerStd(_, n,v) => {
				//Ignore dangerous headers. And set trailers as headers.
				if !n.is_dangerous_request() { self.headers.set_header_std(n, v); }
				Ok(())
			},
			Element::Trailer(_, n,v) => {
				//Ignore dangerous headers. And set trailers as headers.
				if !is_dangerous_header_request(n) { self.headers.set_header(n, v); }
				Ok(())
			},
			Element::Version(_, v) => Ok(self.headers.set_special(ShdrVersion, v)),
			//Stuff only used in responses.
			Element::Reason(sid, _)|Element::Status(sid, _) => self.do_forward_fault(sid, None,
				cow!("Received response headers in request"), false, sink),
			Element::ReapStream(sid) => {
				self.streams.remove(&sid);
				self.countdown.remove(&sid);
				self.blocked_steams.remove(&sid);
				Ok(())
			},
			//We need to ACK settings completed and ping.
			Element::SettingsComplete(ack) => ack.ack(sink).map_err(|err|{
				match err.extract_io_error() {
					Ok(err) => SinkError::WriteFailed(DfltIRWError::new(err)),
					Err(err) => SinkError::SAckError(err.to_string()),
				}
			}),
			Element::Ping(ack, _) => ack.ack(sink).map_err(|err|{
				match err.extract_io_error() {
					Ok(err) => SinkError::WriteFailed(DfltIRWError::new(err)),
					Err(err) => SinkError::PAckError(err.to_string()),
				}
			}),
			Element::Repriorize(nsid, _, _, weight) => {
				if !self.ready_streams.uses_alt_priorities() {
					self.ready_streams.reprioritize_by_sid(nsid, weight);
				}
				Ok(())
			},
			//Extensible priorities repriorization.
			//The spec requires connection error if stream id is not 0. However, this code is past
			//stream handoff, and thus can not raise connection errors UNDER ANY CIRCUMSTANCES.
			Element::Unknown(0, PRIORITY_UPDATE, _, p) => if self.ready_streams.uses_alt_priorities() {
				//Priorizing idle stream is supposed to quasi-open it? I do not think there is
				//any sane way to handle it here, so just ignore such requests.
				if let Some((sid, prio)) = H2ExtPriority::decode_packet(p) {
					let prio2 = alt_prio(prio.priority, prio.incremental);
					self.ready_streams.reprioritize_by_sid(sid, prio2);
				}
				Ok(())
			} else {
				Ok(())	//Just ignore the priority when not in alt mode.
			},
			Element::Unknown(_, _, _, _) => Ok(()),		//Not interesting.
			Element::Goaway(code, msg) =>  if code > 0 {
				increment_metric!(error_in self.g);
				increment_metric!(error_goaway self.g);
				//Print the message, since it might not be reliably printed.
				//This is connection-specific.
				log!(WARNING "Received HTTP/2 error GOAWAY code={code} msg='{msg}'",
					msg=SafeShowByteString(msg));
				Err(SinkError::Goaway(code, msg.to_owned()))
			} else {
				Ok(())
			},
			//We are not interested in stream resets, because we get reap events for those anyway.
			Element::SentReset(_, _) => Ok(()),
			Element::ReceivedReset(_, _) => Ok(()),
			//We are not interested of setting events, setting acks and pongs.
			Element::Pong(_) => Ok(()),
			//Extensible priorities.
			//The spec says to treat any value other than 0 or 1 as connection error. However, this
			//code is past stream handoff and can not raise connection errors UNDER ANY CIRCUMSTANCES.
			Element::Setting(SETTINGS_NO_RFC7540_PRIORITIES, 1) =>
				Ok(self.ready_streams.enable_alt_mode()),
			Element::Setting(_, _) => Ok(()),
			Element::SettingsAck => Ok(()),
			//Raw path/authority.
			Element::PathRaw(_, p) => Ok(self.headers.set_special(ShdrPathRaw, p)),
			Element::AuthorityRaw(_, p) => Ok(self.headers.set_special(ShdrAuthorityRaw, p)),
			//Tracing.
			Element::TraceHeaderFragment(sid, fragment, soh, eoh) =>
				Ok(do_trace_header_fragment(sid, fragment, soh, eoh)),
			Element::TraceHeader(sid, name, value) =>
				Ok(do_trace_header(sid, name, value)),
			_ => Ok(()),	//Ignore unknown events.
		};
		//If error happened, set abort reason. This also sets the aborted flag.
		if let Err(e) = r { sink.abort(e); }
	}
}

struct ReapSinkWrapper<'a>(&'a mut Sink, u32);

impl<'a> SendEvent for ReapSinkWrapper<'a>
{
	fn reaped(&mut self) {  self.0.reap_if(self.1, true); }
	fn aborted(&mut self, _: u32) {}
}

struct SinkWrapper<'a,'b:'a,W:WriterM>(&'a mut Sink, &'a mut Connector<'b>, &'a mut W);

impl<'a,'b:'a,W:WriterM> ElementSink for SinkWrapper<'a,'b,W>
{
	fn sink<'h,'g:'h>(&mut self, elem: Element<'h,'g>) { self.0.sink(elem, self.2, self.1) }
	fn protocol_supported(&self, protocol: &[u8]) -> bool
	{
		//The only supported protocol is websocket.
		protocol == b"websocket"
	}
}

struct RequestWrite<'a,W:WriterM>
{
	writer: &'a mut W,
	h2conn: &'a mut HttpConnection,
	ready_streams: &'a mut ReadyStreamTable,
	reap: ReapMonitor,
	sid: u32,
	maxamount: usize,
	nontrivial: bool,
	error: Option<BackwardInterfaceError>,
}

impl<'a,W:WriterM> RequestWrite<'a,W>
{
	//Returns true if data was sent, false if not.
	fn _write_backward_data(&mut self, block: &[u8]) -> Result<Option<usize>, BackwardInterfaceError>
	{
		let sid = match StreamIdNonZero::new(self.sid) {
			Some(x) => x,
			None => {
				//This is connection-specific.
				log!(ERROR "Backward data on invalid stream {sid}", sid=self.sid);
				return Ok(None);
			}
		};
		//Cap the write size to leave emergency space, and to respect maxamount. Maxsize has nothing to do
		//with block length, so it can be smaller or bigger!
		let maxsize = min(self.writer.freespace(BUFFER_EMERG_SPACE), self.maxamount);
		let block = &block[..min(maxsize, block.len())];
		//Return None to dissociate if this points to a nonexistent stream.
		let r = {
			let ref mut sink = self.writer;
			let reap = &mut self.reap;
			self.h2conn.stream_handle(sid, Ok(false), |mut strm|{
				if block.len() > 0 {
					strm.send_data_frame(*sink, reap, false, NO_PADDING, block)
				} else {
					Ok(true)
				}
			})
		};
		let status = match r {
			Ok(false) => None,
			Ok(true) => Some(block.len()),
			Err(x) => match x.extract_io_error() {
				Ok(e) => fail!(BackwardInterfaceError::WriteFailed(e.to_string())),
				Err(e) => fail!(BackwardInterfaceError::DataSendError(e.to_string())),
			}
		};
		self.nontrivial |= block.len() > 0;
		Ok(status)
	}
	fn _write_backward_trailers<H:for<'c> HeaderBlockTrait<'c>>(&mut self, block: &mut H) ->
		Result<(), BackwardInterfaceError>
	{
		let sid = match StreamIdNonZero::new(self.sid) {
			Some(x) => x,
			//This is connection-specific.
			None => return Ok(log!(ERROR "Backward trailers on invalid stream {sid}", sid=self.sid)),
		};
		let mut buf = [MaybeUninit::uninit();MAX_HBLOCK_SIZE];
		let ref mut sink = self.writer;
		let reap = &mut self.reap;
		let res = match serialize_trailers::<HeaderEncoder,_>(&mut buf, block) {
			//If there are nontrivial headers, send them as EOS headers. Ignore if stream does
			//not exist.
			Ok(h) if h.len() > 0 => {
				let send_headers_frame = |mut strm: RespondWith|{
					strm.send_headers_frame(*sink, reap, true, true, NO_PADDING, None, h)
				};
				self.h2conn.stream_handle(sid, Ok(false), send_headers_frame)
			},
			//Otherwise send empty DATA frame with EOS flag, since empty headers are not allowed.
			//Again, ignore if stream does not exist.
			Ok(_) => {
				let send_data_frame = |mut strm: RespondWith|{
					strm.send_data_frame(*sink, reap, true, NO_PADDING, &[])
				};
				self.h2conn.stream_handle(sid, Ok(false), send_data_frame)
			},
			Err(err) => {
				//If this is trailers that failed, just send end of stream as DATA, since
				//empty HEADERS are not allowed. This is connection-specific.
				log!(ERROR "Sending backward trailers failed: {err}");
				let send_data_frame = |mut strm: RespondWith|{
					strm.send_data_frame(*sink, reap, true, NO_PADDING, &[])
				};
				self.h2conn.stream_handle(sid, Ok(false), send_data_frame)
			}
		};
		match res {
			Ok(_) => (),
			Err(ref err) if err.stream_not_open() => (),	//Ignore.
			//StreamOpenFailed can not happen, since it can only happen if we try to open a stream, but
			//this stream has been opened by the peer.
			Err(err) => match err.extract_io_error() {
				Ok(err) => fail!(BackwardInterfaceError::WriteFailed(err.to_string())),
				Err(err) => fail!(BackwardInterfaceError::HeadersSendError(err.to_string())),
			}
		};
		//Trailers always count as nontrivial progress.
		self.nontrivial = true;
		Ok(())
	}
}

impl<'a,W:WriterM> RequestWriteReturn for RequestWrite<'a,W>
{
	fn max_amount(&self) -> usize { self.maxamount }
	//Returns false if secondary should detach.
	fn write_backward_data(&mut self, block: &[u8], underflow: bool) -> Option<usize>
	{
		let written = self._write_backward_data(block).map_err(|e|self.error = Some(e)).unwrap_or(None);
		//If !underflow was specified, or not everything was written, readd the stream to list of
		//ready streams.
		if !underflow || written.unwrap_or(block.len()) < block.len() {
			self.ready_streams.insert_by_sid(self.sid);
		}
		written
	}
	fn write_backward_trailers(&mut self, t: &mut OwnedTrailerBlock)
	{
		self._write_backward_trailers(t).map_err(|e|self.error = Some(e)).ok();
	}
}

struct H2ExtPriority
{
	priority: Option<u8>,
	incremental: bool,
}

impl H2ExtPriority
{
	fn new() -> H2ExtPriority
	{
		H2ExtPriority {
			priority: None,
			incremental: false,
		}
	}
	fn handle_line(&mut self, line: &[u8])
	{
		if let Ok(line) = from_utf8(line) {
			for part in line.split(',') {
				let part = part.trim();
				self.incremental |= part == "i" || part == "I";
				if let Some(p) = part.strip_prefix("u=").or_else(||part.strip_prefix("U=")) {
					if let Ok(p) = u8::from_str(p) { self.priority=Some(p&127); }
				}
			}
		}
	}
	fn decode_packet(pkt: &[u8]) -> Option<(u32, H2ExtPriority)>
	{
		let (head, tail) = split_at(pkt, 4).ok()?;
		let mut sid = [0;4];
		sid.copy_from_slice(head);
		let sid = u32::from_be_bytes(sid) & 0x7FFFFFFF;
		let mut p = H2ExtPriority::new();
		p.handle_line(tail);
		Some((sid, p))
	}
}


pub struct Connection
{
	conn: HttpConnection,
	sink: Sink,
	settings_sent: bool,
	recv_quit: u8,
	pending_error: Option<Cow<'static, str>>,
	dead_backends: Vec<(u32, bool)>,
	_conntrack: IncomingConnectionState,
}

impl Connection
{
	pub fn new(client_info: ClientInfoIn, mut conntrack: IncomingConnectionState, g: ConfigT,
		trace_flag: bool, max_streams: Option<u32>) -> Connection
	{
		conntrack.set_h2();
		let mut conn = HttpConnection::new(true);
		conn.set_use_std_flag();	//Use standard headers.
		let mut r = Connection {
			conn: conn,
			sink: Sink {
				headers: HeaderBlock::new(),
				streams: HashMap::new(),
				countdown: HashMap::new(),
				client_info: client_info,
				blocked_steams: HashSet::new(),
				ready_streams: ReadyStreamTable::new(),
				maxstream: 0,
				h2extprio: H2ExtPriority::new(),
				g: g,
			},
			settings_sent: false,
			recv_quit: 0,
			pending_error: None,
			dead_backends: Vec::new(),
			_conntrack: conntrack,
		};
		//If desired, set maximum stream count.
		if let Some(max_streams) = max_streams { r.conn.set_max_streams(max_streams); }
		if trace_flag { r.conn.set_trace_flag(true); }
		r
	}
	pub fn get_active_slaves(&self) -> Vec<CID>
	{
		let mut l = Vec::new();
		for (_, s) in self.sink.streams.iter() {
			let cid = s.get_slave_cid();
			if !cid.is_invalid() { l.push(cid); }
		}
		l
	}
	pub fn need_read(&self) -> bool
	{
		//The only reason why not to read the connection is that the connection is blocked, that is,
		//there exists at least one blocked stream. Also if wait send error is set, don't read.
		self.sink.blocked_steams.len() == 0 && self.pending_error.is_none()
	}
	pub fn backward_has_data(&mut self, mut irq: Interrupter, req: &RequestInfo)
	{
		//Add the stream to list of streams waiting, and then issue interrupt. The interrupt is needed in
		//case backend only supports pulling and issues its ready indication while our buffers are empty.
		self.sink.ready_streams.insert_by_sid(req.sid);
		irq.interrupt();
	}
	pub fn forward_unblock(&mut self, mut irq: Interrupter, req: &RequestInfo)
	{
		//Unblock the specified stream. If all streams get unblocked, fire interrupt to ensure that the
		//events get rechecked.
		self.sink.blocked_steams.remove(&req.sid);
		if self.need_read() { irq.interrupt(); }
	}
	pub fn is_idle(&self) -> bool
	{
		self.sink.streams.is_empty()
	}
	pub fn send_idle_goaway(&mut self, w: &mut impl WriterM, quit: bool) -> Result<(), Cow<'static, str>>
	{
		//Send a GOAWAY.
		increment_metric!(error_goaway self.sink.g);
		let strmid = StreamId::new(self.sink.maxstream).unwrap_or(StreamId::zero());
		let errcode = 0;
		let err = if quit { "Server going away" } else { "Closing idle connection" };
		//This is connection-specific.
		log!(WARNING "Sending HTTP/2 error GOAWAY code={errcode} msg='{err}'");
		self.conn.send_goaway_frame(w, strmid, errcode, &err).ok();
		Ok(())
	}
	pub fn handle_request_event(&mut self, w: &mut impl WriterM) -> Result<(), Cow<'static, str>>
	{
		//Handle possible quit event.
		if self.recv_quit > 0 {
			//Only send GOAWAY once.
			if self.recv_quit == 1 {
				self.send_idle_goaway(w, true)?;
				self.recv_quit = 2;
			}
			//If there are no more streams, kill the connection.
			fail_if!(self.is_idle(), cow!("{qk}", qk=BackwardInterfaceError::QuitKill));
		}
		//Try sending SETTINGS (first event only).
		self.send_settings(w)?;
		//Try clearing requests that have lost their backends.
		while let Some((nsid, connected)) = self.dead_backends.pop() {
			let sid = match StreamIdNonZero::new(nsid) { Some(x) => x, None => continue };
			let (code, errmsg) = if connected {
				(BAD_GATEWAY, CONNECTION_LOST)
			} else {
				(GATEWAY_TIMEOUT, CONNECT_ERROR)
			};
			//Just ignore if the stream does not exist. If it does, no need to send forward error,
			//because slave connection has gone down. However, send error headers or tear down the
			//stream. The explicit type for ignore_failure is to assist type inference.
			let ignore_failure: Result<(), DfltIRWError> = Ok(());
			if let Some(strm) = self.sink.streams.get_mut(&nsid) {
				self.conn.stream_handle(sid, ignore_failure, |mut strmh|{
					let mut sink = RWriteWrapper2(w, &mut strmh, ReapMonitor(false));
					//Assume url is available anyway.
					if !strm.send_internal_response(&mut sink,
						&InternalResponse::err(code, errmsg) , None)? {
						sink.error_forward_failure(errmsg)?
					}
					Ok(())	//Ok, sent error headers or dumped stream.
				}).map_err(|err|{
					cow!(F "{err}")
				})?;
			}
		}
		//Some sources only support pulling, so this is needed in order to start the process in case
		//some backend requests pulling while our buffers are empty. In case write buffer is not empty,
		//we will eventually get call to handle_write_underflow(), which will also attempt pulling.
		if w.is_empty() { self.handle_write_underflow(w)?; }
		Ok(())
	}
	pub fn notify_quit(&mut self, mut irq: Interrupter)
	{
		self.recv_quit = 1;
		irq.interrupt();
	}
	pub fn notify_died(&mut self, slave: CID, sid: u32, mut irq: Interrupter, connected: bool)
	{
		//Verify the source cid. Ignore event if the source CID does not match.
		if self.sink.streams.get(&sid).map(|t|{
			slave.is_invalid() || slave != t.get_slave_cid()
		}).unwrap_or(true) { return; }
		//Add to list of dead backends, and fire interrupt.
		self.dead_backends.push((sid, connected));
		irq.interrupt();
	}
	fn try_pull_slave(&mut self, w: &mut impl WriterM, nsid: u32) -> Result<bool, Cow<'static, str>>
	{
		let sid = match StreamIdNonZero::new(nsid) { Some(x) => x, None => return Ok(false) };
		//What is the largest amount that can be written?
		let maxamt = self.conn.get_max_data_on_stream(sid);
		let maxamt = min(maxamt, 16384);
		if maxamt == 0 { return Ok(false); }	//This stream can not make progress.
		let (r, nontrivial) = {
			let (r, reap, nontrivial, error) = {
				let mut wreq = RequestWrite {
					writer: w,
					h2conn: &mut self.conn,
					ready_streams: &mut self.sink.ready_streams,
					reap: ReapMonitor(false),
					sid: nsid,
					maxamount: maxamt,
					nontrivial: false,
					error: None,
				};
				let r = self.sink.streams.get_mut(&nsid).map(|x|x.request_backward_data(&mut wreq)).
					unwrap_or(false);
				(r, wreq.reap.0, wreq.nontrivial, wreq.error)
			};
			//If operation failed, disconnect the connection with the saved error. Note that this does
			//not happen if the request points to nonexistent connection. In that case r=false instead.
			if let Some(err) = error { fail!(cow!(F "{err}")); }
			self.sink.reap_if(nsid, reap);	//Reap the stream if needed.
			(r, nontrivial)
		};
		if !r {
			//This is very probably after headers have been received, so do not even try to send
			//error headers. Instead, immediately send RST_STREAM.
			self.conn.stream_handle(sid, Ok(()), |mut strmh|{
				strmh.send_rst_stream_frame(w, &mut ReapMonitor(false), 2).map_err(|err|{
					cow!(F "Error sending stream reset: {err}")
				}).map(|_|())
			})?;
			self.sink.reap_if(nsid, true);	//The stream is now gone.
		}
		//This is true if nontrivial write happened, else fals.e
		Ok(nontrivial)
	}
	pub fn handle_write_underflow(&mut self, w: &mut impl WriterM) -> Result<(), Cow<'static, str>>
	{
		//The write buffer is now empty.
		self.check_pending_error(w)?;
		let mut failed_to_make_progress = Vec::new();
		//Try pulling streams that are ready.
		while w.is_empty() {
			if let Some(sid) = self.sink.ready_streams.pop_highest_priority() {
				if !self.try_pull_slave(w, sid)? {
					failed_to_make_progress.push(sid);
				}
			} else {
				break;
			}
		}
		//Re-insert all the streams that failed to make progress into ready-queue, as they were removed.
		for sid in failed_to_make_progress { self.sink.ready_streams.insert_by_sid(sid); }
		if self.need_quit_irq() { w.interrupt(); }
		Ok(())
	}
	pub fn handle_read_event(&mut self, w: &mut impl ReaderWriterM, c: &mut Connector) ->
		Result<(), Cow<'static, str>>
	{
		self.send_settings(w.as_writer())?;
		let r = {
			let (source, sink) = w.split();
			self.conn.receive(source, &mut SinkWrapper(&mut self.sink, c, sink))
		};
		match r {
			Ok(true) => (),
			//Immediately tear down the connection, as it is supposed to be at idle.
			Ok(false) => fail!(cow!("Normal close")),
			Err(IoOrStreamError::Io(err)) => fail!(cow!(F "Read Error: {err}")),
			Err(IoOrStreamError::Stream(err)) => {
				let strmid = StreamId::new(self.sink.maxstream).unwrap_or(StreamId::zero());
				let error = cow!(F "Fatal HTTP/2 exception: {err}");
				let errcode = match err.error_code() {
					Some(c) => c,
					None => fail!(error)	//Close immediately.
				};
				//Send a GOAWAY. On success, wait for error to be sent (set pending_error).
				//If this fails, just close the connection immedately. This is connection-
				//specific.
				increment_metric!(error_goaway self.sink.g);
				log!(WARNING "Sending HTTP/2 error GOAWAY code={errcode} msg='{err}'");
				if self.conn.send_goaway_frame(w.as_writer(), strmid, errcode, &err).is_ok() {
					self.pending_error = Some(error);
				} else {
					fail!(error);
				}
			}
		}
		//Give more credit to streams that need that.
		let gcamt = WindowIncrement::new(GIVE_CREDIT_AMOUNT).ok_or_else(||{
			cow!("Internal error: GIVE_CREDIT_AMOUNT not valid")
		})?;
		for (sid, _) in self.sink.streams.iter() {
			let sid = match StreamId::new(*sid) { Some(x) => x, None => continue };
			if let Some(credit) = self.conn.get_stream_rx_credit(sid) {
				if credit < GIVE_CREDIT_THRESHOLD {
					//println!("Giving {GIVE_CREDIT_AMOUNT} RX credits on stream {sid}");
					self.conn.send_window_update_frame(w.as_writer(), sid, gcamt).map_err(|err|{
						cow!(F "Failed to send window update: {err}")
					})?;
				}
			}
		}
		//Give more credit to connection if needed.
		if self.conn.get_stream_rx_credit(StreamId::zero()).map(|x|{
			x < GIVE_CREDIT_THRESHOLD
		}).unwrap_or(false) {
			//println!("Giving {GIVE_CREDIT_AMOUNT} RX credits on connection");
			self.conn.send_window_update_frame(w.as_writer(), StreamId::zero(), gcamt).map_err(|err|{
				cow!(F "Failed to send window update: {err}")
			})?;
		}

		//The last stream might have gotten closed.
		if self.need_quit_irq() { w.as_writer().interrupt(); }
		//The pending_error may have been set.
		self.check_pending_error(w.as_writer())?;
		//If send buffer is empty, do underflow, as window changes may have unblocked something.
		if w.as_writer().is_empty() { self.handle_write_underflow(w.as_writer())?; }
		//This does not need interrupt if unblocked, because the primary connection gets its eventset
		//is recalculated anyway.
		Ok(())
	}
	//Check for pending error. This should be called if any of the following happens:
	//
	//1) pending_error gets set.
	//2) write buffer is empty.
	//
	//It is not necressary to establish that either has happened, just that either may have happened.
	fn check_pending_error(&mut self, sink: &mut impl WriterM) -> Result<(), Cow<'static, str>>
	{
		if sink.is_empty() {
			match self.pending_error.as_ref() {
				Some(err) => fail!(err.clone()),
				None => (),
			}
		}
		Ok(())
	}
	//Send settings. This should be called from the first requested or read event, so it goes out the first
	//thing in the connection. It is safe to call multiple times, it does nothing on subsequent iterations.
	fn send_settings(&mut self, w: &mut impl WriterM) -> Result<(), Cow<'static, str>>
	{
		//In case settings have not been sent yet, do that.
		if !self.settings_sent {
			self.settings_sent = true;
			let settings = SettingsHackIter(0, self.conn.get_max_streams());
			self.conn.send_settings_frame(w, settings).map_err(|err|{
				cow!(F "Failed to send SETTINGS: {err}")
			})?;
		}
		Ok(())
	}
	fn need_quit_irq(&self) -> bool
	{
		self.sink.streams.is_empty() && self.recv_quit > 0
	}
	fn do_backward_headers_serialized(&mut self, h: &[u8], sid: StreamIdNonZero, sink: &mut impl WriterM,
		block: &mut HeaderBlock, req: &RequestInfo, backend_conn: ConnId, backend_name: BackendName) ->
		Result<bool, BackwardInterfaceError>
	{
		let status = print_backend_response(block, req, backend_conn, backend_name);
		//The following can reap the stream, if reply is sent should not depend on that.
		let existed = self.sink.streams.contains_key(&req.sid);
		let mut reap = ReapSinkWrapper(&mut self.sink, sid.into_inner());
		let r = self.conn.stream_handle(sid, Ok(false), |mut strm|{
			strm.send_headers_frame(sink, &mut reap, block.is_no_body(), true, NO_PADDING, None, h)
		});
		//Notify that final reply has been sent. Use cached status because the above
		//might reap the stream.
		if existed && status >= 200 { req.fate_backend(); }
		r.map_err(|err|{
			match err.extract_io_error() {
				Ok(err) => BackwardInterfaceError::WriteFailed(err.to_string()),
				Err(err) => BackwardInterfaceError::HeadersSendError(err.to_string()),
			}
		})
	}
	fn do_backward_headers_error(&mut self, err: HeaderError, sid: StreamIdNonZero, sink: &mut impl WriterM,
		req: &RequestInfo) -> Result<bool, BackwardInterfaceError>
	{
		//Treat H2 bad TE specially.
		if err == HeaderError::UnsupportedH2TE {
			log!(WARNING "{url}...Transfer-Encoding not supported", url=req.url);
			syslog!(WARNING "{url}...Transfer-Encoding not supported", url=req.url);
			//Type inference assist.
			let ignore_failure: Result<(), DfltIRWError> = Ok(());
			self.conn.stream_handle(sid, ignore_failure, |mut strmh|{
				//Straight out send abort.
				strmh.send_rst_stream_frame(sink, &mut ReapMonitor(false), 13).
					map_err(|err|{
					DfltIRWError::new(format_args!("Write error: {err} \
						[cause: Unsupported Transfer-Encoding]"))
				})?;
				Ok(())	//Ok, sent error headers or dumped stream.
			}).map_err(|err|{
				BackwardInterfaceError::DfltIRWError(err)
			})?;
		}
		//We are sending headers, so an error header can always be sent. Send a 502
		//failure.
		let errmsg: Cow<'static, str> = cow!(F "Error encoding backward headers: {err}");
		let status = InternalResponse::err(BAD_GATEWAY, errmsg.clone());
		print_internal_response(req, &status, false);
		if let Some(strm) = self.sink.streams.get_mut(&req.sid) {
			//Type inference assist.
			let ignore_failure: Result<(), DfltIRWError> = Ok(());
			self.conn.stream_handle(sid, ignore_failure, |mut strmh|{
				let mut sink = RWriteWrapper2(sink, &mut strmh, ReapMonitor(false));
				//If someone calls write_backward_headers, assumed url is available.
				if !strm.send_internal_response(&mut sink, &status, None)? {
					sink.error_forward_failure(&errmsg)?
				}
				Ok(())	//Ok, sent error headers or dumped stream.
			}).map_err(|err|{
				BackwardInterfaceError::DfltIRWError(err)
			})?;
		}
		Ok(false)
	}
}

//The second argument is number of streams.
struct SettingsHackIter(usize, u32);

impl Iterator for SettingsHackIter
{
	type Item = (u16, u32);
	fn next(&mut self) -> Option<(u16, u32)>
	{
		let builtin = HTTP2_SUPPORTED_SETTINGS;
		let idx = self.0;
		self.0 += 1;
		if idx < builtin.len() {
			let (etype, evalue) = builtin[idx];
			//Substitute setting 3 (stream count) with the correct value.
			let evalue = if etype == 3 { self.1 } else { evalue };
			Some((etype, evalue))
		} else if idx == builtin.len() {
			Some((SETTINGS_NO_RFC7540_PRIORITIES, 1))		//Deprecate priorities.
		} else {
			None
		}
	}
}

impl BackwardInterface for Connection
{
	fn write_backward_headers(&mut self, sink: &mut impl WriterM, block: &mut HeaderBlock,
		req: &RequestInfo, backend_conn: ConnId, backend_name: BackendName) ->
		Result<bool, BackwardInterfaceError>
	{
		let sid = match StreamIdNonZero::new(req.sid) {
			Some(x) => x,
			None => {
				//This is connection-specific.
				log!(ERROR "Backward headers on invalid stream {sid}", sid=req.sid);
				return Ok(false);
			}
		};
		//Alt priority.
		if self.sink.ready_streams.uses_alt_priorities() {
			//Read as HTTP/1.x headers, as this is still backend side.
			let mut prio = H2ExtPriority::new();
			block.for_each_named(StandardHttpHeader::Priority, |value|prio.handle_line(value));
			//249 gives the default priority of progressive 3.
			let cprio = 255 - self.sink.ready_streams.get_priority_for_sid(req.sid).unwrap_or(249);
			let nurg = prio.priority.unwrap_or(cprio/2);
			let ninc = prio.incremental || cprio % 2 != 0;
			self.sink.ready_streams.reprioritize_by_sid(req.sid, alt_prio(Some(nurg), ninc));
		}
		block.transform_h1_to_h2();
		//Notify transaction about status code, so FLAG_100_CONTINUE can be cleared.
		if let Some(strm) = self.sink.streams.get_mut(&req.sid) {
			strm.notify_status_code(block.get_status());
		}

		let mut buf = [MaybeUninit::uninit();MAX_HBLOCK_SIZE];
		let status = match serialize_headers::<HeaderEncoder>(&mut buf, block, HeaderKind::Response) {
			//If there are nontrivial headers, send them, using whatever the status of trailer flag is
			//as EOS flag, of if no_body is set. Return false to dissociate if this points to
			//nonexistent stream.
			Ok(h) if h.len() > 0 => self.do_backward_headers_serialized(h, sid, sink, block, req,
				backend_conn, backend_name)?,
			//Otherwise, this is illegal, as non-trailer headers should always be nontrivial.
			Ok(_) => fail!(BackwardInterfaceError::EmptyHeaderBlock),
			Err(err) => self.do_backward_headers_error(err, sid, sink, req)?,
		};
		//This is not the primary connection, so recalculate polling status in case of block/unblock.
		if self.need_read() || self.need_quit_irq() { sink.interrupt(); }
		Ok(status)
	}
	fn write_backward_data(&mut self, sink: &mut impl WriterM, block: &[u8], req: &RequestInfo) ->
		Result<Option<usize>, BackwardInterfaceError>
	{
		let sid = match StreamIdNonZero::new(req.sid) {
			Some(x) => x,
			None => {
				//This is connection-specific.
				log!(ERROR "Backward data on invalid stream {sid}", sid=req.sid);
				return Ok(None);
			}
		};
		let maxamt = self.conn.get_max_data_on_stream(sid);
		let maxamt = min(maxamt, 16384);
		let (status, reap) = {
			let mut wreq = RequestWrite {
				writer: sink,
				h2conn: &mut self.conn,
				ready_streams: &mut self.sink.ready_streams,
				reap: ReapMonitor(false),
				sid: req.sid,
				nontrivial: false,
				maxamount: maxamt,	//The size is limited elswhere.
				error: None,
			};
			let status = wreq._write_backward_data(block)?;
			(status, wreq.reap.0)
		};
		//If needed, reap the stream, and interrupt to start reading again, in case this stream was blocked.
		self.sink.reap_if(req.sid, reap);	//Reap the stream if needed.
		if self.need_read() || self.need_quit_irq()  { sink.interrupt(); }
		Ok(status)
	}
	fn write_backward_trailers(&mut self, sink: &mut impl WriterM, block: &mut HeaderBlock,
		req: &RequestInfo) -> Result<(), BackwardInterfaceError>
	{
		let reap = {
			let mut wreq = RequestWrite {
				writer: sink,
				h2conn: &mut self.conn,
				ready_streams: &mut self.sink.ready_streams,
				reap: ReapMonitor(false),
				sid: req.sid,
				nontrivial: false,
				maxamount: USIZE_MAX,	//The size is limited elswhere.
				error: None,
			};
			wreq._write_backward_trailers(block)?;
			wreq.reap.0
		};
		//If needed, reap the stream, and interrupt to start reading again, in case this stream was blocked.
		self.sink.reap_if(req.sid, reap);
		if self.need_read() || self.need_quit_irq()  { sink.interrupt(); }
		Ok(())
	}
	fn write_backward_error(&mut self, sink: &mut impl WriterM, req: &RequestInfo,
		status: InternalResponse) -> Result<(), BackwardInterfaceError>
	{
		let sid = match StreamIdNonZero::new(req.sid) {
			Some(x) => x,
			None => {
				//This is connection-specific.
				log!(ERROR "Backward error on invalid stream {sid}", sid=req.sid);
				return Ok(());
			}
		};
		if let Some(strm) = self.sink.streams.get_mut(&req.sid) {
			let ignore_failure: Result<(), DfltIRWError> = Ok(());
			self.conn.stream_handle(sid, ignore_failure, |mut strmh|{
				let mut sink = RWriteWrapper2(sink, &mut strmh, ReapMonitor(false));
				//If someone calls write_backward_error, assume that URL data is available.
				if !strm.send_internal_response(&mut sink, &status, None)? {
					sink.error_forward_failure(status)?
				}
				Ok(())	//Ok, sent error headers or dumped stream.
			}).map_err(|err|{
				BackwardInterfaceError::DfltIRWError(err)
			})?;
		}
		//This is not primary connection, so we need to interrupt if unblocked.
		if self.need_read() || self.need_quit_irq() { sink.interrupt(); }
		Ok(())
	}
}
