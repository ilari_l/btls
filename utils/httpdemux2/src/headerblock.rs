use crate::UninitializedSink;
use crate::bitmask::Bitfield;
use btls_aux_http::http::EncodeHeaders;
use btls_aux_http::http::StandardHttpHeader;
use btls_aux_http::status::get_default_reason;
use btls_aux_memory::PrintBuffer;
use btls_aux_memory::trim_ascii;
use btls_aux_memory::SafeShowByteString;
use std::fmt::Debug;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::fmt::Write as FmtWrite;
use std::mem::MaybeUninit;
use std::ops::Deref;
use std::ops::Range;

mod range;
mod storage;
pub(crate) use self::range::*;
pub(crate) use self::storage::*;

static CHUNKED: &'static [u8] = b"chunked";
static CLOSE: &'static [u8] = b"close";
const UPGRADE: &'static [u8] = b"upgrade";
const CONNECT: &'static [u8] = b"CONNECT";
const WEBSOCKET: &'static [u8] = b"websocket";


macro_rules! def_specialshdr
{
	(IMPL $name:ident,$field:ident) => {
		impl SpecialStringHeader for $name
		{
			fn access<'a>(hb: &'a mut SpecialHeaders) -> &'a mut SliceRange { &mut hb.$field }
			fn access_ro<'a>(hb: &'a SpecialHeaders) -> &'a SliceRange { &hb.$field }
		}
	};
	(PUBLIC $name:ident,$field:ident) => {
		pub struct $name;
		def_specialshdr!(IMPL $name,$field);
	};
	(PRIVATE $name:ident,$field:ident) => {
		struct $name;
		def_specialshdr!(IMPL $name,$field);
	};
}

pub trait SpecialStringHeader
{
	fn access<'a>(hb: &'a mut SpecialHeaders) -> &'a mut SliceRange;
	fn access_ro<'a>(hb: &'a SpecialHeaders) -> &'a SliceRange;
}

def_specialshdr!(PUBLIC ShdrAuthorityO, p_authority_orig);
def_specialshdr!(PUBLIC ShdrAuthorityT, p_authority_trans);
def_specialshdr!(PUBLIC ShdrContentType, content_type);
def_specialshdr!(PUBLIC ShdrIpaddr, x_forwarded_for);
def_specialshdr!(PUBLIC ShdrMethod, p_method);
def_specialshdr!(PUBLIC ShdrPathO, p_path_orig);
def_specialshdr!(PUBLIC ShdrPathT, p_path_trans);
def_specialshdr!(PUBLIC ShdrProtocol, p_protocol);
def_specialshdr!(PUBLIC ShdrQuery, p_query);
def_specialshdr!(PUBLIC ShdrReason, p_reason);
def_specialshdr!(PUBLIC ShdrScheme, p_scheme);
def_specialshdr!(PUBLIC ShdrVersion, p_version);
def_specialshdr!(PUBLIC ShdrClientCn, x_forwarded_client_cn);
def_specialshdr!(PUBLIC ShdrClientSpkihash, x_forwarded_client_spkihash);
def_specialshdr!(PUBLIC ShdrPathRaw, p_path_raw);
def_specialshdr!(PUBLIC ShdrAuthorityRaw, p_authority_raw);
def_specialshdr!(PUBLIC ShdrLocation, p_location);
def_specialshdr!(PUBLIC ShdrUserAgent, p_user_agent);
def_specialshdr!(PRIVATE ShdrWebsocketAccept, sec_websocket_accept);
def_specialshdr!(PRIVATE ShdrWebsocketKey, sec_websocket_key);
def_specialshdr!(PRIVATE ShdrContentLength, content_length);

#[derive(Clone,Debug,PartialEq,Eq)]
pub enum HeaderError
{
	///Illegal Path.
	IllegalPath,
	///Illegal Authority.
	IllegalAuthority,
	///Illegal asterisk not OPTIONS.
	IllegalAsteriskNotOptions,
	///Illegal asterisk with query.
	IllegalAsteriskQuery,
	///Illegal CONNECT method.
	IllegalConnect,
	///Illegal HTTP/2 upgrade.
	IllegalH2Upgrade,
	///Illegal insecure access.
	IllegalInsecure,
	///Illegal non-last chunked.
	IllegalNonLastChunked,
	///Illegal scheme.
	IllegalScheme,
	///Illegal status.
	IllegalStatus(u16),
	///Internal error: Can not serialize content-length.
	IntBadContentLength,
	///Internal error: Bad pointer.
	IntBadPointer,
	///Internal error: Should not have faulted.
	IntNoFault,
	//Serialization error.
	SerializeError,
	///Too large headers.
	TooLargeHeaders,
	///Too many headers.
	TooManyHeaders,
	///Unsupported HTTP/2 transfer-encoding.
	UnsupportedH2TE,
}

impl Display for HeaderError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::HeaderError::*;
		match self {
			&IllegalPath => f.write_str("Illegal path"),
			&IllegalAuthority => f.write_str("Illegal authority"),
			&IllegalAsteriskNotOptions => f.write_str("Illegal :path=* with :method!=OPTIONS"),
			&IllegalAsteriskQuery => f.write_str("Illegal :path=* with query"),
			&IllegalConnect => f.write_str("CONNECT method not allowed"),
			&IllegalH2Upgrade => f.write_str("HTTP/2 :protocol used with :method!=CONNECT"),
			&IllegalInsecure => f.write_str("Illegal insecure access"),
			&IllegalNonLastChunked => f.write_str("Chunked is only allowed as last Transfer-Encoding"),
			&IllegalScheme => f.write_str("Unexpected scheme in request"),
			&IllegalStatus(status) => write!(f, "Illegal status code: {status}"),
			&IntBadContentLength => f.write_str("Internal error: Bad content-length"),
			&IntBadPointer => f.write_str("Internal error: Bad pointer"),
			&IntNoFault => f.write_str("Internal error: No fault when handling fault"),
			&SerializeError => f.write_str("Serialization error"),
			&TooLargeHeaders => f.write_str("Headers too big"),
			&TooManyHeaders => f.write_str("Too many headers"),
			&UnsupportedH2TE => f.write_str("Transfer-Encoding not supported in HTTP/2"),
		}
	}
}

pub trait HeaderBlockTrait<'b>
{
	fn is_overflow(&self) -> Option<HeaderError>;
	fn set_overflow(&mut self, o: HeaderError);
	fn normals(&'b self) -> (&'b [u8], &'b [HeaderRange]);
}

impl<'b> HeaderBlockTrait<'b> for HeaderBlock
{
	fn is_overflow(&self) -> Option<HeaderError> { self.is_overflow() }
	fn set_overflow(&mut self, o: HeaderError) { self.set_overflow(o) }
	fn normals(&'b self) -> (&'b [u8], &'b [HeaderRange])
	{
		(self.table.borrow_raw(), self.table2.borrow_raw())
	}
}

define_bitfield_type!(AttribFlagsTag, u8);
//True if client sent Expect: 100-Continue
define_bitfield_bit!(AflagExp100C, AttribFlagsTag, 0);
//True if request method is not HEAD or GET.
define_bitfield_bit!(AflagUnsafe, AttribFlagsTag, 1);
//True if client requested upgrade.
define_bitfield_bit!(AflagUpgrade, AttribFlagsTag, 2);
//True is request method is HEAD.
define_bitfield_bit!(AflagIsHead, AttribFlagsTag, 3);
//True if request was secure.
define_bitfield_bit!(AflagSecure, AttribFlagsTag, 4);

#[derive(Clone,Debug)]
pub struct RequestAttributes
{
	flags: Bitfield<AttribFlagsTag>,
}

impl RequestAttributes
{
	pub fn is_head(&self) -> bool { self.flags.get(AflagIsHead) }
	pub fn is_unsafe(&self) -> bool { self.flags.get(AflagUnsafe) }
	pub fn is_upgrade(&self) -> bool { self.flags.get(AflagUpgrade) }
	pub fn is_expect_100_continue(&self) -> bool { self.flags.get(AflagExp100C) }
	pub fn is_secure(&self) -> bool { self.flags.get(AflagSecure) }
}

#[derive(Clone)]
pub enum HeaderSourceComponent<'a>
{
	Slice(&'a [u8]),
	Range(Range<usize>),
}

define_bitfield_type!(HeaderFlagsTag, u16);
define_bitfield_bit!(HflagBody, HeaderFlagsTag, 0);
define_bitfield_bit!(HflagChunked, HeaderFlagsTag, 1);
define_bitfield_bit!(HflagNotChunked, HeaderFlagsTag, 2);
define_bitfield_bit!(HflagTe, HeaderFlagsTag, 3);
define_bitfield_bit!(HflagCClose, HeaderFlagsTag, 4);
define_bitfield_bit!(HflagEmitCClose, HeaderFlagsTag, 5);
define_bitfield_bit!(HflagSecure, HeaderFlagsTag, 6);
define_bitfield_bit!(HflagRequest, HeaderFlagsTag, 7);
define_bitfield_bit!(HflagChained, HeaderFlagsTag, 8);
define_bitfield_bit!(HflagReqCert, HeaderFlagsTag, 9);

pub struct SpecialHeaders
{
	content_length: SliceRange,
	content_type: SliceRange,
	p_authority_orig: SliceRange,
	p_authority_trans: SliceRange,
	p_authority_raw: SliceRange,
	p_location: SliceRange,
	p_method: SliceRange,
	p_path_orig: SliceRange,
	p_path_trans: SliceRange,
	p_path_raw: SliceRange,
	p_protocol: SliceRange,
	p_query: SliceRange,
	p_reason: SliceRange,
	p_scheme: SliceRange,
	p_user_agent: SliceRange,
	p_version: SliceRange,
	sec_websocket_accept: SliceRange,
	sec_websocket_key: SliceRange,
	x_forwarded_for: SliceRange,
	x_forwarded_client_cn: SliceRange,
	x_forwarded_client_spkihash: SliceRange,
}

impl Default for SpecialHeaders
{
	fn default() -> SpecialHeaders
	{
		SpecialHeaders {
			content_length: SliceRange::none(),
			content_type: SliceRange::none(),
			p_authority_orig: SliceRange::none(),
			p_authority_trans: SliceRange::none(),
			p_authority_raw: SliceRange::none(),
			p_location: SliceRange::none(),
			p_method: SliceRange::none(),
			p_path_orig: SliceRange::none(),
			p_path_trans: SliceRange::none(),
			p_path_raw: SliceRange::none(),
			p_protocol: SliceRange::none(),
			p_query: SliceRange::none(),
			p_reason: SliceRange::none(),
			p_scheme: SliceRange::none(),
			p_user_agent: SliceRange::none(),
			p_version: SliceRange::none(),
			sec_websocket_accept: SliceRange::none(),
			sec_websocket_key: SliceRange::none(),
			x_forwarded_for: SliceRange::none(),
			x_forwarded_client_cn: SliceRange::none(),
			x_forwarded_client_spkihash: SliceRange::none(),
		}
	}
}

pub struct HeaderBlock
{
	table: HeaderStorage,
	table2: HeaderTable,
	content_length: Option<u64>,
	special: SpecialHeaders,
	status: u16,
	overflow: Option<HeaderError>,
	flags: Bitfield<HeaderFlagsTag>,
}


impl HeaderBlock
{
	pub fn new() -> HeaderBlock
	{
		HeaderBlock {
			table: HeaderStorage::new(),
			table2: HeaderTable::new(),
			content_length: None,
			special: Default::default(),
			status: 0,
			overflow: None,
			flags: Bitfield::new().set(HflagBody),
		}
	}
	pub fn clear(&mut self)
	{
		self.table.clear();
		self.table2.clear();
		self.content_length = None;
		self.special = Default::default();
		self.status = 0;
		self.overflow = None;
		//The chained flag is special: It will not be cleared. This is because header blocks are specific
		//to apertures, and if aperture is chained, it will always be chained.
		let is_chained = self.flags.get(HflagChained);
		self.flags = Bitfield::new().set(HflagBody).force(HflagChained, is_chained);
	}

	pub fn workaround_synapse_bug(&mut self)
	{
		//If content-length is 0, clear content-length, it should not be there.
		if self.content_length == Some(0) {
			self.content_length = None;
			self.special.content_length = SliceRange::none();
		}
	}
	pub fn set_authority(&mut self, v: &[u8])
	{
		self.set_special(ShdrAuthorityO, v);
		//Copy the original authority to translated authority.
		self.special.p_authority_trans = self.special.p_authority_orig;
	}
	pub fn set_path(&mut self, v: &[u8])
	{
		self.set_special(ShdrPathO, v);
		//Copy original path to translated path.
		self.special.p_path_trans = self.special.p_path_orig;
	}
	pub fn set_special<Field:SpecialStringHeader>(&mut self, _: Field, v: &[u8])
	{
		let evalue = f_return!(self.__write_value(v), ());
		*Field::access(&mut self.special) = evalue;
	}
	pub fn clear_special<Field:SpecialStringHeader>(&mut self, _: Field)
	{
		*Field::access(&mut self.special) = SliceRange::none();
	}
	pub fn set_special_multipart<'a,Field:SpecialStringHeader,I:Iterator<Item=HeaderSourceComponent<'a>>>(
		&mut self, _: Field, v: I)
	{
		let evalue = f_return!(self.__write_value_multipart(v), ());
		*Field::access(&mut self.special) = evalue;
	}
	pub fn set_content_length(&mut self, v: u64)
	{
		//Content-length is unreliable if is_nonchunked is set.
		if !self.flags.get(HflagNotChunked) {
			self.content_length = Some(v);
			//Make symbolic version too.
			let mut tmp = [MaybeUninit::uninit();24];
			let mut tmp = PrintBuffer::new(&mut tmp);
			write!(tmp, "{v}").ok();
			let tmp = tmp.into_inner_bytes();
			//We should never see lengths over 20 digits.
			if tmp.len() > 20 {  return self.set_overflow(HeaderError::IntBadContentLength); }
			self.set_special(ShdrContentLength, tmp);
		}
	}
	pub fn set_request(&mut self) { self.flags.set_imp(HflagRequest); }
	pub fn set_chained(&mut self) { self.flags.set_imp(HflagChained); }
	pub fn set_emit_close(&mut self) { self.flags.set_imp(HflagEmitCClose); }
	pub fn set_body(&mut self, body: bool) { self.flags.force_imp(HflagBody, body); }
	pub fn set_req_client_cert(&mut self, stat: bool) { self.flags.force_imp(HflagReqCert, stat); }

	pub fn set_status(&mut self, v: u16) { self.status = v; }
	pub fn set_secure(&mut self, secure: bool) { self.flags.force_imp(HflagSecure, secure); }
	pub fn set_connection_header(&mut self, _: &[u8]) {}	//These can always be dropped.
	pub fn set_connection_header_std(&mut self, n: StandardHttpHeader)
	{
		//The only interesting one is Close. It sets HflagCClose.
		match n {
			StandardHttpHeader::Close => self.flags.set_imp(HflagCClose),
			_ => (),
		}
	}
	pub fn set_header_std(&mut self, n: StandardHttpHeader, v: &[u8])
	{
		//Filter specials.
		match n {
			StandardHttpHeader::Cookie => {
				//If possible, directly concatenate cookies.
				if self._set_cookie_maybe_special(v).is_some() { return; }
			},
			//Connection is special, but that should be reported via set_connection_header*.
			StandardHttpHeader::ContentLength => return,
			StandardHttpHeader::ContentType => return self.set_special(ShdrContentType, v),
			StandardHttpHeader::Location => return self.set_special(ShdrLocation, v),
			StandardHttpHeader::UserAgent => return self.set_special(ShdrUserAgent, v),
			StandardHttpHeader::SecWebsocketAccept => return self.set_special(ShdrWebsocketAccept, v),
			StandardHttpHeader::SecWebsocketKey => return self.set_special(ShdrWebsocketKey, v),
			StandardHttpHeader::SetCookie2 => return,	//Obsolete.
			StandardHttpHeader::Cookie2 => return,		//Obsolete.
			_ => (),
		}
		//Add the header.
		let rvalue = f_return!(self.__write_value(v), ());
		self._set_header_tail(SliceRangeName::standard(n), rvalue);
		//If this is Transfer-Encoding, scan to update the is_chunked and is_nonchunked fields.
		if n == StandardHttpHeader::TransferEncoding {
			self.flags.set_imp(HflagTe);
			for item in v.split(|x|*x==b',') {
				let item = trim_ascii(item);
				let chunked = item == CHUNKED;
				if self.flags.get(HflagChunked) {
					self.set_overflow(HeaderError::IllegalNonLastChunked);
				}
				self.flags.or_imp(HflagChunked, chunked);
				self.flags.or_imp(HflagNotChunked, !chunked);
				//If is_nonchunked got set, the content-length is unreliable.
				if !self.flags.get(HflagNotChunked) {
					self.content_length = None;
					self.special.content_length = SliceRange::none();
				}
			}
		}
	}
	pub fn set_header(&mut self, n: &[u8], v: &[u8])
	{
		let ename = f_return!(self.__write_name(n), ());
		let evalue = f_return!(self.__write_value(v), ());
		self._set_header_tail(ename, evalue)
	}
	pub fn set_header_multipart<'a,I:Iterator<Item=HeaderSourceComponent<'a>>>(&mut self, n: &[u8], v: I)
	{
		let ename = f_return!(self.__write_name(n), ());
		let evalue = f_return!(self.__write_value_multipart(v), ());
		self._set_header_tail(ename, evalue)
	}
	pub fn set_header_multipart_std<'a,I:Iterator<Item=HeaderSourceComponent<'a>>>(&mut self,
		n: StandardHttpHeader, v: I)
	{
		let evalue = f_return!(self.__write_value_multipart(v), ());
		self._set_header_tail(SliceRangeName::standard(n), evalue)
	}
	//Returns Some(()) if cookie was successfully set, None if it was not.
	fn _set_cookie_maybe_special(&mut self, v: &[u8]) -> Option<()>
	{
		static SEPARATOR: &'static [u8] = b"; ";
		//Read last header. If it is not cookie, or its end does not match blockuse (there is something
		//after) do not proceed.
		let header = self.table2.borrow_last_header()?;
		fail_if_none!(!header.is_cookie());
		match self.table.write_append(header.get_value_raw(), SEPARATOR, v) {
			Ok(Some(range)) => {
				header.set_value(range);
				Some(())
			}
			Ok(None) => None,
			//Do not overwrite any previous error.
			Err(err) => {
				if self.overflow.is_none() { self.overflow = Some(err); }
				None
			}
		}
	}
	fn _set_header_tail(&mut self, ename: SliceRangeName, evalue: SliceRange)
	{
		//Don't do this if overflow is set, as there is no guarantees about what crap block contains then.
		if self.overflow.is_none() {
			if !self.table2.push_header(HeaderRange::new(ename, evalue)) {
				return self.overflow = Some(HeaderError::TooManyHeaders)
			}
		}
	}
	fn __write_name(&mut self, n: &[u8]) -> Option<SliceRangeName>
	{
		self.__handle_write_error(|table|table.write_name(n))
	}
	fn __write_value(&mut self, v: &[u8]) -> Option<SliceRange>
	{
		self.__handle_write_error(|table|table.write(v))
	}
	fn __write_value_multipart<'a,I:Iterator<Item=HeaderSourceComponent<'a>>>(&mut self, v: I) ->
		Option<SliceRange>
	{
		self.__handle_write_error(|table|table.write_multipart(v))
	}
	fn __handle_write_error<T,F:FnOnce(&mut HeaderStorage) -> Result<T, HeaderError>>(&mut self, f: F) ->
		Option<T>
	{
		//Do not overwrite any previous error.
		let overflow = &mut self.overflow;
		f(&mut self.table).map_err(|err|{
			if overflow.is_none() { *overflow = Some(err); }
		}).ok()
	}
	pub fn set_overflow(&mut self, o: HeaderError) { if self.overflow.is_none() { self.overflow = Some(o); } }

	pub fn is_secure(&self) -> bool { self.flags.get(HflagSecure) }
	pub fn is_chunked(&self) -> bool { self.flags.get(HflagChunked) }
	pub fn is_connection_close(&self) -> bool { self.flags.get(HflagCClose) }
	pub fn is_chained(&self) -> bool { self.flags.get(HflagChained) }
	pub fn is_transfer_encoding(&self) -> bool { self.flags.get(HflagTe) }
	pub fn is_close(&self) -> bool { self.flags.get(HflagCClose) }
	pub fn is_no_body(&self) -> bool { !self.flags.get(HflagBody) }
	pub fn is_req_client_cert(&mut self) -> bool { self.flags.get(HflagReqCert) }

	pub fn get_special_range<Field:SpecialStringHeader>(&self, _: Field) -> Option<Range<usize>>
	{
		Field::access_ro(&self.special).range()
	}
	pub fn get_special<'a,Field:SpecialStringHeader>(&'a self, _: Field) -> Option<&'a [u8]>
	{
		self.table.get_value2(*Field::access_ro(&self.special))
	}
	pub fn get_content_length(&self) -> Option<u64> { self.content_length }
	pub fn get_status<'a>(&'a self) -> u16 { self.status }

	pub fn get_attributes(&self) -> RequestAttributes
	{
		let method = self.get_special(ShdrMethod).unwrap_or(b"");
		let mut expect_100_continue = false;
		self.for_each_named(StandardHttpHeader::Expect, |value|for item in value.split(|x|*x==b',') {
			let item = trim_ascii(item);
			expect_100_continue |= item == b"100-continue";
		});
		RequestAttributes {
			flags: Bitfield::new().
				force(AflagExp100C, expect_100_continue).
				force(AflagIsHead, method == b"HEAD").
				force(AflagUnsafe, method != b"HEAD" && method != b"GET").
				force(AflagUpgrade, self.get_special_range(ShdrProtocol).is_some()).
				force(AflagSecure, self.is_secure())
		}
	}
	pub fn for_each_normal<'a>(&'a self, mut cb: impl FnMut(MaybeStandardName, &[u8]))
	{
		let (blockdata, headers) = self.normals();
		for header in headers.iter() { if let Some((name, value)) = header.get(blockdata) {
			cb(name, value);
		}}
	}
	pub fn for_each_named<'a>(&'a self, hname: StandardHttpHeader, mut cb: impl FnMut(&[u8]))
	{
		let (blockdata, headers) = self.normals();
		for header in headers.iter() { if let Some((name, value)) = header.get(blockdata) {
			if name.as_standard() == Some(hname) { cb(value); }
		}}
	}

	//This can not delete special headers (ones with set method).
	pub fn delete_matching<F>(&mut self, predicate: F) where F: Fn(MaybeStandardName) -> bool
	{
		self.table2.delete_matching(&self.table, predicate)
	}
	//This can not delete special headers (ones with set method).
	pub fn delete_matching_std<F>(&mut self, predicate: F) where F: Fn(StandardHttpHeader) -> bool
	{
		self.table2.delete_matching_std(predicate);
	}

	pub fn is_overflow(&self) -> Option<HeaderError> { self.overflow.clone() }

	pub(crate) fn _fix_http2_cookies(&mut self)
	{
		let cookie_count = self.table2.count_cookies();
		if cookie_count < 2 { return; }	//Nothing to merge.
		//Swap out all cookies and add a new cookie.
		let cookies = self.table2.collect_remove_cookies(cookie_count);
		self.set_header_multipart_std(StandardHttpHeader::Cookie, CookieIterator(cookies.deref(), 0));
	}
	pub fn transform_h2_to_h1(&mut self, has_body: bool)
	{
		//If there is no protocol and no content-length but body exists, add chunked.
		if !self.flags.get(HflagChunked) && !self.special.p_protocol.is_set() &&
			self.content_length.is_none() && has_body {
			//This should set is_chunked.
			self.set_header_std(StandardHttpHeader::TransferEncoding, CHUNKED);	
		}
		if self.overflow.is_some() { return; }
		if self.special.p_protocol.is_set() && self.get_special(ShdrMethod) != Some(CONNECT) {
			return self.overflow = Some(HeaderError::IllegalH2Upgrade);
		}
		let is_websoket;
		{
			let protocol = self.get_special(ShdrProtocol);
			is_websoket = protocol == Some(WEBSOCKET);
		}
		//If protocol is websockets, add sec-websocket-key and set method to GET.
		if is_websoket {
			self.set_special(ShdrMethod, b"GET");
			self.set_special(ShdrWebsocketKey, b"0000000000000000000000==");
		}
		//Fix cookies.
		self._fix_http2_cookies();
	}
	pub fn transform_h1_to_h2(&mut self)
	{
		if self.overflow.is_some() { return; }
		//Drop sec-websocket-accept. Upgrade is dropped automatically.
		self.special.sec_websocket_accept = SliceRange::none();
		//Only chunked is allowed, and even that is not signaled.
		if self.flags.get(HflagNotChunked) { return self.overflow = Some(HeaderError::UnsupportedH2TE); }
		//Delete headers not allowed in HTTP/2.
		self.delete_matching_std(|x|{
			x==StandardHttpHeader::Connection ||
				x==StandardHttpHeader::ProxyConnection ||
				x==StandardHttpHeader::KeepAlive ||
				x==StandardHttpHeader::TransferEncoding ||
				x==StandardHttpHeader::Upgrade
		});
		self.flags.clear_imp(HflagTe);
		self.flags.clear_imp(HflagChunked);
		//If status is 101, change it to 200, as that is the code to accept upgrade.
		if self.status == 101 { self.status = 200; }
	}
	pub fn get_url(&self, rscheme: Option<&[u8]>, rauthority: Option<&[u8]>, flag: bool) -> String
	{
		//This is used to redirect entiere authorities, so use original path in order to reflect
		//what client sent back in another origin.
		let scheme = rscheme.or_else(||self.get_special(ShdrScheme)).unwrap_or(b"");
		let authority = rauthority.or_else(||self.get_special(ShdrAuthorityO)).unwrap_or(b"");
		let path = self.get_special(ShdrPathO).unwrap_or(b"");
		let query = self.get_special(ShdrQuery).unwrap_or(b"");
		//URLs should never contain characters that SafeShowByteString escapes. And the URL has been already
		//checked for those characters.
		format!("{scheme}://{authority}{path}{query}{http_flag}",
			scheme=SafeShowByteString(scheme), authority=SafeShowByteString(authority),
			path=SafeShowByteString(path), query=SafeShowByteString(query),
			http_flag=if flag { "#http" } else { "" })
	}
	pub fn get_conn_url(&self) -> String
	{
		//This is the requested URL, so always use original authority and path, even if those have been
		//translated.
		let method = self.get_special(ShdrMethod).map(SafeShowByteString);
		let scheme = self.get_special(ShdrScheme).map(SafeShowByteString).
			unwrap_or(SafeShowByteString(b"<unknown>"));
		let authority = self.get_special(ShdrAuthorityO).map(SafeShowByteString).
			unwrap_or(SafeShowByteString(b""));
		let path = self.get_special(ShdrPathO).map(SafeShowByteString);
		let version = self.get_special(ShdrVersion).map(SafeShowByteString);
		//If there is no method, path nor version, the URL is totally malformed.
		if method.is_none() && path.is_none() && version.is_none() {
			return format!("<Malformed request>");
		}
		let method = method.unwrap_or(SafeShowByteString(b"<UNKNOWN>"));
		let path = path.unwrap_or(SafeShowByteString(b"<no path>"));
		let version = version.unwrap_or(SafeShowByteString(b"<unknown version>"));
		//The authority defaults to "", for all those HTTP/1.0 requests with no Host header.
		format!("{method} {scheme}://{authority}{path} {version}")
	}
	fn get_authority<'a>(&'a self, raw: bool) -> Option<&'a [u8]>
	{
		//Used in serializing output header block, so use translated value.
		if raw { self.get_special(ShdrAuthorityRaw) } else { self.get_special(ShdrAuthorityT) }
	}
	fn get_path<'a>(&'a self, raw: bool) -> Option<&'a [u8]>
	{
		//Used in serializing output header block, so use translated value.
		if raw { self.get_special(ShdrPathRaw) } else { self.get_special(ShdrPathT) }
	}
}

struct CookieIterator<'a>(&'a [Range<usize>], usize);

impl<'a> Iterator for CookieIterator<'a>
{
	type Item = HeaderSourceComponent<'static>;
	fn next(&mut self) -> Option<HeaderSourceComponent<'static>>
	{
		if self.1 % 2 == 1 {
			//Separator. Only read if next element exists. +1 changes /2.
			self.1 += 1;
			self.0.get(self.1/2)?;
			return Some(HeaderSourceComponent::Slice(b"; "));
		} else {
			//Range. +1 does not change /2.
			self.1 += 1;
			self.0.get(self.1/2).map(|range|HeaderSourceComponent::Range(range.clone()))
		}
	}
}

pub struct OwnedTrailerBlock
{
	block: Vec<u8>,
	header: Vec<HeaderRange>,
	overflow: Option<HeaderError>,
}

impl OwnedTrailerBlock
{
	pub fn blank() -> OwnedTrailerBlock
	{
		OwnedTrailerBlock {
			block: Vec::new(),
			header: Vec::new(),
			overflow: None,
		}
	}
	pub fn new(block: &HeaderBlock) -> OwnedTrailerBlock
	{
		if let Some(overflow) = block.overflow.as_ref() {
			return OwnedTrailerBlock{
				block: Vec::new(),
				header: Vec::new(),
				overflow: Some(overflow.clone()),
			};
		}
		let mut space = 0;
		let mut hspace = 0;
		block.for_each_normal(|name, value|{
			space += name.extend_len();
			space += value.len();
			hspace += 1;
		});
		let mut b = OwnedTrailerBlock{
			block: Vec::with_capacity(space),
			header: Vec::with_capacity(hspace),
			overflow: None,
		};
		//Trailers can never have specials.
		block.for_each_normal(|name, value|{
			let ename = SliceRangeName::write_extend(&mut b.block, name);
			let evalue = SliceRange::write_extend(&mut b.block, value);
			b.header.push(HeaderRange::new(ename, evalue));
		});
		b
	}
}

impl<'b> HeaderBlockTrait<'b> for OwnedTrailerBlock
{
	fn is_overflow(&self) -> Option<HeaderError> { self.overflow.clone() }
	fn set_overflow(&mut self, o: HeaderError) { self.overflow = Some(o); }
	fn normals(&'b self) -> (&'b [u8], &'b [HeaderRange]) { (&self.block, &self.header) }
}

pub enum HeaderKind
{
	Request(u8),
	Response,
}

pub const SEFLAG_HOST_RAW: u8 = 1;
pub const SEFLAG_PATH_RAW: u8 = 2;

fn serialize_tail<'a,E:EncodeHeaders,H:for<'b> HeaderBlockTrait<'b>>(mut buf: UninitializedSink<'a>,
	block: &mut H, mut error: bool) -> Result<&'a [u8], HeaderError>
{
	let (blockdata, headers) = block.normals();
	for header in headers.iter() { if let Some((name, value)) = header.get(blockdata) {
		let r = match name.try_as_standard() {
			Ok(name) => E::header_std(&mut buf, name, value),
			Err(name) => E::header(&mut buf, name, value),
		};
		error |= r.is_err();
	}}
	error |= E::end(&mut buf).is_err();
	let buf = buf.into_slice();
	if error && block.is_overflow().is_none() { block.set_overflow(HeaderError::SerializeError);}
	//If overflow is set, return that error, otherwise return the serialized buffer.
	match block.is_overflow() { Some(e) => Err(e), None => Ok(buf) }
}

pub fn serialize_trailers<'a,E:EncodeHeaders,H:for<'b> HeaderBlockTrait<'b>>(buf: &'a mut [MaybeUninit<u8>],
	block: &mut H) -> Result<&'a [u8], HeaderError>
{
	//Serialize even if header block is bad, as we will detect the error afterwards.
	let buf = UninitializedSink::new(buf);
	serialize_tail::<E,H>(buf, block, false)
}

macro_rules! special_header
{
	($E:ident $error:ident $buf:ident $block:ident $token:ident $hname:ident) => {
		if let Some(value) = $block.get_special($token) {
			$error |= $E::header_std(&mut $buf, StandardHttpHeader::$hname, value).is_err();
		}
	}
}

pub fn serialize_headers<'a,E:EncodeHeaders>(buf: &'a mut [MaybeUninit<u8>], block: &mut HeaderBlock,
	kind: HeaderKind) -> Result<&'a [u8], HeaderError>
{
	//Serialize even if header block is bad, as we will detect the error afterwards.
	let mut error = false;
	let mut buf = UninitializedSink::new(buf);
	error |= match kind {
		HeaderKind::Request(flags) => {
			let method = block.get_special(ShdrMethod).unwrap_or(b"");
			let scheme = block.get_special(ShdrScheme).unwrap_or(b"");
			let authority = block.get_authority(flags & SEFLAG_HOST_RAW != 0).unwrap_or(b"");
			let path = block.get_path(flags & SEFLAG_PATH_RAW != 0).unwrap_or(b"");
			let query = if flags & SEFLAG_PATH_RAW != 0 {
				b""	//Path already contains query!
			} else {
				block.get_special(ShdrQuery).unwrap_or(b"")
			};
			E::request_line(&mut buf, method, scheme, authority, path, query)
		}
		HeaderKind::Response => {
			let status = block.get_status();
			let dflt_reason = get_default_reason(status);
			let reason = block.get_special(ShdrReason).unwrap_or(dflt_reason.deref().as_bytes());
			E::status_line(&mut buf, status, reason)
		},
	}.is_err();
	if !E::is_h2() && block.flags.get(HflagEmitCClose) {
		error |= E::header_std(&mut buf, StandardHttpHeader::Connection, CLOSE).is_err();
	}
	special_header!(E error buf block ShdrContentLength ContentLength);
	special_header!(E error buf block ShdrContentType ContentType);
	special_header!(E error buf block ShdrIpaddr XForwardedFor);
	special_header!(E error buf block ShdrAuthorityO XForwardedHost);
	special_header!(E error buf block ShdrScheme XForwardedProto);
	if block.flags.get(HflagRequest) {
		let v = if block.flags.get(HflagSecure) { b"1" } else { b"0" };
		error |= E::header_std(&mut buf, StandardHttpHeader::XForwardedSecure, v).is_err();
	}
	special_header!(E error buf block ShdrAuthorityO XForwardedServer);
	special_header!(E error buf block ShdrIpaddr XRealIp);
	if !E::is_h2() {
		if let Some(value) = block.get_special(ShdrProtocol) {
			error |= E::header_std(&mut buf, StandardHttpHeader::Upgrade, value).is_err();
			error |= E::header_std(&mut buf, StandardHttpHeader::Connection, UPGRADE).is_err();
		}
		special_header!(E error buf block ShdrWebsocketAccept SecWebsocketAccept);
		special_header!(E error buf block ShdrWebsocketKey SecWebsocketKey);
	}
	special_header!(E error buf block ShdrClientCn XForwardedClientCn);
	special_header!(E error buf block ShdrClientSpkihash XForwardedClientSpkihash);
	special_header!(E error buf block ShdrLocation Location);
	special_header!(E error buf block ShdrUserAgent UserAgent);
	serialize_tail::<E,HeaderBlock>(buf, block, error)
}


fn withpfx(f: &mut Formatter, pfx: &str, v: Option<&[u8]>) -> Result<(), FmtError>
{
	if let Some(v) = v {
		write!(f, "{pfx} {v}\n", v=SafeShowByteString(v))?;
	}
	Ok(())
}

macro_rules! store_error
{
	($slot:ident $x:expr) => {{
		if $slot.is_some() { return; }
		if let Err(e) = $x { $slot = Some(e); }
	}}
}

impl Debug for HeaderBlock
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		f.write_str("----- HEADER BLOCK -----\n")?;
		withpfx(f, "authority(raw)", self.get_special(ShdrAuthorityRaw))?;
		withpfx(f, "authority(original)", self.get_special(ShdrAuthorityO))?;
		withpfx(f, "authority(translated)", self.get_special(ShdrAuthorityT))?;
		withpfx(f, "method", self.get_special(ShdrMethod))?;
		withpfx(f, "path(raw)", self.get_special(ShdrPathRaw))?;
		withpfx(f, "path(original)", self.get_special(ShdrPathO))?;
		withpfx(f, "path(translated)", self.get_special(ShdrPathT))?;
		withpfx(f, "protocol", self.get_special(ShdrProtocol))?;
		withpfx(f, "query", self.get_special(ShdrQuery))?;
		withpfx(f, "reason", self.get_special(ShdrReason))?;
		withpfx(f, "scheme", self.get_special(ShdrScheme))?;
		withpfx(f, "content-type", self.get_special(ShdrContentType))?;
		withpfx(f, "ip-address", self.get_special(ShdrIpaddr))?;
		withpfx(f, "client-cn", self.get_special(ShdrClientCn))?;
		withpfx(f, "client-spki-hash", self.get_special(ShdrClientSpkihash))?;
		withpfx(f, "location", self.get_special(ShdrLocation))?;
		withpfx(f, "user-agent", self.get_special(ShdrUserAgent))?;
		withpfx(f, "websocket-key", self.get_special(ShdrWebsocketKey))?;
		withpfx(f, "websocket-accept", self.get_special(ShdrWebsocketAccept))?;
		withpfx(f, "content-length", self.get_special(ShdrContentLength))?;
		write!(f, "secure {secure}\n", secure=self.flags.get(HflagSecure))?;
		if self.status > 0 { write!(f, "status {status}\n", status=self.get_status())?; }
		withpfx(f, "version", self.get_special(ShdrVersion))?;
		let mut err = None;
		self.for_each_normal(|n,v|store_error!(err
			write!(f, "{n}: {v}\n", n=SafeShowByteString(n.as_bytes()), v=SafeShowByteString(v))
		));
		if let Some(err) = err { fail!(err); }
		f.write_str("------------------------\n")?;
		Ok(())
	}
}
