use crate::ConfigT;
use crate::connection::BackendName;
use crate::connection::combine_timestamps;
use crate::connection_midlevel::ConnectionFreeList;
use crate::connection_midlevel::ForwardInterface;
use crate::connection_midlevel::ForwardInterfaceError;
use crate::connection_midlevel::Interrupter;
use crate::connection_lowlevel::CID;
use crate::connection_midlevel::WriterS;
use crate::connection_out::BackendError;
use crate::headerblock::HeaderBlock;
use crate::headerblock::OwnedTrailerBlock;
use crate::headerblock::ShdrIpaddr;
use crate::headerblock::ShdrMethod;
use crate::headerblock::ShdrPathT;
use crate::headerblock::ShdrProtocol;
use crate::headerblock::ShdrReason;
use crate::http::RequestInfo;
use crate::http::add_server;
use crate::http::RequestWriteReturn;
use crate::http::TransactionStateOut;
use crate::metrics_dp::OutgoingConnectionState;
use crate::routing_dp::HostRoutingDataRef;
use crate::utils::RcTextString;
use btls_aux_fail::ResultExt;
use btls_aux_filename::Filename;
use btls_aux_filename::FilenameBuf;
use btls_aux_http::http::equals_case_insensitive;
use btls_aux_http::http::is_pchar;
use btls_aux_http::http::is_tchar;
use btls_aux_http::http::StandardHttpHeader;
use btls_aux_memory::EscapeByteString;
use btls_aux_memory::PrintBuffer;
use btls_aux_memory::trim_ascii;
use btls_aux_time::daynumber_into_date;
use btls_aux_time::number_of_day;
use btls_aux_time::PointInTime;
use btls_aux_time::Timer;
use btls_aux_unix::FileDescriptor;
use btls_aux_unix::OpenFlags;
use btls_aux_unix::OsError;
use btls_aux_unix::SeekTarget;
use btls_aux_time::ShowTime;
use btls_aux_unix::Stat;
use btls_aux_unix::StatModeFormat;
use btls_daemon_helper_lib::cow;
use btls_util_logging::ConnId;
use btls_util_logging::syslog;
use std::borrow::Cow;
use std::cmp::max;
use std::cmp::min;
use std::fmt::Write as FmtWrite;
use std::io::Error as IoError;
use std::io::ErrorKind as IoErrorKind;
use std::mem::MaybeUninit;
use std::mem::replace;
use std::ops::Deref;
use std::ops::Range;
use std::path::Path;
use std::path::PathBuf;
use std::str::from_utf8;
use std::str::FromStr;


include!(concat!(env!("OUT_DIR"), "/mimetypes.inc"));

macro_rules! printbuffer
{
	($buf:expr, $($args:tt)*) => {{
		let mut buffer = PrintBuffer::new(&mut $buf);
		write!(buffer, $($args)*).ok();		//This should work.
		buffer.into_inner_bytes()
	}}
}

pub struct ConnectionParameters
{
	pub category: RcTextString,
	pub root: RcTextString,
}

pub struct Connection
{
	self_cid: CID,
	shutdown_timer: Timer,
	self_category: RcTextString,
	root: RcTextString,
	conntrack: OutgoingConnectionState,
	headers: HeaderBlock,
	readbuffer: Vec<u8>,	//Not inlined in order to not blow the size.
	//Remaining size of current file.
	remaining_size: u64,
	transaction: Option<TransactionStateOut>,
	current_file: Option<FileDescriptor>,
	current_position: u64,
	filename: String,
	g: ConfigT,
	read_error: Option<IoError>,
	//Path, Do not compress flag, error page flag.
	fixed_file: Option<(PathBuf, bool, bool)>,
}

thread_local!(static EMPTY_STRING: RcTextString = RcTextString::new(""));

macro_rules! store_error
{
	($slot:ident $x:expr) => {{
		if $slot.is_some() { return; }
		if let Err(e) = $x { $slot = Some(e); }
	}}
}

impl Connection
{
	fn __new(category: RcTextString, root: RcTextString, file: Option<(PathBuf, bool, bool)>, g: ConfigT) ->
		Connection
	{
		Connection {
			//Initially shutdown timer is disabled, because at first there is connect
			//delay. Only after connect completes without request does the timers be set to idle.
			shutdown_timer: Timer::new(),
			self_cid: CID::invalid(),
			self_category: category,
			root: root,
			conntrack: OutgoingConnectionState::new_connecting(g),
			transaction: None,
			headers: HeaderBlock::new(),
			readbuffer: vec![0;16384],
			current_file: None,
			current_position: 0,
			remaining_size: 0,
			filename: String::new(),
			g: g,
			read_error: None,
			fixed_file: file,
		}
	}
	pub fn new(p: ConnectionParameters, g: ConfigT) -> Connection
	{
		Self::__new(p.category.to_owned(), p.root.to_owned(), None, g)
	}
	pub fn new_file(p: PathBuf, compress: bool, is_error: bool, g: ConfigT) -> Connection
	{
		let e = EMPTY_STRING.with(|x|x.clone());
		Self::__new(e.clone(), e, Some((p,compress,is_error)), g)
	}
	pub fn get_next_timed_event(&self) -> Option<PointInTime>
	{
		combine_timestamps(&[self.shutdown_timer.expires()], min)
	}
	pub fn handle_timed_event(&mut self, now: PointInTime) -> Result<(), Cow<'static, str>>
	{
		//The connection is at idle. Shut it down immediately.
		fail_if!(self.shutdown_timer.triggered(now),cow!("Connection shut down as unneeded"));
		Ok(())
	}
	pub fn handle_request_event(&mut self, selfid: ConnId) -> Result<(), Cow<'static, str>>
	{
		if let Some(err) = replace(&mut self.read_error, None) {
			fail!(cow!("Reading {filename}: {err}", filename=self.filename));
		}

		if let Some(transaction) = self.transaction.as_mut() {
			//If headers have not been sent yet, do send them.
			if transaction.rx_phase_is_headers() {
				transaction.do_backward_headers(&mut self.headers, self.remaining_size > 0, selfid,
					BackendName::Static(&self.root)).map_err(|err|err.to_string())?;
				self.headers.clear();
				//If remaining size is > 0, notify ready. Otherwise there is no body anyway.
				if self.remaining_size > 0 {
					transaction.do_notify_ready();
				}
			}
		}
		self.check_eof_close().map_err(|err|err.to_string())?;
		Ok(())
	}
	fn set_timers_for_idle(&mut self)
	{
		//Schedule immediate shutdown on fixed files.
		if self.fixed_file.is_some() {
			return self.shutdown_timer.rearm(std::time::Duration::from_secs(0));
		}
		//In idle, if shutdown timer is armed.
		let shutdown_timeout = self.g.with(|g|g.shutdown_timeout);
		self.shutdown_timer.rearm(shutdown_timeout);
		self.conntrack.set_idle();
	}
	fn set_timers_for_request(&mut self)
	{
		//For requests, shutdown timer is cleared.
		self.shutdown_timer.disarm();
		self.conntrack.set_request();
	}
	//Close connection if needed. This should be called when:
	//
	//1) incoming_end gets set.
	//2) outgoing_end gets set.
	//
	//Note that one does not need to actually establish that any of the above actually has happened. It
	//sufficies if any of the above may have happened.
	fn check_eof_close(&mut self) -> Result<(), ForwardInterfaceError>
	{
		//No need to check for remaining_size, because rx done will not happen unless remaining_size=0.
		if self.transaction.as_ref().map(|x|x.rx_and_tx_is_done()).unwrap_or(true) {
			//Clear request, it is no longer needed. Set timers for idle.
			self.transaction = None;
			self.set_timers_for_idle();
			ConnectionFreeList::add_by_cid(&self.self_cid, self.self_category.clone());
		}
		Ok(())
	}
	fn get_file_for_request(&self, block: &HeaderBlock, id: ConnId, method: &[u8]) ->
		Result<String, BackendError>
	{
		//Use translated name.
		let path = block.get_special(ShdrPathT).ok_or_else(||{
			syslog!(WARNING "[For {id}] Uh, request with no path?");
			BackendError::FileNotFound
		})?;
		//Path * is special. Respond to OPTIONS on it.
		fail_if!(path == b"*" && method == b"OPTIONS", BackendError::OptionsRequest);
		if !path.iter().cloned().all(is_pchar) {
			syslog!(NOTICE "[For {id}] Requested invalid file {path}", path=EscapeByteString(path));
			fail!(BackendError::FileNotFound);
		}
		let path = from_utf8(path).map_err(|_|{
			//This is actually internal error.
			syslog!(ERROR "Uh, {path} consists of pchars but is not UTF-8?",
				path=EscapeByteString(path));
			BackendError::FileNotFound
		})?;
		let mut fullpath = self.root.deref().to_owned();
		for part in path.split('/') {
			match part {
				"" => (),	//Ignore empty parts.
				"."|".." => {
					//Pull the IP address.
					let ip = block.get_special(ShdrIpaddr).and_then(|x|from_utf8(x).ok()).
						unwrap_or("<unknown>");
					syslog!(WARNING "[For {id}] Path traversal exploit attempt from {ip} \
						({path})");
					fail!(BackendError::FileNotFound);
				},
				other => {
					fullpath.push('/');
					fullpath.push_str(other);
				}
			}
		}
		Ok(fullpath)
	}
	fn process_headers(&mut self, block: &HeaderBlock, id: ConnId) -> Result<(), BackendError>
	{
		let method = block.get_special(ShdrMethod).unwrap_or(b"");
		let mut maybe_compress = true;
		let mut skip_range = false;
		let (mut fullpath, f) = if let Some(&(ref p,cf,is_error)) = self.fixed_file.as_ref() {
			maybe_compress = cf;
			skip_range = is_error;
			(FilenameBuf::from_pathbuf(p.clone()), false)
		} else {
			let fullpath = self.get_file_for_request(block, id, method)?;
			(FilenameBuf::from_string(fullpath), true)
		};
		let mut st = complete_file_path(&mut fullpath, f)?;
		let mtime = st.mtime();
		let mut rawsize = st.size();
		let mut content_type = "application/octet-stream";
		//Content-type.
		{
			let path = fullpath.into_bytes();
			let mut extstart = path.len();
			while extstart > 0 && path[extstart-1] != b'/' && path[extstart-1] != b'.' { extstart -= 1; }
			if extstart > 0 && path[extstart-1] == b'.' {
				let extension = &path[extstart..];
				if let Ok(p) = MIMETYPES.binary_search_by(|x|x.0.as_bytes().cmp(extension)) {
					if let Some(&(_,t)) = MIMETYPES.get(p) { content_type = t; }
				}
			}
		}

		//Check that nobody tries upgrade, as we do not support that.
		fail_if!(block.get_special(ShdrProtocol).is_some(), BackendError::UpgradeNotSupported);

		//If we get this far with OPTIONS request, answer that with 204.
		fail_if!(method == b"OPTIONS", BackendError::OptionsRequest);

		//Content-Encoding support.
		let content_type = more_specific_content_type(&mut fullpath, content_type);
		let (format, formatc) = if !maybe_compress {
			negotiate_content_encoding(&mut fullpath, block, mtime, &mut st)
		} else {
			(&[][..], b'u')	//Uncompressed.
		};

		match method {
			b"HEAD" => {
				//Set rawsize from stat size, so that the sizes of compressed versions will be
				//correct.
				rawsize = st.size();
				self.current_file = None;		//Ignore.
			},
			b"GET" => {
				st = open_file(fullpath.into_path(), &mut self.current_file, &mut rawsize)?;
			},
			_ => fail!(BackendError::SafeMethodsOnly)
		}
		//Do not perform conditional processing on error pages, always send those pages in full.
		let mut unmodified = false;
		let mut range: Option<Range<u64>> = None;
		let mut etag = [0u8;53];
		if !skip_range {
			//Perform range processing even for HEAD, so the response is the same as in GET case,
			//except for not sending the body.
			let mut err = None;
			block.for_each_named(StandardHttpHeader::Range, |value|{
				store_error!(err parse_range_header(&mut range, value, rawsize));
			});
			if let Some(err) = err { fail!(err); }
			//If resulting range is trivial, remove it.
			if range == Some(0..rawsize.wrapping_sub(1)) { range = None; }
			etag = write_etag(formatc, st);
			let mut unmodified1 = false;
			let mut unmodified2 = false;
			let mut ifnonematch = false;
			//If-Range and if-modified-since.
			block.for_each_normal(|name,value|{
				if name == StandardHttpHeader::IfRange {
					if value.starts_with(b"\"") {
						//Etag. If no match, clear range.
						if value != &etag[..] { range = None; }
					} else {
						//Timestamp. If mtime is bigger, clear range.
						if st.mtime().0 > parse_imfdate(value) { range = None; }
					}
				}
				if name == StandardHttpHeader::IfModifiedSince {
					if st.mtime().0 <= parse_imfdate(value) { unmodified1 = true; }
				}
				if name == StandardHttpHeader::IfNoneMatch {
					ifnonematch = true;
					for item in value.split(|x|*x==b',') {
						let item = trim_ascii(item);
						if item == &etag[..] { unmodified2 = true; }
					}
				}
			});
			unmodified = unmodified2 || (!ifnonematch && unmodified1);
		}
		self.compose_headers(range, rawsize, content_type.deref(), &etag[..], format, unmodified, st)?;
		if method == b"HEAD" { self.remaining_size = 0; }	//HEAD has no actual body.
		//Seek file for start.
		if let Some(current_file) = self.current_file.as_ref() {
			if let Err(err) = current_file.seek(SeekTarget::Set(self.current_position)) {
				syslog!(ERROR "Unable to seek file {fullpath}: {err}");
				fail!(BackendError::InternalError("Unable to seek file"));
			}
		}
		Ok(())
	}
	fn compose_headers(&mut self, range: Option<Range<u64>>, rawsize: u64, content_type: &str, etag: &[u8],
		format: &[u8], unmodified: bool, st: Stat) -> Result<(), BackendError>
	{
		let mut last_modified = [MaybeUninit::uninit();29];
		let last_modified = write_date(ShowTime::from_i64(st.mtime().0), &mut last_modified);
		self.headers.clear();
		if unmodified {
			//Unmodified. Send no payload.
			self.headers.set_status(304);
			self.headers.set_special(ShdrReason, b"Not modified");
			self.headers.set_content_length(rawsize);
			self.remaining_size = 0;
		} else if let Some(range) = range.as_ref() {
			//If range is set, then this is range reply. There is only ever one range.
			//parse_range_header() already checked if this is valid, so assume so.
			let mut buffer = [MaybeUninit::uninit();128];
			let buffer = printbuffer!(buffer, "bytes {start}-{end}/{rawsize}", start=range.start,
				end=range.end);
			self.headers.set_status(206);
			self.headers.set_special(ShdrReason, b"Partial content");
			self.remaining_size = range.end.saturating_sub(range.start)+1;
			self.current_position = range.start;
			self.headers.set_header_std(StandardHttpHeader::ContentRange, buffer);
			self.headers.set_content_length(self.remaining_size);
		} else {
			self.headers.set_status(200);
			self.headers.set_special(ShdrReason, b"OK");
			self.remaining_size = rawsize;
			self.current_position = 0;
			self.headers.set_content_length(self.remaining_size);
		}
		self.headers.set_header_std(StandardHttpHeader::AcceptRanges, b"bytes");
		self.headers.set_header_std(StandardHttpHeader::ContentType, content_type.as_bytes());
		if format != b"" { self.headers.set_header_std(StandardHttpHeader::ContentEncoding, format); }
		//The response might not have etag.
		if !etag.starts_with(b"\0") { self.headers.set_header_std(StandardHttpHeader::Etag, etag); }
		self.headers.set_header_std(StandardHttpHeader::LastModified, &last_modified);
		add_date_header(&mut self.headers);
		add_server(&mut self.headers, "static-backend");
		Ok(())
	}
	fn fix_position(&mut self, mut irq: Interrupter) -> bool
	{
		//Short write! Need to fix position.
		let current_file = self.current_file.as_ref().
			ok_or_else(||IoError::new(IoErrorKind::InvalidInput, "Trying to read NULL file"));
		let result = current_file.and_then(|current_file|{
			if let Err(err) = current_file.seek(SeekTarget::Set(self.current_position)) {
				Err(IoError::from_raw_os_error(err.to_inner()))
			} else {
				Ok(())
			}
		});
		if let Err(err) = result {
			self.read_error = Some(err);
			irq.interrupt();
			return false;
		}
		true
	}
}

struct ContentEncoding
{
	extension: &'static str,
	encoding: &'static [u8],
	mletter: u8,			//u is reserved.
}

//This is encoded from least preferred to most preferred. It can have maximum of 64 entries.
static CONTENT_ENCODINGS: [ContentEncoding;3] = [
	ContentEncoding{extension:".gz", encoding:b"gzip", mletter:b'g'},
	ContentEncoding{extension:".zst", encoding:b"zstd", mletter:b'z'},
	ContentEncoding{extension:".br", encoding:b"br", mletter:b'b'},
];

fn _check_mimetype<'a>(x: &str) -> Result<(), ()>
{
	let mut state = 0;
	for c in x.chars() {
		let a = match c as u32 { x@0..=127 => x as u8, _ => 128 };
		state = match state {
			//States 3, 8 and 11 may end the string, so accept CR and LF.
			3|8|11 if a == 13 || a == 10 => break,
			//For all states, restrict a to 9 (tab), 32-126 (ASCII printables) and 128 (high-plane).
			_ if a != 9 && (a < 32 || a > 126) && a != 128 => fail!(()),
			//State 0 is start of string. The next character must be tchar. If so, transition to state 1.
			0 if is_tchar(a) => 1,
			//State 1 is main type continuing. Next character must be either tchar (eat it) or solidus
			//(transition to state 2).
			1 if is_tchar(a) => 1,
			1 if c == '/' => 2,
			//State 2 is separator between types. The next character must be tchar. If so, transition to
			//state 3.
			2 if is_tchar(a) => 3,
			//State 3 is main type continuing. Next character must be either tchar (eat it) or space
			//or tab (transition to state 4) or ';' (transition to state 5). The string may end here.
			3 if is_tchar(a) => 3,
			3 if c == ' ' || c == '\t' => 4,
			3 if c == ';' => 5,
			//State 4 is eating whitespace before ';'. Next character must be either space or tab (eat
			//it) or ';' (transition to state 5).
			4 if c == ' ' || c == '\t' => 4,
			4 if c == ';' => 5,
			//State 5 is ; eating whitespace after ';' and empty parameter name. Next character must
			//be either tchar (transition to state 6), space or tab (eat it).
			5 if is_tchar(a) => 6,
			5 if c == ' ' || c == '\t' => 5,
			//State 6 is non-empty parameter name. Next character must be either tchar (eat it) or
			//'=' (transition to state 7).
			6 if is_tchar(a) => 6,
			6 if c == '=' => 7,
			//State 7 is on parameter '='. Next character mus either be tchar (transtion to state 8) or
			//'"' (transition to state 9).
			7 if is_tchar(a) => 8,
			7 if c == '\"' => 9,
			//State 8 is non-empty token parameter value. Next character must be either tchar (eat it)
			//space or tab (transition to state 4). The string may end here.
			8 if is_tchar(a) => 8,
			8 if c == ' ' || c == '\t' => 4,
			//State 9 is innards of quoted string value. Next character must either be '\' (transition
			//to state 10), '"' (transition to state 11), or other character (eat it).
			9 if c == '\\' => 10,
			9 if c == '\"' => 11,
			9  => 9,
			//State 10 is escape in quoted string. It must any character (transition to state
			//9)
			10 => 9,
			//State 11 is end of quoted string. This behaves like state 4, except it accepts, so string
			//may end here.
			11 if c == ' ' || c == '\t' => 4,
			11 if c == ';' => 5,
			//All others are errors.
			_ => fail!(())
		};
	}
	//Now, the string can end in states 3, 8 and 11.
	if state == 3 || state == 8 || state == 11 { Ok(()) } else { Err(()) }
}

pub fn check_mimetype<'a>(x: &'a str) -> Option<&'a str>
{
	match _check_mimetype(x) {
		//Truncate at first CR or LF.
		Ok(_) => Some(&x[..x.find(|x|x=='\r'||x=='\n').unwrap_or(x.len())]),
		Err(_) => None,
	}
}

fn more_specific_content_type(fullpath: &mut FilenameBuf, content_type: &'static str) -> Cow<'static, str>
{
	let default = Cow::Borrowed(content_type);
	let olen = fullpath.len();
	//Note, this must be restored before returning.
	fullpath.push(".mimeoverride");
	let fp = {
		//Ignore illegal path (use ENOENT, which gets squashed anyway)
		let p = fullpath.into_unix_path().ok_or(OsError::ENOENT);
		match p.and_then(|p|p.open(OpenFlags::O_RDONLY|OpenFlags::O_CLOEXEC)) {
			Ok(x) => Some(x),
			Err(err) => {
				//Ignore ENOENT to cut down on warning spam.
				if err != OsError::ENOENT {
					syslog!(ERROR "Failed to open {fullpath}.mimeoverride: {err}");
				}
				None
			}
		}
	};
	fullpath.truncate(olen);
	//If we did not open any file, we still need to truncate (above) and return default.
	let fp = f_return!(fp, default);
	let mut buf = [MaybeUninit::uninit();512];
	match fp.read_uninit2(&mut buf) {
		Ok(buf) => match from_utf8(buf).ok().and_then(|x|check_mimetype(x)) {
			Some(x) => return Cow::Owned(x.to_owned()),
			None => syslog!(WARNING "Not valid mimetype in {fullpath}.mimeoverride"),
		},
		Err(err) => syslog!(ERROR "Failed to read {fullpath}.mimeoverride: {err}")
	}
	default
}

fn negotiate_content_encoding(fullpath: &mut FilenameBuf, block: &HeaderBlock, mtime: (i64, u32), st: &mut Stat) ->
	(&'static [u8], u8)
{
	let mut formatc = b'u';
	let mut format: &'static [u8] = b"";
	let mut ext = "";
	//Content-Encoding support.
	let mut sbits = 0u64;	//64 is enough, right?
	block.for_each_named(StandardHttpHeader::AcceptEncoding, |value|{
		for item in value.split(|x|*x==b',') {
			let item = trim_ascii(item.split(|x|*x==b';').next().unwrap_or(b""));
			for (slevel, edata) in CONTENT_ENCODINGS.iter().enumerate() {
				if equals_case_insensitive(item, edata.encoding) {
					sbits |= 1 << slevel;
				}
			}
		}
	});
	for (slevel, edata) in CONTENT_ENCODINGS.iter().enumerate() {
		if sbits & 1 << slevel == 0 { continue; }
		let olen = fullpath.len();
		fullpath.push(edata.extension);
		if let Some(Ok(st2)) = fullpath.into_unix_path().map(|p|p.stat()) {
			if st2.mtime() >= mtime {
				format = edata.encoding;
				formatc = edata.mletter;
				ext = edata.extension;
				*st = st2;
			}
		}
		fullpath.truncate(olen);
	}
	fullpath.push(ext);
	(format, formatc)
}

//Wed, 12 Jun 2019 06:19:03 GMT
//     !"  %. !)+) @& !) @#
//
//1560320343
macro_rules! write_byte
{
	($buf:expr, $itr:expr, $c:expr) => {{ $buf.get_mut($itr).map(|t|*t=$c); $itr = $itr + 1; }}
}

macro_rules! rd2d
{
	($buf:expr, $itr:expr) => {
		(*$buf.get($itr).unwrap_or(&0)&15).wrapping_mul(10).wrapping_add(*$buf.get($itr+1).unwrap_or(&0)&15)
	}
}

#[test]
fn test_d()
{
	assert_eq!(parse_imfdate(b"@g9d:`&gA%^HQi+Y5>Xt=W~|#XRwO"), 1560320343);
	//>                             0c  5e 19b9 06 19 03
	//                               ,  %. !)+)  & !)  #
	//                              0<  5> 19;9 06 19 03
	//                              @L  EN AIKI @F AI @C
	//                              P\  U^ QY[Y PV QY PS
	//                              `l  en aiki `f ai `c
	//                              p|  u~ qy{y pv qy ps
	//>                             12  64 2019
	//                              !"  &$ " !)
	//                              12  64 2019
	//                              AB  FD B@AI
	//                              QR  VT RPQY
	//                              ab  fd b`ai
	//                              qr  vt rpqy
}

fn parse_imfdate(date: &[u8]) -> i64
{
	let day = rd2d!(date, 5);
	let month = match rd2d!(date, 9) {
		24 => 1, 52 => 2, 12 => 3, 2 => 4, 19 => 5, 64 => 6, 62 => 7, 57 => 8, 50 => 9, 34 => 10,
		156 => 11, 53 => 12, _ => 1
	};
	let century = rd2d!(date, 12);
	let year = rd2d!(date, 14);
	let hour = rd2d!(date, 17);
	let minute = rd2d!(date, 20);
	let second = rd2d!(date, 23);
	let year = (century as i64) * 100 + (year as i64);
	let year = year - 10000 * ((year - 1) >> 63);
//println!("{year}-{month}-{day} {hour}-{minute}-{second}");
	let dnum = number_of_day(year, month, 1).unwrap_or(0);
	dnum * 86400 + day as i64 * 86400 + hour as i64 * 3600 + minute as i64 * 60 + second as i64 - 86400
}

pub(crate) fn add_date_header(headers: &mut HeaderBlock)
{
	let mut tmp = [MaybeUninit::uninit();29];
	headers.set_header_std(StandardHttpHeader::Date, write_date(ShowTime::now(), &mut tmp));
}

fn write_date<'a>(ts: ShowTime, buffer: &'a mut [MaybeUninit<u8>;29]) -> &'a [u8]
{
	static WEEKDAYS: [&'static str;7] = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
	static MONTHS: [&'static str;12] = [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
	];
	//Always round down.
	let daynum = ts.number_of_day();
	let (year, month, day) = daynumber_into_date(daynum);
	let year = year % 10000;
	let (hour, minute, second) = ts.time_of_day();
	let weekday = (daynum as usize + 3) % 7;	//1.1.1970 is Thursday.
	{
		let mut p = PrintBuffer::new(buffer);
		write!(p, "{weekday}, {day:02} {month} {year:04} {hour:02}:{minute:02}:{second:02} GMT",
			weekday=WEEKDAYS[weekday], month=MONTHS[month as usize - 1]).ok();
		p.into_inner_bytes()
	}
//println!("ts={ts} -> {p}", p=EscapeByteString(p.into_inner()));
}
//Mon, 06 May 2019 18:43:18 GMT
//00000000001111111111222222222
//01234567890123456789012345678

/*
#[test]
fn test_date()
{
	let mut i = -62135596800;
	while i < 253433923200 {
		let mut buf = [MaybeUninit::uninit();29];
		let buf = write_date(i, &mut buf);
		let j = parse_imfdate(&buf);
		assert_eq!(i, j);
		i += 131071*23;
	}
}
*/

fn write_num(buffer: &mut [u8], itr: &mut usize, mut n: u64)
{
	for _ in 0..10 {
		static SYMBOLS: &'static [u8;85] = b"#$&()*+,-.0123456789:<=>?ABCDEFGHIJKLMNOPQRSTUVWXYZ[]^_\
			abcdefghijklmnopqrstuvwxyz{|}~";
		let x = (n % SYMBOLS.len() as u64) as usize;
		write_byte!(buffer, *itr, SYMBOLS[x]);
		n /= SYMBOLS.len() as u64;
	}
}

fn write_etag(vcode: u8, st: Stat) -> [u8;53]
{
	let mut buffer = [0;53];
	let mut itr = 0;
	write_byte!(buffer, itr, b'\"');
	write_byte!(buffer, itr, vcode);
	write_num(&mut buffer, &mut itr, st.device());
	write_num(&mut buffer, &mut itr, st.inode());
	write_num(&mut buffer, &mut itr, st.mtime().0 as u64);
	write_num(&mut buffer, &mut itr, st.mtime().1 as u64);
	write_num(&mut buffer, &mut itr, st.size());
	write_byte!(buffer, itr, b'\"');
	assert_eq!(itr, 53);
	buffer
}

fn readvalue(x: &str) -> Result<Option<u64>, ()>
{
	Ok(if x != "" { Some(dtry!(u64::from_str(x))) } else { None })
}

fn parse_range_header(range: &mut Option<Range<u64>>, hdrval: &[u8], fsize: u64) ->  Result<(), BackendError>
{
	use crate::connection_out::BackendError::BadRangeRequested as BadRange;
	static ILLSYNTAX: &'static str = "Illegal range syntax";
	static RANGE_OOB: &'static str = "Range goes out of bounds";
	static RANGE_BW: &'static str = "Range bounds are backwards";
	//Transform to UTF-8, ignoring invalid headers.
	let hdrval = from_utf8(hdrval).set_err(BadRange(fsize, ILLSYNTAX))?;
	//Split at =.
	let mut itr = hdrval.splitn(2, '=');
	if itr.next() != Some("bytes") { return Ok(()); }	//Unsupported unit.
	let hdrval = itr.next().ok_or(BadRange(fsize, ILLSYNTAX))?;
	for r in hdrval.split(',') {
		//Split at -.
		let mut itr = r.splitn(2, '-');
		let rstart = itr.next().ok_or(BadRange(fsize, ILLSYNTAX))?;
		let rend = itr.next().ok_or(BadRange(fsize, ILLSYNTAX))?;
		let rstart = readvalue(rstart).set_err(BadRange(fsize, ILLSYNTAX))?;
		let rend = readvalue(rend).set_err(BadRange(fsize, ILLSYNTAX))?;
		let (rstart, rend) = match (rstart, rend) {
			(Some(rstart), Some(rend)) => {
				fail_if!(rend >= fsize, BadRange(fsize, RANGE_OOB));
				fail_if!(rstart > rend, BadRange(fsize, RANGE_BW));
				(rstart, rend)
			},
			(Some(rstart), None) => {
				fail_if!(rstart >= fsize, BadRange(fsize, RANGE_OOB));
				(rstart, fsize - 1)	//fsize=0 unavoidably trips the above check.
			},
			(None, Some(rend)) => {
				fail_if!(rend > fsize || rend == 0, BadRange(fsize, RANGE_OOB));
				(fsize - rend, fsize - 1)	//fsize=0 unavoidably trips the above check.
			},
			(None, None) => fail!(BadRange(fsize, ILLSYNTAX))
		};
		*range = Some(if let Some(range) = range.clone() {
			min(range.start, rstart)..max(range.end, rend)
		} else {
			rstart..rend
		});
	}
	Ok(())
}

fn open_file(fullpath: &Path, handle: &mut Option<FileDescriptor>, size: &mut u64) -> Result<Stat, BackendError>
{
	let path = Filename::from_path(fullpath).into_unix_path().ok_or_else(||{
		syslog!(ERROR "Failed to open {fullpath}: Illegal path", fullpath=fullpath.display());
		BackendError::FileNotFound
	})?;
	let fd = path.open(OpenFlags::O_RDONLY|OpenFlags::O_CLOEXEC).map_err(|err|{
		syslog!(ERROR "Failed to open {fullpath}: {err}", fullpath=fullpath.display());
		BackendError::FileNotFound
	})?;
	//Update size if possible.
	match fd.stat() {
		Ok(st) => {
			*size = st.size();
			*handle = Some(fd);
			Ok(st)
		},
		Err(err) => {
			syslog!(ERROR "Failed to fstat {fullpath}: {err}", fullpath=fullpath.display());
			Err(BackendError::FileNotFound)
		}
	}
}

fn complete_file_path(fullpath: &mut FilenameBuf, dir: bool) -> Result<Stat, BackendError>
{
	//Just check if this is directory.
	if let Some(Ok(x)) = fullpath.into_unix_path().map(|p|p.stat()) {
		if dir && x.format() == StatModeFormat::DIR { fullpath.push("/index.html"); }
	}
	let st = match fullpath.into_unix_path().map(|p|p.stat()) {
		Some(Ok(st)) if st.format() == StatModeFormat::REG => Ok(st),
		Some(Ok(_)) => Err(syslog!(NOTICE "File is irregular {fullpath}")),
		Some(Err(err)) if err == OsError::ENOENT =>
			Err(syslog!(NOTICE "File does not exist {fullpath}")),
		Some(Err(err)) => Err(syslog!(ERROR "Failed to stat {fullpath}: {err}")),
		None => Err(syslog!(ERROR "Failed to stat {fullpath}: File name contains NUL")),
	}.set_err(BackendError::FileNotFound)?;
	Ok(st)
}

impl ForwardInterface for Connection
{
	fn write_forward_headers(&mut self, sink: &mut impl WriterS, block: &mut HeaderBlock, req: &RequestInfo,
		hostdata: HostRoutingDataRef, parent: ConnId) ->
		Result<(), Result<BackendError, ForwardInterfaceError>>
	{
		//If request is being serviced, then new request can not be initated.
		fail_if!(self.transaction.is_some(), Ok(BackendError::ConnectionLost));
		//The return status is weird, as this can return both soft errors (Err(Ok(x))), and hard errors
		//(Err(Err(x))). Soft error should be passed as-is to caller. Hard errors should trigger garbage-
		//collection of the connection and ConnectionLost error code.
		//Check that the header block is OK, fail if not.
		if let Some(err) = block.is_overflow() { fail!(Ok(BackendError::BadHeadersReceived(err))); }

		if let Err(err) = self.process_headers(block, parent) {
			//The CID may be INVALID_CID in case this is called before connection is actually
			//established. In this case, the connection coming up adds the thing to freelist.
			if !self.self_cid.is_invalid() {
				ConnectionFreeList::add_by_cid(&self.self_cid, self.self_category.clone());
			}
			fail!(Ok(err));
		}

		//If self_cid is INVALID_CID, the connection is not on freelist and is not established yet, so
		//wait until that before setting timers.
		if !self.self_cid.is_invalid() { self.set_timers_for_request(); }

		//Set the timers for request, store the received request. The state of outgoing_end depends on if
		//there is request body or not. Also, if upgrade is set, reserve space for the body. incoming_end
		//is false, because response will be sent.
		let mut transaction = TransactionStateOut::new(req, hostdata);
		if block.is_no_body() { transaction.notify_tx_done(); }
		self.transaction = Some(transaction);
		sink.interrupt();	//Do interrupt in order to start sending stuff.
		Ok(())
	}
	fn write_forward_data(&mut self, _: &mut impl WriterS, _: &[u8]) -> Result<bool, ForwardInterfaceError>
	{
		//This should never happen, but if it does, just ignore the data.
		Ok(true)
	}
	fn write_forward_trailers(&mut self, _: &mut impl WriterS, _: &mut HeaderBlock) ->
		Result<(), ForwardInterfaceError>
	{
		//This should never happen, but in case it does, clean up the tx side.
		if let Some(transaction) = self.transaction.as_mut() { transaction.notify_tx_done(); }
		self.check_eof_close()
	}
	fn write_forward_error(&mut self, error: &str) -> Result<(), ForwardInterfaceError>
	{
		//This is special in that there is never any state that can not be uncorrupted.
		syslog!(NOTICE "static file backend: Master error: {error}");
		self.remaining_size = 0;
		self.transaction = None;
		self.current_file = None;
		self.set_timers_for_idle();
		Ok(())
	}
	fn get_master(&self) -> CID { self.transaction.as_ref().map(|x|x.get_cid()).unwrap_or_else(CID::invalid) }
	fn get_sid(&self) -> u32 { self.transaction.as_ref().map(|x|x.get_sid()).unwrap_or(0) }
	fn notify_connected(&mut self, cid: CID)
	{
		//If there is no pending request, then transition to idle timers, and add to freelist.
		self.self_cid = cid;
		if self.transaction.is_none() {
			//Fixed files are never added to freelist. Schedule immediate shutdown on those.
			if self.fixed_file.is_some() {
				return self.shutdown_timer.rearm(std::time::Duration::from_secs(0));
			}
			ConnectionFreeList::add_by_cid(&self.self_cid, self.self_category.clone());
			self.set_timers_for_idle();
		} else {
			self.set_timers_for_request();
		}
	}
	fn request_data<S:RequestWriteReturn>(&mut self, sink: &mut S, mut irq: Interrupter) -> bool
	{
		if self.remaining_size == 0 { return false; }
		//Offer the entiere secondary buffer.
		let ostatus = self.remaining_size > 0;
		let amt2 = match match self.current_file.as_ref() {
			Some(current_file) => Some(current_file.read(&mut self.readbuffer)),
			None => None,
		} {
			Some(Ok(amt)) if amt > 0 || self.remaining_size == 0 =>
				min(amt as u64, self.remaining_size) as usize,
			Some(Ok(_)) => {
				self.read_error = Some(IoError::new(IoErrorKind::UnexpectedEof,
					format!("Read end of file, expected {remain} more bytes",
					remain=self.remaining_size)));
				0
			},
			Some(Err(e)) => {
				self.read_error = Some(IoError::from_raw_os_error(e.to_inner()));
				0
			},
			None => {
				self.read_error = Some(IoError::new(IoErrorKind::InvalidInput,
					"Trying to read NULL file"));
				0
			},
		};
		//No further progress on read error.
		if self.read_error.is_some() {
			irq.interrupt();
			return false;
		}
		match sink.write_backward_data(&self.readbuffer[..amt2], self.remaining_size == amt2 as u64) {
			Some(amt) => {
				self.current_position += amt as u64;
				self.remaining_size -= amt as u64;
				if amt2 != amt {
					//Short write! Need to fix position.
					if !self.fix_position(irq) { return false; }
				}
			},
			None => {
				//Another short write.
				self.fix_position(irq);
				return false
			}
		}
		//If the secondary buffer got negative edge, stuff the trailers in, if one has those. And mark
		//rx as ended. Then interrupt to resume sending.
		if ostatus && self.remaining_size == 0 {
			self.current_file = None;
			sink.write_backward_trailers(&mut OwnedTrailerBlock::blank());
			self.transaction.as_mut().map(|x|x.notify_rx_done());
			irq.interrupt();
		}
		true
	}
}
