use std::fs::remove_file;
use std::io::Write;
use std::io::Read;
use std::os::unix::net::UnixListener;
use std::os::unix::net::UnixStream;
use std::thread::spawn;


fn socket_thread(mut s: UnixStream)
{
	//First read until we get \r\n\r\n, signifiying end of HTTP request.
	let mut buf = [0;16384];
	let mut ptr = 0;
	loop {
		let amt = s.read(&mut buf[ptr..]).unwrap();
		ptr += amt;
		if amt == 0 { break; }
		if ptr >= 4 && &buf[ptr-4..ptr] == b"\r\n\r\n" { break; }
	}
	//Then write response, including partial body.
	s.write_all(b"HTTP/1.1 200 OK\r\nContent-Length:562949953421312\r\n\r\n").unwrap();
	for _ in 0..34359738368u64 { s.write_all(&buf).unwrap(); }
	//Terminate.
}

fn main()
{
	remove_file("/tmp/testpartial.sock").ok();
	let l = UnixListener::bind("/tmp/testpartial.sock").unwrap();
	loop {
		let s = match l.accept() { Ok((s,_)) => s, Err(_) => continue };
		spawn(||socket_thread(s));
	}
}
