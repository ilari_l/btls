extern crate btls_daemon_helper_lib;
use btls_aux_fail::f_break;
use btls_daemon_helper_lib::bind_metrics_socket;
use btls_daemon_helper_lib::MetricsMap;
use btls_daemon_helper_lib::PerThreadMetrics;
use btls_daemon_helper_lib::PrintUi;
use btls_daemon_helper_lib::PrintUptime;
use btls_daemon_helper_lib::receive_metrics_from;
use btls_daemon_helper_lib::TrackCpuUsage;
use std::io::stderr;
use std::io::Write;
use std::time::Duration;


pub const GMETRIC_UPTIME: u32 = 4;

macro_rules! define_http_gmetrics
{
	($([$value:tt $symbol:ident $variable:ident];)*) => {
		$(pub const $symbol: u32 = $value;)*
	}
}

//Define the metrics.
include!("../metrics.inc");

macro_rules! show
{
	($ui:expr, $($args:tt)*) => { $ui.show(format_args!($($args)*)) }
}

macro_rules! single
{
	($ui:expr, $($args:tt)*) => { $ui.single(format_args!($($args)*)) }
}

fn do_print_ui(ui: &mut PrintUi, cpu: &mut TrackCpuUsage, arr: &MetricsMap, ts: Duration)
{
	//Global metrics.
	let g_tcp_read = arr.read(GMETRIC_TCP_READ);
	let g_tcp_write = arr.read(GMETRIC_TCP_WRITE);
	let g_serv_read = arr.read(GMETRIC_SERV_READ);
	let g_serv_write = arr.read(GMETRIC_SERV_WRITE);
	let g_in_acc = arr.read(GMETRIC_CONN_ACCEPT);
	let g_in_comm = arr.read(GMETRIC_CONN_COMMIT);
	let g_in_ret = arr.read(GMETRIC_CONN_RETIRE);
	let g_in_uncomm = g_in_acc - g_in_comm;
	let g_in_act = g_in_comm - g_in_ret;
	let g_fe_proxy = arr.read(GMETRIC_IN_PROXY);
	let g_fe_http1 = arr.read(GMETRIC_IN_H1);
	let g_fe_http2 = arr.read(GMETRIC_IN_H2);
	let g_fe_dead = arr.read(GMETRIC_IN_DEAD);
	let g_be_conn = arr.read(GMETRIC_OUT_CONNECTING);
	let g_be_idle = arr.read(GMETRIC_OUT_IDLE);
	let g_be_keepalive = arr.read(GMETRIC_OUT_KEEPALIVE);
	let g_be_request = arr.read(GMETRIC_OUT_REQUEST);
	let g_be_dead = arr.read(GMETRIC_OUT_DEAD);
	let g_flt_panic = arr.read(GMETRIC_DROP_ABORT);
	let g_flt_master = arr.read(GMETRIC_DROP_MASTER);
	let g_flt_slave = arr.read(GMETRIC_DROP_SLAVE);
	let g_req_https = arr.read(GMETRIC_REQ_HTTPS);
	let g_req_https_conn = arr.read(GMETRIC_CONN_HTTPS);
	let g_req_http = arr.read(GMETRIC_REQ_HTTP);
	let g_req_http_blk = arr.read(GMETRIC_REQ_HTTP_BLOCKED);
	let g_req_http_force = arr.read(GMETRIC_REQ_HTTP_FORCED);
	let g_req_http_conn = arr.read(GMETRIC_CONN_HTTP);
	let g_req_blk = arr.read(GMETRIC_REQ_BLOCKED);
	let g_err_in = arr.read(GMETRIC_ERROR_IN);
	let g_err_tcp = arr.read(GMETRIC_ERROR_TCP);
	let g_err_serv = arr.read(GMETRIC_ERROR_SERV);
	let g_err_conn = arr.read(GMETRIC_ERROR_CONNECT);
	let g_err_goaway = arr.read(GMETRIC_ERROR_GOAWAY);
	let g_st_101 = arr.read(GMETRIC_STATUS_101);
	let g_st_304 = arr.read(GMETRIC_STATUS_304);
	let g_st_400 = arr.read(GMETRIC_STATUS_400);
	let g_st_2xx = arr.read(GMETRIC_STATUS_2XX);
	let g_st_3xx = arr.read(GMETRIC_STATUS_3XX);
	let g_st_4xx = arr.read(GMETRIC_STATUS_4XX);
	let g_st_5xx = arr.read(GMETRIC_STATUS_5XX);
	let g_st_oth = arr.read(GMETRIC_STATUS_OTHER);
	let g_res_req = arr.read(GMETRIC_RESPONSE_REQUEST);
	let g_res_be = arr.read(GMETRIC_RESPONSE_BACKEND);
	let g_res_int = arr.read(GMETRIC_RESPONSE_INTERNAL);
	let g_res_none = arr.read(GMETRIC_RESPONSE_NORESPONSE);
	let g_res_wait = g_res_req - g_res_be - g_res_int - g_res_none;

	ui.rewind();

	let tss = ts.subsec_nanos();
	let ts = ts.as_secs();
	let uptime = PrintUptime(arr.get(GMETRIC_UPTIME));
	show!(ui, "Received at: {ts}.{tss:09} (Uptime: {uptime})");

	ui.label("Bytes");
	single!(ui, "In: {g_tcp_read}->{g_serv_write}");
	single!(ui, "Out: {g_serv_read}->{g_tcp_write}");

	show!(ui, "Incoming: {g_in_acc}=({g_in_uncomm} uncommit + {g_in_act} active + {g_in_ret} dead)");

	ui.label("Frontend");
	single!(ui, "{g_fe_proxy} Proxy");
	single!(ui, "{g_fe_http1} HTTP/1");
	single!(ui, "{g_fe_http2} HTTP/2");
	single!(ui, "{g_fe_dead} Dead");

	ui.label("Backend");
	single!(ui, "{g_be_conn} Conn");
	single!(ui, "{g_be_idle} Idle");
	single!(ui, "{g_be_keepalive} Keep");
	single!(ui, "{g_be_request} Req");
	single!(ui, "{g_be_dead} Dead");

	ui.label("Fault");
	single!(ui, "{g_flt_panic} Panic");
	single!(ui, "{g_flt_master} MasterErr");
	single!(ui, "{g_flt_slave} SlaveErr");

	ui.label("Error");
	single!(ui, "{g_err_in} HTTP");
	single!(ui, "{g_err_tcp} TCP");
	single!(ui, "{g_err_serv} Serv");
	single!(ui, "{g_err_conn} Conn");
	single!(ui, "{g_err_goaway} Goaway");
		
	show!(ui, "Requests: {g_res_req}=({g_res_be} backend + {g_res_int} internal + {g_res_none} nores + \
		{g_res_wait} wait)");

	ui.label("Bstatus");
	single!(ui, "101:{g_st_101}");
	single!(ui, "304:{g_st_304}");
	single!(ui, "400:{g_st_400}");
	single!(ui, "2xx:{g_st_2xx}");
	single!(ui, "3xx:{g_st_3xx}");
	single!(ui, "4xx:{g_st_4xx}");
	single!(ui, "5xx:{g_st_5xx}");
	single!(ui, "?:{g_st_oth}");

	ui.label("Security");
	single!(ui, "Yes:{g_req_https} in {g_req_https_conn}");
	single!(ui, "No:{g_req_http}({g_req_http_blk} blk {g_req_http_force} redir) in {g_req_http_conn}");
	single!(ui, "Blocked: {g_req_blk}");

	let mut tcount = 0;
	let mut total_run = 0;
	let mut total_sleep = 0;
	loop {
		let m = f_break!(PerThreadMetrics::new(&arr, tcount));
		show!(ui, "{m}");	//Standard form.
		tcount += 1;
		total_run += m.total_run.as_us();
		total_sleep += m.total_sleep.as_us();
	}
	show!(ui, "CPU usage: {cpu}", cpu=cpu.process(total_run, total_sleep));
}

fn main()
{
	let mut cpu = TrackCpuUsage::new();
	let mut itr = std::env::args();
	itr.next();
	let fd = match bind_metrics_socket(itr.next().as_deref()) {
		Ok(x) => x,
		Err(err) => return writeln!(stderr(), "{err}").unwrap()
	};
	let mut ui = PrintUi::new();
	loop {
		let (ts, arr) = f_break!(receive_metrics_from(&fd));
		do_print_ui(&mut ui, &mut cpu, &arr, ts);
	}
}
