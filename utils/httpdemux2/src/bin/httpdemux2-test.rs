extern crate btls_aux_http;
extern crate btls_aux_serialization;
use btls_aux_http::__hpack_encode_header;
use btls_aux_serialization::Sink;
use std::collections::HashMap;
use std::fmt::Write as FmtWrite;
use std::fs::remove_file;
use std::io::Write as IoWrite;
use std::io::Read as IoRead;
use std::net::Shutdown;
use std::os::unix::net::UnixListener;
use std::os::unix::net::UnixStream;
use std::panic::AssertUnwindSafe;
use std::panic::catch_unwind;
use std::str::from_utf8;
use std::thread::sleep;
use std::time::Duration;


fn http2_data(strmid: u32, eos: bool, content: &[u8]) -> Vec<u8>
{
	let mut out = Vec::new();
	out.write_u24(content.len() as u32).unwrap();
	out.write_u8(0).unwrap();
	out.write_u8(if eos { 1 } else { 0 }).unwrap();
	out.write_u32(strmid).unwrap();
	out.extend_from_slice(content);
	out
}

fn http2_header(strmid: u32, eos: bool, h: &[(&str, &str)], x32: bool, fpadsz: Option<u32>) -> Vec<u8>
{
	let mut out = Vec::new();
	out.write_u24(0).unwrap();	//Reserve.
	out.write_u8(1).unwrap();
	out.write_u8(if eos { 5 } else { 4 }).unwrap();
	out.write_u32(strmid).unwrap();
	if x32 { out.write_u8(32).unwrap(); }
	for &(k,v) in h.iter() { __hpack_encode_header(&mut out, k.as_bytes(), v.as_bytes()).unwrap(); }
	let l = out.len();
	out.alter_u24(0, fpadsz.unwrap_or(l as u32 - 9)).unwrap();
	out
}

fn http2_reset(strmid: u32, code: u32) -> Vec<u8>
{
	let mut out = Vec::new();
	out.write_u24(4).unwrap();
	out.write_u8(3).unwrap();
	out.write_u8(0).unwrap();
	out.write_u32(strmid).unwrap();
	out.write_u32(code).unwrap();
	out
}

fn assert_got(s: &mut UnixStream, r: &[u8])
{
	let mut v = vec![0;r.len()];
	let mut x = 0;
	s.set_read_timeout(Some(Duration::from_secs(6))).unwrap();
	while x < v.len() {
		x += match s.read(&mut v[x..]) {
			Ok(0) => break,
			Ok(y) => y,
			Err(err) => if err.raw_os_error() == Some(11) { break; } else { panic!("I/O error: {err}"); }
		}
	}
	if let (Ok(l), Ok(r)) = (from_utf8(&v[..x]), from_utf8(&r)) {
		if l == r { return; }
		let mut fdiff = 0;
		for ((i, c), (_, d)) in l.char_indices().zip(r.char_indices()) {
			if c != d { fdiff = i; break; }
		}
		println!("<Stripping first {fdiff} bytes>");
		assert_eq!(&l[fdiff..], &r[fdiff..]);
		return;
	}
	let l = &v[..x];
	if l != r {
		let mut fdiff = 0;
		for ((i, c), (_, d)) in l.iter().cloned().enumerate().zip(r.iter().cloned().enumerate()) {
			if c != d { fdiff = i; break; }
		}
		println!("<Stripping first {fdiff} bytes>");
		assert_eq!(&l[fdiff..], &r[fdiff..]);
	}
}

fn assert_eof(s: &mut UnixStream)
{
	let mut v = vec![0;256];
	let mut x = 0;
	s.set_read_timeout(Some(Duration::from_secs(6))).unwrap();
	while x < v.len() {
		x += match s.read(&mut v[x..]) {
			Ok(0) => break,
			Ok(y) => y,
			Err(err) => if err.raw_os_error() == Some(11) { break; } else { panic!("I/O error: {err}"); }
		}
	}
	if let Ok(l) = from_utf8(&v[..x]) {
		assert_eq!(l, "");
		return;
	}
	let l = &v[..x];
	assert_eq!(l, b"");
}


fn mkrequest(meth: &str, path: &str, headers: &[(&str, &str)], body: &str) -> Vec<u8>
{
	let mut s = String::new();
	write!(s, "{meth} {path} HTTP/1.1\r\n").unwrap();
	for &(name, value) in headers.iter() {
		write!(s, "{name}:{value}\r\n").unwrap();
	}
	write!(s, "\r\n{body}").unwrap();
	s.into_bytes()
}

fn mkresponse(status: u16, reason: &str, headers: &[(&str, &str)], body: &str) -> Vec<u8>
{
	let mut s = String::new();
	write!(s, "HTTP/1.1 {status} {reason}\r\n").unwrap();
	for &(name, value) in headers.iter() {
		write!(s, "{name}:{value}\r\n").unwrap();
	}
	write!(s, "\r\n{body}").unwrap();
	s.into_bytes()
}

fn send_http_proxyhdr(s: &mut UnixStream)
{
	s.write_all(b"\x0D\x0A\x0D\x0A\x00\x0D\x0A\x51\x55\x49\x54\x0A\x21\x11\x00\x0c\
\x7f\x00\x00\x01\x7f\x00\x00\x01\xff\xfe\x00\x50").unwrap();
}

fn send_https_proxyhdr(s: &mut UnixStream)
{
	s.write_all(b"\x0D\x0A\x0D\x0A\x00\x0D\x0A\x51\x55\x49\x54\x0A\x21\x11\x00\x14\
\x7f\x00\x00\x01\x7f\x00\x00\x01\xff\xfe\x00\x50\
\x20\x00\x05\x00\x00\x00\x00\x00").unwrap();
}

fn send_https_proxyhdr_hash(s: &mut UnixStream)
{
	s.write_all(b"\x0D\x0A\x0D\x0A\x00\x0D\x0A\x51\x55\x49\x54\x0A\x21\x11\x00\x1b\
\x7f\x00\x00\x01\x7f\x00\x00\x01\xff\xfe\x00\x50\
\x20\x00\x0c\x00\x00\x00\x00\x00\
	\xe0\x00\x04\xde\xad\xbe\xef").unwrap();
}

fn send_https_proxyhdr_cn(s: &mut UnixStream)
{
	s.write_all(b"\x0D\x0A\x0D\x0A\x00\x0D\x0A\x51\x55\x49\x54\x0A\x21\x11\x00\x25\
\x7f\x00\x00\x01\x7f\x00\x00\x01\xff\xfe\x00\x50\
\x20\x00\x16\x00\x00\x00\x00\x00\
	\xe0\x00\x04\xde\xad\xbe\xef\x22\x00\x07foo\xc4bar").unwrap();
}

fn send_https2_proxyhdr(s: &mut UnixStream)
{
	s.write_all(b"\x0D\x0A\x0D\x0A\x00\x0D\x0A\x51\x55\x49\x54\x0A\x21\x11\x00\x19\
\x7f\x00\x00\x01\x7f\x00\x00\x01\xff\xfe\x00\x50\
\x20\x00\x05\x00\x00\x00\x00\x00\x01\x00\x02h2\
PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n\0\0\0\x04\0\0\0\0\0").unwrap();
}

const HTTP2_FIXED_MAGIC_REPLY: &'static [u8] = b"\0\0\x2a\x04\0\0\0\0\0\
\x00\x01\x00\x00\x10\x00\
\x00\x02\x00\x00\x00\x00\
\x00\x03\x00\x00\x00\x64\
\x00\x04\x00\x00\xff\xff\
\x00\x05\x00\x00\x40\x00\
\x00\x08\x00\x00\x00\x01\
\x00\x09\x00\x00\x00\x01\
\0\0\0\x04\x01\0\0\0\0\
\0\0\x04\x08\0\0\0\0\0\x3b\x9a\xca\x00";

const ST_CONNECT: &'static str = "Conecting";
const ST_ACCEPT: &'static str = "Accepting connection";
const ST_SNDREQ: &'static str = "Sending request";
const ST_RCVREQ: &'static str = "Receiving request";
const ST_SNDRES: &'static str = "Sending response";
const ST_RCVRES: &'static str = "Receiving response";
const STD_CONNCLOSE: (&'static str, &'static str) = ("connection", "close");
const STD_CONNUPGD: (&'static str, &'static str) = ("connection", "upgrade");
const STD_WEBSOCKET: (&'static str, &'static str) = ("upgrade", "websocket");
const STD_UPGDS: (&'static str, &'static str) = ("upgrade", "foo,websocket");
const STD_HOST: (&'static str, &'static str) = ("host", "httpdemux2.test");
const STD_INTRES: (&'static str, &'static str) = ("server", "httpdemux2/0.9.7 internal-response");
const STD_CHUNKED: (&'static str, &'static str) = ("transfer-encoding", "chunked");
const STD_XFFOR: (&'static str, &'static str) = ("x-forwarded-for", "127.0.0.1");
const STD_XFHOST: (&'static str, &'static str) = ("x-forwarded-host", "httpdemux2.test");
const STD_XFPROTO: (&'static str, &'static str) = ("x-forwarded-proto", "https");
const STD_XFSECURE: (&'static str, &'static str) = ("x-forwarded-secure", "1");
const STD_XFSERVER: (&'static str, &'static str) = ("x-forwarded-server", "httpdemux2.test");
const STD_XREALIP: (&'static str, &'static str) = ("x-real-ip", "127.0.0.1");
const STD_VIA: (&'static str, &'static str) = ("via", "1.1 foobar");
const STD_VIA2: (&'static str, &'static str) = ("via", "2 foobar");
const STD_CDN_LOOP: (&'static str, &'static str) = ("cdn-loop", "foobar");
const STD_XCTO: (&'static str, &'static str) = ("x-content-type-options", "nosniff");
const STD_CSP: (&'static str, &'static str) = ("content-security-policy", "default-src 'none'");
const STD_BSTREAM: (&'static str, &'static str) = ("content-type", "application/octet-stream");
const STD_UTF8: (&'static str, &'static str) = ("content-type", "text/plain;charset=utf-8");
const STD_EXPECT: (&'static str, &'static str) = ("expect", "100-continue");
const TE: &'static str = "transfer-encoding";
const CLEN: &'static str = "content-length";

struct Test(usize);

impl Test
{
	fn _do<T,F>(&mut self, name: &str, f: F) -> T where F: FnOnce() -> T
	{
		self.0 += 1;
		print!("{name}...\x1b[K");
		std::io::stdout().flush().unwrap();
		let r = f();
		println!("OK");
		r
	}
}

macro_rules! fnwrap { ($name:ident) => { (stringify!($name), $name) } }

static TESTS: &'static [(&'static str, fn(&mut Test, &mut UnixListener))] = &[
	fnwrap!(test_http11_get_basic),
	fnwrap!(test_http11_get_basic_hash),
	fnwrap!(test_http11_get_basic_cn),
	fnwrap!(test_http11_get_looping),
	fnwrap!(test_http11_get_sequential_2b),
	fnwrap!(test_http11_get_sequential_1b),
	fnwrap!(test_http11_get_parallel),
	fnwrap!(test_http11_get_blank),
	fnwrap!(test_http11_get_blank_chunked),
	fnwrap!(test_http11_get_blank_204),
	fnwrap!(test_http11_get_blank_304),
	fnwrap!(test_http11_get_blank_304_chunked),
	fnwrap!(test_http11_get_query),
	fnwrap!(test_http11_get_truncated),
	fnwrap!(test_http11_get_connection_close),
	fnwrap!(test_http11_get_pending_eof),
	fnwrap!(test_http11_get_undelimited),
	fnwrap!(test_http11_get_undelimited_bzip2),
	fnwrap!(test_http11_get_chunked),
	fnwrap!(test_http11_get_chunked_ext),
	fnwrap!(test_http11_get_chunked_bzip2),
	fnwrap!(test_http11_get_chunked_truncated),
	fnwrap!(test_http11_get_chunked_bzip2_truncated),
	fnwrap!(test_http11_get_chunked_bad),
	fnwrap!(test_http11_get_acme),
	fnwrap!(test_http11_get_pkivalidation),
	fnwrap!(test_http11_get_bootleg_certificate),
	fnwrap!(test_http11_get_badpath),
	fnwrap!(test_http11_get_badquery),
	fnwrap!(test_http11_head_basic),
	fnwrap!(test_http11_head_proxyurl),
	fnwrap!(test_http11_head_proxyurl_host),
	fnwrap!(test_http11_head_proxyurl_nohost),
	fnwrap!(test_http11_head_proxyurl_mismatch),
	fnwrap!(test_http11_head_remapped0),
	fnwrap!(test_http11_head_remapped1),
	fnwrap!(test_http11_head_remapped2),
	fnwrap!(test_http11_head_remapped3),
	fnwrap!(test_http11_head_remappedyy),
	fnwrap!(test_http11_head_remappedw),
	fnwrap!(test_http11_head_access_denied),
	fnwrap!(test_http11_head_chunked),
	fnwrap!(test_http11_head_badpath),
	fnwrap!(test_http11_post_basic),
	fnwrap!(test_http11_post_empty),
	fnwrap!(test_http11_post_nolength),
	fnwrap!(test_http11_post_undelimited_bzip2),
	fnwrap!(test_http11_post_chunked),
	fnwrap!(test_http11_post_chunked_bzip2),
	fnwrap!(test_http11_post_connection_close),
	fnwrap!(test_http11_post_pending_eof),
	fnwrap!(test_http11_post_trailers),
	fnwrap!(test_http11_post_trailers_bad),
	fnwrap!(test_http11_post_trailers_bad2),
	fnwrap!(test_http11_post_301bug),
	fnwrap!(test_http11_post_302bug),
	fnwrap!(test_http11_post_307nobug),
	fnwrap!(test_http11_post_308nobug),
	fnwrap!(test_http11_post_303),
	fnwrap!(test_http11_post_100continue),
	fnwrap!(test_http11_post_100continue_redirect),
	fnwrap!(test_http11_post_100continue_redirect_chunked),
	fnwrap!(test_http11_post_100continue_redirect_redial),
	fnwrap!(test_http11_post_100continue_badclient),
	fnwrap!(test_http11_post_100continue_badserver),
	fnwrap!(test_http11_post_faulty_chunk),
	fnwrap!(test_http11_post_faulty_chunk2),
	fnwrap!(test_http11_post_truncated),
	fnwrap!(test_http11_post_truncated_chunked),
	fnwrap!(test_http11_post_truncated_chunked2),
	fnwrap!(test_http11_post_maybe_smuggled),
	fnwrap!(test_http11_post_maybe_smuggled2),
	fnwrap!(test_http11_post_maybe_smuggled3),
	fnwrap!(test_http11_post_maybe_smuggled4),
	fnwrap!(test_http11_connect),
	fnwrap!(test_http11_connect_like_get),
	fnwrap!(test_http11_badmeth_basic),
	fnwrap!(test_http11_no_host),
	fnwrap!(test_http11_continuation),
	fnwrap!(test_http11_illegal0),
	fnwrap!(test_http11_illegal8),
	fnwrap!(test_http11_illegal10),
	fnwrap!(test_http11_illegal11),
	fnwrap!(test_http11_illegal12),
	fnwrap!(test_http11_illegal13),
	fnwrap!(test_http11_illegal14),
	fnwrap!(test_http11_illegal31),
	fnwrap!(test_http11_illegal127),
	fnwrap!(test_http11_get_basic_500),
	fnwrap!(test_http11_get_basic),
	fnwrap!(test_http11_illegal_hname),
	fnwrap!(test_http11_badvers09),
	fnwrap!(test_http11_badvers20),
	fnwrap!(test_http11_badreqline),
	fnwrap!(test_http11_badreqline2),
	fnwrap!(test_http11_truncated_request),
	fnwrap!(test_http11_truncated_request2),
	fnwrap!(test_http11_truncated_request3),
	fnwrap!(test_http11_borderline_legal),
	fnwrap!(test_http11_backend_immediate_drop),
	fnwrap!(test_http11_backend_immediate_drop_post),
	fnwrap!(test_http11_backend_immediate_drop_100continue),
	fnwrap!(test_http11_backend_invalid_statusline1),
	fnwrap!(test_http11_backend_invalid_statusline2),
	fnwrap!(test_http11_backend_bad_version09),
	fnwrap!(test_http11_backend_bad_version20),
	fnwrap!(test_http11_backend_incomplete_headers1),
	fnwrap!(test_http11_backend_incomplete_headers2),
	fnwrap!(test_http11_websocket_required),
	fnwrap!(test_http11_websocket_basic),
	fnwrap!(test_http11_websocket_forwarding1),
	fnwrap!(test_http11_websocket_forwarding2),
	fnwrap!(test_http11_https_insecure),
	fnwrap!(test_http11_http_secure),
	fnwrap!(test_http11_http_hstspreload),
	fnwrap!(test_http11_https_hstspreload),
	fnwrap!(test_http11_too_many_headers),
	fnwrap!(test_http11_too_big_headers),
	fnwrap!(test_http2_establish_connection),
	fnwrap!(test_http2_ping),
	fnwrap!(test_http2_get_simple),
	fnwrap!(test_http2_get_looping),
	fnwrap!(test_http2_get_query),
	fnwrap!(test_http2_get_badpath),
	fnwrap!(test_http2_get_chunked),
	fnwrap!(test_http2_get_chunked_trailers),
	fnwrap!(test_http2_get_chunked_bzip2),
	fnwrap!(test_http2_get_sequential_1b),
	fnwrap!(test_http2_get_sequential_2b),
	fnwrap!(test_http2_get_parallel),
	fnwrap!(test_http2_get_parallel_backendlost),
	fnwrap!(test_http2_get_parallel_backendlost_intransfer),
	fnwrap!(test_http2_get_illegal_headers),
	fnwrap!(test_http2_head_simple),
	fnwrap!(test_http2_head_no_authority),
	fnwrap!(test_http2_head_http_scheme),
	fnwrap!(test_http2_head_legacy_host),
	fnwrap!(test_http2_head_authority_mismatch),
	fnwrap!(test_http2_post_simple),
	fnwrap!(test_http2_post_trailers),
	fnwrap!(test_http2_post_length),
	fnwrap!(test_http2_post_length_tdiscard),
	fnwrap!(test_http2_post_length_bad),
	fnwrap!(test_http2_post_length_bad2),
	fnwrap!(test_http2_post_maybe_smuggled),
	fnwrap!(test_http2_post_maybe_smuggled2),
	fnwrap!(test_http2_post_maybe_smuggled3),
	fnwrap!(test_http2_post_maybe_smuggled4),
	fnwrap!(test_http2_post_301),
	fnwrap!(test_http2_websocket),
	fnwrap!(test_http2_websocket_halfclose1),
	fnwrap!(test_http2_websocket_halfclose2),
	fnwrap!(test_http2_illegal_connect),
	fnwrap!(test_http2_two_headers),
	fnwrap!(test_http11_get_static),
	fnwrap!(test_http2_connection_limit),
	fnwrap!(test_http2_stream_limit),
	fnwrap!(test_http2_connection_limit_error),

	fnwrap!(test_fragmented_get),
];


fn main()
{
	remove_file("/tmp/httdemux2.test").ok();
	let mut l = UnixListener::bind("/tmp/httdemux2.test").unwrap();
	let mut fail = 0;
	for &(name, test) in TESTS.iter() {
		println!("----- {name} -----");
		if let Err(_) = catch_unwind(AssertUnwindSafe(||{
			let mut c = Test(0);
			test(&mut c, &mut l);
			for _ in 0..c.0+1 { print!("\x1b[1A\x1b[K"); }
			println!("\x1b[32m[PASS]\x1b[0m ----- {name} ----- ");
		})) {
			println!("\x1b[31m[FAIL]\x1b[0m ----- {name} ----- ");
			sleep(Duration::from_secs(3));
			fail += 1;
		}
	}
	if fail > 0 {
		println!("----- {fail} FAILED -----");
	} else {
		println!("----- ALL PASS -----");
	}
}

enum ChecklistItem<'a>
{
	Connect(&'a str, u32, &'a str),
	SendProxy(&'a str, u32),
	SendProxyHash(&'a str, u32),
	SendProxyCn(&'a str, u32),
	SendProxyInsecure(&'a str, u32),
	Accept(&'a str, u32),
	DropS(&'a str, u32),
	H2Exch(&'a str, u32),
	Wait,
	TxRaw(&'a str, u32, &'a [u8]),
	TxH1Req(&'a str, u32, &'a str, &'a str, &'a [(&'a str, &'a str)]),
	TxH1Res(&'a str, u32, u16, &'a str, &'a [(&'a str, &'a str)]),
	TxH2Req(&'a str, u32, u32, bool, &'a str, &'a str, &'a [(&'a str, &'a str)]),
	TxH2Data(&'a str, u32, u32, bool, &'a [u8]),
	TxH2Trailers(&'a str, u32, u32, &'a [(&'a str, &'a str)]),
	TxTooManyHeaders(&'a str, u32),
	TxTooBigHeaders(&'a str, u32),
	TxEof(&'a str, u32),
	RxRaw(&'a str, u32, &'a [u8]),
	RxH1Req(&'a str, u32, &'a str, &'a str, &'a [(&'a str, &'a str)]),
	RxH1Res(&'a str, u32, u16, &'a str, &'a [(&'a str, &'a str)]),
	RxH2Res(&'a str, u32, u32, bool, u16, &'a str, &'a [(&'a str, &'a str)]),
	RxH2Data(&'a str, u32, u32, bool, &'a [u8]),
	RxH2Trailers(&'a str, u32, u32, &'a [(&'a str, &'a str)]),
	RxH1Err(&'a str, u32, u16, &'a str),
	RxH1ErrF(&'a str, u32, &'a str),
	RxH1ErrHF(&'a str, u32, &'a str),
	RxEof(&'a str, u32),
	ArbitraryCode(&'a str, Box<dyn Fn(&mut HashMap<u32, UnixStream>)->()>),
}

fn test(t: &mut Test, l: &mut UnixListener, checklist: &[ChecklistItem])
{
	let mut con = HashMap::new();
	for item in checklist { match item {
		&ChecklistItem::Connect(name, slot, target) =>
			t._do(name, ||{con.insert(slot, UnixStream::connect(target).unwrap());}),
		&ChecklistItem::SendProxy(name, slot) =>
			t._do(name, ||send_https_proxyhdr(con.get_mut(&slot).unwrap())),
		&ChecklistItem::SendProxyHash(name, slot) =>
			t._do(name, ||send_https_proxyhdr_hash(con.get_mut(&slot).unwrap())),
		&ChecklistItem::SendProxyCn(name, slot) =>
			t._do(name, ||send_https_proxyhdr_cn(con.get_mut(&slot).unwrap())),
		&ChecklistItem::SendProxyInsecure(name, slot) =>
			t._do(name, ||send_http_proxyhdr(con.get_mut(&slot).unwrap())),
		&ChecklistItem::Accept(name, slot) =>
			t._do(name, ||{con.insert(slot, l.accept().map(|(u,_)|u).unwrap());}),
		&ChecklistItem::DropS(name, slot) => t._do(name, ||{con.remove(&slot);}),
		&ChecklistItem::Wait => sleep(Duration::new(0, 100000000)),
		&ChecklistItem::H2Exch(name, slot) =>
			t._do(name, ||{
				send_https2_proxyhdr(con.get_mut(&slot).unwrap());
				assert_got(con.get_mut(&slot).unwrap(), HTTP2_FIXED_MAGIC_REPLY);
			}),
		&ChecklistItem::TxRaw(name, slot, data) =>
			t._do(name, ||con.get_mut(&slot).unwrap().write_all(data).unwrap()),
		&ChecklistItem::TxH1Req(name, slot, method, path, headers) =>
			t._do(name, ||con.get_mut(&slot).unwrap().write_all(&mkrequest(method, path, headers, "")).
				unwrap()),
		&ChecklistItem::TxH1Res(name, slot, status, reason, headers) =>
			t._do(name, ||con.get_mut(&slot).unwrap().write_all(&mkresponse(status, reason, headers,
				"")).unwrap()),
		&ChecklistItem::TxH2Req(name, slot, sid, eos, method, path, headers) => {
			let mut hset = vec![(":method",method), (":scheme", "https"),
				(":authority", "httpdemux2.test"), (":path", path)];
			hset.extend_from_slice(headers);
			t._do(name, ||con.get_mut(&slot).unwrap().write_all(&http2_header(sid, eos, &hset, false,
				None)).unwrap());
		},
		&ChecklistItem::TxH2Data(name, slot, sid, eos, data) =>
			t._do(name, ||con.get_mut(&slot).unwrap().write_all(&http2_data(sid, eos, data)).unwrap()),
		&ChecklistItem::TxH2Trailers(name, slot, sid, headers) => {
			t._do(name, ||con.get_mut(&slot).unwrap().write_all(&http2_header(sid, true,
				headers, false, None)).unwrap());
		},
		&ChecklistItem::TxTooManyHeaders(name, slot) => t._do(name, ||for _ in 0..520 {
			con.get_mut(&slot).unwrap().write_all(b"x:y\r\n").unwrap();
		}),
		&ChecklistItem::TxTooBigHeaders(name, slot) => t._do(name, ||for _ in 0..256 {
			con.get_mut(&slot).unwrap().write_all(b"abcdefghijklmnopqrstuvwxyz:\
				abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz\r\n").unwrap();
		}),
		&ChecklistItem::TxEof(name, slot) =>
			t._do(name, ||con.get_mut(&slot).unwrap().shutdown(Shutdown::Write).unwrap()),
		&ChecklistItem::RxRaw(name, slot, data) =>
			t._do(name, ||assert_got(con.get_mut(&slot).unwrap(), data)),
		&ChecklistItem::RxH1Req(name, slot, method, path, headers) =>
			t._do(name, ||assert_got(con.get_mut(&slot).unwrap(), &mkrequest(method, path, headers,
				""))),
		&ChecklistItem::RxH1Res(name, slot, status, reason, headers) =>
			t._do(name, ||assert_got(con.get_mut(&slot).unwrap(), &mkresponse(status, reason, headers,
				""))),
		&ChecklistItem::RxH2Res(name, slot, sid, eos, status, reason, headers) => {
			let status = status.to_string();
			let mut hset: Vec<(&str, &str)> = vec![(":status",&status), ("x-http2-reason", reason)];
			hset.extend_from_slice(headers);
			t._do(name, ||assert_got(con.get_mut(&slot).unwrap(), &http2_header(sid, eos,
				&hset, true, None)));
		},
		&ChecklistItem::RxH2Data(name, slot, sid, eos, data) =>
			t._do(name, ||assert_got(con.get_mut(&slot).unwrap(), &http2_data(sid, eos, data))),
		&ChecklistItem::RxH2Trailers(name, slot, sid, headers) => {
			t._do(name, ||assert_got(con.get_mut(&slot).unwrap(), &http2_header(sid, true,
				headers, false, None)));
		},
		&ChecklistItem::RxH1Err(name, slot, status, reason) => {
			t._do(name, ||{
				let conn = con.get_mut(&slot).unwrap();
				let r2 = if status == 421 {
					format!("{reason}\r\n").into_bytes()
				} else {
					format!("{status} {reason}\r\n").into_bytes()
				};
				let r1 = mkresponse(status, reason, &[(CLEN,&r2.len().to_string()),
					STD_UTF8, STD_INTRES, STD_CSP, STD_XCTO], "");
				assert_got(conn, &r1);
				assert_got(conn, &r2);
			});
		},
		&ChecklistItem::RxH1ErrF(name, slot, reason) => {
			t._do(name, ||{
				let conn = con.get_mut(&slot).unwrap();
				let r1 = mkresponse(400, &format!("Parse error: {reason}"),
					&[STD_CONNCLOSE,(CLEN,&(reason.len()+19).to_string()),STD_UTF8, STD_INTRES,
					STD_CSP, STD_XCTO], "");
				let r2 = format!("400 Parse error: {reason}\r\n").into_bytes();
				assert_got(conn, &r1);
				assert_got(conn, &r2);
				assert_eof(conn);
			});
		},
		&ChecklistItem::RxH1ErrHF(name, slot, reason) => {
			t._do(name, ||{
				let conn = con.get_mut(&slot).unwrap();
				let r1 = mkresponse(400, &format!("Parse error: {reason}"),
					&[STD_CONNCLOSE,(CLEN,&(reason.len()+19).to_string()),STD_UTF8, STD_INTRES,
					STD_CSP, STD_XCTO], "");
				assert_got(conn, &r1);
				assert_eof(conn);
			});
		},
		&ChecklistItem::RxEof(name, slot) =>  t._do(name, ||assert_eof(con.get_mut(&slot).unwrap())),
		&ChecklistItem::ArbitraryCode(name, ref code) => t._do(name, ||code(&mut con)),
	}}
}

/*******************************************************************************************************************/

fn test_http11_get_basic(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("G", 1, b"Hello, World!\n"),
		RxH1Res("H", 0, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("I", 0, b"Hello, World!\n"),
		TxEof("J", 0),
		RxEof("K", 0),
	]);
}

fn test_http11_get_basic_hash(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxyHash("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, ("x-forwarded-client-spkihash", "deadbeef"), STD_VIA,
			STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("G", 1, b"Hello, World!\n"),
		RxH1Res("H", 0, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("I", 0, b"Hello, World!\n"),
		TxEof("J", 0),
		RxEof("K", 0),
	]);
}

fn test_http11_get_basic_cn(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxyCn("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, ("x-forwarded-client-cn", "foo\u{f0c4}bar"),
			("x-forwarded-client-spkihash", "deadbeef"), STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("G", 1, b"Hello, World!\n"),
		RxH1Res("H", 0, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("I", 0, b"Hello, World!\n"),
		TxEof("J", 0),
		RxEof("K", 0),
	]);
}

fn test_http11_get_looping(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST,STD_CDN_LOOP]),
		RxH1Err("H", 0, 500, "Request is looping through proxy"),
		TxEof("J", 0),
		RxEof("K", 0),
	]);
}

fn test_http11_get_sequential_1b(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[(CLEN,"14")]),
		TxRaw("G", 1, b"Hello, World!\n"),
		RxH1Res("H", 0, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("I", 0, b"Hello, World!\n"),
		TxH1Req("J", 0, "GET", "/bar", &[STD_HOST]),
		RxH1Req("K", 1, "GET", "/bar", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("L", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("M", 1, b"hello, world!\n"),
		RxH1Res("N", 0, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("O", 0, b"hello, world!\n"),
		TxEof("P", 0),
		RxEof("Q", 0),
	]);
}

fn test_http11_get_sequential_2b(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("G", 1, b"Hello, World!\n"),
		RxH1Res("H", 0, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("I", 0, b"Hello, World!\n"),
		TxH1Req("J", 0, "GET", "/bar", &[STD_HOST]),
		Accept("K", 1),
		RxH1Req("L", 1, "GET", "/bar", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("M", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("N", 1, b"hello, world!\n"),
		RxH1Res("O", 0, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("P", 0, b"hello, world!\n"),
		TxEof("Q", 0),
		RxEof("R", 0),
	]);
}

fn test_http11_get_parallel(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		Connect("B", 5, "/tmp/foo.sock"),
		SendProxy("C", 0),
		SendProxy("D", 5),
		TxH1Req("E", 0, "GET", "/foo", &[STD_HOST]),
		Accept("F", 1),
		TxH1Req("G", 5, "GET", "/bar", &[STD_HOST]),
		Accept("H", 6),
		RxH1Req("I", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		RxH1Req("J", 6, "GET", "/bar", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("K", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxH1Res("L", 6, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("M", 1, b"Hello, World!\n"),
		TxRaw("N", 6, b"hello, world!\n"),
		RxH1Res("O", 0, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxH1Res("P", 5, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("Q", 0, b"Hello, World!\n"),
		RxRaw("R", 5, b"hello, world!\n"),
		TxEof("S", 0),
		TxEof("T", 5),
		RxEof("U", 0),
		RxEof("V", 5),
	]);
}

fn test_http11_get_blank(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"0")]),
		RxH1Res("G", 0, 200, "OK", &[(CLEN,"0"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxEof("H", 0),
		RxEof("I", 0),
	]);
}

fn test_http11_get_blank_chunked(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, STD_CHUNKED]),
		RxH1Res("G", 0, 200, "OK", &[STD_BSTREAM, STD_CHUNKED, STD_XCTO, STD_CSP]),
		TxRaw("H", 1, b"0\r\n\r\n"),
		RxRaw("I", 0, b"0\r\n\r\n"),
		TxEof("J", 0),
		RxEof("K", 0),
	]);
}

fn test_http11_get_blank_204(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 204, "Empty", &[STD_CONNCLOSE]),
		RxH1Res("G", 0, 204, "Empty", &[]),
		TxEof("H", 0),
		RxEof("I", 0),
	]);
}

fn test_http11_get_blank_304(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 304, "Not modified", &[STD_CONNCLOSE, (CLEN,"125")]),
		RxH1Res("G", 0, 304, "Not modified", &[(CLEN,"125")]),
		//RxH1Res("G", 0, 304, "Not modified", &[(CLEN,"125"), STD_BSTREAM]),
		TxEof("H", 0),
		RxEof("I", 0),
	]);
}

fn test_http11_get_blank_304_chunked(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 304, "Not modified", &[STD_CONNCLOSE, STD_CHUNKED]),
		RxH1Res("G", 0, 304, "Not modified", &[STD_CHUNKED]),
		TxEof("H", 0),
		RxEof("I", 0),
	]);
}


fn test_http11_get_query(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo?bar", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo?bar", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("G", 1, b"Hello, World!\n"),
		RxH1Res("H", 0, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("I", 0, b"Hello, World!\n"),
		TxEof("J", 0),
		RxEof("K", 0),
	]);
}

fn test_http11_get_truncated(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"15")]),
		TxRaw("G", 1, b"Hello, World!\n"),
		TxEof("H", 1),
		RxH1Res("I", 0, 200, "OK", &[(CLEN,"15"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("J", 0, b"Hello, World!\n"),
		RxEof("K", 0),
	]);
}

fn test_http11_get_connection_close(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST, STD_CONNCLOSE]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("G", 1, b"Hello, World!\n"),
		RxH1Res("H", 0, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("I", 0, b"Hello, World!\n"),
		RxEof("K", 0),
	]);
}

fn test_http11_get_pending_eof(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		TxEof("D", 0),
		Accept("E", 1),
		RxH1Req("F", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("G", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("H", 1, b"Hello, World!\n"),
		RxH1Res("I", 0, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("J", 0, b"Hello, World!\n"),
		RxEof("K", 0),
	]);
}


fn test_http11_get_undelimited(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE]),
		RxH1Res("G", 0, 200, "OK", &[STD_BSTREAM, STD_CHUNKED, STD_XCTO, STD_CSP]),
		TxRaw("H", 1, b"Hello, World!\n"),
		RxRaw("I", 0, b"e\r\nHello, World!\n\r\n"),
		TxRaw("J", 1, b"hello, world!\n"),
		RxRaw("K", 0, b"e\r\nhello, world!\n\r\n"),
		TxEof("L", 1),
		RxRaw("M", 0, b"0\r\n\r\n"),
		TxEof("N", 0),
		RxEof("O", 0),
	]);
}

fn test_http11_get_undelimited_bzip2(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, (TE,"bzip2")]),
		RxH1Res("G", 0, 200, "OK", &[STD_BSTREAM, (TE,"bzip2"), STD_CHUNKED, STD_XCTO, STD_CSP]),
		TxRaw("H", 1, b"Hello, World!\n"),
		RxRaw("I", 0, b"e\r\nHello, World!\n\r\n"),
		TxRaw("J", 1, b"hello, world!\n"),
		RxRaw("K", 0, b"e\r\nhello, world!\n\r\n"),
		TxEof("L", 1),
		RxRaw("M", 0, b"0\r\n\r\n"),
		TxEof("N", 0),
		RxEof("O", 0),
	]);
}

fn test_http11_get_chunked(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, STD_CHUNKED]),
		RxH1Res("H", 0, 200, "OK", &[STD_BSTREAM, STD_CHUNKED, STD_XCTO, STD_CSP]),
		TxRaw("I", 1, b"e\r\nHello, World!\n\r\n"),
		RxRaw("J", 0, b"e\r\nHello, World!\n\r\n"),
		TxRaw("K", 1, b"e\r\nhello, world!\n\r\n"),
		RxRaw("L", 0, b"e\r\nhello, world!\n\r\n"),
		TxRaw("M", 1, b"0\r\n\r\n"),
		RxRaw("N", 0, b"0\r\n\r\n"),
		TxEof("O", 0),
		RxEof("P", 0),
	]);
}

fn test_http11_get_chunked_ext(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, STD_CHUNKED]),
		RxH1Res("H", 0, 200, "OK", &[STD_BSTREAM, STD_CHUNKED, STD_XCTO, STD_CSP]),
		TxRaw("I", 1, b"e;hi\r\nHello, World!\n\r\n"),
		RxRaw("J", 0, b"e\r\nHello, World!\n\r\n"),
		TxRaw("K", 1, b"e;zot\r\nhello, world!\n\r\n"),
		RxRaw("L", 0, b"e\r\nhello, world!\n\r\n"),
		TxRaw("M", 1, b"0;xyzzy\r\n\r\n"),
		RxRaw("N", 0, b"0\r\n\r\n"),
		TxEof("O", 0),
		RxEof("P", 0),
	]);
}

fn test_http11_get_chunked_bzip2(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, (TE,"bzip2, chunked")]),
		RxH1Res("H", 0, 200, "OK", &[STD_BSTREAM, (TE,"bzip2, chunked"), STD_XCTO, STD_CSP]),
		TxRaw("I", 1, b"e\r\nHello, World!\n\r\n"),
		RxRaw("J", 0, b"e\r\nHello, World!\n\r\n"),
		TxRaw("K", 1, b"e\r\nhello, world!\n\r\n"),
		RxRaw("L", 0, b"e\r\nhello, world!\n\r\n"),
		TxRaw("M", 1, b"0\r\n\r\n"),
		RxRaw("N", 0, b"0\r\n\r\n"),
		TxEof("O", 0),
		RxEof("P", 0),
	]);
}

fn test_http11_get_chunked_truncated(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, STD_CHUNKED]),
		RxH1Res("H", 0, 200, "OK", &[STD_BSTREAM, STD_CHUNKED, STD_XCTO, STD_CSP]),
		TxRaw("I", 1, b"e\r\nHello, World!\n\r\n"),
		RxRaw("J", 0, b"e\r\nHello, World!\n\r\n"),
		TxRaw("K", 1, b"e\r\nhello, world!\n\r\n"),
		RxRaw("L", 0, b"e\r\nhello, world!\n\r\n"),
		TxEof("M", 1),
		RxEof("N", 0),
	]);
}

fn test_http11_get_chunked_bzip2_truncated(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, (TE,"bzip2, chunked")]),
		RxH1Res("H", 0, 200, "OK", &[STD_BSTREAM, (TE,"bzip2, chunked"), STD_XCTO, STD_CSP]),
		TxRaw("I", 1, b"e\r\nHello, World!\n\r\n"),
		RxRaw("J", 0, b"e\r\nHello, World!\n\r\n"),
		TxRaw("K", 1, b"e\r\nhello, world!\n\r\n"),
		RxRaw("L", 0, b"e\r\nhello, world!\n\r\n"),
		TxEof("M", 1),
		RxEof("N", 0),
	]);
}

fn test_http11_get_chunked_bad(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, STD_CHUNKED]),
		RxH1Res("H", 0, 200, "OK", &[STD_BSTREAM, STD_CHUNKED, STD_XCTO, STD_CSP]),
		TxRaw("I", 1, b"e\r\nHello, World!\nX\r\n"),
		RxRaw("J", 0, b"e\r\nHello, World!\n\r\n"),
		RxEof("P", 0),
	]);
}

fn test_http11_get_acme(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/.well-known/acme-challenge/foo", &[STD_HOST]),
		RxH1Err("D", 0, 403, "ACME HTTP-01 validation not allowed"),
		TxH1Req("E", 0, "GET", "/.well-known/acme-challenge/foo", &[STD_HOST]),
		RxH1Err("F", 0, 403, "ACME HTTP-01 validation not allowed"),
		TxEof("G", 0),
		RxEof("H", 0),
	]);
}

fn test_http11_get_pkivalidation(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/.well-known/pki-validation/bar", &[STD_HOST]),
		RxH1Err("D", 0, 403, "Method 6 validation not allowed"),
		TxH1Req("E", 0, "GET", "/.well-known/pki-validation", &[STD_HOST]),
		RxH1Err("F", 0, 403, "Method 6 validation not allowed"),
		TxEof("G", 0),
		RxEof("H", 0),
	]);
}

fn test_http11_get_bootleg_certificate(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/.well-known%2facme-challenge/foo", &[STD_HOST]),
		RxH1Err("D", 0, 403, "ACME HTTP-01 validation not allowed"),
		TxH1Req("E", 0, "GET", "/.well-known%2fpki-validation/bar", &[STD_HOST]),
		RxH1Err("F", 0, 403, "Method 6 validation not allowed"),
		TxH1Req("G", 0, "GET", "/.well-known%2fpki-validation", &[STD_HOST]),
		RxH1Err("H", 0, 403, "Method 6 validation not allowed"),
		TxEof("I", 0),
		RxEof("J", 0),
	]);
}

fn test_http11_get_badpath(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo[x", &[STD_HOST]),
		RxH1ErrF("D", 0, "Illegal path: Illegal byte 91 (position 4 state 0-0)"),
	]);
}

fn test_http11_get_badquery(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo?[x", &[STD_HOST]),
		RxH1ErrF("D", 0, "Illegal query: Illegal byte 91 (position 1 state 0-0)"),
	]);
}

fn test_http11_head_basic(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "HEAD", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "HEAD", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"1453")]),
		RxH1Res("G", 0, 200, "OK", &[(CLEN,"1453"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxEof("H", 0),
		RxEof("I", 0),
	]);
}

fn test_http11_head_proxyurl(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "HEAD", "https://httpdemux2.test/foo", &[("host","")]),
		Accept("D", 1),
		RxH1Req("E", 1, "HEAD", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"1453")]),
		RxH1Res("G", 0, 200, "OK", &[(CLEN,"1453"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxEof("H", 0),
		RxEof("I", 0),
	]);
}

fn test_http11_head_proxyurl_host(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "HEAD", "https://httpdemux2.test/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "HEAD", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"1453")]),
		RxH1Res("G", 0, 200, "OK", &[(CLEN,"1453"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxEof("H", 0),
		RxEof("I", 0),
	]);
}

fn test_http11_head_proxyurl_nohost(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "https://httpdemux2.test/foo", &[]),
		RxH1ErrF("D", 0, "No virtual host specified"),
	]);
}

fn test_http11_head_proxyurl_mismatch(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "https://httpdemux2.test/foo", &[("host","foo")]),
		RxH1ErrF("D", 0, "Illegal authority: Inconsistent with previous value"),
	]);
}

fn test_http11_head_remapped0(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "HEAD", "/test-0", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "HEAD", "/test-a", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[(CLEN,"1453")]),
		RxH1Res("G", 0, 200, "OK", &[(CLEN,"1453"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxH1Req("H", 0, "HEAD", "/test-0/", &[STD_HOST]),
		RxH1Req("I", 1, "HEAD", "/test-a/", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("J", 1, 200, "OK", &[(CLEN,"1453")]),
		RxH1Res("K", 0, 200, "OK", &[(CLEN,"1453"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxH1Req("L", 0, "HEAD", "/test-0/x", &[STD_HOST]),
		RxH1Req("M", 1, "HEAD", "/test-a/x", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("N", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"1453")]),
		RxH1Res("O", 0, 200, "OK", &[(CLEN,"1453"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxEof("P", 0),
		RxEof("Q", 0),
	]);
}

fn test_http11_head_remapped1(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "HEAD", "/test-1", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "HEAD", "/test-b/", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[(CLEN,"1453")]),
		RxH1Res("G", 0, 200, "OK", &[(CLEN,"1453"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxH1Req("H", 0, "HEAD", "/test-1/", &[STD_HOST]),
		RxH1Req("I", 1, "HEAD", "/test-b/", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("J", 1, 200, "OK", &[(CLEN,"1453")]),
		RxH1Res("K", 0, 200, "OK", &[(CLEN,"1453"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxH1Req("L", 0, "HEAD", "/test-1/x", &[STD_HOST]),
		RxH1Req("M", 1, "HEAD", "/test-b/x", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("N", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"1453")]),
		RxH1Res("O", 0, 200, "OK", &[(CLEN,"1453"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxEof("P", 0),
		RxEof("Q", 0),
	]);
}

fn test_http11_head_remapped2(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "HEAD", "/test-2", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "HEAD", "/test-2", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[(CLEN,"1453")]),
		RxH1Res("G", 0, 200, "OK", &[(CLEN,"1453"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxH1Req("H", 0, "HEAD", "/test-2/", &[STD_HOST]),
		RxH1Req("I", 1, "HEAD", "/test-c/", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("J", 1, 200, "OK", &[(CLEN,"1453")]),
		RxH1Res("K", 0, 200, "OK", &[(CLEN,"1453"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxH1Req("L", 0, "HEAD", "/test-2/x", &[STD_HOST]),
		RxH1Req("M", 1, "HEAD", "/test-c/x", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("N", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"1453")]),
		RxH1Res("O", 0, 200, "OK", &[(CLEN,"1453"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxEof("P", 0),
		RxEof("Q", 0),
	]);
}

fn test_http11_head_remapped3(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "HEAD", "/test-3", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "HEAD", "/test-3", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[(CLEN,"1453")]),
		RxH1Res("G", 0, 200, "OK", &[(CLEN,"1453"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxH1Req("H", 0, "HEAD", "/test-3/", &[STD_HOST]),
		RxH1Req("I", 1, "HEAD", "/test-d/", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("J", 1, 200, "OK", &[(CLEN,"1453")]),
		RxH1Res("K", 0, 200, "OK", &[(CLEN,"1453"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxH1Req("L", 0, "HEAD", "/test-3/x", &[STD_HOST]),
		RxH1Req("M", 1, "HEAD", "/test-d/x", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("N", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"1453")]),
		RxH1Res("O", 0, 200, "OK", &[(CLEN,"1453"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxEof("P", 0),
		RxEof("Q", 0),
	]);
}

fn test_http11_head_remappedyy(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "HEAD", "/test-yy", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "HEAD", "/test-z", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[(CLEN,"1453")]),
		RxH1Res("G", 0, 200, "OK", &[(CLEN,"1453"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxH1Req("H", 0, "HEAD", "/test-yy/", &[STD_HOST]),
		RxH1Req("I", 1, "HEAD", "/test-z/", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("J", 1, 200, "OK", &[(CLEN,"1453")]),
		RxH1Res("K", 0, 200, "OK", &[(CLEN,"1453"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxH1Req("L", 0, "HEAD", "/test-yy/x", &[STD_HOST]),
		RxH1Req("M", 1, "HEAD", "/test-z/x", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("N", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"1453")]),
		RxH1Res("O", 0, 200, "OK", &[(CLEN,"1453"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxEof("P", 0),
		RxEof("Q", 0),
	]);
}

fn test_http11_head_remappedw(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "HEAD", "/test-w", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "HEAD", "/test-xx", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[(CLEN,"1453")]),
		RxH1Res("G", 0, 200, "OK", &[(CLEN,"1453"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxH1Req("H", 0, "HEAD", "/test-w/", &[STD_HOST]),
		RxH1Req("I", 1, "HEAD", "/test-xx/", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("J", 1, 200, "OK", &[(CLEN,"1453")]),
		RxH1Res("K", 0, 200, "OK", &[(CLEN,"1453"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxH1Req("L", 0, "HEAD", "/test-w/x", &[STD_HOST]),
		RxH1Req("M", 1, "HEAD", "/test-xx/x", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("N", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"1453")]),
		RxH1Res("O", 0, 200, "OK", &[(CLEN,"1453"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxEof("P", 0),
		RxEof("Q", 0),
	]);
}

fn test_http11_head_access_denied(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "HEAD", "/secret", &[STD_HOST]),
		RxH1Res("D", 0, 404, "Not found", &[(CLEN,"15"), STD_UTF8, STD_INTRES, STD_CSP, STD_XCTO]),
		TxH1Req("E", 0, "HEAD", "/secret/", &[STD_HOST]),
		RxH1Res("F", 0, 404, "Not found", &[(CLEN,"15"), STD_UTF8, STD_INTRES, STD_CSP, STD_XCTO]),
		TxH1Req("G", 0, "HEAD", "/secret/x", &[STD_HOST]),
		RxH1Res("H", 0, 404, "Not found", &[(CLEN,"15"), STD_UTF8, STD_INTRES, STD_CSP, STD_XCTO]),
		TxEof("I", 0),
		RxEof("J", 0),
	]);
}

fn test_http11_head_chunked(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "HEAD", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "HEAD", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, STD_CHUNKED]),
		RxH1Res("G", 0, 200, "OK", &[STD_BSTREAM, STD_CHUNKED, STD_XCTO, STD_CSP]),
		TxEof("H", 0),
		RxEof("I", 0),
	]);
}

fn test_http11_head_badpath(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "HEAD", "/foo[x", &[STD_HOST]),
		RxH1ErrHF("D", 0, "Illegal path: Illegal byte 91 (position 4 state 0-0)"),
	]);
}

fn test_http11_post_basic(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,(CLEN,"5")]),
		TxRaw("D", 0, b"PING!"),
		Accept("E", 1),
		RxH1Req("F", 1, "POST", "/foo", &[STD_HOST, (CLEN,"5"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		RxRaw("G", 1, b"PING!"),
		TxH1Res("H", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"6")]),
		TxRaw("I", 1, b"PONG!\n"),
		RxH1Res("J", 0, 200, "OK", &[(CLEN,"6"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("K", 0, b"PONG!\n"),
		TxEof("L", 0),
		RxEof("M", 0),
	]);
}

fn test_http11_post_empty(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,(CLEN,"0")]),
		Accept("E", 1),
		RxH1Req("F", 1, "POST", "/foo", &[STD_HOST, (CLEN,"0"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("H", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"6")]),
		TxRaw("I", 1, b"PONG!\n"),
		RxH1Res("J", 0, 200, "OK", &[(CLEN,"6"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("K", 0, b"PONG!\n"),
		TxEof("L", 0),
		RxEof("M", 0),
	]);
}

fn test_http11_post_nolength(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST]),
		Accept("E", 1),
		RxH1Req("F", 1, "POST", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("H", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"6")]),
		TxRaw("I", 1, b"PONG!\n"),
		RxH1Res("J", 0, 200, "OK", &[(CLEN,"6"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("K", 0, b"PONG!\n"),
		TxEof("L", 0),
		RxEof("M", 0),
	]);
}

fn test_http11_post_undelimited_bzip2(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,(TE,"bzip2")]),
		TxRaw("D", 0, b"PING!"),
		TxEof("E", 0),
		Accept("F", 1),
		RxH1Req("G", 1, "POST", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, (TE,"bzip2"), STD_CHUNKED, STD_VIA, STD_CDN_LOOP]),
		RxRaw("H", 1, b"5\r\nPING!\r\n"),
		RxRaw("I", 1, b"0\r\n\r\n"),
		TxH1Res("J", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"6")]),
		TxRaw("K", 1, b"PONG!\n"),
		RxH1Res("L", 0, 200, "OK", &[(CLEN,"6"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("M", 0, b"PONG!\n"),
		RxEof("N", 0),
	]);
}

fn test_http11_post_chunked(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,STD_CHUNKED]),
		TxRaw("D", 0, b"5\r\nPING!\r\n"),
		TxRaw("E", 0, b"0\r\n\r\n"),
		Accept("F", 1),
		RxH1Req("G", 1, "POST", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_CHUNKED, STD_VIA, STD_CDN_LOOP]),
		RxRaw("H", 1, b"5\r\nPING!\r\n"),
		RxRaw("I", 1, b"0\r\n\r\n"),
		TxH1Res("J", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"6")]),
		TxRaw("K", 1, b"PONG!\n"),
		RxH1Res("L", 0, 200, "OK", &[(CLEN,"6"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("M", 0, b"PONG!\n"),
		TxEof("N", 0),
		RxEof("O", 0),
	]);
}

fn test_http11_post_chunked_bzip2(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,(TE,"bzip2,chunked")]),
		TxRaw("D", 0, b"5\r\nPING!\r\n"),
		TxRaw("E", 0, b"0\r\n\r\n"),
		Accept("F", 1),
		RxH1Req("G", 1, "POST", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, (TE,"bzip2,chunked"), STD_VIA, STD_CDN_LOOP]),
		RxRaw("H", 1, b"5\r\nPING!\r\n"),
		RxRaw("I", 1, b"0\r\n\r\n"),
		TxH1Res("J", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"6")]),
		TxRaw("K", 1, b"PONG!\n"),
		RxH1Res("L", 0, 200, "OK", &[(CLEN,"6"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("M", 0, b"PONG!\n"),
		TxEof("N", 0),
		RxEof("O", 0),
	]);
}

fn test_http11_post_connection_close(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,(CLEN,"5"), STD_CONNCLOSE]),
		TxRaw("D", 0, b"PING!"),
		Accept("E", 1),
		RxH1Req("F", 1, "POST", "/foo", &[STD_HOST, (CLEN,"5"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		RxRaw("G", 1, b"PING!"),
		TxH1Res("H", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"6")]),
		TxRaw("I", 1, b"PONG!\n"),
		RxH1Res("J", 0, 200, "OK", &[(CLEN,"6"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("K", 0, b"PONG!\n"),
		RxEof("L", 0),
	]);
}

fn test_http11_post_pending_eof(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,(CLEN,"5")]),
		TxRaw("D", 0, b"PING!"),
		TxEof("E", 0),
		Accept("F", 1),
		RxH1Req("G", 1, "POST", "/foo", &[STD_HOST, (CLEN,"5"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		RxRaw("H", 1, b"PING!"),
		TxH1Res("I", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"6")]),
		TxRaw("J", 1, b"PONG!\n"),
		RxH1Res("K", 0, 200, "OK", &[(CLEN,"6"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("L", 0, b"PONG!\n"),
		RxEof("M", 0),
	]);
}

fn test_http11_post_trailers(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,STD_CHUNKED]),
		TxRaw("D", 0, b"5\r\nPING!\r\n"),
		TxRaw("E", 0, b"0\r\nzot:qux\r\n\r\n"),
		Accept("F", 1),
		RxH1Req("G", 1, "POST", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_CHUNKED, STD_VIA, STD_CDN_LOOP]),
		RxRaw("H", 1, b"5\r\nPING!\r\n"),
		RxRaw("I", 1, b"0\r\nzot:qux\r\n\r\n"),
		TxH1Res("J", 1, 200, "OK", &[STD_CONNCLOSE, STD_CHUNKED]),
		TxRaw("K", 1, b"6\r\nPONG!\n\r\n"),
		TxRaw("L", 1, b"0\r\nquux:xyzzy\r\n\r\n"),
		RxH1Res("M", 0, 200, "OK", &[STD_BSTREAM, STD_CHUNKED, STD_XCTO, STD_CSP]),
		RxRaw("N", 0, b"6\r\nPONG!\n\r\n"),
		RxRaw("O", 0, b"0\r\nquux:xyzzy\r\n\r\n"),
		TxEof("P", 0),
		RxEof("Q", 0),
	]);
}

fn test_http11_post_trailers_bad(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,STD_CHUNKED]),
		TxRaw("D", 0, b"5\r\nPING!\r\n"),
		TxRaw("E", 0, b"0\r\nzot:qux\r\n\r\n"),
		Accept("F", 1),
		RxH1Req("G", 1, "POST", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_CHUNKED, STD_VIA, STD_CDN_LOOP]),
		RxRaw("H", 1, b"5\r\nPING!\r\n"),
		RxRaw("I", 1, b"0\r\nzot:qux\r\n\r\n"),
		TxH1Res("J", 1, 200, "OK", &[STD_CONNCLOSE, STD_CHUNKED]),
		TxRaw("K", 1, b"6\r\nPONG!\n\r\n"),
		TxRaw("L", 1, b"0\r\nquux:xyzzy\r\n"),
		TxEof("M", 1),
		RxH1Res("N", 0, 200, "OK", &[STD_BSTREAM, STD_CHUNKED, STD_XCTO, STD_CSP]),
		RxRaw("O", 0, b"6\r\nPONG!\n\r\n"),
		RxEof("P", 0),
	]);
}

fn test_http11_post_trailers_bad2(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,STD_CHUNKED]),
		TxRaw("D", 0, b"5\r\nPING!\r\n"),
		TxRaw("E", 0, b"0\r\nzot:qux\r\n\r\n"),
		Accept("F", 1),
		RxH1Req("G", 1, "POST", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_CHUNKED, STD_VIA, STD_CDN_LOOP]),
		RxRaw("H", 1, b"5\r\nPING!\r\n"),
		RxRaw("I", 1, b"0\r\nzot:qux\r\n\r\n"),
		TxH1Res("J", 1, 200, "OK", &[STD_CONNCLOSE, STD_CHUNKED]),
		TxRaw("K", 1, b"6\r\nPONG!\n\r\n"),
		TxRaw("L", 1, b"0\r\nquux:xyzzy"),
		TxEof("M", 1),
		RxH1Res("N", 0, 200, "OK", &[STD_BSTREAM, STD_CHUNKED, STD_XCTO, STD_CSP]),
		RxRaw("O", 0, b"6\r\nPONG!\n\r\n"),
		RxEof("P", 0),
	]);
}

fn test_http11_post_301bug(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,(CLEN,"5")]),
		TxRaw("D", 0, b"PING!"),
		Accept("E", 1),
		RxH1Req("F", 1, "POST", "/foo", &[STD_HOST, (CLEN,"5"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		RxRaw("G", 1, b"PING!"),
		TxH1Res("H", 1, 301, "Redirect", &[STD_CONNCLOSE, (CLEN,"1"),
			("location:","https://httpdemux2.test/bar")]),
		TxRaw("I", 1, b"X"),
		RxH1Res("J", 0, 308, "Redirect", &[(CLEN,"1"),STD_BSTREAM,
			("location:","https://httpdemux2.test/bar"),STD_XCTO, STD_CSP]),
		RxRaw("K", 0, b"X"),
		//RxH1Err("I", 0, 502, "Illegal status 301 for unsafe method"),
		TxH1Req("L", 0, "GET", "/.well-known/acme-challenge/foo", &[STD_HOST]),
		RxH1Err("M", 0, 403, "ACME HTTP-01 validation not allowed"),
		TxEof("N", 0),
		RxEof("O", 0),
	]);
}

fn test_http11_post_302bug(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,(CLEN,"5")]),
		TxRaw("D", 0, b"PING!"),
		Accept("E", 1),
		RxH1Req("F", 1, "POST", "/foo", &[STD_HOST, (CLEN,"5"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		RxRaw("G", 1, b"PING!"),
		TxH1Res("H", 1, 302, "Redirect", &[STD_CONNCLOSE, (CLEN,"1"),
			("location:","https://httpdemux2.test/bar")]),
		TxRaw("I", 1, b"X"),
		RxH1Res("J", 0, 307, "Redirect", &[(CLEN,"1"),STD_BSTREAM,
			("location:","https://httpdemux2.test/bar"),STD_XCTO, STD_CSP]),
		RxRaw("K", 0, b"X"),
		TxH1Req("L", 0, "GET", "/.well-known/acme-challenge/foo", &[STD_HOST]),
		RxH1Err("M", 0, 403, "ACME HTTP-01 validation not allowed"),
		TxEof("N", 0),
		RxEof("O", 0),
	]);
}

fn test_http11_post_307nobug(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,(CLEN,"5")]),
		TxRaw("D", 0, b"PING!"),
		Accept("E", 1),
		RxH1Req("F", 1, "POST", "/foo", &[STD_HOST, (CLEN,"5"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		RxRaw("G", 1, b"PING!"),
		TxH1Res("H", 1, 307, "Redirect", &[STD_CONNCLOSE, (CLEN,"1"),
			("location:","https://httpdemux2.test/bar")]),
		TxRaw("I", 1, b"X"),
		RxH1Res("J", 0, 307, "Redirect", &[(CLEN,"1"),STD_BSTREAM,
			("location:","https://httpdemux2.test/bar"),STD_XCTO, STD_CSP]),
		RxRaw("K", 0, b"X"),
		TxEof("L", 0),
		RxEof("M", 0),
	]);
}

fn test_http11_post_308nobug(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,(CLEN,"5")]),
		TxRaw("D", 0, b"PING!"),
		Accept("E", 1),
		RxH1Req("F", 1, "POST", "/foo", &[STD_HOST, (CLEN,"5"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		RxRaw("G", 1, b"PING!"),
		TxH1Res("H", 1, 308, "Redirect", &[STD_CONNCLOSE, (CLEN,"1"),
			("location:","https://httpdemux2.test/bar")]),
		TxRaw("I", 1, b"X"),
		RxH1Res("J", 0, 308, "Redirect", &[(CLEN,"1"),STD_BSTREAM,
			("location:","https://httpdemux2.test/bar"),STD_XCTO, STD_CSP]),
		RxRaw("K", 0, b"X"),
		TxEof("L", 0),
		RxEof("M", 0),
	]);
}

fn test_http11_post_303(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,(CLEN,"5")]),
		TxRaw("D", 0, b"PING!"),
		Accept("E", 1),
		RxH1Req("F", 1, "POST", "/foo", &[STD_HOST, (CLEN,"5"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		RxRaw("G", 1, b"PING!"),
		TxH1Res("H", 1, 303, "Redirect", &[(CLEN,"1"),("location:","https://httpdemux2.test/foo")]),
		TxRaw("I", 1, b"X"),
		RxH1Res("J", 0, 303, "Redirect", &[(CLEN,"1"),STD_BSTREAM,
			("location:","https://httpdemux2.test/foo"),STD_XCTO, STD_CSP]),
		RxRaw("K", 0, b"X"),
		TxH1Req("L", 0, "GET", "/foo", &[STD_HOST]),
		RxH1Req("M", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("N", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("O", 1, b"Hello, World!\n"),
		RxH1Res("P", 0, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("Q", 0, b"Hello, World!\n"),
		TxEof("R", 0),
		RxEof("S", 0),
	]);
}

fn test_http11_post_100continue(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,(CLEN,"5"),STD_EXPECT]),
		Accept("D", 1),
		RxH1Req("E", 1, "POST", "/foo", &[STD_HOST, (CLEN,"5"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_EXPECT, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 100, "Go ahead", &[]),
		RxH1Res("G", 0, 100, "Go ahead", &[]),
		TxRaw("H", 0, b"PING!"),
		RxRaw("I", 1, b"PING!"),
		TxH1Res("J", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"6")]),
		TxRaw("K", 1, b"PONG!\n"),
		RxH1Res("L", 0, 200, "OK", &[(CLEN,"6"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("M", 0, b"PONG!\n"),
		TxEof("N", 0),
		RxEof("O", 0),
	]);
}

fn test_http11_post_100continue_redirect(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,(CLEN,"5"),STD_EXPECT]),
		Accept("D", 1),
		RxH1Req("E", 1, "POST", "/foo", &[STD_HOST, (CLEN,"5"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_EXPECT, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 307, "Redirect", &[(CLEN,"1"),("location","https://httpdemux2.test/bar")]),
		TxRaw("G", 1, b"X"),
		RxH1Res("H", 0, 307, "Redirect", &[(CLEN,"1"),STD_BSTREAM,
			("location","https://httpdemux2.test/bar"),STD_XCTO, STD_CSP]),
		RxRaw("I", 0, b"X"),
		TxRaw("J", 0, &[0;5]),
		TxH1Req("K", 0, "POST", "/bar", &[STD_HOST,(CLEN,"5"),STD_EXPECT]),
		RxRaw("L", 1, &[0;5]),
		RxH1Req("M", 1, "POST", "/bar", &[STD_HOST, (CLEN,"5"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_EXPECT, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("N", 1, 100, "Go ahead", &[]),
		RxH1Res("O", 0, 100, "Go ahead", &[]),
		TxRaw("P", 0, b"PING!"),
		RxRaw("Q", 1, b"PING!"),
		TxH1Res("R", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"6")]),
		TxRaw("S", 1, b"PONG!\n"),
		RxH1Res("T", 0, 200, "OK", &[(CLEN,"6"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("U", 0, b"PONG!\n"),
		TxEof("V", 0),
		RxEof("W", 0),
	]);
}

fn test_http11_post_100continue_redirect_chunked(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,STD_CHUNKED,STD_EXPECT]),
		Accept("D", 1),
		RxH1Req("E", 1, "POST", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_CHUNKED, STD_EXPECT, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 307, "Redirect", &[(CLEN,"1"),("location","https://httpdemux2.test/bar")]),
		TxRaw("G", 1, b"X"),
		RxH1Res("H", 0, 307, "Redirect", &[(CLEN,"1"),STD_BSTREAM,
			("location","https://httpdemux2.test/bar"),STD_XCTO, STD_CSP]),
		RxRaw("I", 0, b"X"),
		TxRaw("J", 0, b"0\r\n\r\n"),
		TxH1Req("K", 0, "POST", "/bar", &[STD_HOST,(CLEN,"5"),STD_EXPECT]),
		RxRaw("L", 1, b"0\r\n\r\n"),
		RxH1Req("M", 1, "POST", "/bar", &[STD_HOST, (CLEN,"5"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_EXPECT, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("N", 1, 100, "Go ahead", &[]),
		RxH1Res("O", 0, 100, "Go ahead", &[]),
		TxRaw("P", 0, b"PING!"),
		RxRaw("Q", 1, b"PING!"),
		TxH1Res("R", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"6")]),
		TxRaw("S", 1, b"PONG!\n"),
		RxH1Res("T", 0, 200, "OK", &[(CLEN,"6"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("U", 0, b"PONG!\n"),
		TxEof("V", 0),
		RxEof("W", 0),
	]);
}

fn test_http11_post_100continue_redirect_redial(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,(CLEN,"5"),STD_EXPECT]),
		Accept("D", 1),
		RxH1Req("E", 1, "POST", "/foo", &[STD_HOST, (CLEN,"5"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_EXPECT, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 307, "Redirect", &[(CLEN,"1"),("location","https://httpdemux2.test/bar"),
			STD_CONNCLOSE]),
		TxRaw("G", 1, b"X"),
		RxH1Res("H", 0, 307, "Redirect", &[(CLEN,"1"),STD_BSTREAM,
			("location","https://httpdemux2.test/bar"),STD_XCTO, STD_CSP]),
		RxRaw("I", 0, b"X"),
		TxRaw("J", 0, &[0;5]),
		TxH1Req("K", 0, "POST", "/bar", &[STD_HOST,(CLEN,"5"),STD_EXPECT]),
		Accept("L", 1),
		RxH1Req("M", 1, "POST", "/bar", &[STD_HOST, (CLEN,"5"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_EXPECT, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("N", 1, 100, "Go ahead", &[]),
		RxH1Res("O", 0, 100, "Go ahead", &[]),
		TxRaw("P", 0, b"PING!"),
		RxRaw("Q", 1, b"PING!"),
		TxH1Res("R", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"6")]),
		TxRaw("S", 1, b"PONG!\n"),
		RxH1Res("T", 0, 200, "OK", &[(CLEN,"6"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("U", 0, b"PONG!\n"),
		TxEof("V", 0),
		RxEof("W", 0),
	]);
}

fn test_http11_post_100continue_badclient(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,(CLEN,"5"),STD_EXPECT]),
		Accept("D", 1),
		RxH1Req("E", 1, "POST", "/foo", &[STD_HOST, (CLEN,"5"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_EXPECT, STD_VIA, STD_CDN_LOOP]),
		TxRaw("F", 0, b"PING!"),
		RxH1Err("G", 0, 400, "Client sent data before getting go-ahead"),
		TxEof("H", 0),
		RxEof("I", 0),
	]);
}

fn test_http11_post_100continue_badserver(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,(CLEN,"5"),STD_EXPECT]),
		Accept("D", 1),
		RxH1Req("E", 1, "POST", "/foo", &[STD_HOST, (CLEN,"5"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_EXPECT, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("J", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"6")]),
		TxRaw("K", 1, b"PONG!\n"),
		RxH1Err("L", 0, 502, "Illegal status 200 for expect: 100-continue"),
		TxEof("M", 0),
		RxEof("N", 0),
	]);
}

fn test_http11_post_faulty_chunk(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,STD_CHUNKED]),
		TxRaw("D", 0, b"g\r\n0123456789abcdef\r\n"),
		TxRaw("E", 0, b"0\r\n\r\n"),
		Accept("F", 1),
		RxH1Req("G", 1, "POST", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_CHUNKED, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("H", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"6")]),
		TxRaw("I", 1, b"PONG!\n"),
		RxH1ErrF("J", 0, "Illegal chunk header"),
	]);
}

fn test_http11_post_faulty_chunk2(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,STD_CHUNKED]),
		TxRaw("D", 0, b"10;\x1f\r\n0123456789abcdef\r\n"),
		TxRaw("E", 0, b"0\r\n\r\n"),
		Accept("F", 1),
		RxH1Req("G", 1, "POST", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_CHUNKED, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("H", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"6")]),
		TxRaw("I", 1, b"PONG!\n"),
		RxH1ErrF("J", 0, "Illegal byte 31"),
	]);
}

fn test_http11_post_truncated(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,(CLEN,"6")]),
		TxRaw("D", 0, b"PING!"),
		TxEof("E", 0),
		Accept("F", 1),
		RxH1Req("G", 1, "POST", "/foo", &[STD_HOST, (CLEN,"6"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		RxRaw("H", 1, b"PING!"),
		RxH1ErrF("I", 0, "Unexpected connection close"),
	]);
}

fn test_http11_post_truncated_chunked(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,STD_CHUNKED]),
		TxRaw("D", 0, b"5\r\nPING!\r\n"),
		TxEof("E", 0),
		Accept("F", 1),
		RxH1Req("G", 1, "POST", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_CHUNKED, STD_VIA, STD_CDN_LOOP]),
		RxRaw("H", 1, b"5\r\nPING!\r\n"),
		RxH1ErrF("I", 0, "Unexpected connection close"),
	]);
}

fn test_http11_post_truncated_chunked2(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,STD_CHUNKED]),
		TxRaw("D", 0, b"5\r\nPING!\r\n"),
		TxRaw("D", 0, b"5\r\n"),
		TxEof("E", 0),
		Accept("F", 1),
		RxH1Req("G", 1, "POST", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_CHUNKED, STD_VIA, STD_CDN_LOOP]),
		RxRaw("H", 1, b"5\r\nPING!\r\n"),
		RxH1ErrF("I", 0, "Unexpected connection close"),
	]);
}

fn test_http11_post_maybe_smuggled(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,STD_CHUNKED,(CLEN,"5")]),
		TxRaw("D", 0, b"5\r\nPING!\r\n"),
		TxRaw("E", 0, b"0\r\n\r\n"),
		RxH1ErrF("F", 0, "Illegal Content-Length"),
	]);
}

fn test_http11_post_maybe_smuggled2(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,(TE,"bzip2"),(CLEN,"5")]),
		TxRaw("D", 0, b"5\r\nPING!\r\n"),
		TxRaw("E", 0, b"0\r\n\r\n"),
		RxH1ErrF("F", 0, "Illegal Content-Length"),
	]);
}

fn test_http11_post_maybe_smuggled3(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,(CLEN,"5"),STD_CHUNKED]),
		TxRaw("D", 0, b"5\r\nPING!\r\n"),
		TxRaw("E", 0, b"0\r\n\r\n"),
		RxH1ErrF("F", 0, "Illegal Content-Length"),
	]);
}

fn test_http11_post_maybe_smuggled4(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,(CLEN,"5"),(TE,"bzip2")]),
		TxRaw("D", 0, b"5\r\nPING!\r\n"),
		TxRaw("E", 0, b"0\r\n\r\n"),
		RxH1ErrF("F", 0, "Illegal Content-Length"),
	]);
}

fn test_http11_connect(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxRaw("C", 0, b"CONNECT foo.bar:443 HTTP/1.1\r\nhost:foo.bar:443\r\n\r\n"),
		RxH1Err("D", 0, 400, "CONNECT method not allowed"),
		TxEof("E", 0),
		RxEof("F", 0),
	]);
}

fn test_http11_connect_like_get(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxRaw("C", 0, b"CONNECT /foo HTTP/1.1\r\nhost:httpdemux3.test\r\n\r\n"),
		RxH1Err("D", 0, 400, "CONNECT method not allowed"),
		TxEof("E", 0),
		RxEof("F", 0),
	]);
}


fn test_http11_badmeth_basic(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "G\tT", "/foo", &[STD_HOST]),
		RxH1ErrF("D", 0, "Illegal method: Illegal byte 9 (position 1)"),
		TxEof("E", 0),
		RxEof("F", 0),
	]);
}

fn test_http11_no_host(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[]),
		RxH1ErrF("D", 0, "No virtual host specified"),
	]);
}

fn test_http11_continuation(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST,("foo","bar\r\n baz")]),
		RxH1ErrF("D", 0, "Illegal continuation"),
	]);
}

fn test_http11_illegal0(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/.well-known/acme-challenge/foo", &[STD_HOST,("foo","x\x00y")]),
		RxH1ErrF("D", 0, "Illegal byte 0"),
	]);
}

fn test_http11_illegal8(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/.well-known/acme-challenge/foo", &[STD_HOST,("foo","x\x08y")]),
		RxH1ErrF("D", 0, "Illegal byte 8"),
	]);
}

fn test_http11_illegal10(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/.well-known/acme-challenge/foo", &[STD_HOST,("foo","x\x0ay")]),
		RxH1ErrF("D", 0, "Illegal line ending"),
	]);
}

fn test_http11_illegal11(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/.well-known/acme-challenge/foo", &[STD_HOST,("foo","x\x0by")]),
		RxH1ErrF("D", 0, "Illegal byte 11"),
	]);
}

fn test_http11_illegal12(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/.well-known/acme-challenge/foo", &[STD_HOST,("foo","x\x0cy")]),
		RxH1ErrF("D", 0, "Illegal byte 12"),
	]);
}

fn test_http11_illegal13(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/.well-known/acme-challenge/foo", &[STD_HOST,("foo","x\x0dy")]),
		RxH1ErrF("D", 0, "Illegal line ending"),
	]);
}

fn test_http11_illegal14(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/.well-known/acme-challenge/foo", &[STD_HOST,("foo","x\x0ey")]),
		RxH1ErrF("D", 0, "Illegal byte 14"),
	]);
}

fn test_http11_illegal31(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/.well-known/acme-challenge/foo", &[STD_HOST,("foo","x\x1fy")]),
		RxH1ErrF("D", 0, "Illegal byte 31"),
	]);
}

fn test_http11_illegal127(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/.well-known/acme-challenge/foo", &[STD_HOST,("foo","x\x7fy")]),
		RxH1ErrF("D", 0, "Illegal byte 127"),
	]);
}

fn test_http11_get_basic_500(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 500, "SERVER ERROR", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("G", 1, b"Hello, World!\n"),
		RxH1Res("H", 0, 500, "SERVER ERROR", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxRaw("I", 0, b"Hello, World!\n"),
		TxEof("J", 0),
		RxEof("K", 0),
	]);
}

fn test_http11_illegal_hname(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/.well-known/acme-challenge/foo", &[STD_HOST,("[foo","xy")]),
		RxH1ErrF("D", 0, "Illegal header name <[foo>"),
	]);
}

fn test_http11_badvers09(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxRaw("C", 0, b"GET / HTTP/0.9\r\nhost:httpdemux2.test\r\n\r\n"),
		RxH1ErrF("D", 0, "Unsupported HTTP version <HTTP/0.9>"),
	]);
}

fn test_http11_badvers20(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxRaw("C", 0, b"GET / HTTP/2.0\r\nhost:httpdemux2.test\r\n\r\n"),
		RxH1ErrF("D", 0, "Unsupported HTTP version <HTTP/2.0>"),
	]);
}

fn test_http11_badreqline(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxRaw("C", 0, b"GET /\r\nhost:httpdemux2.test\r\n\r\n"),
		RxH1ErrF("D", 0, "Illegal request line <GET />"),
	]);
}

fn test_http11_badreqline2(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxRaw("C", 0, b"GET\r\nhost:httpdemux2.test\r\n\r\n"),
		RxH1ErrF("D", 0, "Illegal request line <GET>"),
	]);
}

fn test_http11_truncated_request(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxRaw("C", 0, b"GET / HTTP/1.1\r\nhost:httpdemux2.test\r\n"),
		TxEof("D", 0),
		RxH1ErrF("E", 0, "Unexpected connection close"),
	]);
}

fn test_http11_truncated_request2(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxRaw("C", 0, b"GET / HTTP/1.1\r\nhost:httpdemux2.test\r\n\r"),
		TxEof("D", 0),
		RxH1ErrF("E", 0, "Unexpected connection close"),
	]);
}

fn test_http11_truncated_request3(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxRaw("C", 0, b"GET / HTTP/1.1\r\nhost:httpdemux2.test\r\nf"),
		TxEof("D", 0),
		RxH1ErrF("E", 0, "Unexpected connection close"),
	]);
}

fn test_http11_borderline_legal(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/.well-known/acme-challenge/foo", &[STD_HOST,("foo","x\ty")]),
		RxH1Err("D", 0, 403, "ACME HTTP-01 validation not allowed"),
		TxH1Req("E", 0, "GET", "/.well-known/acme-challenge/foo", &[STD_HOST,("foo","x y")]),
		RxH1Err("F", 0, 403, "ACME HTTP-01 validation not allowed"),
		TxH1Req("G", 0, "GET", "/.well-known/acme-challenge/foo", &[STD_HOST,("foo","x~y")]),
		RxH1Err("H", 0, 403, "ACME HTTP-01 validation not allowed"),
		TxEof("I", 0),
		RxEof("J", 0),
	]);
}

fn test_http11_backend_immediate_drop(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		DropS("E", 1),
		RxH1Err("F", 0, 502, "Connection to backend lost"),
		TxH1Req("G", 0, "GET", "/.well-known/acme-challenge/foo", &[STD_HOST]),
		RxH1Err("H", 0, 403, "ACME HTTP-01 validation not allowed"),
		TxEof("I", 0),
		RxEof("J", 0),
	]);
}

fn test_http11_backend_immediate_drop_post(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,STD_CHUNKED]),
		Accept("D", 1),
		DropS("E", 1),
		RxH1Err("F", 0, 502, "Connection to backend lost"),
		TxRaw("G", 0, b"0\r\n\r\n"),
		TxH1Req("H", 0, "GET", "/.well-known/acme-challenge/foo", &[STD_HOST]),
		RxH1Err("I", 0, 403, "ACME HTTP-01 validation not allowed"),
		TxEof("J", 0),
		RxEof("K", 0),
	]);
}

fn test_http11_backend_immediate_drop_100continue(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "POST", "/foo", &[STD_HOST,STD_CHUNKED,STD_EXPECT]),
		Accept("D", 1),
		DropS("E", 1),
		RxH1Err("F", 0, 502, "Connection to backend lost"),
		TxRaw("G", 0, b"0\r\n\r\n"),
		TxH1Req("H", 0, "GET", "/.well-known/acme-challenge/foo", &[STD_HOST]),
		RxH1Err("I", 0, 403, "ACME HTTP-01 validation not allowed"),
		TxEof("J", 0),
		RxEof("K", 0),
	]);
}

fn test_http11_backend_invalid_statusline1(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxRaw("F", 1, b"HTTP/1.1 200\r\nconnection: close\r\ncontent-length:14\r\n\r\n"),
		TxRaw("G", 1, b"Hello, World!\n"),
		RxH1Err("H", 0, 502, "Connection error: Illegal status line"),
		TxEof("I", 0),
		RxEof("J", 0),
	]);
}

fn test_http11_backend_invalid_statusline2(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxRaw("F", 1, b"HTTP/1.1\r\nconnection: close\r\ncontent-length:14\r\n\r\n"),
		TxRaw("G", 1, b"Hello, World!\n"),
		RxH1Err("H", 0, 502, "Connection error: Illegal status line"),
		TxEof("I", 0),
		RxEof("J", 0),
	]);
}

fn test_http11_backend_bad_version09(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxRaw("F", 1, b"HTTP/0.9 200 OK\r\nconnection: close\r\ncontent-length:14\r\n\r\n"),
		TxRaw("G", 1, b"Hello, World!\n"),
		RxH1Err("H", 0, 502, "Connection error: HTTP/0.9 is not supported"),
		TxEof("I", 0),
		RxEof("J", 0),
	]);
}

fn test_http11_backend_bad_version20(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxRaw("F", 1, b"HTTP/2.0 200 OK\r\nconnection: close\r\ncontent-length:14\r\n\r\n"),
		TxRaw("G", 1, b"Hello, World!\n"),
		RxH1Err("H", 0, 502, "Connection error: Unsupported HTTP version"),
		TxEof("I", 0),
		RxEof("J", 0),
	]);
}

fn test_http11_backend_incomplete_headers1(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxRaw("F", 1, b"HTTP/1.1 200 OK\r\nconnection: close\r\ncontent-length:14\r\n\r"),
		TxEof("G", 1),
		RxH1Err("H", 0, 502, "Connection to backend lost"),
		TxEof("I", 0),
		RxEof("J", 0),
	]);
}

fn test_http11_backend_incomplete_headers2(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxRaw("F", 1, b"HTTP/1.1 200 OK\r\nconnection: close\r\ncontent-length:14\r\n"),
		TxEof("G", 1),
		RxH1Err("H", 0, 502, "Connection to backend lost"),
		TxEof("I", 0),
		RxEof("J", 0),
	]);
}

fn test_http11_websocket_required(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 426, "Only websocket supported", &[STD_CONNCLOSE, (CLEN,"14"),
			("upgrade", "websocket,foo"), STD_CONNUPGD]),
		TxRaw("G", 1, b"Hello, World!\n"),
		RxH1Res("H", 0, 426, "Only websocket supported", &[(CLEN,"14"), STD_BSTREAM,
			("upgrade", "websocket,foo"), STD_XCTO, STD_CSP]),
		RxRaw("I", 0, b"Hello, World!\n"),
		TxEof("J", 0),
		RxEof("K", 0),
	]);
}

fn test_http11_websocket_basic(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST, STD_UPGDS, STD_CONNUPGD]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_WEBSOCKET, STD_CONNUPGD, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 101, "Upgrading to websocket", &[STD_CONNCLOSE, STD_WEBSOCKET, STD_CONNUPGD]),
		TxRaw("G", 1, b"Hello, World!\n"),
		RxH1Res("H", 0, 101, "Upgrading to websocket", &[STD_WEBSOCKET, STD_CONNUPGD]),
		TxRaw("I", 0, b"hello, world!\n"),
		RxRaw("J", 0, b"Hello, World!\n"),
		RxRaw("K", 1, b"hello, world!\n"),
		TxEof("L", 0),
		RxEof("M", 1),
		TxEof("N", 1),
		RxEof("O", 0),
	]);
}

fn test_http11_websocket_forwarding1(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST, STD_UPGDS, STD_CONNUPGD]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_WEBSOCKET, STD_CONNUPGD, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 101, "Upgrading to websocket", &[STD_CONNCLOSE, STD_WEBSOCKET, STD_CONNUPGD]),
		RxH1Res("G", 0, 101, "Upgrading to websocket", &[STD_WEBSOCKET, STD_CONNUPGD]),
		TxRaw("H", 1, b"XYZZY"),
		TxRaw("I", 0, b"foobar"),
		RxRaw("J", 0, b"XYZZY"),
		RxRaw("K", 1, b"foobar"),
		TxRaw("H", 1, b"zot"),
		TxRaw("I", 0, b"quux"),
		RxRaw("J", 0, b"zot"),
		RxRaw("K", 1, b"quux"),
		TxEof("L", 0),
		RxEof("M", 1),
		TxRaw("H", 1, b"zot2"),
		RxRaw("J", 0, b"zot2"),
		TxEof("N", 1),
		RxEof("O", 0),
	]);
}

fn test_http11_websocket_forwarding2(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/foo", &[STD_HOST, STD_UPGDS, STD_CONNUPGD]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_WEBSOCKET, STD_CONNUPGD, STD_VIA, STD_CDN_LOOP]),
		TxH1Res("F", 1, 101, "Upgrading to websocket", &[STD_CONNCLOSE, STD_WEBSOCKET, STD_CONNUPGD]),
		RxH1Res("G", 0, 101, "Upgrading to websocket", &[STD_WEBSOCKET, STD_CONNUPGD]),
		TxRaw("H", 1, b"XYZZY"),
		TxRaw("I", 0, b"foobar"),
		RxRaw("J", 0, b"XYZZY"),
		RxRaw("K", 1, b"foobar"),
		TxRaw("H", 1, b"zot"),
		TxRaw("I", 0, b"quux"),
		RxRaw("J", 0, b"zot"),
		RxRaw("K", 1, b"quux"),
		TxEof("L", 1),
		RxEof("M", 0),
		TxRaw("H", 0, b"zot2"),
		RxRaw("J", 1, b"zot2"),
		TxEof("N", 0),
		RxEof("O", 1),
	]);
}

fn test_http11_https_insecure(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxyInsecure("B", 0),
		TxRaw("C", 0, b"GET https://httpdemux2.test/ HTTP/1.1\r\nhost:\r\n\r\n"),
		RxH1Err("D", 0, 400, "Unexpected scheme in request"),
		TxRaw("E", 0, b"GET https://httpdemux2.test/ HTTP/1.1\r\nhost:\r\n\r\n"),
		RxH1Err("F", 0, 400, "Unexpected scheme in request"),
		TxEof("G", 0),
		RxEof("H", 0),
	]);
}

fn test_http11_http_secure(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxRaw("C", 0, b"GET http://httpdemux2.test/ HTTP/1.1\r\nhost:\r\n\r\n"),
		RxH1Err("D", 0, 400, "Unexpected scheme in request"),
		TxRaw("E", 0, b"GET http://httpdemux2.test/ HTTP/1.1\r\nhost:\r\n\r\n"),
		RxH1Err("F", 0, 400, "Unexpected scheme in request"),
		TxEof("G", 0),
		RxEof("H", 0),
	]);
}

fn test_http11_http_hstspreload(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxyInsecure("B", 0),
		TxRaw("C", 0, b"GET http://httpdemux2.test.app/ HTTP/1.1\r\nHost:\r\n\r\n"),
		RxH1Err("D", 0, 400, "Illegal insecure access"),
		TxRaw("E", 0, b"GET http://httpdemux2.test.app/ HTTP/1.1\r\nHost:\r\n\r\n"),
		RxH1Err("F", 0, 400, "Illegal insecure access"),
		TxEof("G", 0),
		RxEof("H", 0),
	]);
}

fn test_http11_https_hstspreload(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxRaw("C", 0, b"GET https://httpdemux2.test.app/ HTTP/1.1\r\nhost:\r\n\r\n"),
		RxH1Err("D", 0, 421, "Unknown virtual host"),
		TxRaw("E", 0, b"GET https://httpdemux2.test.app/ HTTP/1.1\r\nhost:\r\n\r\n"),
		RxH1Err("F", 0, 421, "Unknown virtual host"),
		TxEof("G", 0),
		RxEof("H", 0),
	]);
}

fn test_http11_too_many_headers(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxRaw("C", 0, b"GET https://httpdemux2.test/ HTTP/1.1\r\nhost:\r\n"),
		TxTooManyHeaders("D", 0),
		TxRaw("E", 0, b"\r\n"),
		RxH1Err("F", 0, 400, "Too many headers"),
		TxRaw("G", 0, b"GET https://httpdemux3.test/ HTTP/1.1\r\nhost:\r\n\r\n"),
		RxH1Err("H", 0, 421, "Unknown virtual host"),
		TxEof("I", 0),
		RxEof("J", 0),
	]);
}

fn test_http11_too_big_headers(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxRaw("C", 0, b"GET https://httpdemux2.test/ HTTP/1.1\r\nhost:\r\n"),
		TxTooBigHeaders("D", 0),
		TxRaw("E", 0, b"\r\n"),
		RxH1Err("F", 0, 400, "Headers too big"),
		TxRaw("G", 0, b"GET https://httpdemux3.test/ HTTP/1.1\r\nhost:\r\n\r\n"),
		RxH1Err("H", 0, 421, "Unknown virtual host"),
		TxEof("I", 0),
		RxEof("J", 0),
	]);
}


fn test_http2_establish_connection(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxEof("C", 0),
		RxEof("D", 0),
	]);
}

fn test_http2_ping(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxRaw("C", 0, &[0, 0, 8, 6, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8]),
		RxRaw("D", 0, &[0, 0, 8, 6, 1, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8]),
		TxEof("E", 0),
		RxEof("F", 0),
	]);
}

fn test_http2_get_simple(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, true, "GET", "/foo", &[]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("G", 1, b"Hello, World!\n"),
		RxH2Res("H", 0, 1, false, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxH2Data("I", 0, 1, false, b"Hello, World!\n"),
		RxH2Data("J", 0, 1, true, b""),
		TxEof("K", 0),
		RxEof("L", 0),
	]);
}

fn test_http2_get_looping(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, true, "GET", "/foo", &[STD_CDN_LOOP]),
		RxH2Res("D", 0, 1, false, 500, "Request is looping through proxy", &[STD_UTF8, STD_INTRES, STD_CSP,
			STD_XCTO]),
		RxH2Data("E", 0, 1, true, b"500 Request is looping through proxy\r\n"),
		TxEof("F", 0),
		RxEof("G", 0),
	]);
}

fn test_http2_get_query(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, true, "GET", "/foo?bar", &[]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo?bar", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("G", 1, b"Hello, World!\n"),
		RxH2Res("H", 0, 1, false, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxH2Data("I", 0, 1, false, b"Hello, World!\n"),
		RxH2Data("J", 0, 1, true, b""),
		TxEof("K", 0),
		RxEof("L", 0),
	]);
}

fn test_http2_get_badpath(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, true, "GET", "/foo[x", &[]),
		RxH2Res("D", 0, 1, false, 400, "Illegal path: Illegal byte 91 (position 4 state 0-0)", &[STD_UTF8,
			STD_INTRES, STD_CSP, STD_XCTO]),
		RxH2Data("E", 0, 1, true, b"400 Illegal path: Illegal byte 91 (position 4 state 0-0)\r\n"),
		TxEof("F", 0),
		RxEof("G", 0),
	]);
}

fn test_http2_get_chunked(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, true, "GET", "/foo", &[]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, STD_CHUNKED]),
		TxRaw("G", 1, b"e\r\nHello, World!\n\r\n"),
		TxRaw("H", 1, b"0\r\n\r\n"),
		RxH2Res("I", 0, 1, false, 200, "OK", &[STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxH2Data("J", 0, 1, false, b"Hello, World!\n"),
		RxH2Data("K", 0, 1, true, b""),
		TxEof("L", 0),
		RxEof("M", 0),
	]);
}

fn test_http2_get_chunked_trailers(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, true, "GET", "/foo", &[]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, STD_CHUNKED]),
		TxRaw("G", 1, b"e\r\nHello, World!\n\r\n"),
		TxRaw("H", 1, b"0\r\nzot:xyzzy\r\n\r\n"),
		RxH2Res("I", 0, 1, false, 200, "OK", &[STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxH2Data("J", 0, 1, false, b"Hello, World!\n"),
		RxH2Trailers("K", 0, 1, &[("zot", "xyzzy")]),
		TxEof("L", 0),
		RxEof("M", 0),
	]);
}

fn test_http2_get_chunked_bzip2(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, true, "GET", "/foo", &[]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, (TE, "bzip2,chunked")]),
		TxRaw("G", 1, b"e\r\nHello, World!\n\r\n"),
		TxRaw("H", 1, b"0\r\n\r\n"),
		RxRaw("I", 0, &http2_reset(1, 13)),
		TxEof("L", 0),
		RxEof("M", 0),
	]);
}

fn test_http2_get_sequential_1b(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, true, "GET", "/foo", &[]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[(CLEN,"14")]),
		TxRaw("G", 1, b"Hello, World!\n"),
		RxH2Res("H", 0, 1, false, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxH2Data("I", 0, 1, false, b"Hello, World!\n"),
		RxH2Data("J", 0, 1, true, b""),
		TxH2Req("K", 0, 3, true, "GET", "/bar", &[]),
		RxH1Req("L", 1, "GET", "/bar", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		TxH1Res("M", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("N", 1, b"hello, world!\n"),
		RxH2Res("O", 0, 3, false, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxH2Data("P", 0, 3, false, b"hello, world!\n"),
		RxH2Data("Q", 0, 3, true, b""),
		TxEof("R", 0),
		RxEof("S", 0),
	]);
}

fn test_http2_get_sequential_2b(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, true, "GET", "/foo", &[]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("G", 1, b"Hello, World!\n"),
		RxH2Res("H", 0, 1, false, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxH2Data("I", 0, 1, false, b"Hello, World!\n"),
		RxH2Data("J", 0, 1, true, b""),
		TxH2Req("K", 0, 3, true, "GET", "/bar", &[]),
		Accept("L", 1),
		RxH1Req("M", 1, "GET", "/bar", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		TxH1Res("N", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("O", 1, b"hello, world!\n"),
		RxH2Res("P", 0, 3, false, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxH2Data("Q", 0, 3, false, b"hello, world!\n"),
		RxH2Data("R", 0, 3, true, b""),
		TxEof("S", 0),
		RxEof("T", 0),
	]);
}

fn test_http2_get_parallel(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, true, "GET", "/foo", &[]),
		Accept("D", 1),
		TxH2Req("E", 0, 3, true, "GET", "/bar", &[]),
		Accept("F", 2),
		RxH1Req("G", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		RxH1Req("H", 2, "GET", "/bar", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		TxH1Res("I", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxH1Res("J", 2, 200, "OK", &[STD_CONNCLOSE, (CLEN,"16")]),
		RxH2Res("K", 0, 1, false, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxH2Res("L", 0, 3, false, 200, "OK", &[(CLEN,"16"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxRaw("M", 1, b"Hello, World!\n"),
		TxRaw("N", 2, b"hello, world!!!\n"),
		RxH2Data("O", 0, 1, false, b"Hello, World!\n"),
		RxH2Data("Q", 0, 1, true, b""),
		RxH2Data("P", 0, 3, false, b"hello, world!!!\n"),
		RxH2Data("R", 0, 3, true, b""),
		TxEof("S", 0),
		RxEof("T", 0),
	]);
}

fn test_http2_get_parallel_backendlost(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, true, "GET", "/foo", &[]),
		Accept("D", 1),
		TxH2Req("E", 0, 3, true, "GET", "/bar", &[]),
		Accept("F", 2),
		RxH1Req("G", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		DropS("H", 2),
		TxH1Res("I", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		RxH2Res("J", 0, 3, false, 502, "Connection to backend lost", &[STD_UTF8, STD_INTRES, STD_CSP,
			STD_XCTO]),
		RxH2Data("K", 0, 3, true, b"502 Connection to backend lost\r\n"),
		RxH2Res("L", 0, 1, false, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxRaw("M", 1, b"Hello, World!\n"),
		RxH2Data("N", 0, 1, false, b"Hello, World!\n"),
		RxH2Data("O", 0, 1, true, b""),
		TxEof("P", 0),
		RxEof("Q", 0),
	]);
}

fn test_http2_get_parallel_backendlost_intransfer(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, true, "GET", "/foo", &[]),
		Accept("D", 1),
		TxH2Req("E", 0, 3, true, "GET", "/bar", &[]),
		Accept("F", 2),
		RxH1Req("G", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		RxH1Req("H", 2, "GET", "/bar", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		TxH1Res("I", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxH1Res("J", 2, 200, "OK", &[STD_CONNCLOSE, (CLEN,"16")]),
		RxH2Res("K", 0, 1, false, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxH2Res("L", 0, 3, false, 200, "OK", &[(CLEN,"16"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxRaw("M", 1, b"Hello, World!\n"),
		TxRaw("N", 2, b"hello, world!\n"),
		DropS("O", 2),
		RxH2Data("P", 0, 1, false, b"Hello, World!\n"),
		RxH2Data("Q", 0, 1, true, b""),
		RxH2Data("R", 0, 3, false, b"hello, world!\n"),
		RxRaw("S", 0, &http2_reset(3, 2)),
		TxEof("T", 0),
		RxEof("U", 0),
	]);
}

fn test_http2_get_illegal_headers(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, true, "GET", "/foo", &[("[foo", "bar")]),
		RxH2Res("D", 0, 1, false, 400, "Illegal header name byte 91", &[STD_UTF8, STD_INTRES, STD_CSP,
			STD_XCTO]),
		RxH2Data("E", 0, 1, true, b"400 Illegal header name byte 91\r\n"),
		TxH2Req("F", 0, 3, true, "GET", "/foo", &[("foo", "bar\x1fzot")]),
		RxH2Res("G", 0, 3, false, 400, "Illegal header value byte 31", &[STD_UTF8, STD_INTRES, STD_CSP,
			STD_XCTO]),
		RxH2Data("H", 0, 3, true, b"400 Illegal header value byte 31\r\n"),
		TxH2Req("I", 0, 5, true, "GET", "/foo", &[("foo", "bar\x0d\x0a zot")]),
		RxH2Res("J", 0, 5, false, 400, "Illegal header value byte 13", &[STD_UTF8, STD_INTRES, STD_CSP,
			STD_XCTO]),
		RxH2Data("K", 0, 5, true, b"400 Illegal header value byte 13\r\n"),
		TxEof("L", 0),
		RxEof("M", 0),
	]);
}

fn test_http2_head_simple(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, true, "HEAD", "/foo", &[]),
		Accept("D", 1),
		RxH1Req("E", 1, "HEAD", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		RxH2Res("G", 0, 1, true, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxEof("H", 0),
		RxEof("I", 0),
	]);
}

fn test_http2_head_no_authority(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Trailers("C", 0, 1, &[(":method","HEAD"),(":scheme","https"),(":path","/foo")]),
		RxH2Res("D", 0, 1, true, 400, "No authority", &[STD_UTF8, STD_INTRES, STD_CSP, STD_XCTO]),
		TxEof("E", 0),
		RxEof("F", 0),
	]);
}

fn test_http2_head_http_scheme(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Trailers("C", 0, 1, &[(":method","HEAD"),(":scheme","http"),(":authority","httpdemux2.test"),
			(":path","/foo")]),
		RxH2Res("D", 0, 1, true, 400, "Unexpected scheme in request", &[STD_UTF8, STD_INTRES, STD_CSP,
			STD_XCTO]),
		TxEof("E", 0),
		RxEof("F", 0),
	]);
}

fn test_http2_head_legacy_host(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Trailers("C", 0, 1, &[(":method","HEAD"),(":scheme","https"),(":path","/foo"),STD_HOST]),
		Accept("D", 1),
		RxH1Req("E", 1, "HEAD", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		TxH1Res("F", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		RxH2Res("G", 0, 1, true, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		TxEof("H", 0),
		RxEof("I", 0),
	]);
}

fn test_http2_head_authority_mismatch(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, true, "HEAD", "/foo", &[("host","httpdemux3.test")]),
		RxH2Res("D", 0, 1, true, 400, "Illegal authority: Inconsistent with previous value", &[STD_UTF8,
			STD_INTRES, STD_CSP, STD_XCTO]),
		TxEof("E", 0),
		RxEof("F", 0),
	]);
}

fn test_http2_post_simple(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, false, "POST", "/foo", &[]),
		Accept("D", 1),
		TxH2Data("E", 0, 1, false, b"PING!"),
		TxH2Data("F", 0, 1, true, b""),
		RxH1Req("G", 1, "POST", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_CHUNKED, STD_VIA2, STD_CDN_LOOP]),
		RxRaw("H", 1, b"5\r\nPING!\r\n"),
		RxRaw("I", 1, b"0\r\n\r\n"),
		TxH1Res("J", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("K", 1, b"Hello, World!\n"),
		RxRaw("L", 0, &[0, 0, 4, 8, 0, 0, 0, 0, 1, 59, 154, 202, 0]),	//Window adjust.
		RxH2Res("M", 0, 1, false, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxH2Data("N", 0, 1, false, b"Hello, World!\n"),
		RxH2Data("O", 0, 1, true, b""),
		TxEof("P", 0),
		RxEof("Q", 0),
	]);
}

fn test_http2_post_trailers(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, false, "POST", "/foo", &[]),
		Accept("D", 1),
		TxH2Data("E", 0, 1, false, b"PING!"),
		TxH2Trailers("F", 0, 1, &[("foo","bar")]),
		RxH1Req("G", 1, "POST", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_CHUNKED, STD_VIA2, STD_CDN_LOOP]),
		RxRaw("H", 1, b"5\r\nPING!\r\n"),
		RxRaw("I", 1, b"0\r\nfoo:bar\r\n\r\n"),
		TxH1Res("J", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("K", 1, b"Hello, World!\n"),
		RxRaw("L", 0, &[0, 0, 4, 8, 0, 0, 0, 0, 1, 59, 154, 202, 0]),	//Window adjust.
		RxH2Res("M", 0, 1, false, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxH2Data("N", 0, 1, false, b"Hello, World!\n"),
		RxH2Data("O", 0, 1, true, b""),
		TxEof("P", 0),
		RxEof("Q", 0),
	]);
}

fn test_http2_post_length(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, false, "POST", "/foo", &[(CLEN,"5")]),
		Accept("D", 1),
		TxH2Data("E", 0, 1, true, b"PING!"),
		RxH1Req("F", 1, "POST", "/foo", &[STD_HOST, (CLEN,"5"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		RxRaw("G", 1, b"PING!"),
		TxH1Res("H", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("I", 1, b"Hello, World!\n"),
		RxRaw("J", 0, &[0, 0, 4, 8, 0, 0, 0, 0, 1, 59, 154, 202, 0]),	//Window adjust.
		RxH2Res("K", 0, 1, false, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxH2Data("L", 0, 1, false, b"Hello, World!\n"),
		RxH2Data("M", 0, 1, true, b""),
		TxEof("N", 0),
		RxEof("O", 0),
	]);
}

fn test_http2_post_length_tdiscard(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, false, "POST", "/foo", &[(CLEN,"5")]),
		Accept("D", 1),
		TxH2Data("E", 0, 1, false, b"PING!"),
		TxH2Trailers("F", 0, 1, &[("zot", "xyzzy")]),
		RxH1Req("G", 1, "POST", "/foo", &[STD_HOST, (CLEN,"5"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		RxRaw("H", 1, b"PING!"),
		TxH1Res("I", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("J", 1, b"Hello, World!\n"),
		RxRaw("K", 0, &[0, 0, 4, 8, 0, 0, 0, 0, 1, 59, 154, 202, 0]),	//Window adjust.
		RxH2Res("L", 0, 1, false, 200, "OK", &[(CLEN,"14"), STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxH2Data("M", 0, 1, false, b"Hello, World!\n"),
		RxH2Data("N", 0, 1, true, b""),
		TxEof("O", 0),
		RxEof("P", 0),
	]);
}

fn test_http2_post_length_bad(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, false, "POST", "/foo", &[(CLEN,"6")]),
		Accept("D", 1),
		TxH2Data("E", 0, 1, true, b"PING!"),
		RxH1Req("F", 1, "POST", "/foo", &[STD_HOST, (CLEN,"6"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		RxRaw("G", 1, b"PING!"),
		RxRaw("J", 0, &[0, 0, 4, 8, 0, 0, 0, 0, 1, 59, 154, 202, 0]),	//Window adjust.
		RxH2Res("K", 0, 1, false, 400, "Content-Length does not match data length", &[STD_UTF8,
			STD_INTRES, STD_CSP, STD_XCTO]),
		RxH2Data("L", 0, 1, true, b"400 Content-Length does not match data length\r\n"),
		TxEof("M", 0),
		RxEof("N", 0),
	]);
}

fn test_http2_post_length_bad2(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, false, "POST", "/foo", &[(CLEN,"9")]),
		Accept("D", 1),
		TxH2Data("E", 0, 1, false, b"PING!"),
		RxH1Req("F", 1, "POST", "/foo", &[STD_HOST, (CLEN,"9"), STD_XFFOR, STD_XFHOST, STD_XFPROTO,
			STD_XFSECURE, STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		RxRaw("G", 1, b"PING!"),
		TxH2Data("E", 0, 1, true, b"ping!"),
		RxRaw("H", 0, &[0, 0, 4, 8, 0, 0, 0, 0, 1, 59, 154, 202, 0]),	//Window adjust.
		RxH2Res("I", 0, 1, false, 400, "Content-Length does not match data length", &[STD_UTF8,
			STD_INTRES, STD_CSP, STD_XCTO]),
		RxH2Data("J", 0, 1, true, b"400 Content-Length does not match data length\r\n"),
		TxEof("K", 0),
		RxEof("L", 0),
	]);
}

fn test_http2_post_301(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, true, "POST", "/foo", &[]),
		Accept("D", 1),
		RxH1Req("E", 1, "POST", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		TxH1Res("F", 1, 301, "Redirecting", &[STD_CONNCLOSE, (CLEN,"14"),
			("location","https://httpdemux2.test/bar")]),
		TxRaw("G", 1, b"Hello, World!\n"),
		//RxH2Res("H", 0, 1, false, 502, "Illegal status 301 for unsafe method", &[STD_UTF8,
		//	STD_INTRES, STD_CSP, STD_XCTO]),
		RxH2Res("H", 0, 1, false, 308, "Redirecting", &[(CLEN,"14"),STD_BSTREAM,
			("location","https://httpdemux2.test/bar"),STD_XCTO, STD_CSP]),
		RxH2Data("I", 0, 1, false, b"Hello, World!\n"),
		RxH2Data("J", 0, 1, true, b""),
		//RxH2Data("I", 0, 1, true, b"502 Illegal status 301 for unsafe method\r\n"),
		TxEof("K", 0),
		RxEof("L", 0),
	]);
}

fn test_http2_post_maybe_smuggled(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, false, "POST", "/foo", &[STD_CHUNKED,(CLEN,"5")]),
		TxH2Data("E", 0, 1, true, b"PING!"),
		RxH2Res("D", 0, 1, false, 400, "Illegal chunked transfer-encoding", &[STD_UTF8,
			STD_INTRES, STD_CSP, STD_XCTO]),
		RxH2Data("L", 0, 1, true, b"400 Illegal chunked transfer-encoding\r\n"),
		TxEof("N", 0),
		//RxEof("O", 0),
	]);
}

fn test_http2_post_maybe_smuggled2(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, false, "POST", "/foo", &[(TE,"bzip2"),(CLEN,"5")]),
		TxH2Data("E", 0, 1, true, b"PING!"),
		RxH2Res("D", 0, 1, false, 400, "Illegal Content-Length", &[STD_UTF8,
			STD_INTRES, STD_CSP, STD_XCTO]),
		RxH2Data("L", 0, 1, true, b"400 Illegal Content-Length\r\n"),
		TxEof("N", 0),
		//RxEof("O", 0),
	]);
}

fn test_http2_post_maybe_smuggled3(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, false, "POST", "/foo", &[(CLEN,"5"),STD_CHUNKED]),
		TxH2Data("E", 0, 1, true, b"PING!"),
		RxH2Res("D", 0, 1, false, 400, "Illegal chunked transfer-encoding", &[STD_UTF8,
			STD_INTRES, STD_CSP, STD_XCTO]),
		RxH2Data("L", 0, 1, true, b"400 Illegal chunked transfer-encoding\r\n"),
		TxEof("N", 0),
		//RxEof("O", 0),
	]);
}

fn test_http2_post_maybe_smuggled4(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, false, "POST", "/foo", &[(CLEN,"5"),(TE,"bzip2")]),
		TxH2Data("E", 0, 1, true, b"PING!"),
		RxH2Res("D", 0, 1, false, 400, "Illegal Content-Length", &[STD_UTF8,
			STD_INTRES, STD_CSP, STD_XCTO]),
		RxH2Data("L", 0, 1, true, b"400 Illegal Content-Length\r\n"),
		TxEof("N", 0),
		//RxEof("O", 0),
	]);
}

fn test_http2_websocket(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, false, "CONNECT", "/foo", &[(":protocol","websocket")]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_WEBSOCKET, STD_CONNUPGD,
			("sec-websocket-key", "0000000000000000000000=="), STD_VIA2, STD_CDN_LOOP]),
		TxH1Res("F", 1, 101, "Upgrading", &[STD_CONNUPGD, STD_WEBSOCKET,
			("sec-websocket-accept", "zzzzzzzzzzzzzzzzzzzzzz==")]),
		TxRaw("G", 1, b"Hello, World!\n"),
		RxRaw("H", 0, &[0, 0, 4, 8, 0, 0, 0, 0, 1, 59, 154, 202, 0]),	//Window adjust.
		RxH2Res("I", 0, 1, false, 200, "Upgrading", &[]),
		RxH2Data("J", 0, 1, false, b"Hello, World!\n"),
		TxH2Data("K", 0, 1, false, b"QUIT\n"),
		RxRaw("L", 1, b"QUIT\n"),
		TxH2Data("M", 0, 1, true, b""),
		TxEof("N", 1),
		RxEof("O", 1),
		RxH2Data("P", 0, 1, true, b""),
		TxEof("Q", 0),
		RxEof("R", 0),
	]);
}

fn test_http2_websocket_halfclose1(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, false, "CONNECT", "/foo", &[(":protocol","websocket")]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_WEBSOCKET, STD_CONNUPGD,
			("sec-websocket-key", "0000000000000000000000=="), STD_VIA2, STD_CDN_LOOP]),
		TxH1Res("F", 1, 101, "Upgrading", &[STD_CONNUPGD, STD_WEBSOCKET,
			("sec-websocket-accept", "zzzzzzzzzzzzzzzzzzzzzz==")]),
		RxRaw("G", 0, &[0, 0, 4, 8, 0, 0, 0, 0, 1, 59, 154, 202, 0]),	//Window adjust.
		RxH2Res("H", 0, 1, false, 200, "Upgrading", &[]),
		TxRaw("I", 1, b"FOO\n"),
		TxH2Data("J", 0, 1, false, b"BAR\n"),
		RxRaw("K", 1, b"BAR\n"),
		RxH2Data("L", 0, 1, false, b"FOO\n"),
		TxRaw("M", 1, b"FOO!\n"),
		TxH2Data("N", 0, 1, false, b"BAR!\n"),
		RxRaw("O", 1, b"BAR!\n"),
		RxH2Data("P", 0, 1, false, b"FOO!\n"),
		TxH2Data("Q", 0, 1, true, b"BAR!!\n"),
		RxRaw("R", 1, b"BAR!!\n"),
		RxEof("S", 1),
		TxRaw("T", 1, b"FOO!!\n"),
		RxH2Data("U", 0, 1, false, b"FOO!!\n"),
		TxEof("V", 1),
		RxH2Data("W", 0, 1, true, b""),
		TxEof("X", 0),
		RxEof("Y", 0),
	]);
}

fn test_http2_websocket_halfclose2(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, false, "CONNECT", "/foo", &[(":protocol","websocket")]),
		Accept("D", 1),
		RxH1Req("E", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_WEBSOCKET, STD_CONNUPGD,
			("sec-websocket-key", "0000000000000000000000=="), STD_VIA2, STD_CDN_LOOP]),
		TxH1Res("F", 1, 101, "Upgrading", &[STD_CONNUPGD, STD_WEBSOCKET,
			("sec-websocket-accept", "zzzzzzzzzzzzzzzzzzzzzz==")]),
		RxRaw("G", 0, &[0, 0, 4, 8, 0, 0, 0, 0, 1, 59, 154, 202, 0]),	//Window adjust.
		RxH2Res("H", 0, 1, false, 200, "Upgrading", &[]),
		TxRaw("I", 1, b"FOO\n"),
		TxH2Data("J", 0, 1, false, b"BAR\n"),
		RxRaw("K", 1, b"BAR\n"),
		RxH2Data("L", 0, 1, false, b"FOO\n"),
		TxRaw("M", 1, b"FOO!\n"),
		TxH2Data("N", 0, 1, false, b"BAR!\n"),
		RxRaw("O", 1, b"BAR!\n"),
		RxH2Data("P", 0, 1, false, b"FOO!\n"),
		TxRaw("Q", 1, b"FOO!!\n"),
		RxH2Data("R", 0, 1, false, b"FOO!!\n"),
		TxEof("S", 1),
		RxH2Data("T", 0, 1, true, b""),
		TxH2Data("U", 0, 1, true, b"BAR!!\n"),
		RxRaw("V", 1, b"BAR!!\n"),
		RxEof("W", 1),
		TxEof("X", 0),
		RxEof("Y", 0),
	]);
}

fn test_http2_illegal_connect(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, true, "CONNECT", "/foo", &[]),
		RxH2Res("D", 0, 1, false, 400, "CONNECT method not allowed", &[STD_UTF8, STD_INTRES, STD_CSP,
			STD_XCTO]),
		RxH2Data("E", 0, 1, true, b"400 CONNECT method not allowed\r\n"),
		TxH2Req("F", 0, 3, true, "CONNECT", "/foo", &[(":protocol", "foo")]),
		RxRaw("G", 0, &http2_reset(3, 10)),
		TxEof("L", 0),
		RxEof("M", 0),
	]);
}

fn test_http2_two_headers(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, false, "GET", "/foo", &[]),
		Wait,
		TxH2Req("D", 0, 1, false, "GET", "/foo", &[]),
		Accept("E", 1),
		RxH1Req("F", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_CHUNKED, STD_VIA2, STD_CDN_LOOP]),
		TxH1Res("G", 1, 200, "OK", &[STD_CONNCLOSE, (CLEN,"14")]),
		TxRaw("H", 1, b"Hello, World!\n"),
		RxRaw("I", 0, &[0, 0, 4, 8, 0, 0, 0, 0, 1, 59, 154, 202, 0]),	//Window adjust.

		RxH2Res("J", 0, 1, false, 400, "Illegal header after final headers", &[STD_UTF8, STD_INTRES,
			STD_CSP, STD_XCTO]),
		RxH2Data("K", 0, 1, true, b"400 Illegal header after final headers\r\n"),
		RxRaw("L", 0, &http2_reset(1, 2)),
		TxEof("M", 1),
		TxEof("N", 0),
		RxEof("O", 0),
	]);
}

fn test_http11_get_static(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		SendProxy("B", 0),
		TxH1Req("C", 0, "GET", "/static/foo", &[STD_HOST]),
		RxH1Err("H", 0, 404, "Not found"),
		TxEof("J", 0),
		RxEof("K", 0),
	]);
}

fn test_http2_connection_limit(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, true, "GET", "/foo", &[]),
		Accept("D", 1),
		TxH2Req("E", 0, 3, true, "GET", "/bar", &[]),
		Accept("F", 2),
		RxH1Req("G", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		RxH1Req("H", 2, "GET", "/bar", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		TxH1Res("I", 1, 200, "OK", &[STD_CONNCLOSE]),
		TxH1Res("J", 2, 200, "OK", &[STD_CONNCLOSE]),
		RxH2Res("K", 0, 1, false, 200, "OK", &[STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxH2Res("L", 0, 3, false, 200, "OK", &[STD_BSTREAM, STD_XCTO, STD_CSP]),
		ArbitraryCode("M", Box::new(|con|{
			for _ in 0..40 {
				con.get_mut(&1).unwrap().write_all(&[1;1000]).unwrap();
				assert_got(con.get_mut(&0).unwrap(), &http2_data(1, false, &[1;1000]));
			}
		})),
		ArbitraryCode("N", Box::new(|con|{
			for _ in 0..25 {
				con.get_mut(&2).unwrap().write_all(&[2;1000]).unwrap();
				assert_got(con.get_mut(&0).unwrap(), &http2_data(3, false, &[2;1000]));
			}
		})),
		TxRaw("O", 1, &[3;1000]),
		RxH2Data("P", 0, 1, false, &[3;535]),
		TxRaw("Q", 0, &[0,0,4,8,0,0,0,0,0,1,0,0,0]),
		RxH2Data("R", 0, 1, false, &[3;465]),
		TxEof("S", 1),
		RxH2Data("T", 0, 1, true, &[]),
		TxEof("U", 2),
		RxH2Data("V", 0, 3, true, &[]),
		TxEof("W", 0),
		RxEof("X", 0),
	]);
}

fn test_http2_stream_limit(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxRaw("C", 0, &[0,0,4,8,0,0,0,0,0,1,0,0,0]),
		TxH2Req("D", 0, 1, true, "GET", "/foo", &[]),
		Accept("E", 1),
		TxH2Req("F", 0, 3, true, "GET", "/bar", &[]),
		Accept("G", 2),
		RxH1Req("H", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		RxH1Req("I", 2, "GET", "/bar", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		TxH1Res("J", 1, 200, "OK", &[STD_CONNCLOSE]),
		TxH1Res("K", 2, 200, "OK", &[STD_CONNCLOSE]),
		RxH2Res("L", 0, 1, false, 200, "OK", &[STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxH2Res("M", 0, 3, false, 200, "OK", &[STD_BSTREAM, STD_XCTO, STD_CSP]),
		ArbitraryCode("N", Box::new(|con|{
			for _ in 0..40 {
				con.get_mut(&1).unwrap().write_all(&[1;1000]).unwrap();
				assert_got(con.get_mut(&0).unwrap(), &http2_data(1, false, &[1;1000]));
			}
		})),
		ArbitraryCode("O", Box::new(|con|{
			for _ in 0..30 {
				con.get_mut(&2).unwrap().write_all(&[2;1000]).unwrap();
				assert_got(con.get_mut(&0).unwrap(), &http2_data(3, false, &[2;1000]));
			}
		})),
		ArbitraryCode("P", Box::new(|con|{
			for _ in 0..25 {
				con.get_mut(&1).unwrap().write_all(&[1;1000]).unwrap();
				assert_got(con.get_mut(&0).unwrap(), &http2_data(1, false, &[1;1000]));
			}
		})),
		ArbitraryCode("Q", Box::new(|con|{
			for _ in 0..30 {
				con.get_mut(&2).unwrap().write_all(&[2;1000]).unwrap();
				assert_got(con.get_mut(&0).unwrap(), &http2_data(3, false, &[2;1000]));
			}
		})),
		TxRaw("R", 1, &[3;1000]),
		RxH2Data("S", 0, 1, false, &[3;535]),
		TxRaw("T", 0, &[0,0,4,8,0,0,0,0,1,1,0,0,0]),
		RxH2Data("U", 0, 1, false, &[3;465]),
		TxEof("V", 1),
		RxH2Data("W", 0, 1, true, &[]),
		TxEof("X", 2),
		RxH2Data("Y", 0, 3, true, &[]),
		TxEof("Z", 0),
		RxEof("AA", 0),
	]);
}

fn test_http2_connection_limit_error(t: &mut Test, l: &mut UnixListener)
{
	use self::ChecklistItem::*;
	test(t, l, &[
		Connect("A", 0, "/tmp/foo.sock"),
		H2Exch("B", 0),
		TxH2Req("C", 0, 1, true, "GET", "/foo", &[]),
		Accept("D", 1),
		TxH2Req("E", 0, 3, true, "GET", "/bar", &[]),
		Accept("F", 2),
		RxH1Req("G", 1, "GET", "/foo", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		RxH1Req("H", 2, "GET", "/bar", &[STD_HOST, STD_XFFOR, STD_XFHOST, STD_XFPROTO, STD_XFSECURE,
			STD_XFSERVER, STD_XREALIP, STD_VIA2, STD_CDN_LOOP]),
		TxH1Res("I", 1, 200, "OK", &[STD_CONNCLOSE]),
		TxH1Res("J", 2, 200, "OK", &[STD_CONNCLOSE]),
		RxH2Res("K", 0, 1, false, 200, "OK", &[STD_BSTREAM, STD_XCTO, STD_CSP]),
		RxH2Res("L", 0, 3, false, 200, "OK", &[STD_BSTREAM, STD_XCTO, STD_CSP]),
		ArbitraryCode("M", Box::new(|con|{
			for _ in 0..40 {
				con.get_mut(&1).unwrap().write_all(&[1;1000]).unwrap();
				assert_got(con.get_mut(&0).unwrap(), &http2_data(1, false, &[1;1000]));
			}
		})),
		ArbitraryCode("N", Box::new(|con|{
			for _ in 0..25 {
				con.get_mut(&2).unwrap().write_all(&[2;1000]).unwrap();
				assert_got(con.get_mut(&0).unwrap(), &http2_data(3, false, &[2;1000]));
			}
		})),
		TxRaw("O", 1, &[3;530]),
		RxH2Data("P", 0, 1, false, &[3;530]),
		TxH2Req("Q", 0, 5, true, "GET", "/foo[x", &[]),
		RxH2Res("R", 0, 5, false, 400, "Illegal path: Illegal byte 91 (position 4 state 0-0)", &[STD_UTF8,
			STD_INTRES, STD_CSP, STD_XCTO]),
		RxH2Data("S", 0, 5, true, b"400 I"),	//Trucation due to insufficient limit.
		TxEof("T", 1),
		RxH2Data("U", 0, 1, true, &[]),
		TxEof("V", 2),
		RxH2Data("W", 0, 3, true, &[]),
		TxEof("X", 0),
		RxEof("Y", 0),
	]);
}

/*******************************************************************************************************************/


fn test_fragmented_get(t: &mut Test, l: &mut UnixListener)
{
	let mut s = t._do(ST_CONNECT, ||{
		let mut s = UnixStream::connect("/tmp/foo.sock").unwrap();
		send_https_proxyhdr(&mut s);
		s
	});
	t._do(ST_SNDREQ, ||{
		s.write_all(b"GET /foo HTTP/1.1\r\nhost:httpdemux2.test\r\n").unwrap();
		let x = b"foo: bar\r\n\r\n";
		for i in 0..x.len() {
			sleep(Duration::new(0, 100000000));
			s.write_all(&x[i..i+1]).unwrap();
		}
	});
	let mut u = t._do(ST_ACCEPT, ||l.accept().map(|(u,_)|u).unwrap());
	t._do(ST_RCVREQ, ||assert_got(&mut u, &mkrequest("GET", "/foo", &[STD_HOST, STD_XFFOR,
		STD_XFHOST, STD_XFPROTO, STD_XFSECURE, STD_XFSERVER, STD_XREALIP, ("foo", "bar"), STD_VIA,
		STD_CDN_LOOP], "")));
	t._do(ST_SNDRES, ||u.write_all(&mkresponse(200, "OK", &[STD_CONNCLOSE, (CLEN,"14")],
		"Hello, World!\n")).unwrap());
	t._do(ST_RCVRES, ||assert_got(&mut s, &mkresponse(200, "OK", &[(CLEN,"14"), STD_BSTREAM,
		STD_XCTO, STD_CSP], "Hello, World!\n")));
}
