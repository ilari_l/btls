use super::BackendResource;
use super::InternalResponse;
use super::in_prefix;
use super::path_setting;
use super::path_setting_dflt;
use crate::ConfigT;
use crate::decode_hex;
use crate::headerblock::HeaderBlock;
use crate::headerblock::HeaderSourceComponent;
use crate::headerblock::ShdrAuthorityO;
use crate::headerblock::ShdrAuthorityT;
use crate::headerblock::ShdrClientSpkihash;
use crate::headerblock::ShdrPathO;
use crate::headerblock::ShdrPathT;
use crate::headerblock::ShdrProtocol;
use crate::headerblock::ShdrQuery;
use crate::headerblock::ShdrVersion;
use crate::http::ConnectionPropertiesI;
use crate::routing_dp::filename_replace;
use crate::routing_dp::HostRoutingDataRef;
use crate::routing_dp::HostStringIndex2;
use crate::routing_dp::IDisableMode;
use crate::routing_dp::InsecureMode;
use crate::routing_dp::MAX_REDIRECT_SUBS;
use crate::routing_dp::path_pattern_replace;
use crate::routing_dp::PathData;
use crate::routing_dp::redirect_pattern_replace;
use crate::routing_dp::RedirectMode;
use crate::routing_dp::RegularExpressionEntryData as RegexEntry;
use crate::routing_dp::RegularExpressionInsecure as RegexInsecure;
use btls_aux_http::http::StandardHttpHeader;
use btls_aux_http::status::BAD_REQUEST;
use btls_aux_http::status::FORBIDDEN;
use btls_aux_http::status::GONE;
use btls_aux_http::status::INTERNAL_SERVER_ERROR;
use btls_aux_http::status::NOT_FOUND;
use btls_aux_http::status::UNAVAILABLE_FOR_LEGAL_REASONS;
use btls_util_logging::log;
use btls_util_regex::RegexMatch;
use btls_aux_memory::trim_ascii;
use std::borrow::Cow;
use std::net::IpAddr;
use std::ops::Deref;
use std::path::Path;
use std::str::from_utf8;


static BAD_HTTP: &'static str = "S in HTTP stands for secure (HTTPS required)";
static REQUEST_LOOPING: &'static str = "Request is looping through proxy";

enum HttpAction
{
	Allow,
	Redirect(RedirectMode, bool),		//Code, Flag.
	Block,
}

#[derive(Copy,Clone,PartialEq,Eq)]
pub(super) struct SpecialPath
{
	root: bool,
	well_known: bool,
}

impl SpecialPath
{
	pub(super) fn new(block: &HeaderBlock, is_unsafe: bool) -> SpecialPath
	{
		//This needs to be original path, as we are checking if request is to root or not,
		//not caring about any remaps.
		let path = block.get_special(ShdrPathO).unwrap_or(b"");
		let query = block.get_special(ShdrQuery).unwrap_or(b"");
		let root = path == b"/" && query == b"" && !is_unsafe;
		let well_known = in_prefix(path, b"/.well-known");
		SpecialPath{root, well_known}
	}
}


pub(super) fn __regex_https_redirect(path_data: &RegexEntry, pdatag: &PathData, block: &HeaderBlock,
	g: ConfigT, secure: bool) -> Result<(), InternalResponse<'static>>
{
	let insecure = path_data.insecure.
		unwrap_or_else(||match pdatag.insecure_mode.unwrap_or(DEFAULT_INSECURE_MODE) {
		InsecureMode::Allow => RegexInsecure::Allow,
		InsecureMode::Redirect(x) => RegexInsecure::Redirect(x),
		_ => RegexInsecure::Block,
	});
	let insecure = match insecure {
		RegexInsecure::Allow => HttpAction::Allow,
		RegexInsecure::Block => HttpAction::Block,
		RegexInsecure::Redirect(x) => HttpAction::Redirect(x, false)
	};
	__redirect_to_https(insecure, block, secure, g)
}

const DEFAULT_INSECURE_MODE: InsecureMode = InsecureMode::Forbidden;

pub(super) fn __do_path_insecure(block: &HeaderBlock, special: SpecialPath, secure: bool, pdata: Option<&PathData>,
	pdatag: &PathData, g: ConfigT) -> Result<(), InternalResponse<'static>>
{
	let insecure_mode = match path_setting_dflt(pdata, pdatag, DEFAULT_INSECURE_MODE, |p|p.insecure_mode) {
		//Allow unconditionally allows the request.
		InsecureMode::Allow => HttpAction::Allow,
		//Don't redirect well-known, as we have no idea what is in there.
		InsecureMode::Redirect(_) if special.well_known => HttpAction::Allow,
		//Redirect unconditionally redirects the request. send_internal_response_early() sets any
		//needed flags. The URL is guaranteed valid, because low level HTTP code chacks that the
		//parts are legal. send_internal_response_early() sets the needed flags.
		InsecureMode::Redirect(code) => HttpAction::Redirect(code, false),
		//RootOnly redirects only the root (is_root). The notes above hold for these too.
		InsecureMode::RootOnly(code) if special.root => HttpAction::Redirect(code, false),
		InsecureMode::RootOnlyFlag(code) if special.root => HttpAction::Redirect(code, true),
		//Otherwise reject the request. send_internal_response_early() sets the needed flags.
		InsecureMode::Forbidden|InsecureMode::RootOnly(_)|InsecureMode::RootOnlyFlag(_) =>
			HttpAction::Block
	};
	__redirect_to_https(insecure_mode, block, secure, g)
}

pub(super) fn __do_host_root_redirect<'a>(block: &HeaderBlock, special: SpecialPath,
	hostdata: &'a HostRoutingDataRef) -> Result<(), InternalResponse<'a>>
{
	if let (true, Some((target, code))) = (special.root, hostdata.get_root_redirect()) {
		let lerrmsg = "root-redirect directive is causing a redirect loop";
		fail_if!(is_looping_redirect(target, block),
			InternalResponse::err(INTERNAL_SERVER_ERROR, lerrmsg));
		fail!(InternalResponse::redir(code, target))
	}
	Ok(())
}

pub(super) fn __do_process_disable<'a>(block: &HeaderBlock, hostdata: &'a HostRoutingDataRef) ->
	Result<(), InternalResponse<'a>>
{
	//Disables act on original paths.
	let path = block.get_special(ShdrPathO).unwrap_or(b"");
	if let Some(x) = hostdata.get_disable(path) {
	//Check if this is a redirect.
		match &x {
			&IDisableMode::MovedPermanently(ptr)|
			&IDisableMode::Found(ptr)|
			&IDisableMode::TemporaryRedirect(ptr)|
			&IDisableMode::PermanentRedirect(ptr) => {
				let y = hostdata.get_string(ptr);
				fail_if!(is_looping_redirect(y, block),
					InternalResponse::err(INTERNAL_SERVER_ERROR,
					"disable directive is causing a redirect loop"));
			},
			_ => (),
		}
		fail!(disable_to_response(x, hostdata));
	}
	Ok(())
}

fn __get_remap(pdata: Option<&PathData>, pdatag: &PathData, raw: fn(&PathData) -> Option<bool>,
	project: fn(&PathData) -> Option<HostStringIndex2>) -> Option<HostStringIndex2>
{
	if path_setting_dflt(pdata, pdatag, false, raw) {
		None	//Remapping raw paths is not supported.
	} else {
		path_setting(pdata, pdatag, project)
	}
}

pub(super) fn __remap_target(block: &mut HeaderBlock, ppath: &str, host: &HostRoutingDataRef,
	pdata: Option<&PathData>, pdatag: &PathData)
{
	if let Some(ref rhost) = __get_remap(pdata, pdatag, |p|p.raw_host, |p|p.remaphost).
		map(|s|host.get_string(s)) {
		//Empty value is ignored.
		if rhost.deref() != "" { block.set_special(ShdrAuthorityT, rhost.as_bytes()); }
	}
	if let Some(ref rpath) = __get_remap(pdata, pdatag, |p|p.raw_path, |p|p.remappath).
		map(|s|host.get_string(s)) {
		//If rpath ends in / but ppath does not and this is not exact match, strip the
		//last /. This is because target already has /.
		let exact_match = ppath.as_bytes() == block.get_special(ShdrPathO).unwrap_or(b"");
		let rpath = if rpath.ends_with("/") && !ppath.ends_with("/") && !exact_match {
			rpath.get(..rpath.len()-1)
		} else {
			rpath.get(..)
		}.unwrap_or("").as_bytes();
		//If ppath ends in / but rpath does not, strip the last /.
		let ppathlen = if ppath.ends_with("/") && !rpath.ends_with(b"/") {
			ppath.len()-1
		} else {
			ppath.len()
		};
		let mut prange = block.get_special_range(ShdrPathO).unwrap_or(0..0);
		prange.start += ppathlen;
		//Assign concatenation of rpath and prange as PATH.
		block.set_special_multipart(ShdrPathT, [HeaderSourceComponent::Slice(rpath),
			HeaderSourceComponent::Range(prange)].iter().cloned());
	}
}

pub(super) fn __process_via_and_cdn_loop(block: &mut HeaderBlock, proxyname: &[u8]) ->
	Result<(), InternalResponse<'static>>
{
	//Scan CDN-LOOP. If proxyname is found, then abort.
	let mut looping = false;
	block.for_each_named(StandardHttpHeader::CdnLoop, |value|for item in value.split(|x|*x==b',') {
		let item = trim_ascii(item);
		looping |= item == proxyname;
	});
	fail_if!(looping, InternalResponse::err(INTERNAL_SERVER_ERROR, REQUEST_LOOPING));
	//Add VIA header and CDN-LOOP.
	let version = if let Some(r) = block.get_special_range(ShdrVersion) {
		//Strip http prefix, if any.
		let r = if block.get_special(ShdrVersion).map(|x|x.starts_with(b"HTTP/")).unwrap_or(false) {
			r.start+5..r.end
		} else { r };
		HeaderSourceComponent::Range(r)
	} else {
		HeaderSourceComponent::Slice(b"1.1")
	};
	let via_value = [version, HeaderSourceComponent::Slice(b" "), HeaderSourceComponent::Slice(proxyname)];
	block.set_header_multipart_std(StandardHttpHeader::Via, via_value.iter().cloned());
	block.set_header_std(StandardHttpHeader::CdnLoop, proxyname);
	Ok(())
}

pub(super) fn __canonicalhost_redirect(hostdata: &HostRoutingDataRef, block: &HeaderBlock) ->
	Result<(), InternalResponse<'static>>
{
	//Canonicalhost redirect. Never do canoncialhost redirect to the same authority, as that
	//would lead to redirect loop. Low level HTTP code checks parts and
	//send_internal_response_early() sets the flags. Use original authority, as that is what is
	//in request and would cause looping redirect.
	if let Some((tauthority, code)) = hostdata.get_canonicalhost() {
		if block.get_special(ShdrAuthorityO).unwrap_or(b"") != tauthority.as_bytes() {
			let url = block.get_url(None, Some(tauthority.as_bytes()), false);
			fail!(InternalResponse::redir(code, url));
		}
	}
	Ok(())
}

pub(super) fn __regex_pattern_redirect<'a>(hostdata: &HostRoutingDataRef, path_data: &RegexEntry,
	block: &HeaderBlock, submatches: &[RegexMatch;MAX_REDIRECT_SUBS], newurl: &'a mut [u8]) ->
	Result<(), InternalResponse<'a>>
{
	if let Some((mode, pattern)) = path_data.redirect {
		let oldpath = block.get_special(ShdrPathO).unwrap_or(b"");
		let pattern = hostdata.get_string(pattern);
		fail!(match redirect_pattern_replace(oldpath, pattern, submatches, newurl) {
			Ok(newurl) => fail!(__send_redirect_to(block, mode, newurl)),
			Err(err) => InternalResponse::err(INTERNAL_SERVER_ERROR,
				format!("Bad redirect pattern: {err}"))
		})
	}
	Ok(())
}

pub(super) fn __regex_backend(hostdata: &HostRoutingDataRef, path_data: &RegexEntry, block: &mut HeaderBlock,
	submatches: &[RegexMatch;MAX_REDIRECT_SUBS], backend: &mut BackendResource) ->
	Result<(), InternalResponse<'static>>
{
	if !backend.is_none() { return Ok(()); }	//Backend already chosen.
	//Only do remap if backend actually will be set.
	let is_websocket = block.get_special(ShdrProtocol) == Some(b"websocket");
	let nbackend = if let (true, Some(xbackend)) = (is_websocket, path_data.ws_backend) {
		BackendResource::Named(xbackend)
	} else if let Some(xbackend) = path_data.backend {
		BackendResource::Named(xbackend)
	} else {
		return Ok(());	//No backend to set, skip remap.
	};
	//Remap host.
	if let Some(rhost) = path_data.remaphost {
		let rhost = hostdata.get_string(rhost);
		//Empty value is ignored.
		if rhost != "" { block.set_special(ShdrAuthorityT, rhost.as_bytes()); }
	}
	//Remap path.
	if let Some(rpath) = path_data.remappath {
		let rpath = hostdata.get_string(rpath);
		let mut newurl = [0;4096];
		let oldpath = block.get_special(ShdrPathO).unwrap_or(b"");
		match path_pattern_replace(oldpath, rpath, &submatches, &mut newurl) {
			Ok(newpath) => block.set_special(ShdrPathT, newpath),
			Err(err) => fail!(InternalResponse::err(INTERNAL_SERVER_ERROR,
				format!("Bad rewrite pattern: {err}")))
		}
	}
	*backend = nbackend;
	Ok(())
}

pub(super) fn __regex_blacklist<'a>(path_data: &'a RegexEntry) -> Result<(), InternalResponse<'static>>
{
	fail_if!(path_data.blacklisted, InternalResponse::err(FORBIDDEN, "Path adminstratively blacklisted"));
	Ok(())
}

pub(super) fn __regex_fc_backend(hostdata: &HostRoutingDataRef, force_error: Option<u16>, path_data: &RegexEntry,
	submatches: &[RegexMatch;MAX_REDIRECT_SUBS], block: &HeaderBlock, backend: &mut BackendResource) ->
	Result<(), InternalResponse<'static>>
{
	if let Some((base, subpath)) = path_data.fc_backend {
		__regex_fc_backend_inner(hostdata, force_error, path_data, base, subpath, &submatches, block,
			backend)?;
	} else if let Some(err) = force_error {
		//Error if there is no fc-backend.
		fail!(InternalResponse::err_dflt(err));
	}
	Ok(())
}

pub(super) fn __check_host(block: &mut HeaderBlock, h: &HostRoutingDataRef, properties: ConnectionPropertiesI,
	ip: Option<IpAddr>) -> Result<(), InternalResponse<'static>>
{
	//Check IP blocklist.
	if ip.map(|ip|h.is_ip_banned(ip)).unwrap_or(false) {
		//IP is banned. TODO: Better error control.
		return Err(InternalResponse::err(FORBIDDEN, "Source IP banned from virtual host"));
	}
	let is_secure = block.is_secure();
	//If req_tls13, req_secure192 or has_acl_list is set, insecure access is disabled.
	if !is_secure && (h.get_req_tls13() || h.get_req_secure192() || h.has_acl_list()) {
		return Err(InternalResponse::err(BAD_REQUEST, "Illegal insecure access"));
	}
	if h.get_req_tls13() && !properties.is_tls13() {
		//No point in sending misdirected request, as the client obviously does not support TLS 1.3
		//or it would have been negotiated.
		return Err(InternalResponse::err(FORBIDDEN, "TLS 1.3 required for access"));
	}
	if h.get_req_secure192() && !properties.is_secure192() {
		//Send misdirected request, so client tries to re-establish connection with adequate
		//security.
		return Err(InternalResponse::RejectedHost("Insufficient connection security"));
	}
	if h.has_acl_list() {
		let spkihash: Option<[u8;32]> = block.get_special(ShdrClientSpkihash).and_then(|spki|{
			let mut spkibin = [0;32];
			decode_hex(&mut spkibin, spki)?;
			Some(spkibin)
		});
		//If no client certificate, give separate message. There is no way check below passes
		//without client certificate. The error code depends on if client certificate was requested
		//or not. If it was, 403 is correct error, if it was not use 421 to try to prompt the
		//U-A to try again.
		if spkihash.is_none() {
			return Err(if block.is_req_client_cert() {
				InternalResponse::err(FORBIDDEN, "Client certificate required")
			} else {
				InternalResponse::RejectedHost("Client certificate required")
			})
		}
		if !h.on_acl_list(spkihash.as_ref()) {
			//Failed ACL check.
			return Err(InternalResponse::err(FORBIDDEN, "Wrong client certificate"));
		}
	}
	Ok(())
}

fn __regex_fc_backend_inner(hostdata: &HostRoutingDataRef, force_error: Option<u16>, path_data: &RegexEntry,
	base: HostStringIndex2, subpath: HostStringIndex2, submatches: &[RegexMatch;MAX_REDIRECT_SUBS],
	block: &HeaderBlock, backend: &mut BackendResource) -> Result<(), InternalResponse<'static>>
{
	let path = Path::new(hostdata.get_string(base));
	if subpath.is_empty() {
		//Handle empty subpath specially: Return a fixed file, complaining if it is not found.
		if path.is_file() {
			*backend = BackendResource::File{
				fname: path.to_path_buf(),
				uncompressed: path_data.uncompressed,
				is_error: force_error.is_some(),
			};
		} else {
			//This is PII.
			log!(INFO "Static file not found: {path}", path=path.display());
			fail!(InternalResponse::err(INTERNAL_SERVER_ERROR, "Nonexistent errordocument file"));
		}
		return Ok(());
	} else if force_error.is_some() {
		fail!(InternalResponse::err(INTERNAL_SERVER_ERROR, "Subpath not allowed for errordocument file"));
	}
	let subpath = hostdata.get_string(subpath);
	let mut newpath = [0;4096];
	let oldpath = block.get_special(ShdrPathO).unwrap_or(b"");
	let mut path = match filename_replace(oldpath, subpath, &submatches, &mut newpath) {
		Ok(Some(newpath)) => path.join(newpath),
		Ok(None) => fail!(InternalResponse::err(BAD_REQUEST, "Illegal path traversal")),
		Err(err) => fail!(InternalResponse::err(INTERNAL_SERVER_ERROR,
			format!("Bad filename pattern: {err}")))
	};
	//If path points to a directory, add index.html.
	if path.is_dir() { path = path.join("index.html"); }
	//Check that path points to a regular file that can be sent.
	if path.is_file() {
		//This is always assumed to be success, so is_error is always false.
		*backend = BackendResource::File{
			fname: path,
			uncompressed: path_data.uncompressed,
			is_error: false,
		};
	}
	Ok(())
}

fn __redirect_to_https(mode: HttpAction, block: &HeaderBlock, secure: bool, g: ConfigT) ->
	Result<(), InternalResponse<'static>>
{
	match mode {
		_ if secure => (),		//HTTPS always allowed.
		HttpAction::Allow => (),	//HTTP explicitly allowed.
		HttpAction::Redirect(code, flag) => {
			increment_metric!(req_http_forced g);
			fail!(InternalResponse::redir(code, block.get_url(Some(b"https"), None, flag)));
		},
		HttpAction::Block => {
			increment_metric!(req_http_blocked g);
			fail!(InternalResponse::err(FORBIDDEN, BAD_HTTP));
		}
	}
	Ok(())
}

fn __send_redirect_to<'a>(block: &HeaderBlock, mode: RedirectMode, to: &'a [u8]) -> InternalResponse<'a>
{
	let not_utf8_q = "Non-UTF8 queries not supported in redirects";
	let to_query = to.iter().any(|&c|c==b'?');
	let to = f_return!(from_utf8(to),
		InternalResponse::err(INTERNAL_SERVER_ERROR, "Redirecting to invalid URL"));
	let to = if let Some(query) = block.get_special(ShdrQuery) {
		let query = query.strip_prefix(b"?").unwrap_or(query);
		let query = f_return!(from_utf8(query), InternalResponse::err(INTERNAL_SERVER_ERROR, not_utf8_q));
		let sep = if to_query { "&" } else { "?" };
		Cow::Owned(format!("{to}{sep}{query}"))
	} else {
		Cow::Borrowed(to)
	};
	InternalResponse::Redirect(mode, to)
}

fn is_looping_redirect(target: &str, request: &HeaderBlock) -> bool
{
	//We can assume target is valid URI here. First strip anything after the first ?, as that is query and
	//fragment, which we do not care about here.
	let target = if let Some(s) = target.find('?') { &target[..s] } else { target };
	//Find first : or / to find out what scheme this is. If neither is found, this has to be one-part
	//path-noscheme (which never collides) or path-empty (which always collides)
	let target = if let Some(s) = target.find(|x|x==':'||x=='/') {
		if target.as_bytes()[s] == b':' {
			//If scheme is not 'http' nor 'https', assume it does not collide. Ignore http vs https.
			//difference.
			if &target[..s] != "http" && &target[..s] != "https" { return false; }
			&target[s+1..]
		} else {
			target
		}
	} else {
		target
	};
	//If target starts with "//", it has authority, otherwise it is pure path.
	let (authority, path) = if let Some(target) = target.strip_prefix("//") {
		//Split authority and path at /. If there is no /, then it is all authority. Then split authority
		//at @, to kill userinfo.
		let (authority, path) = if let Some(s) = target.find('/') {
			(&target[..s], &target[s..])
		} else {
			(target, "")
		};
		//If path is empty, assume / was meant.
		let path = if path == "" { "/" } else { path };
		let authority = if let Some(s) = authority.find('@') { &authority[s+1..] } else { authority };
		(Some(authority), path)
	} else {
		//If path does not start with '/', it does not match, except if it is empty, which always matches.
		if target == "" { return true; }
		if !target.starts_with("/") { return false; }
		(None, target)
	};
	//For collision to happen the path has to match, and authority has to match (or be none). User originals,
	//as that was in the request.
	if !paths_match(path, request.get_special(ShdrPathO).unwrap_or(b"")) { return false; }
	if authority.map(|a|!authorities_match(a, request.get_special(ShdrAuthorityO).unwrap_or(b""))).
		unwrap_or(false) {
		return false;
	}
	//Ok, they do collide.
	true
}

fn disable_to_response<'a>(d: IDisableMode, h: &'a HostRoutingDataRef) -> InternalResponse<'a>
{
	use crate::routing_dp::IDisableMode::*;
	let reason = match &d {
		&MovedPermanently(p)|&Found(p)|&TemporaryRedirect(p)|&PermanentRedirect(p)|&Forbidden(p)|
			&NotFound(p)|&Gone(p)|&UnavailableForLegalReasons(p) => h.get_string(p)
	};
	match d {
		MovedPermanently(_) => InternalResponse::redir(RedirectMode::MovedPermanently, reason),
		Found(_) => InternalResponse::redir(RedirectMode::Found, reason),
		TemporaryRedirect(_) => InternalResponse::redir(RedirectMode::TemporaryRedirect, reason),
		PermanentRedirect(_) => InternalResponse::redir(RedirectMode::PermanentRedirect, reason),
		Forbidden(_) => InternalResponse::err(FORBIDDEN, reason),
		NotFound(_) => InternalResponse::err(NOT_FOUND, reason),
		Gone(_) => InternalResponse::err(GONE, reason),
		UnavailableForLegalReasons(_) => InternalResponse::err(UNAVAILABLE_FOR_LEGAL_REASONS, reason),
	}
}

fn paths_match(p1: &str, p2: &[u8]) -> bool
{
	let mut p1i = p1.as_bytes().iter().cloned();
	let mut p2i = p2.iter().cloned();
	loop {
		let p1c = p1i.next();
		let p2c = p2i.next();
		let p1c = if p1c == Some(b'%') { Some(unescape_from_iterator(&mut p1i)) } else { p1c };
		let p2c = if p2c == Some(b'%') { Some(unescape_from_iterator(&mut p2i)) } else { p2c };
		match (p1c, p2c) {
			(None, None) => return true,
			(x, y) => if x != y { return false; }
		}
	}
}

fn authorities_match(a1: &str, a2: &[u8]) -> bool
{
	let mut a1i = a1.as_bytes().iter().cloned();
	let mut a2i = a2.iter().cloned();
	loop {
		let a1c = a1i.next();
		let a2c = a2i.next();
		let a1c = if a1c == Some(b'%') { Some(unescape_from_iterator(&mut a1i)) } else { a1c };
		let a2c = if a2c == Some(b'%') { Some(unescape_from_iterator(&mut a2i)) } else { a2c };
		match (a1c, a2c) {
			(None, None) => return true,
			(Some(x), Some(y)) => {
				//Case-insensitive match.
				let x = if x.wrapping_sub(65) < 26 { x + 32 } else { x };
				let y = if y.wrapping_sub(65) < 26 { y + 32 } else { y };
				if x != y { return false; }
			},
			(_, _) => return false
		}
	}
}

fn unescape_from_iterator<I:Iterator<Item=u8>>(itr: &mut I) -> u8
{
	let a = itr.next().unwrap_or(0).wrapping_add(16) & 31;
	let b = itr.next().unwrap_or(0).wrapping_add(16) & 31;
	let a = a.wrapping_sub(a / 16 * 6);
	let b = b.wrapping_sub(b / 16 * 6);
	a.wrapping_mul(16).wrapping_add(b)
}
