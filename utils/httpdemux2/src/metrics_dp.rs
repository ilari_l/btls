use crate::ConfigT;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering;


macro_rules! define_http_gmetrics
{
	($([$value:tt $symbol:ident $variable:ident];)*) => {
		#[derive(Default)]
		pub struct GlobalMetrics
		{
			$(pub $variable: AtomicUsize,)*
		}
	}
}

//Define the metrics.
include!("metrics.inc");

impl GlobalMetrics
{
	pub fn new() -> GlobalMetrics
	{
		GlobalMetrics {..Default::default()}
	}
	pub fn increment_metric<'a,F>(&'a self, amt: usize, f: F)
		where F: FnOnce(&'a GlobalMetrics) -> &'a AtomicUsize
	{
		f(self).fetch_add(amt, Ordering::Relaxed);
	}
}

pub trait State
{
	fn word<'a>(&self, g: &'a GlobalMetrics) -> &'a AtomicUsize;
}

impl State for StateO
{
	fn word<'a>(&self, g: &'a GlobalMetrics) -> &'a AtomicUsize
	{
		match self {
			&StateO::Connecting => &g.out_connecting,
			&StateO::Idle => &g.out_idle,
			&StateO::Keepalive => &g.out_keepalive,
			&StateO::Request => &g.out_request,
			&StateO::Dead => &g.out_dead,
		}
	}
}

impl State for StateI
{
	fn word<'a>(&self, g: &'a GlobalMetrics) -> &'a AtomicUsize
	{
		match self {
			&StateI::Proxy => &g.in_proxy,
			&StateI::H1 => &g.in_h1,
			&StateI::H2 => &g.in_h2,
			&StateI::Dead => &g.in_dead,
		}
	}
}

pub enum StateO
{
	Connecting,
	Idle,
	Keepalive,
	Request,
	Dead,
}

pub enum StateI
{
	Proxy,
	H1,
	H2,
	Dead,
}

fn set_state<S:State>(state: &mut S, new: S, inout: &GlobalMetrics)
{
	new.word(inout).fetch_add(1, Ordering::Relaxed);
	state.word(inout).fetch_sub(1, Ordering::Relaxed);
	*state = new;
}

fn new_state<S:State>(new: S, inout: &GlobalMetrics) -> S
{
	new.word(inout).fetch_add(1, Ordering::Relaxed);
	new
}

pub struct OutgoingConnectionState(StateO, ConfigT);

impl Drop for OutgoingConnectionState
{
	fn drop(&mut self) { self.1.metrics(|g|set_state(&mut self.0, StateO::Dead, g)) }
}

impl OutgoingConnectionState
{
	pub fn new_connecting(g: ConfigT) -> OutgoingConnectionState
	{
		OutgoingConnectionState(g.metrics(|g|new_state(StateO::Connecting, g)), g)
	}
	pub fn set_idle(&mut self)
	{
		self.1.metrics(|g|set_state(&mut self.0, StateO::Idle, g));
	}
	pub fn set_keepalive(&mut self)
	{
		self.1.metrics(|g|set_state(&mut self.0, StateO::Keepalive, g));
	}
	pub fn set_request(&mut self)
	{
		self.1.metrics(|g|set_state(&mut self.0, StateO::Request, g));
	}
}

pub struct IncomingConnectionState(StateI, ConfigT);

impl Drop for IncomingConnectionState
{
	fn drop(&mut self) { self.1.metrics(|g|set_state(&mut self.0, StateI::Dead, g)); }
}

impl IncomingConnectionState
{
	pub fn new_proxy(g: ConfigT) -> IncomingConnectionState
	{
		IncomingConnectionState(g.metrics(|g|new_state(StateI::Proxy, g)), g)
	}
	pub fn set_h1(&mut self) { self.1.metrics(|g|set_state(&mut self.0, StateI::H1, g)); }
	pub fn set_h2(&mut self) { self.1.metrics(|g|set_state(&mut self.0, StateI::H2, g)); }
}
