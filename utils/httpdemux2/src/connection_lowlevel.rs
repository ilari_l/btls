use crate::ConfigT;
use crate::HttpBridgeProxyEx;
use crate::connection::ConnectionInner;
use crate::connection_midlevel::Krypted;
use btls_aux_time::PointInTime;
use btls_daemon_helper_lib::AddTokenCallback;
use btls_daemon_helper_lib::AllocatedToken;
use btls_daemon_helper_lib::ConnectionInfo;
use btls_daemon_helper_lib::cow;
use btls_daemon_helper_lib::FileDescriptor;
use btls_daemon_helper_lib::LocalIrq;
use btls_daemon_helper_lib::Poll;
use btls_daemon_helper_lib::TokenAllocatorPool;
use btls_daemon_helper_lib::UpperLayerConnection;
use btls_util_logging::ConnId;
use btls_util_logging::get_current_task;
use btls_util_logging::set_current_task;
use std::borrow::Cow;
use std::cell::RefCell;
use std::panic::AssertUnwindSafe;
use std::panic::catch_unwind;
use std::rc::Rc;
use std::rc::Weak;

//pub type CID = u64;

thread_local!(static LOCKS: Rc<RefCell<Vec<CID>>> = Rc::new(RefCell::new(Vec::new())));
thread_local!(static NEXT_CID: Rc<RefCell<u64>> = Rc::new(RefCell::new(1)));

#[derive(Copy,Clone,PartialEq,Eq,Debug)]
pub struct PreCID(u64);

impl PreCID
{
	pub(crate) fn next() -> PreCID
	{
		let r = NEXT_CID.with(|next_cid|{
			let mut next_cid = next_cid.borrow_mut();
			let val = *next_cid;
			*next_cid += 1;
			val
		});
		PreCID(r)
	}
	fn to_cid(self, conn: &Rc<RefCell<ConnectionMid>>) -> CID
	{
		CID(self.0, Rc::downgrade(conn))
	}
}

#[derive(Clone)]
pub struct CID(u64, Weak<RefCell<ConnectionMid>>);

impl std::fmt::Debug for CID
{
	fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error>
	{
		std::fmt::Debug::fmt(&self.0, f)
	}
}

//Equality only considers the integer part.
impl std::cmp::PartialEq for CID
{
	fn eq(&self, a: &CID) -> bool { self.0 == a.0 }
}
impl std::cmp::Eq for CID {}

//Hash only considers the integer part.
impl std::hash::Hash for CID
{
	fn hash<H:std::hash::Hasher>(&self, h: &mut H) { std::hash::Hash::hash(&self.0, h) }
}

impl CID
{
	//Invalid CIDs point to dangling structure.
	pub fn invalid() -> CID { CID(0, Weak::new()) }
	fn __call_with_connection(&self, func: impl FnOnce(&mut ConnectionInner) -> Result<(), Cow<'static, str>>) ->
		Result<(), Cow<'static, str>>
	{
		//Add the CID to LOCKS, so it can be cleaned up on fault.
		LOCKS.with(|x|x.borrow_mut().push(self.clone()));
		let r = if let Some(mid) = self.1.upgrade() {
			if let Ok(mut ctx) = mid.try_borrow_mut() {
				if !ctx.killed {
					let old_task = ctx.inner.switch_to();
					let f = func(&mut ctx.inner);
					set_current_task(old_task);
					f
				} else {
					Err(cow!("Trying to enter zombie task"))
				}
			} else {
				Err(cow!("Trying to enter busy task"))
			}
		} else {
			Err(cow!("Trying to enter nonexistent task"))
		};
		//This should always yield Some(self).
		LOCKS.with(|x|x.borrow_mut().pop());
		r
	}
	fn __kill_task(&self)
	{
		if let Some(mid) = self.1.upgrade() {
			//The connection better have been unlocked by the fault.
			if let Ok(mut mid) = mid.try_borrow_mut() {
				mid.killed = true;
				increment_metric!(dropped_abort mid.inner.get_config());
				if let Some(irq) = mid.kill_irq.as_ref() { irq.irq(); }
			}
		}
	}
	pub fn call_with_connection(&self,
		func: impl FnOnce(&mut ConnectionInner) -> Result<(), Cow<'static, str>>) ->
		Result<(), Cow<'static, str>>
	{
		//INVALID_CID is never valid.
		fail_if!(self.is_invalid(), cow!("connection has NULL handle"));
		//Check if LOCKS is empty. If it is, this is topmost task entry, and catch_unwind() is needed in
		//order to clean up after some task has failed. Do not do catch_unwind() on nested entries, as all
		//the active tasks need to be killed on fault.
		let top_entry = LOCKS.with(|x|x.borrow().is_empty());
		if top_entry {
			let old_task = get_current_task();
			match catch_unwind(AssertUnwindSafe(||{
				self.__call_with_connection(func)
			})) {
				Ok(r) => r,
				Err(_) => {
					//Restore the old task in case func() aborted with task switched.
					set_current_task(old_task);
					//We faulted. Kill all the tasks in LOCKS and remove them, as they are no
					//longer being executed.
					while let Some(kcid) = LOCKS.with(|x|x.borrow_mut().pop()) {
						kcid.__kill_task();
					}
					Err(cow!("Fatal exception in HTTP task"))
				}
			}
		} else {
			self.__call_with_connection(func)
		}
	}
	pub(crate) fn force_enter_task(&self, f: impl FnOnce(&mut ConnectionInner))
	{
		if let Some(mid) = self.1.upgrade() {
			//The connection should have been unlocked by the fault.
			if let Ok(mut mid) = mid.try_borrow_mut() {
				f(&mut mid.inner);
			}
		}
	}
	pub fn is_invalid(&self) -> bool { self.0 == 0 }
}

struct ConnectionMid
{
	killed: bool,			//Set only on fault.
	kill_irq: Option<LocalIrq>,	//IRQ to send on fault so connection gets promptly cleaned up.
	inner: ConnectionInner,		//Inner connection.
}

//Allocate a new connection.
pub fn allocate_connection(c: ConnectionInner, pcid: PreCID) -> (Connection, CID)
{
	let c = Rc::new(RefCell::new(ConnectionMid {
		killed: false,
		kill_irq: None,
		inner: c,
	}));
	let cid = pcid.to_cid(&c);
	//Report the CID to connection, as there is cyclic dependency otherwise.
	c.borrow_mut().inner.set_self_cid(&cid);
	(Connection{cid:cid.clone(), _sref: c}, cid)
}

pub struct Connection
{
	//The singular strong reference to the connection inner, keeping it alive.
	//There may be transient other references, especially when executing the task.
	_sref: Rc<RefCell<ConnectionMid>>,
	//CID of the connection.
	cid: CID,
}

impl Connection
{
	pub fn accept(poll: &mut Poll, allocator: &mut TokenAllocatorPool, socket: FileDescriptor,
		krypted: impl Krypted, socketinfo: ConnectionInfo, force_proxy: bool, g: ConfigT) ->
		Result<(Connection, Vec<AllocatedToken>, bool), Cow<'static, str>>
	{
		ConnectionInner::new(poll, allocator, socket, krypted, socketinfo, force_proxy, g).
			map(|(c,t,v,pcid)|(allocate_connection(c, pcid).0,t,v))
	}
	pub fn accept_ex(poll: &mut Poll, allocator: &mut TokenAllocatorPool, socket: FileDescriptor,
		info: HttpBridgeProxyEx, g: ConfigT) ->
		Result<(Connection, Vec<AllocatedToken>), Cow<'static, str>>
	{
		ConnectionInner::new_with_info(poll, allocator, socket, info, g).
			map(|(c,t,pcid)|(allocate_connection(c, pcid).0,t))
	}
}

impl UpperLayerConnection for Connection
{
	fn handle_fault(&mut self, poll: &mut Poll) -> Option<Cow<'static, str>>
	{
		self.cid.call_with_connection(|conn|Ok(conn.handle_fault(poll))).err()
	}
	fn handle_init_event(&mut self, id: ConnId, poll: &mut Poll) -> Result<(), Cow<'static, str>>
	{
		self.cid.call_with_connection(|conn|conn.handle_init(id, poll))
	}
	fn handle_quit_event(&mut self)
	{
		self.cid.call_with_connection(|conn|conn.handle_quit()).ok();
	}
	//Handle a timed event (return Err(()) if connection should be killed). Might panic.
	fn handle_timed_event(&mut self, now: PointInTime, poll: &mut Poll) -> Result<(), Cow<'static, str>>
	{
		self.cid.call_with_connection(|conn|conn.handle_timed_event(now, poll))
	}
	//Handle a socket event (return Err(()) if connection should be killed). Might panic.
	fn handle_event(&mut self, poll: &mut Poll, tok: usize, kind: u8, id: ConnId,
		addtoken: &mut AddTokenCallback) -> Result<(), Cow<'static, str>>
	{
		self.cid.call_with_connection(|conn|conn.handle_event(poll, tok, kind, id, addtoken))
	}
	//Remove the pollers. Must not panic, including with poisoned locks.
	fn remove_pollers(&mut self, poll: &mut Poll)
	{
		//We can not panic there, so just ignore locking.
		self.cid.force_enter_task(|inner|inner.remove_pollers(poll));
	}
	//Get next timed event. Called early and after calling handle_timed_event() or handle_event().
	fn get_next_timed_event(&self, _: PointInTime) -> Option<PointInTime>
	{
		//We can not panic there, so just ignore locking. We should not have locks anyway.
		//If no backend found, return now to die as quickly as possible.
		let mut t_ev = Some(PointInTime::now());
		self.cid.force_enter_task(|inner|t_ev = inner.get_next_timed_event());
		t_ev
	}
	fn get_main_fd(&self) -> i32
	{
		//We are not supposed to hold any locks here.
		let mut fd = -1;
		self.cid.force_enter_task(|inner|fd = inner.get_raw_fd());
		fd
	}
}
