use btls_aux_http::http::StandardHttpHeader;
use std::ops::Range;

const U16_MAX_USIZE: usize = u16::MAX as usize;

#[derive(Copy,Clone,Debug)]
enum _MaybeStandardName<'a>
{
	Standard(StandardHttpHeader),
	Custom(&'a [u8]),
}

#[derive(Copy,Clone,Debug)]
pub struct MaybeStandardName<'a>(_MaybeStandardName<'a>);

impl<'a> Eq for MaybeStandardName<'a> {}

impl From<StandardHttpHeader> for MaybeStandardName<'static>
{
	fn from(x: StandardHttpHeader) -> MaybeStandardName<'static>
	{
		MaybeStandardName(_MaybeStandardName::Standard(x))
	}
}

impl<'a> From<&'a [u8]> for MaybeStandardName<'a>
{
	fn from(x: &'a [u8]) -> MaybeStandardName<'a>
	{
		let r = match StandardHttpHeader::from(x) {
			Some(x) => _MaybeStandardName::Standard(x),
			None => _MaybeStandardName::Custom(x),
		};
		MaybeStandardName(r)
	}
}

impl<'a> From<&'a str> for MaybeStandardName<'a>
{
	fn from(x: &'a str) -> MaybeStandardName<'a>
	{
		let r = match StandardHttpHeader::from_str(x) {
			Some(x) => _MaybeStandardName::Standard(x),
			None => _MaybeStandardName::Custom(x.as_bytes()),
		};
		MaybeStandardName(r)
	}
}

impl<'a> MaybeStandardName<'a>
{
	pub fn as_bytes(&self) -> &'a [u8]
	{
		match self.0 {
			_MaybeStandardName::Standard(x) => x.to_str().as_bytes(),
			_MaybeStandardName::Custom(x) => x,
		}
	}
	pub fn as_standard(&self) -> Option<StandardHttpHeader>
	{
		match self.0 {
			_MaybeStandardName::Standard(x) => Some(x),
			_MaybeStandardName::Custom(_) => None,
		}
	}
	pub fn try_as_standard(&self) -> Result<StandardHttpHeader, &'a [u8]>
	{
		match self.0 {
			_MaybeStandardName::Standard(x) => Ok(x),
			_MaybeStandardName::Custom(y) => Err(y),
		}
	}
	pub fn extend_len(&self) -> usize
	{
		match self.0 {
			//Standards are stored inline and thus take 0 bytes.
			_MaybeStandardName::Standard(_) => 0,
			_MaybeStandardName::Custom(x) => x.len(),
		}
	}
}

impl<'a,'b> PartialEq<MaybeStandardName<'a>> for MaybeStandardName<'b>
{
	fn eq(&self, a: &MaybeStandardName<'a>) -> bool
	{
		use self::_MaybeStandardName as MSN;
		//Exploit the invariant that MaybeStandardName is always Standard for standard headers. Thus,
		//cross-terms can never match.
		match (self.0, a.0) {
			(MSN::Standard(x), MSN::Standard(y)) => x == y,
			(MSN::Custom(x), MSN::Custom(y)) => x == y,
			_ => false
		}
	}
}

impl<'a,'b> PartialEq<&'a [u8]> for MaybeStandardName<'b>
{
	fn eq(&self, a: &&'a [u8]) -> bool { self.as_bytes() == *a }
}

impl<'a,'b> PartialEq<&'a str> for MaybeStandardName<'b>
{
	fn eq(&self, a: &&'a str) -> bool { self.as_bytes() == a.as_bytes() }
}

impl<'a,'b> PartialEq<StandardHttpHeader> for MaybeStandardName<'b>
{
	fn eq(&self, a: &StandardHttpHeader) -> bool
	{
		//Exploit the invariant that MaybeStandardName is always Standard for standard headers.
		match self.0 {
			_MaybeStandardName::Standard(b) => *a == b,
			_MaybeStandardName::Custom(_) => false
		}
	}
}

impl<'a,'b> PartialEq<MaybeStandardName<'a>> for &'b [u8]
{
	fn eq(&self, a: &MaybeStandardName<'a>) -> bool { *a == *self }
}

impl<'a,'b> PartialEq<MaybeStandardName<'a>> for &'b str
{
	fn eq(&self, a: &MaybeStandardName<'a>) -> bool { *a == *self }
}

impl<'a,'b> PartialEq<MaybeStandardName<'a>> for StandardHttpHeader
{
	fn eq(&self, a: &MaybeStandardName<'a>) -> bool { *a == *self }
}

#[derive(Copy,Clone,Debug)]
#[repr(C)]	//Guarantee SliceRangeName is 4 bytes.
pub struct SliceRangeName(u16,u16);

impl SliceRangeName
{
	fn none() -> SliceRangeName { SliceRangeName(u16::MAX,u16::MAX) }
	pub fn standard(x: StandardHttpHeader) -> SliceRangeName { SliceRangeName(u16::MAX, x as u16) }
	pub fn new2(start: usize, end: usize) -> Option<SliceRangeName>
	{
		//Range is unpresentable if its start is at least u16::MAX, or if its length is greater than u16::MAX.
		let length = end.wrapping_sub(start);
		fail_if_none!(start >= U16_MAX_USIZE || length > U16_MAX_USIZE);
		Some(SliceRangeName(start as u16, length as u16))
	}
	pub fn write_extend(array: &mut Vec<u8>, name: MaybeStandardName) -> SliceRangeName
	{
		//Actually standard?
		let name = match name.0 {
			_MaybeStandardName::Standard(h) => return SliceRangeName::standard(h),
			_MaybeStandardName::Custom(h) => h,
		};
		//No, write full name.
		let sptr = array.len() as u16;
		array.extend_from_slice(name);
		SliceRangeName(sptr, name.len() as u16)
	}
	pub fn get<'a>(self, array: &'a [u8]) -> Option<MaybeStandardName<'a>>
	{
		//Specials have offset 65535.
		if self.0 == u16::MAX {
			let sh = StandardHttpHeader::from_id(self.1)?;
			return Some(MaybeStandardName(_MaybeStandardName::Standard(sh)));
		}
		let a = self.0 as usize;
		let b = self.1 as usize;
		//Assume this is not standard.
		fail_if_none!(a + b > array.len());
		let name = unsafe{array.get_unchecked(a..a+b)};
		Some(MaybeStandardName(_MaybeStandardName::Custom(name)))
	}
	fn is_cookie(self) -> bool { self.0 == u16::MAX && self.1 == StandardHttpHeader::Cookie as u16 }
	fn try_as_standard(self) -> Option<StandardHttpHeader>
	{
		//Specials have offset 65535.
		fail_if_none!(self.0 != u16::MAX);
		StandardHttpHeader::from_id(self.1)
	}
}

#[derive(Copy,Clone,Debug)]
#[repr(C)]	//Guarantee SliceRange is 4 bytes.
pub struct SliceRange(u16,u16);

impl SliceRange
{
	pub fn none() -> SliceRange { SliceRange(u16::MAX,u16::MAX) }
	pub fn new2(start: usize, end: usize) -> Option<SliceRange>
	{
		//Range is unpresentable if its start is at least u16::MAX, or if its length is greater than u16::MAX.
		let length = end.wrapping_sub(start);
		fail_if_none!(start >= U16_MAX_USIZE || length > U16_MAX_USIZE);
		Some(SliceRange(start as u16, length as u16))
	}
	pub fn write_extend(array: &mut Vec<u8>, value: &[u8]) -> SliceRange
	{
		let sptr = array.len() as u16;
		array.extend_from_slice(value);
		SliceRange(sptr, value.len() as u16)
	}
	pub fn get<'a>(self, array: &'a [u8]) -> Option<&'a [u8]>
	{
		let range = self.range()?;
		array.get(range)
	}
	pub fn range(self) -> Option<Range<usize>>
	{
		fail_if_none!(self.0 == u16::MAX);
		let a = self.0 as usize;
		let b = self.1 as usize;
		Some(a..a+b)
	}
	pub fn is_set(self) -> bool { self.0 < u16::MAX }
}

#[derive(Copy,Clone,Debug)]
#[repr(C)]	//Guarantee HeaderRange is 8 bytes.
pub struct HeaderRange(SliceRangeName,SliceRange);

impl HeaderRange
{
	pub fn none() -> HeaderRange { HeaderRange(SliceRangeName::none(), SliceRange::none()) }
	pub fn new(name: SliceRangeName, value: SliceRange) -> HeaderRange { HeaderRange(name, value) }
	pub fn get<'a>(self, array: &'a [u8]) -> Option<(MaybeStandardName<'a>, &'a [u8])>
	{
		if let (Some(x), Some(y)) = (self.0.get(array), self.1.get(array)) {
			Some((x, y))
		} else {
			None
		}
	}
	pub fn get_name<'a>(self, array: &'a [u8]) -> Option<MaybeStandardName<'a>> { self.0.get(array) }
	pub fn is_cookie(self) -> bool { self.0.is_cookie() }
	pub fn set_value(&mut self, v: SliceRange) { self.1 = v; }
	pub fn get_value_range(self) -> Option<Range<usize>> { self.1.range() }
	pub fn get_value_raw(self) -> SliceRange { self.1 }
	pub fn get_name_standard(self) -> Option<StandardHttpHeader> { self.0.try_as_standard() }
}
