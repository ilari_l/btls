use super::HeaderError;
use super::HeaderRange;
use super::HeaderSourceComponent;
use super::MaybeStandardName;
use super::SliceRange;
use super::SliceRangeName;
use btls_aux_fail::ResultExt;
use btls_aux_http::http::StandardHttpHeader;
use btls_aux_memory::split_at;
use btls_aux_memory::copy_if_fits;
use std::cmp::min;
use std::ops::Range;


const MAX_HBLOCK_SIZE: usize = 16384;
const MAX_HEADER_COUNT: usize = 512;

pub struct HeaderStorage
{
	storage: [u8;MAX_HBLOCK_SIZE],
	used: usize,
}

impl HeaderStorage
{
	pub fn new() -> HeaderStorage
	{
		HeaderStorage {
			storage: [0;MAX_HBLOCK_SIZE],
			used: 0,
		}
	}
	pub fn clear(&mut self)
	{
		self.used = 0;
	}
	pub fn borrow_raw<'a>(&'a self) -> &'a [u8] { self.storage.get(..self.used).unwrap_or(&self.storage) }
	pub fn get_name<'a>(&'a self, range: HeaderRange) -> Option<MaybeStandardName<'a>>
	{
		let slice = self.storage.get(..self.used)?;
		range.get_name(slice)
	}
	pub fn get_value2<'a>(&'a self, range: SliceRange) -> Option<&[u8]>
	{
		let slice = self.storage.get(..self.used)?;
		range.get(slice)
	}
	//Ok(None) means append is not possible. Otherwise on success returns updated range.
	pub fn write_append(&mut self, range: SliceRange, separator: &[u8], v: &[u8]) ->
		Result<Option<SliceRange>, HeaderError>
	{
		//If end of range does not match end of used part, append is not possible. If there is no previous
		//range, perform normal write.
		let range = match range.range() {
			Some(r) => r,
			None => return self.write(v).map(Some)
		};
		if range.end != self.used { return Ok(None); }
		//Calculate offsets and copy.
		let tstart = range.start;
		let mut end = self.used;
		Self::__copy_append(&mut self.storage, &mut end, separator)?;
		Self::__copy_append(&mut self.storage, &mut end, v)?;
		//Ok, successful. Commit.
		self.__commit_create_header(tstart, end, SliceRange::new2).map(Some)
	}
	pub fn write(&mut self, v: &[u8]) -> Result<SliceRange, HeaderError>
	{
		self.__write_single_common(v, SliceRange::new2)
	}
	pub fn write_name(&mut self, v: &[u8]) -> Result<SliceRangeName, HeaderError>
	{
		self.__write_single_common(v, SliceRangeName::new2)
	}
	pub fn write_multipart<'a,I:Iterator<Item=HeaderSourceComponent<'a>>>(&mut self, v: I) ->
		Result<SliceRange, HeaderError>
	{
		use HeaderError as HE;
		let start = self.used;
		//Need to split the block into used head and unused tail because ranges can operate on both at
		//once. The error case should never ever trip.
		let head: &mut [u8] = &mut self.storage;
		let (head, tail) = split_at(head, self.used).set_err(HE::TooLargeHeaders)?;
		let mut written = 0;
		for part in v {
			let src = match part {
				HeaderSourceComponent::Slice(x) => x,
				HeaderSourceComponent::Range(rng) => head.get(rng).ok_or(HE::IntBadPointer)?,
			};
			Self::__copy_append(tail, &mut written, src)?;
		}
		let end = start + written;
		self.__commit_create_header(start, end, SliceRange::new2)
	}
	fn __copy_append(buffer: &mut [u8], ptr: &mut usize, chunk: &[u8]) ->
		Result<(), HeaderError>
	{
		let amt = buffer.get_mut(*ptr..).and_then(|dst|copy_if_fits(dst, chunk)).
			ok_or(HeaderError::TooLargeHeaders)?;
		*ptr += amt;
		Ok(())
	}
	fn __write_single_common<T,F:FnOnce(usize, usize) -> Option<T>>(&mut self, v: &[u8], f: F) ->
		Result<T, HeaderError>
	{
		let start = self.used;
		let mut end = start;
		Self::__copy_append(&mut self.storage, &mut end, v)?;
		self.__commit_create_header(start, end, f)
	}
	fn __commit_create_header<T,F:FnOnce(usize, usize) -> Option<T>>(&mut self, start: usize, end: usize, f: F) ->
		Result<T, HeaderError>
	{
		//First grab the range before updating used, so the write is rolled back if the slice range
		//creation fails (it would only fail if offsets are too large, which they are not).
		let r = f(start, end).ok_or(HeaderError::TooLargeHeaders)?;
		self.used = end;
		Ok(r)
	}
}

pub struct HeaderTable
{
	storage: [HeaderRange; MAX_HEADER_COUNT],
	used: usize,
}

impl HeaderTable
{
	pub fn new() -> HeaderTable
	{
		HeaderTable {
			storage: [HeaderRange::none(); MAX_HEADER_COUNT],
			used: 0,
		}
	}
	pub fn borrow_raw<'a>(&'a self) -> &'a [HeaderRange]
	{
		self.storage.get(..self.used).unwrap_or(&self.storage)
	}
	pub fn clear(&mut self)
	{
		self.used = 0;
	}
	pub fn borrow_last_header(&mut self) -> Option<&mut HeaderRange>
	{
		let index = self.used.checked_sub(1)?;
		self.storage.get_mut(index)
	}
	pub fn push_header(&mut self, header: HeaderRange) -> bool
	{
		let slot = f_return!(self.storage.get_mut(self.used), false);
		*slot = header;
		//The above will trip before this overflows.
		self.used += 1;
		true
	}
	pub fn delete_matching<F:Fn(MaybeStandardName) -> bool>(&mut self, hstorage: &HeaderStorage,
		predicate: F)
	{
		self.__delete_matching_core(predicate, |header|hstorage.get_name(header))
	}
	pub fn delete_matching_std<F:Fn(StandardHttpHeader) -> bool>(&mut self, predicate: F)
	{
		//This will not delete stuff where get() returns None.
		self.__delete_matching_core(predicate, |header|header.get_name_standard())
	}
	pub fn count_cookies(&self) -> usize
	{
		let mut cookie_count = 0;
		//First, count number of cookies. If there is 0 or 1, there is nothing to fix.
		let headers = min(self.used, self.storage.len());
		for ritr in 0..headers {
			//This is smaller than self.header.len() and thus in-range.
			let header = unsafe{*self.storage.get_unchecked(ritr)};
			if header.is_cookie() { cookie_count += 1; }
		}
		cookie_count
	}
	pub fn collect_remove_cookies(&mut self, count: usize) -> Vec<Range<usize>>
	{
		let mut cookies = Vec::with_capacity(count);
		let mut witr = 0;
		let headers = min(self.used, self.storage.len());
		for ritr in 0..headers {
			//This is smaller than self.header.len() and thus in-range.
			let header = unsafe{*self.storage.get_unchecked(ritr)};
			if header.is_cookie() {
				//Remove cookie, save range.
				if let Some(value) = header.get_value_range() { cookies.push(value); }
			} else {
				//Keep header. Assume this always succeeds. witr increments by at most 1, so is
				//always less than ritr, which is in-range.
				unsafe{*self.storage.get_unchecked_mut(witr) = header};
				witr += 1;
			}
		}
		self.used = witr;
		cookies
	}
	fn __delete_matching_core<T,G:Fn(HeaderRange) -> Option<T>, F:Fn(T) -> bool>(&mut self, predicate: F, get: G)
	{
		let mut witr = 0;
		let headers = min(self.used, self.storage.len());
		for ritr in 0..headers {
			//This is less than self.header.len(), and thus in-range.
			let header = unsafe{*self.storage.get_unchecked(ritr)};
			let name = get(header);
			if name.map(|x|!predicate(x)).unwrap_or(true) {
				//witr can increment by at most 1 per iteration, and therefore it is always at
				//most ritr, which is always in range.
				unsafe{*self.storage.get_unchecked_mut(witr) = header};
				witr += 1;
			}
		}
		self.used = witr;
	}
}
