use std::fmt::Debug;


pub trait InternalBitOps
{
	fn zero() -> Self;
	fn not(self) -> Self;
	fn or_assign(&mut self, b: Self);
	fn or(self, b: Self) -> Self;
	fn and_assign(&mut self, b: Self);
	fn and(self, b: Self) -> Self;
	fn nonzero(self) -> bool;
}

macro_rules! def_internalbitops
{
	($xtype:ident) => {
		impl InternalBitOps for $xtype
		{
			fn zero() -> Self { 0 }
			fn not(self) -> Self { !self }
			fn or_assign(&mut self, b: Self) { *self |= b; }
			fn or(self, b: Self) -> Self { self | b }
			fn and_assign(&mut self, b: Self) { *self &= b; }
			fn and(self, b: Self) -> Self { self & b }
			fn nonzero(self) -> bool { self != 0 }
		}
	}
}

def_internalbitops!(u8);
def_internalbitops!(u16);

pub trait BitfieldTag
{
	type Storage: Copy+Sized+InternalBitOps+Debug;
}

#[macro_export]
macro_rules! define_bitfield_type
{
	($name:ident, $inner:ident) => {
		#[derive(Debug)]
		struct $name;
		impl crate::bitmask::BitfieldTag for $name { type Storage = $inner; }
	}
}

pub trait BitfieldBit: Copy
{
	type Tag: BitfieldTag;
	fn mask() -> <<Self as BitfieldBit>::Tag as BitfieldTag>::Storage;
}

#[macro_export]
macro_rules! define_bitfield_bit
{
	($name:ident, $tag:ident, $bnum:expr) => {
		#[derive(Copy,Clone)]
		struct $name;
		impl crate::bitmask::BitfieldBit for $name {
			type Tag = $tag;
			fn mask() -> <<Self as crate::bitmask::BitfieldBit>::Tag as
				crate::bitmask::BitfieldTag>::Storage
			{
				1 << $bnum
			}
		}
	}
}

#[derive(Copy,Debug)]
pub struct Bitfield<Tag:BitfieldTag>(Tag::Storage);

impl<Tag:BitfieldTag> Clone for Bitfield<Tag> { fn clone(&self) -> Self { Bitfield(self.0) }}

impl<Tag:BitfieldTag> Bitfield<Tag>
{
	pub fn new() -> Bitfield<Tag> { Bitfield(<<Tag as BitfieldTag>::Storage as InternalBitOps>::zero()) }
	pub fn get<Bit:BitfieldBit<Tag=Tag>>(&self, _: Bit) -> bool
	{
		self.0.and(<Bit as BitfieldBit>::mask()).nonzero()
	}
	pub fn clear_imp<Bit:BitfieldBit<Tag=Tag>>(&mut self, _: Bit)
	{
		self.0.and_assign(<Bit as BitfieldBit>::mask().not());
	}
	pub fn get_clear_imp<Bit:BitfieldBit<Tag=Tag>>(&mut self, b: Bit) -> bool
	{
		let r = self.get(b);
		self.clear_imp(b);
		r
	}
	pub fn get_set_imp<Bit:BitfieldBit<Tag=Tag>>(&mut self, b: Bit) -> bool
	{
		let r = self.get(b);
		self.set_imp(b);
		r
	}
	pub fn set_imp<Bit:BitfieldBit<Tag=Tag>>(&mut self, _: Bit)
	{
		self.0.or_assign(<Bit as BitfieldBit>::mask());
	}
	pub fn or_imp<Bit:BitfieldBit<Tag=Tag>>(&mut self, b: Bit, f: bool) { if f { self.set_imp(b) }; }
	pub fn force_imp<Bit:BitfieldBit<Tag=Tag>>(&mut self, b: Bit, f: bool)
	{
		if f { self.set_imp(b) } else { self.clear_imp(b) };
	}
	pub fn clear<Bit:BitfieldBit<Tag=Tag>>(self, _: Bit) -> Self
	{
		Bitfield(self.0.and(<Bit as BitfieldBit>::mask().not()))
	}
	pub fn set<Bit:BitfieldBit<Tag=Tag>>(self, _: Bit) -> Self
	{
		Bitfield(self.0.or(<Bit as BitfieldBit>::mask()))
	}
	pub fn force<Bit:BitfieldBit<Tag=Tag>>(self, b: Bit, f: bool) -> Self
	{
		if f { self.set(b) } else { self.clear(b) }
	}
}
