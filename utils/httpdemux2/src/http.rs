use crate::ConfigT;
use crate::bitmask::Bitfield;
use crate::connection::BackendName;
use crate::connection::Connector;
use crate::connection::ConnectorTrait;
use crate::connection::print_internal_response;
use crate::connection::print_internal_response_eurl;
use crate::connection_lowlevel::CID;
use crate::connection_midlevel::ConnectionFreeList;
use crate::connection_midlevel::SinkError;
use crate::connection_out::BackendError;
use crate::connection_out::ConnectionParameters as ConnectionOutParams;
use crate::headerblock::HeaderBlock;
use crate::headerblock::HeaderError;
use crate::headerblock::HeaderKind;
use crate::headerblock::HeaderSourceComponent;
use crate::headerblock::MaybeStandardName;
use crate::headerblock::OwnedTrailerBlock;
use crate::headerblock::RequestAttributes;
use crate::headerblock::SEFLAG_HOST_RAW;
use crate::headerblock::SEFLAG_PATH_RAW;
use crate::headerblock::serialize_headers;
use crate::headerblock::ShdrAuthorityO;
use crate::headerblock::ShdrAuthorityT;
use crate::headerblock::ShdrClientCn;
use crate::headerblock::ShdrClientSpkihash;
use crate::headerblock::ShdrContentType;
use crate::headerblock::ShdrIpaddr;
use crate::headerblock::ShdrMethod;
use crate::headerblock::ShdrPathO;
use crate::headerblock::ShdrPathT;
use crate::headerblock::ShdrProtocol;
use crate::headerblock::ShdrQuery;
use crate::headerblock::ShdrReason;
use crate::headerblock::ShdrScheme;
use crate::headerblock::ShdrUserAgent;
use crate::headerblock::ShdrVersion;
use crate::routing_dp::BackendData;
use crate::routing_dp::HostRoutingDataRef;
use crate::routing_dp::HostStringIndex2;
use crate::routing_dp::PathData;
use crate::routing_dp::RedirectMode;
use crate::routing_dp::RegularExpressionEntryData;
use crate::staticfile::add_date_header;
use crate::staticfile::ConnectionParameters as StaticFileParams;
use crate::utils::RcTextString;
use btls_aux_fail::ResultExt;
use btls_aux_http::http::EncodeHeaders;
use btls_aux_http::http::is_dangerous_header_response;
use btls_aux_http::http::StandardHttpHeader as HttpHdr;
use btls_aux_http::status::BAD_GATEWAY;
use btls_aux_http::status::BAD_REQUEST;
use btls_aux_http::status::FORBIDDEN;
use btls_aux_http::status::GATEWAY_TIMEOUT;
use btls_aux_http::status::get_default_reason;
use btls_aux_http::status::INTERNAL_SERVER_ERROR;
use btls_aux_http::status::MISDIRECTED_REQUEST;
use btls_aux_http::status::METHOD_NOT_ALLOWED;
use btls_aux_http::status::NO_CONTENT;
use btls_aux_http::status::NOT_FOUND;
use btls_aux_http::status::NOT_MODIFIED;
use btls_aux_http::status::RANGE_NOT_SATISFIABLE;
use btls_aux_memory::BEWriteBuffer;
use btls_aux_memory::PrintBuffer;
use btls_aux_memory::trim_ascii;
use btls_aux_unix::Path as UnixPath;
use btls_aux_unix::StatModeFormat;
use btls_daemon_helper_lib::cow;
use btls_util_logging::ConnId;
use btls_util_logging::log;
use btls_util_logging::syslog;
use btls_util_regex::RegexMatch;
pub use httpdemux2_hsts::on_hsts_preload_list;
use std::borrow::Cow;
use std::cell::RefCell;
use std::cmp::min;
use std::collections::HashSet;
use std::fmt::Debug;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::fmt::Write as FmtWrite;
use std::iter::once;
use std::mem::MaybeUninit;
use std::mem::replace;
use std::net::IpAddr;
use std::ops::Deref;
use std::ops::DerefMut;
use std::ops::Range;
use std::path::PathBuf;
use std::ptr::null_mut;
use std::rc::Rc;
use std::str::from_utf8;
use std::str::FromStr;
use std::sync::Arc;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::AtomicPtr;
use std::sync::atomic::Ordering as MemOrdering;
use std::usize::MAX as USIZE_MAX;

mod actions;
use self::actions::*;

const MAX_HBLOCK_SIZE: usize = 16384;
static DEFAULT_CT: &'static [u8] = b"application/octet-stream";
static TEXT_UTF8: &'static [u8] = b"text/plain;charset=utf-8";
static TEXT_ISO8859: &'static [u8] = b"text/plain;charset=Iso-8859-1";
static DEFAULT_CSP: &'static [u8] = b"default-src 'none'";
static FORCED_XCTO: &'static [u8] = b"nosniff";
static CHUNKED: &'static [u8] = b"chunked";
static ILLEGAL_EXPECT_100_DATA: &'static str = "Client sent data before getting go-ahead";
pub static CONNECTION_LOST: &'static str = "Connection to backend lost";
pub static CONNECT_ERROR: &'static str = "Error connecting to backend";
const CONNECT: &'static [u8] = b"CONNECT";
const OPTIONS: &'static [u8] = b"OPTIONS";
static BAD_METHOD6: &'static str = "Method 6 validation not allowed";
static BAD_ACME: &'static str = "ACME HTTP-01 validation not allowed";



#[derive(Debug,Clone)]
pub struct HeaderRewrite
{
	pub delete: HashSet<Vec<u8>>,
	pub add: Vec<(Vec<u8>, Vec<u8>)>,
}

impl HeaderRewrite
{
	//Only standard headers may be protected.
	fn is_protected(name: Option<HttpHdr>, response: bool) -> bool
	{
		match name {
			//Some specials.
			Some(HttpHdr::Cookie) => true,
			Some(HttpHdr::Location) => true,
			Some(HttpHdr::UserAgent) => true,
			//Hop-by-hop stuff
			Some(HttpHdr::Connection) => true,
			Some(HttpHdr::Upgrade) => true,
			//Alias of authority. Only in requests.
			Some(HttpHdr::Host) => !response,
			//Special requests.
			Some(HttpHdr::Expect) => !response,
			//The content attributes.
			Some(HttpHdr::ContentType) => true,
			Some(HttpHdr::ContentLength) => true,
			Some(HttpHdr::ContentEncoding) => true,
			//Transfer attributes
			Some(HttpHdr::TransferEncoding) => true,
			//Websockets
			Some(HttpHdr::SecWebsocketAccept) => true,
			Some(HttpHdr::SecWebsocketKey) => true,
			Some(HttpHdr::SecWebsocketVersion) => true,
			Some(HttpHdr::SecWebsocketProtocol) => true,
			Some(HttpHdr::SecWebsocketExtensions) => true,
			//Been-there and source. Only in requests.
			Some(HttpHdr::CdnLoop) => !response,
			Some(HttpHdr::Via) => !response,
			Some(HttpHdr::XRealIp) => !response,
			Some(HttpHdr::XForwardedClientCn) => !response,
			Some(HttpHdr::XForwardedClientSpkihash) => !response,
			Some(HttpHdr::XForwardedFor) => !response,
			Some(HttpHdr::XForwardedHost) => !response,
			Some(HttpHdr::XForwardedProto) => !response,
			Some(HttpHdr::XForwardedSecure) => !response,
			Some(HttpHdr::XForwardedServer) => !response,
			//Some security headers that are forced, response only.
			Some(HttpHdr::ContentSecurityPolicy) => response,
			Some(HttpHdr::XContentTypeOptions) => response,
			//The remaining are not protected.
			_ => false,
		}
	}
	fn rewrite_headers(&self, block: &mut HeaderBlock, response: bool)
	{
		if block.is_overflow().is_some() { return; }
		//First, delete all headers that need to be deleted. Cookie is special as it can be deleted even
		//if it is otherwise protected!
		block.delete_matching(|x|{
			let xs = x.as_standard();
			let protected = HeaderRewrite::is_protected(xs, response);
			self.delete.contains(x.as_bytes()) && (!protected || xs == Some(HttpHdr::Cookie))
		});
		//Then force all.
		for &(ref name, ref value) in self.add.iter() {
			//Do not do anything with protected headers.
			let name = MaybeStandardName::from(name.deref());
			if HeaderRewrite::is_protected(name.as_standard(), response) { continue; }
			let mut statustmp = [MaybeUninit::uninit();3];
			let hitr = if response {
				let mut tmp = PrintBuffer::new(&mut statustmp);
				write!(tmp, "{status}", status=block.get_status()).ok();
				let status = tmp.into_inner_bytes();
				InterpolateIterator {
					ptr: value,
					pos: 0,
					a: 0..0,
					c: 0..0,
					h: 0..0,
					i: 0..0,
					m: 0..0,
					p: 0..0,
					q: 0..0,
					r: block.get_special_range(ShdrReason).unwrap_or(0..0),
					s: Err(status),
				}
			} else {
				//Due to legacy reasons, this uses translated authority/path.
				InterpolateIterator {
					ptr: value,
					pos: 0,
					a: block.get_special_range(ShdrAuthorityT).unwrap_or(0..0),
					c: block.get_special_range(ShdrClientCn).unwrap_or(0..0),
					h: block.get_special_range(ShdrClientSpkihash).unwrap_or(0..0),
					i: block.get_special_range(ShdrIpaddr).unwrap_or(0..0),
					m: block.get_special_range(ShdrMethod).unwrap_or(0..0),
					p: block.get_special_range(ShdrPathT).unwrap_or(0..0),
					q: block.get_special_range(ShdrQuery).unwrap_or(0..0),
					r: 0..0,
					s: Ok(block.get_special_range(ShdrScheme).unwrap_or(0..0),),
				}
			};
			match name.try_as_standard() {
				Ok(name) => block.set_header_multipart_std(name, hitr),
				Err(name) => block.set_header_multipart(name, hitr),
			}
		}
	}
}

struct InterpolateIterator<'a>
{
	ptr: &'a [u8],
	pos: usize,
	a: Range<usize>,
	c: Range<usize>,
	h: Range<usize>,
	i: Range<usize>,
	m: Range<usize>,
	p: Range<usize>,
	q: Range<usize>,
	r: Range<usize>,
	s: Result<Range<usize>, &'a [u8]>,
}

impl<'a> Iterator for InterpolateIterator<'a>
{
	type Item = HeaderSourceComponent<'a>;
	fn next(&mut self) -> Option<HeaderSourceComponent<'a>>
	{
		match self.ptr.get(self.pos)? {
			&b'%' => {
				let r = match self.ptr.get(self.pos+1) {
					Some(&b'a') => HeaderSourceComponent::Range(self.a.clone()),
					Some(&b'c') => HeaderSourceComponent::Range(self.c.clone()),
					Some(&b'h') => HeaderSourceComponent::Range(self.h.clone()),
					Some(&b'i') => HeaderSourceComponent::Range(self.i.clone()),
					Some(&b'm') => HeaderSourceComponent::Range(self.m.clone()),
					Some(&b'p') => HeaderSourceComponent::Range(self.p.clone()),
					Some(&b'q') => HeaderSourceComponent::Range(self.q.clone()),
					Some(&b'r') => HeaderSourceComponent::Range(self.r.clone()),
					Some(&b's') => match self.s.clone() {
						Ok(s) => HeaderSourceComponent::Range(s),
						Err(s) => HeaderSourceComponent::Slice(s),
					},
					Some(&b'%') => HeaderSourceComponent::Slice(b"%"),
					_ => HeaderSourceComponent::Range(0..0),
				};
				self.pos += 2;
				Some(r)
			},
			_ => {
				let spos = self.pos;
				while *self.ptr.get(self.pos).unwrap_or(&b'%') != b'%' {
					self.pos += 1;
				}
				Some(HeaderSourceComponent::Slice(&self.ptr[spos..self.pos]))
			},
		}
	}
}

#[test]
fn test_header_rewriting()
{
	use btls_aux_memory::SafeShowByteString;
	let mut block = HeaderBlock::new();
	block.set_authority(b"authority_foo");
	block.set_special(ShdrIpaddr, b"[2001:db8::1]");
	block.set_special(ShdrMethod, b"GET");
	block.set_special(ShdrPathT, b"/path/to/somewhere");
	block.set_special(ShdrQuery, b"?q");
	block.set_special(ShdrScheme, b"https");
	block.set_header(b"todelete", b"foo");
	block.set_header(b"retain", b"bar");
	block.set_header(b"zot", b"yyy");
	block.set_header_std(HttpHdr::CdnLoop, b"protected");
	let mut rw = HeaderRewrite {
		delete: HashSet::new(),
		add: Vec::new(),
	};
	rw.delete.insert("todelete".to_owned().into_bytes());
	rw.delete.insert("cdn-loop".to_owned().into_bytes());
	rw.add.push(("zot".to_owned().into_bytes(),
		"authority=%a ip=%i method=%m path=%p query=%q scheme=%s!!!%%!!!".to_owned().into_bytes()));
	rw.add.push(("cdn-loop".to_owned().into_bytes(), "inserted".to_owned().into_bytes()));
	rw.rewrite_headers(&mut block, false);
	let mut hbuf = [MaybeUninit::uninit();4096];
	let hbuf = serialize_headers::<::btls_aux_http::http1::HeaderEncoder>(&mut hbuf, &mut block,
		HeaderKind::Request(0)).unwrap();
	assert_eq!(SafeShowByteString(hbuf).to_string(), "GET /path/to/somewhere?q HTTP/1.1\r\n\
host:authority_foo\r\n\
x-forwarded-for:[2001:db8::1]\r\n\
x-forwarded-host:authority_foo\r\n\
x-forwarded-proto:https\r\n\
x-forwarded-server:authority_foo\r\n\
x-real-ip:[2001:db8::1]\r\n\
retain:bar\r\n\
zot:yyy\r\n\
cdn-loop:protected\r\n\
zot:authority=authority_foo ip=[2001:db8::1] method=GET path=/path/to/somewhere query=?q scheme=https!!!%!!!\r\n\
\r\n");
}

#[test]
fn test_cookie_magic()
{
	let mut block = HeaderBlock::new();
	block.set_authority(b"authority_foo");
	block.set_special(ShdrIpaddr, b"[2001:db8::1]");
	block.set_special(ShdrMethod, b"GET");
	block.set_special(ShdrPathT, b"/path/to/somewhere");
	block.set_special(ShdrQuery, b"?q");
	block.set_special(ShdrScheme, b"https");
	block.set_header_std(HttpHdr::Cookie, b"foo=1");
	block.set_header_std(HttpHdr::Cookie, b"bar=3");
	block.set_header(b"xyzzy", b"yyy");
	block.set_header_std(HttpHdr::Cookie, b"zot=42");
	let mut hbuf = [MaybeUninit::uninit();4096];
	let hbuf = serialize_headers::<::btls_aux_http::http1::HeaderEncoder>(&mut hbuf, &mut block,
		HeaderKind::Request(0)).unwrap();
	assert_eq!(String::from_utf8_lossy(hbuf), "GET /path/to/somewhere?q HTTP/1.1\r\n\
host:authority_foo\r\n\
x-forwarded-for:[2001:db8::1]\r\n\
x-forwarded-host:authority_foo\r\n\
x-forwarded-proto:https\r\n\
x-forwarded-server:authority_foo\r\n\
x-real-ip:[2001:db8::1]\r\n\
cookie:foo=1; bar=3\r\n\
xyzzy:yyy\r\n\
cookie:zot=42\r\n\
\r\n");
	block._fix_http2_cookies();
	let mut hbuf = [MaybeUninit::uninit();4096];
	let hbuf = serialize_headers::<::btls_aux_http::http1::HeaderEncoder>(&mut hbuf, &mut block,
		HeaderKind::Request(0)).unwrap();
	assert_eq!(String::from_utf8_lossy(hbuf), "GET /path/to/somewhere?q HTTP/1.1\r\n\
host:authority_foo\r\n\
x-forwarded-for:[2001:db8::1]\r\n\
x-forwarded-host:authority_foo\r\n\
x-forwarded-proto:https\r\n\
x-forwarded-server:authority_foo\r\n\
x-real-ip:[2001:db8::1]\r\n\
xyzzy:yyy\r\n\
cookie:foo=1; bar=3; zot=42\r\n\
\r\n");
}

#[derive(Debug)]
pub struct SendResponseToken
{
	not_sent: AtomicBool,
	g: ConfigT,
}

impl Drop for SendResponseToken
{
	fn drop(&mut self)
	{
		//If the token is dropped without sending, count it as no response.
		self.noresponse();
	}
}

impl SendResponseToken
{
	fn new(g: ConfigT) -> SendResponseToken
	{
		increment_metric!(http_reqs g);
		SendResponseToken {
			not_sent: AtomicBool::new(true),
			g: g,
		}
	}
	fn noresponse(&self) -> bool
	{
		if self.not_sent.swap(false, MemOrdering::Relaxed) {
			increment_metric!(http_reqs_noresp self.g);
			true
		} else {
			false
		}
	}
	fn backend(&self) -> bool
	{
		if self.not_sent.swap(false, MemOrdering::Relaxed) {
			increment_metric!(http_reqs_bend self.g);
			true
		} else {
			false
		}
	}
	fn internal(&self) -> bool
	{
		if self.not_sent.swap(false, MemOrdering::Relaxed) {
			increment_metric!(http_reqs_int self.g);
			true
		} else {
			false
		}
	}
}

#[derive(Clone,Debug)]
pub struct EarlyRequestInfo
{
	//The name of the proxy, for anti-looping.
	pub proxyname: Arc<Vec<u8>>,
	//The URL accessed, if known.
	pub url: Rc<String>,
	//Client IP, if known.
	pub client_ip: Rc<String>,
	//Emitted fate.
	pub response_token: Rc<SendResponseToken>,
	//Log flags.
	pub logflags: u32,
	//Is curl?
	is_mozilla: bool,
}

fn xreq_get_proxyname<'a>(xreq: Result<&'a RequestInfo, &'a EarlyRequestInfo>) -> &'a [u8]
{
	match xreq { Ok(req) => req.proxyname.deref(), Err(ereq) => ereq.proxyname.deref()}.deref()
}

fn xreq_get_url<'a>(xreq: Result<&'a RequestInfo, &'a EarlyRequestInfo>) -> &'a str
{
	match xreq { Ok(req) => req.url.deref(), Err(ereq) => ereq.url.deref()}.deref()
}

fn xreq_get_token<'a>(xreq: Result<&'a RequestInfo, &'a EarlyRequestInfo>) -> &'a SendResponseToken
{
	match xreq { Ok(req) => req.response_token.deref(), Err(ereq) => ereq.response_token.deref()}
}

fn maybe_reqinfo_inner<'a>(xreq: &'a Result<(RequestInfo, HostRoutingDataRef), EarlyRequestInfo>) ->
	Result<&'a RequestInfo, &'a EarlyRequestInfo>
{
	match xreq { &Ok((ref x,_)) => Ok(x), &Err(ref x) => Err(x) }
}

fn maybe_reqinfo_inner_h<'a>(xreq: &'a Result<(RequestInfo, HostRoutingDataRef), EarlyRequestInfo>) ->
	(Result<&'a RequestInfo, &'a EarlyRequestInfo>, Option<&'a HostRoutingDataRef>)
{
	match xreq {
		&Ok((ref x,ref y)) => (Ok(x), Some(y)),
		&Err(ref x) => (Err(x), None)
	}
}

#[derive(Copy,Clone,Debug)]
pub struct RequestInfoCfg
{
	//Add CORS header.
	pub add_cors: bool,
	//Force MIME type.
	pub force_mime: Option<HostStringIndex2>,
	//Serialization flags.
	pub s_flags: u8,
	//Log flags.
	pub logflags: u32,
	///Forced error code.
	pub force_error: Option<u16>,
}

#[derive(Clone,Debug)]
pub struct RequestInfo
{
	//The name of the proxy, for anti-looping.
	pub proxyname: Arc<Vec<u8>>,
	//Request attributes.
	pub attributes: RequestAttributes,
	//The URL accessed.
	pub url: Rc<String>,
	//The connection ID of master connection.
	pub cid: CID,
	//The stream ID under master connection.
	pub sid: u32,
	//The connection ID of slave connection.
	pub slave_cid: CID,
	//Configuration parameters.
	pub cfg: RequestInfoCfg,
	//Client IP.
	pub client_ip: Rc<String>,
	//Emitted fate.
	pub response_token: Rc<SendResponseToken>,
	///No redirect fixup.
	pub no_redirect_fixup: bool,
	///Is curl?
	pub is_mozilla: bool,
}

impl RequestInfo
{
	pub fn fate_backend(&self) { self.response_token.backend(); }
}

fn is_info(status: u16) -> bool { status >= 100 && status < 200 }
fn is_buggy_redirect(status: u16) -> bool { status == 301 || status == 302 }


thread_local!(static TMP_HEADERBLOCK: Rc<RefCell<HeaderBlock>> = Rc::new(RefCell::new(HeaderBlock::new())) );
thread_local!(static TEST_PLAIN_RESPONSE: Rc<RefCell<bool>> = Rc::new(RefCell::new(false)) );

pub fn test_set_plain_response()
{
	TEST_PLAIN_RESPONSE.with(|x|*x.borrow_mut() = true);
}


pub trait InternalResponseWriter
{
	///The encoder to use for header blocks.
	type Encoder: EncodeHeaders;
	///Error type for operations.
	type Error: Sized+Display+Debug;
	///Get the maximum size of payload.
	fn query_maxsize(&self) -> usize;
	///Send headers-only payload, with request ending immediately after.
	fn flush_headers_only(&mut self, headers: &[u8]) ->
		Result<(), <Self as InternalResponseWriter>::Error>;
	///Send headers followed by payload, with request ending after payload.
	fn flush_headers_and_payload(&mut self, headers: &[u8], payload: &[u8]) ->
		Result<(), <Self as InternalResponseWriter>::Error>;
	///Return a precanned double fault headers.
	fn df_precan_headers() -> &'static [u8];
	///Terminate this stream and return ok. If not possible, return error to tear down the entiere connection.
	fn error_forward_failure<D:Display>(&mut self, cause: D) ->
		Result<(), <Self as InternalResponseWriter>::Error>;
	///Get the CID and SID to use in requests.
	fn get_cid_and_sid(&self) -> (CID, u32);
	///True to include content-length, false to omit it.
	fn have_content_length(&self) -> bool;
}

#[derive(Clone,Debug)]
pub struct DfltIRWError(String);

impl DfltIRWError
{
	pub fn new<D:Display>(item: D) -> DfltIRWError { DfltIRWError(item.to_string()) }
}

impl Display for DfltIRWError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> { Display::fmt(&self.0, f) }
}


#[derive(Clone)]
pub(crate) enum InternalResponse<'a>
{
	Redirect(RedirectMode, Cow<'a,str>),	//Target
	BadRequestF(&'a str),			//Reason.
	NotAcceptableS(&'a str),		//Reason.
	RangeNotSatisfiable(&'a str, u64),	//Reason, length
	MisdirectedRequest,
	RejectedHost(&'a str),			//Reason.
	NoBodySuccess(&'a str),			//Reason.
	GenericError(u8, Option<Cow<'a,str>>, bool),	//Code - 400, reason(or_default), warning.
	GlobalOptions,
}

enum ResponseCategory
{
	Redirect,		//Redirect
	Error,			//Error.
	MethodNotAllowed,	//Method not allowed.
	RangeError,		//Range error.
	NoContent,		//No content.
	BadHost,		//Bad host.
	GlobalOptions,		//Global options.
}

const R_TEXT_PLAIN: u16 = 1;
const R_STRICT_CSP: u16 = 2;
const R_DEFAULT: u16 = 4;
const R_FATAL: u16 = 8;
const R_WARNING: u16 = 16;
const R_NOHOST: u16 = 32;
const R_EURL_FLAGS: u16 = 48;
const EURL_SHIFT: u32 = 4;
static BAD_VHOST: &'static str = "Unknown virtual host";
static GLOBAL_OPTIONS: &'static str = "Responding to server-global OPTIONS";

impl<'a> InternalResponse<'a>
{
	pub fn err(code: u16, x: impl Into<Cow<'a,str>>) -> InternalResponse<'a>
	{
		//5xx errors generate warnings anyway.
		Self::__err(code, Some(x), code / 100 == 5)
	}
	fn err_w(code: u16, x: impl Into<Cow<'a,str>>) -> InternalResponse<'a>
	{
		Self::__err(code, Some(x), true)
	}
	fn err_dflt(code: u16) -> InternalResponse<'a> { Self::__err::<String>(code, None, false) }
	fn __err<X:Into<Cow<'a,str>>>(code: u16, x: Option<X>, warn: bool) -> InternalResponse<'a>
	{
		InternalResponse::GenericError((code - 400) as u8, x.map(|x|x.into()), warn)
	}
	fn redir(mode: RedirectMode, x: impl Into<Cow<'a,str>>) -> InternalResponse<'a>
	{
		InternalResponse::Redirect(mode, x.into())
	}
	fn get_reason(&'a self, unsafe_req: bool) -> Cow<'a, str>
	{
		use self::InternalResponse::*;
		match self {
			&Redirect(RedirectMode::MovedPermanently, _)|&Redirect(RedirectMode::Found, _)
				if unsafe_req => get_default_reason(METHOD_NOT_ALLOWED),
			&Redirect(m, _) => get_default_reason(m.status()),
			&BadRequestF(r)|&NotAcceptableS(r)|
				&RangeNotSatisfiable(r,_)|&RejectedHost(r)|&NoBodySuccess(r) => Cow::Borrowed(r),
			&GenericError(_,Some(ref r),_) => Cow::Borrowed(r.deref()),
			&MisdirectedRequest => Cow::Borrowed(BAD_VHOST),
			&GenericError(c,None,_) => get_default_reason(400 + c as u16),
			&GlobalOptions => Cow::Borrowed(GLOBAL_OPTIONS),
		}
	}
	fn get_status(&self, unsafe_req: bool) -> u16
	{
		use self::InternalResponse::*;
		match self {
			&Redirect(RedirectMode::MovedPermanently, _)|&Redirect(RedirectMode::Found, _)
				if unsafe_req => METHOD_NOT_ALLOWED,
			&Redirect(m, _) => m.status(),
			&BadRequestF(_) => BAD_REQUEST,
			&NotAcceptableS(_) => METHOD_NOT_ALLOWED,
			&RangeNotSatisfiable(_,_) => RANGE_NOT_SATISFIABLE,
			&MisdirectedRequest|&RejectedHost(_) => MISDIRECTED_REQUEST,
			&NoBodySuccess(_) => NO_CONTENT,
			&GenericError(c,_,_) => 400 + c as u16,
			&GlobalOptions => NO_CONTENT,
		}
	}
	fn get_argument(&self) -> u64
	{
		use self::InternalResponse::*;
		match self {
			//Only RangeNotSatisfiable has nontrivial argument.
			&RangeNotSatisfiable(_,l) => l,
			_ => 0
		}
	}
	fn get_message(&'a self) -> &'a str
	{
		use self::InternalResponse::*;
		match self {
			//This is not strictly correct, but R_METHOD_NOT_ALLOWED does not use the message.
			&Redirect(_, ref r) => r.deref(),
			&BadRequestF(r)|&NotAcceptableS(r)|&RangeNotSatisfiable(r,_)|&RejectedHost(r)|
				&NoBodySuccess(r) => r,
			&GenericError(_,Some(ref r),_) => r.deref(),
			&MisdirectedRequest => BAD_VHOST,
			&GenericError(c,None,_) => match get_default_reason(400 + c as u16) {
				Cow::Owned(_) => "Unknown Error",
				Cow::Borrowed(s) => s,
			},
			&GlobalOptions => GLOBAL_OPTIONS,
		}
	}
	fn get_category(&self, unsafe_req: bool) -> ResponseCategory
	{
		use self::InternalResponse::*;
		match self {
			&Redirect(RedirectMode::MovedPermanently, _)|&Redirect(RedirectMode::Found, _)
				if unsafe_req => ResponseCategory::MethodNotAllowed,
			&Redirect(_, _) => ResponseCategory::Redirect,
			&RejectedHost(_)|&BadRequestF(_)|&GenericError(_,_,_) => ResponseCategory::Error,
			&NotAcceptableS(_) => ResponseCategory::MethodNotAllowed,
			&RangeNotSatisfiable(_,_) => ResponseCategory::RangeError,
			&MisdirectedRequest => ResponseCategory::BadHost,
			&NoBodySuccess(_) => ResponseCategory::NoContent,
			&GlobalOptions => ResponseCategory::GlobalOptions,
		}
	}
	fn get_flags(&self) -> u16
	{
		const XHTML: u16 = R_TEXT_PLAIN|R_STRICT_CSP;
		use self::InternalResponse::*;
		match self {
			//Strictly speaking not corret, but both redirects and failed redirects happen to have
			//the same flags.
			&Redirect(_, _) => XHTML,
			//Ordinary responses.
			&RejectedHost(_)|&NotAcceptableS(_)|
				&RangeNotSatisfiable(_,_)|&GenericError(_,_,false) => XHTML,
			//Few responses are important enough to print at WARNING level.
			&GenericError(_,_,true) => XHTML|R_WARNING,
			//No such host is a bit special. It is controlled by bit 8 of log flags, instead of bit
			//7 non-warning internal responses usually are.
			&MisdirectedRequest => XHTML|R_NOHOST,
			//This should not have R_TEXT_PLAIN nor R_STRICT_CSP, as it has no body.
			&NoBodySuccess(_) => 0,
			&GlobalOptions => 0,
			//Do not use R_FATAL for anything else, it will not work.
			&BadRequestF(_) => XHTML|R_FATAL,
		}
	}
}

impl<'a> Display for InternalResponse<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::InternalResponse::*;
		match self {
			&Redirect(m, ref target) => {
				let kind = match m {
					RedirectMode::MovedPermanently => "Moved permanently",
					RedirectMode::Found => "Found",
					RedirectMode::TemporaryRedirect => "Temporary redirect",
					RedirectMode::PermanentRedirect => "Permanent redirect",
				};
				write!(f, "{kind} -> {target}")
			},
			&BadRequestF(err) => write!(f, "Bad request: {err}"),
			&NotAcceptableS(err) => write!(f, "Method not allowed: {err}"),
			&RangeNotSatisfiable(err,_) => write!(f, "Range not satisfiable: {err}"),
			&MisdirectedRequest => f.write_str(BAD_VHOST),
			&RejectedHost(err) => write!(f, "Rejected host: {err}"),
			&NoBodySuccess(err) => write!(f, "No body: {err}"),
			&GenericError(code,Some(ref err),_) =>
				write!(f, "{reason}: {err}", reason=get_default_reason(400 + code as u16)),
			&GenericError(code,None,_) => write!(f, "Forced error: {code}", code=400+code as u16),
			&GlobalOptions => write!(f, "Global OPTIONS"),
		}
	}
}

include!(concat!(env!("OUT_DIR"), "/product.inc"));

static _PRODUCT: AtomicPtr<(String, String)> = AtomicPtr::new(null_mut());
static _PRODUCT_SECONDARY: AtomicPtr<Vec<(String, String)>> = AtomicPtr::new(null_mut());

fn get_product_name_version() -> (&'static [u8], &'static [u8])
{
	unsafe{
		let x = _PRODUCT.load(MemOrdering::Relaxed);
		if !x.is_null() {
			((*x).0.as_bytes(), (*x).1.as_bytes())
		} else {
			(PRODUCT_NAME.as_bytes(), PRODUCT_VERSION.as_bytes())
		}
	}
}

fn get_product_name_version_stock() -> (&'static [u8], &'static [u8])
{
	(PRODUCT_NAME.as_bytes(), PRODUCT_VERSION.as_bytes())
}

fn get_product_name_version_secondary() -> &'static [(String, String)]
{
	unsafe{
		let x = _PRODUCT_SECONDARY.load(MemOrdering::Relaxed);
		if !x.is_null() { &*x } else { &[] }
	}
}

pub fn set_product_name_and_version(name: &'static str, version: &'static str)
{
	let newval = Box::into_raw(Box::new((name.to_owned(), version.to_owned())));
	_PRODUCT.store(newval, MemOrdering::Release);
}

pub fn set_secondary_product_name_and_version(data: &[(&str, &str)])
{
	let v: Vec<(String, String)> = data.iter().map(|(n,v)|((*n).to_owned(), (*v).to_owned())).collect();
	_PRODUCT_SECONDARY.store(Box::into_raw(Box::new(v)), MemOrdering::Release);
}

macro_rules! hitr_ret_str
{
	($state:expr, $str:expr) => {{ $state += 1; return Some(HeaderSourceComponent::Slice($str)) }};
}

struct ProductIter<'a>((&'a [u8], &'a [u8]), u8);

impl<'a> Iterator for ProductIter<'a>
{
	type Item = HeaderSourceComponent<'a>;
	fn next(&mut self) -> Option<HeaderSourceComponent<'a>>
	{
		match self.1 {
			0 => hitr_ret_str!(self.1, (self.0).0),
			1 => hitr_ret_str!(self.1, b"/"),
			2 => hitr_ret_str!(self.1, (self.0).1),
			3..=255 => None
		}
	}
}

struct ProductsIter<'a>(&'a [(String, String)], usize);

impl<'a> Iterator for ProductsIter<'a>
{
	type Item = HeaderSourceComponent<'a>;
	fn next(&mut self) -> Option<HeaderSourceComponent<'a>>
	{
		let elem = self.0.get(self.1 >> 2)?;
		match self.1 & 3 {
			0 => hitr_ret_str!(self.1, b" "),
			1 => hitr_ret_str!(self.1, elem.0.as_bytes()),
			2 => hitr_ret_str!(self.1, b"/"),
			3 => hitr_ret_str!(self.1, elem.1.as_bytes()),
			_ => None	//Not reachable.
		}
	}
}

fn hsc_sl<'a>(y: &'a str) -> std::iter::Once<HeaderSourceComponent<'a>>
{
	once(HeaderSourceComponent::Slice(y.as_bytes()))
}

pub fn add_server(block: &mut HeaderBlock, clazz: &str)
{
	let product = ProductIter(get_product_name_version(), 0);
	let stock = ProductIter(get_product_name_version_stock(), 0);
	let secondary = get_product_name_version_secondary();
	let space = hsc_sl(" ");
	let clazz = hsc_sl(clazz);
	if secondary.len() > 0 {
		//product (stock, secondary) clazz
		let p = product.chain(space.clone()).chain(stock).chain(ProductsIter(secondary, 0)).
			chain(space).chain(clazz);
		block.set_header_multipart_std(HttpHdr::Server, p);
	} else {
		//product clazz
		let p = product.chain(space).chain(clazz);
		block.set_header_multipart_std(HttpHdr::Server, p);
	}
}

fn copy_product_to_html(prod: (&[u8], &[u8]), payload: &mut BEWriteBuffer)
{
	htmlescape(prod.0, payload); payload.append(b" "); htmlescape(prod.1, payload);
}

fn copy_server_to_html(payload: &mut BEWriteBuffer)
{
	copy_product_to_html(get_product_name_version(), payload);
	let secondary = get_product_name_version_secondary();
	if secondary.len() > 0 {
		payload.append(b" (");
		copy_product_to_html(get_product_name_version_stock(), payload);
		for (sname, sver) in secondary.iter() {
			payload.append(b", ");
			copy_product_to_html((sname.as_bytes(), sver.as_bytes()), payload);
		}
		payload.append(b")");
	}
}

fn htmlescape(data: &[u8], output: &mut BEWriteBuffer)
{
	for ch in data.iter().cloned() { match ch {
		b'<' => output.append(b"&lt;"),
		b'>' => output.append(b"&gt;"),
		b'&' => output.append(b"&amp;"),
		b'"' => output.append(b"&quot;"),
		b'\'' => output.append(b"&apos;"),
		ch => output.append_byte(ch),
	}}
}

#[derive(Copy,Clone)]
enum InterpolateComponent
{
	Raw(&'static str),
	StatusCode,
	Reason,
	ReasonRaw,
	HostName,
	Server,
	Linebreak2,
}

static XHTML_UTF8: &'static [u8] = b"application/xhtml+xml;charset=utf-8";
static ERROR_MSG: &'static [InterpolateComponent] = &[
	InterpolateComponent::Raw(r##"<?xml version="1.0"?>
<html:html xmlns:html="http://www.w3.org/1999/xhtml"><html:head><html:title>Error "##),
	InterpolateComponent::StatusCode,
	InterpolateComponent::Raw(r##"</html:title></html:head><html:body><html:h1>HTTP error"##),
	InterpolateComponent::Linebreak2,
	InterpolateComponent::StatusCode,
	InterpolateComponent::Raw(r##" "##),
	InterpolateComponent::Reason,
	InterpolateComponent::Linebreak2,
	InterpolateComponent::Raw(r##"</html:h1><html:hr/>"##),
	InterpolateComponent::Server,
	InterpolateComponent::Raw(r##"</html:body></html:html>"##)
];

static ERROR_MSG_TEXT: &'static [InterpolateComponent] = &[
	InterpolateComponent::StatusCode,
	InterpolateComponent::Raw(r##" "##),
	InterpolateComponent::ReasonRaw,
	InterpolateComponent::Raw("\r\n"),
];

static REDIRECT_MSG: &'static [InterpolateComponent] = &[
	InterpolateComponent::Raw(r##"<?xml version="1.0"?>
<html:html xmlns:html="http://www.w3.org/1999/xhtml"><html:head><html:title>Redirecting...</html:title>
</html:head><html:body><html:h1>Redireting to <html:a href=""##),
	InterpolateComponent::Reason,
	InterpolateComponent::Raw(r##"">"##),
	InterpolateComponent::Linebreak2,
	InterpolateComponent::Reason,
	InterpolateComponent::Linebreak2,
	InterpolateComponent::Raw(r##"</html:a></html:h1><html:hr/>"##),
	InterpolateComponent::Server,
	InterpolateComponent::Raw(r##"</html:body></html:html>"##)
];

static REDIRECT_MSG_TEXT: &'static [InterpolateComponent] = &[
	InterpolateComponent::Raw(r##"Redireting to "##),
	InterpolateComponent::Reason,
	InterpolateComponent::Raw("\r\n"),
];

static UNKNOWN_HOST_MSG: &'static [InterpolateComponent] = &[
	InterpolateComponent::Raw(r##"<?xml version="1.0"?>
<html:html xmlns:html="http://www.w3.org/1999/xhtml"><html:head><html:title>Unknown Host</html:title>
</html:head><html:body><html:h1>"##),
	InterpolateComponent::Linebreak2,
	InterpolateComponent::Raw(r##"What the heck is "##),
	InterpolateComponent::HostName,
	InterpolateComponent::Raw(r##"??? It is not in virtual host database."##),
	InterpolateComponent::Linebreak2,
	InterpolateComponent::Raw(r##"</html:h1><html:hr/>"##),
	InterpolateComponent::Server,
	InterpolateComponent::Raw(r##"</html:body></html:html>"##)
];

static UNKNOWN_HOST_MSG_TXT: &'static [InterpolateComponent] = &[
	InterpolateComponent::Raw("Unknown virtual host\r\n"),
];

pub(crate) fn send_internal_response<'a,I:InternalResponseWriter>(sink: &mut I,
	request: Result<&RequestInfo, &EarlyRequestInfo>, hostdata: Option<&HostRoutingDataRef>, head: bool,
	response: &InternalResponse) ->
	Result<bool, I::Error>
{
	let mut statustmp = [MaybeUninit::uninit();3];
	let mut force_text = TEST_PLAIN_RESPONSE.with(|x|*x.borrow());
	force_text |= match request { Ok(r) => !r.is_mozilla, Err(r) => !r.is_mozilla };
	TMP_HEADERBLOCK.with(|x|{
		let mut block = x.borrow_mut();
		block.clear();
		let maxsize = if head { USIZE_MAX } else { sink.query_maxsize() };
		let unsafe_req = request.map(|x|x.attributes.is_unsafe()).unwrap_or(false);
		let code = response.get_status(unsafe_req);
		let category = response.get_category(unsafe_req);
		let len = response.get_argument();
		let msg = response.get_message();
		let flags = response.get_flags();
		block.set_status(code);
		block.set_special(ShdrReason, response.get_reason(unsafe_req).as_bytes());
		add_server(&mut block, "internal-response");
		//Status codes 204 and 304 are special in that there is no content-length.
		let no_content = code == NO_CONTENT || code == NOT_MODIFIED;

		let mut statustmp = PrintBuffer::new(&mut statustmp);
		write!(statustmp, "{code}").ok();
		let statustmp = statustmp.into_inner_bytes();
		let mut reason = msg;
		let mut payload_segs2: &'static [InterpolateComponent] = ERROR_MSG;
		if force_text { payload_segs2 = ERROR_MSG_TEXT; }

		match category {
			ResponseCategory::Redirect => {
				payload_segs2 = REDIRECT_MSG;	//Redirect, not error.
				if force_text { payload_segs2 = REDIRECT_MSG_TEXT; }
				block.set_header_std(HttpHdr::Location, msg.as_bytes());
			},
			ResponseCategory::BadHost => {
				payload_segs2 = UNKNOWN_HOST_MSG;	//Special error.
				if force_text { payload_segs2 = UNKNOWN_HOST_MSG_TXT; }
			},
			ResponseCategory::MethodNotAllowed => {
				reason = "Method not allowed";	//Nonstandard reason.
				block.set_header_std(HttpHdr::Allow, b"GET,HEAD");
			},
			ResponseCategory::RangeError => {
				let mut htmp = [MaybeUninit::uninit();64];
				let mut htmp = PrintBuffer::new(&mut htmp);
				write!(htmp, "bytes */{len}").ok();	//Should succeed.
				block.set_header_std(HttpHdr::ContentRange, htmp.into_inner_bytes());
			},
			ResponseCategory::NoContent => {
				//204 never has a body! So all payload segs must be empty.
				payload_segs2 = &[];
			},
			ResponseCategory::GlobalOptions => {
				//204 never has a body! So all payload segs must be empty.
				payload_segs2 = &[];
			},
			ResponseCategory::Error => {
			},
		}

		if flags & R_TEXT_PLAIN != 0 {
			let ctype = if force_text { TEXT_UTF8 } else { XHTML_UTF8 };
			block.set_header_std(HttpHdr::ContentType, ctype);
		}
		if flags & R_DEFAULT != 0 { block.set_header_std(HttpHdr::ContentType, DEFAULT_CT); }
		if flags & R_STRICT_CSP != 0 {
			block.set_header_std( HttpHdr::ContentSecurityPolicy, DEFAULT_CSP);
			block.set_header_std(HttpHdr::XContentTypeOptions, FORCED_XCTO);
		}
		if flags & R_FATAL != 0 { block.set_emit_close(); }

		//Format the payload. At the end, clip the unused parts and mark everything else as initialized.
		let mut payload = [MaybeUninit::uninit();16384];
		let mut payload = BEWriteBuffer::new(&mut payload);
		for seg in payload_segs2.iter().cloned() { match seg {
			InterpolateComponent::Raw(x) => payload.append(x.as_bytes()),
			InterpolateComponent::Linebreak2 => payload.append(b"\n\n"),
			InterpolateComponent::StatusCode => payload.append(statustmp),	//Assume numeric.
			InterpolateComponent::Reason =>		//Not necressarily nicely behaved.
				htmlescape(reason.as_bytes(), &mut payload),
			InterpolateComponent::ReasonRaw => payload.append(reason.as_bytes()),
			InterpolateComponent::HostName => {
				//This is bit annoying: The only place that can generate misdirected request is
				//just before request object is created. Thus URL has to be pulled from backup
				//URL, and that is URL, not just the hostname. And it is not actually an URL, as
				//it has method too. And path can have <something> tucked at the end.
				let url = xreq_get_url(request);
				//Throw away part before first " " and then :, as that is method and scheme.
				let url = url.find(' ').map(|pt|&url[pt+1..]).unwrap_or(url);
				let url = url.find(':').map(|pt|&url[pt+1..]).unwrap_or(url);
				//Now URL should start with //, throw those away.
				let url = url.strip_prefix("//").unwrap_or(url);
				//Throw away part after (and including) first < or /.
				let url = url.find(|c|c=='/'||c=='<').map(|pt|&url[..pt]).unwrap_or(url);
				//This should be hostname...
				htmlescape(url.as_bytes(), &mut payload);
			},
			InterpolateComponent::Server => {	//Not necressarily nicely behaved.
				copy_server_to_html(&mut payload);
				payload.append(b" at ");
				htmlescape(xreq_get_proxyname(request), &mut payload);
			},
		}}
		let payload = payload.into_inner();

		let contentlen = payload.len();
		let contentlen = min(contentlen, maxsize);
		if sink.have_content_length() && !no_content { block.set_content_length(contentlen as u64); }
		//Rewrite the response if there is request available. Internal responses do not rewrite
		//location.
		if let Some(hostdata) = hostdata {
			hostdata.get_response_rewrite().rewrite_headers(block.deref_mut(), true);
		}
		//Add date header to response.
		add_date_header(block.deref_mut());
		//If HTTP/2, do transform to avoid sending transfer-encoding, which is illegal. Hack: Use the
		//have_content_length(), which is set in HTTP/1, but clear in HTTP/2.
		if !sink.have_content_length() { block.transform_h1_to_h2(); }
		match request {
			Ok(req) => print_internal_response(req, &response, flags & R_WARNING != 0),
			Err(req) => print_internal_response_eurl(req, response,
				(flags & R_EURL_FLAGS) >> EURL_SHIFT),
		}
		//If we have a request, use its fate_internal to avoid double counting. If we do not have request,
		//just count internal response.
		let ok_to_send = xreq_get_token(request).internal();
		//If we have already sent the final response, do not send it again.
		if !ok_to_send { return Ok(false); }
		let mut buf = [MaybeUninit::uninit();MAX_HBLOCK_SIZE];
		let r = match serialize_headers::<I::Encoder>(&mut buf, block.deref_mut(), HeaderKind::Response) {
			Ok(h) => if no_content || head || contentlen == 0 {
				sink.flush_headers_only(h)
			} else {
				sink.flush_headers_and_payload(h, payload)
			},
			//This only needs request wrapped in Some().
			Err(_) if flags & R_FATAL == 0 => do_double_fault(sink, !head, request),
			//Double fault during handling of fatal error is truly unhandleable. Signal failure to
			//send headers, so HTTP/1.x connection is just jettisoned and HTTP/2 gets RST_STREAM.
			Err(_) => return sink.error_forward_failure("Double fault during handling fatal error").
				map(|_|false)
		};
		r.map(|_|true)	//Always sent if OK.
	})
}

fn mk_204<'a>(x: &'a str, _: u64) -> InternalResponse<'a> { InternalResponse::NoBodySuccess(x) }
fn mk_bad_request<'a>(x: &'a str, _: u64) -> InternalResponse<'a> { InternalResponse::err(BAD_REQUEST, x)  }
fn mk_bad_gateway<'a>(x: &'a str, _: u64) -> InternalResponse<'a> { InternalResponse::err(BAD_GATEWAY, x) }
fn mk_not_found<'a>(x: &'a str, _: u64) -> InternalResponse<'a> { InternalResponse::err(NOT_FOUND, x) }
fn mk_not_acceptable<'a>(x: &'a str, _: u64) -> InternalResponse<'a> { InternalResponse::NotAcceptableS(x) }
fn mk_range_not_satisfiable<'a>(x: &'a str, l: u64) -> InternalResponse<'a>
{
	InternalResponse::RangeNotSatisfiable(x, l)
}
fn mk_internal_server_error<'a>(x: &'a str, _: u64) -> InternalResponse<'a>
{
	InternalResponse::err(INTERNAL_SERVER_ERROR, x)
}

pub fn send_internal_response_to_backend_error<I:InternalResponseWriter>(sink: &mut I, request: &RequestInfo,
	hostdata: &HostRoutingDataRef, head: bool, be_error: BackendError) -> Result<(), I::Error>
{
	type MkT = fn(&str,u64) -> InternalResponse;
	let (code, msg, size) = match be_error {
		BackendError::BadHeadersReceived(err) => (mk_bad_request as MkT, cow!(F "Backend error: {err}"), 0),
		//Connection was lost. This causes 502 error.
		BackendError::ConnectionLost => (mk_bad_gateway as MkT, cow!("Connection to backend lost"), 0),
		//Bad backend type.
		BackendError::UnsupportedBackend => (mk_internal_server_error as MkT,
			cow!("Internal error: Trying to send request to illegal connection type"), 0),
		//File not found. This causes 404 error.
		BackendError::FileNotFound => (mk_not_found as MkT, cow!("Not found"), 0),
		//Not acceptable causes 405 error.
		BackendError::SafeMethodsOnly => (mk_not_acceptable as MkT,
			cow!("Only HEAD and GET are supported"), 0),
		BackendError::BadRangeRequested(size, msg) => (mk_range_not_satisfiable as  MkT,
			cow!(F "Requested invalid range: {msg}"), size),
		BackendError::UpgradeNotSupported => (mk_bad_request as MkT, cow!("Upgrade not supported"), 0),
		BackendError::OptionsRequest => (mk_204 as MkT, cow!("Responding to OPTIONS"), 0),
		BackendError::InternalError(err) =>
			(mk_internal_server_error as MkT, cow!(F "Internal error: {err}"), 0),
	};
	//If it is not possible to send the response, kill the stream. Pass dummy_cow as URL and IP because request
	//is always available.
	if !send_internal_response(sink, Ok(request), Some(hostdata), head, &code(&msg, size))? {
		sink.error_forward_failure(msg)?;
	}
	Ok(())
}

fn do_double_fault<I:InternalResponseWriter>(sink: &mut I, body: bool,
	request: Result<&RequestInfo, &EarlyRequestInfo>) -> Result<(), I::Error>
{
	//This goes to both logs.
	log!(WARNING "{request}...DOUBLE FAULT", request=xreq_get_url(request));
	syslog!(WARNING "{request}...DOUBLE FAULT", request=xreq_get_url(request));
	//Things are very dire. Send out a precanned response, as that is the only thing we can do reliably.
	let maxsize = if !body { USIZE_MAX } else { sink.query_maxsize() };
	let precan_header = I::df_precan_headers();
	let precan_body = b"500 INTERNAL ERROR: DOUBLE FAULT\r\n";
	let contentlen = min(maxsize, precan_body.len());
	if body && contentlen > 0 {
		sink.flush_headers_and_payload(precan_header, &precan_body[..])
	} else {
		sink.flush_headers_only(precan_header)
	}
}

pub trait RequestWriteReturn
{
	fn max_amount(&self) -> usize;
	//Returns None if secondary should detach.
	fn write_backward_data(&mut self, d: &[u8], underflow: bool) -> Option<usize>;
	fn write_backward_trailers(&mut self, t: &mut OwnedTrailerBlock);
}

define_bitfield_type!(TransactionFlagsTag, u16);
//FLAG_RX_END => The HTTP message in transaction has been completely received.
define_bitfield_bit!(TflagRxEnd, TransactionFlagsTag, 0);
//FLAG_TX_END => The HTTP message in transaction has been completely sent.
define_bitfield_bit!(TflagTxEnd, TransactionFlagsTag, 1);
//FLAG_CONN_CLOSE => Requesting to close the connection after this transaction completes.
define_bitfield_bit!(TflagCClose, TransactionFlagsTag, 2);
//FLAG_RX_HDREND => All the headers in HTTP message in transaction have been received.
define_bitfield_bit!(TflagRxHdrEnd, TransactionFlagsTag, 3);
//FLAG_FINAL_REPLY => The final headers have been sent.
define_bitfield_bit!(TflagFinalReply, TransactionFlagsTag, 4);
//FLAG_IS_HEAD => The HTTP request in transaction is HEAD.
define_bitfield_bit!(TflagIsHead, TransactionFlagsTag, 5);
//FLAG_100_CONTINUE => Requesting Expect: 100-Continue behavior, goahead not received.
define_bitfield_bit!(Tflag100Continue, TransactionFlagsTag, 6);
//FLAG_UPGRADE => UPgrade was either proposed or done.
define_bitfield_bit!(TflagUpgrade, TransactionFlagsTag, 7);
//FLAG_ERROR => The received HTTP message in transaction is errorneous and is being ignored.
define_bitfield_bit!(TflagError, TransactionFlagsTag, 8);
//FLAG_UNSAFE_OUT => The request method is unsafe on slave side.
define_bitfield_bit!(TflagUnsafeOut, TransactionFlagsTag, 9);
//FLAG_UPGRADE_PROPOSED_OUT => Upgrade was proposed.
define_bitfield_bit!(TflagUpgradeProposedOut, TransactionFlagsTag, 10);
//FLAG_UPGRADE_ACKNOWLEDGE => Upgrade was acknowledged.
define_bitfield_bit!(TflagUpgradeAcknowledgeIn, TransactionFlagsTag, 14);

#[derive(Copy,Clone,Debug)]
pub struct ConnectionPropertiesI(u8);
const CPROP_SECURE192: u8 = 1<<0;
const CPROP_TLS13: u8 = 1<<1;

impl ConnectionPropertiesI
{
	pub(crate) fn to_internal(s: crate::ConnectionProperties) -> ConnectionPropertiesI
	{
		let mut f = 0;
		if s.secure192 { f |= CPROP_SECURE192; }
		if s.tls13 { f |= CPROP_TLS13; }
		ConnectionPropertiesI(f)
	}
	pub(crate) fn unknown() -> ConnectionPropertiesI { ConnectionPropertiesI(0) }
	pub(crate) fn is_secure192(self) -> bool { self.0 & CPROP_SECURE192 != 0 }
	pub(crate) fn is_tls13(self) -> bool { self.0 & CPROP_TLS13 != 0 }
}

#[derive(Clone,Debug)]
pub struct ClientInfoIn
{
	//Client IP for transaction.
	pub client_ip: Rc<String>,
	//Client CN.
	pub client_cn: Option<Rc<String>>,
	//Client SPKI hash.
	pub client_spkihash: Option<Rc<String>>,
	//Connection properties.
	pub properties: ConnectionPropertiesI,
	//Requested client certificate.
	pub requested_cert: bool,
}

///Set the client information.
impl ClientInfoIn
{
	pub fn set_client_info(&self, block: &mut HeaderBlock)
	{
		block.set_req_client_cert(self.requested_cert);
		if let Some(cn) = self.client_cn.as_ref() {
			block.set_special(ShdrClientCn, cn.as_bytes());
		}
		if let Some(spkihash) = self.client_spkihash.as_ref() {
			block.set_special(ShdrClientSpkihash, spkihash.as_bytes());
		};
		block.set_special(ShdrIpaddr, self.client_ip.as_bytes());
	}
}

enum BackendResource
{
	Named(HostStringIndex2),
	File{fname: PathBuf, uncompressed: bool, is_error: bool},
	None,
}

impl BackendResource
{
	fn is_none(&self) -> bool { matches!(self, BackendResource::None) }
}

#[derive(Debug)]
pub struct TransactionStateIn
{
	//The request associated with the transaction, if known.
	request: Result<(RequestInfo, HostRoutingDataRef), EarlyRequestInfo>,
	//Global configuration.
	g: ConfigT,
	//Connection properties.
	properties: ConnectionPropertiesI,
	//Flags.
	flags: Bitfield<TransactionFlagsTag>,
}

macro_rules! er
{
	($selfx:ident $sink:ident $req:ident $hostdata:ident $x:expr) => {
		f_return!($x, F[|err|$selfx.send_internal_response_early($sink, &$req, &$hostdata, err)])
	};
}


fn __do_path_specifics(block: &mut HeaderBlock, hostdata: &HostRoutingDataRef, pdata: Option<&PathData>,
	pdatag: &PathData, cfg: &mut RequestInfoCfg, backend: &mut BackendResource) ->
	Result<(), InternalResponse<'static>>
{
	//noaccess disallows all access.
	fail_if!(path_setting_dflt(pdata, pdatag, false, |p|p.noaccess),
		InternalResponse::err(NOT_FOUND, "Not found"));
	//backend overrides the backend.
	if let Some(pbackend) = path_setting(pdata, pdatag, |p|p.backend) {
		*backend = BackendResource::Named(pbackend);
	}
	//Set HOST_RAW/PATH_RAW flags.
	if path_setting_dflt(pdata, pdatag, false, |p|p.raw_host) { cfg.s_flags |= SEFLAG_HOST_RAW; }
	if path_setting_dflt(pdata, pdatag, false, |p|p.raw_path) { cfg.s_flags |= SEFLAG_PATH_RAW; }
	//File cache backend. It replaces the backend if it hits. Use translated paths, as that is what
	//the backend uses.
	if let Some(fcbackend) = path_setting(pdata, pdatag, |p|p.fc_backend) {
		let path = block.get_special(ShdrPathT).unwrap_or(b"");
		let backend_name = hostdata.get_string(fcbackend);
		if fc_backend_hit(backend_name, path) {
			*backend = BackendResource::Named(fcbackend);
		}
	}
	cfg.add_cors = path_setting_dflt(pdata, pdatag, false, |p|p.add_cors);
	Ok(())
}

fn path_setting_dflt<T:Sized>(pdata: Option<&PathData>, pdatag: &PathData, dflt: T,
	project: fn(&PathData) -> Option<T>) -> T
{
	path_setting(pdata, pdatag, project).unwrap_or(dflt)
}

fn path_setting<T:Sized>(pdata: Option<&PathData>, pdatag: &PathData, project: fn(&PathData) -> Option<T>) ->
	Option<T>
{
	pdata.and_then(project).or_else(||project(pdatag))
}

const DEFAULT_LOG_FLAGS: u32 = !0;

thread_local!(static UNKNOWN: Rc<String> = Rc::new(String::from("(null)")));
thread_local!(static ROOT_FIXED: RcTextString = RcTextString::new("/does-not-exist"));
thread_local!(static CATEGORY_FIXED: RcTextString = RcTextString::new("Fixedfile"));

impl TransactionStateIn
{
	///Create a new incoming transaction state.
	pub fn new(client_info: ClientInfoIn, g: ConfigT) -> TransactionStateIn
	{
		let unknown = UNKNOWN.with(|u|u.clone());
		let (proxyname, g_log_flags) = g.with(|g|{
			let pname = g.proxy_name.clone();
			let gflags = g.global_logflags.load(MemOrdering::Acquire) as u32;
			(pname, gflags)
		});
		TransactionStateIn {
			request: Err(EarlyRequestInfo {
				proxyname: proxyname,
				client_ip: client_info.client_ip,
				url: unknown,
				response_token: Rc::new(SendResponseToken::new(g)),
				logflags: g_log_flags,
				is_mozilla: true,		//Assume for early request.
			}),
			g: g,
			properties: client_info.properties,
			flags: Bitfield::new(),
		}
	}
	///Did client use Connection: Close?
	pub fn client_wants_close_after_request(&self) -> bool { self.flags.get(TflagCClose) }
	///Is current RX phase HEADERS?
	pub fn rx_phase_is_headers(&self) -> bool { !self.flags.get(TflagRxHdrEnd)  }
	///Is current TX phase HEADERS?
	pub fn tx_phase_is_headers(&self) -> bool
	{
		xreq_get_token(maybe_reqinfo_inner(&self.request)).not_sent.load(MemOrdering::Relaxed)
	}
	///Is current TX phase DATA?
	pub fn tx_phase_is_data(&self) -> bool { !self.tx_phase_is_headers() && !self.flags.get(TflagTxEnd) }
	///Is current TX phase DONE?
	pub fn tx_phase_is_done(&self) -> bool { self.flags.get(TflagTxEnd) }
	///Are both current RX and TX phses DONE?
	pub fn rx_and_tx_is_done(&self) -> bool { self.flags.get(TflagRxEnd) && self.flags.get(TflagTxEnd) }
	//Did client propose upgrade?
	pub fn client_proposed_upgrade(&self) -> bool { self.flags.get(TflagUpgrade) }
	//Get the CID of the slave, if any (returns INVALID_CID if there is no slave).
	pub fn get_slave_cid(&self) -> CID
	{
		self.request.as_ref().map(|x|x.0.slave_cid.clone()).ok().unwrap_or_else(CID::invalid)
	}

	///Mark that method is HEAD if flag=true, otherwise mark method as non-HEAD.
	pub fn mark_as_head(&mut self, flag: bool) { self.flags.or_imp(TflagIsHead, flag); }
	///Mark that upgrade failed, the server did not agree.
	pub fn upgrade_failed(&mut self)
	{
		//Failing upgrade clears the FLAG_UPGRADE, behaving like the client did not propose the upgrade,
		//and sets RX phase to DONE.
		if self.flags.get(TflagUpgrade) {
			self.flags.clear_imp(TflagUpgrade);
			self.flags.set_imp(TflagRxEnd);
		}
	}
	///Notify about receiving backward headers with given status code.
	pub fn notify_status_code(&mut self, status: u16)
	{
		//If status code is 101 or 200 (HTTP/2 transform code overwrites 101 with 200, and 200 otherwise is
		//not allowed), and upgrade was proposed, acknowledge upgrade.
		if self.flags.get(TflagUpgrade) && (status == 101 || status == 200) {
			self.flags.set_imp(TflagUpgradeAcknowledgeIn);
		}
		//If status code is 100 or 300-599 (except 304), clear FLAG_100_CONTINUE.
		if status == 100 || (status >= 300 && status <= 303) || status >= 305 {
			self.flags.clear_imp(Tflag100Continue);
		}
	}
	//Set request client information.
	//Check if request should be blocked early. Returns secure flag.
	fn check_early_blocking(&mut self, block: &mut HeaderBlock) -> bool
	{
		let secure = block.is_secure();
		//Check that requests to HSTS-preloaded domains are secure.
		//Check x-forwarded-client-spkihash against ACL list. If header is not present, then the ACL list
		//must be empty. The value is always empty in non-secure mode.
		if let Some(error) = {
			let hostname = block.get_special(ShdrAuthorityO).unwrap_or(b"");
			if !secure && on_hsts_preload_list(hostname) {
				Some(HeaderError::IllegalInsecure)
			} else {
				None	//No error.
			}
		} {
			block.set_overflow(error);
		}

		//Check that method is not CONNECT. That method is not allowed. This will not fire on HTTP/2
		//upgrade, because transform_h2_to_h1() overwrites the method with GET.
		if block.get_special(ShdrMethod) == Some(CONNECT) {
			block.set_overflow(HeaderError::IllegalConnect);
		}
		//Check that the authority and path are valid. For authority, any set authority is valid. For
		//path, the path must either start with / or be *. The * is only allowed with OPTIONS.
		if block.get_special(ShdrAuthorityO).is_none() {
			block.set_overflow(HeaderError::IllegalAuthority);
		}
		let tpath = block.get_special(ShdrPathO).unwrap_or(b"");
		if tpath == b"*" {
			if block.get_special(ShdrQuery).is_some() {
				block.set_overflow(HeaderError::IllegalAsteriskQuery);
			}
			if block.get_special(ShdrMethod) != Some(OPTIONS) {
				block.set_overflow(HeaderError::IllegalAsteriskNotOptions);
			}
		} else if !tpath.starts_with(b"/") {
			block.set_overflow(HeaderError::IllegalPath);
		}
		//Check the scheme is expected versus the secure flag.
		let exp_scheme = if secure { "https" } else { "http" };
		if block.get_special(ShdrScheme) != Some(exp_scheme.as_bytes()) {
			block.set_overflow(HeaderError::IllegalScheme);
		}
		secure
	}
	//Set request attribute flags. Returns attributes and URL.
	fn set_request_attributes(&mut self, block: &mut HeaderBlock, has_body: bool) -> RequestAttributes
	{
		//Client requests for Connection: Close, Upgrade:, Expect: 100-continue and has_body are saved
		//into flags. It is assumed that any upgrade attempt has has_body=true.
		let attributes = block.get_attributes();
		if block.is_connection_close() { self.flags.set_imp(TflagCClose); }
		if block.get_special(ShdrProtocol).is_some() { self.flags.set_imp(TflagUpgrade); }
		if !has_body { self.flags.set_imp(TflagRxEnd); }
		if attributes.is_expect_100_continue() { self.flags.set_imp(Tflag100Continue); }
		//Add chunked encoding if there is no content-length and no chunked, but there is T-E.
		if !block.is_chunked() && block.get_content_length().is_none() && block.is_transfer_encoding() {
			//This should set is_chunked.
			block.set_header_std(HttpHdr::TransferEncoding, CHUNKED);	
		}
		attributes
	}
	//Check for early request overflow.
	fn check_early_overflow<I:InternalResponseWriter>(&mut self, sink: &mut I, block: &HeaderBlock) ->
		Result<bool, I::Error>
	{
		//If overflow flag is set in the request block, reject it. This sends a response and ends the
		//stream, so one needs to set FLAG_FINAL_REPLY and FLAG_TX_END. We do not set FLAG_RX_END, as there
		//can be received data. The slave is not set, so any sent data will just be discarded.
		Ok(if let Some(err) = block.is_overflow() {
			let (request, hostdata) = maybe_reqinfo_inner_h(&self.request);
			send_internal_response(sink, request, hostdata, self.flags.get(TflagIsHead),
				&InternalResponse::err(BAD_REQUEST, &err.to_string()))?;
			self.flags.set_imp(TflagTxEnd);
			false		//Sent failure reply.
		} else {
			true		//Succeeded.
		})
	}
	//Fetch hostdata for the request.
	fn __get_hostdata(&mut self, block: &mut HeaderBlock) ->
		Result<HostRoutingDataRef, InternalResponse<'static>>
	{
		match self.g.with(|g|g.routetable.get_data_for(block.get_special(ShdrAuthorityO).unwrap_or(b""))) {
			Some(h) => {
				//Set logflags from host before calling check_host, so logging settings of the
				//host apply to any errors raised by host accept check.
				let mut ip = None;
				if let Err(ref mut ereq) = self.request.as_mut() {
					ereq.logflags = h.global_path_settings().logflags.
						unwrap_or(DEFAULT_LOG_FLAGS);
					ip = IpAddr::from_str(&ereq.client_ip).ok();
				}
				__check_host(block, &h, self.properties, ip).map(|_|h)
			},
			None => Err(InternalResponse::MisdirectedRequest),
		}
	}
	//Fetch hostdata for the request.
	fn get_hostdata<I:InternalResponseWriter>(&mut self, sink: &mut I, block: &mut HeaderBlock) ->
		Result<Option<HostRoutingDataRef>, I::Error>
	{
		//Look up the hostdata entry for the accessed vhost. If this entry does not exist, send an error,
		//which impiles setting FLAG_FINAL_REPLY and FLAG_TX_END. We do not set FLAG_RX_END, as there
		//can be received data. The slave is not set, so any sent data will just be discarded.
		Ok(match self.__get_hostdata(block) {
			Ok(x) => Some(x),
			Err(e) => {
				let is_head = self.flags.get(TflagIsHead);
				//Reject this. The address in headers can be assumed valid in all cases.
				let (request, hostdata) = maybe_reqinfo_inner_h(&self.request);
				send_internal_response(sink, request, hostdata, is_head, &e)?;
				self.flags.set_imp(TflagTxEnd);
				None
			}
		})
	}
	///Received forward headers.
	pub fn do_forward_headers<I:InternalResponseWriter>(&mut self, sink: &mut I, block: &mut HeaderBlock,
		connector: &mut Connector, has_body: bool, connid: ConnId) -> Result<(), I::Error>
	{
		//Grab the URL and source IP. Note that if in chained mode, the source IP must be grabbed from
		//the header block. If source block has errors straight from start, do best-effort.
		let early_req = if let Err(ref mut ereq) = &mut self.request {
			ereq.url = Rc::new(block.get_conn_url());
			if block.is_chained() {
				let client_ip = block.get_special(ShdrIpaddr).and_then(|ip|from_utf8(ip).ok()).
					unwrap_or("<unknown>");
				ereq.client_ip = Rc::new(client_ip.to_owned())
			};
			if let Some(ua) = block.get_special(ShdrUserAgent) {
				ereq.is_mozilla = ua.starts_with(b"Mozilla/");
			}
			ereq.clone()
		} else {
			//What is going on? There already is request context, even if there is not supposed to be,
			//as this function is supposed to set it up, and it is only called in response to headers,
			//which there only can be one. Trailers would use do_forward_trailers().
			syslog!(CRITICAL "do_forward_headers() with request available for {connid}. \
				Called multiple times?");
			return Ok(());
		};
		block.set_request();
		//Do not do anything if either FLAG_ERROR or FLAG_RX_HDREND is set, as in first case we are
		//forwarding after error, and everything should be ignored, and in second case we have already
		//received headers, and there ca not be second one.
		if self.flags.get(TflagError) { return Ok(()); }
		if self.flags.get(TflagRxHdrEnd) { return Ok(()); }
		self.flags.set_imp(TflagRxHdrEnd);

		//Get rid of alt-used, and set if request has body. Client IP address has already been set.
		block.delete_matching_std(|x|x==HttpHdr::AltUsed);
		block.set_body(has_body);

		let secure = self.check_early_blocking(block);
		let attributes = self.set_request_attributes(block, has_body);
		if !self.check_early_overflow(sink, block)? { return Ok(()); }

		//Server-Global OPTIONS.
		if block.get_special(ShdrAuthorityO) == Some(b"") && block.get_special(ShdrPathO) == Some(b"*") {
			return self.send_internal_response(sink, &InternalResponse::GlobalOptions, None).map(|_|());
		}
		//Disallow some dangerous stuff.
		if let Err(err) = self.do_forbid_dangerous_stuff(block) {
			increment_metric!(req_danger self.g);
			//This goes to both logs.
			log!(WARNING "Blocked dangerous URL {url}", url=early_req.url);
			syslog!(WARNING "Blocked dangerous URL {url}", url=early_req.url);
			return self.send_internal_response(sink, &err, None).map(|_|());
		}

		//Already sent the response about bad virtual host.
		let hostdata = f_return!(self.get_hostdata(sink, block)?, Ok(()));

		//The things are valid enough to contstruct a request control block. The slave is not available yet,
		//so mark the slave as NULL.
		let (cid, sid) = sink.get_cid_and_sid();
		let req = RequestInfo {
			cid: cid,
			sid: sid,
			attributes: attributes,
			url: early_req.url,
			slave_cid: CID::invalid(),
			proxyname: early_req.proxyname,
			cfg: RequestInfoCfg {
				add_cors: false,
				force_mime: self.get_forced_mime_type(block, &hostdata),
				s_flags: 0,	//s_flags not available yet (path-dependent), assume 0.
				force_error: None,
				logflags: hostdata.global_path_settings().logflags.unwrap_or(DEFAULT_LOG_FLAGS),
			},
			client_ip: early_req.client_ip,
			response_token: early_req.response_token,
			no_redirect_fixup: hostdata.get_no_redirect_fixup(),
			is_mozilla: early_req.is_mozilla,
		};
		if secure { increment_metric!(req_https self.g); } else { increment_metric!(req_http self.g); }
		self.forward_headers_woth_request(sink, req, &hostdata, block, connector, connid, secure)
	}
	fn __process_regex_line<I:InternalResponseWriter>(&mut self, req: &mut RequestInfo,
		hostdata: &HostRoutingDataRef, sink: &mut I, submatches: &[RegexMatch;32],
		backend: &mut BackendResource, path_data: &RegularExpressionEntryData, block: &mut HeaderBlock,
		first: &mut bool, secure: bool) ->
		Result<(), I::Error>
	{
		//Flags and flaglike things.
		if let Some(logflags) = path_data.logflags { req.cfg.logflags = logflags; }
		if let Some(force_mime) = path_data.force_mime { req.cfg.force_mime = Some(force_mime); }
		if path_data.add_cors { req.cfg.add_cors = true; }
		if path_data.raw_host { req.cfg.s_flags |= SEFLAG_HOST_RAW; }
		if path_data.raw_path { req.cfg.s_flags |= SEFLAG_PATH_RAW; }
		if path_data.error >= 400 && path_data.error <= 599 {
			req.cfg.force_error = Some(path_data.error);
		}
		let mut newurl = [0;4096];
		//Set processed and break on any terminal entry. This is not required on any error,
		//as errors are terminal anyway.
		let pdatag = hostdata.global_path_settings();
		er!(self sink req hostdata __regex_blacklist(path_data));
		er!(self sink req hostdata __regex_https_redirect(path_data, pdatag, block, self.g, secure));
		//Only perform canonical host redirect and disable processing the first time around, as
		//these do not depend on path.
		if replace(first, false) {
			er!(self sink req hostdata __canonicalhost_redirect(&hostdata, block));
			er!(self sink req hostdata __do_process_disable(block, &hostdata));
		}
		er!(self sink req hostdata __regex_pattern_redirect(&hostdata, path_data, block, submatches,
			&mut newurl));
		er!(self sink req hostdata __regex_fc_backend(&hostdata, req.cfg.force_error, path_data,
			submatches, block, backend));
		er!(self sink req hostdata __regex_backend(&hostdata, path_data, block, submatches,
			backend));
		Ok(())
	}
	fn __process_legacy_path_line<I:InternalResponseWriter>(&mut self, req: &mut RequestInfo,
		hostdata: &HostRoutingDataRef, sink: &mut I, backend: &mut BackendResource,
		path_specific: Option<(&str, &PathData)>, block: &mut HeaderBlock, first: &mut bool,
		secure: bool) -> Result<(), I::Error>
	{
		let (ppath, pdata) = if let Some((ppath, pdata)) = path_specific {
			(ppath, Some(pdata))
		} else {
			("/", None)
		};
		let pdatag = hostdata.global_path_settings();
		//Reset the log flags, now overlay is known.
		req.cfg.logflags = path_setting_dflt(pdata, pdatag, DEFAULT_LOG_FLAGS, |p|p.logflags);
		//Remap authority and path if requested. This remapping is always done, regardless of if
		//HOST_RAW/PATH_RAW is set. HOST_RAW/PATH_RAW only affects emitting the authority/path. This
		//remapping is needed for sdata lookup.
		__remap_target(block, ppath, &hostdata, pdata, pdatag);
		//Redirects.
		let special = SpecialPath::new(block, req.attributes.is_unsafe());
		er!(self sink req hostdata __do_path_insecure(block, special, secure, pdata, pdatag, self.g));
		er!(self sink req hostdata __do_host_root_redirect(block, special, &hostdata));
		//Only do these if path-regex processing did not.
		if replace(first, false) {
			er!(self sink req hostdata __canonicalhost_redirect(&hostdata, block));
			er!(self sink req hostdata __do_process_disable(block, &hostdata));
		}
		er!(self sink req hostdata __do_path_specifics(block, &hostdata, pdata, pdatag,
			&mut req.cfg, backend));
		Ok(())
	}
	fn forward_headers_woth_request<I:InternalResponseWriter>(&mut self, sink: &mut I, mut req: RequestInfo,
		hostdata: &HostRoutingDataRef, block: &mut HeaderBlock, connector: &mut Connector, connid: ConnId,
		secure: bool) -> Result<(), I::Error>
	{
		//Do regex blocks.
		let mut submatches = [RegexMatch::new();32];
		let mut backend = BackendResource::None;
		let mut regexidx = 0;
		let mut first = true;
		let dfltcfg = req.cfg;
		while let Some((nregexidx, path_data)) = {
			let oldpath = block.get_special(ShdrPathO).unwrap_or(b"");
			hostdata.lookup_regex(oldpath, &mut submatches, regexidx)
		} {
			regexidx = nregexidx;	//Store position.
			req.cfg = dfltcfg;	//Undo any changes.
			self.__process_regex_line(&mut req, hostdata, sink, &submatches, &mut backend, path_data,
				block, &mut first, secure)?;
			if !backend.is_none() { break; }	//Backend has been chosen.
		}
		if backend.is_none() {
			req.cfg = dfltcfg;	//Undo any changes.
			let oldpath = block.get_special(ShdrPathO).unwrap_or(b"");
			let path_specific = hostdata.lookup_path_specifics(oldpath);
			self.__process_legacy_path_line(&mut req, hostdata, sink, &mut backend, path_specific,
				block, &mut first, secure)?;
		}
		er!(self sink req hostdata __process_via_and_cdn_loop(block, &req.proxyname));
		//Rewrite headers.
		hostdata.get_request_rewrite().rewrite_headers(block, false);
		//Check this again. send_internal_response_early() sets the needed flags.
		if let Some(err) = block.is_overflow() {
			return self.send_internal_response_early(sink, &req, &hostdata,
				InternalResponse::err(BAD_REQUEST, &err.to_string()));
		}
		//OK, headers are processed, connect to backend.
		self.connect_to_backend(sink, block, req, hostdata, backend, connector, connid)
	}
	fn do_forbid_dangerous_stuff(&self, block: &HeaderBlock) -> Result<(), InternalResponse<'static>>
	{
		//Forbid access to some dangerous stuff. This is not bypassable by percent-encoding, since the low-
		//level code partially undecodes percent-encoding. And since all characters here are on the valid
		//list, any percent-encodes attempting to hide these will be decoded. send_internal_response_early
		//sets any flags needed.
		if let Some(path) = block.get_special(ShdrPathO) {
			fail_if!(in_prefix(path, b"/.well-known/pki-validation"),
				InternalResponse::err_w(FORBIDDEN, BAD_METHOD6));
			fail_if!(in_prefix(path, b"/.well-known/acme-challenge"),
				InternalResponse::err_w(FORBIDDEN, BAD_ACME));
		}
		Ok(())
	}
	fn get_forced_mime_type<'a>(&self, block: &HeaderBlock, hostdata: &'a HostRoutingDataRef) ->
		Option<HostStringIndex2>
	{
		//Mime type forcing always uses original paths.
		let path = block.get_special(ShdrPathO).unwrap_or(b"");
		hostdata.get_mime_type(path)
	}
	fn fill_connection_params(backend: &str, sbackend: &BackendData) -> ConnectionOutParams
	{
		ConnectionOutParams {
			category: RcTextString::new(backend),
			keepalive: sbackend.get_keepalive(),
			use_once: sbackend.get_use_once(),
		}
	}
	fn connect_to_backend<I:InternalResponseWriter>(&mut self, sink: &mut I, block: &mut HeaderBlock,
		req: RequestInfo, hostdata: &HostRoutingDataRef, backend: BackendResource,
		connector: &mut Connector, connid: ConnId) -> Result<(), I::Error>
	{
		let backend = match backend {
			BackendResource::Named(p) => hostdata.get_string(p),
			BackendResource::File{fname, uncompressed, is_error} => {
				//These slaves are never reused.
				let slave = connector.static_file(fname, uncompressed, is_error).map_err(|err|{
					syslog!(WARNING "Failed to connect to fixed file backend: {err}");
					InternalResponse::err(GATEWAY_TIMEOUT, CONNECT_ERROR)
				});
				return self.connected_to_backend(sink, block, req, hostdata, slave, connid);
			},
			BackendResource::None => {
				//No need to print log message, as the requestlog entry will be printed at WARNING
				//severity anyway.
				return self.send_internal_response_early(sink, &req, &hostdata,
					InternalResponse::err(INTERNAL_SERVER_ERROR, "No backend assigned"));
			}
		};
		//The backend to use.
		let sbackend = self.g.with(|g|g.routetable.get_backend_by_name(backend.deref().deref()));

		//Check if pool has free connection, and if yes, reuse it. Default to INVALID_CID if connection
		//open fails. This is so that one can use normal abort path. However, never reuse connections for
		//upgrade.
		let is_upgrade = self.flags.get(TflagUpgrade);
		let slave = if let Some(cid) = ConnectionFreeList::get_by_cclass(backend, is_upgrade) {
			Ok(cid)
		} else if let Some(backend2) = backend.strip_prefix("static:") {
			connector.staticbackend(StaticFileParams{
				category: RcTextString::new(backend),
				root: RcTextString::new(backend2),
			}).map_err(|err|{
				syslog!(WARNING "Failed to connect to backend {backend}: {err}");
				InternalResponse::err(GATEWAY_TIMEOUT, CONNECT_ERROR)
			})
		} else if backend.deref().deref() == "" {
			//Empty backend name is special, generating not found errors.
			Err(InternalResponse::err(NOT_FOUND, "Not found"))
		} else if let Some(sbackend) = sbackend.as_ref() {
			let mut p = Self::fill_connection_params(&backend, sbackend);
			p.use_once |= is_upgrade;
			//These connections are not speculative, so autoselect subtarget.
			connector.connect_target2(p, sbackend.get_targets(), backend).map_err(|err|{
				syslog!(WARNING "Failed to connect to backend {backend}: {err}");
				InternalResponse::err(GATEWAY_TIMEOUT, CONNECT_ERROR)
			})
		} else {
			Err(InternalResponse::err(INTERNAL_SERVER_ERROR, "No backend available"))
		};
		self.connected_to_backend(sink, block, req, hostdata, slave, connid)
	}
	fn connected_to_backend<I:InternalResponseWriter>(&mut self, sink: &mut I, block: &mut HeaderBlock,
		mut req: RequestInfo, hostdata: &HostRoutingDataRef, slave: Result<CID, InternalResponse>,
		connid: ConnId) -> Result<(), I::Error>
	{
		//Set the slave CID.
		//If no valid slave could be assigned, abort. send_internal_response_early() sets the flags.
		match slave {
			Ok(slave_cid) => req.slave_cid = slave_cid,
			Err(status) => return self.send_internal_response_early(sink, &req, hostdata,
				status),
		}
		//Forward the headers to slave, treating nonexistent connections just like connections that were
		//lost. The above code should have raised an error if it can not construct a valid connection.
		let mut status = Err(BackendError::ConnectionLost);
		req.slave_cid.call_with_connection(|conn|{
			Ok(status = conn.write_forward_headers(block, &req, hostdata.clone(), connid))
		}).ok();
		//If forwarding headers failed, send error headers back. This is always an option, because reply
		//headers can not be sent before query headers. This counts as TX end and final reply. Because
		//there is no valid request, everything subsequent will be ignored.
		if let Err(e) = status {
			send_internal_response_to_backend_error(sink, &req, hostdata,
				self.flags.get(TflagIsHead), e)?;
			self.flags.set_imp(TflagTxEnd);
		} else {
			//Ok, store the request as valid.
			self.request = Ok((req, hostdata.clone()));
		}
		Ok(())
	}
	//Received data.
	pub fn do_forward_data<I:InternalResponseWriter>(&mut self, d: &[u8], sink: &mut I, blocked: &mut bool) ->
		Result<(), I::Error>
	{
		//Since we are forwarding data, FLAG_ERROR and FLAG_RX_END must be clear, and FLAG_RX_HDREND
		//must be set.
		if self.flags.get(TflagError) || !self.flags.get(TflagRxHdrEnd) || self.flags.get(TflagRxEnd) {
			return Ok(());
		}

		//If there is no request, just ignore the data, but set unblocked. The reason for setting unblocked
		//is that the data can be sinked to bit bucket.
		if let Ok((request,hostdata)) = self.request.as_ref() {
			//If Expect: 100-continue has been received, but no reply headers, then raise an error.
			//Since there are no reply headers, one can send error headers. This assers FLAG_TX_END
			//and FLAG_FINAL_REPLY. The error is signaled to slave, but reply is ignored. Clear
			//the 100-continue flag and set upgrade acknowledge to avoid double-faulting. Request is
			//available, so pass dummy bakups. Also do the same if upgrade has been proposed but not
			//accepted.
			if self.flags.get(Tflag100Continue) ||
				(self.flags.get(TflagUpgrade) && !self.flags.get(TflagUpgradeAcknowledgeIn)) {
				send_internal_response(sink, Ok(request), Some(hostdata),
					self.flags.get(TflagIsHead),
					&InternalResponse::err(BAD_REQUEST, ILLEGAL_EXPECT_100_DATA))?;
				request.slave_cid.call_with_connection(|conn|{
					Ok(conn.write_forward_error(ILLEGAL_EXPECT_100_DATA))
				}).ok();
				self.flags.set_imp(TflagTxEnd);
				self.flags.clear_imp(Tflag100Continue);
				self.flags.set_imp(TflagUpgradeAcknowledgeIn);
			} else {
				//Treat nonexistent connections like connections that were lost.
				let mut status = Err(BackendError::ConnectionLost);
				request.slave_cid.call_with_connection(|conn|{
					Ok(status = conn.write_forward_data(d))
				}).ok();
				*blocked = !match status {
					//If the forward was successful, return the unblock status returned by
					//the forward.
					Ok(x) => x,
					//If the forward failed, always unblock the channel. Try sending error
					//headers, falling back to stream error and then disconnect. This also
					//TX end and final reply be asserted, since there will be no more data.
					Err(e) => {
						send_internal_response_to_backend_error(sink, request, hostdata,
							self.flags.get(TflagIsHead), e)?;
						self.flags.set_imp(TflagTxEnd);
						false
					},
				};
			}
		} else {
			*blocked = false;
		}
		Ok(())
	}
	pub fn do_forward_trailers<I:InternalResponseWriter>(&mut self, sink: &mut I, block: &mut HeaderBlock) ->
		Result<(), I::Error>
	{
		//There should not be IP address set on these blocks.
		block.clear_special(ShdrIpaddr);
		//Since we are forwarding trailers, FLAG_ERROR and FLAG_RX_END must be clear, and
		//FLAG_RX_HDREND set.
		if self.flags.get(TflagError) || !self.flags.get(TflagRxHdrEnd) || self.flags.get(TflagRxEnd) {
			return Ok(());
		}
		self.flags.set_imp(TflagRxEnd);

		//Only forward if there was a request. Otherwise just ignore the trailers.
		if let Ok((request,hostdata)) = self.request.as_ref() {
			//If Expect: 100-continue has been received, but no reply headers, then raise an error.
			//Since there are no reply headers, one can send error headers. This assers FLAG_TX_END
			//and FLAG_FINAL_REPLY. The error is signaled to slave, but reply is ignored. Request is
			//available, so pass dummy backups. Also do the same if upgrade has been proposed but not
			//accepted.
			if self.flags.get(Tflag100Continue) ||
				(self.flags.get(TflagUpgrade) && !self.flags.get(TflagUpgradeAcknowledgeIn)) {
				send_internal_response(sink, Ok(request), Some(hostdata),
					self.flags.get(TflagIsHead),
					&InternalResponse::err(BAD_REQUEST, ILLEGAL_EXPECT_100_DATA))?;
				request.slave_cid.call_with_connection(|conn|{
					Ok(conn.write_forward_error(ILLEGAL_EXPECT_100_DATA))
				}).ok();
				self.flags.set_imp(TflagTxEnd);
			} else {
				//Treat nonexistent connections like connections that were lost.
				let mut status = Err(BackendError::ConnectionLost);
				request.slave_cid.call_with_connection(|conn|{
					Ok(status = conn.write_forward_trailers(block))
				}).ok();
				//If forward failed, and headers have not been sent yet, send error headers.
				//Since this is headers, it causes TX_END and FLAG_FINAL_REPLY to be asserted.
				if let (true, Err(e)) = (self.tx_phase_is_headers(), status) {
					send_internal_response_to_backend_error(sink, request, hostdata,
						self.flags.get(TflagIsHead), e)?;
					self.flags.set_imp(TflagTxEnd);
				}
			}
		}
		Ok(())
	}
	pub fn do_forward_error<I:InternalResponseWriter>(&mut self, msg: Cow<'static, str>, sink: &mut I,
		is_abort: bool, block: &HeaderBlock) -> Result<(), I::Error>
	{
		//Do not forward two errors in the same transaction. This must hold even if first was sent before
		//headers were complete.
		if self.flags.get_set_imp(TflagError) { return Ok(()); }

		//Reflect error back. This always happens, except for aborts, regardless of rx end or there not
		//being a valid request. Fall back to trying to abort stream and then aborting the entiere
		//connection. This causes TX_END and final reply to be asserted. The error code is always bad
		//request.
		//
		//The reason not doing this for aborts is that higher-quality error message may be sent before
		//tearing down the entiere connection.
		if !is_abort {
			if self.tx_phase_is_headers() {
				//Try setting the request URL to get the best available approximation for the
				//request.
				try_set_request_url(&mut self.request, block);
				let (request, hostdata) = maybe_reqinfo_inner_h(&self.request);
				send_internal_response(sink, request, hostdata, self.flags.get(TflagIsHead),
					&InternalResponse::err(BAD_REQUEST, msg.deref()))?;
				self.flags.set_imp(TflagTxEnd);
			} else {
				//Late error, try killing the stream, falling back to killing the connection.
				sink.error_forward_failure(&msg)?;
			}
		}
		//Only forward if there was a request and rx end is not set. Otherwise just ignore the error,
		//other than reflecting it back.
		if let (false, Ok((request,_))) = (self.flags.get(TflagRxEnd), self.request.as_ref()) {
			//If this goes to nowhere, just ignore that.
			request.slave_cid.call_with_connection(|conn|{
				Ok(conn.write_forward_error(msg.deref()))
			}).ok();
		}
		//Set the RX_END flag, to avoid falsely tripping the pipelining detection. There is no guarantee
		//that events are forwarded for connection in error before next message starts. The FLAG_ERROR
		//prevents events from causing errors if they still occur.
		self.flags.set_imp(TflagRxEnd);
		Ok(())
	}
	//If data could be provoded (possibly zero-length), returns true. If the connection is not valid, returns
	//false.
	pub fn request_backward_data<S:RequestWriteReturn>(&mut self, sink: &mut S) -> bool
	{
		//Treat nonexistent connections the same as connections that do not provode data.
		if let Ok((request,_)) = self.request.as_ref() {
			let mut status = false;
			request.slave_cid.call_with_connection(|conn|{
				Ok(status = conn.request_data(sink))
			}).ok();
			status
		} else {
			false
		}
	}
	pub(crate) fn send_internal_response<I:InternalResponseWriter>(&mut self, sink: &mut I,
		response: &InternalResponse, _block: Option<&HeaderBlock>) -> Result<bool, I::Error>
	{
		//If response headers have not been sent, send a response. Otherwise say that can not be
		//done. Sending error response causes TX end and final response.
		Ok(if self.tx_phase_is_headers() {
			let (request, hostdata) = maybe_reqinfo_inner_h(&self.request);
			send_internal_response(sink, request, hostdata, self.flags.get(TflagIsHead), response)?;
			//Completed stream. Assume that errors always have status that clears FLAG_100_CONTINUE.
			self.flags.set_imp(TflagTxEnd);
			self.flags.clear_imp(Tflag100Continue);
			true
		} else {
			false
		})
	}
	pub fn close_backward(&mut self)
	{
		//Assert tx end and final reply, because the reply direction has ended.
		self.flags.set_imp(TflagTxEnd);
		//Count as no response, if there has been no response sent.
		xreq_get_token(maybe_reqinfo_inner(&self.request)).noresponse();
	}
	fn send_internal_response_early<I:InternalResponseWriter>(&mut self, sink: &mut I, req: &RequestInfo,
		hostdata: &HostRoutingDataRef, response: InternalResponse) -> Result<(), I::Error>
	{
		//If response headers have already been sent, ignore them, since that means earlier error reply
		//has been sent and should take percedence. Request is available, so pass dummy backups.
		Ok(if self.tx_phase_is_headers() {
			send_internal_response(sink, Ok(req), Some(hostdata), self.flags.get(TflagIsHead),
				&response)?;
			self.flags.set_imp(TflagTxEnd);
		})
	}
}

fn fc_backend_hit(backend: &str, path: &[u8]) -> bool
{
	//If backend does not start with "static:", it is not supported as filecache backend. Strip the prefix
	//too to get the root path.
	let backend = f_return!(backend.strip_prefix("static:"), false);
	//If the path does not start with / or contains traversal, return no match.
	let mut state = 0;
	for b in path.iter().cloned() {
		state = match (state, b) {
			//State 0: At start of string. If 1 move to state 1, else reject.
			(0, b'/') => 1,
			(0, _) => return false,		//Does not start with /.
			//State 1: After /. If ., move to state 3, if / stay, otherwise move to state 2.
			(1, b'.') => 3,
			(1, b'/') => 1,
			//State 2: After pchar. If / move to state 1, else stay.
			(2, b'/') => 1,
			//State 3: After /. If /, that is traversal, otherwise move to state 2.
			(3, b'/') => return false,	// /./ is traversal.
			(3, b'.') => 4,
			//State 4: After /.. If /, that is traversal, otherwise move to state 2.
			(4, b'/') => return false,	// /../ is traversal.
			(_, _) => 2,
		};
	}
	//The thing can end up in state 1 or 2. State 0 is empty string, and state 3/4 would be traversal.
	if state != 1 && state != 2 { return false; }
	//Concatenate to form the actual path. If this contains NUL, it is not valid.
	let mut fpath = Vec::with_capacity(backend.len() + path.len() + 11);
	fpath.extend_from_slice(backend.as_bytes());
	fpath.extend_from_slice(path);
	match UnixPath::new(&fpath).and_then(|p|p.stat().ok()).map(|st|st.format()) {
		Some(StatModeFormat::REG) => return true,	//Hit.
		Some(StatModeFormat::DIR) => (),		//Need to consider /index.html.
		_ => return false,				//Not found, or WTF is this?
	};
	//The path hit a directory. Add /index.html. This has to be a regular file.
	fpath.extend_from_slice(b"/index.html");
	UnixPath::new(&fpath).and_then(|p|p.stat().ok()).map(|st|st.format()) == Some(StatModeFormat::REG)
}

fn try_set_request_url(req: &mut Result<(RequestInfo, HostRoutingDataRef), EarlyRequestInfo>,
	block: &HeaderBlock)
{
	match req {
		Ok((ref mut req,_)) => __try_set_request_url(&mut req.url, block),
		Err(ref mut req) => __try_set_request_url(&mut req.url, block),
	}
}

fn __try_set_request_url(url: &mut Rc<String>, block: &HeaderBlock)
{
	//Don't overwrite non-unknown entries.
	if Rc::ptr_eq(url, &UNKNOWN.with(|x|x.clone())) {
		*url = Rc::new(block.get_conn_url());
	}
}

fn in_prefix(urlpath: &[u8], prefix: &[u8]) -> bool
{
	let after = f_return!(urlpath.strip_prefix(prefix), false);
	//Check if next character is EOF or /.
	after == b"" || after.starts_with(b"/")
}

#[derive(Debug)]
pub struct TransactionStateOut
{
	//The request state.
	request: RequestInfo,
	//The host routing data.
	hostdata: HostRoutingDataRef,
	//Flags.
	flags: Bitfield<TransactionFlagsTag>,
	g: ConfigT,
}

impl TransactionStateOut
{
	///Create a new incoming transaction state.
	pub fn new(request: &RequestInfo, hostdata: HostRoutingDataRef) -> TransactionStateOut
	{
		let mut flags = Bitfield::new();
		//Record attributes.
		if request.attributes.is_expect_100_continue() { flags.set_imp(Tflag100Continue); }
		if request.attributes.is_head() { flags.set_imp(TflagIsHead); }
		if request.attributes.is_unsafe() { flags.set_imp(TflagUnsafeOut); }
		if request.attributes.is_upgrade() { flags.set_imp(TflagUpgradeProposedOut); }
		TransactionStateOut {
			request: request.clone(),
			hostdata: hostdata,
			flags: flags,
			g: request.response_token.g,
		}
	}
	///Is current RX phase HEADERS?
	pub fn rx_phase_is_headers(&self) -> bool { !self.flags.get(TflagRxHdrEnd) }
	///Is current TX phase DONE?
	pub fn tx_phase_is_done(&self) -> bool { self.flags.get(TflagTxEnd) }
	//Get if the transaction has ended.
	pub fn rx_and_tx_is_done(&self) -> bool { self.flags.get(TflagRxEnd) && self.flags.get(TflagTxEnd) }
	///Is rx end and close?
	pub fn is_rx_end_and_close(&self) -> bool
	{
		self.flags.get(TflagRxEnd) && self.flags.get(TflagCClose)
	}
	///Get the master cid.
	pub fn get_cid(&self) -> CID { self.request.cid.clone() }
	///Get the master sid.
	pub fn get_sid(&self) -> u32 { self.request.sid }
	///Is upgrade?
	pub fn is_upgraded(&self) -> bool { self.flags.get(TflagUpgrade) }

	///Notify RX done
	pub fn notify_rx_done(&mut self) { self.flags.set_imp(TflagRxEnd); }
	///Set tx end
	pub fn notify_tx_done(&mut self) { self.flags.set_imp(TflagTxEnd); }
	//Send the remainder of RX to nowhere.
	pub fn discard_remainder(&mut self) { self.flags.set_imp(TflagError); }
	//Is TflagError set?
	pub fn is_discard_remainder(&self) -> bool { self.flags.get(TflagError) }

	pub fn do_backward_headers(&mut self, block: &mut HeaderBlock, has_body: bool, backend: ConnId,
		backend_name: BackendName) -> Result<(), SinkError>
	{
		let mut status = block.get_status();
		//Count the status.
		match status {
			101 => increment_metric!(reqstat_101 self.g),
			304 => increment_metric!(reqstat_304 self.g),
			400 => increment_metric!(reqstat_400 self.g),
			100..=199 => (),	//Ignore informative status.
			200..=299 => increment_metric!(reqstat_2xx self.g),
			300..=399 => increment_metric!(reqstat_3xx self.g),
			401..=499 => increment_metric!(reqstat_4xx self.g),
			500..=599 => increment_metric!(reqstat_5xx self.g),
			status => {
				syslog!(WARNING "{backend} sent back unknown status code {status}");
				increment_metric!(reqstat_other self.g);
			},
		};

		block.set_body(has_body);
		//If discard is set, discard everything.
		if self.flags.get(TflagError) { return Ok(()); }

		//Get rid of alt-svc.
		block.delete_matching_std(|x|x==HttpHdr::AltSvc);

		//Do not accept headers since final reply is received. Anything 200-599 is final reply.
		if self.flags.get(TflagFinalReply) {
			//This most probably causes stream abort.
			return self.do_backward_error(
				InternalResponse::err(BAD_GATEWAY, "Received second set of final headers"));
		}
		//If status is 101, there must be protocol.
		if status == 101 && block.get_special_range(ShdrProtocol).is_none() {
			return self.do_backward_error(InternalResponse::err(BAD_GATEWAY,
				"Backend sent 101 code, but no upgrade header"));
		}
		//If expecting 100-continue response, check that status code is 100 or 300-599.
		if self.flags.get(Tflag100Continue) {
			match status {
				100 => (),		//Acceptable.
				300..=599 => (),	//Acceptable.
				_ => {
					//This status is not legal, send back an error.
					return self.do_backward_error(InternalResponse::err(BAD_GATEWAY,
						format!("Illegal status {status} for expect: 100-continue")));
				}
			}
			self.flags.clear_imp(Tflag100Continue);
		}
		//If reply is to unsafe method, check that status is not 301 or 302.
		//Apparently Wordpress uses these for login. Bleh.
		if self.flags.get(TflagUnsafeOut) && is_buggy_redirect(status) && !self.request.no_redirect_fixup {
			status = match status { 301 => 308, 302 => 307, x => x};
			block.set_status(status);
		}
		//Transfer-Encoding and content-length is not allowed in 1xx or 204 responses.
		//Blech, synapse has a bug, it sends Content-Length 0 with 204 responses.
		if status == 204 { block.workaround_synapse_bug(); }
		if is_info(status) || status == 204 {
			if block.is_transfer_encoding() {
				return self.do_backward_error(InternalResponse::err(BAD_GATEWAY,
					format!("Illegal status {status} for Transfer-Encoding")));
			}
			if block.get_content_length().is_some() {
				return self.do_backward_error(InternalResponse::err(BAD_GATEWAY,
					format!("Illegal status {status} for Content-Length")));
			}
		}
		//If upgrade was not proposed, status 101 is illegal.
		let proposed_upgrade = self.flags.get(TflagUpgradeProposedOut);
		if !proposed_upgrade && status == 101 {
			return self.do_backward_error(
				InternalResponse::err(BAD_GATEWAY, "Unrequested upgrade sent by backend"));
		}
		//If upgrade was proposed, status 200-299 and 304 are illegal, and other final status triggers
		//TX done, as there is no body.
		if proposed_upgrade && (status / 100 == 2 || status == 304)  {
			return self.do_backward_error(
				InternalResponse::err(BAD_GATEWAY, "Backend did not upgrade when requested"));
		}
		if proposed_upgrade && status >= 200 { self.flags.set_imp(TflagTxEnd); }

		//Final headers end the headers. And also 101 upgrade headers.
		if !is_info(status) || status == 101 { self.flags.set_imp(TflagRxHdrEnd); }
		//If status is 101, this is being upgraded.
		if status == 101 { self.flags.set_imp(TflagUpgrade); }

		//If connection: close is present, record it. Also assume that there is no length indication,
		//then behave like connection: close was sent.
		if block.is_connection_close() || (!block.is_chunked() && block.get_content_length().is_none()) {
			self.flags.set_imp(TflagCClose);
		}
		//No keepalive with HTTP/1.0.
		if block.get_special(ShdrVersion).unwrap_or(b"") == b"HTTP/1.0" { self.flags.set_imp(TflagCClose); }

		//For final responses, do some rewriting.
		if !is_info(status) {
			//Rewrite content-type if requested per query, but only for 2xx responses.
			if let (Some(x), 2) = (self.request.cfg.force_mime, block.get_status() / 100) {
				let x = self.hostdata.get_string(x);
				block.set_special(ShdrContentType, x.as_bytes());
			}
			//If error is forced, change the status code. This needs to happen below forcing MIME,
			//as mime type is forced on errors.
			if let Some(errorcode) = self.request.cfg.force_error { block.set_status(errorcode); }
			//If status is not 204 and there is no content-length, force chunked encoding.
			if status != 204 && block.get_content_length().is_none() && !block.is_chunked() {
				block.set_header_std(HttpHdr::TransferEncoding, CHUNKED);
			}
			//Status codes 204 and 304 are magic.
			let is_magic = status == 204 || status == 304;
			//Some content-types are buggy, rewrite those to non-buggy ones. Also do not rewrite
			//content-type of 304 response, as it can break stuff (and for safety, 204 response).
			let newct = match block.get_special(ShdrContentType).unwrap_or(b"") {
				b"text/plain" => Some(TEXT_ISO8859),
				b"text/plain; charset=ISO-8859-1" => Some(TEXT_ISO8859),
				b"text/plain; charset=iso-8859-1" => Some(TEXT_ISO8859),
				b"text/plain; charset=UTF-8" => Some(TEXT_UTF8),
				b"" if !is_magic => Some(DEFAULT_CT),
				_ => None,
			};
			//Check if content-type is one of the whitelisted ones.
			//Don't force CSP none on 304 responses, as it breaks stuff..
			let ct_whitelist = is_magic || {
				let ct = block.get_special(ShdrContentType).unwrap_or(b"");
				match trim_ascii(ct.split(|x|*x==b';').next().unwrap_or(b"")) {
					b"text/html" => true,
					b"application/xhtml+xml" => true,
					_ => false
				}
			};
			if self.request.cfg.add_cors && !self.flags.get(TflagUnsafeOut) {
				block.set_header_std(HttpHdr::AccessControlAllowOrigin, b"*");
			}
			if let Some(newct) = newct { block.set_special(ShdrContentType, newct); }
			//Rewrite the response headers.
			self.hostdata.get_response_rewrite().rewrite_headers(block, true);
			self.hostdata.get_location_rewrite().rewrite_headers(block);
			//Force XCTO, and sometimes CSP.
			block.delete_matching_std(|name|(!is_magic && name == HttpHdr::XContentTypeOptions) ||
				(!ct_whitelist && name == HttpHdr::ContentSecurityPolicy));
			if !is_magic { block.set_header_std(HttpHdr::XContentTypeOptions, FORCED_XCTO); }
			if !ct_whitelist {
				block.set_header_std(HttpHdr::ContentSecurityPolicy, DEFAULT_CSP);
			}
		}
		//HTTP responses are not supposed to have any dangerous headers, delete them off. This prevents
		//sending HSTS on http://, which is just wrong.
		if !self.request.attributes.is_secure() {
			block.delete_matching(|h|is_dangerous_header_response(h.as_bytes()));
		}
		//Forward the header block. There are multiple cases here:
		//
		//1) If this points to valid connection and succeeds, carry on.
		//2) If this points to valid connection but fails, then dissociate.
		//3) If this points to invalid connection, then dissociate.
		if self.request.cid.call_with_connection(|conn|{
			conn.write_backward_headers(block, &self.request, backend, backend_name).
				set_err(Cow::Borrowed(""))
		}).is_err() { self.dissociate()?; }
		//If we sent final headers, then note that.
		if !is_info(status) {
			self.flags.set_imp(TflagFinalReply);
		}
		//If there is no body, the receive ends here.
		if !has_body { self.flags.set_imp(TflagRxEnd); }
		Ok(())
	}
	pub fn do_backward_data(&mut self, d: &[u8]) -> Result<usize, SinkError>
	{
		//Forward the data block. There are multiple cases here:
		//
		//1) If this points to valid connection and succeeds, carry on.
		//2) If this points to valid connection but fails, then dissociate.
		//3) If this points to invalid connection, then dissociate.
		//
		//Note that if dissociated, all data is accepted.
		if self.flags.get(TflagError) { return Ok(d.len()); }
		let mut amt = 0;
		match self.request.cid.call_with_connection(|conn|{
			Ok(amt = conn.write_backward_data(d, &self.request).set_err("")?)
		}) {
			Ok(_) => Ok(amt),
			Err(_) => { self.dissociate()?; Ok(d.len()) },
		}
	}
	pub fn do_backward_trailers(&mut self, block: &mut HeaderBlock) -> Result<(), SinkError>
	{
		//Note that we need to note that backward trailers happened, regardless of discard flag.
		self.flags.set_imp(TflagRxEnd);
		if self.flags.get(TflagError) { return Ok(()); }
		if self.request.cid.call_with_connection(|conn|{
			Ok(conn.write_backward_trailers(block, &self.request))
		}).is_err() { self.dissociate()?; }
		//We do not dissociate in the successful case, in order to wait for TX to complete.
		Ok(())
	}
	pub(crate) fn do_backward_error(&mut self, status: InternalResponse) -> Result<(), SinkError>
	{
		self.request.cid.call_with_connection(|conn|{
			Ok(conn.write_backward_error(&self.request, status))
		}).ok();
		//Unconditionally dissociate after error.
		self.dissociate()
	}
	pub fn do_notify_ready(&mut self)
	{
		//Do not care about errors, this is for master anyway.
		self.request.cid.call_with_connection(|conn|Ok(conn.backward_has_data(&self.request))).ok();
	}
	pub fn is_busy(&self) -> bool
	{
		let mut ret = false;
		self.request.cid.call_with_connection(|conn|Ok(ret = conn.write_buffer_has_data())).ok();
		ret
	}
	pub fn unblock_master(&mut self)
	{
		self.request.cid.call_with_connection(|conn|Ok(conn.forward_unblock(&self.request))).ok();
		//Just ignore requests to invalid and nonexistent destinations.
	}
	fn dissociate(&mut self) -> Result<(), SinkError>
	{
		//Note that if connection does dissociate, there are no headers sent, because either
		//the connection points nowhere, or the headers were already sent.
		//On dissociation, if the TX has not ended, or if connection is upgraded, tear it
		//down. Otherwise discard the remaining reply.
		fail_if!(!self.flags.get(TflagTxEnd), SinkError::MasterFailTx);
		fail_if!(self.flags.get(TflagUpgrade), SinkError::MasterFailUpgraded);
		self.flags.set_imp(TflagError);
		Ok(())
	}
}

#[test]
fn test_product_name_version()
{
	let (name, version) = get_product_name_version();
	assert_eq!(name, PRODUCT_NAME.as_bytes());
	assert_eq!(version, PRODUCT_VERSION.as_bytes());
	set_product_name_and_version("foo", "barzot");
	let (name, version) = get_product_name_version();
	assert_eq!(name, b"foo");
	assert_eq!(version, b"barzot");
}


#[test]
fn path_test_overlap()
{
	let mut p = PathData::new();
	let mut g = PathData::new();
	assert_eq!(path_setting_dflt(None, &g, !0, |p|p.logflags), !0);
	g.logflags = Some(42);
	assert_eq!(path_setting_dflt(None, &g, !0, |p|p.logflags), 42);
	assert_eq!(path_setting_dflt(Some(&p), &g, !0, |p|p.logflags), 42);
	p.logflags = Some(67);
	assert_eq!(path_setting_dflt(Some(&p), &g, !0, |p|p.logflags), 67);
	g.logflags = None;
	assert_eq!(path_setting_dflt(Some(&p), &g, !0, |p|p.logflags), 67);
	p.logflags = None;
	assert_eq!(path_setting_dflt(Some(&p), &g, !0, |p|p.logflags), !0);
}

#[test]
fn path_test_overlap2()
{
	let mut p = PathData::new();
	let mut g = PathData::new();
	assert_eq!(path_setting(None, &g, |p|p.logflags), None);
	g.logflags = Some(42);
	assert_eq!(path_setting(None, &g, |p|p.logflags), Some(42));
	assert_eq!(path_setting(Some(&p), &g, |p|p.logflags), Some(42));
	p.logflags = Some(67);
	assert_eq!(path_setting(Some(&p), &g, |p|p.logflags), Some(67));
	g.logflags = None;
	assert_eq!(path_setting(Some(&p), &g, |p|p.logflags), Some(67));
	p.logflags = None;
	assert_eq!(path_setting(Some(&p), &g, |p|p.logflags), None);
}
