use crate::ConfigT;
use crate::HttpBridgeProxyEx;
use crate::bitmask::Bitfield;
use crate::connection_in_h1::ConnectionMode as ConnectionModeH1;
use crate::connection_logical::LogicalConnection;
use crate::connection_lowlevel::allocate_connection;
use crate::connection_lowlevel::CID;
use crate::connection_lowlevel::PreCID;
use crate::connection_midlevel::_Krypted;
use crate::connection_midlevel::ConnectionFreeList;
use crate::connection_midlevel::Dekryptize;
use crate::connection_midlevel::Interrupter;
use crate::connection_midlevel::Krypted;
use crate::connection_midlevel::Kryptize;
use crate::connection_midlevel::LinkState;
use crate::connection_midlevel::LinkStatus;
use crate::connection_midlevel::ReaderWriterM;
use crate::connection_midlevel::ReaderWriterS;
use crate::connection_midlevel::ReaderWriterX;
use crate::connection_midlevel::SinkError;
use crate::connection_midlevel::WriterB;
use crate::connection_midlevel::WriterBC;
use crate::connection_midlevel::WriterM;
use crate::connection_midlevel::WriterMC;
use crate::connection_midlevel::WriterS;
use crate::connection_midlevel::WriterS2;
use crate::connection_midlevel::WriterSC;
use crate::connection_midlevel::WriterUnconnected;
use crate::connection_midlevel::WriterXC;
use crate::connection_midlevel::WriterXC2;
use crate::connection_out::BackendError;
use crate::connection_out::ConnectionParameters as ConnectionOutParams;
use crate::headerblock::HeaderBlock;
use crate::headerblock::ShdrReason;
use crate::http::ConnectionPropertiesI;
use crate::http::RequestInfo;
use crate::http::ClientInfoIn;
use crate::http::DfltIRWError;
use crate::http::EarlyRequestInfo;
use crate::http::InternalResponse;
use crate::http::RequestWriteReturn;
use crate::metrics_dp::GlobalMetrics;
use crate::routing_dp::HostRoutingDataRef;
use crate::staticfile::ConnectionParameters as StaticFileParams;
use crate::utils::Buffer;
use crate::utils::cia_to_ip;
use btls_aux_http::http::ReceiveBuffer as HttpReceiveBuffer;
use btls_aux_http::http::Sink as HttpSink;
use btls_aux_http::http::Source as HttpSource;
use btls_aux_memory::SafeShowByteString;
use btls_aux_time::PointInTime;
use btls_aux_time::TimeUnit;
use btls_aux_unix::SocketFlags;
use btls_aux_unix::SocketType;
use btls_daemon_helper_lib::AddressHandle;
use btls_daemon_helper_lib::AddTokenCallback;
use btls_daemon_helper_lib::AllocatedToken;
use btls_daemon_helper_lib::ConnectionInfo;
use btls_daemon_helper_lib::ConnectionInfoAddress;
use btls_daemon_helper_lib::cow;
use btls_daemon_helper_lib::FileDescriptor;
use btls_daemon_helper_lib::FdPoller;
use btls_daemon_helper_lib::IO_WAIT_HUP;
use btls_daemon_helper_lib::IO_WAIT_INTR;
use btls_daemon_helper_lib::IO_WAIT_ONESHOT;
use btls_daemon_helper_lib::IO_WAIT_READ;
use btls_daemon_helper_lib::IO_WAIT_WRITE;
use btls_daemon_helper_lib::is_fatal_error;
use btls_daemon_helper_lib::ListenerLoop;
use btls_daemon_helper_lib::Poll;
use btls_daemon_helper_lib::SocketAddrEx;
use btls_daemon_helper_lib::SocketAddrExHandle;
use btls_daemon_helper_lib::TokenAllocatorPool;
use btls_util_logging::ConnId;
use btls_util_logging::log;
use btls_util_logging::set_current_task;
use btls_util_logging::syslog;
use std::borrow::Cow;
use std::cmp::max;
use std::cmp::min;
use std::fmt::Debug;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::io::Error as IoError;
use std::io::ErrorKind as IoErrorKind;
use std::mem::MaybeUninit;
use std::mem::replace;
use std::mem::swap;
use std::mem::transmute;
use std::ops::Deref;
use std::ops::DerefMut;
use std::path::PathBuf;
use std::rc::Rc;
use std::slice::from_raw_parts_mut;
use std::sync::Arc;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering;


#[derive(Clone)]
pub enum BackendName<'a>
{
	Static(&'a str),
	Handle(Option<&'a AddressHandle>),
}

impl<'a> Display for BackendName<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&BackendName::Static(dir) => write!(f, "static:{dir}"),
			&BackendName::Handle(Some(ref x)) => x.print_addr(f),
			&BackendName::Handle(None) => f.write_str("<unknown>"),
		}
	}
}

//This uses HeaderBlock instead of HeaderBlockTrait, because OwnedTrailerBlock sure does not do anything sane
//here.
pub fn print_backend_response(block: &HeaderBlock, req: &RequestInfo, backend_conn: ConnId,
	backend_name: BackendName) -> u16
{
	let status = block.get_status();
	let bit = match status {
		100..=199 => 1,
		200..=299 => 2,
		300..=303 => 3,
		304 => 2,	//304 is treated as 2xx.
		305..=399 => 3,
		400..=499 => 4,
		500..=599 => 5,
		_ => 0
	};
	if req.cfg.logflags & (1 << bit) != 0 {
		//Print to log because PII.
		let reason = block.get_special(ShdrReason).unwrap_or(b"");
		log!(INFO "{url}(from {ip})...{status}({reason}) (via {backend_conn})",
			url=req.url, ip=req.client_ip, reason=SafeShowByteString(reason));
	}
	if bit == 5 {
		//Print internal errors also to syslog, without client IP.
		let reason = block.get_special(ShdrReason).unwrap_or(b"");
		syslog!(WARNING "{url}...{status}({reason}) (via {backend_name})",
			url=req.url, reason=SafeShowByteString(reason));
	}
	status
}

pub(crate) fn print_internal_response(req: &RequestInfo, status: &InternalResponse, warning: bool)
{
	let (level, bit) = if warning { ('W', 6) } else { ('I', 7) };
	if req.cfg.logflags & (1 << bit) != 0 {
		//Print to log because PII.
		log!(_ level, "{url}(from {ip})...{status}", url=req.url, ip=req.client_ip);
	}
	if warning {
		//Print warnings also to syslog, without client IP.
		syslog!(WARNING "{url}...{status}", url=req.url);
	}
}

pub(crate) fn print_internal_response_eurl(ereq: &EarlyRequestInfo, status: &InternalResponse, cat: u16)
{
	//cat=2 is used by no such host errors.
	let bit = match cat { 0 => 7, 1 => 6, _ => 8 };
	let level = match cat { 0 => 'I', 1 => 'W', _ => 'I' };
	if ereq.logflags & (1 << bit % 32) != 0 {
		//Print to log because PII.
		log!(_ level, "{url}(from {ip})...{status}", url=ereq.url, ip=ereq.client_ip);
	}
	if cat == 1 {
		//Print category 1 (warning) messages also to syslog with no client IP.
		syslog!(WARNING "{url}...{status}", url=ereq.url);
	}
}

fn alloc_token_err(_: impl Sized) -> Cow<'static, str> { cow!("Failed to allocate token") }

#[derive(Copy,Clone)]
struct MetricsAttribute<'a>
{
	set: &'a GlobalMetrics,
	service: bool,
}

impl<'a> MetricsAttribute<'a>
{
	fn new(g: ConfigT, service: bool) -> MetricsAttribute<'a>
	{
		MetricsAttribute {
			set: g.metrics_ref(),
			service: service,
		}
	}
	fn read(&self, amt: usize)
	{
		if self.service {
			increment_metric!(serv_read [amt] self.set)
		} else {
			increment_metric!(tcp_read [amt] self.set)
		}
	}
	fn write(&self, amt: usize)
	{
		if self.service {
			increment_metric!(serv_write [amt] self.set)
		} else {
			increment_metric!(tcp_write [amt] self.set)
		}
	}
	fn error(&self)
	{
		if self.service {
			increment_metric!(error_serv self.set)
		} else {
			increment_metric!(error_tcp self.set)
		}
	}
}

fn attribute_error_m(g: &GlobalMetrics, service: bool)
{
	if service { increment_metric!(error_serv g) } else { increment_metric!(error_tcp g) }
}


const MAX_WRITE: usize = 131072;	//128kB.
const MAX_READ: usize = 16400;

pub fn combine_timestamps<C>(set: &[Option<PointInTime>], cmp: C) -> Option<PointInTime>
	where C: Fn(PointInTime, PointInTime) -> PointInTime
{
	let mut first = None;
	for ent in set.iter().cloned() {
		first = match (first, ent) {
			(Some(a), Some(b)) => Some(cmp(a, b)),
			(Some(a), None) => Some(a),
			(None, Some(b)) => Some(b),
			(None, None) => None,
		};
	}
	first
}

pub struct Reader<'a>
{
	dekryptize: Option<&'a mut (dyn Dekryptize+'static)>,
	socket: FileDescriptor,
	last_read_ts: &'a mut PointInTime,
	shutdown: &'a mut bool,
	buffered_data: &'a mut bool,
	saved_buffer: &'a mut Vec<u8>,
	metrics: MetricsAttribute<'a>,
}

fn handle_read_ret<'a>(r: Result<&'a [u8], IoError>, ts: &mut PointInTime, shutdown: &mut bool,
	metrics: MetricsAttribute) -> Result<Option<&'a [u8]>, IoError>
{
	match r {
		Ok(buf) => if buf.len() > 0 {
			metrics.read(buf.len());		//Attribute data read.
			*ts = PointInTime::now();		//Bump TS.
			Ok(Some(buf))
		} else {
			*shutdown = true;			//Got EOF.
			*ts = PointInTime::now();		//Bump TS (got EOF).
			Ok(None)
		},
		Err(ref e) if !is_fatal_error(e) => Ok(Some(&[])),
		Err(e) => {
			metrics.error();
			Err(e)
		},
	}
}

impl<'a> Reader<'a>
{
	fn _readu_dekryptized(&mut self, data: &mut [MaybeUninit<u8>], dekryptize: &mut dyn Dekryptize) ->
		Result<Option<usize>, IoError>
	{
		//Only read TCP if there is no pending data and TCP has not been shut down yet.
		if !*self.buffered_data && !*self.shutdown {
			let mut buf = [MaybeUninit::uninit();20480];
			if let Some(buf) = handle_read_ret(self.socket.read_uninit2(&mut buf), self.last_read_ts,
				self.shutdown, self.metrics)? {
				if let Err(err) = dekryptize.dekryptize(buf) {
					self.metrics.error();
					fail!(err);
				};
			}
		}
		Ok(match dekryptize.dekryptized_input(data) {
			Some((amt, more)) => {
				*self.buffered_data = more;
				Some(amt)
			},
			None => {
				*self.buffered_data = false;		//Ran out of buffered data.
				None					//EOF.
			},
		})
	}
	//Note: Ok(Some(0)) means 0 bytes were successfully read, not EOF.
	fn _read(&mut self, data: &mut [u8]) -> Result<Option<usize>, IoError>
	{
		//Use the uninitialized variant. Safety: u8 and MaybeUninit<u8> are layout-compatible, and the
		//uninitialized variant never deinitializes anything.
		self._readu(unsafe{transmute(data)})
	}
	fn _readu(&mut self, data: &mut [MaybeUninit<u8>]) -> Result<Option<usize>, IoError>
	{
		let dekryptize = replace(&mut self.dekryptize, None);
		if let Some(dekryptize) = dekryptize {
			let r = self._readu_dekryptized(data, dekryptize);
			self.dekryptize = Some(dekryptize);
			return r;
		} else {
			//Do not read after shutdown.
			if *self.shutdown { return Ok(None); }
			let r = handle_read_ret(self.socket.read_uninit2(data), self.last_read_ts, self.shutdown,
				self.metrics)?;
			//Return the amount handled, or None on EOF.
			Ok(r.map(|x|x.len()))
		}
	}
}

impl<'a> HttpSource for Reader<'a>
{
	type Buffer = ReaderWriterBuffer;
	type Error = IoError;
	fn read(&mut self, buf: &mut ReaderWriterBuffer) -> Result<bool, IoError>
	{
		//Replace the buffer with the saved one.
		buf.0 = replace(self.saved_buffer, Vec::new());
		//HACK: Reduce buffer size to 16384 bytes for services. This prevents nasty write tearing with
		//HTTP/2.
		let mamt = if self.metrics.service { min(MAX_READ, 16384) } else { MAX_READ };
		//Resize the buffer to be sufficiently big for the read.
		buf.0.truncate(0);
		if buf.0.capacity() < mamt { buf.0.reserve(mamt); }
		//Grab a buffer to read.
		let rbuf: &mut [MaybeUninit<u8>] = unsafe{from_raw_parts_mut(buf.0.as_mut_ptr().cast(), mamt)};
		match self._readu(rbuf) {
			Ok(amt) => {
				//If data was read, include it in buffer. If this is EOF, amt=None, so the length
				//will remain at 0.
				amt.map(|amt|unsafe{buf.0.set_len(amt)});
				Ok(amt.is_some())		//More data?
			},
			Err(e) => if is_fatal_error(&e) {
				self.metrics.error();
				fail!(e);
			} else {
				Ok(true)	//Assume more data.
			}
		}
	}
	fn reclaim_buffer(&mut self, buf: ReaderWriterBuffer)
	{
		//Take the buffer that was used in read and save it for the next read.
		*self.saved_buffer = buf.0;
	}
}


pub struct ReaderWriter<'a>
{
	reader: Reader<'a>,
	writer: Writer<'a>
}

impl<'a> ReaderWriterM for ReaderWriter<'a>
{
	type Reader = Reader<'a>;
	type Writer = Writer<'a>;
	fn as_writer<'b>(&'b mut self) -> &'b mut Writer<'a> { &mut self.writer }
	fn split<'b>(&'b mut self) -> (&'b mut Reader<'a>, &'b mut Writer<'a>)
	{
		(&mut self.reader, &mut self.writer)
	}
}

impl<'a> ReaderWriterS for ReaderWriter<'a>
{
	type Reader = Reader<'a>;
	type Writer = Writer<'a>;
	fn as_writer<'b>(&'b mut self) -> &'b mut Writer<'a> { &mut self.writer }
	fn split<'b>(&'b mut self) -> (&'b mut Reader<'a>, &'b mut Writer<'a>)
	{
		(&mut self.reader, &mut self.writer)
	}
}

impl<'a> ReaderWriterX for ReaderWriter<'a>
{
	fn bump_timeout(&mut self)
	{
		*self.reader.last_read_ts = PointInTime::now();
		*self.writer.last_write_ts = PointInTime::now();
	}
	fn aborted(&self) -> bool { self.writer.aborted() }
	fn read(&mut self, buf: &mut [u8]) -> Result<Option<usize>, IoError>
	{
		match self.reader._read(buf) {
			Ok(x) => Ok(x),
			Err(ref e) if !is_fatal_error(e) => Ok(Some(0)),
			Err(e) => Err(e)
		}
	}
}

#[derive(Clone)]
pub struct KryptedTarget
{
	addr: SocketAddrExHandle,
	krypt: Option<Arc<dyn _Krypted+Send+Sync>>,
}

impl Debug for KryptedTarget
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		//TODO: Print the krypt stuff.
		Display::fmt(&self.addr, f)
	}
}

impl KryptedTarget
{
	pub fn is_krypted(&self) -> bool { self.krypt.is_some() }
	pub fn is_ok(&self) -> bool { self.addr.borrow_status().get_status() }
	pub fn new_plaintext(addr: SocketAddrEx) -> KryptedTarget
	{
		KryptedTarget {
			addr: SocketAddrExHandle::new(addr),
			krypt: None,
		}
	}
	pub fn new_tls_client(addr: SocketAddrEx, krypt: Arc<crate::connection_krypt_tls::TlsClient>) ->
		KryptedTarget
	{
		KryptedTarget {
			addr: SocketAddrExHandle::new(addr),
			krypt: Some(krypt),
		}
	}
}

pub trait ConnectorTrait
{
	fn connect_target2<'t>(&mut self, p: ConnectionOutParams, targets: Arc<(Vec<KryptedTarget>, AtomicUsize)>,
		tname: &str) -> Result<CID, Cow<'static, str>>;
	fn staticbackend(&mut self, p: StaticFileParams) -> Result<CID, Cow<'static, str>>;
	fn static_file(&mut self, p: PathBuf, uncompressed: bool, is_error: bool) ->
		Result<CID, Cow<'static, str>>;
}

pub struct Connector<'b>
{
	addtoken: &'b mut AddTokenCallback,
	g: ConfigT,
}

impl<'a> Connector<'a>
{
	pub fn set_source(&mut self, addr: &ConnectionInfoAddress) { self.addtoken.set_source(addr); }
}

fn choose_among<'a>(targets: &'a [KryptedTarget], filter: impl Fn(&KryptedTarget) -> bool, tnum: usize) ->
	Option<&'a KryptedTarget>
{
	let count = targets.iter().filter(|t|filter(t)).count();
	//Are there any valid choices.
	fail_if_none!(count == 0);
	//This should always produce a valid index.
	targets.iter().filter(|t|filter(t)).nth(tnum % count)
}

fn choose_priority<'a>(targets: &'a [KryptedTarget], tnum: usize) -> Option<&'a KryptedTarget>
{
	//Fast path for one target. If targets.len() > 0, then targets[0] is in range.
	if targets.len() == 1 { return Some(&targets[0]); }
	//First loop through targets that are OK and no-krypt.
	if let Some(target) = choose_among(targets, |t|t.is_ok()&&!t.is_krypted(), tnum) {
		return Some(target);
	}
	//Then loop through targets that are OK and krypt.
	if let Some(target) = choose_among(targets, |t|t.is_ok()&&t.is_krypted(), tnum) {
		return Some(target);
	}
	//And finally through targets that are not OK.
	if let Some(target) = choose_among(targets, |t|!t.is_ok(), tnum) {
		return Some(target);
	}
	None
}


impl<'b> ConnectorTrait for Connector<'b>
{
	fn connect_target2<'t>(&mut self, p: ConnectionOutParams, targets: Arc<(Vec<KryptedTarget>, AtomicUsize)>,
		tname: &str) -> Result<CID, Cow<'static, str>>
	{
		ConnectionInner::connect_tail2(targets, self.addtoken, p, tname, self.g)
	}
	fn staticbackend(&mut self, p: StaticFileParams) -> Result<CID, Cow<'static, str>>
	{
		ConnectionInner::staticbackend(self.addtoken, p, self.g)
	}
	fn static_file(&mut self, p: PathBuf, uncompressed: bool, is_error: bool) ->
		Result<CID, Cow<'static, str>>
	{
		ConnectionInner::static_file(self.addtoken, p, uncompressed, is_error, self.g)
	}
}

pub struct ReaderWriterBuffer(Vec<u8>);

impl HttpReceiveBuffer for ReaderWriterBuffer
{
	fn new() -> ReaderWriterBuffer
	{
		ReaderWriterBuffer(Vec::new())
	}
	fn borrow<'a>(&'a mut self) -> &'a mut [u8]
	{
		self.0.deref_mut()
	}
}

fn handle_write_ret(r: Result<usize, IoError>, ts: &mut PointInTime, metrics: MetricsAttribute) ->
	Result<usize, IoError>
{
	match r {
		Ok(amt) => {
			//Attribute the written parts, update the timestamp.
			if amt > 0 { *ts = PointInTime::now(); }
			metrics.write(amt);
			Ok(amt)
		},
		Err(ref e) if !is_fatal_error(e) => Ok(0),
		Err(e) => {
			metrics.error();
			fail!(e);
		},
	}
}

fn format_connect_err(name: &SocketAddrEx, err: impl Display) -> Cow<'static, str>
{
	cow!(F "Failed to connect to backend {name}: {err}")
}

pub struct Writer<'a>
{
	kryptize: Option<&'a mut (dyn Kryptize+'static)>,
	socket: FileDescriptor,
	write_buffer: &'a mut Buffer,
	last_write_ts: &'a mut PointInTime,
	id: ConnId,
	shutdown: &'a mut bool,
	irq: Interrupter,
	cid: CID,
	abort_error: Option<SinkError>,
	metrics: MetricsAttribute<'a>,
}

impl<'a> Writer<'a>
{
	fn __discard_sg_data(data: &[&[u8]], fragcpl: &mut usize, fragitr: &mut usize, mut amt: usize)
	{
		//Now, remove amt from data buffer.
		while *fragcpl < data.len() {
			let fleft = data[*fragcpl].len().saturating_sub(*fragitr);
			if amt < fleft {
				//Not enough data to cover the rest of fragment. So iteration ends here.
				*fragitr += amt;
				break;
			} else {
				//Entiere fragment removed. Mark as complete, next fragment starts at
				//the beginning.
				amt -= fleft; 
				*fragcpl += 1;
				*fragitr = 0;
			}
		}
	}
	fn __writev_kryptized(&mut self, data: &[&[u8]], kryptize: &mut dyn Kryptize) -> Result<(), IoError>
	{
		let was_empty = self.write_buffer.is_empty();
		//Connected is asserted. Do this in two passes. The first pass, while write_buffer is
		//empty, kryptize fragments and immediately try to send them out. In the second pass, kryptize
		//fragments and buffer them.
		let tmp = kryptize.kryptize(data).map_err(|e|{self.metrics.error(); e})?;
		ConnectionInner::_try_flush_buffer(self.write_buffer, &self.socket, &tmp)?;
		kryptize.return_buffer(tmp);
		//If buffer now has data, fire an IRQ in order to flush the data.
		if was_empty && self.write_buffer.has_data() { self.interrupt(); }
		Ok(())
	}
	fn __shutdown_kryptized(&mut self, kryptize: &mut dyn Kryptize) -> Result<(), IoError>
	{
		let was_empty = self.write_buffer.is_empty();
		let tmp = kryptize.kryptize_eof().map_err(|e|{self.metrics.error(); e})?;
		ConnectionInner::_try_flush_buffer(self.write_buffer, &self.socket, &tmp)?;
		kryptize.return_buffer(tmp);
		//If buffer now has data, fire an IRQ in order to flush the data.
		if was_empty && self.write_buffer.has_data() { self.interrupt(); }
		Ok(())
	}
}

impl<'a> WriterB for Writer<'a>
{
	fn get_id(&self) -> ConnId { self.id }
	fn aborted(&self) -> bool { self.abort_error.is_some() }
	fn interrupt(&mut self) { self.irq.interrupt(); }
	fn is_empty(&self) -> bool { self.write_buffer.is_empty() }
	fn shutdown(&mut self) -> Result<(), IoError>
	{
		if replace(self.shutdown, true) { return Ok(()); }
		//Kryptized writers have their own special version.
		let kryptize = replace(&mut self.kryptize, None);
		if let Some(kryptize) = kryptize {
			let r = self.__shutdown_kryptized(kryptize);
			self.kryptize = Some(kryptize);
			return r;
		}
		self.socket.shutdown_write().ok();
		//Clear buffers.
		self.write_buffer.clear();
		Ok(())
	}
	fn abort(&mut self, err: SinkError) { self.abort_error = Some(err); }
}

impl<'a> WriterM for Writer<'a>
{
	fn freespace(&self, bias: usize) -> usize { self.write_buffer.freespace(bias) }
	fn get_cid(&self) -> CID { self.cid.clone() }
}

impl<'a> WriterBC for Writer<'a>
{
	fn take_error_or<T:Sized>(&mut self, val: T) -> Result<T, Cow<'static, str>>
	{
		match replace(&mut self.abort_error, None) {
			Some(err) => Err(cow!(F "{err}")),
			None => Ok(val)
		}
	}
}

impl<'a> WriterS2 for Writer<'a>
{
	fn swap_buffers(&mut self, buf: &mut Buffer)
	{
		let kryptize = replace(&mut self.kryptize, None);
		if let Some(kryptize) = kryptize {
			//Write the given buffer and clear it (since this is logcally swap with empty buffer).
			//Then return the borrowed kryptize.
			let r = self.__writev_kryptized(&[buf.as_slice()], kryptize);
			buf.clear();
			self.kryptize = Some(kryptize);
			//This is complicated by the fact that swap_buffers can not return an error. So store
			//the error into abort.
			if let Err(r) = r {
				self.abort_error = Some(SinkError::WriteFailed(DfltIRWError::new(r)));
			}
		} else {
			swap(self.write_buffer, buf);
		}
		self.irq.interrupt();
	}
}

impl<'a> WriterSC for Writer<'a>
{
	fn is_ready(&self) -> bool { true }	//Always ready.
}

impl<'a> WriterS for Writer<'a> {}
impl<'a> WriterMC for Writer<'a> {}
impl<'a> WriterXC for Writer<'a> {}
impl<'a> WriterXC2 for Writer<'a> {}

impl<'a> HttpSink for Writer<'a>
{
	type Error = IoError;
	fn write(&mut self, buf: &[&[u8]]) -> Result<(), IoError>
	{
		//If shutdown is set, do not accept data.
		fail_if!(*self.shutdown, IoError::new(IoErrorKind::Other, "Connection closed for writing"));
		//Kryptized connections need to be handled specially.
		let kryptize = replace(&mut self.kryptize, None);
		if let Some(kryptize) = kryptize {
			let r = self.__writev_kryptized(buf, kryptize);
			self.kryptize = Some(kryptize); //Return pointer.
			return r;
		}
		//If write buffer is empty and socket is connected, try immediate write.
		let (mut wskip, odata) = if self.write_buffer.is_empty() {
			(handle_write_ret(self.socket.writev(buf), self.last_write_ts, self.metrics)?, false)
		} else {
			(0, true)
		};
		//Now, cache everything except first wskip bytes.
		for frag in buf.iter() {
			let frag = if wskip >= frag.len() {
				wskip -= frag.len();
				continue;
			} else {
				let frag = &frag[wskip..];
				wskip = 0;
				frag
			};
			self.write_buffer.append(frag).map_err(|_|{
				IoError::new(IoErrorKind::Other, "Send-Q overflow")
			})?;
		}
		//If buffer now has data, fire an IRQ in order to flush the data.
		if !odata && self.write_buffer.has_data() {
			self.irq.interrupt();
		}
		Ok(())
	}
}

macro_rules! with_maybe_connected
{
	($selfx:ident $closure:expr) => {
		if $selfx.link_state.is_connection_in_progress() {
			$selfx.with_writer_unconnected($closure)
		} else {
			$selfx.with_writer_connected($closure)
		}
	}
}

define_bitfield_type!(ConnectionFlagsTag, u8);
define_bitfield_bit!(CflagSlave, ConnectionFlagsTag, 0);

struct ConnectToSuccessful
{
	tcp_ok: bool,
	krypt_ok: bool,
	socket: FileDescriptor,
	krypted: Option<(Box<dyn Kryptize+'static>, Box<dyn Dekryptize+'static>)>,
}

fn do_connect_to(target: &KryptedTarget, token: &AllocatedToken) -> Result<ConnectToSuccessful, String>
{
	//Connect the socket.
	let addr = target.addr.borrow_addr();
	let af = addr.get_family().ok_or_else(||"Unknown address type")?;
	let flags = SocketFlags::NONBLOCK|SocketFlags::CLOEXEC;
	let fd = FileDescriptor::socket2(af, SocketType::STREAM, Default::default(), flags).map_err(|err|{
		format!("socket: {err}")
	})?;
	let immediate = match fd.connect_ex(addr) {
		//This is HTTP, so do not perform address status change on connect.
		Ok(immediate) => immediate,
		//But connect failure is a failure.
		Err(err) => {
			target.addr.borrow_status().set_status(false);
			fail!(format!("connect: {err}"));
		}
	};
	//Disable Nagle. Do not care if this fails.
	if immediate { fd.set_tcp_nodelay(true).ok(); }
	//Set up TLS.
	let (krypted, krypted_ready) = target.krypt.new_krypted(ListenerLoop::remote_irq_handle(token));
	Ok(ConnectToSuccessful {
		tcp_ok: immediate,
		krypt_ok: krypted_ready,
		socket: fd,
		krypted: krypted,
	})
}

struct PrintSecurityParameters<'a>(Option<&'a (dyn Kryptize+'static)>);

impl<'a> Display for PrintSecurityParameters<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match &self.0 {
			&Some(ref k) => k.print_parameters(f),
			&None => f.write_str("plaintext")
		}
	}
}

thread_local!(static DUMMY_TARGETS_SET: Arc<(Vec<KryptedTarget>, AtomicUsize)> =
	Arc::new((Vec::new(), AtomicUsize::new(0))));

pub struct ConnectionInner
{
	krypted: Option<(Box<dyn Kryptize+'static>, Box<dyn Dekryptize+'static>)>,
	data_in_dekryptize: bool,				//Data waiting in dekryptize.
	targets_set: Arc<(Vec<KryptedTarget>, AtomicUsize)>,	//Set of targets.
	socket: FdPoller,
	socketinfo: ConnectionInfo,
	token: AllocatedToken,
	link_state: LinkState,
	attempts: usize,
	last_read_ts: PointInTime,
	last_write_ts: PointInTime,
	self_cid: CID,
	write_buffer: Buffer,
	recycled_read_buffer: Vec<u8>,
	connid: ConnId,				//Connection name.
	rshutdown: bool,
	wshutdown: bool,
	logical: LogicalConnection,
	g: ConfigT,
	flags: Bitfield<ConnectionFlagsTag>,
}

impl ConnectionInner
{
	//Call closure on logical connection. The connection must be in unconnected or half-connected state
	//(that is, connect_timeout must be armed).
	//
	//Returns whatever value the specified closure returns.
	fn with_writer_unconnected<T>(&mut self,
		f: impl FnOnce(&mut WriterUnconnected, &mut LogicalConnection) -> T) -> T
	{
		if self.link_state.is_fully_connected() {
			syslog!(CRITICAL "with_writer_unconnected(): Connection {conn} is already connected",
				conn=self.connid);
		}
		let mut w = WriterUnconnected::new(self.connid, &self.token);
		f(&mut w, &mut self.logical)
	}
	//Call closure on logical connection. The connection must be in connected state (that is,
	//connect_timeout must not be armed).
	//
	//Returns whatever value the specified closure returns.
	fn with_writer_connected<T>(&mut self, f: impl FnOnce(&mut Writer, &mut LogicalConnection) -> T) -> T
	{
		if self.link_state.is_connection_in_progress() {
			syslog!(CRITICAL "with_writer_connected(): Connection {conn} is not already connected",
				conn=self.connid);
		}
		let kryptize = match &mut self.krypted {
			&mut Some((ref mut kryptize, _)) => Some(kryptize.deref_mut()),
			&mut None => None,
		};
		let mut w = Writer{
			kryptize: kryptize,
			write_buffer: &mut self.write_buffer,
			last_write_ts: &mut self.last_write_ts,
			socket: self.socket.inner(),
			id: self.connid,
			shutdown: &mut self.wshutdown,
			irq: Interrupter::new(&self.token),
			cid: self.self_cid.clone(),
			abort_error: None,
			metrics: MetricsAttribute::new(self.g, self.flags.get(CflagSlave)),
		};
		f(&mut w, &mut self.logical)
	}
	fn __do_read_connected(&mut self, _: &mut Poll, addtoken: &mut AddTokenCallback, id: ConnId) ->
		Result<(), Cow<'static, str>>
	{
		let (kryptize, dekryptize) = match &mut self.krypted {
			&mut Some((ref mut kryptize, ref mut dekryptize)) =>
				(Some(kryptize.deref_mut()), Some(dekryptize.deref_mut())),
			&mut None => (None, None),
		};
		let mut w = ReaderWriter{
			reader: Reader {
				dekryptize: dekryptize,
				buffered_data: &mut self.data_in_dekryptize,
				saved_buffer: &mut self.recycled_read_buffer,
				socket: self.socket.inner(),
				last_read_ts: &mut self.last_read_ts,
				shutdown: &mut self.rshutdown,
				metrics: MetricsAttribute::new(self.g, self.flags.get(CflagSlave)),
			},
			writer: Writer{
				kryptize: kryptize,
				write_buffer: &mut self.write_buffer,
				last_write_ts: &mut self.last_write_ts,
				socket: self.socket.inner(),
				id: id,
				irq: Interrupter::new(&self.token),
				shutdown: &mut self.wshutdown,
				cid: self.self_cid.clone(),
				abort_error: None,
				metrics: MetricsAttribute::new(self.g, self.flags.get(CflagSlave)),
			}
		};
		let mut wc = Connector {
			addtoken: addtoken,
			g: self.g,
		};
		self.logical.handle_read_event(&mut w, &mut wc, &mut self.socketinfo, Interrupter::new(&self.token),
			self.g)?;
		//If writer aborts, close the connection
		if let Some(err) = w.writer.abort_error { fail!(cow!(F "{err}")); }
		Ok(())
	}
	//Read from connection and pass the read data to logical connection. The connection must be in connected
	//state (that is, connect_timeout must not be armed).
	//
	//This method takes care of repeating the read if there is data still lurking in dekryptize.
	//
	//On success, returns Ok(())
	//On error, returns Err(errmsg).
	fn _do_read_connected(&mut self, poll: &mut Poll, addtoken: &mut AddTokenCallback, id: ConnId) ->
		Result<(), Cow<'static, str>>
	{
		if self.link_state.is_connection_in_progress() {
			syslog!(CRITICAL "_do_read_connected(): Connection {conn} is not already connected",
				conn=self.connid);
		}
		//Try reading at least once. However, if the read leaves invisible buffered data,
		//try again until none remains.
		loop {
			self.__do_read_connected(poll, addtoken, id)?;
			if !self.data_in_dekryptize { break; }
		}
		Ok(())
	}
	//Try flushing data from send buffer into connection. The connection must be in connected
	//state (that is, connect_timeout must not be armed).
	//
	//This method takes care of calling handle_write_underflow() if the send buffer underflows.
	fn _do_write_connected(&mut self) -> Result<(), Cow<'static, str>>
	{
		if self.link_state.is_connection_in_progress() {
			syslog!(CRITICAL "_do_write_connected(): Connection {conn} is not already connected",
				conn=self.connid);
		}
		//If has_data() undergoes negative edge, call handle_write_underflow. The connection is connected
		//because this can only be called on connected connection.
		if self.write_buffer.has_data() && self._do_flush_send_buffer()? {
			self.with_writer_connected(|w, logical|logical.handle_write_underflow(w))?;
		}
		Ok(())
	}
	//Handle transition from half-connected to fully connected state.
	fn _transition_to_fully_connected(&mut self) -> Result<(), Cow<'static, str>>
	{
		//Print message for slaves.
		if self.flags.get(CflagSlave) {
			//Print to log because this is connection-specific.
			let target = self.link_state.get_current_target();
			let sp = match self.krypted.as_ref() {
				Some((k,_)) => Some(k.deref()),
				None => None
			};
			log!(INFO "Acquired connection to {target} ({sp})", sp=PrintSecurityParameters(sp));
		}
		//Transition to connnected state.
		self.link_state.connection_established();
		//Notify that socket has been connected. This can never be called on unconnected,
		//because the connection just established.
		let cid = self.self_cid.clone();
		let underflow = self.with_writer_connected(|w, logical|logical.notify_connected(cid, w))?;
		//notify_connected flushes buffers and thus can cause underflow to happen. Need to call
		//handle_write_underflow if underflow is set and primary slave buffer is empty. The later
		//is because the write can fail to write all the data to wire.
		if underflow && !self.write_buffer.has_data() {
			self.with_writer_connected(|w, logical|logical.handle_write_underflow(w))?;
		}
		Ok(())
	}
	fn _try_flush_buffer(wrbuf: &mut Buffer, fd: &FileDescriptor,  data: &[u8]) -> Result<(), IoError>
	{
		let mut itr = 0;
		if wrbuf.is_empty() {
			//Write buffer is empty, try to immediately flush.
			while itr < data.len() { match fd.write_const(&data[itr..]) {
				Ok(r) => itr += r,
				Err(ref e) if e.kind() == IoErrorKind::Interrupted => (),
				Err(ref e) if e.kind() == IoErrorKind::WouldBlock => break,
				Err(e) => fail!(e)
			}}
		}
		//Push remainder to buffer.
		if itr < data.len() {
			wrbuf.append(&data[itr..]).map_err(|_|IoError::new(IoErrorKind::Other, "Send-q overflow"))?;
		}
		Ok(())
	}
	//Handle write in half-connected state. Connection is half-connected if CflagTcpConnected is set, and
	//connect_timeout is armed.
	fn _do_write_half_connected(&mut self) -> Result<(), Cow<'static, str>>
	{
		//Try flushing send buffer. This is required to flush any previous output from kryptize, should
		//that be unable to be sent immediately.
		self._do_flush_send_buffer()?;
		//Half-connected state is only possible with kryptize.
		let just_connected;
		if let &mut Some((ref mut kryptize, _)) = &mut self.krypted {
			let metrics = MetricsAttribute::new(self.g, self.flags.get(CflagSlave));
			while kryptize.need_tx() {
				//Do not offer any data when handshaking.
				let tmp = kryptize.kryptize(&[]).map_err(|err|{
					metrics.error();
					cow!(F "{err}")
				})?;
				Self::_try_flush_buffer(&mut self.write_buffer, self.socket.borrow(), &tmp).
					map_err(|err|{
					cow!(F "TCP write error: {err}")
				})?;
				kryptize.return_buffer(tmp);
			}
			//If send buffer is empty, and kryptize is in abort, fail connection. The failure only
			//happens after flushing the second buffer, in order to actually send the alert.
			if self.write_buffer.is_empty() {
				if let Some(err) = kryptize.get_abort_reason() {
					metrics.error();
					fail!(cow!(F "Handshake error: {err}"));
				}
			}
			//If kryptize is now ready, connect.
			just_connected = kryptize.is_ready();
		} else {
			fail!(cow!("_do_write_half_connected() is only allowed with kryptize"));
		}
		if just_connected { self._transition_to_fully_connected()?; }
		Ok(())
	}
	//Handle read in half-connected state. Connection is half-connected if CflagTcpConnected is set, and
	//connect_timeout is armed.
	//
	//Note that this may end up in state where data_in_dekryptize is set and connection is in fully connected
	//state. In this case, interrupt is fired, and it should be serviced by calling _do_read_connected().
	fn _do_read_half_connected(&mut self) -> Result<(), Cow<'static, str>>
	{
		//Half-connected state is only possible with dekryptize.
		let just_connected;
		if let &mut Some((_, ref mut dekryptize)) = &mut self.krypted {
			let metrics = MetricsAttribute::new(self.g, self.flags.get(CflagSlave));
			let mut buf = [MaybeUninit::uninit();20480];
			let ret = handle_read_ret(self.socket.borrow().read_uninit2(&mut buf),
				&mut self.last_read_ts, &mut self.rshutdown, metrics).map_err(|err|{
				cow!(F "{err}")
			})?;
			if let Some(buf) = ret {
				if !dekryptize.need_rx() {
					metrics.error();
					fail!(cow!("Spurious receive from peer before handshake complete"));
				}
				//No need to check for any abort due to this: Remote aborts are assumed to
				//complete instantly, so the read should return Err(), triggering abort.
				if let Err(err) = dekryptize.dekryptize(buf) {
					metrics.error();
					fail!(cow!(F "{err}"));
				};
			}
			if self.rshutdown && !dekryptize.is_ready() {
				metrics.error();
				fail!(cow!("Connection closed by peer before handshake complete"));
			}
			//If dekryptize has output, set hidden input pending flag. Even if dekryptize should not
			//provode data if in handshake, it can happen that the peer immediately sends data after
			//completing the handshake. And that will cause output here.
			self.data_in_dekryptize = dekryptize.has_output();
			//If dekryptize is now ready, connect.
			just_connected = dekryptize.is_ready();
			//If connected, and there is data in receive buffer, fire IRQ so that data will get
			//processed.
			if just_connected && self.data_in_dekryptize { self.token.as_irq().irq(); }
		} else {
			fail!(cow!("_do_read_half_connected() is only allowed with dekryptize"));
		}
		if just_connected { self._transition_to_fully_connected()?; }
		Ok(())
	}
	//Try flushing data from send buffer into connection. This can be called in any state. Returns true if
	//send buffer is empty.
	fn _do_flush_send_buffer(&mut self) -> Result<bool, Cow<'static, str>>
	{
		if self.write_buffer.is_empty() { return Ok(true); }
		let metrics = MetricsAttribute::new(self.g, self.flags.get(CflagSlave));
		//Flush the output to wire, discarding the sent part.
		let amt = handle_write_ret(self.socket.borrow().write_const(self.write_buffer.as_slice()),
			&mut self.last_write_ts, metrics).map_err(|err|{
			cow!(F "Write error: {err}")
		})?;
		self.write_buffer.discard(amt);
		Ok(self.write_buffer.is_empty())
	}

	fn __new_master(socket: FileDescriptor, socketinfo: ConnectionInfo, krypt: impl Krypted,
		allocator: &mut TokenAllocatorPool, inner: LogicalConnection, g: ConfigT) ->
		Result<(ConnectionInner, Vec<AllocatedToken>, PreCID), Cow<'static, str>>
	{
		//Allocate token.
		let token = allocator.allocate().map_err(alloc_token_err)?;
		let tokens = vec![token.clone()];
		//Create krypted. This is the only possible reason for connect timeout on masters. So only set
		//the connect timeout (to proxy_timeout) if there is a krypted that is not ready immediately.
		//Note that absent krypted needs to treated as ready immediately.
		let (krypted, krypted_ready) = krypt.new_krypted(ListenerLoop::remote_irq_handle(&token));
		let timeout = if !krypted_ready { Some(g.with(|g|g.proxy_timeout)) } else { None };
		let (mut c, cid) = ConnectionInner::_init_self_default(socket, &socketinfo, token, inner,
			LinkState::new_master(timeout), g);
		c.krypted = krypted;					//Possibly krypted.
		Ok((c, tokens, cid))
	}
	//Reset the target of connect.
	fn __reset_target(&mut self, target: &KryptedTarget, poll: &mut Poll) -> Result<(), Cow<'static, str>>
	{
		let raddr = target.addr.borrow_addr();
		//__reset_target is not allowed in fully connected state.
		fail_if!(self.link_state.is_fully_connected(),
			format!("__reset_target: connection timeout for {conn} not armed", conn=self.connid));
		let conn = do_connect_to(target, &self.token).map_err(|e|format_connect_err(raddr, e))?;
		//Reset krypted, socket and socketinfo, as those have changed. Note that old poller must be removed
		//before changing the socket. Set address handle in order to correctly mark remote sockets as bad.
		if self.socket.borrow().as_raw_fd() >= 0 { self.socket.remove(poll); }
		self.socket = FdPoller::new(conn.socket, self.token.value());
		self.krypted = conn.krypted;
		self.socketinfo = ConnectionInfo::internal_to_socket("httpdemux2", raddr);
		if conn.tcp_ok && conn.krypt_ok {
			self._transition_to_fully_connected()?;
		} else {
			let handle = target.addr.borrow_status().clone();
			self.__reset_target_wait_tail(poll, raddr, handle,conn.tcp_ok)?
		}
		//Set the targe address, so any sucesses/failures get blamed to the right backend.
		self.logical.__set_output_address(raddr);
		Ok(())
	}
	fn __reset_target_wait_tail(&mut self, poll: &mut Poll, target: &SocketAddrEx, handle: AddressHandle,
		tcpstate: bool) -> Result<(), Cow<'static, str>>
	{
		self.link_state.set_low_level_status(tcpstate, self.connid);
		//Reset connect_name, as it has changed, and reset polls.
		self.__set_initial_wait(poll).map_err(|err|{
			cow!(F "Error connecting to {target}: {err}")
		})?;
		//Set connect_timeout_att, so individual attempts may be timed out.
		let delayed = self.g.with(|g|g.connect_attempt_timeout);
		self.link_state.attempt_connection(target, handle, delayed, self.connid);
		Ok(())
	}
	fn __new_slave2(targets: Arc<(Vec<KryptedTarget>, AtomicUsize)>, allocator: &mut TokenAllocatorPool,
		inner: LogicalConnection, g: ConfigT) ->
		Result<(ConnectionInner, Vec<AllocatedToken>, PreCID), Cow<'static, str>>
	{
		let token = allocator.allocate().map_err(alloc_token_err)?;
		let tokens = vec![token.clone()];
		let delayed = g.with(|g|g.connect_timeout);
		//No need to fire interrupt, because init() event can be used to initialize the first connection.
		//The connection is never connected even at low level, let alone handshaked. As consequence,
		//the address is always unknown.
		let socketinfo = ConnectionInfo::internal_to_socket("httpdemux2", &SocketAddrEx::Unknown);
		let (mut c, pcid) = ConnectionInner::_init_self_default(FileDescriptor::blank(), &socketinfo, token,
			inner, LinkState::new_slave(false, false, delayed), g);
		c.targets_set = targets;				//Targets.
		c.flags.set_imp(CflagSlave);				//Always slave.
		Ok((c, tokens, pcid))
	}
	pub fn new_with_info(poll: &mut Poll, allocator: &mut TokenAllocatorPool, socket: FileDescriptor,
		info: HttpBridgeProxyEx, g: ConfigT) ->
		Result<(ConnectionInner, Vec<AllocatedToken>, PreCID), Cow<'static, str>>
	{
		let info2 = ClientInfoIn {
			client_ip: Rc::new(cia_to_ip(info.source)),
			client_cn: info.client_cn.map(|x|Rc::new(x.to_owned())),
			client_spkihash: info.client_spkihash.map(|x|Rc::new(x.to_owned())),
			properties: info.properties.to_internal(),
			requested_cert: info.requested_cert,
		};
		//Conntrack always starts in proxy state.
		let inner = if info.is_h2 {
			fail_if!(!info.is_secure, "HTTP/2 is secure-only");
			increment_metric!(conn_https g);
			LogicalConnection::new_h2(info2, info.conn_sni.as_bytes(), g)
		} else {
			//This is never chained.
			let mode = if info.is_secure {
				increment_metric!(conn_https g);
				ConnectionModeH1::Secure
			} else {
				increment_metric!(conn_http g);
				ConnectionModeH1::Insecure
			};
			LogicalConnection::new_h1(mode, info2, g)
		};
		//This is only used for proxy, which does not matter as this has no proxy info. And trace flag
		//does not matter, as the final protocol is already known. Since this is accepted connection,
		//TCP is always connected. Note that this is only supported by internal links, so it never uses
		//TLS. So kryptize is always None.
		let pinfo = ConnectionInfo::internal_to_internal("", "");
		let (mut c, tokens, pcid) = ConnectionInner::__new_master(socket, pinfo, (), allocator, inner, g)?;
		
		//Fire interrupt so HTTP/2 SETTINGS are written.
		if info.is_h2 { c.token.as_irq().irq(); }
		//Wait for readability.
		c.socket.update(poll, IO_WAIT_READ|IO_WAIT_ONESHOT).map_err(|err|{
			cow!(F "Failed to register socket: {err}")
		})?;
		Ok((c, tokens, pcid))
	}
	pub fn new(poll: &mut Poll, allocator: &mut TokenAllocatorPool, socket: FileDescriptor,
		krypto: impl Krypted, socketinfo: ConnectionInfo, force_proxy: bool, g: ConfigT) ->
		Result<(ConnectionInner, Vec<AllocatedToken>, bool, PreCID), Cow<'static, str>>
	{
		//See if this connection should read PROXY info.
		let (use_proxy, use_chain, proxy_timeout) = g.with(|g|{
			let proxy_timeout = g.proxy_timeout;
			//If krypto has is_server_slave() set, this is always chained and never a proxy.
			if krypto.is_server_slave() { return Ok((false, true, proxy_timeout)); }
			//The use of proxy can be forced.
			let use_proxy = force_proxy || g.proxy_config.proxy_allowed(&socketinfo);
			//Force-proxy onnections are never in chained mode.
			let use_chain = !force_proxy && g.chained_mode;
			if !use_proxy && use_chain { return Err(cow!("Illegal source IP")); }
			//the use_chain condition is use_proxy && use_chain, but use_chain => use_proxy due
			//to the above check.
			Ok((use_proxy & !use_chain, use_chain, proxy_timeout))
		})?;
		//Ensure the socket is in nonblocking mode.
		socket.set_nonblock().map_err(|err|{
			cow!(F "Failed to set socket nonblocking: {err}")
		})?;
		//Possibly use chained mode.
		let mode = if use_chain { ConnectionModeH1::Chained } else { ConnectionModeH1::Insecure };
		let inner = match use_proxy {
			false => {
				let info = ClientInfoIn {
					client_ip: Rc::new(cia_to_ip(&socketinfo.src)),
					//Client CN and SPKIHASH are not available for plaintext connections.
					client_cn: None,
					client_spkihash: None,
					properties: ConnectionPropertiesI::unknown(),
					requested_cert: false,
				};
				increment_metric!(conn_http g);
				LogicalConnection::new_h1(mode, info, g)
			},
			true => LogicalConnection::new_proxy(proxy_timeout, force_proxy, g),
		};
		//Since this is accepted connection, TCP is always connected.
		let (mut c, tokens, cid) = ConnectionInner::__new_master(socket, socketinfo, krypto, allocator,
			inner, g)?;
		//Wait for readability.
		c.socket.update(poll, IO_WAIT_READ|IO_WAIT_ONESHOT).map_err(|err|{
			cow!(F "Failed to register socket: {err}")
		})?;
		//The address is valid if there is no proxy, otherwise there can be another address.
		Ok((c, tokens, !use_proxy, cid))
	}
	fn connect_tail2(targets: Arc<(Vec<KryptedTarget>, AtomicUsize)>, addtoken: &mut AddTokenCallback,
		params: ConnectionOutParams, tname: &str, g: ConfigT) -> Result<CID, Cow<'static, str>>
	{
		let target = format!("backend:{tname}");
		let socketinfo = ConnectionInfo::internal_to_internal("httpdemux2", &target);
		let inner = LogicalConnection::new_out(params, None, g);
		let (c, tokens, pcid) = ConnectionInner::__new_slave2(targets, addtoken.allocator(), inner, g)?;
		//No need to register this, as first connect attempt will be from init().
		let (conn, cid) = allocate_connection(c, pcid);
		addtoken.spawn(Box::new(conn), Cow::Owned(tokens), socketinfo);
		Ok(cid)
	}
	fn __set_initial_wait(&mut self, poll: &mut Poll) -> Result<(), Cow<'static, str>>
	{
		match self.link_state.status() {
			//If low-level link is not up, set write wait for it to come up.
			LinkStatus::Connecting => self.socket.update(poll, IO_WAIT_WRITE|IO_WAIT_ONESHOT).
				map_err(|err|{
				cow!(F "Failed to register socket: {err}")
			})?,
			//If low-level link is up, but handshaking is in progress, fire interrupt in order to
			//activate krypted.
			LinkStatus::Handshaking => self.token.as_irq().irq(),
			//__set_initial_wait should never be used in fully connected state.
			LinkStatus::Connected => syslog!(CRITICAL
				"__set_initial_wait() for {conn} in fuly connected state", conn=self.connid),
		}
		Ok(())
	}
	fn __with_internal_backend(addtoken: &mut AddTokenCallback, category: &str, lconn: LogicalConnection,
		g: ConfigT) -> Result<CID, Cow<'static, str>>
	{
		//Still need a token for this, and a dummy poller.
		let socketinfo = ConnectionInfo::local_to_internal(category);
		let token = addtoken.allocator().allocate().map_err(alloc_token_err)?;
		let tokens = vec![token.clone()];
		//The connection is always fully connected, so use new_fully_connected() as link status.
		let (mut c, pcid) = ConnectionInner::_init_self_default(FileDescriptor::blank(), &socketinfo, token,
			lconn, LinkState::new_fully_connected(), g);
		c.flags.set_imp(CflagSlave);		//Always slave.
		let (conn, cid) = allocate_connection(c, pcid);
		//Finally, spawn the connection and return handle.
		addtoken.spawn(Box::new(conn), Cow::Owned(tokens), socketinfo);
		Ok(cid)
	}
	fn static_file(addtoken: &mut AddTokenCallback, params: PathBuf, uncompressed: bool, is_error: bool,
		g: ConfigT) -> Result<CID, Cow<'static, str>>
	{
		Self::__with_internal_backend(addtoken, "Static file",
			LogicalConnection::new_static_file(params, uncompressed, is_error, g), g)
	}
	fn staticbackend(addtoken: &mut AddTokenCallback, params: StaticFileParams, g: ConfigT) ->
		Result<CID, Cow<'static, str>>
	{
		let category = params.category.clone();
		Self::__with_internal_backend(addtoken, &category, LogicalConnection::new_static(params, g), g)
	}
	fn _init_self_default(socket: FileDescriptor, socketinfo: &ConnectionInfo, token: AllocatedToken,
		inner: LogicalConnection, link_state: LinkState, g: ConfigT) -> (ConnectionInner, PreCID)
	{
		let pcid = PreCID::next();
		let c = ConnectionInner {
			krypted: None,
			data_in_dekryptize: false,
			targets_set: DUMMY_TARGETS_SET.with(|x|x.clone()),
			socket: FdPoller::new(socket, token.value()),
			socketinfo: socketinfo.clone(),
			token: token,
			link_state: link_state,
			attempts: 0,
			last_read_ts: PointInTime::now(),
			last_write_ts: PointInTime::now(),
			self_cid: CID::invalid(),
			write_buffer: Buffer::new_bounded(MAX_WRITE),
			recycled_read_buffer: Vec::new(),	//This will be grown as needed.
			connid: ConnId::unknown(),
			rshutdown: false,
			wshutdown: false,
			logical: inner,
			g: g,
			flags: Bitfield::new(),
		};
		(c, pcid)
	}
	fn get_transfer_timeout(&self) -> PointInTime
	{
		//Some backends might never timeout. Also, no timeouts until connected.
		if self.link_state.is_connection_in_progress() || self.logical.transmit_never_timeouts() {
			return PointInTime::from_now(TimeUnit::Seconds(86400));
		}
		let is_idle = self.logical.is_master_idle();
		let transfer_timeout = self.g.with(|g|{
			if is_idle { g.http_idle_timeout } else { g.transfer_timeout }
		});
		max(self.last_read_ts, self.last_write_ts).retrard(transfer_timeout)
	}
	fn rearm_socket(&mut self, poll: &mut Poll) -> Result<(), Cow<'static, str>>
	{
		//Half-connected states. kryptize.need_tx() should set need_w, and dekryptize.need_rx()
		//should set need_r.
		let (kneed_w, kneed_r) = if self.link_state.status() == LinkStatus::Handshaking {
			if let &Some((ref kryptize, ref dekryptize)) = &self.krypted {
				(kryptize.need_tx(), dekryptize.need_rx())
			} else {
				(false, false)
			}
		} else {
			(false, false)
		};

		//The read side is armed if:
		// - If fully connected and inner connection asks for it. Or
		// - Kryptize needs to receive data to make progress.
		let need_r = kneed_r ||
			self.link_state.status() == LinkStatus::Connected && self.logical.need_read();
		//The write side is armed if:
		// - Low-level connection is not up yet. Or
		// - Kryptize needs to transmit data to make progres. Or
		// - There is data in write buffer.
		//
		//There is no need to consider data_in_dekryptize, because interrupt is scheduled anyway if
		//needed.
		let need_w = kneed_w || self.link_state.status() == LinkStatus::Connecting ||
			self.write_buffer.has_data();
		let mut ready = IO_WAIT_ONESHOT;
		if need_r && !self.rshutdown { ready = ready | IO_WAIT_READ; }
		if !need_r && !self.rshutdown { ready = ready | IO_WAIT_HUP; }
		if need_w && !self.wshutdown { ready = ready | IO_WAIT_WRITE; }
		//Do not register dummy sockets, it will fail.
		if self.socket.borrow().as_raw_fd() < 0 { return Ok(()) }
		self.socket.update(poll, ready).map_err(|err|{
			cow!(F "Failed to update socket poller: {err}")
		})?;
		Ok(())
	}
	fn _try_next_address(&mut self, poll: &mut Poll, retry: bool) -> bool
	{
		let (ref targets, _) = self.targets_set.deref();
		//If this is not slave, there never is next address to try.
		if !self.flags.get(CflagSlave) || targets.len() == 0 { return false; }
		//Special case for single target. Try only once, never retry.
		if targets.len() == 1 {
			if retry { return false; }
			let target = targets[0].clone();
			let ret = self.__reset_target(&target, poll);
			if let Err(err) = ret.as_ref() { syslog!(WARNING "{err}"); }
			return ret.is_ok();
		}
		//These connections can only be initiated if target_set is set.
		let mut count = 0;
		loop {
			let (ref targets, ref tnum) = self.targets_set.deref();
			let tnum = tnum.fetch_add(1, Ordering::AcqRel);
			//If too many targets have alrady been probed, return a failure.
			if count >= targets.len() || self.attempts > 2 * targets.len() { break; }
			count += 1;
			self.attempts += 1;
			//Pull a target.
			let target = f_break!(choose_priority(targets.deref(), tnum).map(|t|t.clone()));
			match self.__reset_target(&target, poll) {
				Ok(()) => return true,		//Connect in progress.
				Err(err) => {
					syslog!(WARNING "{err}");
					continue;
				}
			};
		}
		//No address found. Return failure.
		false
	}
	fn _print_connect_failed(&self, err: impl Display, handshake: bool)
	{
		let kind = if handshake { "handshaking with" } else { "connecting to" };
		syslog!(WARNING "Error {kind} {target}: {err}", target=self.link_state.get_current_target());
	}
	fn _kill_retry_socket(&mut self, poll: &mut Poll, addtoken: &mut AddTokenCallback, e: impl Display,
		handshake: bool) -> Result<(), Cow<'static, str>>
	{
		//Immediately kill the socket and set ingore error, in order to avoid broken pipe.
		if self.socket.borrow().as_raw_fd() >= 0 { self.socket.remove(poll); }
		self.socket = FdPoller::new(FileDescriptor::blank(), self.token.value());
		addtoken.set_ignore_error();
		//This is definite failure.
		self.link_state.mark_current_target_bad();
		increment_metric!(error_connect self.g);
		self._print_connect_failed(e, handshake);
		fail_if!(!self._try_next_address(poll, true), cow!("Failed to connect to backend"));
		Ok(())
	}
	fn _do_so_error(&mut self, poll: &mut Poll, addtoken: &mut AddTokenCallback) ->
		Result<(), Cow<'static, str>>
	{
		let so_error = self.socket.borrow_mut().so_error();
		match so_error {
			Err(err) => if is_fatal_error(&err) {
				attribute_error_m(self.g.metrics_ref(), true);
				fail!(cow!(F "Failed to read SO_ERROR: {err}"));
			},
			Ok(Err(err)) => self._kill_retry_socket(poll, addtoken, err, false)?,
			Ok(_) => {
				//Disable nagle. Ignore failure.
				self.socket.borrow_mut().set_tcp_nodelay(true).ok();
				//If krypted is set, even if TCP established, the connection might still
				//be in not ready state. If connection is ready, transition to fully connected
				//state, otherwise transition to half-connected state.
				if self.krypted.as_ref().map(|(ref k,_)|k.is_ready()).unwrap_or(true) {
					self._transition_to_fully_connected()?;
				} else {
					self.link_state.lowlevel_up();
				}
			},
		}
		Ok(())
	}
	fn _try_handshake<E:Display>(&mut self, poll: &mut Poll, addtoken: &mut AddTokenCallback,
		f: impl FnOnce(&mut ConnectionInner) -> Result<(), E>) -> Result<(), Cow<'static, str>>
	{
		if let Err(e) = f(self) { self._kill_retry_socket(poll, addtoken, e, true)?; }
		Ok(())
	}
	pub fn handle_fault(&mut self, _: &mut Poll)
	{
		//No better error than Broken pipe.
		self.logical.set_error(cow!("Broken pipe"), false);
	}
	pub fn get_next_timed_event(&self) -> Option<PointInTime>
	{
		//Timeout connect attempts.
		if let Some(timeout) = self.link_state.next_timeout() { return Some(timeout); }
		//Otherwise this is connected, get timeout from inner connection or transfer timeout.
		combine_timestamps(&[self.logical.get_next_timed_event(), Some(self.get_transfer_timeout())], min)
	}
	pub fn handle_timed_event(&mut self, now: PointInTime, poll: &mut Poll) -> Result<(), Cow<'static, str>>
	{
		let handshaking = self.link_state.status() == LinkStatus::Handshaking;
		//If attempt has timed out, but the whole connection has not, pick next target and retry. Do not
		//need to care about the first attempt, as it is triggered by connection init, and not need to
		//care about resetting connect_timeout_att, as it is done by _try_next_address().
		if self.link_state.is_attempt_timed_out(now) {
			//Handle masters specially: This can only come from Krypted failure.
			fail_if!(!self.flags.get(CflagSlave), "Crypto handshake timed out");
			//This attempt timed out. Mark this as bad to avoid retries.
			self.link_state.mark_current_target_bad();
			self._print_connect_failed("Timed out", handshaking);
			fail_if!(!self._try_next_address(poll, true), cow!("Failed to connect to backend"));
		}
		//If there is pending connect timeout, handle that.
		if self.link_state.is_connection_timed_out(now) {
			//Handle masters specially: This can only come from Krypted failure.
			fail_if!(!self.flags.get(CflagSlave), "Crypto handshake timed out");
			//Connect timeout is a failure.
			self.link_state.mark_current_target_bad();
			self._print_connect_failed("Timed out", handshaking);
			fail!(cow!("Failed to connect to backend: Timed out"));
		}
		//If connecting, do not handle any other timeouts. In practicular, do not call any
		//inner timeouts. Otherwise do transfer timeout and logical connection timeout. The check guards
		//against calling handle_timed_event on anything unconnected.
		if self.link_state.is_connection_in_progress() { return Ok(()); }
		if now >= self.get_transfer_timeout() {
			//If in master idle, use special message.
			let is_idle = self.logical.is_master_idle();
			let is_connecting = self.link_state.is_connection_in_progress();
			let msg = if is_idle && !is_connecting {
				//For HTTP/2, try send GOAWAY.
				self.with_writer_connected(|w, logical|logical.send_h2_idle_goaway(w))?;
				"Closed idle connection"
			} else {
				"Connection timed out"
			};
			fail!(Cow::Borrowed(msg));
		}
		self.with_writer_connected(|w, logical|logical.handle_timed_event(now, w))
	}
	pub fn remove_pollers(&mut self, poll: &mut Poll)
	{
		self.socket.remove(poll);
	}
	//Handle socket init event.
	pub fn handle_init(&mut self, id: ConnId, _: &mut Poll) -> Result<(), Cow<'static, str>>
	{
		//If there is pending connect, fire IRQ so handle_event happens immediately. Do this instead
		//starting connection here, as we can not print proper messages here.
		if self.link_state.is_connection_in_progress() && self.socket.borrow().as_raw_fd() < 0 {
			self.token.as_irq().irq();
		}
		//If fully connected, initialize the connection immediately. This should happen
		//immediately if the previous loop initializes the connection to ready.
		if self.link_state.is_fully_connected() {
			let cid = self.self_cid.clone();
			//This can not be called on unconnected because this branch is specifically the case when
			//the connection is already connected.
			self.with_writer_connected(|w, logical|logical.notify_connected(cid, w))?;
		}
		//Record the connid.
		self.connid = id.to_owned();
		Ok(())
	}
	//Handle quit event.
	pub fn handle_quit(&mut self) -> Result<(), Cow<'static, str>>
	{
		//If connected (quits only pertain to masters, so this should be always true), forward to logical
		//connection.
		if self.link_state.is_fully_connected() {
			self.with_writer_connected(|w, logical|logical.notify_quit(w.irq))?;
		}
		Ok(())
	}
	//Handle a socket event (return Err(()) if connection should be killed). Might panic.
	pub fn handle_event(&mut self, poll: &mut Poll, tok: usize, kind: u8, id: ConnId,
		addtoken: &mut AddTokenCallback) -> Result<(), Cow<'static, str>>
	{
		//If error is set, fail the connection immediately.
		self.logical.get_error()?;
		//Ignore events dispatched to the wrong connection.
		if tok != self.token.value() { return Ok(()); }
		//If there is pending connect, but no socket, start first connect. This starts connect_timeout_att.
		//If this can not be performed, return special error about there being no backends available.
		if self.link_state.is_connection_in_progress() && self.socket.borrow().as_raw_fd() < 0 {
			fail_if!(!self._try_next_address(poll, false),
				cow!("Failed to connect to backend: No backends available"));
		}
		//Handle Read/HUP events by trying to read.
		if kind & (IO_WAIT_READ|IO_WAIT_HUP) != 0 { match self.link_state.status() {
			//There should not be read events before low-level link is up.
			LinkStatus::Connecting => (),
			//Read event while handshaking means that kryptize is waiting for data and now can make
			//progress. Drive it. If this fails, try next.
			LinkStatus::Handshaking => 
				self._try_handshake(poll, addtoken, |this|this._do_read_half_connected())?,
			//Read event when fully connected means that data may have arrived.
			LinkStatus::Connected => self._do_read_connected(poll, addtoken, id)?,
		}}
		//Handle Write events by either doing so_error or trying to write.
		if kind & IO_WAIT_WRITE != 0 { match self.link_state.status() {
			//Write events when connecting signify that the low-level link has came up, so get the
			//status of low-level link.
			LinkStatus::Connecting => self._do_so_error(poll, addtoken)?,
			//Write events when handshaking mean that kryptize can now transmit data that it could not
			//before. Try to drive kryptize. If this fails, try next.
			LinkStatus::Handshaking => 
				self._try_handshake(poll, addtoken, |this|this._do_write_half_connected())?,
			//Write events when fully connected get processed as usual. Try to find data to transmit.
			LinkStatus::Connected => self._do_write_connected()?,
		}}
		//Handle interrupt events specially.
		if kind & IO_WAIT_INTR != 0 { match self.link_state.status() {
			//There should not be any interrupts in Connecting state.
			LinkStatus::Connecting => (),
			//Interrupt in handshaking can happen if kryptize wakes up and wants to transmit.
			//Try to drive kryptize. Writes to non-ready socket are harmless, as the socket is
			//non-blocking. If this fails, try next.
			LinkStatus::Handshaking =>
				self._try_handshake(poll, addtoken, |this|this._do_write_half_connected())?,
			//Transition to fully connected state can leave hidden data in dekryptize. Flush
			//such data. If that happens, _do_read_half_connected() will schedule an interrupt
			//to happen for that purpose. In any case, do normal request event.
			LinkStatus::Connected => {
				if self.data_in_dekryptize { self._do_read_connected(poll, addtoken, id)?; }
				self.with_writer_connected(|w, logical|logical.handle_request_event(w))?;
			},
		}}
		self.rearm_socket(poll)?;
		Ok(())
	}
	pub fn write_buffer_has_data(&self) -> bool { self.write_buffer.has_data() }
	pub fn write_backward_headers(&mut self, block: &mut HeaderBlock, req: &RequestInfo, backend: ConnId,
		backend_name: BackendName) -> Result<(), ()>
	{
		//This can not be called on unconnected because this can only be triggered by a slave on master,
		//and unconnected master never has any slaves.
		self.with_writer_connected(|w, logical|{
			logical.write_backward_headers(w, block, req, backend, backend_name)
		})
	}
	pub fn write_backward_trailers(&mut self, block: &mut HeaderBlock, req: &RequestInfo)
	{
		//This can not be called on unconnected because this can only be triggered by a slave on master,
		//and unconnected master never has any slaves.
		self.with_writer_connected(|w, logical|logical.write_backward_trailers(w, block, req))
	}
	pub fn write_backward_data(&mut self, block: &[u8], req: &RequestInfo) -> Result<usize, ()>
	{
		//This can not be called on unconnected because this can only be triggered by a slave on master,
		//and unconnected master never has any slaves.
		self.with_writer_connected(|w, logical|logical.write_backward_data(w, block, req))
	}
	pub(crate) fn write_backward_error(&mut self, req: &RequestInfo, status: InternalResponse)
	{
		//This can not be called on unconnected because this can only be triggered by a slave on master,
		//and unconnected master never has any slaves.
		self.with_writer_connected(|w, logical|logical.write_backward_error(w, req, status))
	}
	pub fn backward_has_data(&mut self, req: &RequestInfo)
	{
		self.logical.backward_has_data(req, Interrupter::new(&self.token))
	}
	pub fn forward_unblock(&mut self, req: &RequestInfo)
	{
		self.logical.forward_unblock(req, Interrupter::new(&self.token))
	}
	pub fn write_forward_headers(&mut self, blk: &mut HeaderBlock, req: &RequestInfo,
		hostdata: HostRoutingDataRef, parent: ConnId) -> Result<(), BackendError>
	{
		with_maybe_connected!(self |w, logical|logical.write_forward_headers(w, blk, req, hostdata, parent))
	}
	pub fn write_forward_trailers(&mut self, block: &mut HeaderBlock, ) ->
		Result<(), BackendError>
	{
		with_maybe_connected!(self |w, logical|logical.write_forward_trailers(w, block))
	}
	pub fn write_forward_data(&mut self, block: &[u8]) -> Result<bool, BackendError>
	{
		with_maybe_connected!(self |w, logical|logical.write_forward_data(w, block))
	}
	pub fn request_data<S:RequestWriteReturn>(&mut self, sink: &mut S) -> bool
	{
		self.logical.request_data(sink, Interrupter::new(&self.token))
	}
	pub fn write_forward_error(&mut self, error: &str)
	{
		self.logical.write_forward_error(error, Interrupter::new(&self.token));
	}
	pub fn get_raw_fd(&self) -> i32 { self.socket.borrow().as_raw_fd() }
	pub fn switch_to(&self) -> Option<ConnId> { set_current_task(Some(self.connid)) }
	pub fn set_self_cid(&mut self, cid: &CID) { self.self_cid = cid.clone(); }
	pub fn get_config(&self) -> ConfigT { self.g }
}

impl Drop for ConnectionInner
{
	fn drop(&mut self)
	{
		//This connection should be withdrawn from freelist of connections, as it has died.
		ConnectionFreeList::delete_by_cid(&self.self_cid);

		//If master connection dies, all its slave connections need to be notified with
		//write_forward_error so they can either terminate if they are in unclean state, or (eventually)
		//transition back to idle.
		let mut slavelist = self.logical.get_slave_list();
		for cid in slavelist.drain(..) {
			if cid.is_invalid() { continue; }	//Ignore invalid CIDs.
			//Panicing is not allowed in this context, so use force_enter_task().
			cid.force_enter_task(|inner|{
				let r = inner.logical.notify_slave_that_master_died();
				increment_metric!(dropped_masters self.g);
				//If requested, tear down the connection.
				if let Err(err) = r {
					inner.logical.set_error(cow!(F "{err}"), true);
					inner.token.as_irq().irq();
				}
			});
		}

		//INVALID_CID is never on the map. Panicing is not allowed in this context, so use
		//force_enter_task().
		let (cid, sid) = self.logical.get_master_of_slave();
		cid.force_enter_task(|inner|{
			let connected = self.link_state.is_fully_connected();
			increment_metric!(dropped_slaves self.g);
			inner.logical.notify_master_that_slave_died(self.self_cid.clone(), sid, connected,
				Interrupter::new(&inner.token));
		});
	}
}
