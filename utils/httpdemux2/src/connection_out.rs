use crate::ConfigT;
use crate::bitmask::Bitfield;
use crate::connection::BackendName;
use crate::connection::combine_timestamps;
use crate::connection_midlevel::ConnectionFreeList;
use crate::connection_midlevel::ForwardInterface;
use crate::connection_midlevel::ForwardInterfaceError;
use crate::connection_midlevel::Interrupter;
use crate::connection_lowlevel::CID;
use crate::connection_midlevel::ReaderWriterS;
use crate::connection_midlevel::SinkError;
use crate::connection_midlevel::WriterS;
use crate::connection_midlevel::WriterS2;
use crate::headerblock::HeaderBlock;
use crate::headerblock::HeaderError;
use crate::headerblock::HeaderKind;
use crate::headerblock::OwnedTrailerBlock;
use crate::headerblock::serialize_headers;
use crate::headerblock::serialize_trailers;
use crate::headerblock::ShdrProtocol;
use crate::headerblock::ShdrReason;
use crate::headerblock::ShdrVersion;
use crate::http::RequestInfo;
use crate::http::InternalResponse;
use crate::http::RequestWriteReturn;
use crate::http::TransactionStateOut;
use crate::metrics_dp::OutgoingConnectionState;
use crate::routing_dp::HostRoutingDataRef;
use crate::utils::Buffer;
use crate::utils::RcTextString;
use btls_aux_http::http1::Element;
use btls_aux_http::http1::ElementSink;
use btls_aux_http::http1::HeaderEncoder;
use btls_aux_http::http1::IoOrStreamError;
use btls_aux_http::http1::Stream;
use btls_aux_http::status::BAD_GATEWAY;
use btls_aux_memory::PrintBuffer;
use btls_aux_time::PointInTime;
use btls_aux_time::Timer;
use btls_daemon_helper_lib::AddressHandle;
use btls_daemon_helper_lib::cow;
use btls_util_logging::ConnId;
use btls_util_logging::log;
use btls_util_logging::syslog;
use std::borrow::Cow;
use std::cmp::min;
use std::fmt::Write;
use std::mem::MaybeUninit;
use std::mem::replace;
use std::sync::Arc;


macro_rules! warn_backend
{
	($cat:expr, $($msg:tt)*) => {
		syslog!(WARNING "Error on backend {cat}: {args}", cat=$cat, args=format_args!($($msg)*))
	}
}

const MAX_HBLOCK_SIZE: usize = 16384;

fn good_backend_status(code: u16) -> bool
{
	//Status code 501 and 505 are exceptional in that they are classed as good codes.
	if code == 501 || code == 505 { return true; }
	//Status code 421 is exceptional in that they are classed as bad.
	if code == 421 { return false; }
	//Good status codes include 1xx, 2xx, 3xx, 4xx. 5xx is bad.
	code >= 100 && code <= 499 
}

///Error from backend.
#[derive(Debug)]
pub enum BackendError
{
	///The headers sent are bad.
	BadHeadersReceived(HeaderError),
	///Connection has been lost.
	ConnectionLost,
	///This connection is not supported as backend.
	UnsupportedBackend,
	//Upgrade not supported.
	UpgradeNotSupported,
	///File not found.
	FileNotFound,
	///Only safe methods supported.
	SafeMethodsOnly,
	///Requested range not satisfiable.
	BadRangeRequested(u64, &'static str),
	///Special: Options request.
	OptionsRequest,
	///Internal error.
	InternalError(&'static str),
}

fn set_backend_status(ahandle: &mut Option<AddressHandle>, status: bool)
{
	if let Some(ahandle) = ahandle.as_mut() { ahandle.set_status(status); }
}

define_bitfield_type!(SinkFlagsTag, u8);
//The connection is performing a keepalive.
define_bitfield_bit!(SflagInKeepalive, SinkFlagsTag, 0);
//Connection is blocked (does not read).
define_bitfield_bit!(SflagBlocked, SinkFlagsTag, 1);
//Connection is idle.
define_bitfield_bit!(SflagIdle, SinkFlagsTag, 2);
//There is pending request that has not received any response.
define_bitfield_bit!(SflagPendingReq, SinkFlagsTag, 4);

struct Sink
{
	//The headers block being decoded.
	headers: HeaderBlock,
	//The secondary buffer, outgoing to incoming (the primary buffer lives on master side).
	o2i_secondary: Buffer,
	//Saved trailers, in case those are received while secondary buffer still has data.
	saved_trailers: Option<OwnedTrailerBlock>,
	//The current transaction. None means no transaction is happening, or this is keepalive.
	transaction: Option<TransactionStateOut>,
	//Address handle.
	ahandle: Option<AddressHandle>,
	//Failure code.
	req_failed_code: u16,
	flags: Bitfield<SinkFlagsTag>,
}

impl Sink
{
	//do_backward_fault is called if outgoing connection gets a fault.
	//
	// - In case final headers have not been sent, this triggers 502 response.
	// - In case final headers have been sent, the stream is torn down (the entiere connection for HTTP/1.x,
	//   just the offending stream for HTTP/2)
	fn do_backward_fault(&mut self, msg: Cow<'static, str>) -> Result<(), SinkError>
	{
		//Whatever this is, something is seriously wrong with the backend.
		set_backend_status(&mut self.ahandle, false);
		if let Some(transaction) = self.transaction.as_mut() {
			//The error code is always BAD_GATEWAY, since these are gateway errors, as they are received
			//from the backend. If this signals an error, terminate connection. Use the error variant
			//of BAD_GATEWAY, because reaching here means something has seriously gone wrong with the
			//backend.
			return transaction.do_backward_error(InternalResponse::err(BAD_GATEWAY, msg));
		} else {
			fail_if!(!self.flags.get(SflagInKeepalive), SinkError::UnexpectedData);
			//No open transaction. Assume this is fault in keepalive, tear down the connection.
			fail!(SinkError::HttpFaultInKeepalive(msg.into_owned()));
		}
	}
	//do_backward_headers is called when a header block is received from outgoing connection.
	//
	// - The block should be forwarded to incoming connection, if any.
	// - In any case, the headers are in self.headers
	fn do_backward_headers(&mut self, has_body: bool, selfid: ConnId) -> Result<(), SinkError>
	{
		if let Some(transaction) = self.transaction.as_mut() {
			let code = self.headers.get_status();
			let good = good_backend_status(code);
			//If bad status code, set RequestFailed, so connection gets torn down instead of being
			//reused. This lets one switch backends in case there is multiple.
			if !good { self.req_failed_code = code; }
			set_backend_status(&mut self.ahandle, good);
			let r = transaction.do_backward_headers(&mut self.headers, has_body, selfid,
				BackendName::Handle(self.ahandle.as_ref()));
			self.headers.clear();
			r
		} else {
			fail_if!(!self.flags.get(SflagInKeepalive), SinkError::UnexpectedData);
			//Check status code, and that there is no connection: close. Set good/bad according to
			//status (2xx is good, all others are bad).
			let status = self.headers.get_status();
			set_backend_status(&mut self.ahandle, status / 100 == 2);
			fail_if!(status / 100 != 2, SinkError::BadKeepaliveStatus(status));
			fail_if!(self.headers.is_close(), SinkError::KeepaliveConnectionClose);
			self.headers.clear();
			//If there is no body, set the in_keepalive to false so connection gets set idle.
			if !has_body { self.flags.clear_imp(SflagInKeepalive); }
			Ok(())
		}
	}
	//do_backward_trailers is called when a (trivial) trailer block is received from outgoing connection.
	fn do_backward_trailers(&mut self, _: &mut impl WriterS) -> Result<(), SinkError>
	{
		//Trailers never change status.
		if let Some(transaction) = self.transaction.as_mut() {
			//If o2i_secondary has data, save the trailer block for later send in order to not misorder
			//data.
			if self.o2i_secondary.has_data() {
				self.saved_trailers = Some(OwnedTrailerBlock::new(&self.headers));
				return Ok(());
			}
			let r = transaction.do_backward_trailers(&mut self.headers);
			self.headers.clear();
			//Set blocked, because we can not start processing another response before getting another
			//request.
			self.flags.set_imp(SflagBlocked);
			r
		} else {
			fail_if!(!self.flags.get(SflagInKeepalive), SinkError::UnexpectedData);
			//Just ignore the trailers. Unset the in keepalive flag, so connetion gets set to idle.
			self.flags.clear_imp(SflagInKeepalive);
			Ok(())
		}
	}
	//do_backward_data is called when a data block is received from outgoing connection.
	fn do_backward_data(&mut self, d: &[u8]) -> Result<(), SinkError>
	{
		let ostatus = self.o2i_secondary.has_data();

		if let Some(transaction) = self.transaction.as_mut() {
			//If o2i_secondary has data, or write buffer has data, throw all data into secondary
			//buffer. This can never fail.
			if ostatus || transaction.is_busy() {
				self.o2i_secondary.append(d).ok();
			} else {
				//Otherwise send the data to master, and buffer the rest. If this fails with an
				//error, tear down the connection as fault is on this end. Retry any short write
				//if there is nothing in primary transmit buffer. Do not write anymore if one hits
				//0 byte write. This can presumably only happen if one hits the flow control limits
				//in HTTP/2, and then one needs to wait for flow control to be restored.
				let mut amt = 0;
				while !transaction.is_busy() && amt < d.len() {
					let namt = transaction.do_backward_data(&d[amt..])?;
					if namt == 0 { break; }
					amt += namt;
				}
				self.o2i_secondary.append(&d[amt..]).ok();
			}
			//If secondary buffer has positive edge, notify master that this is ready for pull.
			//Do not block here, because there may be stuff in flight.
			if !ostatus && self.o2i_secondary.has_data() { transaction.do_notify_ready(); }
			Ok(())
		} else {
			//If this happens outside transaction and keepalive, that is BAD.
			if !self.flags.get(SflagInKeepalive) {
				set_backend_status(&mut self.ahandle, false);
				fail!(SinkError::UnexpectedData);
			}
			//Just ignore keepalive data.
			Ok(())
		}
	}
	fn do_start_message(&mut self) -> Result<(), SinkError>
	{
		//Clear the headers for the incoming block.
		self.headers.clear();
		Ok(())
	}
	fn sink(&mut self, elem: Element, sink: &mut impl WriterS)
	{
		//Check aborted flag on sink. It can be set by failures in processing various events. If the
		//flag gets set, we want to tear down the connection as soon as possible.
		if sink.aborted() { return; }
		let selfid = sink.get_id();
		//If SflagIdle is set, backend sent unsolicted reply. Can not handle that.
		if self.flags.get(SflagIdle) { return sink.abort(SinkError::UnsolictedReply); }
		if self.flags.get(SflagBlocked) { return sink.abort(SinkError::UnexpectedData); }
		let r: Result<(), SinkError> = match elem {
			//In case of abort, send forward fault, but do no more, as connection will be torn down
			//with more descriptive error. Note that with interim headers, say there is a body, as
			//there will be more stuff coming.
			Element::Abort(err) => if err.is_connection_lost() {
				self.do_backward_fault(cow!("Connection to backend lost"))
			} else {
				self.do_backward_fault(cow!(F "Connection error: {err}"))
			},
			Element::ContentLength(l) => Ok(self.headers.set_content_length(l)),
			Element::Data(d) => self.do_backward_data(d),
			Element::EndOfHeaders => self.do_backward_headers(true, selfid),
			Element::EndOfHeadersInterim => self.do_backward_headers(true, selfid),
			Element::EndOfMessage if self.transaction.as_ref().map(|x|x.rx_phase_is_headers()).
				unwrap_or(false) => self.do_backward_headers(false, selfid),
			Element::EndOfMessage => self.do_backward_trailers(sink),
			Element::HeaderStd(n,v) => {
				//Ignore dangerous headers.
				if !n.is_dangerous_response() { self.headers.set_header_std(n, v) }
				Ok(())
			},
			//All dangerous response headers use HeaderStd event.
			Element::Header(n,v) => Ok(self.headers.set_header(n, v)),
			Element::HeadersFaulty(err) => self.do_backward_fault(cow!(F "{err}")),
			Element::Protocol(p) => Ok(self.headers.set_special(ShdrProtocol, p)),
			Element::Reason(r) => Ok(self.headers.set_special(ShdrReason, r)),
			Element::StartOfMessage => {
				//No longer waiting for response, as it has started.
				self.flags.clear_imp(SflagPendingReq);
				self.do_start_message()
			},
			Element::Status(s) => {
				//Check that the received status code is valid.
				if s < 100 || s > 599 {
					self.headers.set_overflow(HeaderError::IllegalStatus(s));
				}
				Ok(self.headers.set_status(s))
			},
			Element::TrailerStd(n,v) => {
				//Ignore dangerous headers. And set trailers as headers.
				if !n.is_dangerous_response() { self.headers.set_header_std(n, v); }
				Ok(())
			},
			//All dangerous response headers use HeaderStd event.
			Element::Trailer(n,v) => Ok(self.headers.set_header(n, v)),
			Element::Version(v) => Ok(self.headers.set_special(ShdrVersion, v)),
			//Stuff only used in requests.
			Element::Authority(_)|Element::Method(_)|Element::Path(_)|Element::Query(_)|
				Element::Scheme(_) =>
				self.do_backward_fault(cow!("Received request header in response")),
			_ => Ok(()),	//Ignore unknown events.
		};
		//If error happened, set abort reason. This also sets the aborted flag.
		if let Err(e) = r { sink.abort(e); }
	}
}

struct SinkWrapper<'a,'c,W:WriterS>(&'c mut Sink, &'a mut W);

impl<'a,'c,W:WriterS> ElementSink for SinkWrapper<'a,'c,W>
{
	fn sink(&mut self, elem: Element)
	{
		self.0.sink(elem, self.1)
	}
	fn protocol_supported(&self, protocol: &[u8]) -> bool
	{
		//The only supported protocol is websocket.
		protocol == b"websocket"
	}
}


define_bitfield_type!(ConnectionFlagsTag, u8);
define_bitfield_bit!(CflagChunked, ConnectionFlagsTag, 0);
define_bitfield_bit!(CflagRshutdown, ConnectionFlagsTag, 1);
define_bitfield_bit!(CflagWshutdown, ConnectionFlagsTag, 2);
define_bitfield_bit!(CflagWshutdownAck, ConnectionFlagsTag, 3);
define_bitfield_bit!(CflagUseOnce, ConnectionFlagsTag, 4);

pub struct ConnectionParameters
{
	pub category: RcTextString,
	pub keepalive: Option<Arc<(String, String)>>,
	pub use_once: bool,
}


pub struct Connection
{
	recv: Stream,
	sink: Sink,
	i2o_secondary: Buffer,
	self_cid: CID,
	keepalive_timer: Timer,
	shutdown_timer: Timer,
	self_category: RcTextString,
	conntrack: OutgoingConnectionState,
	keepalive: Option<Arc<(String, String)>>,
	g: ConfigT,
	flags: Bitfield<ConnectionFlagsTag>,
}

impl Connection
{
	pub fn new(p: ConnectionParameters, ahandle: Option<AddressHandle>, g: ConfigT) -> Connection
	{
		let mut recv = Stream::new(b"http");
		recv.set_use_std_flag();	//Use standard headers.
		recv.set_reply(true);		//Process replies.
		Connection {
			recv: recv,
			i2o_secondary: Buffer::new(),
			//Initially keepalive and shutdown timers are disabled, because at first there is connect
			//delay. Only after connect completes without request does the timers be set to idle.
			keepalive_timer: Timer::new(),
			shutdown_timer: Timer::new(),
			self_cid: CID::invalid(),
			self_category: p.category,
			conntrack: OutgoingConnectionState::new_connecting(g),
			keepalive: p.keepalive,
			g: g,
			flags: Bitfield::new().force(CflagUseOnce, p.use_once),
			sink: Sink {
				ahandle: ahandle,
				headers: HeaderBlock::new(),
				o2i_secondary: Buffer::new(),
				//Do not initially set SflagIdle, only after connected.
				flags: Bitfield::new().set(SflagBlocked),
				saved_trailers: None,
				transaction: None,
				req_failed_code: 0,
			}
		}
	}
	pub fn __set_address_handle(&mut self, handle: AddressHandle) { self.sink.ahandle = Some(handle) }
	pub fn get_next_timed_event(&self) -> Option<PointInTime>
	{
		combine_timestamps(&[self.keepalive_timer.expires(), self.shutdown_timer.expires()], min)
	}
	pub fn need_read(&self) -> bool
	{
		//If in idle, do attempt to read, in order to detect backend closing the connection.
		if self.sink.flags.get(SflagIdle) { return true; }
		//In addition to explcit blocked flag, block if secondary buffer outgoing->incoming has data in it.
		!self.sink.flags.get(SflagBlocked) && !self.sink.o2i_secondary.has_data()
	}
	pub fn handle_write_underflow(&mut self, w: &mut impl WriterS2) -> Result<(), Cow<'static, str>>
	{
		//Only need to do this if secondary buffer is not empty. If it is empty, there is nothing to do,
		//as master will write to buffers again when more data arrives.
		if self.i2o_secondary.has_data() {
			//Swap the primary buffer with secondary buffer.
			w.swap_buffers(&mut self.i2o_secondary);
			//Call to unblock the master connection, now that secondary buffer is empty.
			if let Some(transaction) = self.sink.transaction.as_mut() {
				transaction.unblock_master();
			}
		}
		//This may have changed conditions in check_eof_close.
		self.check_eof_close(w, false).map_err(|err|{
			cow!(F "{err}")
		})?;
		Ok(())
	}
	pub fn handle_read_event(&mut self, w: &mut impl ReaderWriterS) -> Result<(), Cow<'static, str>>
	{
		let r = {
			let (source, sink) = w.split();
			let mut sink = SinkWrapper(&mut self.sink, sink);
			self.recv.push(source, &mut sink)
		};
		match r {
			Ok(true) => (),
			//EOF when in idle is bad: The backend keepalive is not long enough, which is fundamentially
			//racy. This does not happen if we close the connection, because the connection just gets
			//immediately severed.
			Ok(false) if self.sink.flags.get(SflagIdle) => {
				let expires = self.keepalive_timer.expires();
				let expires = expires.and_then(|exp|exp.time_to());
				let expires = expires.map(|exp|exp.as_millis()).unwrap_or(0);
				warn_backend!(&self.self_category,
					"Backend closed idle connection, this is racy (left={expires}ms)!");
				fail!(cow!("Connection keepalive failed"))
			},
			//EOF when waiting for reply is special: That is connection error.
			Ok(false) if self.sink.flags.get(SflagPendingReq) => {
				warn_backend!(&self.self_category, "No reply");
				fail!(cow!("Connection closed without reply"))
			},
			//Tear down connection on EOF when it reaches idle.
			Ok(false) => self.check_eof_close(w.as_writer(), true).map_err(|err|{
				cow!(F "{err}")
			})?,
			Err(IoOrStreamError::Io(err)) => {
				warn_backend!(&self.self_category, "TCP read: {err}");
				fail!(cow!(F "Read Error: {err}"))
			},
			//HTTP errors can not happen with upgraded streams.
			Err(IoOrStreamError::Stream(err)) => {
				warn_backend!(&self.self_category, "HTTP read: {err}");
				fail!(cow!(F "Error in HTTP stream: {err}"))
			},
		}
		//If incoming_end is set and response headers contain connection close, behave like EOF was
		//received. This is because connection is supposed to end after connection close response.
		let is_eof = self.sink.transaction.as_ref().map(|x|{
			x.is_rx_end_and_close()
		}).unwrap_or(false);
		self.check_eof_close(w.as_writer(), is_eof).map_err(|err|{
			cow!(F "{err}")
		})?;
		Ok(())
	}
	pub fn handle_timed_event(&mut self, now: PointInTime, w: &mut impl WriterS) ->
		Result<(), Cow<'static, str>>
	{
		if self.keepalive_timer.triggered(now) {
			self.set_timers_for_keepalive();
			self.sink.flags.set_imp(SflagInKeepalive);
			ConnectionFreeList::delete_by_cid(&self.self_cid);
			//Keepalive on * is special.
			if let Some(&(ref kauth, ref kpath)) = self.keepalive.as_deref() {
				//kpath * is special, it performs OPTIONS instead of HEAD.
				let option = kpath == "*";
				//Perform keepalive. The connection must be at idle because keepalive timer
				//does not run otherwise. Also set x-forwarded-secure to avoid 403 from insecure
				//request in case this is chained to something that only supports HTTPS.
				let p1 = if option { &b"OPTIONS "[..] } else { &b"HEAD "[..] };
				static P2: &'static [u8] = b" HTTP/1.1\r\nx-forwarded-proto:https\r\n\
					x-forwarded-secure:1\r\nhost:";
				static P3: &'static [u8] = b"\r\n\r\n";
				w.write(&[p1, kpath.as_bytes(), P2, kauth.as_bytes(), P3]).map_err(|err|{
					cow!(F "Failed to send keepalive request: {err}")
				})?;
				self.recv.set_reply(!option);	//OPTIONS may have body.
				self.sink.flags.clear_imp(SflagBlocked);
				self.sink.flags.set_imp(SflagPendingReq);	//Waiting for reply.
				w.interrupt();	//Ensure unblock gets noted.
			} else {
				fail!(cow!("Keepalive URL not available"));
			}
		}
		//The connection is at idle. Shut it down immediately.
		fail_if!(self.shutdown_timer.triggered(now), cow!("Connection shut down as unneeded"));
		Ok(())
	}
	pub fn handle_request_event(&mut self, _w: &mut impl WriterS) -> Result<(), Cow<'static, str>>
	{
		Ok(())	//No request events are performed that would be interesting at this level.
	}
	fn set_timers_for_idle(&mut self)
	{
		//In idle, the keepalive timer is armed. And if shutdown timer was not armed, it is armed. The
		//reason for not rearming shutdown timer is that if coming from keepalive, the shutdown timer should
		//not be changed.
		let keepalive_timeout = self.g.with(|g|g.keepalive_timeout);
		self.keepalive_timer.rearm(keepalive_timeout);
		if self.shutdown_timer.is_unarmed() {
			let shutdown_timeout = self.g.with(|g|g.shutdown_timeout);
			self.shutdown_timer.rearm(shutdown_timeout);
		}
		//Keepalive ends, if it was in progress. Idle starts.
		self.sink.flags.clear_imp(SflagInKeepalive);
		self.sink.flags.set_imp(SflagIdle);
		self.conntrack.set_idle();
	}
	fn set_timers_for_keepalive(&mut self)
	{
		//In keepalive, the keepalive timer is cleared. Shutdown timer remains running. Keepalive starts.
		self.keepalive_timer.disarm();
		self.sink.flags.set_imp(SflagInKeepalive);
		self.sink.flags.clear_imp(SflagIdle);
		self.conntrack.set_keepalive();
	}
	fn set_timers_for_request(&mut self)
	{
		//For requests, both keepalive and shutdown timers are cleared.
		self.keepalive_timer.disarm();
		self.shutdown_timer.disarm();
		self.sink.flags.clear_imp(SflagIdle);
		self.conntrack.set_request();
	}
	//Close the current transaction, if any and return normal close.
	fn _normal_close(&mut self) -> ForwardInterfaceError
	{
		self.sink.transaction = None;
		ForwardInterfaceError::NormalClose
	}
	//Close connection if needed. This should be called when:
	//
	//1) incoming_end gets set.
	//2) outgoing_end gets set.
	//3) Write buffer is emptied.
	//4) wshutdown gets set.
	//
	//Note that one does not need to actually establish that any of the above actually has happened. It
	//sufficies if any of the above may have happened.
	fn check_eof_close(&mut self, sink: &mut impl WriterS, set_recv_eof: bool) ->
		Result<(), ForwardInterfaceError>
	{
		//set_recv_eof impiles both incoming_end (this request) and rshutdown (end of all read data).
		self.flags.or_imp(CflagRshutdown, set_recv_eof);
		let write_empty = sink.is_empty() && self.i2o_secondary.is_empty();
		let read_empty = self.sink.o2i_secondary.is_empty();
		//If there is open transaction with discard remainder set, forcibly tear down the connection,
		//as it is in who knows what state.
		if self.sink.transaction.as_ref().map(|x|x.is_discard_remainder()).unwrap_or(false) {
			warn_backend!(&self.self_category, "Discard remainder");
			fail!(ForwardInterfaceError::SlaveFault);
		}
		//If all the following are true:
		//
		//1) Transaction has ended.
		//2) Write buffer (primary and secondary) is empty.
		//3) Read buffer (secondary) is empty. Primary read buffer is on master side.
		//4) Connection is upgraded.
		//
		//Then close the connection.
		fail_if!(self.sink.transaction.as_ref().map(|x|{
			x.rx_and_tx_is_done() && x.is_upgraded()
		}).unwrap_or(false) && write_empty && read_empty, self._normal_close());
		//If all the following are true:
		//
		//1) Transaction has finished transmitting, or there is no transaction.
		//2) rshutdown is set.
		//3) Write buffer (primary and secondary) is empty.
		//4) Read buffer (secondary) is empty. Primary read buffer is on master side.
		//
		//Then close the connection.
		fail_if!(self.flags.get(CflagRshutdown) && self.sink.transaction.as_ref().map(|x|x.tx_phase_is_done()).
			unwrap_or(true) && write_empty && read_empty, self._normal_close());
		//If all the following are true:
		//
		//1) Transaction has ended or does not exist.
		//2) Write buffer (primary and secondary) is empty.
		//3) Read buffer (secondary) is empty. Primary read buffer is on master side.
		//4) in_keepalive is false.
		//
		//The the connection transitions to idle.
		if self.sink.transaction.as_ref().map(|x|{
			x.rx_and_tx_is_done()
		}).unwrap_or(true) && write_empty && read_empty && !self.sink.flags.get(SflagInKeepalive) {
			//If using use-once, close the backends immediately after request completes. Request
			//failed on sink has similar effect, but different error.
			fail_if!(self.flags.get(CflagUseOnce), self._normal_close());
			if self.sink.req_failed_code > 0 {
				warn_backend!(&self.self_category, "HTTP code {code}",
					code=self.sink.req_failed_code);
				fail!(ForwardInterfaceError::BackendError);
			}
			//Clear request, it is no longer needed. Set timers for idle.
			self.sink.transaction = None;
			self.set_timers_for_idle();
			ConnectionFreeList::add_by_cid(&self.self_cid, self.self_category.clone());
		}
		//Then furthermore if all the following are true:
		//
		//1) wshutdown is set.
		//2) wshutdown_ack is not set.
		//3) Write buffer (primary and secondary) is empty.
		//
		//The shut down the write side of the socket.
		if self.flags.get(CflagWshutdown) && !self.flags.get(CflagWshutdownAck) && write_empty {
			if let Err(err) = sink.shutdown() {
				warn_backend!(&self.self_category, "TCP close: {err}");
				fail!(ForwardInterfaceError::WriteFailed(err));
			}
			self.flags.set_imp(CflagWshutdownAck);
		}
		//If both directions have gotten closed, close the connection.
		fail_if!(self.flags.get(CflagRshutdown) && self.flags.get(CflagWshutdownAck),
			self._normal_close());

		Ok(())
	}
}

impl ForwardInterface for Connection
{
	fn write_forward_headers(&mut self, sink: &mut impl WriterS, block: &mut HeaderBlock,
		req: &RequestInfo, hostdata: HostRoutingDataRef, _: ConnId) ->
		Result<(), Result<BackendError, ForwardInterfaceError>>
	{
		//This event can arrive before the connection has actually connected. In that case, we can write
		//into the buffers, as immediate send is disabled, so data will be buffered.
		//If EOF has been received, no new requests can be initiated.
		fail_if!(self.flags.get(CflagRshutdown) || self.flags.get(CflagWshutdown),
			Ok(BackendError::ConnectionLost));
		//If request is being serviced, then new request can not be initated.
		fail_if!(self.sink.transaction.is_some() || self.sink.flags.get(SflagInKeepalive),
			Ok(BackendError::ConnectionLost));
		//If use_once is set on backend, set connection: close to tell the backend about that.
		if self.flags.get(CflagUseOnce) { block.set_emit_close(); }
		//The return status is weird, as this can return both soft errors (Err(Ok(x))), and hard errors
		//(Err(Err(x))). Soft error should be passed as-is to caller. Hard errors should trigger garbage-
		//collection of the connection and ConnectionLost error code.
		//Write the headers. This also detects if header block is good or not.
		let mut buf = [MaybeUninit::uninit();MAX_HBLOCK_SIZE];
		match serialize_headers::<HeaderEncoder>(&mut buf, block, HeaderKind::Request(req.cfg.s_flags)) {
			//This failing is a hard error.
			Ok(h) => sink.write(&[h]).map_err(|e|Err(ForwardInterfaceError::WriteFailed(e)))?,
			//However, serialize_headers itself failing is a soft error, as this is caused by a bad
			//header block.
			Err(e) => fail!(Ok(BackendError::BadHeadersReceived(e)))
		};
		//Set the chunked flag.
		self.flags.force_imp(CflagChunked, block.is_chunked());
		//Prepare to receive response.
		self.recv.set_reply(req.attributes.is_head());
		self.sink.flags.clear_imp(SflagBlocked);
		self.sink.flags.set_imp(SflagPendingReq);
		sink.interrupt();	//If connection is not reading, make it read.
		//If self_cid is INVALID_CID, this connection is not on freelist.
		if !self.self_cid.is_invalid() {
			ConnectionFreeList::delete_by_cid(&self.self_cid);
			self.set_timers_for_request();
		}
		//Set the timers for request, store the received request. The state of outgoing_end depends on if
		//there is request body or not. Also, if upgrade is set, reserve space for the body. incoming_end
		//is false, because response will be sent.
		let mut transaction = TransactionStateOut::new(req, hostdata);
		if block.is_no_body() && block.get_special(ShdrProtocol).is_none() { transaction.notify_tx_done(); }
		self.sink.transaction = Some(transaction);
		Ok(())
	}
	fn write_forward_data(&mut self, sink: &mut impl WriterS, block: &[u8]) ->
		Result<bool, ForwardInterfaceError>
	{
		//It is enough to have freeform text error, because the only error that can happen is connection
		//being lost.
		//If primary buffer is not empty, write to secondary buffer, and then block the master connection.
		//If not chunked, just directly send data. Otherwise write chunk header and then data. Ignore empty
		//writes.
		if block.len() > 0 {
			if self.flags.get(CflagChunked) {
				//Format the chunk length as chunk header.
				let mut tmp = [MaybeUninit::uninit();20];
				let mut tmp = PrintBuffer::new(&mut tmp);
				write!(tmp, "{blen:x}\r\n", blen=block.len()).ok();
				let tmp = tmp.into_inner_bytes();
				if sink.is_empty() {
					sink.write(&[tmp, block, b"\r\n"])
				} else {
					//Write secondary buffer instead. There is no size bound, so these never
					//fail.
					self.i2o_secondary.append(&tmp).ok();
					self.i2o_secondary.append(block).ok();
					self.i2o_secondary.append(b"\r\n").ok();
					Ok(())
				}
			} else if sink.is_empty() {
				sink.write(&[block])
			} else {
				self.i2o_secondary.append(block).ok();
				Ok(())
			}.map_err(|err|{
				ForwardInterfaceError::WriteFailed(err)
			})?
		}
		//If there is data in the secondary buffer, signal to block the master connection.
		Ok(self.i2o_secondary.is_empty())
	}
	fn write_forward_trailers(&mut self, sink: &mut impl WriterS, block: &mut HeaderBlock) ->
		Result<(), ForwardInterfaceError>
	{
		//If outgoing_end is already set, ignore this, as there should not be two trailers. Otherwise set
		//outgoing_end because the outgoing data ends here.
		//If connection is upgraded, set wshutdown as it should be closed after any pending data is flushed.
		if let Some(transaction) = self.sink.transaction.as_mut() {
			if transaction.tx_phase_is_done() { return Ok(()); }
			transaction.notify_tx_done();
			if transaction.is_upgraded() { self.flags.set_imp(CflagWshutdown); }
		} else {
			return Ok(());	//Not supposed to be forwarding trailers without open transaction.
		}
		//There is no rewriting of trailers.
		//Do not send trailers if not chunked, as there are no place for trailers in that case.
		if self.flags.get(CflagChunked) {
			//Okay, serialize and send the trailer block. Note that if main sink is not empty, we need
			//to stuff the trailers into secondary buffer in order to send in proper order. The case
			//where primary is empty but secondary is not can not happen, because that cause immediate
			//underflow event, which swaps the buffers. There is also no size bound on secondary buffers,
			//so the appends never fail.
			const BLOCK_END: &'static [u8] = b"0\r\n";
			const TRANSFER_TERMINATOR: &'static [u8] = b"0\r\n\r\n";
			let mut buf = [MaybeUninit::uninit();MAX_HBLOCK_SIZE];
			match serialize_trailers::<HeaderEncoder,_>(&mut buf, block) {
				Ok(h) => if sink.is_empty() { 
					sink.write(&[BLOCK_END, h])
				} else {
					self.i2o_secondary.append(BLOCK_END).ok();
					self.i2o_secondary.append(h).ok();
					Ok(())
				},
				Err(err) => {
					//Trailers are never critical, so just send empty trailers.
					log!(ERROR "Sending trailers failed: {err}");
					if sink.is_empty() {
						sink.write(&[TRANSFER_TERMINATOR])
					} else {
						self.i2o_secondary.append(TRANSFER_TERMINATOR).ok();
						Ok(())
					}
				}
			}.map_err(|e|ForwardInterfaceError::WriteFailed(e))?;
		}
		//End conditions may have changed (e.g., outgoing_end or wshutdown).
		self.check_eof_close(sink, false)?;
		Ok(())
	}
	fn write_forward_error(&mut self, error: &str) -> Result<(), ForwardInterfaceError>
	{
		//If transaction is ended, close connection. This happens with upgraded connections
		//being closed in certain order.
		fail_if!(self.sink.transaction.as_ref().map(|x|x.rx_and_tx_is_done()).unwrap_or(false),
			self._normal_close());
		//If outgoing_end is not set, that means that outgoing stream is in inconsistent state, so the
		//connection can not continue. Also, if this is upgraded connection, tear it down, as the rest will
		//not make it.
		//wshutdown is not set, because it is only used with upgrades, and this tears down the connection
		//if used with upgrade.
		//Clear the request to direct the response to nowhere. The master has already reflected the
		//failure back, so the stream has ended.
		if let Some(transaction) = self.sink.transaction.as_mut() {
			fail_if!(!transaction.tx_phase_is_done() || transaction.is_upgraded(),
				ForwardInterfaceError::MasterError(error.to_owned()));
			transaction.discard_remainder();
		} else {
			return Ok(());	//Not supposed to be forwarding errors without open transaction.
		}
		//This action never contributes to delayed shutdown, as it does not touch any end flags or buffers.
		Ok(())
	}
	fn get_master(&self) -> CID
	{
		self.sink.transaction.as_ref().map(|x|x.get_cid()).unwrap_or_else(CID::invalid)
	}
	fn get_sid(&self) -> u32 { self.sink.transaction.as_ref().map(|x|x.get_sid()).unwrap_or(0) }
	fn notify_connected(&mut self, cid: CID)
	{
		//If there is no pending request, then transition to idle timers, and add to freelist.
		//Except if there is keepalive specified, in that case, perform immediate keepalive. This
		//socket is being processed, so the timer event will be noticed, even without the IRQ.
		//After the keepalive is successfully completed, the connection will join the freelist and be
		//available to serve requests. If backend is use-once and there has been no request for it yet,
		//shut it down immediately.
		self.self_cid = cid;
		if self.sink.transaction.is_none() {
			if self.flags.get(CflagUseOnce) {
				return self.shutdown_timer.rearm(std::time::Duration::from_secs(0));
			} else if self.keepalive.is_some() {
				self.keepalive_timer.rearm(PointInTime::now());
			} else {
				ConnectionFreeList::add_by_cid(&self.self_cid, self.self_category.clone());
				self.set_timers_for_idle();
			}
		} else {
			self.set_timers_for_request();
		}
	}
	fn request_data<S:RequestWriteReturn>(&mut self, sink: &mut S, mut irq: Interrupter) -> bool
	{
		//Offer the entiere secondary buffer.
		let ostatus = self.sink.o2i_secondary.has_data();
		match sink.write_backward_data(self.sink.o2i_secondary.as_slice(), true) {
			Some(amt) => self.sink.o2i_secondary.discard(amt),
			None => return false
		}
		//If the secondary buffer got negative edge, stuff the trailers in, if one has those.
		if ostatus && !self.sink.o2i_secondary.has_data() {
			if let Some(mut trailers) = replace(&mut self.sink.saved_trailers, None) {
				sink.write_backward_trailers(&mut trailers);
				//Set the transaction rx end, as this ended the transaction.
				self.sink.transaction.as_mut().map(|x|x.notify_rx_done());
			}
			//Do interrupt, so to resume sending.
			irq.interrupt();
		}
		true
	}
}
