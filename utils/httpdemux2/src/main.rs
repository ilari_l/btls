#![deny(unused_must_use)]
#![warn(unsafe_op_in_unsafe_fn)]

#[macro_use] extern crate btls_aux_fail;
use btls::certificates::LocalKeyPair;
use btls::utility::Mutex;
use btls_aux_config::BLines;
use btls_aux_config::LineWordIterator;
use btls_aux_fail::ResultExt;
use btls_aux_http::http::is_tchar;
use btls_aux_http::http::partial_decode_authority;
use btls_aux_http::http::partial_decode_path;
use btls_aux_inotify::CLOSED_WRITABLE;
use btls_aux_inotify::CREATED;
use btls_aux_inotify::DELETED;
use btls_aux_inotify::InotifyEventSink;
use btls_aux_inotify::InotifyTrait;
use btls_aux_inotify::InotifyWatcher;
use btls_aux_inotify::MOVED_FROM;
use btls_aux_inotify::MOVED_TO;
use btls_aux_serialization::Source;
use btls_aux_time::PointInTime;
use btls_aux_time::TimeUnit;
use btls_aux_unix::RawFdT;
use btls_aux_unix::SocketFamily;
use btls_daemon_helper_lib::abort;
use btls_daemon_helper_lib::cloned;
use btls_daemon_helper_lib::cow;
use btls_daemon_helper_lib::ensure_stdfd_open;
use btls_daemon_helper_lib::FileDescriptor;
use btls_daemon_helper_lib::get_max_files;
use btls_daemon_helper_lib::incompatible_with_certbot;
use btls_daemon_helper_lib::incompatible_with_panic_abort;
use btls_daemon_helper_lib::initialize_from_systemd;
use btls_daemon_helper_lib::IpSet;
use btls_daemon_helper_lib::Listener;
use btls_daemon_helper_lib::ListenerLoop;
use btls_daemon_helper_lib::ListenerLoopEvents;
use btls_daemon_helper_lib::LocalMetrics;
use btls_daemon_helper_lib::LocalThreadHandle;
use btls_daemon_helper_lib::MetricsCollector;
use btls_daemon_helper_lib::parse_netmask;
use btls_daemon_helper_lib::ProxyConfiguration;
use btls_daemon_helper_lib::ProxyConfigurationEntry;
use btls_daemon_helper_lib::RcuHashMap;
use btls_daemon_helper_lib::run_metrics_thread;
use btls_daemon_helper_lib::running_as_root;
use btls_daemon_helper_lib::SighupPipe;
use btls_daemon_helper_lib::SocketAddrEx;
use btls_daemon_helper_lib::start_threads;
use btls_daemon_helper_lib::translate_multiple_address;
use btls_daemon_helper_lib::Thread;
use btls_daemon_helper_lib::ThreadNewParams;
use btls_daemon_helper_lib::ThreadSpawnedParams;
use btls_util_logging::syslog;
use httpdemux2::BackendData;
use httpdemux2::check_mimetype;
use httpdemux2::check_placeholder;
use httpdemux2::ClientCertificate;
use httpdemux2::ConfigT;
use httpdemux2::ConnectionFactory2;
use httpdemux2::decode_hex;
use httpdemux2::DisableMode;
use httpdemux2::GlobalConfiguration;
use httpdemux2::GlobalConfigurationWrapper;
use httpdemux2::GlobalMetrics as GlobalMetrics2;
use httpdemux2::HeaderRewrite;
use httpdemux2::HostRoutingData;
use httpdemux2::HostRoutingDataFactory;
use httpdemux2::InsecureMode;
use httpdemux2::is_valid_uri;
use httpdemux2::KryptedTarget;
use httpdemux2::PathData;
use httpdemux2::RedirectMode;
use httpdemux2::RegularExpression;
use httpdemux2::REntry;
use httpdemux2::RInsecure;
use httpdemux2::RoutingContext;
use httpdemux2::test_set_plain_response;
use httpdemux2::TlsClient;
use httpdemux2::TlsServer;
use httpdemux2::UrlParseError;
use std::borrow::Borrow;
use std::borrow::Cow;
use std::cmp::min;
use std::collections::BTreeMap;
use std::collections::HashMap;
use std::collections::HashSet;
use std::env::args_os;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::fs::File;
use std::fs::read_dir;
use std::fs::read_link;
use std::hash::Hash;
use std::io::ErrorKind as IoErrorKind;
use std::io::Read;
use std::mem::replace;
use std::mem::transmute;
use std::net::IpAddr;
use std::ops::Deref;
use std::ops::DerefMut;
use std::panic::catch_unwind;
use std::panic::AssertUnwindSafe;
use std::path::Path;
use std::path::PathBuf;
use std::str::FromStr;
use std::sync::Arc;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering;
use std::thread::Builder;
use std::thread::sleep;
use std::time::Duration;
use std::time::Instant;


//Sanity check.
#[cfg_attr(not(any(target_pointer_width="32",target_pointer_width="64")), unsupported_pointer_width)]

fn parse_duration(optname: &str, x: &str) -> Duration
{
	btls_daemon_helper_lib::parse_duration(x).unwrap_or_else(|err|{
		abort!("{optname}: {err}")
	})
}

fn trans_any_target(tgt: &str) -> Result<SocketAddrEx, Cow<'static, str>>
{
	translate_multiple_address(tgt, None, None).map_err(|err|{
		cow!(F "{err}")
	})?.get(0).map(cloned).ok_or_else(||{
		cow!("No addresses")
	})
}


const MAX_FDS_REDUCE: usize = 100;

struct Config
{
	metrics_path: String,				//Report metrics to.
	hosts_path: String,				//Hosts path.
	proxy_name: String,				//Proxy name.
	tls_server: String,				//TLS server.
	chained_mode: bool,				//Chained mode?
	proxy_config: ProxyConfiguration,		//Proxy configuration.
	dremap_table: HashMap<SocketAddrEx, SocketAddrEx>,
	connect_timeout: TimeUnit,			//Connect timeout.
	connect_attempt_timeout: TimeUnit,		//Connect attempt timeout.
	transfer_timeout: TimeUnit,			//transfer timeout.
	keepalive_timeout: TimeUnit,			//keepalive timeout.
	shutdown_timeout: TimeUnit,			//shutdown timeout.
	proxy_timeout: TimeUnit,			//Timeout to receive PROXY headers.
	http_idle_timeout: TimeUnit,			//Timeout for idle master connections.
	shosts: Option<PathBuf>,			//Secondary hosts file.
	test_mode: bool,
	global_logflags: u32,
}

impl Config
{
	fn from_cmdline() -> Config
	{
		let mut metrics_path = String::new();
		let mut proxy_config = ProxyConfiguration::new();
		let mut dremap_table = HashMap::new();
		let mut connect_timeout = TimeUnit::Seconds(10);
		let mut connect_attempt_timeout = TimeUnit::Seconds(2);
		let mut transfer_timeout = TimeUnit::Seconds(15);
		let mut proxy_timeout = TimeUnit::Seconds(5);
		let mut keepalive_timeout = TimeUnit::Seconds(10);
		let mut shutdown_timeout = TimeUnit::Seconds(120);
		let mut http_idle_timeout = TimeUnit::Seconds(5);
		let mut proxy_name = String::new();
		let mut hosts = String::new();
		let mut tls_server = String::new();
		let mut chained_mode = false;
		let mut shosts = None;
		let mut test_mode = false;
		let mut global_logflags = 0xFFFFFFFF;
		let mut dummy = true;
		for i in args_os() {
			if dummy { dummy = false; continue; }
			let xstr = i.clone().into_string().unwrap_or_else(|err|{
				abort!("Invalid argument: `{err}`", err=err.to_string_lossy())
			});
			//--listen-fd, --threads, --ack and --log-as are handled elsewhere.
			xstr.strip_prefix("--metrics=").map(|x|{
				metrics_path = x.to_owned();
			});
			xstr.strip_prefix("--assume-nat=").map(|x|{
				let mut itr = x.splitn(2, "~");
				let a = itr.next().and_then(|x|trans_any_target(x).ok());
				let b = itr.next().and_then(|x|trans_any_target(x).ok());
				if let (Some(a), Some(b)) = (a, b) {
					dremap_table.insert(b, a);	//This is reversed.
				} else {
					abort!("Invalid --assume-name= spec")
				}
			});
			xstr.strip_prefix("--allow-proxy=").map(|x|{
				for entry in ProxyConfigurationEntry::from_multi(x) { proxy_config.push(entry); }
			});
			xstr.strip_prefix("--proxy-name=").map(|x|{
				proxy_name = x.to_owned();
			});
			xstr.strip_prefix("--tls-host=").map(|x|{
				tls_server = x.to_owned();
			});
			xstr.strip_prefix("--secondary-etc-hosts=").map(|x|{
				shosts = Some(PathBuf::from(x.to_owned()));
			});
			xstr.strip_prefix("--hosts=").map(|x|{
				hosts = x.to_owned();
			});
			xstr.strip_prefix("--connect-timeout=").map(|x|{
				connect_timeout = TimeUnit::Duration(parse_duration("--connect-timeout=<sec>", x));
			});
			xstr.strip_prefix("--connect-attempt-timeout=").map(|x|{
				connect_attempt_timeout = TimeUnit::Duration(parse_duration(
					"--connect-attempt-timeout=<sec>", x));
			});
			xstr.strip_prefix("--transfer-timeout=").map(|x|{
				transfer_timeout = TimeUnit::Duration(parse_duration("--transfer-timeout=<sec>", x));
			});
			xstr.strip_prefix("--proxy-timeout=").map(|x|{
				proxy_timeout = TimeUnit::Duration(parse_duration("--proxy-timeout=<sec>", x));
			});
			xstr.strip_prefix("--keepalive-timeout=").map(|x|{
				keepalive_timeout = TimeUnit::Duration(parse_duration("--keepalive-timeout=<sec>",
					x));
			});
			xstr.strip_prefix("--shutdown-timeout=").map(|x|{
				shutdown_timeout = TimeUnit::Duration(parse_duration("--shutdown-timeout=<sec>", x));
			});
			xstr.strip_prefix("--idle-timeout=").map(|x|{
				http_idle_timeout = TimeUnit::Duration(parse_duration("--idle-timeout=<sec>", x));
			});
			xstr.strip_prefix("--global-logflags=").map(|x|{
				global_logflags = u32::from_str_radix(x, 16).
					unwrap_or_else(|_|abort!("Invalid --global-logflags"));
			});
			if xstr == "--test-plain-reply" {
				test_mode = true;
			}
			if xstr == "--chained" {
				chained_mode = true;
			}
		}
		if hosts.len() == 0 { abort!("Non-empty --hosts=<dir> required"); }
		Config {
			metrics_path: metrics_path,
			dremap_table: dremap_table,
			proxy_config: proxy_config,
			chained_mode: chained_mode,
			tls_server: tls_server,
			proxy_name: proxy_name,
			connect_timeout: connect_timeout,
			connect_attempt_timeout: connect_attempt_timeout,
			transfer_timeout: transfer_timeout,
			proxy_timeout: proxy_timeout,
			keepalive_timeout: keepalive_timeout,
			shutdown_timeout: shutdown_timeout,
			http_idle_timeout: http_idle_timeout,
			hosts_path: hosts,
			shosts: shosts,
			test_mode: test_mode,
			global_logflags: global_logflags,
		}
	}
}

#[derive(Clone)]
struct HttpWorkerParams
{
	maxfiles: usize,
	listener_fds: Vec<RawFdT>,
	mpath: String,
	test_mode: bool,
	g: ConfigT,
}

struct Metrics
{
	g: Arc<GlobalMetrics>,
	l: Arc<Vec<Arc<LocalMetrics>>>,
}

impl MetricsCollector for Metrics
{
	fn fill(&self, r: &mut BTreeMap<u32, usize>)
	{
		self.g.deref().report(r);
		for i in self.l.iter() { i.report(r); }
	}
}

struct HttpWorker
{
	gstate: ListenerLoop
}

impl Thread for HttpWorker
{
	type Local = ();
	type LocalMetrics = LocalMetrics;
	type Parameters = HttpWorkerParams;
	type Parameters2 = RoutingManager;
	fn new<'a>(p: ThreadNewParams<'a,Self>) -> Self
	{
		let ThreadNewParams{gparameters: gp, lmetrics: lm, sigpipe: sp, tid, irqpipe, ..} = p;
		if gp.test_mode { test_set_plain_response(); }
		let gstate = setup_listener_loop(gp.maxfiles, sp, &gp.listener_fds, tid, irqpipe, lm.clone(), gp.g);
		HttpWorker {gstate: gstate}
	}
	fn spawned(p: ThreadSpawnedParams<Self>)
	{
		let ThreadSpawnedParams{gparameters: gp, gparameters2: rm, contexts: lm, ..} = p;
		run_controlplane_thread(rm);
		run_metrics_thread(&gp.mpath, Metrics {
			g: Arc::new(GlobalMetrics::new(gp.g.metrics_arc())),
			l: lm,
		});
	}
	fn run(mut self) { self.gstate.main_event_loop(); }
}

fn main()
{
	ensure_stdfd_open();
	let sinfo = initialize_from_systemd("httpdemux2");
	incompatible_with_panic_abort();
	incompatible_with_certbot();
	if running_as_root() { abort!("Can't run program as root"); }

	let listeners = sinfo.sockets();
	if listeners.len() == 0 { abort!("No ports to listen"); }

	let maxfiles = get_max_files() - MAX_FDS_REDUCE;
	syslog!(NOTICE "Using maximum of {maxfiles} file descriptors");

	let config = Config::from_cmdline();

	//Incoming TLS.
	let incoming_tls: Option<TlsServer> = if config.tls_server.len() > 0 {
		let h = TlsServer::new(Path::new(&config.tls_server)).unwrap_or_else(|err|{
			abort!("Error loading TLS hostkey: {err}")
		});
		Some(h)
	} else {
		None
	};
	let proxy_name = incoming_tls.as_ref().map(|tls|tls.get_name()).
		unwrap_or(config.proxy_name.clone().into_bytes());
	if proxy_name.len() == 0 { abort!("Non-empty --proxy-name=<name> required"); }

	let (rm, rc) = RoutingManager::new(Path::new(&config.hosts_path), config.shosts.clone());
	let global_config = GlobalConfiguration {
		dremap_table: config.dremap_table,
		proxy_config: config.proxy_config,
		proxy_name: Arc::new(proxy_name),
		connect_timeout: config.connect_timeout,
		connect_attempt_timeout: config.connect_attempt_timeout,
		transfer_timeout: config.transfer_timeout,
		proxy_timeout: config.proxy_timeout,
		keepalive_timeout: config.keepalive_timeout,
		shutdown_timeout: config.shutdown_timeout,
		http_idle_timeout: config.http_idle_timeout,
		routetable: rc,
		//If incoming_tls is set, then chained_mode is impiled.
		chained_mode: config.chained_mode || incoming_tls.is_some(),
		global_logflags: Arc::new(AtomicUsize::new(config.global_logflags as usize)),
		incoming_tls: incoming_tls.map(|s|Arc::new(s)),
	};
	let global_metrics = Arc::new(GlobalMetrics2::new());
	let global_config = Box::new(GlobalConfigurationWrapper::new(global_config, global_metrics.clone()));
	let global_config: ConfigT = unsafe{transmute(Box::into_raw(global_config))};

	start_threads::<HttpWorker>(sinfo, "HTTP worker", HttpWorkerParams {
		maxfiles: maxfiles,
		listener_fds: listeners,
		mpath: config.metrics_path,
		test_mode: config.test_mode,
		g: global_config,
	}, rm);
}

fn run_controlplane_thread(mut rm: RoutingManager)
{
	let b = Builder::new();
	let b = b.name(String::from("Controlplane"));
	if let Err(err) = b.spawn(move || {
		rm.do_loop();
	}) {
		abort!("Failed to launch controlplane thread: {err}")
	}
}

fn setup_listener_loop(maxfiles: usize, sigpipe: SighupPipe, listener_fds: &Vec<RawFdT>, tid: Option<u64>,
	irqpipe: LocalThreadHandle, local_metrics: Arc<LocalMetrics>, g: ConfigT) -> ListenerLoop
{
	//Construct connection factory.
	let factory = ConnectionFactory2::new2(g);
	let collector = ListenerLoopCollector::new(local_metrics, g);
	//Construct listener loop. Each thread should have its own.
	let mut gstate = ListenerLoop::new(maxfiles, sigpipe, irqpipe).unwrap_or_else(|err|{
		abort!("Creating event loop: {err}")
	});
	if let Some(tid) = tid { gstate.set_pid(tid); }
	gstate.set_events(collector);
	//Add the file descriptors.
	for i in listener_fds.iter() {
		let j = Listener::new(FileDescriptor::new(*i), factory.clone());
		gstate.add_listener(j);
	}
	gstate
}

/*******************************************************************************************************************/
pub const GMETRIC_UPTIME: u32 = 4;

pub struct ListenerLoopCollector(Arc<LocalMetrics>, ConfigT);

impl ListenerLoopCollector
{
	pub fn new(local_metrics: Arc<LocalMetrics>, g: ConfigT) -> Box<ListenerLoopCollector>
	{
		Box::new(ListenerLoopCollector(local_metrics, g))
	}
}

impl ListenerLoopEvents for ListenerLoopCollector
{
	fn locals(&self) -> Arc<LocalMetrics> { self.0.clone() }
	fn incoming(&self) { self.1.metrics(|g|g.conn_accept.fetch_add(1, Ordering::Relaxed)); }
	fn committed(&self) { self.1.metrics(|g|g.conn_commit.fetch_add(1, Ordering::Relaxed)); }
	fn retired(&self) { self.1.metrics(|g|g.conn_retire.fetch_add(1, Ordering::Relaxed)); }
}

fn instant_to_diff(now: Instant, base: Instant) -> usize
{
	let delta = now.saturating_duration_since(base);
	delta.as_secs() as usize * 1000000 + delta.subsec_nanos() as usize / 1000
}

pub struct GlobalMetrics
{
	basetime: Instant,
	inner: Arc<GlobalMetrics2>,
}

impl GlobalMetrics
{
	pub fn new(inner: Arc<GlobalMetrics2>) -> GlobalMetrics
	{
		GlobalMetrics {
			basetime: Instant::now(),
			inner: inner,
		}
	}
}

macro_rules! define_http_gmetrics
{
	($([$value:tt $symbol:ident $variable:ident];)*) => {
		impl GlobalMetrics
		{
			pub fn report(&self, r: &mut BTreeMap<u32, usize>)
			{
				let m = &self.inner;
				r.insert(GMETRIC_UPTIME, instant_to_diff(Instant::now(), self.basetime));
				$(r.insert($value, m.$variable.load(Ordering::Relaxed));)*
			}
		}
	}
}

//Define the metrics.
include!("metrics.inc");
/*******************************************************************************************************************/
const TRAILING_JUNK: &'static str = "Trailing junk on line";

enum PRegexParseError
{
	ExpectedRegex,
	NoSlashRoot,
	NoColonRedirect,
	UnrecognizedFlags,
	BadError,
	Duplicate(&'static str),
	Unrecognized(String),
	BadPattern(&'static str, String),
	InvalidRedirect(String),
	BadRegex(String),
	BadError2(u16),
}

impl Display for PRegexParseError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::PRegexParseError::*;
		match self {
			&ExpectedRegex => f.write_str("Expected regex, got end of line"),
			&NoSlashRoot => f.write_str("Files root path does not start with /"),
			&NoColonRedirect => f.write_str("No colon in redirect"),
			&UnrecognizedFlags => f.write_str("Unrecognized flags"),
			&BadError => f.write_str("Error code is not numeric"),
			&Duplicate(k) => write!(f, "Duplicate path-regex {k} directive"),
			&Unrecognized(ref k) => write!(f, "Unrecognized path-regex directive {k}"),
			&BadPattern(k, ref err) => write!(f, "Bad {k} pattern: {err}"),
			&InvalidRedirect(ref k) => write!(f, "Invalid redirect mode {k}"),
			&BadRegex(ref k) => write!(f, "Invalid regular expression: {k}"),
			&BadError2(code) => write!(f, "Bad error code {code}"),
		}
	}
}


enum DisableParseError
{
	BadDisableMode(String),
	BadPath(String),
	BadPathStart,
	BadRedirectTarget(UrlParseError),
	ExpectedDisableMode,
	ExpectedDisableModeArgument,
	ExpectedPath,
	Junk,
}

impl Display for DisableParseError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::DisableParseError::*;
		match self {
			&BadDisableMode(ref mode) => write!(f, "Bad mode '{mode}'"),
			&BadPath(ref path) => write!(f, "Bad path '{path}'"),
			&BadPathStart => f.write_str("Path must start with '/'"),
			&BadRedirectTarget(ref uri) => write!(f, "Bad redirect target URI: {uri}"),
			&ExpectedDisableMode => f.write_str("Expected mode, got end of line"),
			&ExpectedDisableModeArgument =>
				f.write_str("Expected reason/target, got end of line"),
			&ExpectedPath => f.write_str("Expected path, got end of line"),
			&Junk => f.write_str(TRAILING_JUNK),
		}
	}
}

enum HeaderParseError
{
	BadHeaderDirective(String),
	BadHeaderName(String),
	BadHeaderValue(String),
	ExpectedHeaderDirective,
	ExpectedHeaderName,
	ExpectedHeaderValue,
	Junk,
}

impl Display for HeaderParseError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::HeaderParseError::*;
		match self {
			&BadHeaderDirective(ref dir) => write!(f, "Bad header directive '{dir}'"),
			&BadHeaderName(ref name) => write!(f, "Bad header name '{name}'"),
			&BadHeaderValue(ref value) => write!(f, "Bad header value '{value}'"),
			&ExpectedHeaderDirective => f.write_str("Expected header directive, got end of line"),
			&ExpectedHeaderName => f.write_str("Expected header name, got end of line"),
			&ExpectedHeaderValue => f.write_str("Expected header value, got end of line"),
			&Junk => f.write_str(TRAILING_JUNK),
		}
	}
}

enum PathDirParseError
{
	BadPath(String),
	BadPathDirective(String),
	BadPathStart,
	ExpectedDBackend,
	ExpectedDFcBackend,
	ExpectedDHost,
	ExpectedDPath,
	ExpectedPath,
	HostPDirective(String),
	PathPDirective(String),
}

impl Display for PathDirParseError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::PathDirParseError::*;
		match self {
			&BadPath(ref path) => write!(f, "Bad path '{path}'"),
			&BadPathDirective(ref dir) => write!(f, "Bad directive '{dir}'"),
			&BadPathStart => f.write_str("Paths must start with '/'"),
			&ExpectedDBackend => f.write_str("Backend: Expected backend, got end of line"),
			&ExpectedDFcBackend => f.write_str("Fc-backend: Expected backend, got end of line"),
			&ExpectedDHost => f.write_str("Host: Expected host, got end of line"),
			&ExpectedDPath => f.write_str("Path: Expected path, got end of line"),
			&ExpectedPath => f.write_str("Expected path, got end of line"),
			&HostPDirective(ref dir) => write!(f, "Host: Bad host '{dir}'"),
			&PathPDirective(ref dir) => write!(f, "Path: Bad path '{dir}'"),
		}
	}
}

fn split_head<'a>(x: &'a [String]) -> (Option<&'a str>, &'a [String])
{
	if x.len() > 0 { (Some(&x[0]), &x[1..]) } else { (None, x) }
}

fn disable_mode_parse(x: &[String]) -> Result<DisableMode, DisableParseError>
{
	use DisableMode as DM;
	let (mode, x) = split_head(x);
	let (path, x) = split_head(x);
	type Ctor = fn(String) -> DisableMode;
	let (mkmode, uri) = match mode {
		None => fail!(DisableParseError::ExpectedDisableMode),
		Some("moved-permanently") => (DM::MovedPermanently as Ctor, true),
		Some("found") => (DM::Found as Ctor, true),
		Some("temporary-redirect") => (DM::TemporaryRedirect as Ctor, true),
		Some("permanent-redirect") => (DM::PermanentRedirect as Ctor, true),
		Some("forbidden") => (DM::Forbidden as Ctor, false),
		Some("not-found") => (DM::NotFound as Ctor, false),
		Some("gone") => (DM::Gone as Ctor, false),
		Some("unavailable-for-legal-reasons") => (DM::UnavailableForLegalReasons as Ctor, false),
		Some(x) => fail!(DisableParseError::BadDisableMode(x.to_owned())),
	};
	let path = path.ok_or(DisableParseError::ExpectedDisableModeArgument)?;
	if uri { is_valid_uri(path).map_err(|u|DisableParseError::BadRedirectTarget(u))?; }
	let ans = mkmode(path.to_owned());
	fail_if!(x.len() > 0, DisableParseError::Junk);
	Ok(ans)
}

fn parse_acl(x: &[String]) -> Result<[u8;32], String>
{
	let (hash, x) = split_head(x);
	let hash = hash.ok_or("Expected hash, got end of line")?;
	let mut acl = [0;32];
	decode_hex(&mut acl, hash.as_bytes()).ok_or_else(||{
		format!("Bad hash '{hash}'")
	})?;
	fail_if!(x.len() > 0, TRAILING_JUNK);
	Ok(acl)
}

fn parse_ipset(ipset: &mut IpSet, x: &[String], polarity: bool) -> Result<(), String>
{
	let (ip, x) = split_head(x);
	let ip = ip.ok_or("Expected IP, got end of line")?;
	let (ip, mask) = parse_netmask(ip).ok_or_else(||{
		format!("Bad IP '{ip}'")
	})?;
	fail_if!(x.len() > 0, TRAILING_JUNK);
	ipset.add(IpAddr::V6(ip), mask, polarity);
	Ok(())
}

fn parse_h2_max_streams(max_streams: &mut Option<u32>, x: &[String]) -> Result<(), String>
{
	let (count, x) = split_head(x);
	let count = count.ok_or("Expected count, got end of line")?;
	let count = u32::from_str(count).set_err("Bad count")?;
	fail_if!(x.len() > 0, TRAILING_JUNK);
	*max_streams = Some(count);
	Ok(())
}

fn redirect_mode_parse(x: &[String]) -> Result<RedirectMode, String>
{
	let (mode, x) = split_head(x);
	let ans = match mode {
		Some("moved-permanently") => RedirectMode::MovedPermanently,
		Some("found") => RedirectMode::Found,
		Some("temporary-redirect") => RedirectMode::TemporaryRedirect,
		Some("permanent-redirect") => RedirectMode::PermanentRedirect,
		Some(mode) => fail!(format!("Bad mode '{mode}'")),
		None => fail!("Expected mode, got end of line")
	};
	fail_if!(x.len() > 0, TRAILING_JUNK);
	Ok(ans)
}

fn insecure_mode_parse(x: &[String]) -> Result<InsecureMode, String>
{
	let (mode, x) = split_head(x);
	let xfn: Result<fn(RedirectMode)->InsecureMode, InsecureMode> = match mode {
		Some("forbidden") => Err(InsecureMode::Forbidden),
		Some("root-only") => Ok(InsecureMode::RootOnly),
		Some("root-only-flag") => Ok(InsecureMode::RootOnlyFlag),
		Some("redirect") => Ok(InsecureMode::Redirect),
		Some("allow") => Err(InsecureMode::Allow),
		Some(mode) => fail!(format!("Bad insecure-mode '{mode}'")),
		None => fail!("Expected mode, got end of line")
	};
	Ok(match xfn {
		Ok(f) => {
			let mode = redirect_mode_parse(x).map_err(|err|{
				format!("Bad redirect: {err}")
			})?;
			f(mode)
		},
		Err(err) => {
			fail_if!(x.len() > 0, TRAILING_JUNK);
			err
		}
	})
}

fn bad_alpn_sni(x: &str) -> bool
{
	x.len() == 0 || x.len() > 255
}

fn handle_target<F>(targets: &mut Vec<(SocketAddrEx, bool)>, mut tail: &[String], shosts: Option<&Path>, f: F) ->
	Result<(), String> where F: Fn(&SocketAddrEx) -> bool
{
	let mut notls = false;
	if tail.last().map(|x|x.deref()) == Some("no-tls") {
		notls = true;
		tail = &tail[..tail.len()-1];
	}
	let mut tmp = parse_target(tail, shosts).map_err(|err|{
		format!("target: {err}")
	})?;
	let tmp = tmp.drain(..).filter(f).map(|x|(x, notls));
	Ok(targets.extend(tmp))
}

fn backend_data_new(name: &str, content: &[u8], shosts: Option<&Path>) -> Result<BackendData, String>
{
	let mut targets: Vec<(SocketAddrEx, bool)> = Vec::new();
	let mut keepalive: Option<(String, String)> = None;
	let mut use_once = false;
	let mut tls_alpn = None;
	let mut tls_sni = String::new();
	let mut tls_pins = Vec::new();
	let mut tls_ccert = None;

	'lineloop: for (row, linec) in BLines::new(content).enumerate() {
		let mut line = Vec::new();
		for word in LineWordIterator::new(linec) { match word {
			Ok(w) => line.push(w.into_owned()),
			Err(err) => {
				syslog!(WARNING "{name} line {line}: {err}", line=row+1);
				continue 'lineloop;
			}
		}}
		if let Err(err) = call(||{
			match split_head(&line) {
				(Some("keepalive"), tail) => {
					let val = parse_keepalive(tail).map_err(|err|{
						format!("keepalive: {err}")
					})?;
					Ok(keepalive = Some(val))
				},
				(Some("use-once"), tail) if tail.len() == 0 => Ok(use_once = true),
				(Some("use-once"), _) => Err(format!("Junk after use-once")),
				(Some("target"), tail) => handle_target(&mut targets, tail, shosts, |_|true),
				(Some("target-ipv4"), tail) => handle_target(&mut targets, tail, shosts, |x|{
					x.get_family() == Some(SocketFamily::INET)
				}),
				(Some("target-ipv6"), tail) => handle_target(&mut targets, tail, shosts, |x|{
					x.get_family() == Some(SocketFamily::INET6)
				}),
				(Some("tls"), tail) => call::<Result<(), String>,_>(||{
					fail_if!(tail.len() < 1, "Expected SNI, got end of line");
					fail_if!(tail.len() > 2, TRAILING_JUNK);
					fail_if!(bad_alpn_sni(&tail[0]), "Bad SNI");
					tls_sni = tail[0].to_owned();
					if tail.len() >= 2 {
						fail_if!(bad_alpn_sni(&tail[0]), "Bad ALPN");
						tls_alpn = Some(tail[1].to_owned());
					}
					Ok(())
				}).map_err(|err|{
					format!("tls: {err}")
				}),
				(Some("tls-connect-timeout"), _) => {
					syslog!(WARNING "{name} line {line}: tls-connect-timeout no longer supported",
						line=row+1);
					Ok(())
				},
				(Some("tls-transfer-timeout"), _) => {
					syslog!(WARNING "{name} line {line}: tls-transfer-timeout no longer supported",
						line=row+1);
					Ok(())
				},
				(Some("tls-trusted-key"), tail) => call::<Result<(), String>,_>(||{
					let mut keyid2 = [0;32];
					fail_if!(tail.len() < 1, "Expected keyhash, got end of line");
					fail_if!(tail.len() > 1, TRAILING_JUNK);
					fail_if!(tail[0].len() != 64, "Bad Keyhash");
					for (idx, c) in tail[0].as_bytes().iter().enumerate() {
						let n = match *c {
							48..=57 => c - 48,
							65..=70 => c - 55,
							97..=102 => c - 87,
							_ => fail!("Bad Keyhash")
						};
						keyid2[idx/2] |= n << 4 - idx%2 * 4;
					}
					tls_pins.push(keyid2);
					Ok(())
				}).map_err(|err|{
					format!("tls-trusted-key: {err}")
				}),
				(Some("tls-client-certificate"), tail) => call::<Result<(), String>,_>(||{
					fail_if!(tail.len() < 1, "Expected key file, got end of line");
					fail_if!(tail.len() < 2, "Expected certificate file, got end of line");
					fail_if!(tail.len() > 2, TRAILING_JUNK);
					let key = LocalKeyPair::new_file(&tail[0]).map_err(|err|{
						format!("Failed to load key: {err}")
					})?;
					let mut certificates = Vec::new();
					let mut certfile = Vec::new();
					File::open(&tail[1]).and_then(|mut fp|{
						fp.read_to_end(&mut certfile)
					}).map_err(|err|{
						format!("Failed to load certificate: {err}")
					})?;
					let mut certfile = Source::new(&certfile);
					while certfile.more() {
						let ent = certfile.asn1_sequence_out().map_err(|err|{
							format!("Failed to parse certificate: {err}")
						})?;
						certificates.push(ent.to_owned());
					}
					fail_if!(certificates.len() == 0,
						"Failed to load certificate: No certificates in chain");
					let cert = certificates.remove(0);
					tls_ccert = Some(ClientCertificate::new(Arc::new(Box::new(key)),
						cert, certificates));
					Ok(())
				}).map_err(|err|{
					format!("tls-client-certificate: {err}")
				}),
				(Some(dir), _) => Err(format!("Bad directive '{dir}'")),
				(None, _) => Ok(()),	//Ignore empty lines.
			}
		}) {
			syslog!(WARNING "{name} line {line}: {err}", line=row+1)
		}
	}
	let targets: Vec<KryptedTarget> = if tls_sni.len() > 0 {
		let tls = Arc::new(TlsClient::new(&tls_sni, tls_alpn.as_deref(), tls_pins.iter().cloned(),
			tls_ccert));
		targets.drain(..).map(|(target, notls)|{
			if !notls {
				KryptedTarget::new_tls_client(target, tls.clone())
			} else {
				KryptedTarget::new_plaintext(target)
			}
		}).collect()
	} else {
		targets.drain(..).map(|(target, _)|KryptedTarget::new_plaintext(target)).collect()
	};

	Ok(BackendData {
		targets: Arc::new((targets,AtomicUsize::new(0))),
		keepalive: keepalive.map(|(x, y)|Arc::new((x,y))),
		use_once: use_once,
	})
}

fn call<T:Sized,F>(f: F) -> T where F: FnOnce() -> T { f() }

fn parse_canonical_host(x: &[String]) -> Result<(String, RedirectMode), String>
{
	let (chost, x) = split_head(x);
	let chost = chost.ok_or("Expected host, got end of line")?;
	let chost = check_authority(chost.to_owned()).map_err(|authority|{
		format!("Bad authority '{authority}'")
	})?;
	let mode = redirect_mode_parse(x).map_err(|mode|{
		format!("Bad redirect: {mode}")
	})?;
	Ok((chost, mode))
}

fn parse_root_redirect(x: &[String]) -> Result<(String, RedirectMode), String>
{
	let (url, x) = split_head(x);
	let url = url.ok_or("Expected target URI, got end of line")?;
	let url = is_valid_uri(url).map_err(|uri|{
		format!("Bad target URI: {uri}")
	})?;
	let mode = redirect_mode_parse(x).map_err(|mode|{
		format!("Bad redirect: {mode}")
	})?;
	Ok((url.to_owned(), mode))
}

fn parse_backend(x: &[String]) -> Result<String, String>
{
	let (bhost, x) = split_head(x);
	let bhost = bhost.ok_or("Expected backend, got end of line")?;
	fail_if!(x.len() > 0, TRAILING_JUNK);
	Ok(bhost.to_owned())
}

fn parse_request_host(x: &[String]) -> Result<String, String>
{
	let (rhost, x) = split_head(x);
	let rhost = rhost.ok_or("Expected host, got end of line")?;
	fail_if!(x.len() > 0, TRAILING_JUNK);
	check_authority(rhost.to_owned()).map_err(|authority|{
		format!("Bad host '{authority}'")
	})
}

fn parse_request_path(x: &[String]) -> Result<String, String>
{
	let (rpath, x) = split_head(x);
	let rpath = rpath.ok_or("Expected path, got end of line")?;
	fail_if!(x.len() > 0, TRAILING_JUNK);
	check_path(rpath.to_owned()).map_err(|path|{
		format!("Bad path '{path}'")
	})
}

fn parse_keepalive(x: &[String]) -> Result<(String, String), String>
{
	let (khost, x) = split_head(x);
	let (kpath, x) = split_head(x);
	let khost = khost.ok_or("Expected host, got end of line")?;
	fail_if!(x.len() > 0, TRAILING_JUNK);
	let host = check_authority(khost.to_owned()).map_err(|authority|{
		format!("Bad host '{authority}'")
	})?;
	let path = if let Some(kpath) = kpath {
		check_path(kpath.to_owned()).map_err(|path|{
			format!("Bad path '{path}'")
		})?
	} else {
		"*".to_owned()
	};
	Ok((host, path))
}

fn parse_target(tail: &[String], shosts: Option<&Path>) -> Result<Vec<SocketAddrEx>, String>
{
	let (target, x) = split_head(tail);
	let target = target.ok_or("Expected target, got end of line")?;
	fail_if!(x.len() > 0, TRAILING_JUNK);
	translate_multiple_address(target, Some(80), shosts).map_err(|e|e.to_string())
}

fn _parse_path(x: &[String], host: &mut HostRoutingDataFactory) -> Result<PathData, PathDirParseError>
{
	let mut data = PathData::new();
	//Default to Forbidden, as insecure directive is needed to enable access.
	data.set_insecure_mode(Some(InsecureMode::Forbidden));
	//Default is false, as these options do not actually inherit.
	data.set_add_cors(false);
	data.set_noaccess(false);
	data.set_raw_host(false);
	data.set_raw_path(false);
	let mut itr = x.iter();
	loop {
		let directive = itr.next();
		match directive.map(|x|x.deref()) {
			Some("insecure") => data.set_insecure_mode(None),	//Inherit.
			Some("insecure-allow") => data.set_insecure_mode(Some(InsecureMode::Allow)),
			Some("insecure-moved-permanently") =>
				data.set_insecure_mode(Some(InsecureMode::Redirect(RedirectMode::MovedPermanently))),
			Some("insecure-found") =>
				data.set_insecure_mode(Some(InsecureMode::Redirect(RedirectMode::Found))),
			Some("insecure-temporary-redirect") => data.set_insecure_mode(
				Some(InsecureMode::Redirect(RedirectMode::TemporaryRedirect))),
			Some("insecure-permanent-redirect") => data.set_insecure_mode(
				Some(InsecureMode::Redirect(RedirectMode::PermanentRedirect))),
			Some("global-access") => data.set_add_cors(true),
			Some("no-access") => data.set_noaccess(true),
			Some("raw-host") => data.set_raw_host(true),
			Some("raw-path") => data.set_raw_path(true),
			Some("backend") => match itr.next() {
				Some(b) => data.set_backend(host, b),
				None => fail!(PathDirParseError::ExpectedDBackend),
			},
			Some("fc-backend") => match itr.next() {
				Some(b) => data.set_fc_backend(host, b),
				None => fail!(PathDirParseError::ExpectedDFcBackend),
			},
			Some("host") => match itr.next() {
				Some(b) => data.set_remaphost(host, &check_authority(b.to_owned()).
					map_err(|f|PathDirParseError::HostPDirective(f))?),
				None => fail!(PathDirParseError::ExpectedDHost),
			},
			Some("path") => match itr.next() {
				Some(b) => data.set_remappath(host, &check_path(b.to_owned()).
					map_err(|h|PathDirParseError::PathPDirective(h))?),
				None => fail!(PathDirParseError::ExpectedDPath),
			},
			Some(x) => fail!(PathDirParseError::BadPathDirective(x.to_owned())),
			None => break
		};
	}
	Ok(data)
}

fn parse_path(x: &[String], host: &mut HostRoutingDataFactory) -> Result<(), PathDirParseError>
{
	use self::PathDirParseError::*;
	let (path, tail) = split_head(x);
	match (path, tail) {
		(Some(path), _) if !path.starts_with("/") => Err(BadPathStart),
		(Some(path), tail) => {
			let path = check_path(path.to_owned()).map_err(|h|BadPath(h))?;
			let data = _parse_path(tail, host)?;
			host.add_path_data(&path, &data);
			Ok(())
		},
		(None, _) => Err(ExpectedPath),
	}
}

fn parse_disable(x: &[String], host: &mut HostRoutingDataFactory) -> Result<(), DisableParseError>
{
	use self::DisableParseError::*;
	let (path, tail) = split_head(x);
	match (path, tail) {
		(Some(path), _) if !path.starts_with("/") => Err(BadPathStart),
		(Some(path), tail) => {
			let path = check_path(path.to_owned()).map_err(|h|BadPath(h))?;
			let data = disable_mode_parse(tail)?;
			host.add_disable_data(&path, &data);
			Ok(())
		},
		(None, _) => Err(DisableParseError::ExpectedPath),
	}
}

fn parse_force_mime(x: &[String], host: &mut HostRoutingDataFactory) -> Result<(), String>
{
	let (path, x) = split_head(x);
	let (mime, x) = split_head(x);
	fail_if!(x.len() > 0, TRAILING_JUNK);
	match (path, mime) {
		(Some(path), _) if !path.starts_with("/") => fail!("Paths must start with '/'"),
		(Some(path), Some(mime)) => {
			let path = check_path(path.to_owned()).map_err(|path|{
				format!("Bad path '{path}'")
			})?;
			let mime = check_mimetype(mime).ok_or_else(||{
				format!("Bad MIME type '{mime}'")
			})?;
			host.add_forcemime_data(&path, mime);
			Ok(())
		},
		(Some(_), None) => fail!("Expected MIME type, got end of line"),
		(None, _) => fail!("Expected path, got end of line"),
	}
}

fn set_once<T>(output: &mut Option<T>, input: T, kind: &'static str) -> Result<(), PRegexParseError>
{
	fail_if!(output.is_some(), PRegexParseError::Duplicate(kind));
	*output = Some(input);
	Ok(())
}

fn parse_rewrite_location(x: &[String], host: &mut HostRoutingDataFactory) -> Result<(), String>
{
	let (regex, x) = x.split_first().ok_or("Expected regex, got end of line")?;
	let regex = RegularExpression::new_pattern(regex).map_err(|k|{
		format!("Invalid regular expression: {k}")
	})?;
	let (pattern, x) = x.split_first().ok_or("Expected target, got end of line")?;
	fail_if!(x.len() > 0, TRAILING_JUNK);
	host.add_location_rewrite(&regex, pattern.as_bytes());
	Ok(())
}


const HTTP_S301: &'static str = "moved-permanently";
const HTTP_S302: &'static str = "found";
const HTTP_S307: &'static str = "temporary-redirect";
const HTTP_S308: &'static str = "permanent-redirect";

fn parse_path_regex(x: &[String], host: &mut HostRoutingDataFactory) -> Result<(), PRegexParseError>
{
	let mut out = REntry {
		add_cors: false,
		backend: None,
		blacklisted: false,
		fc_backend: None,
		ws_backend: None,
		force_mime: None,
		insecure: None,
		logflags: None,
		raw_host: false,
		raw_path: false,
		redirect: None,
		remaphost: None,
		remappath: None,
		uncompressed: false,
		error: 0,	//FIXME: Implement setting this.
	};
	let (regex, tail) = x.split_first().ok_or(PRegexParseError::ExpectedRegex)?;
	let regex = RegularExpression::new_pattern(regex).map_err(PRegexParseError::BadRegex)?;
	for i in tail.iter() {
		if i == "global-access" {
			out.add_cors = true;
		} else if let Some(backend) = i.strip_prefix("backend=") {
			set_once(&mut out.backend, backend.to_owned(), "backend")?;
		} else if let Some(backend) = i.strip_prefix("websockets=") {
			set_once(&mut out.ws_backend, backend.to_owned(), "websockets")?;
		} else if i == "blacklist" {
			out.blacklisted = true;
		} else if let Some(error) = i.strip_prefix("error=") {
			match u16::from_str(error) {
				Ok(code) if code >= 400 && code <= 599 => out.error = code,
				Ok(code) => fail!(PRegexParseError::BadError2(code)),
				Err(_) => fail!(PRegexParseError::BadError),
			}
		} else if let Some(fc_backend) = i.strip_prefix("files=") {
			let (root, path) = match fc_backend.find("//") {
				Some(p) => (&fc_backend[..p], &fc_backend[p+2..]),
				None => (&fc_backend[..], ""),
			};
			fail_if!(!root.starts_with("/"), PRegexParseError::NoSlashRoot);
			check_placeholder(path.as_bytes()).
				map_err(|e|PRegexParseError::BadPattern("files", e))?;
			set_once(&mut out.fc_backend, (root.to_owned(), path.to_owned()), "files")?;
		} else if i == "secure" {
			set_once(&mut out.insecure, RInsecure::Block, "insecure")?;
		} else if i == "no-security" {
			set_once(&mut out.insecure, RInsecure::Allow, "insecure")?;
		} else if let Some(insecure) = i.strip_prefix("http-redirect=") {
			let kind = match insecure {
				HTTP_S301 => RedirectMode::MovedPermanently,
				HTTP_S302 => RedirectMode::Found,
				HTTP_S307 => RedirectMode::TemporaryRedirect,
				HTTP_S308 => RedirectMode::PermanentRedirect,
				_ => fail!(PRegexParseError::InvalidRedirect(insecure.to_owned())),
			};
			set_once(&mut out.insecure, RInsecure::Redirect(kind), "insecure")?;
		} else if let Some(flags) = i.strip_prefix("log-flags=") {
			match u32::from_str_radix(flags, 16) {
				Ok(flags) => set_once(&mut out.logflags, flags, "log flags")?,
				Err(_) => fail!(PRegexParseError::UnrecognizedFlags),
			}
		} else if let Some(mime) = i.strip_prefix("mime=") {
			set_once(&mut out.force_mime, mime.to_owned(), "mime type")?;
		} else if i == "raw-host" {
			out.raw_host = true;
		} else if i == "raw-path" {
			out.raw_path = true;
		} else if let Some(redirect) = i.strip_prefix("redirect=") {
			let (mode, remainder) = match redirect.find(":") {
				Some(p) => (&redirect[..p], &redirect[p+1..]),
				None => fail!(PRegexParseError::NoColonRedirect),
			};
			let mode = match mode {
				HTTP_S301 => RedirectMode::MovedPermanently,
				HTTP_S302 => RedirectMode::Found,
				HTTP_S307 => RedirectMode::TemporaryRedirect,
				HTTP_S308 => RedirectMode::PermanentRedirect,
				_ => fail!(PRegexParseError::InvalidRedirect(mode.to_owned())),
			};
			check_placeholder(remainder.as_bytes()).
				map_err(|e|PRegexParseError::BadPattern("redirect", e))?;
			set_once(&mut out.redirect, (mode, remainder.to_owned()), "redirect")?;
		} else if let Some(remaphost) = i.strip_prefix("remap-host=") {
			set_once(&mut out.remaphost, remaphost.to_owned(), "remap-host")?;
		} else if let Some(remappath) = i.strip_prefix("remap-path=") {
			check_placeholder(remappath.as_bytes()).
				map_err(|e|PRegexParseError::BadPattern("remap-path", e))?;
			set_once(&mut out.remappath, remappath.to_owned(), "remap-path")?;
		} else if i == "uncompressed" {
			out.uncompressed = true;
		} else {
			fail!(PRegexParseError::Unrecognized(i.clone()));
		}
	}
	host.add_regex(&regex, &out);
	Ok(())
}


fn check_path(h: String) -> Result<String, String>
{
	fail_if!(!h.starts_with("/"), h.clone());
	let mut h2 = h.clone().into_bytes();
	let hlen = partial_decode_path(&mut h2).map_err(|_|h.clone())?.len();
	h2.truncate(hlen);
	String::from_utf8(h2).map_err(|_|h.clone())
}

fn check_authority(h: String) -> Result<String, String>
{
	let mut h2 = h.clone().into_bytes();
	let hlen = partial_decode_authority(&mut h2).map_err(|_|h.clone())?.len();
	h2.truncate(hlen);
	String::from_utf8(h2).map_err(|_|h.clone())
}


fn check_header_name(h: &str) -> Result<(), HeaderParseError>
{
	fail_if!(!h.as_bytes().iter().cloned().all(is_tchar), HeaderParseError::BadHeaderName(h.to_owned()));
	Ok(())
}

fn is_special(c: u8) -> bool { c == 127 || (c < 32 && c != 9) }

fn check_header_value(h: &str) -> Result<(), HeaderParseError>
{
	fail_if!(h.as_bytes().iter().cloned().any(is_special), HeaderParseError::BadHeaderValue(h.to_owned()));
	Ok(())
}

fn parse_header(tgt: &mut HeaderRewrite, x: &[String]) -> Result<(), HeaderParseError>
{
	let (operation, x) = split_head(x);
	let (header, x) = split_head(x);
	let (value, x) = split_head(x);
	match (operation, header, value, x.len()) {
		(Some("delete"), Some(header), None, 0) => {
			check_header_name(header)?;
			tgt.delete.insert(header.to_ascii_lowercase().into_bytes());
			Ok(())
		},
		(Some("set"), Some(header), Some(value), 0) => {
			check_header_name(header)?;
			check_header_value(value)?;
			let header = header.to_ascii_lowercase();
			tgt.delete.insert(header.clone().into_bytes());
			tgt.add.push((header.into_bytes(), value.to_owned().into_bytes()));
			Ok(())
		},
		(Some("append"), Some(header), Some(value), 0) => {
			check_header_name(header)?;
			check_header_value(value)?;
			tgt.add.push((header.to_ascii_lowercase().into_bytes(), value.to_owned().into_bytes()));
			Ok(())
		},
		(Some("delete"), Some(_), Some(_), _) => fail!(HeaderParseError::Junk),
		(Some("delete"), None, _, _) => fail!(HeaderParseError::ExpectedHeaderName),
		(Some("set"), Some(_), Some(_), _) => fail!(HeaderParseError::Junk),
		(Some("set"), Some(_), None, _) => fail!(HeaderParseError::ExpectedHeaderValue),
		(Some("set"), None, _, _) => fail!(HeaderParseError::ExpectedHeaderName),
		(Some("append"), Some(_), Some(_), _) => fail!(HeaderParseError::Junk),
		(Some("append"), Some(_), None, _) => fail!(HeaderParseError::ExpectedHeaderValue),
		(Some("append"), None, _, _) => fail!(HeaderParseError::ExpectedHeaderName),
		(Some(x), _, _, _) => fail!(HeaderParseError::BadHeaderDirective(x.to_owned())),
		(None, _, _, _) => fail!(HeaderParseError::ExpectedHeaderDirective)
	}
}

fn bs<'a,T:Copy>(x: &'a (String, T)) -> (&'a str, T)
{
	(&x.0.deref(), x.1)
}

fn host_routing_data_new(name: &str, content: &[u8]) -> Result<HostRoutingData, String>
{
	let mut ipset = IpSet::new();
	let mut h2_trace = false;
	let mut h2_max_streams = None;
	let mut redir_fixup = false;
	let mut request_rewrite = HeaderRewrite{delete: HashSet::new(), add: Vec::new()};
	let mut response_rewrite = HeaderRewrite{delete: HashSet::new(), add: Vec::new()};
	let mut data = HostRoutingDataFactory::new();
	let mut gpdata = PathData::new();

	'lineloop: for (row, linec) in BLines::new(content).enumerate() {
		let mut line = Vec::new();
		for word in LineWordIterator::new(linec) { match word {
			Ok(w) => line.push(w.into_owned()),
			Err(err) => {
				syslog!(WARNING "{name} line {line}: {err}", line=row+1);
				continue 'lineloop;
			}
		}}
		if let Err(err) = call(||match split_head(&line) {
			(Some("insecure"), tail) => {
				let val = insecure_mode_parse(tail).map_err(|err|{
					format!("insecure: {err}")
				})?;
				Ok(gpdata.set_insecure_mode(Some(val)))
			},
			(Some("canonical-host"), tail) => {
				let val = parse_canonical_host(tail).map_err(|err|{
					format!("canonical-host: {err}")
				})?;
				Ok(data.set_canonicalhost(bs(&val)))
			},
			(Some("request-host"), tail) => {
				let val = parse_request_host(tail).map_err(|err|{
					format!("request-host: {err}")
				})?;
				Ok(gpdata.set_remaphost(&mut data, &val))
			},
			(Some("request-path"), tail) => {
				let val = parse_request_path(tail).map_err(|err|{
					format!("request-path: {err}")
				})?;
				Ok(gpdata.set_remappath(&mut data, &val))
			},
			(Some("path"), tail) => parse_path(tail, &mut data).map_err(|err|{
				format!("path: {err}")
			}),
			(Some("path-regex"), tail) => parse_path_regex(tail, &mut data).map_err(|err|{
				format!("path-regex: {err}")
			}),
			(Some("disable"), tail) => parse_disable(tail, &mut data).map_err(|err|{
				format!("disable: {err}")
			}),
			(Some("backend"), tail) => {
				let val = parse_backend(tail).map_err(|err|{
					format!("backend: {err}")
				})?;
				Ok(gpdata.set_backend(&mut data, &val))
			},
			(Some("fc-backend"), tail) => {
				let val = parse_backend(tail).map_err(|err|{
					format!("fc-backend: {err}")
				})?;
				Ok(gpdata.set_fc_backend(&mut data, &val))
			},
			(Some("request"), tail) => parse_header(&mut request_rewrite, tail).map_err(|err|{
				format!("request: {err}")
			}),
			(Some("response"), tail) => parse_header(&mut response_rewrite, tail).map_err(|err|{
				format!("response: {err}")
			}),
			(Some("root-redirect"), tail) => {
				let val = parse_root_redirect(tail).map_err(|err|{
					format!("root-redirect: {err}")
				})?;
				Ok(data.set_root_redirect(bs(&val)))
			},
			(Some("force-mime"), tail) => parse_force_mime(tail, &mut data).map_err(|err|{
				format!("force-mime: {err}")
			}),
			(Some("rewrite-location"), tail) => parse_rewrite_location(tail, &mut data).map_err(|err|{
				format!("rewrite-location: {err}")
			}),
			(Some("raw-host"), tail) if tail.len() == 0 => Ok(gpdata.set_raw_host(true)),
			(Some("raw-path"), tail) if tail.len() == 0 => Ok(gpdata.set_raw_path(true)),
			(Some("h2-trace"), tail) if tail.len() == 0 => Ok(h2_trace = true),
			(Some("h2-max-streams"), tail) => parse_h2_max_streams(&mut h2_max_streams, tail).
				map_err(|err|{
				format!("h2-max-streams: {err}")
			}),
			(Some("no-redirect-fixup"), tail) if tail.len() == 0 => Ok(redir_fixup = false),
			(Some("redirect-fixup"), tail) if tail.len() == 0 => Ok(redir_fixup = true),
			(Some("no-log-ok"), tail) if tail.len() == 0 => Ok(gpdata.set_logflags(!4)),
			(Some("no-log"), tail) if tail.len() == 0 => Ok(gpdata.set_logflags(0)),
			(Some("acl"), tail) => {
				let val = parse_acl(tail).map_err(|err|{
					format!("acl: {err}")
				})?;
				Ok(data.add_acl(val))
			},
			(Some("ip-allow"), tail) => parse_ipset(&mut ipset, tail, false).map_err(|err|{
				format!("ip-allow: {err}")
			}),
			(Some("ip-deny"), tail) => parse_ipset(&mut ipset, tail, true).map_err(|err|{
				format!("ip-deny: {err}")
			}),
			(Some(dir), _) => Err(format!("Bad directive '{dir}'")),
			(None, _) => Ok(()),	//Ignore empty lines.
		}) {
			syslog!(WARNING "{name} line {line}: {err}", line=row+1)
		}
	}
	//Only set ip filter mask if nontrivial.
	let ipset = ipset.freeze();
	if ipset.nontrivial() { data.set_ip_bans(Arc::new(ipset)); }
	data.set_request_rewrite(request_rewrite);
	data.set_response_rewrite(response_rewrite);
	data.set_global_path(gpdata);
	data.set_no_redirect_fixup();	//Default to no redirect fixup.
	if h2_trace { data.set_h2_trace_flag(); }
	if let Some(max_streams) = h2_max_streams { data.set_h2_max_streams(max_streams); }
	if redir_fixup { data.set_redirect_fixup(); }
	Ok(data.done())
}

fn routing_context_new() -> RoutingContext
{
	RoutingContext {
		hosts: RcuHashMap::new(),
		backends: RcuHashMap::new(),
	}
}

struct Sink(Arc<Mutex<Vec<(Vec<u8>, u32)>>>);

impl InotifyEventSink for Sink
{
	fn log_message(&self, msg: &str)
	{
		syslog!(WARNING "[config] inotify: {msg}");
	}
	fn inotify_event(&self, fname: &[u8], event: u32) -> bool
	{
		self.0.lock().push((fname.to_owned(), event));
		true
	}
}

pub struct RoutingManager
{
	context: RoutingContext,
	host_to_class: OneToSetMap<String, String>,
	class_to_config: HashMap<String, Arc<HostRoutingData>>,
	backend_to_config: HashMap<String, Arc<BackendData>>,
	host_changelist: HashMap<String, (Option<String>, PointInTime)>,
	class_changelist: HashMap<String, (Option<Arc<HostRoutingData>>, PointInTime)>,
	backend_changelist: HashMap<String, (Option<Arc<BackendData>>, PointInTime)>,
	first_change: Option<PointInTime>,
	delete_timeout: TimeUnit,
	watcher: Option<InotifyWatcher>,
	watcher_q: Arc<Mutex<Vec<(Vec<u8>, u32)>>>,
	hpath: PathBuf,
	oldfiles: Vec<String>,
	shosts: Option<PathBuf>,
}

impl RoutingManager
{
	pub fn new(hostpath: &Path, shosts: Option<PathBuf>) -> (RoutingManager, RoutingContext)
	{
		let wq = Arc::new(Mutex::new(Vec::new()));
		let watcher = match InotifyWatcher::new(hostpath, Box::new(Sink(wq.clone())),
			CREATED | CLOSED_WRITABLE | MOVED_FROM | MOVED_TO | DELETED) {
			Ok(x) => Some(x),
			Err(err) => {
				syslog!(ERROR "Unable to watch {hostpath}: {err}", hostpath=hostpath.display());
				None
			}
		};
		let ctx = routing_context_new();
		let mut rm = RoutingManager {
			context: ctx.clone(),
			host_to_class: OneToSetMap::new(),
			class_to_config: HashMap::new(),
			backend_to_config: HashMap::new(),
			host_changelist: HashMap::new(),
			class_changelist: HashMap::new(),
			backend_changelist: HashMap::new(),
			first_change: None,
			delete_timeout: TimeUnit::Seconds(5),
			watcher: watcher,
			watcher_q: wq,
			hpath: hostpath.to_path_buf(),
			oldfiles: Vec::new(),
			shosts: shosts,
		};
		//Run the managment loop once, so managment is initialized by the time managment thread is started,
		//as launch ACK is given immediately after, and this daemon is better be ready for service at that
		//point.
		rm._do_loop(true);
		(rm, ctx)
	}
	fn compose_change_list(&mut self, frun: &mut bool) -> Option<Vec<String>>
	{
		let mut secondary = Vec::new();
		if let (false, Some(watcher)) = (*frun, self.watcher.as_mut()) {
			let waitfor = match self.first_change {
				Some(fc) => Some(f_return!(fc.time_to(), None)),
				None => None,
			};
			let waitfor = waitfor.map(|d|(d.as_secs() as u32) * 1000 +
				d.subsec_nanos() / 100000);
			watcher.wait(waitfor);
			let mut wq = replace(self.watcher_q.lock().deref_mut(), Vec::new());
			for (fname, _) in wq.drain(..) {
				String::from_utf8(fname).map(|x|secondary.push(x)).ok();
			}
		} else {
			if !replace(frun, false) { sleep(Duration::from_secs(300)); }
			let mut oldfiles = replace(&mut self.oldfiles, Vec::new());
			let ditr = match read_dir(&self.hpath) {
				Ok(x) => x,
				Err(err) => {
					syslog!(ERROR "[config] Failed to read {hpath}: {err}",
						hpath=self.hpath.display());
					return None;
				}
			};
			for entry in ditr {
				let entry = match entry {
					Ok(x) => x,
					Err(err) => {
						syslog!(ERROR "[config] Failed to read entry from {hpath}: {err}",
							hpath=self.hpath.display());
						break;
					}
				};
				let s = f_continue!(entry.file_name().to_str()).to_owned();
				secondary.push(s.clone());
				self.oldfiles.push(s.clone());
			}
			for entry in oldfiles.drain(..) { secondary.push(entry); }
		}
		Some(secondary)
	}
	fn process_hostfile(&mut self, file: &str)
	{
		let host = file[5..].to_owned();
		match read_link(self.hpath.join(&file)) {
			Ok(x) => {
				let clazz = f_return!(x.to_str()).to_owned();
				if clazz.deref().contains("/") { return; }
				syslog!(NOTICE "[config] Adding host {host} -> {clazz}");
				self.queue_host_change(host, Some(clazz));
			},
			Err(err) => if err.kind() == IoErrorKind::NotFound {
				syslog!(NOTICE "[config] Removing host {host}");
				self.queue_host_change(host, None);
			} else {
				syslog!(ERROR "[config] Failed to read link {file}: {err}");
				return;
			}
		}
	}
	fn process_backendfile(&mut self, file: &str, shosts: Option<&Path>)
	{
		let backend = file[8..].to_owned();
		let mut content = Vec::new();
		if let Err(err) = File::open(self.hpath.join(&file)).and_then(|mut fp|{
			fp.read_to_end(&mut content)
		}) {
			if err.kind() == IoErrorKind::NotFound {
				syslog!(NOTICE "[config] Removing backend {backend}");
				self.queue_backend_change(backend, None);
				return;
			} else {
				syslog!(ERROR "[config] Failed to read {file}: {err}");
				return;
			}
		};
		//Catch any panics.
		let cfg = match catch_unwind(AssertUnwindSafe(||{
			backend_data_new(&backend, &content, shosts)
		})) {
			Ok(x) => x,
			Err(_) => return syslog!(CRITICAL "[config] Internal error: Parser CRASHED on {file}")
		};
		let cfg = match cfg {
			Ok(x) => Arc::new(x),
			Err(err) => return syslog!(ERROR "[config] Failed to parse {file}: {err}")
		};
		syslog!(NOTICE "[config] Adding backend {backend}: {cfg}");
		self.queue_backend_change(backend, Some(cfg));
	}
	fn process_classfile(&mut self, file: &str)
	{
		let clazz = file.to_owned();
		let mut content = Vec::new();
		if let Err(err) = File::open(self.hpath.join(&file)).and_then(|mut fp|{
			fp.read_to_end(&mut content)
		}) {
			if err.kind() == IoErrorKind::NotFound {
				syslog!(NOTICE "[config] Removing class {clazz}");
				self.queue_class_change(clazz, None);
				return;
			} else {
				syslog!(ERROR "[config] Failed to read {file}: {err}");
				return;
			}
		};
		//Catch any panics.
		let cfg = match catch_unwind(AssertUnwindSafe(||{
			host_routing_data_new(&clazz, &content)
		})) {
			Ok(x) => x,
			Err(_) => return syslog!(CRITICAL "[config] Internal error: Parser CRASHED on {file}")
		};
		let cfg = match cfg {
			Ok(x) => Arc::new(x),
			Err(err) => return syslog!(ERROR "[config] Failed to parse {file}: {err}")
		};
		syslog!(NOTICE "[config] Adding class {file}: {cfg}");
		self.queue_class_change(clazz, Some(cfg));
	}
	pub fn do_loop(&mut self)
	{
		self._do_loop(false);
	}
	fn _do_loop(&mut self, only_once: bool)
	{
		let mut frun = only_once;
		loop {
			self.run_changes();
			let secondary = f_continue!(self.compose_change_list(&mut frun));
			for file in secondary.iter() {
				//Ignore any file that starts with . or ends with ~.
				if file.starts_with(".") || file.ends_with("~") { continue; }
				if file.starts_with("host.") {
					self.process_hostfile(file);
				} else if file.starts_with("backend.") {
					let shosts = self.shosts.clone();
					self.process_backendfile(file, shosts.as_deref());
				} else {
					self.process_classfile(file);
				}
			}
			if only_once { break; }
		}
	}
	fn queue_host_change(&mut self, host: String, clazz: Option<String>)
	{
		let timeout = if clazz.is_none() && self.watcher.is_some() {
			//The holddowns only exist with watcher.
			self.delete_timeout
		} else {
			TimeUnit::Seconds(0)
		};
		let holddown = PointInTime::from_now(timeout);
		self.host_changelist.insert(host, (clazz, holddown));
		self.first_change = Some(min(self.first_change.unwrap_or(holddown), holddown));
	}
	fn queue_class_change(&mut self, clazz: String, config: Option<Arc<HostRoutingData>>)
	{
		let timeout = if config.is_none() { self.delete_timeout } else { TimeUnit::Seconds(0) };
		let holddown = PointInTime::from_now(timeout);
		self.class_changelist.insert(clazz, (config, holddown));
		self.first_change = Some(min(self.first_change.unwrap_or(holddown), holddown));
	}
	fn queue_backend_change(&mut self, backend: String, config: Option<Arc<BackendData>>)
	{
		let timeout = if config.is_none() { self.delete_timeout } else { TimeUnit::Seconds(0) };
		let holddown = PointInTime::from_now(timeout);
		self.backend_changelist.insert(backend, (config, holddown));
		self.first_change = Some(min(self.first_change.unwrap_or(holddown), holddown));
	}
	fn run_changes(&mut self)
	{
		let now = PointInTime::now();
		let mut min_failed = None;
		//Compose list of hosts and classes to execute.
		let mut execute_hosts = Vec::new();
		let mut execute_classes = Vec::new();
		let mut execute_backends = Vec::new();
		for (name, &(_, holddown)) in self.host_changelist.iter() {
			if holddown <= now {
				execute_hosts.push(name.clone());
			} else {
				min_failed = Some(min(min_failed.unwrap_or(holddown), holddown));
			}
		}
		for (name, &(_, holddown)) in self.class_changelist.iter() {
			if holddown <= now {
				execute_classes.push(name.clone());
			} else {
				min_failed = Some(min(min_failed.unwrap_or(holddown), holddown));
			}
		}
		for (name, &(_, holddown)) in self.backend_changelist.iter() {
			if holddown <= now {
				execute_backends.push(name.clone());
			} else {
				min_failed = Some(min(min_failed.unwrap_or(holddown), holddown));
			}
		}
		for backend in execute_backends.drain(..) { match self.backend_changelist.remove(&backend) {
			Some((Some(config), _)) => {
				self.backend_to_config.insert(backend.clone(), config.clone());
				self.context.backends.put_arc(backend.clone(), config.clone());
			},
			Some((None, _)) => {
				self.backend_to_config.remove(&backend);
				self.context.backends.delete(&backend);
			},
			None => (),
		}}
		for clazz in execute_classes.drain(..) { match self.class_changelist.remove(&clazz) {
			Some((Some(config), _)) => {
				warn_about_backend(&clazz.deref(), config.get_backend_nt(),
					&self.backend_to_config, true);
				for path in config.iterate_paths() {
					warn_about_backend(&clazz.deref(), path.1.get_backend_nt(&config),
						&self.backend_to_config, false);
				}
				self.class_to_config.insert(clazz.clone(), config.clone());
				if let Some(preimage) = self.host_to_class.preimages(&clazz) {
					for host in preimage.iter() {
						self.context.hosts.put_arc(host.clone(), config.clone());
					}
				}
			},
			Some((None, _)) =>  {
				self.class_to_config.remove(&clazz);
				if let Some(preimage) = self.host_to_class.preimages(&clazz) {
					for host in preimage.iter() { self.context.hosts.delete(host); }
				}
			},
			None => (),
		}}
		for host in execute_hosts.drain(..) { match self.host_changelist.remove(&host) {
			Some((Some(clazz), _)) => {
				self.host_to_class.insert(host.clone(), clazz.clone());
				if let Some(config) = self.class_to_config.get(&clazz) {
					self.context.hosts.put_arc(host.clone(), config.clone());
				} else {
					syslog!(WARNING "[config] Host {host}: Nonexistent class {clazz}");
					self.context.hosts.delete(&host);
				}
			},
			Some((None, _)) => {
				self.host_to_class.remove(&host);
				self.context.hosts.delete(&host);
			}
			None => (),
		}}
		self.first_change = min_failed;
	}
}

fn warn_about_backend(clazz: &str, backend: Option<&str>, b2c: &HashMap<String, Arc<BackendData>>,
	warn_none: bool)
{
	if let Some(backend) = backend {
		//Empty backend and backends starting with "static:" are magic.
		if backend != "" && !backend.starts_with("static:") && !b2c.contains_key(backend) {
			syslog!(WARNING "[config] Class {clazz}: Nonexistent backend {backend}");
		}
	} else if warn_none{
		syslog!(WARNING "[config] Class {clazz}: No backend");
	}
}

///One-to-set map.
///
///One can add mappings A -> B to this map, and it can then return what are the A that are preimages of given B.
pub struct OneToSetMap<A:Sized+Hash+Eq+Clone,B:Sized+Hash+Eq+Clone>
{
	a_to_b: HashMap<A,B>,
	b_to_a: HashMap<B,HashSet<A>>,
}

impl<A:Sized+Hash+Eq+Clone,B:Sized+Hash+Eq+Clone> OneToSetMap<A,B>
{
	///Create new.
	pub fn new() -> OneToSetMap<A,B>
	{
		OneToSetMap {
			a_to_b: HashMap::new(),
			b_to_a: HashMap::new(),
		}
	}
	///Return set of preimages of `b`.
	pub fn preimages<'a, Q:?Sized+Hash+Eq>(&'a self, b: &Q) -> Option<&'a HashSet<A>> where B:Borrow<Q>
	{
		self.b_to_a.get(b)
	}
	///Remove `a` from map.
	pub fn remove<'a, Q:?Sized+Hash+Eq>(&mut self, a: &Q) -> Option<B> where A:Borrow<Q>
	{
		if let Some(b) = self.a_to_b.remove(a) {
			let e = self.b_to_a.get_mut(&b).map(|x|{
				x.remove(a);
				x.len() == 0
			}).unwrap_or(false);
			if e { self.b_to_a.remove(&b); }	//Remove useless entries.
			Some(b)
		} else {
			None
		}
	}
	///Insert `a`, mapped to `b` to map.
	pub fn insert(&mut self, a: A, b: B) -> Option<B>
	{
		let r = self.remove(&a);	//Remove old entry first.
		self.a_to_b.insert(a.clone(), b.clone());
		self.b_to_a.entry(b).or_insert_with(||HashSet::new()).insert(a);
		r
	}
}
