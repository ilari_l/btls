use crate::ConfigT;
use crate::bitmask::Bitfield;
use crate::connection::BackendName;
use crate::connection::Connector;
use crate::connection_lowlevel::CID;
use crate::connection_midlevel::BackwardInterface;
use crate::connection_midlevel::BackwardInterfaceError;
use crate::connection_midlevel::ForwardInterface;
use crate::connection_midlevel::Interrupter;
use crate::connection_midlevel::ReaderWriterX;
use crate::connection_midlevel::WriterB;
use crate::connection_midlevel::WriterMC;
use crate::connection_midlevel::WriterS;
use crate::connection_midlevel::WriterSC;
use crate::connection_midlevel::WriterXC;
use crate::connection_midlevel::WriterXC2;
use crate::connection_in_h1::Connection as ConnectionInH1;
use crate::connection_in_h1::ConnectionMode as ConnectionModeH1;
use crate::connection_in_h2::Connection as ConnectionInH2;
use crate::connection_midlevel::SinkError;
use crate::connection_out::BackendError;
use crate::connection_out::Connection as ConnectionOut;
use crate::connection_out::ConnectionParameters as OutConnectionParameters;
use crate::headerblock::HeaderBlock;
use crate::http::ConnectionPropertiesI;
use crate::http::RequestInfo;
use crate::http::ClientInfoIn;
use crate::http::InternalResponse;
use crate::http::RequestWriteReturn;
use crate::metrics_dp::IncomingConnectionState;
use crate::routing_dp::HostRoutingDataRef;
use crate::staticfile::Connection as StaticFile;
use crate::staticfile::ConnectionParameters as StaticConnectionParameters;
use crate::utils::Buffer;
use crate::utils::cia_to_ip;
use btls_aux_fail::ResultExt;
use btls_aux_http::http::Sink as HttpSink;
use btls_aux_memory::Hexdump;
use btls_aux_memory::iterate_mostly_utf8_fragments;
use btls_aux_memory::MUtf8StringFragment;
use btls_aux_serialization::Source;
use btls_aux_serialization::VectorLength;
use btls_aux_time::PointInTime;
use btls_aux_time::TimeUnit;
use btls_daemon_helper_lib::AddressHandle;
use btls_daemon_helper_lib::cloned;
use btls_daemon_helper_lib::ConnectionInfo;
use btls_daemon_helper_lib::cow;
use btls_daemon_helper_lib::is_fatal_error;
use btls_daemon_helper_lib::SocketAddrEx;
use btls_util_logging::ConnId;
use std::borrow::Cow;
use std::io::Error as IoError;
use std::io::ErrorKind as IoErrorKind;
use std::mem::replace;
use std::path::PathBuf;
use std::rc::Rc;

const MAX_WRITE: usize = 131072;	//128kB.

struct ReadProxy
{
	timeout: PointInTime,
	buffer: Vec<u8>,
	tgtsize: usize,
	conntrack: Option<IncomingConnectionState>,
	unconditional: bool,
}

impl ReadProxy
{
	pub fn new(proxy_timeout: TimeUnit, conntrack: IncomingConnectionState, force_proxy: bool) -> ReadProxy
	{
		ReadProxy {
			timeout: PointInTime::from_now(proxy_timeout),
			buffer: Vec::new(),
			tgtsize: 16,	//Initially it is 16.
			conntrack: Some(conntrack),
			//If force_proxy is set, set unconditional, so the address change does not get
			//printed into logs.
			unconditional: force_proxy,
		}
	}
}

struct WriteOrBuffer<'a,T:WriterSC>
{
	write_down: bool,
	shutdown_flag: &'a mut bool,
	buffer: &'a mut Buffer,
	underlying: &'a mut T,
}

impl<'a,T:WriterSC> WriterB for WriteOrBuffer<'a,T>
{
	fn get_id(&self) -> ConnId { self.underlying.get_id() }
	fn aborted(&self) -> bool { self.underlying.aborted() }
	fn interrupt(&mut self) { self.underlying.interrupt() }
	fn is_empty(&self) -> bool { self.underlying.is_empty() }
	fn shutdown(&mut self) -> Result<(), IoError>
	{
		if self.write_down {
			self.underlying.shutdown()
		} else {
			Ok(*self.shutdown_flag = true)
		}
	}
	fn abort(&mut self, err: SinkError) { self.underlying.abort(err) }
}

impl<'a,T:WriterSC> WriterS for WriteOrBuffer<'a,T>
{
}

impl<'a,T:WriterSC> HttpSink for WriteOrBuffer<'a,T>
{
	type Error = IoError;
	fn write(&mut self, buf: &[&[u8]]) -> Result<(), IoError>
	{
		if self.write_down {
			self.underlying.write(buf)
		} else {
			//Not ready, write to buffer.
			for frag in buf.iter() {
				self.buffer.append(frag).
					map_err(|_|IoError::new(IoErrorKind::Other, "Send-Q overflow"))?;
			}
			Ok(())
		}
	}
}

macro_rules! wrap_write_slave
{
	($selfx:ident $w:ident) => {{
		let is_ready = $w.is_ready();
		WriteOrBuffer {
			buffer: &mut $selfx.buffer,
			shutdown_flag: &mut $selfx.shutdown_flag,
			underlying: $w,
			write_down: is_ready,
		}
	}}
}

enum ConnType
{
	ReadingProxyHeaders(ReadProxy),
	InH1(ConnectionInH1),
	InH2(ConnectionInH2),
	Out(ConnectionOut),
	Static(StaticFile),
}

pub struct LogicalConnection
{
	buffer: Buffer,
	shutdown_flag: bool,
	error: Option<Cow<'static, str>>,
	inner: ConnType,
}

impl LogicalConnection
{
	pub fn get_slave_list(&self) -> Vec<CID>
	{
		match &self.inner {
			&ConnType::InH1(ref x) => x.get_active_slaves(),
			&ConnType::InH2(ref x) => x.get_active_slaves(),
			_ => Vec::new()
		}
	}
	pub fn get_master_of_slave(&self) -> (CID, u32)
	{
		match &self.inner {
			&ConnType::Out(ref slave) => (slave.get_master(), slave.get_sid()),
			&ConnType::Static(ref slave) => (slave.get_master(), slave.get_sid()),
			_ => (CID::invalid(), 0)
		}
	}
	pub fn notify_slave_that_master_died(&mut self) -> Result<(), Cow<'static, str>>
	{
		match &mut self.inner {
			&mut ConnType::Out(ref mut x) => x.write_forward_error("Master connection died"),
			&mut ConnType::Static(ref mut x) => x.write_forward_error("Master connection died"),
			_ => return Ok(()),	//This is actually a master.
		}.map_err(|err|{
			cow!(F "{err}")
		})
	}
	pub fn notify_master_that_slave_died(&mut self, cid: CID, sid: u32, connected: bool, irq: Interrupter)
	{
		match &mut self.inner {
			&mut ConnType::InH1(ref mut x) => x.notify_died(cid, irq, connected),
			&mut ConnType::InH2(ref mut x) => x.notify_died(cid, sid, irq, connected),
			_ => (),	//Ignore.
		}
	}
	pub fn backward_has_data(&mut self, req: &RequestInfo, irq: Interrupter)
	{
		match &mut self.inner {
			&mut ConnType::InH1(ref mut x) => x.backward_has_data(irq, req),
			&mut ConnType::InH2(ref mut x) => x.backward_has_data(irq, req),
			_ => (),
		};
	}
	pub fn forward_unblock(&mut self, req: &RequestInfo, irq: Interrupter)
	{
		match &mut self.inner {
			&mut ConnType::InH1(ref mut x) => x.forward_unblock(irq, req),
			&mut ConnType::InH2(ref mut x) => x.forward_unblock(irq, req),
			_ => (),
		};
	}
	pub fn request_data<S:RequestWriteReturn>(&mut self, sink: &mut S, irq: Interrupter) -> bool
	{
		match &mut self.inner {
			&mut ConnType::Out(ref mut x) => x.request_data(sink, irq),
			&mut ConnType::Static(ref mut x) => x.request_data(sink, irq),
			_ => false	//Not a valid backend
		}
	}
	pub fn notify_quit(&mut self, irq: Interrupter) -> Result<(), Cow<'static, str>>
	{
		match &mut self.inner {
			&mut ConnType::InH1(ref mut x) => x.notify_quit(irq),
			&mut ConnType::InH2(ref mut x) => x.notify_quit(irq),
			_ => (),
		};
		Ok(())
	}
	pub fn notify_connected(&mut self, cid: CID, flush_to: &mut dyn WriterS) -> Result<bool, Cow<'static, str>>
	{
		let mut underflow = false;
		//Flush the buffer if it has data in it.
		if self.buffer.has_data() {
			//Since this sends all, if there is any data, underflow happens.
			underflow = true;
			flush_to.write(&[self.buffer.as_slice()]).map_err(|err|{
				cow!(F "{err}")
			})?;
		}
		self.buffer.clear_drop();	//Do not need this anymore.
		if self.shutdown_flag {
			flush_to.shutdown().map_err(|err|{
				cow!(F "{err}")
			})?;
		}
		match &mut self.inner {
			&mut ConnType::Out(ref mut x) => x.notify_connected(cid),
			&mut ConnType::Static(ref mut x) => x.notify_connected(cid),
			_ => (),	//Masters always assumed connected.
		}
		Ok(underflow)
	}
	pub fn need_read(&self) -> bool
	{
		match &self.inner {
			&ConnType::ReadingProxyHeaders(_) => true,
			&ConnType::InH1(ref x) => x.need_read(),
			&ConnType::InH2(ref x) => x.need_read(),
			&ConnType::Out(ref x) => x.need_read(),
			&ConnType::Static(_) => false,
		}
	}
	pub fn write_forward_error(&mut self, error: &str, mut irq: Interrupter)
	{
		//The master connection is failing anyway, so no point in signaling it back.
		if let Err(err) = match &mut self.inner {
			&mut ConnType::Out(ref mut x) => x.write_forward_error(error),
			&mut ConnType::Static(ref mut x) => x.write_forward_error(error),
			_ => return
		} {
			//write_forward_error() handler in logical connection decided that the connection can
			//not go on. Set connection error and fire IRQ in order to tear the connection down.
			self.error = Some(cow!(F "{err}"));
			irq.interrupt();
		}
	}
	pub fn write_forward_data(&mut self, w: &mut impl WriterSC, block: &[u8]) -> Result<bool, BackendError>
	{
		let r = {
			let mut w = wrap_write_slave!(self w);
			match &mut self.inner {
				&mut ConnType::Out(ref mut c) => c.write_forward_data(&mut w, block),
				&mut ConnType::Static(ref mut c) => c.write_forward_data(&mut w, block),
				_ => fail!(BackendError::UnsupportedBackend)
			}
		};
		r.map_err(|err|{
			cow!(F "{err}")
		}).and_then(|r|{
			w.take_error_or(r)
		}).map_err(|err|{
			//write_forward_data() handler in logical connection decided that the connection
			//can not go on, or the connection write failed. Set connection error and fire
			//IRQ in order to tear the connection down. Signal that the backend connection was
			//lost.
			self.error = Some(err);
			w.interrupt();
			BackendError::ConnectionLost
		})
	}
	pub fn write_forward_trailers(&mut self, w: &mut impl WriterSC, block: &mut HeaderBlock) ->
		Result<(), BackendError>
	{
		let r = {
			let mut w = wrap_write_slave!(self w);
			match &mut self.inner {
				&mut ConnType::Out(ref mut c) => c.write_forward_trailers(&mut w, block),
				&mut ConnType::Static(ref mut c) => c.write_forward_trailers(&mut w, block),
				_ => fail!(BackendError::UnsupportedBackend)
			}
		};
		r.map_err(|err|{
			cow!(F "{err}")
		}).and_then(|r|{
			w.take_error_or(r)
		}).map_err(|err|{
			//write_forward_trailers() handler in logical connection decided that the connection
			//can not go on, or the connection write failed. Set connection error and fire
			//IRQ in order to tear the connection down. Signal that the backend connection was
			//lost.
			self.error = Some(err);
			w.interrupt();
			BackendError::ConnectionLost
		})?;
		Ok(())
	}
	pub fn write_forward_headers(&mut self, w: &mut impl WriterSC, block: &mut HeaderBlock, req: &RequestInfo,
		hostdata: HostRoutingDataRef, parent: ConnId) -> Result<(), BackendError>
	{
		let r = {
			let mut w = wrap_write_slave!(self w);
			match &mut self.inner {
				&mut ConnType::Out(ref mut c) => c.write_forward_headers(&mut w, block, req,
					hostdata, parent),
				&mut ConnType::Static(ref mut c) =>
					c.write_forward_headers(&mut w, block, req, hostdata, parent),
				_ => fail!(BackendError::UnsupportedBackend)
			}
		};
		let mut r = match r {
			Err(Err(err)) => Err(Err(cow!(F "{err}"))),
			Err(Ok(x)) => Err(Ok(x)),
			Ok(x) => Ok(x),
		};
		//If writer took fault, propagate it as hard error. This overrides any hard or soft error from
		//logical write_forward_headers().
		if let Err(e) = w.take_error_or(()) { r = Err(Err(e)); }
		match r {
			Ok(_) => return Ok(()),
			Err(Ok(x)) => fail!(x), //Return soft errors as is.
			Err(Err(e)) => {
				//Set connection error and raise interrupt so this connection is garbage-
				//collected. Signal to master that backend connection was lost.
				self.error = Some(e);
				w.interrupt();
				fail!(BackendError::ConnectionLost);
			}
		}
	}
	pub fn write_backward_headers(&mut self, w: &mut impl WriterMC, block: &mut HeaderBlock, req: &RequestInfo,
		backend: ConnId, backend_name: BackendName) -> Result<(), ()>
	{
		let r = match &mut self.inner {
			&mut ConnType::InH1(ref mut c) =>
				c.write_backward_headers(w, block, req, backend, backend_name),
			&mut ConnType::InH2(ref mut c) =>
				c.write_backward_headers(w, block, req, backend, backend_name),
			_ => Ok(false),
		};
		//NormalClose and QuitKill are special in that those should not cause write_backward_headers() to
		//fail. Those two still cause the master to close.
		let not_an_error = match &r {
			&Err(BackwardInterfaceError::NormalClose) => true,
			&Err(BackwardInterfaceError::QuitKill) => true,
			_ => false
		};
		let r = r.map_err(|err|{
			cow!(F "{err}")
		}).and_then(|r|{
			w.take_error_or(r)
		}).map_err(|err|{
			self.error = Some(err)
		}).unwrap_or(false);
		//If error got set, raise IRQ so the connection dies.
		if self.error.is_some() { w.interrupt(); }
		if r || not_an_error { Ok(()) } else { Err(()) }
	}
	pub fn write_backward_trailers(&mut self, w: &mut impl WriterMC, block: &mut HeaderBlock,
		req: &RequestInfo)
	{
		let r = match &mut self.inner {
			&mut ConnType::InH1(ref mut c) => c.write_backward_trailers(w, block, req),
			&mut ConnType::InH2(ref mut c) => c.write_backward_trailers(w, block, req),
			_ => Ok(()),
		};
		r.map_err(|err|{
			cow!(F "{err}")
		}).and_then(|r|{
			w.take_error_or(r)
		}).map_err(|err|{
			self.error = Some(err);
		}).ok();
		//If error got set, raise IRQ so the connection dies.
		if self.error.is_some() { w.interrupt(); }
	}
	pub fn write_backward_data(&mut self, w: &mut impl WriterMC, block: &[u8], req: &RequestInfo) ->
		Result<usize, ()>
	{
		let r = match &mut self.inner {
			&mut ConnType::InH1(ref mut c) => c.write_backward_data(w, block, req),
			&mut ConnType::InH2(ref mut c) => c.write_backward_data(w, block, req),
			_ => Ok(None),
		};
		let r = r.map_err(|err|{
			cow!(F "{err}")
		}).and_then(|r|{
			w.take_error_or(r)
		}).map_err(|e|{
			self.error = Some(e)
		}).unwrap_or(None);
		//If error got set, raise IRQ so the connection dies.
		if self.error.is_some() { w.interrupt(); }
		dtry!(NORET r)
	}
	pub(crate) fn write_backward_error(&mut self, w: &mut impl WriterMC, req: &RequestInfo,
		status: InternalResponse)
	{
		let r = match &mut self.inner {
			&mut ConnType::InH1(ref mut c) => c.write_backward_error(w, req, status),
			&mut ConnType::InH2(ref mut c) => c.write_backward_error(w, req, status),
			_ => Ok(()),
		};
		r.map_err(|err|{
			cow!(F "{err}")
		}).and_then(|r|{
			w.take_error_or(r)
		}).map_err(|err|{
			self.error = Some(err)
		}).ok();
		//If error got set, raise IRQ so the connection dies.
		if self.error.is_some() { w.interrupt(); }
	}
	pub fn send_h2_idle_goaway(&mut self, w: &mut impl WriterXC) -> Result<(), Cow<'static, str>>
	{
		if let &mut ConnType::InH2(ref mut c) = &mut self.inner {
			c.send_idle_goaway(w, false)?;
		}
		Ok(())
	}
	pub fn is_master_idle(&self) -> bool
	{
		match &self.inner {
			&ConnType::InH1(ref c) => c.is_idle(),
			&ConnType::InH2(ref c) => c.is_idle(),
			//Slave types can never be master idle.
			_ => false
		}
	}
	pub fn handle_request_event(&mut self, w: &mut impl WriterXC) -> Result<(), Cow<'static, str>>
	{
		let r = match &mut self.inner {
			&mut ConnType::InH1(ref mut c) => c.handle_request_event(w),
			&mut ConnType::InH2(ref mut c) => c.handle_request_event(w),
			&mut ConnType::Out(ref mut c) => c.handle_request_event(w),
			&mut ConnType::Static(ref mut c) => c.handle_request_event(w.get_id()),
			_ => Ok(()),
		};
		r.and_then(|r|{
			w.take_error_or(r)
		})
	}
	pub fn handle_write_underflow(&mut self, w: &mut impl WriterXC2) -> Result<(), Cow<'static, str>>
	{
		let r = match &mut self.inner {
			&mut ConnType::InH1(ref mut c) => c.handle_write_underflow(w),
			&mut ConnType::InH2(ref mut c) => c.handle_write_underflow(w),
			&mut ConnType::Out(ref mut c) => c.handle_write_underflow(w),
			_ => Ok(()),    //Should never be called on ReadingProxyHeaders or Static.
		};
		r.and_then(|r|{
			w.take_error_or(r)
		})
	}
	pub fn handle_timed_event(&mut self, now: PointInTime, w: &mut impl WriterXC) ->
		Result<(), Cow<'static, str>>
	{
		let r = match &mut self.inner {
			&mut ConnType::ReadingProxyHeaders(ref p) => {
				fail_if!(p.timeout > now, cow!("Timed out waiting for PROXY headers"));
				Ok(())
			},
			&mut ConnType::InH1(_) => Ok(()),
			&mut ConnType::InH2(_) => Ok(()),
			&mut ConnType::Out(ref mut c) => c.handle_timed_event(now, w),
			&mut ConnType::Static(ref mut c) => c.handle_timed_event(now),
		};
		r.and_then(|r|{
			w.take_error_or(r)
		})
	}
	pub fn get_next_timed_event(&self) -> Option<PointInTime>
	{
		match &self.inner {
			&ConnType::ReadingProxyHeaders(ref p) => Some(p.timeout.clone()),
			&ConnType::InH1(_) => None,
			&ConnType::InH2(_) => None,
			&ConnType::Out(ref c) => c.get_next_timed_event(),
			&ConnType::Static(ref c) => c.get_next_timed_event(),
		}
	}
	pub fn transmit_never_timeouts(&self) -> bool
	{
		//Currently only static never times out.
		match &self.inner {
			&ConnType::Static(_) => true,
			_ => false,
		}
	}
	pub fn handle_read_event(&mut self, w: &mut impl ReaderWriterX, wc: &mut Connector,
		socketinfo: &mut ConnectionInfo, mut irq: Interrupter, g: ConfigT) -> Result<(), Cow<'static, str>>
	{
		let mut newinner = None;
		match &mut self.inner {
			&mut ConnType::ReadingProxyHeaders(ref mut p) => {
				//While not at target size, read more.
				if p.buffer.len() < p.tgtsize {
					let osize = p.buffer.len();
					p.buffer.resize(p.tgtsize, 0);
					match w.read(&mut p.buffer[osize..]) {
						Ok(None) => fail!(cow!("Received EOF reading PROXY headers")),
						Ok(Some(amt)) => p.buffer.resize(osize + amt, 0),
						Err(err) => if is_fatal_error(&err) {
							attribute_error(g, true);
							fail!(cow!(F "Read error: {err}"));
						} else {
							p.buffer.resize(osize, 0)
						}
					};
				}
				if p.buffer.len() == 16 && p.tgtsize == 16 {
					fail_if!(&p.buffer[..12] != b"\r\n\r\n\0\r\nQUIT\n", "Bad PROXY magic");
					//Read the extended target size.
					let esize = p.buffer[14] as usize * 256 + p.buffer[15] as usize;
					p.tgtsize += esize;
				}
				if p.buffer.len() == p.tgtsize {
					//Do not print address change for unconditional PROXY headers.
					let extended = socketinfo.from_proxyv2(&p.buffer, p.unconditional).
						ok_or_else(||{
						cow!("Bad PROXY header")
					})?;
					//Set address, now that it is known.
					wc.set_source(&socketinfo.src);
					let extended = handle_extended_proxy_data(extended).map_err(|err|{
						cow!(F "Bad PROXY header: {err}")
					})?;
					fail_if!(extended.flags.get(PflagUnknownAlpn),
						cow!("Unrecognized ALPN"));
					fail_if!(extended.flags.get(PflagH2) && !extended.flags.get(PflagSecure),
						cow!("HTTP/2 is secure-only"));
					//Bump the transfer timeouts, in order to not count the PROXY execution
					//time.
					w.bump_timeout();
					let conntrack = replace(&mut p.conntrack, None).ok_or_else(||{
						cow!("Internal Error: Conntrack object AWOL")
					})?;
					let info = ClientInfoIn {
						client_ip: Rc::new(cia_to_ip(&socketinfo.src)),
						//Client CN and SPKIHASH are not available for plaintext connections.
						client_cn: extended.client_cn.map(|x|Rc::new(x)),
						client_spkihash: extended.client_spkihash.map(|x|Rc::new(x)),
						properties: ConnectionPropertiesI::unknown(),
						requested_cert: true,   //Assume yes.
					};
					newinner = Some(if extended.flags.get(PflagH2) {
						let (trace, max_streams) = if_enable_tracing(g,
							extended.sni.as_deref());
						//Send interrupt so SETTINGS gets sent.
						irq.interrupt();
						ConnType::InH2(ConnectionInH2::new(info, conntrack, g, trace,
							max_streams))
					} else {
						//This is never chained.
						let mode = if extended.flags.get(PflagSecure) {
							increment_metric!(conn_https g);
							ConnectionModeH1::Secure
						} else {
							increment_metric!(req_http g);
							ConnectionModeH1::Insecure
						};
						ConnType::InH1(ConnectionInH1::new(mode, info, conntrack, g))
					});
				}
				Ok(())
			},
			&mut ConnType::InH1(ref mut c) => c.handle_read_event(w, wc),
			&mut ConnType::InH2(ref mut c) => c.handle_read_event(w, wc),
			&mut ConnType::Out(ref mut c) => c.handle_read_event(w),
			&mut ConnType::Static(_) => Ok(()),     //Should be never called.
		}?;
		//No state replace if writer fails.
		if w.aborted() { return Ok(()); }
		//Replace inner if needed.
		if let Some(newinner) = newinner { self.inner = newinner; }
		Ok(())
	}
	pub fn set_error(&mut self, err: Cow<'static, str>, overwrite: bool)
	{
		if overwrite || self.error.is_none() { self.error = Some(err); }
	}
	pub fn get_error(&self) -> Result<(), Cow<'static, str>>
	{
		match &self.error {
			&Some(ref x) => fail!(x.clone()),
			&None => Ok(()),	//Go on.
		}
	}
	fn new(ctype: ConnType) -> LogicalConnection
	{
		LogicalConnection {
			buffer: Buffer::new_bounded(MAX_WRITE),
			shutdown_flag: false,
			inner: ctype,
			error: None,
		}
	}
	pub fn new_h1(mode: ConnectionModeH1, info: ClientInfoIn, g: ConfigT) -> LogicalConnection
	{
		let conntrack = IncomingConnectionState::new_proxy(g);
		LogicalConnection::new(ConnType::InH1(ConnectionInH1::new(mode, info, conntrack, g)))
	}
	pub fn new_h2(info2: ClientInfoIn, sni: &[u8], g: ConfigT) -> LogicalConnection
	{
		let conntrack = IncomingConnectionState::new_proxy(g);
		let (trace_flag, max_streams) = if_enable_tracing(g, Some(sni));
		LogicalConnection::new(ConnType::InH2(ConnectionInH2::new(info2, conntrack, g, trace_flag,
			max_streams)))
	}
	pub fn new_proxy(proxy_timeout: TimeUnit, force_proxy: bool, g: ConfigT) -> LogicalConnection
	{
		let conntrack = IncomingConnectionState::new_proxy(g);
		LogicalConnection::new(ConnType::ReadingProxyHeaders(ReadProxy::new(proxy_timeout, conntrack,
			force_proxy)))
	}
	pub fn new_out(params: OutConnectionParameters, address_handle: Option<&AddressHandle>, g: ConfigT) ->
		LogicalConnection
	{
		let address_handle = address_handle.map(cloned);
		LogicalConnection::new(ConnType::Out(ConnectionOut::new(params, address_handle, g)))
	}
	pub fn new_static(params: StaticConnectionParameters, g: ConfigT) -> LogicalConnection
	{
		LogicalConnection::new(ConnType::Static(StaticFile::new(params, g)))
	}
	pub fn new_static_file(params: PathBuf, uncompressed: bool, is_error: bool, g: ConfigT) -> LogicalConnection
	{
		LogicalConnection::new(ConnType::Static(StaticFile::new_file(params, !uncompressed, is_error, g)))
	}
	pub fn __set_output_address(&mut self, addr: &SocketAddrEx)
	{
		//Only Out supports setting address.
		if let &mut ConnType::Out(ref mut obj) = &mut self.inner {
			let ahandle = AddressHandle::new(addr);
			obj.__set_address_handle(ahandle);
		}
	}
}

define_bitfield_type!(ProxyExtFlagsTag, u8);
define_bitfield_bit!(PflagUnknownAlpn, ProxyExtFlagsTag, 0);
define_bitfield_bit!(PflagH2, ProxyExtFlagsTag, 1);
define_bitfield_bit!(PflagSecure, ProxyExtFlagsTag, 2);


struct ProxyExtended
{
	flags: Bitfield<ProxyExtFlagsTag>,
	client_cn: Option<String>,
	client_spkihash: Option<String>,
	sni: Option<Vec<u8>>,
}

fn push_utf8_chars(x: &str, y: &mut String)
{
	for c in x.chars() {
		let cp = c as u32;
		if cp < 32 || cp == 127 {
			//The argument is 0xF000 - 0xF07F, which is less than 0x10FFFF and not in gap 0xD800 - 0xDFFF.
			y.push(unsafe{char::from_u32_unchecked(0xF000+cp)});
		} else {
			y.push(c);
		}
	}
}

fn translate_u8_to_utf8(x: &[u8]) -> String
{
	let mut s = String::new();
	for frag in iterate_mostly_utf8_fragments(x) { match frag {
		MUtf8StringFragment::String(y) => push_utf8_chars(y, &mut s),
		//The argument is 0xF080 - 0xF0FF, which is less than 0x10FFFF and not in gap 0xD800 - 0xDFFF.
		MUtf8StringFragment::Byte(b) => s.push(unsafe{char::from_u32_unchecked(0xF000+b as u32)}),
	}}
	s
}

const TLV_LENGTH: VectorLength = VectorLength::variable(0, VectorLength::MAX_16BIT);

fn handle_extended_proxy_data(edata: &[u8]) -> Result<ProxyExtended, Cow<'static, str>>
{
	let mut tlvdata = Source::new(edata);
	let mut r = ProxyExtended {
		flags: Bitfield::new(),
		client_cn: None,
		client_spkihash: None,
		sni: None,
	};
	while tlvdata.more() {
		let kind = tlvdata.u8().set_err("No TLV kind")?;
		let mut tlvdata = tlvdata.source(TLV_LENGTH).set_err("No TLV data")?;
		if kind == 0x20 {
			//Ignore client certificate stuff.
			tlvdata.u8().set_err("PP2_TYPE_SSL truncated")?;
			tlvdata.u32().set_err("PP2_TYPE_SSL truncated")?;
			r.flags.set_imp(PflagSecure);
			//PP2_TYPE_SSL.
			while tlvdata.more() {
				let kind = tlvdata.u8().set_err("No TLV kind")?;
				let tlvdata = tlvdata.source(TLV_LENGTH).set_err("No TLV data")?;
				if kind == 1 {
					//ALPN, as sent by btls-inetd.
					match tlvdata.as_slice() {
						b"http/1.1" => (),
						b"h2" => r.flags.set_imp(PflagH2),
						_ => r.flags.set_imp(PflagUnknownAlpn),
					}
				}
				if kind == 0x22 {
					r.client_cn = Some(translate_u8_to_utf8(tlvdata.as_slice()));
				}
				if kind == 0xE0 {
					let hash = Hexdump(tlvdata.as_slice()).to_string();
					r.client_spkihash = Some(hash);
				}
			}
		} else if kind == 1 {
			//ALPN.
			match tlvdata.as_slice() {
				b"http/1.1" => (),
				b"h2" => r.flags.set_imp(PflagH2),
				_ => r.flags.set_imp(PflagUnknownAlpn),
			}
		} else if kind == 2 {
			//SNI.
			r.sni = Some(tlvdata.as_slice().to_vec());
		}
	}
	Ok(r)
}

fn attribute_error(g: ConfigT, service: bool)
{
	if service { increment_metric!(error_serv g) } else { increment_metric!(error_tcp g) }
}

fn if_enable_tracing(g: ConfigT, sni: Option<&[u8]>) -> (bool, Option<u32>)
{
	const DEFAULT: (bool, Option<u32>) = (false, None);
	//If no SNI, do not enable. Copy the SNI, as it needs to be lowercased.
	let mut sni = match sni { Some(x) => x.to_vec(), None => return DEFAULT };
	//ASII-Lowercase the name.
	for b in sni.iter_mut() { if b.wrapping_sub(65) < 26 { *b += 32; } }
	g.with(|g|{
		//If no host entry, don't trace.
		g.routetable.get_data_for(&sni).map(|h|{
			let trace = h.get_h2_trace_flag();
			let max_streams = h.get_h2_max_streams();
			(trace, max_streams)
		}).unwrap_or(DEFAULT)
	})
}

#[test]
fn test_translate_u8_to_utf8()
{
	assert_eq!(translate_u8_to_utf8(b""), "");
	assert_eq!(translate_u8_to_utf8(b"foo"), "foo");
	assert_eq!(translate_u8_to_utf8(b"foo\xc3\x84"), "foo\u{c4}");
	assert_eq!(translate_u8_to_utf8(b"foo\xc3"), "foo\u{f0c3}");
	assert_eq!(translate_u8_to_utf8(b"foo\xc3\xc3bar"), "foo\u{f0c3}\u{f0c3}bar");
}
