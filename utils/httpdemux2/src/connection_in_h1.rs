use crate::ConfigT;
use crate::is_dangerous_header_request;
use crate::bitmask::Bitfield;
use crate::connection::BackendName;
use crate::connection::Connector;
use crate::connection::print_backend_response;
use crate::connection::print_internal_response;
use crate::connection_lowlevel::CID;
use crate::connection_midlevel::BackwardInterface;
use crate::connection_midlevel::BackwardInterfaceError;
use crate::connection_midlevel::Interrupter;
use crate::connection_midlevel::ReaderWriterM;
use crate::connection_midlevel::SinkError;
use crate::connection_midlevel::WriterM;
use crate::headerblock::HeaderBlock;
use crate::headerblock::HeaderBlockTrait;
use crate::headerblock::HeaderError;
use crate::headerblock::HeaderKind;
use crate::headerblock::OwnedTrailerBlock;
use crate::headerblock::serialize_headers;
use crate::headerblock::serialize_trailers;
use crate::headerblock::ShdrAuthorityRaw;
use crate::headerblock::ShdrClientCn;
use crate::headerblock::ShdrClientSpkihash;
use crate::headerblock::ShdrIpaddr;
use crate::headerblock::ShdrMethod;
use crate::headerblock::ShdrPathRaw;
use crate::headerblock::ShdrProtocol;
use crate::headerblock::ShdrQuery;
use crate::headerblock::ShdrScheme;
use crate::headerblock::ShdrVersion;
use crate::http::RequestInfo;
use crate::http::ClientInfoIn;
use crate::http::CONNECT_ERROR;
use crate::http::CONNECTION_LOST;
use crate::http::DfltIRWError;
use crate::http::InternalResponse;
use crate::http::InternalResponseWriter;
use crate::http::RequestWriteReturn;
use crate::http::TransactionStateIn;
use crate::metrics_dp::IncomingConnectionState;
use crate::utils::strip_port;
use btls_aux_fail::ResultExt;
use btls_aux_http::http1::Element;
use btls_aux_http::http1::ElementSink;
use btls_aux_http::http1::HeaderEncoder;
use btls_aux_http::http1::IoOrStreamError;
use btls_aux_http::http1::Stream;
use btls_aux_http::status::BAD_GATEWAY;
use btls_aux_http::status::GATEWAY_TIMEOUT;
use btls_aux_memory::PrintBuffer;
use btls_daemon_helper_lib::cow;
use btls_util_logging::ConnId;
use btls_util_logging::log;
use std::borrow::Cow;
use std::cmp::min;
use std::fmt::Display;
use std::fmt::Write as FmtWrite;
use std::mem::MaybeUninit;
use std::usize::MAX as USIZE_MAX;


const MAX_HBLOCK_SIZE: usize = 16000;
const BUFFER_EMERG_SPACE: usize = 8192;

struct RWriteWrapper<'a,W:WriterM>(&'a mut W);

impl<'a,W:WriterM> InternalResponseWriter for RWriteWrapper<'a,W>
{
	type Encoder = HeaderEncoder;
	type Error = DfltIRWError;
	fn query_maxsize(&self) -> usize { USIZE_MAX }
	fn flush_headers_only(&mut self, headers: &[u8]) -> Result<(), DfltIRWError>
	{
		self.0.write(&[headers]).map_err(|e|DfltIRWError::new(e))
	}
	fn flush_headers_and_payload(&mut self, headers: &[u8], payload: &[u8]) ->
		Result<(), DfltIRWError>
	{
		self.0.write(&[headers, payload]).map_err(|e|DfltIRWError::new(e))
	}
	fn df_precan_headers() -> &'static [u8]
	{
		b"HTTP/1.1 500 INTERNAL ERROR: DOUBLE FAULT\r\n\
Content-Length:34\r\n\
Content-Type:text/plain;charset=utf-8\r\n\
Content-Security-Policy:default-src 'none'\r\n\
X-Content-Type-Options:nosniff\r\n\
\r\n"
	}
	//We can not handle errors, where headers can not be sent. Just kill the connection.
	fn error_forward_failure<D:Display>(&mut self, cause: D) -> Result<(), DfltIRWError>
	{
		Err(DfltIRWError::new(format_args!("Headers already sent [cause: {cause}]")))
	}
	fn have_content_length(&self) -> bool { true }
	fn get_cid_and_sid(&self) -> (CID, u32)
	{
		(self.0.get_cid(), 0)
	}
}

define_bitfield_type!(SinkFlagsTag, u8);
//Connection is insecure.
define_bitfield_bit!(SflagInsecure, SinkFlagsTag, 0);
//Connection is blocked (does not read).
define_bitfield_bit!(SflagBlocked, SinkFlagsTag, 1);
//Connection is in chained mode.
define_bitfield_bit!(SFlagChained, SinkFlagsTag, 2);


struct Sink
{
	//The current header block.
	headers: HeaderBlock,
	//The current transaction.
	transaction: Option<TransactionStateIn>,
	//Global config.
	g: ConfigT,
	//Flags.
	flags: Bitfield<SinkFlagsTag>,
	//The client info.
	client_info: ClientInfoIn,
}

impl Sink
{
	fn _sink(&mut self, elem: Element, sink: &mut impl WriterM, connector: &mut Connector) ->
		Result<(), SinkError>
	{
		let id = sink.get_id();
		match elem {
			//Data received. If there is open transaction, forward the data to that transaction.
			//Otherwise ignore the block.
			Element::Data(d) => if let Some(transaction) = self.transaction.as_mut() {
				let mut blocked = self.flags.get(SflagBlocked);
				transaction.do_forward_data(d, &mut RWriteWrapper(sink),
					&mut blocked).map_err(|e|SinkError::WriteFailed(e))?;
				self.flags.force_imp(SflagBlocked, blocked);
			},
			//End of headers received. If there is open transaction, forward the headers to that
			//transaction. Othewise ignore the block. Always unconditionally clear the headers so that
			//trailers can be received.
			Element::EndOfHeaders => if let Some(transaction) = self.transaction.as_mut() {
				let r = transaction.do_forward_headers(&mut RWriteWrapper(sink), &mut self.headers,
					connector, true, id);
				self.headers.clear();
				r.map_err(|e|SinkError::WriteFailed(e))?;
			} else {
				self.headers.clear();
			},
			//End of message. If in headers, forward as headers if there is an open transaction.
			//Otherwise ignore. Note that if there is no transaction open, end of message is never
			//treated as end of headers. There is no need to clear headers because start of next
			//message is going to do it.
			Element::EndOfMessage if self.transaction.as_ref().map(|x|x.rx_phase_is_headers()).
				unwrap_or(false) => if let Some(transaction) = self.transaction.as_mut() {
				transaction.do_forward_headers(&mut RWriteWrapper(sink), &mut self.headers,
					connector, false, id).
					map_err(|e|SinkError::WriteFailed(e))?;
			},
			//If message ends not in headers, forward trailers if there is an open transaction, otherwse
			//ignore.
			Element::EndOfMessage => if let Some(transaction) = self.transaction.as_mut() {
				transaction.do_forward_trailers(&mut RWriteWrapper(sink), &mut self.headers).
					map_err(|e|SinkError::WriteFailed(e))?;
			},
			//If start of message, clear headers (for headers) and start a new transaction. Check that
			//there is no existing transaction in progress (signinfying pipelining, which is not
			//supported). Do not rely that past transactions are cleared.
			Element::StartOfMessage if self.transaction.as_ref().map(|t|!t.rx_and_tx_is_done()).
				unwrap_or(false) => fail!(SinkError::UnsupportedPipelining),
			Element::StartOfMessage => {
				self.headers.clear();
				//If not in chained mode, set the client source headers immediately, so to have
				//IP address available in errors.
				if !self.flags.get(SFlagChained) {
					self.client_info.set_client_info(&mut self.headers);
				}
				//This only is needed on headers, which always start here.
				self.headers.set_secure(!self.flags.get(SflagInsecure));
				self.transaction = Some(TransactionStateIn::new(self.client_info.clone(), self.g));
			},
			//Various header and trailer fields.
			Element::Method(m) => {
				//Method. If there is a pending transaction, notify it if the method is HEAD or not,
				//sine those have special responses.
				if let Some(transaction) = self.transaction.as_mut() {
					transaction.mark_as_head(m == b"HEAD");
				}
				self.headers.set_special(ShdrMethod, m)
			},
			Element::Authority(a) => {
				//Authority. Strip port number and set it as authority,
				//X-Forwarded-Server and X-Forwarded-Host.
				let a = strip_port(a);
				self.headers.set_authority(a);
			},
			//Content-length, path, protocol, query, scheme or version. Just set the field in headers.
			Element::ContentLength(l) => self.headers.set_content_length(l),
			Element::Path(p) => self.headers.set_path(p),
			Element::Protocol(p) => self.headers.set_special(ShdrProtocol, p),
			Element::Query(q) => self.headers.set_special(ShdrQuery, q),
			Element::Scheme(s) => self.headers.set_special(ShdrScheme, s),
			Element::Version(v) => self.headers.set_special(ShdrVersion, v),
			Element::ConnectionHeaderStd(n) => self.headers.set_connection_header_std(n),
			Element::ConnectionHeader(n) => self.headers.set_connection_header(n),
			//Generic headers and trailers. Any dangerous headers should be stripped.
			Element::HeaderStd(n, v) => {
				use btls_aux_http::http::StandardHttpHeader as SH;
				//Process a few headers (X-Forwarded-For, X-Forwarded-Proto,
				//X-Forwarded-Secure) specially if in chained mode.
				if self.flags.get(SFlagChained) {
					if n == SH::XForwardedFor {
						self.headers.set_special(ShdrIpaddr, v);
					} else if n == SH::XForwardedProto {
						self.headers.set_special(ShdrScheme, v);
					} else if n == SH::XForwardedSecure {
						self.headers.set_secure(v == b"1");
					} else if n == SH::XForwardedClientCn {
						self.headers.set_special(ShdrClientCn, v);
					} else if n == SH::XForwardedClientSpkihash {
						self.headers.set_special(ShdrClientSpkihash, v);
					} else if n == SH::XForwardedHost || n == SH::XForwardedServer {
						//Ignore these, since these are added anyway.
					} else {
						//These may be classified as dangerous, but forward as upstream is
						//trusted.
						self.headers.set_header_std(n, v);
					}
				} else if !n.is_dangerous_request() {
					//Ignore dangerous headers.
					self.headers.set_header_std(n, v);
				}
			},
			Element::Header(n,v) => {
				//All the specials use HeaderStd Event, so do not process here.
				//These may be classified as dangerous, but forward as upstream is
				//trusted.
				if self.flags.get(SFlagChained) || !is_dangerous_header_request(n) {
					self.headers.set_header(n, v);
				}
			},
			Element::TrailerStd(n,v) => {
				//Ignore dangerous headers.
				if !n.is_dangerous_request() { self.headers.set_header_std(n, v); }
			},
			Element::Trailer(n,v) => {
				//Ignore dangerous headers.
				if !is_dangerous_header_request(n) { self.headers.set_header(n, v); }
			},
			//Abort. If there is an open transaction, forward error to it as abort. Otherwise ignore,
			//the connection will be torn down anyway.
			Element::Abort(err) => {
				increment_metric!(error_in self.g);
				if let Some(transaction) = self.transaction.as_mut() {
					transaction.do_forward_error(cow!(F "Connection error: {err}"),
						&mut RWriteWrapper(sink), true, &self.headers).map_err(|err|{
						SinkError::WriteFailed(err)
					})?;
				}
			},
			//Headers faulty. If there is an open transaction, forward error to it as normal error.
			//Otherwise ignore, assuming reply has already been sent.
			Element::HeadersFaulty(err) => if let Some(transaction) = self.transaction.as_mut() {
				transaction.do_forward_error(cow!(F "{err}"), &mut RWriteWrapper(sink),
					false, &self.headers).map_err(|err|{
					SinkError::WriteFailed(err)
				})?
			},
			//These are not supposed to be possible.
			Element::EndOfHeadersInterim|Element::Reason(_)|Element::Status(_) =>
				if let Some(transaction) = self.transaction.as_mut() {
				transaction.do_forward_error(cow!("Internal error: Received impossible HTTP event"),
					&mut RWriteWrapper(sink), false, &self.headers).
					map_err(|e|SinkError::WriteFailed(e))?
			},
			//Raw path/authority.
			Element::PathRaw(p) => self.headers.set_special(ShdrPathRaw, p),
			Element::AuthorityRaw(p) => self.headers.set_special(ShdrAuthorityRaw, p),
			_ => (),	//Ignore unknown events.
		};
		Ok(())
	}
	fn sink(&mut self, elem: Element, sink: &mut impl WriterM, connector: &mut Connector)
	{
		//Check aborted flag on sink. It can be set by failures in processing various events. If the
		//flag gets set, we want to tear down the connection as soon as possible.
		if sink.aborted() { return; }
		let r: Result<(), SinkError> = self._sink(elem, sink, connector);
		//If transaction reached idle, remove the transaction.
		if self.transaction.as_ref().map(|x|x.rx_and_tx_is_done()).unwrap_or(false) {
			self.transaction = None;
		}
		//If error happened, set abort reason. This also sets the aborted flag.
		if let Err(e) = r { sink.abort(e); }
	}
}

struct SinkWrapper<'a,'b:'a,W:WriterM>(&'a mut Sink, &'a mut Connector<'b>, &'a mut W);

impl<'a,'b:'a,W:WriterM> ElementSink for SinkWrapper<'a,'b,W>
{
	fn sink(&mut self, elem: Element)
	{
		self.0.sink(elem, self.2, self.1)
	}
	fn protocol_supported(&self, protocol: &[u8]) -> bool
	{
		//The only supported protocol is websocket.
		protocol == b"websocket"
	}
}

fn write_data(writer: &mut impl WriterM, block: &[u8], chunked: bool) -> Result<usize, BackwardInterfaceError>
{
	//Ignore empty write requests.
	if block.len() == 0 { return Ok(0); }
	//Cap the write size to leave emergency space. The minimum is because free space can be bigger than the
	//block!
	let block = &block[..min(writer.freespace(BUFFER_EMERG_SPACE), block.len())];
	//If not chunked, just directly send data. Otherwise write chunk header and then data.
	if chunked {
		//Format the chunk length as chunk header. Block length is at most 2^64-1, so it fits to 20 bytes.
		let mut tmp = [MaybeUninit::uninit();20];
		let mut tmp = PrintBuffer::new(&mut tmp);
		write!(tmp, "{blen:x}\r\n", blen=block.len()).ok();
		let tmp = tmp.into_inner_bytes();
		writer.write(&[tmp, block, b"\r\n"])
	} else {
		writer.write(&[block])
	}.map_err(|e|BackwardInterfaceError::WriteFailed(e.to_string()))?;
	Ok(block.len())
}

struct RequestWrite<'a,W:WriterM>
{
	writer: &'a mut W,
	chunked: bool,
	trailers: bool,
	got_underflow: bool,
	error: Option<BackwardInterfaceError>,
}

impl<'a,W:WriterM> RequestWrite<'a,W>
{
	fn _write_backward_trailers<H:for<'c> HeaderBlockTrait<'c>>(&mut self, block: &mut H) ->
		Result<(), BackwardInterfaceError>
	{
		//Set the trailers flag, so the code knows all backward stuff on connection is completed.
		self.trailers = true;
		//Do not send trailers if not chunked, as there are no place for trailers in that case.
		if !self.chunked { return Ok(()); }

		//Okay, serialize and send the trailer block. Note that final chunk has to be sent as well.
		let mut buf = [MaybeUninit::uninit();MAX_HBLOCK_SIZE];
		match serialize_trailers::<HeaderEncoder,_>(&mut buf, block) {
			Ok(h) => self.writer.write(&[b"0\r\n", h]),
			Err(err) => {
				//Trailers are never critical, so just send empty trailers.
				//The message is connection-specific.
				log!(ERROR "Sending trailers failed: {err}");
				self.writer.write(&[&b"0\r\n\r\n"[..]])
			}
		}.map_err(|e|BackwardInterfaceError::WriteFailed(e.to_string()))
	}
}

impl<'a,W:WriterM> RequestWriteReturn for RequestWrite<'a,W>
{
	fn max_amount(&self) -> usize { 32768 }
	//Returns false if secondary should detach.
	fn write_backward_data(&mut self, block: &[u8], underflow: bool) -> Option<usize>
	{
		//We do not care about the underflow condiion, since we do not keep a list of connections to poll
		//(as there is only 1).
		//Store the error, so connection can be torn down with correct error.
		let r = write_data(self.writer, block, self.chunked).map_err(|e|self.error = Some(e)).
			map(|x|Some(x)).unwrap_or(None);
		if r == Some(block.len()) && underflow { self.got_underflow = true; }
		r
	}
	fn write_backward_trailers(&mut self, t: &mut OwnedTrailerBlock)
	{
		self._write_backward_trailers(t).map_err(|e|self.error = Some(e)).ok();
	}
}

define_bitfield_type!(ConnectionFlagsTag, u16);
//Connection has been upgraded.
define_bitfield_bit!(CflagUpgraded, ConnectionFlagsTag, 0);
//EOF has been read
define_bitfield_bit!(CflagPendingEof, ConnectionFlagsTag, 1);
//If true, once send buffers have cleared, close the connection.
define_bitfield_bit!(CflagWshutdown, ConnectionFlagsTag, 2);
//If true, connection has been closed for writing, no more output can be sent.
define_bitfield_bit!(CflagWshutdownAck, ConnectionFlagsTag, 3);
//Ready.
define_bitfield_bit!(CflagReady, ConnectionFlagsTag, 4);
//If true, the output is chunked, if false, the output is raw.
define_bitfield_bit!(CflagChunked, ConnectionFlagsTag, 5);
//If true, the slave has died, but the death has not been reacted to yet.
define_bitfield_bit!(CflagNotifyDiedFlag, ConnectionFlagsTag, 6);
define_bitfield_bit!(CflagDiedConnected, ConnectionFlagsTag, 7);
//If true, the master has received quit signal.
define_bitfield_bit!(CflagRecvQuit, ConnectionFlagsTag, 8);


pub struct Connection
{
	//The HTTP receive stream.
	recv: Stream,
	//Sink for events from the HTTP receive stream.
	sink: Sink,
	//The pending error. If set, the connection will be torn down once write buffers have been flushed.
	errmsg: Option<BackwardInterfaceError>,
	_conntrack: IncomingConnectionState,
	flags: Bitfield<ConnectionFlagsTag>,
}

pub enum ConnectionMode
{
	Insecure,
	Secure,
	Chained,
}

impl Connection
{
	pub fn new(mode: ConnectionMode, client_info: ClientInfoIn, mut conntrack: IncomingConnectionState,
		g: ConfigT) -> Connection
	{
		conntrack.set_h1();
		let flags = Bitfield::new();
		let flags = match mode {
			ConnectionMode::Insecure => flags.set(SflagInsecure),
			ConnectionMode::Secure => flags,
			ConnectionMode::Chained => flags.set(SFlagChained).set(SflagInsecure),
		};
		let dflt_scheme = if flags.get(SflagInsecure) { "http" } else { "https" };
		let mut recv = Stream::new(dflt_scheme.as_bytes());
		recv.set_use_std_flag();	//Use standard headers.
		let mut ret = Connection {
			recv: recv,
			errmsg: None,
			flags: Bitfield::new(),
			_conntrack: conntrack,
			sink: Sink {
				headers: HeaderBlock::new(),
				transaction: None,
				flags: flags,
				client_info: client_info,
				g: g,
			}
		};
		//Set the chained flag on header block if connection is chained. This flag is special in that
		//clear() will not clear it.
		if ret.sink.flags.get(SFlagChained) { ret.sink.headers.set_chained(); }
		ret
	}
	pub fn get_active_slaves(&self) -> Vec<CID>
	{
		//There can only be slave if there is a transaction open. Open transaction does not imply having
		//a slave, but in that case get_slave_cid() returns INVALID_CID, which gets ignored as slave.
		self.sink.transaction.as_ref().map(|x|vec![x.get_slave_cid()]).unwrap_or(Vec::new())
	}
	pub fn need_read(&self) -> bool
	{
		//Normally connection is always ready to read. However, there are three exceptions:
		//
		//1) The sink is blocked.
		//2) There is pending teardown.
		//3) There is pending EOF.
		!self.sink.flags.get(SflagBlocked) && self.errmsg.is_none() && !self.flags.get(CflagPendingEof)
	}
	pub fn forward_unblock(&mut self, mut irq: Interrupter, req: &RequestInfo)
	{
		//Verify the source cid. Ignore event if the source CID does not match.
		if self.sink.transaction.as_ref().map(|t|{
			req.slave_cid.is_invalid() || req.slave_cid != t.get_slave_cid()
		}).unwrap_or(true) { return; }
		//Clear the blocked status. The interrupt is necressary because this connection can be in state
		//where it does not listen to any events. The interrupt will then reset the events listened to.
		self.sink.flags.clear_imp(SflagBlocked);
		irq.interrupt();
	}
	fn cleanup_backward_link(&mut self, w: &mut impl WriterM)
	{
		//Signal that the receive stream is closed.
		self.sink.transaction.as_mut().map(|x|x.close_backward());
		//There are two cases:
		//
		//1) Connection not upgraded: The remaining request will be ignored, so unblock the stream. Then
		//interrupt it in case connection is not listening.
		//
		//2) Connection upgraded: Trigger write shutdown once send buffer is cleared. No need for interrupt
		//because either check_eof_close() will close the thing this round, or the connection is waiting
		//for write event.
		if !self.flags.get(CflagUpgraded) {
			self.sink.flags.clear_imp(SflagBlocked);
			w.interrupt();
		} else {
			self.flags.set_imp(CflagWshutdown);
		}
	}
	pub fn handle_write_underflow(&mut self, w: &mut impl WriterM) -> Result<(), Cow<'static, str>>
	{
		//Check if connection close condition has been reached, since this can alter condition 2).
		self.check_eof_close(w, false).map_err(|err|{
			cow!(F "{err}")
		})?;
		//Try pulling data from slave to refill our buffers. In case this can not pull any data, it means
		//that the slave is currently out of data, and will then push data to us once it obtains data.
		//No need to check if write buffer is empty: It is at start of handle_write_underflow() and
		//the check_eof_close() above can not write anything.
		self.try_pull_from_slave(w).map_err(|err|{
			cow!(F "{err}")
		})?;
		Ok(())
	}
	pub fn notify_quit(&mut self, mut irq: Interrupter)
	{
		self.flags.set_imp(CflagRecvQuit);
		irq.interrupt();
	}
	pub fn notify_died(&mut self, slave: CID, mut irq: Interrupter, connected: bool)
	{
		//Verify the source cid. Ignore event if the source CID does not match.
		if self.sink.transaction.as_ref().map(|t|{
			slave.is_invalid() || slave != t.get_slave_cid()
		}).unwrap_or(true) { return; }
		//Set the notify_died_flag and raise interrupt, so that connection reacts promptly.
		self.flags.set_imp(CflagNotifyDiedFlag);
		self.flags.force_imp(CflagDiedConnected, connected);
		irq.interrupt();
	}
	pub fn backward_has_data(&mut self, mut irq: Interrupter, _: &RequestInfo)
	{
		//The backend signals it has data in backward buffers that it could not write yet. Raise the ready
		//flag and raise interrupt. The interrupt is needed if backend only supports pull and our send
		//buffer is empty. We have no way to check if our send buffer is empty or not, so assume it is
		//empty.
		self.flags.set_imp(CflagReady);
		irq.interrupt();
	}
	pub fn is_idle(&self) -> bool
	{
		self.sink.transaction.is_none()
	}
	pub fn handle_request_event(&mut self, w: &mut impl WriterM) -> Result<(), Cow<'static, str>>
	{
		//Handle possible quit event.
		if self.flags.get(CflagRecvQuit) {
			//If there is no outstanding request, quit immediately.
			fail_if!(self.is_idle(), cow!("{qk}", qk=BackwardInterfaceError::QuitKill));
			//If there is outstanding request, check_eof_close closes the connection.
		}
		//Handle possible notify died event.
		if self.flags.get_clear_imp(CflagNotifyDiedFlag) {
			//If there is no transaction open, these events may just be ignored.
			if let Some(transaction) = self.sink.transaction.as_mut() {
				//There are three cases:
				//
				//1) Connection has been upgraded: Disconnect with connection lost error once write
				//buffer has been drained. Upgraded connections can not function without slave
				//connnection on the other side.
				//
				//2) All data has been sent. In this case, close the receiving side of current
				//request. The HTTP parse code will eventually cycle to next request, which will
				//then generate new message event to reopen the streams.
				//
				//3) There is unsent data. In this case, try sending error headers, and if not
				//possible, immediately tear down the connection.
				if self.flags.get(CflagUpgraded) {
					self.errmsg = Some(if self.flags.get(CflagDiedConnected) {
						BackwardInterfaceError::BackendLost
					} else {
						BackwardInterfaceError::BackendConnect
					});
				} else if transaction.tx_phase_is_done() {
					transaction.close_backward();
				} else {
					let (code, errmsg) = if self.flags.get(CflagDiedConnected) {
						(BAD_GATEWAY, CONNECTION_LOST)
					} else {
						(GATEWAY_TIMEOUT, CONNECT_ERROR)
					};
					//Assume URL is available at this point.
					if !transaction.send_internal_response(&mut RWriteWrapper(w),
						&InternalResponse::err(code, errmsg), None).map_err(|err|{
							cow!(F "{err}")
						})? {
						fail!(Cow::Borrowed(errmsg));
					}
				}
			}
		}
		//If buffer is empty, try pulling. This is what triggers pulling if some backend requests pulling
		//while our send buffer is empty. This can be missed due to send buffer having data on it, because
		//in that case handle_write_underflow() will eventually be called, and it will try to pull data.
		if w.is_empty() {
			self.try_pull_from_slave(w).map_err(|err|{
				cow!(F "{err}")
			})?;
		}
		//The state of close condition 3) may have changed, so recheck close.
		self.check_eof_close(w, false).map_err(|err|{
			cow!(F "{err}")
		})?;
		Ok(())
	}
	fn try_pull_from_slave(&mut self, w: &mut impl WriterM) -> Result<(), BackwardInterfaceError>
	{
		if !self.flags.get(CflagReady) { return Ok(()); }	//Nothing to pull.
		let mut sink = RequestWrite {
			writer: w,
			chunked: self.flags.get(CflagChunked),
			trailers: false,
			error: None,
			got_underflow: false,
		};
		//Poll potentially many times. Specifically until:
		//1) The backend clears the ready flag.
		//2) Write buffer is no longer empty.
		//3) Write fails.
		//4) But at least once in any case.
		let mut r;
		loop {
			r = self.sink.transaction.as_mut().map(|x|x.request_backward_data(&mut sink)).
				unwrap_or(false);
			//If operation failed, disconnect the connection with the saved error. Note that this does
			//not happen if the request points to nonexistent connection. In that case r=false instead.
			//The same happens if there is no open transaction.
			if let Some(err) = sink.error { fail!(err); }
			//If trailers were sent, the backward link has ended, clean it up.
			if sink.trailers { self.cleanup_backward_link(sink.writer); }
			if sink.got_underflow { self.flags.clear_imp(CflagReady); }
			if !r || !sink.writer.is_empty() || !self.flags.get(CflagReady) { break; }
		}
		//If r=false, Connection to backend was lost. Generate error headers, or tear down the connection
		//if sending error headers is not possible. However, ignore this if there is no open
		//transaction, as things are immediately ready for a new request, and reply to previous request
		//was presumably sent.
		if let (false, Some(transaction)) = (r, self.sink.transaction.as_mut()) {
			//Assume URL is available at this point.
			if !transaction.send_internal_response(&mut RWriteWrapper(sink.writer),
				&InternalResponse::err(BAD_GATEWAY, CONNECTION_LOST) , None).
				map_err(|e|BackwardInterfaceError::DfltIRWError(e))? {
				fail!(BackwardInterfaceError::BackendLost);
			}
		}
		//cleanup_backward_link or send_internal_response may have been called, so recheck EOF status.
		self.check_eof_close(sink.writer, false)?;
		Ok(())
	}
	pub fn handle_read_event<'a,'b:'a>(&mut self, w: &mut impl ReaderWriterM, c: &'a mut Connector<'b>) ->
		Result<(), Cow<'static, str>>
	{
		match {
			let (source, sink) = w.split();
			let mut sink = SinkWrapper(&mut self.sink, c, sink);
			self.recv.push(source, &mut sink)
		} {
			Ok(true) => (),
			//If EOF is received, check if connection close condition has been reached, since this can
			//alter condition 1).
			Ok(false) => self.check_eof_close(w.as_writer(), true).map_err(|err|{
				cow!(F "{err}")
			})?,
			//If read fails, just immediately tear down the connection.
			Err(IoOrStreamError::Io(err)) => fail!(cow!(F "Read Error: {err}")),
			Err(IoOrStreamError::Stream(err)) => {
				let error = BackwardInterfaceError::StreamError(err.to_string());
				//If a fatal stream error occurs, try sending a fatal 400 response. In case this
				//succeeds, wait until write buffer is flushed before tearing down the connection.
				//If this fails, including due to lack of open transaction, just immediately kill
				//the connection. HTTP parsing emits start of message immediately upon receiving
				//anything past the old message, and not having transaction open otherwise means
				//that response has been sent.
				if let Some(transaction) = self.sink.transaction.as_mut() {
					//Try setting the source IP, to get better messages.
					self.sink.headers.set_special(ShdrIpaddr,
						self.sink.client_info.client_ip.as_bytes());

					if transaction.send_internal_response(&mut RWriteWrapper(w.as_writer()),
						&InternalResponse::BadRequestF(&error.to_string()),
						Some(&self.sink.headers)).map_err(|err|{
							cow!(F "{err}")
						})? {
						//Set the delayed close reason. This will close connection when its
						//write buffer is empty.
						self.errmsg = Some(error);
					} else {
						//If can not send error, just kill the connection immediately.
						fail!(cow!(F "{error}"));
					}
				} else {
					//Can not send error due to no transaction open. Kill the connection
					//immediately.
					fail!(cow!(F "{error}"));
				}
			}
		}
		//This may have altered close reason 4).
		self.check_eof_close(w.as_writer(), false).map_err(|err|{
			cow!(F "{err}")
		})?;
		Ok(())
	}
	//Check the close condition. This should be called if:
	//
	//1) EOF is received (with set_recv_eof=true).
	//2) In handle_write_underflow().
	//3) After close_rx(), cleanup_backward_link() or send_internal_response() is called.
	//4) After errmsg is set.
	//
	//Note that one does not need to actually establish that any of the above actually has happened. It
	//sufficies if any of the above may have happened.
	fn check_eof_close(&mut self, sink: &mut impl WriterM, set_recv_eof: bool) ->
		Result<(), BackwardInterfaceError>
	{
		//If set_recv_eof is true, assert receive EOF.
		self.flags.or_imp(CflagPendingEof, set_recv_eof);
		//Ignore event otherwise unless primary buffer is empty (if it is not, it will empty eventually.
		if !sink.is_empty() { return Ok(()); }
		//If transaction has ended, clear it. But save requested connection close first. This only happens
		//if transfer buffer is empty.
		let mut conn_close = false;
		if self.sink.transaction.as_ref().map(|x|{
			conn_close = x.client_wants_close_after_request();
			x.rx_and_tx_is_done()
		}).unwrap_or(false) { self.sink.transaction = None; }

		//If the following conditions are all true:
		//
		//1) Connection: Close was specified, or EOF was received.
		//2) Request state is idle.
		//
		//Then close the connection.
		fail_if!((conn_close || self.flags.get(CflagPendingEof)) && self.sink.transaction.is_none(),
			BackwardInterfaceError::NormalClose);
		//There is alternate teardown path: If all the following are true:
		//
		//1) There is pending teardown.
		//
		//Then terminate the connection.
		match self.errmsg.as_ref() { Some(x) => fail!(x.clone()), None => () };
		//Then furthermore if all the following are true:
		//
		//1) wshutdown is set.
		//2) wshutdown_ack is not set.
		//
		//The shut down the write side of the socket.
		if self.flags.get(CflagWshutdown) && !self.flags.get(CflagWshutdownAck) {
			if let Err(err) = sink.shutdown() {
				fail!(BackwardInterfaceError::WriteFailed(err.to_string()));
			}
			self.flags.set_imp(CflagWshutdownAck);
		}
		//If both directions have gotten closed, close the connection.
		fail_if!(self.flags.get(CflagPendingEof) && self.flags.get(CflagWshutdownAck),
			BackwardInterfaceError::NormalClose);
		//If there is no transaction and quit is set, close the connection.
		fail_if!(self.flags.get(CflagRecvQuit) && self.sink.transaction.is_none(),
			BackwardInterfaceError::QuitKill);

		Ok(())
	}
}

impl BackwardInterface for Connection
{
	fn write_backward_headers(&mut self, sink: &mut impl WriterM, block: &mut HeaderBlock,
		req: &RequestInfo, backend_conn: ConnId, backend_name: BackendName) ->
		Result<bool, BackwardInterfaceError>
	{
		let mut trans_idle = block.is_no_body();
		//This may be just ignored if there is no open transaction (with signal to dissociate).
		let ret = if let Some(transaction) = self.sink.transaction.as_mut() {
			//Check that the slave CID is correct. If not, ignore with request to dissociate. Also check
			//that connection is in headers tx phase.
			if req.slave_cid.is_invalid() || req.slave_cid != transaction.get_slave_cid() ||
				!transaction.tx_phase_is_headers() {
				return Ok(false);
			}
			//Initially pull is disabled.
			self.flags.clear_imp(CflagReady);
			//Notify transaction about status code, so FLAG_100_CONTINUE can be cleared.
			transaction.notify_status_code(block.get_status());

			//Set chunked and upgraded, according to the received header block. The value of chunked
			//after interim headers does not matter. Note that if upgraded is set, then chunked never
			//is.
			self.flags.or_imp(CflagUpgraded, block.get_special(ShdrProtocol).is_some());
			let upgraded = self.flags.get(CflagUpgraded);
			self.flags.force_imp(CflagChunked, block.is_chunked() && !upgraded);
			//If no upgrade, NAK it.
			if !self.flags.get(CflagUpgraded) && transaction.client_proposed_upgrade() {
				self.recv.nak_upgrade().set_err(BackwardInterfaceError::UpgradeNakFailed)?;
				transaction.upgrade_failed();
			}
			//If client requested close, acknowledge that.
			if transaction.client_wants_close_after_request() { block.set_emit_close(); }
			let mut dissociate = false;
			let mut buf = [MaybeUninit::uninit();MAX_HBLOCK_SIZE];
			match serialize_headers::<HeaderEncoder>(&mut buf, block, HeaderKind::Response) {
				Ok(h) => {
					let status = print_backend_response(block, req, backend_conn,
						backend_name);
					sink.write(&[h]).
						map_err(|e|BackwardInterfaceError::WriteFailed(e.to_string()))?;
					//Notify that final reply has been sent if status code is 101 or >=200.
					if status == 101 || status >= 200 { req.fate_backend(); }
				},
				Err(_) => {
					//We are position to send error headers, because this is headers, not
					//trailers, which means the final response has not been sent yet. Send a
					//502 error.
					let errmsg = block.is_overflow().unwrap_or(HeaderError::IntNoFault).
						to_string();
					let status = InternalResponse::err(BAD_GATEWAY, &errmsg);
					print_internal_response(req, &status, false);
					//Assume URL available.
					transaction.send_internal_response(&mut RWriteWrapper(sink), &status, None).
						map_err(|e|BackwardInterfaceError::WriteFailed(e.to_string()))?;
					//Dissociate the slave connection and transition to idle. This can be
					//performed even if upload is still in progress, since the stream read
					//knows how to ignore incoming data.
					dissociate = true;
					trans_idle = true;
				}
			}
			!dissociate
		} else {
			return Ok(false)
		};
		//The state can immediately transition to finished, in case the response has no body, or if
		//error headers get sent.
		if trans_idle { self.cleanup_backward_link(sink); }
		self.check_eof_close(sink, false)?;
		Ok(ret)
	}
	fn write_backward_data(&mut self, sink: &mut impl WriterM, block: &[u8], req: &RequestInfo) ->
		Result<Option<usize>, BackwardInterfaceError>
	{
		//Verify the source cid. Ignore event if the source CID does not match. Signal the slave to
		//dissociate. Also do this if connection is not in tx data transfer phase.
		if self.sink.transaction.as_ref().map(|t|{
			req.slave_cid.is_invalid() || req.slave_cid != t.get_slave_cid() || !t.tx_phase_is_data()
		}).unwrap_or(true) { return Ok(None); }

		//Otherwise forward the data.
		write_data(sink, block, self.flags.get(CflagChunked)).map(|x|Some(x))
	}
	fn write_backward_trailers(&mut self, sink: &mut impl WriterM, block: &mut HeaderBlock,
		req: &RequestInfo) -> Result<(), BackwardInterfaceError>
	{
		//Verify the source cid. Ignore event if the source CID does not match. Also ignore if not in
		//tx data transfer phase.
		if self.sink.transaction.as_ref().map(|t|{
			req.slave_cid.is_invalid() || req.slave_cid != t.get_slave_cid() || !t.tx_phase_is_data()
		}).unwrap_or(true) { return Ok(()); }
		//This is end of data, so clean up the backward link.
		self.cleanup_backward_link(sink);
		//Send the trailers.
		let r = RequestWrite {
			writer: sink,
			chunked: self.flags.get(CflagChunked),
			trailers: false,
			error: None,
			got_underflow: false,
		}._write_backward_trailers(block);
		self.flags.clear_imp(CflagReady);
		//Check if connection close condition has been reached, since this can alter condition 3).
		self.check_eof_close(sink, false)?;
		r
	}
	fn write_backward_error(&mut self, sink: &mut impl WriterM, req: &RequestInfo,
		status: InternalResponse) -> Result<(), BackwardInterfaceError>
	{
		//Verify the source cid. Ignore event if the source CID does not match.
		if self.sink.transaction.as_ref().map(|t|{
			req.slave_cid.is_invalid() || req.slave_cid != t.get_slave_cid()
		}).unwrap_or(true) { return Ok(()); }

		//Otherwise write error response. If this fails, tear down the connection. This does not happen
		//if there is no open transaction (but we can not reach here then anyway).
		if let Some(transaction) = self.sink.transaction.as_mut() {
			//Assume URL available.
			if !transaction.send_internal_response(&mut RWriteWrapper(sink), &status, None).
				map_err(|e|BackwardInterfaceError::DfltIRWError(e))? {
				//Can not send error. Tear down the connection.
				fail!(BackwardInterfaceError::BackendError(status.to_string()));
			}
			//If no upgrade, NAK it.
			if !self.flags.get(CflagUpgraded) && transaction.client_proposed_upgrade() {
				self.recv.nak_upgrade().set_err(BackwardInterfaceError::UpgradeNakFailed)?;
				transaction.upgrade_failed();
			}
		}
		//Check if connection close condition has been reached, since this can alter condition 3).
		self.check_eof_close(sink, false)?;
		Ok(())
	}
}
