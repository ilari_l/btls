use crate::connection::BackendName;
use crate::connection::combine_timestamps;
use crate::connection_lowlevel::CID;
use crate::connection_out::BackendError;
use crate::headerblock::HeaderBlock;
use crate::http::RequestInfo;
use crate::http::DfltIRWError;
use crate::http::InternalResponse;
use crate::http::RequestWriteReturn;
use crate::routing_dp::HostRoutingDataRef;
use crate::utils::Buffer;
use crate::utils::RcTextString;
use btls_aux_http::http::Sink as HttpSink;
use btls_aux_http::http::Source as HttpSource;
use btls_aux_memory::SafeShowByteString;
use btls_aux_time::PointInTime;
use btls_aux_time::Timer;
use btls_aux_time::TimeUnit;
use btls_daemon_helper_lib::AddressHandle;
use btls_daemon_helper_lib::AllocatedToken;
use btls_daemon_helper_lib::cow;
use btls_daemon_helper_lib::LocalIrq;
use btls_daemon_helper_lib::RemoteIrq;
use btls_daemon_helper_lib::SocketAddrEx;
use btls_util_logging::ConnId;
use btls_util_logging::syslog;
use std::borrow::Cow;
use std::cell::RefCell;
use std::cmp::min;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::io::Error as IoError;
use std::io::ErrorKind as IoErrorKind;
use std::mem::MaybeUninit;
use std::mem::replace;
use std::rc::Rc;
use std::sync::Arc;

pub trait Dekryptize
{
	//Obtain dekryptized input.
	fn dekryptized_input(&mut self, buf: &mut [MaybeUninit<u8>]) -> Option<(usize, bool)>;
	//Provode input to dekryptize. buf.len() == 0 means EOF!
	fn dekryptize(&mut self, buf: &[u8]) -> Result<(), IoError>;
	//Wants to rx data for handshake?
	fn need_rx(&self) -> bool;
	//Has output to provode?
	fn has_output(&self) -> bool;
	//Is ready?
	fn is_ready(&self) -> bool;
}

pub trait Kryptize
{
	//return (insize, outsize)
	fn kryptize(&mut self, input: &[&[u8]]) -> Result<Vec<u8>, IoError>;
	//return (outsize, was_all)
	fn kryptize_eof(&mut self) -> Result<Vec<u8>, IoError>;
	//Return buffer.
	fn return_buffer(&mut self, _b: Vec<u8>) {}
	//Wants to tx data for handshake?
	fn need_tx(&self) -> bool;
	//Is ready?
	fn is_ready(&self) -> bool;
	//Get abort reason.
	fn get_abort_reason(&self) -> Option<String>;
	//Print parameters.
	fn print_parameters(&self, f: &mut Formatter) -> Result<(), FmtError>;
}

pub trait _Krypted
{
	fn __is_server_slave(&self) -> bool { false }
	fn __new_krypted(&self, irq: RemoteIrq) -> (Box<dyn Kryptize+'static>, Box<dyn Dekryptize+'static>);
}

pub trait Krypted
{
	fn new_krypted(&self, irq: RemoteIrq) ->
		(Option<(Box<dyn Kryptize+'static>, Box<dyn Dekryptize+'static>)>, bool);
	fn is_server_slave(&self) -> bool;
}

impl<K:_Krypted+?Sized> Krypted for Option<Arc<K>>
{
	fn new_krypted(&self, irq: RemoteIrq) ->
		(Option<(Box<dyn Kryptize+'static>, Box<dyn Dekryptize+'static>)>, bool)
	{
		match self {
			&Some(ref tmpl) => {
				let (e,d) = tmpl.__new_krypted(irq);
				let r = e.is_ready();
				(Some((e,d)),r)
			},
			None => (None, true)
		}
	}
	fn is_server_slave(&self) -> bool { self.as_ref().map(|s|s.__is_server_slave()).unwrap_or(false) }
}

impl Krypted for ()
{
	fn new_krypted(&self, _: RemoteIrq) ->
		(Option<(Box<dyn Kryptize+'static>, Box<dyn Dekryptize+'static>)>, bool)
	{
		(None, true)
	}
	fn is_server_slave(&self) -> bool { false }
}

pub enum SinkError
{
	WriteFailed(DfltIRWError),
	ResetError(String),
	SAckError(String),
	PAckError(String),
	Goaway(u32, Vec<u8>),
	UnsupportedPipelining,
	UnexpectedData,
	HttpFaultInKeepalive(String),
	BadKeepaliveStatus(u16),
	KeepaliveConnectionClose,
	MasterFailUpgraded,
	MasterFailTx,
	UnsolictedReply,
}

impl Display for SinkError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&SinkError::WriteFailed(ref err) => write!(f, "Write error: {err}"),
			&SinkError::ResetError(ref err) => write!(f, "Error resetting stream: {err}"),
			&SinkError::SAckError(ref err) => write!(f, "Error ACKing SETTINGS: {err}"),
			&SinkError::PAckError(ref err) => write!(f, "Error ACKing PING: {err}"),
			&SinkError::Goaway(code, ref msg) =>
				write!(f, "Received GOAWAY {code}[{msg}]", msg=SafeShowByteString(msg)),
			&SinkError::UnsupportedPipelining => write!(f, "Pipelining is not supported"),
			&SinkError::UnexpectedData => write!(f, "Received unexpected data from backend"),
			&SinkError::HttpFaultInKeepalive(ref err) => write!(f, "HTTP error during keepalive: {err}"),
			&SinkError::BadKeepaliveStatus(code) => write!(f, "Bad HTTP status {code} for keepalive"),
			&SinkError::KeepaliveConnectionClose => write!(f, "Connection: close in keepalive"),
			&SinkError::MasterFailUpgraded => write!(f, "upgrade corrupted by failed master"),
			&SinkError::MasterFailTx => write!(f, "transmit corrupted by failed master"),
			&SinkError::UnsolictedReply => write!(f, "Unsolicted reply from slave"),
		}
	}
}


pub trait ReaderWriterM
{
	type Reader: HttpSource<Error=IoError>;
	type Writer: WriterM;
	fn as_writer<'a>(&'a mut self) -> &'a mut <Self as ReaderWriterM>::Writer;
	fn split<'a>(&'a mut self) ->
		(&'a mut <Self as ReaderWriterM>::Reader, &'a mut <Self as ReaderWriterM>::Writer);
}

pub trait ReaderWriterS
{
	type Reader: HttpSource<Error=IoError>;
	type Writer: WriterS;
	fn as_writer<'a>(&'a mut self) -> &'a mut <Self as ReaderWriterS>::Writer;
	fn split<'a>(&'a mut self) ->
		(&'a mut <Self as ReaderWriterS>::Reader, &'a mut <Self as ReaderWriterS>::Writer);
}

pub trait ReaderWriterX: ReaderWriterM+ReaderWriterS
{
	fn bump_timeout(&mut self);
	fn aborted(&self) -> bool;
	fn read(&mut self, buf: &mut [u8]) -> Result<Option<usize>, IoError>;
}

pub trait WriterB
{
	fn get_id(&self) -> ConnId;
	fn aborted(&self) -> bool;
	fn interrupt(&mut self);
	fn is_empty(&self) -> bool;
	fn shutdown(&mut self) -> Result<(), IoError>;
	fn abort(&mut self, err: SinkError);
}

pub trait WriterBC
{
	fn take_error_or<T:Sized>(&mut self, val: T) -> Result<T, Cow<'static, str>>;
}

pub trait WriterM: WriterB+HttpSink<Error=IoError>
{
	fn freespace(&self, bias: usize) -> usize;
	fn get_cid(&self) -> CID;
}

pub trait WriterMC: WriterM+WriterBC { }
pub trait WriterXC: WriterM+WriterS+WriterBC {}
pub trait WriterXC2: WriterM+WriterS2+WriterBC {}
pub trait WriterSC: WriterS+WriterBC
{
	fn is_ready(&self) -> bool;
}

pub trait WriterS: WriterB+HttpSink<Error=IoError>
{
}

pub trait WriterS2: WriterS
{
	fn swap_buffers(&mut self, buf: &mut Buffer);
}

#[derive(Copy,Clone)]
pub struct Interrupter(LocalIrq);

impl Interrupter
{
	pub fn new(token: &AllocatedToken) -> Interrupter { Interrupter(token.as_irq()) }
	pub fn interrupt(&mut self) { self.0.irq(); }
}

pub struct WriterUnconnected
{
	id: ConnId,
	abort: Option<SinkError>,
	irq: Interrupter,
}

impl WriterUnconnected
{
	pub fn new(id: ConnId, token: &AllocatedToken) -> WriterUnconnected
	{
		WriterUnconnected {
			id: id,
			abort: None,
			irq: Interrupter::new(token),
		}
	}
}

impl WriterB for WriterUnconnected
{
	fn get_id(&self) -> ConnId { self.id }
	fn aborted(&self) -> bool { self.abort.is_some() }
	fn interrupt(&mut self) { self.irq.interrupt(); }
	fn is_empty(&self) -> bool { false }	//Should never be called.
	fn shutdown(&mut self) -> Result<(), IoError>
	{
		Err(IoError::new(IoErrorKind::Other, "Shutdown not supported for unconnected writer"))
	}
	fn abort(&mut self, err: SinkError) { self.abort = Some(err); }
}

impl WriterS for WriterUnconnected {}

impl WriterBC for WriterUnconnected
{
	fn take_error_or<T:Sized>(&mut self, val: T) -> Result<T, Cow<'static, str>>
	{
		match replace(&mut self.abort, None) {
			Some(err) => Err(cow!(F "{err}")),
			None => Ok(val)
		}
	}
}

impl WriterSC for WriterUnconnected
{
	fn is_ready(&self) -> bool { false }	//Never ready.
}

impl HttpSink for WriterUnconnected
{
	type Error = IoError;
	fn write(&mut self, _: &[&[u8]]) -> Result<(), IoError>
	{
		Err(IoError::new(IoErrorKind::Other, "Write not supported for unconnected writer"))
	}
}


///Error from backward interface operation.
#[derive(Clone)]
pub enum BackwardInterfaceError
{
	WriteFailed(String),
	DfltIRWError(DfltIRWError),
	HeadersSendError(String),
	DataSendError(String),
	StreamError(String),
	BackendError(String),
	UpgradeNakFailed,
	EmptyHeaderBlock,
	NormalClose,
	BackendLost,
	BackendConnect,
	QuitKill,
}

impl Display for BackwardInterfaceError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&BackwardInterfaceError::WriteFailed(ref err) => write!(f, "Write error: {err}"),
			&BackwardInterfaceError::DfltIRWError(ref err) => Display::fmt(err, f),
			&BackwardInterfaceError::HeadersSendError(ref err) =>
				write!(f, "Internal error: Error sending HEADERS: {err}"),
			&BackwardInterfaceError::DataSendError(ref err) =>
				write!(f, "Internal error: Error sending DATA: {err}"),
			&BackwardInterfaceError::StreamError(ref err) => write!(f, "Parse error: {err}"),
			&BackwardInterfaceError::BackendError(ref err) => write!(f, "Backend error: {err}"),
			&BackwardInterfaceError::UpgradeNakFailed =>
				write!(f, "Internal error: Failed to NAK upgrade"),
			&BackwardInterfaceError::EmptyHeaderBlock =>
				write!(f, "Internal error: Empty header block"),
			&BackwardInterfaceError::NormalClose => write!(f, "Normal close"),
			&BackwardInterfaceError::BackendLost => write!(f, "Failed to connect to backend"),
			&BackwardInterfaceError::BackendConnect => write!(f, "Connection to backend lost"),
			&BackwardInterfaceError::QuitKill => write!(f, "Killing idle connection when quitting"),
		}
	}
}

///Interface to connection, backwards.
pub(crate) trait BackwardInterface
{
	///Write a header block to connection.
	///
	///The writer `sink` coresponds to this connection. The header block to write is `block` (already has
	///been modified according to the request. The request information is `req`.
	///
	///If headers are successfully sent, returns `Ok(true)`. If header sending fails, returns `Ok(false)`.
	///If an error happens in connection while sending, returns `Err(x)`. The last case causes the connection
	///to be torn down, as does abort on sink.
	fn write_backward_headers(&mut self, sink: &mut impl WriterM, block: &mut HeaderBlock,
		req: &RequestInfo, backend_conn: ConnId, backend_name: BackendName) ->
		Result<bool, BackwardInterfaceError>;
	///Write data into connection.
	///
	///The writer `sink` coresponds to this connection. The data block to write is `block`. The request
	///information is `req`.
	///
	///If `x` bytes of data are successfully sent, returns `Ok(Some(x))` (`x` may be `0`). If data sending
	///fails, returns `Ok(None)`. If an error happens in connection while sending, returns `Err(x)`. The last
	///case causes the connection to be torn down, as does abort on sink.
	fn write_backward_data(&mut self, sink: &mut impl WriterM, block: &[u8], req: &RequestInfo) ->
		Result<Option<usize>, BackwardInterfaceError>;
	///Write trailers into connection.
	///
	///The writer `sink` coresponds to this connection. The trailer block to write is `block` (already has
	///been modified according to the request. The request information is `req`.
	///
	///If successful, returns `Ok(())`. If an error happens in connection while sending, returns `Err(x)`. The
	///last case causes the connection to be torn down, as does abort on sink.
	fn write_backward_trailers(&mut self, sink: &mut impl WriterM, block: &mut HeaderBlock,
		req: &RequestInfo) -> Result<(), BackwardInterfaceError>;
	///Write an error to connection.
	///
	///The writer `sink` coresponds to this connection. The request information is `req`. The HTTP error code
	///to use is `status`.
	///
	///If successful, returns `Ok(())`. If an error happens in connection while sending, returns `Err(x)`. The
	///last case causes the connection to be torn down, as does abort on sink.
	fn write_backward_error(&mut self, sink: &mut impl WriterM, req: &RequestInfo,
		status: InternalResponse) -> Result<(), BackwardInterfaceError>;
}

///Error from forward interface operation.
pub enum ForwardInterfaceError
{
	WriteFailed(IoError),
	NormalClose,
	MasterError(String),
	SlaveFault,
	BackendError,
}

impl Display for ForwardInterfaceError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&ForwardInterfaceError::WriteFailed(ref err) => write!(f, "Write error: {err}"),
			&ForwardInterfaceError::NormalClose => write!(f, "Normal close"),
			&ForwardInterfaceError::MasterError(ref err) => write!(f, "Error in master: {err}"),
			&ForwardInterfaceError::SlaveFault => write!(f, "Connection state corrupt"),
			&ForwardInterfaceError::BackendError => write!(f, "Backend error"),
		}
	}
}


///Interface to connection, forwards.
pub trait ForwardInterface
{
	///Write a header block to connection.
	///
	///The writer `sink` coresponds to this connection. The header block to write is `block` (already has
	///been modified according to the request. The request information is `req`.
	///
	///If successful, returns `Ok(())`. If an error happens in connection while sending, returns `Err(x)`. The
	///last case causes the connection to be torn down, as does abort on sink.
	fn write_forward_headers(&mut self, sink: &mut impl WriterS, block: &mut HeaderBlock, req: &RequestInfo,
		hostdata: HostRoutingDataRef, parent: ConnId) ->
		Result<(), Result<BackendError, ForwardInterfaceError>>;
	///Write data into connection.
	///
	///The writer `sink` coresponds to this connection. The data block to write is `block`.
	///
	///If data are successfully sent, returns `Ok(true)`. If data sending fails, returns `Ok(false)`.
	///If an error happens in connection while sending, returns `Err(x)`. The last case causes the connection
	///to be torn down, as does abort on sink.
	fn write_forward_data(&mut self, sink: &mut impl WriterS, block: &[u8]) ->
		Result<bool, ForwardInterfaceError>;
	///Write trailers into connection.
	///
	///The writer `sink` coresponds to this connection. The trailer block to write is `block` (already has
	///been modified according to the request.
	///
	///If successful, returns `Ok(())`. If an error happens in connection while sending, returns `Err(x)`. The
	///last case causes the connection to be torn down, as does abort on sink.
	fn write_forward_trailers(&mut self, sink: &mut impl WriterS, block: &mut HeaderBlock) ->
		Result<(), ForwardInterfaceError>;
	///Write an error to connection.
	///
	///The error message is `error`.
	///
	///If successful, returns `Ok(())`. If an error happens in connection while sending, returns `Err(x)`. The
	///last case causes the connection to be torn down.
	fn write_forward_error(&mut self, error: &str) -> Result<(), ForwardInterfaceError>;
	///Get the CID of the master connection.
	fn get_master(&self) -> CID;
	///Get the SID in the master connection.
	fn get_sid(&self) -> u32;
	///Notify that the interface is connected.
	fn notify_connected(&mut self, cid: CID);
	///Request data from the interface to be sent to `sink`. The `irq` points to this connection.
	///
	///Returns `true` on success, `false` on failure.
	fn request_data<S:RequestWriteReturn>(&mut self, sink: &mut S, irq: Interrupter) -> bool;
}


pub struct ConnectionFreeList
{
	cclass: HashMap<CID, RcTextString>,
	freelist: HashMap<RcTextString, HashSet<CID>>,
}

impl ConnectionFreeList
{
	fn new() -> ConnectionFreeList
	{
		ConnectionFreeList {
			cclass: HashMap::new(),
			freelist: HashMap::new(),
		}
	}
	fn _delete_by_cid(&mut self, cid: &CID)
	{
		if let Some(cclass) = self.cclass.get(cid) {
			if self.freelist.get_mut(cclass).map(|x|{x.remove(cid);x.len()==0}).unwrap_or(false) {
				self.freelist.remove(cclass);
			}
		}
		self.cclass.remove(&cid);
	}
	fn _add_by_cid(&mut self, cid: &CID, cclass: RcTextString)
	{
		if cid.is_invalid() { return; }	//Do not add invalid CID to freelist!
		if cclass.len() == 0 { return; }	//Empty class is not valid!
		self.cclass.insert(cid.clone(), cclass.clone());
		self.freelist.entry(cclass).or_insert(HashSet::new()).insert(cid.clone());
	}
	fn _get_by_cclass(&mut self, cclass: &str) -> Option<CID>
	{
		self.freelist.get_mut(cclass).and_then(|x|{
			let item = x.iter().cloned().next();
			if let &Some(ref item) = &item { x.remove(item); }
			item
		})
	}
	pub fn delete_by_cid(cid: &CID)
	{
		FREELIST.with(|x|x.borrow_mut()._delete_by_cid(cid));
	}
	pub fn add_by_cid(cid: &CID, cclass: RcTextString)
	{
		FREELIST.with(|x|x.borrow_mut()._add_by_cid(cid, cclass));
	}
	pub fn get_by_cclass(cclass: &str, not_found: bool) -> Option<CID>
	{
		if not_found { return None; }		//Simulate not found.
		FREELIST.with(|x|x.borrow_mut()._get_by_cclass(cclass))
	}
}

#[derive(Copy,Clone,PartialEq)]
pub(crate) enum LinkStatus
{
	Connected,	//Fully connected.
	Handshaking,	//The low-level link is up, but handshaking is in progress.
	Connecting,	//low-level link is connecting.
}

pub(crate) struct LinkState
{
	//If timeout is set, there is a connection attempt in progress. If there is stable connection, then
	//timeout is unset. This is the case for both master and slave connections. However, master connection
	//can only be in attempt if handshaking is in progress. Slave connection can be in attempt both when
	//trying to acquire low-level link or when handshaking.
	timeout: Timer,
	//If timeout_att is set, then the slave connection is trying to acquire low-level link or handshaking,
	//and gives timeout for current attempt. If an attempt times out, but the main connect timer does not,
	//a new attempt can be made. The attempt timeout is never set for master connection, as handshaking can
	//only happen once.
	timeout_att: Timer,
	//The target of current attempt.
	to: SocketAddrEx,
	to_handle: Option<AddressHandle>,
	//Set if the low-level transport is connected.
	lowlevel_connected: bool,
}

impl LinkState
{
	//Make new master connection state. Low-level connection is always established. pending_timeout is the
	//timeout to handshake. If pending_timeout=None, the connection is immediately fully connected.
	pub(crate) fn new_master(pending_timeout: Option<TimeUnit>) -> LinkState
	{
		LinkState {
			timeout: Timer::new_target(pending_timeout),
			timeout_att: Timer::new(),		//Never used.
			to: SocketAddrEx::Unknown,		//Masters are always connected.
			to_handle: None,
			lowlevel_connected: true,		
		}
	}
	//Make new slave conection.
	pub(crate) fn new_slave(lowlevel: bool, handshaked: bool, timeout: TimeUnit) -> LinkState
	{
		//If both lowlevel and handshake are ready, the connection is connected. Otherwise register
		//timeout to wait for connection to come fully up.
		let timeout = if !lowlevel || !handshaked { Some(timeout) } else { None };
		LinkState {
			timeout: Timer::new_target(timeout),
			timeout_att: Timer::new(),		//Not set yet, as there is no attempt yet.
			to: SocketAddrEx::Unknown,
			to_handle: None,
			lowlevel_connected: lowlevel,
		}
	}
	//Make new connection state that is always connected.
	pub(crate) fn new_fully_connected() -> LinkState
	{
		LinkState {
			timeout: Timer::new(),
			timeout_att: Timer::new(),
			to: SocketAddrEx::Unknown,
			to_handle: None,
			lowlevel_connected: true,
		}
	}
	pub(crate) fn is_fully_connected(&self) -> bool { self.timeout.is_unarmed() }
	pub(crate) fn is_connection_in_progress(&self) -> bool { self.timeout.is_armed() }
	pub(crate) fn status(&self) -> LinkStatus
	{
		if self.timeout.is_unarmed() { return LinkStatus::Connected; }
		if self.lowlevel_connected { return LinkStatus::Handshaking; }
		LinkStatus::Connecting
	}
	//Low-level link has come up.
	pub(crate) fn lowlevel_up(&mut self) { self.lowlevel_connected = true; }
	//Link has come fully up.
	pub(crate) fn connection_established(&mut self)
	{
		//Set low level link up and clear timeouts.
		self.lowlevel_connected = true;
		self.timeout.disarm();
		self.timeout_att.disarm();
		self.to = SocketAddrEx::Unknown;
		self.to_handle = None;
	}
	//Set low level link status.
	pub(crate) fn set_low_level_status(&mut self, up: bool, connid: ConnId)
	{
		if self.timeout.is_unarmed() {
			return syslog!(CRITICAL "set_low_level_status() called with fc-connection {connid}");
		}
		self.lowlevel_connected = up;
	}
	//Set attempt timer.
	pub(crate) fn attempt_connection(&mut self, to: &SocketAddrEx, handle: AddressHandle, timeout: TimeUnit,
		connid: ConnId)
	{
		if self.timeout.is_unarmed() {
			return syslog!(CRITICAL "attempt_connection() called with fc-connection {connid}");
		}
		self.to = to.clone();
		self.to_handle = Some(handle);
		self.timeout_att = Timer::new_target(timeout);
	}
	pub(crate) fn next_timeout(&self) -> Option<PointInTime>
	{
		//Return first of the timeout events.
		return combine_timestamps(&[self.timeout.expires(), self.timeout_att.expires()], min);
	}
	pub(crate) fn is_attempt_timed_out(&mut self, now: PointInTime) -> bool
	{
		self.timeout.is_armed() && self.timeout_att.triggered(now)
	}
	pub(crate) fn is_connection_timed_out(&mut self, now: PointInTime) -> bool
	{
		let f = self.timeout.triggered(now);
		//Triggering the timeout clears it. However, main timeout being clear means fully connected
		//connection, which is wrong. Set timeout to far future. The connection context is being torn
		//down anyway.
		if f { self.timeout.rearm(TimeUnit::Seconds(100000)); }
		f
	}
	pub(crate) fn get_current_target(&self) -> &SocketAddrEx { &self.to }
	pub(crate) fn mark_current_target_bad(&mut self)
	{
		let address_handle = replace(&mut self.to_handle, None);
		address_handle.map(|a|a.set_status(false));
	}
}


thread_local!(static FREELIST: Rc<RefCell<ConnectionFreeList>> = Rc::new(RefCell::new(ConnectionFreeList::new())));
