use crate::connection::KryptedTarget;
use crate::headerblock::ShdrLocation;
use crate::http::HeaderRewrite;
use btls_aux_fail::ResultExt;
use btls_aux_filename::Filename;
use btls_aux_http::http::is_xdigit;
use btls_aux_http::status::FOUND;
use btls_aux_http::status::MOVED_PERMANENTLY;
use btls_aux_http::status::PERMANENT_REDIRECT;
use btls_aux_http::status::TEMPORARY_REDIRECT;
use btls_aux_memory::Attachment;
use btls_aux_memory::Hexdump;
use btls_aux_memory::SafeShowByteString;
use btls_aux_memory::split_attach_first;
use btls_aux_memory::split_attach_last;
use btls_daemon_helper_lib::FrozenIpSet;
use btls_daemon_helper_lib::RcuHashMap;
use btls_daemon_helper_lib::reverse_addr_for;
use btls_util_logging::syslog;
use btls_util_regex::get_regex_invariant_prefix;
use btls_util_regex::REG_AUTOANCHOR;
use btls_util_regex::REG_EXTENDED;
use btls_util_regex::REG_NOSUB;
use btls_util_regex::RegexHandle;
use btls_util_regex::RegexMatch;
use btls_util_regex::RegexSpecial;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::mem::replace;
use std::net::IpAddr;
use std::net::Ipv6Addr;
use std::ops::Deref;
use std::path::Path;
use std::str::from_utf8;
use std::str::from_utf8_unchecked;
use std::str::FromStr;
use std::sync::Arc;
use std::sync::atomic::AtomicUsize;
use std::usize::MAX as USIZE_MAX;


#[derive(Copy,Clone)]
pub struct UrlParseError(_UrlParseError);
#[derive(Copy,Clone)]
enum _UrlParseError
{
	BadFragmentChar(char),
	BadFragmentEscape,
	BadHostChar(char),
	BadHostEscape,
	BadIpv6Address,
	BadIpv6FutureChar(char),
	BadIpvFutureSeparator,
	BadIpvFutureTooShort,
	BadIpvFutureVersion(char),
	BadPathChar(char),
	BadPathEscape,
	BadPortChar(char),
	BadQueryChar(char),
	BadQueryEscape,
	BadSchemeChar(char),
	BadSchemeEmpty,
	BadUserinfoChar(char),
	BadUserinfoEscape,
}

impl From<_UrlParseError> for UrlParseError
{
	fn from(x: _UrlParseError) -> UrlParseError { UrlParseError(x) }
}

impl Display for UrlParseError
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::_UrlParseError::*;
		match &self.0 {
			&BadFragmentChar(ch) => write!(f, "Bad character '{ch}' in fragment"),
			&BadFragmentEscape => f.write_str("Bad escape sequence in fragment"),
			&BadHostChar(ch) => write!(f, "Bad character '{ch}' in host"),
			&BadHostEscape => f.write_str("Bad escape sequence in host"),
			&BadIpv6Address => f.write_str("Bad IPv6 address"),
			&BadIpv6FutureChar(ch) => write!(f, "Bad character '{ch}' in IPvFuture address"),
			&BadIpvFutureSeparator => f.write_str("Bad IPvFuture address separator"),
			&BadIpvFutureTooShort => f.write_str("IPvFuture address too short"),
			&BadIpvFutureVersion(ch) => write!(f, "Bad IPvFuture version '{ch}'"),
			&BadPathChar(ch) => write!(f, "Bad character '{ch}' in path"),
			&BadPathEscape => f.write_str("Bad escape sequence in path"),
			&BadPortChar(ch) => write!(f, "Bad character '{ch}' in port"),
			&BadQueryChar(ch) => write!(f, "Bad character '{ch}' in query"),
			&BadQueryEscape => f.write_str("Bad escape sequence in query"),
			&BadSchemeChar(ch) => write!(f, "Bad character '{ch}' in scheme"),
			&BadSchemeEmpty => f.write_str("Empty scheme is not valid"),
			&BadUserinfoChar(ch) => write!(f, "Bad character '{ch}' in userinfo"),
			&BadUserinfoEscape => f.write_str("Bad escape sequence in userinfo"),
		}
	}
}

fn is_alpha(x: u8) -> bool
{
	match x { b'A'..=b'Z'|b'a'..=b'z' => true, _ => false }
}

fn is_scheme(x: char) -> bool
{
	match x { 'A'..='Z'|'a'..='z'|'0'..='9'|'+'|'-'|'.' => true, _ => false }
}

fn is_unreserved(x: char) -> bool
{
	match x { 'A'..='Z'|'a'..='z'|'0'..='9'|'-'|'.'|'_'|'~' => true, _ => false }
}

fn is_subdelim(x: char) -> bool
{
	match x { '!'|'$'|'&'|'\''|'('|')'|'*'|'+'|','|';'|'=' => true, _ => false }
}

fn is_pchar_noesc(c: char) -> bool
{
	is_unreserved(c)||is_subdelim(c)||c==':'||c=='@'
}

fn char_to_byte(c: Option<char>) -> u8
{
	match c.map(|x|x as u32).unwrap_or(0) { x@0..=127 => x as u8, _ => 0 }
}

fn is_escape_tail<I:Iterator<Item=char>>(x: &mut I) -> bool
{
	if !is_xdigit(char_to_byte(x.next())) { return false; }
	if !is_xdigit(char_to_byte(x.next())) { return false; }
	true
}

fn strip_uri_fragment<'a>(x: &'a str) -> Result<&'a str, UrlParseError>
{
	//If URI is valid, then the first '#' in URI has to start the fragment.
	let (x, y) = if let Some(s) = x.find('#') { (&x[..s], &x[s+1..]) } else { (x, "") };
	let mut itr = y.chars();
	loop { match itr.next() {
		Some('%') => fail_if!(!is_escape_tail(&mut itr), _UrlParseError::BadFragmentEscape),
		Some(c) if is_pchar_noesc(c) || c == '/' || c == '?' => (),
		Some(c) => fail!(_UrlParseError::BadFragmentChar(c)),
		None => break,
	}}
	Ok(x)
}

fn strip_uri_query<'a>(x: &'a str) -> Result<&'a str, UrlParseError>
{
	//If URI is valid, then the first '?' in URI has to start the query.
	let (x, y) = if let Some(s) = x.find('?') { (&x[..s], &x[s+1..]) } else { (x, "") };
	let mut itr = y.chars();
	loop { match itr.next() {
		Some('%') => fail_if!(!is_escape_tail(&mut itr), _UrlParseError::BadQueryEscape),
		Some(c) if is_pchar_noesc(c) || c == '/' || c == '?' => (),
		Some(c) => fail!(_UrlParseError::BadQueryChar(c)),
		None => break,
	}}
	Ok(x)
}

fn strip_uri_scheme<'a>(x: &'a str) -> Result<&'a str, UrlParseError>
{
	//If there is : before /, the : ends the scheme. Otherwise there is no scheme.
	Ok(if let Some(s) = x.find(|x|x==':'||x=='/') {
		if x.as_bytes()[s] == b':' {
			//First character must be alpha, others can be digts and few others.
			let mut itr = (&x[..s]).chars();
			match itr.next().ok_or(_UrlParseError::BadSchemeEmpty)? {
				c if !is_alpha(char_to_byte(Some(c))) => fail!(_UrlParseError::BadSchemeChar(c)),
				_ => (),
			};
			for c in itr {
				fail_if!(!is_scheme(c), _UrlParseError::BadSchemeChar(c));
			}
			&x[s+1..]
		} else {
			x
		}
	} else {
		x
	})
}

fn split_uri_authority<'a>(x: &'a str) -> (&'a str, &'a str)
{
	//If remainder starts with "//", parse part before first '/' as the authority and return rest as path.
	if let Some(x) = x.strip_prefix("//") {
		split_attach_first(x, "/", Attachment::Right).unwrap_or((x, ""))
	} else {
		("", x)
	}
}

fn split_uri_userinfo<'a>(x: &'a str) -> (&'a str, &'a str)
{
	//Split x on first '@', as it separates the parts.
	split_attach_first(x, "@", Attachment::Center).unwrap_or(("", x))
}

fn check_uri_path(x: &str) -> Result<(), UrlParseError>
{
	//If URI is valid, then the path will be *(pchar / "/"), and conversely if path is not of that form, then
	//URI is not valid. This is due to interaction of checks above with various forms.
	let mut itr = x.chars();
	loop { match itr.next() {
		Some('%') => fail_if!(!is_escape_tail(&mut itr), _UrlParseError::BadPathEscape),
		Some(c) if is_pchar_noesc(c) || c == '/' => (),
		Some(c) => fail!(_UrlParseError::BadPathChar(c)),
		None => break,
	}}
	Ok(())
}

fn check_uri_userinfo(x: &str) -> Result<(), UrlParseError>
{
	//x needs to be *( unreserved / pct-encoded / sub-delims / ":" ).
	let mut itr = x.chars();
	loop { match itr.next() {
		Some('%') => fail_if!(!is_escape_tail(&mut itr), _UrlParseError::BadUserinfoEscape),
		Some(c) if is_unreserved(c) || is_subdelim(c) || c == ':' => (),
		Some(c) => fail!(_UrlParseError::BadUserinfoChar(c)),
		None => break,
	}}
	Ok(())
}

fn split_uri_port<'a>(x: &'a str) -> (&'a str, &'a str)
{
	//If last character is "]", then the whole thing is host. Otherwise split at last ':', giving the whole
	//to host if not found.
	if !x.ends_with("]") {
		split_attach_last(x, ":", Attachment::Center).unwrap_or_else(|x|(x, ""))
	} else {
		(x, "")
	}
}

fn check_uri_port(x: &str) -> Result<(), UrlParseError>
{
	//x needs to be *digit.
	let mut itr = x.chars();
	loop { match itr.next() {
		Some(c) if (c as u32).wrapping_sub(48) < 9  => (),
		Some(c) => fail!(_UrlParseError::BadPortChar(c)),
		None => break,
	}}
	Ok(())
}

pub fn is_valid_uri<'a>(orig_x: &'a str) -> Result<&'a str, UrlParseError>
{
	let x = strip_uri_fragment(orig_x)?;
	let x = strip_uri_query(x)?;
	let x = strip_uri_scheme(x)?;
	let (authority, path) = split_uri_authority(x);
	let (userinfo, authority) = split_uri_userinfo(authority);
	check_uri_path(path)?;
	check_uri_userinfo(userinfo)?;
	let (host, port) = split_uri_port(authority);
	check_uri_port(port)?;
	//There are 3 (4) host forms.
	if let Some(host) = host.strip_prefix("[v").and_then(|host|host.strip_suffix("]")) {
		//IPvFuture.
		fail_if!(host.len() < 3, _UrlParseError::BadIpvFutureTooShort);
		let mut itr = host.chars();
		let x1 = itr.next().ok_or(_UrlParseError::BadIpvFutureTooShort)?;
		let x2 = itr.next().ok_or(_UrlParseError::BadIpvFutureTooShort)?;
		fail_if!(!is_xdigit(char_to_byte(Some(x1))), _UrlParseError::BadIpvFutureVersion(x1));
		fail_if!(x2 != '.', _UrlParseError::BadIpvFutureSeparator);
		loop { match itr.next() {
			Some(c) if is_unreserved(c)||is_subdelim(c)||c==':' => (),
			Some(c) => fail!(_UrlParseError::BadIpv6FutureChar(c)),
			None => break,
		}}
	} else if let Some(host) = host.strip_prefix("[").and_then(|host|host.strip_suffix("]")) {
		//IPv6 address.
		Ipv6Addr::from_str(host).set_err(_UrlParseError::BadIpv6Address)?;
	} else {
		//Reg-name (or special case IPv4address)
		let mut itr = host.chars();
		loop { match itr.next() {
			Some('%') => fail_if!(!is_escape_tail(&mut itr), _UrlParseError::BadHostEscape),
			Some(c) if is_unreserved(c) || is_subdelim(c) || c == ':' => (),
			Some(c) => fail!(_UrlParseError::BadHostChar(c)),
			None => break,
		}}
	}
	Ok(orig_x)
}

const HREQ_TLS13: u8 = 1<<0;
const HREQ_SECURE192: u8 = 1<<1;

#[derive(Clone,Debug)]
pub struct RegularExpression
{
	original: String,
	invariant: String,
	start_slash: bool,
	pattern: Pattern,
}

impl RegularExpression
{
	pub fn new(regex: &str) -> Result<RegularExpression, String>
	{
		Self::__new(regex, REG_EXTENDED|REG_AUTOANCHOR|REG_NOSUB)
	}
	pub fn new_pattern(regex: &str) -> Result<RegularExpression, String>
	{
		Self::__new(regex, REG_EXTENDED|REG_AUTOANCHOR)
	}
/*
	pub fn new_path_prefix(prefix: &str) -> RegularExpression
	{
		//Note that if prefix includes trailing slash, start_slash must be false, as slash is included.
		//and does not come as next requirement. And / is treated specially.
		RegularExpression {
			original: prefix.to_owned(),
			invariant: prefix.to_owned(),
			start_slash: !prefix.ends_with("/"),
			pattern: if prefix == "/" { Pattern::ExactMatch } else { Pattern::AlwaysMatch },
		}
	}
*/
	fn __new(regex: &str, flags: i32) -> Result<RegularExpression, String>
	{
		let mut errorbuf = [0;256];
		let mut invariant = [0;1024];
		let ret = get_regex_invariant_prefix(regex.as_bytes(), &mut invariant)?;
		let pattern = match ret.special {
			Some(RegexSpecial::ExactMatch) => Pattern::ExactMatch,
			Some(RegexSpecial::AlwaysMatch) => Pattern::AlwaysMatch(true),
			Some(RegexSpecial::AlwaysMatchNC) => Pattern::AlwaysMatch(false),
			None => match RegexHandle::new(regex.as_bytes(), flags, &mut errorbuf) {
				Ok(pattern) => Pattern::Regex(Arc::new(pattern)),
				Err(err) => fail!(SafeShowByteString(err).to_string())
			}
		};
		Ok(RegularExpression{
			original: regex.to_owned(),
			//This can always be assumed to be valid UTF-8.
			invariant: unsafe{from_utf8_unchecked(ret.invariant).to_string()},
			start_slash: ret.next_slash,
			pattern: pattern,
		})
	}
}

#[derive(Clone,Debug)]
pub enum DisableMode
{
	MovedPermanently(String),
	Found(String),
	TemporaryRedirect(String),
	PermanentRedirect(String),
	Forbidden(String),
	NotFound(String),
	Gone(String),
	UnavailableForLegalReasons(String),
}

#[derive(Copy,Clone,Debug)]
pub(crate) enum IDisableMode
{
	MovedPermanently(HostStringIndex2),
	Found(HostStringIndex2),
	TemporaryRedirect(HostStringIndex2),
	PermanentRedirect(HostStringIndex2),
	Forbidden(HostStringIndex2),
	NotFound(HostStringIndex2),
	Gone(HostStringIndex2),
	UnavailableForLegalReasons(HostStringIndex2),
}

struct PrintDisable<'a>(&'a HostRoutingData, &'a IDisableMode);

impl<'a> Display for PrintDisable<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		use self::IDisableMode::*;
		let (pfx, ptr) = match self.1 {
			&MovedPermanently(x) =>("moved-permanently", x),
			&Found(x) =>("found", x),
			&TemporaryRedirect(x) =>("temporary-redirect", x),
			&PermanentRedirect(x) =>("permanent-redirect", x),
			&Forbidden(x) =>("forbidden", x),
			&NotFound(x) =>("not-found", x),
			&Gone(x) =>("gone", x),
			&UnavailableForLegalReasons(x) =>("unavailable-for-legal-reasons", x),
		};
		write!(f, "{pfx} {arg}", arg=self.0.get_string(ptr))
	}
}

#[derive(Copy,Clone,Debug,PartialEq)]
pub enum RedirectMode
{
	MovedPermanently,
	Found,
	TemporaryRedirect,
	PermanentRedirect,
}

impl Display for RedirectMode
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&RedirectMode::MovedPermanently => f.write_str("moved_permanently"),
			&RedirectMode::Found => f.write_str("found"),
			&RedirectMode::TemporaryRedirect => f.write_str("temporary_redirect"),
			&RedirectMode::PermanentRedirect => f.write_str("permanent_redirect"),
		}
	}
}

impl RedirectMode
{
	pub fn status(&self) -> u16
	{
		use self::RedirectMode::*;
		match self {
			&MovedPermanently => MOVED_PERMANENTLY,
			&Found => FOUND,
			&TemporaryRedirect => TEMPORARY_REDIRECT,
			&PermanentRedirect => PERMANENT_REDIRECT,
		}
	}
}

#[derive(Copy,Clone,Debug,PartialEq)]
pub enum InsecureMode
{
	Forbidden,
	RootOnly(RedirectMode),
	RootOnlyFlag(RedirectMode),
	Redirect(RedirectMode),
	Allow,
}

pub struct BackendData
{
	pub targets: Arc<(Vec<KryptedTarget>, AtomicUsize)>,
	pub keepalive: Option<Arc<(String, String)>>,
	pub use_once: bool,
}

impl BackendData
{
	pub(crate) fn get_targets<'a>(&'a self) -> Arc<(Vec<KryptedTarget>, AtomicUsize)>
	{
		self.targets.clone()
	}
	pub(crate) fn get_keepalive(&self) -> Option<Arc<(String, String)>>
	{
		self.keepalive.clone()
	}
	pub(crate) fn get_use_once(&self) -> bool { self.use_once }
}

impl Display for BackendData
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		for target in self.targets.0.iter() {
			write!(f, "target {target:?}")?;
		}
		if let (false, Some(keepalive)) = (self.use_once, self.keepalive.as_ref()) {
			write!(f," keepalive: http://{host}{path}", host=keepalive.0, path=keepalive.1)?;
		} else if self.use_once {
			f.write_str(" use_once")?;
		}
		Ok(())
	}
}

macro_rules! add_space
{
	($var:ident $f:ident) => { if replace(&mut $var, true) { $f.write_str(" ")?; } }
}

#[derive(Clone,Debug)]
struct PathPrefixTable
{
	//(prefix, regex_idx_list, parent).
	entries: Vec<(HostStringIndex2, usize, Vec<usize>)>,
}

impl PathPrefixTable
{
	fn new(table: Vec<HostStringIndex2>, stringtab: &String) -> PathPrefixTable
	{
		let mut entries: Vec<(HostStringIndex2, usize, Vec<usize>)> = Vec::new();
		//Every prefix in table once.
		let mut found_prefixes: HashSet<HostStringIndex2> = HashSet::new();
		for entry in table.iter() { found_prefixes.insert(entry.clone()); }
		//Make sorted table.
		for entry in found_prefixes.drain() { entries.push((entry, !0, Vec::new())); }
		entries.sort_by(|&(a,_,_),&(b,_,_)|stringtab.get(a).cmp(stringtab.get(b)));
		set_path_tree_indices(&mut entries, stringtab);
		let fb = entries.len();
		//Fill regex_idx_list entries.
		for (idx, entry) in table.iter().enumerate() {
			let entry = stringtab.get(*entry);
			//This better always match.
			let mut sidx = entries.binary_search_by(|&(x,_,_)|stringtab.get(x).cmp(entry)).
				unwrap_or(fb);
			while sidx < entries.len() && stringtab.get(entries[sidx].0).starts_with(entry) {
				entries[sidx].2.push(idx);
				sidx += 1;
			}
		}
		PathPrefixTable{entries}
	}
	fn __get_list_for_prefix<'a>(&'a self, path: &[u8], stringtab: &'a impl HostStringTableGet) ->
		Option<(&'a str, &'a [usize])>
	{
		match self.entries.binary_search_by(|&(x,_,_)|stringtab.get(x).as_bytes().cmp(path)) {
			Ok(i) => self.entries.get(i),
			Err(i) => {
				//Step 1 place back to get last match. If i=0, this wraps to invalid index.
				let mut entry = self.entries.get(i.wrapping_sub(1))?;
				//Try parent if not match. It is not sufficient to chase parent just one time, as
				//it can happen that one has /a, /a/b, /a/b/c, /a/b/c/d and is looking for /a/c,
				//that finds the last one, but correct one is the first.
				while !path.starts_with(stringtab.get(entry.0).as_bytes()) {
					entry = self.entries.get(entry.1)?;
				}
				Some(entry)
			}
		}.map(|x|(stringtab.get(x.0), x.2.deref()))
	}
	fn get_list_for_prefix<'a>(&'a self, path: &[u8], stringtab: &'a impl HostStringTableGet) ->
		(&'a str, &'a [usize])
	{
		self.__get_list_for_prefix(path, stringtab).unwrap_or(("", &[]))
	}
}

#[derive(Clone,Debug)]
struct RegularExpressionTable
{
	entries: Vec<RegularExpressionEntry>,
	prefixes: PathPrefixTable,
}

#[derive(Clone,Debug)]
enum Pattern
{
	///Match only exact. There are no submatches.
	ExactMatch,
	///Always match, producing everything but invariant prefix as submatch #1.
	AlwaysMatch(bool),
	///Regular expression.
	Regex(Arc<RegexHandle>),
}

#[derive(Clone,Debug)]
struct RegularExpressionEntry
{
	original: HostStringIndex2,
	invariant_length: usize,
	start_slash: bool,
	pattern: Pattern,
	data: RegularExpressionEntryData,
}

impl RegularExpressionEntry
{
	fn new(regex: &RegularExpression, data: &REntry, stringtab: &mut impl HostStringTableSet) ->
		RegularExpressionEntry
	{
		let data = RegularExpressionEntryData {
			insecure: data.insecure.clone(),
			blacklisted: data.blacklisted,
			add_cors: data.add_cors,
			force_mime: data.force_mime.as_ref().map(|x|stringtab.set(x)),
			logflags: data.logflags,
			raw_host: data.raw_host,
			raw_path: data.raw_path,
			remaphost: data.remaphost.as_ref().map(|x|stringtab.set(x)),
			remappath: data.remappath.as_ref().map(|x|stringtab.set(x)),
			backend: data.backend.as_ref().map(|x|stringtab.set(x)),
			ws_backend: data.ws_backend.as_ref().map(|x|stringtab.set(x)),
			fc_backend: data.fc_backend.as_ref().map(|(x,y)|{
				let x = stringtab.set(x);
				let y = stringtab.set(y);
				(x, y)
			}),
			redirect: data.redirect.as_ref().map(|&(mode, ref target)|{
				let target = stringtab.set(target);
				(mode, target)
			}),
			uncompressed: data.uncompressed,
			error: data.error,
		};
		RegularExpressionEntry {
			original: stringtab.set(&regex.original),
			invariant_length: regex.invariant.len(),
			start_slash: regex.start_slash,
			pattern: regex.pattern.clone(),
			data: data,
		}
	}
}

impl RegularExpressionTable
{
	fn new(entries: Vec<RegularExpressionEntry>, prefixes: Vec<HostStringIndex2>, stringtab: &String) ->
		RegularExpressionTable
	{
		let prefixes = PathPrefixTable::new(prefixes, stringtab);
		RegularExpressionTable { entries, prefixes }
	}
	fn lookup<'a>(&'a self, path: &[u8], stringtab: &impl HostStringTableGet, submatches: &mut [RegexMatch],
		first: usize) -> Option<(usize, &'a RegularExpressionEntryData)>
	{
		let mut errorbuf = [0;256];
		let (mpfx, entrylist) = self.prefixes.get_list_for_prefix(path, stringtab);
		fail_if_none!(entrylist.len() == 0);	//No regexps can match.
		let mpfxlen = mpfx.len();
		//Loop over all regexps with given prefix.
		for &entryidx in entrylist.iter() { if let Some(entry) = self.entries.get(entryidx) {
			//If this entry has already been considered on previous round, skip it.
			if entryidx < first { continue; }
			//If mpfxlen is invariant_length and start_slash is set, then next character in path
			//must be / or one past end, or regex will not match.
			if entry.start_slash && mpfxlen == entry.invariant_length {
				if path.get(mpfxlen).unwrap_or(&b'/') != &b'/' { continue; }
			}
			//This might match, try match.
			let whole = RegexMatch::new_range(0..path.len());
			let trailing = RegexMatch::new_range(entry.invariant_length..path.len());
			match match &entry.pattern {
				&Pattern::ExactMatch => Ok(if path.len() == entry.invariant_length {
					assign2sm(submatches, whole, None);
					true
				} else {
					false
				}),
				&Pattern::AlwaysMatch(m1) => {
					assign2sm(submatches, whole, if m1 { Some(trailing) } else { None });
					Ok(true)
				},
				&Pattern::Regex(ref h) => h.match_string(path, false, false, submatches,
					&mut errorbuf)
			} {
				//entryidx+1 in order to skip this entry on next round.
				Ok(true) => return Some((entryidx+1,&entry.data)),
				Ok(false) => (),
				Err(err) => syslog!(WARNING "Regex match failed: {err}",
					err=SafeShowByteString(err))
			}
		}}
		None
	}
}

fn assign2sm(submatches: &mut [RegexMatch], a: RegexMatch, b: Option<RegexMatch>)
{
	submatches.get_mut(0).map(|out|*out = a);
	if let Some(b) = b { submatches.get_mut(1).map(|out|*out = b); }
}

#[derive(Copy,Clone,Debug)]
pub enum RegularExpressionInsecure
{
	///Block request.
	Block,
	///Redirect request to https.
	Redirect(RedirectMode),
	///Allow request.
	Allow,
}

#[derive(Clone,Debug)]
pub(crate) struct RegularExpressionEntryData
{
	pub insecure: Option<RegularExpressionInsecure>,
	pub blacklisted: bool,
	pub redirect: Option<(RedirectMode, HostStringIndex2)>,
	pub add_cors: bool,
	pub raw_host: bool,
	pub raw_path: bool,
	pub uncompressed: bool,
	pub logflags: Option<u32>,
	pub remaphost: Option<HostStringIndex2>,
	pub remappath: Option<HostStringIndex2>,
	pub force_mime: Option<HostStringIndex2>,
	pub fc_backend: Option<(HostStringIndex2, HostStringIndex2)>,
	pub ws_backend: Option<HostStringIndex2>,
	pub backend: Option<HostStringIndex2>,
	pub error: u16,
}

#[derive(Clone,Debug)]
pub struct REntry
{
	pub insecure: Option<RegularExpressionInsecure>,
	pub blacklisted: bool,
	pub redirect: Option<(RedirectMode, String)>,
	pub add_cors: bool,
	pub raw_host: bool,
	pub raw_path: bool,
	pub uncompressed: bool,
	pub logflags: Option<u32>,
	pub force_mime: Option<String>,
	pub remaphost: Option<String>,
	pub remappath: Option<String>,
	pub fc_backend: Option<(String, String)>,
	pub ws_backend: Option<String>,
	pub backend: Option<String>,
	pub error: u16,
}

#[derive(Clone,Debug)]
struct LocationRewriteEntry
{
	source: RegularExpression,
	target: Vec<u8>,
}

#[derive(Clone,Debug)]
pub(crate) struct LocationRewrite
{
	entries: Vec<LocationRewriteEntry>,
}

impl LocationRewrite
{
	fn __rewrite_matches(source: &RegularExpression, location: &[u8], submatches: &mut [RegexMatch],
		errorbuf: &mut [u8]) -> bool
	{
		let invariant = &source.invariant;
		//TODO: Filter matches for better performance.
		//Check that invariant part matches.
		//If start_slash is set, next character after invariant part must be / or end of string.
		if !location.starts_with(invariant.as_bytes()) { return false; }
		if source.start_slash {
			if location.get(invariant.len()).unwrap_or(&b'/') != &b'/' { return false; }
		}
		let whole = RegexMatch::new_range(0..location.len());
		let trailing = RegexMatch::new_range(invariant.len()..location.len());
		(match &source.pattern {
			&Pattern::ExactMatch => Ok(if location.len() == invariant.len() {
				assign2sm(submatches, whole, None);
				true
			} else {
				false
			}),
			&Pattern::AlwaysMatch(m1) => {
				assign2sm(submatches, whole, if m1 { Some(trailing) } else { None });
				Ok(true)
			},
			&Pattern::Regex(ref h) => h.match_string(location, false, false, submatches, errorbuf)
		}).unwrap_or_else(|err|{
			syslog!(WARNING "Regex match failed: {err}", err=SafeShowByteString(err));
			false
		})
	}
	pub(crate) fn rewrite_headers(&self, block: &mut crate::headerblock::HeaderBlock)
	{
		//Do not do anything if there is no location.
		let location = match block.get_special(ShdrLocation) { Some(l) => l, None => return };
		let mut output = [0;1024];
		let mut errorbuf = [0;256];
		let mut submatches = [RegexMatch::new();32];
		for LocationRewriteEntry{source, target} in self.entries.iter() {
			if Self::__rewrite_matches(source, location, &mut submatches, &mut errorbuf) {
				if let Ok(out) = placeholder_replace(&mut output, location, target, &submatches) {
					//Only do this if rewrite was successful. There can only be one of each
					//special header, so this overwrites the old value.
					block.set_special(ShdrLocation, out);
					return;
				}
			}
		}
	}
}

pub(crate) const MAX_REDIRECT_SUBS: usize = 32;
const FLAG_STRIP_SLASH: u32 = 1;
const FLAG_EMPTY_NE: u32 = 2;

fn parse_placeholder<'a>(placeholder: &'a [u8]) -> Result<(u32,usize,Option<&'a [u8]>), String>
{
	let mut flags = 0;
	//Find first : and split at it if found.
	let mut fcolon = None;
	for i in 0..placeholder.len() {
		if placeholder[i] == b':' {
			fcolon = Some(i);
			break;
		}
	}
	let (mut placeholder, tail) = match fcolon {
		Some(p) => (&placeholder[..p],Some(&placeholder[p+1..])),
		None => (placeholder, None)
	};
	while let Some((kind, ttail)) = placeholder.split_first() {
		match kind {
			b's' => flags |= FLAG_STRIP_SLASH,
			b'e' => flags |= FLAG_EMPTY_NE,
			_ => break
		}
		placeholder = ttail;
	}
	let placeholder = from_utf8(placeholder).set_err("Invalid non-UTF8 placeholder")?;
	let placeholder = usize::from_str(placeholder).set_err("Placeholder is not numeric")?;
	fail_if!(placeholder >= MAX_REDIRECT_SUBS, "Placeholder out of range");
	Ok((flags,placeholder,tail))
}

/*
	let exact_match = old_prefix.as_bytes() == block.get_special(ShdrPathO).unwrap_or(b"");
	let new_prefix = if new_prefix.ends_with("/") && !old_prefix.ends_with("/") && !exact_match {
		new_prefix.slice_r(..new_prefix.len()-1)
	} else {
		new_prefix.slice_r(..)
	}.unwrap_or("").as_bytes();
	let old_prefixlen = if old_prefix.ends_with("/") && !new_prefix.ends_with(b"/") {
		old_prefix.len()-1
	} else {
		old_prefix.len()
	};
	let mut prange = block.get_special_range(ShdrPathO).unwrap_or(0..0);
	prange.start += old_prefixlen;
	block.set_special_multipart(ShdrPathT, [HeaderSourceComponent::Slice(new_prefix),
		HeaderSourceComponent::Range(prange)].iter().cloned());


	old	path		new		result		tail	expression
	/foo	/foo		/bar		/bar		""	/bar{1}
	/foo	/foo/		/bar		/bar/		/	/bar{1}
	/foo	/foo/zot	/bar		/bar/zot	/zot	/bar{1}

	/foo	/foo		/bar/		/bar/		""	/bar/{s1:}
	/foo	/foo/		/bar/		/bar/		/	/bar/{s1:}
	/foo	/foo/zot	/bar/		/bar/zot	/zot	/bar/{s1:}

	/foo/	/foo/		/bar		/bar/		""	/bar/{s1:}
	/foo/	/foo/zot	/bar		/bar/zot	zot	/bar/{s1:}

	/foo/	/foo/		/bar/		/bar/		""	/bar/{s1:}
	/foo/	/foo/zot	/bar/		/bar/zot	zot	/bar/{s1:}

*/

fn get_placeholder_value<'a>(placeholder: &'a [u8], frags: &[Option<&'a [u8]>]) -> Result<&'a [u8], String>
{
	//Few special placeholders.
	if placeholder == b"lc" { return Ok(b"{"); }
	if placeholder == b"rc" { return Ok(b"}"); }
	let (flags, placeholder, default) = parse_placeholder(placeholder)?;
	let mut frag = frags.get(placeholder).and_then(|f|*f);
	//FLAG_STRIP_SLASH strips all leading /.
	if flags & FLAG_STRIP_SLASH != 0 { if let Some(frag) = frag.as_mut() {
		while let Some((b'/',tail)) = frag.split_first() { *frag = tail; }
	}}
	//FLAG_EMPTY_NE turns entry into None if it is empty. This happens after stripping any slashes.
	if flags & FLAG_EMPTY_NE != 0 {
		if frag == Some(b"") { frag = None; }
	}
	let frag = frag.or(default).ok_or("Placeholder points to non-existent fragment")?;
	Ok(frag)
}

fn do_write_placeholder(out: &mut [u8], pattern: &[u8], frags: &[Option<&[u8]>], witr: &mut usize, pstart: usize,
	idx: usize) -> Result<(), String>
{
	let placeholder = pattern.get(pstart..idx).ok_or("Internal error: Invalid placeholder range")?;
	let frag = get_placeholder_value(placeholder, frags)?;
	//Copy fragment to output.
	let nwitr = *witr + frag.len();
	fail_if!(nwitr < *witr || nwitr >= out.len(), "Output URL too long");
	(&mut out[*witr..nwitr]).copy_from_slice(frag);
	*witr = nwitr;
	Ok(())
}

fn placeholder_replace<'a>(out: &'a mut [u8], path: &[u8], pattern: &[u8], subs: &[RegexMatch;MAX_REDIRECT_SUBS]) ->
	Result<&'a [u8], String>
{
	let mut frags: [Option<&[u8]>;MAX_REDIRECT_SUBS] = [None;MAX_REDIRECT_SUBS];
	for idx in 0..MAX_REDIRECT_SUBS {
		frags[idx] = subs[idx].slice(path);
	}
	let mut placeholder_start = None;
	let mut witr = 0;
	for (idx, &c) in pattern.iter().enumerate() {
		if let Some(pstart) = placeholder_start {
			match c {
				b'{' => fail!("Invalid { in placeholder"),
				b'}' => {
					do_write_placeholder(out, pattern, &frags, &mut witr, pstart, idx)?;
					placeholder_start = None;
				},
				_ => (),	//Skip character.
			}
		} else {
			match c {
				b'{' => placeholder_start = Some(idx + 1),
				b'}' => fail!("Invalid } not part of placeholder"),
				c => {
					//If not processing a placeholder, and character is not curly
					//bracket, just copy it to output.
					fail_if!(witr >= out.len(), "Output URL too long");
					out[witr] = c;
					witr += 1;
				}
			}
		}
	}
	fail_if!(placeholder_start.is_some(), "Placeholder runs to end of pattern");
	let url = out.get(..witr).ok_or("Internal error: Wrote too much to URL output")?;
	Ok(url)
}

pub fn check_placeholder(pattern: &[u8]) -> Result<(), String>
{
	let mut placeholder_start = None;
	for (idx, &c) in pattern.iter().enumerate() {
		if let Some(pstart) = placeholder_start {
			match c {
				b'{' => fail!("Invalid { in placeholder"),
				b'}' => {
					let placeholder = pattern.get(pstart..idx).
						ok_or("Internal error: Invalid placeholder range")?;
					parse_placeholder(placeholder)?;
					placeholder_start = None;
				},
				_ => (),	//Skip character.
			}
		} else {
			match c {
				b'{' => placeholder_start = Some(idx + 1),
				b'}' => fail!("Invalid } not part of placeholder"),
				_ => ()	//Literial character.
			}
		}
	}
	fail_if!(placeholder_start.is_some(), "Placeholder runs to end of pattern");
	Ok(())
}

fn __pattern_replace<'a>(path: &[u8], to: &str, subs: &[RegexMatch;MAX_REDIRECT_SUBS],
	out: &'a mut [u8], allow_query: bool) -> Result<&'a [u8], String>
{
	let url = placeholder_replace(out, path, to.as_bytes(), subs)?;
	//If query is allowed, use full syntax, otherwise only accept paths.
	let valid = if allow_query { redirect_url_valid(url) } else { path_valid(url) };
	if valid { Ok(url) } else { fail!("Bad constructed URL"); }
}

pub(crate) fn redirect_pattern_replace<'a>(path: &[u8], to: &str, subs: &[RegexMatch;MAX_REDIRECT_SUBS],
	out: &'a mut [u8]) -> Result<&'a [u8], String>
{
	let url = placeholder_replace(out, path, to.as_bytes(), subs)?;
	if redirect_url_valid(url) { Ok(url) } else { fail!("Bad constructed URL"); }
}

pub(crate) fn path_pattern_replace<'a>(path: &[u8], to: &str, subs: &[RegexMatch;MAX_REDIRECT_SUBS],
	out: &'a mut [u8]) -> Result<&'a [u8], String>
{
	let url = placeholder_replace(out, path, to.as_bytes(), subs)?;
	if path_valid(url) { Ok(url) } else { fail!("Bad constructed URL"); }
}

//Output is guaranteed to be free of traversals and not start with /.
pub(crate) fn filename_replace<'a>(path: &[u8], to: &str, subs: &[RegexMatch;MAX_REDIRECT_SUBS],
	out: &'a mut [u8]) -> Result<Option<&'a Path>, String>
{
	let mut url = placeholder_replace(out, path, to.as_bytes(), subs)?;
	//Never generate slash as first character.
	while let Some(t) = url.strip_prefix(b"/") { url = t; }
	const STATE_INVALID: i32 = -4;
	const STATE_DOT: i32 = -1;
	const STATE_DOTDOT: i32 = -2;
	const STATE_AFTER_SLASH: i32 = 0;
	const STATE_NOT_BAD: i32 = 1;
	let mut state = STATE_AFTER_SLASH;
	for &c in url.iter() { state = match (state, c) {
		(STATE_INVALID, _) => STATE_INVALID,
		(STATE_AFTER_SLASH, b'.') => STATE_DOT,
		(STATE_AFTER_SLASH, b'/') => STATE_INVALID,
		(STATE_DOT, b'.') => STATE_DOTDOT,
		(STATE_DOT, b'/') => STATE_INVALID,
		(STATE_DOTDOT, b'/') => STATE_INVALID,
		(STATE_NOT_BAD, b'/') => STATE_AFTER_SLASH,
		(_, _) => STATE_NOT_BAD,
	}}
	let url = Filename::from_bytes_checked(url).ok_or("Invalid filename")?.into_path();
	if state < 0 { Ok(None) } else { Ok(Some(url)) }
}

fn redirect_url_valid(url: &[u8]) -> bool
{
	from_utf8(url).ok().and_then(|url|is_valid_uri(url).ok()).is_some()
}

fn path_valid(url: &[u8]) -> bool
{
	//This needs to force first character to be '/', as these are supposed to be valid paths.
	url.starts_with(b"/") && from_utf8(url).ok().and_then(|url|check_uri_path(url).ok()).is_some()
}

#[derive(Clone,Debug)]
pub struct PathData
{
	pub(crate) noaccess: Option<bool>,
	pub(crate) add_cors: Option<bool>,
	pub(crate) backend: Option<HostStringIndex2>,
	pub(crate) fc_backend: Option<HostStringIndex2>,
	pub(crate) remaphost: Option<HostStringIndex2>,
	pub(crate) remappath: Option<HostStringIndex2>,
	pub(crate) raw_host: Option<bool>,
	pub(crate) raw_path: Option<bool>,
	pub(crate) insecure_mode: Option<InsecureMode>,
	pub(crate) logflags: Option<u32>,
	_dummy: ()
}

struct PrintPathData<'a>(&'a HostRoutingData, &'a PathData);

impl<'a> Display for PrintPathData<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		let selfx = self.1;
		let host = self.0;
		let mut one = false;
		if let Some(backend) = selfx.backend {
			let backend = host.get_string(backend);
			if backend.len() > 0 {
				add_space!(one f); write!(f, "backend {backend}")?;
			} else {
				add_space!(one f); write!(f, "backend \"\"")?;
			}
		}
		if let Some(fc_backend) = selfx.fc_backend {
			add_space!(one f); write!(f, "fc-backend {bk}", bk=PrintHostString(host, fc_backend))?;
		}
		if let Some(remaphost) = selfx.remaphost {
			add_space!(one f); write!(f, "host {host}", host=PrintHostString(host, remaphost))?;
		}
		if let Some(remappath) = selfx.remappath {
			add_space!(one f); write!(f, "path {path}", path=PrintHostString(host, remappath))?;
		}
		if let Some(flags) = selfx.logflags.as_ref() {
			add_space!(one f); write!(f, "logflags {flags}")?;
		}
		if let Some(insecure_mode) = selfx.insecure_mode {
			add_space!(one f); write!(f, "insecure {insecure_mode:?}")?;
		}
		if selfx.add_cors == Some(true) { add_space!(one f); f.write_str("cors")?; }
		if selfx.add_cors == Some(false) { add_space!(one f); f.write_str("!cors")?; }
		if selfx.noaccess == Some(true) { add_space!(one f); f.write_str("no-access")?; }
		if selfx.noaccess == Some(false) { add_space!(one f); f.write_str("!no-access")?; }
		if selfx.raw_host == Some(true) { add_space!(one f); f.write_str("raw-host")?; }
		if selfx.raw_host == Some(false) { add_space!(one f); f.write_str("!raw-host")?; }
		if selfx.raw_path == Some(true) { add_space!(one f); f.write_str("raw-path")?; }
		if selfx.raw_path == Some(false) { add_space!(one f); f.write_str("!raw-path")?; }
		Ok(())
	}
}

impl PathData
{
	pub fn new() -> PathData
	{
		PathData {
			noaccess: None,
			add_cors: None,
			backend: None,
			fc_backend: None,
			remaphost: None,
			remappath: None,
			raw_host: None,
			raw_path: None,
			insecure_mode: None,
			logflags: None,
			_dummy: ()
		}
	}
	pub fn set_insecure_mode(&mut self, mode: Option<InsecureMode>) { self.insecure_mode = mode; }
	pub fn set_add_cors(&mut self, mode: bool) { self.add_cors = Some(mode); }
	pub fn set_noaccess(&mut self, mode: bool) { self.noaccess = Some(mode); }
	pub fn set_raw_host(&mut self, mode: bool) { self.raw_host = Some(mode); }
	pub fn set_raw_path(&mut self, mode: bool) { self.raw_path = Some(mode); }
	pub fn set_remaphost(&mut self, host: &mut HostRoutingDataFactory, mode: &str)
	{
		self.remaphost = Some(host.new_string(mode));
	}
	pub fn set_remappath(&mut self, host: &mut HostRoutingDataFactory, mode: &str)
	{
		self.remappath = Some(host.new_string(mode));
	}
	pub fn set_backend(&mut self, host: &mut HostRoutingDataFactory, mode: &str)
	{
		self.backend = Some(host.new_string(mode));
	}
	pub fn set_fc_backend(&mut self, host: &mut HostRoutingDataFactory, mode: &str)
	{
		self.fc_backend = Some(host.new_string(mode));
	}
	pub fn set_logflags(&mut self, mode: u32) { self.logflags = Some(mode); }
	pub fn get_backend_nt<'a>(&self, host: &'a HostRoutingData) -> Option<&'a str>
	{
		self.backend.map(|p|host.get_string(p))
	}
}

#[derive(Copy,Clone,Debug,PartialEq,Eq,Hash)]
pub struct HostStringIndex2(u32,u32);

struct PrintHostString<'a>(&'a HostRoutingData, HostStringIndex2);

impl<'a> Display for PrintHostString<'a>
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		f.write_str(self.0.get_string(self.1))
	}
}

impl HostStringIndex2
{
	//0,0 is always valid empty string.
	//pub(crate) fn new() -> HostStringIndex2 { HostStringIndex2(0, 0) }
	//Empty string can be recognized from its zero length.
	pub(crate) fn is_empty(&self) -> bool { self.1 == 0 }
}

pub trait HostStringTableGet
{
	fn get<'a>(&'a self, idx: HostStringIndex2) -> &'a str;
}

pub trait HostStringTableSet: HostStringTableGet
{
	fn set(&mut self, s: &str) -> HostStringIndex2;
}

impl HostStringTableGet for String
{
	fn get<'a>(&'a self, index: HostStringIndex2) -> &'a str
	{
		let start = index.0 as usize;
		let length = index.1 as usize;
		let end = start + length;
		str::get(self, start..end).unwrap_or_else(||{
			syslog!(CRITICAL "Invalid string pointer: start={start} length={length} datalen={dlen}",
				dlen=self.len());
			"(null)"
		})
	}
}

struct BuildingHostStrings<'a>(&'a mut String, &'a mut Option<HashMap<String, HostStringIndex2>>);

impl<'x> HostStringTableGet for BuildingHostStrings<'x>
{
	fn get<'a>(&'a self, index: HostStringIndex2) -> &'a str
	{
		HostStringTableGet::get(self.0, index)
	}
}

impl<'x> HostStringTableSet for BuildingHostStrings<'x>
{
	fn set(&mut self, string: &str) -> HostStringIndex2
	{
		if let Some(scache) = self.1.as_ref() {
			if let Some(key) = scache.get(string) { return *key; }
		}
		let start = self.0.len() as u32;
		let length = string.len() as u32;
		self.0.push_str(string);
		let ret = HostStringIndex2(start, length);
		if let Some(scache) = self.1.as_mut() {
			if !scache.contains_key(string) { scache.insert(string.to_owned(), ret); }
		}
		ret
	}
}

#[derive(Debug)]
pub struct HostRoutingData
{
	strings: String,
	canonicalhost: Option<(HostStringIndex2, RedirectMode)>,
	root_redirect: Option<(HostStringIndex2, RedirectMode)>,
	global_path: PathData,
	paths: Vec<(HostStringIndex2, usize, PathData)>,
	disable: Vec<(HostStringIndex2, IDisableMode)>,
	forcemime: Vec<(HostStringIndex2, HostStringIndex2)>,
	//Regular expression paths.
	regex_paths: RegularExpressionTable,
	location_rewrite: LocationRewrite,
	requirements: u8,
	trace_flag: bool,
	h2_max_streams: Option<u32>,
	no_redirect_fixup: bool,
	acl: HashSet<[u8;32]>,	//Shadow copy of TLS ACLs.
	//These can be Arc, since these are never cloned in the dataplane processing.
	ip_banned: Option<Arc<FrozenIpSet>>,
	request_rewrite: HeaderRewrite,
	response_rewrite: HeaderRewrite,
	_dummy: ()
}

#[derive(Clone,Debug)]
pub struct HostRoutingDataRef(Arc<HostRoutingData>);

impl Display for HostRoutingData
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		f.write_str("backend ")?;
		if let Some(backend) = self.global_path.backend {
			let backend = self.get_string(backend);
			if backend.len() > 0 {
				Display::fmt(backend, f)?;
			} else {
				f.write_str("\"\"")?;
			}
		} else {
			f.write_str("(null)")?;
		};
		if let Some(&(target, rkind)) = self.canonicalhost.as_ref() {
			write!(f, "\n\tcanonical_host {target} {rkind}", target=PrintHostString(self, target))?;
		}
		if let Some(req_host) = self.global_path.remaphost {
			write!(f, "\n\trewrite_host {target}", target=PrintHostString(self, req_host))?;
		}
		if let Some(req_path) = self.global_path.remappath {
			write!(f, "\n\trewrite_path {target}", target=PrintHostString(self, req_path))?;
		}
		if let Some(&(root_redirect, mode)) = self.root_redirect.as_ref() {
			write!(f, "\n\troot_redirect {target} {mode}", target=PrintHostString(self, root_redirect))?;
		}
		if let Some(flags) = self.global_path.logflags {
			write!(f, "\n\tlogflags {flags}")?;
		}
		f.write_str("\n\tinsecure ")?;
		match self.global_path.insecure_mode {
			Some(InsecureMode::Forbidden) => f.write_str("forbidden")?,
			Some(InsecureMode::RootOnly(flag)) => write!(f, "root-only {flag}")?,
			Some(InsecureMode::RootOnlyFlag(flag)) => write!(f, "root-only-flag {flag}")?,
			Some(InsecureMode::Redirect(flag)) => write!(f, "redirect {flag}")?,
			Some(InsecureMode::Allow) => f.write_str("allow")?,
			None => (),
		};
		for p in self.paths.iter() {
			write!(f, "\n\tpath {path} {pdata}",
				path=PrintHostString(self, p.0), pdata=PrintPathData(self, &p.2))?;
		}
		for e in self.regex_paths.entries.iter() {
			use std::fmt::Write;
			let mut attribs = String::new();
			let d = &e.data;
			if d.blacklisted { write!(attribs, " blacklist").ok(); }
			if d.raw_host { write!(attribs, " raw-host").ok(); }
			if d.raw_path { write!(attribs, " raw-path").ok(); }
			if d.uncompressed { write!(attribs, " uncompressed").ok(); }
			if d.add_cors { write!(attribs, " add-cors").ok(); }
			if let Some(flags) = d.logflags {
				write!(attribs, " log-flags={flags}").ok();
			}
			if let Some(x) = d.remaphost {
				write!(attribs, " remap-host={target}", target=PrintHostString(self, x)).ok();
			}
			if let Some(x) = d.remappath {
				write!(attribs, " remap-path={target}", target=PrintHostString(self, x)).ok();
			}
			if let Some(x) = d.force_mime {
				write!(attribs, " mime={target}", target=PrintHostString(self, x)).ok();
			}
			if let Some((x, y)) = d.fc_backend {
				write!(attribs, " static={base}//{path}",
					base=PrintHostString(self, x), path=PrintHostString(self, y)).ok();
			}
			if let Some(x) = d.backend {
				write!(attribs, " backend={target}", target=PrintHostString(self, x)).ok();
			}
			if let Some((mode, y)) = d.redirect {
				write!(attribs, " redirect={mode}:{target}", target=PrintHostString(self, y)).ok();
			}
			match d.insecure {
				Some(RegularExpressionInsecure::Allow) => write!(attribs, " no-security"),
				Some(RegularExpressionInsecure::Block) => write!(attribs, " secure"),
				Some(RegularExpressionInsecure::Redirect(redir)) =>
					write!(attribs, " http-redirect={redir}"),
				None => Ok(()),
			}.ok();
			write!(f, "\n\tpath-regex {target} {attribs}", target=PrintHostString(self, e.original))?;
		}
		for d in self.disable.iter() {
			write!(f, "\n\tdisable {target} {mode}",
				target=PrintHostString(self, d.0), mode=PrintDisable(self, &d.1))?;
		}
		for d in self.forcemime.iter() {
			write!(f, "\n\tforce-mime {target} {mime}",
				target=PrintHostString(self, d.0), mime=PrintHostString(self, d.1))?;
		}
		for r in self.request_rewrite.delete.iter() {
			write!(f, "\n\trequest delete {header}", header=SafeShowByteString(r))?;
		}
		for &(ref n, ref v) in self.request_rewrite.add.iter() {
			write!(f, "\n\trequest add {header} {value}",
				header=SafeShowByteString(n), value=SafeShowByteString(v))?;
		}
		for r in self.response_rewrite.delete.iter() {
			write!(f, "\n\tresponse delete {header}", header=SafeShowByteString(r))?;
		}
		for &(ref n, ref v) in self.response_rewrite.add.iter() {
			write!(f, "\n\tresponse add {header} {value}",
				header=SafeShowByteString(n), value=SafeShowByteString(v))?;
		}
		for acl in self.acl.iter() {
			write!(f, "\n\tacl {acl}", acl=Hexdump(acl))?;
		}
		if self.global_path.raw_host == Some(true) { f.write_str("\n\traw-host")?; }
		if self.global_path.raw_path == Some(true) { f.write_str("\n\traw-path")?; }
		if self.requirements & HREQ_TLS13 != 0 { f.write_str("\n\ttls13-only")?; }
		if self.requirements & HREQ_SECURE192 != 0 { f.write_str("\n\tsecure192")?; }
		if self.trace_flag { f.write_str("\n\thttp2-trace")?; }
		if let Some(h2_max_streams) = self.h2_max_streams {
			write!(f, "\n\thttp2-max-streams {h2_max_streams}")?;
		}
		if self.no_redirect_fixup { f.write_str("\n\tno-redirect-fixup")?; }
		Ok(())
	}
}

thread_local!(static DUMMY_REWRITE: Arc<HeaderRewrite> = Arc::new(HeaderRewrite {
	delete: HashSet::new(),
	add: Vec::new(),
}));

impl HostRoutingData
{
	fn get_string<'a>(&'a self, index: HostStringIndex2) -> &'a str
	{
		HostStringTableGet::get(&self.strings, index)
	}
	pub fn iterate_paths<'a>(&'a self) -> impl Iterator<Item=(&'a str, &'a PathData)>+'a
	{
		//Drop the index in the middle. Assume that the get_string never fails.
		self.paths.iter().map(move |&(x,_,ref z)|(self.get_string(x),z))
	}
	pub fn get_backend_nt(&self) -> Option<&str> { self.global_path.get_backend_nt(self) }
/*
	#[cfg(test)]
	fn new_string(&mut self, s: &str) -> HostStringIndex2
	{
		let x = self.strings.len() as u32;
		self.strings.push_str(s);
		HostStringIndex2(x, s.len() as u32)
	}
*/
	#[cfg(test)]
	fn wrap(self) -> HostRoutingDataRef { HostRoutingDataRef(Arc::new(self)) }
}

impl HostRoutingDataRef
{
	//Get string corresponding to HostStringIndex2.
	pub(crate) fn get_string<'a>(&'a self, index: HostStringIndex2) -> &'a str
	{
		HostStringTableGet::get(&self.0.strings, index)
	}
	pub(crate) fn lookup_regex<'a>(&'a self, path: &[u8], submatches: &mut [RegexMatch], first: usize) ->
		Option<(usize, &'a RegularExpressionEntryData)>
	{
		self.0.regex_paths.lookup(path, &self.0.strings, submatches, first)
	}
	pub(crate) fn lookup_path_specifics<'a>(&'a self, path: &[u8]) -> Option<(&'a str, &'a PathData)>
	{
		let selfx = self.0.deref();
		//Take the lower bound of path in pathlist and then scan ancestors downward the tree.
		let mut candidate = selfx.paths.binary_search_by(|&(s,_,_)|{
			selfx.get_string(s).as_bytes().cmp(path)
		}).unwrap_or_else(|x|x.saturating_sub(1));
		while let Some(&(cpath, next_candidate, ref pathdata)) = selfx.paths.get(candidate) {
			//Assume this never fails.
			let cpath = selfx.get_string(cpath);
			if prefix_matches_path(cpath, path) {
				return Some((cpath, pathdata));
			}
			candidate = next_candidate;
		}
		//The path does not exist at all. There is no path-specific data.
		None
	}
	pub(crate) fn global_path_settings<'a>(&'a self) -> &'a PathData { &self.0.global_path }
	pub(crate) fn get_request_rewrite(&self) -> &HeaderRewrite { &self.0.request_rewrite }
	pub(crate) fn get_response_rewrite(&self) -> &HeaderRewrite { &self.0.response_rewrite }
	pub(crate) fn get_location_rewrite(&self) -> &LocationRewrite { &self.0.location_rewrite }
	pub(crate) fn get_root_redirect(&self) -> Option<(&str, RedirectMode)>
	{
		let selfx = self.0.deref();
		let (tgt, mode) = selfx.root_redirect?;
		Some((selfx.get_string(tgt), mode))
	}
	pub(crate) fn get_canonicalhost(&self) -> Option<(&str, RedirectMode)>
	{
		let selfx = self.0.deref();
		let (tgt, mode) = selfx.canonicalhost?;
		Some((selfx.get_string(tgt), mode))
	}
	pub(crate) fn get_mime_type(&self, path: &[u8]) -> Option<HostStringIndex2>
	{
		let selfx = self.0.deref();
		selfx.forcemime.binary_search_by(|&(s,_)|{
			//This get_string() should never fail.
			selfx.get_string(s).as_bytes().cmp(path)
		}).ok().and_then(|idx|selfx.forcemime.get(idx)).map(|x|x.1)
	}
	pub(crate) fn get_disable<'a>(&'a self, path: &[u8]) -> Option<IDisableMode>
	{
		let selfx = self.0.deref();
		selfx.disable.binary_search_by(|&(s,_)|{
			selfx.get_string(s).as_bytes().cmp(path)
		}).ok().and_then(|idx|selfx.disable.get(idx)).map(|x|x.1)
	}
	pub(crate) fn has_acl_list(&self) -> bool { self.0.acl.len() > 0 }
	pub(crate) fn on_acl_list(&self, conn: Option<&[u8;32]>) -> bool
	{
		let selfx = self.0.deref();
		selfx.acl.len() == 0 || if let Some(conn) = conn { selfx.acl.contains(conn) } else { false }
	}
	pub(crate) fn is_ip_banned(&self, ip: IpAddr) -> bool
	{
		self.0.ip_banned.as_ref().map(|bans|bans.lookup(ip)).unwrap_or(false)
	}
	pub(crate) fn get_req_tls13(&self) -> bool { self.0.requirements & HREQ_TLS13 != 0 }
	pub(crate) fn get_req_secure192(&self) -> bool { self.0.requirements & HREQ_SECURE192 != 0 }
	pub(crate) fn get_h2_trace_flag(&self) -> bool { self.0.trace_flag }
	pub(crate) fn get_h2_max_streams(&self) -> Option<u32> { self.0.h2_max_streams }
	pub(crate) fn get_no_redirect_fixup(&self) -> bool { self.0.no_redirect_fixup }
	//#[cfg(test)]
	//fn unwrap(self) -> HostRoutingData { Arc::try_unwrap(self.0).ok().expect("Not sole reference") }
}

pub struct HostRoutingDataFactory
{
	strings: String,
	stringcache: Option<HashMap<String, HostStringIndex2>>,
	canonicalhost: Option<(HostStringIndex2, RedirectMode)>,
	root_redirect: Option<(HostStringIndex2, RedirectMode)>,
	global_path: PathData,
	paths: Vec<(HostStringIndex2, usize, PathData)>,
	disable: Vec<(HostStringIndex2, IDisableMode)>,
	forcemime: Vec<(HostStringIndex2, HostStringIndex2)>,
	//Regular expression paths.
	regex_paths: Vec<RegularExpressionEntry>,
	regex_path_pfx: Vec<HostStringIndex2>,
	location_rewrite: Vec<LocationRewriteEntry>,
	//regex_paths: RegularExpressionTable,
	requirements: u8,
	trace_flag: bool,
	h2_max_streams: Option<u32>,
	no_redirect_fixup: bool,
	acl: HashSet<[u8;32]>,	//Shadow copy of TLS ACLs.
	//These can be Arc, since these are never cloned in the dataplane processing.
	ip_banned: Option<Arc<FrozenIpSet>>,
	request_rewrite: HeaderRewrite,
	response_rewrite: HeaderRewrite,
}

impl HostRoutingDataFactory
{
	pub fn new() -> HostRoutingDataFactory
	{
		HostRoutingDataFactory {
			strings: String::new(),
			stringcache: Some(HashMap::new()),
			request_rewrite: HeaderRewrite {
			delete: HashSet::new(),
			add: Vec::new(),
			},
			response_rewrite: HeaderRewrite {
				delete: HashSet::new(),
				add: Vec::new(),
			},
			location_rewrite: Vec::new(),
			global_path: PathData::new(),
			paths: Vec::new(),
			canonicalhost: None,
			root_redirect: None,
			regex_paths: Vec::new(),
			regex_path_pfx: Vec::new(),
			disable: Vec::new(),
			forcemime: Vec::new(),
			acl: HashSet::new(),
			ip_banned: None,
			requirements: 0,
			trace_flag: false,
			h2_max_streams: None,
			no_redirect_fixup: false,
		}
	}
	pub fn done(self) -> HostRoutingData
	{
		let strings = self.strings;
		let regex_paths = RegularExpressionTable::new(self.regex_paths, self.regex_path_pfx, &strings);
		//Paths needs to be sorted.
		let mut paths = self.paths;
		paths.sort_by(|&(k1,_,_),&(k2,_,_)|{
			HostStringTableGet::get(&strings, k1).cmp(&HostStringTableGet::get(&strings, k2))
		});
		paths.dedup_by(|(k1,_,_),(k2,_,_)|k1==k2);
		set_path_tree_indices(&mut paths, &strings);
		//Disable needs to be sorted.
		let mut disable = self.disable;
		disable.sort_by(|&(k1,_),&(k2,_)|{
			HostStringTableGet::get(&strings, k1).cmp(&HostStringTableGet::get(&strings, k2))
		});
		disable.dedup_by(|(k1,_),(k2,_)|k1==k2);
		//forcemime needs to be sorted.
		let mut forcemime = self.forcemime;
		forcemime.sort_by(|&(k1,_),&(k2,_)|{
			HostStringTableGet::get(&strings, k1).cmp(&HostStringTableGet::get(&strings, k2))
		});
		forcemime.dedup_by(|(k1,_),(k2,_)|k1==k2);

		HostRoutingData {
			strings: strings,
			request_rewrite: self.request_rewrite,
			response_rewrite: self.response_rewrite,
			location_rewrite: LocationRewrite {
				entries: self.location_rewrite,
			},
			global_path: self.global_path,
			paths: paths,
			canonicalhost: self.canonicalhost,
			root_redirect: self.root_redirect,
			regex_paths: regex_paths,
			disable: disable,
			forcemime: forcemime,
			acl: self.acl,
			ip_banned: self.ip_banned,
			requirements: self.requirements,
			trace_flag: self.trace_flag,
			h2_max_streams: self.h2_max_streams,
			no_redirect_fixup: self.no_redirect_fixup,
			_dummy: ()
		}
	}
	pub fn get_string<'a>(&'a self, index: HostStringIndex2) -> &'a str
	{
		HostStringTableGet::get(&self.strings, index)
	}
	fn new_string(&mut self, string: &str) -> HostStringIndex2
	{
		BuildingHostStrings(&mut self.strings, &mut self.stringcache).set(string)
	}
	pub fn add_regex(&mut self, regex: &RegularExpression, entry: &REntry)
	{
		let invariant_pfx = self.new_string(&regex.invariant);
		let mut set = BuildingHostStrings(&mut self.strings, &mut self.stringcache);
		self.regex_paths.push(RegularExpressionEntry::new(&regex, entry, &mut set));
		self.regex_path_pfx.push(invariant_pfx);
	}
	pub fn add_path_data(&mut self, path: &str, data: &PathData)
	{
		let path = self.new_string(path);
		self.paths.push((path, USIZE_MAX, data.clone()));
	}
	pub fn add_disable_data(&mut self,path: &str, data: &DisableMode)
	{
		use DisableMode as DM;
		use IDisableMode as IM;
		type Ctor = fn(HostStringIndex2)->IM;
		let path = self.new_string(path);
		let (ctor, s) = match data {
			DM::MovedPermanently(ref s) => (IM::MovedPermanently as Ctor, s),
			DM::Found(ref s) => (IM::Found as Ctor, s),
			DM::TemporaryRedirect(ref s) => (IM::TemporaryRedirect as Ctor, s),
			DM::PermanentRedirect(ref s) => (IM::PermanentRedirect as Ctor, s),
			DM::Forbidden(ref s) => (IM::Forbidden as Ctor, s),
			DM::NotFound(ref s) => (IM::NotFound as Ctor, s),
			DM::Gone(ref s) => (IM::Gone as Ctor, s),
			DM::UnavailableForLegalReasons(ref s) => (IM::UnavailableForLegalReasons as Ctor, s),
		};
		let data = ctor(self.new_string(s));
		self.disable.push((path, data));
	}
	pub fn add_forcemime_data(&mut self, path: &str, data: &str)
	{
		let path = self.new_string(path);
		let data = self.new_string(data);
		self.forcemime.push((path, data));
	}
	pub fn set_global_path(&mut self, data: PathData) { self.global_path = data; }
	pub fn set_canonicalhost(&mut self, mode: (&str, RedirectMode))
	{
		self.canonicalhost = Some((self.new_string(mode.0), mode.1));
	}
	pub fn set_root_redirect(&mut self, mode: (&str, RedirectMode))
	{
		self.root_redirect = Some((self.new_string(mode.0), mode.1));
	}
	pub fn add_acl(&mut self, acl: [u8;32]) { self.acl.insert(acl); }
	pub fn add_location_rewrite(&mut self, source: &RegularExpression, target: &[u8])
	{
		self.location_rewrite.push(LocationRewriteEntry {
			source: source.clone(),
			target: target.to_vec(),
		})
	}
	pub fn set_request_rewrite(&mut self, r: HeaderRewrite) { self.request_rewrite = r; }
	pub fn set_response_rewrite(&mut self, r: HeaderRewrite) { self.response_rewrite = r; }
	pub fn set_ip_bans(&mut self, bans: Arc<FrozenIpSet>) { self.ip_banned = Some(bans); }
	pub fn set_req_tls13(&mut self) { self.requirements |= HREQ_TLS13; }
	pub fn set_req_secure192(&mut self) { self.requirements |= HREQ_SECURE192; }
	pub fn set_h2_trace_flag(&mut self) { self.trace_flag = true; }
	pub fn set_h2_max_streams(&mut self, max_streams: u32) { self.h2_max_streams = Some(max_streams); }
	pub fn set_redirect_fixup(&mut self) { self.no_redirect_fixup = false; }
	pub fn set_no_redirect_fixup(&mut self) { self.no_redirect_fixup = true; }
}

fn set_path_tree_indices<'a,T>(tree: &mut [(HostStringIndex2, usize, T)], table: &impl HostStringTableGet)
{
	let mut index = 0;
	//This will always run the entiere tree, as the starts_with check always is satisfied.
	_set_subordinate_indices(tree, USIZE_MAX, None, &mut index, table);
}

fn _set_subordinate_indices<'a,T>(tree: &mut [(HostStringIndex2, usize, T)], parent: usize,
	entry: Option<HostStringIndex2>, index: &mut usize, table: &impl HostStringTableGet)
{
	//entry = None corresponds to "", which always matches.
	let entry = entry.map(|x|table.get(x)).unwrap_or("");
	while *index < tree.len() && table.get(tree[*index].0).starts_with(entry) {
		tree[*index].1 = parent;
		//Attempt to run subordinates from position index + 1 with index as parent. In any case, index has
		//been used.
		let entry = tree[*index].0;
		let parent = *index;
		*index += 1;
		_set_subordinate_indices(tree, parent, Some(entry), index, table);
	}
}

fn prefix_matches_path(cpath: &str, path: &[u8]) -> bool
{
	//The match rules are:
	// - Exact match always matches.
	// - Otherwise the candidate has to begin with /.
	// - Otherwise the candidate must not be /
	// - Otherwise the candidate must be prefix of path.
	// - Otherwise either the candidate must end in '/', or be followed by '/' in path.
	path == cpath.as_bytes() ||
		(cpath.starts_with("/") && cpath.deref().deref() != "/" &&
		path.starts_with(cpath.as_bytes()) && (cpath.ends_with("/") ||
		path.get(cpath.len()) == Some(&b'/')))
}

#[test]
fn test_path_specific_lookup()
{
	let mut h = HostRoutingDataFactory::new();
	h.add_path_data("/", &PathData::new());
	h.add_path_data("/foo", &PathData::new());
	h.add_path_data("/foo/c", &PathData::new());
	h.add_path_data("/goo", &PathData::new());
	h.add_path_data("/foo/a/b", &PathData::new());
	h.add_path_data("/foo/a", &PathData::new());
	let h = h.done().wrap();
	assert_eq!(h.lookup_path_specifics(b"/foo/0").expect("Expected to find path entry A").0.deref().deref(),
		"/foo");
	assert_eq!(h.lookup_path_specifics(b"/foo/a").expect("Expected to find path entry B").0.deref().deref(),
		"/foo/a");
	assert_eq!(h.lookup_path_specifics(b"/foo/b").expect("Expected to find path entry C").0.deref().deref(),
		"/foo");
	assert_eq!(h.lookup_path_specifics(b"/foo/a/a").expect("Expected to find path entry D").0.deref().deref(),
		"/foo/a");
	assert_eq!(h.lookup_path_specifics(b"/foo/a/b").expect("Expected to find path entry E").0.deref().deref(),
		"/foo/a/b");
	assert_eq!(h.lookup_path_specifics(b"/foo/a/c").expect("Expected to find path entry F").0.deref().deref(),
		"/foo/a");
	assert_eq!(h.lookup_path_specifics(b"/").expect("Expected to find path entry G").0.deref().deref(),
		"/");
	assert!(h.lookup_path_specifics(b"/hoo").is_none());	//This should not fall back to root!
}

#[derive(Clone)]
pub struct RoutingContext
{
	pub hosts: RcuHashMap<String, HostRoutingData>,
	pub backends: RcuHashMap<String, BackendData>,
}

impl RoutingContext
{
	pub fn get_data_for(&self, authority: &[u8]) -> Option<HostRoutingDataRef>
	{
		//Empty name is reserved for server-global functions.
		if authority == b"" { return None; }
		if let Ok(authority) = from_utf8(authority) {
			//Try to parse authority as IP address. If sucessful, replace by RDNS name. However,
			//this lookup only matches on direct match, never on wildcard.
			if let Some(ip) = IpAddr::from_str(authority).ok() {
				let authority = reverse_addr_for(ip);
				self.hosts.get(authority.deref())
			} else if let Some(ret) = self.hosts.get(authority) {
				Some(ret)
			} else {
				//No direct match. Substitute a wildcard.
				match authority.find('.') {
					Some(x) => self.hosts.get(format!("*{rest}", rest=&authority[x..]).deref()),
					None => self.hosts.get("*"),
				}
			}.map(HostRoutingDataRef)
		} else {
			None
		}
	}
	pub fn get_backend_by_name(&self, backend: &str) -> Option<Arc<BackendData>>
	{
		return self.backends.get(backend)
	}
}

fn _is_send<T:Sized+Send>(_: T) {}
fn _is_sync<T:Sized+Sync>(_: T) {}
/*
#[cfg(test)]
static BOOLEAN_MASK_F: [(Option<bool>, Option<bool>, bool);9] = [
	(None, None, false),
	(None, Some(false), false),
	(None, Some(true), true),
	(Some(false), None, false),
	(Some(false), Some(false), false),
	(Some(false), Some(true), true),
	(Some(true), None, true),
	(Some(true), Some(false), false),
	(Some(true), Some(true), true),
];

#[cfg(test)]
static BOOLEAN_MASK_FS: [(Option<bool>, bool);3] = [
	(None, false),
	(Some(false), false),
	(Some(true), true),
];

#[cfg(test)]
fn path_test_boolean(get: impl Fn(&HostRoutingData, Option<&PathData>) -> bool,
	mut set: impl FnMut(&mut PathData, Option<bool>))
{
	let mut h = HostRoutingDataFactory::new().done();
	let mut p = PathData::new();
	for (he, r) in BOOLEAN_MASK_FS.iter() {
		set(&mut h.global_path, *he);
		assert!(get(&h, None) == *r);
	}
	for (he, pe, r) in BOOLEAN_MASK_F.iter() {
		set(&mut h.global_path, *he);
		set(&mut p, *pe);
		assert!(get(&h, Some(&p)) == *r);
	}
}

#[cfg(test)]
fn path_test_boolean_masked<G>(get: G, mut setm: impl FnMut(&mut PathData, Option<bool>),
	mut setv: impl FnMut(&mut PathData, Option<HostStringIndex2>), v1: Option<&str>, v2: Option<&str>,
	v: Option<&str>) where for<'a> G: Fn(&'a HostRoutingData, Option<&'a PathData>) -> Option<&'a str>
{
	let mut h = HostRoutingDataFactory::new().done();
	let mut p = PathData::new();
	let v1i = v1.map(|s|h.new_string(s));
	let v2i = v2.map(|s|h.new_string(s));
	setv(&mut h.global_path, v1i);
	setv(&mut p, v2i);
	for (he, r) in BOOLEAN_MASK_FS.iter() {
		setm(&mut h.global_path, *he);
		if *r {
			assert!(get(&h, None).is_none());
		} else {
			assert_eq!(get(&h, None), v1);	//Never masked by v2.
		}
	}
	for (he, pe, r) in BOOLEAN_MASK_F.iter() {
		setm(&mut h.global_path, *he);
		setm(&mut p, *pe);
		if *r {
			assert!(get(&h, Some(&p)).is_none());
		} else {
			assert_eq!(get(&h, Some(&p)), v);
		}
	}
}

#[test]
fn path_test_noaccess()
{
	path_test_boolean(|h,p|h.get_noaccess(p), |p,v|p.noaccess=v);
}

#[test]
fn path_test_add_cors()
{
	path_test_boolean(|h,p|h.get_add_cors(p), |p,v|p.add_cors=v);
}

#[test]
fn path_test_raw_host()
{
	path_test_boolean(|h,p|h.get_raw_host(p), |p,v|p.raw_host=v);
}

#[test]
fn path_test_raw_path()
{
	path_test_boolean(|h,p|h.get_raw_path(p), |p,v|p.raw_path=v);
}

#[test]
fn path_test_remaphost()
{
	path_test_boolean_masked(|h,p|h.get_remaphost(p), |p,v|p.raw_host=v, |p,v|p.remaphost=v,
		None, None, None);
	path_test_boolean_masked(|h,p|h.get_remaphost(p), |p,v|p.raw_host=v, |p,v|p.remaphost=v,
		Some("foo"), None, Some("foo"));
	path_test_boolean_masked(|h,p|h.get_remaphost(p), |p,v|p.raw_host=v, |p,v|p.remaphost=v,
		None, Some("bar"), Some("bar"));
	path_test_boolean_masked(|h,p|h.get_remaphost(p), |p,v|p.raw_host=v, |p,v|p.remaphost=v,
		Some("foo"), Some("bar"), Some("bar"));
}

#[test]
fn path_test_remappath()
{
	path_test_boolean_masked(|h,p|h.get_remappath(p), |p,v|p.raw_path=v, |p,v|p.remappath=v,
		None, None, None);
	path_test_boolean_masked(|h,p|h.get_remappath(p), |p,v|p.raw_path=v, |p,v|p.remappath=v,
		Some("foo"), None, Some("foo"));
	path_test_boolean_masked(|h,p|h.get_remappath(p), |p,v|p.raw_path=v, |p,v|p.remappath=v,
		None, Some("bar"), Some("bar"));
	path_test_boolean_masked(|h,p|h.get_remappath(p), |p,v|p.raw_path=v, |p,v|p.remappath=v,
		Some("foo"), Some("bar"), Some("bar"));
}

#[test]
fn path_test_bakend()
{
	let mut h = HostRoutingDataFactory::new().done();
	let mut p = PathData::new();
	let foo = h.new_string("foo");
	let bar = h.new_string("bar");
	assert!(h.get_backend(None).is_none());
	h.global_path.backend = Some(foo);
	assert_eq!(h.get_backend(None).unwrap(), foo);
	assert_eq!(h.get_backend(Some(&p)).unwrap(), foo);
	p.backend = Some(bar);
	assert_eq!(h.get_backend(Some(&p)).unwrap(), bar);
	h.global_path.backend = None;
	assert_eq!(h.get_backend(Some(&p)).unwrap(), bar);
	p.backend = None;
	assert!(h.get_backend(Some(&p)).is_none());
}
*/
