#!/usr/bin/python3
from hashlib import sha256;
import time;

def write_file(filename, contents):
	m = sha256();
	m.update(contents);
	newhash = m.digest();
	try:
		fp=open(filename,"rb");
		oldcontent = fp.read();
		m = sha256();
		m.update(oldcontent);
		oldhash = m.digest();
		#If hashes match, no update.
		if oldhash == newhash: return False
	except IOError:
		#Failed to compute hash. Assume update.
		pass;
	print("Updating "+filename);
	fp=open(filename,"wb");
	fp.write(contents);
	return True;

data = open("../../../data/hsts.inc","rb");
data = data.read().decode("utf-8");
ignore = True;
dname = {};
for line in data.splitlines():
	if line == "%%":
		ignore = not ignore;
		continue;
	if ignore: continue;
	line = line.split(",");
	name = line[0].strip();
	name = name[::-1];	#Reverse name.
	flag = int(line[1].strip());
	dname[name] = flag;
#Remove duplicates, as they cause trouble.
todelete = [];
for name in dname:
	candidate = name;
	while True:
		p = candidate.rfind(".");
		if p < 0: break;
		candidate = candidate[0:p];
		if candidate in dname and dname[candidate]: todelete.append(name);
for name in todelete: del dname[name];
snames=[];
for name in dname: snames.append(name);
snames.sort();
output=b"";
for name in snames:
	c = 10 if dname[name] else 13;
	output += chr(c).encode()+name.encode();


data = open("../../../data/timestamp.txt","rb");
timestamp = int(data.read().decode("utf-8"));
daynum = timestamp//86400;
ts = time.gmtime(timestamp);
vcode = "{:02}{:02}{:02}".format(ts.tm_year-2000, ts.tm_mon, ts.tm_mday);

if write_file("src/hsts.dat", output):
	daynum = daynum.to_bytes(8, byteorder='little');
	write_file("src/build-date.dat",daynum);
	print("Updated HSTS database");
	fp=open("Cargo.toml.in","r");
	fpw=open("Cargo.toml","w");
	lines = [line.rstrip('\n') for line in fp];
	for line in lines:
		p = line.find("@minor_versioncode@");
		if p < 0:
			#No mods, print as is.
			print(line, file=fpw);
		else:
			print(line[:p]+vcode+line[p+19:], file=fpw);
