use std::cmp::Ordering as CmpOrdering;

static HSTS_DAT: &'static [u8] = include_bytes!("hsts.dat");

fn on_hsts_preload_list_position(index: usize, name: &[u8]) -> CmpOrdering
{
	//If index is out of range, it is too high.
	if index >= HSTS_DAT.len() { return CmpOrdering::Greater; }
	let mut itr = (&HSTS_DAT[index..]).iter().cloned();
	//Find next synchronization point.
	loop {
		let undo = itr.clone();
		let c = itr.next().unwrap_or(10);
		if c == 10 || c == 13 { itr = undo; break; }
	}
	//If this gives None, mid is too high.
	let opcode = match itr.next() { Some(x) => x, None => return CmpOrdering::Greater };
	//Read name backwards.
	for i in 0..name.len() {
		let c = name[name.len()-i-1];
		let d = itr.next();
		match d {
			//End of name. Opcode 13 means our name is longer => too low.
			Some(13|10) if opcode == 13 => return CmpOrdering::Less,
			//End of name. Opcode 10 matches dot. Otherwise our name is longer. => too low.
			Some(13|10) if opcode == 10 && c == b'.' => return CmpOrdering::Equal,
			Some(13|10) => return CmpOrdering::Less,
			//Other characters mean too low/high according to compare.
			Some(d) if d > c => return CmpOrdering::Greater,
			Some(d) if d < c => return CmpOrdering::Less,
			Some(_) => (),	//Move to next.
			//None means too high.
			None => return CmpOrdering::Greater
		}
	}
	//Now either 13 or 10 is match, anything else is excess characters => too bigh.
	match itr.next() {
		Some(13|10)|None => return CmpOrdering::Equal,
		_ => return CmpOrdering::Greater
	}
}

pub fn on_hsts_preload_list(name: &[u8]) -> bool
{
	let mut base = 0;
	let mut size = HSTS_DAT.len();
	while size > 1 {
		let vtry = base + size / 2;
		match on_hsts_preload_list_position(vtry, name) {
			CmpOrdering::Less => base = vtry,
			CmpOrdering::Equal => return true,
			CmpOrdering::Greater => (),
		}
		size /= 2;
	}
	on_hsts_preload_list_position(base, name) == CmpOrdering::Equal
}

#[test]
fn hsts_tests()
{
	assert!(!on_hsts_preload_list(b""));
	assert!(!on_hsts_preload_list(b"x.000"));
	assert!(on_hsts_preload_list(b"foo.app"));
	assert!(!on_hsts_preload_list(b"example.com"));
	assert!(on_hsts_preload_list(b"0100dev.com"));
	assert!(!on_hsts_preload_list(b"x.0100dev.com"));
	assert!(!on_hsts_preload_list(b"x.zzz"));
	assert!(on_hsts_preload_list(b"test.app"));
}
