use std::collections::BTreeMap;
use std::env::var;
use std::fs::File;
use std::io::Write;
use std::path::Path;

fn main()
{
	const VERSION_OVERRIDE: &'static str = "version.override";
	use ::std::io::Read as IR;
	let mut content = String::new();
	let mut content2 = String::new();
	let mut override_version = String::new();
	::std::fs::File::open("mimetypes.dat").and_then(|mut fp|fp.read_to_string(&mut content)).unwrap();
	let mut types = BTreeMap::new();
	for line in content.lines() {
		if line.starts_with("#") || line.len() == 0 { continue; }
		let parts = line.split('\t').filter(|x|x.len()>0).collect::<Vec<_>>();
		if parts.len() != 2 { panic!("Bad mime line {line}"); }
		types.insert(parts[0], parts[1]);
	}
	if ::std::fs::File::open("/etc/mime.types").and_then(|mut fp|fp.read_to_string(&mut content2)).is_ok() {
		for line in content2.lines() {
			if line.starts_with("#") || line.len() == 0 { continue; }
			let parts = line.split(|x|x==' '||x=='\t').filter(|x|x.len()>0).collect::<Vec<_>>();
			for i in 1..parts.len() {
				types.insert(parts[i], parts[0]);
			}
		}
	}
	::std::fs::File::open(VERSION_OVERRIDE).and_then(|mut fp|fp.read_to_string(&mut override_version)).ok();

	let out_dir = var("OUT_DIR").unwrap();
	let product = var("CARGO_PKG_NAME").unwrap();
	let version = var("CARGO_PKG_VERSION").unwrap();
	let version = if override_version.len() > 0 { override_version } else { version };
	let dest_path = Path::new(&out_dir).join("product.inc");
	let mut f = File::create(&dest_path).unwrap();
	writeln!(f, "static PRODUCT_NAME: &'static str = \"{product}\";").unwrap();
	writeln!(f, "static PRODUCT_VERSION: &'static str = \"{version}\";", version=version.trim()).unwrap();

	let dest_path = Path::new(&out_dir).join("mimetypes.inc");
	let mut f = File::create(&dest_path).unwrap();
	writeln!(f, "static MIMETYPES: &'static [(&'static str, &'static str)] = &[").unwrap();
	for (i, j) in types.iter() { writeln!(f, "(\"{i}\", \"{j}\"),").unwrap(); }
	writeln!(f, "];").unwrap();

	//Only emit version.override as dependency if it exists, since otherwise cargo will pointlessly rebuild
	//the crate every time if the file is not found. On the other hand, if version override exists, it needs
	//to be listed as dependency as otherwise version number will be stuck on rebuilds.
	if Path::new(VERSION_OVERRIDE).exists() { println!("cargo:rerun-if-changed={VERSION_OVERRIDE}"); }
	//The build script has no dependicies besides the Cargo.toml variables, so prevent rebuilds if
	//the documentation changes. Also mimetypes.dat needs rebuild for it.
	println!("cargo:rerun-if-changed=mimetypes.dat");
}
