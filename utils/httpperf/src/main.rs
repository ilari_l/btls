#![allow(improper_ctypes)]
#![warn(unsafe_op_in_unsafe_fn)]

extern crate btls;
extern crate btls_aux_unix;
use btls::ClientConfiguration;
use btls::ClientConnection;
use btls::Connection;
use btls::transport::Queue;
use btls::transport::PullTcpBuffer;
use btls::transport::PushTcpBuffer;
use btls::callbacks::TlsCallbacks;
use btls::certificates::TrustAnchor;
use btls::utility::Mutex;
use btls_aux_fail::f_break;
use btls_aux_unix::FileDescriptorB;
use std::io::ErrorKind;
use std::io::Read;
use std::io::Write;
use std::time::Duration;
use std::time::Instant;
use std::ops::Deref;
use std::net::TcpStream;
use std::process::exit;
use std::sync::Arc;
use std::thread::sleep;
use std::os::unix::io::AsRawFd;


struct Controller
{
}

impl TlsCallbacks for Controller
{
}

fn format_duration(d: Duration) -> String
{
	if d.as_secs() == 0 {
		format!("{t}ms", t=d.subsec_nanos() / 1000000)
	} else  {
		format!("{s}{ss:03}ms", s=d.as_secs(), ss=d.subsec_nanos() / 1000000)
	}
}

//<addr> <host> <path> <anchor>

fn send_raw(data: PullTcpBuffer, tls: &mut ClientConnection, stream: &mut TcpStream)
{
	match tls.pull_tcp_data(data) {
		Ok(buf) => if let Err(err) = stream.write_all(&buf).and_then(|_|stream.flush()) {
			println!("Error writing to TCP: {err}");
			exit(1);
		},
		Err(err) =>  {
			println!("Error outputting from TLS: {err}");
			exit(1);
		}
	}
}

fn send(data: &[u8], tls: &mut ClientConnection, stream: &mut TcpStream)
{
	let data = if data.len() > 0 { PullTcpBuffer::write(data) } else { PullTcpBuffer::no_data() };
	send_raw(data, tls, stream)
}

macro_rules! fatal
{
	($($x:tt)*) => {{
		eprintln!($($x)*);
		exit(1);
	}}
}

fn receive(tls: &mut ClientConnection, rq: &mut Queue, stream: &mut TcpStream, buffer: &mut Vec<u8>)
{
	let amt = match stream.read(buffer) {
		Ok(0) => fatal!("Peer unexpectedly closed the connection"),
		Ok(r) => r,
		Err(err) => match err.kind() {
			ErrorKind::WouldBlock|ErrorKind::Interrupted => return,
			_ => fatal!("TCP read error: {err}")
		}
	};
	let (tmp, eofd) =  tls.push_tcp_data(PushTcpBuffer::u8_size(buffer, amt)).
		unwrap_or_else(|err|fatal!("TLS ERROR: {err}"));
	rq.write(&tmp).ok();
	if eofd { rq.send_eof(); }
}


fn main()
{
	let mut buffer = vec![0;16645];
	let mut a = std::env::args();
	a.next();
	let addr = a.next().expect("Expected address");
	let host = a.next().expect("Expected host");
	let path = a.next().expect("Expected path");
	let anchor = a.next().expect("Expected anchor");

	let httpreq = format!("GET /{path} HTTP/1.1\r\nHost: {host}\r\n\r\n");

	let controller = Arc::new(Mutex::new(Controller{}));

	//Load certificate.
	let mut client_config = ClientConfiguration::new();
	let ta = match TrustAnchor::from_certificate_file(&anchor) {
		Ok(x) => x,
		Err(err) => panic!("Can't load TA {anchor}: {err}")
	};
	client_config.add_trust_anchor(&ta);

	let basetime = Instant::now();
	let mut stream = match TcpStream::connect(addr.deref()) {
		Ok(stream) => stream,
		Err(err) => {
			println!("Error connecting to {addr}: {err}");
			return;
		}
	};
	//Disable nagle.
	unsafe { FileDescriptorB::new(stream.as_raw_fd()).set_tcp_nodelay(true).ok(); }
	println!("TCP: {t}", t=format_duration(Instant::now().duration_since(basetime)));
	let mut rq = Queue::new();
	let mut tls = client_config.make_connection(&host, Box::new(controller.clone()));
	loop {
		let flags = tls.get_status();
		if !flags.is_handshaking() { break; }
		if flags.is_want_tx() { send(&[], &mut tls, &mut stream); }
		if flags.is_want_rx() { receive(&mut tls, &mut rq, &mut stream, &mut buffer); }
	}
	println!("TLS: {t}", t=format_duration(Instant::now().duration_since(basetime)));
	send(httpreq.as_bytes(), &mut tls, &mut stream);
	let mut first = true;
	let mut received = Vec::new();
	let mut hsize = 0;
	let mut dsize = 0;
	loop {
		//Read only.
		receive(&mut tls, &mut rq, &mut stream, &mut buffer);
		if first && rq.buffered() > 0 {
			first = false;
			println!("Data: {t}", t=format_duration(Instant::now().duration_since(basetime)));
		}
		//Now, check when all data is received.
		let old = received.len();
		received.resize(old + rq.buffered(), 0);
		let x = f_break!(rq.read(&mut received[old..]));
		received.resize(old + x, 0);
		//Now, there is supposed to be '\r\n\r\n' in the receive buffer somewhere.

		//Now, there is supposed to be '\r\nContent-Length: ' in the receive buffer somewhere.
		let mut offset = 0;
		while offset < received.len() - 18 {
			if &received[offset..][..17] == b"\r\nContent-Length:" { break; }
			if &received[offset..][..17] == b"\r\ncontent-length:" { break; }
			offset += 1;
		}
		offset += 17;
		//The content length is..
		let mut clen = 0usize;
		while received.len() > offset && received[offset] != 13 {
			if received[offset] >= 48 && received[offset] <= 57 {
				clen = clen * 10 + (received[offset] - 48) as usize;
			}
			offset += 1;
		}
		dsize = clen;
		//Find end of headers.
		while offset < received.len() - 4 {
			if &received[offset..][..4] == b"\r\n\r\n" { break; }
			offset += 1;
		}
		offset += 4;
		hsize = offset;
		if received.len() >= offset + clen { break; }	//Done.
	}
	println!("Done: {t} ({recv}={hsize}+{dsize} bytes)",
		recv=received.len(), t=format_duration(Instant::now().duration_since(basetime)));
	//Send EOF, wait for close.
	send_raw(PullTcpBuffer::no_data().eof(), &mut tls, &mut stream);
	//Wait a bit.
	sleep(Duration::from_millis(500));
}
