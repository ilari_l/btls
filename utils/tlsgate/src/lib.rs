#![warn(unsafe_op_in_unsafe_fn)]
extern crate btls;
#[macro_use] extern crate btls_aux_fail;
extern crate btls_aux_time;
extern crate btls_aux_unix;
extern crate btls_daemon_helper_lib;
use btls::ClientConfiguration;
use btls::ClientConnection;
use btls::Connection as BtlsConnection;
use btls::callbacks::SelectClientCertificateParameters;
use btls::callbacks::TlsCallbacks;
use btls::certificates::ClientCertificate;
use btls::certificates::Hasher;
use btls::certificates::KeyPair2;
use btls::certificates::SignatureAlgorithmTls2;
use btls::certificates::FutureReceiver;
use btls::certificates::TemporaryRandomStream;
pub use btls::certificates::HostSpecificPin;
pub use btls::certificates::LocalKeyPair;
use btls::transport::Queue;
use btls::transport::PullTcpBuffer;
use btls::transport::PushTcpBuffer;
use btls_aux_fail::ResultExt;
use btls_aux_time::PointInTime;
use btls_aux_time::Timer;
use btls_aux_time::TimeUnit;
use btls_aux_unix::SocketFlags;
use btls_aux_unix::SocketType;
use btls_daemon_helper_lib::AddressHandle;
use btls_daemon_helper_lib::AddTokenCallback;
use btls_daemon_helper_lib::AllocatedToken;
use btls_daemon_helper_lib::ConnectionInfo;
use btls_daemon_helper_lib::CyclicArray;
use btls_daemon_helper_lib::FdPoller;
use btls_daemon_helper_lib::FileDescriptor;
use btls_daemon_helper_lib::IO_WAIT_HUP;
use btls_daemon_helper_lib::IO_WAIT_INTR;
use btls_daemon_helper_lib::IO_WAIT_ONESHOT;
use btls_daemon_helper_lib::IO_WAIT_READ;
use btls_daemon_helper_lib::IO_WAIT_WRITE;
use btls_daemon_helper_lib::is_fatal_error;
use btls_daemon_helper_lib::ListenerLoop;
use btls_daemon_helper_lib::Poll;
use btls_daemon_helper_lib::RemoteIrq;
use btls_daemon_helper_lib::SocketAddrEx;
use btls_daemon_helper_lib::SocketAddrExHandle;
use btls_daemon_helper_lib::TokenAllocatorPool;
use btls_daemon_helper_lib::UpperLayerConnection;
use btls_daemon_helper_lib::UpperLayerConnectionFactory;
use btls_util_logging::ConnId;
use btls_util_logging::log;
use std::borrow::Cow;
use std::cell::RefCell;
use std::io::ErrorKind;
use std::io::Read;
use std::mem::MaybeUninit;
use std::ops::Deref;
use std::sync::Arc;


//A certificate that has been loaded signing.
#[derive(Clone)]
pub struct LoadedCertificate
{
	//The keypair that does the signing.
	keypair: Arc<Box<dyn KeyPair2+Send+Sync>>,
	//The certificate.
	certificate: Arc<Vec<u8>>,
	//The certificate chain to send.
	chain: Arc<Vec<Vec<u8>>>,
}

impl LoadedCertificate
{
	pub fn new(keypair: Arc<Box<dyn KeyPair2+Send+Sync>>, certificate: Vec<u8>, chain: Vec<Vec<u8>>) ->
		LoadedCertificate
	{
		LoadedCertificate {
			keypair: keypair,
			certificate: Arc::new(certificate),
			chain: Arc::new(chain),
		}
	}
	fn select(&self, criteria: SelectClientCertificateParameters) -> Result<Box<dyn ClientCertificate+Send>, ()>
	{
		let available = self.keypair.get_signature_algorithms().iter().
			filter_map(|x|x.downcast_tls().map(|y|y.tls_id()));
		let algo = dtry!(criteria.choose_sigalg(available));
		Ok(Box::new(SelectedCertificate {
			keypair: self.keypair.clone(),
			tls_algorithm: algo,
			certificate: self.certificate.clone(),
			chain: self.chain.clone(),
		}))
	}
}


//A certificate that has been selected for signing.
#[derive(Clone)]
struct SelectedCertificate
{
	//The keypair that does the signing.
	keypair: Arc<Box<dyn KeyPair2+Send+Sync>>,
	//The TLS algorithm number to use.
	tls_algorithm: SignatureAlgorithmTls2,
	//The certificate.
	certificate: Arc<Vec<u8>>,
	//The certificate chain to send.
	chain: Arc<Vec<Vec<u8>>>,
}

impl ClientCertificate for SelectedCertificate
{
	fn sign(&self, data: &mut dyn FnMut(&mut Hasher), rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<(u16, Vec<u8>), ()>>
	{
		btls::certificates::tls_sign_helper(self.keypair.deref().deref(), data, self.tls_algorithm, rng)
	}
	fn certificate<'a>(&'a self) -> &'a [u8] { self.certificate.deref().deref() }
	fn chain<'a>(&'a self) -> &'a [Vec<u8>] { self.chain.deref().deref() }
	//We don't implement get_ocsp nor get_scts. These get replaced by defaults.
}


#[derive(Clone)]
pub struct GlobalConfiguration
{
	pub connect_timeout: TimeUnit,
	pub transfer_timeout: TimeUnit,
	pub address_list: Arc<CyclicArray<SocketAddrExHandle>>,
	pub sni: Arc<String>,
	pub pins: Arc<Vec<HostSpecificPin>>,
	pub certificate: Option<LoadedCertificate>,
	pub alpn: Option<Arc<String>>,
}

pub struct ConnectionFactory2(GlobalConfiguration);

impl ConnectionFactory2
{
	pub fn new2(cfg: GlobalConfiguration) -> Arc<dyn UpperLayerConnectionFactory>
	{
		Arc::new(ConnectionFactory2(cfg))
	}
}

impl UpperLayerConnectionFactory for ConnectionFactory2
{
	fn new(&self, poll: &mut Poll, allocator: &mut TokenAllocatorPool, fd: FileDescriptor, _: ConnectionInfo) ->
		Result<(Box<dyn UpperLayerConnection>, Vec<AllocatedToken>, bool), Cow<'static, str>>
	{
		let mut dummy = SocketAddrEx::Unknown;
		//Always pass !has_higher_layer, because this is used for raw TLS, which has no notion of higher
		//layer. Httpdemux2 calls Connection::accept directly. We do not need the AddressHandle the
		//Connection::accept() returns. The subtarget is always None, because we do not care about
		//subtarget as connections are always non-speculative.
		let (conn,tokens,_) = Connection::accept(poll, allocator, fd, &self.0, &mut dummy, false, None)?;
		//The address_valid is always false, because these connections can not be K-Lined at all.
		Ok((Box::new(conn), tokens, false))
	}
	//All remaps are trivial.
	fn remap(&self, addr: SocketAddrEx) -> SocketAddrEx { addr }
}

struct Controller
{
	certificate: Option<LoadedCertificate>,
}

impl Controller
{
	fn new(certificate: Option<LoadedCertificate>) -> Controller
	{
		Controller {
			certificate: certificate,
		}
	}
}

impl TlsCallbacks for Controller
{
	fn select_client_certificate(&mut self, criteria: SelectClientCertificateParameters) ->
		Option<Box<dyn ClientCertificate+Send>>
	{
		if let Some(ref key) = self.certificate.as_ref() { key.select(criteria).ok() } else { None }
	}
}

pub struct Connection
{
	error: Option<Cow<'static, str>>,
	address_handle: AddressHandle,
	has_higher_layer: bool,
	sock_in: FdPoller,
	sock_out: FdPoller,
	delayed: Option<SocketAddrEx>,
	transfer_timeout_duration: TimeUnit,
	token_master: AllocatedToken,
	token_slave: AllocatedToken,
	connect_timeout: Timer,
	transfer_timeout: Timer,
	tls: ClientConnection,
	irq: RemoteIrq,
	black_q: Queue,
	red_q: Queue,
	red_end_ack: bool,
	black_end_ack: bool,
	red_eof: bool,
	black_eof: bool,
}

impl Connection
{
	pub fn accept(poll: &mut Poll, allocator: &mut TokenAllocatorPool, socket: FileDescriptor,
		gconfig: &GlobalConfiguration, tgtaddr: &mut SocketAddrEx, has_higher_layer: bool,
		subtarget: Option<usize>) ->
		Result<(Connection, Vec<AllocatedToken>, AddressHandle), Cow<'static, str>>
	{
		//Need 3 tokens.
		let token = allocator.allocate().set_err("Can't allocate Tokens")?;
		let token_slave = allocator.allocate().set_err("Can't allocate Tokens")?;
		//Ensure the socket is in nonblocking mode.
		socket.set_nonblock().map_err(|err|format!("Failed to set socket nonblocking: {err}"))?;

		//Initialize TLS client connection.
		let mut config = ClientConfiguration::new();
		if let Some(ref alpn) = gconfig.alpn.as_ref() { config.add_alpn(alpn); }
		let controller = Controller::new(gconfig.certificate.clone());
		let irq = ListenerLoop::remote_irq_handle(&token_slave);
		let tls = config.make_connection_pinned(gconfig.sni.deref(), gconfig.pins.deref(),
			Box::new(controller));

		let destination = if let Some(subtarget) = subtarget {
			gconfig.address_list.index(subtarget).ok_or_else(||format!("Bad subtarget {subtarget}"))?
		} else {
			gconfig.address_list.get(|a|a.status_ok()).ok_or("No outbound targets available")?
		};
		*tgtaddr = destination.borrow_addr().clone();		//Save the address used for display.
		let address_handle = destination.borrow_status();

		let af = tgtaddr.get_family().
			ok_or_else(||format!("Failed to create socket {tgtaddr}: Unknown address type"))?;
		let flags = SocketFlags::NONBLOCK|SocketFlags::CLOEXEC;
		let fd = FileDescriptor::socket2(af, SocketType::STREAM, Default::default(), flags).map_err(|err|{
			format!("Failed to create socket {tgtaddr}: {err}")
		})?;
		let mut connect_timeout2 = None;
		let delayed = match fd.connect_ex(tgtaddr) {
			Ok(true) => {
				//Success only if there is no higher layer.
				if !has_higher_layer { address_handle.set_status(false); }
				log!(INFO "Connection to `{tgtaddr}` established.");
				//Kick, so to start the writing.
				token_slave.as_irq().irq();
				None
			},
			Ok(false) => {
				log!(INFO "Connection to `{tgtaddr}` in progress...");
				//Time out connects so they do not remain hanging for very long time.
				connect_timeout2 = Some(gconfig.connect_timeout);
				Some(tgtaddr.clone())
			},
			Err(err) => {
				//This is definite failure.
				address_handle.set_status(false);
				fail!(format!("Failed to connect to {destination}: {err}"));
			}
		};
		let socket = FdPoller::new(socket, token.value());
		let mut fd = FdPoller::new(fd, token_slave.value());
		//Watch for writability of outgoing socket in any case.
		fd.update(poll, IO_WAIT_WRITE | IO_WAIT_ONESHOT).
			map_err(|err|format!("Failed to register socket: {err}"))?;

		let tokens = vec![token.clone(), token_slave.clone()];
		Ok((Connection{
			error: None,
			address_handle: address_handle.clone(),
			has_higher_layer: has_higher_layer,
			sock_in: socket,
			sock_out: fd,
			delayed: delayed,
			transfer_timeout_duration: gconfig.transfer_timeout,
			token_master: token,
			token_slave: token_slave,
			connect_timeout: Timer::new_target(connect_timeout2),
			transfer_timeout: Timer::new(),
			tls: tls,
			irq: irq,
			black_q: Queue::new(),
			red_q: Queue::new(),
			red_end_ack: false,
			black_end_ack: false,
			red_eof: false,
			black_eof: false,
		}, tokens, address_handle.clone()))
	}
	fn __return_hard_error_or_ok(&self) -> Result<(), Cow<'static, str>>
	{
		match &self.error {
			&Some(ref x) => Err(x.clone()),
			&None => Ok(()),
		}
	}
}

thread_local!(static BLACK_BUFFER: RefCell<Vec<u8>> = RefCell::new(Vec::new()));

struct SendIrq(RemoteIrq);
impl btls::transport::WaitBlockCallback for SendIrq
{
	fn unblocked(&mut self) { self.0.irq(); }
}

impl Connection
{
	fn handle_black_irq(&mut self) -> Result<(), Cow<'static, str>>
	{
		//Do write, so the TLS can do whatever it needs to do.
		let tmp = self.tls.pull_tcp_data(PullTcpBuffer::no_data()).
			map_err(|err|format!("TLS error: {err}"))?;
		self.black_q.write(&tmp).ok();	//This should never fail.
		Ok(())
	}
	fn handle_black_in_with_buffer(&mut self, buffer: &mut Vec<u8>) -> Result<(), Cow<'static, str>>
	{
		let tls_error = |err:btls::Error|format!("TLS error: {err}");
		const BUFSZ: usize = 16645;
		if buffer.len() < BUFSZ { buffer.resize(BUFSZ, 0); }
		self.transfer_timeout.rearm(self.transfer_timeout_duration);
		let amt = match self.sock_out.borrow_mut().read(buffer) {
			Ok(0) => {
				self.tls.push_tcp_data(PushTcpBuffer::end()).map_err(tls_error)?;
				self.black_eof = true;
				return Ok(());
			},
			Ok(r) => r,
			Err(ref e) if e.kind() == ErrorKind::WouldBlock => return Ok(()),
			Err(ref e) if e.kind() == ErrorKind::Interrupted => return Ok(()),
			Err(err) => fail!(format!("TCP read(local): {err}"))
		};
		let (tmp, eofd) = self.tls.push_tcp_data(PushTcpBuffer::u8_size(buffer, amt)).map_err(tls_error)?;
		self.red_q.write(&tmp).ok();
		if eofd { self.red_q.send_eof(); }
		Ok(())
	}
	fn handle_black_in(&mut self) -> Result<(), Cow<'static, str>>
	{
		BLACK_BUFFER.with(|bb|self.handle_black_in_with_buffer(&mut bb.borrow_mut()))
	}
	fn handle_red_in(&mut self) -> Result<(), Cow<'static, str>>
	{
		self.transfer_timeout.rearm(self.transfer_timeout_duration);
		let mut buf = [MaybeUninit::uninit();16384];
		let buf = match self.sock_in.borrow_mut().read_uninit2(&mut buf) {
			Ok(x) => { self.red_eof |= x.len() == 0; x },
			Err(err) => if is_fatal_error(&err) {
				fail!(format!("TCP read(local): {err}"));
			} else {
				&[]
			}
		};
		let tmp = if self.red_eof { PullTcpBuffer::no_data().eof() } else { PullTcpBuffer::write(buf) };
		let tmp = self.tls.pull_tcp_data(tmp).map_err(|err|format!("TLS error: {err}"))?;
		self.black_q.write(&tmp).ok();		//This should never fail.
		Ok(())
	}
	fn handle_red_out(&mut self) -> Result<(), Cow<'static, str>>
	{
		//The only thing to do is just flush the output, and EOF if needed.
		if let Err(err) = self.red_q.call_write(self.sock_in.borrow_mut()) { if is_fatal_error(&err) {
			fail!(format!("TCP write(local): {err}"));
		}}
		if self.red_q.no_more_output() {
			match self.sock_in.borrow_mut().shutdown_write() {
				Ok(_) => self.red_end_ack = true,
				Err(err) => if is_fatal_error(&err) {
					fail!(format!("shutdown: {err}"));
				}
			}
		}
		Ok(())
	}
	fn handle_black_out(&mut self) -> Result<(), Cow<'static, str>>
	{
		//The only thing to do is just flush the output. No need to send EOF or to queue stuff
		//(that is done by read and interrupt cases.
		if let Err(err) = self.black_q.call_write(self.sock_out.borrow_mut()) { if is_fatal_error(&err) {
			fail!(format!("TCP write(remote): {err}"));
		}}
		Ok(())
	}
	fn handle_black_accept(&mut self, id: ConnId) -> Result<(), Cow<'static, str>>
	{
		//This should have connected.
		match self.sock_out.borrow_mut().so_error() {
			Err(err) => if is_fatal_error(&err) {
				fail!(format!("Failed to read SO_ERROR: {err}"));
			},
			Ok(Err(err)) => {
				//Failed to connect is definite failure.
				self.address_handle.set_status(false);
				fail!(format!("Failed to connect to {a}: {err}",
					a=self.delayed.as_ref().unwrap_or(&SocketAddrEx::Unknown)));
			},
			Ok(_) => {
				//Only set success here, if we do not have higher layer that can set success.
				if !self.has_higher_layer { self.address_handle.set_status(true); }
				log!(INFO "[{id}] Connection established.");
				self.delayed = None;
				//Do kick, in order to start the write.
				self.token_slave.as_irq().irq();
				//This does NOT unset connect timeout, as that is only done after
				//TLS handshake finishes. Nor will it set transfer timeout, as that
				//also is set after TLS handshake finishes.
			}
		}
		Ok(())
	}
}

impl UpperLayerConnection for Connection
{
	fn handle_fault(&mut self, _: &mut Poll) -> Option<Cow<'static, str>>
	{
		self.__return_hard_error_or_ok().err()
	}
	//Handle a timed event (return Err(()) if connection should be killed). Might panic.
	fn handle_timed_event(&mut self, now: PointInTime, _: &mut Poll) -> Result<(), Cow<'static, str>>
	{
		//If there is pending connect timeout, handle that.
		fail_if!(self.connect_timeout.triggered(now), "Failed to connect: Timed out");
		fail_if!(self.transfer_timeout.triggered(now), "Connection timed out");
		self.__return_hard_error_or_ok()
	}
	//Remove the pollers. Must not panic, including with poisoned locks.
	fn remove_pollers(&mut self, poll: &mut Poll)
	{
		self.sock_in.remove(poll);
		self.sock_out.remove(poll);
	}
	//Get next timed event. Called early and after calling handle_timed_event() or handle_event().
	fn get_next_timed_event(&self, _: PointInTime) -> Option<PointInTime>
	{
		//Timeout connect attempts.
		if self.connect_timeout.is_armed() { return self.connect_timeout.expires(); }
		//Timeout transfers.
		if self.transfer_timeout.is_armed() { return self.transfer_timeout.expires(); }
		None
	}
	fn get_main_fd(&self) -> i32 { self.sock_in.borrow().as_raw_fd() }
	//Handle a socket event (return Err(()) if connection should be killed). Might panic.
	fn handle_event(&mut self, poll: &mut Poll, tok: usize, kind: u8, id: ConnId,
		_: &mut AddTokenCallback) -> Result<(), Cow<'static, str>>
	{
		if tok == self.token_slave.value() && kind & IO_WAIT_INTR != 0 {
			self.handle_black_irq()?;
		}
		if tok == self.token_master.value() && kind & (IO_WAIT_READ|IO_WAIT_HUP) != 0 && !self.red_eof {
			self.handle_red_in()?;
		}
		if tok == self.token_slave.value() && kind & (IO_WAIT_READ|IO_WAIT_HUP) != 0 && !self.black_eof {
			self.handle_black_in()?;
		}
		if tok == self.token_master.value() && kind & IO_WAIT_WRITE != 0 || self.red_q.buffered() > 0 ||
			self.red_q.no_more_output() && self.red_end_ack {
			self.handle_red_out()?;
		}
		if (tok == self.token_slave.value() && kind & IO_WAIT_WRITE != 0 || self.black_q.buffered() > 0) &&
			self.delayed.is_none() {
			self.handle_black_out()?;
		}
		if tok == self.token_slave.value() && kind & IO_WAIT_WRITE != 0 && self.delayed.is_some() {
			self.handle_black_accept(id)?;
		}
		let tls_status = self.tls.get_status();
		let mut lost_tx_race = false;
		//If connect finished, swap the timeout.
		if !tls_status.is_handshaking() && self.connect_timeout.is_armed() {
			self.connect_timeout.disarm();
			self.transfer_timeout.rearm(self.transfer_timeout_duration);
		}
		//If transmit is empty, poll TLS errors
		if self.black_q.buffered() == 0 { if let Some(abort) = self.tls.get_error() {
			fail!(format!("TLS error: {abort}"));
		}}
		//Wait for TLS to be ready if blocking.
		if tls_status.is_blocked() {
			let r = self.tls.register_on_unblock(Box::new(SendIrq(self.irq.clone())));
			//If the race is lost, behave like is_want_tx is set.
			lost_tx_race |= !r;
		}

		//If red_eof is set and black buffer is empty, assert black_end_ack.
		if self.red_eof && self.black_q.buffered() == 0 { self.black_end_ack = true; }
		//If both red_end_ack and black_end_ack are asserted, then everything has been transmitted both
		//ways, close the link.
		fail_if!(self.red_end_ack && self.black_end_ack, "Normal close");
		//Assert incoming read if outgoing queue is empty and TLS can transmit. However, for the opposite
		//direction, use wants read instead of can receive.
		let e1 = self.red_eof;
		let e2 = false;
		let r1 = tls_status.is_may_tx() && self.black_q.buffered() == 0 && !self.red_eof;
		let r2 = tls_status.is_want_rx() && self.red_q.buffered() == 0 && !self.black_eof;
		//If TLS wants write, assert the interrupt, so it gets done.
		if tls_status.is_want_tx() || lost_tx_race { self.token_slave.as_irq().irq(); }
		//If queues have stuff, assert the corresponding write. Also register outgoing write if
		//delayconnect is still set.
		let w1 = self.red_q.buffered() > 0 || (self.red_q.eof_sent() && !self.red_end_ack);
		let w2 = self.black_q.buffered() > 0 || self.delayed.is_some();
		do_poll_again(r1, w1, e1, poll, &mut self.sock_in)?;
		do_poll_again(r2, w2, e2, poll, &mut self.sock_out)?;
		self.__return_hard_error_or_ok()
	}
}

fn do_poll_again(r: bool, w: bool, e: bool, poll: &mut Poll, sock: &mut FdPoller) -> Result<(), Cow<'static, str>>
{
	let mut s = IO_WAIT_ONESHOT;
	if r { s = s | IO_WAIT_READ; }
	if w { s = s | IO_WAIT_WRITE; }
	if !r && !w && !e { s = s | IO_WAIT_HUP; }
	sock.update(poll, s).map_err(|err|format!("Failed to update socket polling: {err}"))?;
	Ok(())
}
