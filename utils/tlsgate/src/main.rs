#![warn(unsafe_op_in_unsafe_fn)]
use btls::certificates::LocalKeyPair;
use btls::certificates::HostSpecificPin;
use btls_aux_serialization::Source;
use btls_aux_time::TimeUnit;
use btls_aux_unix::RawFdT;
use btls_daemon_helper_lib::abort;
use btls_daemon_helper_lib::CyclicArray;
use btls_daemon_helper_lib::ensure_stdfd_open;
use btls_daemon_helper_lib::FileDescriptor;
use btls_daemon_helper_lib::get_max_files;
use btls_daemon_helper_lib::incompatible_with_panic_abort;
use btls_daemon_helper_lib::initialize_from_systemd;
use btls_daemon_helper_lib::Listener;
use btls_daemon_helper_lib::ListenerLoop;
use btls_daemon_helper_lib::LocalThreadHandle;
use btls_daemon_helper_lib::running_as_root;
use btls_daemon_helper_lib::SighupPipe;
use btls_daemon_helper_lib::SocketAddrEx;
use btls_daemon_helper_lib::SocketAddrExHandle;
use btls_daemon_helper_lib::start_threads;
use btls_daemon_helper_lib::Thread;
use btls_daemon_helper_lib::ThreadNewParams;
use btls_daemon_helper_lib::ThreadSpawnedParams;
use btls_util_logging::set_no_syslog;
use btls_util_logging::syslog;
use btls_tlsgate::ConnectionFactory2;
use btls_tlsgate::GlobalConfiguration;
use btls_tlsgate::LoadedCertificate;
use std::env::args_os;
use std::fs::File;
use std::io::Read as IoRead;
use std::net::SocketAddr;
use std::net::ToSocketAddrs;
use std::str::FromStr;
use std::sync::Arc;
use std::time::Duration;

const MAX_FDS_REDUCE: usize = 100;

fn parse_duration(optname: &str, x: &str) -> Duration
{
	btls_daemon_helper_lib::parse_duration(x).unwrap_or_else(|err|abort!("{optname}: {err}"))
}

fn process_connect(list: &mut Vec<SocketAddrExHandle>, addr: &str)
{
	match addr.to_socket_addrs() {
		Ok(itr) => for addr in itr {
			list.push(SocketAddrExHandle::new(match addr {
				SocketAddr::V6(x) => SocketAddrEx::V6(x),
				SocketAddr::V4(x) => SocketAddrEx::V4(x),
			}));
		},
		Err(err) => syslog!(ERROR "Error looking up '{addr}': {err}")
	}
}

struct Config
{
	connect_timeout: TimeUnit,			//Connect timeout.
	transfer_timeout: TimeUnit,			//transfer timeout.
	address_list: Vec<SocketAddrExHandle>,
	sni: String,
	pins: Vec<HostSpecificPin>,
	certificate: Option<LoadedCertificate>,
	alpn: Option<String>,
}

impl Config
{
	fn from_cmdline() -> Config
	{
		let mut connect_timeout = TimeUnit::Seconds(10);
		let mut transfer_timeout = TimeUnit::Seconds(15);
		let mut dummy = true;
		let mut any_address = false;
		let mut address_list = Vec::new();
		let mut pins = Vec::new();
		let mut sni = None;
		let mut port = 443;
		let mut key: Option<LocalKeyPair> = None;
		let mut certificate = None;
		let mut alpn = None;
		for i in args_os() {
			if dummy { dummy = false; continue; }
			let xstr = i.clone().into_string().unwrap_or_else(|err|{
				abort!("Invalid argument: `{err}`", err=err.to_string_lossy())
			});
			//--listen-fd, --threads, --ack and --log-as are handled elsewhere.
			xstr.strip_prefix("--connect-timeout=").map(|x|{
				connect_timeout = TimeUnit::Duration(parse_duration("--connect-timeout=<sec>", x));
			});
			xstr.strip_prefix("--transfer-timeout=").map(|x|{
				transfer_timeout = TimeUnit::Duration(parse_duration("--transfer-timeout=<sec>", x));
			});
			xstr.strip_prefix("--connect=").map(|x|{
				process_connect(&mut address_list, x);
				any_address = true;
			});
			xstr.strip_prefix("--alpn=").map(|x|{
				alpn = Some(x.to_owned());
			});
			xstr.strip_prefix("--port=").map(|x|{
				port = u16::from_str(x).unwrap_or_else(|_|abort!("Invalid --port=<port>"));
			});
			xstr.strip_prefix("--key=").map(|keyfile|{
				let ikey = LocalKeyPair::new_file(keyfile).unwrap_or_else(|err|{
					abort!("Failed to load load private key {keyfile}: {err}")
				});
				key = Some(ikey);
			});
			xstr.strip_prefix("--certificate=").map(|certfname|{
				let mut certificates = Vec::new();
				let mut certfile = Vec::new();
				File::open(certfname).and_then(|mut fp|{
					fp.read_to_end(&mut certfile)
				}).unwrap_or_else(|err|{
					abort!("Can't read certificate file {certfname}: {err}")
				});
				let mut certfile = Source::new(&certfile);
				while certfile.more() {
					let ent = certfile.asn1_sequence_out().unwrap_or_else(|err|{
						abort!("Can't parse certificate {certfname}: {err}")
					});
					certificates.push(ent.to_owned());
				}
				if certificates.len() == 0 { abort!("No certificates in {certfname}"); }
				let cert = certificates.remove(0);
				certificate = Some((cert, certificates));
			});
			if !xstr.starts_with("-") {
				if sni.is_some() {
					let mut key = [0;32];
					let mut carry = 0;
					if xstr.len() != 64 { abort!("Bad thumbprint length for {xstr}"); }
					for (i, c) in xstr.as_bytes().iter().cloned().enumerate() {
						let v = match c as u32 {
							c@48..=57 => c - 48,
							c@65..=70 => c - 55,
							c@97..=102 => c - 87,
							_ => abort!("Bad thumbprint character for {xstr}")
						} as u8;
						if carry == 0 {
							carry = 1 + v;
						} else {
							key[i/2] = (carry - 1) * 16 + v;
							carry = 0;
						}
					}
					pins.push(HostSpecificPin::trust_server_key_by_sha256(&key, false, false));
				} else {
					sni = Some(xstr.to_owned());
				}
			}
		}
		let sni = sni.unwrap_or_else(||abort!("No SNI specified"));
		if pins.len() == 0 { abort!("No thumbprints specified"); }
		if !any_address { process_connect(&mut address_list, &format!("{sni}:{port}")); }
		let certificate = match (key, certificate) {
			(Some(k), Some((c1, c2))) => Some(LoadedCertificate::new(Arc::new(Box::new(k)), c1, c2)),
			(None, None) => None,
			(_, _) => abort!("--key and --certificate must both be present or absent")
		};
		Config {
			connect_timeout: connect_timeout,
			transfer_timeout: transfer_timeout,
			address_list: address_list,
			sni: sni,
			pins: pins,
			certificate: certificate,
			alpn: alpn,
		}
	}
}

#[derive(Clone)]
struct TlsWorkerParams
{
	global_config: GlobalConfiguration,
	maxfiles: usize,
	listener_fds: Vec<RawFdT>,
}

struct TlsWorker
{
	gstate: ListenerLoop
}

impl Thread for TlsWorker
{
	type Local = ();
	type LocalMetrics = ();
	type Parameters = TlsWorkerParams;
	type Parameters2 = ();
	fn new<'a>(p: ThreadNewParams<'a,Self>) -> Self
	{
		let ThreadNewParams{gparameters: gp, sigpipe: sp, tid, irqpipe, ..} = p;
		let gstate = setup_listener_loop(gp.maxfiles, &gp.listener_fds, gp.global_config, irqpipe, sp, tid);
		TlsWorker {gstate: gstate}
	}
	fn spawned(_: ThreadSpawnedParams<Self>) {}
	fn run(mut self) { self.gstate.main_event_loop(); }
}

fn main()
{
	ensure_stdfd_open();
	set_no_syslog();
	let sinfo = initialize_from_systemd("btls-tlsgate");
	incompatible_with_panic_abort();
	if running_as_root() { abort!("Can't run program as root"); }

	let listeners = sinfo.sockets();
	if listeners.len() == 0 { abort!("No ports to listen"); }

	let maxfiles = get_max_files() - MAX_FDS_REDUCE;
	syslog!(NOTICE "Using maximum of {maxfiles} file descriptors");

	let config = Config::from_cmdline();

	let global_config = GlobalConfiguration {
		connect_timeout: config.connect_timeout,
		transfer_timeout: config.transfer_timeout,
		address_list: Arc::new(CyclicArray::new(config.address_list)),
		sni: Arc::new(config.sni),
		pins: Arc::new(config.pins),
		certificate: config.certificate,
		alpn: config.alpn.map(|x|Arc::new(x)),
	};

	start_threads::<TlsWorker>(sinfo, "TLS worker", TlsWorkerParams {
		global_config: global_config,
		maxfiles: maxfiles,
		listener_fds: listeners,
	}, ());
}

fn setup_listener_loop(maxfiles: usize, listener_fds: &Vec<RawFdT>, global_config: GlobalConfiguration,
	irqpipe: LocalThreadHandle, sigpipe: SighupPipe, tid: Option<u64>) -> ListenerLoop
{
	//Construct connection factory.
	let factory = ConnectionFactory2::new2(global_config);
	//Construct listener loop. Each thread should have its own.
	let mut gstate = ListenerLoop::new(maxfiles, sigpipe, irqpipe).unwrap_or_else(|err|{
		abort!("Creating event loop: {err}")
	});
	if let Some(tid) = tid { gstate.set_pid(tid); }
	//Add the file descriptors.
	for i in listener_fds.iter() {
		let j = Listener::new(FileDescriptor::new(*i), factory.clone());
		gstate.add_listener(j);
	}
	gstate
}
