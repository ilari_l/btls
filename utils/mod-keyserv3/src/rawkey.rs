use ::os_syslog;
use ::scope_to_id;
use btls_aux_fail::f_return;
use btls_aux_fail::fail_if_none;
use btls_aux_futures::FutureReceiver;
use btls_aux_hash::sha256;
use btls_aux_keypair_local::LocalKeyPair;
use btls_aux_memory::Hexdump;
use btls_aux_memory::split_at;
use btls_aux_unix::Gid;
use btls_aux_unix::LogLevel;
use btls_aux_unix::Uid;
use btls_aux_random::TemporaryRandomLifetimeTag;
use btls_aux_signatures::Hasher;
use btls_aux_signatures::KeyPair2;
use btls_aux_signatures::SignatureAlgorithm2;
use btls_aux_signatures::SignatureAlgorithmTls2;
use btls_aux_signatures::SignatureFormat;
use btls_aux_signatures::TemporaryRandomStream;
use std::collections::BTreeMap;
use std::collections::BTreeSet;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::fs::File;
use std::io::Read;
use std::ops::Deref;
use std::path::Path;
use std::sync::Arc;


#[derive(Copy,Clone,Debug)]
pub enum SignatureClass
{
	TlsServer,
	TlsClient,
	Any
}

impl SignatureClass
{
	fn looks_like_tls12_server(data: &[u8]) -> bool
	{
		//The type data has to be available.
		if data.len() < 69 { return false; }
		//Recognize various curves.
		match (data[64], data[65], data[66], data[67], data[68], data.len()) {
			(3, 0, 23,  65, 4, 133) => true,	//NIST P-256.
			(3, 0, 24,  97, 4, 165) => true,	//NIST P-384
			(3, 0, 25, 133, 4, 201) => true,	//NIST P-512.
			(3, 0, 26,  65, 4, 133) => true,	//Brainpool 256.
			(3, 0, 27,  97, 4, 165) => true,	//Brainpool 384.
			(3, 0, 28, 129, 4, 197) => true,	//Brainpool 512.
			(3, 0, 29,  32, _, 100) => true,	//X25519
			(3, 0, 30,  56, _, 124) => true,	//X448.
			(_, _,  _,   _, _,   _) => false	//What is this?
		}
	}
	fn looks_like_tls13_server(data: &[u8]) -> bool
	{
		//The first 98 bytes are actually fixed.
		static SERVER_SIGNATURE_PREFIX: [u8; 98] = [
			 32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,   32,  32,
			 32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,   32,  32,
			 32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,   32,  32,
			 32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,   32,  32,
			 84,  76,  83,  32,  49,  46,  51,  44,  32, 115, 101, 114, 118, 101,  114,  32,
			 67, 101, 114, 116, 105, 102, 105,  99,  97, 116, 101,  86, 101, 114,  105, 102,
			121,   0
		];
		let (pfx, hash) = f_return!(split_at(data, SERVER_SIGNATURE_PREFIX.len()), false);
		if hash.len() < 32 { return false; }		//Hash is at least 32 bytees.
		pfx == &SERVER_SIGNATURE_PREFIX
	}
	fn looks_like_tls13_client(data: &[u8]) -> bool
	{
		//The first 98 bytes are actually fixed.
		static CLIENT_SIGNATURE_PREFIX: [u8; 98] = [
			 32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,   32,  32,
			 32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,   32,  32,
			 32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,   32,  32,
			 32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,  32,   32,  32,
			 84,  76,  83,  32,  49,  46,  51,  44,  32,  99, 108, 105, 101, 110,  116,  32,
			 67, 101, 114, 116, 105, 102, 105,  99,  97, 116, 101,  86, 101, 114,  105, 102,
			121,   0
		];
		let (pfx, hash) = f_return!(split_at(data, CLIENT_SIGNATURE_PREFIX.len()), false);
		if hash.len() < 32 { return false; }		//Hash is at least 32 bytees.
		pfx == CLIENT_SIGNATURE_PREFIX
	}
	fn looks_like_quic_security_issue(data: &[u8]) -> bool
	{
		//QUIC is a security problem.
		(data.len() >= 28 && &data[..28] == &b"QUIC server config signature"[..])
			|| (data.len() >= 37 && &data[..37] == &b"QUIC CHLO and server config signature"[..])
	}
	pub fn get_class(data: &[u8]) -> SignatureClass
	{
		if Self::looks_like_quic_security_issue(data) { return SignatureClass::Any; }
		if Self::looks_like_tls12_server(data) { return SignatureClass::TlsServer; }
		if Self::looks_like_tls13_server(data) { return SignatureClass::TlsServer; }
		if Self::looks_like_tls13_client(data) { return SignatureClass::TlsClient; }
		//Default to Any.
		SignatureClass::Any
	}
}

#[derive(Clone,Debug,PartialEq,Eq,PartialOrd,Ord)]
pub enum KeyAclSubject
{
	User(Uid),
	Group(Gid),
	LLIF(u32),
	X25519Key([u8;32]),
	None
}

impl Display for KeyAclSubject
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&KeyAclSubject::User(uid) => write!(fmt, "UID {uid}"),
			&KeyAclSubject::Group(gid) => write!(fmt, "GID {gid}"),
			&KeyAclSubject::LLIF(iid) => write!(fmt, "LLIF {iid}"),
			&KeyAclSubject::X25519Key(key) => {
				write!(fmt, "X25519 key {key}", key=Hexdump(&key))?;
				Ok(())
			},
			&KeyAclSubject::None => fmt.write_str("NONE")
		}
	}
}

impl KeyAclSubject
{
	pub fn new(word: &str, groupdb: Option<&BTreeMap<String, Vec<KeyAclSubject>>>) ->
		Option<OneOrMany<KeyAclSubject>>
	{
		if let Some(word) = word.strip_prefix("group:") {
			let members = groupdb.and_then(|x|x.get(word).map(|y|y.clone()))?;
			Some(OneOrMany::Many(members))
		} else if let Some(word) = word.strip_prefix("uid:") {
			let id = Uid::from_str(word)?;
			Some(OneOrMany::One(KeyAclSubject::User(id)))
		} else if let Some(word) = word.strip_prefix("gid:") {
			let id = Gid::from_str(word)?;
			Some(OneOrMany::One(KeyAclSubject::Group(id)))
		} else if let Some(word) = word.strip_prefix("llif:") {
			let id = scope_to_id(word).ok()?;
			Some(OneOrMany::One(KeyAclSubject::LLIF(id)))
		} else if let Some(word) = word.strip_prefix("x25519:") {
			fail_if_none!(word.len() != 64);
			let mut out = [0;32];
			for (i, c) in word.as_bytes().iter().enumerate() {
				let h = match *c {
					48..=57 => c - 48,
					65..=70 => c - 55,
					97..=102 => c - 87,
					_ => return None
				};
				out[i/2] |= h << 4 - i % 2 * 4;
			}
			Some(OneOrMany::One(KeyAclSubject::X25519Key(out)))
		} else {
			None
		}
	}
}

#[derive(Clone,Debug)]
pub struct KeyAclGrant
{
	sign_tls_server: bool,
	sign_tls_client: bool,
	sign_any: bool,
	dump: bool,
}

impl KeyAclGrant
{
	pub fn dummy_grant() -> KeyAclGrant
	{
		KeyAclGrant {
			sign_tls_server: false,
			sign_tls_client: false,
			sign_any: false,
			dump: false,
		}
	}
	pub fn has_any_grant(&self) -> bool
	{
		self.sign_tls_server || self.sign_tls_client || self.sign_any || self.dump
	}
	pub fn can_sign(&self, data: &[u8]) -> bool { self.has_grant_for(SignatureClass::get_class(data)) }
	fn has_grant_for(&self, class: SignatureClass) -> bool
	{
		self.sign_any || match class {
			SignatureClass::TlsServer => self.sign_tls_server,
			SignatureClass::TlsClient => self.sign_tls_client,
			SignatureClass::Any => false,
		}
	}
	fn new_dump() -> KeyAclGrant
	{
		KeyAclGrant {
			sign_tls_server: false,
			sign_tls_client: false,
			sign_any: false,
			dump: true,
		}
	}
	fn new_any() -> KeyAclGrant
	{
		let mut g = Self::new_dump();
		g.sign_any = true;
		g
	}
	fn new_client() -> KeyAclGrant
	{
		let mut g = Self::new_dump();
		g.sign_tls_client = true;
		g
	}
	fn new_server() -> KeyAclGrant
	{
		let mut g = Self::new_dump();
		g.sign_tls_server = true;
		g
	}
}

struct Key
{
	///The actual key.
	key: Box<dyn KeyPair2+Send+Sync>,
	///Name of the key.
	name: String,
	///Grants.
	grants: BTreeMap<KeyAclSubject, KeyAclGrant>,
	///Tags.
	tags: BTreeSet<String>,
	///The key id.
	kid: [u8;32],
}

macro_rules! bad_grant
{
	($fname:expr, $line:expr) => {
		os_syslog(LogLevel::Warning, &format!("Bad grant line in {file}: {line}", file=$fname, line=$line));
	}
}

macro_rules! unwrap_or_bad
{
	($fname:expr, $line:expr, $word:expr) => {
		match $word {
			Some(x) => x,
			None => {
				bad_grant!($fname, $line);
				continue;
			}
		}
	}
}

macro_rules! basic_grant_line
{
	($fname:expr, $line:expr, $token:expr, $groupdb:expr) => {{
		let gs = unwrap_or_bad!(&$fname, $line, $token);
		//TODO: Pass the database.
		let subj = unwrap_or_bad!(&$fname, $line, KeyAclSubject::new(gs, $groupdb));
		subj
	}}
}

pub enum OneOrMany<T:Sized>
{
	One(T),
	Many(Vec<T>),
}

fn insert_multiple_grants(map: &mut BTreeMap<KeyAclSubject, KeyAclGrant>, keys: OneOrMany<KeyAclSubject>,
	grant: KeyAclGrant)
{
	match keys {
		OneOrMany::One(key) => {map.insert(key, grant);},
		OneOrMany::Many(mut keys) => for key in keys.drain(..) { map.insert(key, grant.clone()); },
	}
}

impl Key
{
	fn new(name: &Path, groupdb: Option<&BTreeMap<String, Vec<KeyAclSubject>>>) -> Result<Key, String>
	{
		let kp = Box::new(Privatekey::new(name)?);
		//Extract part after last /.
		let (kname, grantfile) = if name.extension().and_then(|e|e.to_str()) == Some("priv") {
			let kname = name.file_stem().and_then(|f|f.to_str()).unwrap_or("").to_owned();
			let mut grantfile = name.to_path_buf();
			grantfile.set_extension("acl");
			let grantfile = grantfile.display().to_string();
			(kname, grantfile)
		} else {
			os_syslog(LogLevel::Warning, &format!(
				"Key files ({name}) without '.priv' extension are deprecated!", name=name.display()));
			let kname = name.display().to_string();
			let kname = kname.rsplit("/").next().unwrap_or(&kname).to_owned();
			let grantfile = format!("{name}.acl", name=name.display());
			(kname, grantfile)
		};
		let mut grants = BTreeMap::new();
		let mut grantdata = String::new();
		let mut tags = BTreeSet::new();
		File::open(&grantfile).and_then(|mut fp|fp.read_to_string(&mut grantdata)).
			map_err(|err|format!("Failed to read {grantfile}: {err}"))?;
		for line in grantdata.lines() {
			let line = line.trim();
			//Skip empty and comment lines.
			if line.len() == 0 || line.as_bytes()[0] == b'#' { continue; }
			let mut words = line.split_whitespace();
			let gtype = unwrap_or_bad!(&grantfile, line, words.next());
			if gtype == "any" {
				let to = basic_grant_line!(grantfile, line, words.next(), groupdb);
				insert_multiple_grants(&mut grants, to, KeyAclGrant::new_any());
			} else if gtype == "tls-server" {
				let to = basic_grant_line!(grantfile, line, words.next(), groupdb);
				insert_multiple_grants(&mut grants, to, KeyAclGrant::new_server());
			} else if gtype == "tls-client" {
				let to = basic_grant_line!(grantfile, line, words.next(), groupdb);
				insert_multiple_grants(&mut grants, to, KeyAclGrant::new_client());
			} else if gtype == "dump" {
				let to = basic_grant_line!(grantfile, line, words.next(), groupdb);
				insert_multiple_grants(&mut grants, to, KeyAclGrant::new_dump());
			} else if gtype == "tag" {
				let gs = unwrap_or_bad!(&grantfile, line, words.next());
				tags.insert(gs.to_owned());
			} else {
				bad_grant!(&grantfile, line);
			}
		}
		Ok(Key {
			kid: sha256(kp.get_public_key2()),
			key: kp,
			name: kname.to_owned(),
			tags: tags,
			grants: grants,
		})
	}
	fn grant_for(&self, subject: &[KeyAclSubject]) -> KeyAclGrant
	{
		let mut grant = KeyAclGrant::dummy_grant();
		for subj in subject.iter() {
			//None never has any grants.
			if *subj == KeyAclSubject::None { continue; }
			let pgrant = self.grants.get(subj).map(|x|x.clone()).
				unwrap_or(KeyAclGrant::dummy_grant());
			grant.sign_any |= pgrant.sign_any;
			grant.sign_tls_server |= pgrant.sign_tls_server;
			grant.dump |= pgrant.dump;
		}
		grant
	}
	fn sign(&self, subject: &[KeyAclSubject], scheme: u16, data: &[u8]) ->
		Result<FutureReceiver<Result<(SignatureFormat, Vec<u8>), ()>>, String>
	{
		let grant = self.grant_for(subject.clone());
		if !grant.can_sign(data) {
			let subj = match subject.get(0) {
				Some(x) => x.clone(),
				None => KeyAclSubject::None
			};
			fail!(format!("Permission to sign data {data:?} denied to subject {subj}"));
		}
		let scheme = f_return!(SignatureAlgorithmTls2::by_tls_id(scheme), Ok(FutureReceiver::from(Err(()))));
		let tag = TemporaryRandomLifetimeTag;
		let mut rng = TemporaryRandomStream::new_system(&tag);
		Ok(self.key.sign_data(data, scheme.to_generic(), &mut rng))
	}
	fn has_any_grant(&self, subject: &[KeyAclSubject]) -> bool
	{
		self.grant_for(subject).has_any_grant()
	}
	fn dump_public_key(&self) -> (Vec<u8>, Vec<u16>)
	{
		let schemes = self.key.get_signature_algorithms().iter().
			filter_map(|alg|alg.downcast_tls().map(|x|x.tls_id())).collect::<Vec<_>>();
		let pubkey = self.key.get_public_key2().to_owned();
		(pubkey, schemes)
	}
	fn get_key_name(&self) -> String { self.name.clone() }
	pub fn get_kid(&self) -> [u8;32] { self.kid }
	pub fn has_tag(&self, tag: &str) -> bool { self.tags.contains(tag) }
}

#[derive(Clone)]
pub struct KeyHandle(Arc<Key>);

impl KeyHandle
{
	fn new(name: &Path, groupdb: Option<&BTreeMap<String, Vec<KeyAclSubject>>>) -> Result<KeyHandle, String>
	{
		Ok(KeyHandle(Arc::new(Key::new(name, groupdb)?)))
	}
	pub fn sign(&self, subject: &[KeyAclSubject], scheme: u16, data: &[u8]) ->
		Result<FutureReceiver<Result<(SignatureFormat, Vec<u8>), ()>>, String>
	{
		self.0.sign(subject, scheme, data)
	}
	pub fn has_any_grant(&self, subject: &[KeyAclSubject]) -> bool { self.0.has_any_grant(subject) }
	pub fn dump_public_key(&self) -> (Vec<u8>, Vec<u16>) { self.0.dump_public_key() }
	pub fn get_key_name(&self) -> String { self.0.get_key_name() }
	pub fn get_kid(&self) -> [u8;32] { self.0.get_kid() }
	pub fn has_tag(&self, tag: &str) -> bool { self.0.has_tag(tag) }
}

pub struct GlobalKeyStorage
{
	keys: Vec<Arc<KeyHandle>>,
	admins: BTreeSet<KeyAclSubject>,
	groups: BTreeMap<String, Vec<KeyAclSubject>>,
	allow_delete: bool,
}

impl GlobalKeyStorage
{
	pub fn new(admins: &[KeyAclSubject], groupdb: BTreeMap<String, Vec<KeyAclSubject>>) -> GlobalKeyStorage
	{
		let mut admins2 = BTreeSet::new();
		for i in admins { admins2.insert(i.clone()); }
		GlobalKeyStorage {
			keys: Vec::new(),
			admins: admins2,
			groups: groupdb,
			allow_delete: true,
		}
	}
	pub fn add_group_member(&mut self, name: &str, member: KeyAclSubject)
	{
		self.groups.entry(name.to_owned()).or_insert_with(||Vec::new()).push(member);
	}
	pub fn add(&mut self, name: &Path) -> Result<(), String>
	{
		self.keys.push(Arc::new(KeyHandle::new(name, Some(&self.groups))?));
		Ok(())
	}
	pub fn delete_by_name(&mut self, name: &str)
	{
		let mut i = 0;
		while i < self.keys.len() {
			if self.keys[i].deref().0.name == name { self.keys.remove(i); } else { i += 1; }
		}
	}
	pub fn get_by_index(&self, index: usize) -> Option<Arc<KeyHandle>>
	{
		self.keys.get(index).map(|x|x.clone())
	}
	pub fn get_by_name(&self, name: &[u8]) -> Option<Arc<KeyHandle>>
	{
		self.keys.iter().cloned().find(|k|k.deref().0.name.as_bytes() == name)
	}
	pub fn restrict_for_subject(&self, subject: &[KeyAclSubject]) -> LocalKeyStorage
	{
		//Create first local key storage with no keys and then update its rights.
		let mut lks = LocalKeyStorage::new(subject);
		lks.update_rights(self);
		lks
	}
	pub fn is_admin(&self, subject: &[KeyAclSubject]) -> bool
	{
		for subj in subject.iter() {
			if self.admins.contains(subj) { return true; }
		}
		false
	}
	pub fn allow_delete(&self) -> bool { self.allow_delete }
	pub fn disable_delete(&mut self) { self.allow_delete = false; }
}

pub enum ByIndexResponse
{
	//Bad index (out of range).
	IndexBad,
	//Index revoked.
	IndexRevoked,
	//Response key.
	Key(Arc<KeyHandle>),
}

pub struct LocalKeyStorage
{
	//The number of indices allocated.
	allocated: usize,
	//Map of indices to keys (can have holes).
	by_index: BTreeMap<usize, Arc<KeyHandle>>,
	//Map of key hashes to keys and indices.
	by_kid: BTreeMap<[u8;32], (Arc<KeyHandle>, usize)>,
	//Map of key hashes to indices (including keys that have once existed).
	kid_to_index: BTreeMap<[u8;32], usize>,
	//The authenticated identities.
	authenticated_as: Vec<KeyAclSubject>
}


impl LocalKeyStorage
{
	pub fn new(subject: &[KeyAclSubject]) -> LocalKeyStorage
	{
		LocalKeyStorage {
			allocated: 0,
			by_index: BTreeMap::new(),
			by_kid: BTreeMap::new(),
			kid_to_index: BTreeMap::new(),
			authenticated_as: subject.to_owned()
		}
	}
	pub fn get_by_name(&self, name: &[u8]) -> Option<(Arc<KeyHandle>, usize)>
	{
		self.by_index.iter().find(|(_,key)|key.0.name.as_bytes() == name).
			map(|(index,key)|(key.clone(), *index))
	}
	pub fn get_by_kid(&self, kid: &[u8;32]) -> Option<(Arc<KeyHandle>, usize)>
	{
		self.by_kid.get(kid).map(|&(ref x,y)|(x.clone(), y))
	}
	pub fn get_by_index(&self, index: usize) -> ByIndexResponse
	{
		if let Some(key) = self.by_index.get(&index) {
			ByIndexResponse::Key(key.clone())
		} else if index < self.allocated {
			//This has been allocated but is not authorized. Assume it has been revoked.
			ByIndexResponse::IndexRevoked
		} else {
			//Never has been allocated.
			ByIndexResponse::IndexBad
		}
	}
	//Return the allocated size, not real size, as one wants to also loop over revoked keys.
	pub fn len(&self) -> usize { self.allocated }
	pub fn update_rights(&mut self, keys: &GlobalKeyStorage)
	{
		//Clear the by_index and by_kid maps in order to deauthorize all keys. Then reauthorize all the
		//authorized keys. kid_to_index is not touched, as if key gets removed and then readded, it needs
		//to retain its index.
		self.by_index.clear();
		self.by_kid.clear();
		//Reauthorize all keys that should reauthorized.
		for i in keys.keys.iter() { if i.has_any_grant(&self.authenticated_as) {
			let kid = i.get_kid();
			//Allocate index. If kid is in kid_to_index, allocated that index. Otherwise allocate
			//next available index, and add to kid_to_index.
			let index = self.kid_to_index.get(&kid).map(|i|*i).unwrap_or_else(||{
				let nindex = self.allocated;
				self.allocated += 1;	//nindex got allocated.
				self.kid_to_index.insert(kid, nindex);
				nindex
			});
			self.by_kid.insert(kid, (i.clone(), index));
			self.by_index.insert(index, i.clone());
		}}
	}
}


pub struct Privatekey(LocalKeyPair);

impl Privatekey
{
	pub fn new<P1: AsRef<Path>>(key: P1) -> Result<Privatekey, String>
	{
		let key = key.as_ref();
		let call_as = key.display().to_string();
		let mut _privkey2 = Vec::new();
		File::open(key).and_then(|mut fp|fp.read_to_end(&mut _privkey2)).
			map_err(|err|format!("Failed to read private key {call_as}: {err}"))?;
		//Try loading as local key pair.
		LocalKeyPair::new(&mut &_privkey2[..], &call_as).map(|x|Privatekey(x)).
			map_err(|err|format!("Private key '{call_as}' is in unknown format: {err}"))
	}
}

impl KeyPair2 for Privatekey
{
	fn sign_data(&self, data: &[u8], algorithm: SignatureAlgorithm2, rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<(SignatureFormat, Vec<u8>), ()>>
	{
		self.0.sign_data(data, algorithm, rng)
	}
	fn sign_callback(&self, algorithm: SignatureAlgorithm2, rng: &mut TemporaryRandomStream,
		msg: &mut dyn FnMut(&mut Hasher)) -> FutureReceiver<Result<(SignatureFormat, Vec<u8>), ()>>
	{
		self.0.sign_callback(algorithm, rng, msg)
	}
	fn get_signature_algorithms<'a>(&'a self) -> &'a [SignatureAlgorithm2] { self.0.get_signature_algorithms() }
	fn get_public_key2<'a>(&'a self) -> &'a [u8] { self.0.get_public_key2() }
	fn get_key_type2<'a>(&'a self) -> &'a str { self.0.get_key_type2() }
}
