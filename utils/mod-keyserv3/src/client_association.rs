use ::association::calculate_session_key;
use ::association::MAC_EXPANSION;
use ::association::MacGenerator;
use ::association::Query;
use ::unix::DatagramSocket;
use ::unix::PacketAddress;
use btls_aux_xed25519::x25519_agree;
use std::cmp::Ordering;
use std::collections::BTreeMap;
use std::ops::Deref;
use std::sync::Arc;


#[derive(Clone,PartialEq,Eq,PartialOrd,Ord)]
pub struct AuthKey
{
	pub private: [u8;32],
	pub public: [u8;32],
}

#[derive(Clone)]
pub struct AssociationKey(Arc<PacketAddress>, Arc<AuthKey>);

impl Ord for AssociationKey
{
	fn cmp(&self, other: &AssociationKey) -> Ordering
	{
		let c1 = self.0.deref().cmp(other.0.deref());
		let c2 = self.1.deref().cmp(other.1.deref());
		if c1.is_ne() { return c1; }
		if c2.is_ne() { return c2; }
		Ordering::Equal
	}
}

impl PartialOrd for AssociationKey
{
	fn partial_cmp(&self, other: &AssociationKey) -> Option<Ordering> { Some(self.cmp(other)) }
}

impl PartialEq for AssociationKey
{
	fn eq(&self, other: &AssociationKey) -> bool { self.cmp(other) == Ordering::Equal }
}

impl Eq for AssociationKey {}

#[derive(Clone)]
pub struct AssociationKey2(Arc<PacketAddress>, u64);

impl Ord for AssociationKey2
{
	fn cmp(&self, other: &AssociationKey2) -> Ordering
	{
		let c1 = self.0.deref().cmp(other.0.deref());
		let c2 = self.1.cmp(&other.1);
		if c1.is_ne() { return c1; }
		if c2.is_ne() { return c2; }
		Ordering::Equal
	}
}

impl PartialOrd for AssociationKey2
{
	fn partial_cmp(&self, other: &AssociationKey2) -> Option<Ordering> { Some(self.cmp(other)) }
}

impl PartialEq for AssociationKey2
{
	fn eq(&self, other: &AssociationKey2) -> bool { self.cmp(other) == Ordering::Equal }
}

impl Eq for AssociationKey2 {}

pub struct AssociationMap(BTreeMap<AssociationKey, MacGenerator>, BTreeMap<AssociationKey2, Arc<AuthKey>>,
	usize);

impl AssociationMap
{
	pub fn new() -> AssociationMap
	{
		AssociationMap(BTreeMap::new(), BTreeMap::new(), 0)
	}
	//Process received AuthM0 message.
	pub fn process_auth_m0(&mut self, addr: Arc<PacketAddress>, auth_a: Arc<AuthKey>, auth_b: Arc<AuthKey>,
		id: u64, auth_s: [u8;32])
	{
		let mut ax = [0; 32];
		let mut bx = [0; 32];
		x25519_agree(&mut ax, &auth_a.private, &auth_s);
		x25519_agree(&mut bx, &auth_b.private, &auth_s);
		let z = calculate_session_key(&ax, &bx, &auth_a.public, &auth_b.public, &auth_s);
		let macgen = MacGenerator::new(id, &z);

		let key = AssociationKey(addr.clone(), auth_b.clone());
		let key2 = AssociationKey2(addr.clone(), id);
		self.0.insert(key, macgen);
		self.1.insert(key2, auth_b);
	}
	//Process received unknown association message.
	pub fn process_unknown_association(&mut self, addr: Arc<PacketAddress>, id: u64)
	{
		let key2 = AssociationKey2(addr.clone(), id);
		let v = self.1.remove(&key2);
		if let Some(v) = v {
			let key = AssociationKey(addr, v);
			self.0.remove(&key);
		}
	}
	//Send a message with given payload to given association. Returns the sequence number if sent, None if the
	//association was unknown (and needs to be set up).
	pub fn send_message<L>(&mut self, fd: &DatagramSocket, addr: Arc<PacketAddress>, auth_b: Arc<AuthKey>,
		msg: Query, mut log: L) -> Option<(u64, usize)> where L: FnMut(&str)
	{
		let (cmd, data) = msg.serialize();
		let mut data2 = vec![0;data.len()+MAC_EXPANSION];
		let key = AssociationKey(addr.clone(), auth_b);
		self.0.get_mut(&key).and_then(|x|x.generate(&mut data2, cmd, &data)).map(|(payload, aid, seq)|{
			//Send the message and return its sequence number.
			if let Err(err) = fd.send_raw(addr.deref(), payload) { log(&err); }
			(aid, seq)
		})
	}
	//Send association setup message. This should be done if send_message fails. Returns txid.
	pub fn send_setup<L>(&mut self, fd: &DatagramSocket, addr: Arc<PacketAddress>, auth_a: Arc<AuthKey>,
		auth_b: Arc<AuthKey>, mut log: L) -> usize where L: FnMut(&str)
	{
		let id = self.2;
		let (cmd, mut payload) = Query::AuthM0(id, auth_a.public, auth_b.public).serialize();
		payload.insert(0, cmd);		//Prepend command.
		self.2 += 1;
		if let Err(err) = fd.send_raw_auth(addr.deref(), &payload) { log(&err); }
		return id;
	}
}
