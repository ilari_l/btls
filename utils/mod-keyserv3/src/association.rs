use btls_aux_fail::fail_if_none;
use btls_aux_hash::Hash;
use btls_aux_hash::Sha256;
use btls_aux_memory::EscapeByteString;
use std::borrow::Cow;
use std::mem::size_of;
use std::ops::Deref;
use std::str::from_utf8;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering;


fn extend_u64(o: &mut Vec<u8>, x: u64) { for i in 0..8 {  o.push((x >> 8 * i) as u8); } }
fn extend_u32(o: &mut Vec<u8>, x: usize) { for i in 0..4 {  o.push((x >> 8 * i) as u8); } }
fn extend_u16(o: &mut Vec<u8>, x: u16) { for i in 0..2 {  o.push((x >> 8 * i) as u8); } }

macro_rules! parse_i
{
	(2 $arr:expr, $off:expr) => {{
		let mut x = 0;
		for i in 0..2 {  x |= ($arr[$off+i] as u16) << 8 * i; }
		x
	}};
	(4 $arr:expr, $off:expr) => {{
		let mut x = 0;
		for i in 0..4 {  x |= ($arr[$off+i] as usize) << 8 * i; }
		x
	}};
	(8 $arr:expr, $off:expr) => {{
		let mut x = 0;
		for i in 0..8 {  x |= ($arr[$off+i] as u64) << 8 * i; }
		x
	}};
}


pub enum QueryParseError
{
	Truncated(u8),
	UnknownCommand(u8),
	UnknownKeyAgreement(u8),
}

pub enum Query<'a>
{
	AuthM0(usize, [u8;32], [u8;32]),
	DumpKey(usize),
	DumpKeyKid([u8;32]),
	DumpKeyName(Cow<'a, [u8]>),
	HasTag(usize, Cow<'a, [u8]>),
	Sign([u8;32], u16, Cow<'a, [u8]>),
	ACreateKey(Cow<'a, [u8]>, u32, u32),
	ADeleteKey(Cow<'a, [u8]>),
	ADumpKey(Cow<'a, [u8]>),
	AKeyName(u32),
}

const CMD_AUTH_M0: u8 = 0;
const CMD_DUMP_KEY: u8 = 1;
const CMD_SIGN: u8 = 2;
const CMD_HAS_TAG: u8 = 3;
const CMD_ADMIN_CREATE_KEY: u8 = 4;
const CMD_ADMIN_DELETE_KEY: u8 = 5;
const CMD_ADMIN_DUMP_KEY: u8 = 6;
const CMD_ADMIN_KEY_NAME: u8 = 7;
const CMD_DUMP_KEY_KID: u8 = 8;
const CMD_DUMP_KEY_NAME: u8 = 9;


impl<'a> Query<'a>
{
	pub fn serialize(&self) -> (u8, Vec<u8>)
	{
		match self {
			&Query::AuthM0(txid, a, b) => {
				let mut out = Vec::with_capacity(69);
				const AUTHM0_OP: u8 = 0; out.push(AUTHM0_OP);
				extend_u32(&mut out, txid);
				out.extend_from_slice(&a);
				out.extend_from_slice(&b);
				(CMD_AUTH_M0, out)
			},
			&Query::DumpKey(kid) => {
				let mut out = Vec::with_capacity(4);
				extend_u32(&mut out, kid);
				(CMD_DUMP_KEY, out)
			},
			&Query::DumpKeyKid(kid) => {
				let mut out = Vec::with_capacity(32);
				out.extend_from_slice(&kid);
				(CMD_DUMP_KEY_KID, out)
			},
			&Query::DumpKeyName(ref name) => {
				let mut out = Vec::with_capacity(name.len());
				out.extend_from_slice(name);
				(CMD_DUMP_KEY_NAME, out)
			},
			&Query::HasTag(kid, ref data) => {
				let mut out = Vec::with_capacity(4+data.len());
				extend_u32(&mut out, kid);
				out.extend_from_slice(data.deref());
				(CMD_HAS_TAG, out)
			},
			&Query::Sign(kid, meth, ref data) => {
				let mut out = Vec::with_capacity(34+data.len());
				out.extend_from_slice(&kid);
				extend_u16(&mut out, meth);
				out.extend_from_slice(data.deref());
				(CMD_SIGN, out)
			},
			&Query::ACreateKey(ref name, keytype, profile) => {
				let mut out = Vec::with_capacity(8+name.len());
				extend_u32(&mut out, keytype as usize);
				extend_u32(&mut out, profile as usize);
				out.extend_from_slice(name.deref());
				(CMD_ADMIN_CREATE_KEY, out)
			},
			&Query::ADeleteKey(ref name) => {
				let mut out = Vec::with_capacity(name.len());
				out.extend_from_slice(name.deref());
				(CMD_ADMIN_DELETE_KEY, out)
			},
			&Query::ADumpKey(ref name) => {
				let mut out = Vec::with_capacity(name.len());
				out.extend_from_slice(name.deref());
				(CMD_ADMIN_DUMP_KEY, out)
			},
			&Query::AKeyName(index) => {
				let mut out = Vec::with_capacity(4);
				extend_u32(&mut out, index as usize);
				(CMD_ADMIN_KEY_NAME, out)
			},
		}
	}
	pub fn parse(cmd: u8, payload: &'a [u8]) -> Result<Query<'a>, QueryParseError>
	{
		Ok(if cmd == CMD_AUTH_M0 {
			//Authenticate.
			if payload.len() < 1 { return Err(QueryParseError::Truncated(cmd)); }
			if payload[0] != 0 { return Err(QueryParseError::UnknownKeyAgreement(payload[0])); }
			if payload.len() < 69 { return Err(QueryParseError::Truncated(cmd)); }
			let _a = &payload[5..37];
			let _b = &payload[37..69];
			let mut a = [0; 32];
			let mut b = [0; 32];
			a.copy_from_slice(_a);
			b.copy_from_slice(_b);
			Query::AuthM0(parse_i!(4 payload, 1), a, b)
		} else if cmd == CMD_DUMP_KEY {
			if payload.len() < 4 { return Err(QueryParseError::Truncated(cmd)); }
			let mut kid = 0;
			for i in 0..4 {  kid |= (payload[i] as usize) << 8 * i; }
			Query::DumpKey(kid)
		} else if cmd == CMD_DUMP_KEY_KID {
			if payload.len() < 32 { return Err(QueryParseError::Truncated(cmd)); }
			let mut kid = [0;32];
			kid.copy_from_slice(&payload[..32]);
			Query::DumpKeyKid(kid)
		} else if cmd == CMD_DUMP_KEY_NAME {
			Query::DumpKeyName(Cow::Borrowed(&payload[..]))
		} else if cmd == CMD_SIGN {
			if payload.len() < 34 { return Err(QueryParseError::Truncated(cmd)); }
			let mut kid = [0;32];
			kid.copy_from_slice(&payload[..32]);
			let mut meth = 0;
			for i in 0..2 {  meth |= (payload[i+32] as u16) << 8 * i; }
			Query::Sign(kid, meth, Cow::Borrowed(&payload[34..]))
		} else if cmd == CMD_HAS_TAG {
			if payload.len() < 4 { return Err(QueryParseError::Truncated(cmd)); }
			let mut kid = 0;
			for i in 0..4 {  kid |= (payload[i] as usize) << 8 * i; }
			Query::HasTag(kid, Cow::Borrowed(&payload[4..]))
		} else if cmd == CMD_ADMIN_CREATE_KEY {
			if payload.len() < 8 { return Err(QueryParseError::Truncated(cmd)); }
			let (mut keytype, mut profile) = (0, 0);
			for i in 0..4 {  keytype |= (payload[i+0] as u32) << 8 * i; }
			for i in 0..4 {  profile |= (payload[i+4] as u32) << 8 * i; }
			Query::ACreateKey(Cow::Borrowed(&payload[8..]), keytype, profile)
		} else if cmd == CMD_ADMIN_DELETE_KEY {
			Query::ADeleteKey(Cow::Borrowed(&payload[..]))
		} else if cmd == CMD_ADMIN_DUMP_KEY {
			Query::ADumpKey(Cow::Borrowed(&payload[..]))
		} else if cmd == CMD_ADMIN_KEY_NAME {
			if payload.len() < 4 { return Err(QueryParseError::Truncated(cmd)); }
			let mut kidx = 0;
			for i in 0..4 {  kidx |= (payload[i] as u32) << 8 * i; }
			Query::AKeyName(kidx)
		} else {
			return Err(QueryParseError::UnknownCommand(cmd));
		})
	}
}

pub enum CReply<'a>
{
	Empty,					//Empty packet.
	Truncated(u8),				//Truncated packet with no association.
	BadAssociation(u64),			//Association.
	BadCommand(u64, u8),			//Association+Command
	UnknownKeyAgreement(u8),		//Method
	BadKeyIndex(u64, usize),		//Association+Keyid
	RevokedKeyIndex(u64, usize),		//Association+Keyid
	BadKey(u64, usize),			//Association+Sequence
	SignUnauthorized(u64, usize),		//Association+Sequence.
	SignFailed(u64, usize),			//Association+Sequence.
	BadMac(u64, usize),			//Association+Sequence.
	AdminUnauthorized(u64, usize),		//Association+Sequence.
	BadKeyName(u64, Cow<'a, [u8]>),		//Association+Name.
	InternalError(u64, Cow<'a, str>),	//Association+Error.
	//Unknown error.
	UnknownSequence(u64, usize, u8),	//Association+Sequence.
	UnknownResponse(u8),
	//Successful.
	PublicKey(u64, usize, Cow<'a, str>, Cow<'a, [u8]>, Vec<u16>),
	Signed(u64, usize, Cow<'a, [u8]>),
	AuthReply(u64, usize, usize, [u8;32]),
	TagReport(u64, usize, bool),
	Ack(u64, usize),
}

impl<'a> CReply<'a>
{
	pub fn parse(msg: &'a [u8]) -> CReply<'a>
	{
		if msg.len() == 0 { return CReply::Empty; }
		match msg[0] {
			0 => {
				//AuthReply.
				if msg.len() < 2 { return CReply::Truncated(0); }
				if msg[1] != 0 { return CReply::UnknownKeyAgreement(msg[1]); }
				if msg.len() < 50 { return CReply::Truncated(0); }
				let mut c = [0;32];
				c.copy_from_slice(&msg[18..50]);
				CReply::AuthReply(parse_i!(8 msg, 2), parse_i!(4 msg, 10), parse_i!(4 msg, 14), c)
			},
			1 => {
				//Public key.
				if msg.len() < 19 { return CReply::Truncated(1); }
				let aid = parse_i!(8 msg, 1);
				let kid = parse_i!(4 msg, 9);
				let nlen = parse_i!(2 msg, 13) as usize;
				let klen = parse_i!(2 msg, 15) as usize;
				let mlen = parse_i!(2 msg, 17) as usize;
				if msg.len() < (19 + nlen + klen + 2 * mlen) { return CReply::Truncated(1); }
				let (_, name) = msg.split_at(19);
				let (name, spki) = name.split_at(nlen);
				let (spki, meth) = spki.split_at(klen);
				let (meth, _) = meth.split_at(2*mlen);
				//If name is not valid unicode, return bad key name, because we do not want such
				//names.
				let name = match from_utf8(name) {
					Ok(name) => name,
					Err(_) => return CReply::BadKeyName(aid, Cow::Borrowed(name))
				};
				let spki = spki;
				let mut xmeth = Vec::with_capacity(mlen);
				for i in 0..mlen { xmeth.push(parse_i!(2 meth, 2 * i)); }
				CReply::PublicKey(aid, kid, Cow::Borrowed(name), Cow::Borrowed(spki), xmeth)
			},
			2 => {
				//Signature.
				if msg.len() < 13 { return CReply::Truncated(2); }
				CReply::Signed(parse_i!(8 msg, 1), parse_i!(4 msg, 9), Cow::Borrowed(&msg[13..]))
			},
			3 => {
				//Tag report.
				if msg.len() < 14 { return CReply::Truncated(3); }
				CReply::TagReport(parse_i!(8 msg, 1), parse_i!(4 msg, 9), msg[13] != 0)
			},
			4 => {
				//ACK.
				if msg.len() < 13 { return CReply::Truncated(4); }
				CReply::Ack(parse_i!(8 msg, 1), parse_i!(4 msg, 9))
			},
			0x80 => {
				//Bad association.
				if msg.len() < 9 { return CReply::Truncated(0x80); }
				CReply::BadAssociation(parse_i!(8 msg, 1))
			},
			0x81 => {
				//Unknown key agreement.
				if msg.len() < 2 { return CReply::Truncated(0x81); }
				CReply::UnknownKeyAgreement(msg[1])
			},
			0x82 => {
				//Bad command.
				if msg.len() < 10 { return CReply::Truncated(0x82); }
				CReply::BadCommand(parse_i!(8 msg, 1), msg[9])
			},
			0x83 => {
				//Bad key.
				if msg.len() < 13 { return CReply::Truncated(0x83); }
				CReply::BadKeyIndex(parse_i!(8 msg, 1), parse_i!(4 msg, 9))
			},
			0x84 => {
				//Bad MAC.
				if msg.len() < 13 { return CReply::Truncated(0x84); }
				CReply::BadMac(parse_i!(8 msg, 1), parse_i!(4 msg, 9))
			},
			0x85 => {
				//Bad key name.
				if msg.len() < 9 { return CReply::Truncated(0x85); }
				CReply::BadKeyName(parse_i!(8 msg, 1), Cow::Borrowed(&msg[9..]))
			},
			0x86 => {
				//Internal error
				if msg.len() < 9 { return CReply::Truncated(0x86); }
				let err = EscapeByteString(&msg[9..]).to_string();
				CReply::InternalError(parse_i!(8 msg, 1), Cow::Owned(err))
			},
			0x87 => {
				//Revoked key index.
				if msg.len() < 9 { return CReply::Truncated(0x87); }
				CReply::RevokedKeyIndex(parse_i!(8 msg, 1), parse_i!(4 msg, 9))
			},
			x@0x90..=0xDF => {
				//Sign unauthorized
				//Sign failed
				//Unknown.
				if msg.len() < 13 { return CReply::Truncated(x); }
				match x {
					0x90 => CReply::SignUnauthorized(parse_i!(8 msg, 1), parse_i!(4 msg, 9)),
					0x91 => CReply::SignFailed(parse_i!(8 msg, 1), parse_i!(4 msg, 9)),
					0x92 => CReply::BadKey(parse_i!(8 msg, 1), parse_i!(4 msg, 9)),
					0x93 => CReply::AdminUnauthorized(parse_i!(8 msg, 1), parse_i!(4 msg, 9)),
					_ => CReply::UnknownSequence(parse_i!(8 msg, 1), parse_i!(4 msg, 9), x),
				}
			},
			x => {
				CReply::UnknownResponse(x)
			}
		}
	}
}

pub enum Reply
{
	Empty,					//Empty packet.
	Truncated(u8),				//Truncated packet with no association.
	TruncatedAssociation(u64, u8),		//Truncated packet with known association.
	BadAssociation(u64),			//Association.
	Replay(u64, usize),			//Association+Sequence
	BadMac(u64, usize),			//Association+Sequence
	BadCommand(u64, u8),			//Association+Command
	UnknownKeyAgreement(u8),		//Method
	BadKeyIndex(u64, usize),		//Association+Keyid
	RevokedKeyIndex(u64, usize),		//Association+Keyid
	BadKey(u64, usize),			//Association+Sequence
	SignUnauthorized(u64, usize),		//Association+Sequence.
	SignFailed(u64, usize),			//Association+Sequence.
	BadKeyName(u64, Vec<u8>),		//Association+Name.
	InternalError(u64, String),		//Association+Error.
	AdminUnauthorized(u64, usize),		//Association+Sequence.
	//Successful.
	PublicKey(u64, usize, String, Vec<u8>, Vec<u16>),	//Dumped Public key.
	Signed(u64, usize, Vec<u8>),
	AuthReply(u64, usize, usize, [u8;32]),
	TagReport(u64, usize, bool),
	Ack(u64, usize),
}

impl Reply
{
	pub fn serialize(&self) -> Vec<u8>
	{
		match self {
			//Do not send any response to badly enough mangled stuff.
			&Reply::Empty => Vec::new(),
			&Reply::Truncated(_) => Vec::new(),
			&Reply::TruncatedAssociation(_, _) => Vec::new(),
			&Reply::Replay(_, _) => Vec::new(),

			&Reply::BadAssociation(aid) => {
				let mut out = Vec::with_capacity(9);
				out.push(0x80);
				extend_u64(&mut out, aid);
				out
			},
			&Reply::UnknownKeyAgreement(kaid) => vec![0x81, kaid],
			&Reply::BadCommand(aid, cmd) => {
				let mut out = Vec::with_capacity(10);
				out.push(0x82);
				extend_u64(&mut out, aid);
				out.push(cmd);
				out
			},
			&Reply::BadKeyIndex(aid, kid) => {
				let mut out = Vec::with_capacity(13);
				out.push(0x83);
				extend_u64(&mut out, aid);
				extend_u32(&mut out, kid);
				out
			},
			&Reply::BadMac(aid, seq) => {
				let mut out = Vec::with_capacity(13);
				out.push(0x84);
				extend_u64(&mut out, aid);
				extend_u32(&mut out, seq);
				out
			},
			&Reply::BadKeyName(aid, ref name) => {
				let mut out = Vec::with_capacity(9+name.len());
				out.push(0x85);
				extend_u64(&mut out, aid);
				out.extend_from_slice(&name);
				out
			},
			&Reply::InternalError(aid, ref name) => {
				let mut out = Vec::with_capacity(9+name.len());
				out.push(0x86);
				extend_u64(&mut out, aid);
				out.extend_from_slice(name.as_bytes());
				out
			},
			&Reply::RevokedKeyIndex(aid, kid) => {
				let mut out = Vec::with_capacity(13);
				out.push(0x87);
				extend_u64(&mut out, aid);
				extend_u32(&mut out, kid);
				out
			},
			&Reply::SignUnauthorized(aid, seq) => {
				let mut out = Vec::with_capacity(13);
				out.push(0x90);
				extend_u64(&mut out, aid);
				extend_u32(&mut out, seq);
				out
			},
			&Reply::SignFailed(aid, seq) => {
				let mut out = Vec::with_capacity(13);
				out.push(0x91);
				extend_u64(&mut out, aid);
				extend_u32(&mut out, seq);
				out
			},
			&Reply::BadKey(aid, seq) => {
				let mut out = Vec::with_capacity(13);
				out.push(0x92);
				extend_u64(&mut out, aid);
				extend_u32(&mut out, seq);
				out
			},
			&Reply::AdminUnauthorized(aid, seq) => {
				let mut out = Vec::with_capacity(13);
				out.push(0x93);
				extend_u64(&mut out, aid);
				extend_u32(&mut out, seq);
				out
			},
			//.......................................
			&Reply::PublicKey(aid, kid, ref name, ref spki, ref meth) => {
				let mut out = Vec::with_capacity(name.len() + spki.len() + 2 * meth.len() + 19);
				out.extend_from_slice(&[1]);
				extend_u64(&mut out, aid);
				extend_u32(&mut out, kid);
				extend_u16(&mut out, name.len() as u16);
				extend_u16(&mut out, spki.len() as u16);
				extend_u16(&mut out, meth.len() as u16);
				out.extend_from_slice(name.as_bytes());
				out.extend_from_slice(spki);
				for m in meth.iter() { extend_u16(&mut out, *m); }
				out
			},
			&Reply::Signed(aid, seq, ref sig) => {
				let mut out = Vec::with_capacity(sig.len() + 13);
				out.extend_from_slice(&[2]);
				extend_u64(&mut out, aid);
				extend_u32(&mut out, seq);
				out.extend_from_slice(sig);
				out
			},
			&Reply::AuthReply(aid, txid, kcount, challenge) => {
				let mut out = Vec::with_capacity(50);
				out.extend_from_slice(&[0, 0]);
				extend_u64(&mut out, aid);
				extend_u32(&mut out, txid);
				extend_u32(&mut out, kcount);
				out.extend_from_slice(&challenge);
				out
			},
			&Reply::TagReport(aid, kid, rpt) => {
				let mut out = Vec::with_capacity(14);
				out.extend_from_slice(&[3]);
				extend_u64(&mut out, aid);
				extend_u32(&mut out, kid);
				out.push(if rpt { 1 } else { 0 });
				out
			},
			&Reply::Ack(aid, seq) => {
				let mut out = Vec::with_capacity(13);
				out.extend_from_slice(&[4]);
				extend_u64(&mut out, aid);
				extend_u32(&mut out, seq);
				out
			},
		}
	}
}

pub enum MacVerifyError
{
	Truncated,
	Replay(usize),
	BadMac(usize),
}

fn calculate_mac_low(secret: &[u8;32], tbs: &[u8]) -> [u8;32]
{
	Sha256::hmac_v(secret, &[b"btls-keyserv3 MAC input\x00", tbs])
}

pub fn calculate_session_key(ax: &[u8;32], bx: &[u8;32], a: &[u8;32], b: &[u8;32], x: &[u8;32]) -> [u8;32]
{
	let mut axbx = [0; 64];
	(&mut axbx[..32]).copy_from_slice(ax);
	(&mut axbx[32..]).copy_from_slice(bx);
	Sha256::hmac_v(&axbx[..], &[b"btls-keyserv3 key derive\x00", &x[..], &a[..], &b[..]])
}

pub struct MacVerifier
{
	key: [u8; 32],
	wcmdserial_top: AtomicUsize,
	wcmdserial_used: AtomicUsize,
}

impl MacVerifier
{
	pub fn new(key: &[u8;32]) -> MacVerifier
	{
		MacVerifier {
			key: *key,
			wcmdserial_top: AtomicUsize::new(0),
			wcmdserial_used: AtomicUsize::new(0),
		}
	}
	pub fn verify<'a>(&self, msg: &'a [u8]) -> Result<(u8, usize, &'a [u8]), MacVerifyError>
	{
		//The message must be at least 9 bytes to even dispatch this far.
		if msg.len() < 45 {
			return Err(MacVerifyError::Truncated);
		}
		let mut cmdserial = 0usize;
		for i in 0..4 {  cmdserial |= (msg[9+i] as usize) << 8 * i; }
		let mut top = self.wcmdserial_top.load(Ordering::Relaxed);
		let mut used = self.wcmdserial_used.load(Ordering::Relaxed);
		let back = top.saturating_sub(cmdserial);
		if back > 8 * size_of::<usize>() && ((used >> (back as u32)) & 1) == 1 {
			return Err(MacVerifyError::Replay(cmdserial));
		}

		//Calculate and check MAC.
		let tbs = &msg[..msg.len()-32];		//Strip MAC.
		let mac = &msg[msg.len()-32..];		//Strip MAC.
		let checkval = calculate_mac_low(&self.key, tbs);
		let mut syndrome = 0;
		for i in 0..32 { syndrome |= checkval[i] ^ mac[i]; }
		if syndrome != 0 {
			return Err(MacVerifyError::BadMac(cmdserial));
		}
		//Update used bits.
		if cmdserial <= top {
			used |= 1usize << (back as u32);
		} else if cmdserial.saturating_sub(top) < 8 * size_of::<usize>() {
			used <<= cmdserial.saturating_sub(top) as u32;	//Shift bits out of window.
			used |= 1;	//This is also used.
			top = cmdserial;
		} else {
			used = 1;	//Only this is used.
			top = cmdserial;
		}
		self.wcmdserial_top.store(top, Ordering::Relaxed);
		self.wcmdserial_used.store(used, Ordering::Relaxed);
		//Return command byte and payload with association id, sequence number and MAC stripped.
		Ok((msg[0], cmdserial, &msg[13..msg.len()-32]))
	}
}

//1 Command byte + 8 association id + 4 sequence number + 32 MAC.
pub const MAC_EXPANSION: usize = 45;

pub struct MacGenerator
{
	key: [u8; 32],
	assoc_id: u64,
	next_serial: AtomicUsize,
}


impl MacGenerator
{
	pub fn new(id: u64, key: &[u8;32]) -> MacGenerator
	{
		MacGenerator {
			key: *key,
			assoc_id: id,
			next_serial: AtomicUsize::new(0),
		}
	}
	pub fn generate<'a>(&self, target: &'a mut [u8], cmd: u8, src: &[u8]) -> Option<(&'a [u8], u64, usize)>
	{
		let tlen = 45 + src.len();
		let dend = 13 + src.len();
		fail_if_none!(target.len() < tlen);
		let target = &mut target[..tlen];

		target[0] = cmd;
		let serial = self.next_serial.load(Ordering::Relaxed);
		for i in 0..8 { target[1+i] |= (self.assoc_id >> 8 * i) as u8; }
		for i in 0..4 { target[9+i] |= (serial >> 8 * i) as u8; }
		(&mut target[13..dend]).copy_from_slice(src);
		self.next_serial.fetch_add(1, Ordering::Relaxed);
		{
			let (tbs, mac) = target.split_at_mut(dend);
			mac.copy_from_slice(&calculate_mac_low(&self.key, tbs));
		}
		Some((target, self.assoc_id, serial))
	}
}
