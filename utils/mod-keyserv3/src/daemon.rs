use ::os_set_log;
use ::os_syslog;
use btls_aux_unix::FileDescriptor;
use btls_aux_unix::LogFacility;
use btls_aux_unix::LogLevel;
use btls_aux_unix::OpenlogFlags;
use btls_aux_unix::Pid;
use btls_aux_unix::PipeFlags;
use btls_aux_unix::setsid;
use btls_aux_unix::systemd_is_notify;
use btls_aux_unix::systemd_notify_startup;
use std::fs::File;
use std::io::stderr;
use std::io::Write;
use std::path::Path;
use std::process::exit;


pub fn daemonize(log_facility: &[u8], pidfile: Option<&Path>, no_detach: bool, logfile: Option<&str>)
{
	if systemd_is_notify() {
		//Open log file if any, as there is no forking.
		if let Some(logfile) = logfile {
			if let Err(err) = os_set_log(logfile.as_bytes()) {
				writeln!(stderr(), "Critical: {err}").unwrap();
			}
		}
		//Open log, signal Systemd we are ready and return.
		::libcm::os_openlog_systemd();
		if let Err(err) = systemd_notify_startup() {
			os_syslog(LogLevel::Critical, &format!("Failed to notify startup to systemd: {err}"));
			exit(1);
		}
		return;
	}
	if no_detach { return; }
	//Create signal pipe.
	let flags = PipeFlags::CLOEXEC;
	let (rspipe, wspipe) = FileDescriptor::pipe2(flags).unwrap_or_else(|err|{
		os_syslog(LogLevel::Critical, &format!("Failed to create pipe: {err}"));
		exit(1);
	});
	//We have to do full daemonization.
	match Pid::fork() {
		Ok(Some(_)) => {
			//We are the parent. Close the write end of signal pipe.
			drop(wspipe);
			//Wait for signal from pipe.
			let mut ok = false;
			loop {
				let mut buf = [0;1];
				let r = rspipe.read(&mut buf);
				if let Ok(r) = r { ok |= r == 1 && buf[0] == b'R'; break; }
			}
			exit(if ok { 0 } else { 1 });
		},
		Ok(None) => {
			//We are the child. Close the read end of the pipe.
			drop(rspipe);
			//Unfortunately we can not report errors to stderr. So activate syslog.
			::libcm::os_openlog(log_facility, OpenlogFlags::PID, LogFacility::Daemon).
				unwrap_or_else(|_|exit(57));
			//Call setsid and fork again. This should never fail.
			setsid().ok();
			match Pid::fork() {
				Ok(None) => {
					//If logfile is set, open it.
					if let Some(logfile) = logfile {
						if let Err(err) = os_set_log(logfile.as_bytes()) {
							::libcm::os_syslog(LogLevel::Critical,
								&format!("Critical: {err}"));
						}
					}
					//Signal we are ready, activate syslog create pidfile and return.
					let tmp = [b'R'];
					wspipe.write(&tmp).ok();
					if let Some(pidfile) = pidfile {
						let out = Pid::current().to_string();
						File::create(pidfile).and_then(|mut fp|fp.write_all(out.as_bytes())).
							unwrap_or_else(|err|os_syslog(LogLevel::Error,
							&format!("Failed to create pidfile: {pf}: {err}",
							pf=pidfile.display())));
					}
					return;
				},
				Ok(Some(_)) => exit(0),
				Err(err) => {
					os_syslog(LogLevel::Critical, &format!("Failed to fork: {err}"));
					exit(1);
				}
			}
		},
		Err(err) => {
			//Error forking.
			os_syslog(LogLevel::Critical, &format!("Failed to fork: {err}"));
			exit(1);
		}
	}
}
