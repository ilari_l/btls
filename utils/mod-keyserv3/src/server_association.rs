use ::calculate_session_key;
use ::GlobalKeyStorage;
use ::KeyHandle;
use ::LocalKeyStorage;
use ::KeyAclSubject;
use ::MacVerifier;
use ::MacVerifyError;
use ::os_syslog;
use ::Reply;
use ::SignatureClass;
use ::SocketCredentials;
use ::association::Query;
use ::association::QueryParseError;
use ::rawkey::ByIndexResponse;
use btls_aux_collections::Mutex;
use btls_aux_collections::RwLock;
use btls_aux_fail::ResultExt;
use btls_aux_futures::FutureSink;
use btls_aux_memory::EscapeByteString;
use btls_aux_random::secure_random;
use btls_aux_random::TemporaryRandomLifetimeTag;
use btls_aux_random::TemporaryRandomStream;
use btls_aux_serialization::Sink;
use btls_aux_signatures::EcdsaCurve;
use btls_aux_signatures::EcdsaKeyDecode;
use btls_aux_signatures::EcdsaKeyPair;
use btls_aux_signatures::Ed25519KeyDecode;
use btls_aux_signatures::Ed25519KeyPair;
use btls_aux_signatures::Ed448KeyDecode;
use btls_aux_signatures::Ed448KeyPair;
use btls_aux_signatures::KcKeyFormat;
use btls_aux_signatures::SignatureFormat;
use btls_aux_signatures::SignatureKeyPair;
use btls_aux_unix::LogLevel;
use btls_aux_unix::OpenFlags;
use btls_aux_unix::OsError;
use btls_aux_unix::Path as UnixPath;
use btls_aux_xed25519::x25519_agree;
use btls_aux_xed25519::x25519_generate;
use std::collections::BTreeMap;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::fs::remove_file;
use std::io::Error as IoError;
use std::io::ErrorKind as IoErrorKind;
use std::mem::replace;
use std::ops::Deref;
use std::ops::DerefMut;
use std::os::unix::ffi::OsStrExt;
use std::os::unix::fs::symlink;
use std::path::Path;
use std::str::from_utf8;
use std::sync::Arc;
use std::time::Duration;
use std::time::Instant;

trait ReplyExt
{
	fn admin_unauthorized(association: &AssociationHandle, sequence: usize) -> Reply;
	fn empty() -> Reply;
	fn truncated(cmd: u8) -> Reply;
	fn bad_association(id: u64) -> Reply;
	fn unknown_key_agreement(agreement: u8) -> Reply;
	fn truncated_association(association: &AssociationHandle, cmd: u8) -> Reply;
	fn replay(association: &AssociationHandle, seqno: usize) -> Reply;
	fn bad_mac(association: &AssociationHandle, seqno: usize) -> Reply;
	fn bad_command(association: &AssociationHandle, cmd: u8) -> Reply;
	fn bad_key_index(association: &AssociationHandle, index: usize) -> Reply;
	fn revoked_key_index(association: &AssociationHandle, index: usize) -> Reply;
	fn bad_key(association: &AssociationHandle, kid: [u8;32], seqno: usize) -> Reply;
	fn bad_key_name(association: &AssociationHandle, name: &[u8]) -> Reply;
	fn internal_error(association: &AssociationHandle, err: &str) -> Reply;
	fn sign_unauthorized(association: &AssociationHandle, sequence: usize, key: &KeyHandle, data: &[u8]) ->
		Reply;
	fn sign_failed(association: &AssociationHandle, sequence: usize, key: &KeyHandle) -> Reply;
	fn public_key(association: &AssociationHandle, keyid: usize, name: String, spki: Vec<u8>,
		methods: Vec<u16>) -> Reply;
	fn signed(association: &AssociationHandle, sequence: usize, data: Vec<u8>) -> Reply;
	fn auth_reply(assoc_id: u64, txid: usize, keycount: usize, kdata: &[u8;32]) -> Reply;
	fn tag_report(association: &AssociationHandle, keyid: usize, flag: bool) -> Reply;
	fn ack(association: &AssociationHandle, seqno: usize) -> Reply;
}

struct PrintAssociation<'a>(&'a AssociationHandle);

impl<'a> Display for PrintAssociation<'a>
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		write!(fmt, "{id}", id=(self.0).0.id)?;
		if let Some(subj) = (self.0).0.subject.get(0) {
			write!(fmt, "({subj})")?;
		} else {
			fmt.write_str("(<Unknown>)")?;
		}
		Ok(())
	}
}

impl ReplyExt for Reply
{
	fn admin_unauthorized(association: &AssociationHandle, sequence: usize) -> Reply
	{
		os_syslog(LogLevel::Warning, &format!("Association {a}: Received unauthorized admin command",
			a=PrintAssociation(association)));
		Reply::AdminUnauthorized(association.0.id, sequence)
	}
	fn empty() -> Reply
	{
		os_syslog(LogLevel::Warning, &format!("Received empty packet"));
		Reply::Empty
	}
	fn truncated(cmd: u8) -> Reply
	{
		os_syslog(LogLevel::Warning, &format!("Received truncated packet with command {cmd}"));
		Reply::Truncated(cmd)
	}
	fn bad_association(id: u64) -> Reply
	{
		os_syslog(LogLevel::Warning, &format!("Received packet with unknown association {id}"));
		Reply::BadAssociation(id)
	}
	fn unknown_key_agreement(agreement: u8) -> Reply
	{
		os_syslog(LogLevel::Warning, &format!("Received unknown key agreement method {agreement}"));
		Reply::UnknownKeyAgreement(agreement)
	}
	fn truncated_association(association: &AssociationHandle, cmd: u8) -> Reply
	{
		os_syslog(LogLevel::Warning, &format!("Association {a}: Received truncated packet with command {cmd}",
			a=PrintAssociation(association)));
		Reply::TruncatedAssociation(association.0.id, cmd)
	}
	fn replay(association: &AssociationHandle, seqno: usize) -> Reply
	{
		os_syslog(LogLevel::Warning, &format!("Association {a}: Received replay of sequence {seqno}",
			a=PrintAssociation(association)));
		Reply::Replay(association.0.id, seqno)
	}
	fn bad_mac(association: &AssociationHandle, seqno: usize) -> Reply
	{
		os_syslog(LogLevel::Warning, &format!("Association {a}: Received bad MAC for sequence {seqno}",
			a=PrintAssociation(association)));
		Reply::BadMac(association.0.id, seqno)
	}
	fn bad_command(association: &AssociationHandle, cmd: u8) -> Reply
	{
		os_syslog(LogLevel::Warning, &format!("Association {a}: Received bad command {cmd}",
			a=PrintAssociation(association)));
		Reply::BadCommand(association.0.id, cmd)
	}
	fn bad_key_index(association: &AssociationHandle, index: usize) -> Reply
	{
		os_syslog(LogLevel::Warning, &format!("Association {a}: Received bad key index {index}",
			a=PrintAssociation(association)));
		Reply::BadKeyIndex(association.0.id, index)
	}
	fn revoked_key_index(association: &AssociationHandle, index: usize) -> Reply
	{
		//No warning about this, as it is expected normal.
		Reply::RevokedKeyIndex(association.0.id, index)
	}
	fn bad_key(association: &AssociationHandle, kid: [u8;32], sequence: usize) -> Reply
	{
		os_syslog(LogLevel::Warning, &format!("Association {a}: Received bad key id {kid:?}",
			a=PrintAssociation(association)));
		Reply::BadKey(association.0.id, sequence)
	}
	fn bad_key_name(association: &AssociationHandle, name: &[u8]) -> Reply
	{
		os_syslog(LogLevel::Warning, &format!("Association {a}: Received bad key name {name}",
			a=PrintAssociation(association), name=EscapeByteString(name)));
		Reply::BadKeyName(association.0.id, name.to_owned())
	}
	fn internal_error(association: &AssociationHandle, err: &str) -> Reply
	{
		os_syslog(LogLevel::Error, &format!("Association {a}: Internal error: {err}",
			a=PrintAssociation(association)));
		Reply::InternalError(association.0.id, err.to_owned())
	}
	fn sign_unauthorized(association: &AssociationHandle, sequence: usize, key: &KeyHandle, data: &[u8]) ->
		Reply
	{
		os_syslog(LogLevel::Warning, &format!(
			"Association {a}: Received unauthorized signature request {data:?} for key {key}",
			a=PrintAssociation(association), key=key.get_key_name()));
		Reply::SignUnauthorized(association.0.id, sequence)
	}
	fn sign_failed(association: &AssociationHandle, sequence: usize, key: &KeyHandle) -> Reply
	{
		os_syslog(LogLevel::Warning, &format!("Association {a}: Failed to sign with key {key}",
			a=PrintAssociation(association), key=key.get_key_name()));
		Reply::SignFailed(association.0.id, sequence)
	}
	fn public_key(association: &AssociationHandle, keyid: usize, name: String, spki: Vec<u8>,
		methods: Vec<u16>) -> Reply
	{
		Reply::PublicKey(association.0.id, keyid, name, spki, methods)
	}
	fn signed(association: &AssociationHandle, sequence: usize, data: Vec<u8>) -> Reply
	{
		Reply::Signed(association.0.id, sequence, data)
	}
	fn auth_reply(assoc_id: u64, txid: usize, keycount: usize, kdata: &[u8;32]) -> Reply
	{
		Reply::AuthReply(assoc_id, txid, keycount, *kdata)
	}
	fn tag_report(association: &AssociationHandle, keyid: usize, flag: bool) -> Reply
	{
		Reply::TagReport(association.0.id, keyid, flag)
	}
	fn ack(association: &AssociationHandle, seqno: usize) -> Reply { Reply::Ack(association.0.id, seqno) }
}

pub trait SignatureReply
{
	fn reply(self, reply: Reply);
}

struct SignatureSink<H:SignatureReply+Send>
{
	handle: AssociationHandle,
	sequence: usize,
	key: Arc<KeyHandle>,
	reply: Option<H>,
}

impl<H:SignatureReply+Send> FutureSink<Result<(SignatureFormat, Vec<u8>), ()>> for SignatureSink<H>
{
	fn sink(&mut self, value: Result<(SignatureFormat, Vec<u8>), ()>)
	{
		let reply = replace(&mut self.reply, None);
		let value = match value {
			Ok((_,x)) => Reply::signed(&self.handle, self.sequence, x),
			Err(_) => Reply::sign_failed(&self.handle, self.sequence, &self.key)
		};
		if let Some(reply) = reply { reply.reply(value); }
	}
}

fn durable_write(p: &Path, mut content: &[u8]) -> Result<(), IoError>
{
	//Generate temporary filename.
	let directory = p.parent();
	let p = UnixPath::new(p.as_os_str().as_bytes()).
		ok_or_else(||IoError::new(IoErrorKind::InvalidInput, "NUL bytes not allowed in filenames"))?;
	let mut index = 0;
	loop {
		index += 1;
		let tmpname = format!("write.{index}.tmp");
		let tmpfile = match directory {
			Some(p) => p.join(tmpname),
			None => Path::new(&tmpname).to_owned()
		};
		let tmpfile = f_continue!(UnixPath::new(tmpfile.as_os_str().as_bytes()));
		let flags = OpenFlags::O_CREAT|OpenFlags::O_EXCL|OpenFlags::O_WRONLY|OpenFlags::O_CLOEXEC;
		let fd = match tmpfile.open(flags) {
			Ok(fd) => fd,
			Err(OsError::EEXIST) => continue,	//Try next name.
			Err(e) => fail!(IoError::from_raw_os_error(e.to_inner()))
		};
		//Write stuff to fd.
		let mut wstatus = Ok(());
		while wstatus.is_ok() && content.len() > 0 {
			let amt = fd.write(content).unwrap_or_else(|e|{wstatus = Err(e); 0});
			content = &content[amt..];
		}
		//Sync the fd and move to final name.
		wstatus.and_then(|_|fd.sync()).and_then(|_|tmpfile.rename(p)).map_err(|e|{
			//Ignore if this unlink fails, as operation has already failed.
			tmpfile.unlink().ok();
			IoError::from_raw_os_error(e.to_inner())
		})?;
		//Successful.
		return Ok(())
	}
}

struct Association
{
	//Identifier.
	id: u64,
	//MAC verifier.
	verifier: MacVerifier,
	//Client key A&B.
	client_key_a: [u8;32],
	client_key_b: [u8;32],
	//Auth. reply.
	auth_reply: [u8;32],
	//Subject for purposes of ACL.
	subject: Vec<KeyAclSubject>,
	//Keys accessible
	keys: RwLock<LocalKeyStorage>,
	//Timeout.
	timeout: Mutex<Instant>,
	//Admin.
	is_admin: bool,
}

#[derive(Clone)]
struct AssociationHandle(Arc<Association>);

impl AssociationHandle
{
	fn call_update_rights(&self, keys: &GlobalKeyStorage)
	{
		self.0.keys.write().update_rights(keys)
	}
	fn with_keys<T,F>(&self, f: F) -> T where F: FnOnce(&LocalKeyStorage) -> T
	{
		f(self.0.keys.read().deref())
	}
	fn is_timed_out(&self) -> Option<u64>
	{
		let to = self.0.timeout.lock();
		let now = Instant::now();
		now.checked_duration_since(*to.deref()).map(|t|t.as_secs())
	}
	fn unwrap_message<'a>(&self, msg: &'a [u8]) -> Result<(u8, usize, &'a [u8]), Reply>
	{
		let (cmd, serial, payload) = match self.0.verifier.verify(msg) {
			Ok(x) => x,
			Err(MacVerifyError::Truncated) => return Err(Reply::truncated_association(self, msg[0])),
			Err(MacVerifyError::Replay(x)) => return Err(Reply::replay(self, x)),
			Err(MacVerifyError::BadMac(x)) => return Err(Reply::bad_mac(self, x)),
		};
		//Bump timeout.
		*(self.0.timeout.lock().deref_mut()) = Instant::now() + Duration::from_secs(90);
		//Return command byte and payload with association id, sequence number and MAC stripped.
		Ok((cmd, serial, payload))
	}
	//The reply is always called, but not necressarily synchronously.
	fn sign<H:SignatureReply+Send+'static>(&self, sequence: usize, kid: [u8;32], scheme: u16, data: &[u8],
		reply: H)
	{
		let dclass = match SignatureClass::get_class(data) {
			SignatureClass::TlsServer => "TLS server key exchange",
			SignatureClass::TlsClient => "TLS client key exchange",
			SignatureClass::Any => "Arbitrary data",
		};
		let (handle, _) = f_return!(self.with_keys(|keys|keys.get_by_kid(&kid)),
			reply.reply(Reply::bad_key(self, kid, sequence)));
		let future = f_return!(handle.sign(&self.0.subject, scheme, data),
			reply.reply(Reply::sign_unauthorized(self, sequence, &handle, data)));
		os_syslog(LogLevel::Notice, &format!("Association {a}: Signing {dclass} with key {key}",
			a=PrintAssociation(self), key=handle.get_key_name()));
		//As optimization, since the future is likely one of those immediately resolving ones, see if the
		//future has resolved, and if it is, immediately fire the event. This is not racy since if the
		//future resolves between the condition and .sink_to(), the sink_to() will synchronously call the
		//method.
		if future.settled() {
			let sig = match future.read() {
				Ok(Ok((_,x))) => Reply::signed(self, sequence, x),
				_ => Reply::sign_failed(self, sequence, &handle)
			};
			return reply.reply(sig);
		}
		future.sink_to(Box::new(SignatureSink {
			handle: self.clone(),
			sequence: sequence,
			key: handle.clone(),
			reply: Some(reply),
		}));
	}
	//The reply is always called synchronously.
	fn dump_public_key_name<H:SignatureReply+Send+'static>(&self, sequence: usize, name: &[u8], reply: H)
	{
		match self.with_keys(|keys|keys.get_by_name(name)) {
			Some((key, _)) => self.__dump_public_key(sequence, key, reply),
			None => reply.reply(Reply::bad_key_name(self, name))
		}
	}
	//The reply is always called synchronously.
	fn dump_public_key_kid<H:SignatureReply+Send+'static>(&self, sequence: usize, kid: [u8;32], reply: H)
	{
		match self.with_keys(|keys|keys.get_by_kid(&kid)) {
			Some((key, _)) => self.__dump_public_key(sequence, key, reply),
			None => reply.reply(Reply::bad_key(self, kid, sequence))
		}
	}
	//The reply is always called synchronously.
	fn dump_public_key<H:SignatureReply+Send+'static>(&self, sequence: usize, index: usize, reply: H)
	{
		match self.with_keys(|keys|keys.get_by_index(index)) {
			ByIndexResponse::Key(key) => self.__dump_public_key(sequence, key, reply),
			ByIndexResponse::IndexRevoked => reply.reply(Reply::revoked_key_index(self, index)),
			ByIndexResponse::IndexBad => reply.reply(Reply::bad_key_index(self, index)),
		}
	}
	//The reply is always called synchronously.
	fn __dump_public_key<H:SignatureReply+Send+'static>(&self, sequence: usize, handle: Arc<KeyHandle>, reply: H)
	{
		let a = handle.get_key_name();
		let (b, c) = handle.dump_public_key();
		reply.reply(Reply::public_key(self, sequence, a, b, c))
	}
	//The reply is always called synchronously.
	fn has_tag<H:SignatureReply+Send+'static>(&self, index: usize, tag: &[u8], reply: H)
	{
		let a = if let Ok(tag) = from_utf8(tag) {
			let handle = match self.with_keys(|keys|keys.get_by_index(index)) {
				ByIndexResponse::Key(x) => x,
				ByIndexResponse::IndexRevoked =>
					return reply.reply(Reply::revoked_key_index(self, index)),
				ByIndexResponse::IndexBad => return reply.reply(Reply::bad_key_index(self, index)),
			};
			handle.has_tag(tag)
		} else {
			false
		};
		reply.reply(Reply::tag_report(self, index, a))
	}
	//Keytypes:
	//0 => ECDSA P-256
	//1 => ECDSA P-384
	//2 => ECDSA P-521
	//3 => Ed25519
	//4 => Ed448
	//5-7 => (reserved)
	fn admin_create_key(&self, name: &[u8], keytype: u32, nprofile: u32, sequence: usize,
		keys: &mut GlobalKeyStorage, base: &Path, pbase: &Path) -> Reply
	{
		if !self.0.is_admin { return Reply::admin_unauthorized(self, sequence); }
		let name = f_return!(from_utf8(name), Reply::bad_key_name(self, name));
		//The name must not contain /, end with '.acl'/'.priv' or already exist.
		if name == "." || name == ".." || name.find("/").is_some() || name.ends_with(".acl") ||
			name.ends_with(".priv") || keys.get_by_name(name.as_bytes()).is_some() {
			return Reply::bad_key_name(self, name.as_bytes());
		}
		//Check profile is valid.
		let profile = pbase.join(nprofile.to_string());
		if !profile.is_file() {
			return Reply::internal_error(self, &format!("Invalid profile number {nprofile}"));
		}

		//let kpath = base.join(name);
		let kpathpriv = base.join(format!("{name}.priv"));
		let kpathacl = base.join(format!("{name}.acl"));
		//Create key.
		let key = f_return!(create_key_low(keytype), F[|err:String|Reply::internal_error(self, &err)]);
		//Save key and load it again. Check that the file does not exist.
		match kpathpriv.metadata() {
			Err(ref e) if e.kind() == IoErrorKind::NotFound => {},
			_ => return Reply::internal_error(self, "File is in way of saving the key")
		}
		f_return!(durable_write(&kpathpriv, &key), 
			F[|err:IoError|Reply::internal_error(self, &err.to_string())]);
		f_return!(symlink(profile, &kpathacl), F[|err:IoError|Reply::internal_error(self, &err.to_string())]);
		//Load the just generated key.
		f_return!(keys.add(&kpathpriv), F[|err:String|Reply::internal_error(self, &err)]);
		os_syslog(LogLevel::Warning, &format!("Association {a}: Created key {name}",
			a=PrintAssociation(self)));
		Reply::ack(self, sequence)
	}
	fn admin_delete_key(&self, name: &[u8], sequence: usize, keys: &mut GlobalKeyStorage, base: &Path) -> Reply
	{
		if !self.0.is_admin || !keys.allow_delete() { return Reply::admin_unauthorized(self, sequence); }
		let name = f_return!(from_utf8(name), Reply::bad_key_name(self, name));
		//The name must not contain /, end with '.acl' and already exist.
		if name == "." || name == ".." || name.find("/").is_some() || name.ends_with(".acl") ||
			keys.get_by_name(name.as_bytes()).is_none() {
			return Reply::bad_key_name(self, name.as_bytes());
		}
		let kpath = base.join(name);
		let kpathacl = base.join(format!("{name}.acl"));
		f_return!(remove_file(kpath).and_then(|_|remove_file(kpathacl)),
			F[|err|Reply::internal_error(self, &format!("Error removing key: {err}"))]);
		keys.delete_by_name(name);
		os_syslog(LogLevel::Warning, &format!("Association {a}: Deleted key {name}",
			a=PrintAssociation(self)));
		Reply::ack(self, sequence)
	}
	fn admin_dump_key(&self, name: &[u8], sequence: usize, keys: &GlobalKeyStorage) -> Reply
	{
		if !self.0.is_admin { return Reply::admin_unauthorized(self, sequence); }
		match keys.get_by_name(&name) {
			Some(handle) => {
				let a = handle.get_key_name();
				let (b, c) = handle.dump_public_key();
				Reply::public_key(self, sequence, a, b, c)
			},
			None => Reply::bad_key_name(self, name)
		}
	}
	fn admin_key_name(&self, index: u32, sequence: usize, keys: &GlobalKeyStorage) -> Reply
	{
		if !self.0.is_admin { return Reply::admin_unauthorized(self, sequence); }
		match keys.get_by_index(index as usize) {
			Some(handle) => {
				let a = handle.get_key_name();
				let (b, c) = handle.dump_public_key();
				Reply::public_key(self, sequence, a, b, c)
			},
			None => Reply::bad_key_index(self, index as usize)
		}
	}
}

const MAX_ASSOCIATIONS: usize = 128;

pub struct Associations(BTreeMap<u64, AssociationHandle>, u64);

impl Associations
{
	pub fn new() -> Associations { Associations(BTreeMap::new(), 1) }
	//Update rights of all connections.
	pub fn call_update_rights(&self, keys: &GlobalKeyStorage)
	{
		for (_, v) in self.0.iter() { v.call_update_rights(keys); }
	}
	//Do a command, and send reply (not necressarily synchronously).
	pub fn do_command<H:SignatureReply+Send+'static>(&mut self, global_keys: &mut GlobalKeyStorage, msg: &[u8],
		sc: SocketCredentials, reply: H, basedir: &Path, pbasedir: &Path)
	{
		let cmd = msg.get(0).map(|x|*x);
		if cmd == Some(0) {
			//Assume at least first byte exists to dispatch this.
			let (txid, a, b) = match Query::parse(0, &msg[1..]) {
				Ok(Query::AuthM0(x, y, z)) => (x, y, z),
				//How on earth?
				Ok(Query::DumpKey(_)) => return reply.reply(Reply::empty()),
				Ok(Query::DumpKeyKid(_)) => return reply.reply(Reply::empty()),
				Ok(Query::DumpKeyName(_)) => return reply.reply(Reply::empty()),
				Ok(Query::Sign(_, _, _)) => return reply.reply(Reply::empty()),
				Ok(Query::HasTag(_, _)) => return reply.reply(Reply::empty()),
				Ok(Query::ACreateKey(_, _, _)) => return reply.reply(Reply::empty()),
				Ok(Query::ADeleteKey(_)) => return reply.reply(Reply::empty()),
				Ok(Query::ADumpKey(_)) => return reply.reply(Reply::empty()),
				Ok(Query::AKeyName(_)) => return reply.reply(Reply::empty()),
				Err(QueryParseError::Truncated(cmd)) => return reply.reply(Reply::truncated(cmd)),
				//This is not supposed to happen.
				Err(QueryParseError::UnknownCommand(_)) => return reply.reply(Reply::empty()),
				Err(QueryParseError::UnknownKeyAgreement(ka)) =>
					return reply.reply(Reply::unknown_key_agreement(ka)),
			};
			//If association already exists, return it.
			match self.find_by_client_key(&a, &b) {
				Some(x) => return reply.reply(Reply::auth_reply(x.0.id, txid,
					x.with_keys(|keys|keys.len()), &x.0.auth_reply)),
				None => ()
			};
			//Otherwise create session. If there are too many, try to drop existing session.
			if self.0.len() > MAX_ASSOCIATIONS {
				let mut id = None;
				let mut largest_excess = 0;
				for (_id, entry) in self.0.iter() {
					if let Some(amt) = entry.is_timed_out() {
						if amt > largest_excess {
							largest_excess = amt;
							id = Some(*_id);
						}
					}
				}
				if let Some(id) = id { self.0.remove(&id); }
			}
			let newsession = self._create(global_keys, &a, &b, sc);
			self.0.insert(newsession.0.id, newsession.clone());
			return reply.reply(Reply::auth_reply(newsession.0.id, txid,
				newsession.with_keys(|keys|keys.len()), &newsession.0.auth_reply));
		} else {
			//Check that length is big enough to contain association Identifier.
			if msg.len() < 1 { return reply.reply(Reply::empty()); }
			if msg.len() < 9 { return reply.reply(Reply::truncated(msg[0])); }
			let mut id = 0;
			for i in 0..8 {  id |= (msg[1+i] as u64) << 8 * i; }
			let association = f_return!(self.find_by_id(id), reply.reply(Reply::bad_association(id)));
			let (cmd, sequence, payload) = f_return!(association.unwrap_message(msg),
				F[|y|reply.reply(y)]);
			match Query::parse(cmd, payload) {
				//How on earth?
				Ok(Query::AuthM0(_, _, _)) => reply.reply(Reply::empty()),
				Ok(Query::DumpKey(kid)) =>
					association.dump_public_key(sequence, kid, reply),
				Ok(Query::DumpKeyKid(kid)) =>
					association.dump_public_key_kid(sequence, kid, reply),
				Ok(Query::DumpKeyName(name)) =>
					association.dump_public_key_name(sequence, name.deref(), reply),
				Ok(Query::Sign(kid, meth, data)) =>
					association.sign(sequence, kid, meth, data.deref(), reply),
				Ok(Query::HasTag(kid, tag)) => association.has_tag(kid, tag.deref(), reply),
				Ok(Query::ACreateKey(name, ktype, profile)) => {
					reply.reply(association.admin_create_key(name.deref(), ktype, profile,
						sequence, global_keys, basedir, pbasedir));
					self.call_update_rights(global_keys);
				},
				Ok(Query::ADeleteKey(name)) => {
					reply.reply(association.admin_delete_key(name.deref(), sequence,
						global_keys, basedir));
					self.call_update_rights(global_keys);
				},
				Ok(Query::ADumpKey(name)) =>
					reply.reply(association.admin_dump_key(name.deref(), sequence, global_keys)),
				Ok(Query::AKeyName(index)) =>
					reply.reply(association.admin_key_name(index, sequence, global_keys)),
				Err(QueryParseError::Truncated(cmd)) => reply.reply(Reply::truncated(cmd)),
				Err(QueryParseError::UnknownCommand(cmd)) =>
					reply.reply(Reply::bad_command(&association, cmd)),
				//This is not supposed to happen.
				Err(QueryParseError::UnknownKeyAgreement(ka)) =>
					reply.reply(Reply::unknown_key_agreement(ka)),
			}
		}
	}
	fn find_by_client_key(&self, a: &[u8;32], b: &[u8;32]) -> Option<AssociationHandle>
	{
		for (_, val) in self.0.iter() {
			if &val.0.client_key_a == a && &val.0.client_key_b == b { return Some(val.clone()); }
		}
		None
	}
	fn find_by_id(&self, id: u64) -> Option<AssociationHandle> { self.0.get(&id).map(|x|x.clone()) }
	fn _create(&mut self, global_keys: &GlobalKeyStorage, a: &[u8;32], b: &[u8;32], sc: SocketCredentials) ->
		AssociationHandle
	{
		let mut x = [0; 32];
		let mut y = [0; 32];
		let mut ax = [0; 32];
		let mut bx = [0; 32];
		secure_random(&mut x);
		x25519_generate(&mut y, &x);
		x25519_agree(&mut ax, &x, &a);
		x25519_agree(&mut bx, &x, &b);
		let z = calculate_session_key(&ax, &bx, a, b, &y);

		let mut credentials = Vec::new();
		//Prefer UID to GID to llif to key.
		if let Some(uid) = sc.uid { credentials.push(KeyAclSubject::User(uid)); }
		if let Some(gid) = sc.gid { credentials.push(KeyAclSubject::Group(gid)); }
		if let Some(llif) = sc.llif { credentials.push(KeyAclSubject::LLIF(llif)); }
		credentials.push(KeyAclSubject::X25519Key(*b));
		let id = self.1;
		self.1 += 1;
		AssociationHandle(Arc::new(Association {
			id: id,
			verifier: MacVerifier::new(&z),
			client_key_a: *a,
			client_key_b: *b,
			auth_reply: y,
			subject: credentials.clone(),
			keys: RwLock::new(global_keys.restrict_for_subject(&credentials[..])),
			timeout: Mutex::new(Instant::now() + Duration::from_secs(90)),
			is_admin: global_keys.is_admin(&credentials[..]),
		}))
	}
}

fn create_key_low(ktype: u32) -> Result<Vec<u8>, String>
{
	let tag = TemporaryRandomLifetimeTag;
	let mut rng = TemporaryRandomStream::new_system(&tag);
	let (kformat, rawcontent) = match ktype {
		0 => generate_key(256, &mut rng),
		1 => generate_key(384, &mut rng),
		2 => generate_key(521, &mut rng),
		3 => generate_key(255, &mut rng),
		4 => generate_key(448, &mut rng),
		//5...7 => Err(format!("RSA keys not supported"))
		ktype => Err(format!("Unknown key type {ktype}"))
	}?;
	let mut out = Vec::new();
	match kformat {
		KcKeyFormat::Ecdsa => {
			let p = EcdsaKeyDecode::new(&rawcontent).set_err("Generated bad keypair representation")?;
			let crvname = p.crv.get_sexp_curve_name();
			let len = p.crv.get_component_bytes();
			write_ecdsa_key(&mut out, crvname, p.d, p.x, p.y, len).set_err("Error serializing key")?;
		},
		KcKeyFormat::Ed25519 => {
			let parse = Ed25519KeyDecode::new(&rawcontent).set_err("Error parsing generated key")?;
			write_eddsa_key(&mut out, "ed25519", &parse.private, &parse.public).
				set_err("Error serializing key")?;
		},
		KcKeyFormat::Ed448 => {
			let parse = Ed448KeyDecode::new(&rawcontent).set_err("Error parsing generated key")?;
			write_eddsa_key(&mut out, "ed448", &parse.private, &parse.public).
				set_err("Error serializing key")?;
		},
		f => return Err(format!("Unknown key format {f:?}"))
	};
	Ok(out)
}

fn generate_key(bits: usize, rng: &mut TemporaryRandomStream) -> Result<(KcKeyFormat, Vec<u8>), String>
{
	let pkeyerr = "Error generating private key";
	match bits {
		255 => {
			//Ed25519.
			let key = Ed25519KeyPair::keypair_generate((), rng).set_err(pkeyerr)?;
			return Ok((KcKeyFormat::Ed25519, key));
		}
		256 => {
			//NSA P-256.
			let key = EcdsaKeyPair::keypair_generate(EcdsaCurve::NsaP256, rng).set_err(pkeyerr)?;
			return Ok((KcKeyFormat::Ecdsa, key));
		},
		384 => {
			//NSA P-384.
			let key = EcdsaKeyPair::keypair_generate(EcdsaCurve::NsaP384, rng).set_err(pkeyerr)?;
			return Ok((KcKeyFormat::Ecdsa, key));
		},
		448 => {
			//Ed448.
			let key = Ed448KeyPair::keypair_generate((), rng).set_err(pkeyerr)?;
			return Ok((KcKeyFormat::Ed448, key));
		},
		521 => {
			//NSA P-521
			let key = EcdsaKeyPair::keypair_generate(EcdsaCurve::NsaP521, rng).set_err(pkeyerr)?;
			return Ok((KcKeyFormat::Ecdsa, key));
		},
		_ => {
			fail!(format!("ECC keys must be 255, 256, 384, 448 or 521 bits"));
		}
	}
}

fn write_integer_sexp<S:Sink>(target: &mut S, val: &[u8]) -> Result<(), ()> { write_padded(target, val, val.len()) }

fn write_padded<S:Sink>(sink: &mut S, data: &[u8], datalen: usize) -> Result<(), ()>
{
	let prefix = format!("{datalen}:");
	for i in prefix.as_bytes().iter() { dtry!(sink.write_u8(*i)); }
	for _ in data.len()..datalen { dtry!(sink.write_u8(0)); }
	if data.len() > datalen {
		dtry!(sink.write_slice(&data[data.len()-datalen..]));
	} else {
		dtry!(sink.write_slice(data));
	}
	Ok(())
}

fn write_ecdsa_key<S:Sink>(sink: &mut S, crv: &str, d: &[u8], x: &[u8], y: &[u8], bytes: usize) -> Result<(), ()>
{
	dtry!(sink.write_u8(40));
	write_integer_sexp(sink, "ecdsa".as_bytes())?;	//Not actually integer but close enough...
	write_integer_sexp(sink, crv.as_bytes())?;	//Not actually integer but close enough...
	write_padded(sink, d, bytes)?;
	write_padded(sink, x, bytes)?;
	write_padded(sink, y, bytes)?;
	dtry!(sink.write_u8(41));
	Ok(())
}

fn write_eddsa_key<S:Sink>(sink: &mut S, sub: &str, privkey: &[u8], pubkey: &[u8]) -> Result<(), ()>
{
	dtry!(sink.write_u8(40));
	write_integer_sexp(sink, sub.as_bytes())?;	//Not actually integer but close enough...
	write_integer_sexp(sink, privkey)?;		//Not actually integer but close enough...
	write_integer_sexp(sink, pubkey)?;		//Not actually integer but close enough...
	dtry!(sink.write_u8(41));
	Ok(())
}
