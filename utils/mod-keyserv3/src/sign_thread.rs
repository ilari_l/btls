use ::association::CReply;
use ::association::Query;
use ::client_association::AssociationMap;
use ::client_association::AuthKey;
use ::unix::DatagramSocket;
use ::unix::PacketAddress;
use ::unix::ReceivedMessage;
use btls_aux_collections::Arc;
use btls_aux_collections::Mutex;
use btls_aux_collections::Once;
use btls_aux_keypair_local_module_definitions::SignatureReply;
use btls_aux_memory::EscapeByteString;
use btls_aux_unix::FileDescriptor;
use btls_aux_unix::FileDescriptorB;
use btls_aux_unix::PipeFlags;
use btls_aux_unix::PollFd;
use btls_aux_unix::PollFlags;
use btls_aux_unix::SocketFamily;
use std::borrow::Cow;
use std::collections::BTreeMap;
use std::collections::HashMap;
use std::collections::VecDeque;
use std::mem::zeroed;
use std::ops::Deref;
use std::ops::DerefMut;
use std::ptr::null_mut;
use std::sync::atomic::AtomicPtr;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering;
use std::thread::Builder as ThreadBuilder;
use std::time::Instant;
use std::time::Duration;

pub struct SignatureQueueEntry
{
	pub pkhash: [u8;32],
	pub reply: SignatureReply,
	pub algorithm: u16,
	pub data: Vec<u8>,
	pub socket: Arc<PacketAddress>,
	pub seqpacket: bool,
	pub auth_a: Arc<AuthKey>,
	pub auth_b: Arc<AuthKey>,
	pub kname: Arc<String>,
}

struct QueuedEntry
{
	q: SignatureQueueEntry,
	deadline: Instant,
	udeadline: Instant,
	pending_assoc: Option<u64>,
	pending_seq: Option<usize>,
	pending_login: Option<usize>,
}

struct SignatureQueue
{
	queue: Mutex<VecDeque<SignatureQueueEntry>>,
}

static QUEUE: AtomicPtr<SignatureQueue> = AtomicPtr::new(null_mut());

fn try_print_error(q: &VecDeque<QueuedEntry>, msg: &str) { if let Some(e) = q.front() { e.q.reply.log(msg); } }

fn try_print_error_from(src: &PacketAddress, q: &VecDeque<QueuedEntry>, msg: &str)
{
	for e in q.iter() {
		if e.q.socket.deref() == src { e.q.reply.log(&format!("From {src}: {msg}")); }
	}
}

fn try_print_error_assoc(src: &PacketAddress, aid: u64, q: &VecDeque<QueuedEntry>, msg: &str)
{
	for e in q.iter() {
		if e.q.socket.deref() == src && e.pending_assoc == Some(aid) {
			e.q.reply.log(&format!("From {src} association {aid}: {msg}"));
		}
	}
}

fn add_poll(polls: &mut Vec<PollFd>, fdcount: &mut usize, fd: FileDescriptorB)
{
	if *fdcount == polls.len() { polls.push(unsafe{zeroed()}); }
	polls[*fdcount].fd = fd;
	polls[*fdcount].events = PollFlags::IN;
	polls[*fdcount].revents = Default::default();
	*fdcount += 1;
}

fn populate_polls(polls: &mut Vec<PollFd>, sigpipe: &FileDescriptor,
	sfds: &BTreeMap<FileDescriptorB, DatagramSocket>, queue: &VecDeque<QueuedEntry>) -> (usize, i32)
{
	let mut fdcount = 0;
	add_poll(polls, &mut fdcount, sigpipe.as_fdb());
	for (fd, _) in sfds.iter() { add_poll(polls, &mut fdcount, *fd); }
	let now = Instant::now();
	let maxwait = queue.front().map(|ent|ent.deadline.saturating_duration_since(now)).
		map(|x|(x.as_secs() * 1000 + (x.subsec_nanos() as u64 + 999999) / 1000000) as i32).unwrap_or(-1);

	(fdcount, maxwait)
}

fn add_to_queue(sigpipe: &FileDescriptor, queue: &mut VecDeque<QueuedEntry>)
{
	let mut tmp = [0;512];
	sigpipe.read(&mut tmp).ok();
	//Got new entries, add them to the internal queue.
	//add_to_queue() is only called in signature thread, so this is guaranteed to be non-NULL.
	let mut guard = unsafe{(*QUEUE.load(Ordering::Relaxed)).queue.lock()};
	while let Some(entry) = guard.deref_mut().pop_front() {
		//Push new entries on front, not back so they send immediately.
		queue.push_front(QueuedEntry{
			q: entry,
			deadline: Instant::now(),
			udeadline: Instant::now() + Duration::from_secs(15),
			pending_assoc: None,
			pending_seq: None,
			pending_login: None,
		});
	}
}

fn extract_request(queue: &mut VecDeque<QueuedEntry>, src: &PacketAddress, aid: u64, seq: usize) ->
	Option<QueuedEntry>
{
	let mut index = queue.len();
	for (idx, e) in queue.iter_mut().enumerate() {
		if e.q.socket.deref() != src || e.pending_assoc != Some(aid)
			|| e.pending_seq != Some(seq) {
			continue;
		}
		index = idx
	}
	//If we found an request, extract it.
	if index < queue.len() { queue.remove(index) } else { None }
}

fn handle_queue_timeout(queue: &mut VecDeque<QueuedEntry>, sfds: &mut BTreeMap<FileDescriptorB, DatagramSocket>,
	sfdk: &mut BTreeMap<SocketFamily, FileDescriptorB>, sfdp: &mut HashMap<PacketAddress, FileDescriptorB>,
	assoc: &mut AssociationMap)
{
	let mut qent = f_return!(queue.pop_front());
	//If ultimate deadline was exceeded, consider signing failed. This is a backstop in case one does not
	//get coherent errors from daemon.
	if Instant::now() > qent.udeadline {
		qent.q.reply.log(&format!("Signature timed out"));
		qent.q.reply.finish(None);
		return;
	}
	let rsockfd = if qent.q.seqpacket {
		if !sfdp.contains_key(&qent.q.socket) {
			//There is not yet socket for it, try registering.
			match DatagramSocket::connect_seqpkt(&qent.q.socket) {
				Ok(sock) => {
					let fd = sock.borrow_fd().as_fdb();
					sfdp.insert(qent.q.socket.deref().clone(), fd);
					sfds.insert(fd, sock);
				},
				Err(err) => qent.q.reply.log(&format!("Error creating socket: {err}")),
			};
		}
		sfdp.get(&qent.q.socket)
	} else if let Some(family) = qent.q.socket.family() {
		if !sfdk.contains_key(&family) {
			//There is not yet socket for it, try registering.
			match DatagramSocket::unconnected(family) {
				Ok(sock) => {
					let fd = sock.borrow_fd().as_fdb();
					sfdk.insert(family, fd);
					sfds.insert(fd, sock);
				},
				Err(err) => qent.q.reply.log(&format!("Error creating socket: {err}")),
			};
		}
		sfdk.get(&family)
	} else {
		None
	};
	//The correct socket better exist now.
	let sock = match rsockfd.and_then(|fd|sfds.get(fd)) {
		Some(x) => x,
		None => {
			//Nope, does not exist. Declare the signing failed.
			qent.q.reply.log(&format!("Can not contact key daemon {d}", d=qent.q.socket));
			qent.q.reply.finish(None);
			return;
		}
	};

	//Look up the association if it exists. If it does, send a signing message. If not, send a login message.
	match {
		let reply = &qent.q.reply;
		let smsg = Query::Sign(qent.q.pkhash, qent.q.algorithm, Cow::Borrowed(&qent.q.data));
		assoc.send_message(sock, qent.q.socket.clone(), qent.q.auth_b.clone(), smsg, |m|reply.log(m))
	} {
		Some((aid, seq)) => {
			qent.pending_assoc = Some(aid);
			qent.pending_seq = Some(seq);
		},
		None => {
			let reply = &qent.q.reply;
			let txid = assoc.send_setup(sock, qent.q.socket.clone(), qent.q.auth_a.clone(),
				qent.q.auth_b.clone(), |m|reply.log(m));
			qent.pending_login = Some(txid);
		}
	}

	//Queue back. The instants are supposed to be monotonic, so the new entry is always last.
	qent.deadline = Instant::now() + Duration::from_millis(50);
	queue.push_back(qent);
}

fn handle_socket_receive(msg: ReceivedMessage, queue: &mut VecDeque<QueuedEntry>, assoc: &mut AssociationMap)
{
	//If socket is seqpacket mode, src_addr is reflected, so it will be correct.
	match CReply::parse(&msg.payload) {
		CReply::Empty => try_print_error_from(&msg.src_addr, queue, "Malformed message: Empty"),
		CReply::Truncated(code) => try_print_error_from(&msg.src_addr, queue,
			&format!("Malformed message: Trunated code {code}")),
		CReply::BadCommand(aid, cmd) => try_print_error_assoc(&msg.src_addr, aid, queue,
			&format!("Unsupported command {cmd}")),
		CReply::UnknownKeyAgreement(meth) => try_print_error_from(&msg.src_addr, queue,
			&format!("Unsupported key agreement {meth}")),
		CReply::BadKeyIndex(aid, kid) => try_print_error_assoc(&msg.src_addr, aid, queue,
			&format!("Unknown key index {kid}")),
		CReply::RevokedKeyIndex(aid, kid) => try_print_error_assoc(&msg.src_addr, aid, queue,
			&format!("Revoked key index {kid}")),
		CReply::PublicKey(aid, kid, _, _, _) => try_print_error_assoc(&msg.src_addr, aid, queue,
			&format!("Unexpected public key for index {kid}")),
		CReply::TagReport(aid, kid, _) => try_print_error_assoc(&msg.src_addr, aid, queue,
			&format!("Unexpected tag report for index {kid}")),
		CReply::UnknownResponse(code) => try_print_error_from(&msg.src_addr, queue,
			&format!("Malformed message: Unknown code {code}")),
		CReply::BadKeyName(aid, ref name) => try_print_error_assoc(&msg.src_addr, aid, queue,
			&format!("Unexpected Bad Key Name {name}", name=EscapeByteString(name.deref()))),
		CReply::InternalError(aid, ref err) => try_print_error_assoc(&msg.src_addr, aid, queue,
			&format!("Internal error: {err}")),
		CReply::Ack(aid, _) => try_print_error_assoc(&msg.src_addr, aid, queue,
			&format!("Malformed message: Unexpected ACK")),
		//Clear the association. Note that MAC failures also clear the association.
		CReply::BadAssociation(aid) => assoc.process_unknown_association(Arc::new(msg.src_addr.clone()),
			aid),
		CReply::BadMac(aid, _) => assoc.process_unknown_association(Arc::new(msg.src_addr.clone()), aid),
		//Process the authentication reply.
		CReply::AuthReply(aid, txid, _, challenge) => {
			let mut index = queue.len();
			for (idx, e) in queue.iter_mut().enumerate() {
				if e.q.socket.deref() != &msg.src_addr || e.pending_login != Some(txid) {
					continue;
				}
				//Ok, this login is for this entry.
				assoc.process_auth_m0(Arc::new(msg.src_addr.clone()), e.q.auth_a.clone(),
					e.q.auth_b.clone(), aid, challenge);
				e.pending_login = None;
				e.pending_assoc = Some(aid);
				index = idx;
			}
			//If we found an association, immediately timeout it, so we will send.
			if let Some(mut e) = queue.remove(index) {
				e.deadline = Instant::now();
				queue.push_front(e);
			}
		},
		CReply::SignUnauthorized(aid, seq) => {
			if let Some(e) = extract_request(queue, &msg.src_addr, aid, seq) {
				e.q.reply.log(&format!("Key {k}: Signature unauthorized", k=e.q.kname));
				e.q.reply.finish(None);
			}
		},
		CReply::AdminUnauthorized(aid, seq) => {
			if let Some(e) = extract_request(queue, &msg.src_addr, aid, seq) {
				e.q.reply.log(&format!("Key {k}: Admin request unauthorized", k=e.q.kname));
				e.q.reply.finish(None);
			}
		},
		CReply::SignFailed(aid, seq) => {
			if let Some(e) = extract_request(queue, &msg.src_addr, aid, seq) {
				e.q.reply.log(&format!("Key {k}: Internal server error", k=e.q.kname));
				e.q.reply.finish(None);
			}
		},
		CReply::UnknownSequence(aid, seq, code) => {
			if let Some(e) = extract_request(queue, &msg.src_addr, aid, seq) {
				e.q.reply.log(&format!("Key {k}: Unknown code {code}", k=e.q.kname));
				e.q.reply.finish(None);
			}
		},
		CReply::BadKey(aid, seq) => {
			if let Some(e) = extract_request(queue, &msg.src_addr, aid, seq) {
				e.q.reply.log(&format!("Key {k}: Unknown key identifier", k=e.q.kname));
				e.q.reply.finish(None);
			}
		},
		CReply::Signed(aid, seq, sig) => {
			if let Some(e) = extract_request(queue, &msg.src_addr, aid, seq) {
				e.q.reply.finish(Some(&sig));
			}
		},
	}
}

fn run_signature_thread(sigpipe: FileDescriptor)
{
	let mut assoc = AssociationMap::new();
	let mut queue: VecDeque<QueuedEntry> = VecDeque::new();
	//There can be up to 3 daemon comm. fds.
	let mut polls: Vec<PollFd> = Vec::with_capacity(4);
	let mut sfds: BTreeMap<FileDescriptorB, DatagramSocket> = BTreeMap::new();
	let mut sfdk: BTreeMap<SocketFamily, FileDescriptorB> = BTreeMap::new();
	let mut sfdp: HashMap<PacketAddress, FileDescriptorB> = HashMap::new();
	let mut fdkill = Vec::new();
	loop {
		let (fdcount, maxwait) = populate_polls(&mut polls, &sigpipe, &sfds, &queue);
		polls.truncate(fdcount);	//Discard any old FDs.
		unsafe{PollFd::poll(&mut polls, maxwait).ok()};
		//If we were waked for events in queue, add them to internal queue.
		if polls[0].revents.is_in() { add_to_queue(&sigpipe, &mut queue); }
		for i in 1..fdcount {  if let Some(sock) = sfds.get(&polls[i].fd) {
			//Also kill the socket if it gets invalidated. This seems to happen with SEQPACKET mode
			//sockets when the other end disconnects.
			if polls[i].revents.is_hup() || polls[i].revents.is_invalid() {
				let addr = sock.get_seqpkt_name();
				let fd = sfdp.remove(&addr);
				if let Some(fd) = fd { fdkill.push(fd); }
				continue;
			}
			if !polls[i].revents.is_in() { continue; }	//Not ready.
			//Received something on the socket from key daemon!
			match sock.receive_raw() {
				Ok(Some(x)) => handle_socket_receive(x, &mut queue, &mut assoc),
				Ok(None) => (),		//Nothing there.
				Err(err) => try_print_error(&queue,
					&format!("Error receiving from socket: {err}")),
			}
		}}
		//Kill all the file descriptors that are no longer needed. Do this before handling queue timeout,
		//so that queue timeout does not resurrect dead sockets, causing them to get polled, causing 100%
		//CPU usage.
		for fd in fdkill.drain(..) { sfds.remove(&fd); }
		while queue.front().map(|ent|ent.deadline <= Instant::now()).unwrap_or(false) {
			handle_queue_timeout(&mut queue, &mut sfds, &mut sfdk, &mut sfdp, &mut assoc);
		}
	}
}

fn get_sigfd() -> &'static AtomicUsize
{
	static F: AtomicUsize = AtomicUsize::new(0);
	&F
}

fn get_thread_once() -> &'static Once
{
	static X: Once = Once::new();
	&X
}

fn __start_sig_thread() -> Result<(), String>
{
	let f = get_sigfd();
	let sigfd = f.load(Ordering::Relaxed);
	if sigfd > 0 { return Ok(()); }	//Done already.

	//Create a signal pipe. We can not show error.
	let flags = PipeFlags::CLOEXEC;
	let (rsigpipe, wsigpipe) = FileDescriptor::pipe2(flags).map_err(|err|format!("Failed to create pipe: {err}"))?;
	//Do not initialize twice. This is not supposed to be called in racy way, so TOCTTOU does not
	//matter.
	if QUEUE.load(Ordering::Acquire).is_null() {
		QUEUE.store(Box::into_raw(Box::new(SignatureQueue {
			queue: Mutex::new(VecDeque::new()),
		})), Ordering::Release)
	}
	ThreadBuilder::new().name(format!("signature-i/o")).spawn(move ||{
		run_signature_thread(rsigpipe);
	}).map_err(|err|format!("Failed to create thread: {err}"))?;
	//Transfer ownership.
	f.store(wsigpipe.into_raw_fd() as usize + 1, Ordering::Relaxed);
	//No need to burn get_thread_once(), because sigfd being nonzero will block creation of second thread.
	Ok(())
}

pub fn try_create_sig_thread()
{
	//We can not show error. But if this fails, then run_create_thread_with_sqe() will run again, and
	//presumably show an error.
	__start_sig_thread().ok();
}

fn run_create_thread_with_sqe(s: &SignatureQueueEntry) -> usize
{
	let f = get_sigfd();
	let x = get_thread_once();
	//If thread already exists, skip creating it.
	let sigfd = f.load(Ordering::Relaxed);
	if sigfd > 0 { return sigfd; }
	x.call_once(||{
		//Show error, if any.
		if let Err(e) = __start_sig_thread() { return s.reply.log(&e); }
	});
	let sigfd = f.load(Ordering::Relaxed);
	sigfd
}

pub fn queue_signature(s: SignatureQueueEntry) -> Result<(), SignatureQueueEntry>
{
	let sigfd = run_create_thread_with_sqe(&s);
	if sigfd == 0 { return Err(s); }	//Failed.
	unsafe {
		//Because run_create_thread_with_sqe() completed successfully (sine sigfd > 0), QUEUE has to be
		//non-NULL.
		(*QUEUE.load(Ordering::Relaxed)).queue.lock().deref_mut().push_back(s);
		//Wake up the task.
		let buf = [0u8;1];
		FileDescriptorB::new(sigfd as i32 - 1).write(&buf).ok();
	}
	Ok(())
}
