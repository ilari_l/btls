#![warn(unsafe_op_in_unsafe_fn)]
extern crate btls_aux_hash;
extern crate btls_aux_memory;
extern crate btls_aux_unix;
extern crate btls_aux_xed25519;
use btls_aux_hash::sha256;
use btls_aux_memory::Hexdump;
use btls_aux_unix::Umask;
use btls_aux_xed25519::x25519_generate;
use std::env::args;
use std::fs::File;
use std::io::Read;
use std::process::exit;



//Main function.
fn main2(argv: &[String]) -> i32
{
	if argv.len() != 2 {
		println!("Syntax: {arg0} <auth>", arg0=argv[0]);
		return 1;
	}
	let mut authdata = Vec::new();
	let authfile = &argv[1];
	match File::open(authfile).and_then(|mut x|x.read_to_end(&mut authdata)) {
		Ok(_) => {
			let private = sha256(&authdata);
			let mut public = [0u8; 32];
			x25519_generate(&mut public, &private);
			println!("x25519:{public}", public=Hexdump(&public));
			return 0;
		},
		Err(err) => {
			println!("Unable to read {authfile}: {err}");
			return 1;
		}
	}
}

fn main()
{
	Umask::PUBLIC.swap();		//Write world-readable files.
	let mut x = Vec::new();
	for i in args() { x.push(i); }
	exit(main2(&x));
}
