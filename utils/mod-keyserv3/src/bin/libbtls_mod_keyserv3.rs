#![warn(unsafe_op_in_unsafe_fn)]
extern crate btls_aux_fail;
extern crate btls_aux_hash;
extern crate btls_aux_memory;
extern crate btls_aux_random;
extern crate btls_aux_unix;
extern crate btls_aux_xed25519;
extern crate btls_mod_keyserv3;
use btls_aux_fail::f_return;
use btls_aux_hash::sha256;
use btls_aux_memory::EscapeByteString;
use btls_aux_memory::Hexdump;
use btls_aux_random::secure_random;
use btls_aux_unix::Umask;
use btls_aux_xed25519::x25519_generate;
use btls_mod_keyserv3::AssociationMap;
use btls_mod_keyserv3::AuthKey;
use btls_mod_keyserv3::CReply;
use btls_mod_keyserv3::DatagramSocket;
use btls_mod_keyserv3::PacketAddress;
use btls_mod_keyserv3::Query;
use btls_mod_keyserv3::ReceivedMessage;
use btls_mod_keyserv3::translate_special_path;
use std::borrow::Cow;
use std::collections::BTreeSet;
use std::env::args;
use std::fmt::Write as FmtWrite;
use std::fs::File;
use std::fs::read_link;
use std::io::Read;
use std::io::stdout;
use std::io::Write;
use std::ops::Deref;
use std::path::Path;
use std::path::PathBuf;
use std::process::exit;
use std::str::FromStr;
use std::sync::Arc;


fn generate_random_auth() -> Arc<AuthKey>
{
	let mut xa = [0;32];
	let mut ya = [0;32];
	secure_random(&mut xa);
	x25519_generate(&mut ya, &xa);
	Arc::new(AuthKey{public: ya, private: xa})
}

fn get_executable_path(argv0: &str) -> PathBuf
{
	read_link(Path::new("/proc/self/exe")).unwrap_or_else(|_|Path::new(argv0).to_path_buf())
}

fn dump_key(argv0: &str, rsock: &str, rauth: Option<&str>, pfx: &str, _name: &str, spki: &[u8],
	methodlist: &[u16]) -> Result<(), String>
{
	//Sanitize the name.
	let mut name = String::new();
	for c in _name.chars() {
		let d = c as u32;
		if match d {
			64..=90 => true,	//@ and uppercase letters.
			97..=122 => true,	//lowercase letters.
			48..=57 => true,	//Numbers.
			45|95 => true,		//Dash, underscore.
			160..=0x10FFFD => true,	//High unicode.
			_ => false,
		} {
			name.push(c);
		} else {
			name.push_str(&format!("%{{{d:x}}}"));
		}
	}

	dump_low(Path::new(&format!("{pfx}.{name}.key")), argv0, &name, rauth, rsock, spki, methodlist)
}

fn __dump_low(argv0: &str, name: &str, rauth: Option<&str>, rsock: &str, spki: &[u8],
	methodlist: &[u16]) -> Result<String, String>
{
	let mut keydump = String::new();
	let canonpath = get_executable_path(argv0).canonicalize().
		map_err(|err|format!("Can not canonicalize module path: {err}"))?;
	//The module name.
	writeln!(keydump, "{p}.so", p=canonpath.display()).unwrap();
	writeln!(keydump, "{rsock}").unwrap();
	writeln!(keydump, "{name}").unwrap();
	let mut methods = String::new();
	for method in methodlist.iter() {
		if methods.len() > 0 { methods.push_str(" "); }
		write!(methods, "{method}").unwrap();
	}
	writeln!(keydump, "{methods}").unwrap();
	writeln!(keydump, "{spki}", spki=Hexdump(spki)).unwrap();
	if let Some(rauth) = rauth {
		//If rauth starts with a ?, it is a special path, which should not be canonicalized, but written
		//raw into file.
		if rauth.starts_with("?") {
			writeln!(keydump, "{rauth}").unwrap();
		} else {
			let acanonpath = Path::new(rauth).canonicalize().
				map_err(|err|format!("Can not canonicalize auth file path: {err}"))?;
			writeln!(keydump, "{p}", p=acanonpath.display()).unwrap();
		}
	}
	Ok(keydump)
}

fn dump_low(kf: &Path, argv0: &str, name: &str, rauth: Option<&str>, rsock: &str, spki: &[u8],
	methodlist: &[u16]) -> Result<(), String>
{
	let keydump = __dump_low(argv0, name, rauth, rsock, spki, methodlist)?;
	File::create(kf).and_then(|mut x|x.write_all(keydump.as_bytes())).
		map_err(|err|format!("Can not create '{kf}': {err}", kf=kf.display()))?;
	eprintln!("Dumped key '{name}'");
	Ok(())
}

fn raw_receive(sock: &DatagramSocket, addr: &PacketAddress) -> Result<Option<ReceivedMessage>, ()>
{
	match sock.receive_raw() {
		Ok(Some(p)) => {
			if &p.src_addr != addr.deref() {
				eprintln!("Received packet from bad source {src_addr} (expected {addr})",
					src_addr=p.src_addr);
				Ok(None)	//Wrong address.
			} else {
				Ok(Some(p))
			}
		},
		Ok(None) => Ok(None),
		Err(err) => {
			eprintln!("Error receiving from {addr}: {err}");
			Err(())
		}
	}
}

fn connect_socket(rsock: &str, auth_a: Arc<AuthKey>, auth_b: Arc<AuthKey>) ->
	Result<(Arc<PacketAddress>, DatagramSocket, AssociationMap, u64, usize), i32>
{
	let suffix = rsock.strip_prefix("?").unwrap_or(rsock);
	let addr = PacketAddress::parse(suffix).map_err(|err|{
		eprintln!("Unable to parse {rsock}: {err}");
		1
	})?;
	let addr = Arc::new(addr);
	let sockret = if rsock.starts_with("?") {
		DatagramSocket::connect_seqpkt(addr.deref())
	} else {
		let family = addr.family().ok_or_else(||{
			eprintln!("{suffix}: Unsupported socket family");
			1
		})?;
		DatagramSocket::unconnected(family)
	};
	let sock = sockret.map_err(|err|{eprintln!("Unable to create socket {addr}: {err}"); 1})?;
	//First, send authentication request.
	let mut assoc = AssociationMap::new();
	let mut err = false;
	assoc.send_setup(&sock, addr.clone(), auth_a.clone(), auth_b.clone(), |msg|{
		eprintln!("Error sending auth request: {msg}");
		err = true;
	});
	if err { return Err(1); }
	//Listen for response.
	let keys_count;
	let assid;
	loop { match raw_receive(&sock, &addr) {
		Ok(Some(p)) => match CReply::parse(&p.payload) {
			CReply::AuthReply(aid, _, kcount, challenge) => {
				assoc.process_auth_m0(Arc::new(p.src_addr.clone()), auth_a.clone(),
					auth_b.clone(), aid, challenge);
				keys_count = kcount;
				assid = aid;
				break;
			},
			_ => {
				eprintln!("Unknown reply from {addr} trying to authenticate");
				return Err(1);
			}
		},
		Ok(None) => (),
		Err(_) => return Err(1)
	}}
	Ok((addr, sock, assoc, assid, keys_count))
}

fn do_dump_recv_loop(addr: &PacketAddress, sock: &DatagramSocket, assid: u64, kidx: usize) -> Result<bool, i32>
{
	let status;
	loop { match raw_receive(sock, addr) {
		Ok(Some(p)) => match CReply::parse(&p.payload) {
			CReply::TagReport(aid, kid, rpt) => {
				if aid != assid { continue; }	//Wrong association.
				if kid != kidx { continue; }	//Wrong key.
				status = rpt;
				break;
			},
			CReply::BadCommand(aid, 3) => {
				if aid != assid { continue; }	//Wrong association.
				//Tag filtering not supported.
				eprintln!("Tag fitlering not suported");
				status = true;
				break;
			},
			//Ignore any revoked keys.
			CReply::RevokedKeyIndex(_, _) => {
				continue;
			},
			_ => {
				eprintln!("Unknown reply from {addr} trying to check tag");
				return Err(1);
			}
		},
		Ok(None) => (),
		Err(_) => return Err(1)
	}}
	Ok(status)
}

fn do_dump(argv0: &str, rsock: &str, rauth: Option<&str>, pfx: &str, auth_a: Arc<AuthKey>, auth_b: Arc<AuthKey>,
	tag: Option<&str>) -> i32
{
	let mut err = false;
	let (addr, sock, mut assoc, assid, mut keys_count) =
		f_return!(connect_socket(rsock, auth_a, auth_b.clone()), F[|e|e]);
	if keys_count == 0 {
		eprintln!("No keys to dump from {addr}");
		return 1;
	}
	let mut kidxs = BTreeSet::new();
	for kidx in 0..keys_count {
		if let Some(tag) = tag {
			//Check tag.
			let cmd = Query::HasTag(kidx, Cow::Borrowed(tag.as_bytes()));
			assoc.send_message(&sock, addr.clone(), auth_b.clone(), cmd, |msg|{
				eprintln!("Error sending key tag request: {msg}");
				err = true;
			});
			match do_dump_recv_loop(&addr, &sock, assid, kidx) {
				Ok(false) => { keys_count = keys_count - 1; },
				Ok(true) => {kidxs.insert(kidx);},
				Err(e) => return e,
			};
		} else {
			kidxs.insert(kidx);
		}
	}
	for kidx in kidxs.iter() {
		//Send command to dump the key.
		let cmd = Query::DumpKey(*kidx);
		assoc.send_message(&sock, addr.clone(), auth_b.clone(), cmd, |msg|{
			eprintln!("Error sending key dump request: {msg}");
			err = true;
		});
	}
	if keys_count == 0 {
		eprintln!("No keys match given tag from {addr}");
		return 1;
	}
	if err { return 1; }
	let mut dumped = BTreeSet::new();
	while dumped.len() < keys_count { match raw_receive(&sock, &addr) {
		Ok(Some(p)) => match CReply::parse(&p.payload) {
			CReply::PublicKey(aid, kidx, name, spki, methodlist) => {
				if aid != assid { continue; }	//Wrong association.
				if dumped.contains(&kidx) { continue; }	//Already done.
				if let Err(err) = dump_key(argv0, rsock, rauth, pfx, name.deref(),
					spki.deref(), methodlist.deref()) {
					eprintln!("Error dumping {pfx}.{name}: {err}");
					return 1;
				}
				dumped.insert(kidx);
			},
			//Ignore any revoked keys.
			CReply::RevokedKeyIndex(_, _) => {
				continue;
			},
			_ => {
				eprintln!("Unknown reply from {addr} trying to dump key");
				return 1;
			}
		},
		Ok(None) => (),
		Err(_) => return 1
	}}
	//Ok
	0
}

fn generate_admin_auths(argv: &[String]) -> Result<(Arc<AuthKey>, Arc<AuthKey>, Option<String>), i32>
{
	let auth_a = generate_random_auth();
	let mut auth_b = generate_random_auth();
	let mut rauth = None;
	//If authentication file exists, load it.
	for arg in argv.iter() { if let Some(authfile) = arg.strip_prefix("--auth=") {
		let mut authdata = Vec::new();
		//The rauth is authfile before translation, so translation can be repeated on access (it might not
		//be the same).
		rauth = Some(authfile.to_owned());
		let authfile = match translate_special_path(authfile) {
			Ok(name) => name,
			Err(err) => {
				eprintln!("Unable to expand filename {authfile}: {err}");
				return Err(1);
			},
		};
		match File::open(authfile.deref()).and_then(|mut x|x.read_to_end(&mut authdata)) {
			Ok(_) => {
				let private = sha256(&authdata);
				let mut public = [0u8; 32];
				x25519_generate(&mut public, &private);
				//Ok, assign new key to slot b.
				auth_b = Arc::new(AuthKey{
					private: private,
					public: public,
				});
			},
			Err(err) => {
				eprintln!("Unable to read {authfile}: {err}");
				return Err(1);
			}
		}
	}}
	Ok((auth_a, auth_b, rauth))
}

fn get_1_arg(argv: &[String]) -> Result<String, i32>
{
	let mut a = None;
	for arg in argv.iter() { if !arg.starts_with("--") {
		if a.is_none() { a = Some(arg.clone()); }
		else {
			eprintln!("Excess arguments");
			return Err(1);
		}
	}}
	let a = a.ok_or_else(||{eprintln!("Missing arguments");1})?;
	Ok(a)
}

fn get_2_arg(argv: &[String]) -> Result<(String, String), i32>
{
	let mut a = None;
	let mut b = None;
	for arg in argv.iter() { if !arg.starts_with("--") {
		if a.is_none() { a = Some(arg.clone()); }
		else if b.is_none() { b = Some(arg.clone()); }
		else {
			eprintln!("Excess arguments");
			return Err(1);
		}
	}}
	let a = a.ok_or_else(||{eprintln!("Missing arguments");1})?;
	let b = b.ok_or_else(||{eprintln!("Missing arguments");1})?;
	Ok((a,b))
}

fn do_admin_dump_op(argv0: &str, name: &str, mut assoc: AssociationMap, sock: DatagramSocket,
	addr: Arc<PacketAddress>, auth_b: Arc<AuthKey>, assid: u64, rauth: Option<&str>, rsock: &str) ->
	Result<(), i32>
{
	let cmd = Query::ADumpKey(Cow::Borrowed(name.as_bytes()));
	let mut err = false;
	assoc.send_message(&sock, addr.clone(), auth_b.clone(), cmd, |msg|{
		eprintln!("Error sending admin key dump: {msg}");
		err = true;
	});
	if err { return Err(1); }

	loop { match raw_receive(&sock, &addr) {
		Ok(Some(p)) => match CReply::parse(&p.payload) {
			CReply::PublicKey(aid, _, _, pubkey, schemes) => {
				if aid != assid { continue; }	//Wrong association.
				if let Err(err) = dump_low(Path::new(&format!("{name}.key")), argv0,
					name, rauth, rsock, pubkey.deref(), schemes.deref()) {
					eprintln!("Error dumping key: {err}");
					return Err(1);
				}
				break;
			},
			CReply::AdminUnauthorized(aid, _) => {
				if aid != assid { continue; }	//Wrong association.
				eprintln!("Admin permission denied");
				return Err(1);
			},
			CReply::BadKeyName(aid, name) => {
				if aid != assid { continue; }	//Wrong association.
				eprintln!("Unknown key {name}", name=EscapeByteString(&name));
				return Err(1);
			},
			CReply::InternalError(aid, err) => {
				if aid != assid { continue; }	//Wrong association.
				eprintln!("Internal error: {err}");
				return Err(1);
			},
			_ => {
				eprintln!("Unknown reply from {addr} trying to list keys");
				return Err(1);
			}
		},
		Ok(None) => (),
		Err(_) => return Err(1)
	}}
	Ok(())
}

fn do_admin_create_key(argv0: &str, argv: &[String]) -> Result<(), i32>
{
	let (auth_a, auth_b, rauth) = generate_admin_auths(argv)?;
	let (arg1, arg2) = get_2_arg(argv)?;
	let (addr, sock, mut assoc, assid, _) = connect_socket(&arg1, auth_a, auth_b.clone())?;
	let mut ktype = 0;
	let mut profile = 0;
	for arg in argv.iter() {
		if let Some(ktype2) = arg.strip_prefix("--type=") {
			ktype = u32::from_str(ktype2).map_err(|_|{eprintln!("Bad type");1})?;
		}
		if let Some(profile2) = arg.strip_prefix("--profile=") {
			profile = u32::from_str(profile2).map_err(|_|{eprintln!("Bad profile");1})?;
		}
	}
	let cmd = Query::ACreateKey(Cow::Borrowed(arg2.as_bytes()), ktype, profile);
	let mut err = false;
	assoc.send_message(&sock, addr.clone(), auth_b.clone(), cmd, |msg|{
		eprintln!("Error sending admin key create: {msg}");
		err = true;
	});
	if err { return Err(1); }
	loop { match raw_receive(&sock, &addr) {
		Ok(Some(p)) => match CReply::parse(&p.payload) {
			CReply::Ack(aid, _) => {
				if aid != assid { continue; }	//Wrong association.
				break;
			},
			CReply::AdminUnauthorized(aid, _) => {
				if aid != assid { continue; }	//Wrong association.
				eprintln!("Admin permission denied");
				return Err(1);
			},
			CReply::BadKeyName(aid, name) => {
				if aid != assid { continue; }	//Wrong association.
				eprintln!("Bad key {name}", name=EscapeByteString(&name));
				return Err(1);
			},
			CReply::InternalError(aid, err) => {
				if aid != assid { continue; }	//Wrong association.
				eprintln!("Internal error: {err}");
				return Err(1);
			},
			_ => {
				eprintln!("Unknown reply from {addr} trying to delete key");
				return Err(1);
			}
		},
		Ok(None) => (),
		Err(_) => return Err(1)
	}}
	do_admin_dump_op(argv0, &arg2, assoc, sock, addr, auth_b, assid, rauth.as_deref(), &arg1)
}

fn do_admin_delete_key(argv: &[String]) -> Result<(), i32>
{
	let (auth_a, auth_b, _) = generate_admin_auths(argv)?;
	let (arg1, arg2) = get_2_arg(argv)?;
	let (addr, sock, mut assoc, assid, _) = connect_socket(&arg1, auth_a, auth_b.clone())?;
	let cmd = Query::ADeleteKey(Cow::Borrowed(arg2.as_bytes()));
	let mut err = false;
	assoc.send_message(&sock, addr.clone(), auth_b.clone(), cmd, |msg|{
		eprintln!("Error sending admin key delete: {msg}");
		err = true;
	});
	if err { return Err(1); }
	loop { match raw_receive(&sock, &addr) {
		Ok(Some(p)) => match CReply::parse(&p.payload) {
			CReply::Ack(aid, _) => {
				if aid != assid { continue; }	//Wrong association.
				eprintln!("Deleted key {arg2}");
				break;
			},
			CReply::AdminUnauthorized(aid, _) => {
				if aid != assid { continue; }	//Wrong association.
				eprintln!("Admin permission denied");
				return Err(1);
			},
			CReply::BadKeyName(aid, name) => {
				if aid != assid { continue; }	//Wrong association.
				eprintln!("Unknown key {name}", name=EscapeByteString(&name));
				return Err(1);
			},
			CReply::InternalError(aid, err) => {
				if aid != assid { continue; }	//Wrong association.
				eprintln!("Internal error: {err}");
				return Err(1);
			},
			_ => {
				eprintln!("Unknown reply from {addr} trying to delete key");
				return Err(1);
			}
		},
		Ok(None) => (),
		Err(_) => return Err(1)
	}}
	Ok(())
}

fn do_admin_dump_key(argv0: &str, argv: &[String]) -> Result<(), i32>
{
	let (auth_a, auth_b, rauth) = generate_admin_auths(argv)?;
	let (arg1, arg2) = get_2_arg(argv)?;
	let (addr, sock, assoc, assid, _) = connect_socket(&arg1, auth_a, auth_b.clone())?;
	do_admin_dump_op(argv0, &arg2, assoc, sock, addr, auth_b, assid, rauth.as_deref(), &arg1)
}

fn do_admin_list_keys(argv: &[String]) -> Result<(), i32>
{
	let (auth_a, auth_b, _) = generate_admin_auths(argv)?;
	let arg1 = get_1_arg(argv)?;
	let (addr, sock, mut assoc, assid, _) = connect_socket(&arg1, auth_a, auth_b.clone())?;
	let mut idx = 0;
	'a: loop {
		let cmd = Query::AKeyName(idx);
		let mut err = false;
		assoc.send_message(&sock, addr.clone(), auth_b.clone(), cmd, |msg|{
			eprintln!("Error sending admin key name: {msg}");
			err = true;
		});
		loop { match raw_receive(&sock, &addr) {
			Ok(Some(p)) => match CReply::parse(&p.payload) {
				CReply::PublicKey(aid, _, kname, _, _) => {
					if aid != assid { continue; }	//Wrong association.
					eprintln!("{kname}");
					idx += 1;
					break;
				},
				CReply::AdminUnauthorized(aid, _) => {
					if aid != assid { continue; }	//Wrong association.
					eprintln!("Admin permission denied");
					return Err(1);
				},
				CReply::BadKeyIndex(aid, _) => {
					if aid != assid { continue; }	//Wrong association.
					break 'a;	//End of keys.
				},
				CReply::InternalError(aid, err) => {
					if aid != assid { continue; }	//Wrong association.
					eprintln!("Internal error: {err}");
					return Err(1);
				},
				_ => {
					eprintln!("Unknown reply from {addr} trying to list keys");
					return Err(1);
				}
			},
			Ok(None) => (),
			Err(_) => return Err(1)
		}}
	}
	Ok(())
}

fn handle_dump_response(cmd: Query, mut assoc: AssociationMap, sock: DatagramSocket, auth_b: Arc<AuthKey>,
	rauth: Option<String>, addr: Arc<PacketAddress>, assid: u64, argv0: &str, rsock: &str, kid_name: &str) ->
	Result<(), i32>
{
	let mut err = false;
	assoc.send_message(&sock, addr.clone(), auth_b.clone(), cmd, |msg|{
		eprintln!("Error sending key dump by name/kid: {msg}");
		err = true;
	});
	if err { return Err(1); }
	loop { match raw_receive(&sock, &addr) {
		Ok(Some(p)) => match CReply::parse(&p.payload) {
			CReply::PublicKey(aid, _, name, spki, methodlist) => {
				if aid != assid { continue; }	//Wrong association.
				let data = __dump_low(argv0, &name, rauth.as_deref(),
					rsock, spki.deref(), methodlist.deref()).map_err(|err|{
					eprintln!("Error dumping key: {err}");
					1
				})?;
				write!(stdout(), "{data}").ok();
				stdout().flush().ok();
				return Ok(())
			},
			//Ignore any revoked keys.
			CReply::RevokedKeyIndex(_, _) => {
				continue;
			},
			CReply::BadKeyName(aid, _) => {
				if aid != assid { continue; }	//Wrong association.
				eprintln!("No key with given name '{kid_name}' in {addr}, or permission denied");
				return Err(1);
			},
			CReply::BadKey(aid, _) => {
				if aid != assid { continue; }	//Wrong association.
				eprintln!("No key with given kid '{kid_name}' in {addr}, or permission denied");
				return Err(1);
			},
			_ => {
				eprintln!("Unknown reply from {addr} trying to dump key");
				return Err(1);
			}
		},
		Ok(None) => (),
		Err(_) => return Err(1)
	}}
}

fn do_dump_by_kid(argv0: &str, argv: &[String]) -> Result<(), i32>
{
	//Use admin-like authentication, even if this is not an admin action.
	let (auth_a, auth_b, rauth) = generate_admin_auths(argv)?;
	let (arg1, arg2) = get_2_arg(argv)?;
	let (addr, sock, assoc, assid, _) = connect_socket(&arg1, auth_a, auth_b.clone())?;
	let mut hash = [0;32];
	let mut err = false;
	for (i, b) in arg2.bytes().enumerate() {
		let h = match b {
			48..=57 => b - 48,
			65..=70 => b - 55,
			97..=102 => b - 87,
			_ => {err = true; 0 },
		};
		hash.get_mut(i/2).map(|y|*y |= h << 4 - i % 2 * 4);
	}
	if arg2.len() != 64 || err {
		eprintln!("Bad hash '{arg2}'");
		return Err(1);
	}
	let cmd = Query::DumpKeyKid(hash);
	handle_dump_response(cmd, assoc, sock, auth_b, rauth, addr, assid, argv0, &arg1, &arg2)
}

fn do_dump_by_name(argv0: &str, argv: &[String]) -> Result<(), i32>
{
	//Use admin-like authentication, even if this is not an admin action.
	let (auth_a, auth_b, rauth) = generate_admin_auths(argv)?;
	let (arg1, arg2) = get_2_arg(argv)?;
	let (addr, sock, assoc, assid, _) = connect_socket(&arg1, auth_a, auth_b.clone())?;
	let cmd = Query::DumpKeyName(Cow::Borrowed(arg2.as_bytes()));
	handle_dump_response(cmd, assoc, sock, auth_b, rauth, addr, assid, argv0, &arg1, &arg2)
}

//Main function.
fn main2(argv: &[String]) -> i32
{
	let mut _argv = Vec::new();
	for i in argv.iter() { _argv.push(i.clone()); }
	let mut argv = _argv;

	if let Some(cmd) = argv.get(1) {
		let exec = &argv[0];
		let tail = &argv[2..];
		if cmd == "admin-create-key" {
			return do_admin_create_key(exec, tail).map(|_|0).unwrap_or_else(|e|e);
		}
		if cmd == "admin-delete-key" {
			return do_admin_delete_key(tail).map(|_|0).unwrap_or_else(|e|e);
		}
		if cmd == "admin-dump-key" {
			return do_admin_dump_key(exec, tail).map(|_|0).unwrap_or_else(|e|e);
		}
		if cmd == "admin-list-keys" {
			return do_admin_list_keys(tail).map(|_|0).unwrap_or_else(|e|e);
		}
		if cmd == "dump-by-kid" {
			return do_dump_by_kid(exec, tail).map(|_|0).unwrap_or_else(|e|e);
		}
		if cmd == "dump-by-name" {
			return do_dump_by_name(exec, tail).map(|_|0).unwrap_or_else(|e|e);
		}
	}
	let mut tag = None;
	let mut tagi = None;
	//Load tag.
	for (idx, i) in argv.iter().enumerate() { if idx > 0 {
		if let Some(i) = i.strip_prefix("--tag=") {
			tag = Some(i.to_owned());
			tagi = Some(idx);
		}
	}}
	if let Some(tagi) = tagi {
		for i in tagi..argv.len()-1 { argv[i] = argv[i+1].clone(); }
		argv.pop();
	}

	if argv.len() < 3 || argv.len() > 4 {
		eprintln!("Syntax: {arg0} <prefix> <socket> [<auth>] [--tag=<tag>]", arg0=argv[0]);
		return 1;
	}
	//Parse socket.
	let pfx = &argv[1];
	let auth_a = generate_random_auth();
	let mut auth_b = generate_random_auth();
	//If authentication file exists, load it.
	if let Some(authfile) = argv.get(3) {
		//Do name translation, in case authentication file path is special (used for user-common
		//authentication).
		let authfile = match translate_special_path(authfile) {
			Ok(name) => name,
			Err(err) => {
				eprintln!("Unable to expand filename {authfile}: {err}");
				return 1;
			}
		};
		let mut authdata = Vec::new();
		match File::open(authfile.deref()).and_then(|mut x|x.read_to_end(&mut authdata)) {
			Ok(_) => {
				let private = sha256(&authdata);
				let mut public = [0u8; 32];
				x25519_generate(&mut public, &private);
				//Ok, assign new key to slot b.
				auth_b = Arc::new(AuthKey{
					private: private,
					public: public,
				});
			},
			Err(err) => {
				eprintln!("Unable to read {authfile}: {err}");
				return 1;
			}
		}
	}
	do_dump(&argv[0], &argv[2], argv.get(3).map(|x|x.deref()), pfx, auth_a, auth_b, tag.as_deref())
}

fn main()
{
	Umask::PUBLIC.swap();	//Write world-readable files.
	let mut x = Vec::new();
	for i in args() { x.push(i); }
	exit(main2(&x));
}
