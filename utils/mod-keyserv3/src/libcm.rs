use btls_aux_collections::Condvar;
use btls_aux_collections::Mutex;
use btls_aux_time::ShowTime;
use btls_aux_unix::FileDescriptor;
use btls_aux_unix::LogFacility;
use btls_aux_unix::Logger;
use btls_aux_unix::LogLevel;
use btls_aux_unix::OpenFlags;
use btls_aux_unix::OpenlogFlags;
use btls_aux_unix::Path as UnixPath;
use std::fmt::Write as FmtWrite;
use std::io::Error as IoError;
use std::mem::replace;
use std::ops::DerefMut;
use std::ptr::null_mut;
use std::sync::atomic::AtomicPtr;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering;
use std::thread::Builder as ThreadBuilder;


pub trait CompareToZero: Sized+Copy
{
	fn is_negative(self) -> bool;
}

macro_rules! impl_cmpz {
	($xtype:ident) => { impl CompareToZero for $xtype { fn is_negative(self) -> bool { self < 0 }} }
}

impl_cmpz!(i8); impl_cmpz!(i16); impl_cmpz!(i32); impl_cmpz!(i64); impl_cmpz!(isize);

//Negative to error.
pub fn libc_to_error<T:CompareToZero>(v: T) -> Result<T, IoError>
{
	if v.is_negative() { Err(IoError::last_os_error()) } else { Ok(v) }
}

static OPENLOG_CALLED: AtomicUsize = AtomicUsize::new(0);
static LOG_SINK: AtomicPtr<LogSink> = AtomicPtr::new(null_mut());

struct LogSink
{
	fd: FileDescriptor,
	queue: Mutex<Vec<String>>,
	condition: Condvar,
	error: bool,
	overflow: bool,
}

//Assumes LOG_SINK is set!
fn log_sink_thread()
{
	let log_sink = LOG_SINK.load(Ordering::Relaxed);
	loop { unsafe {
		let mut guard = (*log_sink).queue.lock();
		while guard.is_empty() {
			guard = (*log_sink).condition.wait(guard);
		}
		let mut queue = replace(guard.deref_mut(), Vec::new());
		let fd = (*log_sink).fd.as_fdb();
		//The lock MUST be dropped during calls to write. Failing to do so might lead to deadlocks.
		//However, the fd MUST be copied before that point.
		drop(guard);
		//Write all accumulated lines and free the memory.
		for logline in queue.drain(..) {
			let mut itr = 0;
			while itr < logline.len() {
				match fd.write(&logline.as_bytes()[itr..]) {
					Ok(amt) => itr += amt,
					Err(e) => if !e.is_transient() {
						//Do not loop on fatal write error.
						break;
					}
				}
			}
		}
	}}
}

pub(crate) fn os_openlog_systemd()
{
	OPENLOG_CALLED.store(2, Ordering::Relaxed);
}

pub(crate) fn os_openlog(ident: &[u8], option: OpenlogFlags, facility: LogFacility) -> Result<(), ()>
{
	//Ownership is transferred, so use into_raw() instead of as_ptr().
	Logger::openlog(ident, option, facility);
	OPENLOG_CALLED.store(1, Ordering::Relaxed);
	Ok(())
}

pub fn os_set_log(name: &[u8]) -> Result<(), String>
{
	let name = UnixPath::new(name).ok_or_else(||format!("Illegal log filename"))?;
	match name.open(OpenFlags::O_WRONLY|OpenFlags::O_CREAT|OpenFlags::O_APPEND|OpenFlags::O_CLOEXEC) {
		Ok(fd) => {
			let sink = LogSink {
				fd: fd,
				queue: Mutex::new(Vec::new()),
				condition: Condvar::new(),
				error: false,
				overflow: false,
			};
			//Start thread. If this fails, set the error flag.
			LOG_SINK.store(Box::into_raw(Box::new(sink)), Ordering::Release);
			let builder = ThreadBuilder::new();
			let thread = builder.spawn(log_sink_thread);
			unsafe{(*LOG_SINK.load(Ordering::Relaxed)).error = thread.is_err();}
		},
		Err(err) => fail!(format!("Failed to open log: {err}"))
	}
	Ok(())
}

fn hiprio_message(priority: LogLevel) -> bool { priority.at_least(LogLevel::Warning) }

pub fn os_syslog(priority: LogLevel, msg: &str)
{
	let pstr = priority.textual_log_priority();
	let mut logged = false;
	let log_sink = LOG_SINK.load(Ordering::Relaxed);
	if unsafe{!log_sink.is_null() && !(*log_sink).error} { unsafe {
		logged = true;
		//Escape some control characters in the string.
		let mut msg2 = String::with_capacity(msg.len() + 40);
		write!(msg2, "{t} {pstr}: ", t=ShowTime::now().format2()).ok();

		for b in msg.chars() {
			match b {
				'\0' => msg2.push_str("<NUL>"),
				'\u{8}' => msg2.push_str("<BS>"),
				'\n' => msg2.push_str("<LF>"),
				'\r' => msg2.push_str("<CR>"),
				'\u{7f}' => msg2.push_str("<DEL>"),
				x => msg2.push(x)
			}
		}
		msg2.push('\n');	//LF to end line.
		//Push into queue.
		let mut guard = (*log_sink).queue.lock();
		let need_wake = guard.is_empty();
		//Guard against queue growing too big.
		if need_wake  {
			//Queue has cleared.
			(*log_sink).overflow = false;
			guard.push(msg2);
			(*log_sink).condition.notify_all();
		} else if (*log_sink).overflow {
			//Message will just get lost.
		} else if guard.len() > 5000 {
			//Log message about lost messages and lose this message. The queue is never empty, so
			//wakeup is never required (there is already one pending).
			guard.push(format!("Critical: Disk unable to keep up! Messages will be lost!\n"));
			(*log_sink).overflow = true;
		} else {
			//Already pending wake.
			guard.push(msg2);
		}
	}}
	let logmode = OPENLOG_CALLED.load(Ordering::Relaxed);
	let facility = if logmode > 0 {
		if hiprio_message(priority) || !logged {
			//Only log high-priority messages to syslog.
			Some(if logmode == 2 { Logger::StderrSystemD } else { Logger::Syslog })
		} else {
			None
		}
	} else {
		Some(Logger::StderrPlain)
	};
	if let Some(facility) = facility { facility.log(None, priority, format_args!("{msg}")); }
}
