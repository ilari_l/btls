use ::os_syslog;
use btls_aux_fail::ResultExt;
use btls_aux_memory::Attachment;
use btls_aux_memory::EscapeByteString;
use btls_aux_memory::split_attach_first;
use btls_aux_random::secure_random;
use btls_aux_unix::Address;
use btls_aux_unix::AddressIpv4;
use btls_aux_unix::AddressIpv6;
use btls_aux_unix::FileDescriptor;
use btls_aux_unix::Gid;
use btls_aux_unix::LogLevel;
use btls_aux_unix::OsError;
use btls_aux_unix::Path as UnixPath;
use btls_aux_unix::Pid;
use btls_aux_unix::SocketFamily;
use btls_aux_unix::SocketFlags;
use btls_aux_unix::SocketType;
use btls_aux_unix::Uid;
use btls_aux_unix::UnixPath as SUnixPath;
use std::cmp::Ordering;
use std::ffi::OsStr;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::fs::remove_file;
use std::io::Error as IoError;
use std::io::ErrorKind as IoErrorKind;
use std::mem::MaybeUninit;
use std::net::Ipv4Addr;
use std::net::Ipv6Addr;
use std::net::SocketAddr;
use std::net::SocketAddrV4;
use std::net::SocketAddrV6;
use std::ops::Deref;
use std::os::unix::ffi::OsStrExt;
use std::path::Path;
use std::path::PathBuf;
use std::str::FromStr;


fn r2l_sockaddr_in(a: SocketAddrV4) -> Address<'static>
{
	Address::Ipv4(AddressIpv4 {
		addr: a.ip().octets(),
		port: a.port()
	})
}

fn r2l_sockaddr_in6(a: SocketAddrV6) -> Address<'static>
{
	Address::Ipv6(AddressIpv6 {
		addr: a.ip().octets(),
		port: a.port(),
		scope: a.scope_id(),
		flow: a.flowinfo(),
	})
}

fn r2l_sockaddr_un(a: &[u8]) -> Result<Address<'static>, String>
{
	//Check first character if this is a path or abstract.
	if let Some(a) = a.strip_prefix(b"@") { r2l_sockaddr_un_abs(a) } else { Ok(r2l_sockaddr_un_path(a)) }
}

fn r2l_sockaddr_un_path(a: &[u8]) -> Address<'static> { Address::Unix(SUnixPath::owned(a)) }

fn r2l_sockaddr_un_abs(a: &[u8]) -> Result<Address<'static>, String>
{
	Ok(Address::Abstract(SUnixPath::owned(a)))
}

pub fn scope_to_id(scope: &str) -> Result<u32, &'static str>
{
	//Interface names with NUL in them are definitely unknown.
	let scope2 = UnixPath::new(scope.as_bytes()).ok_or("unknown interface name")?;
	scope2.interface_index().ok_or("unknown interface name")
}

fn parse_ipv6_address(addr: &str) -> Result<SocketAddrV6, &'static str>
{
	let err = "invalid IP address syntax";
	let mut itr = addr.rsplitn(2, ':');
	let port = itr.next().ok_or(err)?;
	let addr = itr.next().ok_or(err)?;
	let port = u16::from_str(port).set_err(err)?;
	let addr = addr.strip_prefix("[").ok_or(err)?;
	let addr = addr.strip_suffix("]").ok_or(err)?;
	let (addr, scope) = match split_attach_first(addr, "%", Attachment::Center) {
		Ok((addr, scope)) => (addr, scope_to_id(scope)?),
		Err(addr) => (addr, 0)
	};
	let ret = SocketAddrV6::new(Ipv6Addr::from_str(addr).set_err(err)?, port, 0, scope);
	Ok(ret)
}

#[derive(Clone,Debug,PartialEq,Eq,Hash)]
pub enum PacketAddress
{
	Ipv4(SocketAddrV4),
	Ipv6(SocketAddrV6),
	UnixPath(Vec<u8>),
	UnixAbstract(Vec<u8>),
	Unknown
}

impl Display for PacketAddress
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		match self {
			&PacketAddress::Ipv4(ref addr) => write!(fmt, "ipv4:{addr}"),
			&PacketAddress::Ipv6(ref addr) => write!(fmt, "ipv6:{addr}(scope {scope}, flow {flow})",
				scope=addr.scope_id(), flow=addr.flowinfo()),
			&PacketAddress::UnixPath(ref addr) => write!(fmt, "unix:{addr}", addr=EscapeByteString(addr)),
			&PacketAddress::UnixAbstract(ref addr) =>
				write!(fmt, "abstract:{addr}", addr=EscapeByteString(addr)),
			&PacketAddress::Unknown => fmt.write_str("<unknown>"),
		}
	}
}

impl Ord for PacketAddress
{
	fn cmp(&self, other: &PacketAddress) -> Ordering
	{
		match (self, other) {
			(&PacketAddress::Ipv4(ref x), &PacketAddress::Ipv4(ref y)) => {
				let c1 = x.ip().cmp(&y.ip());
				let c2 = x.port().cmp(&y.port());
				if c1.is_ne() { return c1; }
				if c2.is_ne() { return c2; }
				Ordering::Equal
			},
			(&PacketAddress::Ipv6(ref x), &PacketAddress::Ipv6(ref y)) => {
				let c1 = x.ip().cmp(&y.ip());
				let c2 = x.port().cmp(&y.port());
				let c3 = x.scope_id().cmp(&y.scope_id());
				let c4 = x.flowinfo().cmp(&y.flowinfo());
				if c1.is_ne() { return c1; }
				if c2.is_ne() { return c2; }
				if c3.is_ne() { return c3; }
				if c4.is_ne() { return c4; }
				Ordering::Equal
			},
			(&PacketAddress::UnixPath(ref x), &PacketAddress::UnixPath(ref y)) => x.cmp(y),
			(&PacketAddress::UnixAbstract(ref x), &PacketAddress::UnixAbstract(ref y)) => x.cmp(y),
			(&PacketAddress::Unknown, &PacketAddress::Unknown) => Ordering::Equal,
			(x, y) => x.discriminant().cmp(&y.discriminant())
		}
	}
}

impl PartialOrd for PacketAddress
{
	fn partial_cmp(&self, other: &PacketAddress) -> Option<Ordering> { Some(self.cmp(other)) }
}

impl PacketAddress
{
	fn discriminant(&self) -> usize
	{
		match self {
			&PacketAddress::Ipv4(_) => 0,
			&PacketAddress::Ipv6(_) => 1,
			&PacketAddress::UnixPath(_) => 2,
			&PacketAddress::UnixAbstract(_) => 3,
			&PacketAddress::Unknown => 4,
		}
	}
	pub fn parse(addr: &str) -> Result<PacketAddress, String>
	{
		Ok(if let Some(addr) = addr.strip_prefix("@") {
			PacketAddress::UnixAbstract(addr.as_bytes().to_owned())
		} else if addr.starts_with("/") {
			PacketAddress::UnixPath(addr.as_bytes().to_owned())
		} else if let Ok(x) = SocketAddrV4::from_str(addr) {
			//We can not use SocketAddr::from_str() here, because it does not like scope specifiers,
			//which we do need to support. So first parse IPv4 address if possible, then parse IPv6
			//address manually.
			PacketAddress::Ipv4(x)
		} else {
			//See the above comment.
			match parse_ipv6_address(addr) {
				Ok(x) => PacketAddress::Ipv6(x),
				Err(err) => return Err(format!("Error parsing UDP socket address: {err}")),
			}
		})
	}
	pub fn to_address(&self) -> Result<Address, String>
	{
		Ok(match self {
			&PacketAddress::Ipv4(x) => r2l_sockaddr_in(x),
			&PacketAddress::Ipv6(x) => r2l_sockaddr_in6(x),
			&PacketAddress::UnixPath(ref x) => Address::Unix(SUnixPath::borrowed(x)),
			&PacketAddress::UnixAbstract(ref x) => Address::Abstract(SUnixPath::borrowed(x)),
			&PacketAddress::Unknown => fail!(format!("Unknown address")),
		})
	}
	pub fn family(&self) -> Option<SocketFamily>
	{
		Some(self.to_address().ok()?.family())
	}
}

pub struct SocketCredentials
{
	pub uid: Option<Uid>,	//UID of other end if known.
	pub gid: Option<Gid>,	//GID of other end if known.
	pub pid: Option<Pid>,	//PID of other end if known.
	pub llif: Option<u32>,	//IF of other end if known.
}

impl SocketCredentials
{
	pub fn error() -> SocketCredentials
	{
		SocketCredentials {
			uid: None,
			gid: None,
			pid: None,
			llif: None,
		}
	}
}

pub struct ReceivedMessage
{
	//The source address of the packet. Note that this is not valid for SOCK_SEQPACKET sockets.
	pub src_addr: PacketAddress,
	//The credentials of the packet
	pub credentials: SocketCredentials,
	//The payload of the packet.
	pub payload: Vec<u8>,
}

fn os_error_to_io_error(e: OsError) -> IoError { IoError::from_raw_os_error(e.to_inner()) }

pub fn receive_message_from_fd(fd: &FileDescriptor) -> Result<Option<ReceivedMessage>, String>
{
	let mut control = [MaybeUninit::uninit();128];	//Should be big enogugh
	let mut buffer = [0;65536];
	let mut control = ::btls_aux_unix::CmsgParser::new2(&mut control);
	let buf = [&mut buffer[..]];
	let (payload, name) = match fd.recvmsg(true, &buf, Some(&mut control), Default::default()).
		map_err(os_error_to_io_error) {
		Ok((x, name)) => {
			let buf = &buf[0];
			(buf.get(..x).unwrap_or(buf).to_owned(), name)
		},
		Err(err) => return if err.kind() == IoErrorKind::Interrupted ||
			err.kind() == IoErrorKind::WouldBlock {
			Ok(None)
		} else {
			Err(format!("recvmsg: {err}"))
		}
	};
	let src_addr = match name {
		Some(Address::Ipv4(x)) => PacketAddress::Ipv4(SocketAddrV4::new(Ipv4Addr::from(x.addr), x.port)),
		Some(Address::Ipv6(x)) => PacketAddress::Ipv6(SocketAddrV6::new(Ipv6Addr::from(x.addr), x.port,
			x.flow, x.scope)),
		Some(Address::Unix(x)) => PacketAddress::UnixPath(x.deref().to_owned()),
		Some(Address::Abstract(x)) => PacketAddress::UnixAbstract(x.deref().to_owned()),
		None => PacketAddress::Unknown,
	};
	let mut credentials = SocketCredentials::error();
	control.iterate(|mtype, payload|{
		if false { return Err(String::new()); }		//Type inference.
		use ::btls_aux_unix::CmsgKind;
		use ::btls_aux_unix::Ucred;
		if mtype == CmsgKind::SCM_CREDENTIALS {
			//Ok, pick the credentials.
			let creds = Ucred::from_bytes(payload).map_err(|err|format!("recvmsg: {err}"))?;
			credentials.uid = Some(creds.uid());
			credentials.gid = Some(creds.gid());
			credentials.pid = Some(creds.pid());
		}
		Ok(())
	})?;
	//Include if scope if available.
	if let &PacketAddress::Ipv6(ref a) = &src_addr {
		let scope = a.scope_id();
		if scope > 0 { credentials.llif = Some(scope); }
	}
	Ok(Some(ReceivedMessage {
		src_addr: src_addr,
		credentials: credentials,
		payload: payload,
	}))
}

fn send_message_to_fd(fd: &FileDescriptor, to: Option<&PacketAddress>, data: &[u8]) -> Result<(), String>
{
	if data.len() == 0 { return Ok(()); }	//No empty packets.
	match to {
		Some(a) => fd.sendto(data, &a.to_address()?, Default::default()),
		None => fd.send(data, Default::default()),
	}.map(|_|()).map_err(|err|format!("sendto: {err}"))
}

pub fn send_auth_message_to_fd(fd: &FileDescriptor, to: Option<&PacketAddress>, data: &[u8]) -> Result<(), String>
{
	use ::btls_aux_unix::CmsgSerializer;
	let unspec = to.is_none();
	let to = match to {
		Some(to) => to.to_address().ok(),
		None => None,
	};
	//Target address may be NULL only if to is None.
	if !unspec && to.is_none() { return Err(format!("Invalid socket target address")); }
	let mut control = [MaybeUninit::uninit();104];		//Should fit one ucred in.

	let mut control = CmsgSerializer::new2(&mut control);
	use ::btls_aux_unix::CmsgKind;
	use ::btls_aux_unix::Ucred;
	control.add(CmsgKind::SCM_CREDENTIALS, Ucred::new().to_inner()).
		map_err(|err|format!("adding_cmsg: {err}"))?;
	fd.sendmsg(to.as_ref(), &[data], Some(&control), Default::default()).
		map_err(|err|format!("sendmsg: {err}"))?;
	Ok(())
}

#[derive(Debug)]
pub enum DatagramSocket
{
	Udp(FileDescriptor),
	Unix(FileDescriptor, Option<PathBuf>),
	Seqpkt(FileDescriptor, PacketAddress),
	SeqpktListener(FileDescriptor),
}

impl Drop for DatagramSocket
{
	fn drop(&mut self)
	{
		//Remove the bound file.
		if let &mut DatagramSocket::Unix(_, Some(ref p)) = self { remove_file(p).ok(); }
	}
}

impl DatagramSocket
{
	pub fn unconnected(family: SocketFamily) -> Result<DatagramSocket, String>
	{
		//Set nonblocking mode in order to avoid deadlocking with the key daemon.
		let flags = SocketFlags::NONBLOCK|SocketFlags::CLOEXEC;
		let fd = FileDescriptor::socket2(family, SocketType::DGRAM, Default::default(), flags).
			map_err(|err|format!("socket: {err}"))?;
		//Unix sockets have to be bound for reply.
		let p = if family == SocketFamily::UNIX {
			let mut rnd = [0;24];
			let mut a = b"/tmp/"[..].to_owned();
			secure_random(&mut rnd);
			let letters = b"abcdefghijklmnopqrstuvwxyz234567";
			for i in rnd.iter() { a.push(letters[(*i & 31) as usize]); }
			let fpath = UnixPath::new(&a).
				ok_or_else(||format!("Illegal socket path '{a}'", a=EscapeByteString(&a)))?;
			let addr = r2l_sockaddr_un_path(&a);
			fd.bind(&addr).map_err(|err|format!("bind: {err}"))?;
			fpath.chmod(511).ok();		//Give 777 perms.
			Some(Path::new(OsStr::from_bytes(&a)).to_path_buf())
		} else {
			None
		};
		Ok(if family == SocketFamily::INET || family == SocketFamily::INET6 {
			//Transfer ownership
			DatagramSocket::Udp(fd)
		} else if family == SocketFamily::UNIX {
			DatagramSocket::Unix(fd, p)
		} else {
			return Err(format!("Unrecognized address family {family}"))
		})
	}
	pub fn listen_fd(fd: FileDescriptor, seqpkt: bool) -> Result<DatagramSocket, String>
	{
		if seqpkt {
			//Listeners have to be nonblocking.
			fd.set_nonblock(true).map_err(|err|format!("set nonblocking: {err}"))?;
			return Ok(DatagramSocket::SeqpktListener(fd));
		}
		let addr = fd.local_address().map_err(|err|format!("getsockname: {err}"))?;
		let addr = addr.ok_or_else(||format!("getsockname: Unknown address type"))?;
		Ok(if addr.family() == SocketFamily::UNIX {
			fd.set_passcred(true).map_err(|err|format!("set passcred: {err}"))?;
			DatagramSocket::Unix(fd, None)
		} else {
			DatagramSocket::Udp(fd)
		})
	}
	pub fn accept(&self) -> Result<Option<DatagramSocket>, String>
	{
		if let &DatagramSocket::SeqpktListener(ref fd) = self {
			//Has to be nonblocking to avoid deadlocks.
			let flags = SocketFlags::NONBLOCK|SocketFlags::CLOEXEC;
			let (fd, source) = match fd.accept2(flags) {
				Ok(x) => x,
				Err(err) => {
					if err == OsError::EINTR { return Ok(None); }
					if err == OsError::EAGAIN { return Ok(None); }
					if err == OsError::EWOULDBLOCK { return Ok(None); }
					return Err(format!("Failed to accept connection: {err}"));
				}
			};
			//Has to be able to receive SCM_CREDENTIALS.
			if source.map(|a|a.family()) == Some(SocketFamily::UNIX) {
				fd.set_passcred(true).map_err(|err|format!("set passcred: {err}"))?;
			}
			//The packet address is always unknown for accepted sockets.
			Ok(Some(DatagramSocket::Seqpkt(fd, PacketAddress::Unknown)))
		} else {
			Err("Only seqpkt listener supports accepting".to_owned())
		}
	}
	pub fn connect_seqpkt(addr: &PacketAddress) -> Result<DatagramSocket, String>
	{
		let oaddr = addr;
		let addr = addr.to_address().map_err(|err|format!("Seqpkt target: {err}"))?;
		let family = addr.family();
		fail_if!(family != SocketFamily::UNIX, format!("Seqpkt target must be valid UNIX socket"));
		//Has to be nonblocking to avoid deadlocks.
		let flags = SocketFlags::NONBLOCK|SocketFlags::CLOEXEC;
		let fd = FileDescriptor::socket2(family, SocketType::SEQPACKET, Default::default(), flags).
			map_err(|err|format!("socket: {err}"))?;
		fd.connect(&addr).map_err(|err|format!("Connect failed: {err}"))?;
		Ok(DatagramSocket::Seqpkt(fd, oaddr.clone()))
	}
	pub fn get_seqpkt_name(&self) -> PacketAddress
	{
		if let &DatagramSocket::Seqpkt(_, ref y) = self { y.clone() } else { PacketAddress::Unknown }
	}
	pub fn is_data_socket(&self) -> bool
	{
		match self {
			&DatagramSocket::Udp(_) => true,
			&DatagramSocket::Unix(_, _) => true,
			&DatagramSocket::Seqpkt(_, _) => true,
			&DatagramSocket::SeqpktListener(_) => false,
		}
	}
	pub fn listen(addr: &str) -> Result<DatagramSocket, String>
	{
		//These are not flagged as NONBLOCK for some reason.
		let flags = SocketFlags::CLOEXEC;
		let (addr, seqpkt) = if let Some(addr) = addr.strip_prefix("?") {
			(addr, true)
		} else {
			(addr, false)
		};
		Ok(if addr.starts_with("@") || addr.starts_with("/") {
			let addr = r2l_sockaddr_un(addr.as_bytes()).map_err(|err|format!("Bad socket path: {err}"))?;
			let stype = if seqpkt { SocketType::SEQPACKET } else { SocketType::DGRAM };
			let fd = FileDescriptor::socket2(SocketFamily::UNIX, stype, Default::default(), flags).
				map_err(|err|format!("socket: {err}"))?;
			fd.bind(&addr).map_err(|err|format!("bind: {err}"))?;
			if seqpkt {
				fd.listen(10).map_err(|err|format!("listen: {err}"))?;
				DatagramSocket::SeqpktListener(fd)
			} else {
				fd.set_passcred(true).map_err(|err|format!("set passcred: {err}"))?;
				DatagramSocket::Unix(fd, None)
			}
		} else {
			if seqpkt { return Err("SEQPACKET mode not supported for IP".to_owned()); }
			let addr = match SocketAddr::from_str(addr) {
				Ok(x) => x,
				Err(err) => return Err(format!("Error parsing UDP socket address: {err}")),
			};
			let addr = match addr {
				SocketAddr::V4(x) => r2l_sockaddr_in(x),
				SocketAddr::V6(x) => r2l_sockaddr_in6(x),
			};
			let fd = FileDescriptor::socket2(addr.family(), SocketType::DGRAM, Default::default(), flags).
				map_err(|err|format!("socket: {err}"))?;
			fd.bind(&addr).map_err(|err|format!("bind: {err}"))?;
			DatagramSocket::Udp(fd)
		})
	}
	pub fn borrow_fd(&self) -> &FileDescriptor
	{
		match self {
			&DatagramSocket::Udp(ref x) => x,
			&DatagramSocket::Unix(ref x, _) => x,
			&DatagramSocket::Seqpkt(ref x, _) => x,
			&DatagramSocket::SeqpktListener(ref x) => x,
		}
	}
	pub fn receive_raw(&self) -> Result<Option<ReceivedMessage>, String>
	{
		match self {
			&DatagramSocket::Udp(ref x) => receive_message_from_fd(x),
			&DatagramSocket::Unix(ref x, _) => receive_message_from_fd(x),
			&DatagramSocket::Seqpkt(ref x, ref y) => {
				let mut r = receive_message_from_fd(x);
				//Use correct source address (from connection target) if available.
				if let &mut Ok(Some(ref mut z)) = &mut r { z.src_addr = y.clone(); }
				r
			},
			&DatagramSocket::SeqpktListener(_) =>
				Err(format!("Seqpkt listener does not support receiving messages")),
		}
	}
	pub fn send_raw(&self, to: &PacketAddress, payload: &[u8]) -> Result<(), String>
	{
		match self {
			&DatagramSocket::Udp(ref x) => send_message_to_fd(x, Some(to), payload),
			&DatagramSocket::Unix(ref x, _) => send_message_to_fd(x, Some(to), payload),
			&DatagramSocket::Seqpkt(ref x, _) => send_message_to_fd(x, None, payload),
			&DatagramSocket::SeqpktListener(_) =>
				Err(format!("Seqpkt listener does not support sending messages")),
		}
	}
	pub fn send_raw_auth(&self, to: &PacketAddress, payload: &[u8]) -> Result<(), String>
	{
		match self {
			//UDP sockets do not support authentication.
			&DatagramSocket::Udp(ref x) => send_message_to_fd(x, Some(to), payload),
			&DatagramSocket::Unix(ref x, _) => send_auth_message_to_fd(x, Some(to), payload),
			&DatagramSocket::Seqpkt(ref x, _) => send_auth_message_to_fd(x, None, payload),
			&DatagramSocket::SeqpktListener(_) =>
				Err(format!("Seqpkt listener does not support sending auth message")),
		}
	}
	pub fn receive_syslog(&self) -> Option<ReceivedMessage>
	{
		match self.receive_raw() {
			Ok(x) => x,
			Err(err) => {
				os_syslog(LogLevel::Warning, &format!("Failed to read from socket: {err}"));
				None
			}
		}
	}
	pub fn send_syslog(&self, to: &PacketAddress, payload: &[u8])
	{
		match self.send_raw(to, payload) {
			Ok(_) => (),
			Err(err) => os_syslog(LogLevel::Warning, &format!("Failed send to socket {to}: {err}"))
		}
	}
	pub fn accept_syslog(&self) -> Option<DatagramSocket>
	{
		match self.accept() {
			Ok(x) => x,
			Err(err) => {
				os_syslog(LogLevel::Warning, &format!("Failed to accept from socket: {err}"));
				None
			}
		}
	}
}
