keyserv3: Key server
====================
This daemon is intended to act as off-process key server for signing.
Separating keys this way from their use makes stealing keys more difficult.


X25519 keys:
============
Unless kernel authentication is used (only works on Linux and with Unix
domain sockets or abstract sockets), one needs to authenticate using X25519
keys. The private key file can be any file at least 32 bytes in size. The
public key can be extracted using 'keyserv3-dump-authid <key>'.


The main daemon executable: keyserv3
====================================
keyserv3 is the main daemon executable. It accepts the folowing options:

--listen=<socket>:
	Listen for datagram socket <socket>. The <socket> can be one of:
	/<path>: Listen for Unix domain socket at given path.
	@<path>: Listen for abstract socket at given path (Linux only).
	<ipv4addr>:<port>: Listen for specified UDP/IPv4 port.
	[<ipv6addr>]:<port>: Listen for specified UDP/IPv6 port.
	?/<path>: Like /<path>, but use SOCK_SEQPACKET.
	?@<path>: Like @<path>, but use SOCK_SEQPACKET.

	If unix domain socket already exists, it is unlinked first.

--pidfile=<file>:
	Write pidfile <file>

--uid=<uid>:
	After loading keys, switch to specified UID. Requires root
	privileges, required if running with root privileges.

--gid=<gid>:
	After loading keys, switch to specified GUID. Requires root
	privileges. If there is no suitable group, use --gid=65534.

--socket-uid=<uid>
	UID to change unix domain sockets to (only affects --listen
	directives after it). Requires root privileges.

--socket-gid=[<gid>]
	GID to change unix domain sockets to (only affects --listen
	directives after it). Changing socket to some non-supplementary group
	requires root privileges. If no <gid> is speicfied, then effect of
	previous --socket-gid is canceled. If <gid> is set, then the sockets
	will be group-accessable.

--no-detach:
	Do not detach from console. Mainly useful for debugging.

--no-delete:
	Disallow admins from deleting keys.

--log=<file>:
	Log to specified file.

<directory>:
	Directory containing keys to load. Can be specified multiple times.


Running from Systemd:
---------------------
If running from Systemd, unit type should be set to 'notify'. Keyserv3 will
autodetect this and act accordingly. --pidfile option should not be used with
Systemd.

Socket activation can be used. The passed listening sockets (should be
datagram mode) are automatically listened (no need to pass --listen).

ACL files:
----------
Foo every key foo, there is foo.acl file alongside that sets the privileges.
Empty lines and lines starting with # are ignored. Otherwise each line
contains a directive.

The following directives can be used:

any <subject>:
	Grant <subject> access to dump the public key and sign anything with
	it.

tls-server <subject>:
	Grant <subject> access to dump the public key and sign with TLS 1.2
	ECDHE server signatures and TLS 1.3 server signatures.

dump <subject>:
	Grant <subject> access to dump the public key, but not sign anything
	with it.

tag <tag>:
	Add tag <tag> (an arbitrary string) to key.


The <subject> can be:

uid:<uid>
	Grant access to UID <uid>. Only works on Linux and for Unix domain
	sockets and abstract sockets.

gid:<gid>
	Grant access to GID <gid>. Only works on Linux and for Unix domain
	sockets and abstract sockets.

llif:<interface>
	Grant access to link-local packets arriving from <interface>.

x25519:<hexstring>
	Grant access to specified X25519 public key.

group:<name>
	Grant access to all members of group <name>.

Groups:
-------
The groups are defined in groups.cfg. Each non-empty/non-comment (does not
start with #) is of form:

<groupname> [<members>...]

Where <groupname> is the name of the group, and [<members>...] is list of
members, separated by spaces. Each member is <subject> from ACL files, but
group: is not allowed.

Admins:
-------
The admins are defined in admins.cfg. Each non-empty/non-comment (does not
start with #) is <subject> from ACL files.

Admins can create keys and dump all keys, regardless of key permissions.


Dump keys from daemon libbtls_mod_keyserv3:
-------------------------------------------
Note: This file must be in the _same_ directory as libbtls_mod_keyserv3.so.
This .so file must either be owned by user running the TLS utilities or
root.

This utility dumps public keys and writes shell wrappers around them, so
btls can load the keys.

Syntax: libbtls_mod_keyserv3 <prefix> <socket> [<auth>] [--tag=<tag>]

The options are:

<prefix>:
	The prefix to prepend to key names.

<socket>:
	The keyserv3 socket to use. Note that this must be canonical name or
	dumping hangs (with error message about wrong source).

<auth>:
	X25519 private key file (optional if kernel authentication is used).

	If this starts with ?, the following placeholders can be used
	(expanded lazily):

	~/ (in start): $HOME or home directory of current user.
	~<name>/ (in start): Home directory of user <name>.
	%%: %
	%u: Name of current user.

--tag=<tag>
	Dump only keys with tag <tag>.

Creating keys:
--------------
Note: This file must be in the _same_ directory as libbtls_mod_keyserv3.so.
This .so file must either be owned by user running the TLS utilities or
root.

This utility creates a key, dumps public key associated and writes shell
wrapper around them, so btls can load the key. Admin privileges are required.

Syntax: libbtls_mod_keyserv3 admin-create-key <socket> <name> [--type=<type>] [--profile=<profile>] [--auth=<auth>]

The options from dumping keys apply. The following additional options are
available:

<type>:
	Type of key.

	0 => ECDSA P-256 (default)
	1 => ECDSA P-384
	2 => ECDSA P-521
	3 => Ed25519
	4 => Ed448

<profile>:
	The access profile of the key (default 0).

Dumping keys by kid or name:
----------------------------
Syntax: libbtls_mod_keyserv3 dump-key-by-name <socket> <name> [--auth=<auth>]
Syntax: libbtls_mod_keyserv3 dump-key-by-kid <socket> <kid> [--auth=<auth>]

The options from dumping keys apply. The key is dumped to stdout.
