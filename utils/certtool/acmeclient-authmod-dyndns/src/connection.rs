use std::io::Read;
use std::io::Write;
use std::mem::replace;
use std::net::SocketAddr;
use std::net::TcpStream;

fn dns_tcp_transaction(socket: &mut TcpStream, query: &[u8]) -> Result<Vec<u8>, String>
{
	let qlenbuf = [(query.len() >> 8) as u8, query.len() as u8];
	socket.write_all(&qlenbuf).map_err(|err|{
		format!("TCP write error: {err}")
	})?;
	socket.write_all(query).map_err(|err|{
		format!("TCP write error: {err}")
	})?;
	let mut rlenbuf = [0;2];
	socket.read_exact(&mut rlenbuf).map_err(|err|{
		format!("TCP read error: {err}")
	})?;
	let replylen = (rlenbuf[0] as usize) * 256 + (rlenbuf[1] as usize);
	let mut reply = vec![0;replylen];
	socket.read_exact(&mut reply).map_err(|err|{
		format!("TCP read error: {err}")
	})?;
	Ok(reply)
}

#[derive(Debug)]
pub struct DnsConnection
{
	target: SocketAddr,
	stream: Option<TcpStream>,
	nextid: u16,
}

impl DnsConnection
{
	pub fn new(target: &SocketAddr) -> DnsConnection
	{
		DnsConnection {
			target: target.clone(),
			stream: None,
			nextid: 0,
		}
	}
	pub fn allocate_transaction(&mut self) -> u16
	{
		let txid = self.nextid;
		self.nextid = self.nextid.wrapping_add(1);
		txid
	}
	fn _transact(&mut self, query: &[u8]) -> Result<Vec<u8>, String>
	{
		loop {
			let stream = replace(&mut self.stream, None);
			if let Some(mut stream) = stream {
				let ans = dns_tcp_transaction(&mut stream, query)?;
				self.stream = Some(stream);
				//Workaround Unbound bug.
				if ans.len() > 2 && ans[2] >= 128 { return Ok(ans); }
			} else {
				//Bring connection back up.
				let stream = TcpStream::connect(&self.target).map_err(|err|{
					format!("Failed to connect to {tgt}: {err}", tgt=self.target)
				})?;
				self.stream = Some(stream);
			}
		}
	}
	pub fn transact(&mut self, query: &[u8]) -> Result<Vec<u8>, String>
	{
		let mut error = String::new();
		for _ in 0..10 {
			match self._transact(query) { Ok(x) => return Ok(x), Err(e) => error = e };
		}
		Err(error)
	}
}
