use crate::connection::DnsConnection;
use btls_aux_dnsmsg::Class as DnsClass;
use btls_aux_dnsmsg::dns_output_message;
use btls_aux_dnsmsg::Dns0x20;
use btls_aux_dnsmsg::Header as DnsHeader;
use btls_aux_dnsmsg::Message as DnsMessage;
use btls_aux_dnsmsg::Name as DnsName;
use btls_aux_dnsmsg::NameBuf as DnsNameBuf;
use btls_aux_dnsmsg::NameCompressContext;
use btls_aux_dnsmsg::OutputMessage;
use btls_aux_dnsmsg::OutputQuery as DnsOutputQuery;
use btls_aux_dnsmsg::OutputResponse as DnsOutputResponse;
use btls_aux_dnsmsg::TsigInfo;
use btls_aux_dnsmsg::TsigSigner;
use btls_aux_dnsmsg::consts::DNSCLASS_NONE;
use btls_aux_dnsmsg::consts::DNSTYPE_SOA;
use btls_aux_dnsmsg::consts::DNSTYPE_TXT;
use btls_aux_hash::Hash;
use btls_aux_hash::Sha256;
use btls_aux_memory::SafeShowByteString;
use std::collections::BTreeMap;
use std::fs::File;
use std::io::Read;
use std::ops::Deref;
use std::path::Path;
use std::path::PathBuf;
use std::time::SystemTime;
use std::time::UNIX_EPOCH;


const CLASS: DnsClass = btls_aux_dnsmsg::consts::DNSCLASS_IN;
const OPCODE_UPDATE: u8 = 5;

pub struct DnsUpdateMessage
{
	zone: DnsNameBuf,
	updates: Vec<(DnsNameBuf,bool,Vec<u8>)>,
	txid: u16,
}

impl OutputMessage for DnsUpdateMessage
{
	fn get_header(&self) -> DnsHeader
	{
		DnsHeader {
			id: self.txid,
			qr: false,
			opcode: OPCODE_UPDATE,
			aa: false,
			tc: false,
			rd: false,
			ra: false,
			z: 0,
			rcode: 0,
			qdcount: 0,
			ancount: 0,
			nscount: 0,
			arcount: 0,
		}
	}
	fn get_question<'a>(&'a self, index: usize) -> Option<DnsOutputQuery<'a>>
	{
		if index == 0 {
			Some(DnsOutputQuery {
				qname: &self.zone,
				dns0x20: Dns0x20::new(),
				qclass: CLASS,
				qtype: DNSTYPE_SOA,
			})
		} else {
			None
		}
	}
	fn get_answer<'a>(&'a self, _: usize) -> Option<DnsOutputResponse<'a>> { None }
	fn get_authority<'a>(&'a self, index: usize) -> Option<DnsOutputResponse<'a>>
	{
		let &(ref to, add, ref content) = self.updates.get(index)?;
		let class = if add { CLASS } else { DNSCLASS_NONE };
		let ttl = if add { 1 } else { 0 };
		Some(DnsOutputResponse {
			owner: to,
			rclass: class,
			rtype: DNSTYPE_TXT,
			data: content,
			ttl: ttl,
		})
	}
	fn get_additional<'a>(&'a self, _: usize) -> Option<DnsOutputResponse<'a>> { None }
	fn get_udp_max(&self) -> u16 { 1200 }
	fn get_edns_flags(&self) -> u16 { 0 }
	fn get_edns_tlvs<'a>(&'a self) -> &'a [u8] { &[] }
}

impl DnsUpdateMessage
{
	pub fn new(zone: &DnsName, txid: u16) -> DnsUpdateMessage
	{
		DnsUpdateMessage {
			zone: zone.to_name_buf(),
			updates: Vec::new(),
			txid: txid,
		}
	}
	pub fn add_txt_record(&mut self, name: &DnsName, content: &[u8])
	{
		let content = encode_character_string(content);
		self.updates.push((name.to_name_buf(), true, content.clone()));
	}
	pub fn remove_txt_record(&mut self, name: &DnsName, content: &[u8])
	{
		let content = encode_character_string(content);
		self.updates.push((name.to_name_buf(), false, content.clone()));
	}
}

fn encode_character_string(data: &[u8]) -> Vec<u8>
{
	let mut data = data;
	let mut output = Vec::new();
	while data.len() > 255 {
		output.push(255);
		let (head, tail) = data.split_at(255);
		output.extend_from_slice(head);
		data = tail;
	}
	output.push(data.len() as u8);
	output.extend_from_slice(data);
	output
}

#[derive(Debug)]
pub struct TsigKey
{
	pub name: DnsNameBuf,
	pub secret: Vec<u8>,
}

impl TsigKey
{
	pub fn parse_file(keyname: &DnsName, filename: Option<&Path>) -> Result<TsigKey, String>
	{
		let filename = match filename {
			Some(x) => x.to_owned(),
			None => PathBuf::from(format!("{keyname}.tsig"))
		};
		let mut content = Vec::new();
		File::open(filename.deref()).and_then(|mut fp|fp.read_to_end(&mut content)).map_err(|err|{
			format!("Failed to load {filename}: {err}", filename=filename.display())
		})?;
		Ok(TsigKey {
			name: keyname.to_owned(),
			secret: content,
		})
	}
}

impl TsigSigner for TsigKey
{
	fn get_keyid<'a>(&'a self) -> &'a DnsName { &self.name }
	fn get_algorithm<'a>(&'a self) -> &'a DnsName { DnsName::tsig_hmac_sha256() }
	fn get_max_mac_length(&self) -> u16 { 32 }
	fn mac_multi(&self, buf: &mut [u8], data: &[&[u8]])
	{
		let mac = Sha256::function().hmac_v(&self.secret, data);
		let mlen = core::cmp::min(buf.len(), mac.len());
		(&mut buf[..mlen]).copy_from_slice(&mac[..mlen]);
	}
}

pub struct UpdatedRecords
{
	//The boolean is in-zone flag.
	contents: Vec<(DnsNameBuf, Vec<u8>, bool)>,
}

struct NameCompressor(BTreeMap<DnsNameBuf, u16>);

impl NameCompressor
{
	fn new() -> NameCompressor { NameCompressor(BTreeMap::new()) }
}

impl NameCompressContext for NameCompressor
{
	fn exchange_ptr(&mut self, name: &DnsName, pointer: u16) -> Option<u16>
	{
		if let Some(ptr) = self.0.get(name) { return Some(*ptr); }
		self.0.insert(name.to_name_buf(), pointer);
		None
	}
}

impl UpdatedRecords 
{
	pub fn new() -> UpdatedRecords { UpdatedRecords { contents: Vec::new() } }
	pub fn add_record(&mut self, name: &DnsName, content: &[u8])
	{
		//These records are never in-zone, as these need to be added to the zone.
		self.contents.push((name.to_owned(), content.to_owned(), false));
	}
	fn __dyndns_tail(msgx: DnsUpdateMessage, zoneserver: &mut DnsConnection, key: &TsigKey) ->
		Result<(), String>
	{
		//msg.add_edns0();
		let txid = msgx.txid;
		//let msg = msg.add_tsig_and_fixup(key);
		let mut msg = Vec::new();
		let mut nc = NameCompressor::new();
		dns_output_message(&msgx, false, &mut msg, &mut nc, Some(TsigInfo {
			chain_mac: None,
			error: 0,
			other: &[],
			fudge: 30,
			timestamp: SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs(),
			refuse_sign: false,
			signer: key,
		})).ok();	//This can not fail.
		let reply = zoneserver.transact(&msg)?;
		let rmsg = DnsMessage::decode(&reply, |_,_,_,_,_,_|()).map_err(|err|{
			format!("Failed to decode update reply: {err}")
		})?;
		fail_if!(!rmsg.header().qr, format!("DNS update reply is marked as query!"));
		let htxid = rmsg.header().id;
		let rcode = rmsg.header().rcode;
		fail_if!(txid != htxid, format!("DNS update reply has incorrect TXID ({txid:04x} != {htxid:04x})!"));
		if let Some(edns) = rmsg.edns() {
			fail_if!(edns.version != 0, format!("Dynamic DNS reply has bad EDNS version {version}",
				version=edns.version));
		}
		let had_tsig = if let Some(tsig) = rmsg.tsig() {
			//If TSIG has an error, that takes percedence to RCODE.
			fail_if!(tsig.error != 0, format!("Dynamic DNS reply TSIG error {err}", err=tsig.error));
			true
		} else {
			false
		};
		//The RCODE has to be !NOERROR
		fail_if!(rcode != 0, format!("Dynamic DNS reply RCODE{rcode}"));
		fail_if!(!had_tsig, format!("Dynamic DNS reply is NOERROR but lacks TSIG!"));
		Ok(())
	}
	pub fn send_add_all(&mut self, zone: &DnsName, zoneserver: &mut DnsConnection, key: &TsigKey) ->
		Result<(), String>
	{
		let mut msg = DnsUpdateMessage::new(zone, zoneserver.allocate_transaction());
		//Do not resend TXT records already in zone (in_zone=true).
		for &(ref name, ref content, in_zone) in self.contents.iter() { if !in_zone {
			msg.add_txt_record(name, content);
		}}
		UpdatedRecords::__dyndns_tail(msg, zoneserver, key)?;
		//Mark all records in-zone, as all were sent. Additionally print records added.
		for &mut (ref name,ref content,ref mut in_zone) in self.contents.iter_mut() { if !*in_zone {
			trace!("dyndns: Added {name} TXT {txt}", txt=SafeShowByteString(content));
			*in_zone = true;
		}}
		Ok(())
	}
	pub fn send_remove_all(&mut self, zone: &DnsName, zoneserver: &mut DnsConnection, key: &TsigKey) ->
		Result<(), String>
	{
		let mut msg = DnsUpdateMessage::new(zone, zoneserver.allocate_transaction());
		//Do not request to delete records not in zone (in_zone=false).
		for &(ref name, ref content, in_zone) in self.contents.iter() { if in_zone {
			msg.remove_txt_record(name, content);
		}}
		UpdatedRecords::__dyndns_tail(msg, zoneserver, key)?;
		//Mark all records not in-zone, as all were removed. Additionally print the records removed.
		for &mut (ref name,ref content,ref mut in_zone) in self.contents.iter_mut() { if *in_zone {
			trace!("dyndns: Removed {name} TXT {txt}", txt=SafeShowByteString(content));
			*in_zone = false;
		}}
		Ok(())
	}
	pub fn clear(&mut self)
	{
		self.contents.clear();
	}
}
