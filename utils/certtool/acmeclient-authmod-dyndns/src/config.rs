use crate::connection::DnsConnection;
use crate::query::fetch_zone_servers;
use crate::update::TsigKey;
use btls_aux_dnsmsg::NameBuf as DnsNameBuf;
use btls_aux_fail::ResultExt;
use std::fs::File;
use std::io::Read;
use std::net::IpAddr;
use std::net::SocketAddr;
use std::net::SocketAddrV4;
use std::net::SocketAddrV6;
use std::net::TcpStream;
use std::path::Path;
use std::path::PathBuf;
use std::str::FromStr;


fn with_words<F>(config: &str, mut cb: F) -> Result<(), String> where F: FnMut(&str) -> Result<(), String>
{
	let mut start_word = 0;
	let mut escape = false;
	for (idx, c) in config.char_indices() {
		if !escape && (c == '\t' || c == ' ') {
			if start_word < idx { cb(&config[start_word..idx])?; }
			start_word = idx + c.len_utf8();
		} else if !escape && c == '\\' {
			escape = true;
		} else if escape {
			escape = false;
		}
	}
	if start_word < config.len() { cb(&config[start_word..])?; }
	Ok(())
}

fn strip_prefix<'a>(string: &'a str, pfx: &str) -> Option<&'a str>
{
	if string.starts_with(pfx) { Some(&string[pfx.len()..]) } else { None }
}

#[derive(Debug)]
pub struct Config
{
	pub zone: DnsNameBuf,
	pub zoneserver: DnsConnection,
	pub nameserver: DnsConnection,
	pub key: TsigKey,
}

impl Config
{
	pub fn get_zone(config: &str) -> String
	{
		let mut zone = String::new();
		with_words(config, |word|{
			if let Some(_zone) = strip_prefix(word, "zone=") { zone = _zone.to_string(); }
			Ok(())
		}).ok();
		zone
	}
	pub fn new(config: &str) -> Result<Config, String>
	{
		let mut zone = None;
		let mut key = None;
		let mut server = None;
		let mut keyfile = None;
		let mut nameserver = None;
		with_words(config, |word|{
			if let Some(_zone) = strip_prefix(word, "zone=") {
				zone = Some(DnsNameBuf::from_master(_zone.as_bytes()).set_err("Bad name in zone=")?);
			} else if let Some(_key) = strip_prefix(word, "key=") {
				key = Some(DnsNameBuf::from_master(_key.as_bytes()).set_err("Bad name in key=")?);
			} else if let Some(_server) = strip_prefix(word, "server=") {
				server = Some(IpAddr::from_str(_server).set_err("Bad IP address in server=")?);
			} else if let Some(_keyfile) = strip_prefix(word, "secret=") {
				keyfile = Some(Path::new(_keyfile).to_path_buf());
			} else if let Some(_nameserver) = strip_prefix(word, "nameserver=") {
				nameserver = Some(IpAddr::from_str(_nameserver).
					set_err("Bad IP address in nameserver=")?);
			} else {
				fail!(format!("Unsupported configuration '{word}'"));
			}
			Ok(())
		})?;

		let zone = zone.ok_or_else(||format!("zone= required"))?;
		let key = key.ok_or_else(||format!("key= required"))?;
		Self::new_tail(zone, key, server, keyfile, nameserver)
	}
	pub(crate) fn new_tail(zone: DnsNameBuf, key: DnsNameBuf, server: Option<IpAddr>, keyfile: Option<PathBuf>,
		nameserver: Option<IpAddr>) -> Result<Config, String>
	{
		let key2 = TsigKey::parse_file(&key, keyfile.as_deref()).map_err(|err|{
			format!("Failed to load key: {err}")
		})?;

		//Recursive nameserver.
		let nameserver = if let Some(nameserver) = nameserver {
			nameserver
		} else {
			let mut rc = String::new();
			let mut nameserver = None;
			File::open("/etc/resolv.conf").and_then(|mut fp|fp.read_to_string(&mut rc)).map_err(|err|{
				format!("Failed to read /etc/resolv.conf: {err}")
			})?;
			for line in rc.lines() {
				let comment = line.find("#").unwrap_or(line.len());
				let line = trim_string(&line[..comment]);
				//Only interested in nameserver lines.
				if let Some(ns) = strip_prefix(line, "nameserver") {
					if !ns.starts_with(" ") && !ns.starts_with("\t") { continue; }
					let ns = trim_string(ns);
					let ns = IpAddr::from_str(ns).map_err(|_|{
						format!("Bad 'nameserver {ns}' in resolv.conf")
					})?;
					nameserver = Some(ns);
					break;
				}
			}
			nameserver.ok_or_else(||format!("No nameserver found from /etc/resolv.conf"))?
		};
		let mut nameserver = DnsConnection::new(&dns_server_socket_addr(nameserver));

		//Zone server.
		let zoneserver = if let Some(zoneserver) = server {
			zoneserver
		} else {
			let zoneservers = fetch_zone_servers(&mut nameserver, &zone).map_err(|err|{
				format!("Failed to find nameserver addresses for {zone}: {err}")
			})?;
			let mut zoneserver = None;
			for zs in zoneservers.iter() {
				//Ignore addresses where this fails. Use first address where this works.
				if TcpStream::connect(dns_server_socket_addr(*zs)).is_ok() {
					zoneserver = Some(*zs);
					break;
				}
			}
			zoneserver.ok_or_else(||{
				format!("{zone} is a zone, but has no reachable nameservers!")
			})?
		};
		let zoneserver = DnsConnection::new(&dns_server_socket_addr(zoneserver));

		Ok(Config {
			zone: zone,
			zoneserver: zoneserver,
			nameserver: nameserver,
			key: key2,
		})
	}
}

fn dns_server_socket_addr(ns: IpAddr) -> SocketAddr
{
	match ns {
		IpAddr::V6(x) => SocketAddr::V6(SocketAddrV6::new(x, 53, 0, 0)),
		IpAddr::V4(x) => SocketAddr::V4(SocketAddrV4::new(x, 53)),
	}
}

fn trim_string(x: &str) -> &str
{
	let y = x.as_bytes();
	let mut ss = 0;
	let mut se = y.len();
	while ss < y.len() && (y[ss] == 9 || y[ss] == 32) { ss += 1; }
	while se > ss && (y[se-1] == 9 || y[se-1] == 32) { se -= 1; }
	&x[ss..se]
}
