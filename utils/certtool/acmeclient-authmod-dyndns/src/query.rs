use crate::connection::DnsConnection;
use btls_aux_dnsmsg::dns_format_query;
use btls_aux_dnsmsg::Class as DnsClass;
use btls_aux_dnsmsg::Dns0x20;
use btls_aux_dnsmsg::Message as DnsMessage;
use btls_aux_dnsmsg::Name as DnsName;
use btls_aux_dnsmsg::NameBuf as DnsNameBuf;
use btls_aux_dnsmsg::Rdata as DnsRdata;
use btls_aux_dnsmsg::Record as DnsRecord;
use btls_aux_dnsmsg::Type as DnsType;
use btls_aux_dnsmsg::consts::DNSTYPE_A;
use btls_aux_dnsmsg::consts::DNSTYPE_AAAA;
use btls_aux_dnsmsg::consts::DNSTYPE_CNAME;
use btls_aux_dnsmsg::consts::DNSTYPE_SOA;
use std::net::IpAddr;
use std::net::Ipv4Addr;
use std::net::Ipv6Addr;

const CLASS: DnsClass = btls_aux_dnsmsg::consts::DNSCLASS_IN;
const OPCODE_QUERY: u8 = 0;


fn dns_query_message(zone: &DnsName, txid: u16, qtype: DnsType) -> Vec<u8>
{
	let mut buf = Vec::new();
	//This can not fail.
	dns_format_query(zone, CLASS, qtype, txid, &Dns0x20::new(), false, &mut buf).ok();
	buf
}

fn for_each_record<F>(answer: &[u8], expect_txid: u16, qname: &DnsName, qtype: DnsType, mut f: F) ->
	Result<(), String> where F: FnMut(DnsRecord)
{
	let answer = DnsMessage::decode(answer, |_,_,_,_,_,_|()).map_err(|err|{
		format!("Failed to decode DNS query reply: {err}")
	})?;
	let header = answer.header();
	fail_if!(header.id != expect_txid, format!("DNS answer has wrong TXID {id} != {expect_txid}", id=header.id));
	fail_if!(!header.qr, format!("DNS answer has QR bit unset"));
	fail_if!(header.opcode != OPCODE_QUERY, format!("DNS answer has bad OPCODE{op}", op=header.opcode));
	fail_if!(header.tc, format!("DNS answer has TC bit set"));
	fail_if!(!header.rd, format!("DNS answer has RD bit clear"));
	fail_if!(!header.ra, format!("DNS answer has RA bit clear"));
	fail_if!(header.rcode != 0, format!("DNS answer has bad RCODE{rcode}", rcode=header.rcode));
	let questions = answer.question_v();
	fail_if!(questions.len() != 1, format!("DNS answer has bad QDCOUNT={qdcount}", qdcount=questions.len()));
	let question = &questions[0];
	fail_if!(question.qname != qname, format!("DNS answer has wrong QNAME"));
	fail_if!(question.qtype != qtype, format!("DNS answer has wrong QTYPE"));
	fail_if!(question.qclass != CLASS, format!("DNS answer has wrong QCLASS"));
	for record in answer.answer().chain(answer.authority()).chain(answer.additional()) { f(record); }
	Ok(())
}

fn cname_soa_common_fetch(socket: &mut DnsConnection, qname: &DnsName, qtype: DnsType) ->
	Result<Option<DnsNameBuf>, String>
{
	let use_txid = socket.allocate_transaction();
	let answer = socket.transact(&dns_query_message(qname, use_txid, qtype))?;
	let mut xname = None;
	let mut error = None;
	for_each_record(&answer, use_txid, qname, qtype, |rr|{
		//Is CNAME/SOA for query target? If no, skip.
		if rr.owner != qname || rr.rtype != qtype || rr.clazz != CLASS { return; }
		let name = match rr.data {
			DnsRdata::SName{target, ..} => target.decompress_owned(),
			DnsRdata::Soa{mname, ..} => mname.decompress_owned(),
			_ => return error = Some(format!("CNAME/SOA is not SName or Soa?"))
		};
		xname = Some(name);
	})?;
	if let Some(error) = error { fail!(error); }
	Ok(xname)
}

pub fn fetch_address_records(socket: &mut DnsConnection, qname: &DnsName) -> Result<Vec<IpAddr>, String>
{
	let use_txid1 = socket.allocate_transaction();
	let use_txid2 = socket.allocate_transaction();
	let mut a = Vec::new();

	let xtype = DNSTYPE_AAAA;
	let answer = socket.transact(&dns_query_message(qname, use_txid1, xtype)).map_err(|err|{
		format!("Failed to look up {qname} AAAA: {err}")
	})?;
	for_each_record(&answer, use_txid1, qname, xtype, |rr|{
		let mut rdata = Vec::<u8>::new();
		rr.data.uncompress(&mut rdata).ok();	//Can not fail.
		if rr.owner != qname || rr.clazz != CLASS || rr.rtype != xtype || rdata.len() != 16 { return; }
		let mut addr = [0;16];
		addr.copy_from_slice(&rdata);
		a.push(IpAddr::V6(Ipv6Addr::from(addr)));
	}).map_err(|err|{
		format!("Failed to look up {qname} AAAA: {err}")
	})?;

	let xtype = DNSTYPE_A;
	let answer = socket.transact(&dns_query_message(qname, use_txid2, xtype)).map_err(|err|{
		format!("Failed to look up {qname} A: {err}")
	})?;
	for_each_record(&answer, use_txid2, qname, xtype, |rr|{
		let mut rdata = Vec::<u8>::new();
		rr.data.uncompress(&mut rdata).ok();	//Can not fail.
		if rr.owner != qname || rr.clazz != CLASS || rr.rtype != xtype || rdata.len() != 4 { return; }
		let mut addr = [0;4];
		addr.copy_from_slice(&rdata);
		a.push(IpAddr::V4(Ipv4Addr::from(addr)));
	}).map_err(|err|{
		format!("Failed to look up {qname} A: {err}")
	})?;

	Ok(a)
}

pub fn fetch_zone_servers(socket: &mut DnsConnection, zone: &DnsName) ->
	Result<Vec<IpAddr>, String>
{
	let soa_mname = cname_soa_common_fetch(socket, zone, DNSTYPE_SOA).map_err(|err|{
		format!("Failed to look up {zone} SOA: {err}")
	})?;
	let soa_mname = soa_mname.ok_or_else(||{
		format!("{zone} is not a zone apex")
	})?;
	fetch_address_records(socket, &soa_mname).map_err(|err|{
		format!("Failed to look up addresses of {soa_mname}: {err}")
	})
}

pub fn fetch_cname_target(socket: &mut DnsConnection, qname: &DnsName) -> Result<DnsNameBuf, String>
{
	let cname = cname_soa_common_fetch(socket, qname, DNSTYPE_CNAME).map_err(|err|{
		format!("Failed to look up {qname} CNAME: {err}")
	})?;
	cname.ok_or_else(||format!("{qname} is not a CNAME"))
}
