#![warn(unsafe_op_in_unsafe_fn)]
#[cfg_attr(not(feature = "main"), macro_use)]
extern crate acmeclient_authmod_helper;
#[macro_use] extern crate btls_aux_fail;
extern crate btls_aux_serialization;
use acmeclient_authmod_helper::ChallengeInfo;
use acmeclient_authmod_helper::DNS_01;
use acmeclient_authmod_helper::DNS01_OWNER_NAME;
use acmeclient_authmod_helper::DNS01_RESPONSE;
use acmeclient_authmod_helper::DynInstance;
use acmeclient_authmod_helper::Instance;
use acmeclient_authmod_helper::MethodId;
use btls_aux_dnsmsg::NameBuf as DnsNameBuf;
use btls_aux_fail::ResultExt;
use config::Config;
#[cfg(test)] use connection::DnsConnection;
#[cfg(test)] use query::fetch_address_records;
use query::fetch_cname_target;
#[cfg(test)] use query::fetch_zone_servers;
#[cfg(test)] use std::net::IpAddr;
#[cfg(test)] use std::net::SocketAddr;
#[cfg(test)] use std::str::FromStr;
#[cfg(test)] use update::DnsUpdateMessage;
#[cfg(test)] use update::TsigKey;
use update::UpdatedRecords;
#[cfg(not(feature = "main"))] use std::net::IpAddr;
use std::path::PathBuf;
use std::time::Duration;


mod config;
mod connection;
mod query;
mod update;

struct State
{
	config: Config,
	records: UpdatedRecords,
}

impl State
{
	fn new(config: Config) -> State
	{
		State {
			config: config,
			records: UpdatedRecords::new(),
		}
	}
	fn add(&mut self, name: &str, content: &str) -> Result<(), String>
	{
		let name = DnsNameBuf::from_master(name.as_bytes()).map_err(|_|{
			format!("{name} is not a valid DNS name")
		})?;
		let cname = fetch_cname_target(&mut self.config.nameserver, &name)?;
		fail_if!(!cname.in_zone_of(&self.config.zone),
			format!("{name} points to {cname}, which is not in {zone}", zone=self.config.zone));
		self.records.add_record(&cname, content.as_bytes());
		Ok(())
	}
	fn send_addition(&mut self) -> Result<(), String>
	{
		self.records.send_add_all(&self.config.zone, &mut self.config.zoneserver, &self.config.key)
	}
	fn send_removal(&mut self) -> Result<(), String>
	{
		self.records.send_remove_all(&self.config.zone, &mut self.config.zoneserver, &self.config.key)
	}
	fn clear(&mut self, zone: &str, cleanup: bool)
	{
		if cleanup { if let Err(err) = self.send_removal() {
			errmsg!("dyndns[{zone}]: Error cleaning up records: {err}");
		}}
		self.records.clear();
	}
}

pub struct AuthHandle
{
	zone: String,
	state: Result<State, String>,
	send_cleanup: bool,
}

impl Drop for AuthHandle
{
	fn drop(&mut self) { self.clear(); }
}

impl Instance for AuthHandle
{
	fn new(config: &str) -> Self
	{
		AuthHandle{
			zone: Config::get_zone(config),
			state: Config::new(config).map(State::new),
			send_cleanup: false,
		}
	}
}
impl DynInstance for AuthHandle
{
	fn add_challenge(&mut self, ch: &ChallengeInfo) -> Result<MethodId, String>
	{
		let dnsid = ch.method_id(DNS_01).ok_or_else(||format!("dns-01 method not supported (CA)"))?;
		let target = ch.attribute(DNS01_OWNER_NAME).ok_or_else(||{
			format!("dns-01 method not supported (QNAME)")
		})?;
		let content = ch.attribute(DNS01_RESPONSE).ok_or_else(||{
			format!("dns-01 method not supported (VALUE)")
		})?;
		match &mut self.state {
			&mut Ok(ref mut state) => state.add(target, content).map_err(|err|{
				format!("{target} failed: {err}")
			})?,
			&mut Err(ref e) => fail!(e.clone()),
		}
		Ok(dnsid)
	}
	fn commit(&mut self) -> Result<(), String>
	{
		match &mut self.state {
			&mut Ok(ref mut state) => {
				state.send_addition().map_err(|err|{
					format!("Commit failed: {err}")
				})?;
				self.send_cleanup = true;	//Ok, successfully sent, clean up.
				Ok(())
			},
			&mut Err(ref e) => Err(e.clone()),
		}
	}
	fn name(&self) -> String { format!("dyndns[{zone}]", zone=self.zone) }
	fn clear(&mut self) -> Duration
	{
		if let &mut Ok(ref mut state) = &mut self.state {
			state.clear(&self.zone, self.send_cleanup);
			self.send_cleanup = false;		//This already performed clean-up.
		}
		Duration::from_secs(1)		//Wait a second for DNS caches to expire.
	}
}

#[cfg(not(feature = "main"))]
authmod_instance!(AuthHandle);

#[cfg(not(feature = "main"))]
pub struct Factory
{
	///The zone root.
	pub zone: String,
	///Key name.
	pub key: String,
	///Secret key file (if not <key>.tsig).
	pub secret: Option<PathBuf>,
	///Zone server IP address (None to autodetect).
	pub zone_server: Option<IpAddr>,
	///Name server IP address (None to autodetect).
	pub name_server: Option<IpAddr>,
}

#[cfg(not(feature = "main"))]
impl Factory
{
	pub fn make(self) -> Result<AuthHandle, String>
	{
		use btls_aux_dnsmsg::NameBuf;
		let zone = NameBuf::from_master(self.zone.as_bytes()).set_err("Bad zone name")?;
		let key = NameBuf::from_master(self.key.as_bytes()).set_err("Bad key name")?;
		let config = Config::new_tail(zone, key, self.zone_server, self.secret, self.name_server)?;
		Ok(AuthHandle {
			zone: self.zone.clone(),
			state: Ok(State::new(config)),
			send_cleanup: false
		})
	}
}

#[cfg(feature = "main")]
fn split_directive<'a>(x: &'a str) -> (&'a str, &'a str)
{
	let y = x.find(",").expect("No comma");
	(&x[..y], &x[y+1..])
}

#[cfg(feature = "main")]
pub fn main()
{
	let xarg = std::env::args().nth(1).unwrap();
	let cfg = Config::new(&xarg).unwrap();
	let mut state = State::new(cfg);
	for rarg in std::env::args().skip(2) {
		let (name, content) = split_directive(&rarg);
		match state.add(name, content) {
			Ok(_) => println!("{rarg} => OK"),
			Err(err) => println!("{rarg} => Error {err}"),
		}
	}
	if let Err(err) = state.send_addition() { eprintln!("send_addition: {err}"); }
	if let Err(err) = state.send_removal() { eprintln!("send_addition: {err}"); }
}


/*******************************************************************************************************************/

#[test]
fn test_name_wire_empty()
{
	assert!(DnsWireName::new("").is_none());
}

#[test]
fn test_name_wire_root()
{
	assert_eq!(DnsWireName::new(".").unwrap().as_slice(), b"\0");
}

#[test]
fn test_name_wire_twodots()
{
	assert!(DnsWireName::new("..").is_none());
}

#[test]
fn test_name_wire_tld()
{
	assert_eq!(DnsWireName::new("xy").unwrap().as_slice(), b"\x02xy\0");
}

#[test]
fn test_name_wire_sld()
{
	assert_eq!(DnsWireName::new("foo.xy").unwrap().as_slice(), b"\x03foo\x02xy\0");
}

#[test]
fn test_name_wire_3ld()
{
	assert_eq!(DnsWireName::new("quxzot.foo.xy").unwrap().as_slice(), b"\x06quxzot\x03foo\x02xy\0");
}

#[test]
fn test_name_wire_embdot()
{
	assert_eq!(DnsWireName::new("foo\\.zot.xy").unwrap().as_slice(), b"\x07foo.zot\x02xy\0");
}

#[test]
fn test_name_wire_embspace()
{
	assert_eq!(DnsWireName::new("foo\\ zot.xy").unwrap().as_slice(), b"\x07foo zot\x02xy\0");
}

#[test]
fn test_name_wire_embnul()
{
	assert_eq!(DnsWireName::new("foo\\000zot.xy").unwrap().as_slice(), b"\x07foo\0zot\x02xy\0");
}

#[test]
fn test_name_wire_embff()
{
	assert_eq!(DnsWireName::new("foo\\255zot.xy").unwrap().as_slice(), b"\x07foo\xffzot\x02xy\0");
}

#[test]
fn test_name_wire_embff_td()
{
	assert_eq!(DnsWireName::new("foo\\255zot.xy.").unwrap().as_slice(), b"\x07foo\xffzot\x02xy\0");
}

#[test]
fn test_name_wire_badspace()
{
	assert!(DnsWireName::new("foo zot.xy").is_none());
}

#[test]
fn test_name_wire_badesc()
{
	assert!(DnsWireName::new("foo\\256zot.xy").is_none());
}

#[test]
fn dns_message()
{
	let mut socket = DnsConnection::new(&SocketAddr::from_str("[fec0::2]:53").unwrap());
	let key = TsigKey::parse_file(&DnsWireName::new("auth.acmezone.example").unwrap(), None).unwrap();
	let mut msg = DnsUpdateMessage::new(&DnsWireName::new("acmezone.example").unwrap(), 12345);
	msg.add_txt_record(&DnsWireName::new("test.acmezone.example").unwrap(), b"testing3...");
	msg.remove_txt_record(&DnsWireName::new("test.acmezone.example").unwrap(), b"testing...");
	msg.add_edns0();
	let content = msg.add_tsig_and_fixup(&key);
	eprintln!("Query: {content:?}");
	let ans = socket.transact(&content).unwrap();
	
	let mut msg = DnsUpdateMessage::new(&DnsWireName::new("acmezone.example").unwrap(), 12346);
	msg.add_txt_record(&DnsWireName::new("test.acmezone.example").unwrap(), b"testing3...");
	msg.remove_txt_record(&DnsWireName::new("test.acmezone.example").unwrap(), b"testing...");
	msg.add_edns0();
	let content = msg.add_tsig_and_fixup(&key);
	let ans = socket.transact(&content).unwrap();
	eprintln!("Reply: {ans:?}");
	assert_eq!(ans[3] & 15, 0);
}

#[test]
fn dns_query3()
{
	let mut socket = DnsConnection::new(&SocketAddr::from_str("[::1]:53").unwrap());
	let qname = DnsWireName::new("example.org").unwrap();
	let a = fetch_address_records(&mut socket, &qname).unwrap();
	assert_eq!(a.len(), 2);
	assert!(match a[0] { IpAddr::V6(_) => true, _ => false });
	assert!(match a[1] { IpAddr::V4(_) => true, _ => false });
}

#[test]
fn dns_zone_servers()
{
	let mut socket = DnsConnection::new(&SocketAddr::from_str("[::1]:53").unwrap());
	let qname = DnsWireName::new("org").unwrap();
	let a = fetch_zone_servers(&mut socket, &qname).unwrap();
	assert_eq!(a.len(), 2);
	assert!(match a[0] { IpAddr::V6(_) => true, _ => false });
	assert!(match a[1] { IpAddr::V4(_) => true, _ => false });
}

#[test]
fn dns_cname()
{
	let mut socket = DnsConnection::new(&SocketAddr::from_str("[::1]:53").unwrap());
	let qname = DnsWireName::new("www.letsencrypt.org").unwrap();
	let a = fetch_cname_target(&mut socket, &qname).unwrap();
	assert_eq!(a, DnsWireName::new("letsencrypt.netlify.com").unwrap());
}
