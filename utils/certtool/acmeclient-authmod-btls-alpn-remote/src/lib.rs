#![warn(unsafe_op_in_unsafe_fn)]
#[macro_use] extern crate acmeclient_authmod_helper;
extern crate btls;
#[macro_use] extern crate btls_aux_fail;
extern crate btls_aux_memory;
extern crate btls_aux_rope3;
extern crate btls_aux_serialization;
use acmeclient_authmod_helper::ChallengeInfo;
use acmeclient_authmod_helper::DynInstance;
use acmeclient_authmod_helper::Instance;
use acmeclient_authmod_helper::MethodId;
use acmeclient_authmod_helper::TLS_ALPN_01;
use acmeclient_authmod_helper::TLSALPN01_KEY_AUTHORIZATION;
use acmeclient_authmod_helper::TLSALPN01_SNI;
use btls::ClientConfiguration;
use btls::ClientConnection;
use btls::Connection;
use btls::callbacks::SelectClientCertificateParameters;
use btls::callbacks::TlsCallbacks;
use btls::certificates::ClientCertificate;
use btls::certificates::FutureReceiver;
use btls::certificates::Hasher;
use btls::certificates::HostSpecificPin;
use btls::certificates::KeyPair2;
use btls::certificates::LocalKeyPair;
use btls::certificates::SignatureAlgorithmTls2;
use btls::certificates::TemporaryRandomStream;
use btls::transport::PullTcpBuffer;
use btls::transport::PushTcpBuffer;
use btls::transport::Queue;
use btls::utility::Mutex;
use btls_aux_memory::EscapeByteString;
use btls_aux_memory::SafeShowByteString;
use btls_aux_rope3::rope3;
use btls_aux_rope3::FragmentWriteOps;
use std::collections::HashMap;
use std::collections::HashSet;
use std::io::ErrorKind;
use std::io::Read;
use std::io::Write;
use std::mem::swap;
use std::net::SocketAddr;
use std::net::TcpStream;
use std::net::ToSocketAddrs;
use std::ops::Deref;
use std::path::Path;
use std::path::PathBuf;
use std::sync::Arc;
use std::thread::sleep;
use std::time::Instant;
use std::time::Duration;


fn fake_certificate(kp: &dyn KeyPair2) -> Result<(SignatureAlgorithmTls2, Vec<u8>), String>
{
	let scheme = kp.get_signature_algorithms().iter().filter_map(|x|x.downcast_tls()).next().
		ok_or("Keypair does not support any schemes")?;
	let spki = kp.get_public_key2();
	//The certificate can be umm... Interesting.
	let cert = rope3!(SEQUENCE(
		SEQUENCE(
			[0](INTEGER 2u8),
			INTEGER 1u8,
			SEQUENCE(OBJECT_IDENTIFIER 0u8),
			SEQUENCE(),
			SEQUENCE(
				GENERALIZED_TIME "00010101000000Z",
				GENERALIZED_TIME "99991231235959Z"
			),
			SEQUENCE(),
			spki,
			[3](SEQUENCE(
				SEQUENCE(
					OBJECT_IDENTIFIER X509_EXT_EKU,
					OCTET_STRING SEQUENCE(
						OBJECT_IDENTIFIER X509_EKU_TLS_CLIENT
					)
				)
			))
		),
		SEQUENCE(OBJECT_IDENTIFIER 0u8),
		BIT_STRING EPSILON
	));
	let cert = FragmentWriteOps::to_vector(&cert);
	Ok((scheme, cert))
}

struct _HostkeyLookup
{
	keypair: Box<dyn KeyPair2+Send+Sync>,
	certificate: Vec<u8>,
	sign_algorithm: SignatureAlgorithmTls2,
}

impl _HostkeyLookup
{
	fn from_file(path: &Path) -> Result<_HostkeyLookup, String>
	{
		let kp = LocalKeyPair::new_file(path).map_err(|err|{
			format!("Failed to read ACME control key: {err}")
		})?;
		let (sign_algorithm, certificate) = fake_certificate(&kp as &dyn KeyPair2)?;
		Ok(_HostkeyLookup {
			keypair: Box::new(kp),
			certificate: certificate,
			sign_algorithm: sign_algorithm,
		})
	}
	fn sign(&self, data: &mut dyn FnMut(&mut Hasher), rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<(u16, Vec<u8>), ()>>
	{
		btls::certificates::tls_sign_helper(self.keypair.deref(), data, self.sign_algorithm, rng)
	}
}

#[derive(Clone)]
pub struct HostkeyLookup(Arc<_HostkeyLookup>);

impl ClientCertificate for HostkeyLookup
{
	fn sign(&self, data: &mut dyn FnMut(&mut Hasher), rng: &mut TemporaryRandomStream) ->
		FutureReceiver<Result<(u16, Vec<u8>), ()>>
	{
		self.0.deref().sign(data, rng)
	}
	fn certificate<'a>(&'a self) -> &'a [u8] { self.0.certificate.deref() }
	fn chain<'a>(&'a self) -> &'a [Vec<u8>] { &[] }	//Always empty chain.
	//We don't implement get_ocsp nor get_scts. These get replaced by defaults.
}

impl HostkeyLookup
{
	pub fn from_file(path: &Path) -> Result<HostkeyLookup, String>
	{
		Ok(HostkeyLookup(Arc::new(_HostkeyLookup::from_file(path)?)))
	}
}


#[derive(Clone)]
struct ServerOptions
{
	spkis: Vec<[u8;32]>,
	ccert: PathBuf,
}

struct Controller
{
	signer: HostkeyLookup,
}

impl TlsCallbacks for Controller
{
	fn select_client_certificate(&mut self, _parameters: SelectClientCertificateParameters) ->
		Option<Box<dyn ClientCertificate + Send>>
	{
		Some(Box::new(self.signer.clone()))
	}
}

impl Controller
{
	fn new(client_cert: &Path) -> Result<Controller, String>
	{
		Ok(Controller {
			signer: HostkeyLookup::from_file(client_cert)?,
		})
	}
}

fn tcp_read(tls: &mut ClientConnection, socket: &mut TcpStream, rqueue: &mut Queue) -> Result<(), String>
{
	let mut buffer = vec![0;16645];
	let amt = match socket.read(&mut buffer) {
		Ok(0) => fail!(format!("Peer unexpectedly closed the connection")),
		Ok(r) => r,
		Err(ref e) if e.kind() == ErrorKind::WouldBlock => return Ok(()),
		Err(ref e) if e.kind() == ErrorKind::Interrupted => return Ok(()),
		Err(err) => fail!(format!("TCP transport error: {err}"))
	};
	let (tmp, eofd) = tls.push_tcp_data(PushTcpBuffer::u8_size(&buffer, amt)).map_err(|err|{
		format!("TLS transport error: {err}")
	})?;
	rqueue.write(&tmp).ok();
	if eofd { rqueue.send_eof(); }
	Ok(())
}

struct CachedConnection
{
	tls: ClientConnection,
	tcp: TcpStream,
}

impl CachedConnection
{
	fn new(addr: SocketAddr, opts: ServerOptions) -> Result<CachedConnection, String>
	{
		let mut tcp = TcpStream::connect(addr).map_err(|err|{
			format!("Failed to connect: {err}")
		})?;
		let mut client_config = ClientConfiguration::new();
		client_config.add_alpn("$btls-acme-control");
		let controller = Arc::new(Mutex::new(Controller::new(&opts.ccert)?));
		let mut pins = Vec::new();
		for pin in opts.spkis.iter() {
			pins.push(HostSpecificPin::trust_server_key_by_sha256(pin, false, false));
		}
		let mut tls = client_config.make_connection_pinned("btls-acme-control", &pins[..],
			Box::new(controller.clone()));
		//Wait for ACK for 10 seconds.
		let dloff = Duration::from_secs(10);
		tcp.set_read_timeout(Some(dloff)).ok();
		let dl = Instant::now() + dloff;
		let mut inq = Queue::new();
		//This never sends eof, so never can get into dead not aborted.
		while dl > Instant::now() && tls.get_status().is_aborted() {
			//Write if needed.
			if tls.get_status().is_want_tx() {
				let outq = tls.pull_tcp_data(PullTcpBuffer::no_data()).map_err(|err|{
					format!("TLS transport error: {err}")
				})?;
				tcp.write_all(&outq).and_then(|_|tcp.flush()).map_err(|err|{
					format!("TCP transport error: {err}")
				})?;
			}
			//Read if needed.
			if tls.get_status().is_want_rx() {
				tcp_read(&mut tls, &mut tcp, &mut inq)?;
				let mut pbuf = [0;4096];
				if let Some(len) = inq.peek(&mut pbuf) {
					let pbuf = &pbuf[..len];
					if pbuf == b"READY\n" { break; }
					if pbuf.len() > 0 && pbuf[pbuf.len()-1] == 10 {
						return Err(format!("Bad inital response from server: {buf}",
							buf=EscapeByteString(&pbuf[..pbuf.len()-1])));
					}
				}
			}
			sleep(Duration::from_millis(50));
		}
		if let Some(err) = tls.get_error() {
			return Err(format!("TLS error: {err}"));
		}
		if Instant::now() > dl { return Err(format!("Handshake timed out")); }
		Ok(CachedConnection {
			tls: tls,
			tcp: tcp,
		})
	}
	fn do_command(&mut self, command: &str) -> Result<String, String>
	{
		//Send the command.
		let mut command = command.to_owned();
		command.push_str("\n");
		let outq = self.tls.pull_tcp_data(PullTcpBuffer::write(command.as_bytes())).map_err(|err|{
			format!("TLS transport error: {err}")
		})?;
		self.tcp.write_all(&outq).and_then(|_|self.tcp.flush()).map_err(|err|{
			format!("TCP transport error: {err}")
		})?;
		//Wait for ACK for 10 seconds.
		let dloff = Duration::from_secs(10);
		self.tcp.set_read_timeout(Some(dloff)).ok();
		let dl = Instant::now() + dloff;
		let mut inq = Queue::new();
		//Wait for OK.
		loop {
			tcp_read(&mut self.tls, &mut self.tcp, &mut inq)?;
			let mut pbuf = [0;16384];
			if let Some(len) = inq.peek(&mut pbuf) {
				let pbuf = &pbuf[..len];
				if !pbuf.ends_with(b"\n") { continue; }	//Wait for more data.
				if pbuf == b"OK\n" { return Ok(String::new()); }
				if pbuf.starts_with(b"OK ") {
					return Ok(SafeShowByteString(&pbuf[3..pbuf.len()-1]).to_string());
				}
				return Err(format!("Bad command response from server: {buf}",
					buf=EscapeByteString(&pbuf[..pbuf.len()-1])));
			}
			if Instant::now() > dl { return Err(format!("Operation timed out")); }
		}
	}
}

struct AuthHandle
{
	added_names: HashSet<String>,
	dnscache: HashMap<String, Vec<SocketAddr>>,
	connections: HashMap<SocketAddr, CachedConnection>,
	opts: ServerOptions,
	error: Option<String>,
}

impl AuthHandle
{
	fn dnscache_lookup(&mut self, name: &str) -> Vec<SocketAddr>
	{
		if let Some(ent) = self.dnscache.get(name) { return ent.clone(); }
		let mut rname = name;
		if name.starts_with("[") && name.ends_with("]") && !name.contains(":") {
			//IPv4 addresses should not have brackets.
			rname = &name[1..name.len()-1];
		}
		let rname = format!("{rname}:443");
		let addr: Vec<SocketAddr> = match rname.clone().to_socket_addrs() {
			Ok(x) => x.collect(),
			Err(_) => return Vec::new()
		};
		self.dnscache.insert(name.to_owned(), addr.clone());
		return addr;
	}
	fn do_command_socket(&mut self, addr: SocketAddr, command: &str, opts: ServerOptions) ->
		Result<String, String>
	{
		if let Some(conn) = self.connections.get_mut(&addr) {
			//Try to use this existing connection. If successful, fine.
			if let Ok(result) = conn.do_command(command) {
				return Ok(result);
			}
		}
		//This connection is BAD. Dump it.
		self.connections.remove(&addr);
		//Establish a new connection. If this does not work, actually fail. Finally cache the connection.
		let mut conn = CachedConnection::new(addr, opts)?;
		let result = conn.do_command(command)?;
		self.connections.insert(addr, conn);
		Ok(result)
	}
	fn do_command2(&mut self, name: &str, command: &str, opts: ServerOptions) ->
		Result<Vec<String>, String>
	{
		let addrs = self.dnscache_lookup(name);
		let mut responses = Vec::new();
		if addrs.len() == 0 {  return Err(format!("No IP address found for {name}")); }
		for addr in addrs.iter() {
			//This can query multiple servers, so no returning the responses.
			let response = self.do_command_socket(*addr, command, opts.clone()).map_err(|err|{
				format!("[{addr}]: {err}")
			})?;
			responses.push(response);
		}
		Ok(responses)
	}
	fn do_provision(&mut self, name: &str, authorization: &str, opts: ServerOptions) -> Result<(), String>
	{
		let cmd = format!("set {name} {authorization}");
		//Set should not return a response.
		self.do_command2(name, &cmd, opts)?;
		trace!("btls-alpn-remote: Provisioned challenge for {name}");
		self.added_names.insert(name.to_owned());
		Ok(())
	}
	fn do_clear(&mut self, name: &str, opts: ServerOptions) -> Result<(), String>
	{
		let cmd = format!("clear {name}");
		//Clear should not return a response.
		self.do_command2(name, &cmd, opts)?;
		trace!("btls-alpn-remote: Unprovisioned challenge for {name}");
		self.added_names.remove(name);
		Ok(())
	}
	fn do_rollback(&mut self)
	{
		let mut tmp = HashSet::new();
		swap(&mut tmp, &mut self.added_names);
		let opts = self.opts.clone();
		for name in tmp.iter() {
			if let Err(err) = self.do_clear(name, opts.clone()) {
				errmsg!("btls-alpn-remote: Failed to clean up for {name}: {err}");
			}
		}
	}
}

impl Drop for AuthHandle
{
	fn drop(&mut self)
	{
		self.do_rollback();
	}
}

impl AuthHandle
{
	fn new_failed(error: String) -> AuthHandle
	{
		AuthHandle {
			error: Some(error),
			//Just some valid values.
			added_names: HashSet::new(),
			dnscache: HashMap::new(),
			connections: HashMap::new(),
			opts: ServerOptions {
				spkis: Vec::new(),
				ccert: Path::new("/").to_path_buf(),
			}
		}
	}
}

impl Instance for AuthHandle
{
	fn new(config: &str) -> Self
	{
		LocalKeyPair::disable_module_loading();
		let mut config = config.split('!');
		{
			//Need to process load directives before loading the key.
			let config2 = config.clone();
			for hspki in config2 {
				if hspki.starts_with("load=") {
					let hspki = &hspki[5..];
					if let Err(err) = LocalKeyPair::load_module_trusted(hspki) {
						let msg = format!("Failed to load module {hspki}: {err}");
						return AuthHandle::new_failed(msg);
					}
					continue;
				}
			}
		}
		let keypath = match config.next() {
			Some(x) => x,
			None => return AuthHandle::new_failed(format!("No control key file found"))
		};
		let mut spkis = Vec::new();
		'a: for hspki in config {
			if hspki.len() != 64 { continue 'a; }
			let mut result = [0;32];
			for (i, c) in hspki.as_bytes().iter().cloned().enumerate() { match c {
				48..=57 => result[i/2] |= (c - 48) << 4 - (i % 2) * 4,
				65..=70 => result[i/2] |= (c - 55) << 4 - (i % 2) * 4,
				97..=102 => result[i/2] |= (c - 87) << 4 - (i % 2) * 4,
				_ => continue 'a
			}}
			spkis.push(result);
		}
		if spkis.len() == 0 { return AuthHandle::new_failed(format!("No Server SPKIs specified")); }
		AuthHandle {
			error: None,
			added_names: HashSet::new(),
			dnscache: HashMap::new(),
			connections: HashMap::new(),
			opts: ServerOptions {
				spkis: spkis,
				ccert: Path::new(keypath).to_path_buf(),
			}
		}
	}
}
impl DynInstance for AuthHandle
{
	fn add_challenge(&mut self, ch: &ChallengeInfo) -> Result<MethodId, String>
	{
		if let Some(error) = self.error.as_ref() { return Err(error.clone()); }
		let httpid = ch.method_id(TLS_ALPN_01).ok_or_else(||format!("tls-alpn-01 method not supported"))?;
		let filename = ch.attribute(TLSALPN01_SNI).
			ok_or_else(||format!("tls-alpn-01 method not supported"))?;
		let content = ch.attribute(TLSALPN01_KEY_AUTHORIZATION).
			ok_or_else(||format!("tls-alpn-01 method not supported"))?;
		//Lowercase the filename.
		let filename = filename.to_ascii_lowercase();
		//Commit.
		let opts = self.opts.clone();
		self.do_provision(&filename, content, opts)?;
		Ok(httpid)
	}
	fn commit(&mut self) -> Result<(), String>
	{
		//Commit is synchronous, so no delay.
		Ok(())
	}
	fn name(&self) -> String { "btls-alpn-remote".to_string() }
	fn clear(&mut self) -> Duration
	{
		//do_rollback already removes all the names.
		self.do_rollback();
		Duration::from_secs(0)		//No need to wait for any caches.
	}
}

authmod_instance!(AuthHandle);
