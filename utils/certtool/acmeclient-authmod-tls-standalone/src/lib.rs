#![warn(unsafe_op_in_unsafe_fn)]
#[macro_use] extern crate acmeclient_authmod_helper;
#[macro_use] extern crate btls_aux_fail;
use acmeclient_authmod_helper::ChallengeInfo;
use acmeclient_authmod_helper::DynInstance;
use acmeclient_authmod_helper::Instance;
use acmeclient_authmod_helper::MethodId;
use acmeclient_authmod_helper::TLS_ALPN_01;
use acmeclient_authmod_helper::TLSALPN01_KEY_AUTHORIZATION;
use acmeclient_authmod_helper::TLSALPN01_SNI;
use acmed2_tls_alpn::AcmeServerCallbacks;
use acmed2_tls_alpn::AcmeServerConnection;
use acmed2_tls_alpn::DualEcDrbg;
use btls_aux_collections::RwLock;
use btls_aux_unix::CmsgParser;
use btls_aux_unix::FileDescriptor;
use btls_aux_unix::FileDescriptorB;
use btls_aux_unix::getrandom;
use btls_aux_unix::OsError;
use btls_aux_unix::Path as UnixPath;
use btls_aux_unix::PollFd;
use btls_aux_unix::PollFlags;
use std::borrow::Cow;
use std::cmp::min;
use std::collections::BTreeMap;
use std::default::Default;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::fmt::Write;
use std::fs::File;
use std::fs::remove_file;
use std::io::Error as IoError;
use std::io::ErrorKind as IoErrorKind;
use std::io::Read as IoRead;
use std::io::Write as IoWrite;
use std::mem::drop;
use std::mem::MaybeUninit;
use std::net::TcpStream;
use std::net::Shutdown;
use std::net::IpAddr;
use std::os::unix::fs::MetadataExt;
use std::os::unix::io::AsRawFd;
use std::os::unix::io::FromRawFd;
use std::os::unix::net::SocketAddr as UnixSocketAddr;
use std::os::unix::net::UnixDatagram;
use std::os::unix::net::UnixListener;
use std::os::unix::net::UnixStream;
use std::panic::AssertUnwindSafe;
use std::panic::catch_unwind;
use std::path::Path;
use std::sync::Arc;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering;
use std::thread::Builder;
use std::thread::JoinHandle;
use std::thread::sleep;
use std::time::Duration;
use std::time::Instant;

struct Configuration
{
	challenge_by_sni: RwLock<BTreeMap<String, (String, String)>>,
}

impl Configuration
{
	fn new() -> Configuration
	{
		Configuration {
			challenge_by_sni: RwLock::new(BTreeMap::new())
		}
	}
}

fn escape<'a>(x: &'a str) -> Cow<'a, str>
{
	//If no control characters, print as-is.
	if !x.chars().any(|c|{
		let c = c as u32;
		c < 32 || c > 126 && c < 160
	}) { return Cow::Borrowed(x) }
	//Contains control characters, need escaping.
	let mut y = String::new();
	for c in x.chars() {
		match c as u32 {
			9 => y.push_str("\\t"),
			10 => y.push_str("\\n"),
			13 => y.push_str("\\r"),
			27 => y.push_str("\\e"),
			92 => y.push_str("\\\\"),
			0..=31|127..=159 => {write!(y, "\\u{{{cp:x}}}", cp=c as u32).ok();},
			_ => y.push(c)
		}
	}
	Cow::Owned(y)
}

impl AcmeServerCallbacks for Configuration
{
	//Perform escape, in case there are some weird characters.
	fn log_error(&self, msg: &str) { errmsg!("tls-standalone: {msg}", msg=escape(msg)); }
	fn log_info(&self, msg: &str)
	{
		//HACK: Shut up about successful validations.
		if msg.find(": Succesful for '").is_some() { return; }
		//FIXME: Do not stuff these into stderr.
		eprintln!("Info: {msg}", msg=escape(msg));
	}
	fn get_challenge(&self, sni_lowercased: &str) -> Option<(String,String)>
	{
		let map = self.challenge_by_sni.read();
		map.get(sni_lowercased).map(|c|c.clone())
	}
	fn successful(&self, sni: &str)
	{
		trace!("tls-standalone: Successful reply for {sni}");
	}
	fn failed(&self, sni: Option<&str>, err: &str)
	{
		let sni = sni.unwrap_or("<NOSNI>");
		trace!("tls-standalone: Validation failed for {sni}: {err}");
	}
}

trait Stream: Send
{
	fn t_as_raw_fd(&self) -> i32;
	fn t_set_read_timeout(&mut self, to: Option<Duration>) -> Result<(), IoError>;
	fn t_set_write_timeout(&mut self, to: Option<Duration>) -> Result<(), IoError>;
	fn t_shutdown(&mut self) -> Result<(), IoError>;
	fn t_read(&mut self, buf: &mut [u8]) -> Result<usize, IoError>;
	fn t_write(&mut self, buf: &[u8]) -> Result<usize, IoError>;
	fn t_set_nonblock(&mut self) -> Result<(), IoError>;
}

impl Stream for UnixStream
{
	fn t_as_raw_fd(&self) -> i32 { self.as_raw_fd() }
	fn t_set_read_timeout(&mut self, to: Option<Duration>) -> Result<(), IoError> { self.set_read_timeout(to) }
	fn t_set_write_timeout(&mut self, to: Option<Duration>) -> Result<(), IoError> { self.set_write_timeout(to) }
	fn t_shutdown(&mut self) -> Result<(), IoError> { self.shutdown(Shutdown::Write) }
	fn t_read(&mut self, buf: &mut [u8]) -> Result<usize, IoError> { self.read(buf) }
	fn t_write(&mut self, buf: &[u8]) -> Result<usize, IoError> { self.write(buf) }
	fn t_set_nonblock(&mut self) -> Result<(), IoError> { self.set_nonblocking(true) }
}

impl Stream for TcpStream
{
	fn t_as_raw_fd(&self) -> i32 { self.as_raw_fd() }
	fn t_set_read_timeout(&mut self, to: Option<Duration>) -> Result<(), IoError> { self.set_read_timeout(to) }
	fn t_set_write_timeout(&mut self, to: Option<Duration>) -> Result<(), IoError> { self.set_write_timeout(to) }
	fn t_shutdown(&mut self) -> Result<(), IoError> { self.shutdown(Shutdown::Write) }
	fn t_read(&mut self, buf: &mut [u8]) -> Result<usize, IoError> { self.read(buf) }
	fn t_write(&mut self, buf: &[u8]) -> Result<usize, IoError> { self.write(buf) }
	fn t_set_nonblock(&mut self) -> Result<(), IoError> { self.set_nonblocking(true) }
}

const MAX_DURATION: u128 = 2_000_000_000;
fn to_i32_saturating(x: u128) -> i32 { min(x, MAX_DURATION) as i32 }

fn is_transient(err: &IoError) -> bool
{
	err.kind() == IoErrorKind::WouldBlock || err.kind() == IoErrorKind::Interrupted
}

trait Task
{
	fn name<'a>(&'a self) -> &'a str;
}

fn with_trap<U:Task,T>(task: &U, func: impl FnOnce(&U) -> T) -> Option<T>
{
	catch_unwind(AssertUnwindSafe(||func(task))).map_err(|_|{
		errmsg!("tls-standalone: Critical: {task}: Fatal exception in handler", task=task.name());
	}).ok()
}

fn with_trap_mut<U:Task,T>(task: &mut U, func: impl FnOnce(&mut U) -> T) -> Option<T>
{
	catch_unwind(AssertUnwindSafe(||func(task))).map_err(|_|{
		errmsg!("tls-standalone: Critical: {task}: Fatal exception in handler", task=task.name());
	}).ok()
}

struct ConnectionTask
{
	fd: FileDescriptorB,
	stream: Box<dyn Stream+'static>,
	complete: bool,
	connection: AcmeServerConnection,
	deadline: Instant,
	addr: String,
	output: Vec<u8>,
	out_eof: bool,
}

impl Task for ConnectionTask
{
	fn name<'a>(&'a self) -> &'a str { &self.addr }
}

impl ConnectionTask
{
	fn setup(mut stream: Box<dyn Stream>, addr: &str, mut drbg: DualEcDrbg, config: Arc<Configuration>) ->
		ConnectionTask
	{
		let connection = AcmeServerConnection::new(&mut drbg, addr, config);

		//Assume these do not fail.
		stream.t_set_read_timeout(Some(Duration::from_secs(10))).ok();
		stream.t_set_write_timeout(Some(Duration::from_secs(10))).ok();
		stream.t_set_nonblock().ok();
		let dl = Instant::now() + Duration::from_secs(15);
		ConnectionTask {
			fd: FileDescriptorB::new(stream.t_as_raw_fd()),
			stream: stream,
			complete: false,
			connection: connection,
			deadline: dl,
			addr: addr.to_owned(),
			output: Vec::new(),
			out_eof: false,
		}
	}
	fn status(&self, tnow: &Instant) -> (bool, i32)
	{
		let w = self.output.len() > 0 || (self.out_eof && !self.complete);
		let dt = if self.deadline > *tnow {
			to_i32_saturating(self.deadline.duration_since(*tnow).as_millis())
		} else {
			0
		};
		(w, dt)
	}
	fn iterate(&mut self) -> bool
	{
		//Try write.
		if self.output.len() > 0 {
			match self.stream.t_write(&self.output) {
				Ok(amt) => {
					//Remove flushed input.
					let clen = self.output.len().saturating_sub(amt);
					for i in 0..clen { self.output[i] = self.output[i+amt]; }
					self.output.truncate(clen);
					//If not all got flushed, try again.
					if self.output.len() > 0 { return true; }
				},
				Err(ref e) if is_transient(e) => return true,	//Try again.
				Err(err) => {
					errmsg!("tls-standalone: {addr}: TCP transport error(W): {err}",
						addr=self.addr);
					return false;
				}
			}
		}
		//If at end, shut down the socket.
		if self.out_eof && !self.complete {
			//Shut down the socket.
			if let Err(err) = self.stream.t_shutdown() {
				if is_transient(&err) { return true; }	//Try again.
				//HACK: Shut up some warnings from race conditions.
				if err.raw_os_error() == Some(107) { return false; }
				errmsg!("tls-standalone: {addr}: TCP transport error(S): {err}", addr=self.addr);
			}
			self.complete = true;
		}
		//Try read.
		let mut pbuf = [0u8;4096];
		let amt = match self.stream.t_read(&mut pbuf) {
			Ok(0) => {
				if !self.complete {
					errmsg!("tls-standalone: {addr}: Received EOF before handshake complete",
						addr=self.addr);
				}
				return false;
			},
			Ok(amt) => amt,
			Err(ref e) if is_transient(e) => return true,	//Wait.
			Err(err) => {
				errmsg!("tls-standalone: {addr}: TCP transport error(R): {err}", addr=self.addr);
				return false;
			}
		};
		let (output, out_eof) = self.connection.transreceive(&pbuf[..amt]);
		self.output = output;
		self.out_eof = out_eof;
		//Need another iteration to flush output.
		true
	}
}

fn listener_thread2(mut listen: impl AcceptsConnections+'static, exit_sig: Arc<AtomicBool>,
	exit_ack: Arc<AtomicBool>, config: Arc<Configuration>)
{
	let mut seed = [0;48];
	let mut drbg = match getrandom(&mut seed, Default::default()) {
		Ok(l) if l == seed.len() => DualEcDrbg::new(&seed),
		Ok(_) => return errmsg!("tls-standalone: Critical: Failed get random data: Short read"),
		//Fall back to /dev/urandom.
		Err(err) if err == OsError::ENOSYS =>
			match File::open("/dev/urandom").and_then(|mut fp|fp.read_exact(&mut seed)) {
			Ok(_) => DualEcDrbg::new(&seed),
			Err(err) => return errmsg!("tls-standalone: Critical: Failed get random data: {err}"),
		},
		Err(err) => return errmsg!("tls-standalone: Critical: Failed get random data: {err}"),
	};
	
	let mut tasktable: BTreeMap<i32, ConnectionTask> = BTreeMap::new();
	let rawlisten = FileDescriptorB::new(listen.t_as_raw_fd());
	let mut polls = Vec::new();
	while !exit_sig.load(Ordering::SeqCst) {
		let mut maxwait: i32 = 200;
		polls.clear();
		//Add rawlisten to polls for read.
		polls.push(PollFd { fd: rawlisten, events: PollFlags::IN, revents: Default::default() });
		//Iterate over connections. Declare garbage here, because connections might end up in garbage
		//here.
		let mut garbage = Vec::new();
		let tnow = Instant::now();
		for (&key, conn) in tasktable.iter() {
			let mode = match with_trap(conn, |conn|conn.status(&tnow)) {
				Some((w, dt)) => {
					maxwait = min(maxwait, dt);
					if w { PollFlags::OUT } else { PollFlags::IN }
				},
				None => {
					//Uh, oh. The task took a trap. Kill it later. Register no events to
					//force it not execute iter handler.
					garbage.push(key);
					PollFlags::default()
				}
			};
			polls.push(PollFd { fd: conn.fd, events: mode, revents: Default::default() });
		}
		//The unsafety is to have the file descriptor open. Just ignore errors, as results are correct.
		unsafe{PollFd::poll(&mut polls, maxwait)}.ok();
		//Iterate over connections.
		let tnow = Instant::now();
		for (pos, (&key, conn)) in tasktable.iter_mut().enumerate() {
			//If not timed out, do iteration.
			let go_on = if tnow < conn.deadline {
				let p = polls[pos+1];
				if p.events.is_in() && p.revents.is_in() || p.events.is_out() && p.revents.is_out() {
					//Kill connection on trap.
					with_trap_mut(conn, |conn|conn.iterate()).unwrap_or(false)
				} else {
					true	//No event for this.
				}
			} else {
				errmsg!("tls-standalone: {addr}: TLS handshake timed out", addr=conn.addr);
				false
			};
			//Get rid of connections that are no longer needed.
			if !go_on { garbage.push(key); }
		}
		//Collect garbage tasks.
		for key in garbage.drain(..) { tasktable.remove(&key); }
		if polls[0].revents.is_in() {
			//There is connection waiting. Add to task table.
			match listen.t_accept() {
				Ok((c, addr)) => {
					let task = ConnectionTask::setup(Box::new(c), &addr.to_string(),
						DualEcDrbg::fork(&mut drbg), config.clone());
					let fd = task.fd.as_raw_fd();
					tasktable.insert(fd, task);
				},
				Err(err) => errmsg!("tls-standalone: Failed to accept connection: {err}")
			}
		}
	}
	drop(listen);
	exit_ack.store(true, Ordering::SeqCst);
}



struct UnixSocketAddrW(UnixSocketAddr);

impl Display for UnixSocketAddrW
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		if let Some(p) = self.0.as_pathname() {
			p.display().fmt(f)
		} else {
			f.write_str("Unix:<unknown>")
		}
	}
}

trait AcceptsConnections: Send
{
	type Connection: Stream;
	type Address: Display+Sized;
	type Error: Display;
	fn t_as_raw_fd(&self) -> i32;
	fn t_accept(&mut self) -> Result<(<Self as AcceptsConnections>::Connection,
		<Self as AcceptsConnections>::Address), <Self as AcceptsConnections>::Error>;
}

impl AcceptsConnections for UnixListener
{
	type Connection = UnixStream;
	type Address = UnixSocketAddrW;
	type Error = IoError;
	fn t_as_raw_fd(&self) -> i32 { self.as_raw_fd() }
	fn t_accept(&mut self) -> Result<(UnixStream, UnixSocketAddrW), IoError>
	{
		self.accept().map(|(a,b)|(a, UnixSocketAddrW(b)))
	}
}

impl AcceptsConnections for UnixDatagram
{
	type Connection = TcpStream;
	type Address = IpAddr;
	type Error = IoError;
	fn t_as_raw_fd(&self) -> i32 { self.as_raw_fd() }
	fn t_accept(&mut self) -> Result<(TcpStream, IpAddr), IoError>
	{
		let gate = FileDescriptorB::new(self.as_raw_fd());
		let mut control = [MaybeUninit::uninit();128];		//Should be big enogugh
		let mut control = CmsgParser::new2(&mut control);
		let mut buffer = [0;128];
		//Unsafety is having the gate open.
		let buffer = match unsafe{gate.recvmsg(false, &[&mut buffer[..]], Some(&mut control),
			Default::default())} {
			Ok((len, _)) => &buffer[..len],
			Err(e) => return Err(IoError::from_raw_os_error(e.to_inner()))
		};
		let mut fd = -1;
		control.iterate(|mtype, payload|{
			if false { return Err(()); }		//Type inference.
			use ::btls_aux_unix::CmsgKind;
			if mtype == CmsgKind::SCM_RIGHTS {
				//Ok, pick the file descriptors.
				for i in 0..payload.len() / 4 {
					let mut fdx = [0u8;4];
					fdx.copy_from_slice(&payload[4*i..4*i+4]);
					let fdx = i32::from_le_bytes(fdx);
					if fd < 0 {
						fd = fdx;
					} else {
						//No space for this, close the fd.
						unsafe{FileDescriptor::new(fdx)};
					}
				}
			}
			Ok(())
		}).ok();
		//Check we got a file descriptor and that connection type is "ACME".
		if fd < 0 { return Err(IoError::from_raw_os_error(OsError::ENODEV.to_inner())); }
		if buffer != b"ACME" {
			unsafe{FileDescriptor::new(fd)};
			return Err(IoError::from_raw_os_error(OsError::EAFNOSUPPORT.to_inner()));
		}
		//Cast this into TCP stream. The unsafety is having the connection open, which it is.
		let s = unsafe{TcpStream::from_raw_fd(fd)};
		let mut a = s.peer_addr().map(|s|s.ip()).unwrap_or(IpAddr::from([0u16;8]));
		//If possible, downgrade address to IPv4.
		if let IpAddr::V6(a2) = a { if &a2.segments()[..6] == [0,0,0,0,0,65535] {
			let mut t = [0;4];
			t.copy_from_slice(&a2.octets()[12..]);
			a = IpAddr::from(t);
		}}
		Ok((s, a))
	}
}

enum Listeners
{
	Direct(UnixListener),
	Tpgate(UnixDatagram),
}

fn spawn_listener_thread(listener: impl AcceptsConnections+'static, exit_sig: &Arc<AtomicBool>,
	config: &Arc<Configuration>, threads: &mut Vec<(JoinHandle<()>, Arc<AtomicBool>)>) ->
	Result<(), AuthHandle>
{
	let exit_sig = exit_sig.clone();
	let ack = Arc::new(AtomicBool::new(false));
	let ack2 = ack.clone();
	let builder = Builder::new();
	let builder = builder.name("TLS listener".to_owned());
	let config = config.clone();
	match builder.spawn(||listener_thread2(listener, exit_sig, ack, config)) {
		Ok(t) => threads.push((t, ack2)),
		Err(err) => return Err(AuthHandle::new_failed(format!("Failed to spawn: {err}")))
	};
	Ok(())
}

pub struct AuthHandle
{
	error: Option<String>,
	threads: Vec<(JoinHandle<()>, Arc<AtomicBool>)>,
	exit_sig: Arc<AtomicBool>,
	config: Arc<Configuration>,
}

impl AuthHandle
{
}

impl Drop for AuthHandle
{
	fn drop(&mut self)
	{
		self.exit_sig.store(true, Ordering::SeqCst);
		while self.threads.len() > 0 {
			//If flag is set, remove thread.
			if (self.threads[0].1).load(Ordering::SeqCst) {
				self.threads.remove(0);
			} else {
				sleep(Duration::new(0, 100000000));
			}
		}
	}
}

impl AuthHandle
{
	fn new_failed(error: String) -> AuthHandle
	{
		AuthHandle {
			error: Some(error),
			//Just some valid values. However, exit_ack must be
			threads: Vec::new(),
			exit_sig: Arc::new(AtomicBool::new(true)),
			config: Arc::new(Configuration::new()),
		}
	}
}

fn check_dir_perms(path: &str) -> Result<(), String>
{
	let path = Path::new(path);
	let path = path.parent();
	//If path was local, then parent is "".
	let path = if path.map(|x|x.as_os_str().len() == 0).unwrap_or(true) {
		Path::new(".")
	} else {
		path.unwrap()	//This must be some()
	};
	let metadata = path.metadata().map_err(|err|{
		format!("Failed to stat {path}: {err}", path=path.display())
	})?;
	if metadata.mode() & 2 != 0 {
		return Err(format!("Critial error: {path} is world-writable", path=path.display()));
	}
	Ok(())
}

impl AuthHandle
{
	fn new_tail(mut listeners: Vec<Listeners>) -> Self
	{
		if listeners.len() == 0 { return AuthHandle::new_failed(format!("No addresses to listen")); }
		let aconfig = Arc::new(Configuration::new());
		//Fire a thread per listener.
		let mut threads = Vec::new();
		let exit_sig = Arc::new(AtomicBool::new(false));
		for listener in listeners.drain(..) { if let Err(e) = match listener {
			Listeners::Direct(l) => spawn_listener_thread(l, &exit_sig, &aconfig, &mut threads),
			Listeners::Tpgate(l) => spawn_listener_thread(l, &exit_sig, &aconfig, &mut threads),
		} {
			return e;
		}}
		AuthHandle {
			error: None,
			threads: threads,
			exit_sig: exit_sig,
			config: aconfig,
		}
	}
	fn push_listener(listeners: &mut Vec<Listeners>, is_tpgate: bool, spec: &str) -> Option<String>
	{
		if let Err(e) = check_dir_perms(spec) { return Some(e); }
		remove_file(spec).ok();	//In case it exists.
		let ret = if is_tpgate {
			UnixDatagram::bind(spec).map(|l|listeners.push(Listeners::Tpgate(l)))
		} else {
			UnixListener::bind(spec).map(|l|listeners.push(Listeners::Direct(l)))
		};
		if let Err(err) = ret {
			return Some(format!("Failed to listen {spec}: {err}"));
		}
		//Force 777 mode so anything can connect.
		UnixPath::new(spec.as_bytes()).and_then(|p|p.chmod(511).ok());
		None
	}
}

impl Instance for AuthHandle
{
	fn new(config: &str) -> Self
	{
		let mut listeners = Vec::new();
		for spec in config.split('!') {
			let (is_tpgate, spec) = if spec.starts_with("tpgate:") {
				(true, &spec[7..])
			} else {
				(false, spec)
			};
			if let Some(e) = Self::push_listener(&mut listeners, is_tpgate, spec) {
				return AuthHandle::new_failed(e);
			}
		}
		Self::new_tail(listeners)
	}
}
impl DynInstance for AuthHandle
{
	fn add_challenge(&mut self, ch: &ChallengeInfo) -> Result<MethodId, String>
	{
		if let Some(error) = self.error.as_ref() { return Err(error.clone()); }
		let httpid = ch.method_id(TLS_ALPN_01).ok_or_else(||format!("tls-alpn-01 method not supported"))?;
		let filename = ch.attribute(TLSALPN01_SNI).
			ok_or_else(||format!("tls-alpn-01 method not supported"))?;
		let content = ch.attribute(TLSALPN01_KEY_AUTHORIZATION).
			ok_or_else(||format!("tls-alpn-01 method not supported"))?;
		//Split at .
		let (p1, p2) = match content.find('.') {
			Some(p) => (&content[..p], &content[p+1..]),	//'.' is 1 byte.
			None => return Err("Invalid tls-alpn-01 key authorization".to_owned())
		};
		//Lowercase the filename.
		let filename = filename.to_ascii_lowercase();
		//Commit.
		let mut map = self.config.challenge_by_sni.write();
		map.insert(filename.clone(), (p1.to_owned(), p2.to_owned()));
		trace!("tls-standalone: Provisioned challenge for {filename}");
		Ok(httpid)
	}
	fn commit(&mut self) -> Result<(), String>
	{
		//Commit is synchronous, so no delay.
		Ok(())
	}
	fn name(&self) -> String { "tls-standalone".to_string() }
	fn clear(&mut self) -> Duration
	{
		let mut map = self.config.challenge_by_sni.write();
		for (name, _) in map.iter() {
			trace!("tls-standalone: Unprovisioned challenge for {name}");
		}
		map.clear();
		Duration::from_secs(0)		//No need to wait for any caches.
	}
}

authmod_instance!(AuthHandle);

fn to_fail(x: Option<String>) -> Result<(), String>
{
	match x { Some(x) => Err(x), None => Ok(()) }
}

pub struct Factory
{
	pub listeners: Vec<String>,
	pub tp_gates: Vec<String>,
}

impl Factory
{
	pub fn make(self) -> Result<AuthHandle, String>
	{
		let mut listeners = Vec::new();
		for spec in self.listeners.iter() {
			to_fail(AuthHandle::push_listener(&mut listeners, false, spec))?;
		}
		for spec in self.tp_gates.iter() {
			to_fail(AuthHandle::push_listener(&mut listeners, true, spec))?;
		}
		let mut ah = AuthHandle::new_tail(listeners);
		if let Some(error) = ah.error.take() { fail!(error); }
		Ok(ah)
	}
}
