extern crate acmeclient_authmod_tls_standalone;
extern crate acmeclient_authmod_helper;
use crate::acmeclient_authmod_helper::DynInstance;
use crate::acmeclient_authmod_helper::Instance;
use acmeclient_authmod_tls_standalone::AuthHandle;
use std::env::args;
use std::mem::transmute;
use std::marker::PhantomData;
use std::ops::Deref;
use std::thread::sleep;
use std::time::Duration;

#[repr(C)]
struct RawString<'a>
{
	base: *const u8,
	size: usize,
	phantom: PhantomData<&'a [u8]>
}

impl<'a> RawString<'a>
{
	fn new(x: &'a str) -> RawString<'a>
	{
		RawString {
			base: x.as_ptr(),
			size: x.len(),
			phantom: PhantomData,
		}
	}
}

#[repr(C)]
struct ChallengeAttribute<'a>
{
	_name: RawString<'a>,
	_value: RawString<'a>,
}

impl<'a> ChallengeAttribute<'a>
{
	fn new(name: &'a str, value: &'a str) -> ChallengeAttribute<'a>
	{
		ChallengeAttribute {
			_name: RawString::new(name),
			_value: RawString::new(value),
		}
	}
}

#[repr(C)]
struct ChallengeMethod<'a>
{
	_name: RawString<'a>,
}

impl<'a> ChallengeMethod<'a>
{
	fn new(name: &'a str) -> ChallengeMethod<'a>
	{
		ChallengeMethod {
			_name: RawString::new(name),
		}
	}
}

#[repr(C)]
struct ChallengeInfo<'a>
{
	kind: RawString<'a>,
	name: RawString<'a>,
	wildcard: i32,
	_attributes: *const ChallengeAttribute<'a>,
	nattributes: usize,
	_methods: *const ChallengeMethod<'a>,
	nmethods: usize,
}

impl<'a> ChallengeInfo<'a>
{
	fn new(kind: &'a str, name: &'a str, wildcard: bool, attributes: &'a [ChallengeAttribute<'a>],
		methods: &'a [ChallengeMethod<'a>]) -> ChallengeInfo<'a>
	{
		ChallengeInfo {
			kind: RawString::new(kind),
			name: RawString::new(name),
			wildcard: if wildcard { 1 } else { 0 },
			_attributes: attributes.as_ptr(),
			nattributes: attributes.len(),
			_methods: methods.as_ptr(),
			nmethods: methods.len(),
		}
	}
}


fn main()
{
	let mut argv = args();
	argv.next();	//Program name.
	let config = argv.next().expect("Expected config string");
	let mut handle = AuthHandle::new(&config);
	let meths = [ChallengeMethod::new("tls-alpn-01")];
	for arg in argv {
		let arg = arg.deref();
		let mut parts = arg.split(',');
		let ident = parts.next().expect("Expected identifier");
		let mut attribs = Vec::new();
		attribs.push(ChallengeAttribute::new("TLSALPN01_SNI", ident));
		for part in parts {
			let mut parts = part.splitn(2, '=');
			let name = parts.next().expect("Expected attribute name");
			let value = parts.next().expect("Expected attribute value");
			attribs.push(ChallengeAttribute::new(name, value));
		}
		let challenge = ChallengeInfo::new("dns", ident, false, &attribs, &meths);
		if let Err(err) = handle.add_challenge(unsafe{transmute(&challenge)}) {
			eprintln!("add_challenge returned failure ({err})!");
		}
	}
	//Wait.
	loop { sleep(Duration::from_secs(1)); }
}
