#![warn(unsafe_op_in_unsafe_fn)]
use std::fmt::Arguments;
use std::marker::PhantomData;
use std::mem::transmute;
use std::panic::AssertUnwindSafe;
use std::panic::catch_unwind;
use std::slice::from_raw_parts;
use std::str::from_utf8_unchecked;


#[repr(C)]
struct RawString<'a>
{
	base: *const u8,
	size: usize,
	phantom: PhantomData<&'a [u8]>
}

impl<'a> RawString<'a>
{
	fn to_str(&self) -> &'a str
	{
		//These always consist of just ASCII, which impiles they are valid UTF-8.
		unsafe{from_utf8_unchecked(from_raw_parts(self.base, self.size))}
	}
}

#[repr(C)]
struct ChallengeAttribute<'a>
{
	_name: RawString<'a>,
	_value: RawString<'a>,
}

impl<'a> ChallengeAttribute<'a>
{
	fn name(&self) -> &'a str { self._name.to_str() }
	fn value(&self) -> &'a str { self._value.to_str() }
}

#[repr(C)]
struct ChallengeMethod<'a>
{
	_name: RawString<'a>,
}

impl<'a> ChallengeMethod<'a>
{
	fn name(&self) -> &'a str { self._name.to_str() }
}

///Identifier to authorize.
pub struct Identifier<'a>
{
	///The kind of identifier (e.g., 'dns').
	pub kind: &'a str,
	///The actual identifier.
	pub name: &'a str,
	///Is wildcard?
	pub wildcard: bool,
}

///Opaque id of method.
pub struct MethodId(usize);

pub const DNS_01: &'static str = "dns-01";
pub const DNS01_KEY_AUTHORIZATION: &'static str = "DNS01_KEY_AUTHORIZATION";
pub const DNS01_OWNER_NAME: &'static str = "DNS01_OWNER_NAME";
pub const DNS01_RESPONSE: &'static str = "DNS01_RESPONSE";
pub const DNS01_TOKEN: &'static str = "DNS01_TOKEN";
pub const HTTP_01: &'static str = "http-01";
pub const HTTP01_CONTENT: &'static str = "HTTP01_CONTENT";
pub const HTTP01_FILENAME: &'static str = "HTTP01_FILENAME";
pub const HTTP01_TOKEN: &'static str = "HTTP01_TOKEN";
pub const HTTP01_URL: &'static str = "HTTP01_URL";
pub const THUMBPRINT: &'static str = "THUMBPRINT";
pub const TLS_ALPN_01: &'static str = "tls-alpn-01";
pub const TLSALPN01_KEY_AUTHORIZATION: &'static str = "TLSALPN01_KEY_AUTHORIZATION";
pub const TLSALPN01_RESPONSE: &'static str = "TLSALPN01_RESPONSE";
pub const TLSALPN01_SNI: &'static str = "TLSALPN01_SNI";
pub const TLSALPN01_TOKEN: &'static str = "TLSALPN01_TOKEN";

///Information about challenge.
#[repr(C)]
pub struct ChallengeInfo<'a>
{
	kind: RawString<'a>,
	name: RawString<'a>,
	wildcard: i32,
	_attributes: *const ChallengeAttribute<'a>,
	nattributes: usize,
	_methods: *const ChallengeMethod<'a>,
	nmethods: usize,
}

impl<'a> ChallengeInfo<'a>
{
	///Get identifier the challenge is for.
	pub fn identifier(&self) -> Identifier<'a>
	{
		Identifier{
			kind: self.kind.to_str(),
			name: self.name.to_str(),
			wildcard: self.wildcard != 0,
		}
	}
	///Get attribute by name.
	pub fn attribute(&self, name: &str) -> Option<&'a str>
	{
		let attributes = unsafe{from_raw_parts(self._attributes, self.nattributes)};
		for i in attributes.iter() {
			if name == i.name() { return Some(i.value()); }
		}
		None
	}
	///Get ID for specified challenge type (e.g., 'http-01'). Returns None if not possible.
	pub fn method_id(&self, name: &str) -> Option<MethodId>
	{
		let methods = unsafe{from_raw_parts(self._methods, self.nmethods)};
		for (idx, i) in methods.iter().enumerate() {
			if name == i.name() { return Some(MethodId(idx)); }
		}
		None
	}
}

fn with_trap<T:Sized, F: FnOnce() -> T>(cb: F, on_ex: T) -> T
{
	match catch_unwind(AssertUnwindSafe(||cb())) {
		Ok(x) => x,
		Err(_) => on_ex
	}
}

//This is a internal helper type, that must be public
#[doc(hidden)]
#[repr(C)]
pub struct InstanceHandle
{
	pub version: usize,
	pub context: *mut (),
	pub destroy: Option<extern fn(*mut ())>,
	pub add_challenge: Option<extern fn(*mut (), &ChallengeInfo) -> i32>,
	pub commit: Option<extern fn(*mut ()) -> i32>,
	pub error: extern fn(*mut (), *const u8),
	pub errorctx: *mut (),
}

//Structure representing the error callback.
struct ErrorCaller<'a>
{
	_error: extern fn(*mut (), *const u8),
	_errorctx: *mut (),
	phantom: PhantomData<&'a [u8]>,
}

impl<'a> ErrorCaller<'a>
{
	///Log an error message.
	fn error(&self, msg: &str)
	{
		let mut msg = msg.as_bytes().to_owned();
		msg.push(0);
		(self._error)(self._errorctx, msg.as_ptr());
	}
}

///The trait implemented by the instance type.
///
///For cleanup, the destructor of the type will be invoked when handle is no longer used.
pub trait Instance: DynInstance+Sized
{
	///Create a new instance with specified configuration.
	fn new(config: &str) -> Self;
}

///Dynamic parts of the instance trait.
pub trait DynInstance
{
	///Add a challenge with specified information. Return index of method, or error message to signal failure.
	fn add_challenge(&mut self, ch: &ChallengeInfo) -> Result<MethodId, String>;
	///Commit added challenges. Return  Ok(()) to signal success, error to signal failure.
	fn commit(&mut self) -> Result<(), String>
	{
		Ok(())
	}
	///Get name of authenticator.
	fn name(&self) -> String;
	///Clear added challenges.
	fn clear(&mut self) -> std::time::Duration;
}

//This is a internal helper type, that must be public
#[doc(hidden)]
pub struct InstanceExtra<I:Instance>
{
	//The error caller does not live forever, but it outlives this structure, so 'static is close enough.
	error: ErrorCaller<'static>,
	instance: I,
}

fn _cb_destroy<I:Instance>(_drop: Box<InstanceExtra<I>>)
{
	//Just let the box drop on the floor.
}

//This is internal helper function, that must be public.
#[doc(hidden)]
pub fn cb_destroy<I:Instance>(ctx: *mut ()) { unsafe{_cb_destroy::<I>(transmute(ctx))} }

fn _cb_add_challenge<I:Instance>(ctx: &mut InstanceExtra<I>, ch: &ChallengeInfo) -> i32
{
	with_trap(||match ctx.instance.add_challenge(ch) {
		Ok(r) => {
			if r.0 >= ch.nmethods {
				ctx.error.error("<Illegal method index>");
				-1
			} else {
				r.0 as i32
			}
		},
		Err(r) => {
			ctx.error.error(&r);
			-1
		}
	}, -1)
}

//This is internal helper function, that must be public.
#[doc(hidden)]
pub fn cb_add_challenge<I:Instance>(ctx: *mut (), ch: &ChallengeInfo) -> i32
{
	unsafe{_cb_add_challenge::<I>(transmute(ctx), ch)}
}

fn _cb_commit<I:Instance>(ctx: &mut InstanceExtra<I>) -> i32
{
	with_trap(||match ctx.instance.commit() {
		Ok(_) => 0,
		Err(r) => {
			ctx.error.error(&r);
			-1
		}
	}, -1)
}

//This is internal helper function, that must be public.
#[doc(hidden)]
pub fn cb_commit<I:Instance>(ctx: *mut ()) -> i32
{
	unsafe{_cb_commit::<I>(transmute(ctx))}
}

//This is internal helper function, that must be public.
#[doc(hidden)]
pub fn cb_new<I:Instance>(handle: &mut InstanceHandle, config: *const u8, configsize: usize) -> *mut ()
{
	//The config should be UTF-8.
	let config = unsafe{from_utf8_unchecked(from_raw_parts(config, configsize))};
	let instance = Box::new(InstanceExtra{
		instance: I::new(config),
		error: ErrorCaller {
			_error: handle.error,
			_errorctx: handle.errorctx,
			phantom: PhantomData,
		}
	});
	unsafe{transmute(Box::into_raw(instance))}
}

///Prelude module. These all should be imported.
pub mod prelude
{
	pub use super::ChallengeInfo;
	pub use super::Identifier;
	pub use super::Instance;
	pub use super::MethodId;
}

#[doc(hidden)]
pub fn __do_handle_error_message(msg: Arguments)
{
	//FIXME: Shove this into suitable callback.
	eprintln!("{msg}")
}

#[doc(hidden)]
pub fn __do_handle_trace_message(_: Arguments)
{
	//These are by default not printed.
}

#[macro_export]
macro_rules! errmsg
{
	($($args:tt)*) => { $crate::__do_handle_error_message(format_args!($($args)*)) }
}

#[macro_export]
macro_rules! trace
{
	($($args:tt)*) => { $crate::__do_handle_trace_message(format_args!($($args)*)) }
}

#[doc(hidden)]
#[macro_export]
macro_rules! authmod_instance_inner
{
	($typex:ident) => {
		extern fn _acmeclient_authmod_destroy(ctx: *mut ()) { $crate::cb_destroy::<$typex>(ctx) }
		extern fn _acmeclient_authmod_add_challenge(ctx: *mut(), ch: &$crate::ChallengeInfo) -> i32
		{
			$crate::cb_add_challenge::<$typex>(ctx, ch)
		}
		extern fn _acmeclient_authmod_commit(ctx: *mut ()) -> i32 { $crate::cb_commit::<$typex>(ctx) }
		extern fn _acmeclient_authmod_auth_open(handle: &mut $crate::InstanceHandle, config: *const u8,
			configsize: usize)
		{
			handle.version = 0;
			handle.context = $crate::cb_new::<$typex>(handle, config, configsize);
			handle.destroy = Some(_acmeclient_authmod_destroy);
			handle.add_challenge = Some(_acmeclient_authmod_add_challenge);
			handle.commit = Some(_acmeclient_authmod_commit);
		}
	}
}

///Declare the entrypoint method.
///
///This is used as `authmod_instance!(InstanceTypeName)`
///
///Where `InstanceTypeName` implements `Instance` trait.
#[cfg(not(feature="builtin"))]
#[macro_export]
macro_rules! authmod_instance
{
	($typex:ident) => {
		authmod_instance_inner!($typex);
		#[no_mangle]
		pub extern fn certtool_acmeclient_auth_open(handle: &mut $crate::InstanceHandle, config: *const u8,
			configsize: usize)
		{
			_acmeclient_authmod_auth_open(handle, config, configsize)
		}
	}
}

///Declare the entrypoint method.
///
///This is used as `authmod_instance!(InstanceTypeName)`
///
///Where `InstanceTypeName` implements `Instance` trait.
#[cfg(feature="builtin")]
#[macro_export]
macro_rules! authmod_instance
{
	($typex:ident) => {
		authmod_instance_inner!($typex);
		//Declare exported auth_open method instead of the normal entrypoint. This has to be extern
		//with C ABI, since it is called as such!
		pub extern fn auth_open(handle: &mut $crate::InstanceHandle, config: *const u8, configsize: usize)
		{
			_acmeclient_authmod_auth_open(handle, config, configsize)
		}
	}
}
