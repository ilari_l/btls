use btls_aux_codegen::mkid;
use btls_aux_codegen::quote;
use btls_aux_codegen::qwrite;
use std::env::var;
use std::fs::File;
use std::io::Write;
use std::path::Path;

static ACCOUNT_STATES: &'static str = "account valid deactivated revoked";
static ORDER_STATES: &'static str = "order pending ready processing valid invalid";
static AUTHZ_STATES: &'static str = "authz pending valid invalid deactivated expired revoked";
static CHALLENGE_STATES: &'static str = "challenge pending processing valid invalid";


fn capitalize(x: &str) -> String
{
	let mut first = true;
	let mut out = String::new();
	for c in x.chars() {
		if first {
			out.extend(c.to_uppercase());
			first = false;
		} else {
			out.push(c);
		}
	}
	out
}

fn write_states(fp: &mut File, data: &str)
{
	let mut elems: Vec<&str> = data.split(' ').collect();
	let kind = elems.remove(0);
	let json: Vec<_> = elems.clone();
	let symbols: Vec<_> = elems.iter().map(|x|mkid(&capitalize(x))).collect();
	let ksym = mkid(&format!("{kind}Status", kind=capitalize(kind)));
	qwrite(fp, quote!{
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum #ksym
{
	#(#symbols),*
}

impl #ksym
{
	pub fn new(x: &str) -> Option<#ksym>
	{
		match x {
			#(#json => Some(#!REPEAT ksym::#symbols)),*,
			_ => None
		}
	}
}

impl std::fmt::Display for #ksym
{
	fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error>
	{
		let x = match *self {
			#(#!REPEAT ksym::#symbols => #json),*
		};
		f.write_str(x)
	}
}
	});
}

fn main()
{
	let out_dir = var("OUT_DIR").unwrap();
	let version = var("CARGO_PKG_VERSION").unwrap();
	let dest_path = Path::new(&out_dir).join("product.inc");
	let mut f = File::create(&dest_path).unwrap();
	writeln!(f, "static CERTTOOL_ACMECLIENT_UA: &'static str = \"certtool-acmeclient/{version}\";").unwrap();
	writeln!(f, "static PRODUCT_VERSION: &'static str = \"{version}\";").unwrap();
	let dest_path = Path::new(&out_dir).join("autogenerated-acme-states.inc.rs");
	let mut f = File::create(&dest_path).unwrap();
	write_states(&mut f, ACCOUNT_STATES);
	write_states(&mut f, ORDER_STATES);
	write_states(&mut f, AUTHZ_STATES);
	write_states(&mut f, CHALLENGE_STATES);
	println!("cargo:rerun-if-changed=build.rs");
}
