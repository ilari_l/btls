use crate::strings::safe_string_to_text;
use crate::strings::SafeOctetsToText;
use crate::strings::SafeStringToText;
use btls_aux_fail::dtry;
use btls_aux_json2::JSON;
use std::cmp::max;
use std::collections::BTreeMap;
use std::fmt::Write as FmtWrite;
use std::iter::repeat;
use std::mem::replace;
use std::str::from_utf8;


fn width(key: &str) -> usize
{
	//TODO better metric (consider combining characters, zerowidth spaces, fullwidth characters).
	key.chars().count()
}

fn spaces(count: usize) -> String
{
	repeat(' ').take(count).collect()
}

fn explain_type<'a>(uri: &'a str) -> String
{
	if let Some(aerr) = uri.strip_prefix("urn:ietf:params:acme:error:") {
		match aerr {
			"accountDoesNotExist" => "Specified account does not exist".to_owned(),
			"badCSR" => "Bad Certificate Signing Request".to_owned(),
			"badNonce" => "Bad anti-replay nonce".to_owned(),
			"badRevocationReason" => "Bad revocation reason".to_owned(),
			"badSignatureAlgorithm" => "Bad JWS signature algorithm".to_owned(),
			"caa" => "CAA records prohibit issuance".to_owned(),
			"compound" => "Multiple different types of problems".to_owned(),
			"connection" => "CA could not connect to the server".to_owned(),
			"dns" => "CA could not look up DNS recors".to_owned(),
			"externalAccountRequired" => "External account binding required".to_owned(),
			"incorrectResponse" => "Incorrect response to validation query".to_owned(),
			"invalidContact" => "Invalid contact".to_owned(),
			"malformed" => "Malformed JWS".to_owned(),
			"rateLimited" => "Rate limit exceeded".to_owned(),
			"rejectedIdentifier" => "Identifier rejected".to_owned(),
			"serverInternal" => "Internal server error".to_owned(),
			"tls" => "TLS error during handshake with server".to_owned(),
			"unauthorized" => "Insufficient authorization for operation".to_owned(),
			"unsupportedContact" => "Unsupported contact type".to_owned(),
			"unsupportedIdentifier" => "Unsupported identifier".to_owned(),
			"userActionRequired" => "User action required".to_owned(),
			_ => safe_string_to_text(uri),
		}
	} else {
		safe_string_to_text(uri)
	}
}

fn read_identifier(id: &BTreeMap<String, JSON>) -> Result<String, ()>
{
	if id.len() != 2 { return Err(()); }
	let itype = dtry!(id.get("type").and_then(|x|x.as_string()));
	let ivalue = dtry!(id.get("value").and_then(|x|x.as_string()));
	Ok(if itype == "dns" {
		format!("DNS name '{name}': ", name=SafeStringToText(ivalue))
	} else if itype == "ip" {
		format!("IP address '{addr}': ", addr=SafeStringToText(ivalue))
	} else {
		format!("Unknown({type}) '{value}': ", type=SafeStringToText(itype), value=SafeStringToText(ivalue))
	})
}

pub fn print_rawoctets(errmsg: &[u8]) -> String
{
		format!("\n\
------------------------------------------------------------------------------\n\
{errmsg}\n\
------------------------------------------------------------------------------\n\
", errmsg=SafeOctetsToText(errmsg))
}


fn _format_json_error(errmsg: &BTreeMap<String, JSON>) -> Result<String, ()>
{
	let mut s = String::new();
	let errtype = dtry!(errmsg.get("type"));
	let errtype = explain_type(dtry!(errtype.as_string()));
	let detail = errmsg.get("detail").and_then(|msg|{
		msg.as_string()
	}).unwrap_or("<no details available>");
	let identifier = errmsg.get("identifier").map(|ident|{
		if let Some(ref d) = ident.as_object() { if let Ok(x) = read_identifier(d) {
			return x
		}}
		format!("<Invalid identifier {ident:?}>: ")
	}).unwrap_or("<global>: ".to_owned());
	write!(s, "{identifier}{errtype} ({detail})").ok();
	if errmsg.get("subproblems").is_some() {
		dtry!(NORET errmsg.get("subproblems")).and_then(|sb|{
			if let Some(ref a) = sb.as_array() {
				for suberr in a.iter() {
					if let Some(ref suberr) = suberr.as_object() {
						let suberr = _format_json_error(suberr)?;
						write!(s, "\n{suberr}").ok();
					} else {
						return Err(())
					}
				}
				Ok(())
			} else {
				Err(())
			}
		})?;
	}
	//Remove type, detail, identifier and subproblems. If any remain, print the rest.
	let errmsg2: BTreeMap<String,JSON> = errmsg.iter().filter(|(name,_)|{
		match &name[..] {
			"type" => false,
			"detail" => false,
			"identifer" => false,
			"subproblems" => false,
			_ => true
		}
	}).map(|(x,y)|{
		(x.clone(),y.clone())
	}).collect();
	if errmsg2.len() > 0 { write!(s, "\n{errmsg}", errmsg=print_error_json_object(&errmsg2, 0)).ok(); }
	Ok(s)
}

pub fn format_json_error(errmsg: &JSON) -> String
{
	if let Some(ref d) = errmsg.as_object() {
		//If this fails, use the defaults.
		if let Ok(msg) = _format_json_error(d) { return msg; }
	}
	print_error_json(errmsg, 0, false)
}

pub fn format_any_error(errmsg: &[u8]) -> String
{
	let nerrmsg = from_utf8(errmsg).ok().and_then(|x|{
		JSON::parse(x).ok()
	});
	match nerrmsg {
		Some(errmsg) => format!("\n\
------------------------------------------------------------------------------\n\
{errmsg}\n\
------------------------------------------------------------------------------\n\
", errmsg=format_json_error(&errmsg)),
		None => format!("\n\
------------------------------------------------------------------------------\n\
{errmsg}\n\
------------------------------------------------------------------------------\n\
", errmsg=SafeOctetsToText(errmsg))
	}
}

pub fn format_validation_record(msg: &JSON) -> String
{
	print_error_json(msg, 0, false).to_string()
}

//The first row starts immediately, it is not indented, unlike the rest. Also the last line has no trailing LF.
///Print a JSON structure with specified indentation and type_special handling.
///
///When starting printing, you probably want indent=0 and type_special=false.
fn print_error_json(errmsg: &JSON, indent: usize, type_special: bool) -> String
{
	if let Some(_) = errmsg.as_null() {
		"null".to_string()
	} else if let Some(x) = errmsg.as_number() {
		x.to_string()
	} else if let Some(x) = errmsg.as_boolean() {
		if x { format!("true") } else { format!("false") }
	} else if let Some(ref s) = errmsg.as_string() {
		if type_special {
			explain_type(s).to_string()
		} else {
			SafeStringToText(s).to_string()
		}
	} else if let Some(ref a) = errmsg.as_array() {
		let mut s = String::new();
		let mut first = true;
		for val in a.iter() {
			if !replace(&mut first, false) {
				s.push('\n');
				s.push_str(&spaces(indent));
			}
			if val.as_array().is_some() {
				s.push_str("    ");
				s.push_str(&print_error_json(val, indent+4, false));
			} else {
				s.push_str(&print_error_json(val, indent, false));
			}
		}
		s
	} else if let Some(ref d) = errmsg.as_object() {
		print_error_json_object(d, indent)
	} else {
		format!("<type???>")
	}
}

fn print_error_json_object(d: &BTreeMap<String,JSON>, indent: usize) -> String
{
	let mut s = String::new();
	let mut mkeylen = 0;
	for (key, _) in d.iter() {
		mkeylen = max(mkeylen, width(key));
	}
	let mut first = true;
	for (key, value) in d.iter() {
		if !replace(&mut first, false) {
			s.push('\n');
			s.push_str(&spaces(indent));
		}
		s.push_str(&safe_string_to_text(key));
		s.push(':');
		s.push_str(&spaces(mkeylen-width(key)+1));
		s.push_str(&print_error_json(value, indent + mkeylen + 2, key == "type"));
	}
	s
}
