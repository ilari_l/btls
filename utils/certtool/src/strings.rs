use btls_aux_fail::fail_if;
use std::borrow::Borrow;
use std::borrow::ToOwned;
use std::cmp::min;
use std::cmp::Ordering;
use std::ffi::OsString;
use std::fmt::Debug;
use std::fmt::Display;
use std::fmt::Formatter;
use std::fmt::Result as FmtResult;
use std::fmt::Write as FmtWrite;
use std::hash::Hash;
use std::hash::Hasher;
use std::mem::replace;
use std::mem::transmute;
use std::ops::Deref;
use std::ops::Index;
use std::ops::Range;
use std::ops::RangeFrom;
use std::ops::RangeFull;
use std::ops::RangeTo;
use std::str::from_utf8_unchecked;
use std::str::Chars;
use std::str::Split;


macro_rules! defs
{
	(index $borrowed:ident $with:ty) => {
		impl Index<$with> for $borrowed
		{
			type Output = $borrowed;
			fn index(&self, index: $with) -> &$borrowed { unsafe{transmute(&self.0[index])} }
		}
	};
	(maybe_deref AsciiStr $x:expr) => { $x };
	(maybe_deref AsciiString $x:expr) => { $x.deref() };
	(maybe_deref HostStr $x:expr) => { $x };
	(maybe_deref HostString $x:expr) => { $x.deref() };
	(maybe_deref SchemeStr $x:expr) => { $x };
	(maybe_deref SchemeString $x:expr) => { $x.deref() };
	(maybe_deref PathStr $x:expr) => { $x };
	(maybe_deref PathString $x:expr) => { $x.deref() };
	(maybe_deref Base64UrlStr $x:expr) => { $x };
	(maybe_deref Base64UrlString $x:expr) => { $x.deref() };
	(partial_ordering $ptype:ident $fortype:ident $borrowed:ident) => {
		impl PartialOrd<$ptype> for $fortype
		{
			fn partial_cmp(&self, a: &$ptype) -> Option<Ordering>
			{
				Some($borrowed::__cmp(&defs!(maybe_deref $fortype self).0,
					&defs!(maybe_deref $ptype a).0))
			}
		}
		impl PartialEq<$ptype> for $fortype
		{
			fn eq(&self, a: &$ptype) -> bool { self.partial_cmp(a) == Some(Ordering::Equal) }
		}
	};
	(total_ordering $fortype:ident $borrowed:ident) => {
		impl Ord for $fortype
		{
			fn cmp(&self, a: &$fortype) -> Ordering
			{
				$borrowed::__cmp(&defs!(maybe_deref $fortype self).0,
					&defs!(maybe_deref $fortype a).0)
			}
		}

		impl Eq for $fortype
		{
		}
		impl Hash for $fortype
		{
			fn hash<H>(&self, state: &mut H) where H: Hasher
			{
				$borrowed::__hash(&defs!(maybe_deref $fortype self).0, state);
			}
		}
		impl Display for $fortype
		{
			fn fmt(&self, f: &mut Formatter) -> FmtResult
			{
				$borrowed::__fmt(&defs!(maybe_deref $fortype self).0, f)
			}
		}
		impl Debug for $fortype
		{
			fn fmt(&self, f: &mut Formatter) -> FmtResult
			{
				write!(f, "\"{string}\"", string=&defs!(maybe_deref $fortype self).0)
			}
		}
		impl PartialEq<str> for $fortype
		{
			fn eq(&self, other: &str) -> bool
			{
				$borrowed::__cmp(&defs!(maybe_deref $fortype self).0, other).is_eq()
			}
		}
		impl PartialEq<$fortype> for str
		{
			fn eq(&self, other: &$fortype) -> bool
			{
				$borrowed::__cmp(self, &defs!(maybe_deref $fortype other).0).is_eq()
			}
		}
	};
	(cross_defs $borrowed:ident $owned:ident) => {
		pub struct $borrowed(str);
		#[derive(Clone)]
		pub struct $owned(String);
		impl ToOwned for $borrowed
		{
			type Owned = $owned;
			fn to_owned(&self) -> $owned { $owned(self.0.to_owned()) }
		}
		impl Deref for $owned
		{
			type Target = $borrowed;
			fn deref(&self) -> &$borrowed { unsafe{transmute(self.0.deref())} }
		}
		impl Borrow<$borrowed> for $owned
		{
			fn borrow(&self) -> &$borrowed { self.deref() }
		}
		impl $borrowed
		{
			//Runs safe_octets_to_text() on the input on error.
			pub fn new_octets<'a>(x: &'a [u8]) -> Result<&'a $borrowed, String>
			{
				fail_if!(!$borrowed::__validate(x),
					ByteToCharIterator::as_string(x.iter().cloned()));
				//The above checks guarantee x is valid UTF-8.
				Ok(unsafe{transmute(from_utf8_unchecked(x))})
			}
			//Runs safe_string_to_text() on the input on error.
			pub fn new_string<'a>(x: &'a str) -> Result<&'a $borrowed, String>
			{
				$borrowed::new_octets(x.as_bytes())
			}
			pub fn as_inner<'a>(&'a self) -> &'a str { unsafe{transmute(&self.0)} }
			pub fn len(&self) -> usize { self.0.len() }
			pub fn as_bytes<'a>(&'a self) -> &'a [u8] { self.0.as_bytes() }
			pub fn chars<'a>(&'a self) -> Chars<'a> { self.0.chars() }
		}
		impl $owned
		{
			pub fn new() -> $owned { $owned(String::new()) }
			pub fn with_capacity(cap: usize) -> $owned
			{
				$owned(String::with_capacity(cap))
			}
			//Runs safe_octets_to_text() on the input on error.
			pub fn new_octets(x: &[u8]) -> Result<$owned, String>
			{
				Ok($borrowed::new_octets(x)?.to_owned())
			}
			//Runs safe_string_to_text() on the input on error.
			pub fn new_string(x: &str) -> Result<$owned, String>
			{
				Ok($borrowed::new_string(x)?.to_owned())
			}
			pub fn new_osstring(x: &OsString) -> Result<$owned, String>
			{
				if let Some(x) = x.to_str() {
					Ok($borrowed::new_string(x)?.to_owned())
				} else {
					//This has to be bad.
					Err(safe_string_to_text(x.to_string_lossy().deref()))
				}
			}
			//Runs safe_string_to_text() on the input on error.
			pub fn from_string(x: String) -> Result<$owned, String>
			{
				fail_if!(!$borrowed::__validate(x.as_bytes()),
					CharToCharIterator::as_string(x.chars()));
				Ok($owned(x))
			}
			//Runs safe_octets_to_text() on the input on error.
			pub fn from_octets(x: Vec<u8>) -> Result<$owned, String>
			{
				fail_if!(!$borrowed::__validate(&x),
					ByteToCharIterator::as_string(x.iter().cloned()));
				//Previous guarantees x is valid UTF-8.
				Ok($owned(unsafe{String::from_utf8_unchecked(x)}))
			}
			pub fn into_inner(self) -> String { self.0 }
		}
		defs!(partial_ordering $borrowed $borrowed $borrowed);
		defs!(partial_ordering $borrowed $owned $borrowed);
		defs!(partial_ordering $owned $borrowed $borrowed);
		defs!(partial_ordering $owned $owned $borrowed);
		defs!(total_ordering $borrowed $borrowed);
		defs!(total_ordering $owned $borrowed);
		defs!(index $borrowed Range<usize>);
		defs!(index $borrowed RangeFrom<usize>);
		defs!(index $borrowed RangeFull);
		defs!(index $borrowed RangeTo<usize>);
	};
}


#[derive(Copy,Clone,PartialEq,Eq,Debug)]
pub enum PChar
{
	Char(char),
	Byte(u8),
}

struct PCharToCharIterator<I:Iterator<Item=PChar>>(I, Option<u32>);

impl<I:Iterator<Item=PChar>> PCharToCharIterator<I>
{
	pub fn new(x: I) -> PCharToCharIterator<I> { PCharToCharIterator(x, None) }
}

impl<I:Iterator<Item=PChar>> Iterator for PCharToCharIterator<I>
{
	type Item=char;
	fn next(&mut self) -> Option<char>
	{
		static HEX: [char;16] = ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'];
		if let Some(cp) = self.1 {
			match cp {
				0x000000..=0x00FFFF => {
					self.1 = Some((cp & 0xFFF) + 0x110000);
					Some(HEX[(cp as usize >> 12) & 15])
				},
				0x010000..=0x0FFFFF => {
					self.1 = Some(cp & 0xFFFF);
					Some(HEX[(cp as usize >> 16) & 15])
				},
				0x100000..=0x10FFFF => {
					self.1 = Some(cp & 0xFFFF);
					Some('1')
				},
				0x110000..=0x110FFF => {
					self.1 = Some((cp & 0xFF) + 0x111000);
					Some(HEX[(cp as usize >> 8) & 15])
				},
				0x111000..=0x1110FF => {
					self.1 = Some((cp & 0xF) + 0x111100);
					Some(HEX[(cp as usize >> 4) & 15])
				},
				0x111100..=0x11110F => {
					self.1 = Some(0x111110);
					Some(HEX[(cp as usize) & 15])
				},
				0x111110 => {
					self.1 = None;
					Some('\u{1433}')
				},
				0x111200..=0x1112FF => {
					self.1 = Some((cp & 0xF) + 0x111100);
					Some(HEX[(cp as usize >> 4) & 15])
				},
				_ => {
					self.1 = None;
					Some('\u{fffd}')
				}
			}
		} else {
			let n = self.0.next();
			match n {
				Some(PChar::Char(x)) => {
					let need_esc = match x as u32 {
						9|10|13 => false,
						32..=126 => false,
						160..=0xD7FF => false,
						0xE000..=0xFDCF => false,
						0xFDF0..=0xFFFD => false,
						0x10000..=0x10FFFF => (x as u32) & 65535 > 65533,
						_ => true,
					};
					if need_esc {
						self.1 = Some(x as u32);
						Some('\u{1438}')
					} else {
						Some(x)
					}
				},
				Some(PChar::Byte(x)) => {
					self.1 = Some(0x111200 + x as u32);
					Some('\u{1438}')
				},
				None => None
			}
		}
	}
}

struct CharToPCharIterator<I:Iterator<Item=char>>(I);

impl<I:Iterator<Item=char>> CharToPCharIterator<I>
{
	pub fn new(x: I) -> CharToPCharIterator<I> { CharToPCharIterator(x) }
}

impl<I:Iterator<Item=char>> Iterator for CharToPCharIterator<I>
{
	type Item=PChar;
	fn next(&mut self) -> Option<PChar> { self.0.next().map(|x|PChar::Char(x)) }
}

struct ByteToPCharIterator<I:Iterator<Item=u8>>(I, u64);

impl<I:Iterator<Item=u8>> ByteToPCharIterator<I>
{
	pub fn new(x: I) -> ByteToPCharIterator<I> { ByteToPCharIterator(x, 1) }
}

impl<I:Iterator<Item=u8>> Iterator for ByteToPCharIterator<I>
{
	type Item=PChar;
	fn next(&mut self) -> Option<PChar>
	{
		loop {
			//If not EOF yet, read a byte and set EOF flag if needed.
			if self.1 >> 62 == 0 {
				if let Some(x) = self.0.next() {
					self.1 = (self.1 << 8) | (x as u64);
				} else {
					self.1 |= 1u64 << 63;
				}
			}
			let nbit = 1u64 << 62;
			self.1 &= !nbit;	//Clear the nonext bit.
			let e = self.1 & (1u64 << 63);
			if self.1 & 0x180 == 0x100 {
				//1 byte UTF-8 codepoint.
				let cp = replace(&mut self.1, 1|e);
				let cp = cp & 0x7F;
				return Some(PChar::Char(char::from_u32(cp as u32 & 0xFF).unwrap_or('\u{fffd}')));
			} else if self.1 >> 63 == 0 && self.1 & 0x1E0 == 0x1C0 && self.1 & 0xFE != 0xC0 {
				//2 byte UTF-8 codepoint, first byte.
				continue;
			} else if self.1 & 0x1E0C0 == 0x1C080 {
				//2 byte UTF-8 codepoint.
				let cp = replace(&mut self.1, 1|e);
				let cp = (cp & 0x3F) | ((cp & 0x1F00) >> 2);
				return Some(PChar::Char(char::from_u32(cp as u32).unwrap_or('\u{fffd}')));
			} else if self.1 >> 63 == 0 && self.1 & 0x1F0 == 0x1E0 {
				//3 byte UTF-8 codepoint, first byte.
				continue;
			} else if self.1 >> 63 == 0 && self.1 & 0x1F0C0 == 0x1E080 && self.1 & 0xFF20 != 0xE000 &&
				self.1 & 0xF20 != 0xD20 {
				//3 byte UTF-8 codepoint, second byte.
				continue;
			} else if self.1 & 0x1F0C0C0 == 0x1E08080 && self.1 & 0xF3F3E != 0xF3F3E &&
				self.1 & 0xF3F30 != 0xF3D10 && self.1 & 0xF3F30 != 0xF3D20 {
				//3 byte UTF-8 codepoint.
				let cp = replace(&mut self.1, 1|e);
				let cp = (cp & 0x3F) | ((cp & 0x3F00) >> 2) | ((cp & 0x0F0000) >> 4);
				return Some(PChar::Char(char::from_u32(cp as u32).unwrap_or('\u{fffd}')));
			} else if self.1 >> 63 == 0 && self.1 & 0x1F8 == 0x1F0 {
				//4 byte UTF-8 codepoint, first byte.
				continue;
			} else if self.1 >> 63 == 0 && self.1 & 0x1F8C0 == 0x1F080 && self.1 & 0xFF30 != 0xF000 {
				//4 byte UTF-8 codepoint, second byte.
				continue;
			} else if self.1 >> 63 == 0 && self.1 & 0x1F8C0C0 == 0x1F08080 {
				//4 byte UTF-8 codepoint, third byte.
				continue;
			} else if self.1 & 0x1F8C0C0C0 == 0x1F0808080 && self.1 & 0xF3F3E != 0xF3F3E {
				//4 byte UTF-8 codepoint.
				let cp = replace(&mut self.1, 1|e);
				let cp = (cp & 0x3F) | ((cp & 0x3F00) >> 2) | ((cp & 0x3F0000) >> 4) |
					((cp & 0x07000000) >> 6);
				return Some(PChar::Char(char::from_u32(cp as u32).unwrap_or('\u{fffd}')));
			} else if self.1 & 0x100000000 == 0x100000000 {
				//Relax from 4 to 3 bytes.
				let r = self.1&0xFFFFFF;
				let cp = replace(&mut self.1, (0x1000000+r)|nbit|e);
				let cp = cp >> 24;
				return Some(PChar::Byte(cp as u8));
			} else if self.1 & 0x1000000 == 0x1000000 {
				//Relax from 3 to 2 bytes.
				let r = self.1&0xFFFF;
				let cp = replace(&mut self.1, (0x10000+r)|nbit|e);
				let cp = cp >> 16;
				return Some(PChar::Byte(cp as u8));
			} else if self.1 & 0x10000 == 0x10000 {
				//Relax from 2 to 1 bytes.
				let r = self.1&0xFF;
				let cp = replace(&mut self.1, (0x100+r)|nbit|e);
				let cp = cp >> 8;
				return Some(PChar::Byte(cp as u8));
			} else if self.1 & 0x100 == 0x100 {
				//Relax from 1 to 0 bytes.
				let cp = replace(&mut self.1, 0x1|e);
				let cp = cp;
				return Some(PChar::Byte(cp as u8));
			} else if self.1 == 1|(1u64<<63) {
				//The end.
				break;
			} else {
				//This can not happen. The above state transitions just can not produce a state that
				//will not match any of the conditions above.
				panic!("UTF-8 decoder in impossible state {state:x}", state=self.1);
			}
		}
		None
	}
}

pub struct ByteToCharIterator<I:Iterator<Item=u8>>(PCharToCharIterator<ByteToPCharIterator<I>>);

impl<I:Iterator<Item=u8>> ByteToCharIterator<I>
{
	pub fn new(x: I) -> ByteToCharIterator<I>
	{
		ByteToCharIterator(PCharToCharIterator::new(ByteToPCharIterator::new(x)))
	}
	pub fn as_string(x: I) -> String { ByteToCharIterator::new(x).collect() }
}

impl<I:Iterator<Item=u8>> Iterator for ByteToCharIterator<I>
{
	type Item=char;
	fn next(&mut self) -> Option<char> { self.0.next() }
}

pub struct CharToCharIterator<I:Iterator<Item=char>>(PCharToCharIterator<CharToPCharIterator<I>>);

impl<I:Iterator<Item=char>> CharToCharIterator<I>
{
	pub fn new(x: I) -> CharToCharIterator<I>
	{
		CharToCharIterator(PCharToCharIterator::new(CharToPCharIterator::new(x)))
	}
	pub fn as_string(x: I) -> String { CharToCharIterator::new(x).collect() }
}

impl<I:Iterator<Item=char>> Iterator for CharToCharIterator<I>
{
	type Item=char;
	fn next(&mut self) -> Option<char> { self.0.next() }
}

pub fn safe_octets_to_text(x: &[u8]) -> String { ByteToCharIterator::as_string(x.iter().cloned()) }
pub fn safe_string_to_text(x: &str) -> String { CharToCharIterator::as_string(x.chars()) }

pub struct SafeOctetsToText<'a>(pub &'a [u8]);

impl<'a> Display for SafeOctetsToText<'a>
{
	fn fmt(&self, f: &mut Formatter) -> FmtResult
	{
		for c in ByteToCharIterator::new(self.0.iter().cloned()) { write!(f, "{c}")?; }
		Ok(())
	}
}

pub struct SafeStringToText<'a>(pub &'a str);

impl<'a> Display for SafeStringToText<'a>
{
	fn fmt(&self, f: &mut Formatter) -> FmtResult
	{
		for c in CharToCharIterator::new(self.0.chars()) { write!(f, "{c}")?; }
		Ok(())
	}
}

unsafe trait FilteredString
{
	fn __cmp(a: &str, b: &str) -> Ordering;
	fn __hash<H>(a: &str, state: &mut H) where H: Hasher;
	fn __fmt(a: &str, f: &mut Formatter) -> FmtResult;
	//this is UNSAFE due to needed to hold the invariant to return false if b is not valid UTF-8.
	fn __validate(b: &[u8]) -> bool;
}

defs!(cross_defs AsciiStr AsciiString);
pub struct AsciiStrSplit<'a>(Split<'a, char>);

impl AsciiStr
{
	pub fn starts_with(&self, a: &str) -> bool { self.0.starts_with(a) }
	pub fn ends_with(&self, a: &str) -> bool { self.0.ends_with(a) }
	pub fn find(&self, c: char) -> Option<usize> { self.0.find(c) }
	pub fn find_c<X>(&self, c: X) -> Option<usize> where X: FnMut(char) -> bool { self.0.find(c) }
	pub fn find_s(&self, c: &AsciiStr) -> Option<usize> { self.0.find(&c.0) }
	pub fn split<'a>(&'a self, c: char) -> AsciiStrSplit<'a> { AsciiStrSplit(self.0.split(c)) }
}

unsafe impl FilteredString for AsciiStr
{
	fn __cmp(a: &str, b: &str) -> Ordering { a.cmp(&b) }
	fn __hash<H>(a: &str, state: &mut H) where H: Hasher { a.hash(state); }
	fn __fmt(a: &str, f: &mut Formatter) -> FmtResult
	{
		//This never contains anything weird.
		f.write_str(a)
	}
	fn __validate(x: &[u8]) -> bool { x.iter().all(|c|*c>=32||*c<=126) }
}


impl AsciiString
{
	pub fn push_str(&mut self, x: &AsciiStr) { self.0.push_str(&x.0); }
	pub fn make_ascii_lowercase(&mut self) { self.0.make_ascii_lowercase(); }
}

impl<'a> From<&'a AsciiStr> for Vec<u8>
{
	fn from(s: &'a AsciiStr) -> Vec<u8> { s.as_bytes().to_owned() }
}

impl<'a> Iterator for AsciiStrSplit<'a>
{
	type Item = &'a AsciiStr;
	fn next(&mut self) -> Option<&'a AsciiStr> { unsafe{transmute(self.0.next())} }
}

pub fn compare_case_insensitive(a: &str, b: &str) -> Ordering
{
	compare_case_insensitive_bytes(a.as_bytes(), b.as_bytes())
}

pub fn compare_case_insensitive_bytes(a: &[u8], b: &[u8]) -> Ordering
{
	//The compare is case-insensitive!
	for i in 0..min(a.len(), b.len()) {
		let a = a[i];
		let b = b[i];
		let a = a + match a { 65..=90 => 32, _ => 0};
		let b = b + match b { 65..=90 => 32, _ => 0};
		if a < b { return Ordering::Less; }
		if a > b { return Ordering::Greater; }
	}
	a.len().cmp(&b.len())
}

fn hash_case_insensitive<H>(a: &str, state: &mut H) where H: Hasher
{
	let a = a.as_bytes();
	for i in 0..a.len() {
		let a = a[i];
		let a = a + match a { 65..=90 => 32, _ => 0};
		a.hash(state);
	}
}

fn format_case_insensitive(a: &str, f: &mut Formatter) -> FmtResult
{
	for i in a.chars() { write!(f, "{i}", i=i.to_ascii_lowercase())?; }
	Ok(())
}



defs!(cross_defs HostStr HostString);

impl HostStr
{
	pub fn ends_with(&self, a: &str) -> bool
	{
		let alen = a.len();
		let slen = self.len();
		if alen > slen { return false; }
		//This is restricted to ASCII, so split at any point can be assumed valid.
		&self[slen-alen..] == a
	}
}

unsafe impl FilteredString for HostStr
{
	fn __cmp(a: &str, b: &str) -> Ordering { compare_case_insensitive(a, b) }
	fn __hash<H>(a: &str, state: &mut H) where H: Hasher { hash_case_insensitive(a, state) }
	fn __fmt(a: &str, f: &mut Formatter) -> FmtResult { format_case_insensitive(a, f) }
	fn __validate(x: &[u8]) -> bool
	{
		//The actual character set is wider, but we only accept DNS names with small extensions.
		//Specifically, : is allowed because it can appear in IPv6 address literials and _ is allowed
		//as it is often misused in names.
		x.iter().all(|c|match *c {
			48..=57|65..=90|97..=122 => true,	//Alphanumerics.
			45|46|58|95 => true,			//-, ., : and _.
			_ => false
		})
	}
}

impl HostString
{
	pub fn push_byte(&mut self, b: u8)
	{
		match b {
			48..=57|65..=90|97..=122 => (),		//Alphanumerics.
			45|46|58|95 => (),			//-, ., : and _.
			_ => return
		}
		self.0.push(b as char);
	}
}

pub fn host_string_to_ascii_string<'a>(h: &'a HostStr) -> &'a AsciiStr { unsafe{transmute(h)} }

defs!(cross_defs SchemeStr SchemeString);

unsafe impl FilteredString for SchemeStr
{
	fn __cmp(a: &str, b: &str) -> Ordering { compare_case_insensitive(a, b) }
	fn __hash<H>(a: &str, state: &mut H) where H: Hasher { hash_case_insensitive(a, state) }
	fn __fmt(a: &str, f: &mut Formatter) -> FmtResult { format_case_insensitive(a, f) }
	fn __validate(x: &[u8]) -> bool
	{
		if x.len() == 0 { return false; }
		if (x[0] & 0xDF).wrapping_sub(65) > 25 { return false; }	//First must be alpha.
		x.iter().all(|c|match *c {
			48..=57|65..=90|97..=122 => true,	//Alphanumerics.
			43|45|46 => true,			//+, - and .
			_ => false
		})
	}
}

defs!(cross_defs PathStr PathString);

impl PathStr
{
	pub fn find_s(&self, c: &AsciiStr) -> Option<usize> { self.0.find(&c.0) }
}

unsafe impl FilteredString for PathStr
{
	fn __cmp(a: &str, b: &str) -> Ordering { a.cmp(&b) }
	fn __hash<H>(a: &str, state: &mut H) where H: Hasher { a.hash(state); }
	fn __fmt(a: &str, f: &mut Formatter) -> FmtResult
	{
		//This never contains anything weird.
		f.write_str(a)
	}
	fn __validate(x: &[u8]) -> bool { x.iter().all(|c|*c>=33||*c<=126) }
}

defs!(cross_defs Base64UrlStr Base64UrlString);

impl Base64UrlStr
{
}

unsafe impl FilteredString for Base64UrlStr
{
	fn __cmp(a: &str, b: &str) -> Ordering { a.cmp(&b) }
	fn __hash<H>(a: &str, state: &mut H) where H: Hasher { a.hash(state); }
	fn __fmt(a: &str, f: &mut Formatter) -> FmtResult
	{
		//This never contains anything weird.
		f.write_str(a)
	}
	fn __validate(x: &[u8]) -> bool
	{
		x.iter().all(|c|match *c {
			48..=57|65..=90|97..=122 => true,	//Alphanumerics.
			45|95 => true,			//- and _.
			_ => false
		})
	}
}

pub fn base64url_string_to_ascii_string<'a>(h: &'a Base64UrlStr) -> &'a AsciiStr { unsafe{transmute(h)} }

pub fn indent(x: &str) -> String
{
	let mut y = String::new();
	let mut first = true;
	for line in x.lines() {
		let indent = if replace(&mut first, false) { "" } else { "\t" };
		writeln!(y, "{indent}{line}").ok();
	}
	y
}
