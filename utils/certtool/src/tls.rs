use super::An;
use super::deadline::Deadline;
use super::debug::Debugging;
use super::debug::DEBUG_TLS;
use super::http::HttpConnection;
use super::strings::AsciiStr;
use super::strings::AsciiString;
use super::strings::HostStr;
use super::strings::HostString;
use super::transport::Target;
use super::transport::Transport;
pub use btls::certificates::TrustAnchor;
pub use btls::certificates::TrustAnchorOrdered;
use btls::ClientConfiguration;
use btls::ClientConnection;
use btls::callbacks::TlsCallbacks;
use btls::Connection;
use btls::transport::Queue;
use btls::transport::PullTcpBuffer;
use btls::transport::PushTcpBuffer;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use std::collections::BTreeSet;
use std::net::SocketAddr;
use std::net::Shutdown;
use std::thread::sleep;
use std::time::Duration;
use std::fmt::Display;
use std::fmt::Formatter;
use std::fmt::Result as FmtResult;


#[derive(Copy,Clone,Debug)]
pub enum ErrorClass
{
	NameLookup,			//Name lookup error.
	Connect,			//Connect error.
	TcpRead,			//TCP read error.
	TcpWrite,			//TCP write error.
	TcpShutdown,			//TCP shutdown error.
	Handshake,			//TLS handshake error.
	Tls,				//Non-handshake TLS error.
	Application,			//Application error (reads only).
}

#[derive(Clone,Debug)]
pub struct TlsFault
{
	target: HostString,		//The host fault occured in.
	service: AsciiString,		//The service fault occured in.
	addr: Option<SocketAddr>,	//The IP address that faulted (if available).
	eclass: ErrorClass,		//The error class.
	estring: String,		//The error string.
}

impl Display for TlsFault
{
	 fn fmt(&self, f: &mut Formatter) -> FmtResult
	 {
		let &TlsFault{ref target, ref service, addr, eclass, ref estring} = self;
		let major = match eclass {
			ErrorClass::NameLookup => "Name lookup",
			ErrorClass::Connect => "TCP connect",
			ErrorClass::TcpRead => "TCP read",
			ErrorClass::TcpWrite => "TCP write",
			ErrorClass::TcpShutdown => "TCP shutdown",
			ErrorClass::Handshake => "TLS handshake",
			ErrorClass::Tls => "TLS",
			ErrorClass::Application => "Application"
		};
		//format!("{major} error: {estring}");
		write!(f, "{major} error on {target}({service})")?;
		if let Some(addr) = addr { write!(f, "[{addr}]")?; }
		write!(f, ": {estring}")
	 }
}


impl TlsFault
{
	pub fn new(target: &Target, eclass: ErrorClass, desc: String) -> TlsFault
	{
		let (tgt, serv) = target.get_target();
		TlsFault{
			target: tgt.to_owned(),
			service: serv.to_owned(),
			addr: Some(target.get_addr()),
			eclass: eclass,
			estring: desc,
		}
	}
	pub fn new_noip(target: &HostStr, service: &AsciiStr, eclass: ErrorClass, desc: String) -> TlsFault
	{
		TlsFault{
			target: target.to_owned(),
			service: service.to_owned(),
			addr: None,
			eclass: eclass,
			estring: desc,
		}
	}
}

struct Controller;
impl TlsCallbacks for Controller {}

#[derive(Clone)]
pub struct TrustAnchorSet(BTreeSet<TrustAnchorOrdered>);

impl TrustAnchorSet
{
	pub fn new() -> TrustAnchorSet
	{
		TrustAnchorSet(BTreeSet::new())
	}
	pub fn from_btls_list(ta: &[TrustAnchor]) -> TrustAnchorSet
	{
		let mut x = TrustAnchorSet::new();
		for entry in ta.iter() {
			x.0.insert(entry.as_ordered());
		}
		x
	}
	pub fn from_set(ta: &BTreeSet<TrustAnchorOrdered>) -> TrustAnchorSet
	{
		TrustAnchorSet(ta.clone())
	}
}

fn tls_tcp_write<T:Transport>(tls: &mut ClientConnection, socket: &mut T, addr: &Target, wqueue: &mut Queue,
	deadline: Deadline) -> Result<bool, TlsFault>
{
	//Do not do this with dead connections in order to not fault prematurely.
	if !tls.get_status().is_dead() {
		let tmp = tls.pull_tcp_data(PullTcpBuffer::no_data()).map_err(|x|{
			TlsFault::new(addr, ErrorClass::Tls, x.to_string())
		})?;
		wqueue.write(&tmp).ok();	//This should never fail.
	}
	let data = wqueue.map_read();
	if data.is_empty() { return Ok(true); }		//Break out on no data.
	let ret = match socket.write(data, deadline)? {
		0 => false,				//Try again.
		amt => {
			//Assume this succeeds.
			wqueue.discard(amt).ok();
			//If there is still need to write, irq_flag.poll() -> true.
			wqueue.buffered() == 0
		}
	};
	Ok(ret)
}

fn tls_tcp_read<T:Transport>(tls: &mut ClientConnection, socket: &mut T, addr: &Target, rqueue: &mut Queue,
	deadline: Deadline) -> Result<bool, TlsFault>
{
	let mut buffer = vec![0;16645];
	//Reads are pseudo-blocking, so no need to consider transient errors.
	let amt = match socket.read(&mut buffer, deadline)? {
		0 => {
			let msg = format!("Peer unexpectedly closed the connection");
			fail!(TlsFault::new(addr, ErrorClass::TcpRead, msg));
		},
		r => r,
	};
	let (tmp, eofd) = tls.push_tcp_data(PushTcpBuffer::u8_size(&buffer, amt)).map_err(|e|{
		TlsFault::new(addr, ErrorClass::Tls, e.to_string())
	})?;
	rqueue.write(&tmp).ok();
	if eofd { rqueue.send_eof(); }
	Ok(true)
}

fn tls_checkerror(addr: &Target, hs: bool, tls: &ClientConnection, allow_eof: bool) -> Result<(), TlsFault>
{
	let eclass = if hs { ErrorClass::Handshake } else { ErrorClass::Tls };
	let flags = tls.get_status();
	if flags.is_dead() {
		if let Some(abort) = tls.get_error() {
			fail!(TlsFault::new(addr, eclass, abort.to_string()));
		} else if !allow_eof {
			fail!(TlsFault::new(addr, eclass, format!("Unexpected close_notify")));
		}
	}
	Ok(())
}

fn client_config_from_trust_pile(trustpile: &TrustAnchorSet) -> Result<ClientConfiguration, TlsFault>
{
	let mut config = ClientConfiguration::new();
	for i in trustpile.0.iter() { config.add_trust_anchor(&TrustAnchor::from_ordered(i)); }
	Ok(config)
}

fn setup_tls_from_config(config: ClientConfiguration, target: &HostStr, _addr: &Target) ->
	Result<ClientConnection, TlsFault>
{
	let tls = config.make_connection(target.as_inner(), Box::new(Controller));
	Ok(tls)
}

fn setup_tls_session(trustpile: &TrustAnchorSet, addr: &Target) -> Result<ClientConnection, TlsFault>
{
	//Create a TLS session.
	let (target, _) = addr.get_target();
	//The trustpile needs to be non-empty.
	let tlsconfig = client_config_from_trust_pile(trustpile)?;
	let tls = setup_tls_from_config(tlsconfig, target, addr)?;
	Ok(tls)
}

fn setup_tls_session_alpncheck(_name: &HostStr, addr: &Target, acme_hash: [u8;32]) ->
	Result<ClientConnection, TlsFault>
{
	//Create a TLS session.
	let (target, _) = addr.get_target();
	let tls = ClientConfiguration::new().make_connection_acme_check(target.as_inner(),
		Box::new(Controller), acme_hash);
	Ok(tls)
}

fn tls_handshake<T:Transport>(tls: &mut ClientConnection, socket: &mut T, addr: &Target,
	rqueue: &mut Queue, wqueue: &mut Queue, allow_eof: bool, deadline: Deadline) -> Result<(), TlsFault>
{
	let mut flags = tls.get_status();
	while flags.is_handshaking() && !flags.is_dead() {
		flags = tls.get_status();
		fail_if!(deadline.expired(),
			TlsFault::new(addr, ErrorClass::Handshake, "Deadline exceeded".to_string()));
		let mut did_something = false;
		if flags.is_want_tx() {
			//Wants write... If the write fails with transient error, call write again.
			if !tls_tcp_write(tls, socket, addr, wqueue, deadline)? { continue; }
			did_something = true;
		}
		flags = tls.get_status();
		if flags.is_dead()  { break; } //Do not take quick fault on read.
		if flags.is_want_rx() {
			//Wants read...
			if !tls_tcp_read(tls, socket, addr, rqueue, deadline)? { continue; }
			did_something = true;
		}
		if !did_something {
			//Do not unnecressarily burn CPU.
			sleep(Duration::from_millis(50));
		}
	}

	tls_checkerror(addr, true, tls, allow_eof)?;
	Ok(())
}

fn tls_write_all<T:Transport>(tls: &mut ClientConnection, socket: &mut T, data: &[u8], addr: &Target,
	wqueue: &mut Queue, deadline: Deadline) -> Result<(), TlsFault>
{
	//The TLS write should accept all, but we need to flush the output.
	let tmp = tls.pull_tcp_data(PullTcpBuffer::write(data)).
		map_err(|x|TlsFault::new(addr, ErrorClass::Tls, x.to_string()))?;
	wqueue.write(&tmp).ok();	//Should never fail.
	tls_push_data(tls, socket, addr, wqueue, deadline)
}

fn tls_push_data<T:Transport>(tls: &mut ClientConnection, socket: &mut T, addr: &Target,
	wqueue: &mut Queue, deadline: Deadline) -> Result<(), TlsFault>
{
	//Push the data.
	while !tls.get_status().is_dead()
	{
		//Wants write... Repeat write until done.
		if tls_tcp_write(tls, socket, addr, wqueue, deadline)? { break; }
	}
	tls_checkerror(addr, false, tls, false)?;
	Ok(())
}

fn tls_read_reply<T:Sized,TR:Transport, F>(tls: &mut ClientConnection, socket: &mut TR, mut cb: F, addr: &Target,
	rqueue: &mut Queue, deadline: Deadline) -> Result<T, TlsFault> where F: FnMut(&[u8]) -> Result<T, String>
{
	fail_if!(deadline.expired(), TlsFault::new(addr, ErrorClass::Tls, "Deadline exceeded".to_string()));
	let mut flags = tls.get_status();
	while rqueue.buffered() > 0 || !flags.is_rx_end() && !flags.is_dead() {
		fail_if!(deadline.expired(), TlsFault::new(addr, ErrorClass::Tls, "Deadline exceeded".to_string()));
		flags = tls.get_status();
		if flags.is_want_rx() {
			//Wants read...
			//If there is still need to write, irq_flag.poll() -> true.
			if !tls_tcp_read(tls, socket, addr, rqueue, deadline)? { continue; }
		}
		flags = tls.get_status();
		//Forward EOF if done.
		if rqueue.buffered() == 0 && flags.is_rx_end() { break; }
		while rqueue.buffered() > 0 {
			let mut buf = [0;16384];
			let size = rqueue.read(&mut buf).unwrap_or(0);
			return cb(&buf[..size]).map_err(|x|TlsFault::new(addr, ErrorClass::Application,
				x.to_string()));
		}
	}
	tls_checkerror(addr, false, tls, false)?;
	static BLANK: [u8;0] = [];
	cb(&BLANK[..]).map_err(|x|TlsFault::new(addr, ErrorClass::Application, x.to_string()))
}

pub struct TlsConnection<T:Transport>
{
	addr: Target,
	tls: ClientConnection,
	tcp: T,
	rqueue: Queue,
	wqueue: Queue,
}

impl<T:Transport> Drop for TlsConnection<T>
{
	fn drop(&mut self)
	{
		//Try to be nice to the other end.
		self.goodbye();
	}
}

pub fn construct_target<T:Transport>(target: &HostStr, service: &AsciiStr, connection: &T) -> Target
{
	let paddr = connection.peer_addr();
	Target::make(target, service, &paddr)
}

impl<T:Transport> TlsConnection<T>
{
	pub fn get_addr(&self) -> SocketAddr { self.addr.get_addr() }
	pub fn call_underlying<F>(&mut self, cb: F) where F: FnOnce(&mut T) { cb(&mut self.tcp) }
	pub fn from_raw_tcp(target: &HostStr, service: &AsciiStr, connection: T, trustpile: &TrustAnchorSet,
		debug: &Debugging, deadline: Deadline) -> Result<TlsConnection<T>, TlsFault>
	{
		let addr = construct_target(target, service, &connection);
		TlsConnection::new_tail(addr, connection, trustpile, debug, deadline)
	}
	fn new_tail(addr: Target, mut connection: T, trustpile: &TrustAnchorSet, debug: &Debugging,
		deadline: Deadline) -> Result<TlsConnection<T>, TlsFault>
	{
		debug!(debug, DEBUG_TLS, "{addr}: handshaking TLS");
		let mut tls = setup_tls_session(trustpile, &addr)?;
		let mut rqueue = Queue::new();
		let mut wqueue = Queue::new();
		tls_handshake(&mut tls, &mut connection, &addr, &mut rqueue, &mut wqueue, false, deadline)?;
		debug!(debug, DEBUG_TLS, "{addr}: TLS handshake successful");
		Ok(TlsConnection {
			addr: addr,
			tls: tls,
			tcp: connection,
			rqueue: rqueue,
			wqueue: wqueue,
		})
	}
	pub fn new_alpn_check(target: &HostStr, challenge: &[u8], target_address: Option<SocketAddr>,
		debug: &Debugging, deadline: Deadline) -> Result<(), TlsFault>
	{
		//The port is hardcoded as 443.
		let (mut connection, addr) = T::establish_connection(target, An("443"), target_address, debug,
			deadline)?;
		debug!(debug, DEBUG_TLS, "{addr}: handshaking TLS");
		let mut challenge2 = [0;32];
		fail_if!(challenge.len() != 32, TlsFault::new(&addr, ErrorClass::Tls,
			format!("Bad ACME challenge length")));
		challenge2.copy_from_slice(challenge);
		let mut tls = setup_tls_session_alpncheck(target, &addr, challenge2)?;
		let mut rqueue = Queue::new();
		let mut wqueue = Queue::new();
		tls_handshake(&mut tls, &mut connection, &addr, &mut rqueue, &mut wqueue, true, deadline)?;
		debug!(debug, DEBUG_TLS, "{addr}: TLS handshake successful");
		//The handshake itself will check the challenge.
		debug!(debug, DEBUG_TLS, "{addr}: ACME TLS-ALPN handshake finished");
		Ok(())
	}
}

//Give 5 seconds for TLS goodbye.
const TLS_GOODBYE_TIMEOUT: Duration = Duration::from_millis(5000);

impl<T:Transport> HttpConnection for TlsConnection<T>
{
	fn send(&mut self, data: &[u8], deadline: Deadline) -> Result<(), TlsFault>
	{
		tls_write_all(&mut self.tls, &mut self.tcp, data, &self.addr, &mut self.wqueue, deadline)
	}
	//Only calls cb with empty data on EOF.
	fn receive<X:Sized, F>(&mut self, deadline: Deadline, cb: F) -> Result<X, TlsFault>
		where F: FnMut(&[u8]) -> Result<X, String>
	{
		tls_read_reply(&mut self.tls, &mut self.tcp, cb, &self.addr, &mut self.rqueue, deadline)
	}
	fn goodbye(&mut self)
	{
		//Give this a deadline.
		let dl = Deadline::new(TLS_GOODBYE_TIMEOUT);
		//Signal we are finished. No need to freak out if this does not work. tls_tcp_write also calls
		//to write encrypted stuff into queue.
		if let Ok(buf) = self.tls.pull_tcp_data(PullTcpBuffer::no_data().eof()) {
			self.wqueue.write(&buf).ok();	//Assume this never fails.
			let _ = tls_tcp_write(&mut self.tls, &mut self.tcp, &self.addr, &mut self.wqueue, dl);
		}
		let _ = self.tcp.shutdown(Shutdown::Write);
		sleep(Duration::from_millis(50));
	}
}

impl<T:Transport> Display for TlsConnection<T>
{
	 fn fmt(&self, f: &mut Formatter) -> FmtResult { write!(f, "{addr}", addr=self.addr) }
}
