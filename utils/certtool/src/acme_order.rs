use crate::A;
use crate::Deadline;
use crate::Debugging;
use crate::acme::AcmeAuthorization;
use crate::acme::AcmeAuthorizationTarget;
use crate::acme::AcmeChallenge;
use crate::acme::AcmeOrder;
use crate::acme::AuthzStatus;
use crate::acme::get_object_explicit;
use crate::acme::OrderStatus;
use crate::acme_service::AcmeServiceRegistered;
use crate::acme_service::get_object_registered;
use crate::acmeerror::format_json_error;
use crate::acmeerror::format_validation_record;
use crate::acmeverify::check_challenge;
use crate::csr::CsrInfo;
use crate::csr::CsrName;
use crate::debug::DEBUG_ACME;
use crate::dns::AddressReplaceList;
use crate::strings::AsciiStr;
use crate::strings::AsciiString;
use crate::strings::Base64UrlStr;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::ResultExt;
use btls_aux_json2::JSON;
use btls_aux_json2::json_object;
use std::collections::BTreeMap;
use std::collections::BTreeSet;
use std::collections::HashSet;
use std::fmt::Display;
use std::fmt::Write;
use std::mem::transmute;
use std::ops::Deref;
use std::ops::DerefMut;
use std::ptr::null_mut;
use std::sync::Mutex;
use std::sync::Condvar;
use std::sync::PoisonError;
use std::sync::atomic::AtomicPtr;
use std::sync::atomic::Ordering;
use std::thread::sleep;
use std::time::Duration;
use std::mem::replace;

const STATE_CHANGE_WAIT: Duration = Duration::from_secs(600);
const STATE_CHANGE_WAIT_S: Duration = Duration::from_secs(20);
const REQUEST_WAIT: Duration = Duration::from_millis(3000);
const VALIDATION_TIMEOUT: Duration = Duration::from_millis(60000);

pub trait Authorizer
{
	type State;
	fn new_state(&self, thumbprint: &Base64UrlStr, debug: &Debugging) -> <Self as Authorizer>::State;
	fn challenge(&self, state: &mut <Self as Authorizer>::State, id: &AcmeAuthorizationTarget,
		challenges: &BTreeMap<AsciiString, AcmeChallenge>, debug: &Debugging) -> Result<AsciiString, String>;
	fn commit(&self, state: &mut <Self as Authorizer>::State, debug: &Debugging) -> Result<(), String>;
	fn dump_log(&self, state: &<Self as Authorizer>::State, debug: &Debugging);
	fn get_replaces(&self) -> &AddressReplaceList;
}

pub struct HostAuthz
{
	aurl: AsciiString,
	id: AcmeAuthorizationTarget,
	chals: BTreeMap<AsciiString, AcmeChallenge>,
	method: Option<AsciiString>,
	done: bool,
	failed: bool,
	was_done: bool,
	is_processing: bool,
}

#[derive(Clone,Debug,Hash,PartialOrd,Ord,PartialEq,Eq)]
struct AuthorizationTarget(AsciiString, AsciiString);

struct AuthorizationLockTableI
{
	authz: BTreeSet<AsciiString>,
	targets: BTreeSet<AuthorizationTarget>,
}

impl AuthorizationLockTableI
{
	fn new() -> AuthorizationLockTableI
	{
		AuthorizationLockTableI {
			authz: BTreeSet::new(),
			targets: BTreeSet::new(),
		}
	}
}

struct AuthorizationLockTable
{
	locks: Mutex<AuthorizationLockTableI>,
	wake: Condvar,
}

impl AuthorizationLockTable
{
	fn singleton() -> &'static AuthorizationLockTable
	{
		static INSTANCE: AtomicPtr<AuthorizationLockTable> = AtomicPtr::new(null_mut());
		let ptr = INSTANCE.load(Ordering::Acquire);
		if !ptr.is_null() {
			//There is already allocated instance.
			return unsafe{transmute(ptr)};
		}
		//Does not exist yet, create.
		let new = Box::into_raw(Box::new(AuthorizationLockTable {
			locks: Mutex::new(AuthorizationLockTableI::new()),
			wake: Condvar::new(),
		}));
		let ptr = match INSTANCE.compare_exchange(null_mut(), new, Ordering::AcqRel, Ordering::Acquire) {
			//Allocated.
			Ok(_) => new,
			//We raced and lost. Free our box, return the old one.
			Err(old) => unsafe {
				drop(Box::from_raw(new));
				old
			}
		};
		//This is guaranteed to be non-NULL.
		unsafe{transmute(ptr)}
	}
}

struct TargetGuard<'a>
{
	to_drop: &'a AuthorizationTarget,
}

impl<'a> Drop for TargetGuard<'a>
{
	fn drop(&mut self)
	{
		let atab = AuthorizationLockTable::singleton();
		let mut guard = atab.locks.lock().unwrap_or_else(PoisonError::into_inner);
		guard.deref_mut().targets.remove(self.to_drop);
		//Notify the condition variable, so threads possibly blocked on the semaphore just freed are
		//released and try to acquire the semaphore again.
		atab.wake.notify_all();
		drop(guard)
	}
}

struct AuthroizationGuard<'a>
{
	to_drop: &'a AsciiStr,
}

impl<'a> Drop for AuthroizationGuard<'a>
{
	fn drop(&mut self)
	{
		let atab = AuthorizationLockTable::singleton();
		let mut guard = atab.locks.lock().unwrap_or_else(PoisonError::into_inner);
		guard.deref_mut().authz.remove(self.to_drop);
		//Notify the condition variable, so threads possibly blocked on the semaphore just freed are
		//released and try to acquire the semaphore again.
		atab.wake.notify_all();
		drop(guard)
	}
}

macro_rules! lock_body
{
	($xty:ident, $guard:ident, $table:ident, $list:ident) => {{
		//Sort the list of targets, in order to avoid ABBA deadlocks.
		let list: BTreeSet<&'a $xty> = $list.collect();
		//Lock the table.
		let atab = AuthorizationLockTable::singleton();
		let mut guards = Vec::new();
		let mut guard = atab.locks.lock().unwrap_or_else(PoisonError::into_inner);
		for item in list { loop {
			//If successful, add guard and go to next.
			if guard.deref_mut().$table.insert(item.to_owned()) {
				guards.push($guard {
					to_drop: item,
				});
				break;
			}
			//The item is locked! Wait for something to get unlocked (which might be the wanted item).
			guard = atab.wake.wait(guard).unwrap_or_else(PoisonError::into_inner);
		}}
		drop(guard);
		guards
	}}
}

fn lock_targets<'a>(list: impl Iterator<Item=&'a AuthorizationTarget>) -> Vec<TargetGuard<'a>>
{
	lock_body!(AuthorizationTarget, TargetGuard, targets, list)
}

fn lock_authorizations<'a>(list: impl Iterator<Item=&'a AsciiStr>) -> Vec<AuthroizationGuard<'a>>
{
	lock_body!(AsciiStr, AuthroizationGuard, authz, list)
}

fn read_host_authz(aurl: &AsciiStr, acme: &mut AcmeServiceRegistered, verbose: bool,
	expected_challenges: &HashSet<AcmeAuthorizationTarget>, debug: &Debugging) ->
	Result<Option<HostAuthz>, String>
{
	//Fetch the authorization.
	let authdata = get_object_registered::<AcmeAuthorization>(aurl, acme, debug)?;
	//Ignore authorization if already ready.
	let status = authdata.status;
	let tgt = &authdata.target;
	if status == AuthzStatus::Valid {
		if verbose { info!(debug, "Reusing validation for {tgt}"); }
		return Ok(None);
	}
	//Can't use expired, deactivated nor revoked authorizations. Those should not be present in non-invalid
	//orders anyway.
	if status == AuthzStatus::Expired || status == AuthzStatus::Deactivated || status == AuthzStatus::Revoked {
		fail!(format!("{tgt} has validation in illegal state {status}"));
	}
	//Handle validation that has a challenge in processing status, specially. Do not re-signal the validation,
	//and thus skip selecting the validation method and checking the challenge.
	let is_processing = authdata.any_challenge_processing;
	//Check that the CA does not send crap as challenge. Lowercase stuff to ignore case.
	let mut target2 = tgt.clone();
	target2.id_value.make_ascii_lowercase();
	fail_if!(!expected_challenges.contains(&target2),
		format!("Got illegal authorization request for {tgt}"));
	if verbose { info!(debug, "Need to prove control of {tgt}"); }
	Ok(Some(HostAuthz{
		aurl: aurl.to_owned(),
		id: tgt.clone(),
		chals: authdata.challenges.clone(),
		method: None,
		done: false,
		failed: false,
		was_done: false,
		is_processing: is_processing,
	}))
}

fn round_wait_authz(authz: &mut [HostAuthz], acme: &mut AcmeServiceRegistered, deadline: Deadline,
	debug: &Debugging) -> Result<(), String>
{
	let mut valid_bug = false;
	//Note: Do not even think about using a.method here, as it does not have any sane value if a.is_processing
	//is set. And a.is_processing does not affect waiting for request to complete.
	let sepline = "------------------------------------------------------------------------------";
	for a in authz.iter_mut() {
		//Do not repoll completed or failed ones.
		if a.done || a.failed { continue; }
		let id = &a.id;
		let aurl = &a.aurl;
		let authdata = get_object_registered::<AcmeAuthorization>(aurl, acme, debug)?;
		let astatus = authdata.status;
		if astatus == AuthzStatus::Valid {
			debug!(debug, DEBUG_ACME, "Got authorization to issue for {id}!");
			a.done = true;
			continue;
		}
		if !valid_bug && authdata.any_challenge_valid {
			warning!(debug, "BROKEN CA: Authorization with valid challenge not valid");
			valid_bug = true;
		}
		//Can just fail on timeout because authorizations should have completed way ago, so something
		//is very badly wrong.
		fail_if!(deadline.expired(), format!("Authorization {id}({aurl}): Status seems stuck at {status}",
			status=authdata.status));
		//If authorization is still pending, wait some more.
		if astatus == AuthzStatus::Pending { continue; }	//Wait some more.
		//Uh, oh... Failed. Note that this also includes states expired, deactivated and revoked, even if
		//those should never be seen.
		let mut errors = None;
		let mut vrecord = None;
		error!(debug, "Authorization {id}({aurl}): got into bad state '{astatus}' (expected valid):");
		for (_, chal) in authdata.challenges.iter() {
			errors = errors.or_else(||chal.error.clone());
			vrecord = vrecord.or_else(||chal.vrecord.clone());
		}
		if let Some(ref errors) = errors.as_ref() {
			error!(debug, "{sepline}");
			error!(debug, "{errors}", errors=format_json_error(errors));
			error!(debug, "{sepline}");
		} else {
			error!(debug, "<No detailed error available>");
		}
		if let Some(ref vrecord) = vrecord.as_ref() {
			error!(debug, "Validation details:");
			error!(debug, "{sepline}");
			error!(debug, "{vrecord}", vrecord=format_validation_record(vrecord));
			error!(debug, "{sepline}");
		};
		a.failed = true;
	}
	Ok(())
}

fn lookup_method<'a>(a: &'a HostAuthz) ->
	Result<(&'a AcmeAuthorizationTarget, &'a AsciiStr, &'a AcmeChallenge), String>
{
	let id = &a.id;
	match a.method {
		Some(ref method) => match a.chals.get(method) {
			Some(adata) => Ok((id, method, adata)),
			None => fail!(format!("Selected authorization method {method} not supported by CA for {id}"))
		},
		None => fail!(format!("No authentication method selected for {id}"))
	}
}

fn check_authz(authz: &mut [HostAuthz], thumbprint: &Base64UrlStr, authorizer: &impl Authorizer, debug: &Debugging) ->
	Result<(), String>
{
	let mut chal_failed = false;
	for a in authz.iter_mut() {
		//If validation is already processing, we have just wait for it, so skip the challenge check.
		//Note that lookup_method() fails if method is not set, but that can only happen if is_processing
		//is set, and that never gets unset.
		if a.is_processing { continue; }
		let (id, method, adata) = lookup_method(&a)?;
		debug!(debug, DEBUG_ACME, "Checking for correct {method} validation for {id}...");
		if let Err(err) = check_challenge(method, &id.id_type, &id.id_value, &adata.token, thumbprint,
			authorizer.get_replaces(), debug, Deadline::new(VALIDATION_TIMEOUT)) {
			error!(debug, "{id} Challenge check failed: {err}", id=a.id);
			chal_failed = true;
		}
	}
	fail_if!(chal_failed, "One or more challenge checks for order failed");
	Ok(())
}

fn signal_authz(authz: &mut [HostAuthz], acme: &mut AcmeServiceRegistered, debug: &Debugging) ->
	Result<(), String>
{
	for a in authz.iter_mut() {
		//If validation is already processing, we have just wait for it, so skip signaling the validation.
		//However, the signaling still should happen in invalid state, in case CA does not have fatal
		//validation errors.
		if a.is_processing { continue; }
		let (id, method, adata) = lookup_method(&a)?;
		debug!(debug, DEBUG_ACME, "Ready for {method} validation for {id}...");
		let vbody = JSON::Object(BTreeMap::new());
		//Authz signaling may be slow.
		acme.post_rawbody_retry(&adata.url, &vbody, debug).map_err(|err|{
			format!("ACKing {id} challenge {method}: {err}")
		})?;
	}
	Ok(())
}

fn prepare_authz<A:Authorizer>(authorizer: &A, authstate: &mut <A as Authorizer>::State, authz: &mut [HostAuthz],
	debug: &Debugging) -> Result<(), String>
{
	for a in authz.iter_mut() {
		//If validation is already processing, we have just wait for it, so skip preparing the validation.
		if a.is_processing { continue; }
		let id = &a.id;
		let kind = authorizer.challenge(authstate, &a.id, &a.chals, debug).map_err(|err|{
			let mpfx = "No known way to authorize";
			if err.len() > 0 { format!("{mpfx} {id}: {err}") } else { format!("{mpfx} {id}") }
		})?;
		debug!(debug, DEBUG_ACME, "Authorizing {id} using {kind}...");
		a.method = Some(kind);
	}
	Ok(())
}

fn prepare_authorizations(authz: &mut [HostAuthz], acme: &mut AcmeServiceRegistered,
	authorizer: &impl Authorizer, verbose: bool, debug: &Debugging) -> Result<(), String>
{
	//Serialize access in case there are multiple simultaneous requests for the same targets.
	let mut targets: Vec<AuthorizationTarget> = Vec::new();
	for a in authz.iter() {
		let id = &a.id;
		let target = AuthorizationTarget(id.id_type.clone(), id.id_value.clone());
		targets.push(target);
	}
	let guards = lock_targets(targets.iter());

	let thumbprint = acme.thumbprint().to_owned();
	let mut astate = authorizer.new_state(&thumbprint, debug);
	prepare_authz(authorizer, &mut astate, authz, debug).map_err(|e|{
		authorizer.dump_log(&astate, debug);
		e
	})?;
	//Commit the authorizations when it makes sense. If auth check fails, dump the log if any, as it may
	//contain hints why the thing failed.
	authorizer.commit(&mut astate, debug).map_err(|err|{
		authorizer.dump_log(&astate, debug);
		let mpfx = "Committing challenges failed";
		if err.len() > 0 { format!("{mpfx}: {err}") } else { mpfx.to_string() }
	})?;
	check_authz(authz, &thumbprint, authorizer, debug).map_err(|err|{
		authorizer.dump_log(&astate, debug);
		err
	})?;
	if verbose { for a in authz.iter() {
		//Objects with is_processing set do not have any sane method.
		if a.is_processing {
			info!(debug, "{id} is already processing, waiting for it to complete", id=a.id);
		} else {
			let method = match a.method.as_ref() {
				Some(method) => method.as_inner(),
				None => "<unknown>"
			};
			info!(debug, "Using {method} to prove control of {id}", id=a.id);
		}
	}}
	signal_authz(authz, acme, debug)?;
	let deadline = Deadline::new(STATE_CHANGE_WAIT);
	//Poll authorization completion.
	for a in authz.iter_mut() {
		debug!(debug, DEBUG_ACME, "Waiting for authorization {id} to change state...", id=a.id);
	}
	loop {
		round_wait_authz(authz, acme, deadline, debug)?;
		let mut finished = true;
		for a in authz.iter_mut() {
			if a.done && !a.was_done && verbose { info!(debug, "Proved control of {id}", id=a.id); }
			a.was_done = a.done;
			finished &= a.done || a.failed;
		}
		if finished { break; }	//Finally done.
		sleep(REQUEST_WAIT);
	}
	//If any authz failed, propagate error, since some ACME CA might have left the order pending. Returning
	//success would cause hang until timeout in that case.
	fail_if!(authz.iter().any(|a|a.failed), "One or more authorizations failed");
	drop(guards);
	Ok(())
}

pub fn raw_sign_csr(call_as: &impl Display, csr: &CsrInfo, acme: &mut AcmeServiceRegistered,
	authorizer: &impl Authorizer, verbose: bool, debug: &Debugging) -> Result<Vec<u8>, String>
{
	let sepline = "------------------------------------------------------------------------------";
	let mut namelist = Vec::new();
	let mut namestr = String::new();
	let mut expected_chals = HashSet::new();
	let raw_csr = csr.get_raw();
	//Collect all the names from the CSR.
	for name in csr.get_names().iter() {
		match name {
			&CsrName::Dns(ref name) => {
				write!(namestr, "{name} ").ok();
				namelist.push(json_object!(type => "dns", value => name));
				let (w,mut y) = if let Some(name) = name.strip_prefix("*.") {
					(true, name.to_owned())
				} else {
					(false, name.clone())
				};
				y.make_ascii_lowercase();	//Lowercase for comparision.
				expected_chals.insert(AcmeAuthorizationTarget {
					id_type: A("dns"),
					id_value: AsciiString::new_string(&y).
						set_err("CSR contains illegal non-ASCII DNS name")?,
					wildcard: w,
				});
			},
			&CsrName::Ip(ref addr) => {
				write!(namestr, "{addr} ").ok();
				namelist.push(json_object!(type => "ip", value => addr));
				let mut y = addr.clone();
				y.make_ascii_lowercase();	//Lowercase for comparision.
				expected_chals.insert(AcmeAuthorizationTarget {
					id_type: A("ip"),
					id_value: AsciiString::new_string(&y).
						set_err("CSR contains illegal non-ASCII IP address")?,
					wildcard: false,	//Never wildcard.
				});
			},
			&CsrName::Unknown(_) => fail!(format!("Unknown name type present in CSR!")),
			&CsrName::Name(_, _) => (),	//Skip these.
		}
	}
	let order_body = json_object!(identifiers => namelist);
	let orderurl = acme.directory().new_order;
	debug!(debug, DEBUG_ACME, "Creating a new order for {call_as}...");
	if verbose { info!(debug, "Creating order for {namestr}..."); }
	//Order creation should be fast.
	let (location, oblob) = acme.post_rawbody(&orderurl, &order_body, &debug, false)?;
	let location = if let Some(location) = location {
		debug!(debug, DEBUG_ACME, "Order identifer for {call_as} is {location}.");
		location
	} else {
		fail!(format!("NewOrder {orderurl} returned success but no URL???"));
	};
	let mut orderdata = get_object_explicit::<AcmeOrder>(&location, &oblob)?;
	if orderdata.status != OrderStatus::Pending && orderdata.status != OrderStatus::Ready {
		error!(debug, "Bad initial state for order {location}: {status} != pending/ready",
		       status=orderdata.status);
		if let Some(ref errors) = orderdata.error.as_ref() {
			error!(debug, "{sepline}");
			error!(debug, "{errors}", errors=format_json_error(errors));
			error!(debug, "{sepline}");
		} else {
			error!(debug, "<No detailed error available>");
		};
		fail!(format!("Order {location}: created in bad initial state '{status}'", status=orderdata.status));
	}
	if verbose { info!(debug, "Order URL is {location}"); }
	let mut unfinished_authz = Vec::new();
	//Do not collect authorizations if order is ready, only pending.
	if orderdata.status == OrderStatus::Pending {
		//If two orders with overlapping authorizations are processed simultaneously, serialize the
		//execution.
		let _guards = lock_authorizations(orderdata.authorizations.iter().map(|x|x.deref()));
		//Read the authorizations.
		for aurl in orderdata.authorizations.iter() {
			read_host_authz(aurl, acme, verbose, &expected_chals, &debug)?.map(|h|{
				unfinished_authz.push(h)
			});
		}
		//Commit the authorizations, signal those and wait for completion.
		prepare_authorizations(&mut unfinished_authz[..], acme, authorizer, verbose, debug)?;
		//The guard will be dropped here, releasing other threads.
	}
	if verbose && unfinished_authz.len() == 0 { info!(debug, "All validation data was reused"); }
	let mut deadline = Deadline::new(STATE_CHANGE_WAIT_S);
	//Reload the order, as it should now have transitioned to ready.
	orderdata = get_object_registered::<AcmeOrder>(&location, acme, &debug)?;
	let mut first = true;
	while orderdata.status == OrderStatus::Pending {
		if first {
			warning!(debug, "BROKEN CA: order {call_as} is still pending with all valid validations.");
		}
		if !replace(&mut first, false) {
			fail_if!(deadline.expired(),
				format!("Order {location}: Status seems stuck at {status}", status=orderdata.status));
		}
		//Reload the order, waiting for valid status. First load is outside loop, so wait even on first
		//round.
		sleep(REQUEST_WAIT);
		orderdata = get_object_registered::<AcmeOrder>(&location, acme, &debug)?;
	}
	if orderdata.status != OrderStatus::Ready {
		error!(debug, "Bad state for order {location}: {status} != ready", status=orderdata.status);
		if let Some(ref errors) = orderdata.error.as_ref() {
			error!(debug, "{sepline}");
			error!(debug, "{errors}", errors=format_json_error(errors));
			error!(debug, "{sepline}");
		} else {
			error!(debug, "<No detailed error available>");
		};
		fail!(format!("Order {location}: got into bad state '{status}' (expected ready)",
			status=orderdata.status));
	}
	if first {
		debug!(debug, DEBUG_ACME, "All identifers in order {call_as} were already authorized.");
	} else {
		debug!(debug, DEBUG_ACME, "Got authorization for all identifers in order {call_as}.");
	}
	//Now grab the finalize URL.
	let finurl = orderdata.finalize.as_ref().map(|x|{
		x.to_owned()
	}).ok_or_else(||{
		format!("Order {location}: ready but no 'finalize'")
	})?;
	if verbose { info!(debug, "Sending CSR for {namestr}"); }
	let finalize_body = json_object!(csr => raw_csr);
	debug!(debug, DEBUG_ACME, "Sending CSR for order {call_as}...");
	//CSR send may be slow.
	acme.post_rawbody(&finurl, &finalize_body, &debug, true)?;
	deadline = Deadline::new(STATE_CHANGE_WAIT);
	//Reload the order, now it should be in processing (or even valid).
	orderdata = get_object_registered::<AcmeOrder>(&location, acme, &debug)?;
	let mut first = true;
	while orderdata.status == OrderStatus::Ready || orderdata.status == OrderStatus::Processing {
		if first && orderdata.status == OrderStatus::Ready {
			warning!(debug, "BROKEN CA: Order {call_as} is still ready with CSR sent.");
		}
		if !replace(&mut first, false) {
			fail_if!(deadline.expired(),
				format!("Order {location}: Status seems stuck at {status}", status=orderdata.status));
		}
		//Reload the order, waiting for valid status. First load is outside loop, so wait even on first
		//round.
		sleep(REQUEST_WAIT);
		orderdata = get_object_registered::<AcmeOrder>(&location, acme, &debug)?;
	}
	//Now the order should be valid...
	if orderdata.status != OrderStatus::Valid {
		error!(debug, "Bad state for order {location}: {status} != valid", status=orderdata.status);
		if let Some(ref errors) = orderdata.error.as_ref() {
			error!(debug, "{sepline}");
			error!(debug, "{errors}", errors=format_json_error(errors));
			error!(debug, "{sepline}");
		} else {
			error!(debug, "<No detailed error available>");
		};
		fail!(format!("Order {location}: got into bad state '{status}' (expected valid)",
			status=orderdata.status));
	}
	//Now grab the certificate URL and the certificate.
	let certurl = orderdata.certificate.as_ref().ok_or_else(||{
		format!("Order {location}: valid but no 'certificate'")
	})?;
	if verbose { info!(debug, "Issued certificate at {certurl}"); }
	let certificate = acme.get(&certurl, &debug)?;

	debug!(debug, DEBUG_ACME, "Got the certificate for order {call_as}!");
	Ok(certificate)
}
