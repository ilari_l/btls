use crate::acme_common::AcmeServiceData;
use crate::acme_common::check_directory_reachable;
use crate::acme_common::check_directory_writable;
use crate::acme_common::check_is_directory;
use crate::acme_common::GlobalConfiguration;
use crate::acme_common::PerCertificateConfiguration;
use crate::acme_common::PerCertificateConfigurations;
use crate::acme_common::webroot_compat_entry;
use crate::acme_daemon::CertDirTag;
use crate::acme_daemon::ReqDirTag;
use crate::acme_daemon::StateDirTag;
use btls_certtool::exiterror;
use btls_certtool::read_file2;
use btls_aux_keypair_local::LocalKeyPair;
use btls_aux_memory::Attachment;
use btls_aux_memory::split_attach_first;
use btls_certtool::aia::IntermediatePile;
use btls_certtool::aia::SpecialIntermediatePile;
use btls_certtool::authmod::DlfcnModule;
use btls_certtool::debug::Debugging;
use btls_certtool::debug::get_debug_maybe;
use btls_certtool::debug::StderrDebug;
use btls_certtool::dns::AddressReplaceList;
use btls_certtool::strings::AsciiString;
use btls_certtool::strings::Base64UrlStr;
use btls_certtool::wcheck::Rootpile;
use std::cell::RefCell;
use std::collections::BTreeMap;
use std::collections::BTreeSet;
use std::collections::HashMap;
use std::ffi::OsString;
use std::fs::File;
use std::io::Error as IoError;
use std::io::ErrorKind as IoErrorKind;
use std::io::Read as IoRead;
use std::io::stderr;
use std::io::Write as IoWrite;
use std::os::unix::fs::MetadataExt;
use std::path::Path;
use std::path::PathBuf;
use std::process::exit;
use std::rc::Rc;


fn isspace(ch: u8) -> bool { ch == 9 || ch == 32 }

fn legacy_get_ca_url() -> AsciiString
{
	let p = Path::new("CA_URL");
	let tgt = p.read_link().ok();
	let tgt = tgt.map(|t|{
		AsciiString::new_osstring(&t.into_os_string()).unwrap_or_else(|bad|{
			exiterror!(E "CA_URL points to invalid URL {bad}")
		})
	});
	if let Some(tgt) = tgt {
		tgt
	} else {
		exiterror!(E "CA_URL does not exist or is not a symbolic link");
	}
}

fn legacy_get_test_ca_url() -> Option<AsciiString>
{
	let p = Path::new("TEST_CA_URL");
	let tgt = p.read_link().ok();
	let tgt = tgt.map(|t|{
		AsciiString::new_osstring(&t.into_os_string()).unwrap_or_else(|bad|{
			exiterror!(E "TEST_CA_URL points to invalid URL {bad}")
		})
	});
	tgt
}

fn legacy_get_auth_dir() -> PathBuf
{
	let p = Path::new("ACME_DIR");
	let tgt = p.read_link().ok();
	tgt.unwrap_or_else(||{
		exiterror!(E "ACME_DIR does not exist or is not a symbolic link")
	})
}

fn legacy_read_blacklist(pile: &mut IntermediatePile)
{
	let p = Path::new("ROOT_BLACKLIST");
	if !p.is_file() { return; }	//Do not require blacklist.
	let mut file = File::open(p).unwrap_or_else(|err|{
		exiterror!(E "Failed to open blacklist file '{p}': {err}'", p=p.display())
	});
	pile.read_blacklist_from_file(&mut file).unwrap_or_else(|err|{
		exiterror!(E "Failed to read blacklist file '{p}': {err}'", p=p.display())
	});
}

fn legacy_read_intermediate_pile(pile: &mut IntermediatePile, debug: &Debugging)
{
	let p = Path::new("INTERMEDIATES");
	if !p.is_file() { return; }	//Do not require blacklist.
	let mut file = File::open(&p).unwrap_or_else(|err|{
		exiterror!(E "Failed to open pile file '{p}': {err}", p=p.display())
	});
	pile.read_intermediate_pile_from_file(&mut file, debug).unwrap_or_else(|err|{
		exiterror!(E "Failed to read pile file '{p}': {err}", p=p.display())
	});
}

fn legacy_read_trust_anchors() -> Rootpile
{
	//This is only ever invoked in outer task, so use new_outer().
	let debug = get_debug_maybe(None, StderrDebug::new_outer());
	let mut ta_pile = Rootpile::new();
	ta_pile.add_file(Path::new("TRUSTPILE"), &debug).unwrap_or_else(|err|{
		writeln!(stderr(), "TRUSTPILE needs to contain the trusted root certificates").ok();
		writeln!(stderr(), "in PEM format").ok();
		exiterror!(E "{err}")
	});
	ta_pile
}

fn legacy_read_modules_file()
{
	let p = Path::new("MODULES");
	if !p.is_file() { return; }	//Do not require modules.
	let mut s = String::new();
	File::open(&p).and_then(|mut fp|{
		fp.read_to_string(&mut s)
	}).unwrap_or_else(|err|{
		exiterror!(E "Failed to read modules file '{p}': {err}", p=p.display())
	});
	for line in s.lines() {
		if line.starts_with("#") || line == "" { continue; }
		if line == "disable-modules" { LocalKeyPair::disable_module_loading(); }
		if line.starts_with("/") {
			LocalKeyPair::load_module_trusted(line).unwrap_or_else(|err|{
				exiterror!(E "Failed to load module {line}: {err}")
			});
		}
	}
}

pub fn legacy_global_config(debug: &Debugging, thumbprint: &Base64UrlStr) -> GlobalConfiguration
{
	legacy_read_modules_file();
	{
		let authdir = legacy_get_auth_dir();
		check_directory_reachable(&authdir).unwrap_or_else(|err|{
			exiterror!(E "{err}")
		});
		check_directory_writable(&authdir, thumbprint).unwrap_or_else(|err|{
			exiterror!(E "{err}")
		});
	}

	let mut globalcfg = GlobalConfiguration {
		debug: debug.clone(),
		pile: IntermediatePile::new(),
		spile: SpecialIntermediatePile::new(),
		replace: AddressReplaceList::new(),
		auth_modules: legacy_load_auth_modules(),
		reqdir: legacy_request_dir_o(),
		statedir: legacy_state_dir_o(),
		logsdir: Path::new("logs").to_path_buf(),
		post_renew: legacy_default_post_renew(),
		post_install: Vec::new(),
		initial_wait: 300,
		round_wait: 43200,
		error_wait: 3600,
		assume_revoked: BTreeSet::new(),
		ca_list: HashMap::new(),
		quiet: false,
		daemon: false,
	};
	legacy_read_blacklist(&mut globalcfg.pile);
	legacy_read_intermediate_pile(&mut globalcfg.pile, &debug);

	//Check that request directory exists, check that state directory is writable, and check that authentication
	//directory is writable.
	check_is_directory(globalcfg.reqdir.as_deref().into_inner()).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});
	check_directory_writable(&globalcfg.statedir.as_deref().into_inner(), thumbprint).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});
	globalcfg
}

fn legacy_default_post_renew() -> Vec<(PathBuf, Vec<OsString>)>
{
	let xname = "./POST_RENEW";
	let p = Path::new(xname);
	let tgt = p.metadata().ok();
	if let Some(tgt) = tgt {
		if tgt.is_file() && tgt.mode() & 64 != 0 {
			return vec![(p.to_owned(), Vec::new())];
		}
	}
	Vec::new()
}


fn legacy_load_auth_modules() -> Vec<(String, DlfcnModule, String)>
{
	let legacydir = legacy_get_auth_dir();
	let mut auth_modules = Vec::new();
	let acontent = read_file2("AUTHMODULES").and_then(|x|{
		String::from_utf8(x).map_err(|_|{
			IoError::new(IoErrorKind::Other, "File is not valid UTF-8")
		})
	}).unwrap_or_else(|err|{
		if err.kind() == IoErrorKind::NotFound {
			String::new()
		} else {
			exiterror!(E "Failed to read AUTHMODULES: {err}");
		}
	});
	for line in acontent.lines() {
		//Trim all whitespace.
		let mut s = 0;
		let mut e = line.len();
		while s < line.len() && isspace(line.as_bytes()[s]) { s += 1; }
		while e > s && isspace(line.as_bytes()[e-1]) { e -= 1; }
		let mut line = &line[s..e];
		let mut domlist = "";

		if line.starts_with("#") { continue; }	//Commented out.
		if line.len() == 0 { continue; }
		if let Some(line2) = line.strip_prefix("[") {		//Domain-restricted.
			let (head, tail) = match split_attach_first(line2, "]", Attachment::Center) {
				Ok(t) => t,
				Err(_) => {
					writeln!(stderr(), "Error: Bad line {line}").ok();
					continue;
				}
			};
			domlist = head;
			line = tail;
		}
		let (module, arguments) = split_attach_first(line, " ", Attachment::Center).unwrap_or((line, ""));
		let am = match DlfcnModule::new(module) {
			Ok(x) => x,
			Err(err) => {
				writeln!(stderr(), "Error: Failed to load {module}: {err}").ok();
				continue;
			}
		};
		auth_modules.push((domlist.to_owned(), am, arguments.to_owned()));
	}
	webroot_compat_entry(&mut auth_modules, legacydir).unwrap_or_else(|err|{
		exiterror!(E "{err}");
	});
	auth_modules
}

pub fn legacy_per_cert_config(debug: &Debugging) -> PerCertificateConfigurations
{
	let mut install_rootpile = Rootpile::new();
	install_rootpile.add_file(Path::new("ROOTPILE"), &debug).unwrap_or_else(|err|{
		exiterror!(E "Can not read ROOTPILE: {err}")
	});
	if install_rootpile.len() == 0 { exiterror!(E "ROOTPILE is empty (this can not work)"); }

	let kf = Path::new("ACCOUNT_KEY");
	if !kf.is_file() { exiterror!(E "Key {key} does not exist or is not a regular file", key=kf.display()); }
	let privkey =  {
		//Do not actually load the key, as it may be something that is not fork-safe.
		let mut contents = Vec::new();
		File::open(&kf).and_then(|mut fp|{
			fp.read_to_end(&mut contents)
		}).unwrap_or_else(|err|{
			exiterror!(E "Error loading {key}: {err}", key=kf.display())
		});
		contents
	};

	let acme = AcmeServiceData {
		name: "legacy".to_owned(),
		url: legacy_get_ca_url(),
		test_url: legacy_get_test_ca_url(),
		key_contents: privkey,
		key: None,
		load_key_err: None,
		key_path: kf.to_owned(),
		trust_anchors: legacy_read_trust_anchors(),
	};
	let certscfg = PerCertificateConfigurations {
		dflt: PerCertificateConfiguration {
			rootpile: install_rootpile,
			key: None,
			certdir: legacy_certificate_dir_o(),
			ca: Rc::new(RefCell::new(acme)),
		},
		special: BTreeMap::new(),
	};
	//Check that certificates is writeable.
	let thumbprint = Base64UrlStr::new_octets(b"test-thumbprint").unwrap();
	check_directory_reachable(certscfg.dflt.certdir.as_deref().into_inner()).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});
	check_directory_writable(certscfg.dflt.certdir.as_deref().into_inner(), &thumbprint).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});
	certscfg
}

fn legacy_request_dir_o() -> ReqDirTag<PathBuf> { ReqDirTag::new(Path::new("requests").to_owned()) }
fn legacy_state_dir_o() -> StateDirTag<PathBuf> { StateDirTag::new(Path::new("certificates").to_owned()) }
fn legacy_certificate_dir_o() -> CertDirTag<PathBuf> { CertDirTag::new(Path::new("certificates").to_owned()) }
