use btls_aux_memory::Attachment;
use btls_aux_memory::split_attach_first;
use std::fmt::Arguments;
use std::fmt::Debug;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;


pub struct Debugging
{
	pub mask: u64,
	pub out: Box<dyn DebuggingTarget+'static>,
}

impl Debug for Debugging
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "Debugging({mask:x})", mask=self.mask)
	}
}

impl Clone for Debugging
{
	fn clone(&self) -> Debugging
	{
		Debugging {
			mask: self.mask,
			out: self.out.clone_self()
		}
	}
}

pub trait DebuggingTarget
{
	fn out(&self, priority: char, args: Arguments);
	fn clone_self(&self) -> Box<dyn DebuggingTarget+'static>;
}

pub struct ForEachLine(Vec<u8>);

impl ForEachLine
{
	pub fn new() -> ForEachLine
	{
		ForEachLine(Vec::new())
	}
	pub fn for_each(&mut self, mut data: &[u8], mut cb: impl FnMut(&[u8]))
	{
		if self.0.len() > 0 {
			//Try flush the existing line.
			match split_attach_first(data, b"\n", Attachment::Center) {
				Ok((head, tail)) => {
					//Line got completed! Call it back and clear.
					self.0.extend_from_slice(head);
					cb(&self.0);
					self.0.clear();
					data = tail;
				}
				Err(data) => {
					//Line still incomplete.
					self.0.extend_from_slice(data);
					return;
				}
			}
		}
		while data.len() > 0 {
			//Process line.
			match split_attach_first(data, b"\n", Attachment::Center) {
				Ok((head, tail)) => {
					//Line got completed! Call it back.
					cb(head);
					data = tail;
				},
				Err(data) => {
					//Incomplete trailing line.
					self.0.extend_from_slice(data);
					break;
				}
			}
		}
	}
}


#[derive(Clone)]
pub struct StderrDebug
{
	inner_task: bool,
}

impl StderrDebug
{
	pub fn new_outer() -> StderrDebug { StderrDebug::new_maybe_inner(false) }
	pub fn new_maybe_inner(inner: bool) -> StderrDebug
	{
		StderrDebug {
			inner_task: inner,
		}
	}
}

pub fn severity_string(x: char) -> &'static str
{
	match x {
		'0' => "Emergency",
		'1' => "Alert",
		'2' => "Critical",
		'3' => "Error",
		'4' => "Warning",
		'5' => "Notice",
		'6' => "Info",
		'7' => "Debug",
		_ => "???"
	}
}

struct PrintSeverity(char);

impl Display for PrintSeverity
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		let x = severity_string(self.0);
		f.write_str(x)
	}
}

impl DebuggingTarget for StderrDebug
{
	fn out(&self, priority: char, args: Arguments)
	{
		use std::io::Write;
		use std::io::stderr;
		for line in args.to_string().lines() {
			let line = if self.inner_task {
				format!("<{priority}>{line}\n")
			} else {
				format!("{priority}: {line}\n", priority=PrintSeverity(priority))
			};
			stderr().write_all(line.as_bytes()).ok();
		}
		stderr().flush().ok();
	}
	fn clone_self(&self) -> Box<dyn DebuggingTarget+'static> { Box::new(self.clone()) }
}

//0x1 is free.
pub const DEBUG_HTTP_SUMMARY: u64 = 0x2;
pub const DEBUG_HTTP_MESSAGES: u64 = 0x4;
pub const DEBUG_HTTP_TRANSFER: u64 = 0x8;
pub const DEBUG_OCSP: u64 = 0x10;
pub const DEBUG_CHAIN_BUILD: u64 = 0x20;
//0x40 is free.
pub const DEBUG_DNS_ADDRESSES: u64 = 0x80;
pub const DEBUG_TLS: u64 = 0x100;
pub const DEBUG_TCP: u64 = 0x200;
pub const DEBUG_ACME_CHECKS: u64 = 0x400;
pub const DEBUG_ACCOUNT: u64 = 0x800;
pub const DEBUG_SIGNATURE: u64 = 0x1000;
pub const DEBUG_ACME: u64 = 0x2000;
pub const DEBUG_MODULES: u64 = 0x4000;
pub const DEBUG_CSR: u64 = 0x8000;
pub const DEBUG_DNS_CACHE: u64 = 0x10000;
pub const DEBUG_DNS_MSG_S: u64 = 0x20000;
pub const DEBUG_DNS_RAWMSG: u64 = 0x40000;
pub const DEBUG_DNS_RECORDS: u64 = 0x80000;

struct DebugItem(&'static str, u64);
static _DEBUG_ITEMS_LIST: [DebugItem;18] = [
	DebugItem("account", DEBUG_ACCOUNT),
	DebugItem("signature", DEBUG_SIGNATURE),
	DebugItem("acme-checks", DEBUG_ACME_CHECKS),
	DebugItem("tcp", DEBUG_TCP),
	DebugItem("tls", DEBUG_TLS),
	DebugItem("dns-addresses", DEBUG_DNS_ADDRESSES),
	DebugItem("ocsp", DEBUG_OCSP),
	DebugItem("chain-build", DEBUG_CHAIN_BUILD),
	DebugItem("http-summary", DEBUG_HTTP_SUMMARY),
	DebugItem("http-messages", DEBUG_HTTP_MESSAGES),
	DebugItem("http-transfer", DEBUG_HTTP_TRANSFER),
	DebugItem("acme", DEBUG_ACME),
	DebugItem("modules", DEBUG_MODULES),
	DebugItem("csr", DEBUG_CSR),
	DebugItem("dns-cache", DEBUG_DNS_CACHE),
	DebugItem("dns-message-summary", DEBUG_DNS_MSG_S),
	DebugItem("dns-raw-message", DEBUG_DNS_RAWMSG),
	DebugItem("dns-records", DEBUG_DNS_RECORDS),
];

fn trans_debug_item(x: &str) -> Option<u64>
{
	for i in _DEBUG_ITEMS_LIST.iter() { if x == i.0 { return Some(i.1); } }
	None
}

#[macro_export]
macro_rules! debug {
	($obj:expr, $level:expr, $($args:tt)*) => {{
		if $level == 0 || $level & $obj.mask != 0 {
			$obj.out.out($crate::debug::SEV_DEBUG, format_args!($($args)*));
		}
	}};
}

pub const SEV_EMERGENCY: char = '0';
pub const SEV_ALERT: char = '1';
pub const SEV_CRITICAL: char = '2';
pub const SEV_ERROR: char = '3';
pub const SEV_WARNING: char = '4';
pub const SEV_NOTICE: char = '5';
pub const SEV_INFO: char = '6';
pub const SEV_DEBUG: char = '7';


#[macro_export]
macro_rules! emergency {
	($obj:expr, $($args:tt)*) => {{ $obj.out.out($crate::debug::SEV_EMERGENCY, format_args!($($args)*)); }}
}

#[macro_export]
macro_rules! alert {
	($obj:expr, $($args:tt)*) => {{ $obj.out.out($crate::debug::SEV_ALERT, format_args!($($args)*)); }}
}

#[macro_export]
macro_rules! critical {
	($obj:expr, $($args:tt)*) => {{ $obj.out.out($crate::debug::SEV_CRITICAL, format_args!($($args)*)); }}
}

#[macro_export]
macro_rules! error {
	($obj:expr, $($args:tt)*) => {{ $obj.out.out($crate::debug::SEV_ERROR, format_args!($($args)*)); }}
}

#[macro_export]
macro_rules! warning {
	($obj:expr, $($args:tt)*) => {{ $obj.out.out($crate::debug::SEV_WARNING, format_args!($($args)*)); }}
}

#[macro_export]
macro_rules! notice {
	($obj:expr, $($args:tt)*) => {{ $obj.out.out($crate::debug::SEV_NOTICE, format_args!($($args)*)); }}
}

#[macro_export]
macro_rules! info {
	($obj:expr, $($args:tt)*) => {{ $obj.out.out($crate::debug::SEV_INFO, format_args!($($args)*)); }}
}

fn get_debug_none<Target:DebuggingTarget+Sized+'static>(target: Target) -> Debugging
{
	Debugging{mask:0, out: Box::new(target)}
}

fn get_debug<Target:DebuggingTarget+Sized+'static>(debugstr: &str, target: Target) -> Debugging
{
	let tmp = Debugging{mask:0, out:Box::new(target)};
	let mut mask = 0;
	for i in debugstr.split(",") {
		match trans_debug_item(i) {
			Some(x) => mask |= x,
			None => warning!(tmp, "Unrecognized debug category '{i}'" )
		};
	}
	Debugging{mask:mask, out: tmp.out}
}

pub fn get_debug_maybe<Target:DebuggingTarget+Sized+'static>(debugstr: Option<&str>, target: Target) -> Debugging
{
	if let Some(x) = debugstr { get_debug(x, target) } else { get_debug_none(target) }
}
