#![warn(unsafe_op_in_unsafe_fn)]
use btls_aux_fail::dtry;
use btls_aux_fail::f_continue;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::ResultExt;
use btls_aux_filename::Filename;
use btls_aux_filename::FilenameBuf;
use btls_aux_memory::SafeShowByteString;
use btls_aux_ocsp::validate_ocsp3;
use btls_aux_signatures::extract_raw_key;
use btls_aux_signatures::iterate_pem_or_derseq_fragments;
use btls_aux_signatures::KeyIdentify2;
use btls_aux_signatures::PemDerseqFragment2 as PFragment;
use btls_aux_signatures::PemFragmentKind as PFKind;
use btls_aux_signatures::SetOnce;
use btls_aux_signatures::SignatureAlgorithmEnabled2;
use btls_aux_time::Timestamp;
use btls_aux_unix::Umask;
use btls_aux_x509certparse::CertificateIssuer;
use btls_aux_x509certparse::ParsedCertificate2;
use csr::get_rsa_bits;
use deadline::Deadline;
use debug::Debugging;
use http::HttpConnection;
use http::split_url;
use httpget::Connection;
use std::cmp::max;
use std::env::args_os;
use std::ffi::OsString;
use std::fs::File;
use std::fs::read_dir;
use std::io::Read;
use std::io::Result as IoResult;
use std::ops::Deref;
#[cfg(unix)] use std::os::unix::fs::symlink;
use std::path::Path;
use std::path::PathBuf;
use std::str::from_utf8;
use std::str::FromStr;
use strings::AsciiStr;
use strings::AsciiString;
use strings::HostString;
use transport::TcpTransport;
use transport::Transport;


#[macro_use] pub mod debug;
pub mod acme;
pub mod acme_order;
pub mod acme_rncheck;
pub mod acme_service;
pub mod acmeerror;
pub mod acmeverify;
pub mod aia;
pub mod authmod;
pub mod csr;
pub mod deadline;
pub mod dns;
pub mod http;
pub mod httpget;
pub mod privatekey;
pub mod strings;
pub mod tls;
pub mod transport;
pub mod wcheck;

static GROUP256_MODULUS: &'static [u8; 256] = include_bytes!("rfc7919-tls-group-256.bin");
static GROUP257_MODULUS: &'static [u8; 384] = include_bytes!("rfc7919-tls-group-257.bin");
static GROUP258_MODULUS: &'static [u8; 512] = include_bytes!("rfc7919-tls-group-258.bin");

include!(concat!(env!("OUT_DIR"), "/product.inc"));

#[macro_export]
macro_rules! exiterror
{
	(P $pfx:expr, $($args:tt)*) => {{
		let err = format!($($args)*);
		println!("{pfx} {err}", pfx=$pfx);
		exit(2);
	}};
	(X $code:expr, $pfx:expr, $($args:tt)*) => {{
		let err = format!($($args)*);
		println!("{pfx} {err}", pfx=$pfx);
		exit($code);
	}};
	(E $($args:tt)*) => {{
		let err = format!($($args)*);
		println!("ERROR {err}");
		exit(2);
	}};
	(L $($args:tt)*) => {{
		let err = format!($($args)*);
		println!("LOW {err}");
		exit(1);
	}};
	(OK $($args:tt)*) => {{
		let err = format!($($args)*);
		println!("OK {err}");
		exit(0);
	}};
}

#[macro_export]
macro_rules! stderr
{
	($($a:tt)*) => {{ let _ = writeln!(stderr(), $($a)*); }};
}

#[macro_export]
macro_rules! abort
{
	($($a:tt)*) => {{ stderr!($($a)*); exit(1); }};
}

#[derive(Clone)]
struct CertChainIterator<'a>(&'a [Vec<u8>], usize);

impl<'a> Iterator for CertChainIterator<'a>
{
	type Item = &'a [u8];
	fn next(&mut self) -> Option<&'a [u8]>
	{
		let t = self.0.get(self.1)?;
		self.1 += 1;
		Some(t.deref())
	}
}

#[derive(Clone,Debug)]
pub struct CertChain(Vec<Vec<u8>>);

impl CertChain
{
	pub fn new() -> CertChain
	{
		CertChain(Vec::new())
	}
	pub fn push(&mut self, der: &[u8]) { self.0.push(der.to_vec()); }
	pub fn iter<'a>(&'a self) -> impl Iterator<Item=&'a [u8]>+Clone+'a { CertChainIterator(&self.0, 0) }
	pub fn push_multiple<'a>(&mut self, chain: impl Iterator<Item=&'a [u8]>)
	{
		for item in chain { self.push(item); }
	}
	fn first_cert(&self) -> Option<&[u8]> { self.0.get(0).map(|t|t.deref()) }
	fn len(&self) -> usize { self.0.len() }
}

pub fn read_single_cert(file: &Path) -> Result<Vec<u8>, String>
{
	let t = read_cert_file(file, false)?;
	let t = t.first_cert().ok_or_else(||{
		format!("File {file} did not contain any certificates", file=file.display())
	})?;
	Ok(t.to_vec())
}


pub fn read_cert_file(p: &Path, strict: bool) -> Result<CertChain, String>
{
	//Read the key.
	let mut contents = Vec::new();
	File::open(p).and_then(|mut x|x.read_to_end(&mut contents)).map_err(|err|{
		format!("Failed to read '{p}': {err}", p=p.display())
	})?;
	//Unwrap PEM if any.
	let mut contents2 = CertChain::new();
	use self::PFragment::*;
	iterate_pem_or_derseq_fragments(&contents, |frag|match frag {
		Comment(_) => Ok(()),
		Error => Err(format!("Not in PEM nor DERseq format")),
		Der(cert)|Pem(PFKind::Certificate,cert) => Ok(contents2.push(cert)),
		Pem(ref kind,_) if strict => Err(format!("Illegal PEM fragment {kind}")),
		Pem(_,_) => Ok(())	//Ignore unknown.
	}).map_err(|err|{
		format!("{p}: {err}", p=p.display())
	})?;
	Ok(contents2)
}

pub fn decode_pem_or_der_csr(data: &[u8], mut comment: impl FnMut(&[u8]) -> Result<(), &'static str>) ->
	Result<Vec<u8>, &'static str>
{
	use self::PFragment::*;
	let mut contents: SetOnce<Vec<u8>> = Default::default();
	let err = "Not a valid certificate request in PEM or DER format";
	iterate_pem_or_derseq_fragments(data, |frag|match frag {
		Comment(line) => comment(line),
		Der(csr)|Pem(PFKind::CertificateRequest,csr) => contents.set(csr.to_owned(), ||err),
		_ => Err(err),
	})?;
	contents.get(||err)
}

fn good_firstline(buffer: &[u8]) -> Result<Option<usize>, ()>
{
	if buffer.len() < 12 { return Ok(None); }
	fail_if!(&buffer[..7] != b"HTTP/1.", ());
	fail_if!(buffer[7].wrapping_sub(48) > 9, ());
	fail_if!(buffer[8] != b' ', ());
	fail_if!(buffer[9].wrapping_sub(48) > 5, ());
	fail_if!(buffer[10].wrapping_sub(48) > 9, ());
	fail_if!(buffer[11].wrapping_sub(48) > 9, ());
	fail_if!(buffer[12] != b' ', ());
	for i in 12..buffer.len()-1 { if buffer[i] == 13 && buffer[i + 1] == 10 { return Ok(Some(i + 2)); } }
	Ok(None)
}

fn check_http_response(buffer: &[u8], eof: bool, ret_etag: &mut Option<AsciiString>,
	lastmod: &mut Option<AsciiString>) -> Result<Option<Vec<u8>>, String>
{
	let flen = match good_firstline(buffer) {
		Ok(Some(x)) => x,
		Ok(None) => return Ok(None),	//Incomplete.
		Err(_) => fail!("HTTP: Bad response-line".to_owned())
	};
	fail_if!(&buffer[9..12] == b"304", String::new());
	fail_if!(buffer[9] != b'2' || &buffer[9..12] == b"206", format!("HTTP: request failed with code {code}",
		code=SafeShowByteString(&buffer[9..12])));
	let buffer = &buffer[flen..];
	//Now, parse headers one by one.
	let mut itr = 0;
	let mut is_chunked = false;
	let mut length = None;
	*ret_etag = None;
	while itr < buffer.len() {
		let x = &buffer[itr..];
		let mut s = None;
		for i in 0..x.len()-1 {
			if x[i] == 13 && x[i + 1] == 10 { s = Some(i); break; }
		}
		let s = match s { Some(x) => x, None => return Ok(None) };
		let x = &x[..s];
		itr += s + 2;
		if x.len() == 0 { break; }	//End of headers.
		fail_if!(x[0] == 9 || x[0] == 32, "HTTP: Unsupported continuation header line".to_owned());
		let mut sp = None;
		for i in 0..x.len() { if x[i] == b':' { sp = Some(i); break; } }
		let sp = sp.ok_or_else(||{
			format!("HTTP: Bad header-line ({line})", line=SafeShowByteString(x))
		})?;
		//Split header into name and value. Lowercase the name, strip leading and trailing whitespace from
		//the value.
		let (hname, hvalue) = (&x[..sp], &x[sp+1..]);
		let mut ps = 0;
		let mut pe = hvalue.len();
		while ps < hvalue.len() && (hvalue[ps] == 9 || hvalue[ps] == 32) { ps += 1; }
		while pe > ps && (hvalue[pe-1] == 9 || hvalue[pe-1] == 32) { pe -= 1; }
		let hvalue = &hvalue[ps..pe];
		let hname: String = hname.iter().cloned().map(|c|c.to_ascii_lowercase() as char).collect();
		//Recognize some special headers.
		if &hname == "content-length" {
			length = Some(from_utf8(hvalue).ok().and_then(|y|{
				usize::from_str(y).ok()
			}).ok_or("HTTP: Bad Content-Length")?);
		} else if &hname == "transfer-encoding" {
			fail_if!(hvalue != b"chunked", "HTTP: Unsupported Transfer-Encoding".to_owned());
			is_chunked = true;
		} else if &hname == "etag" {
			*ret_etag = Some(AsciiString::new_octets(hvalue).map_err(|bad|{
				format!("Bad etag '{bad}'")
			})?);
		} else if &hname == "last-modified" {
			*lastmod = Some(AsciiString::new_octets(hvalue).map_err(|bad|{
				format!("Bad last-modified '{bad}'")
			})?);
		}
	}
	let buffer = &buffer[itr..];	//Body.
	if !is_chunked {
		//Just return the chunk if available.
		return Ok(if let Some(len) = length {
			if len <= buffer.len() { Some((&buffer[..len]).to_owned()) } else { None }
		} else if eof {
			Some(buffer.to_owned())
		} else {
			None
		});
	} else {
		let mut ret = Vec::new();
		let mut itr = 0;
		while itr < buffer.len() {
			let x = &buffer[itr..];
			let mut s = None;
			for i in 0..x.len()-1 {
				if x[i] == 13 && x[i + 1] == 10 { s = Some(i); break; }
			}
			let s = match s { Some(x) => x, None => return Ok(None) };
			let x = &x[..s];
			itr += s + 2;
			//Find ; in x if any.
			let mut s = x.len();
			for i in 0..x.len() { if x[i] == b';' { s = i; break; } }
			let x = &x[..s];
			let size = from_utf8(x).ok().and_then(|y|{
				usize::from_str(y).ok()
			}).ok_or("HTTP: Bad chunk length")?;
			if size == 0 { return Ok(Some(ret)); }	//That is all.
			ret.extend_from_slice(&buffer[itr..][..size]);
			itr += size;
			fail_if!(buffer[itr] != 13 || buffer[itr+1] != 10, "HTTP: Bad chunk ending CRLF");
			itr += 2;
		}
		Ok(None)	//Not end yet.
	}
}
/*
#[test]
fn test_parse_blast()
{
	assert_eq!(check_http_response(b"\
		HTTP/1.1 200 OK\r\n\
		\r\n\
		Hello", true), Ok(Some((&b"Hello"[..]).to_owned())));
}

#[test]
fn test_parse_304()
{
	assert_eq!(check_http_response(b"\
		HTTP/1.1 304 Not Modified\r\n\
		\r\n", true), Err(String::new()));
}

#[test]
fn test_parse_blast_noeof()
{
	assert_eq!(check_http_response(b"\
		HTTP/1.1 200 OK\r\n\
		\r\n\
		Hello", false), Ok(None));
}

#[test]
fn test_parse_length()
{
	assert_eq!(check_http_response(b"\
		HTTP/1.1 200 OK\r\n\
		CONTENT-LENGTH: \t 5  \t  \r\n\
		\r\n\
		Hello, World!", false), Ok(Some((&b"Hello"[..]).to_owned())));
}

#[test]
fn test_parse_chunked()
{
	assert_eq!(check_http_response(b"\
		HTTP/1.1 200 OK\r\n\
		TRANSFER-encoding: \t chunked  \t  \r\n\
		\r\n\
		5;HI\r\n\
		Hello\r\n\
		2\r\n\
		, \r\n\
		6;\r\n\
		World!\r\n\
		0\r\n", false), Ok(Some((&b"Hello, World!"[..]).to_owned())));
}
*/

pub fn fetch_http_file(url: &AsciiStr, ret_etag: &mut Option<AsciiString>, lastmod: &mut Option<AsciiString>,
	debug: &Debugging, deadline: Deadline) -> Result<Vec<u8>, String>
{
	let (scheme, host, port, path) = split_url(url, None).set_err("Invalid URL syntax")?;
	let is_http = scheme == "http";
	let defport = 80;
	fail_if!(!is_http, "Only http:// URLs are supported");
	//No spaces or '#' in path.
	fail_if!(path.chars().any(|c|c==' '||c=='#'), "Invalid URL syntax");

	let port = port.unwrap_or(defport);
	let oport = Ao(port.to_string());
	//Connect.
	let (tcp, _) = TcpTransport::establish_connection(host, &oport, None, debug, deadline).map_err(|err|{
		err.to_string()
	})?;
	let mut tcp = Connection::new_tcp_connected(host, &oport, tcp, debug).map_err(|err|{
		format!("HTTP GET {url}: {err}")
	})?;
	//If hostname contains : (IPv6 address) it needs escaping.
	let need_escape = host.chars().any(|c|c==':');
	let (lh, rh) = if need_escape { ("[", "]") } else { ("", "") };
	//Form the HTTP query and send it.
	let hosthdr = if port == defport {
		format!("{lh}{host}{rh}")
	} else {
		format!("{lh}{host}{rh}:{port}")
	};
	let etag2 = if let Some(etag) = ret_etag.as_ref() {
		format!("if-none-match: {etag}\r\n")
	} else {
		String::new()
	};
	let lastmod2 = if let Some(lastmod) = lastmod.as_ref() {
		format!("if-modified-since: {lastmod}\r\n")
	} else {
		String::new()
	};
	let httpquery = format!("GET {path} HTTP/1.1\r\nHost: {hosthdr}\r\nconnection: close\r\n\
		user-agent: certtool-install/{PRODUCT_VERSION}\r\n{etag2}{lastmod2}\r\n");
	tcp.send(httpquery.as_bytes(), deadline).map_err(|err|{
		format!("HTTP GET {url}: {err}")
	})?;
	//Ok, now wait for response.
	let mut response = Vec::new();
	loop {
		let mut eof = false;

		tcp.receive(deadline, |data|{
			eof = data.len() == 0;
			let osize = response.len();
			response.resize(osize + data.len(), 0);
			(&mut response[osize..]).copy_from_slice(data);
			Ok(())
		}).map_err(|err|{
			format!("HTTP GET {url}: {err}")
		})?;
		fail_if!(response.len() > 192 * 1024, "HTTP: Response too big".to_owned());
		if let Some(x) = check_http_response(&response, eof, ret_etag, lastmod)? { return Ok(x); }
		fail_if!(eof, "HTTP: Response truncated".to_owned());
	}
}

pub fn do_world_umask()
{
	Umask::PUBLIC.swap();
}

pub fn with_private_umask<T, F>(f: F) -> T where F: FnOnce() -> T
{
	let mask = Umask::PRIVATE.swap();	//Set private.
	let x = f();
	mask.swap();	//Restore old.
	x
}


#[cfg(unix)]
pub fn symlink_equiv<P: AsRef<Path>, Q: AsRef<Path>>(src: P, dst: Q) -> IoResult<()> { symlink(src, dst) }

#[cfg(not(unix))]
pub fn symlink_equiv<P: AsRef<Path>, Q: AsRef<Path>>(src: P, dst: Q) -> IoResult<()>
{
	let content = dst.as_ref().display().to_string();
	File::create(src).and_then(|mut x|x.write_all(content.as_bytes()))
}

static BASE64CHARS: &'static [u8;64] = b"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

fn push_base64grp(ret: &mut Vec<u8>, tmp: usize)
{
	ret.push(BASE64CHARS[(tmp>>18)&63]);
	ret.push(BASE64CHARS[(tmp>>12)&63]);
	ret.push(BASE64CHARS[(tmp>>6)&63]);
	ret.push(BASE64CHARS[tmp&63]);
}

fn push_base64_tail(ret: &mut Vec<u8>, tmp: usize, modulus: u32)
{
	if modulus == 1 {
		ret.push(BASE64CHARS[(tmp>>2)&63]);
		ret.push(BASE64CHARS[(tmp<<4)&63]);
		ret.push(b'=');
		ret.push(b'=');
	}
	if modulus == 2 {
		ret.push(BASE64CHARS[(tmp>>10)&63]);
		ret.push(BASE64CHARS[(tmp>>4)&63]);
		ret.push(BASE64CHARS[(tmp<<2)&63]);
		ret.push(b'=');
	}
}

///Encode a Data using base64
pub fn encode_base64(data: &[u8]) -> String
{
	let mut ret = Vec::new();
	let mut modulus = 0;
	let mut tmp = 0usize;
	for i in data.iter() {
		tmp = (tmp << 8) | (*i as usize);
		modulus += 1;
		if modulus == 3 {
			push_base64grp(&mut ret, tmp);
			modulus = 0;
		}
	}
	push_base64_tail(&mut ret, tmp, modulus);
	from_utf8(&ret).unwrap().to_owned()
}

///Encode a PEM object.
pub fn encode_pem_object(raw: &[u8], kind: &[u8]) -> Vec<u8>
{
	let mut ret = Vec::new();
	let mut lineblk = 0;
	let mut modulus = 0;
	let mut tmp = 0usize;
	ret.extend_from_slice(b"-----BEGIN ");
	ret.extend_from_slice(kind);
	ret.extend_from_slice(b"-----\n");
	for i in raw.iter() {
		tmp = (tmp << 8) | (*i as usize);
		modulus += 1;
		if modulus == 3 {
			push_base64grp(&mut ret, tmp);
			lineblk = lineblk + 1;
			if lineblk == 18 {
				ret.push(b'\n');
				lineblk = 0;
			}
			modulus = 0;
		}
	}
	push_base64_tail(&mut ret, tmp, modulus);
	if lineblk > 0 || modulus > 0 { ret.push(b'\n'); }
	ret.extend_from_slice(b"-----END ");
	ret.extend_from_slice(kind);
	ret.extend_from_slice(b"-----\n");
	ret
}

///Encode a Cert in PEM format
pub fn encode_pem_cert(cert: &[u8]) -> Vec<u8> { encode_pem_object(cert, b"CERTIFICATE") }

pub fn bintextcode_enc(x: &[u8]) -> HostString
{
	let mut s = HostString::with_capacity(x.len() * 8 / 5 + 1);
	let mut accum = 0;
	let mut accum_bits = 0;
	for i in x.iter() {
		accum += (*i as u32) << accum_bits;
		accum_bits += 8;
		while accum_bits >= 5 {
			let c = match accum&31 {
				c@0..=25 => c as u8 + 97,
				c@26..=31 => c as u8 + 22,
				_ => 45,	//Ca not happen.
			};
			s.push_byte(c);
			accum >>= 5;
			accum_bits -= 5;
		}
	}
	if accum_bits > 0 {
		let c = match accum&31 {
			c@0..=25 => c as u8 + 97,
			c@26..=31 => c as u8 + 22,
			_ => 45,	//Ca not happen.
		};
		s.push_byte(c);
	}
	s
}

pub fn bintextcode_dec(x: &str) -> Vec<u8>
{
	let mut s = Vec::with_capacity(x.len() * 5 / 8 + 1);
	let mut accum = 0;
	let mut accum_bits = 0;
	for i in x.chars() {
		let i = i as u32;
		let c = match i {
			65..=90|97..=122 => (i & 31) - 1,
			48..=53 => i - 22,
			_ => 0,
		};
		accum += c << accum_bits;
		accum_bits += 5;
		while accum_bits >= 8 {
			s.push(accum as u8);
			accum >>= 8;
			accum_bits -= 8;
		}
	}
	s
}


#[allow(non_snake_case)] pub fn S(x: &str) -> String { x.to_owned() }
#[allow(non_snake_case)] pub fn A(x: &str) -> AsciiString { AsciiString::new_string(x).unwrap() }
#[allow(non_snake_case)] pub fn An<'a>(x: &'a str) -> &'a AsciiStr { AsciiStr::new_string(x).unwrap() }
#[allow(non_snake_case)] pub fn Ao(x: String) -> AsciiString { AsciiString::from_string(x).unwrap() }


pub struct OcspValidationResult
{
	pub remaining: u32,
	pub lifetime_low: bool,
}

pub enum OcspValidationError
{
	Revoked,	//Certificate has been REVOKED.
	Other(String),	//Other errror.
}

pub fn check_ocsp<'a>(ocsp: &[u8], serial: &[u8], issuer: CertificateIssuer<'a>, issuerkey: &[u8]) ->
	Result<OcspValidationResult, OcspValidationError>
{
	let time_now = Timestamp::now();
	let result = validate_ocsp3(ocsp, issuer, serial, issuerkey, time_now,
		SignatureAlgorithmEnabled2::unsafe_any_policy()). map_err(|x|if x.is_revoked() {
		OcspValidationError::Revoked
	} else {
		OcspValidationError::Other(x.to_string())
	})?;
	let lifetime = result.not_before.delta(result.not_after);
	let lived = result.not_before.delta(time_now);
	let remain = lifetime.saturating_sub(lived);
	//Declare lifetime low if less than 1/3 remain, or if less than 6 hours remain.
	let lifetime_low = remain < lifetime / 3 || remain < 21600;
	Ok(OcspValidationResult{remaining: remain as u32, lifetime_low:lifetime_low})
}

pub fn read_file<P1: AsRef<Path>>(name: P1) -> Result<Vec<u8>, String>
{
	let name = name.as_ref();
	read_file2(name).map_err(|err|{
		format!("Can not read file '{name}': {err}", name=name.display())
	})
}

pub fn read_file2<P1: AsRef<Path>>(name: P1) -> Result<Vec<u8>, std::io::Error>
{
	let mut contents = Vec::new();
	File::open(name).and_then(|mut file|file.read_to_end(&mut contents))?;
	Ok(contents)
}

pub fn path_append_extension(p: &Path, next: &str) -> PathBuf
{
	let mut p2 = p.to_owned();
	if let Some(f) = p.file_name() {
		let mut f = f.to_owned();
		f.push(".");
		f.push(next);
		p2.set_file_name(f);
	} else {
		//This is bit questionable thing to do, but...
		p2.set_file_name(next)
	}
	p2
}

//Currently has restriction that the filename part of basename must consist of valid UTF-8.
//basename may have directory part.
pub fn next_directory_filename<P:AsRef<Path>>(basename: P) -> Result<PathBuf, String>
{
	let basename = basename.as_ref();
	let parentdir = basename.parent().unwrap_or(Path::new("."));
	let parentdir = if parentdir.as_os_str().is_empty() { Path::new(".") } else { parentdir };
	let parentdir = parentdir.canonicalize().map_err(|err|{
		format!("Unable to canonicalize path: {err}")
	})?;
	let mut prefix = Filename::from_path_file_name(basename).ok_or_else(||{
		format!("Invalid target filename '{basename}'", basename=basename.display())
	})?.to_owned();
	prefix.push("-");	//Add the '-' into prefix.
	let names = read_dir(&parentdir).map_err(|err|{
		format!("Failed to read directory '{parent}': {err}", parent=parentdir.display())
	})?;
	let mut next = 0u64;
	for i in names {
		let i = i.map_err(|err|{
			format!("Failed to read directory '{parent}' entry: {err}", parent=parentdir.display())
		})?;
		let f = FilenameBuf::from_os_string(i.file_name());
		//The names where split fails or tail is not unicode are not interesting.
		let n = match f.match_prefix(&prefix).and_then(|x|x.into_str()) {
			Some(tail) => match u64::from_str(tail) { Ok(x) => x + 1, Err(_) => continue },
			None => continue
		};
		next = max(next, n);            //To biggest.
	}
	let mut nname = parentdir.to_path_buf();
	prefix.push_fmt(format_args!("{next:04}"));
	nname.push(prefix.into_os_str());
	Ok(nname)
}

pub fn get_rsa_dh_parameters(rsa_mag: Option<usize>) -> Vec<u8>
{
	if let Some(rsa_mag) = rsa_mag {
		let trailer = [2, 1, 2];
		let (header, body) = if rsa_mag <= 2048 {
			//2048-bit parameters.
			([48, 130, 1, 8, 2, 130, 1, 1, 0], &GROUP256_MODULUS[..])
		} else if rsa_mag <= 3072 {
			//3072 bit parameters.
			([48, 130, 1, 136, 2, 130, 1, 129, 0], &GROUP257_MODULUS[..])
		} else {
			//4096 bit parameters.
			([48, 130, 2, 8, 2, 130, 2, 1, 0], &GROUP258_MODULUS[..])
		};
		let mut out = Vec::with_capacity(header.len() + body.len() + trailer.len());
		out.extend_from_slice(&header);
		out.extend_from_slice(body);
		out.extend_from_slice(&trailer);
		encode_pem_object(&out, b"DH PARAMETERS")
	} else {
		Vec::new()
	}
}

pub fn _get_dh_params(cert: &[u8]) -> Result<Vec<u8>, ()>
{
	let p = dtry!(ParsedCertificate2::from(cert));
	let p = p.pubkey;
	let algo = KeyIdentify2::identify(&p);
	let keydata = extract_raw_key(&p);
	let mut rsa_mag = None;
	match (algo, keydata) {
		(KeyIdentify2::RsaGeneric, Ok(keydata)) => rsa_mag = get_rsa_bits(keydata).ok(),
		(KeyIdentify2::RsaPss, Ok(keydata)) => rsa_mag = get_rsa_bits(keydata).ok(),
		_ => (),
	}
	Ok(get_rsa_dh_parameters(rsa_mag))
}

pub fn get_dh_params(cert: &[u8]) -> Vec<u8> { _get_dh_params(cert).unwrap_or(Vec::new()) }

#[derive(Clone)]
pub struct VectorIterator<T:Sized+Clone>(Vec<T>, usize);

impl<T:Sized+Clone> Iterator for VectorIterator<T>
{
	type Item = T;
	fn next(&mut self) -> Option<T>
	{
		if self.1 < self.0.len() {
			let ret = Some(((self.0)[self.1]).to_owned());
			self.1 += 1;
			ret
		} else {
			None
		}
	}
}

pub struct ParseArgs(Vec<OsString>, String);

impl ParseArgs
{
	pub fn new(dfltname: &str) -> ParseArgs { ParseArgs(args_os().collect(), dfltname.to_owned()) }
	pub fn get_progname(&self) -> String
	{
		if (self.0).len() > 0 { (self.0)[0].to_string_lossy().into_owned() } else { (self.1).clone() }
	}
	pub fn is_option(&self, pfx: &str) -> Result<bool, String>
	{
		let mut ans = false;
		for i in (self.0).iter().skip(1) {
			let j = i.to_string_lossy();
			let j = j.deref();
			if j == "--" { break; }		//End of arguments.
			if j == pfx {
				fail_if!(ans, format!("Option '{pfx}' can only be specified once"));
				ans = true;
			}
		}
		Ok(ans)
	}
	pub fn getopt_single_str(&self, pfx: &str) -> Result<Option<String>, String>
	{
		let mut x = self.getopt_multi_str(pfx);
		let y = match x.next() { Some(x) => x, None => return Ok(None) };
		fail_if!(x.next().is_some(), format!("Option '{pfx}' can only be specified once"));
		Ok(Some(y))
	}
	pub fn getopt_single_path(&self, pfx: &str) -> Result<Option<PathBuf>, String>
	{
		let mut x = self.getopt_multi_path(pfx);
		let y = match x.next() { Some(x) => x, None => return Ok(None) };
		fail_if!(x.next().is_some(), format!("Option '{pfx}' can only be specified once"));
		Ok(Some(y))
	}
	pub fn getopt_multi_str(&self, pfx: &str) -> VectorIterator<String>
	{
		let mut ans = Vec::new();
		for i in (self.0).iter().skip(1) {
			let j = i.to_string_lossy();
			let j = j.deref();
			if j == "--" { break; }		//End of arguments.
			if let Some(j) = j.strip_prefix(pfx) { ans.push(j.to_owned()); }
		}
		VectorIterator(ans, 0)
	}
	pub fn getopt_multi_path(&self, pfx: &str) -> VectorIterator<PathBuf>
	{
		let mut ans = Vec::new();
		for i in (self.0).iter().skip(1) {
			let j = FilenameBuf::from_os_string(i.clone());
			if j == "--" { break; }		//End of arguments.
			//If not valid split point, definitely does not match.
			let (head, tail) = f_continue!(j.split_at(pfx.len()));
			if head == pfx { ans.push(PathBuf::from(tail.to_owned().into_os_string())); }
		}
		VectorIterator(ans, 0)
	}
	pub fn get_files(&self) -> VectorIterator<PathBuf>
	{
		let mut ans = Vec::new();
		let mut any = false;
		for i in (self.0).iter().skip(1) {
			let j = i.to_string_lossy();
			let j = j.deref();
			if j == "--" { any = true; continue; }
			if !j.starts_with("-") || any { ans.push(From::from(i.to_owned())); }
		}
		VectorIterator(ans, 0)
	}
	pub fn get_all(&self) -> VectorIterator<AllArgsString>
	{
		let mut ans = Vec::new();
		let mut any = false;
		for i in (self.0).iter().skip(1) {
			let j = i.to_string_lossy();
			let j = j.deref();
			if j == "--" { any = true; continue; }
			if any || !j.starts_with("-") {
				ans.push(AllArgsString::NotOption(i.to_owned()));
			} else {
				ans.push(AllArgsString::MaybeOption(i.to_owned()));
			}
		}
		VectorIterator(ans, 0)
	}
}

#[derive(Clone)]
pub enum AllArgsString
{
	MaybeOption(OsString),
	NotOption(OsString)
}

#[cfg(test)]
fn test_bindecode_with(data: &[u8])
{
	let data2 = bintextcode_enc(data);
	let data3 = bintextcode_dec(data2.as_inner());
	assert_eq!(&data3[..], data);
}

#[test]
fn test_bindecode()
{
	let randomat = b"\x46\x3f\x94\xad\xb6\xa4\x02\x90\xcf\x26\x79\xfc\x32\x63\xc0\xbc\
		\xf1\x67\x0e\xe8\x03\xe7\xe2\x92\xf6\xf9\x1b\x91\x63\xf0\x1c\xf2";
	for i in 0..randomat.len()+1 {
		test_bindecode_with(&randomat[..i]);
	}
	test_bindecode_with(&[255;128][..]);
}
