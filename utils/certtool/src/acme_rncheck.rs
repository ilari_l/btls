use crate::acme_service::AcmeService;
use crate::aia::aia_fetch_ocsp_parsed;
use crate::aia::AiaOcspError;
use crate::aia::ari_get_url;
use crate::debug::Debugging;
use crate::strings::AsciiString;
use btls_aux_fail::f_return;
use btls_aux_fail::fail;
use btls_aux_fail::ResultExt;
use btls_aux_filename::Filename;
use btls_aux_hash::sha256;
use btls_aux_json2::JSON;
use btls_aux_memory::Attachment;
use btls_aux_memory::split_attach_first;
use btls_aux_time::DecodeTsForUser2;
use btls_aux_time::number_of_day;
use btls_aux_time::Timestamp;
use btls_aux_x509certparse::ParsedCertificate2;
use std::cmp::min;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::str::FromStr;

pub trait CertificateStore
{
	//The certificate is in DER format.
	fn get_certificate(&self, filename: &Filename) -> Result<Option<Vec<u8>>, String>;
	//The issuer is in DER format.
	fn get_issuer_cert(&self, filename: &Filename) -> Result<Option<Vec<u8>>, String>;
	//Check if certificate is assumed revoked.
	fn assume_revoked(&self, filename: &Filename, eecert: &ParsedCertificate2) -> Result<bool, String>;
	//Check if CSR has changed.
	fn csr_changed(&self, filename: &Filename, eecert: &ParsedCertificate2) ->
		Result<Option<&'static str>, String>;
	//Update OCSP.
	fn update_ocsp(&mut self, filename: &Filename, ocsp: Vec<u8>, debug: &Debugging);
}

#[derive(Copy,Clone)]
#[non_exhaustive]
pub enum RenewCause
{
	Forced,
	Csr(&'static str),
	NoCertificate,
	Expired,
	Revoked,
	AssumeRevoked,
	OverTime(u64),
	Ari,
}

impl Display for RenewCause
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match *self {
			RenewCause::Forced => f.write_str("Renewal forced"),
			RenewCause::Csr(msg) => write!(f, "CSR: {msg}"),
			RenewCause::NoCertificate => f.write_str("no certificate found"),
			RenewCause::Expired => f.write_str("certificate has EXPIRED"),
			RenewCause::Revoked => f.write_str("certificate has been REVOKED"),
			RenewCause::AssumeRevoked => f.write_str("assuming certificate revoked"),
			RenewCause::Ari => f.write_str("CA requested renew via ARI"),
			RenewCause::OverTime(t) if t < 600 => write!(f, "over threshold by {t}s"),
			RenewCause::OverTime(t) if t < 36000 => write!(f, "over threshold by {t}min", t=t/60),
			RenewCause::OverTime(t) if t < 345600 => write!(f, "over threshold by {t}h", t=t/3600),
			RenewCause::OverTime(t) => write!(f, "over threshold by {t}d", t=t/86400),
		}
	}
}

#[derive(Copy,Clone)]
pub enum ScheduleRenewal
{
	Now(RenewCause),
	Later(Timestamp),
}

pub fn get_renew_when(store: &mut impl CertificateStore, basename: &Filename, force: bool,
	acme: &mut Option<&mut AcmeService>, debug: &Debugging) -> Result<ScheduleRenewal, String>
{
	if force { return Ok(ScheduleRenewal::Now(RenewCause::Forced)); }
	let mut has_ari = None;
	let now = Timestamp::now();
	//Start by grabbing the certificate. However, before parsing it, do CSR check.
	let eecert = match store.get_certificate(basename) {
		//There is no certificate.
		Ok(None) => return Ok(ScheduleRenewal::Now(RenewCause::NoCertificate)),
		Ok(Some(t)) => t,
		Err(err) => fail!(format!("Failed to read certificate '{basename}': {err}"))
	};
	let eecert = ParsedCertificate2::from(&eecert).map_err(|err|{
		format!("Failed to parse certificate '{basename}': {err}")
	})?;
	if now > eecert.not_after {
		warning!(debug, "Certificate {basename} has expired, renew NOW");
		return Ok(ScheduleRenewal::Now(RenewCause::Expired));
	}
	//Assume revoked.
	match store.assume_revoked(basename, &eecert) {
		Ok(true) => return Ok(ScheduleRenewal::Now(RenewCause::AssumeRevoked)),
		Ok(false) => (),
		Err(err) => fail!(format!("Failed to check for assumed revocation for '{basename}': {err}")),
	}
	//CSR checks.
	match store.csr_changed(basename, &eecert) {
		Ok(Some(msg)) => return Ok(ScheduleRenewal::Now(RenewCause::Csr(msg))),
		Ok(None) => (),
		Err(err) => fail!(format!("Failed to check for CSR change for '{basename}': {err}")),
	}
	//Optional issuer certificate for ARI and OCSP.
	let isscert = match store.get_issuer_cert(basename) {
		Ok(t) => t,
		Err(err) => {
			warning!(debug, "Failed to read issuer certificate for '{basename}': {err}");
			None
		}
	};
	let isscert = match isscert.as_ref() {
		Some(cert) => match ParsedCertificate2::from(cert) {
			Ok(t) => Some(t),
			Err(err) => {
				warning!(debug, "Failed to parse issuer certificate for '{basename}': {err}");
				None
			}
		}
		None => None
	};
	//ARI checks need acme CA and issuer.
	if let (Some(ref mut acme), Some(issuer)) = (acme, isscert.as_ref()) {
		match ari_need_renew(acme, basename, &eecert, issuer, debug, &mut has_ari) {
			Ok(true) => return Ok(ScheduleRenewal::Now(RenewCause::Ari)),
			Ok(false) => (),
			Err(err) => warning!(debug, "Error checking ARI for '{basename}': {err}"),
		}
	}
	//OCSP checks need issuer.
	if let Some(issuer) = isscert.as_ref() {
		match aia_fetch_ocsp_parsed(&eecert, issuer.pubkey, Timestamp::now(), debug) {
			Ok(ocsp) => store.update_ocsp(basename, ocsp, debug),
			Err(AiaOcspError::NoOcsp) =>
				info!(debug, "Certificate '{basename}' has no OCSP responder URL"),
			Err(AiaOcspError::Revoked) => {
				warning!(debug, "Certificate '{basename}' is revoked, renew NOW");
				return Ok(ScheduleRenewal::Now(RenewCause::Revoked));
			},
			Err(AiaOcspError::Other(err)) =>
				warning!(debug, "Error checking OCSP for '{basename}': {err}"),
		}
	}

	let lifetime = eecert.not_before.delta(eecert.not_after) as i64;
	let tdiv = if has_ari.is_some() { 6 } else { 3 };
	let threshold = min(lifetime / tdiv, 2592000);
	let mut renew_on = eecert.not_after + -threshold;	//Timestamp has +, but not -.
	//If has ARI info with gives earlier renew, override from ARI.
	if let Some(ari) = has_ari { renew_on = renew_on.first(ari);  }
	let t = if now < renew_on {
		ScheduleRenewal::Later(renew_on)
	} else {
		ScheduleRenewal::Now(RenewCause::OverTime(renew_on.delta(now)))
	};
	Ok(t)
}

fn pushlen(to: &mut Vec<u8>, what: &[u8])
{
	to.extend_from_slice(&(what.len() as u64).to_le_bytes());
	to.extend_from_slice(what);
}

fn ari_need_renew(service: &mut AcmeService, basename: &Filename, psub: &ParsedCertificate2,
	piss: &ParsedCertificate2, debug: &Debugging, has_ari: &mut Option<Timestamp>) -> Result<bool, String>
{
	//If no ARI available, not renew.
	let directory = service.directory();
	let base_url = f_return!(directory.ari_base.as_deref(), Ok(false));
	let url: AsciiString = ari_get_url(base_url, &psub, piss.pubkey)?;
	let ari = service.get_real(&url, debug).map_err(|err|{
		format!("Unable to fetch ARI data: {err}")
	})?;
	let ari = String::from_utf8(ari).set_err("ARI data is not UTF-8")?;
	let ari = JSON::parse(&ari).set_err("ARI data is not valid JSON")?;
	let ari = ari.as_object().ok_or("ARI data is not object")?;
	let expain = ari.get("explanationURL").and_then(|url|url.as_string());
	let ri = ari.get("suggestedWindow").ok_or("No suggestedWindow")?.as_object().
		ok_or("suggestedWindow is not object")?;
	let start = ri.get("start").ok_or("No start")?.as_string().ok_or("start is not string")?;
	let end = ri.get("end").ok_or("No end")?.as_string().ok_or("end is not string")?;
	let start = parse_ari_time(start).ok_or_else(||format!("Failed to parse start time '{start}'"))?;
	let end = parse_ari_time(end).ok_or_else(||format!("Failed to parse end time '{end}'"))?;
	let now = Timestamp::now();
	if now > Timestamp::new(end) {
		notice!(debug, "ARI: Renew {basename} NOW");
		if let Some(expain) = expain { notice!(debug, "See: {expain}"); }
		return Ok(true);
	}
	let threshold = if start <= end {
		let mut random_seed = Vec::new();
		pushlen(&mut random_seed, psub.issuer.as_raw_issuer());
		pushlen(&mut random_seed, psub.serial_number);
		random_seed.extend_from_slice(&start.to_le_bytes());
		random_seed.extend_from_slice(&end.to_le_bytes());
		let random_seed = sha256(&random_seed);
		let length = min(end.saturating_sub(start).saturating_add(1), 1<<50);
		//length is always > 0.
		let mut acc = 0;
		for &b in random_seed.iter() { acc = (256 * acc + b as i64) % length; }
		Timestamp::new(start + acc)
	} else {
		Timestamp::new(end)
	};
	//This certificate has ARI info, save the renew threshold.
	*has_ari = Some(threshold);
	if expain.is_some() || Timestamp::now() >= threshold {
		notice!(debug, "ARI: {basename} Threshold {threshold}", threshold=DecodeTsForUser2(threshold));
		if let Some(expain) = expain { notice!(debug, "See: {expain}"); }
	}
	Ok(Timestamp::now() >= threshold)
}

fn strip_leading_zeros(mut x: &str) -> &str
{
	while let Some(y) = x.strip_prefix("0") { x = y; }
	if x == "" { x = "0"; }
	x
}

fn parse_ari_time(time: &str) -> Option<i64>
{
	let (date, time) = split_attach_first(time, "T", Attachment::Center).ok()?;
	let mut date = date.split("-");
	let year = i64::from_str(strip_leading_zeros(date.next()?)).ok()?;
	let month = u8::from_str(strip_leading_zeros(date.next()?)).ok()?;
	let day = u8::from_str(strip_leading_zeros(date.next()?)).ok()?;
	let date = number_of_day(year, month, day).ok()?;
	let time = time.get(..8)?;	//There is various crap after 8 time bytes.
	let mut time = time.split(":");
	let hour = u8::from_str(strip_leading_zeros(time.next()?)).ok()? as i64;
	let minute = u8::from_str(strip_leading_zeros(time.next()?)).ok()? as i64;
	let second = u8::from_str(strip_leading_zeros(time.next()?)).ok()? as i64;
	Some(86400 * date + 3600 * hour + 60 * minute + second)
}
