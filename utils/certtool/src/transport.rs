#[cfg(test)] use crate::An;
use crate::deadline::Deadline;
use crate::debug::DEBUG_TCP;
use crate::debug::Debugging;
use crate::dns::domain_address_records;
use crate::dns::domain_set_preferred_address;
use crate::strings::AsciiStr;
use crate::strings::AsciiString;
use crate::strings::HostStr;
use crate::strings::HostString;
use crate::tls::ErrorClass;
use crate::tls::TlsFault;
use btls_aux_dnsmsg::consts::DNSCLASS_IN;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_unix::Address;
use btls_aux_unix::AddressIpv4;
use btls_aux_unix::AddressIpv6;
use btls_aux_unix::FileDescriptor;
use btls_aux_unix::PollFd;
use btls_aux_unix::PollFlags;
use btls_aux_unix::ShutdownFlags;
use btls_aux_unix::SocketFlags;
use btls_aux_unix::SocketType;
use std::fmt::Display;
use std::fmt::Formatter;
use std::fmt::Result as FmtResult;
use std::fs::File;
use std::io::Error as IoError;
use std::io::ErrorKind as IoErrorKind;
use std::io::Read as IoRead;
use std::marker::PhantomData;
use std::net::IpAddr;
use std::net::Shutdown;
use std::net::SocketAddr;
use std::net::SocketAddrV4;
use std::net::SocketAddrV6;
use std::str::from_utf8;
use std::str::FromStr;
use std::time::Duration;

#[derive(Clone)]
pub struct Target
{
	target: HostString,
	service: AsciiString,
	addr: SocketAddr,
}

impl Target
{
	pub fn get_addr(&self) -> SocketAddr { self.addr.clone() }
	pub fn get_target<'a>(&'a self) -> (&'a HostStr, &'a AsciiStr) { (&self.target, &self.service) }
	pub fn make(target: &HostStr, service: &AsciiStr, addr: &SocketAddr) -> Target
	{
		Target{target: target.to_owned(), service: service.to_owned(), addr: addr.clone()}
	}
}

impl Display for Target
{
	fn fmt(&self, f: &mut Formatter) -> FmtResult
	{
		let &Target{ref target, ref service, addr} = self;
		write!(f, "{target}({service})[{addr}]")
	}
}

pub trait Transport: Sized
{
	fn establish_connection(target: &HostStr, service: &AsciiStr, target_address: Option<SocketAddr>,
		debug: &Debugging, deadline: Deadline) -> Result<(Self, Target), TlsFault>;
	fn peer_addr(&self) -> SocketAddr;
	fn shutdown(&mut self, how: Shutdown) -> Result<(), TlsFault>;
	fn read(&mut self, buf: &mut [u8], deadline: Deadline) -> Result<usize, TlsFault>;
	fn write(&mut self, buf: &[u8], deadline: Deadline) -> Result<usize, TlsFault>;
	fn write_all(&mut self, buf: &[u8], deadline: Deadline) -> Result<(), TlsFault>
	{
		let mut i = 0;
		while i < buf.len() {
			let buf = &buf[i..];
			let amt = self.write(buf, deadline)?;
			i += amt;
		}
		Ok(())
	}
}

const TCP_OPERATION_TIMEOUT: Duration = Duration::from_millis(15000);
const TCP_OPERATION_TIMEOUT_S: Duration = Duration::from_millis(5000);

pub fn is_fatal(err: &IoError) -> bool
{
	!(err.kind() == IoErrorKind::Interrupted || err.kind() == IoErrorKind::WouldBlock)
}

fn get_timeout_wait(deadline: Deadline, looped_once: &mut bool) -> Result<u64, ()>
{
	let wait_duration = deadline.nanos_to() / 1000000;
	fail_if!(wait_duration == 0 && *looped_once, ());		//Timed out!
	*looped_once = true;
	Ok(wait_duration)
}

fn connect_timed_out() -> IoError { IoError::new(IoErrorKind::TimedOut, "TCP socket connect timed out") }

fn establish_tcp_conn(target: &Target, deadline: Deadline) -> Result<FileDescriptor, IoError>
{
	let addr = match &target.addr {
		&SocketAddr::V4(ref a) => Address::Ipv4(AddressIpv4 {
			addr: a.ip().octets(),
			port: a.port()
		}),
		&SocketAddr::V6(ref a) => Address::Ipv6(AddressIpv6 {
			addr: a.ip().octets(),
			port: a.port(),
			scope: a.scope_id(),
			flow: a.flowinfo(),
		}),
	};
	let deadline = deadline.derive(TCP_OPERATION_TIMEOUT);
	let mut looped_once = false;

	//Create the socket.
	let fd;
	loop {
		get_timeout_wait(deadline, &mut looped_once).map_err(|_|connect_timed_out())?;
		let flags = SocketFlags::NONBLOCK|SocketFlags::CLOEXEC;
		match FileDescriptor::socket2(addr.family(), SocketType::STREAM, Default::default(), flags) {
			Ok(x) => { fd = x; break },
			Err(err) => {
				let err = IoError::from_raw_os_error(err.to_inner());
				fail_if!(is_fatal(&err), err);
			}
		};
	}
	//Set nonblocking mode. This operation can not fail with soft error.
	fd.set_nonblock(true).map_err(|err|IoError::from_raw_os_error(err.to_inner()))?;
	//Connect the socket.
	looped_once = false;
	loop {
		get_timeout_wait(deadline, &mut looped_once).map_err(|_|connect_timed_out())?;
		match fd.connect(&addr) {
			Ok(true) => return Ok(fd),	//Connected immediately.
			Ok(false) => break,		//Wait.
			Err(err) => {
				let err = IoError::from_raw_os_error(err.to_inner());
				fail_if!(is_fatal(&err), err);
			}
		}
	}

	//Wait for status & get status.
	looped_once = false;
	loop {
		let wait_duration = get_timeout_wait(deadline, &mut looped_once).map_err(|_|connect_timed_out())?;
		let mut p = [PollFd {fd: fd.as_fdb(), events: PollFlags::OUT, revents: Default::default()}];
		let r = unsafe{PollFd::poll(&mut p, wait_duration as i32)};
		//If no state change, continue waiting.
		if r.is_err() || p[0].revents.is_none() { continue; }

		match fd.so_error() {
			//On success, yield the socket.
			Ok(Ok(_)) => return Ok(fd),
			//Connect errors are always fatal.
			Ok(Err(err)) => fail!(IoError::from_raw_os_error(err.to_inner())),
			//Fetch errors are fatal if permanent.
			Err(err) => {
				let err = IoError::from_raw_os_error(err.to_inner());
				fail_if!(is_fatal(&err), err);
			}
		}
	}
}

fn oisspace(x: u8) -> bool { x == 9 || x == 10 || x == 13 || x == 32 }

pub fn port_for_service(service: &AsciiStr) -> Result<u16, String>
{
	if let Ok(port) = u16::from_str(service.as_inner()) { return Ok(port); }
	if service == "http" { return Ok(80); }
	if service == "https" { return Ok(443); }
	if service == "domain" { return Ok(53); }
	if service == "domains" { return Ok(853); }
	//Read /etc/services for more services.
	let unknown = Err(format!("Unknown service '{service}'"));
	let mut fp = match File::open("/etc/services") { Ok(x) => x, Err(_) => return unknown };
	let mut content = Vec::new();
	match fp.read_to_end(&mut content) { Ok(x) => x, Err(_) => return unknown };

	let service = service.as_bytes();
	for l in content.split(|c|*c == b'\n') {
		//Truncate comments and spaces before.
		let mut i = l.split(|c|*c == b'#');
		let mut l = match i.next() { Some(x) => x, None => continue };
		while l.len() > 0 && oisspace(l[l.len() - 1]) { l = &l[..l.len()-1]; }
		if l.len() == 0 { continue; }
		//Next, split the remainder on whitespace.
		let mut port = None;
		let mut found = false;
		let mut id = 0;
		for j in l.split(|c|(*c == 9||*c == 32)) {
			if id == 1 {	//Port number.
				if let Some(j) = j.strip_suffix(b"/tcp") {	//Only consider TCP ports.
					port = from_utf8(j).ok().and_then(|x|u16::from_str(x).ok());
				}
			} else {
				found |= service == j;		//Found the service.
			}
			if j.len() > 0 { id += 1; }
		}
		//If port was found, return it.
		if let (true, Some(port)) = (found, port) { return Ok(port); }
	}
	unknown
}

#[test]
fn test_port_for_service()
{
	assert_eq!(port_for_service(An("bgp")), Ok(179));
	assert_eq!(port_for_service(An("kerberos-master")), Ok(751));
	assert_eq!(port_for_service(An("kdc")), Ok(750));
	assert!(port_for_service(An("foobarbaz")).is_err());
}

struct NotSend(PhantomData<*mut u8>);

impl NotSend
{
	fn new() -> NotSend { NotSend(PhantomData) }
}

//TCP sockets don't take use from many threads kindly.
pub struct TcpTransport(FileDescriptor, Target, NotSend);

fn __get_address_list(target: &HostStr, service: &AsciiStr, debug: &Debugging, deadline: Deadline) ->
	Result<Vec<SocketAddr>, TlsFault>
{
	debug!(debug, DEBUG_TCP, "Looking up address for {target}:{service}...");
	let mut addrs: Vec<SocketAddr> = Vec::new();
	let addrlist = domain_address_records(target.as_bytes(), DNSCLASS_IN, false, debug, deadline).map_err(|err|{
		TlsFault::new_noip(target, service, ErrorClass::NameLookup, err)
	})?;
	let port = port_for_service(service).map_err(|err|{
		TlsFault::new_noip(target, service, ErrorClass::NameLookup, err)
	})?;
	for addr in addrlist.iter() {
		let addr = match addr {
			&IpAddr::V4(ref x) => SocketAddr::V4(SocketAddrV4::new(x.clone(), port)),
			&IpAddr::V6(ref x) => SocketAddr::V6(SocketAddrV6::new(x.clone(), port, 0, 0)),
		};
		debug!(debug, DEBUG_TCP, "Found address {addr} for {target}:{service}.");
		addrs.push(addr);
	}
	Ok(addrs)
}

impl Transport for TcpTransport
{
	fn establish_connection(target: &HostStr, service: &AsciiStr, target_address: Option<SocketAddr>,
		debug: &Debugging, deadline: Deadline) -> Result<(TcpTransport, Target), TlsFault>
	{
		let ipaddr_list = match target_address {
			Some(x) => vec!(x),
			None => __get_address_list(target, service, debug, deadline)?,
		};
		if ipaddr_list.len() == 0 {
			let msg = format!("No IP addresses available");
			fail!(TlsFault::new_noip(target, service, ErrorClass::Connect, msg));
		}
		for ipaddr in ipaddr_list.iter() {
			let addr = Target::make(target, service, ipaddr);
			debug!(debug, DEBUG_TCP, "Trying {addr}...");
			let odeadline = deadline;
			let deadline = deadline.derive(TCP_OPERATION_TIMEOUT_S);
			let fd = match establish_tcp_conn(&addr, deadline) {
				Ok(x) => {
					//Set the successful address as preferred.
					domain_set_preferred_address(target.as_bytes(), DNSCLASS_IN,
						Some(ipaddr.ip()), debug);
					x
				},
				Err(err) => {
					//If there is just one address (e.g., the address is forced), return the
					//error. Also return error for deadline exceeded.
					fail_if!(ipaddr_list.len() == 1 || odeadline.expired(),
						TlsFault::new(&addr, ErrorClass::Connect, err.to_string()));
					warning!(debug, "Failed to establish connection to {target}:{service} \
						[{ipaddr}]: {err}");
					continue;
				}
			};
			debug!(debug, DEBUG_TCP, "Connected to {addr}.");
			return Ok((TcpTransport(fd, addr.clone(), NotSend::new()), addr));
		}
		//Clear the preferred address since nothing worked.
		domain_set_preferred_address(target.as_bytes(), DNSCLASS_IN, None, debug);
		let errmsg = format!("All targets failed");
		Err(TlsFault::new_noip(target, service, ErrorClass::Connect, errmsg))
	}
	fn peer_addr(&self) -> SocketAddr { self.1.addr }
	fn shutdown(&mut self, how: Shutdown) -> Result<(), TlsFault>
	{
		let mode = match how {
			Shutdown::Read => ShutdownFlags::RD,
			Shutdown::Write => ShutdownFlags::WR,
			Shutdown::Both => ShutdownFlags::RDWR,
		};
		loop {
			if let Err(err) = self.0.shutdown(mode) {
				let err = IoError::from_raw_os_error(err.to_inner());
				if !is_fatal(&err) { continue; }
				let msg = err.to_string();
				fail!(TlsFault::new(&self.1, ErrorClass::TcpShutdown, msg));
			} else {
				return Ok(());
			}
		}
	}
	fn read(&mut self, buf: &mut [u8], deadline: Deadline) -> Result<usize, TlsFault>
	{
		let mut looped_once = false;
		loop {
			let wait_duration = get_timeout_wait(deadline, &mut looped_once).map_err(|_|{
				let msg = "Timed out".to_string();
				TlsFault::new(&self.1, ErrorClass::TcpRead, msg)
			})?;
			let mut p = [PollFd {
				fd: self.0.as_fdb(),
				events: PollFlags::IN|PollFlags::HUP,
				revents: Default::default()
			}];
			let r = unsafe{PollFd::poll(&mut p, wait_duration as i32)};
			//If no state change, continue waiting.
			if r.is_err() || p[0].revents.is_none() { continue; }
			match self.0.read(buf) {
				Ok(r) => return Ok(r),
				Err(err) => {
					let err = IoError::from_raw_os_error(err.to_inner());
					if !is_fatal(&err) { continue; }
					let msg = err.to_string();
					fail!(TlsFault::new(&self.1, ErrorClass::TcpRead, msg));
				}
			}
		}
	}
	fn write(&mut self, buf: &[u8], deadline: Deadline) -> Result<usize, TlsFault>
	{
		let mut looped_once = false;
		loop {
			let wait_duration = get_timeout_wait(deadline, &mut looped_once).map_err(|_|{
				let msg = "Timed out".to_string();
				TlsFault::new(&self.1, ErrorClass::TcpWrite, msg)
			})?;
			let mut p = [PollFd {
				fd: self.0.as_fdb(),
				events: PollFlags::OUT,
				revents: Default::default()
			}];
			let r = unsafe{PollFd::poll(&mut p, wait_duration as i32)};
			//If no state change, continue waiting.
			if r.is_err() || p[0].revents.is_none() { continue; }
			match self.0.write(buf) {
				Ok(r) => return Ok(r),
				Err(err) => {
					let err = IoError::from_raw_os_error(err.to_inner());
					if !is_fatal(&err) { continue; }
					let msg = err.to_string();
					fail!(TlsFault::new(&self.1, ErrorClass::TcpWrite, msg));
				}
			}
		}
	}
}
