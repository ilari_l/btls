use crate::acme_daemon::CertDirTag;
use crate::acme_daemon::ReqDirTag;
use crate::acme_daemon::StateDirTag;
use crate::acme_install::try_write_directory;
use crate::acme_legacy::legacy_global_config;
use crate::acme_legacy::legacy_per_cert_config;
use crate::acme_newconfig::parse_new_config;
use crate::acme_order::OrderConfiguration;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::fail_if_none;
use btls_aux_filename::FilenameMatch;
use btls_aux_hash::sha256;
use btls_aux_signatures::base64url;
use btls_aux_unix::Gid;
use btls_aux_unix::Uid;
use btls_certtool::exiterror;
use btls_certtool::acme_service::AccountIdDatabase;
use btls_certtool::acme_service::AcmeServiceRegistered;
use btls_certtool::aia::IntermediatePile;
use btls_certtool::aia::SpecialIntermediatePile;
use btls_certtool::authmod::DlfcnModule;
use btls_certtool::debug::Debugging;
use btls_certtool::debug::get_debug_maybe;
use btls_certtool::debug::StderrDebug;
use btls_certtool::dns::AddressReplaceList;
use btls_certtool::privatekey::Privatekey;
use btls_certtool::strings::AsciiStr;
use btls_certtool::strings::AsciiString;
use btls_certtool::strings::Base64UrlStr;
use btls_certtool::strings::SafeOctetsToText;
use btls_certtool::wcheck::Rootpile;
use std::cell::RefCell;
use std::collections::BTreeMap;
use std::collections::BTreeSet;
use std::collections::HashMap;
use std::env::set_current_dir;
use std::env::set_var;
use std::env::var;
use std::ffi::OsString;
use std::fmt::Debug;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::fs::File;
use std::io::Read as IoRead;
use std::io::Write as IoWrite;
use std::os::unix::fs::MetadataExt;
use std::path::Path;
use std::path::PathBuf;
use std::process::exit;
use std::rc::Rc;
use std::str::from_utf8;



pub struct AccountDatabase(pub PathBuf);

impl AccountDatabase
{
	fn filename(&self, url: &AsciiStr, thumbprint: &Base64UrlStr) -> PathBuf
	{
		self.0.join(&format!("{acct}.acct",
			acct=base64url(&sha256(format!("{url}#{thumbprint}").as_bytes()))))
	}
}

impl AccountIdDatabase for AccountDatabase
{
	fn lookup(&mut self, url: &AsciiStr, thumbprint: &Base64UrlStr) -> Option<AsciiString>
	{
		let filename = self.filename(url, thumbprint);
		let mut content = String::new();
		fail_if_none!(File::open(&filename).and_then(|mut fp|fp.read_to_string(&mut content)).is_err());
		AsciiString::from_string(content).ok()
	}
	fn store(&mut self, url: &AsciiStr, thumbprint: &Base64UrlStr, acctid: &AsciiStr)
	{
		let filename = self.filename(url, thumbprint);
		File::create(&filename).and_then(|mut fp|fp.write_all(acctid.as_bytes())).ok();
	}
}


pub fn check_is_directory(path: &Path) -> Result<(), String>
{
	let tgt = path.metadata().ok();
	if let Some(tgt) = tgt { fail_if!(!tgt.is_dir(), format!("{path} is not a directory", path=path.display())); }
	else { fail!(format!("{path} does not exist", path=path.display())); }
	Ok(())
}

pub fn check_directory_writable(path: &Path, thumbprint: &Base64UrlStr) -> Result<(), String>
{
	let tgt = path.metadata().ok();
	if let Some(tgt) = tgt { fail_if!(!tgt.is_dir(), format!("{path} is not a directory", path=path.display())); }
	try_write_directory(path, &thumbprint).map_err(|err|{
		format!("Directory {path} is not writable: {err}", path=path.display())
	})?;
	Ok(())
}

pub fn check_directory_reachable(path: &Path) -> Result<(), String>
{
	let mut path = path.canonicalize().map_err(|err|{
		format!("Failed to canonicalize {path}: {err}", path=path.display())
	})?;
	let tgt = path.metadata().ok();
	if let Some(tgt) = tgt {
		fail_if!(!tgt.is_dir(), format!("{path} is not a directory", path=path.display()));
	} else {
		fail!(format!("{path} does not exist", path=path.display()));
	}
	loop {
		let meta = path.metadata().map_err(|err|{
			format!("Failed to stat {path}: {err}", path=path.display())
		})?;
		fail_if!(meta.mode() & 365 != 365,
			format!("{path} has insufficient permissions (need a+rx)", path=path.display()));
		path = match path.parent() { Some(x) => x.to_owned(), None => return Ok(()) };
	}
}

pub fn webroot_compat_entry(auth_modules: &mut Vec<(String, DlfcnModule, String)>, legacydir: PathBuf) ->
	Result<(), String>
{
	//Add webroot module for path.
	let webroot = DlfcnModule::new("$builtin/webroot").map_err(|err|{
		format!("Failed to initialize webroot module: {err}'")
	})?;
	auth_modules.push((String::new(), webroot, legacydir.display().to_string()));
	Ok(())
}


#[derive(Clone,Debug)]
pub struct GlobalConfiguration
{
	pub debug: Debugging,
	pub replace: AddressReplaceList,
	pub auth_modules: Vec<(String, DlfcnModule, String)>,
	pub pile: IntermediatePile,
	pub spile: SpecialIntermediatePile,
	pub reqdir: ReqDirTag<PathBuf>,
	pub statedir: StateDirTag<PathBuf>,
	pub logsdir: PathBuf,
	pub post_renew: Vec<(PathBuf, Vec<OsString>)>,
	pub post_install: Vec<PathBuf>,
	pub initial_wait: u64,
	pub round_wait: u64,
	pub error_wait: u64,
	pub assume_revoked: BTreeSet<(Vec<u8>,Vec<u8>)>,
	pub ca_list: HashMap<String, (Rc<RefCell<AcmeServiceData>>, Rootpile)>,
	pub quiet: bool,
	pub daemon: bool,
}


#[derive(Clone,Debug)]
pub struct PerCertificateConfigurations
{
	pub dflt: PerCertificateConfiguration,
	pub special: BTreeMap<FilenameMatch, PerCertificateConfiguration>,
}

#[derive(Clone)]
pub struct PerCertificateConfiguration
{
	pub rootpile: Rootpile,
	pub key: Option<PathBuf>,
	pub certdir: CertDirTag<PathBuf>,
	pub ca: Rc<RefCell<AcmeServiceData>>,
}

impl Debug for PerCertificateConfiguration
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		f.write_str("PerCertificateConfiguration { rootpile: [")?;
		let mut first = true;
		for entry in self.rootpile.iter() {
			if !first { f.write_str(", ")?; }
			first = false;
			write!(f, "{entry:?}")?;
		}
		write!(f, "], key: {key:?}, certdir: {certdir:?}, ca: {ca:?} }}", key=self.key,
			certdir=self.certdir.deref_inner(), ca=self.ca)
	}
}


impl PerCertificateConfiguration
{
	pub fn to_order<'a>(&'a self, g: &'a GlobalConfiguration) -> OrderConfiguration<'a>
	{
		OrderConfiguration {
			debug: g.debug.clone(),
			replace: &g.replace,
			auth_modules: &g.auth_modules,
			pile: &g.pile,
			spile: &g.spile,
			rootpile: &self.rootpile,
			key: self.key.as_deref(),
			assume_revoked: &g.assume_revoked,
			certdir: self.certdir.as_deref(),
			statedir: g.statedir.as_deref(),
			reqdir: g.reqdir.as_deref(),
			quiet: g.quiet,
			daemon: g.daemon,
		}
	}
}

#[derive(Clone,Debug)]
pub struct AcmeServiceData
{
	pub name: String,
	pub url: AsciiString,
	pub test_url: Option<AsciiString>,
	pub key_contents: Vec<u8>,
	pub key: Option<Rc<Privatekey>>,
	pub load_key_err: Option<String>,
	pub key_path: PathBuf,
	pub trust_anchors: Rootpile,
}

impl AcmeServiceData
{
	pub fn dummy(name: &str) -> AcmeServiceData
	{
		AcmeServiceData {
			name: name.to_string(),
			url: AsciiString::new(),
			test_url: None,
			key_contents: Vec::new(),
			key: None,
			load_key_err: None,
			key_path: PathBuf::new(),
			trust_anchors: Rootpile::new(),
		}
	}
}

pub struct AcmeServiceDataCache
{
	pub cache: BTreeMap<usize, Rc<RefCell<AcmeServiceRegistered>>>,
}


const XID_MAX: u32 = 4294960000;

fn get_uids() -> (Uid, Uid)
{
	let ruid = Uid::get_real();
	let uid = Uid::get_effective();
	if ruid.to_inner() > XID_MAX || uid.to_inner() > XID_MAX { exiterror!(E "Bad UID from get(e)uid"); }
	(ruid, uid)
}

fn get_gids() -> (Gid, Gid)
{
	let rgid = Gid::get_real();
	let gid = Gid::get_effective();
	if rgid.to_inner() > XID_MAX || gid.to_inner() > XID_MAX { exiterror!(E "Bad UID from get(e)uid"); }
	(rgid, gid)
}

pub fn handle_setuid() -> Uid
{
	let (ruid, uid) = get_uids();
	let (rgid, gid) = get_gids();
	//Sanity checks.
	if ruid.is_super() { exiterror!(E "This program can not be run as root"); }
	if ruid != uid { exiterror!(E "This program can not be run as setuid"); }
	if rgid != gid { exiterror!(E "This program can not be run as setgid"); }
	uid
}

pub fn homedir_setup(uid: Uid)
{
	if let Ok(val) = var("HOME") {
		set_current_dir(&val).unwrap_or_else(|err|{
			exiterror!(E "Failed to chdir to '{val}': {err}")
		});
	} else {
		//Chdir to home directory from user database.
		let pw = Uid::get_real().record().unwrap_or_else(||{
			exiterror!(E "UID {uid} does not exist in user database.")
		});
		let hdir = {
			let bytes = pw.homedir();
			from_utf8(bytes).unwrap_or_else(|_|{
				exiterror!(E "Illegal user home directory: {bytes}", bytes=SafeOctetsToText(bytes))
			}).to_owned()
		};
		//Set HOME correctly.
		set_var("HOME", &hdir);
		set_current_dir(&hdir).unwrap_or_else(|err|{
			exiterror!(E "Failed to chdir to '{hdir}': {err}")
		});
	}
}

pub fn load_configuration(force_inner: bool) -> (GlobalConfiguration, PerCertificateConfigurations)
{
	//If keys are to be loaded, this is inner.
	let debug = get_debug_maybe(var("CERTTOOL_DEBUG").ok().as_deref(), StderrDebug::new_maybe_inner(force_inner));
	if let Some((a, b)) = parse_new_config(debug.clone()) { return (a, b); }
	let certscfg = legacy_per_cert_config(&debug);
	let thumbprint = Base64UrlStr::new_octets(b"test-thumbprint").unwrap();
	let mut globalcfg = legacy_global_config(&debug, &thumbprint);
	//Do not care about trust anchor list.
	globalcfg.ca_list.insert("default".to_string(), (certscfg.dflt.ca.clone(), Rootpile::new()));
	(globalcfg, certscfg)
}
