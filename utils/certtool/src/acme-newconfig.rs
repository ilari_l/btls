use crate::acme_common::AcmeServiceData;
use crate::acme_common::check_directory_reachable;
use crate::acme_common::check_directory_writable;
use crate::acme_common::check_is_directory;
use crate::acme_common::GlobalConfiguration;
use crate::acme_common::PerCertificateConfiguration;
use crate::acme_common::PerCertificateConfigurations;
use crate::acme_daemon::CertDirTag;
use crate::acme_daemon::ReqDirTag;
use crate::acme_daemon::StateDirTag;
use btls_aux_fail::f_continue;
use btls_aux_fail::f_return;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::fail_if_none;
use btls_aux_fail::ResultExt;
use btls_aux_filename::Filename;
use btls_aux_filename::FilenameBuf;
use btls_aux_filename::FilenameMatch;
use btls_aux_keypair_local::LocalKeyPair;
use btls_certtool::alert;
use btls_certtool::read_cert_file;
use btls_certtool::with_private_umask;
use btls_certtool::warning;
use btls_certtool::aia::IntermediatePile;
use btls_certtool::aia::SpecialIntermediatePile;
use btls_certtool::authmod::DlfcnModule;
use btls_certtool::debug::Debugging;
use btls_certtool::dns::AddressReplace;
use btls_certtool::dns::AddressReplaceList;
use btls_certtool::privatekey::generate_and_dump_key;
use btls_certtool::strings::AsciiStr;
use btls_certtool::strings::AsciiString;
use btls_certtool::strings::Base64UrlStr;
use btls_certtool::wcheck::Rootpile;
use std::cell::RefCell;
use std::collections::BTreeMap;
use std::collections::BTreeSet;
use std::collections::HashMap;
use std::ffi::OsString;
use std::fmt::Display;
use std::fs::create_dir_all;
use std::fs::File;
use std::io::ErrorKind as IoErrorKind;
use std::io::Read as IoRead;
use std::io::Write as IoWrite;
use std::mem::replace;
use std::ops::Deref;
use std::ops::Range;
use std::os::unix::fs::MetadataExt;
use std::path::Path;
use std::path::PathBuf;
use std::process::exit;
use std::rc::Rc;
use std::str::FromStr;


enum LineState
{
	INITIAL,	//Initial state, or whitespace.
	QUOTED,		//Quoted string.
	COMMENT,	//Comment.
	TOKEN,		//Token.
	AQUOTED,	//After quoted.
	EQUOTED,	//Escape quoted.
}

fn compact_line(l: &str) -> Result<Vec<String>, &'static str>
{
	use self::LineState::*;
	let mut word = String::new();
	let mut out = Vec::new();
	let mut status = LineState::INITIAL;
	for c in l.chars() {
		status = match (status, c) {
			(INITIAL, '\t'|' ') => INITIAL,			//Ignore whitspace.
			(INITIAL, '\"') => QUOTED,			//Start quoted string.
			(INITIAL, '#') => COMMENT,			//Start comment.
			(INITIAL, _) => {word.push(c);TOKEN},		//Start token.
			(QUOTED, '\"') => {
				out.push(replace(&mut word, String::new()));
				AQUOTED
			},
			(QUOTED, '\\') => EQUOTED,
			(QUOTED, _) => {word.push(c);QUOTED},		//Continue quoted.
			(COMMENT, _) => COMMENT,			//Comments continue.
			(TOKEN, '\t'|' ') => {
				out.push(replace(&mut word, String::new()));
				INITIAL
			},
			(TOKEN, '\"') => fail!("Token contains \""),
			(TOKEN, '#') => {
				out.push(replace(&mut word, String::new()));
				COMMENT
			},
			(TOKEN, _) => {word.push(c);TOKEN},		//Continue token.
			(AQUOTED, ' '|'\t') => INITIAL,			//Back to whitespace.
			(AQUOTED, '#') => COMMENT,			//Comment.
			(AQUOTED, _) => fail!("Expected whitespace or # after end of quoted string"),
			(EQUOTED, '\\') => {word.push('\\');QUOTED},	//Escape for \.
			(EQUOTED, '\"') => {word.push('\"');QUOTED},	//Escape for ".
			(EQUOTED, _) => fail!("Invalid escape character"),
		};
	}
	match status {
		QUOTED|EQUOTED => fail!("Missing closing \" in quoted string"),
		TOKEN => out.push(replace(&mut word, String::new())),
		_ => ()
	};
	Ok(out)
}

struct LineContents
{
	number: usize,
	content: Vec<String>
}

fn parse_file(f: &str, filename: impl Display, debug: &Debugging) -> Vec<LineContents>
{
	let mut r = Vec::new();
	let mut line = 1;
	for l in f.lines() {
		let l = compact_line(l).unwrap_or_else(|err|{
			warning!(debug, "Config: {filename} Line {line}: {err}");
			Vec::new()
		});
		if l.len() > 0 {
			r.push(LineContents{
				number: line,
				content: l,
			});
		}
		line += 1;
	}
	r
}

fn read_text_file<P:AsRef<Path>>(path: P) -> Result<String, String>
{
	let path = path.as_ref();
	let mut content = Vec::new();
	File::open(path).and_then(|mut fp|fp.read_to_end(&mut content)).map_err(|err|{
		format!("Failed to read {path}: {err}", path=path.display())
	})?;
	let content = String::from_utf8(content).map_err(|err|{
		format!("Failed to read {path}: Invalid UTF-8 at byte {idx}",
			path=path.display(), idx=err.utf8_error().valid_up_to())
	})?;
	Ok(content)
}

fn cast_create_dir(dir: Option<(usize, String)>, kind: &str) -> Result<PathBuf, String>
{
	let (line, dir) = dir.ok_or_else(||{
		format!("{kind} directory not specified")
	})?;
	let dir = PathBuf::from(dir);
	create_dir_all(&dir).map_err(|err|{
		format!("Line {line}: mkdir {dir}: {err}", dir=dir.display())
	})?;
	Ok(dir)
}

fn make_writable_directory(dir: String, thumbprint: &Base64UrlStr) -> Result<PathBuf, String>
{
	let dir = PathBuf::from(dir);
	create_dir_all(&dir).map_err(|err|{
		format!("mkdir {dir}: {err}", dir=dir.display())
	})?;
	check_directory_reachable(&dir)?;
	check_directory_writable(&dir, thumbprint)?;
	Ok(dir)
}

fn reject_more<'a, I:Iterator<Item=&'a String>>(mut itr: I) -> Result<(), String>
{
	fail_if!(itr.next().is_some(), format!("Junk after end of line"));
	Ok(())
}

#[derive(Clone,Debug,PartialEq,Eq)]
pub enum KeyType
{
	Ecdsa256,
	Ecdsa384,
	Ecdsa521,
	Ed25519,
	Ed448,
}

impl KeyType
{
	fn parse<'a, I:Iterator<Item=&'a String>>(mut itr: I) -> Result<KeyType, String>
	{
		match itr.next().ok_or("Expected key type")?.deref() {
			"ecdsa" => { reject_more(itr)?; Ok(KeyType::Ecdsa256) },
			"ecdsa256" => { reject_more(itr)?; Ok(KeyType::Ecdsa256) },
			"ecdsa384" => { reject_more(itr)?; Ok(KeyType::Ecdsa384) },
			"ecdsa521" => { reject_more(itr)?; Ok(KeyType::Ecdsa521) },
			"ed25519" => { reject_more(itr)?; Ok(KeyType::Ed25519) },
			"ed448" => { reject_more(itr)?; Ok(KeyType::Ed448) },
			ktype => fail!(format!("Unrecognized key type {ktype}"))
		}
	}
}

enum CaDirective
{
	Url(String),
	TestUrl(String),
	Key(String),
	KeyType(KeyType),
	TrustAnchor(String),
	CheckAnchor(String),
}

impl CaDirective
{
	fn parse<'a, I:Iterator<Item=&'a String>>(mut itr: I) -> Result<CaDirective, String>
	{
		match itr.next().ok_or("Expected ca sub-directive")?.deref() {
			"url" => {
				let a = itr.next().ok_or("Expected url")?.deref();
				reject_more(itr)?;
				Ok(CaDirective::Url(a.to_owned()))
			},
			"test-url" => {
				let a = itr.next().ok_or("Expected url")?.deref();
				reject_more(itr)?;
				Ok(CaDirective::TestUrl(a.to_owned()))
			},
			"key" => {
				let a = itr.next().ok_or("Expected key path")?.deref();
				reject_more(itr)?;
				Ok(CaDirective::Key(a.to_owned()))
			},
			"key-type" => {
				Ok(CaDirective::KeyType(KeyType::parse(itr)?))
			},
			"trust-anchor" => {
				let a = itr.next().ok_or("Expected file path")?.deref();
				reject_more(itr)?;
				Ok(CaDirective::TrustAnchor(a.to_owned()))
			},
			"check-anchor" => {
				let a = itr.next().ok_or("Expected file path")?.deref();
				reject_more(itr)?;
				Ok(CaDirective::CheckAnchor(a.to_owned()))
			},
			directive => fail!(format!("Unrecognized ca sub-directive {directive}"))
		}
	}
}

enum CertDirective
{
	Ca(String),
	Key(String),
	Directory(String),
}

impl CertDirective
{
	fn parse<'a, I:Iterator<Item=&'a String>>(mut itr: I) -> Result<CertDirective, String>
	{
		match itr.next().ok_or("Expected ca sub-directive")?.deref() {
			"ca" => {
				let a = itr.next().ok_or("Expected CA name")?.deref();
				reject_more(itr)?;
				Ok(CertDirective::Ca(a.to_owned()))
			},
			"key" => {
				let a = itr.next().ok_or("Expected file path")?.deref();
				reject_more(itr)?;
				Ok(CertDirective::Key(a.to_owned()))
			},
			"directory" => {
				let a = itr.next().ok_or("Expected directory path")?.deref();
				reject_more(itr)?;
				Ok(CertDirective::Directory(a.to_owned()))
			},
			directive => fail!(format!("Unrecognized certificate sub-directive {directive}"))
		}
	}
}

struct CaInfo
{
	url: Option<(usize, String)>,
	test_url: Option<(usize, String)>,
	key: Option<(usize, String)>,
	key_type: Option<(usize, KeyType)>,
	trust_anchors: Vec<(usize, String)>,
	check_anchors: Vec<(usize, String)>,
}

#[derive(Debug,Clone)]
struct CaInfo2
{
	url: AsciiString,
	test_url: Option<AsciiString>,
	key: Vec<u8>,
	keypath: PathBuf,
	trust_anchors: Rootpile,
	check_anchors: Rootpile,
}

impl CaInfo
{
	fn new() -> CaInfo
	{
		CaInfo {
			url: None,
			test_url: None,
			key: None,
			key_type: None,
			trust_anchors: Vec::new(),
			check_anchors: Vec::new(),
		}
	}
	fn use_directive(&mut self, line: usize, d: CaDirective) -> Result<(), String>
	{
		match d {
			CaDirective::Url(x) => {
				if let Some(&(line, ref x2)) = self.url.as_ref() { if &x != x2 {
					fail!(format!("Conflicting CA URL on line {line}"));
				}}
				self.url = Some((line, x));
			},
			CaDirective::TestUrl(x) => {
				if let Some(&(line, ref x2)) = self.test_url.as_ref() { if &x != x2 {
					fail!(format!("Conflicting CA test URL on line {line}"));
				}}
				self.test_url = Some((line, x));
			},
			CaDirective::Key(x) => {
				if let Some(&(line, ref x2)) = self.key.as_ref() { if &x != x2 {
					fail!(format!("Conflicting CA key on line {line}"));
				}}
				self.key = Some((line, x));
			},
			CaDirective::KeyType(x) => {
				if let Some(&(line, ref x2)) = self.key_type.as_ref() { if &x != x2 {
					fail!(format!("Conflicting CA key type on line {line}"));
				}}
				self.key_type = Some((line, x));
			},
			CaDirective::TrustAnchor(x) => self.trust_anchors.push((line, x)),
			CaDirective::CheckAnchor(x) => self.check_anchors.push((line, x)),
		}
		Ok(())
	}
	fn finish_ca(mut self, debug: &Debugging) -> Result<CaInfo2, String>
	{
		let url = {
			let (_, url) = self.url.ok_or("CA URL not specified")?;
			AsciiString::new_string(&url).map_err(|bad|{
				format!("Bad URL {bad}")
			})?
		};
		let test_url = match self.test_url {
			Some((_, test_url)) => {
				let url = AsciiString::new_string(&test_url).map_err(|bad|{
					format!("Bad test-URL {bad}")
				})?;
				Some(url)
			},
			None => None,
		};
		let mut trust_anchors = Rootpile::new();
		for (line, file) in self.trust_anchors.drain(..) {
			trust_anchors.add_file(Path::new(file.deref()), &debug).map_err(|err|{
				format!("Line {line}: {err}")
			})?;
		}
		fail_if!(trust_anchors.len() == 0, format!("No trust anchors specified"));

		let mut check_anchors = Rootpile::new();
		for (line, file) in self.check_anchors.drain(..) {
			check_anchors.add_file(Path::new(file.deref()), &debug).map_err(|err|{
				format!("Line {line}: {err}")
			})?;
		}

		let kf = PathBuf::from(self.key.ok_or("CA account key not specified")?.1);
		if let Err(err) = kf.metadata() { if err.kind() == IoErrorKind::NotFound {
			let keytype = self.key_type.unwrap_or((0, KeyType::Ecdsa256)).1;
			let key = create_key(keytype, debug).map_err(|err|{
				format!("Failed to create key: {err}")
			})?;
			with_private_umask(||{
				File::create(&kf).and_then(|mut fp|{
					fp.write_all(&key)
				}).map_err(|err|{
					format!("Failed to write {key}: {err}", key=kf.display())
				})
			})?;
		}}
		fail_if!(!kf.is_file(),
			format!("Key {key} does not exist or is not a regular file", key=kf.display()));
		let privkey =  {
			//Do not actually load the key, as it may be something that is not fork-safe.
			let mut contents = Vec::new();
			File::open(&kf).and_then(|mut fp|fp.read_to_end(&mut contents)).map_err(|err|{
				format!("Error loading {key}: {err}", key=kf.display())
			})?;
			contents
		};

		Ok(CaInfo2 {
			url: url,
			test_url: test_url,
			key: privkey,
			keypath: kf.to_owned(),
			trust_anchors: trust_anchors,
			check_anchors: check_anchors,
		})
	}
}

fn create_key(keytype: KeyType, debug: &Debugging) -> Result<Vec<u8>, String>
{
	let bits = match keytype {
		KeyType::Ecdsa256 => 256,
		KeyType::Ecdsa384 => 384,
		KeyType::Ecdsa521 => 521,
		KeyType::Ed25519 => 255,
		KeyType::Ed448 => 448,
	};
	generate_and_dump_key(bits, false, debug.clone())
}

struct ParsedFile
{
	ca_list: HashMap<String, CaInfo>,
	validation_dir: Option<(usize, String)>,
	requests_dir: Option<(usize, String)>,
	certificates_dir: Option<(usize, String)>,
	state_dir: Option<(usize, String)>,
	logs_dir: Option<(usize, String)>,
	default_ca: Option<(usize, String)>,
	blacklist_url: Vec<String>,
	sint_whitelist: HashMap<FilenameBuf, (usize, Range<u64>)>,
	replace: Vec<(usize, String)>,
	intermediate_file: Vec<(usize, String)>,
	disable_modules: bool,
	key_modules: Vec<(usize, String)>,
	post_renew: Vec<(usize, String, Vec<OsString>)>,
	post_install: Vec<(usize, String)>,
	authentication_modules: Vec<(usize, Option<String>, String, Vec<String>)>,
	certificate_ca: HashMap<String, (usize, String)>,
	certificate_key: HashMap<String, (usize, String)>,
	certificate_dir: HashMap<String, (usize, String)>,
	initial_wait: Option<(usize, u64)>,
	round_wait: Option<(usize, u64)>,
	error_wait: Option<(usize, u64)>,
	no_aia: bool,
	assume_revoked: BTreeSet<(Vec<u8>,Vec<u8>)>,
}

#[derive(Debug)]
struct ParsedFile2
{
	global_config: GlobalConfiguration,
	cert_config: PerCertificateConfigurations,
}

impl ParsedFile
{
	fn new() -> ParsedFile
	{
		ParsedFile {
			ca_list: HashMap::new(),
			validation_dir: None,
			requests_dir: None,
			certificates_dir: None,
			state_dir: None,
			logs_dir: None,
			default_ca: None,
			blacklist_url: Vec::new(),
			sint_whitelist: HashMap::new(),
			replace: Vec::new(),
			intermediate_file: Vec::new(),
			disable_modules: false,
			key_modules: Vec::new(),
			post_renew: Vec::new(),
			post_install: Vec::new(),
			authentication_modules: Vec::new(),
			certificate_ca: HashMap::new(),
			certificate_key: HashMap::new(),
			certificate_dir: HashMap::new(),
			initial_wait: None,
			round_wait: None,
			error_wait: None,
			no_aia: false,
			assume_revoked: BTreeSet::new(),
		}
	}
	fn parse_file<P:AsRef<Path>>(filename: P, debug: &Debugging) -> Result<ParsedFile2, String>
	{
		let filename = filename.as_ref();
		let content = read_text_file(filename)?;
		let content = parse_file(&content, filename.display(), &debug);
		let mut state = ParsedFile::new();
		for directive in content.iter() {
			let line = directive.number;
			ParsedDirective::parse_raw(directive).and_then(|directive|{
				state.use_directive(line, directive)
			}).unwrap_or_else(|err|{
				warning!(debug, "{filename} Line {line}: {err}", filename=filename.display())
			});
		}
		state.finish(debug).map_err(|err|{
			format!("{filename} {err}", filename=filename.display())
		})
	}
	fn use_directive(&mut self, line: usize, d: ParsedDirective) -> Result<(), String>
	{
		match d {
			ParsedDirective::DisableAia => self.no_aia = true,
			ParsedDirective::Ca(n, d) => {
				self.ca_list.entry(n).or_insert_with(||CaInfo::new()).use_directive(line, d)?;
			},
			ParsedDirective::ValidationDirectory(s) => {
				if let Some(&(line, ref s2)) = self.validation_dir.as_ref() { if &s != s2 {
					fail!(format!("Conflicting validation directory on line {line}"));
				}}
				self.validation_dir = Some((line, s));
			},
			ParsedDirective::RequestsDirectory(s) => {
				if let Some(&(line, ref s2)) = self.requests_dir.as_ref() { if &s != s2 {
					fail!(format!("Conflicting requests directory on line {line}"));
				}}
				self.requests_dir = Some((line, s));
			},
			ParsedDirective::CertificatesDirectory(s) => {
				if let Some(&(line, ref s2)) = self.certificates_dir.as_ref() { if &s != s2 {
					fail!(format!("Conflicting certificates directory on line {line}"));
				}}
				self.certificates_dir = Some((line, s));
			},
			ParsedDirective::StateDirectory(s) => {
				if let Some(&(line, ref s2)) = self.state_dir.as_ref() { if &s != s2 {
					fail!(format!("Conflicting state directory on line {line}"));
				}}
				self.state_dir = Some((line, s));
			},
			ParsedDirective::LogsDirectory(s) => {
				if let Some(&(line, ref s2)) = self.logs_dir.as_ref() { if &s != s2 {
					fail!(format!("Conflicting logs directory on line {line}"));
				}}
				self.logs_dir = Some((line, s));
			},
			ParsedDirective::BlacklistUrl(s) => self.blacklist_url.push(s),
			ParsedDirective::IntermediateFile(s) => self.intermediate_file.push((line, s)),
			ParsedDirective::Replace(s) => self.replace.push((line, s)),
			ParsedDirective::DisableModules => self.disable_modules = true,
			ParsedDirective::KeyModule(s) => self.key_modules.push((line, s)),
			ParsedDirective::AuthenticationModule(a, b) =>
				self.authentication_modules.push((line, None, a, b)),
			ParsedDirective::AuthenticationModuleDomain(a, b, c) =>
				self.authentication_modules.push((line, Some(a), b, c)),
			ParsedDirective::Certificate(n, CertDirective::Ca(d)) => {
				if let Some(&(line, ref d2)) = self.certificate_ca.get(&n) { if &d != d2 {
					fail!(format!("Conflicting certificate CA on line {line}"));
				}}
				self.certificate_ca.insert(n, (line, d));
			},
			ParsedDirective::Certificate(n, CertDirective::Key(d)) => {
				if let Some(&(line, ref d2)) = self.certificate_key.get(&n) { if &d != d2 {
					fail!(format!("Conflicting certificate key on line {line}"));
				}}
				self.certificate_key.insert(n, (line, d));
			},
			ParsedDirective::Certificate(n, CertDirective::Directory(d)) => {
				if let Some(&(line, ref d2)) = self.certificate_dir.get(&n) { if &d != d2 {
					fail!(format!("Conflicting certificate directory on line {line}"));
				}}
				self.certificate_dir.insert(n, (line, d));
			},
			ParsedDirective::PostRenew(a, b) => self.post_renew.push((line, a, b)),
			ParsedDirective::PostInstall(a) => self.post_install.push((line, a)),
			ParsedDirective::DefaultCa(s) => {
				if let Some(&(line, ref s2)) = self.default_ca.as_ref() { if &s != s2 {
					fail!(format!("Conflicting default CA on line {line}"));
				}}
				self.default_ca = Some((line, s));
			},
			ParsedDirective::InitialWait(w) => {
				if let Some(&(line, ref w2)) = self.initial_wait.as_ref() { if &w != w2 {
					fail!(format!("Conflicting initial wait on line {line}"));
				}}
				self.initial_wait = Some((line, w));
			},
			ParsedDirective::RoundWait(w) => {
				if let Some(&(line, ref w2)) = self.round_wait.as_ref() { if &w != w2 {
					fail!(format!("Conflicting round wait on line {line}"));
				}}
				self.round_wait = Some((line, w));
			},
			ParsedDirective::ErrorWait(w) => {
				if let Some(&(line, ref w2)) = self.error_wait.as_ref() { if &w != w2 {
					fail!(format!("Conflicting error wait on line {line}"));
				}}
				self.error_wait = Some((line, w));
			},
			ParsedDirective::SIntWhitelist(n, d) => {
				if let Some(&(line, ref d2)) = self.sint_whitelist.get(&n) { if &d != d2 {
					fail!(format!("Conflicting issuer whitelist on line {line}"));
				}}
				self.sint_whitelist.insert(n, (line, d));
			},
			ParsedDirective::AssumeRevoked(issid, serial) => {
				self.assume_revoked.insert((issid, serial));
			},
		}
		Ok(())
	}
	fn finish(mut self, debug: &Debugging) -> Result<ParsedFile2, String>
	{
		let mut ca_list = HashMap::new();
		let mut authentication_modules = Vec::new();
		let mut replace = AddressReplaceList::new();
		let mut post_renew = Vec::new();
		let mut post_install = Vec::new();
		let initial_wait = self.initial_wait.map(|(_, x)|x).unwrap_or(300);
		let round_wait = self.round_wait.map(|(_, x)|x).unwrap_or(43200);
		let error_wait = self.round_wait.map(|(_, x)|x).unwrap_or(3600);
		let assume_revoked = self.assume_revoked;
		for (k, v) in self.ca_list.drain() {
			let v = f_continue!(v.finish_ca(debug).map_err(|err|{
				warning!(debug, "Config: CA {k}: {err}")
			}));
			let vc = v.check_anchors;
			let v = Rc::new(RefCell::new(AcmeServiceData {
				name: k.clone(),
				url: v.url,
				test_url: v.test_url,
				key_contents: v.key,
				key: None,
				load_key_err: None,
				key_path: v.keypath,
				trust_anchors: v.trust_anchors,
			}));
			ca_list.insert(k.clone(), (v, vc));
		}
		for (line, value) in self.replace.drain(..) {
			let entry = f_continue!(AddressReplace::new_from_line(&value).map_err(|err|{
				warning!(debug, "Config: Line {line}: addr-replace: {err}")
			}));
			replace.add(entry);
		}
		for (line, file) in self.key_modules.drain(..) {
			LocalKeyPair::load_module_trusted(&file).unwrap_or_else(|err|{
				warning!(debug, "Config: Line {line}: loadmodule: {err}")
			});
		}
		if self.disable_modules { LocalKeyPair::disable_module_loading(); }
		let (default_ca, default_ca_checkanchor) = {
			//Default CA to (default).
			let (line, default_ca) = match &self.default_ca {
				&Some((line, ref name)) => (line, name.deref()),
				None => (0, "(default)"),
			};
			ca_list.get(default_ca).cloned().unwrap_or_else(||{
				warning!(debug, "Config: Line {line}: nonexistent default CA '{default_ca}'");
				(Rc::new(RefCell::new(AcmeServiceData::dummy(default_ca))), Rootpile::new())
			})
		};

		let requests_dir = cast_create_dir(self.requests_dir, "Request")?;
		let certificates_dir = cast_create_dir(self.certificates_dir, "Certificates")?;
		let state_dir = cast_create_dir(self.state_dir, "State")?;
		let logs_dir = cast_create_dir(self.logs_dir, "Logs")?;

		let thumbprint = Base64UrlStr::new_octets(b"test-thumbprint").unwrap();
		check_directory_reachable(&certificates_dir)?;
		check_directory_writable(&certificates_dir, thumbprint)?;
		check_directory_writable(&state_dir, thumbprint)?;
		check_directory_writable(&logs_dir, thumbprint)?;
		check_is_directory(&requests_dir)?;

		//Whitelist special chain entries.
		let mut sint_whitelist = BTreeMap::new();
		for (filename, (_, range)) in self.sint_whitelist.drain() {
			sint_whitelist.insert(filename, range);
		}
		//Root Blacklist
		let mut intermediates = IntermediatePile::new();
		for entry in self.blacklist_url.drain(..) {
			let entry = f_continue!(AsciiStr::new_octets(entry.as_bytes()).map_err(|err|{
				warning!(debug, "Config: Bad blacklist URL {err}")
			}));
			intermediates.blacklist(entry);
		}

		//Various certificate files.
		for (line, file) in self.intermediate_file.drain(..) {
			let content = f_continue!(read_cert_file(Path::new(file.deref()), false).map_err(|err|{
				warning!(debug, "Config: Line {line}: Intermediate: {err}")
			}));
			for (seqno, cert) in content.iter().enumerate() {
				intermediates.add(cert, &debug).unwrap_or_else(|err|{
					warning!(debug, "Config: Line {line}: parsecert#{idx} {file}: {err}",
						idx=seqno+1)
				});
			}
		}
		let mut sintermediates = SpecialIntermediatePile::new();
		let ts = crate::acme_daemon::get_nowts();	//Use decimal time.
		sintermediates.scan_special_entries(&certificates_dir, ts, &sint_whitelist, &debug);
		if self.no_aia { sintermediates.disable_aia(); }
		for (line, a,b,mut c) in self.authentication_modules.drain(..) {
			let _b = b;
			let b = PathBuf::from(_b.clone());
			if !_b.starts_with("$builtin/") {
				let f = f_continue!(b.metadata().map_err(|err|{
					warning!(debug, "Config: Line {line}: stat {b}: {err}", b=b.display())
				}));
				if !f.is_file() || f.mode() & 64 == 0 {
					warning!(debug, "Config: Line {line}: {b}: Irregular or not executable",
						b=b.display());
					continue;
				}
			}
			let b = f_continue!(DlfcnModule::new(&b).map_err(|err|{
				warning!(debug, "Line {line}: load {b}: {err}", b=b.display())
			}));
			//Concatenate arguments split by spaces.
			let mut cc = String::new();
			for c in c.drain(..) {
				if cc.len() > 0 { cc.push(' '); }
				cc.push_str(&c);
			}
			authentication_modules.push((a.unwrap_or(String::new()),b,cc));
		}
		for (line, a,b) in self.post_renew.drain(..) {
			let a = PathBuf::from(a);
			let f = f_continue!(a.metadata().map_err(|err|{
				warning!(debug, "Config: Line {line}: stat {a}: {err}", a=a.display())
			}));
			if !f.is_file() || f.mode() & 64 == 0 {
				warning!(debug, "Config: Line {line}: {a}: Irregular or not executable",
					a=a.display());
				continue;
			}
			post_renew.push((a,b));
		}
		for (line, a) in self.post_install.drain(..) {
			let a = PathBuf::from(a);
			let f = f_continue!(a.metadata().map_err(|err|{
				warning!(debug, "Line {line}: stat {a}: {err}", a=a.display())
			}));
			if !f.is_file() || f.mode() & 64 == 0 {
				warning!(debug, "Config: Line {line}: {a}: Irregular or not executable",
					a=a.display());
				continue;
			}
			post_install.push(a);
		}

		let cert_dflt = PerCertificateConfiguration {
			key: None,	//Default certificate never has nontrivial key.
			certdir: CertDirTag::new(certificates_dir),
			rootpile: default_ca_checkanchor.clone(),
			ca: default_ca.clone(),
		};
		let mut cert_special = BTreeMap::new();
		for (k, (line, v)) in self.certificate_ca.drain() {
			let k = FilenameMatch::new(&k);
			let (v1, v2) = ca_list.get(v.deref()).cloned().unwrap_or_else(||{
				warning!(debug, "Config: Line {line}: Undefined CA '{v}'");
				(Rc::new(RefCell::new(AcmeServiceData::dummy(&v))), Rootpile::new())
			});
			let entry = cert_special.entry(k).or_insert_with(||{
				cert_dflt.clone()
			});
			entry.ca = v1;
			entry.rootpile = v2;
		}
		for (k, (_, v)) in self.certificate_key.drain() {
			let k = FilenameMatch::new(&k);
			//We do not check these, since they are not guaranteed to be readable.
			cert_special.entry(k).or_insert_with(||cert_dflt.clone()).key = Some(PathBuf::from(v));
		}
		for (k, v) in self.certificate_dir.drain() {
			let k = FilenameMatch::new(&k);
			//This is never supposed to use the passed kind, since file is always Some().
			let dir = cast_create_dir(Some(v), "")?;
			check_directory_reachable(&dir)?;
			check_directory_writable(&dir, thumbprint)?;
			cert_special.entry(k).or_insert_with(||{
				cert_dflt.clone()
			}).certdir = CertDirTag::new(dir);
		}
		//Create directories.
		if let Some((line, vdir)) = self.validation_dir {
			Self::do_validation_dir(&mut authentication_modules, line, vdir, thumbprint, debug);
		}

		Ok(ParsedFile2 {
			global_config: GlobalConfiguration {
				debug: debug.clone(),
				auth_modules: authentication_modules,
				reqdir: ReqDirTag::new(requests_dir),
				statedir: StateDirTag::new(state_dir),
				logsdir: logs_dir,
				pile: intermediates,
				spile: sintermediates,
				replace: replace,
				post_renew: post_renew,
				post_install: post_install,
				initial_wait: initial_wait,
				round_wait: round_wait,
				error_wait: error_wait,
				assume_revoked: assume_revoked,
				ca_list: ca_list,
				quiet: false,
				daemon: false,
			},
			cert_config: PerCertificateConfigurations {
				dflt: cert_dflt,
				special: cert_special,
			},
		})
	}
	fn do_validation_dir(modules: &mut Vec<(String, DlfcnModule, String)>, line: usize, vdir: String,
		thumbprint: &Base64UrlStr, debug: &Debugging)
	{
		let vdir = f_return!(make_writable_directory(vdir, thumbprint).map_err(|err|{
			warning!(debug, "Config: Line {line}: Bad webroot dir: {err}");
		}), ());
		//Add webroot module for path.
		match DlfcnModule::new("$builtin/webroot") {
			Ok(module) => {
				let domain = String::new();
				let dir = vdir.display().to_string();
				modules.push((domain, module, dir));
			},
			Err(err) => warning!(debug, "Failed to load webroot module: {err}'")
		}
	}
}


enum ParsedDirective
{
	Ca(String, CaDirective),
	ValidationDirectory(String),
	RequestsDirectory(String),
	CertificatesDirectory(String),
	StateDirectory(String),
	LogsDirectory(String),
	BlacklistUrl(String),
	SIntWhitelist(FilenameBuf, Range<u64>),
	IntermediateFile(String),
	Replace(String),
	DisableModules,
	KeyModule(String),
	AuthenticationModule(String, Vec<String>),
	AuthenticationModuleDomain(String, String, Vec<String>),
	Certificate(String, CertDirective),
	PostRenew(String, Vec<OsString>),
	PostInstall(String),
	DefaultCa(String),
	InitialWait(u64),
	RoundWait(u64),
	ErrorWait(u64),
	DisableAia,
	AssumeRevoked(Vec<u8>, Vec<u8>),
}

impl ParsedDirective
{
	fn parse_raw(d: &LineContents) -> Result<ParsedDirective, String>
	{
		let mut itr = d.content.iter();
		match itr.next().ok_or("Expected directive")?.deref() {
			"ca" => {
				let caname = itr.next().ok_or("Expected CA name")?.deref();
				Ok(ParsedDirective::Ca(caname.to_owned(), CaDirective::parse(itr)?))
			},
			"validation-directory" => {
				let dir = itr.next().ok_or("Expected directory path")?.deref();
				reject_more(itr)?;
				Ok(ParsedDirective::ValidationDirectory(dir.to_owned()))
			},
			"requests-directory" => {
				let dir = itr.next().ok_or("Expected directory path")?.deref();
				reject_more(itr)?;
				Ok(ParsedDirective::RequestsDirectory(dir.to_owned()))
			},
			"certificates-directory" => {
				let dir = itr.next().ok_or("Expected directory path")?.deref();
				reject_more(itr)?;
				Ok(ParsedDirective::CertificatesDirectory(dir.to_owned()))
			},
			"state-directory" => {
				let dir = itr.next().ok_or("Expected directory path")?.deref();
				reject_more(itr)?;
				Ok(ParsedDirective::StateDirectory(dir.to_owned()))
			},
			"logs-directory" => {
				let dir = itr.next().ok_or("Expected directory path")?.deref();
				reject_more(itr)?;
				Ok(ParsedDirective::LogsDirectory(dir.to_owned()))
			},
			"blacklist" => {
				let file = itr.next().ok_or("Expected URL")?.deref();
				reject_more(itr)?;
				Ok(ParsedDirective::BlacklistUrl(file.to_owned()))
			},
			"intermediate" => {
				let file = itr.next().ok_or("Expected file path")?.deref();
				reject_more(itr)?;
				Ok(ParsedDirective::IntermediateFile(file.to_owned()))
			},
			"disable-modules" => {
				reject_more(itr)?;
				Ok(ParsedDirective::DisableModules)
			},
			"disable-aia" => {
				reject_more(itr)?;
				Ok(ParsedDirective::DisableAia)
			},
			"key-module" => {
				let file = itr.next().ok_or("Expected file path")?.deref();
				reject_more(itr)?;
				Ok(ParsedDirective::KeyModule(file.to_owned()))
			},
			"authentication-module" => {
				let file = itr.next().ok_or("Expected file path")?.deref();
				let args = itr.cloned().collect();
				Ok(ParsedDirective::AuthenticationModule(file.to_owned(), args))
			},
			"authentication-module-domains" => {
				let dom = itr.next().ok_or("Expected domain list")?.deref();
				let file = itr.next().ok_or("Expected file path")?.deref();
				let args = itr.cloned().collect();
				Ok(ParsedDirective::AuthenticationModuleDomain(dom.to_owned(), file.to_owned(),
					args))
			},
			"certificate" => {
				let certname = itr.next().ok_or("Expected certificate name")?.deref();
				Ok(ParsedDirective::Certificate(certname.to_owned(), CertDirective::parse(itr)?))
			},
			"post-renew" => {
				let file = itr.next().ok_or("Expected executable")?.deref();
				let args = itr.cloned().map(|x|OsString::from(x)).collect();
				Ok(ParsedDirective::PostRenew(file.to_owned(), args))
			},
			"post-install" => {
				let file = itr.next().ok_or("Expected executable")?.deref();
				reject_more(itr)?;
				Ok(ParsedDirective::PostInstall(file.to_owned()))
			},
			"default-ca" => {
				let dir = itr.next().ok_or("Expected CA name")?.deref();
				reject_more(itr)?;
				Ok(ParsedDirective::DefaultCa(dir.to_owned()))
			},
			"dns-replace" => {
				let a = itr.next().ok_or("Expected replace spec")?.deref();
				let b = itr.next().ok_or("Expected replace spec")?.deref();
				let c = itr.next().ok_or("Expected replace spec")?.deref();
				let d = itr.next().ok_or("Expected replace spec")?.deref();
				reject_more(itr)?;
				let dir = format!("{a} {b} {c} {d}");
				Ok(ParsedDirective::Replace(dir))
			},
			"initial-wait" => {
				let dir = itr.next().ok_or("Expected duration")?.deref();
				reject_more(itr)?;
				let dir = u64::from_str(dir).set_err("Invalid duration")?;
				Ok(ParsedDirective::InitialWait(dir))
			},
			"round-wait" => {
				let dir = itr.next().ok_or("Expected duration")?.deref();
				reject_more(itr)?;
				let dir = u64::from_str(dir).set_err("Invalid duration")?;
				Ok(ParsedDirective::RoundWait(dir))
			},
			"error-wait" => {
				let dir = itr.next().ok_or("Expected duration")?.deref();
				reject_more(itr)?;
				let dir = u64::from_str(dir).set_err("Invalid duration")?;
				Ok(ParsedDirective::ErrorWait(dir))
			},
			"issuer-whitelist" => {
				let filename = itr.next().ok_or("Expected filename")?.deref();
				let range = itr.next().ok_or("Expected time range")?.deref();
				reject_more(itr)?;
				let (start, end) = match range.find("-") {
					Some(p) => (&range[..p], &range[p+1..]),
					None => fail!("Invalid time range")
				};
				let start = u64::from_str(start).set_err("Invalid time range")?;
				let end = u64::from_str(end).set_err("Invalid time range")?;
				let prange = start..end;
				Ok(ParsedDirective::SIntWhitelist(Filename::from_str(filename).to_owned(), prange))
			},
			"assume-revoked" => {
				let issid = itr.next().ok_or("Expected issuer ID")?.deref();
				let serial = itr.next().ok_or("Expected serial number")?.deref();
				reject_more(itr)?;
				let issid = hextobytes(issid).ok_or("Bad issuer ID")?;
				let serial = hextobytes(serial).ok_or("Bad serial")?;
				Ok(ParsedDirective::AssumeRevoked(issid, serial))
			},
			directive => fail!(format!("Unrecognized directive {directive}"))
		}
	}
}

fn hextobytes(hexes: &str) -> Option<Vec<u8>>
{
	if hexes.len() % 2 != 0 { return None; }
	let mut out = Vec::new();
	let mut carry = None;
	for c in hexes.chars() {
		let v = c.to_digit(16)? as u8;
		if let Some(h) = carry.take() {
			out.push(h * 16 + v);
		} else {
			carry = Some(v);
		}
	}
	Some(out)
}

pub fn parse_new_config(debug: Debugging) -> Option<(GlobalConfiguration, PerCertificateConfigurations)>
{
	let filename = Path::new("acmetool.cfg");
	fail_if_none!(!filename.is_file());
	let parsed = ParsedFile::parse_file(filename, &debug).unwrap_or_else(|err|{
		alert!(debug, "Unable to parse configuration: {err}");
		exit(65);
	});
	Some((parsed.global_config, parsed.cert_config))
}
