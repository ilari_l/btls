use super::Ao;
use super::bintextcode_dec;
use super::acmeverify::is_get_redirect;
use super::deadline::Deadline;
use super::http::HttpRequest;
use super::http::HttpReply;
use super::http::HttpConnection;
use super::http::HttpPendingReply;
use super::http::StreamSource;
use super::debug::Debugging;
use super::debug::DEBUG_HTTP_SUMMARY;
use super::debug::DEBUG_HTTP_TRANSFER;
use super::tls::construct_target;
use super::tls::ErrorClass;
use super::tls::TlsConnection;
use super::tls::TlsFault;
use super::tls::TrustAnchorSet;
use super::strings::AsciiStr;
use super::strings::HostStr;
use super::transport::Target;
use super::transport::TcpTransport;
use super::transport::Transport;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_http::http::EncodeHeaders;
use btls_aux_http::http::StandardHttpHeader;
use btls_aux_http::http1::HeaderEncoder;
use btls_aux_http::http1::Stream;
use btls_aux_http::http1::IoOrStreamError;
use btls_aux_memory::Attachment;
use btls_aux_memory::SafeShowByteString;
use btls_aux_memory::split_attach_first;
use std::borrow::Cow;
use std::net::Shutdown;
use std::net::SocketAddr;
use std::fmt::Display;
use std::fmt::Formatter;
use std::fmt::Result as FmtResult;
use std::ops::Deref;


pub enum Connection<T:Transport>
{
	Tcp(T, Target),
	Tls(TlsConnection<T>),
}

impl<T:Transport> Display for Connection<T>
{
	 fn fmt(&self, f: &mut Formatter) -> FmtResult
	 {
		match self {
			&Connection::Tcp(_, ref addr) => write!(f, "{addr}"),
			&Connection::Tls(ref conn) => write!(f, "{conn}"),
		}
	 }
}

impl<T:Transport> Connection<T>
{
	pub fn new_tcp_connected(target: &HostStr, service: &AsciiStr, connection: T, _debug: &Debugging) ->
		Result<Connection<T>, TlsFault>
	{
		let addr = construct_target(target, service, &connection);
		Ok(Connection::Tcp(connection, addr))
	}
	pub fn new_tls_connected(target: &HostStr, service: &AsciiStr, connection: T,
		trustpile: &TrustAnchorSet, debug: &Debugging, deadline: Deadline) -> Result<Connection<T>, TlsFault>
	{
		Ok(Connection::Tls(TlsConnection::from_raw_tcp(target, service, connection, trustpile,
			debug, deadline)?))
	}
/*
	fn new(target: &HostStr, service: &AsciiStr, target_address: Option<SocketAddr>, debug: &Debugging,
		deadline: Deadline) -> Result<Connection<T>, TlsFault>
	{
		let (connection, addr) = T::establish_connection(target, service, target_address, debug, deadline)?;
		Ok(Connection::Tcp(connection, addr))
	}
*/
	pub fn peer_address(&self) -> SocketAddr
	{
		match self {
			&Connection::Tcp(_, ref addr) => addr.get_addr(),
			&Connection::Tls(ref x) => x.get_addr(),
		}
	}
}

impl<T:Transport> HttpConnection for Connection<T>
{
	fn send(&mut self, data: &[u8], deadline: Deadline) -> Result<(), TlsFault>
	{
		match self {
			&mut Connection::Tcp(ref mut x, _) => {
				let mut dptr = 0;
				while dptr < data.len() {
					let data = &data[dptr..];
					match x.write(data, deadline)? {
						x => dptr += x,
					};
				}
				Ok(())
			},
			&mut Connection::Tls(ref mut x) => x.send(data, deadline),
		}
	}
	//Only calls cb with empty data on EOF.
	fn receive<X:Sized, F>(&mut self, deadline: Deadline, mut cb: F) -> Result<X, TlsFault>
		where F: FnMut(&[u8]) -> Result<X, String>
	{
		match self {
			&mut Connection::Tcp(ref mut x, ref addr) => {
				let mut buf = [0u8;16384];
				//read is pseudo-blocking, so it only returns 0 bytes read on EOF.
				let size = x.read(&mut buf, deadline)?;
				return cb(&buf[..size]).map_err(|x|{
					TlsFault::new(addr, ErrorClass::Application, x.to_string())
				});
			},
			&mut Connection::Tls(ref mut x) => x.receive(deadline, cb),
		}
	}
	fn goodbye(&mut self)
	{
		match self {
			&mut Connection::Tcp(ref mut x, _) => x.shutdown(Shutdown::Write).unwrap_or(()),
			&mut Connection::Tls(ref mut x) => x.goodbye(),
		}
	}
}

pub fn get_insecure(url: &AsciiStr, target_address: Option<SocketAddr>, debug: &Debugging,
	deadline: Deadline) -> Result<HttpReply, String>
{
	//HTTPS is NOT supported.
	fail_if!(!url.as_inner().starts_with("http://"), format!("Not valid HTTP URL: {url}"));
	let req = HttpRequest::from_url(&url).map_err(|err|{
		format!("Can't parse HTTP URL to get: {err}")
	})?;
	//Authority needs port number if not 80.
	let key = req.connect_to();
	let (mut conn, _) = TcpTransport::establish_connection(&key.0, &key.1, target_address, debug, deadline).
		map_err(|err|{
		format!("Error connecting to {url}: {err}")
	})?;
	//Format the request and send it.
	let request = (|| -> Result<Vec<u8>, ()> {
		let mut request = Vec::new();
		let authority = match req.port {
			80 => Cow::Borrowed(req.target.as_bytes()),
			port => Cow::Owned(format!("{tgt}:{port}", tgt=req.target).into_bytes()),
		};
		let (path, query) = split_attach_first(req.path.as_bytes(), b"?", Attachment::Right).
			unwrap_or((req.path.as_bytes(), b""));
		HeaderEncoder::request_line(&mut request, req.method.as_bytes(), b"", authority.deref(), path,
			query)?;
		for (n, v) in req.headers.iter() {
			HeaderEncoder::header(&mut request, n.as_bytes(), v.as_bytes())?;
		}
		HeaderEncoder::header(&mut request, b"connection", b"close")?;	//Never keepalive.
		HeaderEncoder::end(&mut request)?;
		Ok(request)
	})().map_err(|_|{
		format!("Internal error: from_url({url}) gave illegal request")
	})?;
	debug!(debug, DEBUG_HTTP_SUMMARY, "GET {url}...");
	conn.write_all(&request, deadline).map_err(|err|{
		format!("GET {url}: {err}")
	})?;
	debug!(debug, DEBUG_HTTP_TRANSFER, "Get {url}: Sent request, waiting for reply...");
	//Receive reply.
	let mut buf = [0u8;8192];
	let mut http_reply = HttpPendingReply::new();
	let mut http_stream = Stream::new(b"foo");	//The default scheme is irrelevant.
	http_stream.set_use_std_flag();			//Use standard headers.
	http_stream.set_reply(false);			//This always GET (not HEAD).
	let reply = loop {
		let amt = conn.read(&mut buf, deadline).map_err(|err|{
			format!("GET {url}: {err}")
		})?;
		let data = &buf[..amt];
		debug!(debug, DEBUG_HTTP_TRANSFER, "GET {url}: Received {len} bytes...", len=data.len());
		let mut src = StreamSource(data);
		match http_stream.push(&mut src, &mut http_reply) {
			Ok(_) => (),
			Err(IoOrStreamError::Io(_)) => fail!(format!("Internal Error: StreamSource failed")),
			Err(IoOrStreamError::Stream(err)) =>
				http_reply.set_error(format!("HTTP stream error: {err}")),
		};
		if let Some(err) = http_reply.get_error() { fail!(err); }
		if http_reply.is_finished() {
			//Finished a reply.
			let freply = http_reply.finish().map_err(|err|{
				format!("Finishing HTTP response failed: {err}")
			})?;
			break freply;
		}
		//This should never happen, but do not hang if it does for some reason.
		fail_if!(amt == 0, "Unexpected EOF from server");
	};
	debug!(debug, DEBUG_HTTP_SUMMARY, "GET {url}: {code} ({reason}",
		code=reply.code, reason=SafeShowByteString(&reply.reason));
	//Just ignore this failing, as it is not actually important.
	conn.shutdown(Shutdown::Write).ok();
	Ok(reply)
}

//Version of get_insecure() that auto-follows redirects.
pub fn get_insecure_redir(url: &AsciiStr, debug: &Debugging, deadline: Deadline) -> Result<Vec<u8>, String>
{
	//Special case for data: URLs. Those are used in some cases.
	if let Some(edata) = url.as_inner().strip_prefix("data:") {
		return Ok(bintextcode_dec(edata));
	}
	let mut redirects = 0;
	let mut requrl = url.to_owned();
	loop {
		let freply = get_insecure(&requrl, None, debug, deadline)?;
		//Follow redirects.
		if is_get_redirect(freply.code) {
			fail_if!(redirects >= 10, "Too many redirects");
			redirects += 1;
			let location = match freply.get_header(StandardHttpHeader::Location) {
				Ok(Some(x)) => x,
				Ok(None) => fail!(format!("No 'location' header in redirect!")),
				Err(err) => fail!(format!("Error reading 'location' header in redirect: {err}"))
			};
			if location.starts_with("//") {
				//Prepend http: for scheme.
				requrl = Ao(format!("http:{location}"));
			} else if location.starts_with("/") {
				//The 7 is for stripping http://.
				let url2 = requrl.as_inner();
				let url2 = &url2[7..];
				let alen = url2.find("/").unwrap_or(url2.len());
				let auth = &url2[..alen];
				requrl = Ao(format!("http://{auth}{location}"));
			} else {
				requrl = location;
			}
			debug!(debug, DEBUG_HTTP_SUMMARY, "GET {url}: Redirected to {requrl}...");
		} else if freply.code / 100 == 2 {
			return Ok(freply.body);
		} else {
			fail!(format!("{code} ({reason})",
				code=freply.code, reason=SafeShowByteString(&freply.reason)));
		}
	}
}
