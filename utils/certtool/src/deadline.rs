use std::cmp::min;
use std::convert::TryInto;
use std::time::Duration;
use std::time::Instant;

#[derive(Copy,Clone)]
pub struct Deadline
{
	deadline: Instant,
}

impl Deadline
{
	pub fn new(max: Duration) -> Deadline
	{
		Deadline {
			deadline: Instant::now() + max,
		}
	}
	pub fn expired(self) -> bool
	{
		Instant::now() > self.deadline
	}
	pub fn derive(self, max: Duration) -> Deadline
	{
		Deadline {
			deadline: min(self.deadline, Instant::now() + max),
		}
	}
	pub fn nanos_to(self) -> u64
	{
		let dt = self.deadline.saturating_duration_since(Instant::now());
		dt.as_nanos().try_into().unwrap_or(u64::MAX)
	}
}
