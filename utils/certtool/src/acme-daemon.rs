use crate::check_sigterm;
use crate::acme_common::AccountDatabase;
use crate::acme_common::AcmeServiceData;
use crate::acme_common::AcmeServiceDataCache;
use crate::acme_common::GlobalConfiguration;
use crate::acme_common::PerCertificateConfiguration;
use crate::acme_common::PerCertificateConfigurations;
use crate::acme_order::determine_next_renew;
use crate::acme_order::run_certificate;
use btls_aux_fail::f_continue;
use btls_aux_fail::f_return;
use btls_aux_fail::fail_if;
use btls_aux_fail::fail_if_none;
use btls_aux_filename::Filename;
use btls_aux_filename::FilenameBuf;
use btls_aux_time::daynumber_into_date;
use btls_aux_time::DecodeTsForUser2;
use btls_aux_time::Timestamp;
use btls_aux_unix::LogFacility;
use btls_aux_unix::Logger;
use btls_aux_unix::LogLevel;
use btls_aux_unix::OpenlogFlags;
use btls_certtool::path_append_extension;
use btls_certtool::critical;
use btls_certtool::debug;
use btls_certtool::error;
use btls_certtool::info;
use btls_certtool::acme_service::AcmeService;
use btls_certtool::acme_service::AcmeServiceRegistered;
use btls_certtool::debug::Debugging;
use btls_certtool::debug::DEBUG_CSR;
use btls_certtool::privatekey::Privatekey;
use std::cell::RefCell;
use std::cmp::min;
use std::collections::BTreeMap;
use std::collections::BTreeSet;
use std::ffi::OsString;
use std::fmt::Arguments;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::fs::read_dir;
use std::ops::Deref;
use std::ops::DerefMut;
use std::rc::Rc;
use std::path::Path;
use std::path::PathBuf;
use std::process::Command;
use std::str::FromStr;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering as MemOrdering;
use std::thread::sleep;
use std::time::Instant;
use std::time::Duration;


static SYSLOG_IS_INIT: AtomicBool = AtomicBool::new(false);

pub fn is_noninteractive() -> bool { SYSLOG_IS_INIT.load(MemOrdering::Relaxed) }

pub fn init_syslog()
{
	Logger::openlog(b"acmetool", OpenlogFlags::PID, LogFacility::Daemon);
	SYSLOG_IS_INIT.store(true, MemOrdering::Relaxed);
}

pub fn write_to_syslog(msg: Arguments, priority: LogLevel)
{
	if !SYSLOG_IS_INIT.load(MemOrdering::Relaxed) { return; }
	Logger::Syslog.log(None, priority, format_args!("{msg}"));
}

macro_rules! syslog_o
{
	($($fmt:tt)*) => { write_to_syslog(format_args!($($fmt)*), LogLevel::Notice) }
}

macro_rules! try_if_none
{
	($a:expr, $b:expr) => { match $a { Some(x) => x, None => return $b } }
}

macro_rules! try_cont
{
	($a:expr) => { match $a { Some(x) => x, None => continue } }
}


#[derive(Copy,Clone,Debug)] pub struct ReqDirTag<T>(T);

impl<T> ReqDirTag<T>
{
	pub fn new(val: T) -> ReqDirTag<T> { ReqDirTag(val) }
	pub fn into_inner(self) -> T { self.0 }
}

impl<T:Deref> ReqDirTag<T>
{
	pub fn as_deref(&self) -> ReqDirTag<&T::Target> { ReqDirTag(self.0.deref()) }
	pub fn deref_inner(&self) -> &T::Target { self.0.deref() }
}

impl<T:AsRef<Path>> ReqDirTag<T>
{
	pub fn display_path<'a>(&'a self) -> impl Display+'a
	{
		self.0.as_ref().display()
	}
}

struct PrintOptionTs(Option<Timestamp>);

impl Display for PrintOptionTs
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		match self.0 {
			Some(t) => Display::fmt(&DecodeTsForUser2(t), f),
			None => f.write_str("(Unknown)")
		}
	}
}


#[derive(Copy,Clone,Debug)] pub struct StateDirTag<T>(T);

impl<T> StateDirTag<T>
{
	pub fn new(val: T) -> StateDirTag<T> { StateDirTag(val) }
	pub fn into_inner(self) -> T { self.0 }
}

impl<T:Deref> StateDirTag<T>
{
	pub fn as_deref(&self) -> StateDirTag<&T::Target> { StateDirTag(self.0.deref()) }
	pub fn deref_inner(&self) -> &T::Target { self.0.deref() }
}

#[derive(Copy,Clone,Debug)] pub struct CertDirTag<T>(T);

impl<T> CertDirTag<T>
{
	pub fn new(val: T) -> CertDirTag<T> { CertDirTag(val) }
	pub fn into_inner(self) -> T { self.0 }
}

impl<T:Deref> CertDirTag<T>
{
	pub fn as_deref(&self) -> CertDirTag<&T::Target> { CertDirTag(self.0.deref()) }
	pub fn deref_inner(&self) -> &T::Target { self.0.deref() }
}

fn f2p<'a>(name: &'a Filename) -> &'a Path { Path::new(name.into_os_str()) }

pub fn f2_request(basedir: ReqDirTag<&Path>, name: &Filename) -> PathBuf
{
	path_append_extension(&basedir.0.join(f2p(name)), "csr")
}
pub fn f2_certificate(basedir: CertDirTag<&Path>, name: &Filename) -> PathBuf
{
	path_append_extension(&basedir.0.join(f2p(name)), "crt")
}
pub fn f2_eecert(basedir: CertDirTag<&Path>, name: &Filename) -> PathBuf
{
	f2_certificate(basedir, name).join("certificate")
}
pub fn f2_keysymlink(basedir: CertDirTag<&Path>, name: &Filename) -> PathBuf
{
	f2_certificate(basedir, name).join("key")
}
pub fn f2_certtmp(basedir: CertDirTag<&Path>, name: &Filename) -> PathBuf
{
	path_append_extension(&basedir.0.join(f2p(name)), "tcrt")
}
pub fn f2_certtmpfail(basedir: CertDirTag<&Path>, name: &Filename) -> PathBuf
{
	path_append_extension(&basedir.0.join(f2p(name)), "tcrt.fail")
}

fn load_random_account_key() -> Result<Privatekey, String>
{
	Privatekey::new_transient().map_err(|err|{
		format!("Error generating account key: {err}")
	})
}

pub struct TimePriorityQueue<T:Clone+Ord>
{
	forward: BTreeMap<Timestamp, BTreeSet<T>>,
	backward: BTreeMap<T, Timestamp>,
}

impl<T:Clone+Ord> TimePriorityQueue<T>
{
	pub fn new() -> TimePriorityQueue<T>
	{
		TimePriorityQueue {
			forward: BTreeMap::new(),
			backward: BTreeMap::new(),
		}
	}
	fn add(&mut self, obj: &T, when: Timestamp)
	{
		//In case the object is already in set, remove it.
		self.remove(obj);
		self.backward.insert(obj.clone(), when);
		self.forward.entry(when).or_insert_with(||{
			BTreeSet::new()
		}).insert(obj.clone());
	}
	fn remove(&mut self, obj: &T)
	{
		if let Some(when) = self.backward.remove(obj) {
			let mut nuke = false;
			if let Some(list) = self.forward.get_mut(&when) {
				list.remove(obj);
				nuke = list.is_empty();
			}
			if nuke { self.forward.remove(&when); }
		}
	}
	fn is_in(&self, obj: &T) -> bool { self.backward.contains_key(obj) }
	pub fn first_timestamp(&self) -> Option<Timestamp> { Some(*self.forward.iter().next()?.0) }
	fn pop(&mut self, maxwhen: Timestamp) -> Option<T>
	{
		let minwhen = self.first_timestamp()?;
		fail_if_none!(minwhen > maxwhen);
		let list = self.forward.get(&minwhen)?;
		let obj = list.iter().next()?.clone();
		self.remove(&obj);
		Some(obj)
	}
	fn retain(&mut self, set: &BTreeSet<T>, mut  callback: impl FnMut(&T))
	{
		let mut delete = Vec::new();
		for (key, _) in self.backward.iter() {
			if !set.contains(key) { delete.push(key.clone()); }
		}
		for entry in delete.drain(..) {
			callback(&entry);
			self.remove(&entry);
		}
	}
}


impl AcmeServiceData
{
	fn load_key(&mut self)
	{
		//If already called, do not retry.
		if self.key.is_some() || self.load_key_err.is_some() { return; }
		match Privatekey::new_content(&self.key_contents, &self.key_path.display().to_string()) {
			Ok(key) => self.key = Some(Rc::new(key)),
			Err(err) => self.load_key_err = Some(format!("Failed to load key for {name}: {err}",
				name=self.name))
		}
	}
	pub fn to_acme_registered(&self, state: &Path, debug: Debugging, test_only: bool) ->
		Result<AcmeServiceRegistered, String>
	{
		let mut acctdb = AccountDatabase(state.to_path_buf());
		let (caurl, account_key) = if !test_only {
			let key = self.key.as_ref().ok_or_else(||{
				self.load_key_err.as_deref().unwrap_or("Internal error: Keys not loaded")
			})?;
			(&self.url, key.clone())
		} else {
			let key = Rc::new(load_random_account_key()?);
			let url = self.test_url.as_ref().ok_or_else(||{
				format!("No test URL available for CA {name}", name=self.name)
			})?;
			(url, key)
		};

		fail_if!(self.trust_anchors.len() == 0,
			format!("No trust anchors to contact CA {name}", name=self.name));

		let acme = AcmeService::new(caurl, &self.trust_anchors, &debug).map_err(|err|{
			format!("ACME service at {caurl} is busted: {err}")
		})?;
		//If the ACME service was not primary, load the primary as non-registered service.
		let primary = if test_only {
			let pacme = AcmeService::new(&self.url, &self.trust_anchors, &debug).map_err(|err|{
				format!("ACME service at {url} is busted: {err}", url=self.url)
			})?;
			Some(pacme)
		} else {
			None
		};
		//Disable registration if in test mode to avoid littering state directory with crap.
		let acme = AcmeServiceRegistered::new(acme, &mut acctdb, account_key, &debug, test_only, primary)?;
		info!(debug, "Account ID is {acct} (thumbprint={thumb})",
			acct=acme.account_id(), thumb=acme.thumbprint());
		Ok(acme)
	}
}

impl AcmeServiceDataCache
{
	pub fn to_acme_registered(&mut self, data: Rc<RefCell<AcmeServiceData>>, state: &Path, debug: Debugging,
		test_only: bool) -> Result<Rc<RefCell<AcmeServiceRegistered>>, String>
	{
		let dataptr = data.deref().as_ptr() as usize;
		if let Some(entry) = self.cache.get(&dataptr) { return Ok(entry.clone()); }
		let acme = data.borrow().to_acme_registered(state, debug, test_only)?;
		let entry = Rc::new(RefCell::new(acme));
		self.cache.insert(dataptr, entry.clone());
		Ok(entry)
	}
}

impl GlobalConfiguration
{
	pub fn call_load_key(&self)
	{
		for (_, (ref key, _)) in self.ca_list.iter() {
			let mut ca = key.borrow_mut();
			ca.load_key();
		}
	}
}

impl PerCertificateConfigurations
{
	pub fn call_load_key(&self)
	{
		self.dflt.call_load_key();
		for (_, entry) in self.special.iter() { entry.call_load_key(); }
	}
	pub(super) fn for_cert(&self, name: &Filename) -> PerCertificateConfiguration
	{
		if let Some(c) = self.special.get(name.into_match_key()) { return c.clone(); }
		return self.dflt.clone();
	}
}

impl PerCertificateConfiguration
{
	fn call_load_key(&self)
	{
		let mut ca = self.ca.borrow_mut();
		ca.load_key();
	}
}

pub fn get_nowts() -> u64
{
	let ts = Timestamp::now().unix_time();
	let (year, month, day) = daynumber_into_date(ts / 86400);
	let hour = ts % 86400 / 3600;
	let minute = ts % 3600 / 60;
	let second = ts % 60;
	(second + 100 * minute + 10000 * hour + (day as i64) * 1000000 + (month as i64) * 100000000 +
		year * 10000000000) as u64
}

pub fn time_limit_variation(base: &Path, debug: Debugging) -> PathBuf
{
	let mut best = base.to_path_buf();
	let mut hts = 0;
	let nowts = get_nowts();
	let basename = Filename::from_os_str(try_if_none!(base.file_name(), best));
	let in_directory = base.parent().unwrap_or(Path::new("."));
	//Scan the directory.
	let dir = try_if_none!(in_directory.read_dir().ok(), best);
	for entry in dir {
		let entry = try_if_none!(entry.ok(), best);
		//Ignore entries that do not start with base, or where tail is not unicode.
		let fname = FilenameBuf::from_os_string(entry.file_name());
		let tail = try_cont!(fname.match_prefix(basename).and_then(|f|f.into_str()));
		let tail = f_continue!(tail.strip_prefix("."));
		let suffix = try_cont!(u64::from_str(tail).ok());
		if suffix > hts && suffix <= nowts {
			best = entry.path();
			hts = suffix;
		}
	}
	debug!(debug, DEBUG_CSR, "{base}: Using time limit variation {best}", base=base.display(),
		best=best.display());
	best
}

pub fn csr_list(directory: ReqDirTag<&Path>) -> Result<Vec<FilenameBuf>, String>
{
	let mut ret = Vec::new();
	let dir = read_dir(directory.deref_inner()).map_err(|err|{
		format!("Unable to get list of CSRs: {err}'")
	})?;
	for entry in dir {
		let entry = entry.map_err(|err|{
			format!("Unable to get list of CSRs: {err}'")
		})?;
		let entry = entry.path();
		if !entry.is_file() { continue; }	//Skip all crap.
		let entry = match entry.file_name() { Some(x) => x, None => continue };
		let entryname = Filename::from_os_str(entry);
		let (basename, extc) = entryname.split_name_extension();
		let extc = extc.and_then(|extension|{
			extension.into_str()
		});
		if extc != Some("csr") { continue; }
		ret.push(basename.to_owned());
	}
	Ok(ret)
}

fn do_maintenance_run_one(globalcfg: &GlobalConfiguration, certscfg: &PerCertificateConfigurations,
	cache: &mut AcmeServiceDataCache, csr: &Filename, some_failed: &mut bool, at_least_one: &mut bool,
	next_renew: &mut Option<Timestamp>)
{
	let req = f2_request(globalcfg.reqdir.as_deref(), csr);
	let req = time_limit_variation(&req, globalcfg.debug.clone());
	let certcfg = certscfg.for_cert(&csr);
	//This is never a test.
	let acme = match cache.to_acme_registered(certcfg.ca.clone(), &globalcfg.statedir.0,
		globalcfg.debug.clone(), false) {
		Ok(x) => x,
		Err(err) => {
			renew_failed(csr, err, some_failed, &globalcfg.debug);
			return;
		}
	};
	let mut acme = acme.borrow_mut();
	//This one being real needs to run post_install.
	let ordercfg = certcfg.to_order(&globalcfg);
	match run_certificate(csr, &req, acme.deref_mut(), &ordercfg, None, next_renew,
		|name|{
		run_post_install(&globalcfg.post_install, vec![name.to_owned()], &globalcfg.debug)
	}) {
		Ok(x) => *at_least_one |= x,
		Err(err) => {
			renew_failed(csr, err, some_failed, &globalcfg.debug);
			return;
		}
	}
	//Fill next_renew time, if it did not get filled by run_certificate(). This happens in the case that actual
	//renew happens.
	if globalcfg.daemon && next_renew.is_none() {
		let dbg = &globalcfg.debug;
		match determine_next_renew(csr, acme.borrow_primary(), &ordercfg) {
			Ok(Some(ts)) => *next_renew = Some(ts),
			Ok(None) => critical!(dbg, "New certificate for {csr} should be renewed immediately???"),
			Err(err) => error!(dbg, "Error determing renew for {csr}: {err}"),
		}
	}
}

//Returns true if at least one renew was done.
#[allow(dead_code)]
pub fn do_maintenance_run(globalcfg: &GlobalConfiguration, certscfg: &PerCertificateConfigurations,
	csrlist: Vec<FilenameBuf>, some_failed: &mut bool, next_renew: &mut Option<Timestamp>) -> bool
{
	let mut at_least_one = false;
	let mut cache = AcmeServiceDataCache{cache: BTreeMap::new()};

	for csr in csrlist.iter() {
		do_maintenance_run_one(globalcfg, certscfg, &mut cache, csr, some_failed, &mut at_least_one,
			next_renew);
	}
	if at_least_one { run_post_renew(&globalcfg.post_renew, &globalcfg.debug); }
	at_least_one
}

fn wait_until(time: Instant) -> Instant
{
	let now = Instant::now();
	let wfor = f_return!(time.checked_duration_since(now), now);
	sleep(wfor);
	time
}

///100ms => 10rps max.
const MIN_ROUND: Duration = Duration::from_millis(100);

pub fn daemon_round(globalcfg: &GlobalConfiguration, certscfg: &PerCertificateConfigurations,
	renew_q: &mut TimePriorityQueue<FilenameBuf>) -> Result<(), String>
{
	let mut cache = AcmeServiceDataCache{cache: BTreeMap::new()};
	//Collect all the CSRs.
	let csrlist = csr_list(ReqDirTag(&globalcfg.reqdir.0))?;
	if csrlist.len() == 0 {
		info!(globalcfg.debug, "No Certificate Signing Requests (CSRs) found in {gcfg}",
			gcfg=globalcfg.reqdir.0.display());
		syslog_o!("No certificate requests to renew");
		return Ok(());
	}
	//Add new CSRs.
	for csr in csrlist.iter() {
		if !renew_q.is_in(csr) {
			info!(globalcfg.debug, "Found new CSR {csr}, processing now");
			renew_q.add(csr, Timestamp::negative_infinity());
		}
	}
	//Remove CSRs that no longer exist.
	let csrlist2: BTreeSet<_> = csrlist.iter().cloned().collect();
	renew_q.retain(&csrlist2, |csr|{
		info!(globalcfg.debug, "Dropped CSR {csr} as it no longer exists");
	});
	let mut at_least_one = false;
	let mut some_failed = false;
	//Use constant timebase for all events x seconds into the future to avoid tearing from using current time
	//as base for events.
	let timebase = Timestamp::now();
	//Have minimum time for rounds in order to avoid sending requests too fast.
	let mut min_req_time = Instant::now();
	while let Some(csr) = renew_q.pop(Timestamp::now()) {
		//Wait until time to process the iteration, and compute time for next iteration.
		min_req_time = wait_until(min_req_time) + MIN_ROUND;
		info!(globalcfg.debug, "Processing {csr}...");
		let mut some_failed2 = false;
		let mut next_renew2 = None;
		//Check SIGTERM around do_maintenance_run_one(), as that function can not be interrupted, but
		//this loop can be.
		check_sigterm();
		do_maintenance_run_one(globalcfg, certscfg, &mut cache, &csr, &mut some_failed2, &mut at_least_one,
			&mut next_renew2);
		check_sigterm();
		//do_maintenance_run_one() might not fill the timestamp, in case error occured.
		let on_error = timebase + globalcfg.error_wait as i64;
		let max_renew = timebase + globalcfg.round_wait as i64;
		let orig_renew = next_renew2;
		let next_renew2 = next_renew2.unwrap_or(on_error);
		let next_renew2 = min(next_renew2, max_renew);
		info!(globalcfg.debug, "Next event for {csr} is {t} (due for renew on {u})",
			t=DecodeTsForUser2(next_renew2), u=PrintOptionTs(orig_renew));
		renew_q.add(&csr, next_renew2);
		some_failed |= some_failed2;
	}
	if at_least_one { run_post_renew(&globalcfg.post_renew, &globalcfg.debug); }
	if some_failed { write_to_syslog(format_args!("Some renewals FAILED"), LogLevel::Alert); }
	//Kill connections as to not cause errors the next time.
	btls_certtool::http::kill_all_connections();
	btls_certtool::dns::kill_all_connections();

	Ok(())	//Successful.
}

pub fn run_script(name: &Path, args: &[OsString], debug: &Debugging)
{
	info!(debug, "Running {name}", name=name.display());
	//Ok, it is executable.
	let mut cmd = Command::new(name.as_os_str());
	for i in args.iter() { cmd.arg(i); }
	match cmd.spawn() {
		Ok(mut child) => {child.wait().ok();},
		Err(err) => {
			error!(debug, "FAILED to execute '{name}': {err}", name=name.display());
			write_to_syslog(format_args!("FAILED to execute '{name}': {err}", name=name.display()),
				LogLevel::Error);
		}
	}
}

pub fn run_post_renew(x: &[(PathBuf, Vec<OsString>)], debug: &Debugging)
{
	for &(ref name, ref args) in x.iter() { run_script(name, args, debug); }
}

pub fn run_post_install(x: &[PathBuf], mut args: Vec<FilenameBuf>, debug: &Debugging)
{
	//Convert all filenames to OS strings.
	let args = args.drain(..).map(|f|f.into_os_string()).collect::<Vec<_>>();
	for name in x.iter() { run_script(name, &args, debug); }
}

fn renew_failed(csr: &Filename, err: String, flag: &mut bool, debug: &Debugging)
{
	*flag = true;
	error!(debug, "{csr} FAILED: {err}");
	write_to_syslog(format_args!("{csr} FAILED: {err}"), LogLevel::Error);
}
