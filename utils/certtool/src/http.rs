use super::A;
use super::An;
use super::Ao;
use super::deadline::Deadline;
use super::debug::Debugging;
use super::debug::DEBUG_HTTP_MESSAGES;
use super::debug::DEBUG_HTTP_SUMMARY;
use super::debug::DEBUG_HTTP_TRANSFER;
use super::strings::AsciiStr;
use super::strings::AsciiString;
use super::strings::host_string_to_ascii_string;
use super::strings::HostStr;
use super::strings::HostString;
use super::strings::PathStr;
use super::strings::PathString;
use super::strings::safe_octets_to_text;
use super::strings::SafeOctetsToText;
use super::strings::SchemeStr;
use super::tls::TlsConnection;
use super::tls::TlsFault;
use super::tls::TrustAnchorSet;
use super::transport::Transport;
use super::transport::TcpTransport;
use super::wcheck::Rootpile;
use btls::certificates::TrustAnchorOrdered;
use btls::utility::GlobalMutex;
use btls_aux_fail::dtry;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::ResultExt;
use btls_aux_http::http::EncodeHeaders;
use btls_aux_http::http::ReceiveBuffer;
use btls_aux_http::http::Source;
use btls_aux_http::http::StandardHttpHeader;
use btls_aux_http::http1::Element;
use btls_aux_http::http1::ElementSink;
use btls_aux_http::http1::HeaderEncoder;
use btls_aux_http::http1::IoOrStreamError;
use btls_aux_http::http1::Stream;
use btls_aux_json2::JSON;
use btls_aux_memory::Attachment;
use btls_aux_memory::split_attach_first;
use std::borrow::Cow;
use std::borrow::ToOwned;
use std::cell::RefCell;
use std::cmp::min;
use std::collections::BTreeMap;
use std::collections::BTreeSet;
use std::fmt::Debug;
use std::fmt::Display;
use std::fmt::Formatter;
use std::fmt::Error as FmtError;
use std::mem::replace;
use std::mem::swap;
use std::ops::Deref;
use std::str::from_utf8;
use std::str::FromStr;
use std::time::Duration;


pub fn split_url<'a>(url: &'a AsciiStr, default_scheme: Option<&'a AsciiStr>) ->
	Result<(&'a SchemeStr, &'a HostStr, Option<u16>, &'a PathStr), ()>
{
	let url = url.as_inner();
	let (scheme, url) = if let Some(url) = url.strip_prefix("//") {
		let scheme = dtry!(default_scheme);
		(scheme.as_inner(), url)
	} else {
		dtry!(split_attach_first(url, "://", Attachment::Center))
	};
	let (host, url) = if let Some(url) = url.strip_prefix("[") {
		dtry!(split_attach_first(url, "]", Attachment::Center))
	} else {
		let p1 = url.find(':').unwrap_or(url.len());
		let p2 = url.find('/').unwrap_or(url.len());
		url.split_at(min(p1, p2))
	};
	let (port, mut path) = if let Some(url) = url.strip_prefix(":") {
		let (port, tail) = split_attach_first(url, "/", Attachment::Right).unwrap_or((url, ""));
		let port = if port == "https" { 443 } else { dtry!(u16::from_str(port)) };
		(Some(port), tail)
	} else {
		(None, url)
	};
	fail_if!(host.len() == 0, ());				//No default for empty host.
	if path.len() == 0 { path = "/"; }			//Default to root.
	fail_if!(!path.starts_with("/"), ());			//Path has to start with /.
	let host = dtry!(HostStr::new_string(host));
	let scheme = dtry!(SchemeStr::new_string(scheme));
	let path = dtry!(PathStr::new_string(path));
	Ok((scheme, host, port, path))
}

#[derive(Clone)]
pub struct HttpRequest
{
	pub is_https: bool,
	pub target: HostString,
	pub port: u16,
	pub method: AsciiString,
	pub path: PathString,		//Also includes query.
	pub headers: BTreeMap<AsciiString, AsciiString>,
	pub body: Option<Vec<u8>>,
}

impl Debug for HttpRequest
{
	 fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	 {
		writeln!(f, "========= REQUEST ========")?;
		writeln!(f, ">{desc}", desc=self.get_description())?;
		for (name, value) in self.headers.iter() { writeln!(f, ">Header: {name}: '{value}'")?; }
		if let &Some(ref body) = &self.body {
			writeln!(f, ">-------- Body --------")?;
			for line in safe_octets_to_text(body).lines() { writeln!(f, ">{line}")?; }
		}
		writeln!(f, "==========================")
	 }
}

impl HttpRequest
{
	pub fn from_url(url: &AsciiStr) -> Result<HttpRequest, String>
	{
		//https://<host>[:<port>][/<path>]
		let (scheme, host, port, path) = split_url(url, None).map_err(|_|{
			format!("Invalid URL {url}")
		})?;
		let defport = if scheme == "http" {
			80
		} else if scheme == "https" {
			443
		} else {
			fail!(format!("Unknown protocol in URL {url}"));
		};
		let port = port.unwrap_or(defport);

		Ok(HttpRequest{
			is_https: scheme == "https",
			target: host.to_owned(),
			port: port,
			method: A("GET"),		//Default.
			path: path.to_owned(),
			headers: BTreeMap::new(),
			body: None
		})
	}
	fn escape_host<'a>(host: &'a HostStr) -> Cow<'a, AsciiStr>
	{
		if host.chars().any(|x|x==':'||x=='/') {
			Cow::Owned(Ao(format!("[{host}]")))
		} else {
			Cow::Borrowed(host_string_to_ascii_string(host))
		}
	}
	pub fn get_description(&self) -> String
	{
		let defport = if self.is_https { 443 } else { 80 };
		let portspec = if self.port == defport { String::new() } else { format!(":{port}", port=self.port) };
		let pspec = if self.is_https { "https://" } else { "http://" };
		format!("{method} {pspec}{host}{portspec}{path}",
			method=self.method, host=Self::escape_host(&self.target), path=self.path)
	}
	pub fn connect_to(&self) -> (HostString, AsciiString)
	{
		(self.target.clone(), Ao(self.port.to_string()))
	}
}

pub struct DisplayCode<'a>(u16, &'a [u8]);

impl<'a> Display for DisplayCode<'a>
{
	 fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	 {
		write!(f, "{code}({reason})", code=self.0, reason=SafeOctetsToText(&self.1))
	 }
}

pub struct HttpReply
{
	pub version: Vec<u8>,
	pub code: u16,
	pub reason: Vec<u8>,
	pub headers: Vec<(StandardHttpHeader, Vec<u8>)>,
	pub body: Vec<u8>,
	pub is_close: bool,
}

impl HttpReply
{
	pub fn body_as_json(&self) -> Result<JSON, String>
	{
		from_utf8(&self.body).ok().and_then(|x|{
			JSON::parse(x).ok()
		}).ok_or_else(||{
			format!("Can't parse response as JSON (HTTP status: {code})", code=self.display_code())
		})
	}
	pub fn get_header(&self, name: StandardHttpHeader) -> Result<Option<AsciiString>, String>
	{
		let mut output = AsciiString::new();
		let mut found_one = false;
		for &(hname, ref hvalue) in self.headers.iter() {
			if hname == name {
				found_one = true;
				//Comma should be valid ASCII.
				if output.len() > 0 { output.push_str(An(",")); }
				let hstr = AsciiString::new_octets(&hvalue).map_err(|bad|{
					format!("Bad value '{bad}' for header '{name}'")
				})?;
				output.push_str(&hstr);
			}
		}
		Ok(if found_one { Some(output) } else { None })
	}
	pub fn is_success(&self) -> bool { self.code / 100 == 2 }
	pub fn is_error(&self) -> bool { self.code / 200 == 2 }
	pub fn display_code<'a>(&'a self) -> DisplayCode<'a> { DisplayCode(self.code, &self.reason) }
	pub fn is_redirect(&self) -> bool { match self.code { 301|302|303|307|308 => true, _ => false } }
	pub fn is_redirect_methodchanging(&self) -> bool { self.code == 303 }
	pub fn is_tempfail(&self) -> bool { matches!(self.code, 429|500..=599) }
	pub fn is_connection_close(&self) -> bool { self.is_close }
}

impl Debug for HttpReply
{
	 fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	 {
		writeln!(f, "========= REPLY ========")?;
		writeln!(f, "<Version: {version}", version=SafeOctetsToText(&self.version))?;
		writeln!(f, "<Status: {code}({reason})", code=self.code, reason=SafeOctetsToText(&self.reason))?;
		for (n, v) in self.headers.iter() {
			writeln!(f, "<Header: {name}: '{value}'", name=n.to_str(), value=SafeOctetsToText(&v))?;
		}
		writeln!(f, "<-------- Body --------")?;
		for line in safe_octets_to_text(&self.body).lines() { writeln!(f, "<{line}")?; }
		writeln!(f, "========================")
	 }
}

pub(crate) struct HttpPendingReply
{
	code: u16,
	reason: Vec<u8>,
	headers: Vec<(StandardHttpHeader, Vec<u8>)>,
	body: Vec<u8>,
	finished: bool,
	error: Option<String>,
	is_close: bool,
}

impl ElementSink for HttpPendingReply
{
	fn sink<'a>(&mut self, elem: Element<'a>)
	{
		match elem {
			Element::Data(d) => self.body.extend_from_slice(d),
			Element::ConnectionHeaderStd(StandardHttpHeader::Close) => self.is_close = true,
			Element::HeaderStd(n, v) => self.headers.push((n, v.to_owned())),
			Element::Status(c) => self.code = c,
			Element::Reason(r) => self.reason = r.to_owned(),
			Element::EndOfMessage => self.finished = true,
			Element::HeadersFaulty(fault) => self.error = Some(format!("Header error: {fault}")),
			Element::Abort(abort) => self.error = Some(format!("HTTP fault: {abort}")),
			_ => (),	//Just ignore all others.
		}
	}
	fn protocol_supported(&self, _: &[u8]) -> bool { false }
}

impl HttpPendingReply
{
	pub(crate) fn new() -> HttpPendingReply
	{
		HttpPendingReply {
			code: 0,
			reason: Vec::new(),
			headers: Vec::new(),
			body: Vec::new(),
			finished: false,
			error: None,
			is_close: false,
		}
	}
	pub(crate) fn set_error(&mut self, error: String)
	{
		if self.error.is_some() { return; }	//Don't overwrite error.
		self.error = Some(error);
	}
	pub(crate) fn get_error(&self) -> Option<String> { self.error.clone() }
	pub(crate) fn is_finished(&self) -> bool { self.finished }
	pub fn finish(self) -> Result<HttpReply, String>
	{
		Ok(HttpReply{
			version: "HTTP/1.1".to_owned().into_bytes(),
			code: self.code,
			reason: self.reason,
			headers: self.headers,
			body: self.body,
			is_close: self.is_close,
		})
	}
}

///HTTP Connection.
pub trait HttpConnection: Display
{
	fn send(&mut self, data: &[u8], deadline: Deadline) -> Result<(), TlsFault>;
	//Only calls cb with empty data on EOF.
	fn receive<T:Sized, F>(&mut self, deadline: Deadline, cb: F) -> Result<T, TlsFault> where F: FnMut(&[u8]) ->
		Result<T, String>;
	fn goodbye(&mut self);
}

pub(crate) struct VBuffer(Vec<u8>);

impl ReceiveBuffer for VBuffer
{
	fn new() -> Self { VBuffer(Vec::new()) }
	fn borrow<'a>(&'a mut self) -> &'a mut [u8] { &mut self.0 }
}

pub(crate) struct StreamSource<'a>(pub(crate) &'a [u8]);

impl<'a> Source for StreamSource<'a>
{
	type Buffer = VBuffer;
	type Error = ();	//Never fails.
	fn read(&mut self, buf: &mut VBuffer) -> Result<bool, ()>
	{
		buf.0 = self.0.to_owned();
		Ok(self.0.len() > 0)
	}
}

fn call<T:Sized,F>(f: F) -> T where F: FnOnce() -> T { f() }

struct HttpsConnection
{
	conn: Option<TlsConnection<TcpTransport>>,
	target: HostString,
	service: AsciiString,
	pile: TrustAnchorSet,
}

impl HttpsConnection
{
	fn __connect_tls(&self, debug: &Debugging, deadline: Deadline) ->
		Result<TlsConnection<TcpTransport>, TlsFault>
	{
		let &HttpsConnection{ref target, ref service, ref pile, ..} = self;
		TcpTransport::establish_connection(target, service, None, debug, deadline).and_then(|(conn, _)|{
			TlsConnection::from_raw_tcp(target, service, conn, pile, debug, deadline)
		})
	}
	fn do_https_request(&mut self, req: &HttpRequest, debug: &Debugging, deadline: Deadline, maxreq: Duration) ->
		Result<HttpReply, String>
	{
		let errmsg = format!("{desc}:", desc=req.get_description());
		let mut last_fail = false;
		const ATTEMPTS: usize = 5;
		for idx in 1..ATTEMPTS+1 {
			//Bound deadline per retry.
			fail_if!(deadline.expired(), format!("{errmsg} Deadline exceeded"));
			let deadline = deadline.derive(maxreq);
			if self.conn.is_none() || replace(&mut last_fail, false) {
				self.conn = match self.__connect_tls(debug, deadline) {
					Ok(x) => Some(x),
					Err(err) => {
						warning!(debug, "{errmsg} try#{idx}: {err}");
						continue;
					}
				};
			}
			if let Some(conn) = self.conn.as_mut() {
				match do_request_response(req, conn, debug, deadline) {
					Ok(ret) => {
						//Check for Connection: close. If present, the connection is NOT
						//healthy, immediately jettison it, so next request does not try
						//to reuse it.
						if ret.is_connection_close() { self.conn = None; }
						return Ok(ret);
					},
					Err(err) => {
						warning!(debug, "{errmsg} try#{idx}: {err}");
						//The connection is presumably NOT healthy, so replace it.
						last_fail = true;
						continue;
					}
				}
			}
		}
		//jettison failed connection, it is not healthy.
		if last_fail { self.conn = None; }
		Err(format!("{errmsg} All {ATTEMPTS} attempts failed"))
	}
}

struct TrustAnchorList
{
	set: BTreeSet<TrustAnchorOrdered>,
}

impl TrustAnchorList
{
	fn add(&mut self, ta_list: &BTreeSet<TrustAnchorOrdered>)
	{
		for ta in ta_list.iter() {
			self.set.insert(ta.clone());
		}
	}
}

static TRUST_ANCHORS: GlobalMutex<TrustAnchorTable> = GlobalMutex::new();

struct TrustAnchorTable
{
	entries: BTreeMap<HostString, TrustAnchorList>,
}

impl Default for TrustAnchorTable
{
	fn default() -> TrustAnchorTable
	{
		TrustAnchorTable {
			entries: BTreeMap::new(),
		}
	}
}

impl TrustAnchorTable
{
	fn __propagate_anchors(&mut self, from: &HostStr, to: &HostStr)
	{
		if let Some(entry) = self.entries.get(from) {
			let list = entry.set.clone();
			self.__add_anchors(to, &list);
		}
	}
	fn __lookup_anchors(&self, host: &HostStr) -> TrustAnchorSet
	{
		if let Some(entry) = self.entries.get(host) {
			TrustAnchorSet::from_set(&entry.set)
		} else {
			TrustAnchorSet::new()
		}
	}
	fn __add_anchors(&mut self, host: &HostStr, ta_list: &BTreeSet<TrustAnchorOrdered>)
	{
		if let Some(to) = self.entries.get_mut(host) {
			to.add(ta_list);
			return;
		}
		let mut t = TrustAnchorList {
			set: BTreeSet::new(),
		};
		t.add(ta_list);
		self.entries.insert(host.to_owned(), t);
	}
	fn propagate_anchors(from: &HostStr, to: &HostStr)
	{
		TRUST_ANCHORS.with(|ta|{
			ta.__propagate_anchors(from, to)
		})
	}
	fn lookup_anchors(host: &HostStr) -> TrustAnchorSet
	{
		TRUST_ANCHORS.with(|ta|{
			ta.__lookup_anchors(host)
		})
	}
	fn add_anchors(host: &HostStr, ta_list: &Rootpile)
	{
		TRUST_ANCHORS.with(|ta|{
			ta.__add_anchors(host, ta_list.borrow_inner())
		})
	}
}

struct ConnectionTable
{
	entries: BTreeMap<(HostString, u16), HttpsConnection>,
}

impl ConnectionTable
{
	fn new() -> ConnectionTable
	{
		ConnectionTable {
			entries: BTreeMap::new(),
		}
	}
	fn __with_connection<T>(&mut self, host: &HostStr, port: u16, f: impl FnOnce(&mut HttpsConnection) -> T) -> T
	{
		let key = (host.to_owned(), port);
		if let Some(conn) = self.entries.get_mut(&key) {
			return f(conn);
		}
		//There is no entry. Create one.
		let mut conn = HttpsConnection {
			conn: None,	//Not established.
			target: host.to_owned(),
			service: Ao(format!("{port}")),
			pile: TrustAnchorTable::lookup_anchors(host),
		};
		let t = f(&mut conn);
		self.entries.insert(key, conn);
		t
	}
	fn with_connection<T>(host: &HostStr, port: u16, f: impl FnOnce(&mut HttpsConnection) -> T) -> T
	{
		CONNS_TABLE.with(|c|{
			c.borrow_mut().__with_connection(host, port, f)
		})
	}
}

thread_local!(static CONNS_TABLE: RefCell<ConnectionTable> = RefCell::new(ConnectionTable::new()));

pub fn add_trust_anchors(host: &HostStr, ta_list: &Rootpile)
{
	TrustAnchorTable::add_anchors(host, ta_list)
}

pub fn propagate_trust_anchors(from: &HostStr, to: &HostStr)
{
	TrustAnchorTable::propagate_anchors(from, to);
}

///Do HTTPS request and response.
pub fn do_https_request(req: &HttpRequest, debug: &Debugging, deadline: Deadline, maxreq: Duration) ->
	Result<HttpReply, String>
{
	let host = &req.target;
	let port = req.port;
	ConnectionTable::with_connection(host, port, |conn|{
		conn.do_https_request(req, debug, deadline, maxreq)
	})
}

fn do_request_response<Conn:HttpConnection>(req: &HttpRequest, connection: &mut Conn, debug: &Debugging,
	deadline: Deadline) -> Result<HttpReply, String>
{
	let connname = connection.to_string();

	let request = call(||{
		if false { return Err(()); }	//Type inference.
		let authority = match (req.is_https, req.port) {
			(false, 80) => Cow::Borrowed(req.target.as_bytes()),
			(true, 443) => Cow::Borrowed(req.target.as_bytes()),
			(_, port) => Cow::Owned(format!("{tgt}:{port}", tgt=req.target).into_bytes()),
		};
		let (path, query) = split_attach_first(req.path.as_bytes(), b"?", Attachment::Right).
			unwrap_or((req.path.as_bytes(), b""));

		let mut request = Vec::new();
		HeaderEncoder::request_line(&mut request, req.method.as_bytes(), b"", authority.deref(), path,
			query)?;
		for (n, v) in req.headers.iter() {
			HeaderEncoder::header(&mut request, n.as_bytes(), v.as_bytes())?;
		}
		if let &Some(ref body) = &req.body {
			HeaderEncoder::header(&mut request, b"content-length", body.len().to_string().as_bytes())?;
			HeaderEncoder::end(&mut request)?;
			request.extend_from_slice(body);
		} else {
			HeaderEncoder::end(&mut request)?;
		}
		Ok(request)
	}).set_err("Internal error: Illegal request to be sent")?;

	debug!(debug, DEBUG_HTTP_SUMMARY, "{connname}: HTTP {desc}...", desc=req.get_description());
	debug!(debug, DEBUG_HTTP_MESSAGES, "{req:?}");
	connection.send(&request, deadline).map_err(|err|{
		err.to_string()
	})?;
	debug!(debug, DEBUG_HTTP_TRANSFER, "{connname}: HTTP {desc}: Sent request, waiting for reply...",
		desc=req.get_description());
	let mut http_reply = HttpPendingReply::new();
	let mut http_stream = Stream::new(b"foo");	//The default scheme is irrelevant.
	http_stream.set_use_std_flag();		//Use standard headers.
	http_stream.set_reply(&req.method == "HEAD");
	let mut freply = None;
	loop {
		match connection.receive(deadline, |data|{
			debug!(debug, DEBUG_HTTP_TRANSFER, "{connname}: HTTP {desc}: Received {len} bytes...",
				desc=req.get_description(), len=data.len());
			let mut src = StreamSource(data);
			match http_stream.push(&mut src, &mut http_reply) {
				Ok(_) => (),
				Err(IoOrStreamError::Io(_)) => fail!(format!("Internal Error: StreamSource failed")),
				Err(IoOrStreamError::Stream(err)) =>
					http_reply.set_error(format!("HTTP stream error: {err}")),
			};
			if let Some(err) = http_reply.get_error() { fail!(err); }
			if http_reply.finished {
				//Finished a reply.
				let mut completed = HttpPendingReply::new();
				swap(&mut completed, &mut http_reply);
				freply = Some(completed.finish()?);
				return Ok(true);	//Finished a reply.
			} else if data.len() == 0 {
				//Nasty edge case: If data.len() == 0 and stream is idle, then the request
				//succeeds, leading to busylooping. Fail request to trigger retry.
				fail!("Unexpected connection close");
			}
			Ok(false)
		}) {
			Ok(true) => break,
			Ok(false) => (),
			Err(err) => fail!(err.to_string())
		};
	}
	if let Some(freply) = freply.as_ref() {
		debug!(debug, DEBUG_HTTP_MESSAGES, "{freply:?}");
		debug!(debug, DEBUG_HTTP_SUMMARY, "{connection}: HTTP {desc} done, status={code}",
			desc=req.get_description(), code=freply.display_code());
	}
	freply.ok_or_else(||{
		format!("Connection prematurely closed on {connection}")
	})
}

pub fn kill_all_connections()
{
	CONNS_TABLE.with(|c|{
		c.borrow_mut().entries.clear();
	})
}

#[test]
fn test_from_url()
{
	//Base case.
	assert_eq!(&HttpRequest::from_url(An("http://foo.bar/")).unwrap().get_description(),
		"GET http://foo.bar/");
	//With path
	assert_eq!(&HttpRequest::from_url(An("http://foo.bar/baz/zot")).unwrap().get_description(),
		"GET http://foo.bar/baz/zot");
	//Missing /
	assert_eq!(&HttpRequest::from_url(An("http://foo.bar")).unwrap().get_description(),
		"GET http://foo.bar/");
	//Redundant port
	assert_eq!(&HttpRequest::from_url(An("http://foo.bar:80/")).unwrap().get_description(),
		"GET http://foo.bar/");
	//Redundant port, Missing /
	assert_eq!(&HttpRequest::from_url(An("http://foo.bar:80")).unwrap().get_description(),
		"GET http://foo.bar/");
	//Non-default port
	assert_eq!(&HttpRequest::from_url(An("http://foo.bar:81/")).unwrap().get_description(),
		"GET http://foo.bar:81/");
	//Non-default, Missing /
	assert_eq!(&HttpRequest::from_url(An("http://foo.bar:81")).unwrap().get_description(),
		"GET http://foo.bar:81/");

	//Base case.
	assert_eq!(&HttpRequest::from_url(An("https://foo.bar/")).unwrap().get_description(),
		"GET https://foo.bar/");
	//With path
	assert_eq!(&HttpRequest::from_url(An("https://foo.bar/baz/zot")).unwrap().get_description(),
		"GET https://foo.bar/baz/zot");
	//Missing /
	assert_eq!(&HttpRequest::from_url(An("https://foo.bar")).unwrap().get_description(),
		"GET https://foo.bar/");
	//Redundant port
	assert_eq!(&HttpRequest::from_url(An("https://foo.bar:443/")).unwrap().get_description(),
		"GET https://foo.bar/");
	//Redundant port, Missing /
	assert_eq!(&HttpRequest::from_url(An("https://foo.bar:443")).unwrap().get_description(),
		"GET https://foo.bar/");
	//Non-default port
	assert_eq!(&HttpRequest::from_url(An("https://foo.bar:444/")).unwrap().get_description(),
		"GET https://foo.bar:444/");
	//Non-default, Missing /
	assert_eq!(&HttpRequest::from_url(An("https://foo.bar:444")).unwrap().get_description(),
		"GET https://foo.bar:444/");
	//Enclosed address
	assert_eq!(&HttpRequest::from_url(An("https://[foo_bar]/")).unwrap().get_description(),
		"GET https://foo_bar/");
	//Enclosed address with port
	assert_eq!(&HttpRequest::from_url(An("https://[foo_bar]:444/")).unwrap().get_description(),
		"GET https://foo_bar:444/");
	//Enclosed address without path
	assert_eq!(&HttpRequest::from_url(An("https://[foo_bar]")).unwrap().get_description(),
		"GET https://foo_bar/");
	//Enclosed address with port without path
	assert_eq!(&HttpRequest::from_url(An("https://[foo_bar]:444")).unwrap().get_description(),
		"GET https://foo_bar:444/");
	//IPv6 address
	assert_eq!(&HttpRequest::from_url(An("https://[2001:db8::1]/")).unwrap().get_description(),
		"GET https://[2001:db8::1]/");

	//Bad paths.
	assert!(HttpRequest::from_url(An("http://")).is_err());
	assert!(HttpRequest::from_url(An("https://")).is_err());
	assert!(HttpRequest::from_url(An("https://foo.bar:444x")).is_err());
	assert!(HttpRequest::from_url(An("https://foo.bar:/444x")).is_err());
	assert!(HttpRequest::from_url(An("https://[foo.bar]:444x")).is_err());
	assert!(HttpRequest::from_url(An("https://[foo.bar]:/444x")).is_err());
	assert!(HttpRequest::from_url(An("https://[foo.bar]x")).is_err());
	assert!(HttpRequest::from_url(An("https://[foo/bar]/x")).is_err());
}
