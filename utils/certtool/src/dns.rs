use crate::An;
use crate::deadline::Deadline;
use crate::debug::Debugging;
use crate::debug::DEBUG_DNS_ADDRESSES;
use crate::debug::DEBUG_DNS_CACHE;
use crate::debug::DEBUG_DNS_MSG_S;
use crate::debug::DEBUG_DNS_RAWMSG;
use crate::debug::DEBUG_DNS_RECORDS;
use crate::http::HttpConnection;
use crate::httpget::Connection;
use crate::strings::HostStr;
use crate::tls::TrustAnchor;
use crate::tls::TrustAnchorSet;
use crate::transport::is_fatal;
use crate::transport::TcpTransport;
use crate::transport::Transport;
use btls::utility::GlobalMutex;
pub use btls_aux_dnsmsg::Class as DnsClass;
use btls_aux_dnsmsg::ClassName as DnsNamePrinterC;
use btls_aux_dnsmsg::ClassName0x20 as DnsNamePrinter;
use btls_aux_dnsmsg::ClassNamePtr as DnsClassNamePtr;
use btls_aux_dnsmsg::dns_format_query;
use btls_aux_dnsmsg::Dns0x20;
use btls_aux_dnsmsg::Header as DnsHeader;
use btls_aux_dnsmsg::Message as DnsMessage;
use btls_aux_dnsmsg::MessageParseError;
use btls_aux_dnsmsg::MessageSection as DnsMessageSection;
use btls_aux_dnsmsg::Name as DnsNameRef;
use btls_aux_dnsmsg::NameBuf as DnsName;
use btls_aux_dnsmsg::NamePtr as NamePointer;
use btls_aux_dnsmsg::Query as DnsQuery;
use btls_aux_dnsmsg::Rdata as Rrdata;
use btls_aux_dnsmsg::Record;
pub use btls_aux_dnsmsg::Type as DnsType;
use btls_aux_dnsmsg::consts::DNSCLASS_ANY;
use btls_aux_dnsmsg::consts::DNSTYPE_A;
use btls_aux_dnsmsg::consts::DNSTYPE_AAAA;
use btls_aux_dnsmsg::consts::DNSTYPE_ANY;
use btls_aux_dnsmsg::consts::DNSTYPE_CNAME;
use btls_aux_dnsmsg::consts::DNSTYPE_NS;
use btls_aux_dnsmsg::consts::DNSTYPE_OPT;
use btls_aux_dnsmsg::consts::DNSTYPE_SOA;
use btls_aux_fail::f_return;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::fail_if_none;
use btls_aux_fail::ResultExt;
use btls_aux_memory::ByteStringLines;
use btls_aux_memory::Hexdump;
use btls_aux_memory::trim_ascii;
use btls_aux_memory::SmallVec;
use btls_aux_memory::SafeShowByteString;
use btls_aux_memory::SafeShowString;
use btls_aux_random::secure_random;
use btls_aux_unix::Address;
use btls_aux_unix::AddressIpv4;
use btls_aux_unix::AddressIpv6;
use btls_aux_unix::FileDescriptor;
use btls_aux_unix::SocketFamily;
use btls_aux_unix::SocketFlags;
use btls_aux_unix::SocketType;
use std::borrow::Cow;
use std::cell::RefCell;
use std::cmp::max;
use std::collections::BTreeMap;
use std::env::var;
use std::env::var_os;
use std::fmt::Display;
use std::fs::File;
use std::io::Read;
use std::io::Error as IoError;
use std::mem::drop;
use std::mem::replace;
use std::net::IpAddr;
use std::net::Ipv4Addr;
use std::net::Ipv6Addr;
use std::net::SocketAddr;
use std::ops::Deref;
use std::ptr::null_mut;
use std::str::from_utf8;
use std::str::FromStr;
use std::sync::atomic::AtomicPtr;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering as MemOrder;
use std::time::Duration;
use std::time::Instant;

static TMPFAIL: &'static str = "Temporary failure in name resolution";

//Cached DNS record data.
#[derive(Clone,Debug)]
struct CachedDnsRecord
{
	expiry: Instant,
	data: Vec<Vec<u8>>,
}

impl CachedDnsRecord
{
	fn new(ttl: u32, data: Vec<Vec<u8>>) -> CachedDnsRecord
	{
		CachedDnsRecord {
			expiry: Instant::now() + Duration::from_secs(ttl as u64),
			data: data,
		}
	}
	fn lookup(&self) -> Option<Vec<Vec<u8>>>
	{
		if Instant::now() <= self.expiry { Some(self.data.clone()) } else { None }
	}
	fn among(&self, record: &[u8]) -> bool
	{
		if Instant::now() > self.expiry { return false; }	//Expired.
		self.data.iter().any(|r|r.deref() == record)
	}
}

enum HostCacheResponse
{
	Ok(Vec<Vec<u8>>),
	Redirect(DnsClass, DnsName),
	HostDoesNotExist,
	NotFoundInCache,
}

#[derive(Clone,Debug)]
enum HostCacheState
{
	Nxdomain(Instant),				//Expiry.
	Cname(Instant, DnsClass, DnsName),		//(Expiry, class, target)
	Records(BTreeMap<DnsType, CachedDnsRecord>),	//records.
}

//Cached DNS record data.
#[derive(Clone,Debug)]
struct CachedDnsHost
{
	preferred: Option<IpAddr>,
	state: HostCacheState,
}

macro_rules! addr_is_valid
{
	($records:expr, $rrtype:expr, $y:expr) => {
		match $records.get(&$rrtype) {
			Some(rrset) => rrset.among(&$y.octets()),
			None => false
		}
	}
}

impl CachedDnsHost
{
	fn new() -> CachedDnsHost
	{
		CachedDnsHost {
			preferred: None,
			state: HostCacheState::Records(BTreeMap::new()),
		}
	}
	fn set_preferred_address(&mut self, addr: Option<IpAddr>, name: impl Display, debug: &Debugging)
	{
		//Since preferred address is validated on use, just accept anything.
		match addr {
			Some(addr) => debug!(debug, DEBUG_DNS_CACHE,
				"Setting preferred address of {name} to {addr}"),
			None => debug!(debug, DEBUG_DNS_CACHE, "Clearing preferred address of {name}"),
		}
		self.preferred = addr;
	}
	fn get_preferred_address(&self, name: impl Display, debug: &Debugging) -> Option<IpAddr>
	{
		//Check that the address is still in cache..
		let ok = if let &HostCacheState::Records(ref records) = &self.state {
			match self.preferred {
				Some(IpAddr::V4(y)) => addr_is_valid!(records, &DNSTYPE_A, y),
				Some(IpAddr::V6(y)) => addr_is_valid!(records, &DNSTYPE_AAAA, y),
				None => false
			}
		} else {
			false
		};
		let addr = if ok { self.preferred } else { None };
		match addr {
			Some(addr) => debug!(debug, DEBUG_DNS_CACHE, "Preferred address of {name} is {addr}"),
			None => debug!(debug, DEBUG_DNS_CACHE, "No preferred address for {name}"),
		}
		addr
	}
	fn got_nxdomain(&mut self, ttl: u32, name: impl Display, debug: &Debugging) -> HostCacheResponse
	{
		debug!(debug, DEBUG_DNS_CACHE, "Storing NXDOMAIN for {name} for {ttl} seconds");
		//Clear all records, as this host does not exist.
		self.state = HostCacheState::Nxdomain(Instant::now() + Duration::from_secs(ttl as u64));
		HostCacheResponse::HostDoesNotExist
	}
	fn got_cname(&mut self, ttl: u32, tclass: DnsClass, target: DnsName, name: impl Display, debug: &Debugging)
	{
		debug!(debug, DEBUG_DNS_CACHE, "Storing CNAME {name} -> {target} for {ttl} seconds",
			target=DnsNamePrinterC(tclass, &target));
		self.state = HostCacheState::Cname(Instant::now() + Duration::from_secs(ttl as u64),
			tclass, target.clone());
	}
	fn store_record(&mut self, rrtype: DnsType, ttl: u32, data: Vec<Vec<u8>>, name: impl Display,
		debug: &Debugging) -> HostCacheResponse
	{
		debug!(debug, DEBUG_DNS_CACHE, "Storing {count} {rrtype} record(s) for {name} for {ttl} seconds",
			count=data.len());
		let record = CachedDnsRecord::new(ttl, data.clone());
		//No need to do anything with preferred address, as it is checked on each use.
		if let &mut HostCacheState::Records(ref mut records) = &mut self.state {
			records.insert(rrtype, record);
			return HostCacheResponse::Ok(data)
		}
		//Not already in records state, store the record.
		let mut records = BTreeMap::new();
		records.insert(rrtype, record);
		self.state = HostCacheState::Records(records);
		HostCacheResponse::Ok(data)
	}
	fn follow_cname(&self) -> Option<(DnsClass,DnsName)>
	{
		match &self.state {
			&HostCacheState::Cname(expiry, clazz, ref target) if Instant::now() <= expiry =>
				Some((clazz, target.clone())),
			_ => None,	//Not CNAME or expired.
		}
	}
	fn lookup_record(&self, rrtype: DnsType, name: impl Display, debug: &Debugging) -> HostCacheResponse
	{
		match &self.state {
			&HostCacheState::Nxdomain(expiry) => if Instant::now() <= expiry {
				debug!(debug, DEBUG_DNS_CACHE, "Lookup name {name} does not exist");
				return HostCacheResponse::HostDoesNotExist;
			},
			&HostCacheState::Cname(expiry, tclass, ref target) => if Instant::now() <= expiry {
				return if rrtype == DNSTYPE_CNAME {
					debug!(debug, DEBUG_DNS_CACHE, "Returning CNAME record for {name}");
					HostCacheResponse::Ok(vec![target.as_raw().to_owned()])
				} else {
					debug!(debug, DEBUG_DNS_CACHE, "Lookup name {name} redirects to {target}",
						target=DnsNamePrinterC(tclass, target));
					HostCacheResponse::Redirect(tclass, target.clone())
				};
			},
			&HostCacheState::Records(ref records) => {
				if let Some(record) = records.get(&rrtype).and_then(|record|record.lookup()) {
					debug!(debug, DEBUG_DNS_CACHE, "Returning {count} {rrtype} records for {name}",
						count=record.len());
					return HostCacheResponse::Ok(record)
				}
			}
		}
		debug!(debug, DEBUG_DNS_CACHE, "Cache missed {rrtype} records for {name}");
		HostCacheResponse::NotFoundInCache
	}
}

//Cached DNS data.
#[derive(Clone,Debug,Default)]
struct CachedDns
{
	hosts: BTreeMap<DnsClass, BTreeMap<DnsName, CachedDnsHost>>
}

impl CachedDns
{
	fn call_with_created_entry<R>(&mut self, clazz: DnsClass, host: &DnsNameRef,
		f: impl FnOnce(DnsNamePrinterC, &mut CachedDnsHost) -> R) -> R
	{
		let name = DnsNamePrinterC(clazz, host);
		if let Some(clazz) = self.hosts.get_mut(&clazz) {
			if let Some(host) = clazz.get_mut(host) {
				return f(name,host);
			}
			return f(name,clazz.entry(host.to_owned()).or_insert(CachedDnsHost::new()));
		}
		f(name,self.hosts.entry(clazz).or_insert(BTreeMap::new()).entry(host.to_owned()).
			or_insert(CachedDnsHost::new()))
	}
	fn call_with_existing_entry<R>(&self, clazz: DnsClass, host: &DnsNameRef,
		f: impl FnOnce(&CachedDnsHost) -> R) -> Option<R>
	{
		if let Some(clazz) = self.hosts.get(&clazz) {
			if let Some(host) = clazz.get(host) {
				return Some(f(host));
			}
		}
		None
	}
	fn chase_cname<'a>(&self, clazz: DnsClass, host: &'a DnsNameRef, debug: &Debugging) ->
		(DnsClass, Cow<'a, DnsNameRef>)
	{
		let mut host = Cow::Borrowed(host);
		let mut clazz = clazz;
		let mut cnames = 0;
		loop {
			//For safety, cut chase if too many CNAMEs.
			if cnames > 10 { break; }
			cnames += 1;
			if let Some(Some((nclazz, nhost))) = self.call_with_existing_entry(clazz, &host,
				|h|h.follow_cname()) {
				//Got CNAME, follow it.
				let old = DnsNamePrinterC(clazz, &host);
				let new = DnsNamePrinterC(nclazz, &nhost);
				debug!(debug, DEBUG_DNS_CACHE, "Lookup name {old} redirects to {new}");
				clazz = nclazz;
				host = Cow::Owned(nhost);
			} else {
				//Not a CNAME, cut chase.
				break;
			}
		}
		(clazz, host)
	}
	fn set_preferred_address(&mut self, clazz: DnsClass, host: &DnsNameRef, addr: Option<IpAddr>,
		debug: &Debugging)
	{
		//Chase CNAMEs, as aliases should inherit the preferred address, and the preferred address should
		//always be considered valid for aliases.
		let (clazz, host) = self.chase_cname(clazz, host, debug);
		//Create address if host does not exist.
		self.call_with_created_entry(clazz, &host, |name,h|h.set_preferred_address(addr, name, debug))
	}
	fn get_preferred_address(&self, clazz: DnsClass, host: &DnsNameRef, debug: &Debugging) -> Option<IpAddr>
	{
		let name = DnsNamePrinterC(clazz, host);
		//Chase CNAMEs, as aliases should inherit the preferred address, and the preferred address should
		//always be considered valid for aliases.
		let (clazz, host) = self.chase_cname(clazz, host, debug);
		//Non-cached host never has preferred address.
		if let Some(Some(ans)) = self.call_with_existing_entry(clazz, &host,
			|h|h.get_preferred_address(name, debug)) {
			return Some(ans);
		}
		debug!(debug, DEBUG_DNS_CACHE, "Cache missed for {name}");
		None
	}
	fn got_nxdomain(&mut self, clazz: DnsClass, host: &DnsNameRef, ttl: u32, debug: &Debugging) ->
		HostCacheResponse
	{
		self.call_with_created_entry(clazz, host, |name,h|h.got_nxdomain(ttl, name, debug))
	}
	fn got_cname(&mut self, clazz: DnsClass, host: &DnsNameRef, ttl: u32, target: DnsName,
		debug: &Debugging)
	{
		let tclass = clazz;	//CNAMEs are always within class.
		self.call_with_created_entry(clazz, host, |name,h|h.got_cname(ttl, tclass, target, name, debug))
	}
	fn store_record(&mut self, clazz: DnsClass, host: &DnsNameRef, rrtype: DnsType, mut ttl: u32,
		data: Vec<Vec<u8>>, debug: &Debugging) -> HostCacheResponse
	{
		//Force TTLs on address records to be at least 5 minutes, since many places set insanely short
		//lifetimes.
		if rrtype == DNSTYPE_A || rrtype == DNSTYPE_AAAA { ttl = max(ttl, 300); }
		self.call_with_created_entry(clazz, host, |name,h|h.store_record(rrtype, ttl, data, name, debug))
	}
	fn lookup_record(&self, clazz: DnsClass, host: &DnsNameRef, rrtype: DnsType, debug: &Debugging) ->
		HostCacheResponse
	{
		let name = DnsNamePrinterC(clazz, host);
		//Non-cached host is always not found in cache..
		if let Some(ans) = self.call_with_existing_entry(clazz, host,
			|h|h.lookup_record(rrtype, name, debug)) {
			return ans;
		}
		debug!(debug, DEBUG_DNS_CACHE, "Cache missed for {name}");
		HostCacheResponse::NotFoundInCache
	}
}

static DNS_CACHE: GlobalMutex<CachedDns> = GlobalMutex::new();
thread_local!(static CONN_CACHE: RefCell<Option<Connection<TcpTransport>>> = RefCell::new(None));

fn with_conn_cache<T>(f: impl FnOnce(&mut Option<Connection<TcpTransport>>) -> T) -> T
{
	CONN_CACHE.with(|cc|{
		f(&mut cc.borrow_mut())
	})
}

fn print_record(debug: &Debugging, owner: NamePointer, rrtype: DnsType, clazz: DnsClass, ttl: u32, data: &Rrdata,
	kind: DnsMessageSection)
{
	let rkind = match kind {
		DnsMessageSection::Query => "Query",
		DnsMessageSection::Answer => "Answer",
		DnsMessageSection::Authority => "Authority",
		DnsMessageSection::Additional => "Additional",
	};
	if kind == DnsMessageSection::Additional && owner.is_root() && rrtype == DNSTYPE_OPT {
		let udpmax = clazz.0;
		let ext_rcode = ttl >> 24;
		let edns_ver = (ttl >> 16) & 0xFF;
		let flags = ttl & 0xFFFF;
		debug!(debug, DEBUG_DNS_RECORDS, "EDNS: udpmax={udpmax}, ext-rcode={ext_rcode}, \
			edns-ver={edns_ver}, flags={flags:04x}: tlvs={data}");
	} else if kind == DnsMessageSection::Query {
		debug!(debug, DEBUG_DNS_RECORDS, "{rkind} {clazz}:{owner} {rrtype}");
	} else {
		debug!(debug, DEBUG_DNS_RECORDS, "{rkind} {clazz}:{owner} {ttl} {data}");
	}
}

enum DnsQueryResponseTailResult
{
	Ok(Vec<Vec<u8>>),
	Nxdomain,
	ParseError(String),
}

fn check_response_query<'a>(header: DnsHeader, mut query: impl Iterator<Item=DnsQuery<'a>>, name: &DnsNameRef,
	clazz: DnsClass, rrtype: DnsType, dns0x20: &Dns0x20) -> Result<(), String>
{
	let pname = DnsNamePrinter(clazz, name, dns0x20);
	let horked = "Dude, your recursive nameserver is horked";
	//Check if query matches.
	fail_if!(!header.qr, format!("{horked} (got query as response)"));
	fail_if!(header.opcode != 0, format!("{horked} (Got OPCODE {opc} for query)", opc=header.opcode));
	match header.rcode {
		0|3 => (),	//NOERROR / NXDOMAIN.
		4 => fail!(format!("{horked} (got NOTIMP for QUERY)")),
		5 => fail!("Permission denied trying to use DNS recursive resolver"),
		rcode => fail!(format!("{pname} {rrtype}: Query failed with error code {rcode}"))
	}
	let query = match query.next() {
		Some(q) => match query.next() { Some(_) => Err(2), None => Ok(q) },
		None => Err(0)
	};
	let query = match query {
		Ok(query) => query,
		Err(cnt) => fail!(format!("{pname} {rrtype}: Expected 1 QUERY in response, got {cnt}"))
	};
	//This comparision is strict with case in order to take advantage of 0x20 randomization.
	let qname = DnsClassNamePtr(query.qclass, query.qname);
	fail_if!(!query.qname.equals_strict(name, dns0x20) || query.qclass != clazz || query.qtype != rrtype,
		format!("Got wrong QUERY in response: {qname} {qtype} != {pname} {rrtype}", qtype=query.qtype));
	Ok(())
}

fn to_master_vec(name: &NamePointer) -> Vec<u8>
{
	let mut mname = Vec::new();
	name.decompress_as_master(&mut mname).ok();	//Can't fail.
	//Lowercase the name.
	for b in mname.iter_mut() { if *b >= 65 && *b <= 90 { *b += 32; } }
	//If name is empty, that means root. Translate to usual root .
	if mname.len() == 0 { mname.push(b'.'); }
	mname
}

fn dns_find_related_tail(rbuf: &[u8], name: &DnsNameRef, clazz: DnsClass, rrtype: DnsType, kind: RelatedNameKind,
	dns0x20: &Dns0x20, addr: SocketAddr, debug: &Debugging) -> Result<Option<Vec<u8>>, String>
{
	let pname = DnsNamePrinter(clazz, name, dns0x20);
	let response = f_return!(DnsMessage::decode(rbuf,|a,b,c,d,e,f|print_record(debug,a,b,c,d,e,f)), F[|err|{
		warning!(debug, "Bad response from {addr} for {pname} {rrtype}: {err}");
		//Assume the DNS nameserver is horked, try another.
		Ok(None)
	}]);
	check_response_query(response.header(), response.question(), name, clazz, rrtype, dns0x20)?;

	//When finding the containing zone, it does not matter if RCODE is NOERROR or NXDOMAIN. Also,
	//check_response_query rejects all errors not NOERROR/NXDOMAIN, so rcode > 0 is NXDOMAIN.
	let find_apex = kind == RelatedNameKind::ZoneRoot;
	fail_if!(response.header().rcode > 0 && !find_apex, format!("Name does not exist"));

	let mut answer = None;
	for record in response.answer() {
		//Ignore wrong class and wrong owner.
		if record.clazz != clazz || record.owner != name { continue; }
		//Check record type.
		fail_if!(record.rtype != rrtype, format!("Wrong RRTYPE {rtype} in response", rtype=record.rtype));
		//There can be only one.
		fail_if!(answer.is_some(), "Unsupported multiple responses");
		//If find_apex, the answer is NS owner. In other cases, need to peek into the record.
		answer = Some(if find_apex {
			to_master_vec(&record.owner)
		} else if let Rrdata::SName{ref target,..} = record.data {
			to_master_vec(target)
		} else if let Rrdata::Other{data,..} = record.data {
			fail_if!(rrtype == DNSTYPE_A && data.len() != 4, "Bad IPv4 address".to_string());
			fail_if!(rrtype == DNSTYPE_AAAA && data.len() != 16, "Bad IPv6 address".to_string());
			data.to_owned()
		} else {
			fail!("Internal error: Bad record kind".to_string());
		});
	}
	if let Some(answer) = answer.take() { return Ok(Some(answer)); }
	if find_apex {
		//No NS record was found. Use owner of SOA from authority section.
		for record in response.authority() {
			//Ignore wrong class and not SOA.
			if record.clazz != clazz || record.rtype != DNSTYPE_SOA { continue; }
			//There can be only one.
			fail_if!(answer.is_some(), "Unsupported multiple responses");
			answer = Some(to_master_vec(&record.owner));
		}
		if let Some(answer) = answer.take() { return Ok(Some(answer)); }
	}
	//Can not determine the zone root. Note that failing on address is special.
	let ktext = match kind {
		RelatedNameKind::ZoneRoot => "zone root",
		RelatedNameKind::Canonical => "canonical name",
		RelatedNameKind::Nameserver => "nameserver",
		RelatedNameKind::Ipv4Address => return Ok(Some(Vec::new())),
		RelatedNameKind::Ipv6Address => return Ok(Some(Vec::new())),
	};
	Err(format!("Failed to determine {ktext}"))
}

struct NameInfo
{
	cname: Option<(u32, DnsName)>,
	other: BTreeMap<DnsType, (u32, Vec<Vec<u8>>)>,
}

impl NameInfo
{
	fn new() -> NameInfo
	{
		NameInfo {
			cname: None,
			other: BTreeMap::new(),
		}
	}
}

fn with_nameinfo_created<R>(map: &mut BTreeMap<DnsClass, BTreeMap<DnsName, NameInfo>>, clazz: DnsClass,
	name: &DnsNameRef, f: impl FnOnce(&mut NameInfo) -> R) -> R
{
	if let Some(clazz) = map.get_mut(&clazz) {
		if let Some(name) = clazz.get_mut(name) {
			return f(name);
		}
		return f(clazz.entry(name.to_owned()).or_insert(NameInfo::new()));
	}
	f(map.entry(clazz).or_insert(BTreeMap::new()).entry(name.to_owned()).or_insert(NameInfo::new()))
}

fn cache_records<'a>(answers: impl Iterator<Item=Record<'a>>, debug: &Debugging) -> Result<(), String>
{
	let mut ninfo: BTreeMap<DnsClass, BTreeMap<DnsName, NameInfo>> = BTreeMap::new();
	for i in answers {
		let clazz = i.clazz;
		let owner = i.owner.decompress_owned();
		let rrtype = i.rtype;
		let rrdata = i.data;
		let ttl = i.ttl;
		with_nameinfo_created(&mut ninfo, clazz, &owner, |entry|{
			if false { return Err(String::new()); }		//Type inference.
			if let Rrdata::SName{rtype:DNSTYPE_CNAME, ref target} = rrdata {
				//Handle CNAMEs specially.
				fail_if!(entry.cname.is_some(), format!("Got Multiple CNAMEs for {owner}",
					owner=DnsNamePrinterC(clazz, &owner)));
				entry.cname = Some((ttl, target.decompress_owned()));
			} else {
				//Ignore some DNSSEC specials. These can be present with CNAMEs.
				if rrtype.can_coexist_with_cname() { return Ok(()); }
				let mut raw_rrdata = Vec::new();
				rrdata.uncompress(&mut raw_rrdata).ok();	//Can't fail.
				//If rrtype has already been encountered, add the new record to rrset.
				if let Some(&mut (ottl, ref mut vector)) = entry.other.get_mut(&rrtype) {
					fail_if!(ottl != ttl, format!("Inconsistent TTLS for {owner} {rrtype}",
						owner=DnsNamePrinterC(clazz, &owner)));
					vector.push(raw_rrdata);
					return Ok(());
				}
				//Not found. Add for first time.
				entry.other.insert(rrtype, (ttl, vec![raw_rrdata]));
			}
			Ok(())
		})?;
	}
	for (&clazz, ref mut classentry) in ninfo.iter_mut() {
		for (ref owner, ref mut nentry) in classentry.iter_mut() {
			fail_if!(nentry.cname.is_some() && nentry.other.len() > 0,
				format!("Contradictionary CNAME and other entries for {owner}",
				owner=DnsNamePrinterC(clazz, owner)));
			if let Some(&mut (ttl, ref mut cname)) = nentry.cname.as_mut() {
				let cname = replace(cname, DnsName::root());
				DNS_CACHE.with(|cache|cache.got_cname(clazz, owner, ttl, cname, debug));
			}
			for (&rrtype, &mut (ttl, ref mut records)) in nentry.other.iter_mut() {
				let records = replace(records, Vec::new());
				DNS_CACHE.with(|cache|{
					cache.store_record(clazz, owner, rrtype, ttl, records, debug)
				});
			}
		}
	}
	Ok(())
}

fn dns_query_response_tail(rbuf: &[u8], name: &DnsNameRef, clazz: DnsClass, rrtype: DnsType, dns0x20: &Dns0x20,
	debug: &Debugging) -> Result<DnsQueryResponseTailResult, String>
{
	let response = f_return!(DnsMessage::decode(rbuf, |a,b,c,d,e,f|{
		print_record(debug,a,b,c,d,e,f)
	}), F[|err: MessageParseError|{
		Ok(DnsQueryResponseTailResult::ParseError(format!("Parse error: {err}")))
	}]);
	f_return!(check_response_query(response.header(), response.question(), name, clazz, rrtype, dns0x20), F[|err|{
		Ok(DnsQueryResponseTailResult::ParseError(err))
	}]);

	let mut responses = Vec::new();
	let found = response.header().rcode == 0;
	//Cache records in any case.
	cache_records(response.answer(), debug)?;
	cache_records(response.authority(), debug)?;
	cache_records(response.additional(), debug)?;
	//Chase CNAMEs, if any. Except if rrype is CNAME or SOA.
	let pname = DnsNamePrinterC(clazz, name);
	let mut name = name.to_owned();
	let mut count = 0;
	loop {
		if rrtype == DNSTYPE_CNAME || rrtype == DNSTYPE_SOA { break; }
		let mut found_cname = false;
		for i in response.answer().chain(response.additional()) {
			if i.clazz != clazz { continue; }	//Wrong class.
			if let &Rrdata::SName{rtype:DNSTYPE_CNAME, ref target} = &i.data {
				//Check owner matches.
				if i.owner != name.deref() { continue; }
				name = target.decompress_owned();
				debug!(debug, DEBUG_DNS_ADDRESSES, "Following CNAME from {pname} to {target}",
					target=DnsNamePrinterC(clazz, &name));
				fail_if!(count > 10, format!("Too many CNAME redirects"));
				count += 1;
				found_cname = true;
			}
		}
		if !found_cname { break; }
	}
	if !found {
		let mut ttl = 0;
		//There should be a SOA in authority... Take its MINIMUM as negative cache duration.
		for i in response.authority() {
			if i.clazz != clazz { continue; }
			if let &Rrdata::Soa{minimum,..} = &i.data { ttl = minimum; }
		}
		DNS_CACHE.with(|cache|cache.got_nxdomain(clazz, &name, ttl, debug));
		return Ok(DnsQueryResponseTailResult::Nxdomain);
	}

	let mut ttl = 0;
	for i in response.answer() {
		//Check owner name.
		if i.owner != name.deref() || i.clazz != clazz || i.rtype != rrtype { continue; }
		ttl = i.ttl;		//store TTL.
		let mut content = Vec::new();
		i.data.uncompress(&mut content).ok();	//Can't fail.
		responses.push(content);
	}
	//Cache this.
	let responses2 = responses.clone();
	DNS_CACHE.with(|cache|cache.store_record(clazz, &name, rrtype, ttl, responses2, debug));
	Ok(DnsQueryResponseTailResult::Ok(responses))
}

fn dns_query_response_transreceive<Conn:HttpConnection>(addr: SocketAddr, connection: &mut Conn,
	name: &DnsNameRef, clazz: DnsClass, rrtype: DnsType, dns0x20: &Dns0x20, debug: &Debugging,
	deadline: Deadline) -> Result<Vec<u8>, String>
{
	static TXID: AtomicUsize = AtomicUsize::new(0);
	let txid = TXID.fetch_add(1, MemOrder::Relaxed);
	let pname = DnsNamePrinter(clazz, name, dns0x20);
	debug!(debug, DEBUG_DNS_MSG_S, "Sending DNS query {txid} for {pname} {rrtype} to {addr}");
	let mut qbuf = SmallVec::<u8,512>::new();
	dns_format_query(name, clazz, rrtype, txid as u16, dns0x20, true, &mut qbuf).
		set_err("Internal error: Query exceeds 512 bytes")?;
	let qbuf2 = qbuf.as_inner();
	debug!(debug, DEBUG_DNS_RAWMSG, "Sending DNS message to {addr}: {qbuf}", qbuf=Hexdump(qbuf2));
	if let Err(err) = connection.send(&qbuf2, deadline) {
		fail!(format!("write: {err}"));
	}
	//Receive the header of response.
	let mut rbuf = Vec::new();
	loop {
		match connection.receive(deadline, |data|{
			if data.len() == 0 { return Ok(2u32); }
			let old = rbuf.len();
			rbuf.resize(old + data.len(), 0);
			(&mut rbuf[old..]).copy_from_slice(data);
			let rlen = if rbuf.len() >= 2 {
				(rbuf[0] as usize) * 256 + (rbuf[1] as usize)
			} else {
				0
			};
			Ok(if rbuf.len() >= 2 + rlen { 1u32 } else { 0u32 })
		}) {
			Ok(1u32) => break,
			Ok(2u32) => fail!(format!("read: Unexpected EOF")),
			Ok(_) => (),
			Err(err) => fail!(format!("read: {err}")),
		}
	}
	rbuf.remove(0);		//Remove the two first bytes, which are message length.
	rbuf.remove(0);
	debug!(debug, DEBUG_DNS_RAWMSG, "Received DNS message from {addr}: {rbuf}", rbuf=Hexdump(&rbuf));
	fail_if!(rbuf.len() < 12, "DNS response header truncated");
	let got_txid = rbuf[0] as u16 * 256 + rbuf[1] as u16;
	fail_if!(got_txid != txid as u16, format!("Bogus DNS TXID: Got {got_txid:04x}, expected {txid:04x}",
		txid=txid as u16));
	fail_if!(rbuf[2] & 128 == 0, "Received back DNS query");
	debug!(debug, DEBUG_DNS_MSG_S, "Received DNS response {txid} for {pname} {rrtype} from {addr}");
	//Parse the response.
	Ok(rbuf)
}

fn cloudflare_dns_trustpile() -> Vec<TrustAnchor>
{
	let raw = include_bytes!("DigiCertGlobalRootCA.crt");
	let mut raw = &raw[..];
	//Assume this can not fail.
	let ta = TrustAnchor::from_certificate(&mut raw).unwrap();
	vec![ta]
}

fn dns_server_contact(addr: &IpAddr, debug: &Debugging, security: Option<&'static str>, deadline: Deadline) ->
	Result<Connection<TcpTransport>, String>
{
	let (secure, trustpile, hostname) = match security {
		Some("cloudflare-dns.com") => (true, cloudflare_dns_trustpile(), "cloudflare-dns.com"),
		Some(err) => fail!(format!("Unknown DoT service {err}")),
		None => (false, Vec::new(), "recursive_dns_resolver")
	};
	let hostname = HostStr::new_string(hostname).unwrap();	//Assume this is always valid.
	let addr = SocketAddr::new(addr.clone(), if secure { 853 } else { 53 });
	debug!(debug, DEBUG_DNS_MSG_S, "Trying to connect to DNS server {addr}{sflag}",
		sflag=if secure { " (SECURE)" } else { "" });
	let (connection, _) = TcpTransport::establish_connection(hostname, An("domain"), Some(addr), debug,
		deadline).map_err(|err|{
		err.to_string()
	})?;
	let connection = if secure {
		let trustpile = TrustAnchorSet::from_btls_list(&trustpile);
		debug!(debug, DEBUG_DNS_MSG_S, "Trying to TLS handshake with DoT server {addr}");
		Connection::new_tls_connected(hostname, if secure { An("domains") } else { An("domain") },
			connection, &trustpile, debug, deadline)
	} else {
		Connection::new_tcp_connected(hostname, An("domain"), connection, debug)
	}.map_err(|x|{
		x.to_string()
	})?;
	debug!(debug, DEBUG_DNS_MSG_S, "Connected to DNS server {addr}");
	Ok(connection)
}

fn find_nameserver_addresses() -> (Vec<IpAddr>, Option<&'static str>)
{
	let mut res = Vec::new();
	if let Some(x) = flags(&FORCE_NAMESERVER, ||var("CERTTOOL_DNSSERVER").ok()) {
		if x == "cloudflare-dns.com" {
			let ipv4 = ipv4_works();
			let ipv6 = ipv6_works();
			if ipv6 { res.push(IpAddr::V6(Ipv6Addr::new(0x2606,0x4700,0x4700,0,0,0,0,0x1111))); }
			if ipv6 { res.push(IpAddr::V6(Ipv6Addr::new(0x2606,0x4700,0x4700,0,0,0,0,0x1001))); }
			if ipv4 { res.push(IpAddr::V4(Ipv4Addr::new(1,1,1,1))); }
			if ipv4 { res.push(IpAddr::V4(Ipv4Addr::new(1,0,0,1))); }
			return (res, Some("cloudflare-dns.com"));
		}
		for i in x.split(",") {
			if let Ok(naddr) = IpAddr::from_str(&i) {
				res.push(naddr);
			}
		}
		return (res, None);
	}
	let mut resolv_contents = String::new();
	if let Err(_) = File::open("/etc/resolv.conf").and_then(|mut x|x.read_to_string(&mut resolv_contents)) {
		return (res, None);
	}
	for i in resolv_contents.lines() {
		let mut w = i.split_whitespace();
		if w.next() != Some("nameserver") { continue; }	//Not interested.
		let nsaddr = match w.next() { Some(x) => x, None => continue };
		let nsaddr = match IpAddr::from_str(nsaddr) { Ok(x) => x, Err(_) => continue };
		res.push(nsaddr);
	}
	(res, None)
}

pub fn get_used_nameservers() -> Result<Vec<IpAddr>, &'static str>
{
	match find_nameserver_addresses() {
		(_, Some(x)) => Err(x),
		(y, None) => Ok(y),
	}
}

fn query_dns_nocache_cc(cc: &mut Option<Connection<TcpTransport>>, name: &DnsNameRef, clazz: DnsClass,
	rrtype: DnsType, nxdomain_success: bool, debug: &Debugging, deadline: Deadline) ->
	Result<Vec<Vec<u8>>, String>
{
	let mut dns0x20 = Dns0x20::new();
	let pname = DnsNamePrinterC(clazz, &name);
	dns0x20.with(secure_random);
	let ret = with_tcp_connection(cc, debug, deadline, |cc, addr|{
		let rbuf = match dns_query_response_transreceive(addr, cc, name, clazz, rrtype, &dns0x20, debug,
			deadline) {
			Ok(rbuf) => rbuf,
			Err(err) => {
				debug!(debug, DEBUG_DNS_MSG_S, "Connection to DNS server {addr} lost");
				warning!(debug, "nameserver {addr}: {err}");
				return Ok(None);
			}
		};
		match dns_query_response_tail(&rbuf, name, clazz, rrtype, &dns0x20, debug)? {
			//This was already cached by dns_query_response_tail.
			DnsQueryResponseTailResult::Ok(data) => Ok(Some(data)),
			//This was already cached by dns_query_response_tail.
			DnsQueryResponseTailResult::Nxdomain => {
				if nxdomain_success {
					Ok(Some(Vec::new()))
				} else {
					fail!(format!("Name does not exist"))
				}
			},
			DnsQueryResponseTailResult::ParseError(err) => {
				warning!(debug, "Bad response from {addr} for {pname} {rrtype}: {err}");
				//Assume the DNS nameserver is horked, try another.
				return Ok(None);
			},
		}
	});
	match ret {
		Ok(Some(t)) => Ok(t),
		Ok(None) => Err(TMPFAIL.to_string()),
		Err(err) => Err(err),
	}
}

fn with_tcp_connection<T>(cc: &mut Option<Connection<TcpTransport>>, debug: &Debugging, deadline: Deadline,
	mut func: impl FnMut(&mut Connection<TcpTransport>, SocketAddr) -> Result<Option<T>, String>) ->
	Result<Option<T>, String>
{
	if let Some(cc) = cc.as_mut() {
		let ns = cc.peer_address();
		match func(cc, ns) {
			Ok(Some(ret)) => return Ok(Some(ret)),
			Ok(None) => (),	//Continue.
			Err(err) => fail!(err)
		}
		//No need to clear the cc on temporary failure, since we will change it below anyway.
	}

	let (nslist, security) = find_nameserver_addresses();
	if nslist.len() == 0 {
		fail!("Failed to find usable nameservers from /etc/resolv.conf".to_string());
	}
	for ns in nslist.iter() {
		let conn = match dns_server_contact(ns, debug, security, deadline) {
			Ok(x) => x,
			//This is treated as temporary error.
			Err(err) => {
				warning!(debug, "nameserver {ns}: connect: {err}");
				continue
			},
		};
		*cc = Some(conn);
		if let Some(cc) = cc.as_mut() {
			let ns = cc.peer_address();
			match func(cc, ns) {
				Ok(Some(ret)) => return Ok(Some(ret)),
				Ok(None) => (),	//Continue.
				Err(err) => fail!(err)
			}
		} else {
			//WTF??? How on earth we can end up here?
			panic!("Set *cc to Some(x) and immediately after it is None?");
		}
	}
	Ok(None)	//No valid response.
}

const MAX_DNS_TIMEOUT: Duration = Duration::from_millis(10000);

#[derive(Copy,Clone,Debug,PartialEq)]
pub enum RelatedNameKind
{
	ZoneRoot,
	Canonical,
	Nameserver,
	Ipv4Address,
	Ipv6Address,
}

pub fn find_related_name(name: &[u8], clazz: DnsClass, kind: RelatedNameKind, debug: &Debugging,
	deadline: Deadline) -> Result<Vec<u8>, String>
{
	let name = DnsName::from_master(name).map_err(|err|{
		format!("Illegal name: {err}")
	})?;
	let deadline = deadline.derive(MAX_DNS_TIMEOUT);
	//No, we do not support QCLASS ANY or QCLASS ANY.
	fail_if!(clazz == DNSCLASS_ANY, "QCLASS ANY not supported");
	let rrtype = match &kind {
		&RelatedNameKind::ZoneRoot => DNSTYPE_NS,
		&RelatedNameKind::Canonical => DNSTYPE_CNAME,
		&RelatedNameKind::Nameserver => DNSTYPE_NS,
		&RelatedNameKind::Ipv4Address => DNSTYPE_A,
		&RelatedNameKind::Ipv6Address => DNSTYPE_AAAA,
	};
	let mut dns0x20 = Dns0x20::new();
	dns0x20.with(secure_random);
	with_conn_cache(|cc|{
		let ret = with_tcp_connection(cc, debug, deadline, |cc, addr|{
			//The QTYPE for these queries is always NS.
			let rbuf = match dns_query_response_transreceive(addr, cc, &name, clazz, rrtype, &dns0x20,
				debug, deadline) {
				Ok(rbuf) => rbuf,
				Err(err) => {
					warning!(debug, "nameserver {addr}: {err}");
					return Ok(None);
				}
			};
			dns_find_related_tail(&rbuf, &name, clazz, rrtype, kind, &dns0x20, addr, debug)
		});
		match ret {
			Ok(Some(t)) => Ok(t),
			Ok(None) => Err(TMPFAIL.to_string()),
			Err(err) => Err(err),
		}
	})
}

pub fn dns_kill_connections()
{
	with_conn_cache(|cc|*cc = None);
}

pub fn query_dns(name: &[u8], clazz: DnsClass, rrtype: DnsType, nxdomain_success: bool, debug: &Debugging,
	deadline: Deadline) -> Result<Vec<Vec<u8>>, String>
{
	let deadline = deadline.derive(MAX_DNS_TIMEOUT);
	let name = DnsName::from_master(name).map_err(|err|{
		format!("Illegal name: {err}")
	})?;
	_query_dns(&name, clazz, rrtype, nxdomain_success, debug, deadline)
}

fn _query_dns(name: &DnsNameRef, mut clazz: DnsClass, rrtype: DnsType, nxdomain_success: bool, debug: &Debugging,
	deadline: Deadline) -> Result<Vec<Vec<u8>>, String>
{
	let mut name = name.to_owned();
	//No, we do not support QCLASS ANY or QCLASS ANY.
	fail_if!(clazz == DNSCLASS_ANY, "QCLASS ANY not supported");
	fail_if!(rrtype == DNSTYPE_ANY, "QTYPE ANY not supported");

	let mut redirects = 0;
	loop {
		//Check cache.
		match DNS_CACHE.with(|cache|cache.lookup_record(clazz, &name, rrtype, debug)) {
			HostCacheResponse::Ok(x) => return Ok(x),
			HostCacheResponse::HostDoesNotExist => if nxdomain_success {
				return Ok(Vec::new());
			} else {
				fail!("Host does not exist");
			},
			//Note that lookup_record() will not give Redirect for CNAME nor SOA lookup.
			HostCacheResponse::Redirect(tclass, target) => {
				fail_if!(redirects == 10, "Too many CNAMEs");
				//Redirect (CNAME).
				clazz = tclass;
				name = target;
				redirects += 1;
			},
			HostCacheResponse::NotFoundInCache => break,	//Do actual query.
		}
	}
	with_conn_cache(|conn|{
		query_dns_nocache_cc(conn, &name, clazz, rrtype, nxdomain_success, debug, deadline)
	})
}


fn is_special_ipv6(addr: &Ipv6Addr) -> bool
{
	let t = addr.segments();
	let k = (t[0] as u64) << 32 | (t[1] as u64) << 16 | (t[2] as u64);
	k >> 45 << 45 == 0x000000000000 ||	//Not global unicast (0000-1FFF)
	k >> 25 << 25 == 0x200100000000 ||	//IETF protocol assignments.
	k >> 16 << 16 == 0x20010db80000 ||	//Documentation.
	k >> 32 << 32 == 0x200200000000 ||	//6to4.
	k             == 0x2620004f8000 ||	//DD-AS112.
	k >> 46 << 46 == 0x400000000000 ||	//Not global unicast (4000-7FFF)
	k >> 47 << 47 == 0x800000000000		//Not global unicast (8000-FFFF)
}

fn is_special_ipv4(addr: &Ipv4Addr) -> bool
{
	let i = addr.octets();
	let k = (i[0] as u32) << 16 | (i[1] as u32) << 8 | (i[2] as u32);
	k >> 16 << 16 == 0x000000 ||	//Localnet.
	k >> 16 << 16 == 0x0a0000 ||	//RFC1918.
	k >> 14 << 14 == 0x644000 ||	//Shared address space.
	k >> 16 << 16 == 0x7f0000 ||	//Loopback.
	k >>  8 <<  8 == 0xa9fe00 ||	//Link-local.
	k >> 12 << 12 == 0xac1000 ||	//RFC1918.
	k             == 0xc00000 ||	//IETF protocol assignments.
	k             == 0xc00002 ||	//Documentation.
	k             == 0xc01fc4 ||	//AS112v4.
	k             == 0xc034c1 ||	//AMT.
	k             == 0xc05863 ||	//6to4.
	k >>  8 <<  8 == 0xc0a800 ||	//RFC1918.
	k             == 0xc0af30 ||	//DD-AS112.
	k >>  9 <<  9 == 0xc61200 ||	//Benchmarking.
	k             == 0xc63364 ||	//Documentation.
	k             == 0xcb0071 ||	//Documentation.
	k >> 21 << 21 == 0xe00000	//Multicast and "class E".
}

fn flags<T:Clone>(x: &AtomicPtr<T>, y: impl FnOnce() -> T) -> T { get_flag(x).unwrap_or_else(y) }

fn ipv6_disabled() -> bool { flags(&IPV6_DISABLE, ||var_os("CERTTOOL_NO_IPV6").is_some()) }
fn ipv4_disabled() -> bool { flags(&IPV4_DISABLE, ||var_os("CERTTOOL_NO_IPV4").is_some()) }

fn ipv4_works() -> bool
{
	if ipv4_disabled() { return false; }
	let dest = Address::Ipv4(AddressIpv4 {
		addr: [1,0,0,9],
		port: 9,
	});
	let fd;
	loop {
		let flags = SocketFlags::CLOEXEC;
		match FileDescriptor::socket2(SocketFamily::INET, SocketType::DGRAM, Default::default(), flags) {
			Ok(x) => {fd = x; break; },
			Err(e) if is_fatal(&IoError::from_raw_os_error(e.to_inner())) => return false,
			Err(_) => continue
		}
	}
	loop {
		let tmp = [b'T'];
		if let Err(err) = fd.sendto(&tmp, &dest, Default::default()) {
			if is_fatal(&IoError::from_raw_os_error(err.to_inner())) { return false; }
			continue;
		} else {
			return true;
		}
	}
}

fn ipv6_works() -> bool
{
	if ipv6_disabled() { return false; }
	let dest = Address::Ipv6(AddressIpv6 {
		addr: [32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
		port: 9,
		flow: 0,
		scope: 0,
	});
	let fd;
	loop {
		let flags = SocketFlags::CLOEXEC;
		match FileDescriptor::socket2(SocketFamily::INET6, SocketType::DGRAM, Default::default(), flags) {
			Ok(x) => {fd = x; break; },
			Err(e) if is_fatal(&IoError::from_raw_os_error(e.to_inner())) => return false,
			Err(_) => continue
		}
	}
	loop {
		let tmp = [b'T'];
		if let Err(err) = fd.sendto(&tmp, &dest, Default::default()) {
			if is_fatal(&IoError::from_raw_os_error(err.to_inner())) { return false; }
			continue;
		} else {
			return true;
		}
	}
}

static IPV4_DISABLE: AtomicPtr<bool> = AtomicPtr::new(null_mut());
static IPV6_DISABLE: AtomicPtr<bool> = AtomicPtr::new(null_mut());
static FORCE_NAMESERVER: AtomicPtr<Option<String>> = AtomicPtr::new(null_mut());

fn get_flag<T:Clone>(flag: &AtomicPtr<T>) -> Option<T>
{
	let ptr = flag.load(MemOrder::Relaxed);
	if ptr.is_null() { None } else { unsafe {Some((*ptr).clone()) } }
}

fn set_flag<T:Clone>(flag: &AtomicPtr<T>, new: Option<T>)
{
	let new = if let Some(new) = new { Box::into_raw(Box::new(new)) } else { null_mut() };
	let old = flag.swap(new, MemOrder::Relaxed);
	if !old.is_null() { unsafe { drop(Box::from_raw(old)); } }	//Let the box drop.
}

pub fn dns_set_ipv4_disable(new: Option<bool>) { set_flag(&IPV4_DISABLE, new); }
pub fn dns_set_ipv6_disable(new: Option<bool>) { set_flag(&IPV6_DISABLE, new); }
pub fn dns_get_ipv4_disable() -> bool { ipv4_disabled() }
pub fn dns_get_ipv6_disable() -> bool { ipv6_disabled() }
pub fn dns_set_forced_nameserver(new: Option<Option<String>>) { set_flag(&FORCE_NAMESERVER, new); }

pub fn domain_set_preferred_address(name: &[u8], clazz: DnsClass, addr: Option<IpAddr>, debug: &Debugging)
{
	if let Ok(name) = DnsName::from_master(name) {
		DNS_CACHE.with(|cache|cache.set_preferred_address(clazz, name.deref(), addr, debug));
	}
}

pub fn domain_address_records(name: &[u8], clazz: DnsClass, force_all: bool, debug: &Debugging,
	deadline: Deadline) -> Result<Vec<IpAddr>, String>
{
	let name = DnsName::from_master(name).map_err(|err|{
		format!("Illegal name: {err}")
	})?;
	let deadline = deadline.derive(MAX_DNS_TIMEOUT);
	let mut out = Vec::new();
	let (ipv4_ok, ipv6_ok) = if force_all {
		debug!(debug, DEBUG_DNS_ADDRESSES, "Forced all address families as enabled");
		(true, true)
	} else {
		let ipv6_ok = ipv6_works();
		let ipv6_st = if ipv6_ok { "yes" } else { "no" };
		debug!(debug, DEBUG_DNS_ADDRESSES, "Checking for IPv6...{ipv6_st}");
		let ipv4_ok = ipv4_works();
		let ipv4_st = if ipv4_ok { "yes" } else { "no" };
		debug!(debug, DEBUG_DNS_ADDRESSES, "Checking for IPv4...{ipv4_st}");
		(ipv4_ok, ipv6_ok)
	};

	let pname = DnsNamePrinterC(clazz, &name);
	let preferred = DNS_CACHE.with(|cache|{
		cache.get_preferred_address(clazz, name.deref(), debug)
	});
	//force_all does not have preferred address.
	if let Some(preferred) = preferred { if !force_all {
		debug!(debug, DEBUG_DNS_ADDRESSES, "{pname} Preferred address is {preferred}");
	}}
	let a6 = if ipv6_ok { _query_dns(&name, clazz, DNSTYPE_AAAA, false, debug, deadline)? } else { Vec::new() };
	let a4 = if ipv4_ok { _query_dns(&name, clazz, DNSTYPE_A, false, debug, deadline)? } else { Vec::new() };
	for i in a6.iter() {
		fail_if!(i.len() != 16, "Invalid AAAA record");
		let mut t = [0u8;16];
		t.copy_from_slice(i);
		let addr = Ipv6Addr::from(t);
		if is_special_ipv6(&addr) {
			debug!(debug, DEBUG_DNS_ADDRESSES, "{pname} Ignoring special address {addr}");
			continue;
		}
		debug!(debug, DEBUG_DNS_ADDRESSES, "{pname} Got address {addr}");
		let addr = IpAddr::V6(addr);
		//Preferred address is inserted first. Otherwise address is put to end of list.
		//If force_all is set, no address is preferred.
		if Some(addr) == preferred && !force_all { out.insert(0, addr); } else { out.push(addr); }
	}
	for i in a4.iter() {
		fail_if!(i.len() != 4, "Invalid A record");
		let mut t = [0u8;4];
		t.copy_from_slice(i);
		let addr = Ipv4Addr::from(t);
		if is_special_ipv4(&addr) {
			debug!(debug, DEBUG_DNS_ADDRESSES, "{pname} Ignoring special address {addr}");
			continue;
		}
		debug!(debug, DEBUG_DNS_ADDRESSES, "{pname} Got address {addr}");
		let addr = IpAddr::V4(addr);
		//Preferred address is inserted first. Otherwise address is put to end of list.
		//If force_all is set, no address is preferred.
		if Some(addr) == preferred && !force_all { out.insert(0, addr); } else { out.push(addr); }
	}
	Ok(out)
}

#[derive(Clone,Debug)]
pub struct AddressReplace
{
	src_addr: IpAddr,
	src_port: u16,
	dst_addr: IpAddr,
	dst_port: u16,
}

impl AddressReplace
{
	pub fn new_from_line(line: &str) -> Result<AddressReplace, String>
	{
		let mut w = line.split_whitespace();
		let a = w.next();
		let b = w.next();
		let c = w.next();
		let d = w.next();
		let e = w.next();
		let (a, b, c, d) = match (a, b, c, d, e) {
			(Some(a), Some(b), Some(c), Some(d), None) => (a, b, c, d),
			(_,_,_,_,_) => fail!(format!("Bad address replace line '{line}'"))
		};
		let a = IpAddr::from_str(a).map_err(|_|{
			format!("Bad source address on replace line '{line}'")
		})?;
		let b = u16::from_str(b).map_err(|_|{
			format!("Bad source port on replace line '{line}'")
		})?;
		let c = IpAddr::from_str(c).map_err(|_|{
			format!("Bad target address on replace line '{line}'")
		})?;
		let d = u16::from_str(d).map_err(|_|{
			format!("Bad target port on replace line '{line}'")
		})?;
		Ok(AddressReplace{src_addr:a, src_port:b, dst_addr:c, dst_port: d})
	}
}

#[derive(Clone,Debug)]
pub struct AddressReplaceList
{
	vector: Vec<AddressReplace>
}

impl AddressReplaceList
{
	pub fn new() -> AddressReplaceList
	{
		AddressReplaceList{vector: Vec::new()}
	}
	pub fn add(&mut self, replace: AddressReplace)
	{
		self.vector.push(replace);
	}
	pub fn parse_file_contents(&mut self, x: &[u8], debug: &Debugging)
	{
		for i in ByteStringLines::new(x) {
			if let Ok(i) = from_utf8(i) {
				let i = i.trim();
				if i.len() == 0 || i.starts_with("#") { continue };
				match AddressReplace::new_from_line(i) {
					Ok(x) => self.vector.push(x),
					Err(err) => warning!(debug, "Failed to parse replace line '{i}': {err}",
						i=SafeShowString(i))
				};
			} else {
				//Do not complain about empty and all-comment lines.
				let i = trim_ascii(i);
				if i.len() == 0 || i[0] == b'#' { continue };
				//Not a comment line, complain.
				warning!(debug, "Failed to parse replace line '{i}': Not UTF-8",
					i=SafeShowByteString(i))
			}
		}
	}
	pub fn map(&self, addr: &IpAddr, port: u16) -> Option<(IpAddr, u16)>
	{
		let mut match_found = false;
		for i in self.vector.iter() {
			if addr != &i.src_addr { continue; }
			match_found = true;
			if port != i.src_port { continue; }
			return Some((i.dst_addr.clone(), i.dst_port));
		}
		fail_if_none!(match_found);		//Found address, but no mapping.
		Some((addr.clone(), port))		//Map to self.
	}
	pub fn map_all(&self, addrs: &[IpAddr], port: u16) -> Vec<SocketAddr>
	{
		let mut out = Vec::new();
		for i in addrs.iter() {
			let (addr, port) = match self.map(i, port) { Some(x) => x, None => continue };
			out.push(SocketAddr::new(addr, port));
		}
		out
	}
	pub fn compatible(&self, addrs: &[IpAddr], port: u16) -> bool
	{
		for i in addrs.iter() {
			//If any address produces unmappable, it is not compatible.
			match self.map(i, port) { Some(_) => (), None => return false };
		}
		addrs.len() > 0
	}
}

pub fn kill_all_connections()
{
	CONN_CACHE.with(|conn|{
		*conn.borrow_mut() = None;
	});
}

#[cfg(test)]
mod test
{
	use crate::deadline::Deadline;
	use crate::debug::Debugging;
	use crate::debug::StderrDebug;
	use super::domain_address_records;
	use super::is_special_ipv4;
	use super::is_special_ipv6;
	use super::DNSTYPE_A;
	use super::query_dns;
	use super::DNSTYPE_AAAA;
	use super::AddressReplace;
	use super::AddressReplaceList;
	use btls_aux_dnsmsg::consts::DNSCLASS_IN;
	use btls_aux_dnsmsg::consts::DNSTYPE_TXT;
	use std::net::IpAddr;
	use std::net::Ipv4Addr;
	use std::net::Ipv6Addr;
	use std::str::FromStr;
	use std::time::Duration;


	#[test]
	fn replace_another()
	{
		let mut r = AddressReplaceList::new();
		r.add(AddressReplace::new_from_line("192.0.2.57 80 203.0.113.5 2080").unwrap());
		r.add(AddressReplace::new_from_line("192.0.2.57 443 203.0.113.5 2443").unwrap());
		assert_eq!(r.map(&IpAddr::from_str("192.0.2.58").unwrap(), 80).unwrap(),
			(IpAddr::from_str("192.0.2.58").unwrap(), 80));
		assert_eq!(r.map(&IpAddr::from_str("192.0.2.58").unwrap(), 443).unwrap(),
			(IpAddr::from_str("192.0.2.58").unwrap(), 443));
		assert_eq!(r.map(&IpAddr::from_str("203.0.113.5").unwrap(), 80).unwrap(),
			(IpAddr::from_str("203.0.113.5").unwrap(), 80));
		assert_eq!(r.map(&IpAddr::from_str("203.0.113.5").unwrap(), 443).unwrap(),
			(IpAddr::from_str("203.0.113.5").unwrap(), 443));
	}

	#[test]
	fn replace_hit()
	{
		let mut r = AddressReplaceList::new();
		r.add(AddressReplace::new_from_line("192.0.2.57 80 203.0.113.5 2080").unwrap());
		r.add(AddressReplace::new_from_line("192.0.2.57 443 203.0.113.5 2443").unwrap());
		assert_eq!(r.map(&IpAddr::from_str("192.0.2.57").unwrap(), 80).unwrap(),
			(IpAddr::from_str("203.0.113.5").unwrap(), 2080));
		assert_eq!(r.map(&IpAddr::from_str("192.0.2.57").unwrap(), 443).unwrap(),
			(IpAddr::from_str("203.0.113.5").unwrap(), 2443));
	}

	#[test]
	fn replace_toipv6()
	{
		let mut r = AddressReplaceList::new();
		r.add(AddressReplace::new_from_line("192.0.2.57 80 fec0::1:0122 2080").unwrap());
		r.add(AddressReplace::new_from_line("192.0.2.57 443 fec0::1:0122 2443").unwrap());
		assert_eq!(r.map(&IpAddr::from_str("192.0.2.57").unwrap(), 80).unwrap(),
			(IpAddr::from_str("fec0::1:0122").unwrap(), 2080));
		assert_eq!(r.map(&IpAddr::from_str("192.0.2.57").unwrap(), 443).unwrap(),
			(IpAddr::from_str("fec0::1:0122").unwrap(), 2443));
	}

	#[test]
	fn replace_toipv4()
	{
		let mut r = AddressReplaceList::new();
		r.add(AddressReplace::new_from_line("2001:db8::5 80 198.51.0.8 2080").unwrap());
		r.add(AddressReplace::new_from_line("2001:db8::5 443 198.51.0.8 2443").unwrap());
		assert_eq!(r.map(&IpAddr::from_str("2001:db8::5").unwrap(), 80).unwrap(),
			(IpAddr::from_str("198.51.0.8").unwrap(), 2080));
		assert_eq!(r.map(&IpAddr::from_str("2001:db8::5").unwrap(), 443).unwrap(),
			(IpAddr::from_str("198.51.0.8").unwrap(), 2443));
	}

	#[test]
	fn replace_no_portmatch()
	{
		let mut r = AddressReplaceList::new();
		r.add(AddressReplace::new_from_line("2001:db8::5 80 fec0::1:0152 2080").unwrap());
		assert_eq!(r.map(&IpAddr::from_str("2001:db8::5").unwrap(), 80).unwrap(),
			(IpAddr::from_str("fec0::1:0152").unwrap(), 2080));
		assert!(r.map(&IpAddr::from_str("2001:db8::5").unwrap(), 443).is_none());
	}


	#[test]
	fn ipv4_special()
	{
		assert!(is_special_ipv4(&Ipv4Addr::from_str("0.0.0.0").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("0.255.255.255").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("1.0.0.0").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("9.255.255.255").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("10.0.0.0").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("10.255.255.255").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("11.0.0.0").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("100.63.255.255").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("100.64.0.0").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("100.127.255.255").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("100.128.0.0").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("126.255.255.255").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("127.0.0.0").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("127.255.255.255").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("128.0.0.0").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("169.253.255.255").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("169.254.0.0").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("169.254.255.255").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("169.255.0.0").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("172.15.255.255").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("172.16.0.0").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("172.31.255.255").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("172.32.0.0").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("191.255.255.255").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("192.0.0.0").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("192.0.0.255").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("192.0.1.0").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("192.0.1.255").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("192.0.2.0").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("192.0.2.255").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("192.0.3.0").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("192.31.195.255").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("192.31.196.0").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("192.31.196.255").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("192.31.197.0").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("192.52.192.255").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("192.52.193.0").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("192.52.193.255").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("192.52.194.0").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("192.88.98.255").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("192.88.99.0").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("192.88.99.255").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("192.88.100.0").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("192.167.255.255").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("192.168.0.0").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("192.168.255.255").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("192.169.0.0").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("192.175.47.255").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("192.175.48.0").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("192.175.48.255").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("192.175.49.0").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("198.17.255.255").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("198.18.0.0").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("198.19.255.255").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("198.20.0.0").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("198.51.99.255").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("198.51.100.0").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("198.51.100.255").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("198.51.101.0").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("203.0.112.255").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("203.0.113.0").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("203.0.113.255").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("203.0.114.0").unwrap()));
		assert!(!is_special_ipv4(&Ipv4Addr::from_str("223.255.255.255").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("224.0.0.0").unwrap()));
		assert!(is_special_ipv4(&Ipv4Addr::from_str("255.255.255.255").unwrap()));
	}

	#[test]
	fn ipv6_special()
	{
		assert!(is_special_ipv6(&Ipv6Addr::from_str("::").unwrap()));
		assert!(is_special_ipv6(&Ipv6Addr::from_str("1FFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF").unwrap()));
		assert!(!is_special_ipv6(&Ipv6Addr::from_str("2000::").unwrap()));
		assert!(!is_special_ipv6(&Ipv6Addr::from_str("2000:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF").unwrap()));
		assert!(is_special_ipv6(&Ipv6Addr::from_str("2001::").unwrap()));
		assert!(is_special_ipv6(&Ipv6Addr::from_str("2001:01FF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF").unwrap()));
		assert!(!is_special_ipv6(&Ipv6Addr::from_str("2001:0200::").unwrap()));
		assert!(!is_special_ipv6(&Ipv6Addr::from_str("2001:0db7:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF").unwrap()));
		assert!(is_special_ipv6(&Ipv6Addr::from_str("2001:0db8::").unwrap()));
		assert!(is_special_ipv6(&Ipv6Addr::from_str("2001:0db8:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF").unwrap()));
		assert!(!is_special_ipv6(&Ipv6Addr::from_str("2001:0db9::").unwrap()));
		assert!(!is_special_ipv6(&Ipv6Addr::from_str("2001:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF").unwrap()));
		assert!(is_special_ipv6(&Ipv6Addr::from_str("2002::").unwrap()));
		assert!(is_special_ipv6(&Ipv6Addr::from_str("2002:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF").unwrap()));
		assert!(!is_special_ipv6(&Ipv6Addr::from_str("2003::").unwrap()));
		assert!(!is_special_ipv6(&Ipv6Addr::from_str("2620:004F:7FFF:FFFF:FFFF:FFFF:FFFF:FFFF").unwrap()));
		assert!(is_special_ipv6(&Ipv6Addr::from_str("2620:004F:8000::").unwrap()));
		assert!(is_special_ipv6(&Ipv6Addr::from_str("2620:004F:8000:FFFF:FFFF:FFFF:FFFF:FFFF").unwrap()));
		assert!(!is_special_ipv6(&Ipv6Addr::from_str("2620:004F:8001::").unwrap()));
		assert!(!is_special_ipv6(&Ipv6Addr::from_str("3FFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF").unwrap()));
		assert!(is_special_ipv6(&Ipv6Addr::from_str("4000::").unwrap()));
		assert!(is_special_ipv6(&Ipv6Addr::from_str("7FFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF").unwrap()));
		assert!(is_special_ipv6(&Ipv6Addr::from_str("8000::").unwrap()));
		assert!(is_special_ipv6(&Ipv6Addr::from_str("FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF").unwrap()));
	}

	#[test]
	fn dns_fetch()
	{
		let debug = Debugging{mask:0xFFFFFFFFFFFFFFFF, out:Box::new(StderrDebug::new_outer())};
		let ans = query_dns(b"example.org", DNSCLASS_IN, DNSTYPE_AAAA, false, &debug,
			Deadline::new(Duration::from_secs(100))).unwrap();
		assert_eq!(ans.len(), 1);
		assert_eq!(&ans[0], &[38, 6, 40, 0, 2, 32, 0, 1, 2, 72, 24, 147, 37, 200, 25, 70]);
	}

	#[test]
	fn dns_nxdomain()
	{
		let debug = Debugging{mask:0xFFFFFFFFFFFFFFFF, out:Box::new(StderrDebug::new_outer())};
		assert!(query_dns(b"invalid3", DNSCLASS_IN, DNSTYPE_AAAA, false, &debug,
			Deadline::new(Duration::from_secs(100))).is_err());
		let ans = query_dns(b"invalid3", DNSCLASS_IN, DNSTYPE_AAAA, true, &debug,
			Deadline::new(Duration::from_secs(100))).unwrap();
		assert_eq!(ans.len(), 0);
	}

	#[test]
	fn se_text()
	{
		let debug = Debugging{mask:0xFFFFFFFFFFFFFFFF, out:Box::new(StderrDebug::new_outer())};
		let ans = query_dns(b"se", DNSCLASS_IN, DNSTYPE_TXT, false, &debug,
			Deadline::new(Duration::from_secs(100))).unwrap();
		assert!(ans.len() > 0);
		assert_eq!(ans[0][0] as usize + 1, ans[0].len());
	}

	#[test]
	fn dns_cname()
	{
		let debug = Debugging{mask:0xFFFFFFFFFFFFFFFF, out:Box::new(StderrDebug::new_outer())};
		let ans = query_dns(b"www.aalto.fi", DNSCLASS_IN, DNSTYPE_A, false, &debug,
			Deadline::new(Duration::from_secs(100))).unwrap();
		assert_eq!(ans.len(), 2);
	}

	#[test]
	fn domain_addresses()
	{
		let debug = Debugging{mask:0xFFFFFFFFFFFFFFFF, out:Box::new(StderrDebug::new_outer())};
		let ans = domain_address_records(b"example.org", DNSCLASS_IN, false, &debug,
			Deadline::new(Duration::from_secs(100))).unwrap();
		assert_eq!(ans.len(), 2);
		assert_eq!(ans[0], IpAddr::V6(Ipv6Addr::from_str("2606:2800:220:1:248:1893:25c8:1946").unwrap()));
		assert_eq!(ans[1], IpAddr::V4(Ipv4Addr::from_str("93.184.216.34").unwrap()));
	}
}
