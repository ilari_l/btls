#![warn(unsafe_op_in_unsafe_fn)]
use btls_aux_filename::Filename;
use btls_certtool::exiterror;
use btls_certtool::ParseArgs;
use btls_certtool::stderr;
use btls_certtool::path_append_extension;
use btls_certtool::do_world_umask;
use btls_certtool::symlink_equiv;
use std::fs::metadata;
use std::fs::rename;
use std::io::stderr;
use std::io::Write;
use std::path::Path;
use std::process::exit;
use std::str::FromStr;


static PROGNAME: &'static str = "certtool-linkdir";

fn main()
{
	let args = ParseArgs::new(PROGNAME);
	let progname = args.get_progname();

	let mut files = args.get_files();
	let target = files.next();
	let fault = files.next().is_some();

	if target.is_none() || fault {
		stderr!("Syntax: {progname} <basename>-<nnnn>");
		stderr!("");
		stderr!("This program atomically updates symbolic link <basename> to point to");
		stderr!("directory <basename>-<nnnn>. <basename>-<nnnn> must be valid unicode and");
		stderr!("must point to directory.");
		stderr!("");
		stderr!("The program prints a status line to stdout. This status line starts with one");
		stderr!("of the following, then contains a space and freeform message:");
		stderr!(" - 'OK':      Successful. Exit value is 0.");
		stderr!(" - 'ERROR':   Error. Exit value is 2.");
		//       +---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----
		exiterror!(E "Bad usage");
	}
	do_world_umask();
	let target = target.unwrap();
	let meta = metadata(&target).unwrap_or_else(|err|{
		exiterror!(E "Failed to stat '{name}': {err}", name=target.display())
	});
	if !meta.is_dir() {
		exiterror!(E "'{name}' is not a directory", name=target.display());
	}
	//Split at last '-'
	let targetf = Filename::from_path(&target);
	let dash_r = targetf.rfind("-").unwrap_or_else(||{
		exiterror!(E "No '-' in name passed to link")
	});
	let basename = targetf.get(..dash_r.start).expect("INTERNAL ERROR: find_last().start is invalid!");
	let suffix = targetf.get(dash_r.end..).expect("INTERNAL ERROR: find_last().end is invalid!");
	suffix.into_str().and_then(|s|u64::from_str(s).ok()).unwrap_or_else(||{
		exiterror!(E "Part after last '-' must be numeric in link")
	});
	//Find the last component of target.
	let targetl = Path::new(target.file_name().unwrap_or_else(||{
		exiterror!(E "WTF? file name is NULL???")
	}));
	let basename = basename.into_path();
	//Ok, link basename to targetl.
	let tempfile = path_append_extension(&basename, "tmp");
	symlink_equiv(&targetl, &tempfile).unwrap_or_else(|err|{
		exiterror!(E "Failed to symlink '{src}' -> '{tgt}': {err}",
			src=tempfile.display(), tgt=targetl.display())
	});
	//Rename the symlink.
	rename(&tempfile, &basename).unwrap_or_else(|err|{
		exiterror!(E "Failed to rename '{src}' -> '{tgt}': {err}",
			src=tempfile.display(), tgt=basename.display())
	});
	exiterror!(OK "Linked '{src}' -> '{tgt}'", src=basename.display(), tgt=targetl.display());
}
