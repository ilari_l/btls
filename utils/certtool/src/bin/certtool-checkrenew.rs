#![warn(unsafe_op_in_unsafe_fn)]
use btls_aux_hash::sha256;
use btls_aux_time::Timestamp;
use btls_aux_x509certparse::DecodeTsForUser2;
use btls_aux_x509certparse::ParsedCertificate2;
use btls_certtool::exiterror;
use btls_certtool::ParseArgs;
use btls_certtool::read_single_cert;
use btls_certtool::stderr;
use btls_certtool::acmeverify::hexencode;
use std::cmp::min;
use std::cmp::max;
use std::fs::File;
use std::io::ErrorKind;
use std::io::Read;
use std::io::stderr;
use std::io::Write;
use std::ops::Deref;
use std::process::exit;
use std::str::FromStr;
use std::time::SystemTime;


static PROGNAME: &'static str = "certtool-chekrenew";
static ARG_ADVANCE: &'static str = "--advance=";
static ARG_CSRCHKSUM: &'static str = "--csr-checksum=";

fn main()
{
	let args = ParseArgs::new(PROGNAME);
	let progname = args.get_progname();

	let mut advance = 0i64;
	if let Some(adv) = args.getopt_single_str(ARG_ADVANCE).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	}) {
		let adv_hours = f64::from_str(&adv).unwrap_or_else(|_|{
			exiterror!(E "Bad advance value '{adv}'")
		});
		//Convert to seconds.
		advance = (adv_hours * 3600.0) as i64;
	}
	let csrchksum = args.getopt_single_path(ARG_CSRCHKSUM).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});

	let mut files = args.get_files();
	let certfile = files.next();
	let csrfile = files.next();
	let fault = files.next().is_some();

	if certfile.is_none() || (csrchksum.is_some() && csrfile.is_none()) || fault {
		stderr!("Syntax: {progname} <certificate> [<source-csr> [{ARG_CSRCHKSUM}<chksumfile>]]");
		stderr!("\t[{ARG_ADVANCE}<adv-hours>]");
		stderr!("");
		stderr!("This program checks if <certificate> (can be in either DER or PEM form) needs");
		stderr!("to be renewed. The following heuristic is used for deciding if a certificate");
		stderr!("needs to be renewed:");
		stderr!(" - If original validity is at most 72 hours, the certificate has less than 24");
		stderr!("   hours of validity left");
		stderr!(" - If the original validity is between 72 hours and 90 days, the certificate");
		stderr!("   has less than one third of its lifetime left");
		stderr!(" - If the original validity is at least 90 days, the certificate has less than");
		stderr!("   30 days of validity left");
		stderr!("");
		stderr!("If missing certificate file is an error depends on if <source-csr> is");
		stderr!("specified or not. If it is not, a missing certificate is an error. However");
		stderr!("missing certificate is not an error if <source-csr> has been specified.");
		stderr!("");
		stderr!("If <source-csr> is specified, this program signals that the certificate");
		stderr!("should be renewed in two additional conditions:");
		stderr!(" - If the specified certificate file does not exist");
		stderr!(" - If the specified source CSR has been modified since the certificate file");
		stderr!("");
		stderr!("If <csrchksumfile> is specified, then <source-csr> is also required.");
		stderr!("This replaces the timestamp check with the following two checks:");
		stderr!(" - If the <csrchksumfile> does not exist");
		stderr!(" - If the first 64 octets of <csrchksumfile> do not case-insentively match");
		stderr!("   the hexadecimal SHA-256 checksum of the <source-csr>");
		stderr!("");
		stderr!("If <certificate> points to a directory, '/certificate' is automatically");
		stderr!("appended.");
		stderr!("");
		stderr!("If <adv-hours> is specified, then the renew is advanced by specified number of");
		stderr!("hours. This count can also be negative to retard renew. However, the renew can");
		stderr!("not be retarded below 18 hours");
		stderr!("");
		stderr!("The program prints a status line to stdout. This status line starts with one");
		stderr!("of the following, then contains a space and freeform message:");
		stderr!(" - 'OK':      Successful, not due for renewal. Exit value is 0.");
		stderr!(" - 'LOW':     Successful, due for renewal. Exit value is 1.");
		stderr!(" - 'ERROR':   Error. Exit value is 2.");
		//       +---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----
		exiterror!(E "Bad usage");
	}
	//Unwrap command line arguments.
	let mut certfile = certfile.unwrap();
	if certfile.is_dir() {
		//Append '/certificate' if <certificate> points to a directory.
		certfile = certfile.join("certificate");
	}
	//If csrchksum is specified, load it.
	let csrchksum: Option<String> = csrchksum.map(|fname|{
		let mut file = File::open(&fname).unwrap_or_else(|err|{
			if err.kind() == ErrorKind::NotFound {
				//This is special.
				exiterror!(L "Checksum file '{fname}' does not exist yet", fname=fname.display())
			} else {
				exiterror!(E "Error opening '{fname}': {err}", fname=fname.display())
			}
		});
		let mut buf = [0u8;64];
		if let Err(err) = file.read_exact(&mut buf) {
			exiterror!(L "Error reading 64 bytes from '{fname}': {err}", fname=fname.display())
		};
		//Translate to lowercase text string.
		buf.iter().cloned().map(|x|x.to_ascii_lowercase() as char).collect()
	});
	//If both certificate and source csr are specified, then the following changes occur:
	// - If source csr is newer than certificate, return CSR validation error.
	// - If source csr exists, but certificate does not, return CSR validation error.
	if let &Some(ref csrfile) = &csrfile {
		let csrfiled = csrfile.display();
		let csr_metadata = csrfile.metadata().unwrap_or_else(|err|{
			exiterror!(E "Error getting metadata for '{csrfiled}': {err}")
		});
		let cert_metadata = certfile.metadata().unwrap_or_else(|err|{
			if err.kind() == ErrorKind::NotFound {
				//This is special.
				exiterror!(L "Certificate '{csrfiled}' does not exist yet")
			} else {
				exiterror!(E "Error getting metadata for '{cert}': {err}", cert=certfile.display())
			}
		});
		let updated = if let Some(csrchksum) = csrchksum {
			let mut file = File::open(csrfile).unwrap_or_else(|err|{
				exiterror!(E "Error opening '{csrfiled}': {err}")
			});
			let mut content = Vec::new();
			if let Err(err) = file.read_to_end(&mut content) {
				exiterror!(E "Error reading '{csrfiled}': {err}")
			};
			hexencode(&sha256(&content)).deref() != csrchksum.deref()
		} else {
			let csr_mod = csr_metadata.modified().unwrap_or(SystemTime::now());
			let cert_mod = cert_metadata.modified().unwrap_or(SystemTime::now());
			csr_mod > cert_mod
		};
		if updated { exiterror!(L "Source CSR for '{csrfiled}' has been updated"); }
	}
	let cert = read_single_cert(&certfile).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});
	let pcert = ParsedCertificate2::from(&cert).unwrap_or_else(|err|{
		exiterror!(E "Certificate '{cert}' malformed: {err}", cert=certfile.display())
	});

	//Show status.
	let lifetime = pcert.not_before.delta(pcert.not_after);
	let lived = pcert.not_before.delta(Timestamp::now());
	let remain = lifetime.saturating_sub(lived);
	let days = ((remain + 4320) / 8640) as f64 / 10.0;
	let threshold = max(86400, min(lifetime / 3, 2592000));
	let threshold = max((threshold as i64) + advance, 64800) as u64;
	if remain <= 0 {
		exiterror!(L "Certificate EXPIRED (on {ts}).", ts=DecodeTsForUser2(pcert.not_after));
	} else if remain < threshold  {
		exiterror!(L "Certificate expiring soon (in {days} days, on {ts})",
			ts=DecodeTsForUser2(pcert.not_after));
	} else {
		exiterror!(OK "Certificate validity OK (expires in {days} days, on {ts})",
			ts=DecodeTsForUser2(pcert.not_after));
	}
}
