#![warn(unsafe_op_in_unsafe_fn)]
extern crate btls_aux_serialization;
#[macro_use] extern crate btls_certtool;
use btls_aux_serialization::Asn1Tag;
use btls_aux_serialization::Source;
use btls_certtool::ParseArgs;
use btls_certtool::do_world_umask;
use btls_certtool::encode_pem_cert;
use std::fs::File;
use std::io::Read;
use std::io::stdin;
use std::io::stderr;
use std::io::Write;
use std::path::Path;
use std::process::exit;


fn sanity_check_cert(cert: &[u8], is_selfsigned: &mut bool) -> Result<(), String>
{
	let mut cert = Source::new(cert);
	let mut top = cert.asn1(Asn1Tag::SEQUENCE).map_err(|err|{
		format!("Bad top-level SEQUENCE: {err}")
	})?;
	let tbsoff = top.global_position();
	let mut tbs = top.asn1(Asn1Tag::SEQUENCE).map_err(|err|{
		format!("Bad TBS SEQUENCE@{tbsoff}: {err}")
	})?;
	let mut icount = 0u32;
	let mut sequence_count = 0u32;
	let mut issuer = Vec::new();
	while tbs.more() {
		let offset = tbs.global_position();
		let n = tbs.asn1_tlv().map_err(|err|{
			format!("Bad TBS item #{icount}@{offset}: {err}")
		})?;
		if n.tag == Asn1Tag::SEQUENCE {
			sequence_count += 1;
			//The issuer name is 2nd SEQUENCE. The subject name is 4th SEQUENCE.
			if sequence_count == 2 { issuer = n.outer.to_owned(); }
			if sequence_count == 4 { *is_selfsigned = n.outer == &issuer[..]; }
		}
		icount += 1;
	}
	let offset = top.global_position();
	top.asn1_slice(Asn1Tag::SEQUENCE).map_err(|err|{
		format!("Bad Signature algorithm@{offset}: {err}")
	})?;
	let offset = top.global_position();
	top.asn1_slice(Asn1Tag::BIT_STRING).map_err(|err|{
		format!("Bad Signature@{offset}: {err}")
	})?;
	top.expect_end().map_err(|_|{
		format!("Junk after signature@{pos}", pos=top.global_position())
	})?;
	cert.expect_end().map_err(|_|{
		format!("Junk after certificate@{pos} (possibly multiple certificate chain?)",
			pos=cert.global_position())
	})?;
	Ok(())
}

static PROGNAME: &'static str = "certtool-pemcert";

fn main()
{
	let args = ParseArgs::new(PROGNAME);
	let progname = args.get_progname();

	let mut files = args.get_files();
	let outputfile = files.next();
	let fault = files.next().is_some();

	if outputfile.is_none() || fault {
		stderr!("Syntax: {progname} <output> <der-certificate.der");
		stderr!("");
		stderr!("This program reads one certificate in DER form from stdin and");
		stderr!("writes it out into <output> in PEM form. On unix-type systems, the");
		stderr!("created file is group and others readable. Also, basic sanity checks");
		stderr!("are performed on the certificate.");
		stderr!("");
		stderr!("The intended use of status SELFSIGNED is terminating chain");
		stderr!("building: Any self-signed certificate is probably root certificate,");
		stderr!("and thus should not be included in certificate chain.");
		stderr!("");
		stderr!("The program prints a status line to stdout. This status line starts with one");
		stderr!("of the following, then contains a space and freeform message:");
		stderr!(" - 'OK':         Successful. Exit value is 0.");
		stderr!(" - 'SELFSIGNED': Successful, but certificate is selfsigned. Exit value is 0.");
		stderr!(" - 'ERROR':      Error. Exit value is 2.");
		//                  +---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----
		exiterror!(E "Bad usage");
	}
	do_world_umask();
	let outputfile = outputfile.unwrap();
	let mut der = Vec::new();
	stdin().read_to_end(&mut der).unwrap_or_else(|err|{
		exiterror!(E "Error reading data from stdin: {err}")
	});
	let mut is_selfsigned = false;
	sanity_check_cert(&der, &mut is_selfsigned).unwrap_or_else(|err|{
		exiterror!(E "The input certificate is malformed: {err}")
	});
	let pem = encode_pem_cert(&der);
	File::create(&outputfile).and_then(|mut x|{
		x.write_all(&pem)
	}).unwrap_or_else(|err|{
		exiterror!(E "Error writing certificate '{out}': {err}", out=Path::new(&outputfile).display())
	});
	if is_selfsigned {
		println!("SELFSIGNED Certificate is self-signed");
	} else {
		println!("OK Certificate has a distinct issuer");
	}
}
