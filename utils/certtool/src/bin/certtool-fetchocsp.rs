#![warn(unsafe_op_in_unsafe_fn)]
use btls_aux_time::Timestamp;
use btls_aux_x509certparse::ParsedCertificate2;
use btls_certtool::exiterror;
use btls_certtool::path_append_extension;
use btls_certtool::ParseArgs;
use btls_certtool::stderr;
use btls_certtool::do_world_umask;
use btls_certtool::read_cert_file;
use btls_certtool::read_single_cert;
use btls_certtool::aia::aia_fetch_ocsp;
use btls_certtool::aia::AiaOcspError;
use btls_certtool::aia::ocsp_get_url2;
use btls_certtool::debug::get_debug_maybe;
use btls_certtool::debug::StderrDebug;
use std::fs::File;
use std::fs::rename;
use std::io::Read;
use std::io::stderr;
use std::io::Write;
use std::env::var;
use std::ops::Deref;
use std::path::Path;
use std::process::exit;


static PROGNAME: &'static str = "certtool-fetchocsp";
static ARG_SHOWURL: &'static str = "--showurl";

fn main()
{
	let args = ParseArgs::new(PROGNAME);
	let progname = args.get_progname();

	let show = args.is_option(ARG_SHOWURL).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});

	let mut files = args.get_files();
	let mut ocspfile = files.next();
	let mut certfile = files.next();
	let mut issfile = files.next();
	let mut fault = files.next().is_some();

	//If no <directory> case, --showurl pushes arguments forward.
	if certfile.is_none() && ocspfile.is_some() {
		//Special directory case.
		let _ocspfile = ocspfile.unwrap();
		certfile = Some(_ocspfile.join("certificate"));
		issfile = Some(_ocspfile.join("issuer"));
		ocspfile = Some(_ocspfile.join("ocsp"));
	} else if issfile.is_none() && certfile.is_some() {
		issfile = certfile.clone();
	} else if show {
		fault |= issfile.is_some();
		issfile = certfile;
		certfile = ocspfile;
		ocspfile = Some(Path::new("DUMMY OCSP OUTPUT FILE").to_owned());	//Avoid special cases.
	}

	if issfile.is_none() || fault {
		stderr!("Syntax: {progname} (<ocsp>|{ARG_SHOWURL}) <certificate> <issuer>");
		stderr!("Syntax: {progname} (<ocsp>|{ARG_SHOWURL}) <fullchain>");
		stderr!("Syntax: {progname} [{ARG_SHOWURL}] <directory>");
		stderr!("");
		stderr!("This program fetches the AIA OCSP for <certficate>, issued by <issuer>.");
		stderr!("Both <certificate> and <issuer> can be in either DER or PEM form.");
		stderr!("The resulting OCSP response is saved to <ocsp>.");
		stderr!("");
		stderr!("If the {ARG_SHOWURL} option is specified, the OCSP GET URL is just shown, not fetched");
		stderr!("");
		stderr!("On unx-type systems, the created file will be readable for group and others");
		stderr!("");
		stderr!("If <fullchain> is specified, then the first certificate read from that file");
		stderr!("is the end-entity certificate and the second is its issuer.");
		stderr!("");
		stderr!("If <directory> is specified instead of <certificate>/<issuer>/<ocsp>, then the");
		stderr!("program behaves as if the following values were given:");
		stderr!(" - <ocsp> = <directory>/ocsp");
		stderr!(" - <certificate> = <directory>/certificate");
		stderr!(" - <issuer> = <directory>/issuer");
		stderr!("");
		stderr!("The program prints a status line to stdout. This status line starts with one");
		stderr!("of the following, then contains a space and freeform message:");
		stderr!(" - <url>:     --showurl is specified, the OCSP url is <url>. Exit value is 0");
		stderr!("              (no trailing space and freeform message is present in this case)");
		stderr!(" - 'OK':      Successful, not due for renewal. Exit value is 0.");
		stderr!(" - 'REVOKED': Certificate has been REVOKED by the CA. Exit value is 2.");
		stderr!(" - 'NOAIA':   The certificate does not have OCSP AIA. Exit value is 2");
		stderr!(" - 'ERROR':   Error. Exit value is 2.");
		//                  +---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----
		exiterror!(E "Bad usage");
	}
	do_world_umask();


	//Convert ocspfile to string.
	let ocspfile = ocspfile.unwrap();

	let debug = get_debug_maybe(var("CERTTOOL_DEBUG").ok().as_deref(), StderrDebug::new_outer());
	let certfile = certfile.unwrap();
	let issfile = issfile.unwrap();
	let (cert1, cert2) = if certfile == issfile {
		let cert = read_cert_file(&certfile, false).unwrap_or_else(|err|{
			exiterror!(E "{err}")
		});
		let mut cert = cert.iter();
		let cert1 = cert.next();
		let cert2 = cert.next();
		match (cert1, cert2) {
			(Some(x), Some(y)) => (x.to_vec(), y.to_vec()),
			_ => exiterror!(E "Expected at least two certificates in '{iss}'", iss=issfile.display())
		}
	} else {
		//Indepedent files.
		let cert1 = read_single_cert(&certfile).unwrap_or_else(|err|{
			exiterror!(E "{err}")
		});
		let cert2 = read_single_cert(&issfile).unwrap_or_else(|err|{
			exiterror!(E "{err}")
		});
		(cert1, cert2)
	};
	if show {
		//Special show-only mode.
		let cert1 = ParsedCertificate2::from(&cert1).unwrap_or_else(|err|{
			exiterror!(E "Unable to parse subject certificate '{cert}': {err}", cert=certfile.display())
		});
		let cert2 = ParsedCertificate2::from(&cert2).unwrap_or_else(|err|{
			exiterror!(E "Unable to parse issuer certificate '{iss}': {err}", iss=issfile.display())
		});
		//Extract issuer key and check issuer.
		if !cert1.issuer.same_as(cert2.subject) {
			exiterror!(E "The issuer certificate did not issue the subject certificate.");
		}
		match ocsp_get_url2(&cert1, cert2.pubkey) {
			Ok(Some(url)) => println!("{url}"),
			Ok(None) => exiterror!(P "NOAIA", "No OCSP URL in certificate"),
			Err(err) => exiterror!(E "{err}")
		}
		return;		//Exit status 0.
	}
	let ocsp = aia_fetch_ocsp(&cert1, &cert2, Timestamp::now(), &debug).unwrap_or_else(|x| {
		match x {
			AiaOcspError::NoOcsp =>
				exiterror!(P "NOAIA", "No OCSP responder URL in subject certificate"),
			AiaOcspError::Revoked => exiterror!(P "REVOKED", "Certificate has been REVOKED"),
			AiaOcspError::Other(err) => exiterror!(E "{err}")
		};
	});
	//aia_fetch_ocsp already validates the response. OCSP is OK, link it.
	write_ocsp_file(&ocspfile, &ocsp);
	exiterror!(OK "Fetched OCSP to '{ocsp}'", ocsp=ocspfile.display());
}

fn write_ocsp_file(ocspfile: &Path, ocsp: &[u8])
{
	let mut old_ocsp = Vec::new();
	let changed = match File::open(&ocspfile).and_then(|mut fp|fp.read_to_end(&mut old_ocsp)) {
		Ok(_) => old_ocsp.deref() != ocsp,
		Err(_) => true				//If read fails, always assume changed.
	};
	if changed {
		//Ok, link basename to ocspfilel.
		let tempfile = path_append_extension(&ocspfile, "tmp");
		let source = &ocspfile;
		//Write OCSP response to tempfile.
		File::create(&tempfile).and_then(|mut f|{
			f.write_all(&ocsp)
		}).unwrap_or_else(|err|{
			exiterror!(E "Failed to write OCSP response to '{tmp}': {err}", tmp=tempfile.display())
		});
		//Rename the response.
		rename(&tempfile, &source).unwrap_or_else(|err|{
			exiterror!(E "Failed to rename '{src}' -> '{tgt}': {err}",
				src=tempfile.display(), tgt=source.display())
		});
	}
}
