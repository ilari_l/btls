#![warn(unsafe_op_in_unsafe_fn)]
use btls_certtool::exiterror;
use btls_certtool::ParseArgs;
use btls_certtool::stderr;
use std::fs::remove_file;
use std::io::stderr;
use std::io::Write;
use std::process::exit;

static PROGNAME: &'static str = "certtool-rmlink";

fn main()
{
	let args = ParseArgs::new(PROGNAME);
	let progname = args.get_progname();

	let mut files = args.get_files();
	let basename = files.next();
	let fault = files.next().is_some();

	if basename.is_none() || fault {
		stderr!("Syntax: {progname} <basename>");
		stderr!("");
		stderr!("This program atomically deletes the symbolic link <basename>");
		stderr!("This is essentially the same as using the rm command.");
		stderr!("");
		stderr!("Exit status 0 -> Successful");
		stderr!("Exit status 2 -> Error");
		stderr!("");
		stderr!("The program prints a status line to stdout. This status line starts with one");
		stderr!("of the following, then contains a space and freeform message:");
		stderr!(" - 'OK':      Successful, not due for renewal. Exit value is 0.");
		stderr!(" - 'ERROR':   Error. Exit value is 2.");
		//                  +---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----
		exiterror!(E "Bad usage");
	}
	let basename = basename.unwrap();
	remove_file(&basename).unwrap_or_else(|err|{
		exiterror!(E "Can't remove '{basename}': {err}", basename=basename.display())
	});
	exiterror!(OK "Link '{basename}' deleted", basename=basename.display());
}
