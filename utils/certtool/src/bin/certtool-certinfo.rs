#![warn(unsafe_op_in_unsafe_fn)]
extern crate btls;
extern crate btls_aux_hash;
extern crate btls_aux_publiccalist;
extern crate btls_aux_serialization;
extern crate btls_aux_signatures;
extern crate btls_aux_time;
extern crate btls_aux_x509certparse;
#[macro_use] extern crate btls_certtool;
use btls::certificates::PGP_WORDLIST;
use btls_aux_fail::f_return;
use btls_aux_hash::sha256;
use btls_aux_memory::Hexdump;
use btls_aux_memory::SafeShowByteString;
use btls_aux_publiccalist::is_public_ca;
use btls_aux_serialization::Asn1Tag;
use btls_aux_serialization::Source;
use btls_aux_signatures::iterate_pem_or_derseq_fragments;
use btls_aux_signatures::PemDerseqFragment2 as PFragment;
use btls_aux_signatures::PemFragmentKind as PFKind;
use btls_aux_signatures::SignatureAlgorithmX5092;
use btls_aux_x509certparse::DecodeTsForUser2;
use btls_aux_x509certparse::dn_to_text;
use btls_aux_x509certparse::ParsedCertificate2;
use btls_aux_x509certparse::TranslateAsn1Oid;
use btls_certtool::ParseArgs;
use btls_certtool::read_file2;
use btls_certtool::csr::get_key_type;
use std::fmt::Display;
use std::io::stderr;
use std::io::Write;
use std::path::Path;
use std::process::exit;


static FEATURE: [&'static str; 5] = [
	"must-staple",
	"must-ems",
	"must-tls13",
	"must-renego",
	"must-ct",
];

fn really_unknown_signature_algorithm(alg: &[u8]) -> String
{
	format!("ERROR ({alg:?})")
}

fn unknown_signature_algorithm(x: &[u8]) -> String
{
	let mut src = Source::new(x);
	let oid = f_return!(src.asn1_slice(Asn1Tag::OID), really_unknown_signature_algorithm(x));
	let params = src.read_remaining();
	if params.len() > 0 {
		format!("Unknown ({oid}[{params:?}])", oid=TranslateAsn1Oid(oid))
	} else {
		format!("Unknown ({oid})", oid=TranslateAsn1Oid(oid))
	}
}

fn signature_algorithm(x: &[u8]) -> String
{
	if let Some(y) = SignatureAlgorithmX5092::by_x509_oid(x) {
		y.to_generic().as_string().to_owned()
	} else {
		unknown_signature_algorithm(x)
	}
}

fn get_issuer_status(x: &[u8]) -> &'static str
{
	if is_public_ca(x) { "public" } else { "private" }
}

fn base64char(val: u8) -> char
{
	val.wrapping_add(match val {
		0..=25 => 65,
		26..=51 => 71,
		52..=61 => 252,
		62 => 237,
		63 => 240,
		_ => unreachable!(),
	}) as char
}

fn as_base64(data: &[u8]) -> String
{
	let mut ret = String::new();
	for i in 0..data.len() / 3 {
		let a = data[3*i+0];
		let b = data[3*i+1];
		let c = data[3*i+2];
		ret.push(base64char(a >> 2));
		ret.push(base64char(((a << 4) | (b >> 4)) & 63));
		ret.push(base64char(((b << 2) | (c >> 6)) & 63));
		ret.push(base64char(c & 63));
	}
	if data.len() % 3 == 1 {
		let a = data[data.len()-1];
		ret.push(base64char(a >> 2));
		ret.push(base64char((a << 4) & 63));
		ret.push('=');
		ret.push('=');
	}
	if data.len() % 3 == 2 {
		let a = data[data.len()-2];
		let b = data[data.len()-1];
		ret.push(base64char(a >> 2));
		ret.push(base64char(((a << 4) | (b >> 4)) & 63));
		ret.push(base64char((b << 2) & 63));
		ret.push('=');
	}
	ret
}

fn as_pgp_words(data: &[u8]) -> String
{
	let mut s = String::new();
	let mut linelen = 0;
	const MAX_LINE: usize = 50;
	let mut parity = false;
	for i in data.iter() {
		let idx = (*i as usize) * 2 + if parity { 1 } else { 0 };
		let word = PGP_WORDLIST[idx];
		if linelen + 1 + word.len() > MAX_LINE {
			s.push_str("\n                          ");
			linelen = 0;
		} else if linelen > 0 {
			s.push(' ');
			linelen += 1;
		}
		s.push_str(word);
		linelen += word.len();
		parity = !parity;
	}
	s
}

static PROGNAME: &'static str = "certtool-certinfo";

fn main()
{
	let args = ParseArgs::new(PROGNAME);
	let progname = args.get_progname();

	let mut files = args.get_files();
	let certfile = files.next();
	let fault = files.next().is_some();

	if certfile.is_none() || fault {
		stderr!("Syntax: {progname} <cert>");
		stderr!("");
		stderr!("This program extracts information about Certificate <cert> and prints it.");
		stderr!("The certifcate can be in either DER or PEM form.");
		stderr!("");
		stderr!("On success, this tool prints the information to stdout and exits with status 0");
		stderr!("");
		stderr!("On error, this tool prints a status line that starts with 'ERROR ' and then");
		stderr!("contains an error message. It also exits with status 2.");
		//       +---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----
		exiterror!(E "Bad usage");
	}
	let certfile = certfile.unwrap();
	//Read the Cert and unwrap PEM.
	let contents = read_file2(&certfile).unwrap_or_else(|err|{
		exiterror!(E "Failed to read '{certfile}': {err}", certfile=Path::new(&certfile).display())
	});
	let mut n = 1;
	use self::PFragment::*;
	iterate_pem_or_derseq_fragments(&contents, |frag|match frag {
		Error => Err(format!("Not in PEM nor DERseq format")),
		Der(cert)|Pem(PFKind::Certificate, cert) => {
			print_certificate(cert, Path::new(&certfile).display(), n);
			Ok(n+=1)
		},
		_ => Ok(()),
	}).unwrap_or_else(|err|{
		exiterror!(E "{certfile}: {err}", certfile=Path::new(&certfile).display())
	});
}

fn print_certificate(contents: &[u8], path: impl Display, n: usize)
{
	let parsed = ParsedCertificate2::from(contents).unwrap_or_else(|err|{
		exiterror!(E "Failed to parse '{path}'(#{n}): {err}")
	});

	let pkhash = sha256(parsed.pubkey);
	let serial = Hexdump(parsed.serial_number);
	let iid = sha256(parsed.issuer.as_raw_issuer());
	let iid = Hexdump(&iid);
	if n > 1 { println!("-------------------------------------------------------------------------------"); }
	println!("Serial number:            {serial}");
	println!("Signature algorithm:      {alg}", alg=signature_algorithm(parsed.sig_algo1));
	println!("Issuer ID:                {iid}");
	println!("Issuer:                   {issuer} [{status}]",
		issuer=dn_to_text(parsed.issuer.as_raw_issuer()).unwrap_or_else(|_|"<ERROR>".to_owned()),
		status=get_issuer_status(parsed.issuer.as_raw_issuer()));
	println!("Validity:                 {from} to {to}",
		from=DecodeTsForUser2(parsed.not_before), to=DecodeTsForUser2(parsed.not_after));
	println!("Subject ID:               {sid}", sid=Hexdump(&sha256(parsed.subject.as_raw_subject())));
	println!("Subject:                  {subj}",
		subj=dn_to_text(parsed.subject.as_raw_subject()).unwrap_or_else(|_|"<ERROR>".to_owned()));
	println!("Public key type:          {ptype}", ptype=get_key_type(&parsed.pubkey));
	println!("Public key type (CP):     {palgo}", palgo=parsed.pubkey_algo);
	println!("Public key (SHA-256):     {hash}", hash=Hexdump(&pkhash));
	println!("Public key (PGPWords):    {hash}", hash=as_pgp_words(&pkhash));
	println!("HPKP pin:                 pin-sha256=\"{hash}\"", hash=as_base64(&pkhash));
	println!("Assume-revoked line:      assume-revoked {iid} {serial}");
	for name in parsed.dnsnames.iter() {
	println!("Valid for DNS name:       {name}", name=SafeShowByteString(name));
	}
	for addr in parsed.dnsnames.iter_addrs() {
	println!("Valid for IP address:     {addr}");
	}
	for name in parsed.names_allow.iter() {
	println!("Allowed for DNS tree:     {name}", name=SafeShowByteString(name));
	}
	for prefix in parsed.names_allow.iter_addrs() {
	println!("Allowed for IP prefix:    {prefix}");
	}
	for name in parsed.names_allow.iter() {
	println!("Disallowed for DNS tree:  {name}", name=SafeShowByteString(name));
	}
	for prefix in parsed.names_allow.iter_addrs() {
	println!("Disallowed for IP prefix: {prefix}");
	}
	print!(  "Usage:                   ");
	if parsed.use_tls_serv {
		print!(" TLS_SERVER");
	}
	if parsed.use_tls_client {
		print!(" TLS_CLIENT");
	}
	if parsed.use_ocsp {
		print!(" OCSP");
	}
	if parsed.use_any {
		print!(" ANY_PROTOCOL");
	}
	if parsed.can_sign {
		print!(" SIGN_DATA");
	}
	if parsed.is_ca {
		print!(" CA");
	}
	if parsed.constrained {
		print!(" CONSTRAINED");
	}
	if parsed.nsa {
		print!(" NSA");
	}
	println!("");
	if let Some(mpl) = parsed.max_path_len {
	println!("Maximum path length:      {mpl}");
	}
	print!(  "Features:                ");
	for i in 0..FEATURE.len() {
		if parsed.req_flags >> i & 1 != 0 {
			print!(" {feature}", feature=FEATURE[i]);
		}
	}
	if parsed.req_flags >> (FEATURE.len() as u32) != 0 {
		print!(" <UNKNOWN>");
	}
	if parsed.req_flags == 0 {
		print!(" <none>");
	}
	println!("");
	for sct in parsed.scts.iter() {
	println!("Certificate Transparency: {sct}", sct=Hexdump(sct));
	}
	if let Some(aiaiss) = parsed.aia_issuer {
	println!("Issuer certificate URL:   {aiaiss}");
	}
	if let Some(ocsp) = parsed.aia_ocsp {
	println!("AIA OCSP responder:       {ocsp}");
	}
	if let Some(acme) = parsed.acme_tls_alpn {
	println!("ACME validation token:    {acme}", acme=Hexdump(acme));
	}

	println!("ToBeSigned SHA-256:       {thash}", thash=Hexdump(&sha256(parsed.tbs)));
	//No need to print signature info, as algorithm is already available.
	println!("Certificate SHA-256:      {chash}", chash=Hexdump(&sha256(parsed.entiere)));
}
