#![warn(unsafe_op_in_unsafe_fn)]
use btls_aux_dnsmsg::consts::DNSCLASS_IN;
use btls_aux_memory::SafeShowByteString;
use btls_certtool::deadline::Deadline;
use btls_certtool::dns::DnsClass;
use btls_certtool::dns::find_related_name;
use btls_certtool::dns::RelatedNameKind;
use btls_certtool::debug::StderrDebug;
use btls_certtool::debug::get_debug_maybe;
use btls_certtool::debug::Debugging;
use std::env::var;
use std::time::Duration;
use std::ops::Deref;
use std::net::Ipv6Addr;
use std::net::Ipv4Addr;

fn show_name(x: &[u8]) -> String
{
	SafeShowByteString(x).to_string()
}

fn show_ip(x: &[u8]) -> String
{
	if x.len() == 16 {
		let mut y = [0;16]; y.copy_from_slice(x);
		Ipv6Addr::from(y).to_string()
	} else if x.len() == 4 {
		let mut y = [0;4]; y.copy_from_slice(x);
		Ipv4Addr::from(y).to_string()
	} else if x.len() == 0 {
		format!("No address found")
	} else {
		format!("???")
	}
}

fn _fulltest(name: &str, debug: &Debugging) -> Result<(), String>
{
	let deadline = Deadline::new(Duration::from_secs(1000));
	let clazz = DNSCLASS_IN;
	let name = name.strip_prefix("*.").unwrap_or(name);
	let name = format!("_acme-challenge.{name}");
	let name2 = match find_related_name(name.as_bytes(), clazz, RelatedNameKind::Canonical, &debug, deadline) {
		Ok(name2) => {
			println!("Ok, {name} is cnamed to {target}", target=show_name(&name2));
			name2
		},
		Err(err) => return Ok(eprintln!("Error determining CNAME for {name}: {err}"))
	};
	let name3 = match find_related_name(&name2, clazz, RelatedNameKind::ZoneRoot, &debug, deadline) {
		Ok(name3) => {
			println!("Ok, {name2} is in zone {name3}", name2=show_name(&name2), name3=show_name(&name3));
			name3
		},
		Err(err) => return Ok(eprintln!("Error determining zone {name2} is in: {err}",
			name2=show_name(&name2)))
	};
	let name4 = match find_related_name(&name3, clazz, RelatedNameKind::Nameserver, &debug, deadline) {
		Ok(name4) => {
			println!("Ok, {name3} is served by {name4}", name3=show_name(&name3), name4=show_name(&name4));
			name3
		},
		Err(err) => return Ok(eprintln!("Error determining server for {name3}: {err}",
			name3=show_name(&name3)))
	};
	_determine_addresses(&name4, debug, clazz)?;
	println!("PASSED");
	Ok(())
}

fn _determine_addresses(name4: &[u8], debug: &Debugging, clazz: DnsClass) -> Result<(), String>
{
	let deadline = Deadline::new(Duration::from_secs(1000));
	let addr1 = match find_related_name(&name4, clazz, RelatedNameKind::Ipv4Address, &debug, deadline) {
		Ok(addr1) => addr1,
		Err(err) => return Ok(eprintln!("Error determining IPv4 address for {name4}: {err}",
			name4=show_name(&name4)))
	};
	let addr2 = match find_related_name(&name4, clazz, RelatedNameKind::Ipv6Address, &debug, deadline) {
		Ok(addr2) => addr2,
		Err(err) => return Ok(eprintln!("Error determining IPv6 address for {name4}: {err}",
			name4=show_name(&name4)))
	};
	match (addr1.len(), addr2.len()) {
		(4,16) => {
			let mut y = [0;4]; y.copy_from_slice(&addr1);
			let mut z = [0;16]; z.copy_from_slice(&addr2);
			println!("{name4} has IP addresses {ipv4} and {ipv6}",
				name4=show_name(&name4), ipv4=Ipv4Addr::from(y), ipv6=Ipv6Addr::from(z));
		},
		(4,0) => {
			let mut y = [0;4]; y.copy_from_slice(&addr1);
			println!("{name4} has IP address {ipv4}", name4=show_name(&name4), ipv4=Ipv4Addr::from(y));
		},
		(0,16) => {
			let mut z = [0;16]; z.copy_from_slice(&addr2);
			println!("{name4} has IP address {ipv6}", name4=show_name(&name4), ipv6=Ipv6Addr::from(z));
		},
		(_,_) => return Ok(eprintln!("No IP addresses found for {name4}", name4=show_name(&name4)))
	}
	Ok(())
}

fn fulltest(name: &str, debug: &Debugging)
{
	if let Err(err) = _fulltest(name, debug) {
		eprintln!("DNS error: {err}");
	}
}

fn determine_addresses(name: &str, debug: &Debugging, clazz: DnsClass)
{
	if let Err(err) = _determine_addresses(name.as_bytes(), debug, clazz) {
		eprintln!("DNS error: {err}");
	}
}

fn main()
{
	let args: Vec<String> = std::env::args().collect();
	let name: &str = &args[2];
	let debug = get_debug_maybe(var("CERTTOOL_DEBUG").ok().as_deref(), StderrDebug::new_outer());
	let (kind, op) = match args[1].deref() {
		"cname" => (RelatedNameKind::Canonical, show_name as fn(&[u8])->String),
		"root" => (RelatedNameKind::ZoneRoot, show_name as fn(&[u8])->String),
		"ns" => (RelatedNameKind::Nameserver, show_name as fn(&[u8])->String),
		"ipv4" => (RelatedNameKind::Ipv4Address, show_ip as fn(&[u8])->String),
		"ipv6" => (RelatedNameKind::Ipv6Address, show_ip as fn(&[u8])->String),
		"ip" => return determine_addresses(name, &debug, DNSCLASS_IN),
		"acme" => return fulltest(name, &debug),
		op => panic!("Bad operation {op}")
	};
	match find_related_name(name.as_bytes(), DNSCLASS_IN, kind, &debug, Deadline::new(Duration::from_secs(1000))) {
		Ok(name) => println!("{result}", result=op(&name)),
		Err(err) => eprintln!("DNS error: {err}"),
	}
}
