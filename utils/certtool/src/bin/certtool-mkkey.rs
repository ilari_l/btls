#![warn(unsafe_op_in_unsafe_fn)]
use btls_aux_filename::Filename;
use btls_aux_unix::AclPermEntry;
use btls_aux_unix::Gid;
use btls_aux_unix::Path as UnixPath;
use btls_aux_unix::Uid;
use btls_aux_unix::Umask;
use btls_certtool::exiterror;
use btls_certtool::ParseArgs;
use btls_certtool::stderr;
use btls_certtool::debug::get_debug_maybe;
use btls_certtool::debug::StderrDebug;
use btls_certtool::privatekey::generate_and_dump_key;
use std::env::var;
use std::fs::File as StdFile;
use std::io::stderr;
use std::io::Write;
use std::path::Path;
use std::process::exit;
use std::str::FromStr;


const DEFAULT_KEYBITS: usize = 256;

macro_rules! stderr
{
	($x:expr) => {{ let _ = writeln!(stderr(), $x); () }};
	($x:expr, $($a:tt)*) => { let _ = writeln!(stderr(), $x, $($a)*); };
}

macro_rules! abort
{
	($x:expr) => {{ stderr!($x); exit(1); }};
	($x:expr, $($a:tt)*) => {{ stderr!($x, $($a)*); exit(1); }};
}

struct DummyAclRead;

impl ::btls_aux_unix::AclRead for DummyAclRead
{
	fn set_user(&mut self, _: AclPermEntry) {}
	fn set_users(&mut self, _: Uid, _: AclPermEntry) {}
	fn set_group(&mut self, _: AclPermEntry) {}
	fn set_groups(&mut self, _: Gid, _: AclPermEntry) {}
	fn set_other(&mut self, _: AclPermEntry) {}
}

struct Acls
{
	user: AclPermEntry,
	group: AclPermEntry,
	other: AclPermEntry,
	users: Vec<(Uid, AclPermEntry)>,
	groups: Vec<(Gid, AclPermEntry)>,
}

impl ::btls_aux_unix::AclWrite for Acls
{
	fn get_user(&self) -> AclPermEntry { self.user }
	fn get_users<F>(&self, mut f: F) where F: FnMut(Uid, AclPermEntry)
	{
		for (uid, perm) in self.users.iter().cloned() { f(uid, perm); }
	}
	fn get_group(&self) -> AclPermEntry { self.group }
	fn get_groups<F>(&self, mut f: F) where F: FnMut(Gid, AclPermEntry)
	{
		for (gid, perm) in self.groups.iter().cloned() { f(gid, perm); }
	}
	fn get_other(&self) -> AclPermEntry { self.other }
}

fn set_privileges_acl(name: &Path, _name: &UnixPath, grantu: Vec<String>, grantg: Vec<String>, was_group: bool)
	-> Result<(), ()>
{
	use btls_aux_unix::OsError;

	let s = _name.stat().map_err(|err|{
		stderr!("Stat '{name}' failed: {err}", name=name.display());
	})?;
	//Map grants to users. Also, remove entries for owners.
	let grantu = grantu.iter().filter_map(|name|{
		let uid = Uid::from_str(&name);
		let uid = uid.unwrap_or_else(||{
			abort!("No user named '{name}' in user database")
		});
		if uid == s.uid() { None } else { Some((uid, AclPermEntry::R)) }
	}).collect::<Vec<_>>();
	let grantg = grantg.iter().filter_map(|name|{
		let gid = Gid::from_str(&name);
		let gid = gid.unwrap_or_else(||{
			abort!("No group named '{name}' in user database")
		});
		if gid == s.gid() { None } else { Some((gid, AclPermEntry::R)) }
	}).collect::<Vec<_>>();

	//Attribute data.
	let attrib = Acls {
		user: AclPermEntry::R,
		group: if was_group { AclPermEntry::R } else { AclPermEntry::NONE },
		other: AclPermEntry::NONE,
		users: grantu,
		groups: grantg,
	};
	if attrib.users.len() == 0 && attrib.groups.len() == 0 {
		//Read ACLs. If there are no ACLs return quietly.
		match _name.getfacl(&mut DummyAclRead) {
			Ok(_) => (),
			//ENODATA and OPNOTSUPP special.
			Err(err) if err == OsError::ENODATA => return Ok(()),
			Err(err) if err == OsError::EOPNOTSUPP => return Ok(()),
			Err(err) => {
				stderr!("get ACL list from '{name}' failed: {err}", name=name.display());
				return Err(());
			}
		}
	}
	_name.setfacl(&attrib).map_err(|err|{
		stderr!("set ACL list to '{name}' failed: {err}", name=name.display());
	})?;
	Ok(())
}

fn set_privileges(oname: &Path, group: Option<String>, grantu: Vec<String>, grantg: Vec<String>) -> Result<(), ()>
{
	let onamed = oname.display();
	let path = Filename::from_path(oname).into_unix_path().ok_or_else(||{
		stderr!("Illegal file path '{onamed}'");
	})?;
	if let &Some(ref group) = &group {
		let uid = Uid::get_real();
		let gid = match Gid::from_str(group) { Some(x) => x, None => {
			stderr!("Group '{group}' does not exist in user database");
			return Err(());
		}};
		path.chown(uid, gid).map_err(|err|{
			stderr!("Chgrp '{onamed}' to '{group}' failed: {err}");
		})?;
		path.chmod(288).map_err(|err|{
			stderr!("Chmod '{onamed}' to 440 failed: {err}");
		})?;
	} else {
		path.chmod(256).map_err(|err|{
			stderr!("Chmod '{onamed}' to 400 failed: {err}");
		})?;
	}
	set_privileges_acl(oname, path, grantu, grantg, group.is_some())
}

static PROGNAME: &'static str = "certtool-mkkey";
static ARG_BITS: &'static str = "--bits=";
static ARG_GROUP: &'static str = "--group=";
static ARG_GRANT: &'static str = "--grant=";
static ARG_GRANTGRP: &'static str = "--grant-group=";
static ARG_ECC: &'static str = "--ecc";
static ARG_ECCH: &'static str = "--ecc-high";
static ARG_ECCV: &'static str = "--ecc-veryhigh";
static ARG_CFRG: &'static str = "--cfrg-ecc";
static ARG_CFRGH: &'static str = "--cfrg-ecc-high";

fn main()
{
	let args = ParseArgs::new(PROGNAME);
	let pname = args.get_progname();

	let mut bits = args.getopt_single_str(ARG_BITS).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});
	let group = args.getopt_single_str(ARG_GROUP).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});

	let sopt_ecc = args.is_option(ARG_ECC).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});
	let sopt_ecc_h = args.is_option(ARG_ECCH).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});
	let sopt_ecc_v = args.is_option(ARG_ECCV).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});
	let sopt_cfrg_ecc = args.is_option(ARG_CFRG).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});
	let sopt_cfrg_ecc_h = args.is_option(ARG_CFRGH).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});

	if sopt_cfrg_ecc_h { bits = Some("448".to_owned()); }
	if sopt_cfrg_ecc { bits = Some("255".to_owned()); }
	if sopt_ecc_v { bits = Some("521".to_owned()); }
	if sopt_ecc_h { bits = Some("384".to_owned()); }
	if sopt_ecc { bits = Some("256".to_owned()); }

	let mut grantusr = Vec::new();
	let mut grantgrp = Vec::new();
	for name in args.getopt_multi_str(ARG_GRANT) { grantusr.push(name); }
	for name in args.getopt_multi_str(ARG_GRANTGRP) { grantgrp.push(name); }

	let mut files = args.get_files();
	let keyfile = files.next();
	let fault = files.next().is_some();

	if keyfile.is_none() || fault {
		stderr!("Syntax: {pname} <output> [{ARG_BITS}<bits>] [{ARG_GROUP}<group>]");
		stderr!("\t[{ARG_GRANT}<user>...] [{ARG_GRANTGRP}<group>...]");
		stderr!("");
		stderr!("This program creates a cryptographic signature key and writes it in");
		stderr!("PEM format to <output>. If <output> already exists, it is not");
		stderr!("overwritten. However, the {ARG_GROUP}, {ARG_GRANT} and {ARG_GRANTGRP}");
		stderr!("options are still processed.");
		stderr!("");
		stderr!("On unix-type systems, unless {ARG_GROUP} is specified, the created file will", );
		stderr!("be only readable by the user. However, {ARG_GRANT} and {ARG_GRANTGRP} will");
		stderr!("still grant read privileges.");
		stderr!("");
		stderr!("The number of bits in generated key is set by {ARG_BITS} option. If not");
		stderr!("specified, the default is 256.");
		stderr!("");
		stderr!("Using {ARG_BITS} values other than 256 or 384 is NOT RECOMMENDED.");
		stderr!("");
		stderr!("In addition to {ARG_BITS}, the following options may be used to set key size:");
		stderr!(" * {ARG_CFRGH}: (Really do not use) Ed448 key");
		stderr!(" * {ARG_CFRG}: (Do not use) Ed25519 key");
		stderr!(" * {ARG_ECCV}: (Do not use) 521 bit ECC key");
		stderr!(" * {ARG_ECCH}: 384 bit ECC key");
		stderr!(" * {ARG_ECC}: 256 bit ECC key");
		stderr!("");
		stderr!("All the previous options override {ARG_BITS} and later ones override former ones.");
		stderr!("");
		stderr!(" The key bits are interpretted as follows:");
		stderr!(" 255 -> Ed25519 key (poorly supported)");
		stderr!(" 256 -> NSA P-256 ECDSA key (almost universally supported)");
		stderr!(" 384 -> NSA P-384 ECDSA key (almost universally supported)");
		stderr!(" 448 -> Ed448 key (very poorly supported)");
		stderr!(" 521 -> NSA P-521 ECDSA key (poorly supported)");
		stderr!("");
		stderr!("{ARG_GROUP}<group>: gives read permission to key to group <group>.");
		stderr!("                 Note that you either need to be member of that group or");
		stderr!("                 superuser.");
		stderr!("{ARG_GRANT}<user> gives read permission to key to user <user>.");
		stderr!("               Note that this only works on Linux and if the filesystem");
		stderr!("               supports POSIX ACLs.");
		stderr!("{ARG_GRANTGRP}<group> gives read permission to key to group user>");
		stderr!("               Note that this only works on Linux and if the filesystem");
		stderr!("               supports POSIX ACLs.");
		stderr!("");
		stderr!("On success, exits with status 0. On error, exits with status 1.");
		exit(1);
	}
	let keyfile = keyfile.unwrap();
	let keyf = keyfile.display();

	//Check if keyfile already exists...
	if keyfile.exists() {
		stderr!("The output file '{keyf}' already exists, not regenerating");
		stderr!("Setting permissions of file '{keyf}'");
		if let Err(_) = set_privileges(&keyfile, group, grantusr, grantgrp) {
			stderr!("WARNING: Failed to set the privileges of '{keyf}'");
		}
		exit(1);
	}

	let bits = if let &Some(ref val) = &bits {
		usize::from_str(val).unwrap_or_else(|_|{
			stderr!("Warning: Invalid number of bits '{val}'");
			DEFAULT_KEYBITS
		})
	} else {
		DEFAULT_KEYBITS
	};

	let debug = get_debug_maybe(var("CERTTOOL_DEBUG").ok().as_deref(), StderrDebug::new_outer());
	let key = generate_and_dump_key(bits, true, debug.clone()).unwrap_or_else(|err|{
		abort!("Failed to generate private key: {err}")
	});
	//Ok, save the key. First always set umask to 077, so to avoid giving excessive privileges at any point.
	Umask::PRIVATE.swap();
	StdFile::create(&keyfile).and_then(|mut x|{
		x.write_all(&key)?;
		x.flush()?;
		Ok(())
	}).unwrap_or_else(|err|{
		abort!("Error writing out '{keyf}': {err}")
	});
	if let Err(_) = set_privileges(&keyfile, group, grantusr, grantgrp) {
		stderr!("WARNING: Failed to set the privileges of '{keyf}'")
	}
}
