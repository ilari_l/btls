#![warn(unsafe_op_in_unsafe_fn)]
use acme_common::load_configuration;
use acme_daemon::daemon_round;
use acme_daemon::ReqDirTag;
use acme_daemon::StateDirTag;
use acme_daemon::TimePriorityQueue;
use acme_daemon::write_to_syslog;
use btls_aux_collections::Arc;
use btls_aux_collections::Mutex;
use btls_aux_fail::f_return;
use btls_aux_time::DecodeTsForUser2;
use btls_aux_time::ShowTime;
use btls_aux_time::wait_for_unix_ts;
use btls_aux_unix::Logger;
use btls_aux_unix::LogLevel;
use btls_aux_unix::RawSignalT;
use btls_aux_unix::Signal;
use btls_aux_unix::SignalHandler;
use btls_aux_unix::SignalSet;
use btls_aux_unix::usleep;
use btls_certtool::critical;
use btls_certtool::do_world_umask;
use btls_certtool::info;
use btls_certtool::notice;
use btls_certtool::debug::Debugging;
use btls_certtool::debug::DebuggingTarget;
use btls_certtool::debug::severity_string;
use std::cmp::min;
use std::fmt::Arguments;
use std::fmt::Write as FmtWrite;
use std::fs::File;
use std::io::Error as IoError;
use std::io::Write as IoWrite;
use std::path::Path;
use std::path::PathBuf;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering as MemOrdering;
use std::sync::mpsc::channel;
use std::sync::mpsc::Receiver;
use std::sync::mpsc::Sender;
use std::thread::Builder;
use std::time::Duration;
use std::time::Instant;


#[path="../acme-common.rs"] mod acme_common;
#[path="../acme-daemon.rs"] mod acme_daemon;
#[path="../acme-install.rs"] mod acme_install;
#[path="../acme-legacy.rs"] mod acme_legacy;
#[path="../acme-newconfig.rs"] mod acme_newconfig;
#[path="../acme-order.rs"] mod acme_order;


struct DaemonLogSender
{
	sender: Sender<(ShowTime, char, String)>,
}

impl DaemonLogSender
{
	fn log_message(&mut self, priority: char, msg: String)
	{
		self.sender.send((ShowTime::now(), priority, msg)).ok();
	}
}

#[derive(Clone)]
struct DaemonLog(Arc<Mutex<DaemonLogSender>>);

impl DebuggingTarget for DaemonLog
{
	fn out(&self, priority: char, args: Arguments)
	{
		//Log priority 0-2 to syslog.
		let sysprio = match priority {
			'0' => Some(LogLevel::Emergency),
			'1' => Some(LogLevel::Alert),
			'2' => Some(LogLevel::Critical),
			_ => None
		};
		if let Some(sysprio) = sysprio { Logger::Syslog.log(None, sysprio, args); }
		let msg = args.to_string();
		self.0.with(|inner|{
			inner.log_message(priority, msg);
		});
	}
	fn clone_self(&self) -> Box<dyn DebuggingTarget+'static>
	{
		Box::new(self.clone())
	}
}

fn log_error(action: &str, target: &Path, err: IoError)
{
	Logger::Syslog.log(None, LogLevel::Critical, format_args!("Failed to {action} log {t}: {err}",
		t=target.display()));
}

fn append_to(target: &Path) -> Option<File>
{
	match File::options().append(true).create(true).open(target) {
		Ok(file) => Some(file),
		Err(err) => {
			log_error("open", target, err);
			None
		},
	}
}

fn log_thread(target: &Path, recv: Receiver<(ShowTime, char, String)>)
{
	let mut logfile = append_to(target);
	loop {
		let mut messages: Vec<(ShowTime, char, String)> = Vec::new();
		//Wait for first message in train. On disconnect, shut down the log.
		match recv.recv() {
			Ok(msg) => messages.push(msg),
			Err(_) => break,
		}
		//Read the subsequent messages in the train. Stop reading when there are no more messages. If this
		//is disconnect, then the next iteration of the loop above will quit.
		while let Ok(msg) = recv.try_recv() { messages.push(msg); }
		//Okay, now format the messages into a single buffer to write.
		let mut buffer = String::new();
		for (ts, priority, msg) in messages.drain(..) {
			let priority = severity_string(priority);
			writeln!(buffer, "{ts} {priority}: {msg}", ts=ts.format3()).ok();
		}
		let buffer = buffer.into_bytes();
		//Okay, now the messages are in buffer. Check if the target file exists. If it does not, reopen.
		let need_reopen = !target.is_file();
		if need_reopen || logfile.is_none() { logfile = append_to(target); }
		//Write the messages
		if let Some(logfile) = logfile.as_mut() {
			if let Err(err) = logfile.write_all(&buffer) { log_error("write", target, err); }
		}
	}
}

fn create_logger(target: &Path) -> DaemonLog
{
	let (sender, recv) = channel::<(ShowTime, char, String)>();
	let sender = DaemonLogSender{sender};
	let sender = DaemonLog(Arc::new(Mutex::new(sender)));
	let target = target.to_owned();
	if let Err(err) = Builder::new().spawn(move ||{
		//Block SIGHUP and SIGTERM, as main thread uses those.
		let mut set = SignalSet::new();
		set.add(Signal::HUP);
		set.add(Signal::TERM);
		set.block_signals().ok();
		//Run the thread.
		log_thread(&target, recv);
	}) {
		Logger::Syslog.log(None, LogLevel::Critical, format_args!("Failed to start log thread: {err}"));
	}
	sender
}

/*******************************************************************************************************************/
macro_rules! syslog_i
{
	($($fmt:tt)*) => { write_to_syslog(format_args!($($fmt)*), LogLevel::Informational) }
}

fn __silence_warnings()
{
	if false {
		let a = ReqDirTag::new(PathBuf::new());
		let b = StateDirTag::new(PathBuf::new());
		let _ = a.display_path();
		let _ = b.deref_inner();
	}
}


fn main()
{
	__silence_warnings();
	do_world_umask();
	let uid = crate::acme_common::handle_setuid();

	//Initialize the built-in auth modules.
	btls_certtool::authmod::initialize_auth_modules();

	crate::acme_common::homedir_setup(uid);

	//Force the context to be inner in order to log properly to SystemD. Since everything is in-process, can load
	//the keys too.
	let (mut globalcfg, certscfg) = load_configuration(true);
	globalcfg.call_load_key();
	certscfg.call_load_key();
	crate::acme_daemon::init_syslog();
	//Create a logger and set the thing to log to it.
	let log = create_logger(&globalcfg.logsdir.join("daemon.log"));
	globalcfg.debug.out = Box::new(log);
	globalcfg.daemon = true;
	//Set up signal handling.
	unsafe{Signal::HUP.set_handler(SignalHandler::Function(sighup_handler))};
	unsafe{Signal::TERM.set_handler(SignalHandler::Function(sigterm_handler))};

	const SCALE: u64 = 1_000_000;
	const ISCALE: u64 = 1_000_000_000 / SCALE;
	let mut renew_q = TimePriorityQueue::new();
	loop {
		match daemon_round(&globalcfg, &certscfg, &mut renew_q) {
			Ok(_) => info!(globalcfg.debug, "Run successful"),
			Err(err) => {
				if err.len() > 0 {
					critical!(globalcfg.debug, "Run failed: {err}");
				} else {
					critical!(globalcfg.debug, "Run FAILED, see logfile");
				}
			},
		}
		//This default only comes to play if there are no requests.
		let mut dwait = 1_000_000 * SCALE;
		let next_ts = renew_q.first_timestamp();
		if let Some(next_ts) = next_ts {
			info!(globalcfg.debug, "Next event at {t}", t=DecodeTsForUser2(next_ts));
			let ns = wait_for_unix_ts(next_ts.unix_time());
			let usecs = (ns + ISCALE - 1) / ISCALE;
			dwait = min(usecs, dwait);
		}
		let t = dwait / 1_000_000;
		if t < 1 {
			syslog_i!("Waiting {dwait}µs");
		} else if t < 60 {
			syslog_i!("Waiting {t}s");
		} else if t < 3600 {
			syslog_i!("Waiting {t}min", t=t/60);
		} else if t < 86400 {
			syslog_i!("Waiting {t}h", t=t/3600);
		} else {
			syslog_i!("Waiting {t}d", t=t/86400);
		}
		f_return!(wait(dwait, &globalcfg.debug), ());
	}
}

static SIGHUP_RECEIVED: AtomicBool = AtomicBool::new(false);
static SIGTERM_RECEIVED: AtomicBool = AtomicBool::new(false);
extern fn sighup_handler(_: RawSignalT) { SIGHUP_RECEIVED.store(true, MemOrdering::SeqCst); }
extern fn sigterm_handler(_: RawSignalT) { SIGTERM_RECEIVED.store(true, MemOrdering::SeqCst); }

fn handle_signals(debug: &Debugging) -> Result<bool, ()>
{
	if SIGHUP_RECEIVED.swap(false, MemOrdering::SeqCst) {
		notice!(debug, "Got SIGHUP, performing extra run");
		return Ok(true);
	}
	if SIGTERM_RECEIVED.swap(false, MemOrdering::SeqCst) {
		notice!(debug, "Got SIGTERM, exiting");
		return Err(());
	}
	Ok(false)
}

fn check_sigterm()
{
	if SIGTERM_RECEIVED.swap(false, MemOrdering::SeqCst) { std::process::exit(0); }
}

fn wait(microseconds: u64, debug: &Debugging) -> Result<(), ()>
{
	info!(debug, "Waiting for {mins} minutes for next run...", mins = microseconds / 60_000_000);
	let deadline = Instant::now() + Duration::from_micros(microseconds);
	loop {
		let now = Instant::now();
		let remaining = if deadline > now { deadline.duration_since(now) } else {
			info!(debug, "Performing timed run");
			break;
		};
		let usecs = remaining.as_secs() * 1000000 + remaining.subsec_nanos() as u64 / 1000;
		let usecs = min(usecs, 0xFFFFFFFF);
		//If there is pending signal, do not wait anymore.
		if handle_signals(debug)? { break; }
		usleep(usecs as _).ok();	//Remainder will be checked anyway.
	}
	Ok(())
}


fn do_commit_fault() -> !
{
	Logger::Syslog.log(None, LogLevel::Alert, format_args!("ACME: CATASTROPHIC ERROR; DATABASE IS CORRUPT"));
	loop { usleep(1_000_000_000).ok(); }	//Halt.
}
