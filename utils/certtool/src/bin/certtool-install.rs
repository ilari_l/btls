#![warn(unsafe_op_in_unsafe_fn)]
use btls_aux_signatures::iterate_pem_or_derseq_fragments;
use btls_aux_signatures::PemDerseqFragment2 as PFragment;
use btls_aux_signatures::PemFragmentKind as PFKind;
use btls_aux_time::Timestamp;
use btls_aux_x509certparse::ParsedCertificate2;
use btls_certtool::CertChain;
use btls_certtool::exiterror;
use btls_certtool::ParseArgs;
use btls_certtool::path_append_extension;
use btls_certtool::get_dh_params;
use btls_certtool::read_cert_file;
use btls_certtool::stderr;
use btls_certtool::encode_pem_cert;
use btls_certtool::symlink_equiv;
use btls_certtool::do_world_umask;
use btls_certtool::next_directory_filename;
use btls_certtool::fetch_http_file;
use btls_certtool::deadline::Deadline;
use btls_certtool::debug::get_debug_maybe;
use btls_certtool::debug::StderrDebug;
use btls_certtool::aia::aia_issuer_chain;
use btls_certtool::aia::aia_fetch_ocsp;
use btls_certtool::aia::AiaOcspError;
use btls_certtool::aia::IntermediatePile;
use btls_certtool::strings::AsciiString;
use btls_certtool::wcheck::do_validate_versus_roots;
use btls_certtool::wcheck::Rootpile;
use btls_certtool::wcheck::override_fields_from_csr;
use btls_certtool::wcheck::ValidateRootError;
use std::collections::BTreeMap;
use std::io::stderr;
use std::io::Read;
use std::io::Write;
use std::io::ErrorKind as IoErrorKind;
use std::env::var;
use std::path::Path;
use std::fs::File;
use std::fs::rename;
use std::fs::create_dir;
use std::process::exit;
use std::str::FromStr;
use std::time::Duration;


fn read_text_file(base: &Path, name: &str) -> Option<AsciiString>
{
	let mut out = None;
	match File::open(base.join(name)) {
		Ok(mut x) => {
			let mut val = String::new();
			x.read_to_string(&mut val).unwrap_or_else(|err|{
				exiterror!(E "Failed to read '{base}/{name}': {err}", base=base.display())
			});
			let val = AsciiString::from_string(val).unwrap_or_else(|bad|{
				exiterror!(E "Failed to read '{base}/{name}': Bad contents {bad}", base=base.display())
			});
			out = Some(val);
		},
		Err(err) => if err.kind() != IoErrorKind::NotFound {
			exiterror!(E "Failed to read '{base}/{name}': {err}", base=base.display())
		}
	}
	out
}

static ETAG: &'static str = "etag";
static LASTMOD: &'static str = "lastmod";
static PROGNAME: &'static str = "certtool-install";
static ARG_FORCE_ETAG: &'static str = "--force-etag";
static ARG_EXP_MODIFY: &'static str = "--expect-modify";
static ARG_ADVANCE: &'static str = "--advance=";
static ARG_ROOT: &'static str = "--root=";
static ARG_BLACKLIST: &'static str = "--blacklist=";
static ARG_PILE: &'static str = "--pile=";
static ARG_NAME: &'static str = "--name=";
static ARG_CSR: &'static str = "--csr=";
static ARG_KEY: &'static str = "--key=";
static ARG_SUBJECT: &'static str = "--subject=";
const FETCH_TIMEOUT: Duration = Duration::from_millis(25000);

fn main()
{
	let debug = get_debug_maybe(var("CERTTOOL_DEBUG").ok().as_deref(), StderrDebug::new_outer());

	let args = ParseArgs::new(PROGNAME);
	let progname = args.get_progname();

	let force = args.is_option(ARG_FORCE_ETAG).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});
	let expect_modify = args.is_option(ARG_EXP_MODIFY).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});

	let mut advance = None;
	if let Some(name) = args.getopt_single_str(ARG_ADVANCE).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	}) {
		let (name, multiplier) = if let Some(name) = name.strip_suffix("m") {
			(name, 60)
		} else if let Some(name) = name.strip_suffix("h") {
			(name, 3600)
		} else if let Some(name) = name.strip_suffix("d") {
			(name, 86400)
		} else {
			(&name[..], 1)
		};
		advance = Some(u64::from_str(name).ok().and_then(|c|{
			c.checked_mul(multiplier)
		}).unwrap_or_else(||{
			exiterror!(E "Failed to parse {ARG_ADVANCE}")
		}));
	}

	let mut rootpile = Rootpile::new();
	let mut validate_rootpile = false;
	for name in args.getopt_multi_path(ARG_ROOT) {
		rootpile.add_file(&name, &debug).unwrap_or_else(|err|{
			exiterror!(E "{err}")
		});
		validate_rootpile = true;
	}

	let mut pile = IntermediatePile::new();
	for name in args.getopt_multi_path(ARG_BLACKLIST) {
		let mut file = File::open(&name).unwrap_or_else(|err|{
			exiterror!(E "Failed to open blacklist file '{name}': {err}'", name=name.display())
		});
		pile.read_blacklist_from_file(&mut file).unwrap_or_else(|err|{
			exiterror!(E "Failed to read blacklist file '{name}': {err}'", name=name.display())
		});
	}

	for name in args.getopt_multi_path(ARG_PILE) {
		let mut file = File::open(&name).unwrap_or_else(|err|{
			exiterror!(E "Failed to open pile file '{name}': {err}'", name=name.display())
		});
		pile.read_intermediate_pile_from_file(&mut file, &debug).unwrap_or_else(|err|{
			exiterror!(E "Failed to read pile file '{name}': {err}'", name=name.display())
		});
	}

	let mut namelist = Vec::new();
	for name in args.getopt_multi_str(ARG_NAME) { namelist.push(name); }

	let csr = args.getopt_single_path(ARG_CSR).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});
	let mut key = args.getopt_single_str(ARG_KEY).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});
	let mut subject = args.getopt_single_str(ARG_SUBJECT).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});

	let mut files = args.get_files();
	let targetfile = files.next();
	let srcfile = files.next();
	let keyfile = files.next();
	let fault = files.next().is_some();

	if srcfile.is_none() || fault {
		stderr!("Syntax: {progname} <target-basename> <source-cert> [<keyfile>]");
		stderr!("\t[{ARG_BLACKLIST}<blacklist>] [{ARG_PILE}<pile>] [{ARG_ROOT}<root>");
		stderr!("\t[{ARG_ADVANCE}<advance>] [{ARG_NAME}<namespec>...] [{ARG_KEY}<keyhash>]");
		stderr!("\t[{ARG_CSR}<templatecsr>] [{ARG_SUBJECT}<subject>]]");
		stderr!("\t[{ARG_EXP_MODIFY}] [{ARG_FORCE_ETAG}]");
		stderr!("");
		stderr!("This program reads the certificate <source-cert> (which can be in either");
		stderr!("DER or PEM form) and fetches the issuer certificates and OCSP for it.");
		stderr!("Finally it creates directory named <target-basename>-<nnnn>, where <nnnn>");
		stderr!("is the next sequence number and writes the files there. Finally it updates");
		stderr!("the symbolic link <target-basename> to point to the created directory.");
		stderr!("");
		stderr!("The <source-cert> can also be a http:// URL. In this case, the resource is");
		stderr!("fetched first. Due to http:// having no security, using --root=<root> is");
		stderr!("recommended.");
		stderr!("");
		stderr!("On unix-type systems, the created files are group and other readable");
		stderr!("");
		stderr!("If <keyfile> is specified, then the directory will also have a file");
		stderr!("called key, which will be a symbolic link into this file.");
		stderr!("");
		stderr!("If <blacklist> is specified, the specified file is read and any non-empty");
		stderr!("lines that do not start with # are added to root certificate URL blacklist.");
		stderr!("Any URLs on this blacklist are assumed to be root certificates and not");
		stderr!("downloaded.");
		stderr!("");
		stderr!("If <pile> is specified, then the certificates contained in the (PEM format)");
		stderr!("file are read. These certificates are used to complete the certificate");
		stderr!("chains. Note that these certificates are assumed to form complete chains.");
		stderr!("That is, if the chain is incomplete, it is not completed. Also, if the issuer");
		stderr!("certificate is found from this pile, AIA in the certificate is ignored.");
		stderr!("If the same certificate is added multiple times, the last non-root certificate");
		stderr!("is used.");
		stderr!("");
		stderr!("If <root> is specified, then the constructed certificate chain is verified");
		stderr!("with the specified roots. These roots can be PEM or DER format. If a");
		stderr!("directory is specified, then all *.pem and *.der files in it are loaded");
		stderr!("as roots.");
		stderr!("");
		stderr!("If <advance> is specified with <root>, then the certificate chain is verified");
		stderr!("for a second time, with the time advanced specified amount of time into the");
		stderr!("future. This is intended to limit the minimum time the certificate can be valid");
		stderr!("for. The number can be suffixed with 'm', 'h' or 'd' for minutes, hours or days.");
		stderr!("E.g., '2592000', '43200m', '720h' and '30d' all mean 30 days.");
		stderr!("");
		stderr!("If <namespec> is specified with <root>, the the certificate chain is verified");
		stderr!("to contain name <namespec>, either directly or via a wildcard. If <namespec>");
		stderr!("itself is a wildcard, then it only matches the corresponding wildcard.");
		stderr!("This option can be specified multiple times to verify multiple names.");
		stderr!("");
		stderr!("If <keyhash> is specified with <root>, then the end-entity certificate is");
		stderr!("verified to have that specific SubjectPublicKeyInfo fingerprint (64 hexadecimal");
		stderr!("digits of SHA-256 hash). One can get the fingerprint for example from the");
		stderr!("'key fingerprint' field of certtool-csrinfo output");
		stderr!("");
		stderr!("If <subject> is specified with <root>, then the certificate subject will be");
		stderr!("checked against that value.");
		stderr!("");
		stderr!("If <templatecsr> is specified with <root>, the subject, key and names are");
		stderr!("checked against the datums found in that CSR");
		stderr!("");
		stderr!("If {ARG_EXP_MODIFY} is specified and HTTP returns Not Modified status, then an");
		stderr!("error is raised instead of exiting successfully.");
		stderr!("");
		stderr!("If {ARG_FORCE_ETAG} is specified, then HTTP modification checks are skipped.");
		stderr!("");
		stderr!("The following files are created in the new directory:");
		stderr!(" - certificate: The end-entity certificate (PEM). If RSA key, will include");
		stderr!("                Appropriate Diffie-Hellman parameters");
		stderr!(" - issuer:      The issuer certificate chain (PEM)");
		stderr!(" - full.pem:    Combination of end-entity certificate and issuer chain (PEM)");
		stderr!("                Appropriate Diffie-Hellman parameters are included for RSA");
		stderr!("                keys.");
		stderr!(" - key:         Symbolic link to key (only if <keyfile> is specified>)");
		stderr!("");
		stderr!("Note that you ALWAYS either need certificate+issuer or full.pem.");
		stderr!("Certificate alone is NOT sufficient.");
		stderr!("");
		stderr!("The program prints a status line to stdout. This status line starts with one");
		stderr!("of the following, then contains a space and freeform message:");
		stderr!(" - 'OK':      Successful. Exit value is 0.");
		stderr!(" - 'ERROR':   Error. Exit value is 2.");
		//       +---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----
		exiterror!(E "Bad usage");
	}
	do_world_umask();

	if let Some(csr) = csr.as_ref() {
		override_fields_from_csr(csr, &mut namelist, &mut key, &mut subject).unwrap_or_else(|err|{
			exiterror!(E "{err}")
		});
	}

	let targetfile = targetfile.unwrap();
	let srcfile = srcfile.unwrap();

	//Check that targetfile is not a directory.
	if targetfile.symlink_metadata().map(|x|{
		x.is_dir()
	}).unwrap_or(false) {
		exiterror!(E "{target} already points to a directory", target=targetfile.display());
	}
	//We have to load the certificate, and fetch the AIA chain.
	let strsrc = srcfile.to_str().unwrap_or("");	//Not URL if not unicode.
	let (cert1, etag, lastmod) = if strsrc.starts_with("http://") || strsrc.starts_with("https://") {
		//There may be nontrivial etag associated with HTTP URL. Load existing etag if any.
		let mut etag = read_text_file(&targetfile, ETAG);
		let mut lastmod = read_text_file(&targetfile, LASTMOD);
		let srcfile = AsciiString::new_osstring(&srcfile.clone().into_os_string()).unwrap_or_else(|bad|{
			exiterror!(E "Bad URL {bad}")
		});
		if force { etag = None; lastmod = None; }	//Erase etags if requested.
		let contents = fetch_http_file(&srcfile, &mut etag, &mut lastmod, &debug,
			Deadline::new(FETCH_TIMEOUT)).unwrap_or_else(|err|{
			if err.len() > 0 {
				exiterror!(E "{err}")
			} else if expect_modify {
				exiterror!(E "Certificate '{srcfile}' not modified, but different version was \
					expected!");
			} else {
				exiterror!(OK "Certificate '{srcfile}' not modified");
			}
		});
		//Unwrap PEM if any.
		let mut contents2 = CertChain::new();
		use self::PFragment::*;
		iterate_pem_or_derseq_fragments(&contents, |frag|match frag {
			Comment(_) => Ok(()),
			Error => Err(format!("Not in PEM nor DERseq format")),
			Der(cert)|Pem(PFKind::Certificate, cert) => Ok(contents2.push(cert)),
			Pem(kind, _) => Err(format!("Unrecognized type '{kind}'")),
		}).unwrap_or_else(|err|{
			exiterror!(E "{srcfile}: {err}")
		});
		(contents2, etag, lastmod)
	} else {
		//There never is nontrivial etag associated with file.
		let cert = read_cert_file(&srcfile, false).unwrap_or_else(|err|{
			exiterror!(E "{err}")
		});
		(cert, None, None)
	};
	//Check that cert1 does not contain multiple elements, since we mess up if so.
	let mut cert1b = cert1.iter();
	let cert1 = cert1b.next().unwrap_or_else(||{
		exiterror!(E "Invalid certificate to install: No certificates")
	});
	let mut tmpcerts = BTreeMap::new();
	while let Some(_icert) = cert1b.next() {
		let icert = ParsedCertificate2::from(_icert).unwrap_or_else(|err|{
			exiterror!(E "Invalid certificate to install: {err}")
		});
		//Nasty hack to circumvent borrow checker.
		let subj = icert.subject.as_raw_subject();
		let soffset = subj.as_ptr() as usize - _icert.as_ptr() as usize;
		let eoffset = soffset + subj.len();
		tmpcerts.insert(&_icert[soffset..eoffset], _icert);
	}
	//Special: Just use empty chain if aia_issuer_chain does not find anything.
	let chain = aia_issuer_chain(&pile, &cert1, tmpcerts, &debug).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	}).unwrap_or_else(||{
		CertChain::new()
	});

	if validate_rootpile {
		do_validate_versus_roots(&cert1, &chain, &rootpile, advance, &namelist[..], key, subject, &debug).
			unwrap_or_else(|err|match err {
			ValidateRootError::UnknownRoot => exiterror!(E "No matching root certificate found"),
			ValidateRootError::Other(err) => exiterror!(E "{err}"),
		});
	}

	//Load the OCSP response. For this, we need the immediate issuer chain.
	let chunk = chain.iter().next();
	let ocsp = if let Some(cert2) = chunk {
		aia_fetch_ocsp(&cert1, &cert2, Timestamp::now(), &debug).unwrap_or_else(|x|match x {
			AiaOcspError::NoOcsp => Vec::new(),	//Special, empty OCSP response.
			AiaOcspError::Revoked => exiterror!(E "Certificate has been REVOKED"),
			AiaOcspError::Other(err) => {
				stderr!("Warning: OCSP fetch failed: {err}");
				Vec::new()
			}
		})
	} else {
		Vec::new()	//Special, empty OCSP response.
	};

	//Grab the DH parameters.
	let dhparams = get_dh_params(&cert1);
	//Convert the certificate into PEM format.
	let mut cert1 = encode_pem_cert(&cert1);
	cert1.extend_from_slice(&dhparams);
	//Convert the chain into PEM format.
	let mut out = Vec::new();
	for chunk in chain.iter() { out.append(&mut encode_pem_cert(&chunk)); }

	//Find the parent directory of specified path and the file name.
	let root = next_directory_filename(&targetfile).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});
	let rootd = root.display();
	do_world_umask();
	create_dir(&root).unwrap_or_else(|err|{
		exiterror!(E "Can't make directory '{rootd}': {err}")
	});
	//Write the certificate files.
	File::create(root.join("certificate")).and_then(|mut f|{
		f.write_all(&cert1)
	}).unwrap_or_else(|err|{
		exiterror!(E "Failed to write '{rootd}/certificate': {err}")
	});
	File::create(root.join("issuer")).and_then(|mut f|{
		f.write_all(&out)
	}).unwrap_or_else(|err|{
		exiterror!(E "Failed to write '{rootd}/issuer': {err}")
	});
	File::create(root.join("full.pem")).and_then(|mut f|{
		f.write_all(&cert1)?;
		f.write_all(&out)?;
		Ok(())
	}).unwrap_or_else(|err|{
		exiterror!(E "Failed to write '{rootd}/full.pem': {err}")
	});
	if ocsp.len() > 0 {
		File::create(root.join("ocsp")).and_then(|mut f|{
			f.write_all(&ocsp)
		}).unwrap_or_else(|err|{
			exiterror!(E "Failed to write '{rootd}/ocsp': {err}")
		});
	}
	if let Some(etag) = etag.as_ref() {
		File::create(root.join(ETAG)).and_then(|mut f|{
			f.write_all(etag.as_bytes())
		}).unwrap_or_else(|err|{
			exiterror!(E "Failed to write '{rootd}/{ETAG}': {err}")
		});
	}
	if let Some(lastmod) = lastmod.as_ref() {
		File::create(root.join(LASTMOD)).and_then(|mut f|{
			f.write_all(lastmod.as_bytes())
		}).unwrap_or_else(|err|{
			exiterror!(E "Failed to write '{rootd}/{LASTMOD}': {err}")
		});
	}
	if let Some(keyfile) = keyfile {
		symlink_equiv(&keyfile, root.join("key")).unwrap_or_else(|err|{
			exiterror!(E "Failed to symlink '{rootd}/key': {err}")
		});
	}
	//Ok, link root to targetfile.
	let relroot = Path::new(root.file_name().unwrap_or(root.as_os_str()));
	let relrootd = relroot.display();
	let tempfile = path_append_extension(&targetfile, "tmp");
	let tempfiled = tempfile.display();
	let targetfiled = targetfile.display();
	symlink_equiv(&relroot, &tempfile).unwrap_or_else(|err|{
		exiterror!(E "Failed to symlink '{tempfiled}' -> '{relrootd}': {err}")
	});
	//Rename the symlink.
	rename(&tempfile, &targetfile).unwrap_or_else(|err|{
		exiterror!(E "Failed to rename '{tempfiled}' -> '{targetfiled}': {err}")
	});
	exiterror!(OK "Installed '{src}' as '{targetfiled}'({rootd})", src=srcfile.display());
}
