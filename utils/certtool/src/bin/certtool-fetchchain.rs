#![warn(unsafe_op_in_unsafe_fn)]
use btls_aux_x509certparse::ParsedCertificate2;
use btls_certtool::CertChain;
use btls_certtool::exiterror;
use btls_certtool::ParseArgs;
use btls_certtool::stderr;
use btls_certtool::do_world_umask;
use btls_certtool::read_single_cert;
use btls_certtool::encode_pem_cert;
use btls_certtool::aia::aia_issuer_chain;
use btls_certtool::aia::IntermediatePile;
use btls_certtool::debug::get_debug_maybe;
use btls_certtool::debug::StderrDebug;
use btls_certtool::strings::AsciiStr;
use std::collections::BTreeMap;
use std::fs::File;
use std::io::stderr;
use std::io::Write;
use std::env::var;
use std::ops::Deref;
use std::path::Path;
use std::process::exit;


static PROGNAME: &'static str = "certtool-fetchchain";
static ARG_SHOWURL: &'static str = "--showurl";
static ARG_FULL: &'static str = "--full";
static ARG_BLACKLIST: &'static str = "--blacklist=";
static ARG_PILE: &'static str = "--pile=";

fn main()
{
	let debug = get_debug_maybe(var("CERTTOOL_DEBUG").ok().as_deref(), StderrDebug::new_outer());

	let args = ParseArgs::new(PROGNAME);
	let progname = args.get_progname();

	let show = args.is_option(ARG_SHOWURL).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});
	let full = args.is_option(ARG_FULL).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});

	let mut pile = IntermediatePile::new();
	for name in args.getopt_multi_path(ARG_BLACKLIST) {
		let mut file = File::open(&name).unwrap_or_else(|err|{
			exiterror!(E "Failed to open blacklist file '{name}': {err}'", name=name.display())
		});
		pile.read_blacklist_from_file(&mut file).unwrap_or_else(|err|{
			exiterror!(E "Failed to read blacklist file '{name}': {err}'", name=name.display())
		});
	}
	for name in args.getopt_multi_path(ARG_PILE) {
		let mut file = File::open(&name).unwrap_or_else(|err|{
			exiterror!(E "Failed to open pile file '{name}': {err}'", name=name.display())
		});
		pile.read_intermediate_pile_from_file(&mut file, &debug).unwrap_or_else(|err|{
			exiterror!(E "Failed to read pile file '{name}': {err}'", name=name.display())
		});
	}

	let mut files = args.get_files();
	let mut targetfile = files.next();
	let mut certfile = files.next();
	let mut fault = files.next().is_some();

	//--showurl pushes arguments forward.
	if show {
		fault |= certfile.is_some();
		certfile = targetfile;
		targetfile = Some(Path::new("DUMMY CERT OUTPUT FILE").to_owned());	//Avoid special cases.
	}

	if certfile.is_none() || fault {
		stderr!("Syntax: {progname} [{ARG_FULL}] (<target>|{ARG_SHOWURL}) <certificate>");
		stderr!("        [{ARG_BLACKLIST}<blacklist>] [{ARG_PILE}<pile>]");
		stderr!("");
		stderr!("This program read the AIA issuer information in the certificate <certificate>");
		stderr!("and fetches the certificate chain if any. The resulting chain is stored in PEM");
		stderr!("format to <target>");
		stderr!("");
		stderr!("If {ARG_SHOWURL} is specified, then no file will be saved, but the issuer AIA URL");
		stderr!("will be printed. {ARG_FULL} and {ARG_PILE}<pile> are ignored in this mode. If the");
		stderr!("certificate does not have issuer AIA, NOAIA status will be given, even if the");
		stderr!("pile would have contained the issuer. Also, blacklists are still evaluated.");
		stderr!(" If blacklist matches, NOAIA status will be given.");
		stderr!("");
		stderr!("If {ARG_FULL} is specified, then the saved chain will also include the");
		stderr!("<certificate>.");
		stderr!("");
		stderr!("On unx-type systems, the created file will be readable for group and others");
		stderr!("");
		stderr!("If <blacklist> is specified, the specified file is read and any non-empty");
		stderr!("lines that do not start with # are added to root certificate URL blacklist.");
		stderr!("Any URLs on this blacklist are assumed to be root certificates and not");
		stderr!("downloaded.");
		stderr!("");
		stderr!("If <pile> is specified, then the certificates contained in the (PEM format)");
		stderr!("file are read. These certificates are used to complete the certificate");
		stderr!("chains. Note that these certificates are assumed to form complete chains.");
		stderr!("That is, if the chain is incomplete, it is not completed. Also, if the issuer");
		stderr!("certificate is found from this pile, AIA in the certificate is ignored.");
		stderr!("If the same certificate is added multiple times, the last non-root certificate");
		stderr!("is used.");
		stderr!("");
		stderr!("The program prints a status line to stdout. This status line starts with one");
		stderr!("of the following, then contains a space and freeform message:");
		stderr!(" - 'OK':      Successful, not due for renewal. Exit value is 0.");
		stderr!(" - 'NOAIA':   The certificate does not have issuer AIA. Exit value is 2");
		stderr!(" - 'ERROR':   Error. Exit value is 2.");
		//       +---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----
		exiterror!(E "Bad usage");
	}
	do_world_umask();

	let targetfile = targetfile.unwrap();
	let certfile = certfile.unwrap();
	let cert1 = read_single_cert(&certfile).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});
	//Special show-only mode.
	if show {
		let cert1 = ParsedCertificate2::from(&cert1).unwrap_or_else(|err|{
			exiterror!(E "Unable to parse subject certificate '{cert}': {err}", cert=certfile.display())
		});
		if let &Some(ref issurl) = &cert1.aia_issuer {
			//Still consider the blacklist.
			let issurl = AsciiStr::new_string(issurl.deref()).unwrap_or_else(|_|{
				exiterror!(P "NOAIA", "Bad issuer URL in certificate")
			});
			if pile.is_blacklisted(issurl.deref()) {
				exiterror!(P "NOAIA", "Issuer certificate blacklisted as root");
			} else {
				println!("{issurl}");
			}
		} else {
			exiterror!(P "NOAIA", "No issuer URL in certificate");
		}
		return;		//Exit status is 0.
	}
	let chain = aia_issuer_chain(&pile, &cert1, BTreeMap::new(), &debug).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	}).unwrap_or_else(||{
		exiterror!(P "NOAIA", "No issuer certificate known (checked local and AIA)")
	});
	//Note, the chain is in concatenated DER format. Prepend end-entity certificate if needed.
	let chain = if full {
		let mut out = CertChain::new();
		out.push(&cert1);
		out.push_multiple(chain.iter());
		out
	} else {
		chain
	};
	//Convert the chain into PEM format.
	let mut out = Vec::new();
	for chunk in chain.iter() { out.append(&mut encode_pem_cert(&chunk)); }

	//Write the response to file.
	File::create(&targetfile).and_then(|mut f|{
		f.write_all(&out)
	}).unwrap_or_else(|err|{
		exiterror!(E "Failed to write chain to '{target}': {err}", target=targetfile.display())
	});
	exiterror!(OK "Fetched chain to '{target}'", target=targetfile.display());
}
