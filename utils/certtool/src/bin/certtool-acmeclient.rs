#![warn(unsafe_op_in_unsafe_fn)]
use acme_common::AcmeServiceData;
use acme_common::AcmeServiceDataCache;
use acme_common::GlobalConfiguration;
use acme_common::load_configuration;
use acme_common::PerCertificateConfigurations;
use acme_daemon::csr_list;
use acme_daemon::do_maintenance_run;
use acme_daemon::f2_request;
use acme_daemon::init_syslog;
use acme_daemon::is_noninteractive;
use acme_daemon::run_post_renew;
use acme_daemon::run_post_install;
use acme_daemon::time_limit_variation;
use acme_daemon::write_to_syslog;
use acme_order::AuthzConfiguration;
use acme_order::OrderTestMode;
use acme_order::parse_csr;
use acme_order::raw_sign_csr;
use acme_order::run_certificate;
use btls_aux_fail::f_return;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_filename::Filename;
use btls_aux_filename::FilenameBuf;
use btls_aux_memory::SafeShowByteString;
use btls_aux_time::ShowTime;
use btls_aux_time::Timestamp;
use btls_aux_time::wait_for_unix_ts;
use btls_aux_time::DecodeTsForUser2;
use btls_aux_unix::FileDescriptor;
use btls_aux_unix::Logger;
use btls_aux_unix::LogLevel;
use btls_aux_unix::OpenFlags;
use btls_aux_unix::Path as UnixPath;
use btls_aux_unix::Pid;
use btls_aux_unix::PipeFlags;
use btls_aux_unix::SpecialFd;
use btls_aux_unix::STDERR;
use btls_aux_unix::usleep;
use btls_aux_unix::WaitStatus;
use btls_certtool::do_world_umask;
use btls_certtool::error;
use btls_certtool::exiterror;
use btls_certtool::info;
use btls_certtool::debug::Debugging;
use btls_certtool::debug::ForEachLine;
use btls_certtool::debug::get_debug_maybe;
use btls_certtool::debug::severity_string;
use btls_certtool::debug::StderrDebug;
use std::cell::RefCell;
use std::collections::BTreeMap;
use std::env::args_os;
use std::env::var;
use std::env::var_os;
use std::fs::File;
use std::fs::rename;
use std::io::Read as IoRead;
use std::io::Write as IoWrite;
use std::io::stderr;
use std::ops::Deref;
use std::ops::DerefMut;
use std::path::PathBuf;
use std::process::exit;
use std::rc::Rc;
use std::str::FromStr;
use std::str::from_utf8;


#[path="../acme-common.rs"] mod acme_common;
#[path="../acme-daemon.rs"] mod acme_daemon;
#[path="../acme-install.rs"] mod acme_install;
#[path="../acme-legacy.rs"] mod acme_legacy;
#[path="../acme-newconfig.rs"] mod acme_newconfig;
#[path="../acme-order.rs"] mod acme_order;

fn test_failed(csr: &Filename, err: String, flag: &mut bool, debug: &Debugging)
{
	*flag = true;
	error!(debug, "Testing {csr}: FAILED: {err}");
	write_to_syslog(format_args!("Testing {csr}: FAILED: {err}"), LogLevel::Error);
}

fn __is_special_test(test: OrderTestMode, csr: &Filename) -> bool
{
	test == OrderTestMode::RenewTest && csr.starts_with("/")
}

fn oneoff_sign_csr(filename: &Filename, gcfg: &GlobalConfiguration, ca: &str) -> Result<(), String>
{
	let mut rawcsr = Vec::new();
	File::open(filename.into_path()).and_then(|mut fp|fp.read_to_end(&mut rawcsr)).map_err(|err|{
		format!("Failed to read CSR: {err}")
	})?;
	let (info, _) = parse_csr(&rawcsr, &filename).map_err(|err|{
		format!("Failed to parse CSR: {err}")
	})?;
	let g = AuthzConfiguration {
		debug: gcfg.debug.clone(),
		auth_modules: &gcfg.auth_modules,
		replace: &gcfg.replace,
	};
	let acme: Rc<RefCell<AcmeServiceData>> = gcfg.ca_list.get(ca).ok_or_else(||{
		format!("Failed to find ca named {ca}")
	})?.0.clone();
	let statedir = gcfg.statedir.deref_inner();
	let mut acme = acme.borrow().to_acme_registered(statedir, gcfg.debug.clone(), false).map_err(|err|{
		format!("Failed to register ACME account: {err}")
	})?;
	let certificate = raw_sign_csr(&filename, &info, &mut acme, &g, true).map_err(|err|{
		format!("Failed to sign CSR: {err}")
	})?;
	//Use SafeShowByteString, as this is raw off the CA. It should not contain anything bad, but it might.
	println!("{certificate}", certificate=SafeShowByteString(&certificate));
	Ok(())
}

fn do_tests(mut csrlist: Vec<FilenameBuf>, test_list: &[FilenameBuf], g: &GlobalConfiguration,
	gp: &PerCertificateConfigurations, test: OrderTestMode) -> Result<(), String>
{
	let mut cache = AcmeServiceDataCache{cache: BTreeMap::new()};
	//Filter the CSR list.
	if test_list.len() > 0 {
		let mut idx = 0;
		while idx < csrlist.len() {
			let mut found = false;
			for i in test_list.iter() { found |= csrlist[idx].deref() == i; }
			if found {
				idx += 1;
			} else {
				csrlist.remove(idx);
			}
		}
	}
	//Add any absolute paths. Note that these entries do not match any filter.
	for acsr in test_list.iter() { if __is_special_test(test, acsr) { csrlist.push(acsr.clone()); } }
	fail_if!(csrlist.len() == 0, "No requests to test");
	let mut some_failed = false;
	let mut force_run = false;
	let mut next_renew = None;
	for csr in csrlist.iter() {
		//Special case for testing with absolute path.
		let csrfile = if __is_special_test(test, csr) {
			PathBuf::from(csr.clone().into_os_string())
		} else {
			let csrfile = f2_request(g.reqdir.as_deref(), csr);
			time_limit_variation(&csrfile, g.debug.clone())
		};
		//When conducting tests, force_renewal=false as it is ignored anyway.
		let gp = gp.for_cert(csr);
		let statedir = g.statedir.deref_inner();
		let acme = match cache.to_acme_registered(gp.ca.clone(), statedir, g.debug.clone(), true) {
			Ok(x) => x,
			Err(err) => {
				test_failed(csr, err, &mut some_failed, &g.debug);
				continue;
			}
		};
		let mut acme = acme.borrow_mut();
		//Install test uses this, so be prepared to run post-install scripts. Need minimum next due
		//for status test.
		match run_certificate(csr, &csrfile, acme.deref_mut(), &gp.to_order(g), Some(test), &mut next_renew,
			|name|{
			run_post_install(&g.post_install, vec![name.to_owned()], &g.debug)
		}) {
			Ok(true) => {
				info!(g.debug, "Testing {csr}: PASS");
				force_run = true;
			},
			Ok(false) => (),	//Quiet.
			Err(err) => {
				test_failed(csr, err, &mut some_failed, &g.debug);
				continue
			}
		};
	}
	fail_if!(some_failed, "Some tests FAILED");
	if test == OrderTestMode::StatusTest {
		if let Some(next_renew) = next_renew {
			eprintln!("<R>{next}", next=next_renew.unix_time());
		}
	}
	if force_run && test == OrderTestMode::InstallTest {
		//In install test mode, run POST_RENEW anyway.
		run_post_renew(&g.post_renew, &g.debug);
	}
	Ok(())
}

fn write_prio(fp: &mut impl IoWrite, prio: &str, x: &[u8])
{
	fp.write_all(prio.as_bytes()).ok();
	fp.write_all(b": ").ok();
	fp.write_all(x).ok();
	fp.write_all(b"\n").ok();
	fp.flush().ok();
}

pub fn dualwrite_prio(fp: &mut File, prio: &str, x: &[u8])
{
	write_prio(fp, prio, x);
	if STDERR.isatty() { write_prio(&mut stderr(), prio, x); }
}


pub fn dualwrite(fp: &mut File, x: &[u8])
{
	fp.write(x).ok();
	fp.flush().ok();
	if STDERR.isatty() { stderr().write(x).ok(); }
}

pub fn get_debugging_inner() -> Debugging
{
	let val = var_os("CERTTOOL_DEBUG");
	let val = val.as_ref().and_then(|x|x.to_str());
	get_debug_maybe(val, StderrDebug::new_maybe_inner(true))
}

//This does not do anything with SIGTERM.
fn check_sigterm() {}


/*******************************************************************************************************************/
fn __silence_warnings(globalcfg: &GlobalConfiguration, certscfg: &PerCertificateConfigurations)
{
	if false {
		let _ = globalcfg.initial_wait;
		let mut renew_q = crate::acme_daemon::TimePriorityQueue::new();
		let _ = crate::acme_daemon::daemon_round(globalcfg, certscfg, &mut renew_q);
	}
}

fn main_renew_process(globalcfg: &GlobalConfiguration, certscfg: &PerCertificateConfigurations,
	interactive: bool) -> Result<(), String>
{
	__silence_warnings(globalcfg, certscfg);
	//Now that we are in inner context, load the keys. Switch to inner stderr,
	//which requires some hacks.
	globalcfg.call_load_key();
	certscfg.call_load_key();
	let mut globalcfg = GlobalConfiguration { debug: get_debugging_inner(), ..globalcfg.clone() };
	//If not interactive, open syslog.
	if !interactive { init_syslog(); }
	//Create as minimal debug as possible to execute some stuff.
	let debug = get_debug_maybe(None, StderrDebug::new_outer());
	let mut test = None;
	let mut test_list = Vec::new();
	let mut full_filename = false;
	let mut oneoff = None;
	for (p, i) in args_os().enumerate() {
		let iu = i.to_str().unwrap_or("");
		let ifn = Filename::from_os_str(&i);
		if p == 1 && iu == "test" {
			full_filename = true;
			test = Some(OrderTestMode::RenewTest);
		} else if p == 1 && iu == "test-status" {
			test = Some(OrderTestMode::StatusTest);
		} else if p == 1 && iu == "reinstall" {
			test = Some(OrderTestMode::InstallTest);
		} else if let (1, Some(iu)) = (p, iu.strip_prefix("unsafe-sign-csr=")) {
			fail_if!(!interactive, "unsafe-sign-csr is only allowed for interactive run");
			full_filename = true;
			oneoff = Some(iu.to_string());
		} else if p == 1 && iu == "force-hook" {
			run_post_renew(&globalcfg.post_renew, &debug);
			return Ok(());
		} else if p == 1 && iu == "dump-config" {
			info!(debug, "Global configuration: {globalcfg:?}");
			info!(debug, "Default certificate: {dflt:?}", dflt=certscfg.dflt);
			for (k,v) in certscfg.special.iter() {
				info!(debug, "certificate {k}: {v:?}", k=k.show());
			}
			return Ok(());
		} else if p == 1 {
			exiterror!(E "Unrecognized mode {iu}");
		}
		//If conducting renew test, and path is absolute, add it to list in order to test it as inactive
		//stuff.
		if p > 1 && iu == "--quiet" {
			globalcfg.quiet = true;
		} else if p > 1 && full_filename && ifn.starts_with("/") {
			test_list.push(ifn.to_owned());
		} else if p > 1 && test.is_some() {
			//Do not add absolute paths, those should be added only for renew test, in order to test
			//inactive stuff.
			if ifn.starts_with("/") { continue; }
			//Remove extra stuff if present.
			let ifn = ifn.match_prefix(Filename::from_str("requests/")).unwrap_or(ifn);
			let ifn = ifn.match_suffix(Filename::from_str(".csr")).unwrap_or(ifn);
			test_list.push(ifn.to_owned());
		}
	}
	if let Some(oneoff) = oneoff {
		fail_if!(test_list.len() != 1, "There must be one CSR with unsafe-sign-csr");
		oneoff_sign_csr(&test_list[0], &globalcfg, &oneoff)?;
		return Ok(());
	}
	//Do process.
	let csrlist = csr_list(globalcfg.reqdir.as_deref()).unwrap_or_else(|err|{
		exiterror!(E "{err}");
	});
	if csrlist.len() == 0 {
		info!(globalcfg.debug, " No Certificate Signing Requests (CSRs) found in {gcfg}",
			gcfg=globalcfg.reqdir.display_path());
		return Ok(());
	}
	if let Some(test) = test {
		//Do a test run.
		do_tests(csrlist, &test_list[..], &globalcfg, &certscfg, test)
	} else {
		let mut some_failed = false;
		let mut next_renew = None;
		do_maintenance_run(&globalcfg, certscfg, csrlist, &mut some_failed, &mut next_renew);
		fail_if!(some_failed, "Some renewals FAILED");
		Ok(())	//Successful.
	}
}


fn main()
{
	do_world_umask();
	let uid = crate::acme_common::handle_setuid();

	//Initialize the built-in auth modules.
	btls_certtool::authmod::initialize_auth_modules();

	if var("__GDB_DIRECT_EXECUTION").is_ok() {
		let (globalcfg, certscfg) = load_configuration(false);
		if let Err(err) = main_renew_process(&globalcfg, &certscfg, true) { eprintln!("Error: {err}"); }
		return;
	}

	crate::acme_common::homedir_setup(uid);

	let mut daemon = false;
	for (p, i) in args_os().enumerate() { daemon |= p == 1 && i.to_str() == Some("daemon"); }
	if daemon { exiterror!(E "Daemon functionality moved to certtool-daemon"); }
	//Just to check the configuration. However, do not load it it into parent process, that will not
	//work!
	let (globalcfg, certscfg) = load_configuration(false);
	let mut dummy = None;	//Next_ts is not interesting.
	if let Err(err) = run_with_log(&globalcfg, &certscfg, true, &mut dummy) {
		if err.len() > 0 {
			eprintln!("Run failed: {err}");
		} else {
			eprintln!("Run failed, see log above");
		}
	}
}

fn run_with_log(globalcfg: &GlobalConfiguration, certscfg: &PerCertificateConfigurations, mut interactive: bool,
	next_ts: &mut Option<Timestamp>) -> Result<(), String>
{
	//Even if interactive is set, only do it if we have controlling TTY.
	interactive &= STDERR.isatty();

	//Set O_CLOEXEC on these, since dup2 will clear it on copy anyway.
	let fd = UnixPath::dev_null().open(OpenFlags::O_RDONLY|OpenFlags::O_CLOEXEC).map_err(|err|{
		format!("Open /dev/null failed: {err}")
	})?;
	let (rpipe1, wpipe1) = FileDescriptor::pipe2(PipeFlags::CLOEXEC).map_err(|err|{
		format!("pipe failed: {err}")
	})?;
	let childpid = match Pid::fork() {
		Err(err) => fail!(format!("Fork failed: {err}")),
		Ok(None) => {
			//Child. Manually specify priority markers to not depend on debug.
			fd.dup2(SpecialFd::STDIN).unwrap_or_else(|err|{
				eprintln!("<2>dup2 failed: {err}");
				exit(1);
			});
			wpipe1.dup2(SpecialFd::STDOUT).unwrap_or_else(|err|{
				eprintln!("<2>dup2 failed: {err}");
				exit(1);
			});
			wpipe1.dup2(SpecialFd::STDERR).unwrap_or_else(|err|{
				eprintln!("<2>dup2 failed: {err}");
				exit(1);
			});
			drop(rpipe1);	//Close read end in child.
			let ret = main_renew_process(globalcfg, certscfg, interactive);
			let ecode = match ret {
				Ok(_) => 0,
				Err(err) => {
					eprintln!("<3>{err}");
					1
				}
			};
			exit(ecode);
		},
		Ok(Some(r)) => r,
	};
	drop(wpipe1);	//Close write end in parent.
	let ts = ShowTime::now();
	let fnamep = format!("acmetool.log.{ts}", ts=ts.format1());
	let fname = globalcfg.logsdir.join(&fnamep);
	let mut fp = File::create(&fname).map_err(|err|{
		format!("Open logfile {fname}: {err}", fname=fname.display())
	})?;
	let mut nstate = 0i32;
	dualwrite(&mut fp, b"------------------------------------------------------------------------------\n");
	dualwrite(&mut fp, format!("Certtool-acmeclient run starting at {ts}\n", ts=ts.format2()).as_bytes());
	dualwrite(&mut fp, b"------------------------------------------------------------------------------\n");
	let mut buffer = ForEachLine::new();
	loop {
		let mut buf = [0u8;4096];
		let buf = match rpipe1.read(&mut buf) {
			Ok(0) => break,
			Ok(r) => &buf[..r as usize],
			Err(_) => continue
		};
		buffer.for_each(buf, |line|{
			let (sev, line) = if line.len() >= 3 && line[0] == b'<' && line[2] == b'>' {
				//Severity 'R' is special. It is next-renew timestamp.
				if line[1] == b'R' {
					let line = &line[3..];
					let line = f_return!(from_utf8(line), ());
					let line = f_return!(i64::from_str(line), ());
					let dw = wait_for_unix_ts(line) / 1_000;
					let line = Timestamp::new(line);
					*next_ts = Some(line);
					dualwrite(&mut fp, format!("Next requested run: {ts} (wait {dw}µs)\n",
						ts = DecodeTsForUser2(line)).as_bytes());
					return;
				}
				//Severity values 0-2 are logged to syslog.
				let sysprio = match line[1] {
					b'0' => Some(LogLevel::Emergency),
					b'1' => Some(LogLevel::Alert),
					b'2' => Some(LogLevel::Critical),
					_ => None
				};
				if let Some(sysprio) = sysprio {
					let line = SafeShowByteString(&line[3..]);
					write_to_syslog(format_args!("{line}"), sysprio);
				}
				//Severity values 0-3 trigger abnormal.
				if line[1] >= 48 && line[1] <= 51 { nstate = -1; }
				(severity_string(line[1] as char), &line[3..])
			} else {
				("message", line)
			};
			dualwrite_prio(&mut fp, sev, line);
		});
	}
	loop { match WaitStatus::wait_pid(childpid, Default::default()) {
		Err(_) => continue,
		Ok(None) => continue,
		Ok(Some(s)) => {
			let err = if let Some(signal) = s.term_sig() {
				format!("Critical: Certool-acmeclient crashed with signal {signal}\n")
			} else if let Some(0) = s.exit_status() {
				String::new()
			} else if let Some(status) = s.exit_status() {
				if status == 66 { catastrophic_error(&mut fp); }
				format!("Critical: Certool-acmeclient exited with code {status}\n")
			} else {
				format!("Critical: Certool-acmeclient unknown abnormal exit\n")
			};
			if err.len() > 0 {
				dualwrite(&mut fp, err.as_bytes());
				nstate = -2;	//Very Abnormal.
			}
			break;
		}
	}}
	let ts = ShowTime::now();
	dualwrite(&mut fp, b"------------------------------------------------------------------------------\n");
	dualwrite(&mut fp, format!("Certtool-acmeclient run ended at {ts}\n", ts=ts.format2()).as_bytes());
	dualwrite(&mut fp, b"------------------------------------------------------------------------------\n");
	//On exceptional event, rename the file. Except if interactive.
	if nstate < 0 && !interactive {
		let fname2 = globalcfg.logsdir.join(&format!("{fnamep}!"));
		rename(&fname, &fname2).ok();
	}
	if nstate < -1 { Err(String::new()) } else { Ok(()) }
}

fn do_commit_fault() -> !
{
	std::process::exit(66);
}

fn catastrophic_error(fp: &mut File)
{
	dualwrite(fp, b"FATAL: CATASTROPHIC ERROR; DATABASE IS CORRUPT");
	//In interactive runs, quit to command line.
	if !is_noninteractive() { std::process::exit(66); }
	Logger::Syslog.log(None, LogLevel::Alert, format_args!("ACME: CATASTROPHIC ERROR; DATABASE IS CORRUPT"));
	loop { usleep(1_000_000_000).ok(); }	//Halt.
}


#[test]
fn test_byte_to_char_ascii()
{
	use btls_certtool::strings::ByteToCharIterator;
	let s = "hello, world!";
	let s2 = ByteToCharIterator::as_string(s.as_bytes().iter().cloned());
	assert_eq!(s2.deref(), s.deref())
}

#[test]
fn test_byte_to_char_ascii_twobyte()
{
	use btls_certtool::strings::ByteToCharIterator;
	let s = "hello,åäö world!";
	let s2 = ByteToCharIterator::as_string(s.as_bytes().iter().cloned());
	assert_eq!(s2.deref(), s.deref())
}

#[test]
fn test_byte_to_char_ascii_threebyte()
{
	use btls_certtool::strings::ByteToCharIterator;
	let s = "hello,\u{800}\u{d7ff}\u{e000}\u{fdcf}\u{fdf0}\u{fffd} world!";
	let s2 = ByteToCharIterator::as_string(s.as_bytes().iter().cloned());
	assert_eq!(s2.deref(), s.deref())
}

#[test]
fn test_byte_to_char_ascii_fourbyte()
{
	use btls_certtool::strings::ByteToCharIterator;
	let s = "hello,\u{10000}\u{1fffd},\u{20000}\u{2fffd}\u{ffffd}\u{100000}\u{10fffd} world!";
	let s2 = ByteToCharIterator::as_string(s.as_bytes().iter().cloned());
	assert_eq!(s2.deref(), s.deref())
}

#[test]
fn test_trans_d7ff()
{
	use btls_certtool::strings::ByteToCharIterator;
	let s = "\u{d7ff}";
	let s2 = ByteToCharIterator::as_string(s.as_bytes().iter().cloned());
	assert_eq!(s2.deref(), s.deref())
}

#[test]
fn test_trans_d7ff_e000()
{
	use btls_certtool::strings::ByteToCharIterator;
	let s = "\u{d7ff}\u{e000}";
	let s2 = ByteToCharIterator::as_string(s.as_bytes().iter().cloned());
	assert_eq!(s2.deref(), s.deref())
}

#[test]
fn test_trans_char_illegals()
{
	use btls_certtool::strings::ByteToCharIterator;
	let s = b"\x00\x08\x0b\x0c\x0e\x1f\x7f\xc2\x80\xc2\x9f";
	let s3 = "ᐸ0000ᐳᐸ0008ᐳᐸ000bᐳᐸ000cᐳᐸ000eᐳᐸ001fᐳᐸ007fᐳᐸ0080ᐳᐸ009fᐳ";
	let s2 = ByteToCharIterator::as_string(s.iter().cloned());
	assert_eq!(s2.as_bytes(), s3.as_bytes());
	assert_eq!(s2.deref(), s3.deref())
}

#[test]
fn test_trans_byte_illegals()
{
	use btls_certtool::strings::ByteToCharIterator;
	let s = b"\xed\xa0\x80\xed\xbf\xbf\xef\xbd\x90\xef\xbd\xaf\xef\xbf\xbe";
	let s3 = "ᐸedᐳᐸa0ᐳᐸ80ᐳᐸedᐳᐸbfᐳᐸbfᐳᐸefᐳᐸbdᐳᐸ90ᐳᐸefᐳᐸbdᐳᐸafᐳᐸefᐳᐸbfᐳᐸbeᐳ";
	let s2 = ByteToCharIterator::as_string(s.iter().cloned());
	assert_eq!(s2.deref(), s3.deref())
}

#[test]
fn test_trans_byte_tooshort()
{
	use btls_certtool::strings::ByteToCharIterator;
	let s = b"\xc1\xbf\xe0\x9f\xbf\xf0\x8f\xbf\xbf";
	let s3 = "ᐸc1ᐳᐸbfᐳᐸe0ᐳᐸ9fᐳᐸbfᐳᐸf0ᐳᐸ8fᐳᐸbfᐳᐸbfᐳ";
	let s2 = ByteToCharIterator::as_string(s.iter().cloned());
	assert_eq!(s2.deref(), s3.deref())
}

#[test]
fn test_trans_byte_truncated1()
{
	use btls_certtool::strings::ByteToCharIterator;
	let s = b"\xc2";
	let s3 = "ᐸc2ᐳ";
	let s2 = ByteToCharIterator::as_string(s.iter().cloned());
	assert_eq!(s2.deref(), s3.deref())
}

#[test]
fn test_trans_byte_truncated2()
{
	use btls_certtool::strings::ByteToCharIterator;
	let s = b"\xe0";
	let s3 = "ᐸe0ᐳ";
	let s2 = ByteToCharIterator::as_string(s.iter().cloned());
	assert_eq!(s2.deref(), s3.deref())
}

#[test]
fn test_trans_byte_truncated3()
{
	use btls_certtool::strings::ByteToCharIterator;
	let s = b"\xe0\xa0";
	let s3 = "ᐸe0ᐳᐸa0ᐳ";
	let s2 = ByteToCharIterator::as_string(s.iter().cloned());
	assert_eq!(s2.deref(), s3.deref())
}

#[test]
fn test_trans_byte_truncated4()
{
	use btls_certtool::strings::ByteToCharIterator;
	let s = b"\xf0";
	let s3 = "ᐸf0ᐳ";
	let s2 = ByteToCharIterator::as_string(s.iter().cloned());
	assert_eq!(s2.deref(), s3.deref())
}

#[test]
fn test_trans_byte_truncated5()
{
	use btls_certtool::strings::ByteToCharIterator;
	let s = b"\xf0\x90";
	let s3 = "ᐸf0ᐳᐸ90ᐳ";
	let s2 = ByteToCharIterator::as_string(s.iter().cloned());
	assert_eq!(s2.deref(), s3.deref())
}

#[test]
fn test_trans_byte_truncated6()
{
	use btls_certtool::strings::ByteToCharIterator;
	let s = b"\xf0\x90\x80";
	let s3 = "ᐸf0ᐳᐸ90ᐳᐸ80ᐳ";
	let s2 = ByteToCharIterator::as_string(s.iter().cloned());
	assert_eq!(s2.deref(), s3.deref())
}

#[test]
fn test_trans_char_illegals_string()
{
	use btls_certtool::strings::CharToCharIterator;
	let s = "\u{00}\u{08}\u{0b}\u{0c}\u{0e}\u{1f}\u{7f}\u{80}\u{9f}";
	let s3 = "ᐸ0000ᐳᐸ0008ᐳᐸ000bᐳᐸ000cᐳᐸ000eᐳᐸ001fᐳᐸ007fᐳᐸ0080ᐳᐸ009fᐳ";
	let s2 = CharToCharIterator::as_string(s.chars());
	assert_eq!(s2.as_bytes(), s3.as_bytes());
	assert_eq!(s2.deref(), s3.deref())
}

#[test]
fn test_trans_char_basic()
{
	use btls_certtool::strings::CharToCharIterator;
	let s = "Hello, World!";
	let s2 = CharToCharIterator::as_string(s.chars());
	assert_eq!(s2.deref(), s.deref())
}
