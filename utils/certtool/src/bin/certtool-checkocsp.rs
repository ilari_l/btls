#![warn(unsafe_op_in_unsafe_fn)]
use btls_aux_x509certparse::ParsedCertificate2;
use btls_certtool::exiterror;
use btls_certtool::ParseArgs;
use btls_certtool::read_single_cert;
use btls_certtool::stderr;
use btls_certtool::check_ocsp;
use btls_certtool::OcspValidationError;
use btls_certtool::read_file2;
use std::io::stderr;
use std::io::Write;
use std::process::exit;


static PROGNAME: &'static str = "certtool-checkocsp";

fn main()
{
	let args = ParseArgs::new(PROGNAME);
	let progname = args.get_progname();

	let mut files = args.get_files();
	let certfile = files.next();
	let mut issfile = files.next();
	let mut ocspfile = files.next();
	let fault = files.next().is_some();

	if certfile.is_none() || (issfile.is_some() && ocspfile.is_none()) || fault {
		stderr!("Syntax: {progname} <certificate> <issuer> <ocsp>", );
		stderr!("Syntax: {progname} <directory>");
		stderr!("");
		stderr!("This program checks if <ocsp> is valid OCSP response for <certificate>,");
		stderr!("as issued by the issuer <issuer>.");
		stderr!("");
		stderr!("<certificate> and <issuer> can be in either DER or PEM form. The OCSP response");
		stderr!("has to be in raw DER form.");
		stderr!("");
		stderr!("If <directory> is specified instead of <certificate>/<issuer>/<ocsp>, then the");
		stderr!("program behaves as if the following values were given:");
		stderr!(" - <certificate> = <directory>/certificate");
		stderr!(" - <issuer> = <directory>/issuer");
		stderr!(" - <ocsp> = <directory>/ocsp");
		stderr!("");
		stderr!("The program prints a status line to stdout. This status line starts with one");
		stderr!("of the following, then contains a space and freeform message:");
		stderr!(" - 'OK':      Successful, not due for renewal. Exit value is 0.");
		stderr!(" - 'LOW':     Successful, due for renewal (or OCSP response missing). Exit");
		stderr!("              value is 1.");
		stderr!(" - 'REVOKED': Certificate has been REVOKED by the CA. Exit value is 3.");
		stderr!(" - 'BAD':     Bad OCSP response. Exit value is 2.");
		stderr!(" - 'ERROR':   Error. Exit value is 2.");
		//       +---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----
		exiterror!(E "Bad usage")
	}

	//Unwrap command line arguments.
	let mut certfile = certfile.unwrap();
	if issfile.is_none() {
		//Special directory case.
		issfile = Some(certfile.join("issuer"));
		ocspfile = Some(certfile.join("ocsp"));
		certfile = certfile.join("certificate");
	}
	let issfile = issfile.unwrap();
	let ocspfile = ocspfile.unwrap();

	//Read certificates.
	let cert1 = read_single_cert(&certfile).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});
	let cert2 = read_single_cert(&issfile).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});
	let cert1 = ParsedCertificate2::from(&cert1).unwrap_or_else(|err|{
		exiterror!(E "Unable to parse subject certificate '{file}': {err}", file=certfile.display())
	});
	let cert2 = ParsedCertificate2::from(&cert2).unwrap_or_else(|err|{
		exiterror!(E "Unable to parse issuer certificate '{file}': {err}", file=issfile.display())
	});
	if !cert1.issuer.same_as(cert2.subject) { 
		exiterror!(E "The issuer certificate did not issue the subject certificate.");
	}

	let issuerkey = &cert2.pubkey;
	//Load and check OCSP.
	if !ocspfile.is_file() { exiterror!(L "OCSP response file missing"); }
	let ocspres = read_file2(&ocspfile).unwrap_or_else(|err|{
		exiterror!(E "Unable to read OCSP file '{file}': {err}", file=ocspfile.display())
	});
	let res = check_ocsp(&ocspres, &cert1.serial_number, cert1.issuer, issuerkey).unwrap_or_else(|err|{
		match err {
			OcspValidationError::Revoked => exiterror!(X 3, "REVOKED", "Certificate has been REVOKED"),
			OcspValidationError::Other(err) => exiterror!(P "BAD", "Failed to validate OCSP: {err}")
		}
	});

	//Message according to time remaining.
	let hours = ((res.remaining + 180) / 360) as f64 / 10.0;
	if res.lifetime_low {
		exiterror!(L "OCSP response expires soon ({hours} hours remain)");
	} else {
		exiterror!(OK "OCSP response seems OK ({hours} hours remain)");
	}
}
