#![warn(unsafe_op_in_unsafe_fn)]
extern crate btls_aux_time;
extern crate btls_aux_signatures;
extern crate btls_aux_x509certparse;
#[macro_use] extern crate btls_certtool;
use btls_certtool::decode_pem_or_der_csr;
use btls_certtool::ParseArgs;
use btls_certtool::read_file2;
use btls_certtool::get_rsa_dh_parameters;
use btls_certtool::csr::CsrInfo;
use std::io::stdout;
use std::io::stderr;
use std::io::Write;
use std::path::Path;
use std::process::exit;


static PROGNAME: &'static str = "certtool-csrinfo";
static ARG_MACHNAME: &'static str = "--machine-names";
static ARG_DHPARMS: &'static str = "--dh-params";

fn main()
{
	let args = ParseArgs::new(PROGNAME);
	let progname = args.get_progname();

	let mr_names = args.is_option(ARG_MACHNAME).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});
	let dh_params = args.is_option(ARG_DHPARMS).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});

	let mut files = args.get_files();
	let csrfile = files.next();
	let mut fault = files.next().is_some();

	fault |= mr_names && dh_params;
	if csrfile.is_none() || fault {
		stderr!("Syntax: {progname} [{ARG_MACHNAME}|{ARG_DHPARMS}] <csr>");
		stderr!("");
		stderr!("This program extracts information about CSR <csr> and prints it. The");
		stderr!("CSR can be in either DER or PEM form.");
		stderr!("");
		stderr!("If the {ARG_MACHNAME} option is specified, then only identifiers will be");
		stderr!("printed, one per line. Each line has one of following forms:");
		stderr!(" - 'dns:<name>' for DNS names");
		stderr!(" - 'ip:<address>' for IP addresses. <address> can be either IPv4 or IPv6.");
		stderr!("");
		stderr!("If the {ARG_DHPARMS} option is specified, matching Diffie-Hellman parameters");
		stderr!("will be emitted to stdout in PEM format. Note that this only works for RSA");
		stderr!("keys, other keys will emit empty output.");
		stderr!("");
		stderr!("On success, this tool prints the information to stdout and exits with status 0");
		stderr!("");
		stderr!("On error, this tool prints a status line that starts with 'ERROR ' and then");
		stderr!("contains an error message. It also exits with status 2.");
		//       +---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----
		exiterror!(E "Bad usage");
	}
	let csrfile = csrfile.unwrap();
	//Read the CSR and unwrap PEM.
	let dfile = Path::new(&csrfile).display();
	let contents = read_file2(&csrfile).unwrap_or_else(|err|{
		exiterror!(E "Failed to read '{dfile}': {err}")
	});
	let contents2 = decode_pem_or_der_csr(&contents, |_|Ok(())).unwrap_or_else(|err|{
		exiterror!(E "{dfile}: {err}")
	});

	let info = CsrInfo::parse(&contents2).unwrap_or_else(|err|{
		exiterror!(E "Failed to parse the CSR '{dfile}': {err}")
	});
	if dh_params {
		let out = get_rsa_dh_parameters(info.get_rsa_mag());
		if out.len() > 0 { stdout().write_all(&out).unwrap(); }
	} else if mr_names {
		let names = info.get_identifiers_for_preauth().unwrap_or_else(|err|{
			exiterror!(E "Failed to extract names from CSR '{dfile}': {err}")
		});
		for (kind,value) in names.iter() { println!("{kind}:{value}"); }
	} else {
		print!("{info}", info=info.to_string());
	}
}
