#![warn(unsafe_op_in_unsafe_fn)]
use btls_certtool::exiterror;
use btls_certtool::ParseArgs;
use btls_certtool::stderr;
use btls_certtool::acmeverify::check_challenge;
use btls_certtool::deadline::Deadline;
use btls_certtool::debug::get_debug_maybe;
use btls_certtool::debug::StderrDebug;
use btls_certtool::dns::AddressReplaceList;
use btls_certtool::strings::AsciiString;
use btls_certtool::strings::Base64UrlString;
use std::fs::File;
use std::io::Read;
use std::io::stderr;
use std::io::Write;
use std::env::var;
use std::process::exit;
use std::time::Duration;


static PROGNAME: &'static str = "certtool-acmeccheck";
static ARG_ASSUME_FORWARDED: &'static str = "--assume-forwarded=";
static ARG_ASSUME_FORWARDED_OLD: &'static str = "--assume-forwareded=";
const VALIDATION_TIMEOUT: Duration = Duration::from_millis(60000);

fn main()
{
	let debug = get_debug_maybe(var("CERTTOOL_DEBUG").ok().as_deref(), StderrDebug::new_outer());

	let args = ParseArgs::new(PROGNAME);
	let progname = args.get_progname();

	let mut replace = AddressReplaceList::new();
	for name in args.getopt_multi_path(ARG_ASSUME_FORWARDED).
		chain(args.getopt_multi_path(ARG_ASSUME_FORWARDED_OLD)) {
		let mut content = Vec::new();
		File::open(&name).and_then(|mut f|f.read_to_end(&mut content)).unwrap_or_else(|err|{
			exiterror!(E "Failed to read assume forwarded file '{name}': {err}'", name=name.display())
		});
		replace.parse_file_contents(&content, &debug);
	}

	let mut files = args.get_files();
	let idkind = files.next();
	let ident = files.next();
	let chalkind = files.next();
	let challenge = files.next();
	let thumbprint = files.next();
	let fault = files.next().is_some();

	if thumbprint.is_none() || fault {
		stderr!("Syntax: {progname} <idkind> <ident> <chaltype> <challenge> <thumbprint>");
		stderr!("        [{ARG_ASSUME_FORWARDED}<affile>]");
		stderr!("");
		stderr!("This program checks ACME validation response of <identifer> of kind <idkind>");
		stderr!("(e.g., 'dns'). The used challenge type is <chaltype> and the challenge nonce");
		stderr!("is <challenge>. The account thumbprint is <thumbprint>.");
		stderr!("");
		stderr!("If <affile> is specified, the specified file is read and any non-empty");
		stderr!("lines that do not start with # are added to assumed port forward list.");
		stderr!("");
		stderr!("The format of each assumed port forward entry is:");
		stderr!("<sourceaddr> <sourceport> <destaddr> <destport>");
		stderr!("");
		stderr!("Note: If there is mapping from given source address, other mapping are assumed");
		stderr!("not to exist. If they actually exist, specify identity mapping (map the.");
		stderr!("address/port pair to itself).");
		stderr!("");
		stderr!("The environment variable CERTTOOL_DNSSERVER can be used to override the recursive");
		stderr!("DNS resolver used (default: read address from /etc/resolv.conf");
		stderr!("");
		stderr!("Currently supported <idkind>: 'dns'");
		stderr!("Currently supported <chaltype>: 'http-01', 'tls-alpn-01', 'dns-01'");
		stderr!("");
		stderr!("The program prints a status line to stdout. This status line starts with one");
		stderr!("of the following, then contains a space and freeform message:");
		stderr!(" - 'OK':      Successful. Exit value is 0.");
		stderr!(" - 'ERROR':   Error. Exit value is 2.");
		//       +---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----
		exiterror!(E "Bad usage");
	}

	let idkind = AsciiString::new_osstring(&idkind.unwrap().into_os_string()).unwrap_or_else(|bad|{
		exiterror!(E "Bad identifier kind {bad}")
	});
	let ident = AsciiString::new_osstring(&ident.unwrap().into_os_string()).unwrap_or_else(|bad|{
		exiterror!(E "Bad identifier {bad}")
	});
	let chalkind = AsciiString::new_osstring(&chalkind.unwrap().into_os_string()).unwrap_or_else(|bad|{
		exiterror!(E "Bad challenge type {bad}")
	});
	let challenge = Base64UrlString::new_osstring(&challenge.unwrap().into_os_string()).unwrap_or_else(|bad|{
		exiterror!(E "Bad challenge {bad}")
	});
	let thumbprint = Base64UrlString::new_osstring(&thumbprint.unwrap().into_os_string()).unwrap_or_else(|bad|{
		exiterror!(E "Bad thumbprint {bad}")
	});

	if let Err(err) = check_challenge(&chalkind, &idkind, &ident, &challenge, &thumbprint, &replace, &debug,
		Deadline::new(VALIDATION_TIMEOUT)) {
		exiterror!(E "{err}")
	}
	exiterror!(OK "Validation check successful");
}
