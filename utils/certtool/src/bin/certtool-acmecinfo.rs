#![warn(unsafe_op_in_unsafe_fn)]
use btls_certtool::exiterror;
use btls_certtool::ParseArgs;
use btls_certtool::stderr;
use btls_certtool::acmeverify::fill_variables_method;
use btls_certtool::strings::AsciiStr;
use btls_certtool::strings::AsciiString;
use btls_certtool::strings::Base64UrlStr;
use btls_certtool::strings::Base64UrlString;
use std::collections::BTreeMap;
use std::io::stderr;
use std::io::Write;
use std::process::exit;


static PROGNAME: &'static str = "certtool-acmecinfo";
static VALIDATION_PFX: &'static str = "--validation-";

fn main()
{
	let args = ParseArgs::new(PROGNAME);
	let progname = args.get_progname();

	let mut validations = BTreeMap::new();
	for name in args.getopt_multi_str(VALIDATION_PFX) {
		let name = AsciiStr::new_string(&name).unwrap_or_else(|bad|{
			exiterror!(E "Bad option --validation-{bad}")
		});
		let pos = name.find('=').unwrap_or_else(||{
			exiterror!(E "No = in {VALIDATION_PFX}")
		});
		let (name, value) = (&name[..pos], &name[pos+1..]);
		let value = Base64UrlStr::new_string(value.as_inner()).unwrap_or_else(|bad|{
			exiterror!(E "Illegal token {bad}")
		});
		validations.insert(name.to_owned(), value.to_owned());
	}

	let mut files = args.get_files();
	let idkind = files.next();
	let ident = files.next();
	let thumbprint = files.next();
	let fault = files.next().is_some();

	if thumbprint.is_none() || fault {
		stderr!("Syntax: {progname} <idkind> <ident> <thumbprint>");
		stderr!("        {VALIDATION_PFX}<method>=<challenge>...");
		stderr!("");
		stderr!("This program computes the ACME validation response for <identifer> of kind");
		stderr!("<idkind> (e.g., 'dns'). This uses validation method <method> with challenge");
		stderr!("<challenge>. The account thumbprint is <thumbprint>.");
		stderr!("");
		stderr!("Currently supported <idkind>: 'dns'");
		stderr!("Currently supported <method>: 'http-01', 'tls-alpn-01', 'dns-01'");
		stderr!("");
		stderr!("If successful, the program prints the validation parameters to stdout in");
		stderr!("the form <keyword>=<value>, where <keyword> can be:");
		stderr!("");
		stderr!("http01_content - The file content for HTTP-01 validation");
		stderr!("http01_filename - The file name (without path) for HTTP-01 validation");
		stderr!("http01_method - 'http-01'");
		stderr!("http01_token - The random token for HTTP-01 validation");
		stderr!("http01_url - The initial URL for HTTP-01 validation");
		stderr!("tlsalpn01_method - 'tls-alpn-01'");
		stderr!("tlsalpn01_token - The response value for TLS-ALPN-01 authentication");
		stderr!("dns01_method - 'dns-01'");
		stderr!("dns01_qname - The QNAME for DNS-01 validation");
		stderr!("dns01_token - The random token for DNS-01 validation");
		stderr!("dns01_value - The response value for DNS-01 validation");
		stderr!("base_thumbprint - Account thumbprint");
		stderr!("base_identifier_kind - Identifier kind (e.g. 'dns')");
		stderr!("base_identifier - Identifier");
		stderr!("");
		stderr!("On error, this tool prints a status line that starts with one of the following");
		stderr!("words, and then space followed by freeform text message, and finally returns");
		stderr!("status code 2:");
		stderr!(" - 'ERROR': Error in operation");
		//       +---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----
		exiterror!(E "Bad usage");
	}

	let idkind = AsciiString::new_osstring(&idkind.unwrap().into_os_string()).unwrap_or_else(|bad|{
		exiterror!(E "Bad identifier kind {bad}")
	});
	let ident = AsciiString::new_osstring(&ident.unwrap().into_os_string()).unwrap_or_else(|bad|{
		exiterror!(E "Bad identifier {bad}")
	});
	let thumbprint = Base64UrlString::new_osstring(&thumbprint.unwrap().into_os_string()).unwrap_or_else(|bad|{
		exiterror!(E "Bad thumbprint {bad}")
	});

	let mut map = BTreeMap::new();
	for (ckind, chal) in validations.iter() {
		if fill_variables_method(&mut map, ckind, &idkind, &ident, chal, &thumbprint).is_err() {
			stderr!("Unknown challenge kind '{ckind}' for '{idkind}'");
		}
	}
	for (k, v) in map.iter() { println!("{k}={v}"); }
}
