#![warn(unsafe_op_in_unsafe_fn)]
use btls_aux_filename::Filename;
use btls_certtool::exiterror;
use btls_certtool::ParseArgs;
use btls_certtool::stderr;
use btls_certtool::do_world_umask;
use btls_certtool::next_directory_filename;
use std::fs::create_dir;
use std::io::stderr;
use std::io::Write;
use std::process::exit;


static PROGNAME: &'static str = "certtool-createdir";

fn main()
{
	let args = ParseArgs::new(PROGNAME);
	let progname = args.get_progname();

	let mut files = args.get_files();
	let basename = files.next();
	let fault = files.next().is_some();

	if basename.is_none() || fault {
		stderr!("Syntax: {progname} <basename>");
		stderr!("");
		stderr!("This program creates a directory named <basename>-<nnnn>, where");
		stderr!("<nnnn> is the next sequence number after the largest found. Any gaps in");
		stderr!("the sequence number space are not filled.");
		stderr!("");
		stderr!("The <basename> must be valid unicode. On unix-type systems, the directory");
		stderr!("is always created with read access for group and others.");
		stderr!("");
		stderr!("This program is meant for creating directories to write the certificate files");
		stderr!("Use this command to create a directory to write the certificate files to.");
		stderr!("");
		stderr!("On success, this tool prints the name of the created directory to stdout and");
		stderr!("exits with status 0");
		stderr!("");
		stderr!("On error, this tool prints a status line that starts with 'ERROR ' and then");
		stderr!("contains an error message. It also exits with status 2.");
		//       +---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----+---- ----
		exiterror!(E "Bad usage");
	}
	do_world_umask();
	let basename = basename.unwrap();
	//Find the parent directory of specified path and the file name.
	let nname = next_directory_filename(&basename).unwrap_or_else(|err|{
		exiterror!(E "{err}")
	});
	//Create that directory, readable for all.
	do_world_umask();
	create_dir(&nname).unwrap_or_else(|err|{
		exiterror!(E "Failed to make directory '{name}': {err}", name=nname.display())
	});
	Filename::from_path(&nname).write_to_io(std::io::stdout()).unwrap_or_else(|err|{
		exiterror!(E "Failed to write directory name: {err}")
	});
}
