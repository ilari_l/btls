use crate::acme_daemon::CertDirTag;
use crate::acme_daemon::f2_certificate as f_certificate;
use crate::acme_daemon::f2_certtmp as f_certtmp;
use crate::acme_daemon::f2_certtmpfail as f_certtmpfail;
use crate::acme_daemon::f2_eecert as f_eecert;
use crate::acme_daemon::f2_keysymlink as f_keysymlink;
use btls_aux_fail::fail;
use btls_aux_filename::Filename;
use btls_aux_time::Timestamp;
use btls_aux_x509certparse::ParsedCertificate2;
use btls_certtool::CertChain;
use btls_certtool::encode_pem_cert;
use btls_certtool::get_dh_params;
use btls_certtool::next_directory_filename;
use btls_certtool::path_append_extension;
use btls_certtool::read_cert_file;
use btls_certtool::symlink_equiv;
use btls_certtool::aia::aia_issuer_chain2;
use btls_certtool::aia::aia_fetch_ocsp;
use btls_certtool::aia::AiaOcspError;
use btls_certtool::aia::IntermediatePile;
use btls_certtool::aia::SpecialIntermediatePile;
use btls_certtool::aia::SpecialIssuerEntry;
use btls_certtool::authmod::random_base64text;
use btls_certtool::debug::Debugging;
use btls_certtool::strings::Base64UrlStr;
use btls_certtool::wcheck::do_check_csr_fields;
use btls_certtool::wcheck::do_validate_versus_roots;
use btls_certtool::wcheck::override_fields_from_csr;
use btls_certtool::wcheck::Rootpile;
use btls_certtool::wcheck::ValidateRootError;
use std::collections::BTreeMap;
use std::fs::create_dir;
use std::fs::File;
use std::fs::remove_file;
use std::fs::rename;
use std::fmt::Write as FmtWrite;
use std::io::Read as IoRead;
use std::io::Write as IoWrite;
use std::ops::Deref;
use std::path::Path;
use std::path::PathBuf;


fn gen_nginx_conf(basedir: CertDirTag<&Path>, basename: &Filename, ocsp: bool) -> String
{
	let mut s = String::new();
	let certdir = f_certificate(basedir, basename);
	let certdirp = certdir.display();
	//TODO: Write the path as raw bytes, in case it has some non-Unicode characters.
	writeln!(s, "include snippets/https.conf;").ok();
	writeln!(s, "ssl_certificate_key {certdirp}/key;").ok();
	writeln!(s, "ssl_certificate {certdirp}/full.pem;").ok();
	if ocsp {
		writeln!(s, "ssl_stapling on;").ok();
		writeln!(s, "ssl_stapling_file {certdirp}/ocsp;").ok();
		writeln!(s, "ssl_stapling_verify off; #Should already be verified.").ok();
	}
	s
}

fn disable_borrow_checker<'a>(outer: &'a [u8], inner: &[u8]) -> &'a [u8]
{
	let soffset = inner.as_ptr() as usize - outer.as_ptr() as usize;
	let eoffset = soffset + inner.len();
	&outer[soffset..eoffset]
}

pub struct InstallTestFlags
{
	pub install_test: bool,
}

pub struct CertificateInstallConfiguration<'a>
{
	pub pile: &'a IntermediatePile,
	pub spile: &'a SpecialIntermediatePile,
	pub debug: Debugging,
	pub rootpile: &'a Rootpile,
	pub certdir: CertDirTag<&'a Path>,
}

pub enum InstallError
{
	Temporary(String),
	Permanent(String),
	Untrusted,
}

pub fn call_install<F>(basename: &Filename, keyfile: Option<&Path>, csr: &Path, g: &CertificateInstallConfiguration,
	force_faulted: bool, test: InstallTestFlags, post_install: F) -> Result<(), InstallError>
	where F: FnOnce(&Filename)
{
	let targetfile = f_certificate(g.certdir, basename);
	let (srcfile, keyfile) = if test.install_test {
		//Reinstall previous. Override keyfile from the previous certificate.
		let name = f_eecert(g.certdir, basename);
		let tgt = {
			let keypath = f_keysymlink(g.certdir, basename);
			keypath.read_link().ok()
		};
		(name, tgt)
	} else if force_faulted {
		(f_certtmpfail(g.certdir, basename), keyfile.map(|x|x.to_path_buf()))
	} else {
		(f_certtmp(g.certdir, basename), keyfile.map(|x|x.to_path_buf()))
	};
	let unlink_at_end: Option<&Path> = if !test.install_test { Some(srcfile.deref()) } else { None };
	let keyfile = keyfile.as_deref();
	//Check that targetfile is not a directory.
	if targetfile.symlink_metadata().map(|x|x.is_dir()).unwrap_or(false) {
		//This is temporary error, since it is related to certificate storage itself.
		let msg = format!("{tgt} is a directory, should be a symbolc link", tgt=targetfile.display());
		fail!(InstallError::Temporary(msg));
	}
	//We have to load the certificate, and fetch the AIA chain. Assume temporary error if this fails.
	let cert1 = read_cert_file(&srcfile, true).map_err(|err|{
		InstallError::Temporary(err.to_string())
	})?;
	//Check that cert1 does not contain multiple elements, since we mess up if so.
	let mut cert1b = cert1.iter();
	let cert1 = cert1b.next().ok_or_else(||{
		InstallError::Permanent(format!("Unparseable certificate: No certificates"))
	})?;
	let pcertee = ParsedCertificate2::from(cert1).map_err(|err|{
		InstallError::Permanent(format!("Unparseable certificate: {err}"))
	})?;
	//Special issuer is handled... specially.
	if let Some(special) = g.spile.get_entry(&pcertee) {
		return call_install_special_issuer(basename, targetfile, &pcertee, csr, special, g, keyfile,
			unlink_at_end, post_install);
	}
	//The not special case.
	let mut tmpcerts = BTreeMap::new();
	while let Some(_icert) = cert1b.next() {
		let icert = ParsedCertificate2::from(_icert).map_err(|err|{
			InstallError::Permanent(format!("Unparseable certificate: {err}"))
		})?;
		//Nasty hack to circumvent borrow checker.
		let subj = disable_borrow_checker(&cert1, icert.subject.as_raw_subject());
		let icert_raw = disable_borrow_checker(&cert1, icert.entiere);
		tmpcerts.insert(subj, icert_raw);
	}
	//Special: Just use empty chain if aia_issuer_chain does not find anything. If AIA is disabled, error is
	//Untrusted, otherwise temporary error.
	let disable_aia = g.spile.aia_disabled();
	let rchain = aia_issuer_chain2(&g.pile, &pcertee, tmpcerts, disable_aia, &g.debug).map_err(|err|{
		if disable_aia { InstallError::Untrusted } else { InstallError::Temporary(err) }
	})?;
	let chain = rchain.unwrap_or(CertChain::new());

	//Check the chain versus what we obtained.
	let mut namelist = Vec::new();
	let mut key = None;
	let mut subject = None;
	override_fields_from_csr(csr, &mut namelist, &mut key, &mut subject).map_err(|err|{
		//CSR failing to parse is temporary.
		InstallError::Temporary(format!("Failed to parse the CSR: {err}"))
	})?;
	let validate_advance = Some(pcertee.not_before.delta(pcertee.not_after) / 3);
	do_validate_versus_roots(&cert1, &chain, &g.rootpile, validate_advance, &namelist, key, subject, &g.debug).
		map_err(|err|match err {
		//Root failing to be found is not fatal.
		ValidateRootError::UnknownRoot => InstallError::Untrusted,
		//Other errors are fatal.
		ValidateRootError::Other(err) =>
			InstallError::Permanent(format!("Certificate validity check failed: {err}"))
	})?;

	//Load the OCSP response. For this, we need the immediate issuer chain.
	let chunk = chain.iter().next();
	let ocsp = if let Some(cert2) = chunk {
		aia_fetch_ocsp(&cert1, &cert2, Timestamp::now(), &g.debug).or_else(handle_failed_ocsp_fetch)?
	} else {
		Vec::new()	//Special, empty OCSP response.
	};

	//Do the rest of actions. All errors are transient.
	call_install_tail(basename, targetfile, &cert1, &ocsp, Err(&chain), g, keyfile, unlink_at_end, post_install).
		map_err(InstallError::Temporary)
}

fn call_install_special_issuer(basename: &Filename, targetfile: PathBuf, peecert: &ParsedCertificate2,
	csr: &Path, special: &SpecialIssuerEntry, g: &CertificateInstallConfiguration, keyfile: Option<&Path>,
	orig_to_unlink: Option<&Path>, post_install: impl FnOnce(&Filename)) -> Result<(), InstallError>
{
	let time = Timestamp::now();
	//Check that certificate parameters are as expected.
	let mut namelist = Vec::new();
	let mut keyhash = None;
	let mut subject = None;
	override_fields_from_csr(csr, &mut namelist, &mut keyhash, &mut subject).map_err(|err|{
		//CSR failing is temporary.
		InstallError::Temporary(format!("Failed to parse the CSR: {err}"))
	})?;
	do_check_csr_fields(peecert, &namelist, keyhash, subject).map_err(|err|{
		//CSR field check failing is permanent error.
		InstallError::Permanent(format!("Certificate fails cross-check with CSR: {err}"))
	})?;
	//Validate certificate versus its issuer.
	special.check_certificate(peecert, time).map_err(|err|{
		//special is always one issuer, so the issuer is guaranteed to be found. So any error is fatal.
		InstallError::Permanent(format!("Certificate validity check failed: {err}"))
	})?;
	//Fetch OCSP.
	let ocsp = special.fetch_ocsp(peecert, time, &g.debug).or_else(handle_failed_ocsp_fetch)?;
	//Do the rest of actions. All errors are transient.
	call_install_tail(basename, targetfile, peecert.entiere, &ocsp, Ok(special), g, keyfile, orig_to_unlink,
		post_install).map_err(InstallError::Temporary)
}

fn handle_failed_ocsp_fetch(err: AiaOcspError) -> Result<Vec<u8>, InstallError>
{
	match err {
		AiaOcspError::NoOcsp => Ok(Vec::new()),		//Special, empty OCSP response.
		//OCSP immediately giving revoked is permanent, other OSCP errors are temporary.
		AiaOcspError::Revoked => fail!(InstallError::Permanent(format!("Certificate has been REVOKED"))),
		AiaOcspError::Other(err) => fail!(InstallError::Temporary(format!("OCSP fetch failed: {err}"))),
	}
}

fn call_install_tail(basename: &Filename, targetfile: PathBuf, ee_cert_der: &[u8], ocsp: &[u8],
	issuer: Result<&SpecialIssuerEntry, &CertChain>, g: &CertificateInstallConfiguration, keyfile: Option<&Path>,
	orig_to_unlink: Option<&Path>, post_install: impl FnOnce(&Filename)) -> Result<(), String>
{
	//Grab the DH parameters.
	let dhparams = get_dh_params(&ee_cert_der);
	//Convert the certificate into PEM format.
	let mut ee_cert = encode_pem_cert(&ee_cert_der);
	ee_cert.extend_from_slice(&dhparams);

	//Convert the chain into PEM format.
	let ca_cert_der: &CertChain = match issuer {
		Ok(i) => i.get_der_contents(),
		Err(c) => c,
	};
	let mut ca_cert = Vec::new();
	for chunk in ca_cert_der.iter() { ca_cert.append(&mut encode_pem_cert(&chunk)); }

	//Find the parent directory of specified path and the file name.
	let root: PathBuf = next_directory_filename(&targetfile).map_err(|err|{
		err.to_string()
	})?;
	create_dir(&root).map_err(|err|{
		format!("Failed to make directory '{root}': {err}", root=root.display())
	})?;
	//Write the certificate files.
	File::create(root.join("certificate")).and_then(|mut f|f.write_all(&ee_cert)).map_err(|err|{
		format!("Failed to write '{root}/certificate': {err}", root=root.display())
	})?;
	let iss_file = root.join("issuer");
	if let Some(sfile) = issuer.ok().map(|i|i.get_chain_file()) {
		//Symlink instead. Note that target is always in parent directory.
		let sfile = Path::new("..").join(sfile.into_path());
		symlink_equiv(&sfile, &iss_file).map_err(|err|{
			format!("Failed to symlink '{src}' -> '{tgt}': {err}",
				src=iss_file.display(), tgt=sfile.display())
		})?;
	} else {
		File::create(&iss_file).and_then(|mut f|f.write_all(&ca_cert)).map_err(|err|{
			format!("Failed to write '{file}': {err}", file=iss_file.display())
		})?;
	}
	File::create(root.join("full.pem")).and_then(|mut f|{
		f.write_all(&ee_cert)?;
		f.write_all(&ca_cert)?;
		Ok(())
	}).map_err(|err|{
		format!("Failed to write '{root}/full.pem': {err}", root=root.display())
	})?;
	File::create(root.join("nginx.conf")).and_then(|mut f|{
		f.write_all(gen_nginx_conf(g.certdir, basename, ocsp.len() > 0).as_bytes())
	}).map_err(|err|{
		format!("Failed to write '{root}/nginx.conf': {err}", root=root.display())
	})?;
	if ocsp.len() > 0 {
		File::create(root.join("ocsp")).and_then(|mut f|f.write_all(ocsp)).map_err(|err|{
			format!("Failed to write '{root}/ocsp': {err}", root=root.display())
		})?;
	}
	if let Some(keyfile) = keyfile {
		symlink_equiv(&keyfile, root.join("key")).map_err(|err|{
			format!("Failed to symlink '{src}' -> '{tgt}/key': {err}",
				src=keyfile.display(), tgt=root.display())
		})?;
	}
	//Ok, link root to targetfile.
	let relroot = Path::new(root.file_name().unwrap_or(root.as_os_str()));
	let tempfile = path_append_extension(&targetfile, "tmp");
	symlink_equiv(&relroot, &tempfile).map_err(|err|{
		format!("Failed to symlink '{src}' -> '{tgt}': {err}",
			src=&tempfile.display(), tgt=relroot.display())
	})?;
	//Rename the symlink.
	rename(&tempfile, &targetfile).map_err(|err|{
		format!("Failed to rename '{src}' -> '{tgt}': {err}",
			src=tempfile.display(), tgt=targetfile.display())
	})?;
	//Unlink the temporary if not in test mode.
	if let Some(orig) = orig_to_unlink { remove_file(orig).ok(); }
	//Call post_install to notify about installation.
	post_install(basename);

	Ok(())
}

pub(crate) fn write_ocsp_file(certpath: &Path, ocsp: &[u8]) -> Result<bool, String>
{
	let ocspfile = certpath.join("ocsp");
	let mut old_ocsp = Vec::new();
	let changed = match File::open(&ocspfile).and_then(|mut fp|fp.read_to_end(&mut old_ocsp)) {
		Ok(_) => old_ocsp.deref() != ocsp,
		Err(_) => true				//If read fails, always assume changed.
	};
	if changed {
		//Ok, link basename to ocspfile.
		let tempfile = path_append_extension(&ocspfile, "tmp");
		//Write OCSP response to tempfile.
		File::create(&tempfile).and_then(|mut f|f.write_all(&ocsp)).map_err(|err|{
			format!("Failed to write OCSP response to {tgt}: {err}", tgt=tempfile.display())
		})?;
		//Rename the response.
		rename(&tempfile, &ocspfile).map_err(|err|{
			format!("Failed to rename {src} -> {tgt}: {err}",
				src=tempfile.display(), tgt=ocspfile.display())
		})?;
	}
	Ok(changed)
}


pub fn try_write_directory(directory: &Path, thumbprint: &Base64UrlStr) ->
	Result<(), String>
{
	//Generate a random challenge.
	let mut challenge = [0;40];
	let challenge = Base64UrlStr::new_string(random_base64text(&mut challenge)).unwrap();
	let filecontent = format!("{challenge}.{thumbprint}");
	let challengefile = directory.join(challenge.as_inner());
	File::create(&challengefile).and_then(|mut f|f.write_all(filecontent.as_bytes())).map_err(|err|{
		format!("Failed to write '{file}': {err}", file=challengefile.display())
	})?;
	//not in domain context, just succeeding above is enough.
	remove_file(&challengefile).ok();
	return Ok(());
}
