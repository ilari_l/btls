use crate::An;
use crate::acme::AcmeAuthorizationTarget;
use crate::acme::AcmeChallenge;
use crate::acmeverify::hexencode;
use crate::debug::DEBUG_MODULES;
use crate::debug::Debugging;
use crate::strings::AsciiStr;
use crate::strings::AsciiString;
use crate::strings::base64url_string_to_ascii_string;
use crate::strings::Base64UrlStr;
use crate::strings::safe_octets_to_text;
use btls::utility::GlobalMutex;
use btls::utility::Mutex;
use btls_aux_dlfcn::LibraryHandle;
use btls_aux_dlfcn::Symbol;
use btls_aux_fail::fail_if;
use btls_aux_fail::fail_if_none;
use btls_aux_filename::Filename;
use btls_aux_filename::FilenameBuf;
use btls_aux_hash::sha256;
use btls_aux_ip::IpAddress;
use btls_aux_random::secure_random;
use btls_aux_signatures::base64url;
use std::collections::BTreeMap;
use std::mem::transmute;
use std::net::IpAddr;
use std::ops::Deref;
use std::path::Path;
use std::ptr::read as ptr_read;
use std::ptr::null;
use std::slice::from_raw_parts;
use std::str::FromStr;
use std::str::from_utf8;
use std::sync::Arc;


#[derive(Copy,Clone)]
pub struct OffsetLength
{
	offset: usize,
	length: usize,
}

impl OffsetLength
{
	fn add(x: &mut Vec<u8>, v: &AsciiStr) -> OffsetLength
	{
		let offset = x.len();
		x.extend_from_slice(v.as_bytes());
		x.push(0);
		OffsetLength{
			offset: offset,
			length: v.len()
		}
	}
}

#[repr(C)]
pub struct RawString
{
	x: *const u8,
	xlen: usize
}

impl RawString
{
	fn new(backing: &Vec<u8>, loc: OffsetLength) -> RawString
	{
		RawString {
			x: (&backing[loc.offset..]).as_ptr(),
			xlen: loc.length
		}
	}
	fn null() -> RawString
	{
		RawString {
			x: null(),
			xlen: 0
		}
	}
}


#[repr(C)]
pub struct AuthChallengeAttribute
{
	key: RawString,
	value: RawString,
	//We can not put any extra fields here because this structure is in array.
}

#[repr(C)]
pub struct AuthChallengeMethod
{
	name: RawString,
	//We can not put any extra fields here because this structure is in array.
}

#[repr(C)]
pub struct AuthChallengeInfo
{
	pub kind: RawString,
	pub name: RawString,
	pub wildcard: i32,
	pub attributes: *const AuthChallengeAttribute,
	pub attributeslen: usize,
	pub methods: *const AuthChallengeMethod,
	pub methodslen: usize,
	backingmemory1: Vec<u8>,
	backingmemory2: Vec<AuthChallengeAttribute>,
	backingmemory3: Vec<AuthChallengeMethod>,
}

impl AuthChallengeInfo
{
	fn new(target: &AcmeAuthorizationTarget, attributes: &[(&AsciiStr, AsciiString)], methods: &[&AsciiStr]) ->
		AuthChallengeInfo
	{
		let mut backingmemory1 = Vec::new();
		let mut backingmemory2 = Vec::new();
		let mut backingmemory3 = Vec::new();
		let mut offsets = Vec::new();
		let mut offsets2 = Vec::new();
		let kindoffset = OffsetLength::add(&mut backingmemory1, &target.id_type);
		let nameoffset = OffsetLength::add(&mut backingmemory1, &target.id_value);
		for i in attributes.iter() {
			offsets.push((OffsetLength::add(&mut backingmemory1, i.0),
				OffsetLength::add(&mut backingmemory1, &i.1)));
		}
		for i in methods.iter() {
			offsets2.push(OffsetLength::add(&mut backingmemory1, i));
		}
		let kind = RawString::new(&backingmemory1, kindoffset);
		let name = RawString::new(&backingmemory1, nameoffset);
		for i in offsets.iter() {
			backingmemory2.push(AuthChallengeAttribute{
				key: RawString::new(&backingmemory1, i.0),
				value: RawString::new(&backingmemory1, i.1),
			});
		}
		backingmemory2.push(AuthChallengeAttribute{
			key: RawString::null(),
			value: RawString::null(),
		});
		for i in offsets2.iter() {
			backingmemory3.push(AuthChallengeMethod{
				name: RawString::new(&backingmemory1, *i),
			});
		}
		backingmemory3.push(AuthChallengeMethod{
			name: RawString::null(),
		});
		AuthChallengeInfo {
			kind: kind,
			name: name,
			wildcard: if target.wildcard { 1 } else { 0 },
			attributes: backingmemory2.as_ptr(), attributeslen: attributes.len(),
			methods: backingmemory3.as_ptr(), methodslen: methods.len(),
			backingmemory1: backingmemory1,
			backingmemory2: backingmemory2,
			backingmemory3: backingmemory3,
		}
	}
}

///Make up some random BASE64URL text into given buffer, and return buffer as string.
pub fn random_base64text<'a>(x: &'a mut [u8]) -> &'a str
{
	static BASE64: &'static [u8;64] = b"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";
	secure_random(x);
	for c in x.iter_mut() {
		*c = BASE64[(*c%64) as usize];
	}
	//This can not actually fail.
	from_utf8(x).unwrap_or("")
}


type CreateAuthInstance = extern fn(&mut AuthInstanceHandle, *const u8, usize);

static DLFCN_MAP: GlobalMutex<BTreeMap<FilenameBuf, DlfcnModule>> = GlobalMutex::new();

#[derive(Clone,Debug)]
pub struct DlfcnModule(Arc<_DlfcnModule>);
impl DlfcnModule
{
	pub fn new<P:AsRef<Path>>(path: P) -> Result<DlfcnModule, String>
	{
		let p = path.as_ref();
		//Builtin modules can never be non-Unicode.
		let is_builtin = p.to_str().map(|x|x.starts_with("$builtin/")).unwrap_or(false);
		let p = if is_builtin {
			p.to_path_buf()
		} else {
			p.canonicalize().map_err(|err|{
				format!("Failed to canonicalize {p}: {err}", p=p.display())
			})?
		};
		let p = Filename::from_os_str(p.as_os_str()).to_owned();
		DLFCN_MAP.with(|x|{
			if let Some(x) = x.get(&p) { return Ok(x.clone()); }
			let d = _DlfcnModule::new(&p)?;
			let d = DlfcnModule(Arc::new(d));
			x.insert(p.clone(), d.clone());
			Ok(d)
		})
	}
	fn get_create_fn(&self) -> Result<CreateAuthInstance, String> { self.0.get_create_fn() }
	fn get_name(&self) -> FilenameBuf { self.0.get_name() }
}

unsafe fn transerr(err: *const u8) -> String
{
	let mut len = 0;
	loop {
		if unsafe{ptr_read(err.offset(len)) == 0} {
			let s = unsafe{from_raw_parts(err, len as usize)};
			return safe_octets_to_text(s);
		}
		len += 1;
	}
}

#[derive(Debug)]
pub struct _DlfcnModule(LibraryHandle, FilenameBuf);

impl _DlfcnModule
{
	pub fn new(path: &Filename) -> Result<_DlfcnModule, String>
	{
		let h = LibraryHandle::new2(path).map_err(|err|{
			format!("Failed to load {path}: {err}")
		})?;
		Ok(_DlfcnModule(h, path.to_owned()))
	}
	pub fn get_create_fn(&self) -> Result<CreateAuthInstance, String>
	{
		unsafe{
			let h = &self.0;
			h.load_typed::<CreateAuthInstance>(b"certtool_acmeclient_auth_open").map_err(|err|{
				format!("Failed to look up certtool_acmeclient_auth_open: {err}")
			})
		}
	}
	pub fn get_name(&self) -> FilenameBuf
	{
		self.1.clone()
	}
}

fn encode_ip_into_dns(x: &AsciiStr) -> AsciiString
{
	let addr = match IpAddr::from_str(x.as_inner()) {
		Ok(x) => x,
		Err(_) => return AsciiString::new()
	};
	//The returned string is always ASCII.
	AsciiString::from_string(IpAddress::from(addr).to_rdns().to_string()).unwrap()
}

pub struct AuthInstanceList(Vec<AuthInstanceHandle>, AsciiString, Arc<Mutex<Vec<String>>>);

impl AuthInstanceList
{
	pub fn new(auth_modules: &[(String, DlfcnModule, String)], thumbprint: &Base64UrlStr,
		debug: &Debugging) -> AuthInstanceList
	{
		let modules = auth_modules;
		let msgq = Arc::new(Mutex::new(Vec::new()));
		let mut ilist = Vec::new();
		for i in modules.iter() {
			let h = match AuthInstanceHandle::new(&i.0, &i.1, &i.2, msgq.clone()) {
				Ok(x) => x,
				Err(err) => {
					warning!(debug, "{err}");
					continue;
				}
			};
			ilist.push(h);
		}
		let thumbprint = base64url_string_to_ascii_string(thumbprint).to_owned();
		AuthInstanceList(ilist, thumbprint, msgq)
	}
	pub fn add_chal(&mut self, id: &AcmeAuthorizationTarget, chals: &BTreeMap<AsciiString, AcmeChallenge>,
		debug: &Debugging) -> Option<AsciiString>
	{
		let mut attributes = Vec::new();
		let mut methods = Vec::new();
		let is_dns = &id.id_type == An("dns");
		let is_ip = &id.id_type == An("ip");
		attributes.push((An("THUMBPRINT"), self.1.clone()));
		for (meth, token) in chals.iter() {
			if meth == An("http-01") && (is_dns || is_ip) {
				methods.push(An("http-01"));
				let tok = base64url_string_to_ascii_string(&token.token);
				attributes.push((An("HTTP01_TOKEN"), tok.to_owned()));
				attributes.push((An("HTTP01_FILENAME"), tok.to_owned()));
				//This is always valid ASCII string if parts are. FIXME: need [] if id is a IPv6
				//address.
				let (lb, rb) = if id.id_value.find(':').is_some() {
					("[","]")
				} else {
					("","")
				};
				let url = AsciiString::from_string(format!(
					"http://{lb}{id}{rb}/.well-known/acme-challenge/{tok}", id=id.id_value)).
					unwrap();
				//The concatenation of two ascii strings with '.' in the middle has to be an ascii
				//string.
				let content = AsciiString::from_string(format!("{tok}.{id}", id=self.1.clone())).
					unwrap();
				attributes.push((An("HTTP01_URL"), url));
				attributes.push((An("HTTP01_CONTENT"), content));
			} else if meth == An("tls-alpn-01") && (is_dns || is_ip) {
				methods.push(An("tls-alpn-01"));
				let name = if is_ip {
					encode_ip_into_dns(&id.id_value)
				} else {
					id.id_value.clone()
				};
				if name.len() > 0 { attributes.push((An("TLSALPN01_SNI"), name)); }
				let tok = base64url_string_to_ascii_string(&token.token);
				attributes.push((An("TLSALPN01_TOKEN"), tok.to_owned()));
				//The concatenation of two ascii strings with '.' in the middle has to be an ascii
				//string.
				let content = AsciiString::from_string(format!("{tok}.{id}", id=self.1.clone())).
					unwrap();
				let rawresponse = hexencode(&sha256(content.as_bytes()));
				attributes.push((An("TLSALPN01_KEY_AUTHORIZATION"), content));
				attributes.push((An("TLSALPN01_RESPONSE"), rawresponse));
			} else if meth == An("dns-01") && is_dns {
				methods.push(An("dns-01"));
				let tok = base64url_string_to_ascii_string(&token.token);
				attributes.push((An("DNS01_TOKEN"), tok.to_owned()));
				//The concatenation of two ascii strings with '.' in the middle has to be an ascii
				//string.
				let content = AsciiString::from_string(format!("{tok}.{id}", id=self.1.clone())).
					unwrap();
				let owner = AsciiString::from_string(format!("_acme-challenge.{id}", id=id.id_value)).
					unwrap();
				//This better be ASCII string.
				let rawresponse = base64url(&sha256(content.as_bytes()));
				attributes.push((An("DNS01_OWNER_NAME"), owner));
				attributes.push((An("DNS01_KEY_AUTHORIZATION"), content));
				attributes.push((An("DNS01_RESPONSE"), AsciiString::from_string(rawresponse).
					unwrap()));
			}
		}
		for i in self.0.iter_mut() {
			match i.add_chal(id, &attributes, &methods) {
				Some(meth) => {
					debug!(debug, DEBUG_MODULES, "module {mod} accepted {id} using {meth}",
						mod=i.error_ctx.0);
					return Some(meth);
				},
				None => {
					debug!(debug, DEBUG_MODULES, "module {mod} did not accept {id}",
						mod=i.error_ctx.0);
				},
			}
		}
		None
	}
	pub fn commit(&mut self) -> Result<(), ()>
	{
		for i in self.0.iter_mut() { i.commit()?; }
		Ok(())
	}
	pub fn dump_log(&self, debug: &Debugging)
	{
		let mut x = self.2.lock();
		//These have already been escaped.
		for i in x.iter() { warning!(debug, "{i}"); }
		x.clear();
	}
}

struct ErrorContext(FilenameBuf, Arc<Mutex<Vec<String>>>);

extern fn message_trampoline(ctx: &ErrorContext, msg: *const u8)
{
	let msg = unsafe{transerr(msg)};
	ctx.1.lock().push(format!("{ctx}: {msg}", ctx=ctx.0));
}

#[repr(C)]
pub struct AuthInstanceHandle
{
	version: usize,
	context: *mut (),
	destroy: Option<extern fn(*mut ())>,
	_add_chal: Option<extern fn(*mut (), ch: &AuthChallengeInfo) -> i32>,
	_commit: Option<extern fn(*mut ()) -> i32>,
	error: extern fn(&ErrorContext, msg: *const u8),
	error_ctx: Box<ErrorContext>,
	counter: u32,
	domlist: String,
}

impl AuthInstanceHandle
{
	fn new(domlist: &str, module: &DlfcnModule, config: &str, msgq: Arc<Mutex<Vec<String>>>) ->
		Result<AuthInstanceHandle, String>
	{
		let initfn = module.get_create_fn().map_err(|err|{
			format!("Module {mod} is not valid auth module: {err}", mod=module.get_name())
		})?;
		let mut config2 = config.as_bytes().to_owned();
		config2.push(0);
		let mut ihandle = unsafe{AuthInstanceHandle {
			version: 0xFFFFFFFF,
			context: transmute(0usize),
			destroy: None,
			_add_chal: None,
			_commit: None,
			error: message_trampoline,
			error_ctx: Box::new(ErrorContext(module.get_name(), msgq.clone())),
			counter: 0,
			domlist: domlist.to_owned(),
		}};
		initfn(&mut ihandle, config2.as_ptr(), config.len());
		fail_if!(ihandle.version != 0, format!("Bad version {ihv}", ihv=ihandle.version));
		fail_if!(ihandle.destroy.is_none(), format!("Destructor is NULL"));
		fail_if!(ihandle._add_chal.is_none(), format!("Add challenge function is NULL"));
		Ok(ihandle)
	}
	fn add_chal(&mut self, target: &AcmeAuthorizationTarget, attributes: &[(&AsciiStr, AsciiString)],
		methods: &[&AsciiStr]) -> Option<AsciiString>
	{
		//Scan domlist to figure out if we should even try.
		let mut best_length = 0;
		let mut best_polarity = self.domlist.len() == 0;	//Always accept if empty, probe otherwise.
		for spec in self.domlist.split(",") {
			let (polarity, spec) = if let Some(spec) = spec.strip_prefix("!") {
				(false, spec)
			} else {
				(true, spec)
			};
			if spec.len() > best_length && target.matches_spec(spec) {
				best_length = spec.len();
				best_polarity = polarity;
			}
		}
		fail_if_none!(!best_polarity);
		//Ok, try this.
		let errqlen = self.error_ctx.1.lock().len();
		let cinfo = AuthChallengeInfo::new(target, attributes, methods);
		let r = if let Some(_add_chal) = self._add_chal {
			(_add_chal)(self.context, &cinfo)
		} else {
			-1
		};
		if r < 0 || r >= methods.len() as i32 {
			let nerrqlen = self.error_ctx.1.lock().len();
			//If no error but failed, print message about that. Also print error about bad index even
			//if error was printed.
			if errqlen == nerrqlen || r >= 0 {
				let emsg = if r >= 0 {
					"<Illegal method index>\0"
				} else {
					"<No error message available>\0"
				};
				(self.error)(self.error_ctx.deref(), emsg.as_bytes().as_ptr());
			}
			None
		} else {
			self.counter += 1;
			Some(methods[r as usize].to_owned())
		}
	}
	fn commit(&mut self) -> Result<(), ()>
	{
		let errqlen = self.error_ctx.1.lock().len();
		if self.counter == 0 { return Ok(()); }	//Ignore if no challenges.
		let r = if let Some(_commit) = self._commit {
			(_commit)(self.context)
		} else {
			0
		};
		if r < 0 {
			let nerrqlen = self.error_ctx.1.lock().len();
			//If no error but failed, print message about that.
			if errqlen == nerrqlen {
				(self.error)(self.error_ctx.deref(), "<No error message available>\0".as_bytes().
					as_ptr());
			}
			Err(())
		} else {
			Ok(())
		}
	}
}

impl Drop for AuthInstanceHandle
{
	fn drop(&mut self) { if let Some(destroy) = self.destroy { (destroy)(self.context); } }
}

macro_rules! define_builtin_module
{
	($krate:ident as $name:expr) => {
		extern crate $krate;
		//Just cast it to some pointer-sized type.
		let init = unsafe{Symbol::new($krate::auth_open as *mut u8)};
		let symboltable: [(&'static str, Symbol);1] = [
			("certtool_acmeclient_auth_open", init)
		];
		LibraryHandle::new_builtin2($name, &symboltable);
	}
}

pub fn initialize_auth_modules()
{
	define_builtin_module!(acmeclient_authmod_tls_standalone as "tls-standalone");
	define_builtin_module!(acmeclient_authmod_tls_standalone_old as "tls-standalone-old");
	define_builtin_module!(acmeclient_authmod_btls_alpn as "tls-alpn");
	define_builtin_module!(acmeclient_authmod_btls_alpn_remote as "tls-alpn-remote");
	define_builtin_module!(acmeclient_authmod_webroot as "webroot");
	define_builtin_module!(acmeclient_authmod_dyndns as "dynamic-dns");
}
