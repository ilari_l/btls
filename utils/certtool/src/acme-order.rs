use crate::acme_daemon::CertDirTag;
use crate::acme_daemon::f2_certificate as f_certificate;
use crate::acme_daemon::f2_certtmp as f_certtmp;
use crate::acme_daemon::f2_certtmpfail as f_certtmpfail;
use crate::acme_daemon::f2_eecert as f_eecert;
use crate::acme_daemon::is_noninteractive;
use crate::acme_daemon::ReqDirTag;
use crate::acme_daemon::StateDirTag;
use crate::acme_daemon::write_to_syslog;
use super::acme_install::call_install;
use super::acme_install::CertificateInstallConfiguration;
use super::acme_install::InstallError;
use super::acme_install::InstallTestFlags;
use super::acme_install::write_ocsp_file;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::ResultExt;
use btls_aux_hash::sha256;
use btls_aux_filename::Filename;
use btls_aux_memory::SafeShowByteString;
use btls_aux_x509certparse::ParsedCertificate2;
use btls_aux_time::Timestamp;
use btls_aux_unix::LogLevel;
use btls_certtool::alert;
use btls_certtool::decode_pem_or_der_csr;
use btls_certtool::error;
use btls_certtool::info;
use btls_certtool::notice;
use btls_certtool::read_single_cert;
use btls_certtool::warning;
use btls_certtool::acme::AcmeAuthorizationTarget;
use btls_certtool::acme::AcmeChallenge;
use btls_certtool::acme_order::Authorizer;
use btls_certtool::acme_order::raw_sign_csr as _raw_sign_csr;
use btls_certtool::acme_rncheck::CertificateStore;
use btls_certtool::acme_rncheck::get_renew_when;
use btls_certtool::acme_rncheck::ScheduleRenewal;
use btls_certtool::acme_service::AcmeService;
use btls_certtool::acme_service::AcmeServiceRegistered;
use btls_certtool::authmod::AuthInstanceList;
use btls_certtool::authmod::DlfcnModule;
use btls_certtool::aia::IntermediatePile;
use btls_certtool::aia::SpecialIntermediatePile;
use btls_certtool::csr::CsrInfo;
use btls_certtool::debug::Debugging;
use btls_certtool::dns::AddressReplaceList;
use btls_certtool::strings::AsciiStr;
use btls_certtool::strings::AsciiString;
use btls_certtool::strings::Base64UrlStr;
use btls_certtool::wcheck::Rootpile;
use std::collections::BTreeMap;
use std::collections::BTreeSet;
use std::fmt::Arguments;
use std::fmt::Display;
use std::fs::File;
use std::fs::remove_file;
use std::fs::rename;
use std::io::ErrorKind as IoErrorKind;
use std::io::Read as IoRead;
use std::io::Write as IoWrite;
use std::path::Path;
use std::path::PathBuf;
use std::time::Duration;
use std::time::SystemTime;


///Test mode.
#[derive(Copy,Clone,PartialEq,Eq)]
pub enum OrderTestMode
{
	///Renew test: Always try to renew, discard the obtaned certificate.
	RenewTest,
	///Install test: Call install on previous certificate.
	InstallTest,
	///Status test: Run due-for-renew checks, but abort before actually renewing.
	StatusTest,
}

///Order flags.
pub struct OrderConfiguration<'a>
{
	///Debug output.
	pub debug: Debugging,
	///Address replace list for verification.
	pub replace: &'a AddressReplaceList,
	///The authentication modules to use.
	pub auth_modules: &'a [(String, DlfcnModule, String)],
	///Pile of intermediates to use for completing chain.
	pub pile: &'a IntermediatePile,
	///Special Pile of intermediates to use for completing chain.
	pub spile: &'a SpecialIntermediatePile,
	///Pile of roots for checking the obtained certificate.
	pub rootpile: &'a Rootpile,
	///The name of private key file. If None, try obtaining from CSR.
	pub key: Option<&'a Path>,
	///Certificate output directory.
	pub certdir: CertDirTag<&'a Path>,
	///State output directory.
	pub statedir: StateDirTag<&'a Path>,
	///Certificates assumed revoked.
	pub assume_revoked: &'a BTreeSet<(Vec<u8>,Vec<u8>)>,
	///Request input directory.
	pub reqdir: ReqDirTag<&'a Path>,
	///Hide not due messages.
	pub quiet: bool,
	pub daemon: bool,
}

struct OrderConfigurationWrapper<'a,'b>
{
	oc: &'b OrderConfiguration<'a>,
	ocsp_done: bool,
}

impl<'a,'b> CertificateStore for OrderConfigurationWrapper<'a,'b>
{
	fn get_certificate(&self, filename: &Filename) -> Result<Option<Vec<u8>>, String>
	{
		let certfile = f_eecert(self.oc.certdir, filename);
		match certfile.metadata() {
			Ok(_) => (),
			Err(err) => if err.kind() == IoErrorKind::NotFound {
				//No such certificate.
				return Ok(None);
			} else {
				fail!(format!("Failed to stat '{certfile}': {err}", certfile=certfile.display()))
			}
		};
		let cert = read_single_cert(&certfile)?;
		Ok(Some(cert))
	}
	fn get_issuer_cert(&self, filename: &Filename) -> Result<Option<Vec<u8>>, String>
	{
		let certfile = f_certificate(self.oc.certdir, filename).join("issuer");
		match certfile.metadata() {
			//This returns no such certificate if file exists but is empty, not if file does not exist.
			Ok(st) => if st.len() == 0 {
				return Ok(None)
			},
			Err(err) =>
				fail!(format!("Failed to stat '{certfile}': {err}", certfile=certfile.display()))
		};
		let cert = read_single_cert(&certfile)?;
		Ok(Some(cert))
	}
	fn assume_revoked(&self, _: &Filename, cert: &ParsedCertificate2) -> Result<bool, String>
	{
		let issid = sha256(cert.issuer.as_raw_issuer());
		let revoke = self.oc.assume_revoked.contains(&(issid.to_vec(), cert.serial_number.to_vec()));
		Ok(revoke)
	}
	fn csr_changed(&self, _: &Filename, _: &ParsedCertificate2) -> Result<Option<&'static str>, String>
	{
		//Don't do automatic CSR change renewals anymore.
		Ok(None)
	}
	fn update_ocsp(&mut self, filename: &Filename, ocsp: Vec<u8>, debug: &Debugging)
	{
		let certdir = f_certificate(self.oc.certdir, filename);
		self.ocsp_done = write_ocsp_file(&certdir, &ocsp).unwrap_or_else(|err|{
			warning!(debug, "{err}");
			false
		});
	}
}

fn to_authz_config<'a>(g: &OrderConfiguration<'a>) -> AuthzConfiguration<'a>
{
	AuthzConfiguration {
		debug: g.debug.clone(),
		replace: g.replace,
		auth_modules: g.auth_modules,
	}
}

fn to_install_config<'a>(g: &OrderConfiguration<'a>) -> CertificateInstallConfiguration<'a>
{
	CertificateInstallConfiguration {
		pile: g.pile,
		spile: g.spile,
		debug: g.debug.clone(),
		rootpile: g.rootpile,
		certdir: g.certdir,
	}
}

fn rename_on_fatal_fault(tempfile: &Path, ftempfile: &Path, test: Option<OrderTestMode>, debug: &Debugging)
{
	//No renames if any test is active.
	if test.is_some() { return; }
	rename(tempfile, ftempfile).unwrap_or_else(|err|{
		//TODO: This should have higher priority?
		error!(debug, "Failed to rename '{tempfile}' -> '{ftempfile}': {err}",
			tempfile=tempfile.display(), ftempfile=ftempfile.display());
	});
}

pub(crate) fn parse_csr(raw: &[u8], call_as: &impl Display) -> Result<(CsrInfo, Option<PathBuf>), String>
{
	let mut keyname = None;
	let contents = decode_pem_or_der_csr(raw, |line|{
		if let Some(line) = line.strip_prefix(b"$certtool-private-key: ") {
			let kname = Filename::from_bytes_checked(line).ok_or("Bad private key filename")?;
			keyname = Some(kname.into_path().to_path_buf());
		}
		Ok(())
	}).map_err(|err|{
		format!("{call_as}: {err}")
	})?;
	let info = CsrInfo::parse(&contents).map_err(|err|{
		format!("{call_as}: Failed to parse CSR: {err}")
	})?;
	Ok((info, keyname))
}

pub fn determine_next_renew(basename: &Filename, acme_primary: &mut AcmeService, g: &OrderConfiguration) ->
	Result<Option<Timestamp>, String>
{
	let mut store = OrderConfigurationWrapper{
		oc: g,
		ocsp_done: false,
	};
	let r = match get_renew_when(&mut store, basename, false, &mut Some(acme_primary), &g.debug)? {
		ScheduleRenewal::Now(_) => None,
		ScheduleRenewal::Later(renew_on) => Some(renew_on),
	};
	Ok(r)
}

///Do a run on certificate.
///
///Parameters:
///
/// * basename: The basename of the certificate, without path or extension.
/// * force_run_flag: Set to true if any certificate is installed or any OCSP response is updated.
/// * force_renewal: If true, then renewal is forced, irrespective of due-for-renew.
/// * request: The CSR to actually use (including time-variant).
/// * acme: The ACME service to use.
/// * g: Configuration for the order.
/// * test: Test flags for order.
/// * next_renew: Set to minimum time left to next renew.
/// * post_install: Hook to run after installing.
///
///Return on success:
///
/// * True: Certificate was renewed.
/// * False: Certificate not renewed.
///
///Return on error:
///
/// * Error message.
pub fn run_certificate(basename: &Filename, request: &Path, acme: &mut AcmeServiceRegistered, g: &OrderConfiguration,
	test: Option<OrderTestMode>, next_renew: &mut Option<Timestamp>, post_install: impl FnOnce(&Filename)) ->
	Result<bool, String>
{
	let dreq = request.display();
	let itest = InstallTestFlags {
		install_test: test == Some(OrderTestMode::InstallTest),
	};
	if test == Some(OrderTestMode::RenewTest) {
		let mut reqcontent = Vec::new();
		File::open(&request).and_then(|mut fp|{
			fp.read_to_end(&mut reqcontent)
		}).map_err(|err|{
			format!("Failed to read CSR '{dreq}': {err}")
		})?;
		let (info, _) = parse_csr(&reqcontent, &dreq)?;
		//This does not do an install, so do not call post_install.
		run_renew(basename, &info, acme, g, test)?;
		return Ok(true);
	}
	if test == Some(OrderTestMode::InstallTest) {
		//See if the certificate exists.
		if !f_eecert(g.certdir, basename).is_file() {
			info!(g.debug, "Testing {basename}: No previous certificate available");
			return Ok(false);
		}
		match call_install(basename, None, request, &to_install_config(g), false, itest, post_install) {
			Ok(_) => return Ok(true),
			Err(InstallError::Untrusted) => fail!(format!("install: Certificate not trusted")),
			Err(InstallError::Temporary(err)) => fail!(format!("install: {err}")),
			Err(InstallError::Permanent(err)) =>
				fail!(format!("install: Permanent certificate error: {err}")),
		}
	}

	let ftempfile = f_certtmpfail(g.certdir, basename);
	let tempfile = f_certtmp(g.certdir, basename);
	let ftempfilep = ftempfile.clone();
	//If fatal error file is older than a week, delete it,
	if let Ok(ftempfilemetadata) = ftempfilep.metadata() {
		if let Ok(modified) = ftempfilemetadata.modified() {
			if let Ok(age) = SystemTime::now().duration_since(modified) {
				if age > Duration::from_secs(7*86400) {
					warning!(g.debug, "Permanent Certificate Error file \
						older than a week (or renewal forced), trying to delete...");
					remove_file(&ftempfilep).ok();
				}
			}
		}
	}

	//Check for failed install last time...
	let failed_install = tempfile.is_file();
	let failed_install_fatal = ftempfile.is_file();

	let mut reqcontent = Vec::new();
	File::open(&request).and_then(|mut fp|{
		fp.read_to_end(&mut reqcontent)
	}).map_err(|err|{
		format!("Failed to read CSR '{dreq}': {err}")
	})?;
	//Grab key name if any. Override from configuration.
	let (info, keyname) = parse_csr(&reqcontent, &dreq)?;
	let keyname = keyname.as_deref();
	let keyname = g.key.or(keyname);
	let names = info.get_identifiers_for_preauth().map_err(|err|{
		format!("{dreq}: Failed to extract names: {err}")
	})?;
	for &(ref namekind, ref name) in names.iter() {
		fail_if!(namekind != "dns", format!("{dreq}: Unsupported name {namekind}:{name}"));
		AsciiStr::new_string(name).map_err(|bad|format!("{dreq}: Bad name {namekind}:{bad}"))?;
	}
	if failed_install || failed_install_fatal {
		if failed_install_fatal {
			warning!(g.debug, "Found permanent fault '{ftempfile}'. Trying to reinstall anyway...",
				ftempfile=ftempfile.display());
		} else {
			warning!(g.debug, "Found scratch file '{tempfile}'. Trying to reinstall...",
				tempfile=tempfile.display());
		}
		match call_install(basename, keyname, &request, &to_install_config(g), failed_install_fatal, itest,
			post_install) {
			Ok(()) => {
				//Worked now.
				if failed_install_fatal {
					notice!(g.debug, "Fault on '{basename}' cleared.");
				}
				return Ok(true);
			},
			Err(InstallError::Untrusted) => {
				if is_noninteractive() {
					alert!(g.debug, "Install: Certificate '{basename}' is untrusted!");
				}
				fail!(format!("install: Certificate not trusted"));
			},
			//Transient error. Fail the operation.
			Err(InstallError::Temporary(err)) => fail!(format!("install: {err}")),
			Err(InstallError::Permanent(err)) => {
				//Permanent error!
				if !failed_install_fatal {
					rename_on_fatal_fault(&tempfile, &ftempfile, test, &g.debug);
				}
				//If not interactive, log to syslog at Alert priority, as things are going very
				//wrong.
				if is_noninteractive() {
					alert!(g.debug, "Fatal error trying to install certificate '{basename}': \
						{err}");
				}
				fail!(format!("install: Permanent certificate error: {err}"));
			}
		}
	}
	//Needs renew?
	let mut store = OrderConfigurationWrapper{
		oc: g,
		ocsp_done: false,
	};
	match get_renew_when(&mut store, basename, false, &mut Some(acme.borrow_primary()), &g.debug) {
		Ok(ScheduleRenewal::Now(cause)) => {
			if test == Some(OrderTestMode::StatusTest) {
				info!(g.debug, "If this was not a test, would renew certificate '{basename}' \
					from '{req}' because {cause}", req=request.display());
				return Ok(false);	//No further processing, since this is just testing.
			} else if g.daemon {
				notice!(g.debug, "Renewing '{basename}' because '{cause}'");
			}
		},
		Ok(ScheduleRenewal::Later(renew_on)) => {
			if store.ocsp_done { info!(g.debug, "Certificate '{basename}' OCSP updated"); }
			*next_renew = Some(next_renew.unwrap_or(renew_on).first(renew_on));
			if !g.quiet && !g.daemon { show_renew_duration(basename, renew_on, &g.debug); }
			return Ok(false);	//No further processing, because certificate is not due.
		},
		Err(err) => {
			if is_noninteractive() {
				alert!(g.debug, "Due-for-renew check failed for '{basename}': {err}");
			}
			fail!(err);
		}
	}

	//Run ACME renew.
	run_renew(basename, &info, acme, g, test).map_err(|err|{
		format!("renew: {err}")
	})?;

	match call_install(basename, keyname, &request, &to_install_config(g), false, itest, post_install) {
		Ok(()) => {
			//Worked.
			notice!(g.debug, "Renewed '{basename}'.");
			write_to_syslog(format_args!("Renewed '{basename}'."), LogLevel::Notice);
			return Ok(true);
		},
		Err(InstallError::Untrusted) => {
			//If not interactive, log to syslog at alert priority, as things are going very badly wrong.
			if is_noninteractive() {
				alert!(g.debug, "Install: Certificate '{basename}' is untrusted!");
			}
			fail!("install: Certificate not trusted");
		},
		Err(InstallError::Temporary(err)) => {
			//Transient error. Fail the operation.
			fail!(format!("install: {err}"));
		},
		Err(InstallError::Permanent(err)) => {
			//Permanent error!
			rename_on_fatal_fault(&tempfile, &ftempfile, test, &g.debug);
			fail!(format!("install: Permanent certificate error: {err}"));
		}
	}
}

pub struct AuthzConfiguration<'a>
{
	pub debug: Debugging,
	pub replace: &'a AddressReplaceList,
	pub auth_modules: &'a [(String, DlfcnModule, String)],
}

impl<'a> Authorizer for AuthzConfiguration<'a>
{
	type State = AuthInstanceList;
	fn new_state(&self, thumbprint: &Base64UrlStr, debug: &Debugging) -> AuthInstanceList
	{
		AuthInstanceList::new(self.auth_modules, thumbprint, debug)
	}
	fn challenge(&self, ilist: &mut AuthInstanceList, id: &AcmeAuthorizationTarget,
		challenges: &BTreeMap<AsciiString, AcmeChallenge>, debug: &Debugging) ->
		Result<AsciiString, String>
	{
		ilist.add_chal(id, challenges, debug).ok_or(String::new())
	}
	fn commit(&self, ilist: &mut AuthInstanceList, _debug: &Debugging) -> Result<(), String>
	{
		ilist.commit().set_err(String::new())
	}
	fn dump_log(&self, ilist: &AuthInstanceList, debug: &Debugging)
	{
		ilist.dump_log(debug);
	}
	fn get_replaces(&self) -> &AddressReplaceList { self.replace }
}

pub(crate) fn raw_sign_csr(call_as: &impl Display, info: &CsrInfo, acme: &mut AcmeServiceRegistered,
	g: &AuthzConfiguration, not_drill: bool) -> Result<Vec<u8>, String>
{
	//Only run real runs with production endpoint.
	fail_if!(!not_drill && acme.is_production(), format!("Refusing to run tests with production endpoint"));
	_raw_sign_csr(call_as, info, acme, g, not_drill, &g.debug)
}

fn run_renew(basename: &Filename, info: &CsrInfo, acme: &mut AcmeServiceRegistered,
	g: &OrderConfiguration, test: Option<OrderTestMode>) -> Result<(), String>
{
	let not_drill = test.is_none();
	let certificate = raw_sign_csr(&basename, info, acme, &to_authz_config(g), not_drill)?;
	//Only save if not test (install test never calls into run_renew().
	if not_drill {
		let name = f_certtmp(g.certdir, basename);
		let ret = File::create(&name).and_then(|mut f|{
			f.write_all(&certificate)?;
			f.flush()
		});
		if let Err(err) = ret {
			//This is not merely bad, this is VERY BAD INDEED.
			warning!(g.debug, "Save FAILED\n{cert}", cert=SafeShowByteString(&certificate));
			alert!(g.debug, "Failed to write certificate to '{name}': {err}",
				name=name.display());
			crate::do_commit_fault();
		}
	}
	Ok(())
}

fn show_renew_duration(basename: &Filename, renew_on: Timestamp, debug: &Debugging)
{
	let now = Timestamp::now();
	let remain = now.delta(renew_on);
	if remain < 100 {
		show_renew(basename, format_args!("{remain} seconds"), debug);
	} else 	if remain < 5997 {
		let minutes = ((remain + 3) / 6) as f64 / 10.0;
		show_renew(basename, format_args!("{minutes} minutes"), debug);
	} else if remain < 359820 {
		let hours = ((remain + 180) / 360) as f64 / 10.0;
		show_renew(basename, format_args!("{hours} hours"), debug);
	} else {
		let days = ((remain + 4320) / 8640) as f64 / 10.0;
		show_renew(basename, format_args!("{days} days"), debug);
	}
}

fn show_renew(basename: &Filename, remain: Arguments, debug: &Debugging)
{
	info!(debug, "Certificate '{basename}' is due for renewal in {remain}");
}
