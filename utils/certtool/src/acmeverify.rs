use super::A;
use super::Ao;
use super::http::HttpRequest;
use super::deadline::Deadline;
use super::debug::DEBUG_ACME_CHECKS;
use super::debug::Debugging;
use super::dns::query_dns;
use super::dns::domain_address_records;
use super::dns::AddressReplaceList;
use super::httpget::get_insecure;
use super::strings::AsciiStr;
use super::strings::AsciiString;
use super::strings::base64url_string_to_ascii_string;
use super::strings::Base64UrlStr;
use super::strings::HostStr;
use super::tls::TlsConnection;
use super::transport::TcpTransport;
use btls_aux_dnsmsg::consts::DNSTYPE_TXT;
use btls_aux_fail::dtry;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_hash::sha256;
use btls_aux_http::http::StandardHttpHeader;
use btls_aux_memory::Hexdump;
use btls_aux_signatures::base64url;
use std::collections::BTreeMap;
use std::net::SocketAddr;
use std::ops::Deref;
use std::time::Duration;


const IN: btls_aux_dnsmsg::Class = btls_aux_dnsmsg::consts::DNSCLASS_IN;

fn follow_redirect(cur_location: &AsciiStr, location: &AsciiStr, challenge: &Base64UrlStr) ->
	Result<AsciiString, String>
{
	let req = HttpRequest::from_url(location).map_err(|err|{
		format!("Can't parse redirect URL: {err}")
	})?;
	//The token has to be preserved and a common mistake
	let challenge_astr = base64url_string_to_ascii_string(challenge);
	if req.path.find_s(challenge_astr).is_none() || req.target.ends_with(".well-known") {
		fail!(format!("{cur_location} misconfigured redirect to {location}"));
	}
	//HTTPS is not supported.
	fail_if!(req.is_https, format!("{cur_location} redirect to HTTPS not allowed"));
	//Ports.
	fail_if!(req.port != 80, format!("{cur_location} redirects to port {location}"));
	//DNS address validity
	//The DNS character set is 0-9 a-z A-Z, - and .
	let mut not_ip = false;
	let mut last_dot = false;
	for i in req.target.chars() {
		match i as u32 {
			45 => not_ip = true,
			46 if !last_dot => (),
			48..=57 => (),
			65..=90 => not_ip = true,
			97..=122 => not_ip = true,
			_ => fail!(format!("{cur_location} redirects to illegal host {location}"))
		};
		last_dot = (i as u32) == 46;
	}
	fail_if!(!not_ip, format!("{cur_location} redirects to IP address {location}"));
	//OK, looks sane.
	Ok(location.to_owned())
}

pub fn hexencode(data: &[u8]) -> AsciiString
{
	AsciiString::from_string(Hexdump(data).to_string()).unwrap()
}

fn get_sockaddrs_for(hostname: &HostStr, port: u16, replaces: &AddressReplaceList, debug: &Debugging,
	deadline: Deadline) -> Result<Vec<SocketAddr>, String>
{
	let addrs = domain_address_records(hostname.as_bytes(), IN, true, debug, deadline).map_err(|err|{
		format!("Failed to look up addresses for '{hostname}': {err}")
	})?;
	let mut stored = BTreeMap::new();
	let mut out = Vec::new();
	for addr in addrs.iter() {
		let (addr2, port) = replaces.map(addr, port).ok_or_else(||{
			format!("No mapping found for address {addr} port {port}")
		})?;
		let key = (addr2.clone(), port);
		if stored.contains_key(&key) { continue; }	//Skip duplicates.
		stored.insert(key, ());
		out.push(SocketAddr::new(addr2, port))
	}
	fail_if!(out.len() == 0, format!("No addresses found for '{hostname}' port {port}"));
	Ok(out)
}

pub(crate) fn is_get_redirect(code: u16) -> bool
{
	//301, 302, 303, 307 and 308.
	match code { 301 => true, 302 => true, 303 => true, 307 => true, 308 => true, _ => false }
}

fn compute_http01_url(hostname: &HostStr, challenge: &Base64UrlStr) -> AsciiString
{
	AsciiString::from_string(format!("http://{hostname}/.well-known/acme-challenge/{challenge}")).unwrap()
}

fn compute_http01_content(challenge: &Base64UrlStr, thumbprint: &Base64UrlStr) -> AsciiString
{
	AsciiString::from_string(format!("{challenge}.{thumbprint}")).unwrap()
}

fn _check_http01_challenge(hostname: &HostStr, challenge: &Base64UrlStr, thumbprint: &Base64UrlStr,
	target_address: SocketAddr, debug: &Debugging, deadline: Deadline) -> Result<(), String>
{
	let addr = target_address.to_string();
	let mut target_address = Some(target_address);
	let url = compute_http01_url(hostname, challenge);
	let origurl = url.clone();
	let mut requrl: AsciiString = url;
	debug!(debug, DEBUG_ACME_CHECKS, "HTTP-01: Checking '{hostname}' from {addr} (starting from '{requrl}', \
		expecting {challenge}.{thumbprint})...");
	loop {
		let freply = get_insecure(&requrl, target_address, debug, deadline)?;
		//Follow redirects.
		if is_get_redirect(freply.code) {
			let location = match freply.get_header(StandardHttpHeader::Location) {
				Ok(Some(x)) => x,
				Ok(None) => fail!(format!("No 'location' header in redirect!")),
				Err(err) => fail!(format!("Error reading 'location' header in redirect: {err}"))
			};
			requrl = follow_redirect(&requrl, &location, challenge)?;
			debug!(debug, DEBUG_ACME_CHECKS, "HTTP-01: Following redirect to '{requrl}'");
			target_address = None;	//Do not bind the address any more.
			continue;
		}
		fail_if!(freply.code != 200,
			format!("Bad HTTP status {code} in reply ({origurl} -> {requrl}) from {addr}",
			code=freply.code));
		let rtgt = if requrl.deref() != origurl.deref() {
			format!(" (redirected from '{origurl}')")
		} else if let Some(tgtaddr) = target_address {
			format!(" (from '{tgtaddr}')")
		} else {
			String::new()
		};
		let (got_chal, got_acct) = dtry!(NORET AsciiStr::new_octets(&freply.body)).and_then(|body|{
			for i in body.chars() {
				match i as u32 {
					45|46|95 => (),
					48..=57|65..=90|97..=122 => (),
					_ => fail!(())
				};
			}
			let mut itr = body.split('.');
			let got_chal = dtry!(itr.next());
			let got_acct = dtry!(itr.next());
			fail_if!(itr.next().is_some(), ());
			fail_if!(got_acct.len() > 256, ());
			let got_chal = dtry!(Base64UrlStr::new_string(got_chal.as_inner()));
			let got_acct = dtry!(Base64UrlStr::new_string(got_acct.as_inner()));
			Ok((got_chal, got_acct))
		}).map_err(|_|{
			format!("Bad HTTP-01 response from '{requrl}'{rtgt}: Does not look like HTTP-01 response")
		})?;
		fail_if!(got_chal != challenge,
			format!("Bad HTTP-01 response from '{requrl}'{rtgt}: Incorrect challenge echoback"));
		//These are safe to print after checks above.
		fail_if!(got_acct != thumbprint, format!("Bad HTTP-01 response from '{requrl}'{rtgt}: \
			Incorrect account thumbprint (got: {got_acct}, expected {thumbprint})"));
		debug!(debug, DEBUG_ACME_CHECKS, "HTTP-01: Check for {hostname} from {addr} successful");
		return Ok(());
	}
}

const MAX_CHALCHK_TIMEOUT: Duration = Duration::from_millis(10000);

pub fn check_http01_challenge(namekind: &AsciiStr, hostname: &AsciiStr, challenge: &Base64UrlStr,
	thumbprint: &Base64UrlStr, mapping: &AddressReplaceList, debug: &Debugging, deadline: Deadline) ->
	Result<(), String>
{
	fail_if!(namekind != CLASS_DNS, format!("HTTP-01: Don't know how to handle name kind '{namekind}'"));
	let hostname = HostStr::new_string(hostname.as_inner()).map_err(|_|{
		format!("Illegal DNS hostname '{hostname}'")
	})?;
	let addrs = get_sockaddrs_for(hostname, 80, mapping, debug, deadline)?;
	for addr in addrs.iter() {
		let deadline = deadline.derive(MAX_CHALCHK_TIMEOUT);
		_check_http01_challenge(hostname, challenge, thumbprint, addr.clone(), debug, deadline)?;
	}
	Ok(())
}

fn compute_tlsalpn01_response(challenge: &Base64UrlStr, thumbprint: &Base64UrlStr) -> Vec<u8>
{
	let ka = format!("{challenge}.{thumbprint}");
	sha256(ka.as_bytes()).to_vec()
}

pub fn check_tls_alpn_01_challenge(namekind: &AsciiStr, hostname: &AsciiStr, challenge: &Base64UrlStr,
	thumbprint: &Base64UrlStr, mapping: &AddressReplaceList, debug: &Debugging, deadline: Deadline) ->
	Result<(), String>
{
	fail_if!(namekind != CLASS_DNS, format!("TLS-ALPN-01: Don't know how to handle name kind '{namekind}'"));
	let hostname = HostStr::new_string(hostname.as_inner()).map_err(|_|{
		format!("Illegal DNS hostname '{hostname}'")
	})?;
	let addrs = get_sockaddrs_for(hostname, 443, mapping, debug, deadline)?;
	let response = compute_tlsalpn01_response(challenge, thumbprint);
	for addr in addrs.iter() {
		let deadline = deadline.derive(MAX_CHALCHK_TIMEOUT);
		debug!(debug, DEBUG_ACME_CHECKS, "TLS-ALPN-01: Checking '{hostname}' from {addr}...");
		let addr2 = Some(addr.clone());
		let checkfn = TlsConnection::<TcpTransport>::new_alpn_check;
		checkfn(hostname, &response, addr2, debug, deadline).map_err(|err|{
			err.to_string()
		})?;
		debug!(debug, DEBUG_ACME_CHECKS, "TLS-ALPN-01: Check for '{hostname}' from {addr} succesful.");
	}
	Ok(())
}

fn compute_dns01_qname(hostname: &AsciiStr) -> AsciiString { Ao(format!("_acme-challenge.{hostname}")) }

fn compute_dns01_value(challenge: &Base64UrlStr, thumbprint: &Base64UrlStr) -> Result<AsciiString, String>
{
	let ka = format!("{challenge}.{thumbprint}");
	Ok(Ao(base64url(&sha256(ka.as_bytes()))))
}

pub fn check_dns_01_challenge(namekind: &AsciiStr, hostname: &AsciiStr, challenge: &Base64UrlStr,
	thumbprint: &Base64UrlStr, _: &AddressReplaceList, debug: &Debugging, deadline: Deadline) ->
	Result<(), String>
{
	fail_if!(namekind != CLASS_DNS, format!("DNS-01: Don't know how to handle name kind '{namekind}'"));
	let name = compute_dns01_value(challenge, thumbprint)?;
	let authname: AsciiString = compute_dns01_qname(hostname);
	debug!(debug, DEBUG_ACME_CHECKS, "DNS-01: checking {hostname} (should find {name} in {authname})... ");
	//No suboperations, so no need to bound deadline.
	let dns_response = query_dns(authname.as_bytes(), IN, DNSTYPE_TXT, false, debug, deadline).map_err(|err|{
		format!("Failed to query TXT records for '{authname}': {err}")
	})?;
	for i in dns_response.iter() {
		if i.len() == 0 { continue; }
		if i[0] != name.len() as u8 { continue; }
		if &i[1..] != name.as_bytes() { continue; }
		//Match!
		debug!(debug, DEBUG_ACME_CHECKS, "DNS-01: checking {authname} successful.");
		return Ok(());
	}
	//Not found.
	fail!(format!("No correct validation response for domain"));
}

pub struct ValidationChecker
{
	pub clazz: &'static str,
	pub name: &'static str,
	pub func: fn(&AsciiStr, &AsciiStr, &Base64UrlStr, &Base64UrlStr, &AddressReplaceList, &Debugging,
		Deadline) -> Result<(), String>,
}

pub const HTTP_01: &'static str = "http-01";
pub const DNS_01: &'static str = "dns-01";
pub const TLS_ALPN_01: &'static str = "tls-alpn-01";
pub const CLASS_DNS: &'static str = "dns";

pub static VALIDATION_NAMES_DNS: [&'static str; 3] = [HTTP_01, TLS_ALPN_01, DNS_01];

pub static VALIDATORS: [ValidationChecker; 3] = [
	ValidationChecker{clazz:CLASS_DNS, name:HTTP_01, func:check_http01_challenge},
	ValidationChecker{clazz:CLASS_DNS, name:DNS_01, func:check_dns_01_challenge},
	ValidationChecker{clazz:CLASS_DNS, name:TLS_ALPN_01, func:check_tls_alpn_01_challenge},
];

pub fn check_challenge(method: &AsciiStr, namekind: &AsciiStr, hostname: &AsciiStr, challenge: &Base64UrlStr,
	thumbprint: &Base64UrlStr, mapping: &AddressReplaceList, debug: &Debugging, deadline: Deadline) ->
	Result<(), String>
{
	for i in VALIDATORS.iter() {
		if method != i.name || namekind != i.clazz { continue; }
		return (i.func)(namekind, hostname, challenge, thumbprint, mapping, debug, deadline);
	}
	fail!(format!("Unknown challenge type {method} for name class {namekind}"));
}

pub fn known_challenge(clazz: &AsciiString, name: &AsciiString) -> bool
{
	for i in VALIDATORS.iter() {
		if name == i.name && clazz == i.clazz{ return true; }
	}
	false
}


pub const HTTP01_CONTENT: &'static str = "http01_content";	//The file content for HTTP-01 validation.
pub const HTTP01_FILENAME: &'static str = "http01_filename";	//same as http01_token.
pub const HTTP01_METHOD: &'static str = "http01_method";	//"http-01".
pub const HTTP01_TOKEN: &'static str = "http01_token";		//The random token for HTTP-01 validation.
pub const HTTP01_URL: &'static str = "http01_url";		//The initial URL for HTTP-01 validation.
pub const DNS01_METHOD: &'static str = "dns01_method";		//"dns-01"
pub const DNS01_QNAME: &'static str = "dns01_qname";		//The QNAME for DNS-01 validation
pub const DNS01_TOKEN: &'static str = "dns01_token";		//The random token for DNS-01 validation.
pub const DNS01_VALUE: &'static str = "dns01_value";		//The response value for DNS-01 validation.
pub const BASE_THUMBPRINT: &'static str = "base_thumbprint";	//Account thumbprint.
pub const BASE_IDKIND: &'static str = "base_identifier_kind";	//Identifier kind (e.g. "dns").
pub const BASE_IDENT: &'static str = "base_identifier";		//Identifier
pub const TLSALPN01_METHOD: &'static str = "tlsalpn01_method";	//"tls-alpn-01"
pub const TLSALPN01_TOKEN: &'static str = "tlsalpn01_token";	//TLS-ALPN-01 random token (hex format).


fn fill_http01_variables_dns(map: &mut BTreeMap<AsciiString, AsciiString>, hostname: &AsciiStr,
	challenge: &Base64UrlStr, thumbprint: &Base64UrlStr) -> Result<(), String>
{
	let hostname = HostStr::new_string(hostname.as_inner()).map_err(|_|{
		format!("Illegal DNS hostname '{hostname}'")
	})?;
	map.insert(A(HTTP01_TOKEN), base64url_string_to_ascii_string(challenge).to_owned());
	map.insert(A(HTTP01_URL), compute_http01_url(hostname, challenge));
	map.insert(A(HTTP01_FILENAME), base64url_string_to_ascii_string(challenge).to_owned());
	map.insert(A(HTTP01_CONTENT), compute_http01_content(challenge, thumbprint));
	map.insert(A(HTTP01_METHOD), A(HTTP_01));
	Ok(())
}

fn fill_dns_01_variables_dns(map: &mut BTreeMap<AsciiString, AsciiString>, hostname: &AsciiStr,
	challenge: &Base64UrlStr, thumbprint: &Base64UrlStr) -> Result<(), String>
{
	map.insert(A(DNS01_TOKEN), base64url_string_to_ascii_string(challenge).to_owned());
	map.insert(A(DNS01_QNAME), compute_dns01_qname(hostname));
	map.insert(A(DNS01_VALUE), compute_dns01_value(challenge, thumbprint)?);
	map.insert(A(DNS01_METHOD), A(DNS_01));
	Ok(())
}

fn fill_tls_alpn_01_variables_dns(map: &mut BTreeMap<AsciiString, AsciiString>, _h: &AsciiStr,
	challenge: &Base64UrlStr, thumbprint: &Base64UrlStr) -> Result<(), String>
{
	map.insert(A(TLSALPN01_TOKEN), hexencode(&compute_tlsalpn01_response(challenge, thumbprint)));
	map.insert(A(TLSALPN01_METHOD), A(TLS_ALPN_01));
	Ok(())
}

struct FillVariables
{
	pub clazz: &'static str,
	pub name: &'static str,
	pub func: fn(&mut BTreeMap<AsciiString, AsciiString>, &AsciiStr, &Base64UrlStr, &Base64UrlStr) ->
		Result<(), String>,
}

static FILL_VARIABLES: [FillVariables; 3] = [
	FillVariables{clazz:CLASS_DNS, name:HTTP_01, func:fill_http01_variables_dns},
	FillVariables{clazz:CLASS_DNS, name:DNS_01, func:fill_dns_01_variables_dns},
	FillVariables{clazz:CLASS_DNS, name:TLS_ALPN_01, func:fill_tls_alpn_01_variables_dns},
];

pub fn fill_variables_method(map: &mut BTreeMap<AsciiString, AsciiString>, method: &AsciiStr, namekind: &AsciiStr,
	name: &AsciiStr, challenge: &Base64UrlStr, thumbprint: &Base64UrlStr) -> Result<(), String>
{
	map.insert(A(BASE_THUMBPRINT), base64url_string_to_ascii_string(thumbprint).to_owned());
	map.insert(A(BASE_IDENT), name.to_owned());
	map.insert(A(BASE_IDKIND), namekind.to_owned());
	for i in FILL_VARIABLES.iter() {
		if method != i.name || namekind != i.clazz { continue; }
		return (i.func)(map, name, challenge, thumbprint);
	}
	fail!(format!("Unknown challenge type {method} for name class {namekind}"));
}

#[cfg(test)]
mod test
{
	use crate::A;
	use crate::An;
	use crate::strings::Base64UrlStr;
	use super::follow_redirect;
	use super::HTTP01_CONTENT;
	use super::HTTP01_FILENAME;
	use super::HTTP01_METHOD;
	use super::HTTP01_TOKEN;
	use super::HTTP01_URL;
	use super::DNS01_METHOD;
	use super::DNS01_QNAME;
	use super::DNS01_TOKEN;
	use super::DNS01_VALUE;
	use super::CLASS_DNS;
	use super::HTTP_01;
	use super::DNS_01;
	use super::fill_variables_method;
	use super::TLSALPN01_METHOD;
	use super::TLSALPN01_TOKEN;
	use super::BASE_IDENT;
	use super::BASE_IDKIND;
	use super::BASE_THUMBPRINT;
	use super::TLS_ALPN_01;
	use std::collections::BTreeMap;

	fn b64(x: &str) -> &Base64UrlStr { Base64UrlStr::new_string(x).unwrap() }

	#[test]
	fn kat_http01()
	{
		let mut map = BTreeMap::new();
		fill_variables_method(&mut map, An(HTTP_01), An(CLASS_DNS), An("bar.foo.example.org"),
			b64("foo-http-01"), b64("zot")).unwrap();
		let mut kat = BTreeMap::new();
		kat.insert(A(BASE_IDENT), A("bar.foo.example.org"));
		kat.insert(A(BASE_IDKIND), A("dns"));
		kat.insert(A(BASE_THUMBPRINT), A("zot"));
		kat.insert(A(HTTP01_CONTENT), A("foo-http-01.zot"));
		kat.insert(A(HTTP01_FILENAME), A("foo-http-01"));
		kat.insert(A(HTTP01_METHOD), A("http-01"));
		kat.insert(A(HTTP01_TOKEN), A("foo-http-01"));
		kat.insert(A(HTTP01_URL), A("http://bar.foo.example.org/.well-known/acme-challenge/foo-http-01"));
		assert_eq!(map, kat);
	}

	#[test]
	fn kat_dns_01()
	{
		let mut map = BTreeMap::new();
		fill_variables_method(&mut map, An(DNS_01), An(CLASS_DNS), An("bar.foo.example.org"),
			b64("foo-dns-01"), b64("zot")).unwrap();
		let mut kat = BTreeMap::new();
		kat.insert(A(BASE_IDENT), A("bar.foo.example.org"));
		kat.insert(A(BASE_IDKIND), A("dns"));
		kat.insert(A(BASE_THUMBPRINT), A("zot"));
		kat.insert(A(DNS01_METHOD), A("dns-01"));
		kat.insert(A(DNS01_QNAME), A("_acme-challenge.bar.foo.example.org"));
		kat.insert(A(DNS01_TOKEN), A("foo-dns-01"));
		kat.insert(A(DNS01_VALUE), A("MJxr6lEnNya1aNTiNH4e_ZLX0B9fusF6moN_a1pD_8I"));
		assert_eq!(map, kat);
	}

	#[test]
	fn kat_tls_alpn_01()
	{
		let mut map = BTreeMap::new();
		fill_variables_method(&mut map, An(TLS_ALPN_01), An(CLASS_DNS), An("bar.foo.example.org"),
			b64("foo-alpn-01"), b64("zot")).unwrap();
		let mut kat = BTreeMap::new();
		kat.insert(A(BASE_IDENT), A("bar.foo.example.org"));
		kat.insert(A(BASE_IDKIND), A("dns"));
		kat.insert(A(BASE_THUMBPRINT), A("zot"));
		kat.insert(A(TLSALPN01_METHOD), A("tls-alpn-01"));
		kat.insert(A(TLSALPN01_TOKEN), A(
			"c41fe13772b45cd8e4b16a5ae9f40189e0f1fd21a60aa2d2be021d0cc9e0f950"));
		assert_eq!(map, kat);
	}

	#[test]
	fn follow_redirect_t()
	{
		let barzot = Base64UrlStr::new_string("barzot").unwrap();
		let empty = An("");
		//Good redirects.
		assert_eq!(&follow_redirect(empty, An("http://example.net/foo/barzot.txt"), barzot).unwrap(),
			An("http://example.net/foo/barzot.txt"));
		//Bad redirect (HTTPS not allowed for security reasons).
		assert!(&follow_redirect(empty, An("https://example.net/foo/barzot.txt"), barzot).is_err());
		assert!(&follow_redirect(empty, An("https://example.net/.well-known/acme-challenge/barzot"),
			barzot).is_err());
		//Bad redirect (loses path component).
		assert!(&follow_redirect(empty, An("http://example.net.well-known/acme-challenge/barzot"), barzot).
			is_err());
		//Bad redirect (to root).
		assert!(&follow_redirect(empty, An("http://example.net/"), barzot).is_err());
		//Bad redirect (invalid URL).
		assert!(&follow_redirect(empty, An("http:/example.net/.well-known/acme-challenge/barzot"), barzot).
			is_err());
		//Bad redirect (to port).
		assert!(&follow_redirect(empty, An("http://example.net:8443/.well-known/acme-challenge/barzot"),
			barzot).is_err());
		//Bad redirect (crossed ports).
		assert!(&follow_redirect(empty, An("https://example.net:80/.well-known/acme-challenge/barzot"),
			barzot).is_err());
		assert!(&follow_redirect(empty, An("http://example.net:443/.well-known/acme-challenge/barzot"),
			barzot).is_err());
		//Bad redirect (redirect to IPv4.).
		assert!(&follow_redirect(empty, An("http://156.25.21.22/.well-known/acme-challenge/barzot"), barzot).
			is_err());
		//Bad redirect (redirect to IPv6.).
		assert!(&follow_redirect(empty, An("http://[2003::25]/.well-known/acme-challenge/barzot"), barzot).
			is_err());
		//Bad redirect (bad hostname).
		assert!(&follow_redirect(empty, An("http://foo_bar.com/.well-known/acme-challenge/barzot"), barzot).
			is_err());
	}
}
