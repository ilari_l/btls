use crate::CertChain;
use crate::decode_pem_or_der_csr;
use crate::read_file2;
use crate::csr::CsrInfo;
use crate::csr::CsrName;
use crate::debug::Debugging;
use btls_aux_fail::f_continue;
use btls_aux_fail::f_return;
use btls_aux_fail::fail_if;
use btls_aux_filename::Filename;
use btls_aux_hash::sha256;
use btls_aux_memory::Hexdump;
use btls_aux_signatures::SignatureAlgorithmEnabled2;
use btls_aux_x509certparse::dn_to_text;
use btls_aux_x509certparse::ParsedCertificate2;
use btls_aux_x509certvalidation::FullCertificateValidation;
use btls_aux_x509certvalidation::full_certificate_validation;
use btls_aux_x509certvalidation::SanityCheckCertificateChainExtended;
use btls_aux_x509certvalidation::IpAddress;
use btls_aux_x509certvalidation::TrustAnchor;
use btls_aux_x509certvalidation::TrustAnchorOrdered;
use btls_aux_time::Timestamp;
use std::collections::BTreeSet;
use std::path::Path;
use std::fmt::Display;
use std::fs::File;
use std::fs::read_dir;
use std::io::Read;
use std::ops::Deref;
use std::str::from_utf8;
use std::str::FromStr;


#[derive(Clone,Debug)]
pub struct Rootpile
{
	certs: BTreeSet<TrustAnchorOrdered>,
}

impl Rootpile
{
	pub fn new() -> Rootpile
	{
		Rootpile {
			certs: BTreeSet::new(),
		}
	}
	pub fn add_file(&mut self, p: &Path, debug: &Debugging) -> Result<(), String>
	{
		handle_rootpile_file(self, p, false, debug)
	}
	pub fn add(&mut self, cert: &[u8], p: impl Display, idx: usize, debug: &Debugging)
	{
		let cert = f_return!(ParsedCertificate2::from(cert).map_err(|err|{
			warning!(debug, "Error parsing certficate #{idx} in '{p}': {err}");
		}), ());
		let ta = f_return!(TrustAnchor::from_certificate2(&cert).map_err(|err|{
			warning!(debug, "Error making trust anchor from #{idx} in '{p}': {err}");
		}), ());
		let ta = TrustAnchorOrdered::new(ta);
		self.certs.insert(ta);
	}
	pub fn len(&self) -> usize { self.certs.len() }
	pub fn iter<'a>(&'a self) -> impl Iterator<Item=&'a TrustAnchorOrdered>+Clone+'a
	{
		self.certs.iter()
	}
	pub(crate) fn borrow_inner(&self) -> &BTreeSet<TrustAnchorOrdered> { &self.certs }
}

fn do_cc_check(eecert: &[u8], icerts: &[&[u8]], trust_anchors: &BTreeSet<TrustAnchorOrdered>, ts: Timestamp,
	advance: Option<u64>, no_root: &mut bool) -> Result<(), String>
{
	let mut ocsp_required = false;
	let mut sct_required = false;
	let dummy = BTreeSet::new();
	let any_policy = SignatureAlgorithmEnabled2::unsafe_any_policy();
	let vparam = FullCertificateValidation {
		issuers: icerts,
		pins: &[][..],
		trust_anchors: trust_anchors,
		time_now: &ts,
		reference: None,		//Names are checked elsewhere.
		strict_algo: false,
		policy: any_policy,
		is_client: false,		//For server certificate.
		feature_flags: 0x7FFFFFFF,	//Everything but UNKNOWN.
		need_ocsp_flag: &mut ocsp_required,
		need_sct_flag: &mut sct_required,
		killist: &dummy,
		require_disclosed_ca: false,
		extended: SanityCheckCertificateChainExtended::default()
	};
	let eecert_p = ParsedCertificate2::from(eecert).map_err(|err|{
		format!("Unable to parse certifiate: {err}")
	})?;
	let ret = full_certificate_validation(&eecert_p, vparam);
	ret.map_err(|err|{
		*no_root = err.is_ca_unknown();
		err.to_string()
	})?;
	if let Some(advance) = advance {
		let limit_ts = ts + advance as i64;
		fail_if!(limit_ts >= eecert_p.not_after, "Insufficient certifiate validity");
		//No need to check intermediates, as full_certificate_validation() enforces validity nesting.
	}
	Ok(())
}

pub(crate) fn compare_case_insenstive(a: &[u8], b: &[u8]) -> bool
{
	if a.len() != b.len() { return false; }
	a.iter().map(|x|if x.wrapping_sub(65) < 26 { *x + 32 } else { *x }).
		zip(b.iter().map(|x|if x.wrapping_sub(65) < 26 { *x + 32 } else { *x })).
		all(|(a,b)|a==b)
}

fn do_validate_versus_roots2(eecert: &[u8], icertc: &CertChain, rootpile: &Rootpile, advance: Option<u64>,
	_debug: &Debugging, no_root: &mut bool) -> Result<(), String>
{
	let icerts: Vec<_> = icertc.iter().collect();
	do_cc_check(eecert, &icerts[..], &rootpile.certs, Timestamp::now(), advance, no_root).map_err(|err|{
		format!("Unable to validate certificate: {err}")
	})?;
	Ok(())
}

#[derive(Clone)]
pub enum ValidateRootError
{
	UnknownRoot,
	Other(String),
}

pub fn do_validate_versus_roots(eecert: &[u8], icertc: &CertChain, rootpile: &Rootpile, advance: Option<u64>,
	names_needed: &[String], keyhash: Option<String>, subject: Option<String>, debug: &Debugging) ->
	Result<(), ValidateRootError>
{
	let mut no_root = false;
	do_validate_versus_roots2(eecert, icertc, rootpile, advance, debug, &mut no_root).map_err(|err|{
		if no_root { ValidateRootError::UnknownRoot } else { ValidateRootError::Other(err) }
	})?;
	//Parse the first certificate for the names.
	let eeparse = ParsedCertificate2::from(eecert).map_err(|err|{
		format!("Unable to parse the EE certificate: {err}")
	}).map_err(ValidateRootError::Other)?;
	//Check CSR fields.
	do_check_csr_fields(&eeparse, names_needed, keyhash, subject).map_err(ValidateRootError::Other)
}

pub fn do_check_csr_fields(eeparse: &ParsedCertificate2, names_needed: &[String], keyhash: Option<String>,
	subject: Option<String>) -> Result<(), String>
{
	for i in names_needed.iter() {
		let mut ok = false;
		for j in eeparse.dnsnames.iter() {
			//Special for wildcards.
			if j.len() >= 2 && &j[..2] == b"*." {
				if let Some(split) = i.find(".") {
					let i = &i[split+1..];
					let j = &j[2..];
					ok |= compare_case_insenstive(j, i.as_bytes());
				}
			}
			//The compare needs to be case-insensitive.
			ok |= compare_case_insenstive(j, i.as_bytes());
		}
		for j in eeparse.dnsnames.iter_addrs() {
			//Ok if the name is an ip and is in block.
			if let Ok(ip) = IpAddress::from_str(i) { ok |= j.matches_ip(ip); }
		}
		fail_if!(!ok, format!("Certificate does not contain name `{i}`"));
	}
	if let Some(khash) = keyhash.as_ref() {
		let pkhash = sha256(eeparse.pubkey);
		let pkhash2 = Hexdump(&pkhash).to_string();
		fail_if!(!compare_case_insenstive(khash.as_bytes(), pkhash2.as_bytes()),
			format!("Wrong key in certificate (got {khash}, expected {pkhash2})"));
	}
	if let Some(subject) = subject.as_ref() {
		let rsubj = dn_to_text(eeparse.subject.as_raw_subject()).unwrap_or_else(|_|{
			"<ERROR>".to_owned()
		});
		fail_if!(rsubj.deref() != subject.deref(),
			format!("Wrong subject in certificate (got '{rsubj}', expected '{subject}')"));
	}
	//The certificate is ok. Return.
	Ok(())
}


fn handle_rootpile_file(rootpile: &mut Rootpile, p: &Path, nested: bool, debug: &Debugging) -> Result<(), String>
{
	if p.is_dir() {
		//Don't step into nested dirs.
		if nested { return Ok(()); }
		//Directory, recursively expand to all *.pem and *.crt files.
		let ditr = read_dir(p).map_err(|err|{
			format!("Error reading directory '{p}': {err}", p=p.display())
		})?;
		for entry in ditr {
			let entry = entry.map_err(|err|{
				format!("Error reading directory '{p}': {err}", p=p.display())
			})?;
			let path = entry.path();
			//None is '..'. Definitely do not do anything with that one. Also don't do anything with
			//files with non-unicode extensions or no extension at all.
			let extn = f_continue!(Filename::from_path_file_name(&path).and_then(|f|{
				f.extension_unicode()
			}));
			if extn == "crt" || extn == "pem" {
				//If this fails, only print a warning.
				if let Err(err) = handle_rootpile_file(rootpile, &path, true, debug) {
					warning!(debug, "{err}")
				}
			}
		}
	} else {
		//Read the cert.
		let mut contents = Vec::new();
		File::open(p).and_then(|mut x|x.read_to_end(&mut contents)).map_err(|err|{
			format!("Error reading '{p}': {err}", p=p.display())
		})?;
		//Unwrap PEM if any.
		use btls_aux_signatures::iterate_pem_or_derseq_fragments;
		use btls_aux_signatures::PemDerseqFragment2::*;
		use btls_aux_signatures::PemFragmentKind as PFKind;
		let mut idx = 0;
		iterate_pem_or_derseq_fragments(&contents, |frag|match frag {
			Comment(_) => Ok(()),
			Error => Err(format!("Not in PEM nor DERseq format")),
			Der(cert)|Pem(PFKind::Certificate,cert) => {
				rootpile.add(cert, p.display(), idx, debug);
				idx += 1;
				Ok(())
			},
			Pem(_,_) => Ok(())	//Ignore unknown.
		}).map_err(|err|{
			format!("Error splitting '{p}': {err}", p=p.display())
		})?;
	}
	Ok(())
}

pub fn override_fields_from_csr(csr: &Path, namelist: &mut Vec<String>, key: &mut Option<String>,
	subject: &mut Option<String>) -> Result<(), String>
{
	let mut check_subject = false;
	//Read the CSR and unwrap PEM.
	let dcsr = csr.display();
	let contents = read_file2(&csr).map_err(|err|{
		format!("Failed to read '{dcsr}': {err}")
	})?;
	let contents2 = decode_pem_or_der_csr(&contents, |line|{
		let line = from_utf8(line).unwrap_or("");
		if line == "$certtool-check-subject" {
			check_subject |= subject.is_none();
		}
		if let Some(line) = line.strip_prefix("$certtool-check-subject: ") {
			*subject = Some(line.to_owned());
			check_subject = false;	//Already overridden.
		}
		Ok(())
	}).map_err(|err|{
		format!("{dcsr}: {err}")
	})?;

	let info = CsrInfo::parse(&contents2).map_err(|err|{
		format!("{dcsr}: Parsing CSR: {err}")
	})?;
	//Override fields.
	if check_subject {
		*subject = Some(dn_to_text(info.get_subject()).unwrap_or_else(|_|{
			"<ERROR>".to_owned()
		}));
	}
	let pkhash = sha256(info.get_pubkey());
	let pkhash2 = Hexdump(&pkhash).to_string();
	*key = Some(pkhash2);
	namelist.clear();
	for i in info.get_names().iter() {
		match i {
			&CsrName::Dns(ref x) => namelist.push(x.clone()),
			&CsrName::Ip(ref x) => namelist.push(x.clone()),
			_ => (),	//Ignore other types.
		}
	}
	Ok(())
}
