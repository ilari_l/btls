use super::An;
use super::debug;
use super::warning;
use crate::acme::AcmeDirectory;
use crate::acme::get_object_explicit;
use crate::acme::HttpsConnect;
use crate::acme::ObjectFromOctets;
use crate::acme::sign_jws;
use crate::acmeerror::format_any_error;
use crate::acmeerror::print_rawoctets;
use crate::deadline::Deadline;
use crate::debug::DEBUG_ACCOUNT;
use crate::debug::DEBUG_SIGNATURE;
use crate::debug::Debugging;
use crate::http::HttpReply;
use crate::http::propagate_trust_anchors;
use crate::http::split_url;
use crate::privatekey::Privatekey;
use crate::strings::AsciiStr;
use crate::strings::AsciiString;
use crate::strings::Base64UrlStr;
use crate::strings::Base64UrlString;
use crate::strings::compare_case_insensitive;
use crate::wcheck::Rootpile;
use btls_aux_fail::fail;
use btls_aux_fail::fail_if;
use btls_aux_fail::ResultExt;
use btls_aux_hash::sha256;
use btls_aux_http::http::StandardHttpHeader;
use btls_aux_json2::JSON;
use btls_aux_json2::json_object;
use btls_aux_signatures::base64url;
use btls_aux_signatures::extract_jwk3;
use std::mem::replace;
use std::ops::Deref;
use std::rc::Rc;
use std::thread::sleep;
use std::time::Duration;


pub struct AcmeService
{
	production: bool,
	underlying: HttpsConnect,
	dir: AcmeDirectory,
	cached_nonce: Option<AsciiString>,
	url: AsciiString,
}

const MAX_REQUEST: Duration = Duration::from_millis(600000);
const MAX_ATTEMPT_S: Duration = Duration::from_millis(30000);
const MAX_ATTEMPT_L: Duration = Duration::from_millis(300000);
const NONCE_FAIL_WAIT: Duration = Duration::from_millis(1000);
const GAP_FAIL_WAIT: Duration = Duration::from_millis(2000);

pub fn get_object_registered<O:ObjectFromOctets>(url: &AsciiStr, acme: &mut AcmeServiceRegistered,
	debug: &Debugging) -> Result<O, String>
{
	let data = acme.get(url, debug)?;
	get_object_explicit(url, &data)
}

impl AcmeService
{
	pub fn new(dirurl: &AsciiStr, pile: &Rootpile, debug: &Debugging) -> Result<AcmeService, String>
	{
		let (scheme, host, _, _) = split_url(dirurl, None).map_err(|_|{
			format!("Invalid directory URL {dirurl}")
		})?;
		fail_if!(scheme != "https", format!("Invalid directory URL {dirurl}"));
		let mut underlying = HttpsConnect::new(host, pile);
		//Directory fetch is always short.
		let directory = underlying.do_http_get(dirurl, debug, Deadline::new(MAX_REQUEST),
			MAX_ATTEMPT_S)?;
		fail_if!(!directory.is_success(),
			format!("Directory {dirurl}: fetch failed with code {code}", code=directory.display_code()));
		let d = get_object_explicit::<AcmeDirectory>(&dirurl, &directory.body)?;
		//ARI might not be on the same host as main ACME for load reasons, so propagate trust anchors so
		//connecting to ARI will work.
		if let Some(ari) = d.ari_base.as_ref() {
			if let Ok((_, arihost, _, _)) = split_url(ari, None) {
				propagate_trust_anchors(host, arihost);
			}
		}
		let production = compare_case_insensitive(dirurl.as_inner(),
			"https://acme-v02.api.letsencrypt.org/directory").is_eq();
		Ok(AcmeService{
			underlying: underlying,
			dir: d,
			production: production,
			cached_nonce: None,
			url: dirurl.to_owned(),
		})
	}
	pub fn directory(&self) -> AcmeDirectory { self.dir.clone() }
	fn nonce(&mut self, debug: &Debugging, deadline: Deadline) -> Result<AsciiString, String>
	{
		//Nonce fetch is always short.
		loop {
			//This fails in case there is not even a response.
			let nonce = self.underlying.do_http_head(&self.dir.new_nonce, debug,
				deadline, MAX_ATTEMPT_S)?;
			//If there is nonce, return it.
			if let Ok(Some(nonce)) = nonce.get_header(StandardHttpHeader::ReplayNonce) {
				return Ok(nonce);
			}
			if nonce.is_success() {
				warning!(debug, "Nonce {nonce}: No replay nonce in resonse",
					nonce=self.dir.new_nonce);
			} else {
				warning!(debug, "Nonce {nonce}: fetch failed with code {code}",
					nonce=self.dir.new_nonce, code=nonce.display_code());
			}
			fail_if!(deadline.expired(), "Deadline expired while waiting for nonce");
			//Hold back even a bit from bombarding the ACME server.
			sleep(NONCE_FAIL_WAIT);
		}
	}
	//This is not Post-as-Get.
	pub fn get_real(&mut self, url: &AsciiStr, debug: &Debugging) -> Result<Vec<u8>, String>
	{
		//Always short.
		let deadline = Deadline::new(MAX_REQUEST);
		let ans = self.underlying.do_http_get(url, debug, deadline, MAX_ATTEMPT_S)?;
		if ans.is_error() {
			fail!(format!("GET {url}: fetch failed with code {code}: {err}",
				code=ans.display_code(), err=format_any_error(&ans.body)));
		}
		Ok(ans.body)
	}
	//The keypair is here because this will eventually be converted to POST-as-GET.
	fn get(&mut self, url: &AsciiStr, account: &AsciiStr, keypair: &Privatekey, debug: &Debugging) ->
		Result<Vec<u8>, String>
	{
		//Always short.
		let deadline = Deadline::new(MAX_REQUEST);
		let ans = self.post_rawres(url, None, Some(account), keypair, debug, deadline, MAX_ATTEMPT_S, true)?;
		if ans.is_error() {
			fail!(format!("GET {url}: fetch failed with code {code}: {err}",
				code=ans.display_code(), err=format_any_error(&ans.body)));
		}
		Ok(ans.body)
	}
	fn post_rawbody_retry(&mut self, url: &AsciiStr, payload: &JSON, account: Option<&AsciiStr>,
		keypair: &Privatekey, debug: &Debugging) -> Result<(), String>
	{
		let deadline = Deadline::new(MAX_REQUEST);
		//This is only used for challenge request, which always uses long timeout and retries like gap.
		self.post_rawres(url, Some(payload), account, keypair, debug, deadline, MAX_ATTEMPT_L, true)?;
		Ok(())
	}
	fn post_rawbody(&mut self, url: &AsciiStr, payload: &JSON, account: Option<&AsciiStr>,
		keypair: &Privatekey, debug: &Debugging, long: bool) ->
			Result<(Option<AsciiString>, Vec<u8>), String>
	{
		let deadline = Deadline::new(MAX_REQUEST);
		let max_attempt = if long { MAX_ATTEMPT_L } else { MAX_ATTEMPT_S };
		let ans = self.post_rawres(url, Some(payload), account, keypair, debug, deadline, max_attempt, false)?;
		let location = ans.get_header(StandardHttpHeader::Location).unwrap_or(None);
		Ok((location, ans.body.clone()))
	}
	fn post_rawres(&mut self, url: &AsciiStr, payload: Option<&JSON>, account: Option<&AsciiStr>,
		keypair: &Privatekey, debug: &Debugging, deadline: Deadline, max_attempt: Duration, is_gap: bool) ->
		Result<HttpReply, String>
	{
		let mut i = 0;
		loop {
			i += 1;
			let nonce2 = replace(&mut self.cached_nonce, None);
			let nonce2 = match nonce2 {
				Some(x) => x,
				//The nonce() automatically uses short deadline.
				None => self.nonce(debug, deadline)?
			};
			debug!(debug, DEBUG_SIGNATURE, "POST {url}: Signing message...");
			let payload = if let Some(payload) = payload.as_ref() {
				sign_jws(keypair, payload.serialize_string().as_bytes(), url, account, &nonce2)
			} else {
				static DUMMY: [u8;0] = [];
				sign_jws(keypair, &DUMMY[..], url, account, &nonce2)
			};
			let payload = payload.map_err(|err|{
				format!("POST {url}: Failed to sign JWS: {err}")
			})?;
			debug!(debug, DEBUG_SIGNATURE, "POST {url}: message signed.");
			let ans = self.underlying.do_http_post(url, payload.as_bytes(),
				Some(An("application/jose+json")), debug, deadline, max_attempt)?;
			self.cached_nonce = ans.get_header(StandardHttpHeader::ReplayNonce).unwrap_or(None);
			if ans.is_error() {
				//For GET-as-POST, error codes 429 and 5xx are always retried. The post CSR submission
				//status checks and certificate download can not be allowed to fail.
				if is_gap && ans.is_tempfail() {
					i -= 1;		//Undo increment of try count.
					warning!(debug, "Get {url} tempfailed: {code}", code=ans.display_code());
					sleep(GAP_FAIL_WAIT);
					continue;
				}
				//Is this BadNonce error?
				let err = ans.body_as_json().map_err(|err|{
					format!("POST {url}: {err}{body}", body=print_rawoctets(&ans.body))
				})?;
				fail_if!(!err.is_object(), format!("POST {url}: Error is not object{body}",
					body= print_rawoctets(&ans.body)));
				let errtype = err.field("type").and_then(|x|x.as_string()).ok_or_else(||{
					format!("POST {url}: No type in error{body}",
						body=print_rawoctets(&ans.body))
				})?;
				if errtype != "urn:ietf:params:acme:error:badNonce" {
					fail!(format!("POST {url}: {body}", body=format_any_error(&ans.body)));
				}
				if i < 10 {
					warning!(debug, "POST {url}: Got bad nonce error. Retrying...");
					fail_if!(self.cached_nonce.is_none(),
						format!("POST {url}: got BadNonce but no new nonce"));
					continue;
				} else {
					fail!(format!("Too many bad nonce errors in loop"))
				}
			}
			return Ok(ans);
		}
	}
	fn register_account(&mut self, keypair: &Privatekey, thumbprint: &Base64UrlStr, debug: &Debugging) ->
		Result<AsciiString, String>
	{
		let acct_body = json_object!(
			termsOfServiceAgreed => true
		);
		let accturl = self.dir.new_account.clone();
		debug!(debug, DEBUG_ACCOUNT, "Trying to register account...");
		//Account registration uses short timeout, as it should be fast.
		let (location, _) = self.post_rawbody(&accturl, &acct_body, None, keypair, debug, false).map_err(|err|{
			format!("Failed to create account: {err}")
		})?;
		let location = if let Some(location) = location {
			debug!(debug, DEBUG_ACCOUNT,
				"Got account identifier {location} for thumbprint {thumbprint}");
			location
		} else {
			fail!(format!("NewAccount {url} returned success but no account URL???",
				url=self.dir.new_account));
		};
		Ok(location)
	}
}

pub struct AcmeServiceRegistered
{
	endpoint: AcmeService,
	key: Rc<Privatekey>,
	thumbprint: Base64UrlString,
	account_id: AsciiString,
	primary: Option<AcmeService>,
}

impl AcmeServiceRegistered
{
	pub fn new<AcctDb:AccountIdDatabase>(mut endpoint: AcmeService, acctids: &mut AcctDb, key: Rc<Privatekey>,
		debug: &Debugging, no_register: bool, primary: Option<AcmeService>) ->
		Result<AcmeServiceRegistered, String>
	{
		let pubkey = extract_jwk3(key.deref()).
			set_err("Failed to export JWK for the account key")?;
		let thumbprint = Base64UrlString::from_string(base64url(&sha256(&pubkey.serialize_string().
			as_bytes()))).set_err("Failed to base64url-encode thumbprint")?;
		let acctid = if let Some(acctid) = acctids.lookup(&endpoint.url, &thumbprint) {
			debug!(debug, DEBUG_ACCOUNT,
				"Assuming account identifier {acctid } for thumbprint {thumbprint}");
			acctid.to_owned()
		} else {
			let acctid = endpoint.register_account(&key, &thumbprint, debug).map_err(|err|{
				format!("Failed to register account: {err}")
			})?;
			//Disable registration if asked. Used for test mode.
			if !no_register { acctids.store(&endpoint.url, &thumbprint, &acctid); }
			acctid
		};
		Ok(AcmeServiceRegistered {
			endpoint: endpoint,
			key: key,
			thumbprint: thumbprint,
			account_id: acctid,
			primary: primary,
		})
	}
	pub fn get(&mut self, url: &AsciiStr, debug: &Debugging) -> Result<Vec<u8>, String>
	{
		self.endpoint.get(url, &self.account_id, &self.key, debug)
	}
	pub fn post_rawbody_retry(&mut self, url: &AsciiStr, payload: &JSON, debug: &Debugging) -> Result<(), String>
	{
		//This always has valid account ID.
		self.endpoint.post_rawbody_retry(url, payload, Some(&self.account_id), &self.key, debug)
	}
	pub fn post_rawbody(&mut self, url: &AsciiStr, payload: &JSON, debug: &Debugging, long: bool) ->
		Result<(Option<AsciiString>, Vec<u8>), String>
	{
		//This always has valid account ID.
		self.endpoint.post_rawbody(url, payload, Some(&self.account_id), &self.key, debug, long)
	}
	pub fn account_id(&self) -> &AsciiStr { &self.account_id }
	pub fn thumbprint(&self) -> &Base64UrlStr { &self.thumbprint }
	pub fn directory(&self) -> AcmeDirectory { self.endpoint.dir.clone() }
	pub fn is_production(&self) -> bool { self.endpoint.production }
	pub fn borrow_primary(&mut self) -> &mut AcmeService
	{
		//Return primary, falling back to endpoint.
		match self.primary.as_mut() {
			Some(x) => x,
			None => &mut self.endpoint
		}
	}
}

pub trait AccountIdDatabase
{
	fn lookup(&mut self, url: &AsciiStr, thumbprint: &Base64UrlStr) -> Option<AsciiString>;
	fn store(&mut self, url: &AsciiStr, thumbprint: &Base64UrlStr, acctid: &AsciiStr);
}
