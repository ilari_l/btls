#![warn(unsafe_op_in_unsafe_fn)]
#[macro_use] extern crate acmeclient_authmod_helper;
use acmeclient_authmod_helper::ChallengeInfo;
use acmeclient_authmod_helper::DynInstance;
use acmeclient_authmod_helper::Instance;
use acmeclient_authmod_helper::MethodId;
use acmeclient_authmod_helper::TLS_ALPN_01;
use acmeclient_authmod_helper::TLSALPN01_KEY_AUTHORIZATION;
use acmeclient_authmod_helper::TLSALPN01_SNI;
use std::fs::File;
use std::fs::remove_file;
use std::io::Write;
use std::path::Path;
use std::path::PathBuf;
use std::thread::sleep;
use std::time::Duration;


struct AuthHandle
{
	basepath: PathBuf,
	tempfiles: Vec<PathBuf>,
	contents: Vec<(String, String)>,
}

impl Drop for AuthHandle
{
	fn drop(&mut self) { self.clear(); }
}

impl Instance for AuthHandle
{
	fn new(config: &str) -> Self
	{
		AuthHandle{
			basepath: Path::new(config).to_path_buf(),
			tempfiles: Vec::new(),
			contents: Vec::new(),
		}
	}
}

impl DynInstance for AuthHandle
{
	fn add_challenge(&mut self, ch: &ChallengeInfo) -> Result<MethodId, String>
	{
		let httpid = ch.method_id(TLS_ALPN_01).ok_or_else(||format!("tls-alpn-01 method not supported"))?;
		let filename = ch.attribute(TLSALPN01_SNI).
			ok_or_else(||format!("tls-alpn-01 method not supported"))?;
		let content = ch.attribute(TLSALPN01_KEY_AUTHORIZATION).
			ok_or_else(||format!("tls-alpn-01 method not supported"))?;
		//Push the names and contents to queue just to show use of commit() method.
		self.contents.push((filename.to_owned(), content.to_owned()));
		Ok(httpid)
	}
	fn commit(&mut self) -> Result<(), String>
	{
		//Create tempfiles for all names and values mentioned in added challenges.
		for (filename, content) in self.contents.drain(..) {
			let fpath = self.basepath.join(filename);
			File::create(&fpath).and_then(|mut fp|fp.write_all(content.as_bytes())).map_err(|err|{
				format!("Failed to write {fpath}: {err}", fpath=fpath.display())
			})?;
			trace!("btls-alpn: Wrote file {fpath}", fpath=fpath.display());
			self.tempfiles.push(fpath);
		}
		sleep(Duration::from_secs(5));	//Wait for reload on ACME directory.
		Ok(())
	}
	fn name(&self) -> String { "btls-alpn".to_string() }
	fn clear(&mut self) -> Duration
	{
		for i in self.tempfiles.drain(..) {
			if let Err(err) = remove_file(&i) {
				errmsg!("btls-alpn: Failed to remove tempfile {i}: {err}", i=i.display());
			} else {
				trace!("btls-alpn: Removed file {i}", i=i.display());
			}
		}
		self.contents.clear();
		Duration::from_secs(0)		//No need to wait for any caches.
	}
}

authmod_instance!(AuthHandle);
