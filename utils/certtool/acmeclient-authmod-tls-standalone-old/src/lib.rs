#![warn(unsafe_op_in_unsafe_fn)]
#[macro_use] extern crate acmeclient_authmod_helper;
extern crate btls;
extern crate btls_aux_unix;
use acmeclient_authmod_helper::ChallengeInfo;
use acmeclient_authmod_helper::DynInstance;
use acmeclient_authmod_helper::Instance;
use acmeclient_authmod_helper::MethodId;
use acmeclient_authmod_helper::TLS_ALPN_01;
use acmeclient_authmod_helper::TLSALPN01_KEY_AUTHORIZATION;
use acmeclient_authmod_helper::TLSALPN01_SNI;
use btls::Connection;
use btls::ServerConfiguration;
use btls::ServerConnection;
use btls::callbacks::TlsCallbacks;
use btls::certificates::CertificateLookup;
use btls::logging::Logging;
use btls::logging::MessageSeverity;
use btls::transport::PullTcpBuffer;
use btls::transport::PushTcpBuffer;
use btls::utility::Mutex;
use btls_aux_unix::CmsgParser;
use btls_aux_unix::FileDescriptor;
use btls_aux_unix::FileDescriptorB;
use btls_aux_unix::OsError;
use btls_aux_unix::Path as UnixPath;
use btls_aux_unix::PollFd;
use btls_aux_unix::PollFlags;
use std::borrow::Cow;
use std::collections::HashSet;
use std::default::Default;
use std::env::var;
use std::fmt::Display;
use std::fs::remove_file;
use std::io::Error as IoError;
use std::io::Read as IoRead;
use std::io::stderr;
use std::io::Write as IoWrite;
use std::mem::drop;
use std::mem::MaybeUninit;
use std::net::TcpStream;
use std::os::unix::fs::MetadataExt;
use std::os::unix::io::AsRawFd;
use std::os::unix::io::FromRawFd;
use std::os::unix::net::SocketAddr as UnixSocketAddr;
use std::os::unix::net::UnixDatagram;
use std::os::unix::net::UnixListener;
use std::os::unix::net::UnixStream;
use std::path::Path;
use std::str::FromStr;
use std::sync::Arc;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering;
use std::thread::Builder;
use std::thread::JoinHandle;
use std::thread::sleep;
use std::time::Duration;
use std::time::Instant;


struct TlsState
{
}

impl TlsCallbacks for TlsState
{
}

fn do_maybe_connection_fault(c: &ServerConnection)
{
	if let Some(reason) = c.get_error() {
		errmsg!("tls-standalone-old: TLS handshake failed: {reason}");
	}
}


struct StderrLog;
impl Logging for StderrLog
{
	fn message(&self, _: MessageSeverity, x: Cow<'static, str>)
	{
		//Just discard errors.
		//Prepend debug prefix to each message line in order to not have these lines interpretted as
		//a failure.
		//FIXME: These should not go into stderr.
		for line in x.lines() { writeln!(stderr(), "Debug: {line}").ok(); }
	}
}

trait Stream: IoRead+IoWrite+Send
{
	fn t_as_raw_fd(&self) -> i32;
	fn t_set_read_timeout(&mut self, to: Option<Duration>) -> Result<(), IoError>;
	fn t_set_write_timeout(&mut self, to: Option<Duration>) -> Result<(), IoError>;
}

impl Stream for UnixStream
{
	fn t_as_raw_fd(&self) -> i32 { self.as_raw_fd() }
	fn t_set_read_timeout(&mut self, to: Option<Duration>) -> Result<(), IoError> { self.set_read_timeout(to) }
	fn t_set_write_timeout(&mut self, to: Option<Duration>) -> Result<(), IoError> { self.set_write_timeout(to) }
}

impl Stream for TcpStream
{
	fn t_as_raw_fd(&self) -> i32 { self.as_raw_fd() }
	fn t_set_read_timeout(&mut self, to: Option<Duration>) -> Result<(), IoError> { self.set_read_timeout(to) }
	fn t_set_write_timeout(&mut self, to: Option<Duration>) -> Result<(), IoError> { self.set_write_timeout(to) }
}

fn connection_thread(mut stream: impl Stream, tls_config: ServerConfiguration)
{
	let rawstream = FileDescriptorB::new(stream.t_as_raw_fd());
	let state = Arc::new(Mutex::new(TlsState {
	}));
	let mut connection = tls_config.make_connection(Box::new(state));
	stream.t_set_read_timeout(Some(Duration::from_secs(10))).ok();
	stream.t_set_write_timeout(Some(Duration::from_secs(10))).ok();
	let dl = Instant::now() + Duration::from_secs(15);
	let mut permanently_unredable = false;
	while !connection.get_status().is_dead() && dl > Instant::now() {
		let readable = wait_for_read(rawstream, 0);
		//If we have permanently_unredable and !want_transmit, then exit this loop, because no further
		//progress will happen.
		if permanently_unredable && !connection.get_status().is_want_tx() { break; }
		if !permanently_unredable && readable && connection.get_status().is_want_rx() {
			let tls_error = |err:btls::Error|errmsg!("tls-standalone-old: TLS transport error: {err}");
			let mut pbuf = [0u8;4096];
			let amt = match stream.read(&mut pbuf) {
				Ok(0) => {
					if let Err(err) = connection.push_tcp_data(PushTcpBuffer::end()) {
						return tls_error(err);
					}
					permanently_unredable = true;
					continue;
				},
				Ok(amt) => amt,
				Err(err) => return errmsg!("tls-standalone-old: TCP transport error: {err}"),
			};
			match connection.push_tcp_data(PushTcpBuffer::u8_size(&pbuf, amt)) {
				Err(err) => return tls_error(err),
				Ok((tmp,_)) => if tmp.len() > 0 {
					return errmsg!("tls-standalone-old: ACME TLS connection produced data?");
				}
			}
			do_maybe_connection_fault(&connection);
		} else if connection.get_status().is_want_tx() {
			match connection.pull_tcp_data(PullTcpBuffer::no_data()) {
				Ok(buf) => if let Err(err) = stream.write_all(&buf).and_then(|_|stream.flush()) {
					return errmsg!("tls-standalone-old: TCP transport error: {err}");
				},
				Err(err) => return errmsg!("tls-standalone-old: TLS transport error: {err}")
			}
			do_maybe_connection_fault(&connection);
		} else {
			//Wait a bit.
			sleep(Duration::new(0, 100000000));
		}
	}
	if dl <= Instant::now() { return errmsg!("tls-standalone-old: TLS handshake timed out"); }
}

fn wait_for_read(fd: FileDescriptorB, timeout: i32) -> bool
{
	let mut p = [PollFd {
		fd: fd,
		events: PollFlags::IN,
		revents: Default::default()
	}];
	let status = unsafe{PollFd::poll(&mut p, timeout)};
	//Event?
	if status.is_err() || !p[0].revents.is_in() { return false; }
	true
}

fn spawn_connection_thread(stream: impl Stream+'static, tls_config: &ServerConfiguration)
{
	let builder = Builder::new();
	let builder = builder.name("TLS connection".to_owned());
	let tls_config = tls_config.clone();
	if let Err(err) = builder.spawn(||connection_thread(stream, tls_config)) {
		errmsg!("tls-standalone-old: Failed to spawn thread: {err}");
	}
}

trait AcceptsConnections: Send
{
	type Connection: Stream;
	type Address: Sized;
	type Error: Display;
	fn t_as_raw_fd(&self) -> i32;
	fn t_accept(&mut self) -> Result<(<Self as AcceptsConnections>::Connection,
		<Self as AcceptsConnections>::Address), <Self as AcceptsConnections>::Error>;
}

impl AcceptsConnections for UnixListener
{
	type Connection = UnixStream;
	type Address = UnixSocketAddr;
	type Error = IoError;
	fn t_as_raw_fd(&self) -> i32 { self.as_raw_fd() }
	fn t_accept(&mut self) -> Result<(UnixStream, UnixSocketAddr), IoError> { self.accept() }
}

impl AcceptsConnections for UnixDatagram
{
	type Connection = TcpStream;
	type Address = ();
	type Error = IoError;
	fn t_as_raw_fd(&self) -> i32 { self.as_raw_fd() }
	fn t_accept(&mut self) -> Result<(TcpStream, ()), IoError>
	{
		let gate = FileDescriptorB::new(self.as_raw_fd());
		let mut control = [MaybeUninit::uninit();128];	//Should be big enogugh
		let mut control = CmsgParser::new2(&mut control);
		let mut buffer = [0;128];
		//Unsafety is having the gate open.
		let buffer = match unsafe{gate.recvmsg(false, &[&mut buffer[..]], Some(&mut control),
			Default::default())} {
			Ok((len, _)) => &buffer[..len],
			Err(e) => return Err(IoError::from_raw_os_error(e.to_inner()))
		};
		let mut fd = -1;
		control.iterate(|mtype, payload|{
			if false { return Err(()); }		//Type inference.
			use ::btls_aux_unix::CmsgKind;
			if mtype == CmsgKind::SCM_RIGHTS {
				//Ok, pick the file descriptors.
				for i in 0..payload.len() / 4 {
					let mut fdx = [0u8;4];
					fdx.copy_from_slice(&payload[4*i..4*i+4]);
					let fdx = i32::from_le_bytes(fdx);
					if fd < 0 {
						fd = fdx;
					} else {
						//No space for this, close the fd.
						unsafe{FileDescriptor::new(fdx)};
					}
				}
			}
			Ok(())
		}).ok();
		//Check we got a file descriptor and that connection type is "ACME".
		if fd < 0 { return Err(IoError::from_raw_os_error(OsError::ENODEV.to_inner())); }
		if buffer != b"ACME" {
			unsafe{FileDescriptor::new(fd)};
			return Err(IoError::from_raw_os_error(OsError::EAFNOSUPPORT.to_inner()));
		}
		//Cast this into TCP stream. The unsafety is having the connection open, which it is.
		Ok((unsafe{TcpStream::from_raw_fd(fd)}, ()))
	}
}

enum Listeners
{
	Direct(UnixListener),
	Tpgate(UnixDatagram),
}

fn listener_thread(mut listen: impl AcceptsConnections+'static, exit_sig: Arc<AtomicBool>, exit_ack: Arc<AtomicBool>,
	tls_config: ServerConfiguration)
{
	let rawlisten = FileDescriptorB::new(listen.t_as_raw_fd());
	while !exit_sig.load(Ordering::SeqCst) {
		if !wait_for_read(rawlisten, 100) { continue; }
		let connection = match listen.t_accept() {
			Ok((x, _)) => x,
			Err(err) => {
				errmsg!("tls-standalone-old: Failed to accept connection: {err}");
				continue;
			}
		};
		spawn_connection_thread(connection, &tls_config);
	}
	drop(listen);
	exit_ack.store(true, Ordering::SeqCst);
}

fn spawn_listener_thread(listener: impl AcceptsConnections+'static, exit_sig: &Arc<AtomicBool>,
	tls_config: &ServerConfiguration, threads: &mut Vec<(JoinHandle<()>, Arc<AtomicBool>)>) ->
	Result<(), AuthHandle>
{
	let exit_sig = exit_sig.clone();
	let ack = Arc::new(AtomicBool::new(false));
	let ack2 = ack.clone();
	let builder = Builder::new();
	let builder = builder.name("TLS listener".to_owned());
	let tls_config = tls_config.clone();
	match builder.spawn(||listener_thread(listener, exit_sig, ack, tls_config)) {
		Ok(t) => threads.push((t, ack2)),
		Err(err) => return Err(AuthHandle::new_failed(format!("Failed to spawn: {err}")))
	};
	Ok(())
}

struct AuthHandle
{
	error: Option<String>,
	threads: Vec<(JoinHandle<()>, Arc<AtomicBool>)>,
	exit_sig: Arc<AtomicBool>,
	tls_config: ServerConfiguration,
	acme_challenges: HashSet<String>,
}

impl AuthHandle
{
}

impl Drop for AuthHandle
{
	fn drop(&mut self)
	{
		self.exit_sig.store(true, Ordering::SeqCst);
		while self.threads.len() > 0 {
			//If flag is set, remove thread.
			if (self.threads[0].1).load(Ordering::SeqCst) {
				self.threads.remove(0);
			} else {
				sleep(Duration::new(0, 100000000));
			}
		}
	}
}

impl AuthHandle
{
	fn new_failed(error: String) -> AuthHandle
	{
		AuthHandle {
			error: Some(error),
			//Just some valid values. However, exit_ack must be
			threads: Vec::new(),
			exit_sig: Arc::new(AtomicBool::new(true)),
			tls_config: ServerConfiguration::from(CertificateLookup::null()),
			acme_challenges: HashSet::new(),
		}
	}
}

fn check_dir_perms(path: &str) -> Result<(), String>
{
	let path = Path::new(path);
	let path = path.parent();
	//If path was local, then parent is "".
	let path = if path.map(|x|x.as_os_str().len() == 0).unwrap_or(true) {
		Path::new(".")
	} else {
		path.unwrap()	//This must be some()
	};
	let metadata = path.metadata().map_err(|err|{
		format!("Failed to stat {path}: {err}", path=path.display())
	})?;
	if metadata.mode() & 2 != 0 {
		return Err(format!("Critial error: {path} is world-writable", path=path.display()));
	}
	Ok(())
}

impl Instance for AuthHandle
{
	fn new(config: &str) -> Self
	{
		let mut tls_config = ServerConfiguration::from(CertificateLookup::null());
		if let Some(dbg) = var("TLS_ALPN_DEBUG").ok().and_then(|v|u64::from_str(&v).ok()) {
			tls_config.set_debug(dbg, Box::new(StderrLog));
		}
		let mut listeners = Vec::new();
		for spec in config.split('!') {
			let (is_tpgate, spec) = if spec.starts_with("tpgate:") {
				(true, &spec[7..])
			} else {
				(false, spec)
			};
			if let Err(e) = check_dir_perms(spec) { return AuthHandle::new_failed(e); }
			remove_file(spec).ok();	//In case it exists.
			let ret = if is_tpgate {
				UnixDatagram::bind(spec).map(|l|listeners.push(Listeners::Tpgate(l)))
			} else {
				UnixListener::bind(spec).map(|l|listeners.push(Listeners::Direct(l)))
			};
			if let Err(err) = ret {
				return AuthHandle::new_failed(format!("Failed to listen {spec}: {err}"));
			}
			//Force 777 mode so anything can connect.
			UnixPath::new(spec.as_bytes()).and_then(|p|p.chmod(511).ok());
		}
		if listeners.len() == 0 { return AuthHandle::new_failed(format!("No addresses to listen")); }
		//Fire a thread per listener.
		let mut threads = Vec::new();
		let exit_sig = Arc::new(AtomicBool::new(false));
		for listener in listeners.drain(..) { if let Err(e) = match listener {
			Listeners::Direct(l) => spawn_listener_thread(l, &exit_sig, &tls_config, &mut threads),
			Listeners::Tpgate(l) => spawn_listener_thread(l, &exit_sig, &tls_config, &mut threads),
		} {
			return e;
		}}
		AuthHandle {
			error: None,
			threads: threads,
			exit_sig: exit_sig,
			tls_config: tls_config,
			acme_challenges: HashSet::new(),
		}
	}
}
impl DynInstance for AuthHandle
{
	fn add_challenge(&mut self, ch: &ChallengeInfo) -> Result<MethodId, String>
	{
		if let Some(error) = self.error.as_ref() { return Err(error.clone()); }
		let httpid = ch.method_id(TLS_ALPN_01).ok_or_else(||format!("tls-alpn-01 method not supported"))?;
		let filename = ch.attribute(TLSALPN01_SNI).
			ok_or_else(||format!("tls-alpn-01 method not supported"))?;
		let content = ch.attribute(TLSALPN01_KEY_AUTHORIZATION).
			ok_or_else(||format!("tls-alpn-01 method not supported"))?;
		//Lowercase the filename.
		let filename = filename.to_ascii_lowercase();
		//Commit.
		self.tls_config.create_acme_challenge(&filename, content);
		trace!("tls-standalone-old: Provisioned challenge for {filename}");
		self.acme_challenges.insert(filename);
		Ok(httpid)
	}
	fn commit(&mut self) -> Result<(), String>
	{
		//Commit is synchronous, so no delay.
		Ok(())
	}
	fn name(&self) -> String { "tls-standalone-old".to_string() }
	fn clear(&mut self) -> Duration
	{
		for filename in self.acme_challenges.drain() {
			trace!("tls-standalone: Unprovisioned challenge for {filename}");
			self.tls_config.delete_acme_challenge(&filename);
		}
		Duration::from_secs(0)		//No need to wait for any caches.
	}
}

authmod_instance!(AuthHandle);
