#![warn(unsafe_op_in_unsafe_fn)]
#[macro_use] extern crate acmeclient_authmod_helper;
#[macro_use] extern crate btls_aux_fail;
use acmeclient_authmod_helper::ChallengeInfo;
use acmeclient_authmod_helper::DynInstance;
use acmeclient_authmod_helper::HTTP_01;
use acmeclient_authmod_helper::HTTP01_CONTENT;
use acmeclient_authmod_helper::HTTP01_FILENAME;
use acmeclient_authmod_helper::Instance;
use acmeclient_authmod_helper::MethodId;
use std::fs::File;
use std::fs::remove_file;
use std::io::Write;
use std::path::Path;
use std::path::PathBuf;
use std::thread::sleep;
use std::time::Duration;


pub struct AuthHandle
{
	basepath: PathBuf,
	tempfiles: Vec<PathBuf>,
	contents: Vec<(String, String)>,
}

impl Drop for AuthHandle
{
	fn drop(&mut self) { self.clear(); }
}

impl Instance for AuthHandle
{
	fn new(config: &str) -> Self
	{
		AuthHandle{
			basepath: Path::new(config).to_path_buf(),
			tempfiles: Vec::new(),
			contents: Vec::new(),
		}
	}
}
impl DynInstance for AuthHandle
{
	fn add_challenge(&mut self, ch: &ChallengeInfo) -> Result<MethodId, String>
	{
		let httpid = ch.method_id(HTTP_01).ok_or_else(||format!("http-01 method not supported"))?;
		let filename = ch.attribute(HTTP01_FILENAME).
			ok_or_else(||format!("http-01 method not supported"))?;
		let content = ch.attribute(HTTP01_CONTENT).
			ok_or_else(||format!("http-01 method not supported"))?;
		//Push the names and contents to queue just to show use of commit() method.
		self.contents.push((filename.to_owned(), content.to_owned()));
		Ok(httpid)
	}
	fn commit(&mut self) -> Result<(), String>
	{
		//Create tempfiles for all names and values mentioned in added challenges.
		for (filename, content) in self.contents.drain(..) {
			let fpath = self.basepath.join(filename);
			File::create(&fpath).and_then(|mut fp|{
				fp.write_all(content.as_bytes())
			}).map_err(|err|{
				format!("Failed to write {fpath}: {err}", fpath=fpath.display())
			})?;
			trace!("webroot: Wrote file {fpath}", fpath=fpath.display());
			self.tempfiles.push(fpath);
		}
		sleep(Duration::from_secs(1));	//Wait for reload.
		Ok(())
	}
	fn name(&self) -> String { "webroot".to_string() }
	fn clear(&mut self) -> Duration
	{
		//Delete all temporary files, and clear contents (not that there should be any entries in
		//contents).
		for i in self.tempfiles.drain(..) {
			if let Err(err) = remove_file(&i) {
				errmsg!("webroot: Failed to remove tempfile {i}: {err}", i=i.display());
			} else {
				trace!("webroot: Removed file {i}", i=i.display());
			}
		}
		self.contents.clear();
		Duration::from_secs(0)		//No need to wait for any caches.
	}
}

authmod_instance!(AuthHandle);

fn check_dir_perms(path: &Path) -> Result<(), String>
{
	use std::os::unix::fs::MetadataExt;
	let metadata = path.metadata().map_err(|err|{
		format!("Failed to stat {path}: {err}", path=path.display())
	})?;
	fail_if!(!metadata.is_dir(), format!("Path {path} is not a directory", path=path.display()));
	fail_if!(metadata.mode() & 2 != 0, format!("Critial error: {path} is world-writable", path=path.display()));
	Ok(())
}

pub struct Factory
{
	pub path: PathBuf,
}

impl Factory
{
	pub fn make(self) -> Result<AuthHandle, String>
	{
		let path = self.path;
		check_dir_perms(&path)?;
		Ok(AuthHandle {
			basepath: path,
			tempfiles: Vec::new(),
			contents: Vec::new(),
		})
	}
}
