#ifndef _acme_auth_module_h_included_
#define _acme_auth_module_h_included_

#include <stdlib.h>

struct raw_string
{
	//NUL terminated.
	const char* base;
	//Number of bytes.
	size_t size;
};

struct auth_challenge_attribute
{
	//Name of the attribute.
	//
	//The currently known attributes:
	// - THUMBPRINT: The ACME account thumbprint.
	// - HTTP01_TOKEN: The HTTP-01 challenge token
	// - HTTP01_FILENAME: The filename the challenge should be written to.
	// - HTTP01_URL: The URL to provision the content into.
	// - HTTP01_CONTENT: The content to write into the validation file.
	// - TLSALPN01_TOKEN: The TLS-ALPN-01 challenge token.
	// - TLSALPN01_SNI: The SNI value that will be sent for TLS-ALPN-01 challenge.
	// - TLSALPN01_KEY_AUTHORIZATION: The key authorization for TLS-ALPN-01.
	// - TLSALPN01_RESPONSE: Hexadecimal encoding of the TLS-ALPN-01 challenge response.
	// - DNS01_TOKEN: The DNS-01 challenge token.
	// - DNS01_OWNER_NAME: The owner name for the DNS TXT record.
	// - DNS01_KEY_AUTHORIZATION: The key authorization for DNS-01.
	// - DNS01_RESPONSE: The raw response to write into DNS TXT record.
	struct raw_string name;
	//Value of the attribute.
	struct raw_string value;
};

struct auth_challenge_method
{
	//Name of the method.
	struct raw_string name;
};

struct auth_challenge_info
{
	//The kind of identifier.
	struct raw_string kind;
	//The identifier name.
	struct raw_string name;
	//Wildcard flag.
	int wildcard;
	//Attributes. Ended by an attribute with NULL name.
	const struct auth_challenge_attribute* attributes;
	//Number of attributes.
	size_t nattributes;
	//Methods. Ended by an methods with NULL name.
	const struct auth_challenge_method* methods;
	//Number of methods.
	size_t nmethods;
};

struct error_context_struct;

struct auth_instance_handle
{
	//Version number.
	//Initially, this is maximum supported version. 0xFFFFFFFF means only version 0 is supported.
	//After call, this must be set to 0.
	size_t version;
	//context for calls, assigned by the module.
	void* context;
	//Callback to call when destroying the handle. Assigned by the module.
	void (*destroy)(void* context);
	//Callback to call when adding a challenge. Return index of the chosen method (from methods array) on
	//sucess, -1 on failure.  Assigned by the module.
	int (*add_challenge)(void* context, struct auth_challenge_info* challenge);
	//Commit the challenges added. Return 0 on success, -1 on failure.  Assigned by the module.
	int (*commit)(void* context);
	//Set error message from this module. This is provoded.
	void (*error)(struct error_context_struct* ctx, const char* message);
	//The context to pass to the error callback. This is provoded.
	struct error_context_struct* error_context;
};

//Initialize an instance handle.
void certtool_acmeclient_auth_open(struct auth_instance_handle* handle, const char* config, size_t configsize);

#endif
