#!/usr/bin/python3
from sys import argv;
from math import log,ceil;

NAMES=[
	"Very bad",				#0
	"Blacklisted",
	"GREASE",
	"Private Use",
	"Reserved",
	"Unassigned",				#5
	"SCSV",
	"KEX: Static RSA",
	"KEX: Non-Ephemeral Diffie-Hellman",
	"KEX: Pre-Shared Key",
	"KEX: Diffie-Hellman",			#10
	"KEX: Elliptic-Curve Diffie-Hellman",
	"KEX: Kerberos5",
	"KEX: Secure Remote Password 6a",
	"KEX: Dragonfly",
	"AUTH: NULL",				#15
	"AUTH: Pre-Shared Key",
	"AUTH: RSA signature",
	"AUTH: DSS signature",
	"AUTH: ECDSA signature",
	"MAC: MD5",				#20
	"MAC: 64 bit",
	"Encryption: NULL",
	"Encryption: RC4",
	"Encryption: IDEA",
	"Encryption: Triple-DES",		#25
	"Encryption: AES-CBC",
	"Encryption: Camellia-CBC",
	"Encryption: SEED",
	"Encryption: AES-GCM",
	"Encryption: ARIA-CBC",			#30
	"Encryption: ARIA-GCM",
	"Encryption: Camellia-GCM",
	"Encryption: AES-CCM",
	"Encryption: Chacha20",
	"Encryption: SM4",			#35
	"Encryption: Kuznyechik",
	"Encryption: Magma",
	"Group: Binary curve",
	"Group: NIST P-256",
	"Group: NIST P-384",			#40
	"Group: NIST P-521",
	"Group: Brainpool",
	"Group: Curve25519",
	"Group: Curve448",
	"Group: GOST",				#45
	"Group: SM2",
	"Group: Finite-Field",
	"Group: Custom elliptic curve",
	"Signature: MD5",
	"Signature: SHA-1",			#50
	"Signature: SHA-224",
	"Signature: RSA",
	"Signature: DSS",
	"Signature: ECDSA",
	"Signature: GOST",			#55
	"Signature: RSA (PSS dedicated key)",
	"Signature: SM3",
	"Signature: IBC (China)",
	"Signature: Ed25519",
	"Signature: Ed448",			#60
	"Export(!) crypto",
	"Total sample",
	"TLS 1.3",
	"KEX: GOST",
	"Modern cipher",			#65
	"Modern group",
	"Modern signature",
	"Encryption: Old Chacha",
	"Total sample (#2)",
];

fp=open(argv[1],"rb");
content=fp.read();

def rightpad(s,n):
	padding = " "*(n-len(s));
	return s+padding;
def leftpad(s,n):
	s = str(s);
	padding = " "*(n-len(s));
	return padding+s;

maxlen = 0;
maxmag = 0;
numerics = [];
for i in range(0,len(NAMES)):
	maxlen = max(maxlen, len(NAMES[i]));
	entry = content[8*i:8*i+8];
	entry = int.from_bytes(entry, byteorder='little');
	numerics.append(entry);
	if entry > 0:
		maxmag = max(maxmag, ceil(log(entry)/log(10)));

total = numerics[62];
total2 = numerics[69];
for i in range(0,len(NAMES)):
	n = rightpad(NAMES[i],maxlen+3);
	v = leftpad(numerics[i],maxmag);
	if i <= 67 and i != 62:
		print("{} {} ({:.3f}%)".format(n,v,100*numerics[i]/total));
	elif i >= 68 and i <= 68:
		print("{} {} ({:.3f}%)".format(n,v,100*numerics[i]/total2));
	else:
		print("{} {} (---)".format(n,v));
